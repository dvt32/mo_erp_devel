### Top-level Makefile for Micro Office code base

# export all variables set in system.properties file to sub-make instances
.EXPORT_ALL_VARIABLES:

# list targets that are not real files
.PHONY: help

# repo root -- this is the prefix for all subdirs
repo_root := $(CURDIR)

# global variables like linux_distribution
include $(repo_root)/common/global.mk

# supported distributions are centos and debian
supported_distro := centos debian
# other distributions are bad
bad_distribution := $(filter-out $(supported_distro),$(linux_distribution))

# if bad_distribution is not empty, then there is an unsupported distribution and we should throw an error
ifneq ($(bad_distribution),)
	$(error supported distributions: $(supported_distro); bad Linux distribution: $(bad_distribution))
endif

# list all targets in 4 columns
# third -e to grep has a tab
help:
	@$(MAKE) --print-data-base --question no-such-target | grep -v -e '^no-such-target' -e '^Makefile' -e '^	:' | awk '/^[^.%][-A-Za-z0-9_]*:/ { print substr($$1, 1, length($$1)-1) }' | sort | pr --omit-pagination --width=80 --columns=4

# dummy target for help target to point to something
no-such-target:

# pull in system.properties variables so they are visible to this make instance and sub-make instances
-include system.properties

system_properties := $(CURDIR)/system.properties

# inhibit creation of __pycache__ and .py[co] files
PYTHONDONTWRITEBYTECODE := anything

# directory to hold generated scripts, etc.
genbuild := $(CURDIR)/build

# packages needed for build process (no need to include make since we cannot run Makefile without make)
ifeq ($(linux_distribution),debian)
build_packages := debconf-utils locales python3 python3-jinja2 rsync patch sed mawk xz-utils
else ifeq ($(linux_distribution),centos)
build_packages := rsync patch sed mawk xz python34 python3-jinja2
endif

# packages that need to be installed for the sites to work
site_packages := libarchive-zip-perl libdate-calc-perl \
        libdbd-pg-perl libemail-mime-encodings-perl \
        libemail-mime-perl libencode-perl libgd-perl \
        libhtml-entities-numbered-perl libhtml-html5-entities-perl \
        libhttp-dav-perl libio-file-withpath-perl libmail-sendmail-perl \
        libmime-base64-urlsafe-perl libmime-tools-perl librpc-xml-perl \
        libsoap-lite-perl libtext-csv-perl \
        libxml-libxslt-perl liblingua-en-nums2words-perl \
	libjson-perl libjson-xs-perl libjson-pp-perl \
	libwww-perl \
        perl-base perl \
        php-tcpdf libsodium18 libsodium-dev \
        php-excel-codeplex \
        python3 python3-gpgme python3-psycopg2
# for payment processing with Payeezy
site_packages += libsoap-lite-perl libdatetime-format-iso8601-perl libxml-tidy-perl libdbi-perl libdbd-pg-perl libjson-perl

# if use_systemd is defined in system.properties, use that; otherwise, try to autodetect
ifeq ($(use_systemd),)
  use_systemd := $(wildcard /bin/systemd)
endif

# database-related variables
ifeq ($(linux_distribution),debian)
# Debian 8
psql_package := postgresql-10 postgresql-plperl-10 postgresql-plpython-10 postgresql-plpython3-10 libjson-perl libjson-xs-perl libjson-pp-perl postgresql-contrib-10
else ifeq ($(linux_distribution),centos)
# CentOS 7
psql_package := postgresql10-server postgresql10-plperl postgresql10-plpython perl-JSON perl-JSON-XS
endif
psycopg2_package := python3-psycopg2
psql_conf_dir := /etc/postgresql/10/main

# web-related variables
web_packages := apache2 libapache2-mod-php5 postfix
apache_mods := cgi dav_fs dav dav_lock php5 rewrite headers auth_form session session_cookie session_crypto request

# database details whose version is associated with git commit
database_sample_base_url := http://devrc.microoffice.com/mo_erp/dbsample
main_database_sample_global_file := master-20150218-global.sql
main_database_sample_data_file := master-20150519-data.pg_dump.xz
main_database_uncompressed_sample_data_file := $(main_database_sample_data_file:.xz=)
dbsample_required_vars := operations_existing_employee_id operations_employee_first_name operations_employee_last_name operations_employee_logon operations_employee_password database_sample_base_url main_database_sample_global_file main_database_sample_data_file database_cache_dir

# db patch file suffix depends on where we are
ifeq ($(is_production_deployment),1)
export patch_suffix := prod
else
export patch_suffix := dev
endif

### common included Makefiles

include $(repo_root)/common/func.mk
include $(repo_root)/common/apache.mk
include $(repo_root)/common/postgresql.mk

### misc rules

all:	setup
	@echo all


setup:	system_properties_check microoffice_repo buildtools environment

builddir:
	@-mkdir -p $(genbuild)

install-build-packages:
	@echo ..install build packages: $(build_packages)
	@$(call install_packages,$(build_packages))

buildtools:	microoffice_repo
	@$(call install_packages,$(build_packages))
	@$(call install_packages,$(site_packages))

environment:
ifeq ($(linux_distribution),centos)
# check if time zone has been configured, and if not, configure it
	@if [ ! -e /etc/localtime ]; then \
	  /bin/cp -f /usr/share/zoneinfo/America/New_York /etc/localtime; \
	fi
# check if locale has been configured, and if not, configure it
	@if [ ! -s /etc/locale.conf ]; then \
	  localectl set-locale LANG=en_US.UTF-8; \
	fi
else ifeq ($(linux_distribution),debian)
# check if time zone has been configured, and if not, configure it
	@if [ ! -s /etc/timezone ] || [ `grep -c $(tz_area)/$(tz_zone) /etc/timezone` -eq 0 ]; then \
		echo tzdata tzdata/Areas select $(tz_area) | debconf-set-selections; \
		echo tzdata tzdata/Zones/$(tz_area) select $(tz_zone) | debconf-set-selections; \
		echo "$(tz_area)/$(tz_zone)" > /etc/timezone; \
		dpkg-reconfigure -f noninteractive tzdata; \
	fi
# check if locale has been configured, and if not, configure it
	@if [ ! -s /etc/locale.gen ] || [ `grep -c ^en_US.UTF-8 /etc/locale.gen` -eq 0 ]; then \
		echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections; \
		echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections; \
		update-locale LANG=en_US.UTF-8; \
		dpkg-reconfigure -f noninteractive locales; \
	fi
endif

curl_pkg:
	@$(call install_packages,curl)

system_properties_check:
	@if [ ! -f $(system_properties) ]; then \
		echo ""; \
		echo "do: cp tools/misc/system.properties.sample system.properties"; \
		echo "then: customize the file for your setup."; echo ""; \
		exit 1; \
	fi

### repo

microoffice_repo: curl_pkg
# add microoffice repo to apt/yum repo list if not already there.
ifeq ($(linux_distribution),centos)
# check that /etc/yum.repos.d/microoffice.repo has size > 0 and has text yumrepo.microoffice.com in content.
# if not, create it and run yum makecache
	@:; \
	MOREPO=/etc/yum.repos.d/microoffice.repo; \
	if [ ! -s $$MOREPO ] || [ `grep -c yumrepo.microoffice.com $$MOREPO` -eq 0 ]; then \
		echo creating $$MOREPO and running yum makecache; \
		rm -f $$MOREPO; \
		cp $(CURDIR)/tools/repo/microoffice.repo $$MOREPO; \
		yum makecache; \
	fi
else ifeq ($(linux_distribution),debian)
# check that /etc/apt/sources.list.d/microoffice.list has size > 0 and has text aptrepo.microoffice.com in content.
# if not, create it and run apt-get update.
#	@:; \
#	MOREPO=/etc/apt/sources.list.d/microoffice.list; \
#	if [ ! -s $$MOREPO ] || [ `grep -c aptrepo.microoffice.com $$MOREPO` -eq 0 ]; then \
#		echo creating $$MOREPO and running apt-get update; \
#		echo 'deb http://aptrepo.microoffice.com/apt/debian jessie main' > $$MOREPO; \
#		curl -s http://aptrepo.microoffice.com/apt/debian/microoffice.gpg.key | apt-key add -; \
#		apt-get update; \
#	fi
else
	$(error unsupported distribution in microoffice_repo rule: $(linux_distribution))
endif


### database

dbsetup: setup postgresql_repo
	@:; \
	$(call install_packages,$(psql_package))
	@if [ -z "$(use_systemd)" ] || [ "$(use_systemd)" = "0" ]; then \
		update-rc.d postgresql defaults; \
		update-rc.d postgresql enable; \
	else \
		if [ ! -e "/etc/systemd/system/multi-user.target.wants/postgresql.service" ]; then \
			systemctl enable postgresql; \
		fi; \
	fi
ifeq ($(is_production_deployment),1)
# if deploying on production, configure DB for SSL with certificates and key and make it listen on all interfaces.
# Also save root.crt, server.key, and server.crt in /var/lib/pgsql/10/data and save ssh key in /var/lib/pgsql/.ssh.
# secret_base_dir is outside the source tree because it contains files that should remain secret except on production.
ifeq ($(secret_base_dir),)
	$(error dbsetup requires secret_base_dir on production server (i.e. when is_production_deployment is 1))
else
	@$(call check_file_exist,$(secret_base_dir)/dbconf/root.crt)
	@$(call check_file_exist,$(secret_base_dir)/dbconf/server.key)
	@$(call check_file_exist,$(secret_base_dir)/dbconf/server.crt)
	@$(call check_file_exist,$(secret_base_dir)/dbconf/id_ecdsa)
	@$(call check_file_exist,$(secret_base_dir)/dbconf/id_ecdsa.pub)
endif
	cp -a $(secret_base_dir)/dbconf/root.crt $(secret_base_dir)/dbconf/server.key $(secret_base_dir)/dbconf/server.crt /var/lib/pgsql/10/data/
	mkdir -p /var/lib/pgsql/.ssh
	chmod 0700 /var/lib/pgsql/.ssh
	cp -a $(secret_base_dir)/dbconf/id_ecdsa $(secret_base_dir)/dbconf/id_ecdsa.pub /var/lib/pgsql/.ssh/
	chown postgres.postgres /var/lib/pgsql/10/data/root.crt /var/lib/pgsql/10/data/server.key /var/lib/pgsql/10/data/server.crt
	chmod 0444 /var/lib/pgsql/10/data/root.crt /var/lib/pgsql/10/data/server.crt
	chmod 0400 /var/lib/pgsql/10/data/server.key
	chmod 0600 /var/lib/pgsql/.ssh/id_ecdsa
	chmod 0644 /var/lib/pgsql/.ssh/id_ecdsa.pub
else
	@:; \
	$(call install_packages,$(psycopg2_package))
endif
	@:; \
	MKTEMPDIR=`/bin/mktemp -d`; \
	$(call safe_patch,$(psql_conf_dir)/postgresql.conf,tools/patches/postgresql.10.conf.patch-$(patch_suffix),$$MKTEMPDIR/patch1); \
	$(call safe_patch,$(psql_conf_dir)/pg_hba.conf,tools/patches/pg_hba.conf.patch-$(patch_suffix),$$MKTEMPDIR/patch2); \
	: `# count number of patches applied`; \
	for patch_file in $$MKTEMPDIR/patch[12]; do num_patches=$$((num_patches+`cat $$patch_file`)); done; \
	rm -f $$MKTEMPDIR/patch[12]; \
	rmdir $$MKTEMPDIR; \
	: `# if no patches applied, do not restart postgresql since it is unneeded`; \
	if [ $$num_patches -gt 0 ]; then \
		if [ -z "$(use_systemd)" ] || [ "$(use_systemd)" = "0" ]; then \
			service postgresql restart; \
		else \
			systemctl restart postgresql; \
		fi; \
	fi
	@psql -d postgres -U postgres < $(CURDIR)/database/ddl/create_function.sql


dbsample:	dbsetup curl_pkg
ifeq ($(is_production_deployment),1)
	$(error dbsample can only be run on development server (i.e. when is_production_deployment is 0))
else
	$(foreach var,$(dbsample_required_vars),$(if $(var),,$(error must specify $(var) when running dbsample)))
endif
	@ # create cache directory if does not already exist
	@-mkdir -p $(database_cache_dir)
	@ # if the cached global file does not exist, download it
ifeq ($(realpath $(database_cache_dir)/$(main_database_sample_global_file)),)
	curl -s $(database_sample_base_url)/$(main_database_sample_global_file) -o $(database_cache_dir)/$(main_database_sample_global_file)
endif
	@ # if the cached data file does not exist, download it and uncompress it
	@ # no silent because file is large and progress meter is helpful
ifeq ($(realpath $(database_cache_dir)/$(main_database_uncompressed_sample_data_file)),)
	curl $(database_sample_base_url)/$(main_database_sample_data_file) -o $(database_cache_dir)/$(main_database_sample_data_file)
	xz -d $(database_cache_dir)/$(main_database_sample_data_file)
endif
	@ # check if database instance already exists; if so, drop it
	@if [ ! "`/bin/echo '\a \t \x \l' | psql -d postgres -U postgres | grep '^Name|$(main_database_name)$$'`" = "" ]; then \
		dropdb -U postgres $(main_database_name); \
	fi
	@ # check if global data has been loaded. we use existence of mo_operator as test.
	@ # if loaded, then clear global data (currently just delete roles).
	@if [ "`/bin/echo '\du' | psql -d postgres -U postgres | grep mo_operator | awk '{print $$1;}'`" = "mo_operator" ]; then \
		psql -d postgres -U postgres < database/ddl/drop_roles.sql; \
	fi
	@ # now load roles from global data file.
	@ # obtain global database dump from source database using pg_dumpall -g > microoffice_global.sql.
	@psql -d postgres -U postgres < $(database_cache_dir)/$(main_database_sample_global_file)
	@ # create database
	@createdb -U $(main_database_create_as_username) $(main_database_name)
	@ # Obtain sample database dump from source database (pg_dump -Fc).
	psql -d $(main_database_name) -U $(main_database_create_as_username) -j $(dbsample_num_jobs) < $(database_cache_dir)/$(main_database_uncompressed_sample_data_file)
	@ # if employee information specified, make one of the scrambled rows use a known email and password.
	@ # only need to update logon, not password, because password is saved in htpasswd in websetup rule.
	/bin/echo "update employee_tbl set first_name = '$(operations_employee_first_name)', last_name = '$(operations_employee_last_name)', windows_logon_name = '$(operations_employee_logon)', allowed_ip_addr = NULL, is_active = TRUE where id = $(operations_existing_employee_id)" | psql -d $(main_database_name) -U $(main_database_create_as_username)
	@ # do the same for the cust_pers_tbl for bizcenter
	/bin/echo "update cust_pers_tbl set first_name = '$(bizcenter_cust_first_name)', last_name = '$(bizcenter_cust_last_name)', email = '$(bizcenter_cust_email)', is_active = TRUE where id = $(bizcenter_existing_cust_id)" | psql -d $(main_database_name) -U $(main_database_create_as_username)


dbscramble:	builddir
	@$(CURDIR)/tools/db/genscramble.py -p $(genbuild) -c $(system_properties) -g
	@env PYTHONPATH=:$(genbuild) $(CURDIR)/tools/db/scrambledb2.py -c $(system_properties)

dbrestore:
	@$(CURDIR)/tools/db/import_dev_db.sh
#	Alter mo_apply to have password and login priviledges
	echo "ALTER ROLE mo_apply WITH PASSWORD '$(apply_db_password)'; ALTER ROLE $(apply_db_username) WITH LOGIN" | sudo -u postgres psql -d $(apply_db_name)

### apache

websetup:	setup apache_pkg apache_start_on_boot ssl_certs web_postgresql_cert
	@:; \
	if [ -z `dpkg-query -l postfix | grep ^ii | awk '{print $$2;}'` ]; then \
		DO_POSTFIX_INSTALL=1; \
	fi; \
	if [ -z "`debconf-get-selections | grep 'postfix/mailname'`" ] && [ -s "/etc/mailname" ]; then \
		echo postfix postfix/mailname string `cat /etc/mailname` | debconf-set-selections; \
	fi; \
	if [ -z "`debconf-get-selections | grep 'postfix/main_mailer_type' | grep 'Internet Site'`" ]; then \
		echo postfix postfix/main_mailer_type select Internet Site | debconf-set-selections; \
	fi; \
	$(call install_packages,$(web_packages)); \
	if [ "$DO_POSTFIX_INSTALL" = "1" ]; then \
		dpkg-reconfigure -f noninteractive postfix; \
	fi
	@if [ -z "$(use_systemd)" ] || [ "$(use_systemd)" = "0" ]; then \
		update-rc.d postfix defaults; \
		update-rc.d postfix enable; \
	else \
		if [ -z "$(wildcard /etc/rc3.d/S*postfix)" ]; then \
			systemctl enable postfix; \
		fi; \
	fi
	@for apache_mod in $(apache_mods); do \
		if [ ! -e "/etc/apache2/mods-enabled/$$apache_mod.load" ]; then \
			a2enmod $$apache_mod; \
		fi; \
	done
	$(eval _quiet=1)
	@$(call safe_patch,/etc/apache2/mods-available/mime.conf,tools/patches/apache2-mime.conf.patch)
	$(eval _quiet=)

### crypto site

keysetup-crypto:
	@make -C crypto_site keysetup

websetup-crypto: system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C crypto_site websetup

install-crypto:
	@make -C crypto_site install

### payment processing server

setup-payment: install-build-packages
	@make -C payment setup

install-payment: install-build-packages
	@make -C payment install

# customer pay site

websetup-pay:	system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C pay_site websetup

install-pay:
	@$(call install_packages,unzip)
	@make -C pay_site install

clean-pay:
	@make -C pay_site clean


# Contact Form handling site

websetup-contact:	system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C contact_site websetup

install-contact:
	@$(call install_packages,unzip)
	@make -C contact_site install

clean-contact:
	@make -C contact_site clean


### static site
websetup-static:
	@$(call install_packages,$(build_packages))
	@make -C static_site websetup

install-static:
	@make -C static_site install

clean-static:
	@make -C static_site clean

### apply site
websetup-apply: system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C apply_site websetup

install-apply:
	@make -C apply_site install

clean-apply:
	@make -C apply_site clean

### Mailing server
setup-report: system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C report setup

install-report:
	@make -C report install

clean-report:
	@make -C report clean

### netauth site
websetup-netauth: system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C netauth_site websetup

install-netauth:
	@make -C netauth_site install

clean-netauth:
	@make -C netauth_site clean

### router site
websetup-router: system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C router websetup

install-router:
	@make -C router install

clean-router:
	@make -C router clean

websetup-transition: system_properties_check microoffice_repo environment
	@$(call install_packages,$(build_packages))
	@make -C transition_site websetup

install-transition:
	@make -C transition_site install

clean-transition:
	@make -C transition_site clean

### procmail server

setup-procmail:
	@make -C procmail setup

install-procmail:
	@make -C procmail install

clean-procmail:
	@make -C procmail clean

### freeswitch recording scripts
setup-freeswitch:
	@make -C tools setup-freeswitch

install-freeswitch:
	@make -C tools install-freeswitch

### web sites
websetup-%:
	@make -C $*_site websetup

make-%:
	@make -C $*_site

install-%:	setup websetup websetup-% make-% install-common
	@make -C $*_site install-extra
	@make -C $*_site install
	@echo install $*_site

clean-%:
	@make -C $*_site clean


### special case: common directory

make-common:
	@make -C common

install-common:	setup websetup make-common
	@make -C common install

clean-common:
	@make -C common clean

### end

from flask import request, abort, redirect, url_for, flash, render_template, g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, BooleanField, HiddenField, TextAreaField, RadioField
from wtforms.fields.html5 import EmailField
from wtforms_components import If
from wtforms import validators, ValidationError
import psycopg2
import psycopg2.extras
import tzconvert
import database
import datetime
import calendar
import smtplib
import credit_card
import random
import chargefacade
from chargefacade import ChargeMail
import decimal
from itsdangerous import URLSafeTimedSerializer
import jinja2
import re
from json import JSONEncoder

convenience_pct_str = '{{ pay_card_convenience_pct }}'
convenience_pct_multiplier = decimal.Decimal(convenience_pct_str) / decimal.Decimal(100)

class AccountMismatchException(Exception):
  """Basic exception for errors raised by shoes"""
  pass

def show_virtual_application():
  form = get_form()
  return render_template('virtual_apply_form.html', form=form)

# Lists of tuples for select fields expire_month and plan
month_choices = [(str(i), str(i) + ' - ' + calendar.month_abbr[i]) for i in range(1, 13)]

def not_null_check(form, field):
  if field.data == '':
    raise ValidationError('Please make a valid selection')

class ApplyVirtualForm(FlaskForm):
  email = EmailField('Email', validators=[validators.InputRequired(), validators.Regexp(r'^[a-zA-Z0-9.!#$%&*+\/=?^_`{|}~-]+@(?:\S{1,63})$', message='Please input a valid email address')], render_kw={'placeholder': 'Email', 'autocomplete': 'email'})
  pmt_method = RadioField('Select Payment Type', choices=[('card', 'Credit Card'), ('ach', "Bank Account")], default='card')
  bank_routing_number = StringField('Bank Routing Number', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='ach', validators.Regexp(r'\d{9}|\d+\*+\d+'))], render_kw={'maxlength':'9', 'placeholder': 'Routing number'})
  bank_account_number = StringField('Bank Account Number', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='ach', validators.Regexp(r'\d{4,17}|\d+\*+\d+'))], render_kw={'placeholder': 'Account number'})
  bank_account_type = SelectField('Account Type', choices=[('checking', 'Checking'), ('savings', 'Savings')], default='checking', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired())])
  bank_owned_by = SelectField('Account Ownership', choices=[('business', 'Business'), ('personal', 'Personal')], default='business', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired())])
  bank_registered_name = StringField('Business Name on Account', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='ach', validators.Length(max=64))], render_kw={'maxlength':'64', 'placeholder': 'Name on account'})
  card_first_name = StringField('First Name on Card', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'placeholder': 'First Name on Card', 'autocomplete': 'cc-given-name'})
  card_last_name = StringField('Last Name on Card', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'placeholder': 'Last Name on Card', 'autocomplete': 'cc-family-name'})
  card_company = StringField('Company on Card', validators=[validators.Optional()], render_kw={'placeholder': 'Company on Card'})
  card_number = StringField('Credit Card Number', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired()),If(lambda form, field: form.pmt_method.data=='card', validators.Regexp(r'^\d{15,16}$'))], render_kw={'autocomplete': 'cc-number', 'placeholder':'################', 'maxlength':'16'})
  card_expire_month = SelectField('Expiration Month', choices=month_choices, validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'cc-exp-month'})
  card_expire_year = StringField('Expiration Year', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired()),If(lambda form, field: form.pmt_method.data=='card', validators.Regexp(r'^\d{2}$'))], render_kw={'autocomplete': 'cc-exp-year', 'placeholder': 'YY', 'maxlength':'2'})
  card_cvv = StringField('CVV/CVC/CSC', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired()),If(lambda form, field: form.pmt_method.data=='card', validators.Regexp(r'^\d{3,4}|\*{3,4}$')),If(lambda form, field: form.pmt_method.data=='card', validators.Length(min=3,max=4))], render_kw={'autocomplete': 'cc-csc', 'placeholder': 'CVV', 'maxlength':'4'})
  card_billing_address_1 = StringField('Address Line 1', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'address-line1', 'placeholder': 'Address Line 1'})
  card_billing_address_2 = StringField('Address Line 2', render_kw={'autocomplete': 'address-line2', 'placeholder': 'Address Line 2' })
  card_billing_city = StringField('City', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'address-level2', 'placeholder': 'City'})
  card_billing_state = SelectField('State', choices=[], validators=[If(lambda form, field: form.card_billing_country.data=='US' and form.pmt_method.data=='card', validators.InputRequired()), If(lambda form, field: form.card_billing_country.data=='US' and form.pmt_method.data=='card', not_null_check)], render_kw={'autocomplete': 'address-level1', 'placeholder': 'State'})
  card_billing_province = StringField('State/Province', render_kw={'autocomplete': 'administrative-area', 'placeholder': 'non-US state/province'})
  card_billing_zip_code = StringField('Zip Code', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'postal-code', 'placeholder': 'Zip Code', 'maxlength':'10'})
  card_billing_country = SelectField('Country', choices=[], validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'country'}, default='US')
  desired_address = SelectField('Desired Address', choices=[], validators=[validators.InputRequired(), not_null_check])
  add_phone_to_plan = BooleanField('Add Phone To Plan',validators=[validators.Optional()])
  add_mail_scanning_to_plan = BooleanField('Add Scanning To Plan', validators=[validators.Optional()])
  add_mail_forwarding_to_plan = BooleanField('Add Mail Forwarding', validators=[validators.Optional()])
  mail_forwarding_frequency = SelectField('Mail Forwarding Frequency', choices=[('weekly','week'), ('biweekly','2 weeks'), ('monthly','month')], validators=[If(lambda form, field: form.add_mail_forwarding_to_plan.data==True, validators.InputRequired())])
  address_pricing_json = ''
  pay_card_convenience_pct = {{ pay_card_convenience_pct }}
  terms = BooleanField('I agree with the TOS', validators=[validators.InputRequired()], default=False)
  find_us = StringField('How did you find us?', validators=[validators.Optional()])
  comments = TextAreaField('Comments', validators=[validators.Optional()])
  submitForm = SubmitField('Submit Payment & Sign Up Now')

  def validate_card_number(form, field):
    if not credit_card.is_valid_luhn(field.data):
      raise ValidationError('invalid card number: %s' % (field.data,))

  def validate_card_billing_zip_code(form, field):
    if form.card_billing_country.data != 'US' or form.pmt_method.data == 'ach':
      return True
    if form.card_billing_country.data == 'US':
      zip = field.data
      match = re.match(r'\d{5}(-\d{4})?', zip)
      if match == None:
        raise ValidationError('invalid billing zip code')
      else:
        zip = zip[:5]
      state = form.card_billing_state.data
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM zip_code_tbl WHERE zip = %s AND state = %s LIMIT 1', (zip, state))
      result = curdict.fetchone()
      if not result:
        raise ValidationError('invalid billing zip code')


  def validate_card_billing_state(form, field):
    if form.pmt_method.data != 'card':
      return
    if form.card_billing_country.data == 'US':
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM state_tbl WHERE abbreviation = %s', (field.data,))
      state = curdict.fetchone()
      if not state:
        raise ValidationError('Invalid U.S. state: %s. Please choose a valid selection' % (field.data,))

  def validate_card_expire_year(form, field):
    if form.pmt_method != 'card':
      return
    today = datetime.date.today()
    try:
      if ((today.year - 2000) * 12 + today.month) > (int(field.data) * 12 + int(form.card_expire_month.data)):
        raise ValidationError('Expire date is in the past: %s/%s' % (form.card_expire_month.data, field.data))
    except ValueError:
      raise ValidationError('Invalid input!')

  def validate_desired_address(form, field):
    # If for whatever reason the id is invalid (malicious user case)
    floor_id = field.data
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('''SELECT F.id FROM floor_tbl F, building_vw B WHERE F.building_id = B.id AND B.is_active AND F.is_active AND F.has_vo_service AND B.type = 'officesuite' AND F.id = %s''', (floor_id,))
    if curdict.rowcount!=1:
      raise ValidationError('Invalid address choice.')

  def validate_terms(form, field):
    if not field.data:
      raise ValidationError('Please indicate you have read and agree to the Terms and Conditions')

  def validate_routing_number(form, field):
    if form.pmt_method.data == 'ach':
      if form.pmt_vault_id != "" and field.data.isdigit():
        conn = database.get_connection()
        curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        curdict.execute('SELECT * FROM fedach_participant_tbl WHERE routing_number = %s', (field.data,))
        fedach_participant = curdict.fetchone()
        if not fedach_participant:
          raise ValidationError('invalid routing number: %s' % (field.data,))
        else:
          return True
      if form.pmt_vault_id.data == "" and not field.data.isdigit():
          return False
      else:
        return True
    else:
      return True

  def validate_account_number(form, field):
    if form.pmt_method.data == 'ach':
      if form.pmt_vault_id != "" and field.data.isdigit():
          return True
      if form.pmt_vault_id.data == "" and not field.data.isdigit():
          return False
      else:
        return True
    else:
      return True


def get_user_date(cust_acct_id):
  conn = database.get_connection()
  cust_timezone = tzconvert.cust_timezone(conn, cust_acct_id)
  # get current datetime in customer's time zone
  now_local = datetime.datetime.now(cust_timezone)
  # extract date portion of datetime to obtain current date from user's perspective
  today_local = now_local.date()
  return today_local

def get_form():
  form = ApplyVirtualForm()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.card_billing_country.choices = [(country['iso_2'], country['name']) for country in curdict]
  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.card_billing_state.choices = [(state['abbreviation'], state['name']) for state in curdict]
  form.card_billing_state.choices.insert(0, ('', '---'))

  curdict.execute('''SELECT B.city, B.address_1, F.label, F.id FROM floor_tbl F, building_vw B WHERE F.building_id = B.id and B.is_active and F.is_active and B.type = 'officesuite' AND F.has_vo_service ORDER BY B.city, B.address_1, F.label''')
  form.desired_address.choices = [(str(bf['id']), bf['city'] + ' - ' +bf['address_1'] + ', ' +bf['label']) for bf in curdict]
  form.desired_address.choices.insert(0, ('', '---'))

  form.address_pricing_json =  get_pricing_dict()
  return form

def get_pricing_dict():
  ''' Gets dictionary for each floor's vo_service fees and current local date (to account for timezone differences)

  Each $ amount is multiplied by 100 to avoid any float calculation errors
  tax is multiplied by 1,000 for the same reason

  '''
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.execute('''SELECT F.ID as floor_id, F.building_id, F.vo_monthly_fee, F.vo_setup_fee, F.vo_phone_fee, F.vo_mail_forwarding_fee, F.vo_mail_scanning_fee, F.tax_rate FROM floor_tbl F, building_tbl B WHERE F.building_id = B.id AND B.type = 'officesuite' AND B.is_active AND F.is_active AND F.has_vo_service''')

  # Each value is a Python Decimal object which is cast to an int (after removing decimal places). Note JSON has varying float precision so conversion is necessary
  # Each floor ID has an associated dictionary with monthly, setup, and phone fees.
  # Can be accessed by dictionary[floor_id][fee_name]
  dictionary = {
    row['floor_id']: {
      'monthly_fee':   int(row['vo_monthly_fee'] * 100),
      'setup_fee':     int(row['vo_setup_fee'] * 100),
      'phone_fee':     int(row['vo_phone_fee'] * 100),
      'forwarding_fee':int(row['vo_mail_forwarding_fee'] * 100),
      'scanning_fee':  int(row['vo_mail_scanning_fee'] * 100),
      'tax_rate':      int(row['tax_rate'] * 1000),
      'local_date':tzconvert.utc_to_building_datetime(datetime.datetime.utcnow(),row['building_id'],conn).strftime('%d')
    }
    for row in curdict
  }
  # encode into JSON and return
  return JSONEncoder().encode(dictionary)


def apply_virtual():
  form = get_form()
  # Form validation
  if not form.validate_on_submit():
    for key in form.errors:
      [flash(error) for error in form.errors[key]]
    return render_template('virtual_apply_form.html', form=form)

  conn = database.get_connection()
  conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE)
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  floor_id = form.desired_address.data
  curdict.execute("SELECT * FROM floor_tbl WHERE id = %s", (floor_id,))
  floor = dict(curdict.fetchone())

  form.email.data = form.email.data.strip().lower()
  form.card_first_name.data = form.card_first_name.data.title()
  form.card_last_name.data = form.card_last_name.data.title()

  amount = floor['vo_monthly_fee'] + floor['vo_setup_fee']
  monthly_amount = floor['vo_monthly_fee']
  if form.add_phone_to_plan.data:
    # calculate phone fee
    phone = decimal.Decimal(floor['vo_phone_fee'])
    phone_tax = phone * (decimal.Decimal(floor['tax_rate']) / 100)
    phone_total = phone + phone_tax
    # round to two decimal places (cents)
    amount += phone_total.quantize(decimal.Decimal('1.00'))
    monthly_amount += phone_total.quantize(decimal.Decimal('1.00'))
    form.service_id = service_id = 6
  else:
    form.service_id = service_id = 4

  if form.add_mail_forwarding_to_plan.data:
    forwarding_fee = decimal.Decimal(floor['vo_mail_forwarding_fee'])
    amount += forwarding_fee.quantize(decimal.Decimal('1.00'))
    monthly_amount += forwarding_fee.quantize(decimal.Decimal('1.00'))

  # check if email address and company name already in database
  mes = ""
  try:
    account, person = get_account_and_person(form, curdict)
  except AccountMismatchException:
    conn.rollback()
    mes.append('Error. Please contact {{ account_mismatch_to }} or use a different email. (Email exists in database but is not associated with an account)')
    return render_template('daypassForm.html', form=form, mes=mes)

  pers_id = str(person['id'])
  account_id = str(account['id'])

  # insert into table pmt_vault_card_tbl and pmt_vault_txn_tbl
  curdict.execute("SELECT nextval('pmt_vault_seq') AS pmt_vault_id")
  pmt_vault_id = curdict.fetchone()['pmt_vault_id']
  curdict.execute("SELECT nextval('pmt_vault_txn_seq') AS txn_id")
  pmt_vault_txn_id = curdict.fetchone()['txn_id']
  curdict.execute("SELECT building_id FROM floor_vw WHERE id = %s", (form.desired_address.data,))
  building_id = curdict.fetchone()['building_id']
  cur_date = tzconvert.utc_to_building_datetime(datetime.datetime.utcnow(), building_id, conn).date()

  # Normalize date if > 28 (months have minimum of 28 days); discount for any overages
  today = get_user_date(account_id).day
  if today > 28:
    mon = get_user_date(account_id).month
    if mon in [1,3,5,7,8,10,12]:
      amount = amount - amount * (today - 28) / 31
    elif mon in [4,6,9,11]:
      amount = amount - amount * (today - 28) / 30
    else:
      amount = amount - amount * (today - 28) / 29
    today = 28

  # round to two decimal places (cents)
  prefee_amount = decimal.Decimal(str(amount)).quantize(decimal.Decimal('1.00'))
  monthly_total = decimal.Decimal(str(monthly_amount)).quantize(decimal.Decimal('1.00'))

  pmt_method = form.pmt_method.data

  if pmt_method == 'card':
    card = process_card_pmt(form, curdict, pmt_vault_id, account_id)
    # add convenience fee
    convenience_fee = decimal.Decimal(str(amount)) * convenience_pct_multiplier
    # round to two decimal places (cents)
    convenience_fee = convenience_fee.quantize(decimal.Decimal('1.00'))

    # calculate total amounts rounded to two decimal places (cents)
  else:
    # No convenience fee for ach
    ach = process_ach_pmt(form, curdict, pmt_vault_id, account_id)
    convenience_fee = decimal.Decimal('0.00')

  total_amount = prefee_amount + convenience_fee
  total_amount = total_amount.quantize(decimal.Decimal('1.00'))


  curdict.execute("INSERT INTO pmt_vault_txn_tbl (id, pmt_vault_id, amount, convenience_fee, scheduled_date, email) VALUES (%s, %s, %s, %s, %s, %s)", (pmt_vault_txn_id, pmt_vault_id, total_amount, convenience_fee, cur_date, form.email.data))
  curdict.execute('INSERT INTO pmt_recur_month_tbl (pmt_vault_id, max_amount, day_of_month, start_date, end_date, note, email, remind_days_before) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING *', (pmt_vault_id, monthly_total, today, None, None, 'initial sign up automatic recurring payment', person['email'], 3))
  recur = dict(curdict.fetchone())

  conn.commit()

  # charging process
  nmi_facade = chargefacade.NmiFacade(conn)

  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']
  is_demo_mode = False
  if ((form.card_first_name.data == 'Demo' and form.card_last_name.data == 'Demo') or form.bank_registered_name.data == 'Demo'):
    is_demo_mode = True
    nmi_facade.set_demo_mode(True)
  if {{ is_production_deployment }}:
    result_pmt_vault_txn = nmi_facade.process_pmt_vault_txn_as_client(pmt_vault_txn_id)
  else:
    nmi_facade.set_demo_mode(True)
    #### Only for development! This should never be done on production ####
    #### Should always fail on production because decrypt.py is not imported by default ####
    result_pmt_vault_txn = nmi_facade.process_pmt_vault_txn(pmt_vault_txn_id)

  # Setup templates on chargeFacade.NmiFacade
  # Set template variables
  setup_templs(pmt_method, nmi_facade)
  more_dict = {'cust_acct_id': account_id, 'cust_acct_name': account['name'], 'pay_base_url': '{{ pay_base_url }}', 'static_base_url': '{{ static_base_url }}', 'email_id': email_id }
  nmi_facade.set_more_tmpl_dict(more_dict)
  nmi_facade.send_mail_vault_txn(result_pmt_vault_txn)

  txn = dict(result_pmt_vault_txn)
  txn['prefee_amount'] = prefee_amount
  if pmt_method == 'card':
    txn['convenience_pct'] = convenience_pct_str
    txn['expire_mmyyyy'] = form.card_expire_month.data + '/' + str(2000 + int(form.card_expire_year.data))
  serializer = URLSafeTimedSerializer('{{ apply_itsdangerous_secret_key }}')
  token = serializer.dumps(form.email.data, salt='confirm_email')
  if pmt_method == 'card':
    card['expire_mmyyyy'] = form.card_expire_month.data + '/' + str(2000 + int(form.card_expire_year.data))
  plan = 'Virtual Office + Phone' if form.add_phone_to_plan.data else 'Virtual Office'
  if form.add_mail_forwarding_to_plan.data:
    plan += ', Mail Forwarding ({})'.format(form.mail_forwarding_frequency.data)
  if form.add_mail_scanning_to_plan.data:
    plan += ', Mail Scanning'
  building = [item for item in form.desired_address.choices if str(item[0]) == str(form.desired_address.data)][0][1]
  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']
  property_dict = { 'recur': recur, 'floor': floor, 'plan': plan, 'building': building, 'static_base_url': '{{ static_base_url }}', 'pay_base_url': '{{ pay_base_url }}', 'apply_base_url': '{{ apply_base_url }}', 'email':form.email.data, 'email_id': email_id, 'token':token }
  if pmt_method == 'card':
    property_dict['card'] = card
  else:
    property_dict['ach'] = ach
  property_dict['txn'] = txn
  property_dict['mes'] = mes # Contains message to user ('another email, different company found')
  # Setup email
  with open('{{ apply_basedir }}/share/virtual_office_confirm.txt', 'r') as tmpl_file:
    confirm_text_tmpl = tmpl_file.read()
  text_template = jinja2.Template(confirm_text_tmpl)
  with open('{{ apply_basedir }}/share/virtual_office_confirm.html', 'r') as tmpl_file:
    confirm_html_tmpl = tmpl_file.read()
  html_template = jinja2.Template(confirm_html_tmpl)
  text_body = text_template.render(property_dict)
  html_body = html_template.render(property_dict)

  # Create BCC list for ChargeMail.send_html_mail
  curdict.execute('''SELECT * FROM employee_tbl WHERE building_id=%s AND is_active AND receive_daypass_receipts''', (building_id,))
  Bcc_list = [row['email'] for row in curdict.fetchall()] # Create list of CMs associated with building
  Bcc_list.append('{{ daypass_mail_bcc }}') ## Also BCC billing
  Bcc_list =  ', '.join(Bcc_list)

  # Transaction success
  if result_pmt_vault_txn['response'] == '1':
    # Add user to virtual_office_tbl
    today_date = get_user_date(account_id)
    note = ''
    if form.comments.data:
      note += form.comments.data
    if today > 28:
      note += '; Discount for date adjustment from %s to 28' % today_date.day
    if form.pmt_method.data == 'card':
      note += '; 3% credit card fee added to initial'

    sql_column_list = ['account_id', 'floor_id', 'setup_fee', 'monthly_fee', 'start_date', 'billing_company', 'initial_pmt_txn_id', 'recur_id', 'note']
    sql_param_list = [account_id, floor_id, floor['vo_setup_fee'], floor['vo_monthly_fee'], today_date, 'MOM', result_pmt_vault_txn['id'], recur['id'], note]

    if form.add_phone_to_plan.data:
      sql_column_list.extend(['has_phone', 'phone_fee'])
      sql_param_list.extend([True, phone_total])
    if form.add_mail_forwarding_to_plan.data:
      sql_column_list.extend(['has_mail_forwarding', 'mail_forwarding_fee', 'mail_forwarding_frequency'])
      sql_param_list.extend([True, floor['vo_mail_forwarding_fee'], form.mail_forwarding_frequency.data])
    if form.add_mail_scanning_to_plan.data:
      sql_column_list.extend(['has_mail_scanning', 'mail_scanning_fee'])
      sql_param_list.extend([True, floor['vo_mail_scanning_fee']])

    SQL_QUERY = str('INSERT INTO virtual_office_tbl (' + ', '.join(sql_column_list) + ') VALUES (' + ', '.join(['%s'] * len(sql_column_list)) + ')')
    SQL_ARGS = tuple(sql_param_list)
    curdict.execute(SQL_QUERY, SQL_ARGS)

    # Send confirmation email
    # Changes are committed by the ChargeMail class
    ChargeMail.send_html_mail(form.email.data, Bcc_list, "Successful Application for Virtual Office Space with Micro Office", text_body, html_body, email_id, account_id, conn, True)

    return render_template('virtual_apply_result.html', **property_dict)
  # Transaction failed
  # Keep everything on records except pmt_recur_month_tbl insertion, flag card as inactive
  '''
  if new_person:
    curdict.execute("DELETE FROM cust_pers_tbl WHERE id = %s", (person['id'],))
  if new_account:
    curdict.execute("DELETE FROM cust_acct_tbl WHERE id = %s", (account_id,))
  curdict.execute("DELETE FROM pmt_nmi_txn_action_tbl WHERE pmt_nmi_txn_id IN (SELECT id FROM pmt_nmi_txn_card_tbl WHERE pmt_vault_txn_id = %s)", (pmt_vault_txn_id,))
  curdict.execute("DELETE FROM pmt_nmi_txn_card_tbl WHERE pmt_vault_txn_id = %s", (pmt_vault_txn_id,))
  curdict.execute("DELETE FROM pmt_vault_txn_tbl WHERE id = %s", (pmt_vault_txn_id,))
  curdict.execute("DELETE FROM pmt_vault_card_tbl WHERE id = %s", (pmt_vault_id,))'''
  curdict.execute('SELECT deactivate_pmt_vault(%s)', (pmt_vault_id,))
  curdict.execute("DELETE FROM pmt_recur_month_tbl WHERE pmt_vault_id = %s", (pmt_vault_id,))
  conn.commit()
  # add errors
  flash('Payment was unsuccessful. Please try again.')
  return render_template('virtual_apply_form.html', form=form)

def get_account_and_person(form, curdict):
  ''' Get the cust_acct_tbl and cust_pers_tbl rows (or create if necessary) '''
  curdict.execute("SELECT * FROM cust_pers_tbl WHERE email = %s", (form.email.data,))
  person = curdict.fetchone()

  if person is None:
    # Create new person and account
    curdict.execute('''SELECT nextval('person_seq') AS pers_id''')
    pers_id = str(curdict.fetchone()['pers_id'])
    curdict.execute('''SELECT nextval('account_seq') AS account_id''')
    account_id = str(curdict.fetchone()['account_id'])
    if form.pmt_method.data == 'card':
      curdict.execute("INSERT INTO cust_pers_tbl (id, cust_acct_id, first_name, last_name, email, is_active, home_address_1, home_address_2, home_city, home_state, home_zip_code, home_country, is_registered) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *", (pers_id, account_id, form.card_first_name.data, form.card_last_name.data, form.email.data, True, form.card_billing_address_1.data, form.card_billing_address_2.data, form.card_billing_city.data, form.card_billing_state.data, form.card_billing_zip_code.data, form.card_billing_country.data, False,))
    else:
      try:
        first_name, last_name = form.bank_registered_name.data.split(maxsplit=1)
      except ValueError:
        first_name = form.bank_registered_name.data
        last_name = ' '
      curdict.execute("INSERT INTO cust_pers_tbl (id, cust_acct_id, first_name, last_name, email, is_active, is_registered) VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING *", (pers_id, account_id, first_name, last_name, form.email.data, True, False,))
    person = curdict.fetchone()

    # create new account
    company = form.card_first_name.data + ' ' + form.card_last_name.data if form.pmt_method.data == 'card' else form.bank_registered_name.data
    curdict.execute("SELECT * FROM cust_acct_tbl WHERE name = %s", (company,))
    account = curdict.fetchone()
    if account is not None: # company name is not unique
      company += ', ' + account_id
    curdict.execute("INSERT INTO cust_acct_tbl (id, name, primary_contact_cust_pers_id, is_active, type, service_id, preferred_payment_method, room_minutes, primary_floor_id, pkg_notify_email, brand) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *", (account_id, company, pers_id, True, 'customer', form.service_id, 'credit card', 240, form.desired_address.data, form.email.data, 'microoffice'))
    account = curdict.fetchone()
  else: # email exists, just validate there's an account associated
    curdict.execute("SELECT * FROM cust_acct_tbl WHERE id = %s", (person['cust_acct_id'],))
    account = curdict.fetchone()
    if account is None:
      # An account in cust_pers_tbl with no associated cust_acct_id
      raise AccountMismatchException()
  return dict(account), dict(person)

def process_card_pmt(form, curdict, pmt_vault_id, account_id):
  expire_date = datetime.date(2000 + int(form.card_expire_year.data), int(form.card_expire_month.data), 1)
  if form.card_billing_country.data != 'US':
    card_billing_state = form.card_billing_province.data
  else:
    card_billing_state = form.card_billing_state.data
  curdict.execute("INSERT INTO pmt_vault_card_tbl (id, cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_city, billing_state, billing_zip_code, billing_country, is_active) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) returning *", (pmt_vault_id, account_id, form.card_number.data, expire_date, form.card_cvv.data, form.card_first_name.data, form.card_last_name.data, form.card_company.data, form.card_billing_address_1.data, form.card_billing_address_2.data, form.card_billing_city.data, card_billing_state, form.card_billing_zip_code.data, form.card_billing_country.data, True))
  card = dict(curdict.fetchone())
  return card


def process_ach_pmt(form, curdict, pmt_vault_id, account_id):
  curdict.execute("INSERT INTO pmt_vault_ach_tbl (id, cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number) VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING *",(pmt_vault_id, account_id, form.bank_account_type.data, form.bank_owned_by.data, form.bank_registered_name.data, form.bank_routing_number.data, form.bank_account_number.data))
  ach = dict(curdict.fetchone())
  return ach

def setup_templs(pmt_method, nmi_facade):
  if pmt_method == 'card':
    card_succ_subj_tmpl = 'Successful card payment to Micro Office'
    card_fail_subj_tmpl = 'ACTION REQUIRED - Failed credit card payment to Micro Office'
    with open('{{ apply_basedir }}/share/card_success_tmpl.txt', 'r') as tmpl_file:
      card_succ_text_tmpl = tmpl_file.read()
    with open('{{ apply_basedir }}/share/card_success_tmpl.html', 'r') as tmpl_file:
      card_succ_html_tmpl = tmpl_file.read()
    with open('{{ apply_basedir }}/share/card_failure_tmpl.txt', 'r') as tmpl_file:
      card_fail_text_tmpl = tmpl_file.read()
    with open('{{ apply_basedir }}/share/card_failure_tmpl.html', 'r') as tmpl_file:
      card_fail_html_tmpl = tmpl_file.read()
    nmi_facade.set_card_succ_tmpl(card_succ_subj_tmpl, card_succ_text_tmpl, card_succ_html_tmpl)
    nmi_facade.set_card_fail_tmpl(card_fail_subj_tmpl, card_fail_text_tmpl, card_fail_html_tmpl)
  else:
    ach_succ_subj_tmpl = 'Successful ACH/bank payment to Micro Office'
    ach_fail_subj_tmpl = 'ACTION REQUIRED - Failed ACH/bank payment to Micro Office'
    with open('{{ apply_basedir }}/share/ach_success_tmpl.txt', 'r') as tmpl_file:
      ach_succ_text_tmpl = tmpl_file.read()
    with open('{{ apply_basedir }}/share/ach_success_tmpl.html', 'r') as tmpl_file:
      ach_succ_html_tmpl = tmpl_file.read()
    with open('{{ apply_basedir }}/share/ach_failure_tmpl.txt', 'r') as tmpl_file:
      ach_fail_text_tmpl = tmpl_file.read()
    with open('{{ apply_basedir }}/share/ach_failure_tmpl.html', 'r') as tmpl_file:
      ach_fail_html_tmpl = tmpl_file.read()
    nmi_facade.set_ach_succ_tmpl(ach_succ_subj_tmpl, ach_succ_text_tmpl, ach_succ_html_tmpl)
    nmi_facade.set_ach_fail_tmpl(ach_fail_subj_tmpl, ach_fail_text_tmpl, ach_fail_html_tmpl)

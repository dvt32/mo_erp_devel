from flask import request, abort, redirect, url_for, flash, render_template, make_response
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import SubmitField, PasswordField, HiddenField, StringField, SelectField, DecimalField, DateField
from wtforms.fields.html5 import EmailField
from wtforms import validators, ValidationError
import datetime
import re
import psycopg2
import psycopg2.extras
import database
import pycurl
import io
from urllib.parse import urlencode
import json
import smtplib
from email.mime.text import MIMEText
import email.utils
import email.header
import calendar
import credit_card
import decimal
import chargefacade
from chargefacade import ChargeMail
import tzconvert
import jinja2
from json import JSONEncoder


month_choices = [(str(i), str(i) + ' - ' + calendar.month_abbr[i]) for i in range(1, 13)]
# convenience fee; use Decimal objects to preserve precision and avoid rounding errors from float/double representation
convenience_pct_str = '{{ pay_card_convenience_pct }}'
convenience_pct_multiplier = decimal.Decimal(convenience_pct_str) / decimal.Decimal(100)

# day pass form
class daypassForm(FlaskForm):
  # contact information
  phone_number = StringField("Phone: ", render_kw = {"placeholder": "(optional)"})
  email = EmailField("Email:", validators = [validators.InputRequired(), validators.Email('Please input a valid email address')], render_kw = {"placeholder":'Email'})
  num_people = SelectField("Number of People:", choices=[(str(x),x) for x in range(1,6)], validators = [validators.InputRequired(message='Please select the number of people you are purchasing for')])
  building_floor = SelectField("Location: ", choices=[], validators = [validators.InputRequired(message = 'Please choose a location')])
  date = StringField("Date of service: ", validators = [validators.InputRequired()], render_kw = {"placeholder": "yyyy-mm-dd"})
  # payment information
  billing_first_name = StringField('First Name', validators=[validators.InputRequired(message='Please provide your first name')], render_kw={'autocomplete': 'cc-given-name', 'placeholder':'First name'})
  billing_last_name = StringField('Last Name', validators=[validators.InputRequired(message='Please provide your last name')], render_kw={'autocomplete': 'cc-family-name', 'placeholder':'Last name'})
  card_number = StringField('Credit Card Number', validators=[validators.InputRequired(), validators.Regexp(r'^\d{15,16}|\d+\*+\d+$')], render_kw={'autocomplete': 'cc-number', 'placeholder':'################', 'maxlength':'16'})
  expire_month = SelectField('Month', choices=month_choices, validators=[validators.InputRequired()], render_kw={'autocomplete': 'cc-exp-month'})
  expire_year = StringField('Year', validators=[validators.InputRequired(),validators.Regexp(r'^\d{2}$')], render_kw={'autocomplete': 'cc-exp-year', 'placeholder': 'YY', 'maxlength':'2'})
  cvv = StringField('CVV/CVC/CSC', validators=[validators.InputRequired(), validators.Regexp(r'^\d{3,4}|\*{3,4}$'), validators.Length(min=3,max=4)], render_kw={'autocomplete': 'cc-csc', 'placeholder': 'CVV', 'maxlength':'4', 'class': 'card-input'})
  billing_company = StringField('Company', render_kw={'autocomplete': 'organization', 'placeholder': 'Company (optional)'})
  billing_address_1 = StringField('Address Line 1', validators=[validators.InputRequired(message='Please provide your address')], render_kw={'autocomplete': 'address-line1', 'placeholder': 'Address Line 1'})
  billing_address_2 = StringField('Address Line 2', render_kw={'autocomplete': 'address-line2', 'placeholder': 'Address Line 2 (optional)'})
  billing_city = StringField('City', validators=[validators.InputRequired(message='Please provide your billing city')], render_kw={'autocomplete': 'address-level2', 'placeholder': 'City'})
  # billing_state = SelectField('State', choices=[], validators=[validators.InputRequired(message='Please provide the state of billing address')], render_kw={'autocomplete': 'address-level1', 'placeholder': 'State'})
  billing_state = SelectField('State', choices=[], render_kw={'autocomplete': 'address-level1', 'placeholder': 'State'})
  billing_province = StringField('Province', render_kw={'autocomplete': 'address-level1', 'placeholder': 'if not in US, type state/province here'})
  billing_zip_code = StringField('Zip Code', validators=[validators.InputRequired()], render_kw={'autocomplete': 'postal-code', 'placeholder': 'Zip Code', 'maxlength':'10'})
  billing_country = SelectField('Country', choices=[], validators=[validators.InputRequired()], render_kw={'autocomplete': 'country'}, default='US')
  pay_card_convenience_pct = {{ pay_card_convenience_pct }}
  address_pricing_json = '' # used to display price for each address
  note = StringField('Note', render_kw={'placeholder': '(Optional note)'})
  submit = SubmitField('Submit')

  def validate_card_number(form, field):
    if field.data.isdigit():
      if not credit_card.is_valid_luhn(field.data):
        raise ValidationError('invalid card number: %s' % (field.data,))
      else:
        return True
    else:
        return False

  def validate_billing_state(form, field):
    if form.billing_country.data == 'US':
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM state_tbl WHERE abbreviation = %s', (field.data,))
      state = curdict.fetchone()
      if not state:
        raise ValidationError('invalid U.S. state: %s; enter a valid two character abbreviation' % (field.data,))

  def validate_billing_province(form, field):
    if form.billing_country != 'US':
      if not form.billing_province:
        raise ValidationError('Please provide the state of your billing address')

  def validate_billing_zip_code(form, field):
    if form.billing_country.data != 'US':
      return True
    zip = field.data
    match = re.match(r'\d{5}(-\d{4})?', zip)
    if match == None:
      raise ValidationError('Not a valid zip code')
    else:
      zip = zip[:5]
    state = form.billing_state.data
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM zip_code_tbl WHERE zip = %s AND state = %s LIMIT 1', (zip, state))
    result = curdict.fetchone()
    if not result:
      return False
    else:
      return True

  def validate_date(form, field):
    date_service = field.data.split('-')
    today = datetime.date.today()
    if (today.year * 12 + today.month > int(date_service[0]) * 12 + int(date_service[1])):
      raise ValidationError('Please choose today or a date later than today')
    elif (today.year * 12 + today.month == int(date_service[0]) * 12 + int(date_service[1]) and today.day > int(date_service[2])):
      raise ValidationError('Please choose today or a date later than today')
    else:
      if (int(date_service[1]) in [1,3,5,7,8,10,12]) and int(date_service[2]) > 31:
        raise ValidationError('Please choose a valid date')
      elif (int(date_service[1]) in [4,6,9,11]) and int(date_service[2]) > 30:
        raise ValidationError('Please choose a valid date')
      elif int(date_service[1]) == 2:
        if int(date_service[0]) % 400 == 0 or (int(date_service[0]) % 100 != 0 and int(date_service[0]) % 4 == 0):
          if int(date_service[2]) > 29:
            raise ValidationError('Please choose a valid date')
        else:
          if int(date_service[2]) > 28:
            raise ValidationError('Please choose a valid date')

  def validate_expire_year(form, field):
    today = datetime.date.today()
    if ((today.year - 2000) * 12 + today.month) > (int(field.data) * 12 + int(form.expire_month.data)):
      raise ValidationError('expire date is in the past: %s/%s' % (form.expire_month.data, field.data))

# display the webpage for day pass
def show_daypassform():
  form = get_form()
  return render_template("daypassForm.html", form = form)

# parse the data from day pass form
def parse_daypassform():
  form = get_form()
  # deal with unvalidated form
  if not form.validate_on_submit():
    return render_template('daypassForm.html', errors = [], form = form)
  # process the filled in information
  form.billing_first_name.data = form.billing_first_name.data.title()
  form.billing_last_name.data = form.billing_last_name.data.title()
  if not form.billing_company.data:
    form.billing_company.data = form.billing_first_name.data + " " + form.billing_last_name.data
  form.email.data = form.email.data.lower().strip()
  if form.billing_country.data != 'US':
    form.billing_state.data = form.billing_province.data
  conn = database.get_connection()
  conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE)
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  mes = []
  # check if it an existing customer and company and then make different changes to cust_acct_tbl, cust_pers_tbl
  curdict.execute("SELECT * FROM cust_pers_tbl WHERE email = %s", (form.email.data,))
  person = curdict.fetchone()
  curdict.execute("SELECT * FROM cust_acct_tbl WHERE name = %s", (form.billing_company.data,))
  account = curdict.fetchone()
  new_person = False
  new_account = False
  if not person:
    new_person = True
    new_account = True
    curdict.execute('''SELECT nextval('person_seq') AS pers_id''')
    pers_id = str(curdict.fetchone()['pers_id'])
    curdict.execute('''SELECT nextval('account_seq') AS account_id''')
    account_id = str(curdict.fetchone()['account_id'])

    if account is not None: # company name is non-unique
      form.billing_company.data += ', ' + account_id
    account_name = form.billing_company.data

    # Create new account and new person
    curdict.execute("INSERT INTO cust_pers_tbl (id, cust_acct_id, first_name, last_name, email, cell_phone_number, is_active, work_address_1, work_address_2, work_city, work_state, work_zip_code, work_country) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (pers_id, account_id, form.billing_first_name.data, form.billing_last_name.data, form.email.data, form.phone_number.data, True, form.billing_address_1.data, form.billing_address_2.data, form.billing_city.data, form.billing_state.data, form.billing_zip_code.data, form.billing_country.data))
    curdict.execute("INSERT INTO cust_acct_tbl (id, name, primary_contact_cust_pers_id, is_active, type, service_id, preferred_payment_method, room_minutes, primary_floor_id, pkg_notify_email, brand) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ", (account_id, account_name, pers_id, True, 'daypass customer', 4, 'credit card', 0, form.building_floor.data, form.email.data, 'microoffice'))
  else:
    pers_id = person['id']
    curdict.execute("SELECT * FROM cust_acct_tbl WHERE id = %s", (person['cust_acct_id'],))
    account = curdict.fetchone()
    if account is None:
      # An account in cust_pers_tbl with no associated cust_acct_id
      mes.append('Error. Please contact {{ account_mismatch_to }} or use a different email. (Email exists in database but is not associated with an account)')
      conn.rollback()
      return render_template('daypassForm.html', form=form, mes=mes)
    else:
      account_id = account['id']
      mes.append('We notice that You have bought daypass before. For your convenience, please log in to buy daypass the next time.')

  # insert into table pmt_vault_card_tbl and pmt_vault_txn_tbl
  curdict.execute("SELECT nextval('pmt_vault_seq') AS pmt_vault_id")
  pmt_vault_id = curdict.fetchone()['pmt_vault_id']
  curdict.execute("SELECT nextval('pmt_vault_txn_seq') AS txn_id")
  pmt_vault_txn_id = curdict.fetchone()['txn_id']
  expire_date = datetime.date(2000 + int(form.expire_year.data), int(form.expire_month.data), 1)
  curdict.execute("INSERT INTO pmt_vault_card_tbl (id, cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_city, billing_state, billing_zip_code, billing_country, is_active) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (pmt_vault_id, account_id, form.card_number.data, expire_date, form.cvv.data, form.billing_first_name.data, form.billing_last_name.data, form.billing_company.data, form.billing_address_1.data, form.billing_address_2.data, form.billing_city.data, form.billing_state.data, form.billing_zip_code.data, form.billing_country.data, True))
  curdict.execute("SELECT * FROM pmt_vault_card_tbl WHERE id = %s", (pmt_vault_id,))
  card = dict(curdict.fetchone())
  curdict.execute("SELECT building_id FROM floor_vw WHERE id = %s", (form.building_floor.data,))
  building_id = curdict.fetchone()['building_id']
  cur_date = tzconvert.utc_to_building_datetime(datetime.datetime.utcnow(), building_id, conn).date()
  # round to two decimal places (cents)
  curdict.execute("SELECT daypass_fee FROM floor_tbl WHERE id = %s", (form.building_floor.data,))
  amount = curdict.fetchone()['daypass_fee'] * int(form.num_people.data)
  prefee_amount = decimal.Decimal(amount).quantize(decimal.Decimal('1.00'))
  # add convenience fee
  convenience_fee = decimal.Decimal(amount) * convenience_pct_multiplier
  # round to two decimal places (cents)
  convenience_fee = convenience_fee.quantize(decimal.Decimal('1.00'))
  total_amount = prefee_amount + convenience_fee
  curdict.execute("INSERT INTO pmt_vault_txn_tbl (id, pmt_vault_id, amount, convenience_fee, scheduled_date, email) VALUES (%s, %s, %s, %s, %s, %s)", (pmt_vault_txn_id, pmt_vault_id, total_amount, convenience_fee, cur_date, form.email.data))
  conn.commit()
  # payment process
  nmi_facade = chargefacade.NmiFacade(conn)

  # Can be tested when using first and last name 'Demo'
  if form.billing_first_name.data == 'Demo' and form.billing_last_name.data == 'Demo':
    nmi_facade.set_demo_mode(True)

  # following lines are for development; as_client fails on dev
  if {{ is_production_deployment }}:
    result_pmt_vault_txn = nmi_facade.process_pmt_vault_txn_as_client(pmt_vault_txn_id)
  else:
    nmi_facade.set_demo_mode(True)
    result_pmt_vault_txn = nmi_facade.process_pmt_vault_txn(pmt_vault_txn_id)

  #send the email nmi mail (success/failed card payment)

  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']
  card_succ_subj_tmpl = 'Successful card payment to Micro Office'
  with open('{{ apply_basedir }}/share/card_success_tmpl.txt', 'r') as tmpl_file:
    card_succ_text_tmpl = tmpl_file.read()
  with open('{{ apply_basedir }}/share/card_success_tmpl.html', 'r') as tmpl_file:
    card_succ_html_tmpl = tmpl_file.read()
  nmi_facade.set_card_succ_tmpl(card_succ_subj_tmpl, card_succ_text_tmpl, card_succ_html_tmpl)
  card_fail_subj_tmpl = 'ACTION REQUIRED - Failed credit card payment to Micro Office'
  with open('{{ apply_basedir }}/share/card_failure_tmpl.txt', 'r') as tmpl_file:
    card_fail_text_tmpl = tmpl_file.read()
  with open('{{ apply_basedir }}/share/card_failure_tmpl.html', 'r') as tmpl_file:
    card_fail_html_tmpl = tmpl_file.read()
  nmi_facade.set_card_fail_tmpl(card_fail_subj_tmpl, card_fail_text_tmpl, card_fail_html_tmpl)
  card_succ_more_dict = {'cust_acct_id': account_id, 'cust_acct_name': form.billing_company.data, 'pay_base_url': '{{ pay_base_url }}', 'static_base_url': '{{ static_base_url }}', 'email_id': email_id }
  nmi_facade.set_more_tmpl_dict(card_succ_more_dict)
  nmi_facade.send_mail_vault_txn(result_pmt_vault_txn)
  result_pmt_vault_txn = dict(result_pmt_vault_txn)
  result_pmt_vault_txn['prefee_amount'] = decimal.Decimal(amount).quantize(decimal.Decimal('1.00'))
  result_pmt_vault_txn['convenience_pct'] = convenience_pct_str
  result_pmt_vault_txn['expire_mmyyyy'] = form.expire_month.data + '/' + str(2000 + int(form.expire_year.data))
  card['expire_mmyyyy'] = form.expire_month.data + '/' + str(2000 + int(form.expire_year.data))

  if result_pmt_vault_txn['response'] != '1':
    # flag the card as inactive since the transaction did not go through
    curdict.execute('SELECT deactivate_pmt_vault(%s)', (pmt_vault_id,))
    conn.commit()
    if result_pmt_vault_txn['response'] == '3' and 'Duplicate' in result_pmt_vault_txn['responsetext']:
      flash('If you intended to make a second payment of the same amount, please wait 5 minutes and try again. The merchant processor marked your most recent one as a duplicate.')
      return render_template("daypassForm.html", form = form)
    else:
      flash('Your payment was failed. Please try again.')
      return render_template("daypassForm.html", form = form)

  # fetch the detailed building and floor information
  curdict.execute("SELECT B.city, B.address_1, F.label FROM floor_vw F, building_vw B WHERE F.building_id = B.id and F.id = %s", (form.building_floor.data,))
  t = curdict.fetchone()
  location = t['city'] + ' - ' +t['address_1'] + ', ' +t['label']

  # setup email (daypass confirmation)
  property_dict = { 'card':card, 'phone_number':form.phone_number.data, 'email':form.email.data, 'amount':amount, 'location':location, 'date_of_service':form.date.data, 'txn':result_pmt_vault_txn, 'static_base_url':'{{ static_base_url }}', 'pay_base_url': '{{ pay_base_url }}', 'apply_base_url':'{{ apply_base_url }}'}

  with open('{{ apply_basedir }}/share/daypass_confirm.txt','r') as templ_file:
    daypass_text_templ = templ_file.read()
  with open('{{ apply_basedir}}/share/daypass_confirm.html', 'r') as templ_file:
    daypass_html_templ = templ_file.read()
  text_template = jinja2.Template(daypass_text_templ)
  html_template = jinja2.Template(daypass_html_templ)
  text_body = text_template.render(property_dict)
  html_body = html_template.render(property_dict)

  curdict.execute('''SELECT B.id from floor_vw F, building_vw B WHERE F.building_id = B.id AND F.id=%s''', (form.building_floor.data,))
  building_id = curdict.fetchone()['id']
  curdict.execute('''SELECT * FROM employee_tbl WHERE building_id=%s AND is_active AND receive_daypass_receipts''', (building_id,))
  Bcc_list = [row['email'] for row in curdict.fetchall()] # Create list of CMs associated with building
  Bcc_list.append('{{ daypass_mail_bcc }}') ## Also BCC billing
  Bcc_list = ', '.join(Bcc_list)


  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']
  # send email
  curdict.execute('INSERT INTO daypass_tbl (cust_acct_id, date_of_service, floor_id, num_people, txn_id, note) VALUES (%s, %s, %s, %s, %s, %s)', (account_id, form.date.data, form.building_floor.data, form.num_people.data, result_pmt_vault_txn['id'], form.note.data,))
  ChargeMail.send_html_mail(form.email.data, Bcc_list, "Daypass Purchased", text_body, html_body, email_id, account_id, conn, False)
  conn.commit()

  # successfully submit form
  return render_template("daypass_congrat.html", mes = mes, pmt_nmi_txn=result_pmt_vault_txn, success = 1, **property_dict)

# get the form and add states and countries choices into the two SelectField
def get_form():
  form = daypassForm()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  # check if user specified valid building nickname (e.g. ?building=impact)
  request_building = request.args.get('building', '').title()
  curdict.execute('SELECT id FROM floor_tbl WHERE building_id IN (SELECT id FROM building_tbl WHERE nickname = %s) AND has_daypass_service LIMIT 1', (request_building,))
  if curdict.rowcount == 1:
    floor_id = curdict.fetchone()['id']
    form.building_floor.hidden= True
    # fetch building and floor information
    curdict.execute('''SELECT B.city, B.address_1, F.label, F.id FROM floor_tbl F, building_vw B WHERE F.building_id = B.id and B.is_active and F.is_active and F.id = %s and B.type = 'officesuite' and F.has_daypass_service''', (floor_id,))
    form.building_floor.choices = [(str(bf['id']), bf['city'] + ' - ' +bf['address_1'] + ', ' +bf['label']) for bf in curdict]
  else:
    # fetch building and floor information
    curdict.execute('''SELECT B.city, B.address_1, F.label, F.id FROM floor_tbl F, building_vw B WHERE F.building_id = B.id and B.is_active and F.is_active and B.type = 'officesuite' and F.has_daypass_service ORDER BY B.city, B.address_1, F.label''')
    form.building_floor.choices = [('', '---Please Select A Location---')]
    form.building_floor.choices.extend([(str(bf['id']), bf['city'] + ' - ' +bf['address_1'] + ', ' +bf['label']) for bf in curdict])
  # fetch country names and US state names
  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.billing_country.choices = [('', '---Please Select Your Country---')]
  form.billing_country.choices.extend([(country['iso_2'], country['name']) for country in curdict])
  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.billing_state.choices = [('', '---Please Select Your State---')]
  form.billing_state.choices.extend([(state['abbreviation'], state['name']) for state in curdict])
  curdict.execute('SELECT id, daypass_fee FROM floor_tbl WHERE has_daypass_service')
  dictionary = {x['id']:int(x['daypass_fee']*100) for x in curdict}
  form.address_pricing_json = JSONEncoder().encode(dictionary)
  return form

def mismatch_mail(company_name, account_id, email_add, person_id):
  mail_body = '''
  Mismatch information for day pass purchase:
  Company Name: %s, cust_acct_tbl  cust_ID: %s;
  Email: %s, cust_acct_tbl cust_ID: %s.
  ''' %(company_name, account_id, email_add, person_id)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = '{{ daypass_mail_from }}'
  mail_msg['To'] = '{{ account_mismatch_to }}'
  mail_msg['Subject'] = email.header.Header('Day Pass submission: mismatch customer company name and email address', 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()

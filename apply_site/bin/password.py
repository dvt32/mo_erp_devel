import psycopg2.extras
from flask import g, flash
from flask import request, render_template
from flask_wtf import FlaskForm
from wtforms.fields.html5 import EmailField
from wtforms import StringField, SubmitField, HiddenField, PasswordField
from wtforms.validators import InputRequired, Email, EqualTo
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired
from email_validator import validate_email, EmailNotValidError
import smtplib
import database
from chargefacade import ChargeMail
import cryptpw


class PasswordForm(FlaskForm):
  token = HiddenField('token', validators=[InputRequired()])
  password = PasswordField('New Password', validators=[InputRequired(), EqualTo('confirm', message='Passwords do not match.')], render_kw={'placeholder': 'New Password'})
  confirm = PasswordField('Confirm Password', validators=[InputRequired()], render_kw={'placeholder': 'Confirm New Password'})
  submitForm = SubmitField('Submit')


def set_password(token):
  serializer = URLSafeTimedSerializer('{{ apply_itsdangerous_secret_key }}')
  try:
    # email tokens in registration/change password emails can exist for 120 minutes and then the user must request a new token
    email = serializer.loads(token, salt='confirm_email', max_age=7200)
  except SignatureExpired as e:
    return render_template('set_password_error.html', error=str(e))
  except BadSignature as e:
    return render_template('set_password_error.html', error=str(e))
  if email:
    email = email.strip()
    try:
      validated_email = validate_email(email)
      email = validated_email['email']
    except EmailNotValidError as e:
      return render_template('set_password_error.html', error=str(e))
  form_token = serializer.dumps(email, salt='set_password')
  form = PasswordForm(token=form_token)
  return render_template('set_password_input.html', form=form)

def set_password_result():
  form = PasswordForm()
  if not form.validate_on_submit():
    return render_template('set_password_input.html', form=form)
  token = form.token.data
  serializer = URLSafeTimedSerializer('{{ apply_itsdangerous_secret_key }}')
  try:
    # form tokens from the set password form can exist for 120 minutes and then the user must request a new token
    email = serializer.loads(token, salt='set_password', max_age=7200)
  except BadSignature as e:
    return render_template('set_password_error.html', error=str(e))
  email = email.strip().lower()
  try:
    validated_email = validate_email(email)
    email = validated_email['email']
  except EmailNotValidError as e:
    return render_template('set_password_error.html', error=str(e))
  hashed_password = cryptpw.hash_password(form.password.data)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT update_cust_pw(%s, %s, %s)', (form.password.data, hashed_password, email,))
  updated_rows = curdict.rowcount
  conn.commit()
  return render_template('set_password_success.html', email=email)

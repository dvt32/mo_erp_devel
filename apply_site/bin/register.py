from flask import request, abort, redirect, url_for, flash, render_template, g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, BooleanField, HiddenField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms_components import If
from wtforms import validators, ValidationError
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired
from email_validator import validate_email, EmailNotValidError
import psycopg2
import psycopg2.extras
import tzconvert
import database
import datetime
import calendar
import smtplib
import credit_card
import random
import chargefacade
from chargefacade import ChargeMail
import decimal
import jinja2
import re
import registermail

def not_null_check(form, field):
  if field.data == '':
    raise ValidationError('Please make a valid selection')

class RegistrationForm(FlaskForm):
  token = HiddenField('token', validators=[validators.InputRequired()])
  cust_pers_id = HiddenField('cust_pers_id', validators=[validators.InputRequired()])
  first_name = StringField('First Name', validators=[validators.InputRequired()], render_kw={'placeholder': 'First Name', 'autocomplete': 'given-name'})
  last_name = StringField('Last Name', validators=[validators.InputRequired()], render_kw={'placeholder': 'Last Name', 'autocomplete': 'given-name'})
  title = StringField('Title', validators=[validators.Optional()], render_kw={'placeholder': 'Title', 'autocomplete': 'honorific-prefix'})
  company = StringField('Company', validators=[validators.Optional()], render_kw={'placeholder': 'Company', 'autocomplete': 'org'})
  biz_desc = StringField('Brief Business Description', validators=[validators.Optional()], render_kw={'placeholder': 'Brief Description'})
  home_address_1 = StringField('Home Address Line 1', validators=[validators.InputRequired()], render_kw={'placeholder': 'Address Line 1', 'autocomplete': 'address-line1'})
  home_address_2 = StringField('Home Address Line 2', validators=[validators.Optional()], render_kw={'placeholder': 'Address Line 2', 'autocomplete': 'address-line2'})
  home_city = StringField('Home City', validators=[validators.InputRequired()], render_kw={'placeholder': 'City', 'autocomplete': 'address-level2'})
  home_state = SelectField('Home State', choices=[], validators=[If(lambda form, field: form.home_country.data=='US', validators.InputRequired()), If(lambda form, field: form.home_country.data=='US', not_null_check)], render_kw={'autocomplete': 'address-level1'})
  home_province = StringField('(or Province/<wbr>Territory)', validators=[validators.Optional()], render_kw={'placeholder': 'Province/Territory', 'autocomplete': 'administrative-area'})
  home_zip_code = StringField('Home Zip Code', validators=[validators.InputRequired()], render_kw={'autocomplete': 'postal-code', 'placeholder': 'Zip Code', 'maxlength':'10'})
  home_country = SelectField('Home Country', choices=[], validators=[validators.InputRequired()], render_kw={'autocomplete': 'country'}, default='US')
  has_work_addr = BooleanField('Add Work Address', default=False)
  work_address_1 = StringField('Work Address Line 1', validators=[If(lambda form, field: form.has_work_addr.data, validators.InputRequired())], render_kw={'placeholder': 'Address Line 1', 'autocomplete': 'address-line1'})
  work_address_2 = StringField('Work Address Line 2', validators=[validators.Optional()], render_kw={'placeholder': 'Address Line 2', 'autocomplete': 'address-line2'})
  work_city = StringField('Work City', validators=[validators.Optional()], render_kw={'placeholder': 'City', 'autocomplete': 'address-level2'})
  work_state = SelectField('Work State', choices=[], validators=[If(lambda form, field: form.work_country.data=='US', validators.Optional()), If(lambda form, field: form.work_country.data=='US' and form.has_work_addr.data, not_null_check)], render_kw={'autocomplete': 'address-level1'})
  work_province = StringField('(or Province/<wbr>Territory)', validators=[validators.Optional()], render_kw={'placeholder': 'Province/Territory', 'autocomplete': 'administrative-area'})
  work_zip_code = StringField('Work Zip Code', validators=[validators.Optional()], render_kw={'autocomplete': 'postal-code', 'placeholder': 'Zip Code', 'maxlength':'10'})
  work_country = SelectField('Work Country', choices=[], validators=[If(lambda form, field: form.has_work_addr.data, validators.InputRequired())], render_kw={'autocomplete': 'country'}, default='US')
  phone = StringField('Cell Phone', validators=[validators.InputRequired()], render_kw={'placeholder': 'e.g. 1234567890', 'autocomplete': 'tel-national'})
  intl_phone = BooleanField('International Phone Number', default=False)
  website = StringField('Company Website', validators=[validators.Optional()], render_kw={'placeholder': 'Website'})
  is_primary_contact = False
  submitForm = SubmitField('Complete Registration')

  def validate_first_name(form, field):
    field.data = field.data.title()

  def validate_last_name(form, field):
    field.data = field.data.title()

  def validate_home_zip_code(form, field):
    return validate_zip(zip=field.data, country=form.home_country.data, state=form.home_state.data)

  def validate_work_zip_code(form, field):
    if form.has_work_addr.data:
      return validate_zip(zip=field.data, country=form.work_country.data, state=form.work_state.data)

  def validate_home_state(form, field):
    return validate_state(state=field.data, country=form.home_country.data)

  def validate_work_state(form, field):
    return validate_state(state=field.data, country=form.work_country.data)

  def validate_phone(form, field):
    if form.intl_phone.data:
      match = re.match(r'^(\+\d+\s)?\d+$', field.data)
      if match == None:
        raise ValidationError('Please enter only digits and the + sign')
    else:
      match = re.match(r'^\d{10}$', field.data)
      if match == None:
        raise ValidationError('Please enter only the 10 digits of your U.S. phone number')


def validate_zip(zip, country, state=None):
  if country != 'US':
    return True
  else:
    match = re.match(r'\d{5}(-\d{4})?', zip)
    if match == None:
      raise ValidationError('invalid zip format')
    else:
      zip = zip[:5]
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM zip_code_tbl WHERE zip = %s AND state = %s LIMIT 1', (zip, state))
      if curdict.rowcount == 0:
        raise ValidationError('invalid zip code')
  return True

def validate_state(state, country):
  if country != 'US':
    return True
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM state_tbl WHERE abbreviation = %s', (state,))
  if curdict.rowcount != 1:
    raise ValidationError('invalid U.S. state: %s; enter a valid two character abbreviation' % (field.data,))
  else:
    return True

def generate_registration_token(cust_pers_id):
  serializer = URLSafeTimedSerializer('{{ apply_itsdangerous_secret_key }}')
  token = serializer.dumps(cust_pers_id, salt='cust_pers_id')
  return token


def register(token):
  ''' Show the registration page to the user.

  Args: token - cust_pers_id encrypted with salt='cust_pers_id'
  '''

  # First decrypt token, should be a cust_pers_id
  serializer = URLSafeTimedSerializer('{{ apply_itsdangerous_secret_key }}')
  try:
    # Allow details to be changed for 1 day = 24 * 60 * 60 = 84600
    cust_pers_id = serializer.loads(token, salt='cust_pers_id')

  # Check if token is valid
  except SignatureExpired:
    flash('Link expired. Please request a new registration link')
    return registermail.show_email_form()
  except BadSignature:
    flash('Invalid link. Please request a new registration link')
    return registermail.show_email_form()
  if not cust_pers_id:
    flash('Invalid link. Please request a new registration link')
    return registermail.show_email_form()
  try:
    # Check if decrypted token is valid type
    cust_pers_id = int(cust_pers_id)
  except ValueError as e:
    flash('Invalid link. Please request a new registration link')
    return registermail.show_email_form()

  # Check if ID is valid
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE id=%s', (cust_pers_id,))
  if curdict.rowcount == 0:
    flash('Something went wrong. Please request a new registration link')
    return registermail.show_email_form()

  # Generate token for second part of form (different salt)
  token = serializer.dumps(cust_pers_id, salt='register')
  form = get_form(register_token=token, cust_pers_id=cust_pers_id)
  return render_template('register_input.html', form=form)


def get_form(register_token=0, cust_pers_id=0):
  form = RegistrationForm()

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.home_country.choices = [(country['iso_2'], country['name']) for country in curdict]
  form.work_country.choices = form.home_country.choices

  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.home_state.choices = [(state['abbreviation'], state['name']) for state in curdict]
  form.home_state.choices.insert(0, ('', '---'))
  form.work_state.choices = form.home_state.choices

  # Set token data if passed
  if register_token:
    form.token.data = register_token

  # populate form if cust_pers_id passed
  if cust_pers_id:
    form.cust_pers_id.data = cust_pers_id

    # User information
    curdict.execute('SELECT * FROM cust_pers_tbl WHERE id=%s', (cust_pers_id,))
    person = curdict.fetchone()

    form.first_name.data = person['first_name']
    form.last_name.data = person['last_name']
    form.title.data = person['salutation']

    form.home_address_1.data = person['home_address_1']
    form.home_address_2.data = person['home_address_2']
    form.home_city.data = person['home_city']
    form.home_state.data = person['home_state']
    form.home_zip_code.data = person['home_zip_code']
    form.home_country.data = person['home_country']

    # only populate work fields if it exists
    if person['work_address_1']:
      form.has_work_addr.data = True
      form.work_address_1.data = person['work_address_1']
      form.work_address_2.data = person['work_address_2']
      form.work_city.data = person['work_city']
      form.work_state.data = person['work_state']
      form.work_zip_code.data = person['work_zip_code']
      form.work_country.data = person['work_country']

    form.phone.data = person['cell_phone_number']

    # Company Information
    curdict.execute('SELECT * FROM cust_acct_tbl WHERE id=%s', (person['cust_acct_id'],))
    account = curdict.fetchone()
    form.website.data = account['web_site']
    form.company.data = account['name']
    form.biz_desc.data = account['description']

    curdict.execute('SELECT * FROM cust_acct_tbl WHERE id=%s AND primary_contact_cust_pers_id=%s', (person['cust_acct_id'], person['id'],))
    if curdict.rowcount == 1:
      form.is_primary_contact = True
  return form


def register_result():
  form = get_form()
  if not form.validate_on_submit():
    for key in form.errors:
      [flash(error) for error in form.errors[key]]
    return render_template('register_input.html', form=form)

  # Value defaults
  if not form.company.data:
    company = str(form.first_name.data + ' ' + form.last_name.data)
  else:
    company = form.company.data

  if form.home_country.data == 'US':
    home_state = form.home_state.data
  else:
    home_state = form.home_province.data

  if form.work_country.data == 'US':
    work_state = form.work_state.data
  else:
    work_state = form.work_province.data

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  token = form.token.data
  serializer = URLSafeTimedSerializer('{{ apply_itsdangerous_secret_key }}')
  try:
    cust_pers_id = serializer.loads(token, salt='register', max_age=7200)
  except SignatureExpired as e:
    flash('Form timed out.')
    return render_template('register_input.html', form=form)
  except BadSignature as e:
    flash('Invalid token. Please request a new one.')
    return render_template('register_input.html', form=form)
  try:
    cust_pers_id = int(cust_pers_id)
  except ValueError as e:
    flash('Something went wrong. Please try again later.')
    return render_template('register_input.html', form=form)

  curdict.execute('SELECT * FROM cust_pers_tbl WHERE id=%s', (cust_pers_id,))
  if curdict.rowcount != 1:
    flash('Invalid token, unable to find customer with id %s' % cust_pers_id)
    return render_template('register_input.html', form=form)

  # is_registered is set to True
  curdict.execute('UPDATE cust_pers_tbl SET first_name=%s, last_name=%s, salutation=%s, cell_phone_number=%s, company=%s, home_address_1=%s, home_address_2=%s, home_city=%s, home_state=%s, home_zip_code=%s, home_country=%s, is_registered=%s WHERE id=%s RETURNING cust_acct_id', (form.first_name.data, form.last_name.data, form.title.data, form.phone.data, form.company.data, form.home_address_1.data, form.home_address_2.data, form.home_city.data, home_state, form.home_zip_code.data, form.home_country.data, True, cust_pers_id,))

  # Company associated with Person
  cust_acct_id = curdict.fetchone()['cust_acct_id']

  # Update work if user gave info
  if form.has_work_addr.data:
    curdict.execute('UPDATE cust_pers_tbl SET work_address_1=%s, work_address_2=%s, work_city=%s, work_state=%s, work_zip_code=%s, work_country=%s WHERE id=%s', (form.work_address_1.data, form.work_address_2.data, form.work_city.data, work_state, form.work_zip_code.data, form.work_country.data, cust_pers_id,))

  # Check to make sure company name is unique
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE name = %s AND id != %s', (company,cust_acct_id,))
  if curdict.rowcount >= 1:
    # append account id if nonunique
    flash('"%s" is already taken. You have been assigned "%s %s".' % (company, company, cust_acct_id))
    company += ' ' + str(cust_acct_id)

  # Only update company info if person is the primary contact
  curdict.execute('UPDATE cust_acct_tbl SET name=%s, web_site=%s, description=%s WHERE id=%s AND primary_contact_cust_pers_id=%s', (company, form.website.data, form.biz_desc.data, cust_acct_id, cust_pers_id,))
  conn.commit()


  # Display confirmation page
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE id=%s', (cust_pers_id,))
  person = curdict.fetchone()
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id=%s', (cust_acct_id,))
  account = curdict.fetchone()


  curdict.execute("SELECT * FROM floor_vw WHERE id = %s", (account['primary_floor_id'],))
  floor = dict(curdict.fetchone())
  if account['service_id'] == 4:
    plan = 'Virtual Office + Phone'
  elif account['service_id'] == 6:
    plan = 'Virtual Office'
  else:
    plan = 'Unknown service. Plan id = %s' % account['service_id']

  property_dict = { 'person': person, 'account': account, 'floor': floor, 'plan': plan }
  conn.close()
  return render_template('register_result.html', **property_dict)

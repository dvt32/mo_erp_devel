from flask import request, abort, redirect, url_for, flash, render_template, g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, BooleanField, HiddenField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms_components import If
from wtforms import validators, ValidationError
import jinja2
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import email.utils
import email.header
import database
import psycopg2
import psycopg2.extras
import register


def send_html_mail(mail_to, mail_subject, text_body, html_body):
  """Send an html text email.

  Args:
  mail_to: email address of recipient
  mail_subject: subject line of the email
  text_body: plaintext version of the email body
  html_body: html version of the email body
  """
  mail_msg = MIMEMultipart('alternative')
  text = MIMEText(text_body, 'plain')
  html = MIMEText(html_body, 'html')
  mail_msg.attach(text)
  mail_msg.attach(html)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = '{{ payment_mail_from }}'
  mail_msg['To'] = mail_to
  mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()

class RegistrationEmailForm(FlaskForm):
  email = EmailField('Your Email', validators=[validators.InputRequired()])
  submitForm = SubmitField('Request Email')

  def validate_email(form, field):
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM cust_pers_tbl WHERE email=%s', (field.data,))
    if curdict.rowcount!=1:
      raise ValidationError('Unable to locate account with email %s' % field.data)

def show_email_form(email=''):
  form = RegistrationEmailForm()
  form.email.data = email
  return render_template('register_request.html', form=form)

def send_registration_email():
  form = RegistrationEmailForm()
  if not form.validate_on_submit():
    for key in form.errors:
      [flash(error) for error in form.errors[key]]
    return render_template('register_request.html', form=form)

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT id FROM cust_pers_tbl WHERE email=%s', (form.email.data,))
  cust_pers_id = curdict.fetchone()['id']
  token = register.generate_registration_token(cust_pers_id)

  # Setup Email Template
  with open('{{ apply_basedir }}/share/registration_email.html', 'r') as templ_file:
    html_templ = jinja2.Template(templ_file.read())
  with open('{{ apply_basedir }}/share/registration_email.txt', 'r') as templ_file:
    text_templ = jinja2.Template(templ_file.read())

  property_dict = {'registration_token':token, 'static_base_url': '{{ static_base_url}}', 'pay_base_url':'{{ pay_base_url }}'}
  email = form.email.data
  subject = 'Manage MicroOffice Account Details'
  html_body = html_templ.render(property_dict)
  text_body = text_templ.render(property_dict)

  ## send email here
  send_html_mail(email, subject, text_body, html_body)

  return render_template('register_request_success.html')

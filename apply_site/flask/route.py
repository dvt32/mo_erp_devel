import sys
sys.path.append('/var/www/sites/{{ apply_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ apply_fqdn }}/lib')
from flask import Flask, render_template, g, request
import apply
import register
import registermail
import postal
import daypass_form
import password

app = Flask(__name__)
app.secret_key = '{{ apply_flask_secret_key }}'
app.debug = {{ apply_flask_app_debug }}

@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@app.route('/apply_virtual')
def apply_virtual():
  return apply.show_virtual_application()

@app.route('/submit_apply_virtual', methods=["POST"])
def submit_apply_virtual():
  return apply.apply_virtual()

@app.route('/zip_check', methods=['POST'])
def zip_check():
  return postal.zip_check()

@app.route('/terms_of_service')
def terms():
  return render_template('terms_of_service.html')

@app.route('/set_password/<token>')
def set_password(token):
  return password.set_password(token)

@app.route('/submit_password', methods=['POST'])
def submit_password():
  return password.set_password_result()

'''
Register does not take enough precautions to prevent attacks
Needs to be updated before enabling.
@app.route('/register', methods=['GET'])
def request_email():
  email = request.args.get('email', None)
  return registermail.show_email_form(email)

@app.route('/register/send_mail', methods=['POST'])
def send_email():
  return registermail.send_registration_email()

@app.route('/register/<token>')
def show_registration_form(token):
  return register.register(token)

@app.route('/register/result', methods=['POST'])
def submit_details():
  return register.register_result()
'''
# display the webpage of day pass
@app.route('/daypass')
def daypass():
  return daypass_form.show_daypassform()

# parse the data received from day pass
@app.route('/daypass/main', methods = ["POST"])
def contact_daypass():
  return daypass_form.parse_daypassform()

@app.context_processor
def define_static_url():
  return dict(static_base_url='{{ static_base_url }}')

from flask import g
import psycopg2

def get_connection():
  conn = getattr(g, '_database', None)
  if conn is None:
    conn = g._database = psycopg2.connect(database='{{ apply_db_name }}', user='{{ apply_db_username }}', password='{{ apply_db_password }}', host='{{ apply_db_host }}', sslmode='{{ apply_db_sslmode }}')
  return conn

import json
from flask import request
import psycopg2.extras
import re
import database


def zip_check():
  if request.form['country'] != 'US':
    return json.dumps("true")
  zip = request.form['zip']
  match = re.match(r'\d{5}(-\d{4})?', zip)
  if match == None:
    return json.dumps("Invalid zip code format: " + request.form['zip'])
  else:
    zip = zip[:5]
  state = request.form['state']
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM zip_code_tbl WHERE zip = %s AND state = %s LIMIT 1', (zip, state))
  result = curdict.fetchone()
  if not result:
    valid = "Invalid zip code for " + request.form['state']
  else: 
    valid = "true"
  return json.dumps(valid)

use strict;
local($^W) = 1;

use LWP::UserAgent;

my $faxage_url = "https://www.faxage.com/httpsfax.php";
my $username = "{{ faxage_username }}";
my $company = "{{ faxage_company }}";
my $password = "{{ faxage_password }}";

sub call_faxage_api {
  my %param_map = @_;
  my $user_agent = LWP::UserAgent->new();
  $param_map{"username"} = $username;
  $param_map{"company"} = $company;
  $param_map{"password"} = $password;
  my $response = $user_agent->post($faxage_url, \%param_map);
  my $response_content = $response->content();
  return $response_content;
}

1;

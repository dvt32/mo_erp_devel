use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use insert_row;

sub enqueue_email(%) {
  my %param_map = @_;
  my $dbh = $param_map{"dbh"};
  my $account_id = $param_map{"account_id"};
  my $from_hdr = $param_map{"from_hdr"};
  my $to_hdr = $param_map{"to_hdr"};
  my $date_hdr = $param_map{"date_hdr"};
  my $subject_hdr = $param_map{"subject_hdr"};
  my $message_body = $param_map{"message_body"};
  my $notes = $param_map{"notes"};
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["from_hdr", $from_hdr]);
  push(@sql_column_list, ["to_hdr", $to_hdr]);
  if ($date_hdr) {
    push(@sql_column_list, ["date_hdr", $date_hdr]);
  } else {
    push(@sql_column_list, ["date_hdr", ["now()"]]);
  }
  push(@sql_column_list, ["subject_hdr", $subject_hdr]);
  push(@sql_column_list, ["message_body", $message_body]);
  if ($notes) {
    push(@sql_column_list, ["notes", $notes]);
  }
  insert_row($dbh, "email_tbl", \@sql_column_list);
}

1;

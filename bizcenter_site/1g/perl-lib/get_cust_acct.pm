use strict;
local($^W) = 1;

use select_row;
use get_web_root_dir;
use lib "{{ bizcenter_commondir }}";
use get_database_handle;

sub get_cust_acct($) {
  my $instance_map = shift;
  if (!$instance_map->{"cust_acct"}) {
    if (my $cust_pers = get_cust_pers($instance_map)) {
      my $dbh = get_database_handle($instance_map, "main");
      if (my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_pers->{"cust_acct_id"})) {
	$instance_map->{"cust_acct"} = $cust_acct;
      }
    } else {
      my $web_root_dir = get_web_root_dir();
      die "show_page: $web_root_dir/auth/login_must_login.html\n";
    }
  }
  return $instance_map->{"cust_acct"};
}

1;

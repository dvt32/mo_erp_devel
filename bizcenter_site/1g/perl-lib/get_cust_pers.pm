use strict;
local($^W) = 1;

use get_cgi;
use get_session;
use select_row;
use lib "{{ bizcenter_commondir }}";
use get_database_handle;

sub get_cust_pers($) {
  my $instance_map = shift;
  if (!$instance_map->{"cust_pers"}) {
    my $session;
    if (($session = get_session($instance_map)) && ($session->{"cust_pers_id"})) {
      my $dbh = get_database_handle($instance_map, "main");
      my $person;
      if ($person = select_row($dbh, "cust_pers_tbl", $session->{"cust_pers_id"})) {
	if ((!$person->{"is_active"}) || (!$person->{"password"})) {
	  undef $person;
	} else {
	  $instance_map->{"cust_pers"} = $person;
	}
      }
    }
  }
  return $instance_map->{"cust_pers"};
}

1;

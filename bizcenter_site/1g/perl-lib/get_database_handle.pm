use strict;
local($^W) = 1;

use DBI;
use is_production_env;

sub get_database_handle($$) {
  my ($instance_map, $handle_name) = @_;
  if (!$instance_map->{"dbh"}) {
    $instance_map->{"dbh"} = {};
  }
  my $dbh;
  if (!($dbh = $instance_map->{"dbh"}->{$handle_name})) {
    my ($database_name, $database_hostname, $data_source, $database_username, $database_password, %database_attr);
    if ($handle_name eq "main") {
      $database_name = "{{ main_database_name }}";
      $database_hostname = "{{ main_database_hostname }}";
      $data_source = "dbi:Pg:dbname=$database_name;host=$database_hostname;sslmode={{ main_database_sslmode }}";
      $database_username = "{{ main_database_bizcenter_username }}";
      $database_password = "{{ main_database_bizcenter_password }}";
      %database_attr = ("pg_server_prepare" => 0);
    } elsif ($handle_name eq "phone") {
      $database_name = "phone";
      $database_hostname = "{{ phone_database_hostname }}";
      $data_source = "dbi:Pg:dbname=$database_name;host=$database_hostname;sslmode={{ phone_database_sslmode }}";
      $database_username = "{{ phone_database_username }}";
      $database_password = "{{ phone_database_password }}";
    } else {
      die "get_database_handle: bad handle name ($handle_name)\n";
    }
    if (scalar(keys(%database_attr)) > 0) {
      $dbh = DBI->connect($data_source, $database_username, $database_password, \%database_attr);
    } else {
      $dbh = DBI->connect($data_source, $database_username, $database_password);
    }
    $instance_map->{"dbh"}->{$handle_name} = $dbh;
  }
  return $dbh;
}

1;

use strict;
local($^W) = 1;

use lib "{{ bizcenter_commondir }}";
use is_production_env;

sub get_php_base_url {
  # php base url for template
  my $php_base_url = "https://bizcenter-devel-php.microoffice.com";
  if(is_production_env()){
    $php_base_url = "https://bizcenter-php.microoffice.com";
  }
  return $php_base_url;
}

1;

use strict;
local($^W) = 1;

use lib "{{ bizcenter_commondir }}";
use is_production_env;

sub get_public_id_cookie_name() {
  if (is_production_env()) {
    return "bizcenter.microoffice.com:public_id";
  } else {
    return "bizcenter-devel.microoffice.com:public_id";
  }
}

1;

use strict;
local($^W) = 1;

use get_cgi;
use get_cust_pers;
use get_web_root_dir;
use lib "{{ bizcenter_commondir }}";
use get_database_handle;

sub init_login_standard($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = (get_cgi($instance_map), get_database_handle($instance_map, "main"), get_cust_pers($instance_map));
  if (!$cust_pers) {
    my $web_root_dir = get_web_root_dir();
    die "show_page: $web_root_dir/auth/login_must_login.html\n";
  }
  return ($cgi, $dbh, $cust_pers);
}

1;

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_cgi_input_charset;
use get_database_charset;
use update_with_statement;
use show_page;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $old_password = Encode::decode($cgi_input_charset, $cgi->param("old_password"));
  my $new_password = Encode::decode($cgi_input_charset, $cgi->param("new_password"));
  my $new_password_2 = Encode::decode($cgi_input_charset, $cgi->param("new_password_2"));
  # user entered a bad old password. display error.
  if ((!$old_password) || ($old_password ne $cust_pers->{"password"})) {
    die "show_page: change_password_bad_old_password.html\n";
  }
  # user did not enter a new password.
  if ((!$new_password) && (!$new_password_2)) {
    die "show_page: change_password_must_enter_new_password.html\n";
  }
  # user did not enter matching new passwords.
  if ($new_password ne $new_password_2) {
    die "show_page: change_password_new_password_mismatch.html\n";
  }
  # if the stored password is NULL or empty string, then no login possible, so don't update the password. Only a Micro Office
  # employee can set a password if it is currently NULL or empty.
  my $db_charset = get_database_charset($instance_map, "main");
  my $sql_stmt = "update cust_pers_tbl set password = ? where id = ? and is_active and length(password) > 0";
  update_with_statement($dbh, $sql_stmt, [Encode::encode($db_charset, $new_password), [$cust_pers->{"id"}, SQL_INTEGER]]);
  # display success page.
  show_page($instance_map, "change_password_success.html");
}

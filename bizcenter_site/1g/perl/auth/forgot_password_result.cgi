#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use get_cgi_input_charset;
use get_database_charset;
use select_row_with_query;
use select_row;
use send_email;
use show_page;
use strip_outside_whitespace;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email"));
  # user did not enter email. display error.
  if ((!$email) || (!($email = strip_outside_whitespace($email)))) {
    die "show_page: forgot_password_must_enter_email.html\n";
  }
  # make email lowercase to avoid case problems.
  $email = lc($email);
  my $db_charset = get_database_charset($instance_map, "main");
  my $sql_query = "select * from cust_pers_tbl where email = ? and length(password) > 0 and is_active";
  my $cust_pers = select_row_with_query($dbh, $sql_query, [Encode::encode($db_charset, $email)]);
  # no row with matching email found. display error. 
  if (!$cust_pers) {
    die "show_page: forgot_password_not_found.html\n";
  }
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_pers->{"cust_acct_id"});
  if ((!$cust_acct) || (!$cust_acct->{"is_active"})) {
    die "show_page: forgot_password_not_found.html\n";
  }
  # found matching email.
  my $password = $cust_pers->{"password"};
  # check if password is blank.
  if (!$password) {
    die "show_page: forgot_password_no_password.html\n";
  }
  # send password in email and display success page. send password via SMTP.
  my $message .= <<"---END_BLOCK---;";
Your password for the Micro Office Solutions online business center (https://bizcenter.microoffice.com) is $password.

We advise you to change the password to something that is both secure (hard to guess for other people) and easy for you to remember.
---END_BLOCK---;
  send_email
    ("from" => "Micro Office Solutions <service\@microoffice.com>",
     "to" => $email,
     "subject" => "Your password request for Micro Office online business center.",
     "message" => $message);
  show_page($instance_map, "forgot_password_found.html");
}

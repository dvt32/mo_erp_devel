#!/usr/bin/perl

use strict;
local($^W) = 1;

use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use get_cgi_input_charset;
use get_database_charset;
use create_session;
use select_row_with_query;
use show_page;
use strip_outside_whitespace;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email"));
  my $password = Encode::decode($cgi_input_charset, $cgi->param("password"));
  my $is_persistent = $cgi->param("is_persistent");
  # user did not enter email. display error.
  if ((!$email) || (!($email = strip_outside_whitespace($email)))) {
    die "show_page: login_must_enter_email.html\n";
  }
  # make email lowercase to avoid case problems.
  $email = lc($email);
  my $db_charset = get_database_charset($instance_map, "main");
  my $sql_query = "select * from cust_pers_tbl where email = ? and is_active and length(password) > 0";
  my $cust_pers = select_row_with_query($dbh, $sql_query, [Encode::encode($db_charset, $email)]);
  # no cust_pers with matching email found, or cust_pers does not support login (due to blank password). display error. 
  if ((!$cust_pers) || (!$cust_pers->{"password"})) {
    die "show_page: login_result_bad.html\n";
  }
  # bad password (either blank or not match). display error.
  if ((!$password) || ($password ne $cust_pers->{"password"})) {
    die "show_page: login_result_bad.html\n";
  }
  # check that linked cust_acct is also active.
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_pers->{"cust_acct_id"});
  if ((!$cust_acct) || (!$cust_acct->{"is_active"})) {
    die "show_page: login_result_bad.html\n";
  }
  # found matching email and password. display success page.
  my $cookie = create_session($instance_map, "cust_pers_id", $cust_pers->{"id"}, $is_persistent);
  show_page($instance_map, "login_result_good.html", $cookie);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use get_cust_acct;
use select_row;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  my $cust_acct = get_cust_acct($instance_map);
  my $floor_id = $cust_acct->{"primary_floor_id"};
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  my $building_id = $floor->{"building_id"};
  # select all cust_accts in the same building as the tenant.
  my %cust_acct_by_id;
  my $sql_query = "select * from cust_acct_tbl where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
  my @sql_param_list = ([$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_acct_by_id);
  # select all people in the same building as the tenant.
  my (%cust_pers_by_id, @cust_pers_id_list);
  $sql_query = "select * from cust_pers_tbl where is_active and cust_pers_tbl.cust_acct_id in (select id from cust_acct_tbl where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)) order by last_name, first_name, middle_name";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_pers_by_id, \@cust_pers_id_list);
  # select all visible phone numbers for people in the same building.
  my (%phone_number_by_id, %phone_number_id_list_by_person_id);
  $sql_query = "select * from phone_number_tbl where visibility <> ?";
  @sql_param_list = ("hidden");
  get_row_by_id_map($dbh_phone, $sql_query, \@sql_param_list, \%phone_number_by_id);
  for my $phone_number (values(%phone_number_by_id)) {
    if (defined($phone_number->{"person_id"})) {
      push(@{$phone_number_id_list_by_person_id{$phone_number->{"person_id"}}}, $phone_number->{"id"});
    }
  }
  # sort the phone number id lists in by_person_id.
  for my $phone_number_id_list (values(%phone_number_id_list_by_person_id)) {
    @{$phone_number_id_list} = sort {$phone_number_by_id{$a}->{"extension"} cmp $phone_number_by_id{$b}->{"extension"}} @{$phone_number_id_list};
  }
  my @directory_entry_list;
  my $directory_entry;
  for my $cust_pers_id (@cust_pers_id_list) {
    my $cust_pers = $cust_pers_by_id{$cust_pers_id};
    my $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    my $cust_acct_id = $cust_pers->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $cust_acct_name = $cust_acct->{"name"};
    my $phone_number_id_list = $phone_number_id_list_by_person_id{$cust_pers_id};
    if (defined($phone_number_id_list) && (scalar(@$phone_number_id_list) > 0)) {
      if (has_external_phone_number($cust_pers)) {
	my $phone_number_id = $phone_number_id_list->[0];
	my $phone_number = $phone_number_by_id{$phone_number_id};
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"cust_acct_name"} = $cust_acct_name;
	$directory_entry->{"intercom"} = $phone_number->{"extension"};
	$directory_entry->{"external_phone_numbers"} = render_external_phone_number($cust_pers);
	push(@directory_entry_list, $directory_entry);
      } else {
	my $phone_number_id = $phone_number_id_list->[0];
	my $phone_number = $phone_number_by_id{$phone_number_id};
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"cust_acct_name"} = $cust_acct_name;
	$directory_entry->{"intercom"} = $phone_number->{"extension"};
	$directory_entry->{"external_phone_numbers"} = "";
	push(@directory_entry_list, $directory_entry);
      }
      for(my $j = 1; $j < @$phone_number_id_list; $j++) {
	my $phone_number_id = $phone_number_id_list->[$j];
	my $phone_number = $phone_number_by_id{$phone_number_id};
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"cust_acct_name"} = $cust_acct_name;
	$directory_entry->{"intercom"} = $phone_number->{"extension"};
	$directory_entry->{"external_phone_numbers"} = "";
	push(@directory_entry_list, $directory_entry);
      }
    } else {
      if (has_external_phone_number($cust_pers)) {
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"cust_acct_name"} = $cust_acct_name;
	$directory_entry->{"intercom"} = "";
	$directory_entry->{"external_phone_numbers"} = render_external_phone_number($cust_pers);
	push(@directory_entry_list, $directory_entry);
      }
    }
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <row-list>
---END_BLOCK---;
  for $directory_entry (@directory_entry_list) {
    my $person_name = HTML::Entities::encode($directory_entry->{"person_name"});
    my $cust_acct_name = HTML::Entities::encode($directory_entry->{"cust_acct_name"});
    my $intercom = $directory_entry->{"intercom"};
    my $external_phone_numbers = HTML::Entities::encode($directory_entry->{"external_phone_numbers"});
    $generated_xml .= <<"---END_BLOCK---;";
    <row>
      <person-name>$person_name</person-name>
      <account-name>$cust_acct_name</account-name>
      <intercom>$intercom</intercom>
      <external-phone-numbers>$external_phone_numbers</external-phone-numbers>
    </row>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </row-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "by_person.xslt", $cgi);
}

sub has_external_phone_number($) {
  my $cust_pers = shift;
  my $has_number = 0;
  for(my $i = 1; $i <= 3; $i++) {
    if (my $phone_number_type = $cust_pers->{"directory_$i"}) {
      if (my $phone_number = $cust_pers->{$phone_number_type . "_phone_number"}) {
	if ($phone_number !~ /^\s*$/) {
	  $has_number = 1;
	}
      }
    }
  }
  return $has_number;
}

sub render_external_phone_number($) {
  my $cust_pers = shift;
  my @phone_number_list;
  for(my $i = 1; $i <= 3; $i++) {
    if (my $phone_number_type = $cust_pers->{"directory_$i"}) {
      if (my $phone_number = $cust_pers->{$phone_number_type . "_phone_number"}) {
	if (($phone_number !~ /^\s*$/) && (!grep {$_ eq $phone_number} @phone_number_list)) {
	  push(@phone_number_list, $phone_number);
	}
      }
    }
  }
  return join(" / ", @phone_number_list);
}


#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use update_row;
use add_time;
use get_timestamp_now;
use timestamp_to_components;
use components_to_timestamp;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $inbound_fax_alert_duration = $cgi->param("inbound_fax_alert_duration");
  if ((!$inbound_fax_alert_duration) || ($inbound_fax_alert_duration !~ /^\d+$/) || ($inbound_fax_alert_duration > 24)) {
    die "show_page: activate_inbound_fax_alerts_bad_duration.html\n";
  }
  my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components(get_timestamp_now());
  ($year, $month, $day, $hour, $minute, $second) =
    add_time($year, $month, $day, $hour, $minute, $second, $inbound_fax_alert_duration, "hour");
  my $inbound_fax_alert_cutoff_dt = components_to_timestamp($year, $month, $day, $hour, $minute, $second);
  my @sql_column_list = (["inbound_fax_alert_cutoff_dt", $inbound_fax_alert_cutoff_dt]);
  update_row($dbh, "cust_pers_tbl", \@sql_column_list, $cust_pers->{"id"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <inbound_fax_alert_cutoff_dt>$inbound_fax_alert_cutoff_dt</inbound_fax_alert_cutoff_dt>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "activate_inbound_fax_alerts.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Business Center :: Batch Resend Fax
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/show_my_docs.cgi">My Documents</a>
        </p>
        <form action="/imaging/batch_resend_fax_result.cgi" method="post" enctype="multipart/form-data">
	  <table>
	    <tr>
	      <td>
		File #1 to Fax:
	      </td>
	      <td>
		<input type="file" name="file_1"/>
		(PDF, DOC, or DOCX)
	      </td>
	    </tr>
	    <tr>
	      <td>
		File #2 to Fax:
	      </td>
	      <td>
		<input type="file" name="file_2"/>
		(PDF, DOC, or DOCX)
	      </td>
	    </tr>
	    <tr>
	      <td>
		File #3 to Fax:
	      </td>
	      <td>
		<input type="file" name="file_3"/>
		(PDF, DOC, or DOCX)
	      </td>
	    </tr>
	  </table>
	  <xsl:apply-templates select="sent_fax_list"/>
	  <p>
	    You are limited to 10 recipients at a time. If you need to send to more than 10 people, perform your send in groups
	    of 10. Do not use this batch resend fax feature to send unsolicited faxes. If you abuse this feature, your access
	    to faxing will be blocked.
	  </p>
	  <p>
	    Do you want a private, dedicated fax number so that fax responses are delivered straight to your email, with
	    notification the instant the fax comes in? If you get a lot of faxes or if you get faxes at arbitrary times in the
	    day without prior warning from people, the private, dedicated fax number can be a helpful time-saver. Email <a
	    href="mailto:service@microoffice.com">service@microoffice.com</a> to sign up for the private, dedicated fax
	    service. There is a monthly fee.
	  </p>
	  <p>
	    <input type="submit" name="submitButton" value="Batch Send Fax"/>
	  </p>
        </form>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sent_fax_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="21" class="standard_table_title">
          Sent Faxes
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td></td>
	<td class="standard_table_spacer_column"></td>
	<td>Job ID</td>
	<td class="standard_table_spacer_column"></td>
	<td>Recipient Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Fax Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Status</td>
	<td class="standard_table_spacer_column"></td>
	<td>Extended Status</td>
	<td class="standard_table_spacer_column"></td>
	<td>Time Sent (MT)</td>
	<td class="standard_table_spacer_column"></td>
	<td>Time Completed (MT)</td>
	<td class="standard_table_spacer_column"></td>
	<td>Xmit Time</td>
	<td class="standard_table_spacer_column"></td>
	<td>Pages</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(sent_fax) > 0">
	  <xsl:apply-templates select="sent_fax"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="21" class="standard_table_body">
	      <div style="text-align: center">You have no recently sent faxes.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
    <p>
      Note: (MT) stands for Mountain Time. The fax server is in Colorado, so the times are reported in that time zone.
      Mountain Time is two hours behind Eastern Time, so add two hours to the times shown.
    </p>
  </xsl:template>

  <xsl:template match="sent_fax">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<input>
	  <xsl:attribute name="type">checkbox</xsl:attribute>
	  <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	</input>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="job_id"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="recipient_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="fax_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="short_status"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="long_status"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="send_time"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="complete_time"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="xmit_time"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="page_count"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

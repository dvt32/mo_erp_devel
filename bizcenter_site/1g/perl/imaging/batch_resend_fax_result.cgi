#!/usr/bin/perl

use strict;
local($^W) = 1;

use CGI;
$CGI::POST_MAX = 8 * 1024 * 1024; # max 8MB uploads
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use send_fax;

my $MAX_FAX_QTY = 10;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my (@filename_list, @file_content_list);
  local($/) = undef;
  for(my $i = 1; $i <= 3; $i++) {
    my $filename = $cgi->param("file_$i");
    my $fh = $cgi->upload("file_$i");
    if ($fh) {
      push(@filename_list, $filename);
      push(@file_content_list, scalar(<$fh>));
    }
  }
  if (!@filename_list) {
    die "show_page: batch_resend_fax_no_documents.html\n";
  }
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @fax_out_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@fax_out_id_list, $1);
    }
  }
  if (scalar(@fax_out_id_list) > $MAX_FAX_QTY) {
    die "show_page: batch_resend_fax_exceeded_max_qty.html\n";
  }
  @fax_out_id_list = sort {$a <=> $b} @fax_out_id_list;
  my $db_charset = get_database_charset($instance_map, "main");
  my @new_fax_out_list;
  eval {
    for my $fax_out_id (@fax_out_id_list) {
      my $fax_out = select_row($dbh, "fax_out_tbl", $fax_out_id);
      my $recipient_name = $fax_out->{"recipient_name"};
      my $fax_number = $fax_out->{"fax_number"};
      my $job_id = send_fax
	(
	 "dbh" => $dbh,
	 "db_charset" => $db_charset,
	 "sending_person" => $cust_pers,
	 "sending_account_type" => "customer",
	 "recipient_name" => $recipient_name,
	 "fax_number" => $fax_number,
	 "filename_list" => \@filename_list,
	 "file_content_list" => \@file_content_list,
	 "send_email_alert" => 0,
	);
      my %new_fax_out = ("job_id" => $job_id, "recipient_name" => $recipient_name, "fax_number" => $fax_number);
      push(@new_fax_out_list, \%new_fax_out);
    }
  };
  if ($@) {
    my $error_message = HTML::Entities::encode($@);
    chomp($error_message);
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <error_message>$error_message</error_message>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "batch_resend_fax_error.xslt", $cgi);
    return;
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sent_fax_list>
---END_BLOCK---;
  for my $new_fax_out (@new_fax_out_list) {
    my $job_id = $new_fax_out->{"job_id"};
    my $recipient_name = HTML::Entities::encode($new_fax_out->{"recipient_name"});
    my $fax_number = HTML::Entities::encode($new_fax_out->{"fax_number"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sent_fax>
      <job_id>$job_id</job_id>
      <recipient_name>$recipient_name</recipient_name>
      <fax_number>$fax_number</fax_number>
    </sent_fax>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </sent_fax_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "batch_resend_fax_result.xslt", $cgi);
}

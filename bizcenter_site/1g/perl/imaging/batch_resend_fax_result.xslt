<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Business Center :: Batch Resend Fax (submitted)
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/show_my_docs.cgi">My Documents</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
	  |
	  <a href="/1g/imaging/batch_resend_fax_input.cgi">Batch Resend Fax</a>
        </p>
	<p>
	  Your faxes were submitted into the queue. Please check the <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a> page for status.
	</p>
	<xsl:apply-templates select="sent_fax_list"/>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sent_fax_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="7" class="standard_table_title">
          Batch Sent Faxes
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Job ID</td>
	<td class="standard_table_spacer_column"></td>
	<td>Recipient Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Fax Number</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(sent_fax) > 0">
	  <xsl:apply-templates select="sent_fax"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="7" class="standard_table_body">
	      <div style="text-align: center">You did not choose any recipients.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
    <p>
      Note: (MT) stands for Mountain Time. The fax server is in Colorado, so the times are reported in that time zone.
      Mountain Time is two hours behind Eastern Time, so add two hours to the times shown.
    </p>
  </xsl:template>

  <xsl:template match="sent_fax">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td><xsl:value-of select="job_id"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="recipient_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="fax_number"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

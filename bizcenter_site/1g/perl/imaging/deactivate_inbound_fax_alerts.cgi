#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my @sql_column_list = (["inbound_fax_alert_cutoff_dt", undef]);
  update_row($dbh, "cust_pers_tbl", \@sql_column_list, $cust_pers->{"id"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "deactivate_inbound_fax_alerts.xslt", $cgi);
}

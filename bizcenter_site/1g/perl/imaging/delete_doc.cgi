#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use delete_row;
use delete_file;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $document_id = $cgi->param("id");
  my $document;
  if ((!$document_id) || (!($document = select_row($dbh, "document_vw", $document_id)))) {
    die "show_page: delete_doc_bad_id.html\n";
  }
  if ((!$document->{"account_id"}) || ($document->{"account_id"} != $cust_pers->{"cust_acct_id"}) || (!$document->{"person_id"}) || ($document->{"person_id"} != $cust_pers->{"id"})) {
    die "show_page: delete_doc_not_owner.html\n";
  }
  my $tbl_name;
  if ($document->{"type"} eq "scan") {
    $tbl_name = "scan_tbl";
  } elsif ($document->{"type"} eq "fax") {
    $tbl_name = "fax_in_tbl";
  } else {
    die "show_page: delete_doc_unknown_document_type.html\n";
  }
  $dbh->begin_work();
  delete_row($dbh, $tbl_name, $document_id);
  delete_file($instance_map, $document->{"file_id"});
  $dbh->commit();
  # generate XML.
  my $type = HTML::Entities::encode($document->{"type"});
  my $filename = HTML::Entities::encode($document->{"filename"});
  my $file_size = int($document->{"file_size"} / 1024) . "K";
  my $file_timestamp = $document->{"file_timestamp"};
  my $note = HTML::Entities::encode($document->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <document>
    <id>$document_id</id>
    <type>$type</type>
    <filename>$filename</filename>
    <file_size>$file_size</file_size>
    <file_timestamp>$file_timestamp</file_timestamp>
    <note>$note</note>
  </document>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_doc.xslt", $cgi);
}

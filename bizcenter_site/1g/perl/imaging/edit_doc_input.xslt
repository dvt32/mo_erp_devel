<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Document
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/show_my_docs.cgi">My Documents</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
        </p>
	<p>
	  Please enter a descriptive note because the note will identify this document in the list of documents
	  belonging to you. Click on the filename to view the document.
	</p>
	<xsl:apply-templates select="document"/>
	<p>
	  Actions you may take on this document:
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>/imaging/download_doc.cgi?id=</xsl:text>
	      <xsl:value-of select="document/id"/>
	    </xsl:attribute>
	    <xsl:text>Download</xsl:text>
	  </a>
	  &middot;
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>/imaging/unclaim_doc.cgi?id=</xsl:text>
	      <xsl:value-of select="document/id"/>
	    </xsl:attribute>
	    <xsl:text>Unclaim</xsl:text>
	  </a>
	  &middot;
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>/imaging/delete_doc.cgi?id=</xsl:text>
	      <xsl:value-of select="document/id"/>
	    </xsl:attribute>
	    <xsl:text>Delete</xsl:text>
	  </a>
	</p>
	<p>
	  If you accidentally claimed a document belonging to someone else, please do not delete it! Instead, unclaim it by
	  clicking on "Unclaim" so that the rightful owner can claim it. To download/save a document to your hard drive, click on
	  "Download". To delete a document from your list, click on "Delete".
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="document">
    <table>
      <tr><td>
	<form action="/imaging/edit_doc_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Document
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Timestamp:
		</td>
		<td>
		  <xsl:value-of select="file_timestamp"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Type:
		</td>
		<td>
		  <xsl:value-of select="type"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Filename:
		</td>
		<td>
		  <a>
		    <xsl:attribute name="href">
		      <xsl:text>/imaging/view_doc.cgi?id=</xsl:text>
		      <xsl:value-of select="id"/>
		    </xsl:attribute>
		    <xsl:value-of select="filename"/>
		  </a>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Size:
		</td>
		<td>
		  <xsl:value-of select="file_size"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

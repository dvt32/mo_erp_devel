#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use update_fax_status;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  # retrieve sent fax status information.
  my (%fax_out_by_id, @fax_out_id_list);
  my $sql_query = "select * from fax_out_tbl where person_id = ? order by creation_dt desc limit 20";
  my @sql_param_list = ([$cust_pers->{"id"}, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%fax_out_by_id, \@fax_out_id_list);
  my %fax_out_by_job_id;
  for my $fax_out (values(%fax_out_by_id)) {
    $fax_out_by_job_id{$fax_out->{"job_id"}} = $fax_out;
  }
  # if there are rows in fax_out_tbl without a status, get the status from FaxAge.
  if (grep {(!$fax_out_by_id{$_}->{"short_status"}) || ($fax_out_by_id{$_}->{"short_status"} eq "pending")} @fax_out_id_list) {
    update_fax_status($dbh, \%fax_out_by_job_id);
  }
  # generate XML.
  my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <cust_pers>
    <email>$email</email>
  </cust_pers>
  <sent-fax-list>
---END_BLOCK---;
  for my $fax_out_id (@fax_out_id_list) {
    my $fax_out = $fax_out_by_id{$fax_out_id};
    my $job_id = $fax_out->{"job_id"};
    my $recipient_name = HTML::Entities::encode($fax_out->{"recipient_name"});
    my $fax_number = $fax_out->{"fax_number"};
    my $short_status = HTML::Entities::encode($fax_out->{"short_status"} || "");
    my $long_status = HTML::Entities::encode($fax_out->{"long_status"} || "");
    my $send_time = HTML::Entities::encode($fax_out->{"send_time"} || "");
    my $complete_time = HTML::Entities::encode($fax_out->{"complete_time"} || "");
    my $xmit_time = HTML::Entities::encode($fax_out->{"xmit_time"} || "");
    my $page_count = $fax_out->{"page_count"} || "";
    $generated_xml .= <<"---END_BLOCK---;";
    <sent-fax>
      <id>$fax_out_id</id>
      <job_id>$job_id</job_id>
      <recipient_name>$recipient_name</recipient_name>
      <fax_number>$fax_number</fax_number>
      <short_status>$short_status</short_status>
      <long_status>$long_status</long_status>
      <send_time>$send_time</send_time>
      <complete_time>$complete_time</complete_time>
      <xmit_time>$xmit_time</xmit_time>
      <page_count>$page_count</page_count>
    </sent-fax>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </sent-fax-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "send_fax_input.xslt", $cgi);
}

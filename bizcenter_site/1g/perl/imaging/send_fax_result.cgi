#!/usr/bin/perl

use strict;
local($^W) = 1;

use CGI;
$CGI::POST_MAX = 16 * 1024 * 1024; # max 16 MB uploads
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use send_fax;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $recipient_name = Encode::decode($cgi_input_charset, $cgi->param("recipient_name"));
  if ((!$recipient_name) || (length($recipient_name) > 32)) {
    die "show_page: send_fax_bad_recipient_name.html\n";
  }
  my $fax_number = $cgi->param("fax_number");
  if ((!$fax_number) || ($fax_number !~ /^\d{10}$/) || ($fax_number =~ /^1/)) {
    die "show_page: send_fax_bad_fax_number.html\n";
  }
  my (@filename_list, @file_content_list);
  local($/) = undef;
  for(my $i = 1; $i <= 3; $i++) {
    my $filename = $cgi->param("file_$i");
    my $fh = $cgi->upload("file_$i");
    if ($fh) {
      push(@filename_list, $filename);
      push(@file_content_list, scalar(<$fh>));
    }
  }
  if (!@filename_list) {
    die "show_page: send_fax_no_documents.html\n";
  }
  my $send_email_alert = $cgi->param("send_email_alert") || 0;
  my $job_id;
  my $db_charset = get_database_charset($instance_map, "main");
  eval {
    $job_id = send_fax
      (
       "dbh" => $dbh,
       "db_charset" => $db_charset,
       "sending_person" => $cust_pers,
       "sending_account_type" => "customer",
       "recipient_name" => $recipient_name,
       "fax_number" => $fax_number,
       "filename_list" => \@filename_list,
       "file_content_list" => \@file_content_list,
       "send_email_alert" => $send_email_alert,
      );
  };
  if ($@) {
    my $error_message = HTML::Entities::encode($@);
    chomp($error_message);
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <error_message>$error_message</error_message>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "send_fax_error.xslt", $cgi);
    return;
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <job_id>$job_id</job_id>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "send_fax_result.xslt", $cgi);
}

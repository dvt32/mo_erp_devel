#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my (%document_by_id, @document_id_list);
  my $sql_query = "select * from document_vw where account_id = ? and person_id = ? order by file_timestamp";
  my @sql_param_list = ([$cust_pers->{"cust_acct_id"}, SQL_INTEGER], [$cust_pers->{"id"}, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%document_by_id, \@document_id_list);
  # generate XML.
  my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <cust_pers>
    <email>$email</email>
  </cust_pers>
  <document-list>
---END_BLOCK---;
  for my $document_id (@document_id_list) {
    my $document = $document_by_id{$document_id};
    my $type = HTML::Entities::encode($document->{"type"});
    my $filename = HTML::Entities::encode($document->{"filename"});
    my $file_size = int($document->{"file_size"} / 1024) . "K";
    my $file_timestamp = $document->{"file_timestamp"};
    my $note = HTML::Entities::encode($document->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <document>
      <id>$document_id</id>
      <type>$type</type>
      <filename>$filename</filename>
      <file_size>$file_size</file_size>
      <file_timestamp>$file_timestamp</file_timestamp>
      <note>$note</note>
    </document>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </document-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_my_docs.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Show My Documents
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
	<script src="/1g/js/toggle_checkboxes.js">
	  &nbsp;
	</script>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
        </p>
	<xsl:apply-templates select="document-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="document-list">
    <form action="/imaging/act_on_my_docs.cgi" method="post">
      <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	<tr>
	  <td colspan="13" class="standard_table_title">
	    My Documents
	  </td>
	</tr>
	<tr class="standard_table_column_heading_row">
	  <td class="standard_table_side_padding"></td>
	  <td align="center">
	    <input type="checkbox" name="select_all" onclick="toggle_checkboxes(this, 'id-');"/>
	  </td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Timestamp</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Type</td>
	  <td class="standard_table_spacer_column"></td>
	  <td align="right">Size</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Filename</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Note</td>
	  <td class="standard_table_side_padding"></td>
	</tr>
	<xsl:choose>
	  <xsl:when test="count(document) > 0">
	    <xsl:apply-templates select="document"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <tr>
	      <td colspan="13" align="center">You have no documents.</td>
	    </tr>
	  </xsl:otherwise>
	</xsl:choose>
      </table>
      <p>
	<input type="submit" name="submit_button" value="Delete Selected Documents"/>
      </p>
      <p>
	If you just want to view your documents, click on the filename of the desired document(s) above.

	If you want to delete one or more documents, click the checkboxes next to the documents you would like to delete, then
	click on the "Delete Selected Documents" button. If you want to fax one or more of your documents, click the checkboxes
	next to the documents you would like to fax, then fill out the recipient name and fax number below and then click on
	the "Send Fax" button below to send the document(s) as a fax.

	To perform other actions on a document, click on the note. Actions you may perform include changing the note,
	downloading the document, unclaiming the document, and deleting the document. If you accidentally claimed a document
	belonging to someone else, please do not delete it! Instead, unclaim it by clicking on the note, then clicking on
	"Unclaim" so that the rightful owner can claim it.
      </p>
      <table>
	<tr>
	  <td>
	    Recipient Name:
	  </td>
	  <td>
	    <input type="text" maxlength="32" name="recipient_name"/>
	  </td>
	</tr>
	<tr>
	  <td>
	    Fax Number:
	  </td>
	  <td>
	    <input type="text" maxlength="10" name="fax_number"/>
	    (10 digits, no punctuation or spaces)
	  </td>
	</tr>
	<xsl:if test="/page/cust_pers/email != ''">
	  <tr>
	    <td>
	      Email Alert:
	    </td>
	    <td>
	      <select name="send_email_alert">
		<option value="1">Yes</option>
		<option value="0" selected="selected">No</option>
	      </select>
	      (if "Yes", email <xsl:value-of select="/page/cust_pers/email"/> with success/failure)
	    </td>
	  </tr>
	</xsl:if>
      </table>
      <p>
	<input type="submit" name="submit_button" value="Send Selected Documents as Fax"/>
      </p>
      <p>
	Do you want a private, dedicated fax number so that fax responses are delivered straight to your email, with
	notification the instant the fax comes in? If you get a lot of faxes or if you get faxes at arbitrary times in the day
	without prior warning from people, the private, dedicated fax number can be a helpful time-saver. Email <a
	href="mailto:service@microoffice.com">service@microoffice.com</a> to sign up for the private, dedicated fax
	service. There is a monthly fee.
      </p>
    </form>
  </xsl:template>

  <xsl:template match="document">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
	<input>
	  <xsl:attribute name="type">checkbox</xsl:attribute>
	  <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	</input>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="file_timestamp"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="type"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td align="right">
	<xsl:value-of select="file_size"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/imaging/view_doc.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="filename"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/imaging/edit_doc_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="note"/>
	</a>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use get_timestamp_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my (%fax_in_by_id, @fax_in_id_list);
  my $sql_query = "select * from fax_in_tbl where fax_number = ? and account_id is null order by file_timestamp";
  my @sql_param_list = ("6462531258");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%fax_in_by_id, \@fax_in_id_list);
  # generate XML.
  my $email = HTML::Entities::encode($cust_pers->{"email"});
  my $inbound_fax_alert_cutoff_dt = $cust_pers->{"inbound_fax_alert_cutoff_dt"} || "";
  if (get_timestamp_now() gt $inbound_fax_alert_cutoff_dt) {
    $inbound_fax_alert_cutoff_dt = "";
  }
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <cust_pers>
    <email>$email</email>
    <inbound_fax_alert_cutoff_dt>$inbound_fax_alert_cutoff_dt</inbound_fax_alert_cutoff_dt>
  </cust_pers>
  <fax_in_list>
---END_BLOCK---;
  for my $fax_in_id (@fax_in_id_list) {
    my $fax_in = $fax_in_by_id{$fax_in_id};
    my $filename = HTML::Entities::encode($fax_in->{"filename"});
    my $file_size = int($fax_in->{"file_size"} / 1024) . "K";
    my $file_timestamp = $fax_in->{"file_timestamp"};
    $generated_xml .= <<"---END_BLOCK---;";
    <fax_in>
      <id>$fax_in_id</id>
      <filename>$filename</filename>
      <file_size>$file_size</file_size>
      <file_timestamp>$file_timestamp</file_timestamp>
    </fax_in>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </fax_in_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_unclaimed_faxes.xslt", $cgi);
}

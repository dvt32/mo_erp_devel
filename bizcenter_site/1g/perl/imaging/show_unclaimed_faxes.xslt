<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Show Unclaimed Faxes
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_my_docs.cgi">My Documents</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
        </p>
	<p>
	  Please view faxes below and claim the ones that are yours. You can manipulate the document once you have claimed
	  it. To view a document, click on the filename of the document. To claim a document, click on the "Claim" next to the
	  filename of the relevant document. Please do not claim documents that do not belong to you. If you claim a document
	  by accident, in "My Documents", find the document and click on the note, then click "Unclaim" so the proper owner can
	  claim it. To receive faxes here, tell your counterparty to send their fax to 646-253-1258. It will arrive in this
	  queue. Be sure to read the <a href="usage_policy_fax.html">usage policy</a> for this shared fax service.
	</p>
	<xsl:apply-templates select="fax_in_list"/>
	<p>
	  <form action="/imaging/activate_inbound_fax_alerts.cgi">
	    Send email alerts to
	    <xsl:value-of select="cust_pers/email"/>
	    for shared faxes that arrive within next
	    <select name="inbound_fax_alert_duration">
	      <option value="1">1 hour</option>
	      <option value="2">2 hours</option>
	      <option value="4">4 hours</option>
	      <option value="8">8 hours</option>
	      <option value="24">24 hours</option>
	    </select>
	    <input type="submit" name="submitButton" value="Go"/>
	  </form>
	</p>
	<p>
	  <xsl:choose>
	    <xsl:when test="cust_pers/inbound_fax_alert_cutoff_dt = ''">
	      You do not currently have inbound fax alerts activated.
	    </xsl:when>
	    <xsl:otherwise>
	      Fax alerts will be sent until <xsl:value-of select="cust_pers/inbound_fax_alert_cutoff_dt"/>.
	      You can <a href="/1g/imaging/deactivate_inbound_fax_alerts.cgi">deactivate inbound fax alerts</a>.
	    </xsl:otherwise>
	  </xsl:choose>
	  Do you want a private, dedicated fax number so that you do not have to look through a shared queue and so that all
	  faxes are emailed to you? If you get a lot of faxes or if you get faxes at arbitrary times in the day without prior
	  warning from people, the private, dedicated fax number can be a helpful time-saver. Email <a
	  href="mailto:service@microoffice.com">service@microoffice.com</a> to sign up for the private, dedicated fax
	  service. There is a monthly fee.
	</p>
	<p>
	  See <a href="usage_policy_fax.html">usage policy</a> for this shared fax service.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="fax_in_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="9" class="standard_table_title">
          Unclaimed Faxes on 646-253-1258
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Timestamp</td>
	<td class="standard_table_spacer_column"></td>
	<td align="right">Size</td>
	<td class="standard_table_spacer_column"></td>
	<td>Filename</td>
	<td class="standard_table_spacer_column"></td>
	<td>Claim Document</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(fax_in) > 0">
	  <xsl:apply-templates select="fax_in"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="9" align="center">There are no unclaimed faxes.</td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="fax_in">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="file_timestamp"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td align="right">
	<xsl:value-of select="file_size"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/imaging/view_doc.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="filename"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td align="center">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/imaging/claim_doc_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:text>Claim</xsl:text>
	</a>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

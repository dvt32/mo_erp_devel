#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my (%scan_by_id, @scan_id_list);
  my $sql_query = "select * from scan_tbl where account_id is null order by file_timestamp";
  get_row_by_id_map($dbh, $sql_query, undef, \%scan_by_id, \@scan_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <scan-list>
---END_BLOCK---;
  for my $scan_id (@scan_id_list) {
    my $scan = $scan_by_id{$scan_id};
    my $filename = HTML::Entities::encode($scan->{"filename"});
    my $file_size = int($scan->{"file_size"} / 1024) . "K";
    my $file_timestamp = $scan->{"file_timestamp"};
    $generated_xml .= <<"---END_BLOCK---;";
    <scan>
      <id>$scan_id</id>
      <filename>$filename</filename>
      <file_size>$file_size</file_size>
      <file_timestamp>$file_timestamp</file_timestamp>
    </scan>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </scan-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_unclaimed_scans.xslt", $cgi);
}

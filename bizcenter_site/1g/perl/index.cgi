#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_php_base_url;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_name = HTML::Entities::encode($cust_acct->{"name"});
  my $first_name = HTML::Entities::encode($cust_pers->{"first_name"});
  my $last_name = HTML::Entities::encode($cust_pers->{"last_name"});
  my $enable_cc_pay = $cust_pers->{"enable_cc_pay"} ? 1 : 0;
  my $php_base_url = get_php_base_url();
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <cust_acct>
    <name>$cust_acct_name</name>
---END_BLOCK---;
  $generated_xml .= <<"---END_BLOCK---;";
  </cust_acct>
  <cust_pers>
    <first_name>$first_name</first_name>
    <last_name>$last_name</last_name>
    <enable_cc_pay>$enable_cc_pay</enable_cc_pay>
  </cust_pers>
  <php-base-url>$php_base_url</php-base-url>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "index.xslt", $cgi);
}

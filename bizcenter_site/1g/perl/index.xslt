<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>Micro Office Solutions :: Business Center</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css" />
	<link rel="icon" type="image/x-icon" href="/1g/favicon.ico" />
	<link rel="shortcut icon" type="image/x-icon" href="/1g/favicon.ico" />
      </head>
      <body>
	<p><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
	<p>Welcome to the business center, <xsl:value-of select="cust_pers/first_name"/><xsl:text> </xsl:text><xsl:value-of select="cust_pers/last_name"/>. Here are your menu options:</p>
	<ul>
	  <li>
	    Online Security:
	    <a href="/1g/auth/logout.cgi">Log Out</a>
	    &middot;
	    <a href="/1g/auth/change_password_input.html">Change Password</a>
	  </li>
	  <li>
	    Profile
	    <ul>
	      <li><a href="/1g/profile/show_account.cgi">Account Details</a></li>
	      <li><a href="/1g/profile/show_aliases.cgi">Aliases</a></li>
	      <li><a href="/1g/profile/show_persons.cgi">Persons in Account</a></li>
	      <li><a href="/1g/profile/show_proximity_cards.cgi">Proximity Cards</a></li>
	      <li><a href="/1g/profile/show_mailboxes.cgi">Mailboxes</a></li>
	      <li><a href="/1g/profile/show_bank_accounts.cgi">Bank Accounts</a></li>
	      <li><a href="/1g/profile/show_credit_cards.cgi">Credit Cards</a></li>
	      <!--
		  <li>Directory Entries</li>
	      -->
	    </ul>
	  </li>
	  <li>
	    Payment
	    <ul>
	      <li>One-Time <a href="/1g/pay/electronic_check_input.cgi">Electronic Check Payment</a></li>
	      <xsl:if test="cust_pers/enable_cc_pay = 1">
		<li>One-Time <a href="/1g/pay/credit_card_payment_input.cgi">Credit Card Payment</a></li>
	      </xsl:if>
	      <li>Paper Check <a href="/1g/pay/address.html">Payment Address</a></li>
	      <!-- <li>Set up <a href="">Recurring Payments</a></li> -->
	      <!-- <li>Payment History</li> -->
	    </ul>
	  </li>
	  <li>
	    Conference Rooms
	    <ul>
	      <li><a href="/1g/meet/show_schedule.cgi">Conference Room Schedules</a></li>
	      <li><a href="/1g/meet/my_room_reservations.cgi">My Room Reservations</a></li>
	      <li><a href="/1g/meet/show_time_remaining.cgi">Show Time Remaining</a></li>
	    </ul>
	  </li>
	  <li>
	    Directory
	    <ul>
	      <li><a href="/1g/directory/by_person.cgi">Tenant Directory (sorted by person)</a></li>
	      <li><a href="/1g/directory/by_company.cgi">Tenant Directory (sorted by company)</a></li>
	    </ul> 
	  </li>
	  <li>
	    Communications and Imaging
	    <ul>
	      <li><a href="/1g/telecom/show_phone_numbers.cgi">Phone Numbers</a></li>
	      <li>
		<a href="/1g/telecom/show_wifi_keys.cgi">WiFi Keys</a>
		&middot;
		<a href="/1g/telecom/show_ip_addresses.cgi">IP Addresses</a>
	      </li>
	      <li>
		Scan/Fax:
		<a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
		&middot;
		<a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
		&middot;
		<a href="/1g/imaging/show_my_docs.cgi">My Documents</a>
		&middot;
		<a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
		&middot;
		<a href="/1g/imaging/batch_resend_fax_input.cgi">Batch Resend Fax</a>
	      </li>
	    </ul>
	  </li>
      <li>Tasks: 
         <ul>   
            <li><a>
               <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/tasks/add</xsl:attribute>
               Add Task
            </a></li>
         
            <li><a>
               <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/tasks</xsl:attribute>
               List Tasks
            </a></li>
         </ul>
      </li>
<!--
          <li>
            Mail
            <ul>
              <li>Mail Status Service</li>
            </ul>
          </li>
          <li>
            Maintenance
            <ul>
              <li>Report a Problem</li>
              <li></li>
            </ul>
          </li>
-->
	</ul>
	<p>Have suggestions or comments? Send feedback to <a href="mailto:service@microoffice.com">service@microoffice.com</a>.</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

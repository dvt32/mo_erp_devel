function toggle_checkboxes(select_all_element, name_prefix) {
  var container_form = select_all_element.form;
  var i = 0;
  var form_element;
  for(i = 0; i < container_form.length; i++) {
    form_element = container_form.elements[i];
    if ((form_element.type == 'checkbox') &&
        (form_element != select_all_element) &&
        (form_element.name.substr(0, name_prefix.length) == name_prefix)) {
      form_element.checked = select_all_element.checked;
    }
  }
}

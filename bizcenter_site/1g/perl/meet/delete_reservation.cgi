#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use delete_row;
use military_time_to_am_pm;
use day_of_week;
use get_day_name;
use get_month_name;
use get_timestamp_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # load reservation.
  my $reservation_id = $cgi->param("reservation_id");
  if ((!defined($reservation_id)) || ($reservation_id !~ /^\d+$/)) {
    die "show_page: delete_reservation_not_found.html\n";
  }
  my $reservation = select_row($dbh, "reservation_tbl", $reservation_id);
  if (!$reservation) {
    die "show_page: delete_reservation_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $reservation->{"account_id"}) {
    die "show_page: delete_reservation_not_owner.html\n";
  }
  # check if reservation start_dt is in the past.
  my $now_dt = get_timestamp_now();
  if ($reservation->{"start_dt"} le $now_dt) {
    die "show_page: delete_reservation_past_time.html\n";
  }
  delete_row($dbh, "reservation_tbl", $reservation_id);
  # load room and floor.
  my $room = select_row($dbh, "room_tbl", $reservation->{"room_id"});
  my $floor = select_row($dbh, "floor_tbl", $room->{"floor_id"});
  # determine reservation information needed for display of page.
  my $generated_xml = "";
  $reservation->{"start_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my ($year, $padded_month, $padded_day, $padded_hour, $padded_minute) = ($1, $2, $3, $4, $5);
  my $month = int($padded_month);
  my $day = int($padded_day);
  my $hour = int($padded_hour);
  my $minute = int($padded_minute);
  my $day_of_week = day_of_week($month, $day, $year);
  my $day_name = get_day_name($day_of_week);
  my $month_name = get_month_name($month);
  my $start_time = military_time_to_am_pm(qq{$padded_hour:$padded_minute});
  $reservation->{"end_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my $end_time = military_time_to_am_pm(qq{$4:$5});
  my $floor_name = $floor->{"label"};
  my $room_name = $room->{"name"};
  my $room_label = HTML::Entities::encode($floor_name . "-" . $room_name);
  my $note = HTML::Entities::encode($reservation->{"note"} || "");
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <room-reservation>
    <day-name>$day_name</day-name>
    <day-number>$day</day-number>
    <month-name>$month_name</month-name>
    <year>$year</year>
    <start-time>$start_time</start-time>
    <end-time>$end_time</end-time>
    <room-name>$room_label</room-name>
    <note>$note</note>
  </room-reservation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_reservation_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use get_row_list;
use get_row_by_id_map;
use military_time_to_am_pm;
use day_of_week;
use get_day_name;
use get_month_name;
use minutes_to_display_time;
use generate_xml_for_room_schedule;
use display_time_to_minutes;
use get_building_id_from_floor_id;
use get_timestamp_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # check if reservation id is specified.
  my $reservation_id = $cgi->param("reservation_id");
  if ((!defined($reservation_id)) || ($reservation_id !~ /^\d+$/)) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  # load reservation.
  my $reservation = select_row($dbh, "reservation_tbl", $reservation_id);
  if (!$reservation) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $reservation->{"account_id"}) {
    die "show_page: edit_reservation_not_owner.html\n";
  }
  # check if reservation start_dt is in the past.
  my $now_dt = get_timestamp_now();
  if ($reservation->{"start_dt"} le $now_dt) {
    die "show_page: edit_reservation_past_time.html\n";
  }
  $reservation->{"start_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my ($year, $padded_month, $padded_day, $padded_hour, $padded_minute) = ($1, $2, $3, $4, $5);
  my $query_date = "$year-$padded_month-$padded_day";
  # load room attached to the reservation.
  my $room = select_row($dbh, "room_tbl", $reservation->{"room_id"});
  my $floor_id = $room->{"floor_id"};
  my $building_id = get_building_id_from_floor_id($dbh, $floor_id);
  # fetch rooms in the building of the tenant that are accessible to tenant.
  my @room_list;
  my $sql_query = "select * from room_tbl where is_active and (floor_id = ? or (is_shared_floor and floor_id in (select id from floor_tbl where building_id = ?)))";
  get_row_list($dbh, $sql_query, [[$floor_id, SQL_INTEGER], [$building_id, SQL_INTEGER]], \@room_list);
  # fetch floors in the building.
  my %floor_by_id;
  $sql_query = "select * from floor_tbl where building_id = ?";
  get_row_by_id_map($dbh, $sql_query, [[$building_id, SQL_INTEGER]], \%floor_by_id);
  # sort rooms by floor short local label, then short name.
  @room_list = sort {
    my $sort_order = 0;
    my $floor_a = $floor_by_id{$a->{"floor_id"}};
    my $floor_b = $floor_by_id{$b->{"floor_id"}};
    my $floor_name_a = $floor_a->{"short_local_label"};
    my $floor_name_b = $floor_b->{"short_local_label"};
    my $room_name_a = $a->{"short_name"};
    my $room_name_b = $b->{"short_name"};
    $sort_order = ($floor_name_a . $room_name_a) cmp ($floor_name_b . $room_name_b);
  } @room_list;
  # time slots are in 15 minute increments. time slot increment must divide evenly into 60.
  my $time_slot_increment = 15;
  my $room_schedule_xml = generate_xml_for_room_schedule
    ("database_handle" => $dbh,
     "floor_id" => $floor_id,
     "query_date" => $query_date,
     "room_list" => \@room_list,
     "time_slot_increment" => $time_slot_increment);
  # populate information needed for display of page.
  my $month = int($padded_month);
  my $day = int($padded_day);
  my $hour = int($padded_hour);
  my $minute = int($padded_minute);
  my $day_of_week = day_of_week($month, $day, $year);
  my $day_name = get_day_name($day_of_week);
  my $month_name = get_month_name($month);
  my $start_time = military_time_to_am_pm(qq{$padded_hour:$padded_minute});
  $reservation->{"end_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my $end_time = military_time_to_am_pm(qq{$4:$5});
  my $existing_start_time_in_minutes = display_time_to_minutes($start_time);
  my $existing_duration_in_minutes = display_time_to_minutes($end_time) - $existing_start_time_in_minutes;
  my $floor = $floor_by_id{$floor_id};
  my $floor_name = $floor->{"label"};
  my $room_name = $room->{"name"};
  my $room_label = HTML::Entities::encode($floor_name . "-" . $room_name);
  my $note = HTML::Entities::encode($reservation->{"note"} || "");
  # determine today's date.
  my ($today_year, $today_month_number, $today_day_number, $today_day_of_week) = (localtime(time()))[5, 4, 3, 6];
  $today_year += 1900;
  $today_month_number++;
  my $today_day_name = get_day_name($today_day_of_week);
  my $today_month_name = get_month_name($today_month_number);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <day-name>$today_day_name</day-name>
    <month-name>$today_month_name</month-name>
    <day-number>$today_day_number</day-number>
    <year>$today_year</year>
  </today>
  <existing-room-reservation>
    <day-name>$day_name</day-name>
    <day-number>$day</day-number>
    <month-name>$month_name</month-name>
    <year>$year</year>
    <start-time>$start_time</start-time>
    <end-time>$end_time</end-time>
    <room-name>$room_label</room-name>
    <note>$note</note>
  </existing-room-reservation>
  <action-list>
    <reserve-room>
      <reservation-id>$reservation_id</reservation-id>
      <date>
        <day-name>$day_name</day-name>
        <day-number>$day</day-number>
        <month-name>$month_name</month-name>
        <year>$year</year>
      </date>
      <room-name>$room_label</room-name>
      <time-select>
---END_BLOCK---;
  for(my $time_slot = 8*60; $time_slot < 24*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    if ($time_slot == $existing_start_time_in_minutes) {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value" selected="selected">$option_label</option>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value">$option_label</option>
---END_BLOCK---;
    }
  }
  for(my $time_slot = 0; $time_slot < 8*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    if ($time_slot == $existing_start_time_in_minutes) {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value" selected="selected">$option_label</option>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value">$option_label</option>
---END_BLOCK---;
    }
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </time-select>
      <duration-select>
---END_BLOCK---;
  my @duration_list =
    ([15, "15 minutes"],
     [30, "30 minutes"],
     [45, "45 minutes"],
     [60, "1 hour"],
     [90, "1.5 hours"],
     [120, "2 hours"],
     [150, "2.5 hours"],
     [180, "3 hours"],
     [210, "3.5 hours"],
     [240, "4 hours"],
     );
  for my $duration_entry (@duration_list) {
    my $duration_in_minutes = $duration_entry->[0];
    my $duration_label = $duration_entry->[1];
    my $selected_attribute = "";
    if ($duration_in_minutes == $existing_duration_in_minutes) {
      $selected_attribute = qq{ selected="selected"};
    }
    $generated_xml .= <<"---END_BLOCK---;";
        <option value="$duration_in_minutes"$selected_attribute>$duration_label</option>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </duration-select>
      <note>$note</note>
    </reserve-room>
  </action-list>
---END_BLOCK---;
  $generated_xml .= $room_schedule_xml;
  $generated_xml .= <<"---END_BLOCK---;";
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_reservation_input.xslt", $cgi);
}

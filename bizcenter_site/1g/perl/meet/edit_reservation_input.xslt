<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Room Reservation
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:call-template name="reservation-table"/>
	<p>
	  To change the date or the room, create a <a href="/1g/meet/show_schedule.cgi">new reservation</a> and then
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>delete_reservation.cgi?reservation_id=</xsl:text>
	      <xsl:value-of select="action-list/reserve-room/reservation-id"/>
	    </xsl:attribute>
	    <xsl:text>delete</xsl:text>
	  </a>
	  this existing one.
	  To see all of your reservations: <a href="/1g/meet/my_room_reservations.cgi">My Room Reservations</a>.
	</p>
	<p>
	  Please be considerate. Keep the duration to two hours or less under normal circumstances. If you are not going to use
	  a room reservation, please
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>delete_reservation.cgi?reservation_id=</xsl:text>
	      <xsl:value-of select="action-list/reserve-room/reservation-id"/>
	    </xsl:attribute>
	    <xsl:text>delete</xsl:text>
	  </a>
	  it so that others may use the room. Deleting room reservations also benefits you because it will remove the time from
	  your monthly usage, thereby reducing the chance that you run out of time for a given month.
	</p>
	<p>
	  The schedule below shows the room reservations on the day of your reservation.
	</p>
	<xsl:apply-templates select="schedule"/>
	<p>
	  Today is 
	  <xsl:value-of select="today/day-name"/>,
	  <xsl:value-of select="today/month-name"/><xsl:text> </xsl:text><xsl:value-of select="today/day-number"/>,
	  <xsl:value-of select="today/year"/>. Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="reservation-table">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="schedule_action_header">
	  Existing Room Reservation
	</td>
	<td class="schedule_action_header">
	  New Room Reservation
	</td>
      </tr>
      <tr valign="top">
	<td class="schedule_action_body">
	  <xsl:apply-templates select="existing-room-reservation"/>
	</td>
	<td class="schedule_action_body">
	  <xsl:apply-templates select="action-list"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="existing-room-reservation">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Room:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="room-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="day-name"/>,
	  <xsl:value-of select="month-name"/><xsl:text> </xsl:text><xsl:value-of select="day-number"/>, <xsl:value-of select="year"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Start Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="start-time"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  End Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="end-time"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="action-list">
    <xsl:apply-templates select="reserve-room"/>
  </xsl:template>

  <xsl:template match="reserve-room">
    <form action="/meet/edit_reservation_result.cgi" method="post">
      <fieldset class="standard_fieldset">
	<legend>
	  <xsl:value-of select="date/day-name"/>,
	  <xsl:value-of select="date/month-name"/><xsl:text> </xsl:text>
	  <xsl:value-of select="date/day-number"/>,
	  <xsl:value-of select="date/year"/>
	  (<xsl:value-of select="room-name"/>)
	</legend>
	<input>
	  <xsl:attribute name="type">hidden</xsl:attribute>
	  <xsl:attribute name="name">reservation_id</xsl:attribute>
	  <xsl:attribute name="value"><xsl:value-of select="reservation-id"/></xsl:attribute>
	</input>
	<table cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <td style="padding: 4px; ">
	      Start:
	    </td>
	    <td>
	      <xsl:apply-templates select="time-select"/>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      Duration:
	    </td>
	    <td>
	      <xsl:apply-templates select="duration-select"/>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      Note:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">note</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	    </td>
	    <td>
	      <input type="submit" name="submitButton" value="Save"/>
	    </td>
	  </tr>
	</table>
      </fieldset>
    </form>
  </xsl:template>

  <xsl:template match="time-select">
    <select name="time">
      <xsl:apply-templates/>
    </select>
  </xsl:template>

  <xsl:template match="duration-select">
    <select name="duration">
      <xsl:apply-templates/>
    </select>
  </xsl:template>

  <xsl:template match="schedule">
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
	<td class="schedule_date_header">
	  <xsl:attribute name="colspan"><xsl:value-of select="count(room-list/room) * 2 + 1"/></xsl:attribute>
	  <xsl:apply-templates select="date"/>
	</td>
      </tr>
      <xsl:apply-templates select="room-list"/>
      <xsl:apply-templates select="reservation-list"/>
    </table>
  </xsl:template>

  <xsl:template match="date">
    <xsl:value-of select="day-name"/>,
    <xsl:value-of select="month-name"/><xsl:text> </xsl:text><xsl:value-of select="day-number"/>, <xsl:value-of select="year"/>
    (<xsl:value-of select="year"/>-<xsl:value-of select="padded-month-number"/>-<xsl:value-of select="padded-day-number"/>)
  </xsl:template>

  <xsl:template match="room-list">
    <tr>
      <td class="schedule_upper_left_corner">
	Time
      </td>
      <xsl:for-each select="room">
	<td class="schedule_header_spacer_column"></td>
	<td class="schedule_room_name_header">
	  <xsl:apply-templates select="room-name"/>
	</td>
      </xsl:for-each>
    </tr>
  </xsl:template>

  <xsl:template match="room-name">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="reservation-list">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="time-block">
    <tr>
      <td class="schedule_time">
	<xsl:apply-templates select="start-time"/>
      </td>
      <xsl:for-each select="room-reservation">
	<td class="schedule_spacer_column"></td>
	<xsl:apply-templates/>
      </xsl:for-each>
    </tr>
  </xsl:template>

  <xsl:template match="start-time">
    <xsl:value-of select="display-time"/>
  </xsl:template>

  <xsl:template match="available-slot">
    <td class="schedule_reservation_available_slot">
      ---
    </td>
  </xsl:template>

  <xsl:template match="occupied-slot">
    <td>
      <xsl:attribute name="class">
	<xsl:if test="(is-start = 'true') and (is-end = 'true')">schedule_reservation_occupied_slot_start_and_end</xsl:if>
	<xsl:if test="(is-start = 'false') and (is-end = 'false')">schedule_reservation_occupied_slot_middle</xsl:if>
	<xsl:if test="(is-start = 'true') and (is-end = 'false')">schedule_reservation_occupied_slot_start</xsl:if>
	<xsl:if test="(is-start = 'false') and (is-end = 'true')">schedule_reservation_occupied_slot_end</xsl:if>
      </xsl:attribute>
      <xsl:value-of select="account-name"/>
    </td>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_cust_acct;
use military_time_to_am_pm;
use day_of_week;
use get_day_name;
use get_month_name;
use minutes_to_display_time;
use get_timestamp_now;
use select_row;
use select_row_with_query;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that the start time is valid.
  my $start_time = $cgi->param("time");
  if ((!defined($start_time)) || ($start_time eq "")) {
    die "show_page: edit_reservation_bad_start_time.html\n";
  }
  if ($start_time !~ /^\d+$/) {
    die "show_page: edit_reservation_bad_start_time.html\n";
  }
  if (($start_time < 0) || ($start_time > 24 * 60)) {
    die "show_page: edit_reservation_bad_start_time.html\n";
  }
  # verify that the duration is valid.
  my $duration = $cgi->param("duration");
  if (!$duration) {
    die "show_page: edit_reservation_bad_duration.html\n";
  }
  if ($duration !~ /^\d+$/) {
    die "show_page: edit_reservation_bad_duration.html\n";
  }
  if (($duration < 0) || ($duration > 4 * 60)) {
    die "show_page: edit_reservation_bad_duration.html\n";
  }
  # verify that the start time plus the duration do not exceed midnight (i.e. cross over into a new day).
  if (($start_time + $duration) > 24 * 60) {
    die "show_page: edit_reservation_day_boundary_error.html\n";
  }
  my $cust_acct = get_cust_acct($instance_map);
  # load reservation.
  my $reservation_id = $cgi->param("reservation_id");
  if ((!defined($reservation_id)) || ($reservation_id !~ /^\d+$/)) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  my $reservation = select_row($dbh, "reservation_tbl", $reservation_id);
  if (!$reservation) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $reservation->{"account_id"}) {
    die "show_page: edit_reservation_not_owner.html\n";
  }
  # check if reservation start_dt is in the past.
  my $now_dt = get_timestamp_now();
  if ($reservation->{"start_dt"} le $now_dt) {
    die "show_page: edit_reservation_past_time.html\n";
  }
  # load room and floor.
  my $room_id = $reservation->{"room_id"};
  my $room = select_row($dbh, "room_tbl", $room_id);
  my $floor = select_row($dbh, "floor_tbl", $room->{"floor_id"});
  # calculate start and end timestamps.
  $reservation->{"start_dt"} =~ /^(\d{4}-\d\d-\d\d)/;
  my $reservation_date = $1;
  my $start_dt = $reservation_date . " " . minutes_to_display_time($start_time) . ":00";
  my $end_dt = $reservation_date . " " . minutes_to_display_time($start_time + $duration) . ":00";
  # get the note.
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of inserting the reservation row.
  $dbh->begin_work();
  my $sql_query = "select * from reservation_tbl where id <> ? and room_id = ? and (start_dt, end_dt) overlaps (timestamp ?, timestamp ?)";
  my @sql_param_list = ([$reservation_id, SQL_INTEGER], [$room_id, SQL_INTEGER], $start_dt, $end_dt);
  # there is an overlapping reservation in the same room. roll back the transaction and display an error.
  if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    $dbh->rollback();
    die "show_page: edit_reservation_overlap_error.html\n";
  }
  # no problems, so update the reservation row.
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["start_dt", $start_dt]);
  push(@sql_column_list, ["end_dt", $end_dt]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "reservation_tbl", \@sql_column_list, $reservation_id);
  # commit the transaction.
  $dbh->commit();
  # determine reservation information needed for display of page.
  my $generated_xml = "";
  $start_dt =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my ($year, $padded_month, $padded_day, $padded_hour, $padded_minute) = ($1, $2, $3, $4, $5);
  my $month = int($padded_month);
  my $day = int($padded_day);
  my $hour = int($padded_hour);
  my $minute = int($padded_minute);
  my $day_of_week = day_of_week($month, $day, $year);
  my $day_name = get_day_name($day_of_week);
  my $month_name = get_month_name($month);
  my $start_time = military_time_to_am_pm(qq{$padded_hour:$padded_minute});
  $end_dt =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my $end_time = military_time_to_am_pm(qq{$4:$5});
  my $floor_name = $floor->{"label"};
  my $room_name = $room->{"name"};
  my $room_label = HTML::Entities::encode($floor_name . "-" . $room_name);
  $note = HTML::Entities::encode($note);
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <room-reservation>
    <day-name>$day_name</day-name>
    <day-number>$day</day-number>
    <month-name>$month_name</month-name>
    <year>$year</year>
    <start-time>$start_time</start-time>
    <end-time>$end_time</end-time>
    <room-name>$room_label</room-name>
    <note>$note</note>
  </room-reservation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_reservation_result_success.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Reservation
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="room-reservation">
    <p>
      You successfully updated the following room reservation:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Room:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="room-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="day-name"/>,
	  <xsl:value-of select="month-name"/><xsl:text> </xsl:text><xsl:value-of select="day-number"/>, <xsl:value-of select="year"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Start Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="start-time"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  End Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="end-time"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

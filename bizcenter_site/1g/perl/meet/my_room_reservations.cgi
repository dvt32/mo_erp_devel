#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_list;
use get_row_by_id_map;
use military_time_to_am_pm;
use day_of_week;
use get_day_name;
use get_month_name;
use get_building_id_from_floor_id;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # fetch reservations belonging to current cust_acct.
  my @reservation_list;
  my $sql_query = "select * from reservation_tbl where account_id = ? and start_dt > current_date order by start_dt";
  my @sql_param_list = ([$cust_acct->{"id"}, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@reservation_list);
  # fetch rooms in the building of the tenant that are accessible to tenant.
  my $floor_id = $cust_acct->{"primary_floor_id"};
  my $building_id = get_building_id_from_floor_id($dbh, $floor_id);
  my %room_by_id;
  $sql_query = "select * from room_tbl where (floor_id = ? or (is_shared_floor and floor_id in (select id from floor_tbl where building_id = ?)))";
  @sql_param_list = ([$floor_id, SQL_INTEGER], [$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%room_by_id);
  # fetch floors in the building.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where building_id = ?", [[$building_id, SQL_INTEGER]], \%floor_by_id);
  # determine reservation information needed for display of page.
  my ($today_year, $today_month_number, $today_day_number, $today_day_of_week) = (localtime(time()))[5, 4, 3, 6];
  $today_year += 1900;
  $today_month_number++;
  my $today_month_name = get_month_name($today_month_number);
  my $today_day_name = get_day_name($today_day_of_week);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <day-name>$today_day_name</day-name>
    <month-name>$today_month_name</month-name>
    <day-number>$today_day_number</day-number>
    <year>$today_year</year>
  </today>
  <reservation-list>
---END_BLOCK---;
  for my $reservation (@reservation_list) {
    my $reservation_id = $reservation->{"id"};
    $reservation->{"start_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
    my ($year, $padded_month, $padded_day, $padded_hour, $padded_minute) = ($1, $2, $3, $4, $5);
    my $month = int($padded_month);
    my $day = int($padded_day);
    my $hour = int($padded_hour);
    my $minute = int($padded_minute);
    my $day_of_week = day_of_week($month, $day, $year);
    my $day_name = get_day_name($day_of_week);
    my $month_name = get_month_name($month);
    my $long_date = qq{$day_name, $month_name $day, $year};
    my $short_date = "$year-$padded_month-$padded_day";
    my $start_time = military_time_to_am_pm(qq{$padded_hour:$padded_minute});
    $reservation->{"end_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
    my $end_time = military_time_to_am_pm(qq{$4:$5});
    my $room = $room_by_id{$reservation->{"room_id"}};
    my $floor = $floor_by_id{$room->{"floor_id"}};
    my $floor_name = $floor->{"label"};
    my $room_name = $room->{"name"};
    my $room_label = HTML::Entities::encode($floor_name . "-" . $room_name);
    my $note = HTML::Entities::encode($reservation->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <reservation>
      <reservation-id>$reservation_id</reservation-id>
      <date>
        <long-date>$long_date</long-date>
        <short-date>$short_date</short-date>
      </date>
      <start-time>$start_time</start-time>
      <end-time>$end_time</end-time>
      <room-name>$room_label</room-name>
      <note>$note</note>
    </reservation>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </reservation-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "my_room_reservations.xslt", $cgi);
}

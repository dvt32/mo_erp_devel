<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: My Room Reservations
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="reservation-list"/>
	<p>
	  The above table shows only room reservations from today forward.
	  Today is
	  <xsl:value-of select="/page/today/day-name"/>,
	  <xsl:value-of select="/page/today/month-name"/><xsl:text> </xsl:text><xsl:value-of select="/page/today/day-number"/>,
	  <xsl:value-of select="/page/today/year"/>.
	</p>
	<p>
	  You may edit your room reservation by clicking on the start time. If you change your mind and no longer need a time
	  slot, please be courteous and delete the reservation by clicking on the start time to edit the room reservation, then
	  clicking on the delete link. Deleting room reservations allows others to book time and also benefits you by removing
	  the time slot from your monthly usage, thereby reducing the chance that you might run out of alloted time in a given
	  month.
	</p>
	<p>
	  Show <a href="/1g/meet/show_schedule.cgi">complete schedule for today</a> or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="reservation-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="12" class="standard_table_title">
	  My Room Reservations
	  (<xsl:value-of select="count(reservation)"/> total)
	</td>
      </tr>
      <tr>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Long Date
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Short Date
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Start Time
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  End Time
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Room
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Note
	</td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(reservation) > 0">
	  <xsl:apply-templates/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="12" class="standard_table_body">
	      <div style="text-align: center">You have no room reservations.</div>
	    </td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="reservation">
    <tr valign="top">
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="date/long-date"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="date/short-date"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>edit_reservation_input.cgi?reservation_id=</xsl:text>
	    <xsl:value-of select="reservation-id"/>
	  </xsl:attribute>
	  <span class="time"><xsl:value-of select="start-time"/></span>
	</a>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<span class="time"><xsl:value-of select="end-time"/></span>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="room-name"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="note"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

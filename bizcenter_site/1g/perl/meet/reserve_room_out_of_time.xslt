<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Reserve a Room :: Out of Time Error
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="error"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="error">
    <p>You have used up your allocated time for the month in which you are trying to book. Your time allocation will reset in the new month, but meanwhile you have several options: (1) pay for additional time or (2) delete reservations that you do not need. This policy has been implemented in order to make conference room time available in a fair fashion for tenants according to their respective sizes on the floor.</p>
    <p>
      You have used <xsl:value-of select="time_used"/> hours. You have up to <xsl:value-of select="time_allocated"/> hours per month.
    </p>
    <p>Please return to the <a href="/1g/index.cgi">main menu</a> and try again. If you choose to hit the back button in your browser, note that you should refresh the schedule page because someone else may have changed the schedule in the meanwhile.</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

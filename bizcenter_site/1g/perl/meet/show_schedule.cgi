#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use lead_pad;
use minutes_to_display_time;
use day_of_week;
use generate_xml_for_room_schedule;
use get_month_name;
use get_day_name;
use generate_xml_for_calendar_month;
use get_building_id_from_floor_id;
use get_row_list;
use get_row_by_id_map;
use get_cust_acct;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  # determine today's date.
  my ($today_year, $today_month_number, $today_day_number, $today_day_of_week) = (localtime(time()))[5, 4, 3, 6];
  $today_year += 1900;
  $today_month_number++;
  my $today_day_name = get_day_name($today_day_of_week);
  my $today_month_name = get_month_name($today_month_number);
  # check if date was specified as a parameter.
  my ($schedule_year, $schedule_month_number, $schedule_day_number, $schedule_day_of_week);
  my ($schedule_padded_month_number, $schedule_padded_day_number, $schedule_day_name, $schedule_month_name);
  my $query_date = $cgi->param("date");
  if ($query_date) {
    if ($query_date =~ /^(\d{4})-(\d\d)-(\d\d)$/) {
      ($schedule_year, $schedule_month_number, $schedule_day_number) = (int($1), int($2), int($3));
      $schedule_day_of_week = day_of_week($schedule_month_number, $schedule_day_number, $schedule_year);
      $schedule_padded_month_number = lead_pad($schedule_month_number, 2);
      $schedule_padded_day_number = lead_pad($schedule_day_number, 2);
      $schedule_day_name = get_day_name($schedule_day_of_week);
      $schedule_month_name = get_month_name($schedule_month_number);
    } else {
      die "show_page: show_schedule_bad_date.html\n";
    }
  } else {
    ($schedule_year, $schedule_month_number, $schedule_day_number, $schedule_day_of_week) =
      ($today_year, $today_month_number, $today_day_number, $today_day_of_week);
    $schedule_padded_month_number = lead_pad($schedule_month_number, 2);
    $schedule_padded_day_number = lead_pad($schedule_day_number, 2);
    $schedule_day_name = get_day_name($schedule_day_of_week);
    $schedule_month_name = get_month_name($schedule_month_number);
    $query_date = "$schedule_year-$schedule_padded_month_number-$schedule_padded_day_number";
  }
  my $cust_acct = get_cust_acct($instance_map);
  my $floor_id = $cust_acct->{"primary_floor_id"};
  my $building_id = get_building_id_from_floor_id($dbh, $floor_id);
  # fetch rooms in the building of the tenant that are accessible to tenant.
  my @room_list;
  my ($sql_query, @sql_param_list);
  my $service_id_therapy = 5;
  if ($cust_acct->{"service_id"} == $service_id_therapy) {
    $sql_query = "select * from room_tbl where is_active and (floor_id = ? or (is_shared_floor and floor_id in (select id from floor_tbl where building_id = ?)))";
    @sql_param_list = ([$floor_id, SQL_INTEGER], [$building_id, SQL_INTEGER]);
  } else {
    $sql_query = "select * from room_tbl where is_active and (floor_id = ? or (is_shared_floor and floor_id in (select id from floor_tbl where building_id = ?))) and room_type = ?";
    @sql_param_list = ([$floor_id, SQL_INTEGER], [$building_id, SQL_INTEGER], "conference");
  }
  get_row_list($dbh, $sql_query, \@sql_param_list, \@room_list);
  # fetch floors in the building.
  my %floor_by_id;
  $sql_query = "select * from floor_tbl where building_id = ?";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%floor_by_id);
  # sort rooms by floor short local label, then short name.
  @room_list = sort {
    my $sort_order = 0;
    my $floor_a = $floor_by_id{$a->{"floor_id"}};
    my $floor_b = $floor_by_id{$b->{"floor_id"}};
    my $floor_name_a = $floor_a->{"short_local_label"};
    my $floor_name_b = $floor_b->{"short_local_label"};
    my $room_name_a = $a->{"short_name"};
    my $room_name_b = $b->{"short_name"};
    $sort_order = ($floor_name_a . $room_name_a) cmp ($floor_name_b . $room_name_b);
  } @room_list;
  # time slots are in 15 minute increments. time slot increment must divide evenly into 60.
  my $time_slot_increment = 15;
  my $room_schedule_xml = generate_xml_for_room_schedule
    ("database_handle" => $dbh,
     "floor_id" => $floor_id,
     "query_date" => $query_date,
     "room_list" => \@room_list,
     "time_slot_increment" => $time_slot_increment);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <day-name>$today_day_name</day-name>
    <month-name>$today_month_name</month-name>
    <day-number>$today_day_number</day-number>
    <year>$today_year</year>
  </today>
---END_BLOCK---;
  $generated_xml .= $room_schedule_xml;
  $generated_xml .= <<"---END_BLOCK---;";
  <action-list>
    <reserve-room>
      <date>
        <year>$schedule_year</year>
        <month-name>$schedule_month_name</month-name>
        <month-number>$schedule_month_number</month-number>
        <padded-month-number>$schedule_padded_month_number</padded-month-number>
        <day-number>$schedule_day_number</day-number>
        <padded-day-number>$schedule_padded_day_number</padded-day-number>
        <day-name>$schedule_day_name</day-name>
      </date>
      <room-select>
        <room-list>
---END_BLOCK---;
  for my $room (@room_list) {
    my $room_id = $room->{"id"};
    my $floor = $floor_by_id{$room->{"floor_id"}};
    my $floor_name = $floor->{"label"};
    my $room_name = $room->{"name"};
    my $room_label = HTML::Entities::encode($floor_name . "-" . $room_name);
    $generated_xml .= <<"---END_BLOCK---;";
          <room>
            <room-id>$room_id</room-id>
            <room-name>$room_label</room-name>
          </room>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
        </room-list>
      </room-select>
      <time-select>
---END_BLOCK---;
  for(my $time_slot = 8*60; $time_slot < 24*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    # 9 AM should be selected by default.
    if ($time_slot == 9*60) {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value" selected="selected">$option_label</option>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value">$option_label</option>
---END_BLOCK---;
    }
  }
  for(my $time_slot = 0; $time_slot < 8*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value">$option_label</option>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </time-select>
      <duration-select>
        <option value="15">15 minutes</option>
        <option value="30">30 minutes</option>
        <option value="45">45 minutes</option>
        <option value="60" selected="selected">1 hour</option>
        <option value="90">1.5 hours</option>
        <option value="120">2 hours</option>
        <option value="150">2.5 hours</option>
        <option value="180">3 hours</option>
        <option value="210">3.5 hours</option>
        <option value="240">4 hours</option>
      </duration-select>
    </reserve-room>
    <switch-day>
      <calendar-month-list>
---END_BLOCK---;
  # generate calendar for the current month and two following months.
  for(my $i = 0; $i < 3; $i++) {
    my $year = $today_year;
    my $month_number = $today_month_number + $i;
    if ($month_number > 12) {
      $month_number -= 12;
      $year++;
    }
    $generated_xml .= generate_xml_for_calendar_month($year, $month_number);
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </calendar-month-list>
    </switch-day>
  </action-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_schedule.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_date_now;
use lead_pad;
use get_row_list;
use timestamp_to_minutes;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $SERVICE_ID_VIRTUAL = 4;
  my $cust_acct_id = $cust_acct->{"id"};
  if ((!$cust_acct) || (!$cust_acct->{"is_active"})) {
    die "show_page: show_time_remaining_bad_account.html\n";
  }
  my ($current_year, $current_month, $current_day) = get_date_now();
  my ($year_upper_bound, $month_upper_bound) = ($current_year, $current_month + 1);
  if ($month_upper_bound > 12) {
    $year_upper_bound++;
    $month_upper_bound = 1;
  }
  $current_month = lead_pad($current_month, 2);
  $month_upper_bound = lead_pad($month_upper_bound, 2);
  my $sql_query = "select * from reservation_tbl where account_id = ? and start_dt >= timestamp ? and start_dt < timestamp ?";
  my @sql_param_list = ([$cust_acct_id, SQL_INTEGER], "$current_year-$current_month-01 00:00:00", "$year_upper_bound-$month_upper_bound-01 00:00:00");
  my @reservation_list;
  get_row_list($dbh, $sql_query, \@sql_param_list, \@reservation_list);
  my $time_used = 0;
  for my $reservation (@reservation_list) {
    $time_used += timestamp_to_minutes($reservation->{"end_dt"}) - timestamp_to_minutes($reservation->{"start_dt"});
  }
  my $time_allocated = $cust_acct->{"room_minutes"};
  my $time_remaining = int($time_allocated - $time_used);
  my $is_virtual = ($cust_acct->{"service_id"} == $SERVICE_ID_VIRTUAL) ? 1 : 0;
  # populate information needed for display of page.
  my $month_name = HTML::Entities::encode(get_month_name($current_month));
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <conference_room_time>
    <current_year>$current_year</current_year>
    <month_name>$month_name</month_name>
    <time_used>$time_used</time_used>
    <time_allocated>$time_allocated</time_allocated>
    <time_remaining>$time_remaining</time_remaining>
    <is_virtual>$is_virtual</is_virtual>
  </conference_room_time>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_time_remaining.xslt", $cgi);
}

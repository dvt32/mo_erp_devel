<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Show Conference Room Time Remaining
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="conference_room_time">
    <p>
      Your conference room time for <xsl:value-of select="month_name"/>
      <xsl:text> </xsl:text><xsl:value-of select="current_year"/> is shown below. Time is displayed in minutes.
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Time Used:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="time_used"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Time Allocated:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="time_allocated"/>
	  <xsl:if test="is_virtual = 1">
	    (you have 4 hours [240 minutes] of base time with the virtual plan; extra hours cost money)
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Time Remaining:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="time_remaining &gt; 0">
	      <xsl:value-of select="time_remaining"/>
	    </xsl:when>
	    <xsl:otherwise>
	      None
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

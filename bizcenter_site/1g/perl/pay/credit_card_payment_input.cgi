#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_by_id_map;
use get_row_list;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  if (!$cust_pers->{"enable_cc_pay"}) {
    die "show_page: credit_card_payment_not_authorized.html\n";
  }
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # fetch credit cards belonging to this account.
  my (%credit_card_by_id, @credit_card_id_list);
  my $sql_query = "select * from credit_card_tbl where account_id = ? and person_id = ? order by card_number";
  my @sql_param_list = ([$cust_acct_id, SQL_INTEGER], [$cust_pers->{"id"}, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%credit_card_by_id, \@credit_card_id_list);
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my ($today_year, $today_month_number, $today_day_number) = (localtime(time()))[5, 4, 3];
  $today_year += 1900;
  $today_month_number++;
  $today_month_number = lead_pad($today_month_number, 2);
  $today_day_number = lead_pad($today_day_number, 2);
  my $next_year = $today_year + 1;
  my @month_name_list = (qw(January February March April May June July August September October November December));
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <cust-acct-id>$cust_acct_id</cust-acct-id>
  <account-name>$account_name</account-name>
  <credit-card-payment>
    <credit-card-list>
---END_BLOCK---;
  for my $credit_card_id (@credit_card_id_list) {
    my $credit_card = $credit_card_by_id{$credit_card_id};
    my $card_nickname = HTML::Entities::encode($credit_card->{"card_nickname"} || "");
    my $card_number = HTML::Entities::encode($credit_card->{"card_number"});
    my $card_expiration_month = HTML::Entities::encode($credit_card->{"card_expiration_month"});
    my $card_expiration_year = HTML::Entities::encode($credit_card->{"card_expiration_year"});
    my $card_security_code = HTML::Entities::encode($credit_card->{"card_security_code"} || "");
    my $billing_name = HTML::Entities::encode($credit_card->{"billing_name"});
    my $billing_company = HTML::Entities::encode($credit_card->{"billing_company"} || "");
    my $billing_address_1 = HTML::Entities::encode($credit_card->{"billing_address_1"});
    my $billing_address_2 = HTML::Entities::encode($credit_card->{"billing_address_2"} || "");
    my $billing_city = HTML::Entities::encode($credit_card->{"billing_city"});
    my $billing_state = HTML::Entities::encode($credit_card->{"billing_state"});
    my $billing_zip_code = HTML::Entities::encode($credit_card->{"billing_zip_code"});
    my $billing_country = HTML::Entities::encode($credit_card->{"billing_country"});
    my $billing_address_num = HTML::Entities::encode($credit_card->{"billing_address_num"} || "");
    my $billing_phone_number = HTML::Entities::encode($credit_card->{"billing_phone_number"} || "");
    my $billing_fax_number = HTML::Entities::encode($credit_card->{"billing_fax_number"} || "");
    my $billing_email_address = HTML::Entities::encode($credit_card->{"billing_email_address"} || "");
    my $note = HTML::Entities::encode($credit_card->{"note"} || "");
    my $is_primary = $credit_card->{"is_primary"} ? 1 : 0;
    # generate derived values.
    my $billing_state_other = "";
    if (!grep {$_->{"name"} eq $billing_state} @state_list) {
      $billing_state_other = $billing_state;
      $billing_state = "";
    }
    $generated_xml .= <<"---END_BLOCK---;";
      <credit-card>
        <id>$credit_card_id</id>
        <card_nickname>$card_nickname</card_nickname>
        <card_number>$card_number</card_number>
        <card_expiration_month>$card_expiration_month</card_expiration_month>
        <card_expiration_year>$card_expiration_year</card_expiration_year>
        <card_security_code>$card_security_code</card_security_code>
        <billing_name>$billing_name</billing_name>
        <billing_company>$billing_company</billing_company>
        <billing_address_1>$billing_address_1</billing_address_1>
        <billing_address_2>$billing_address_2</billing_address_2>
        <billing_city>$billing_city</billing_city>
        <billing_state>$billing_state</billing_state>
        <billing_state_other>$billing_state_other</billing_state_other>
        <billing_zip_code>$billing_zip_code</billing_zip_code>
        <billing_country>$billing_country</billing_country>
        <billing_address_num>$billing_address_num</billing_address_num>
        <billing_phone_number>$billing_phone_number</billing_phone_number>
        <billing_fax_number>$billing_fax_number</billing_fax_number>
        <billing_email_address>$billing_email_address</billing_email_address>
        <note>$note</note>
        <is_primary>$is_primary</is_primary>
      </credit-card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </credit-card-list>
  </credit-card-payment>
  <current-date>
    <month>$today_month_number</month>
    <day>$today_day_number</day>
    <year>$today_year</year>
  </current-date>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $number = lead_pad($i, 2);
    my $name = $month_name_list[$i - 1];
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$number</number>
      <name>$name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    my $day = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$day</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <year-list>
    <year>$today_year</year>
    <year>$next_year</year>
  </year-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    my $abbreviation = HTML::Entities::encode($state->{"abbreviation"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>
      <name>$name</name>
      <abbreviation>$abbreviation</abbreviation>
    </state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>
      <name>$name</name>
    </country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "credit_card_payment_input.xslt", $cgi);
}

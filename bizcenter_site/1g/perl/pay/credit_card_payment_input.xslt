<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Credit Card Payment
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<style>
	  span.field_name {
            font-weight: bold;
            text-decoration: underline;
          }
	</style>
	<table cellspacing="0" cellpadding="4px" border="0">
	  <tr>
	    <td class="map_table_key">
	      Customer Account ID:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="cust-acct-id"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Customer Name:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="account-name"/>
	    </td>
	  </tr>
	</table>
	<br/>
	<xsl:apply-templates select="credit-card-payment"/>
	<p>
	  Explanation of Fields:
	</p>
	<ul>
	  <li>
	    <span class="field_name">Security Code</span>: if you are using Visa, MasterCard, or Discover, the security code is a 3-digit number printed on
	    the back of your card. It appears after and to the right of your card number. If you are using American Express, the security code is a 4-digit
	    number printed on the front of your card. It appears after and to the right of your card number. Here is a <a href="/1g/img/card_security_code.gif"
	    alt="Card Security Code">picture example of a Visa, MasterCard, and Discover card</a>. Here is a <a href="/1g/img/amex_security_code.gif" alt="AMEX
	    Security Code">picture example of an American Express card</a>.
	  </li>
	  <li>
	    <span class="field_name">Billing Name</span> must match the name on your credit card bill.
	  </li>
	  <li>
	    <span class="field_name">Billing Company</span> is optional and should be the name of your company, if one is listed on your credit card.
	  </li>
	  <li>
	    <span class="field_name">Billing Address Number</span> is the first (numeric) part of the street address and is an added verification for credit
	    card processing. If your street address were 123 Main Street, the Billing Address Number would be 123.
	  </li>
	  <li>
	    <span class="field_name">Amount of Payment</span> is in
	    dollars and cents (xxx.xx). You should omit the leading
	    dollar sign. Note that we will add a 3% - 4% convenience fee.
	  </li>
	  <li>
	    <span class="field_name">Date of Payment</span> is the date that you would like the credit card charge to
	    occur. Your credit card will not be charged before this date. Should you change your mind one business day before
	    the Date of Payment you specify, email <a href="mailto:service@microoffice.com">service@microoffice.com</a> to
	    cancel your payment or modify your payment date. If you choose today, the credit card will be charged immediately.
	  </li>
	  <li>
	    <span class="field_name">Invoice Number(s)</span> is to
	    tell us which invoices you are paying using this
	    credit card payment. This is optional, but strongly
	    recommended.
	  </li>
	  <li>
	    <span class="field_name">Descriptive Note</span> is for a
	    quick note to us about the payment (e.g. special handling
	    instructions or odd situations). This is optional.
	  </li>
	</ul>
	<p>
	  Additional Notes:
	</p>
	<ul>
	  <li>The transaction will appear on your credit card statement as a charge from "MICRO OFFICE MGMT".</li>
	  <li>
	    This electronic transaction is protected by SSL, evidenced
	    by the lock in the lower right hand corner of your browser (
	    <img src="/1g/img/ie_lock.png" alt="Internet Explorer lock"/>
	    in Microsoft Internet Explorer or
	    <img src="/1g/img/firefox_lock.png" alt="Mozilla Firefox lock"/>
	    in Mozilla Firefox).  SSL significantly reduces the
	    chances of a third party intercepting your sensitive
	    financial information.
	  </li>
	</ul>
	<p>
	  Click the "Next Step &gt;" button above to proceed to the next step in the payment process, or if you want to cancel without making a payment at
	  this time, you may return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit-card-payment">
    <script>
      var credit_card_list = new Array();
      var credit_card_index = 0;
      <xsl:for-each select="credit-card-list/credit-card">
        credit_card_list[credit_card_index] = new Array(18);
        credit_card_list[credit_card_index][0] = <xsl:value-of select="id"/>;
        credit_card_list[credit_card_index][1] = "<xsl:value-of select="card_number"/>";
        credit_card_list[credit_card_index][2] = "<xsl:value-of select="card_expiration_month"/>";
        credit_card_list[credit_card_index][3] = "<xsl:value-of select="card_expiration_year"/>";
        credit_card_list[credit_card_index][4] = "<xsl:value-of select="card_security_code"/>";
        credit_card_list[credit_card_index][5] = "<xsl:value-of select="billing_name"/>";
        credit_card_list[credit_card_index][6] = "<xsl:value-of select="billing_company"/>";
        credit_card_list[credit_card_index][7] = "<xsl:value-of select="billing_address_1"/>";
        credit_card_list[credit_card_index][8] = "<xsl:value-of select="billing_address_2"/>";
        credit_card_list[credit_card_index][9] = "<xsl:value-of select="billing_city"/>";
        credit_card_list[credit_card_index][10] = "<xsl:value-of select="billing_state"/>";
        credit_card_list[credit_card_index][11] = "<xsl:value-of select="billing_state_other"/>";
        credit_card_list[credit_card_index][12] = "<xsl:value-of select="billing_zip_code"/>";
        credit_card_list[credit_card_index][13] = "<xsl:value-of select="billing_country"/>";
        credit_card_list[credit_card_index][14] = "<xsl:value-of select="billing_address_num"/>";
        credit_card_list[credit_card_index][15] = "<xsl:value-of select="billing_phone_number"/>";
        credit_card_list[credit_card_index][16] = "<xsl:value-of select="billing_fax_number"/>";
        credit_card_list[credit_card_index][17] = "<xsl:value-of select="billing_email_address"/>";
        credit_card_index++;
      </xsl:for-each>
      <xsl:text disable-output-escaping="yes">
        function select_credit_card(select_object) {
          var select_option_list = select_object.options;
          var credit_card_id = select_option_list[select_object.selectedIndex].value;
          var this_form = select_object.form;
          if (credit_card_id == "") {
            this_form.elements["card_number"].value = "";
            this_form.elements["card_expiration_month"].selectedIndex = 0;
            this_form.elements["card_expiration_year"].value = "";
            this_form.elements["card_security_code"].value = "";
            this_form.elements["billing_name"].value = "";
            this_form.elements["billing_company"].value = "";
            this_form.elements["billing_address_1"].value = "";
            this_form.elements["billing_address_2"].value = "";
            this_form.elements["billing_city"].value = "";
            this_form.elements["billing_state"].selectedIndex = 0;
            this_form.elements["billing_state_other"].value = "";
            this_form.elements["billing_zip_code"].value = "";
            var country_select_object = this_form.elements["billing_country"];
            var country_option_list = country_select_object.options;
            for(var j = 0; j &lt; country_option_list.length; j++) {
              if (country_option_list[j].value == 'United States') {
                country_select_object.selectedIndex = j;
              }
            }
            this_form.elements["billing_address_num"].value = "";
            this_form.elements["billing_phone_number"].value = "";
            this_form.elements["billing_fax_number"].value = "";
            this_form.elements["billing_email_address"].value = "";
            return;
          }
          for(var i = 0; i &lt; credit_card_list.length; i++) {
            if (credit_card_id == credit_card_list[i][0]) {
              this_form.elements["card_number"].value = credit_card_list[i][1];
              var expiration_month_select_object = this_form.elements["card_expiration_month"];
              var expiration_month_option_list = expiration_month_select_object.options;
              for(var j = 0; j &lt; expiration_month_option_list.length; j++) {
                if (expiration_month_option_list[j].value == credit_card_list[i][2]) {
                  expiration_month_select_object.selectedIndex = j;
                }
              }
              this_form.elements["card_expiration_year"].value = credit_card_list[i][3];
              this_form.elements["card_security_code"].value = credit_card_list[i][4];
              this_form.elements["billing_name"].value = credit_card_list[i][5];
              this_form.elements["billing_company"].value = credit_card_list[i][6];
              this_form.elements["billing_address_1"].value = credit_card_list[i][7];
              this_form.elements["billing_address_2"].value = credit_card_list[i][8];
              this_form.elements["billing_city"].value = credit_card_list[i][9];
              this_form.elements["billing_state"].selectedIndex = 0;
              var state_select_object = this_form.elements["billing_state"];
              var state_option_list = state_select_object.options;
              for(var j = 0; j &lt; state_option_list.length; j++) {
                if (state_option_list[j].value == credit_card_list[i][10]) {
                  state_select_object.selectedIndex = j;
                }
              }
              this_form.elements["billing_state_other"].value = credit_card_list[i][11];
              this_form.elements["billing_zip_code"].value = credit_card_list[i][12];
              var country_select_object = this_form.elements["billing_country"];
              var country_option_list = country_select_object.options;
              for(var j = 0; j &lt; country_option_list.length; j++) {
                if (country_option_list[j].value == credit_card_list[i][13]) {
                  country_select_object.selectedIndex = j;
                }
              }
              this_form.elements["billing_address_num"].value = credit_card_list[i][14];
              this_form.elements["billing_phone_number"].value = credit_card_list[i][15];
              this_form.elements["billing_fax_number"].value = credit_card_list[i][16];
              this_form.elements["billing_email_address"].value = credit_card_list[i][17];
            }
          }
        }
      </xsl:text>
    </script>
    <table>
      <tr><td>
	<div align="center" style="color: white; background-color: black;">
	  Make a One-Time Credit Card Payment
	</div>
	<form action="/pay/credit_card_payment_step2.cgi" method="post" name="credit_card_payment_form">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Credit Card Details
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Credit Card Selector:
		</td>
		<td>
                  <select name="credit_card_id" onChange="select_credit_card(this);">
		    <option value="">[---]</option>
		    <xsl:for-each select="credit-card-list/credit-card">
		      <option>
			<xsl:if test="is_primary = 1">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:choose>
			  <xsl:when test="card_nickname != ''">
			    <xsl:value-of select="card_nickname"/>
			  </xsl:when>
			  <xsl:otherwise>
			    <xsl:value-of select="card_number"/>
			  </xsl:otherwise>
			</xsl:choose>
		      </option>
		    </xsl:for-each>
		  </select>
		  (<a href="/1g/profile/show_credit_cards.cgi">add/edit credit cards</a>)
		</td>
	      </tr>
	      <tr>
		<td></td>
		<td>-or-</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Card Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">card_number</xsl:attribute>
		    <xsl:attribute name="size">18</xsl:attribute>
		    <xsl:attribute name="maxlength">16</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Expiration Date:
		</td>
		<td>
                  <select name="card_expiration_month">
		    <xsl:for-each select="/page/month-list/month">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			(<xsl:value-of select="number"/>) <xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <xsl:text> </xsl:text>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">card_expiration_year</xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		  </input>
		  (4 digit year)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">card_security_code</xsl:attribute>
		    <xsl:attribute name="maxlength">4</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_name</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Company:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_company</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Address 1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_1</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Address 2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_2</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing City:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_city</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing State:
		</td>
		<td>
                  <select name="billing_state">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/state-list/state">
		      <option>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  or Province/Territory:
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_state_other</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Zip Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_zip_code</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Country:
		</td>
		<td>
                  <select name="billing_country">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/country-list/country">
		      <option>
			<xsl:if test="/page/credit-card/billing_country = 'United States'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Address Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_num</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_phone_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Fax Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_fax_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Email Address:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_email_address</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	  <fieldset class="standard_fieldset">
	    <legend>
	      Payment Details
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Amount of Payment:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">amount</xsl:attribute>
		    <xsl:attribute name="size">9</xsl:attribute>
		    <xsl:attribute name="maxlength">9</xsl:attribute>
		  </input>
		  <xsl:text> (convenience fee will be added)</xsl:text>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Date of Payment:
		</td>
		<td>
                  <select name="charge_month">
		    <xsl:for-each select="/page/month-list/month">
		      <option>
			<xsl:if test="number = /page/current-date/month">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
                  <select name="charge_day">
		    <xsl:for-each select="/page/day-list/day">
		      <option>
			<xsl:if test=". = /page/current-date/day">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
                  <select name="charge_year">
		    <xsl:for-each select="/page/year-list/year">
		      <option>
			<xsl:if test=". = /page/current-date/year">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Invoice Number(s):
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">invoice_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Descriptive Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		  </input>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	  <div align="center">
	    <input type="submit" name="submitButton" value="Next Step &gt;"/>
	  </div>
	</form>
      </td></tr>
    </table>
    <script>
      select_credit_card(document.credit_card_payment_form.elements["credit_card_id"]);
    </script>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use POSIX;
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use lead_pad;
use is_valid_credit_card;
use days_in_month;
use strip_outside_whitespace;
use credit_card_type;
use charge_card;
use insert_row;
use send_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  if (!$cust_pers->{"enable_cc_pay"}) {
    die "show_page: credit_card_payment_not_authorized.html\n";
  }
  # get CGI form input.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $card_number = strip_outside_whitespace($cgi->param("card_number") || "");
  my $card_expiration_month = $cgi->param("card_expiration_month") || "";
  my $card_expiration_year = $cgi->param("card_expiration_year") || "";
  my $card_security_code = strip_outside_whitespace($cgi->param("card_security_code") || "");
  my $billing_name = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_name")) || "");
  my $billing_company = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_company")) || "");
  my $billing_address_1 = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_address_1")) || "");
  my $billing_address_2 = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_address_2")) || "");
  my $billing_city = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_city")) || "");
  my $billing_state = Encode::decode($cgi_input_charset, $cgi->param("billing_state")) || "";
  my $billing_zip_code = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_zip_code")) || "");
  my $billing_country = Encode::decode($cgi_input_charset, $cgi->param("billing_country")) || "";
  my $billing_address_num = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_address_num")) || "");
  my $billing_phone_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_phone_number")) || "");
  my $billing_fax_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_fax_number")) || "");
  my $billing_email_address = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_email_address")) || "");
  my $amount = strip_outside_whitespace($cgi->param("amount") || "");
  my $charge_month = $cgi->param("charge_month") || "";
  my $charge_day = $cgi->param("charge_day") || "";
  my $charge_year = $cgi->param("charge_year") || "";
  my $invoice_number = strip_outside_whitespace($cgi->param("invoice_number") || "");
  my $note = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("note")) || "");
  # validate card number.
  if (!is_valid_credit_card($card_number)) {
    die "show_page: credit_card_payment_bad_card_number.html\n";
  }
  # verify that expiration month is valid.
  if (($card_expiration_month !~ /^\d+$/) || ($card_expiration_month < 1) || ($card_expiration_month > 12)) {
    die "show_page: credit_card_payment_bad_expiration_month.html\n";
  } else {
    $card_expiration_month = lead_pad(int($card_expiration_month), 2);
  }
  # verify that expiration year is valid.
  if ($card_expiration_year !~ /^\d{4}$/) {
    die "show_page: credit_card_payment_bad_expiration_year.html\n";
  }
  # verify that security code is either blank or 3 or 4 digits.
  if (($card_security_code ne "") && ($card_security_code !~ /^\d{3,4}$/)) {
    die "show_page: credit_card_payment_bad_security_code.html\n";
  }
  # verify that billing name exists.
  if (!$billing_name) {
    die "show_page: credit_card_payment_missing_billing_name.html\n";
  }
  # verify that address 1 is not blank.
  if (!$billing_address_1) {
    die "show_page: credit_card_payment_missing_address.html\n";
  }
  # verify that city is not blank.
  if (!$billing_city) {
    die "show_page: credit_card_payment_missing_city.html\n";
  }
  # verify that state is not blank.
  if (!$billing_state) {
    die "show_page: credit_card_payment_missing_state.html\n";
  }
  # verify that zip code is not blank.
  if (!$billing_zip_code) {
    die "show_page: credit_card_payment_missing_zip_code.html\n";
  }
  # verify that country is not blank.
  if (!$billing_country) {
    die "show_page: credit_card_payment_missing_country.html\n";
  }
  # verify that billing address number is valid.
  if (($billing_address_num) && ($billing_address_num !~ /^\d+$/)) {
    die "show_page: credit_card_payment_bad_billing_address_num.html\n";
  }
  # verify that amount of payment is proper format.
  if ($amount !~ /^(\d+|\d+\.\d\d)$/) {
    die "show_page: credit_card_payment_bad_amount.html\n";
  }
  # determine today's date.
  my ($today_year, $today_month, $today_day) = (localtime(time()))[5, 4, 3];
  $today_year += 1900;
  $today_month++;
  $today_month = lead_pad($today_month, 2);
  $today_day = lead_pad($today_day, 2);
  # verify that charge month is valid.
  if (($charge_month !~ /^(\d\d)$/) || (int($charge_month) < 1) || (int($charge_month) > 12)) {
    die "show_page: credit_card_payment_bad_charge_month.html\n";
  }
  # verify that charge year is valid.
  if (($charge_year !~ /^(\d{4})$/) || ($charge_year < $today_year) || ($charge_year > ($today_year + 1))) {
    die "show_page: credit_card_payment_bad_charge_year.html\n";
  }
  # verify that charge day is valid.
  if (($charge_day !~ /^(\d\d)$/) || (int($charge_day) < 1) || (int($charge_day) > days_in_month($charge_month, $charge_year))) {
    die "show_page: credit_card_payment_bad_charge_day.html\n";
  }
  # calculate convenience fee and total amount of charge.
  my $convenience_fee;
  my $card_type = credit_card_type($card_number);
  if ($card_type eq "amex") {
    $convenience_fee = POSIX::ceil($amount * 3.5)/100;
  } elsif (($card_type eq "visa") || ($card_type eq "mastercard")) {
    $convenience_fee = POSIX::ceil($amount * 3)/100;
  } elsif ($card_type eq "discover") {
    $convenience_fee = POSIX::ceil($amount * 3)/100;
  } else {
    $convenience_fee = POSIX::ceil($amount * 3.5)/100;
  }
  if ($convenience_fee !~ /\./) {
    $convenience_fee .= ".00";
  } elsif ($convenience_fee =~ /\.\d$/) {
    $convenience_fee .= "0";
  }
  my $total_amount = $amount + $convenience_fee;
  if ($total_amount !~ /\./) {
    $total_amount .= ".00";
  } elsif ($total_amount =~ /\.\d$/) {
    $total_amount .= "0";
  }
  # get remote IP address.
  my $remote_addr = $cgi->remote_addr() || undef;
  my $remote_host = $cgi->remote_host() || undef;
  my $charge_date = "$charge_year-$charge_month-$charge_day";
  my $today_date = "$today_year-$today_month-$today_day";
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # begin database transaction so our updates are atomic.
  $dbh->begin_work();
  # insert rows into database.
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list = ();
  @sql_column_list = ();
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["card_number", $card_number]);
  push(@sql_column_list, ["card_expiration_month", $card_expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $card_expiration_year]);
  push(@sql_column_list, ["card_security_code", $card_security_code]);
  push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $billing_name)]);
  push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $billing_company)]);
  push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $billing_address_num)]);
  push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
  push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $billing_address_1)]);
  push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $billing_address_2)]);
  push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $billing_city)]);
  push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $billing_state)]);
  push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $billing_country)]);
  push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $billing_phone_number)]);
  push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $billing_fax_number)]);
  push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $billing_email_address)]);
  push(@sql_column_list, ["subtotal", $amount]);
  push(@sql_column_list, ["charge_date", $charge_date]);
  push(@sql_column_list, ["invoice_number", $invoice_number]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  if ($charge_date le $today_date) {
    push(@sql_column_list, ["processed", 1, SQL_BOOLEAN]);
  }
  push(@sql_column_list, ["convenience_fee", $convenience_fee]);
  push(@sql_column_list, ["remote_addr", $remote_addr]);
  push(@sql_column_list, ["remote_host", $remote_host]);
  my $credit_card_payment_id = insert_row($dbh, "credit_card_payment_tbl", \@sql_column_list, "credit_card_payment_seq");
  my (%linkpoint_order, %linkpoint_response, $credit_card_txn_success);
  if ($charge_date le $today_date) {
  # initiate transaction and check for result.
    %linkpoint_order =
      (
       "client_ip_address" => $remote_addr || "",
       "total_charge" => $total_amount,
       "card_number" => $card_number,
       "card_expiration_month" => $card_expiration_month,
       "card_expiration_year" => $card_expiration_year,
       "card_security_code" => $card_security_code,
       "billing_name" => $billing_name,
       "billing_company" => $billing_company,
       "billing_address_1" => $billing_address_1,
       "billing_address_2" => $billing_address_2,
       "billing_city" => $billing_city,
       "billing_state" => $billing_state,
       "billing_zip_code" => $billing_zip_code,
       "billing_country" => $billing_country,
       "billing_address_num" => $billing_address_num,
       "billing_phone_number" => $billing_phone_number,
       "billing_fax_number" => $billing_fax_number,
       "billing_email_address" => $billing_email_address,
      );
    %linkpoint_response = charge_card($instance_map, %linkpoint_order);
    $credit_card_txn_success = $linkpoint_response{"r_approved"} eq "APPROVED";
    @sql_column_list = ();
    push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
    push(@sql_column_list, ["card_number", $card_number]);
    push(@sql_column_list, ["card_expiration_month", $card_expiration_month]);
    push(@sql_column_list, ["card_expiration_year", $card_expiration_year]);
    push(@sql_column_list, ["card_security_code", $card_security_code]);
    push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $billing_name)]);
    push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $billing_company)]);
    push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $billing_address_num)]);
    push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
    push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $billing_address_1)]);
    push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $billing_address_2)]);
    push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $billing_city)]);
    push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $billing_state)]);
    push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $billing_country)]);
    push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $billing_phone_number)]);
    push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $billing_fax_number)]);
    push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $billing_email_address)]);
    push(@sql_column_list, ["total_charge", $total_amount]);
    push(@sql_column_list, ["processed_dt", ["now()"]]);
    push(@sql_column_list, ["response_avs", $linkpoint_response{"r_avs"}]);
    push(@sql_column_list, ["response_order_num", $linkpoint_response{"r_ordernum"}]);
    push(@sql_column_list, ["response_error", $linkpoint_response{"r_error"}]);
    push(@sql_column_list, ["response_approved", $linkpoint_response{"r_approved"}]);
    push(@sql_column_list, ["response_code", $linkpoint_response{"r_code"}]);
    push(@sql_column_list, ["response_message", $linkpoint_response{"r_message"}]);
    push(@sql_column_list, ["response_txn_dt", $linkpoint_response{"r_time"}]);
    push(@sql_column_list, ["response_reference_num", $linkpoint_response{"r_ref"}]);
    push(@sql_column_list, ["response_tdate", $linkpoint_response{"r_tdate"}]);
    push(@sql_column_list, ["notes", Encode::encode($db_charset, $note)]);
    push(@sql_column_list, ["ip_address", $remote_addr]);
    push(@sql_column_list, ["credit_card_payment_id", $credit_card_payment_id]);
    insert_row($dbh, "credit_card_txn_tbl", \@sql_column_list);
  }
  # commit the transaction.
  $dbh->commit();
  # email accounts_receivable with credit card transaction notification.
  my $email_subject = "";
  my $email_message = "";
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  my $account_name = $cust_acct->{"name"} || "";
  my $person_name = $cust_pers->{"first_name"} . " " . $cust_pers->{"last_name"};
  my $obscured_card_number = "";
  $obscured_card_number .= substr($card_number, 0, 3);
  $obscured_card_number .= "*" x (length($card_number) - 6);
  $obscured_card_number .= substr($card_number, length($card_number) - 3);
  $email_message .= <<"---END_BLOCK---;";
The following credit card payment was submitted by $person_name ($account_name):

card_number: $obscured_card_number
subtotal: \$$amount
convenience_fee: \$$convenience_fee
total_charge: \$$total_amount
charge_date: $charge_date
invoice_number: $invoice_number
note: $note

---END_BLOCK---;
  # populate information needed for display of page.
  $card_number = HTML::Entities::encode($card_number);
  $card_expiration_month = HTML::Entities::encode($card_expiration_month);
  $card_expiration_year = HTML::Entities::encode($card_expiration_year);
  $card_security_code = HTML::Entities::encode($card_security_code);
  $billing_name = HTML::Entities::encode($billing_name);
  $billing_company = HTML::Entities::encode($billing_company);
  $billing_address_1 = HTML::Entities::encode($billing_address_1);
  $billing_address_2 = HTML::Entities::encode($billing_address_2);
  $billing_city = HTML::Entities::encode($billing_city);
  $billing_state = HTML::Entities::encode($billing_state);
  $billing_zip_code = HTML::Entities::encode($billing_zip_code);
  $billing_country = HTML::Entities::encode($billing_country);
  $billing_address_num = HTML::Entities::encode($billing_address_num);
  $billing_phone_number = HTML::Entities::encode($billing_phone_number);
  $billing_fax_number = HTML::Entities::encode($billing_fax_number);
  $billing_email_address = HTML::Entities::encode($billing_email_address);
  if ($amount !~ /\./) {
    $amount = $amount . ".00";
  }
  $amount = HTML::Entities::encode($amount);
  $charge_month = HTML::Entities::encode($charge_month);
  $charge_day = HTML::Entities::encode($charge_day);
  $charge_year = HTML::Entities::encode($charge_year);
  $charge_date = HTML::Entities::encode($charge_date);
  $invoice_number = HTML::Entities::encode($invoice_number);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <credit-card-payment>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_address_num>$billing_address_num</billing_address_num>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <amount>$amount</amount>
    <charge_month>$charge_month</charge_month>
    <charge_day>$charge_day</charge_day>
    <charge_year>$charge_year</charge_year>
    <charge_date>$charge_date</charge_date>
    <invoice_number>$invoice_number</invoice_number>
    <note>$note</note>
    <convenience_fee>$convenience_fee</convenience_fee>
    <total_amount>$total_amount</total_amount>
  </credit-card-payment>
</page>
---END_BLOCK---;
  my $xslt_file = "";
  if ($charge_date le $today_date) {
    $email_message .= <<"---END_BLOCK---;";
response_avs: $linkpoint_response{"r_avs"}
response_order_num: $linkpoint_response{"r_ordernum"}
response_error: $linkpoint_response{"r_error"}
response_approved: $linkpoint_response{"r_approved"}
response_code: $linkpoint_response{"r_code"}
response_message: $linkpoint_response{"r_message"}
response_txn_dt: $linkpoint_response{"r_time"}
response_reference_num: $linkpoint_response{"r_ref"}
response_tdate: $linkpoint_response{"r_tdate"}
---END_BLOCK---;
    if ($credit_card_txn_success) {
      $xslt_file = "credit_card_payment_result_success.xslt";
      $email_subject = "cc txn \$$total_amount $account_name (ID #$cust_acct_id)";
    } else {
      $xslt_file = "credit_card_payment_result_failed.xslt";
      $email_subject = "cc fail \$$total_amount $account_name (ID #$cust_acct_id)";
    }
  } else {
    $xslt_file = "credit_card_payment_result_scheduled.xslt";
    $email_subject = "cc sched \$$total_amount $account_name (ID #$cust_acct_id) on $charge_date";
    $email_message .= <<"---END_BLOCK---;";
This transaction has been scheduled for a future date.
---END_BLOCK---;
  } 
  send_email
    ("from" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
     "to" => "accounts_receivable\@microoffice.com",
     "subject" => $email_subject,
     "message" => $email_message);
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, $xslt_file, $cgi);
}

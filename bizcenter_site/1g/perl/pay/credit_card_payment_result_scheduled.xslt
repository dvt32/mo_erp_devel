<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Credit Card Payment (Scheduled)
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<p>
	  You have scheduled the following credit card payment:
	</p>
	<xsl:apply-templates select="credit-card-payment"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit-card-payment">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Card Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Expiration Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_expiration_month"/>
	  <xsl:text>/</xsl:text>
	  <xsl:value-of select="card_expiration_year"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Security Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_security_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Company:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_company"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing State:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Fax Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_fax_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Email Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_email_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Entered Amount:
	</td>
	<td class="map_table_value">
	  $<xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Date of Payment:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="charge_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Invoice Number(s):
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="invoice_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Descriptive Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
    <p>There is a convenience fee for handling credit cards (3% - 4% of the transaction amount):</p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Entered Amount:
	</td>
	<td class="map_table_value" style="text-align: right;">
	  $<xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Convenience Fee:
	</td>
	<td class="map_table_value" style="text-align: right; border-bottom: thin solid black;">
	  $<xsl:value-of select="convenience_fee"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Total Amount:
	</td>
	<td class="map_table_value" style="text-align: right;">
	  $<xsl:value-of select="total_amount"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

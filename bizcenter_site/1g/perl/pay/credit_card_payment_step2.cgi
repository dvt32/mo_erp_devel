#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use POSIX;
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use lead_pad;
use is_valid_credit_card;
use days_in_month;
use strip_outside_whitespace;
use credit_card_type;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  if (!$cust_pers->{"enable_cc_pay"}) {
    die "show_page: credit_card_payment_not_authorized.html\n";
  }
  # get CGI form input.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $card_number = strip_outside_whitespace($cgi->param("card_number") || "");
  my $card_expiration_month = $cgi->param("card_expiration_month") || "";
  my $card_expiration_year = $cgi->param("card_expiration_year") || "";
  my $card_security_code = strip_outside_whitespace($cgi->param("card_security_code") || "");
  my $billing_name = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_name")) || "");
  my $billing_company = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_company")) || "");
  my $billing_address_1 = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_address_1")) || "");
  my $billing_address_2 = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_address_2")) || "");
  my $billing_city = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_city")) || "");
  my $billing_state = Encode::decode($cgi_input_charset, $cgi->param("billing_state")) || "";
  my $billing_state_other = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_state_other")) || "");
  my $billing_zip_code = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_zip_code")) || "");
  my $billing_country = Encode::decode($cgi_input_charset, $cgi->param("billing_country")) || "";
  my $billing_address_num = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_address_num")) || "");
  my $billing_phone_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_phone_number")) || "");
  my $billing_fax_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_fax_number")) || "");
  my $billing_email_address = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("billing_email_address")) || "");
  my $amount = strip_outside_whitespace($cgi->param("amount") || "");
  my $charge_month = $cgi->param("charge_month") || "";
  my $charge_day = $cgi->param("charge_day") || "";
  my $charge_year = $cgi->param("charge_year") || "";
  my $invoice_number = strip_outside_whitespace($cgi->param("invoice_number") || "");
  my $note = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("note")) || "");
  # validate card number.
  if (!is_valid_credit_card($card_number)) {
    die "show_page: credit_card_payment_bad_card_number.html\n";
  }
  # verify that expiration month is valid.
  if (($card_expiration_month !~ /^\d+$/) || ($card_expiration_month < 1) || ($card_expiration_month > 12)) {
    die "show_page: credit_card_payment_bad_expiration_month.html\n";
  } else {
    $card_expiration_month = lead_pad(int($card_expiration_month), 2);
  }
  # verify that expiration year is valid.
  if ($card_expiration_year !~ /^\d{4}$/) {
    die "show_page: credit_card_payment_bad_expiration_year.html\n";
  }
  # verify that security code is either blank or 3 or 4 digits.
  if (($card_security_code ne "") && ($card_security_code !~ /^\d{3,4}$/)) {
    die "show_page: credit_card_payment_bad_security_code.html\n";
  }
  # verify that billing name exists.
  if (!$billing_name) {
    die "show_page: credit_card_payment_missing_billing_name.html\n";
  }
  # verify that address 1 is not blank.
  if (!$billing_address_1) {
    die "show_page: credit_card_payment_missing_address.html\n";
  }
  # verify that city is not blank.
  if (!$billing_city) {
    die "show_page: credit_card_payment_missing_city.html\n";
  }
  # verify that state/province/territory is not blank and that only one is specified.
  if ($billing_state && $billing_state_other) {
    die "show_page: credit_card_payment_bad_state.html\n";
  } elsif (!$billing_state && !$billing_state_other) {
    die "show_page: credit_card_payment_missing_state.html\n";
  } elsif (!$billing_state) {
    $billing_state = $billing_state_other;
  }
  # verify that zip code is not blank.
  if (!$billing_zip_code) {
    die "show_page: credit_card_payment_missing_zip_code.html\n";
  }
  # verify that country is not blank.
  if (!$billing_country) {
    die "show_page: credit_card_payment_missing_country.html\n";
  }
  # verify that billing address number is valid.
  if (($billing_address_num) && ($billing_address_num !~ /^\d+$/)) {
    die "show_page: credit_card_payment_bad_billing_address_num.html\n";
  }
  # verify that amount of payment is proper format.
  if ($amount !~ /^(\d+|\d+\.\d\d)$/) {
    die "show_page: credit_card_payment_bad_amount.html\n";
  }
  # determine today's date.
  my ($today_year, $today_month, $today_day) = (localtime(time()))[5, 4, 3];
  $today_year += 1900;
  $today_month++;
  $today_month = lead_pad($today_month, 2);
  $today_day = lead_pad($today_day, 2);
  # verify that charge month is valid.
  if (($charge_month !~ /^(\d\d)$/) || (int($charge_month) < 1) || (int($charge_month) > 12)) {
    die "show_page: credit_card_payment_bad_charge_month.html\n";
  }
  # verify that charge year is valid.
  if (($charge_year !~ /^(\d{4})$/) || ($charge_year < $today_year) || ($charge_year > ($today_year + 1))) {
    die "show_page: credit_card_payment_bad_charge_year.html\n";
  }
  # verify that charge day is valid.
  if (($charge_day !~ /^(\d\d)$/) || (int($charge_day) < 1) || (int($charge_day) > days_in_month($charge_month, $charge_year))) {
    die "show_page: credit_card_payment_bad_charge_day.html\n";
  }
  # populate information needed for display of page.
  $card_number = HTML::Entities::encode($card_number);
  $card_expiration_month = HTML::Entities::encode($card_expiration_month);
  $card_expiration_year = HTML::Entities::encode($card_expiration_year);
  $card_security_code = HTML::Entities::encode($card_security_code);
  $billing_name = HTML::Entities::encode($billing_name);
  $billing_company = HTML::Entities::encode($billing_company);
  $billing_address_1 = HTML::Entities::encode($billing_address_1);
  $billing_address_2 = HTML::Entities::encode($billing_address_2);
  $billing_city = HTML::Entities::encode($billing_city);
  $billing_state = HTML::Entities::encode($billing_state);
  $billing_zip_code = HTML::Entities::encode($billing_zip_code);
  $billing_country = HTML::Entities::encode($billing_country);
  $billing_address_num = HTML::Entities::encode($billing_address_num);
  $billing_phone_number = HTML::Entities::encode($billing_phone_number);
  $billing_fax_number = HTML::Entities::encode($billing_fax_number);
  $billing_email_address = HTML::Entities::encode($billing_email_address);
  if ($amount !~ /\./) {
    $amount = $amount . ".00";
  }
  $amount = HTML::Entities::encode($amount);
  $charge_month = HTML::Entities::encode($charge_month);
  $charge_day = HTML::Entities::encode($charge_day);
  $charge_year = HTML::Entities::encode($charge_year);
  $invoice_number = HTML::Entities::encode($invoice_number);
  $note = HTML::Entities::encode($note);
  my $convenience_fee;
  my $card_type = credit_card_type($card_number);
  if ($card_type eq "amex") {
    $convenience_fee = POSIX::ceil($amount * 3.5)/100;
  } elsif (($card_type eq "visa") || ($card_type eq "mastercard")) {
    $convenience_fee = POSIX::ceil($amount * 3)/100;
  } elsif ($card_type eq "discover") {
    $convenience_fee = POSIX::ceil($amount * 3)/100;
  } else {
    $convenience_fee = POSIX::ceil($amount * 3.5)/100;
  }
  if ($convenience_fee !~ /\./) {
    $convenience_fee .= ".00";
  } elsif ($convenience_fee =~ /\.\d$/) {
    $convenience_fee .= "0";
  }
  my $total_amount = $amount + $convenience_fee;
  if ($total_amount !~ /\./) {
    $total_amount .= ".00";
  } elsif ($total_amount =~ /\.\d$/) {
    $total_amount .= "0";
  }
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <credit-card-payment>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_address_num>$billing_address_num</billing_address_num>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <amount>$amount</amount>
    <charge_month>$charge_month</charge_month>
    <charge_day>$charge_day</charge_day>
    <charge_year>$charge_year</charge_year>
    <charge_date>$charge_year-$charge_month-$charge_day</charge_date>
    <invoice_number>$invoice_number</invoice_number>
    <note>$note</note>
    <convenience_fee>$convenience_fee</convenience_fee>
    <total_amount>$total_amount</total_amount>
  </credit-card-payment>
  <today>$today_year-$today_month-$today_day</today>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "credit_card_payment_step2.xslt", $cgi);
}

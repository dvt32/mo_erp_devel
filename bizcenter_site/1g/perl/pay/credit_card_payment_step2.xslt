<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Credit Card Payment (Step 2)
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<p>
	  Please confirm the following credit card payment:
	</p>
	<xsl:apply-templates select="credit-card-payment"/>
	<p>
	  Otherwise, you may cancel by choosing to return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit-card-payment">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Card Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Expiration Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_expiration_month"/>
	  <xsl:text>/</xsl:text>
	  <xsl:value-of select="card_expiration_year"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Security Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_security_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Company:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_company"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing State:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Fax Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_fax_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Email Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_email_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Entered Amount:
	</td>
	<td class="map_table_value">
	  $<xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Date of Payment:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="translate(charge_date,'-','0') &gt; translate(/page/today,'-','0')">
	      <xsl:value-of select="charge_date"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="charge_date"/> (Process Payment Immediately)
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Invoice Number(s):
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="invoice_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Descriptive Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
    <p>There is a convenience fee for handling credit cards (3% - 4% of the transaction amount):</p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Entered Amount:
	</td>
	<td class="map_table_value" style="text-align: right;">
	  $<xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Convenience Fee:
	</td>
	<td class="map_table_value" style="text-align: right; border-bottom: thin solid black;">
	  $<xsl:value-of select="convenience_fee"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Total Amount:
	</td>
	<td class="map_table_value" style="text-align: right;">
	  $<xsl:value-of select="total_amount"/>
	</td>
      </tr>
    </table>
    <xsl:choose>
      <xsl:when test="translate(charge_date,'-','0') &gt; translate(/page/today,'-','0')">
	<p>Click on "Schedule Payment" below to proceed. Your payment will be processed on <xsl:value-of select="charge_date"/>.</p>
      </xsl:when>
      <xsl:otherwise>
	<p>Click on "Make Payment" below to proceed.</p>
      </xsl:otherwise>
    </xsl:choose>
    <form action="/pay/credit_card_payment_result.cgi" method="post" name="credit_card_payment_form">
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">card_number</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="card_number"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">card_expiration_month</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="card_expiration_month"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">card_expiration_year</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="card_expiration_year"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">card_security_code</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="card_security_code"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_name</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_name"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_company</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_company"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_address_1</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_address_1"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_address_2</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_address_2"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_city</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_city"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_state</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_state"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_zip_code</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_zip_code"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_country</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_country"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_address_num</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_address_num"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_phone_number</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_phone_number"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_fax_number</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_fax_number"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">billing_email_address</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="billing_email_address"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">amount</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="amount"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">charge_month</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="charge_month"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">charge_day</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="charge_day"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">charge_year</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="charge_year"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">invoice_number</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="invoice_number"/></xsl:attribute>
      </input>
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">note</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
      </input>
      <xsl:choose>
	<xsl:when test="translate(charge_date,'-','0') &gt; translate(/page/today,'-','0')">
	  <input type="submit" name="submitButton" value="Schedule Payment"/>
	</xsl:when>
	<xsl:otherwise>
	  <input type="submit" name="submitButton" value="Make Payment"/>
	</xsl:otherwise>
      </xsl:choose>
    </form>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

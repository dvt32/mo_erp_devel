#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_by_id_map;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # fetch bank accounts belonging to this account.
  my (%bank_account_by_id, @bank_account_id_list);
  my $sql_query = "select * from bank_account_tbl where account_id = ? order by depository_name, bank_account_number";
  my @sql_param_list = ([$cust_acct_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%bank_account_by_id, \@bank_account_id_list);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my ($today_year, $today_month_number, $today_day_number) = (localtime(time()))[5, 4, 3];
  $today_year += 1900;
  $today_month_number++;
  $today_month_number = lead_pad($today_month_number, 2);
  $today_day_number = lead_pad($today_day_number, 2);
  my $next_year = $today_year + 1;
  my @month_name_list = (qw(January February March April May June July August September October November December));
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <cust-acct-id>$cust_acct_id</cust-acct-id>
  <account-name>$account_name</account-name>
  <electronic-check>
    <bank-account-type-list>
      <bank-account-type>checking</bank-account-type>
      <bank-account-type>savings</bank-account-type>
      <bank-account-type>money market</bank-account-type>
    </bank-account-type-list>
    <bank-account-list>
---END_BLOCK---;
  for my $bank_account_id (@bank_account_id_list) {
    my $bank_account = $bank_account_by_id{$bank_account_id};
    my $depository_name = HTML::Entities::encode($bank_account->{"depository_name"});
    my $address_1 = HTML::Entities::encode($bank_account->{"address_1"});
    my $address_2 = HTML::Entities::encode($bank_account->{"address_2"} || "");
    my $city = HTML::Entities::encode($bank_account->{"city"});
    my $state = HTML::Entities::encode($bank_account->{"state"});
    my $zip_code = HTML::Entities::encode($bank_account->{"zip_code"});
    my $country = HTML::Entities::encode($bank_account->{"country"});
    my $bank_account_type = HTML::Entities::encode($bank_account->{"bank_account_type"});
    my $bank_routing_number = $bank_account->{"bank_routing_number"};
    my $bank_account_number = $bank_account->{"bank_account_number"};
    my $notes = HTML::Entities::encode($bank_account->{"notes"} || "");
    my $bank_account_customer_name = HTML::Entities::encode($bank_account->{"bank_account_customer_name"});
    my $bank_account_ownership_type = HTML::Entities::encode($bank_account->{"bank_account_ownership_type"});
    my $bank_account_nickname = HTML::Entities::encode($bank_account->{"bank_account_nickname"} || "");
    my $is_primary = $bank_account->{"is_primary"} ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
      <bank-account>
        <id>$bank_account_id</id>
        <depository_name>$depository_name</depository_name>
        <address_1>$address_1</address_1>
        <address_2>$address_2</address_2>
        <city>$city</city>
        <state>$state</state>
        <zip_code>$zip_code</zip_code>
        <country>$country</country>
        <bank_account_type>$bank_account_type</bank_account_type>
        <bank_routing_number>$bank_routing_number</bank_routing_number>
        <bank_account_number>$bank_account_number</bank_account_number>
        <notes>$notes</notes>
        <bank_account_customer_name>$bank_account_customer_name</bank_account_customer_name>
        <bank_account_ownership_type>$bank_account_ownership_type</bank_account_ownership_type>
        <bank_account_nickname>$bank_account_nickname</bank_account_nickname>
        <is_primary>$is_primary</is_primary>
      </bank-account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </bank-account-list>
    <current-date>
      <month>$today_month_number</month>
      <day>$today_day_number</day>
      <year>$today_year</year>
    </current-date>
    <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $number = lead_pad($i, 2);
    my $name = $month_name_list[$i - 1];
    $generated_xml .= <<"---END_BLOCK---;";
      <month>
        <number>$number</number>
        <name>$name</name>
      </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </month-list>
    <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    my $day = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
      <day>$day</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </day-list>
    <year-list>
      <year>$today_year</year>
      <year>$next_year</year>
    </year-list>
  </electronic-check>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "electronic_check_input.xslt", $cgi);
}

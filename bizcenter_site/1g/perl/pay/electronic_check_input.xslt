<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Electronic Check Payment
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<style>
	  span.field_name {
            font-weight: bold;
            text-decoration: underline;
          }
	</style>
	<table cellspacing="0" cellpadding="4px" border="0">
	  <tr>
	    <td class="map_table_key">
	      Customer Account ID:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="cust-acct-id"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Customer Name:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="account-name"/>
	    </td>
	  </tr>
	</table>
	<br/>
	<xsl:apply-templates select="electronic-check"/>
	<p>
	  Here are some locations where bank routing numbers and bank account numbers usually appear on checks:
	</p>
	<p>
	  <img src="/1g/img/check_bottom_one.gif" alt="routing number and account number example 1"/>
	  - or -
	  <img src="/1g/img/check_bottom_two.gif" alt="routing number and account number example 2"/>
	</p>
	<p>
	  Explanation of Fields:
	</p>
	<ul>
	  <li>
	    <span class="field_name">Name on Bank Account</span> must match the name on your checking or savings account. This is usually your name or the
	    name of your company, depending on what name your bank account is listed under.
	  </li>
	  <li>
	    <span class="field_name">Bank Routing Number</span> is
	    shown in the pictures above as "Routing Transit
	    Number". It is a 9 digit number used to identify your
	    bank. It is sometimes called the Routing Transit Number or
	    the ABA Number (ABA stands for American Banking
	    Association). Several examples are shown in the pictures
	    above as "Routing Transit Number".
	  </li>
	  <li>
	    <span class="field_name">Bank Account Number</span> is
	    your checking or savings account number. The number of
	    digits varies depending on the conventions of your
	    bank. Several examples are shown in the pictures above as
	    "Checking Account Number" (but it could equally well be
	    your savings account number).
	  </li>
	  <li>
	    <span class="field_name">Amount of Payment</span> is in
	    dollars and cents (xxx.xx). You should omit the leading
	    dollar sign.
	  </li>
	  <li>
	    <span class="field_name">Check Date</span> is the date
	    that you would like the withdrawal to occur. Your account
	    will not be debited before this date. Should you change
	    your mind one business day before the Check Date you
	    specify, email <a
	    href="mailto:service@microoffice.com">service@microoffice.com</a>
	    to cancel your payment or modify your payment date.
	  </li>
	  <li>
	    <span class="field_name">Invoice Number(s)</span> is to
	    tell us which invoices you are paying using this
	    electronic check. This is optional, but strongly
	    recommended.
	  </li>
	  <li>
	    <span class="field_name">Descriptive Note</span> is for a
	    quick note to us about the payment (e.g. special handling
	    instructions or odd situations). This is optional.
	  </li>
	</ul>
	<p>
	  Additional Notes:
	</p>
	<ul>
	  <li>Since this is a paperless transaction, Micro Office Solutions and your bank do not use a check number.</li>
	  <li>The transaction will appear on your bank statement as an ACH debit from "MICRO OFC".</li>
	  <li>
	    This electronic transaction is protected by SSL, evidenced
	    by the lock in the lower right hand corner of your browser (
	    <img src="/1g/img/ie_lock.png" alt="Internet Explorer lock"/>
	    in Microsoft Internet Explorer or
	    <img src="/1g/img/firefox_lock.png" alt="Mozilla Firefox lock"/>
	    in Mozilla Firefox).  SSL significantly reduces the
	    chances of a third party intercepting your sensitive
	    financial information.
	  </li>
	</ul>
	<p>
	  Click "Make Payment" above to make your payment, or if you
	  want to cancel without making a payment at this time, you
	  may return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="electronic-check">
    <script>
      var bank_account_list = new Array();
      var bank_account_index = 0;
      <xsl:for-each select="bank-account-list/bank-account">
        bank_account_list[bank_account_index] = new Array(5);
        bank_account_list[bank_account_index][0] = <xsl:value-of select="id"/>;
        bank_account_list[bank_account_index][1] = "<xsl:value-of select="bank_account_customer_name"/>";
        bank_account_list[bank_account_index][2] = "<xsl:value-of select="bank_account_type"/>";
        bank_account_list[bank_account_index][3] = "<xsl:value-of select="bank_routing_number"/>";
        bank_account_list[bank_account_index][4] = "<xsl:value-of select="bank_account_number"/>";
        bank_account_index++;
      </xsl:for-each>
      <xsl:text disable-output-escaping="yes">
        function select_bank_account(select_object) {
          var select_option_list = select_object.options;
          var bank_account_id = select_option_list[select_object.selectedIndex].value;
          var this_form = select_object.form;
          if (bank_account_id == "") {
            this_form.elements["bank_account_name"].value = "";
            this_form.elements["bank_account_type"].selectedIndex = 0;
            this_form.elements["routing_number"].value = "";
            this_form.elements["account_number"].value = "";
            return;
          }
          for(var i = 0; i &lt; bank_account_list.length; i++) {
            if (bank_account_id == bank_account_list[i][0]) {
              this_form.elements["bank_account_name"].value = bank_account_list[i][1];
              var bank_account_type = bank_account_list[i][2];
              var bank_account_type_select_object = this_form.elements["bank_account_type"];
              var type_option_list = bank_account_type_select_object.options;
              for(var j = 0; j &lt; type_option_list.length; j++) {
                if (type_option_list[j].value == bank_account_type) {
                  bank_account_type_select_object.selectedIndex = j;
                }
              }
              this_form.elements["routing_number"].value = bank_account_list[i][3];
              this_form.elements["account_number"].value = bank_account_list[i][4];
            }
          }
        }
      </xsl:text>
    </script>
    <table>
      <tr><td>
	<div align="center" style="color: white; background-color: black;">
	  Make a One-Time Electronic Check Payment
	</div>
	<form action="/pay/electronic_check_result.cgi" method="post" name="electronic_check_form">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Bank Account Details
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Selector:
		</td>
		<td>
                  <select name="bank_account_id" onChange="select_bank_account(this);">
		    <option value="">[---]</option>
		    <xsl:for-each select="bank-account-list/bank-account">
		      <option>
			<xsl:if test="is_primary = 1">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:choose>
			  <xsl:when test="bank_account_nickname != ''">
			    <xsl:value-of select="bank_account_nickname"/>
			  </xsl:when>
			  <xsl:otherwise>
			    <xsl:value-of select="depository_name"/>
			    <xsl:text> </xsl:text>
			    <xsl:value-of select="bank_account_number"/>
			  </xsl:otherwise>
			</xsl:choose>
		      </option>
		    </xsl:for-each>
		  </select>
		  (<a href="/1g/profile/show_bank_accounts.cgi">add/edit bank accounts</a>)
		</td>
	      </tr>
	      <tr>
		<td></td>
		<td>-or-</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Name on Bank Account:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">bank_account_name</xsl:attribute>
		    <xsl:attribute name="size">22</xsl:attribute>
		    <xsl:attribute name="maxlength">22</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (your name or your company's name)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Type:
		</td>
		<td>
                  <select name="bank_account_type">
		    <xsl:for-each select="bank-account-type-list/bank-account-type">
		      <option>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Routing Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">routing_number</xsl:attribute>
		    <xsl:attribute name="size">9</xsl:attribute>
		    <xsl:attribute name="maxlength">9</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">account_number</xsl:attribute>
		    <xsl:attribute name="size">17</xsl:attribute>
		    <xsl:attribute name="maxlength">17</xsl:attribute>
		  </input>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	  <fieldset class="standard_fieldset">
	    <legend>
	      Payment Details
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Amount of Payment:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">amount</xsl:attribute>
		    <xsl:attribute name="size">9</xsl:attribute>
		    <xsl:attribute name="maxlength">9</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Check Date:
		</td>
		<td>
                  <select name="debit_month">
		    <xsl:for-each select="month-list/month">
		      <option>
			<xsl:if test="number = ../../current-date/month">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
                  <select name="debit_day">
		    <xsl:for-each select="day-list/day">
		      <option>
			<xsl:if test=". = ../../current-date/day">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
                  <select name="debit_year">
		    <xsl:for-each select="year-list/year">
		      <option>
			<xsl:if test=". = ../../current-date/year">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Invoice Number(s):
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">invoice_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Descriptive Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">notes</xsl:attribute>
		  </input>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	  <div align="center">
	    <input type="submit" name="submitButton" value="Make Payment"/>
	  </div>
	</form>
      </td></tr>
    </table>
    <script>
      select_bank_account(document.electronic_check_form.elements["bank_account_id"]);
    </script>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

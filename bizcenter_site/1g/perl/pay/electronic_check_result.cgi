#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_cust_acct;
use strip_outside_whitespace;
use days_in_month;
use insert_row;
use enqueue_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $bank_account_name = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("bank_account_name")) || "");
  my $bank_account_type = $cgi->param("bank_account_type") || "";
  my $routing_number = strip_outside_whitespace($cgi->param("routing_number") || "");
  my $account_number = strip_outside_whitespace($cgi->param("account_number") || "");
  my $amount = strip_outside_whitespace($cgi->param("amount") || "");
  my $debit_month = $cgi->param("debit_month") || "";
  my $debit_day = $cgi->param("debit_day") || "";
  my $debit_year = $cgi->param("debit_year") || "";
  my $invoice_number = strip_outside_whitespace($cgi->param("invoice_number") || "");
  my $notes = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("notes")) || "");
  # compute today's date.
  my ($today_year) = (localtime(time()))[5];
  $today_year += 1900;
  # verify that bank account name is not blank and that it is not too long.
  if ((!$bank_account_name) || (length($bank_account_name) > 22)) {
    die "show_page: electronic_check_bad_bank_account_name.html\n";
  }
  # verify that bank account type is either checking or savings.
  if ($bank_account_type !~ /^(checking|savings)$/) {
    die "show_page: electronic_check_bad_bank_account_type.html\n";
  }
  # verify that routing number is proper format.
  if ($routing_number !~ /^\d{9}$/) {
    die "show_page: electronic_check_bad_routing_number.html\n";
  }
  # verify that account number is proper format.
  if (($account_number !~ /^\d+$/) || (length($account_number) > 17)) {
    die "show_page: electronic_check_bad_account_number.html\n";
  }
  # verify that amount of payment is proper format.
  if ($amount !~ /^(\d+|\d+\.\d\d)$/) {
    die "show_page: electronic_check_bad_amount.html\n";
  }
  # verify that debit month is valid.
  if (($debit_month !~ /^(\d\d)$/) || (int($debit_month) < 1) || (int($debit_month) > 12)) {
    die "show_page: electronic_check_bad_debit_month.html\n";
  }
  # verify that debit year is valid.
  if (($debit_year !~ /^(\d{4})$/) || ($debit_year < $today_year) || ($debit_year > ($today_year + 1))) {
    die "show_page: electronic_check_bad_debit_year.html\n";
  }
  # verify that debit day is valid.
  if (($debit_day !~ /^(\d\d)$/) || (int($debit_day) < 1) || (int($debit_day) > days_in_month($debit_month, $debit_year))) {
    die "show_page: electronic_check_bad_debit_day.html\n";
  }
  # get the remote IP address and hostname.
  my $remote_addr = $cgi->remote_addr() || undef;
  my $remote_host = $cgi->remote_host() || undef;
  # generate SQL date for debit_date from debit_month, debit_day, debit_year.
  my $debit_date = $debit_year . "-" . $debit_month . "-" . $debit_day;
  # begin transaction of inserting a new row.
  $dbh->begin_work();
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["bank_account_name", Encode::encode($db_charset, $bank_account_name)]);
  push(@sql_column_list, ["bank_account_type", $bank_account_type]);
  push(@sql_column_list, ["routing_number", $routing_number]);
  push(@sql_column_list, ["account_number", $account_number]);
  push(@sql_column_list, ["amount", $amount]);
  push(@sql_column_list, ["debit_date", $debit_date]);
  push(@sql_column_list, ["invoice_number", $invoice_number]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  push(@sql_column_list, ["remote_addr", $remote_addr]);
  push(@sql_column_list, ["remote_host", $remote_host]);
  insert_row($dbh, "electronic_check_tbl", \@sql_column_list);
  my $number_length = length($account_number);
  my $obscured_account_number = "*" x ($number_length - 4);
  $obscured_account_number .= substr($account_number, $number_length - 4);
  # enqueue the receipt email.
  my $receipt_email_message = <<"---END_BLOCK---;";
Thank you for your payment of \$$amount. Your payment will be debited from your $obscured_account_number bank account on $debit_date. We appreciate your business.
---END_BLOCK---;
  enqueue_email
    ("dbh" => $dbh,
     "account_id" => $cust_acct_id,
     "from_hdr" => "Micro Office Solutions <service\@microoffice.com>",
     "to_hdr" => $cust_pers->{"email"},
     "subject_hdr" => "Receipt for electronic check payment",
     "message_body" => $receipt_email_message,
    );
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $account_name = $cust_acct->{"name"};
  my @month_name_list = (qw(January February March April May June July August September October November December));
  my $text_debit_date = $month_name_list[$debit_month - 1] . " " . $debit_day . ", " . $debit_year;
  $account_name = HTML::Entities::encode($account_name);
  $bank_account_name = HTML::Entities::encode($bank_account_name);
  $invoice_number = HTML::Entities::encode($invoice_number);
  $notes = HTML::Entities::encode($notes);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <electronic-check>
    <cust-acct-id>$cust_acct_id</cust-acct-id>
    <account-name>$account_name</account-name>
    <bank-account-name>$bank_account_name</bank-account-name>
    <bank-account-type>$bank_account_type</bank-account-type>
    <routing-number>$routing_number</routing-number>
    <account-number>$account_number</account-number>
    <amount>$amount</amount>
    <debit-date>$text_debit_date</debit-date>
    <invoice-number>$invoice_number</invoice-number>
    <notes>$notes</notes>
  </electronic-check>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "electronic_check_result_success.xslt", $cgi);
}

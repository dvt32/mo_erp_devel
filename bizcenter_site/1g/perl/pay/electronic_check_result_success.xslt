<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Electronic Check Payment
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<p>
	  You successfully entered the following electronic check payment:
	</p>
	<xsl:apply-templates select="electronic-check"/>
	<p>
	  If you change your mind, you may cancel the payment or
	  modify the payment up to one business day before the check
	  date by emailing
	  <a href="mailto:service@microoffice.com">service@microoffice.com</a>.
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="electronic-check">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Customer Account ID:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cust-acct-id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank-account-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank-account-type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Routing Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="routing-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Amount:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Check Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="debit-date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Invoice Number(s):
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="invoice-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Descriptive Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # load bank account.
  my $bank_account_id = $cgi->param("id");
  if ((!defined($bank_account_id)) || ($bank_account_id !~ /^\d+$/)) {
    die "show_page: delete_bank_account_not_found.html\n";
  }
  # begin transaction.
  $dbh->begin_work();
  # check that bank account row exist.
  my $bank_account = select_row($dbh, "bank_account_tbl", $bank_account_id);
  if (!$bank_account) {
    $dbh->rollback();
    die "show_page: delete_bank_account_not_found.html\n";
  }
  # check that bank account belongs to user.
  if ($cust_acct->{"id"} != $bank_account->{"account_id"}) {
    $dbh->rollback();
    die "show_page: delete_bank_account_not_owner.html\n";
  }
  # issue the delete command to the database to delete the bank account.
  delete_row($dbh, "bank_account_tbl", $bank_account_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $bank_account_nickname = HTML::Entities::encode($bank_account->{"bank_account_nickname"} || "");
  my $depository_name = HTML::Entities::encode($bank_account->{"depository_name"});
  my $address_1 = HTML::Entities::encode($bank_account->{"address_1"});
  my $address_2 = HTML::Entities::encode($bank_account->{"address_2"} || "");
  my $city = HTML::Entities::encode($bank_account->{"city"});
  my $state = HTML::Entities::encode($bank_account->{"state"});
  my $zip_code = HTML::Entities::encode($bank_account->{"zip_code"});
  my $country = HTML::Entities::encode($bank_account->{"country"});
  my $bank_account_customer_name = HTML::Entities::encode($bank_account->{"bank_account_customer_name"});
  my $bank_account_ownership_type = HTML::Entities::encode($bank_account->{"bank_account_ownership_type"});
  my $bank_account_type = HTML::Entities::encode($bank_account->{"bank_account_type"});
  my $number_length;
  $number_length = length($bank_account->{"bank_routing_number"});
  my $bank_routing_number = "*" x ($number_length - 4);
  $bank_routing_number .= substr($bank_account->{"bank_routing_number"}, $number_length - 4);
  $number_length = length($bank_account->{"bank_account_number"});
  my $bank_account_number = "*" x ($number_length - 4);
  $bank_account_number .= substr($bank_account->{"bank_account_number"}, $number_length - 4);
  my $notes = HTML::Entities::encode($bank_account->{"notes"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <bank-account>
    <account_name>$account_name</account_name>
    <bank_account_nickname>$bank_account_nickname</bank_account_nickname>
    <depository_name>$depository_name</depository_name>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <bank_account_customer_name>$bank_account_customer_name</bank_account_customer_name>
    <bank_account_ownership_type>$bank_account_ownership_type</bank_account_ownership_type>
    <bank_account_type>$bank_account_type</bank_account_type>
    <bank_routing_number>$bank_routing_number</bank_routing_number>
    <bank_account_number>$bank_account_number</bank_account_number>
    <notes>$notes</notes>
  </bank-account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_bank_account.xslt", $cgi);
}

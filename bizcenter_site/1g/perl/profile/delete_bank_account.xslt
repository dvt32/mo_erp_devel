<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Delete Bank Account
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="bank-account"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="bank-account">
    <p>
      You successfully deleted the following bank account:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Bank Account Nickname:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_account_nickname"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Depository Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="depository_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Depository Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Depository Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Depository City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Depository State:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Depository Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Depository Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_account_customer_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Ownership:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_account_ownership_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_account_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Routing Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_routing_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_account_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

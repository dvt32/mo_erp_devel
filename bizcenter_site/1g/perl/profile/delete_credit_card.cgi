#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # load credit card.
  my $credit_card_id = $cgi->param("id");
  if ((!defined($credit_card_id)) || ($credit_card_id !~ /^\d+$/)) {
    die "show_page: delete_credit_card_not_found.html\n";
  }
  # begin transaction.
  $dbh->begin_work();
  # check that credit card row exist.
  my $credit_card = select_row($dbh, "credit_card_tbl", $credit_card_id);
  if (!$credit_card) {
    die "show_page: delete_credit_card_not_found.html\n";
  }
  # check that credit card belongs to user.
  if (($cust_acct->{"id"} != $credit_card->{"account_id"}) || ($cust_pers->{"id"} != $credit_card->{"person_id"})) {
    die "show_page: delete_credit_card_not_owner.html\n";
  }
  # issue the delete command to the database to delete the credit card.
  delete_row($dbh, "credit_card_tbl", $credit_card_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $card_nickname = HTML::Entities::encode($credit_card->{"card_nickname"});
  my $number_length = length($credit_card->{"card_number"});
  my $card_number = "*" x ($number_length - 4);
  $card_number .= substr($credit_card->{"card_number"}, $number_length - 4);
  my $card_expiration_month = $credit_card->{"card_expiration_month"};
  my $card_expiration_year = $credit_card->{"card_expiration_year"};
  my $billing_name = HTML::Entities::encode($credit_card->{"billing_name"});
  my $billing_company = HTML::Entities::encode($credit_card->{"billing_company"} || "");
  my $billing_address_1 = HTML::Entities::encode($credit_card->{"billing_address_1"});
  my $billing_address_2 = HTML::Entities::encode($credit_card->{"billing_address_2"} || "");
  my $billing_city = HTML::Entities::encode($credit_card->{"billing_city"});
  my $billing_state = HTML::Entities::encode($credit_card->{"billing_state"});
  my $billing_zip_code = HTML::Entities::encode($credit_card->{"billing_zip_code"});
  my $billing_country = HTML::Entities::encode($credit_card->{"billing_country"});
  my $billing_address_num = HTML::Entities::encode($credit_card->{"billing_address_num"});
  my $billing_phone_number = HTML::Entities::encode($credit_card->{"billing_phone_number"} || "");
  my $billing_fax_number = HTML::Entities::encode($credit_card->{"billing_fax_number"} || "");
  my $billing_email_address = HTML::Entities::encode($credit_card->{"billing_email_address"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <credit-card>
    <account_name>$account_name</account_name>
    <card_nickname>$card_nickname</card_nickname>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_address_num>$billing_address_num</billing_address_num>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
  </credit-card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_credit_card.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use select_row_with_query;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_pers_id = $cgi->param("id");
  if ((!defined($cust_pers_id)) || ($cust_pers_id !~ /^\d+$/)) {
    die "show_page: delete_person_not_found.html\n";
  }
  # begin transaction.
  $dbh->begin_work();
  my $cust_pers = select_row($dbh, "cust_pers_tbl", $cust_pers_id);
  if (!$cust_pers) {
    die "show_page: delete_person_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $cust_pers->{"cust_acct_id"}) {
    die "show_page: delete_person_not_owner.html\n";
  }
  # check that the cust_pers is not linked to any other tables.
  my $num_links = 0;
  my $sql_query = "select * from cust_acct_tbl where primary_contact_cust_pers_id = ?";
  if (select_row_with_query($dbh, $sql_query, [[$cust_pers_id, SQL_INTEGER]])) {
    $num_links++;
  }
  $sql_query = "select * from phone_number_tbl where person_id = ?";
  if (select_row_with_query($dbh, $sql_query, [[$cust_pers_id, SQL_INTEGER]])) {
    $num_links++;
  }
  $sql_query = "select * from proximity_card_tbl where person_id = ?";
  if (select_row_with_query($dbh, $sql_query, [[$cust_pers_id, SQL_INTEGER]])) {
    $num_links++;
  }
  # problem, so roll back transaction.
  if ($num_links > 0) {
    $dbh->rollback();
    die "show_page: delete_person_link_exists.html\n";
  }
  # otherwise, no links, so continue with deletion.
  delete_row($dbh, "cust_pers_tbl", $cust_pers_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $salutation = HTML::Entities::encode($cust_pers->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($cust_pers->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($cust_pers->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($cust_pers->{"last_name"} || "");
  my $nickname = HTML::Entities::encode($cust_pers->{"nickname"} || "");
  my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
  my $work_phone_number = HTML::Entities::encode($cust_pers->{"work_phone_number"} || "");
  my $home_phone_number = HTML::Entities::encode($cust_pers->{"home_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($cust_pers->{"cell_phone_number"} || "");
  my $directory_1 = HTML::Entities::encode($cust_pers->{"directory_1"} || "");
  my $directory_2 = HTML::Entities::encode($cust_pers->{"directory_2"} || "");
  my $directory_3 = HTML::Entities::encode($cust_pers->{"directory_3"} || "");
  my $is_active = $cust_pers->{"is_active"} ? 1 : 0;
  my $is_registered = $cust_pers->{"register_with_building"} ? 1 : 0;
  my $note = HTML::Entities::encode($cust_pers->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <person>
    <salutation>$salutation</salutation>
    <first-name>$first_name</first-name>
    <middle-name>$middle_name</middle-name>
    <last-name>$last_name</last-name>
    <nickname>$nickname</nickname>
    <email>$email</email>
    <work-phone-number>$work_phone_number</work-phone-number>
    <home-phone-number>$home_phone_number</home-phone-number>
    <cell-phone-number>$cell_phone_number</cell-phone-number>
    <directory-1>$directory_1</directory-1>
    <directory-2>$directory_2</directory-2>
    <directory-3>$directory_3</directory-3>
    <is-active>$is_active</is-active>
    <is-registered>$is_registered</is-registered>
    <note>$note</note>
  </person>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_person_success.xslt", $cgi);
}

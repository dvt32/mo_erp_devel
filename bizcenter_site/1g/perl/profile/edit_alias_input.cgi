#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # check if alias id is specified.
  my $alias_id = $cgi->param("id");
  if ((!defined($alias_id)) || ($alias_id !~ /^\d+$/)) {
    die "show_page: edit_alias_not_found.html\n";
  }
  # load alias.
  my $alias = select_row($dbh, "alias_tbl", $alias_id);
  if (!$alias) {
    die "show_page: edit_alias_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $alias->{"account_id"}) {
    die "show_page: edit_alias_not_owner.html\n";
  }
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $name = HTML::Entities::encode($alias->{"name"});
  my $note = HTML::Entities::encode($alias->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <alias>
    <account-name>$account_name</account-name>
    <id>$alias_id</id>
    <name>$name</name>
    <note>$note</note>
  </alias>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_alias_input.xslt", $cgi);
}

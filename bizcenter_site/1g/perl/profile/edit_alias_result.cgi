#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # load alias.
  my $alias_id = $cgi->param("id");
  if ((!defined($alias_id)) || ($alias_id !~ /^\d+$/)) {
    die "show_page: edit_alias_not_found.html\n";
  }
  my $alias = select_row($dbh, "alias_tbl", $alias_id);
  if (!$alias) {
    die "show_page: edit_alias_not_found.html\n";
  }
  if ($cust_acct_id != $alias->{"account_id"}) {
    die "show_page: edit_alias_not_owner.html\n";
  }
  # get the form fields.
  my $name = Encode::decode($cgi_input_charset, $cgi->param("name")) || "";
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # verify that name is not blank.
  if (!$name) {
    die "show_page: edit_alias_bad_name.html\n";
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["name", Encode::encode($db_charset, $name)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "alias_tbl", \@sql_column_list, $alias_id);
  # populate information needed for display of page.
  my $operation = "edit";
  $name = HTML::Entities::encode($name);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>$operation</operation>
  <alias>
    <name>$name</name>
    <note>$note</note>
  </alias>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_alias_result_success.xslt", $cgi);
}

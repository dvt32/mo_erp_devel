<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Bank Account
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="bank-account"/>
	<ol>
	  <li>
	    The nickname is displayed in lists so that you can better identify which bank account to select.
	  </li>
	  <li>
	    The depository name is the name of your bank.
	  </li>
	  <li>
            The bank account name is the name listed as the owner of the bank account (usually this is your name or your company's name).
	  </li>
	  <li>
            Select personal for the bank account ownership if the bank account is listed under your name or another person's name. Select business if the
            bank account is listed under a company's name.
	  </li>
	  <li>
	    The primary bank account is selected by default when you are prompted for a bank account to pay for something.
	  </li>
	</ol>
	<p>
	  <a>
	    <xsl:attribute name="href">/profile/delete_bank_account.cgi?id=<xsl:value-of select="bank-account/id"/></xsl:attribute>
	    <xsl:text>Delete this bank account</xsl:text>
	  </a>
	  or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="bank-account">
    <table>
      <tr><td>
	<form action="/profile/edit_bank_account_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Bank Account for <xsl:value-of select="account_name"/>
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Nickname<sup>1</sup>:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">bank_account_nickname</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="bank_account_nickname"/></xsl:attribute>
		    <xsl:attribute name="maxlength">32</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Depository Name<sup>2</sup>:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">depository_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="depository_name"/></xsl:attribute>
		    <xsl:attribute name="maxlength">48</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Depository Address 1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">address_1</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="address_1"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Depository Address 2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">address_2</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="address_2"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Depository City:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">city</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="city"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Depository State:
		</td>
		<td>
                  <select name="state">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/state-list/state">
		      <option>
			<xsl:if test="/page/bank-account/state = name">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  or Province/Territory:
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">state_other</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="state_other"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Depository Zip Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">zip_code</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="zip_code"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Depository Country:
		</td>
		<td>
                  <select name="country">
		    <xsl:for-each select="/page/country-list/country">
		      <option>
			<xsl:if test="/page/bank-account/country = name">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Name<sup>3</sup>:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">bank_account_customer_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="bank_account_customer_name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Ownership<sup>4</sup>:
		</td>
		<td>
                  <select name="bank_account_ownership_type">
		    <xsl:for-each select="/page/ownership-type-list/ownership-type">
		      <option>
			<xsl:if test="/page/bank-account/bank_account_ownership_type = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Type:
		</td>
		<td>
                  <select name="bank_account_type">
		    <xsl:for-each select="/page/bank-account-type-list/bank-account-type">
		      <option>
			<xsl:if test="/page/bank-account/bank_account_type = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Routing Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">bank_routing_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="bank_routing_number"/></xsl:attribute>
		    <xsl:attribute name="maxlength">9</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bank Account Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">bank_account_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="bank_account_number"/></xsl:attribute>
		    <xsl:attribute name="maxlength">17</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Notes:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">notes</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="notes"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Is Primary Bank Account?<sup>5</sup>
		</td>
		<td>
                  <select name="is_primary">
		    <option>
		      <xsl:if test="is_primary = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>Yes</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="is_primary = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>No</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

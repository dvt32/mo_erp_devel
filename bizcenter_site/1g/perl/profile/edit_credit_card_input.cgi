#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use get_row_list;
use get_month_name;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # check if credit card id is specified.
  my $credit_card_id = $cgi->param("id");
  if ((!defined($credit_card_id)) || ($credit_card_id !~ /^\d+$/)) {
    die "show_page: edit_credit_card_not_found.html\n";
  }
  # load credit card.
  my $credit_card = select_row($dbh, "credit_card_tbl", $credit_card_id);
  if (!$credit_card) {
    die "show_page: edit_credit_card_not_found.html\n";
  }
  if (($cust_acct->{"id"} != $credit_card->{"account_id"}) || ($cust_pers->{"id"} != $credit_card->{"person_id"})) {
    die "show_page: edit_credit_card_not_owner.html\n";
  }
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $card_nickname = HTML::Entities::encode($credit_card->{"card_nickname"} || "");
  my $number_length = length($credit_card->{"card_number"});
  my $card_number = "*" x ($number_length - 4);
  $card_number .= substr($credit_card->{"card_number"}, $number_length - 4);
  my $card_expiration_month = int($credit_card->{"card_expiration_month"});
  my $card_expiration_year = $credit_card->{"card_expiration_year"};
  my $card_security_code = $credit_card->{"card_security_code"};
  if (!defined($card_security_code)) {
    $card_security_code = "";
  }
  my $billing_name = HTML::Entities::encode($credit_card->{"billing_name"});
  my $billing_company = HTML::Entities::encode($credit_card->{"billing_company"} || "");
  my $billing_address_1 = HTML::Entities::encode($credit_card->{"billing_address_1"});
  my $billing_address_2 = HTML::Entities::encode($credit_card->{"billing_address_2"} || "");
  my $billing_city = HTML::Entities::encode($credit_card->{"billing_city"});
  my $billing_state = HTML::Entities::encode($credit_card->{"billing_state"});
  my $billing_zip_code = HTML::Entities::encode($credit_card->{"billing_zip_code"});
  my $billing_country = HTML::Entities::encode($credit_card->{"billing_country"});
  my $billing_address_num = HTML::Entities::encode($credit_card->{"billing_address_num"} || "");
  my $billing_phone_number = HTML::Entities::encode($credit_card->{"billing_phone_number"} || "");
  my $billing_fax_number = HTML::Entities::encode($credit_card->{"billing_fax_number"} || "");
  my $billing_email_address = HTML::Entities::encode($credit_card->{"billing_email_address"} || "");
  my $is_primary = $credit_card->{"is_primary"} ? 1 : 0;
  # generate derived values.
  my $billing_state_other = "";
  if (!grep {$_->{"name"} eq $billing_state} @state_list) {
    $billing_state_other = $billing_state;
    $billing_state = "";
  }
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <credit-card>
    <account_name>$account_name</account_name>
    <id>$credit_card_id</id>
    <card_nickname>$card_nickname</card_nickname>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_state_other>$billing_state_other</billing_state_other>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_address_num>$billing_address_num</billing_address_num>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <is_primary>$is_primary</is_primary>
  </credit-card>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = "(" . lead_pad($i, 2) . ")" . " " . get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <name>$month_name</name>
      <value>$i</value>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    my $abbreviation = HTML::Entities::encode($state->{"abbreviation"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>
      <name>$name</name>
      <abbreviation>$abbreviation</abbreviation>
    </state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    my $abbreviation = HTML::Entities::encode($country->{"abbreviation"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>
      <name>$name</name>
      <abbreviation>$abbreviation</abbreviation>
    </country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_credit_card_input.xslt", $cgi);
}

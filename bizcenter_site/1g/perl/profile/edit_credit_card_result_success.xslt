<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Credit Card
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Credit Card
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="credit-card"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit-card">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following credit card:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new credit card:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Card Nickname:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_nickname"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Card Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Expiration Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_expiration_month"/>
	  <xsl:text>/</xsl:text>
	  <xsl:value-of select="card_expiration_year"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Security Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_security_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Company:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_company"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing State:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Fax Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_fax_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Email Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_email_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Is Primary Card?
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is_primary = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

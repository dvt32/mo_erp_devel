<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Person
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="person"/>
	<p>
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>delete_person.cgi?id=</xsl:text>
	      <xsl:value-of select="person/id"/>
	    </xsl:attribute>
	    <xsl:text>Delete</xsl:text>
	  </a>
	  this person or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="person">
    <table>
      <tr><td>
	<form action="/profile/edit_person_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Person for <xsl:value-of select="account-name"/>
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Salutation:
		</td>
		<td>
                  <select name="salutation">
		    <option>
		      <xsl:if test="salutation = 'Mr.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Mr.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Ms.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Ms.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Mrs.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Mrs.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Miss'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Miss</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  First Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">first_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="first-name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Middle Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">middle_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="middle-name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Last Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">last_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="last-name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Nickname:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">nickname</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="nickname"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="email"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Work Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">work_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="work-phone-number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Home Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">home_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="home-phone-number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Cell Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">cell_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="cell-phone-number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Other Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">other_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="other-phone-number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Directory 1:
		</td>
		<td>
                  <select name="directory_1">
		    <option value="">
		      <xsl:if test="directory-1 = ''">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>(none)</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-1 = 'work'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>work</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-1 = 'home'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>home</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-1 = 'cell'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>cell</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-1 = 'other'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>other</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Directory 2:
		</td>
		<td>
                  <select name="directory_2">
		    <option value="">
		      <xsl:if test="directory-2 = ''">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>(none)</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-2 = 'work'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>work</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-2 = 'home'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>home</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-2 = 'cell'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>cell</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-2 = 'other'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>other</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Directory 3:
		</td>
		<td>
                  <select name="directory_3">
		    <option value="">
		      <xsl:if test="directory-3 = ''">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>(none)</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-3 = 'work'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>work</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-3 = 'home'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>home</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-3 = 'cell'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>cell</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="directory-3 = 'other'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>other</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Is Active?:
		</td>
		<td>
                  <select name="is_active">
		    <option>
		      <xsl:if test="is-active = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>Yes</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="is-active = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>No</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Is Registered?:
		</td>
		<td>
                  <select name="is_registered">
		    <option>
		      <xsl:if test="is-registered = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>Yes</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="is-registered = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>No</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

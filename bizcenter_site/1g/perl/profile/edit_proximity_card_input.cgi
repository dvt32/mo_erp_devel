#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use get_row_by_id_map;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # check if proximity card id is specified.
  my $proximity_card_id = $cgi->param("id");
  if ((!defined($proximity_card_id)) || ($proximity_card_id !~ /^\d+$/)) {
    die "show_page: edit_proximity_card_not_found.html\n";
  }
  # load proximity card.
  my $proximity_card = select_row($dbh, "proximity_card_tbl", $proximity_card_id);
  if (!$proximity_card) {
    die "show_page: edit_proximity_card_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $proximity_card->{"account_id"}) {
    die "show_page: edit_proximity_card_not_owner.html\n";
  }
  # fetch access levels
  my %access_level_by_id;
  get_row_by_id_map($dbh, "select * from access_level_tbl", undef, \%access_level_by_id);
  # fetch all cust_pers rows belonging to the cust_acct.
  my @cust_pers_list;
  my $sql_query = "select * from cust_pers_tbl where cust_acct_id = ? order by last_name, first_name, middle_name";
  my @sql_param_list = ([$cust_acct_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@cust_pers_list);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $number = HTML::Entities::encode($proximity_card->{"number"});
  my $status = HTML::Entities::encode($proximity_card->{"status"});
  my $access_level_id = $proximity_card->{"access_level_id"};
  my $access_level_name = "";
  if (defined($access_level_id) && exists($access_level_by_id{$access_level_id})) {
    $access_level_name = HTML::Entities::encode($access_level_by_id{$access_level_id}->{"name"});
  }
  my $cust_pers_id = defined($proximity_card->{"person_id"}) ? $proximity_card->{"person_id"} : "";
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <status-list>
    <status>active</status>
    <status>lost</status>
    <status>inactive</status>
  </status-list>
  <person-list>
---END_BLOCK---;
  for my $cust_pers (@cust_pers_list) {
    my $loop_cust_pers_id = defined($cust_pers->{"id"}) ? $cust_pers->{"id"} : "";
    my $person_name = HTML::Entities::encode($cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"});
  $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$loop_cust_pers_id</id>
      <name>$person_name</name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
  <proximity-card>
    <account-name>$account_name</account-name>
    <id>$proximity_card_id</id>
    <number>$number</number>
    <status>$status</status>
    <access-level>$access_level_name</access-level>
    <person-id>$cust_pers_id</person-id>
  </proximity-card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_proximity_card_input.xslt", $cgi);
}

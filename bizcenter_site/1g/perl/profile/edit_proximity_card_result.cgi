#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use update_with_statement;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  my $proximity_card_id = $cgi->param("id");
  if ((!defined($proximity_card_id)) || ($proximity_card_id !~ /^\d+$/)) {
    die "show_page: edit_proximity_card_not_found.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  my $proximity_card = select_row($dbh, "proximity_card_tbl", $proximity_card_id);
  if (!$proximity_card) {
    $dbh->rollback();
    die "show_page: edit_proximity_card_not_found.html\n";
  }
  if ((!defined($proximity_card->{"account_id"})) || ($cust_acct->{"id"} != $proximity_card->{"account_id"})) {
    $dbh->rollback();
    die "show_page: edit_proximity_card_not_owner.html\n";
  }
  # get the form fields.
  my $status = $cgi->param("status") || "";
  my $person_id = $cgi->param("person_id") || "";
  # verify that status is a valid choice..
  if ($status !~ /^(active|lost|inactive)$/) {
    $dbh->rollback();
    die "show_page: edit_proximity_card_bad_status.html\n";
  }
  my $person_name = "";
  if ($person_id eq "") {
    $person_id = undef;
  } else {
    if ($person_id =~ /^\d+$/) {
      # verify that the person_id is a person belonging to the cust_acct.
      my $cust_pers = select_row($dbh, "cust_pers_tbl", $person_id);
      if ((!$cust_pers) || ($cust_pers->{"cust_acct_id"} != $cust_acct_id)) {
	$dbh->rollback();
	die "show_page: edit_proximity_card_bad_person.html\n";
      }
      # need person_name for display of result page.
      $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    } else {
      $dbh->rollback();
      die "show_page: edit_proximity_card_bad_person.html\n";
    }
  }
  my $sql_stmt = "update proximity_card_tbl set status = ?, person_id = ? where id = ? and account_id = ?";
  my @sql_param_list;
  push(@sql_param_list, $status);
  push(@sql_param_list, [$person_id, SQL_INTEGER]);
  push(@sql_param_list, [$proximity_card_id, SQL_INTEGER]);
  push(@sql_param_list, [$cust_acct_id, SQL_INTEGER]);
  update_with_statement($dbh, $sql_stmt, \@sql_param_list);
  # commit the transaction.
  $dbh->commit();
  # fetch the database information we need for display.
  my $access_level_name = "";
  if (defined($proximity_card->{"access_level_id"})) {
    if (my $row = select_row($dbh, "access_level_tbl", $proximity_card->{"access_level_id"})) {
      $access_level_name = $row->{"name"};
    }
  }
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $number = HTML::Entities::encode($proximity_card->{"number"});
  $person_name = HTML::Entities::encode($person_name);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <proximity-card>
    <account-name>$account_name</account-name>
    <number>$number</number>
    <status>$status</status>
    <access-level>$access_level_name</access-level>
    <person>$person_name</person>
  </proximity-card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_proximity_card_result_success.xslt", $cgi);
}

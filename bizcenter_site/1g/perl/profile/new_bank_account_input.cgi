#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_list;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account>
    <name>$account_name</name>
  </account>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    my $abbreviation = HTML::Entities::encode($state->{"abbreviation"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>
      <name>$name</name>
      <abbreviation>$abbreviation</abbreviation>
    </state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <bank-account-type-list>
    <bank-account-type>checking</bank-account-type>
    <bank-account-type>savings</bank-account-type>
    <bank-account-type>money market</bank-account-type>
  </bank-account-type-list>
  <ownership-type-list>
    <ownership-type>personal</ownership-type>
    <ownership-type>business</ownership-type>
  </ownership-type-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    my $abbreviation = HTML::Entities::encode($country->{"abbreviation"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>
      <name>$name</name>
      <abbreviation>$abbreviation</abbreviation>
    </country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_bank_account_input.xslt", $cgi);
}

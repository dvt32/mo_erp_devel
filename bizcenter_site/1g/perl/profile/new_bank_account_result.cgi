#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use lead_pad;
use insert_row;
use update_with_statement;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $bank_account_nickname = Encode::decode($cgi_input_charset, $cgi->param("bank_account_nickname")) || "";
  my $depository_name = Encode::decode($cgi_input_charset, $cgi->param("depository_name")) || "";
  my $address_1 = Encode::decode($cgi_input_charset, $cgi->param("address_1")) || "";
  my $address_2 = Encode::decode($cgi_input_charset, $cgi->param("address_2")) || "";
  my $city = Encode::decode($cgi_input_charset, $cgi->param("city")) || "";
  my $state = Encode::decode($cgi_input_charset, $cgi->param("state")) || "";
  my $state_other = Encode::decode($cgi_input_charset, $cgi->param("state_other")) || "";
  my $zip_code = Encode::decode($cgi_input_charset, $cgi->param("zip_code")) || "";
  my $country = Encode::decode($cgi_input_charset, $cgi->param("country")) || "";
  my $bank_account_customer_name = Encode::decode($cgi_input_charset, $cgi->param("bank_account_customer_name")) || "";
  my $bank_account_ownership_type = Encode::decode($cgi_input_charset, $cgi->param("bank_account_ownership_type")) || "";
  my $bank_account_type = Encode::decode($cgi_input_charset, $cgi->param("bank_account_type")) || "";
  my $bank_routing_number = $cgi->param("bank_routing_number") || "";
  my $bank_account_number = $cgi->param("bank_account_number") || "";
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes")) || "";
  my $is_primary = $cgi->param("is_primary") || 0;
  # verify that depository name exists.
  if (!$depository_name) {
    die "show_page: edit_bank_account_missing_depository_name.html\n";
  }
  # verify that address 1 is not blank.
  if (!$address_1) {
    die "show_page: edit_bank_account_missing_address.html\n";
  }
  # verify that city is not blank.
  if (!$city) {
    die "show_page: edit_bank_account_missing_city.html\n";
  }
  # verify that state/province/territory is not blank and that only one is specified.
  if ($state && $state_other) {
    die "show_page: edit_bank_account_bad_state.html\n";
  } elsif (!$state && !$state_other) {
    die "show_page: edit_bank_account_missing_state.html\n";
  } elsif (!$state) {
    $state = $state_other;
  }
  # verify that zip code is not blank.
  if (!$zip_code) {
    die "show_page: edit_bank_account_missing_zip_code.html\n";
  }
  # verify that country is not blank.
  if (!$country) {
    die "show_page: edit_bank_account_missing_country.html\n";
  }
  # verify that customer name is not blank.
  if (!$bank_account_customer_name) {
    die "show_page: edit_bank_account_missing_customer_name.html\n";
  }
  # verify that ownership type is valid.
  if ($bank_account_ownership_type !~ /^personal|business$/) {
    die "show_page: edit_bank_account_bad_ownership_type.html\n";
  }
  # verify that account type is valid.
  if ($bank_account_type !~ /^checking|savings|money market$/) {
    die "show_page: edit_bank_account_bad_account_type.html\n";
  }
  # verify that the new bank routing number is valid.
  if ($bank_routing_number !~ /^\d{9}$/) {
    die "show_page: edit_bank_account_bad_bank_routing_number.html\n";
  }
  # verify that the new bank account number is valid.
  if ($bank_account_number !~ /^\d+$/) {
    die "show_page: edit_bank_account_bad_bank_account_number.html\n";
  }
  # begin database transaction for atomicity.
  $dbh->begin_work();
  # if bank account marked as primary, remove primary flag from other bank accounts.
  if ($is_primary) {
    my $sql_stmt = "update bank_account_tbl set is_primary = false where account_id = ?";
    my @sql_param_list = ([$cust_acct_id, SQL_INTEGER]);
    update_with_statement($dbh, $sql_stmt, \@sql_param_list);
  }
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["bank_account_nickname", Encode::encode($db_charset, $bank_account_nickname)]);
  push(@sql_column_list, ["depository_name", Encode::encode($db_charset, $depository_name)]);
  push(@sql_column_list, ["address_1", Encode::encode($db_charset, $address_1)]);
  push(@sql_column_list, ["address_2", Encode::encode($db_charset, $address_2)]);
  push(@sql_column_list, ["city", Encode::encode($db_charset, $city)]);
  push(@sql_column_list, ["state", Encode::encode($db_charset, $state)]);
  push(@sql_column_list, ["zip_code", Encode::encode($db_charset, $zip_code)]);
  push(@sql_column_list, ["country", Encode::encode($db_charset, $country)]);
  push(@sql_column_list, ["bank_account_customer_name", Encode::encode($db_charset, $bank_account_customer_name)]);
  push(@sql_column_list, ["bank_account_ownership_type", Encode::encode($db_charset, $bank_account_ownership_type)]);
  push(@sql_column_list, ["bank_account_type", Encode::encode($db_charset, $bank_account_type)]);
  push(@sql_column_list, ["bank_routing_number", $bank_routing_number]);
  push(@sql_column_list, ["bank_account_number", $bank_account_number]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  push(@sql_column_list, ["is_primary", $is_primary, SQL_BOOLEAN]);
  insert_row($dbh, "bank_account_tbl", \@sql_column_list);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $bank_account_nickname = HTML::Entities::encode($bank_account_nickname);
  $depository_name = HTML::Entities::encode($depository_name);
  $address_1 = HTML::Entities::encode($address_1);
  $address_2 = HTML::Entities::encode($address_2);
  $city = HTML::Entities::encode($city);
  $state = HTML::Entities::encode($state);
  $zip_code = HTML::Entities::encode($zip_code);
  $country = HTML::Entities::encode($country);
  $bank_account_customer_name = HTML::Entities::encode($bank_account_customer_name);
  $bank_account_ownership_type = HTML::Entities::encode($bank_account_ownership_type);
  $bank_account_type = HTML::Entities::encode($bank_account_type);
  $bank_routing_number = HTML::Entities::encode($bank_routing_number);
  $bank_account_number = HTML::Entities::encode($bank_account_number);
  $notes = HTML::Entities::encode($notes);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <bank-account>
    <bank_account_nickname>$bank_account_nickname</bank_account_nickname>
    <depository_name>$depository_name</depository_name>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <bank_account_customer_name>$bank_account_customer_name</bank_account_customer_name>
    <bank_account_ownership_type>$bank_account_ownership_type</bank_account_ownership_type>
    <bank_account_type>$bank_account_type</bank_account_type>
    <bank_routing_number>$bank_routing_number</bank_routing_number>
    <bank_account_number>$bank_account_number</bank_account_number>
    <notes>$notes</notes>
    <is_primary>$is_primary</is_primary>
  </bank-account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_bank_account_result_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use lead_pad;
use update_with_statement;
use insert_row;
use send_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $card_nickname = Encode::decode($cgi_input_charset, $cgi->param("card_nickname")) || "";
  my $card_number = $cgi->param("card_number") || "";
  my $card_expiration_month = $cgi->param("card_expiration_month") || "";
  my $card_expiration_year = $cgi->param("card_expiration_year") || "";
  my $card_security_code = $cgi->param("card_security_code") || "";
  my $billing_name = Encode::decode($cgi_input_charset, $cgi->param("billing_name")) || "";
  my $billing_company = Encode::decode($cgi_input_charset, $cgi->param("billing_company")) || "";
  my $billing_address_1 = Encode::decode($cgi_input_charset, $cgi->param("billing_address_1")) || "";
  my $billing_address_2 = Encode::decode($cgi_input_charset, $cgi->param("billing_address_2")) || "";
  my $billing_city = Encode::decode($cgi_input_charset, $cgi->param("billing_city")) || "";
  my $billing_state = Encode::decode($cgi_input_charset, $cgi->param("billing_state")) || "";
  my $billing_state_other = Encode::decode($cgi_input_charset, $cgi->param("billing_state_other")) || "";
  my $billing_zip_code = Encode::decode($cgi_input_charset, $cgi->param("billing_zip_code")) || "";
  my $billing_country = Encode::decode($cgi_input_charset, $cgi->param("billing_country")) || "";
  my $billing_address_num = Encode::decode($cgi_input_charset, $cgi->param("billing_address_num")) || "";
  my $billing_phone_number = Encode::decode($cgi_input_charset, $cgi->param("billing_phone_number")) || "";
  my $billing_fax_number = Encode::decode($cgi_input_charset, $cgi->param("billing_fax_number")) || "";
  my $billing_email_address = Encode::decode($cgi_input_charset, $cgi->param("billing_email_address")) || "";
  my $is_primary = $cgi->param("is_primary") || 0;
  # verify that the new card number is valid.
  if ($card_number !~ /^\d{15,16}$/) {
    die "show_page: edit_credit_card_bad_card_number.html\n";
  }
  # verify that expiration month is valid.
  if (($card_expiration_month !~ /^\d+$/) || ($card_expiration_month < 1) || ($card_expiration_month > 12)) {
    die "show_page: edit_credit_card_bad_expiration_month.html\n";
  } else {
    $card_expiration_month = lead_pad(int($card_expiration_month), 2);
  }
  # verify that expiration year is valid.
  if ($card_expiration_year !~ /^\d{4}$/) {
    die "show_page: edit_credit_card_bad_expiration_year.html\n";
  }
  # verify that security code is either blank or 3 or 4 digits.
  if (($card_security_code ne "") && ($card_security_code !~ /^\d{3,4}$/)) {
    die "show_page: edit_credit_card_bad_security_code.html\n";
  }
  # verify that billing name exists.
  if (!$billing_name) {
    die "show_page: edit_credit_card_missing_billing_name.html\n";
  }
  # verify that address 1 is not blank.
  if (!$billing_address_1) {
    die "show_page: edit_credit_card_missing_address.html\n";
  }
  # verify that city is not blank.
  if (!$billing_city) {
    die "show_page: edit_credit_card_missing_city.html\n";
  }
  # verify that state/province/territory is not blank and that only one is specified.
  if ($billing_state && $billing_state_other) {
    die "show_page: edit_credit_card_bad_state.html\n";
  } elsif (!$billing_state && !$billing_state_other) {
    die "show_page: edit_credit_card_missing_state.html\n";
  } elsif (!$billing_state) {
    $billing_state = $billing_state_other;
  }
  # verify that zip code is not blank.
  if (!$billing_zip_code) {
    die "show_page: edit_credit_card_missing_zip_code.html\n";
  }
  # verify that country is not blank.
  if (!$billing_country) {
    die "show_page: edit_credit_card_missing_country.html\n";
  }
  # verify that billing address number is valid.
  if (($billing_address_num) && ($billing_address_num !~ /^\d+$/)) {
    die "show_page: edit_credit_card_bad_billing_address_num.html\n";
  }
  # begin database transaction for atomicity.
  $dbh->begin_work();
  # if card marked as primary, remove primary flag from other credit cards linked to same account.
  if ($is_primary) {
    my $sql_stmt = "update credit_card_tbl set is_primary = false where account_id = ?";
    update_with_statement($dbh, $sql_stmt, [[$cust_acct_id, SQL_INTEGER]]);
  }
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $cust_pers->{"id"}, SQL_INTEGER]);
  push(@sql_column_list, ["card_nickname", Encode::encode($db_charset, $card_nickname)]);
  push(@sql_column_list, ["card_number", $card_number]);
  push(@sql_column_list, ["card_expiration_month", $card_expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $card_expiration_year]);
  push(@sql_column_list, ["card_security_code", $card_security_code]);
  push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $billing_name)]);
  push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $billing_company)]);
  push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $billing_address_1)]);
  push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $billing_address_2)]);
  push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $billing_city)]);
  push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $billing_state)]);
  push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
  push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $billing_country)]);
  push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $billing_address_num)]);
  push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $billing_phone_number)]);
  push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $billing_fax_number)]);
  push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $billing_email_address)]);
  push(@sql_column_list, ["is_primary", $is_primary, SQL_BOOLEAN]);
  insert_row($dbh, "credit_card_tbl", \@sql_column_list);
  # commit the transaction.
  $dbh->commit();
  # email accounts_receivable with new credit card notification.
  my $email_message = "";
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  my $account_name = $cust_acct->{"name"} || "";
  my $person_name = $cust_pers->{"first_name"} . " " . $cust_pers->{"last_name"};
  my $card_length = length($card_number);
  my $obscured_card_number = "";
  $obscured_card_number .= substr($card_number, 0, 3);
  $obscured_card_number .= "*" x ($card_length - 6);
  $obscured_card_number .= substr($card_number, $card_length - 3);
  $email_message .= <<"---END_BLOCK---;";
The following new card was added by $person_name ($account_name):

card_nickname: $card_nickname
card_number: $obscured_card_number
card_expiration_month: $card_expiration_month
card_expiration_year: $card_expiration_year
billing_name: $billing_name
billing_company: $billing_company
billing_address_1: $billing_address_1
billing_address_2: $billing_address_2
billing_city: $billing_city
billing_state: $billing_state
billing_zip_code: $billing_zip_code
billing_country: $billing_country

Please update QuickBooks and the operations site recurring transactions.
---END_BLOCK---;
  send_email
    ("from" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
     "to" => "accounts_receivable\@microoffice.com",
     "subject" => "New credit card from $person_name ($account_name)",
     "message" => $email_message);
  # populate information needed for display of page.
  $card_nickname = HTML::Entities::encode($card_nickname);
  $billing_name = HTML::Entities::encode($billing_name);
  $billing_company = HTML::Entities::encode($billing_company);
  $billing_address_1 = HTML::Entities::encode($billing_address_1);
  $billing_address_2 = HTML::Entities::encode($billing_address_2);
  $billing_city = HTML::Entities::encode($billing_city);
  $billing_state = HTML::Entities::encode($billing_state);
  $billing_zip_code = HTML::Entities::encode($billing_zip_code);
  $billing_country = HTML::Entities::encode($billing_country);
  $billing_address_num = HTML::Entities::encode($billing_address_num);
  $billing_phone_number = HTML::Entities::encode($billing_phone_number);
  $billing_fax_number = HTML::Entities::encode($billing_fax_number);
  $billing_email_address = HTML::Entities::encode($billing_email_address);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <credit-card>
    <card_nickname>$card_nickname</card_nickname>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_address_num>$billing_address_num</billing_address_num>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <is_primary>$is_primary</is_primary>
  </credit-card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_credit_card_result_success.xslt", $cgi);
}

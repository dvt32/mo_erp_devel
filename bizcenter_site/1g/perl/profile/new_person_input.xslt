<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: New Person
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="account"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account">
    <table>
      <tr><td>
	<form action="/profile/new_person_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Person for <xsl:value-of select="name"/>
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Salutation:
		</td>
		<td>
                  <select name="salutation">
		    <option>
		      <xsl:text>Mr.</xsl:text>
		    </option>
		    <option>
		      <xsl:text>Ms.</xsl:text>
		    </option>
		    <option>
		      <xsl:text>Mrs.</xsl:text>
		    </option>
		    <option>
		      <xsl:text>Miss</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  First Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">first_name</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Middle Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">middle_name</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Last Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">last_name</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Nickname:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">nickname</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Work Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">work_phone_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Home Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">home_phone_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Cell Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">cell_phone_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Other Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">other_phone_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Directory 1:
		</td>
		<td>
                  <select name="directory_1">
		    <option value="">
		      <xsl:text>(none)</xsl:text>
		    </option>
		    <option>
		      <xsl:text>work</xsl:text>
		    </option>
		    <option>
		      <xsl:text>home</xsl:text>
		    </option>
		    <option>
		      <xsl:text>cell</xsl:text>
		    </option>
		    <option>
		      <xsl:text>other</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Directory 2:
		</td>
		<td>
                  <select name="directory_2">
		    <option value="">
		      <xsl:text>(none)</xsl:text>
		    </option>
		    <option>
		      <xsl:text>work</xsl:text>
		    </option>
		    <option>
		      <xsl:text>home</xsl:text>
		    </option>
		    <option>
		      <xsl:text>cell</xsl:text>
		    </option>
		    <option>
		      <xsl:text>other</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Directory 3:
		</td>
		<td>
                  <select name="directory_3">
		    <option value="">
		      <xsl:text>(none)</xsl:text>
		    </option>
		    <option>
		      <xsl:text>work</xsl:text>
		    </option>
		    <option>
		      <xsl:text>home</xsl:text>
		    </option>
		    <option>
		      <xsl:text>cell</xsl:text>
		    </option>
		    <option>
		      <xsl:text>other</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Is Active?:
		</td>
		<td>
                  <select name="is_active">
		    <option>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>Yes</xsl:text>
		    </option>
		    <option>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>No</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Is Registered?:
		</td>
		<td>
                  <select name="is_registered">
		    <option>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>Yes</xsl:text>
		    </option>
		    <option>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>No</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

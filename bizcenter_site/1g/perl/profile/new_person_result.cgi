#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row_with_query;
use insert_row;
use strip_outside_whitespace;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $salutation = Encode::decode($cgi_input_charset, $cgi->param("salutation")) || "";
  my $first_name = Encode::decode($cgi_input_charset, $cgi->param("first_name")) || "";
  my $middle_name = Encode::decode($cgi_input_charset, $cgi->param("middle_name")) || "";
  my $last_name = Encode::decode($cgi_input_charset, $cgi->param("last_name")) || "";
  my $nickname = Encode::decode($cgi_input_charset, $cgi->param("nickname")) || "";
  my $email = lc(strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("email")) || ""));
  my $work_phone_number = Encode::decode($cgi_input_charset, $cgi->param("work_phone_number")) || "";
  my $home_phone_number = Encode::decode($cgi_input_charset, $cgi->param("home_phone_number")) || "";
  my $cell_phone_number = Encode::decode($cgi_input_charset, $cgi->param("cell_phone_number")) || "";
  my $other_phone_number = Encode::decode($cgi_input_charset, $cgi->param("other_phone_number")) || "";
  my $directory_1 = Encode::decode($cgi_input_charset, $cgi->param("directory_1")) || "";
  my $directory_2 = Encode::decode($cgi_input_charset, $cgi->param("directory_2")) || "";
  my $directory_3 = Encode::decode($cgi_input_charset, $cgi->param("directory_3")) || "";
  my $is_active = $cgi->param("is_active") || 0;
  my $is_registered = $cgi->param("is_registered") || 0;
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # verify that name is not blank.
  if ((!$first_name) || (!$last_name)) {
    die "show_page: edit_person_missing_name.html\n";
  }
  # verify that salutation is not blank and is valid.
  if ($salutation !~ /^(Mr\.|Ms\.|Mrs\.|Miss)$/) {
    die "show_page: edit_person_missing_salutation.html\n";
  }
  # verify that directory_x is valid.
  if (($directory_1 ne "") && ($directory_1 !~ /^(work|home|cell|other)$/)) {
    die "show_page: edit_person_bad_directory_option.html\n";
  }
  if (($directory_2 ne "") && ($directory_2 !~ /^(work|home|cell|other)$/)) {
    die "show_page: edit_person_bad_directory_option.html\n";
  }
  if (($directory_3 ne "") && ($directory_3 !~ /^(work|home|cell|other)$/)) {
    die "show_page: edit_person_bad_directory_option.html\n";
  }
  # begin transaction.
  $dbh->begin_work();
  # check no row with a duplicate email.
  if ($email) {
    my $sql_query = "select * from person_vw where email = ?";
    if (select_row_with_query($dbh, $sql_query, [$email])) {
      $dbh->rollback();
      die "show_page: edit_person_duplicate_email.html\n";
    }
  }
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["salutation", Encode::encode($db_charset, $salutation)]);
  push(@sql_column_list, ["first_name", Encode::encode($db_charset, $first_name)]);
  push(@sql_column_list, ["middle_name", Encode::encode($db_charset, $middle_name)]);
  push(@sql_column_list, ["last_name", Encode::encode($db_charset, $last_name)]);
  push(@sql_column_list, ["nickname", Encode::encode($db_charset, $nickname)]);
  if ($email) {
    push(@sql_column_list, ["email", Encode::encode($db_charset, $email)]);
  }
  push(@sql_column_list, ["work_phone_number", Encode::encode($db_charset, $work_phone_number)]);
  push(@sql_column_list, ["home_phone_number", Encode::encode($db_charset, $home_phone_number)]);
  push(@sql_column_list, ["cell_phone_number", Encode::encode($db_charset, $cell_phone_number)]);
  push(@sql_column_list, ["other_phone_number", Encode::encode($db_charset, $other_phone_number)]);
  push(@sql_column_list, ["directory_1", Encode::encode($db_charset, $directory_1)]);
  push(@sql_column_list, ["directory_2", Encode::encode($db_charset, $directory_2)]);
  push(@sql_column_list, ["directory_3", Encode::encode($db_charset, $directory_3)]);
  push(@sql_column_list, ["is_active", $is_active, SQL_BOOLEAN]);
  push(@sql_column_list, ["register_with_building", $is_registered, SQL_BOOLEAN]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  insert_row($dbh, "cust_pers_tbl", \@sql_column_list);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $salutation = HTML::Entities::encode($salutation);
  $first_name = HTML::Entities::encode($first_name);
  $middle_name = HTML::Entities::encode($middle_name);
  $last_name = HTML::Entities::encode($last_name);
  $nickname = HTML::Entities::encode($nickname);
  $email = HTML::Entities::encode($email);
  $work_phone_number = HTML::Entities::encode($work_phone_number);
  $home_phone_number = HTML::Entities::encode($home_phone_number);
  $cell_phone_number = HTML::Entities::encode($cell_phone_number);
  $other_phone_number = HTML::Entities::encode($other_phone_number);
  $directory_1 = HTML::Entities::encode($directory_1);
  $directory_2 = HTML::Entities::encode($directory_2);
  $directory_3 = HTML::Entities::encode($directory_3);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <person>
    <salutation>$salutation</salutation>
    <first-name>$first_name</first-name>
    <middle-name>$middle_name</middle-name>
    <last-name>$last_name</last-name>
    <nickname>$nickname</nickname>
    <email>$email</email>
    <work-phone-number>$work_phone_number</work-phone-number>
    <home-phone-number>$home_phone_number</home-phone-number>
    <cell-phone-number>$cell_phone_number</cell-phone-number>
    <other-phone-number>$other_phone_number</other-phone-number>
    <directory-1>$directory_1</directory-1>
    <directory-2>$directory_2</directory-2>
    <directory-3>$directory_3</directory-3>
    <is-active>$is_active</is-active>
    <is-registered>$is_registered</is-registered>
    <note>$note</note>
  </person>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_person_result_success.xslt", $cgi);
}

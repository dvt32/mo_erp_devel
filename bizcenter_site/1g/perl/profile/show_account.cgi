#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $primary_contact_cust_pers_id = $cust_acct->{"primary_contact_cust_pers_id"};
  # fetch cust_pers that is listed as the primary contact
  my $cust_pers = select_row($dbh, "cust_pers_tbl", $primary_contact_cust_pers_id);
  if (!$cust_pers) {
    die "bad cust_pers_id: $primary_contact_cust_pers_id";
  }
  # populate information for display of page.
  my $name = HTML::Entities::encode($cust_acct->{"name"});
  my $primary_contact_person_name = HTML::Entities::encode($cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"});
  my $is_visible = $cust_acct->{"visible"} ? 1 : 0;
  my $external_name = HTML::Entities::encode($cust_acct->{"external_name"} || "");
  my $description = HTML::Entities::encode($cust_acct->{"description"} || "");
  my $web_site = HTML::Entities::encode($cust_acct->{"web_site"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account>
    <name>$name</name>
    <primary-contact-person-name>$primary_contact_person_name</primary-contact-person-name>
    <is-visible>$is_visible</is-visible>
    <external-name>$external_name</external-name>
    <description>$description</description>
    <web-site>$web_site</web-site>
  </account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_account.xslt", $cgi);
}

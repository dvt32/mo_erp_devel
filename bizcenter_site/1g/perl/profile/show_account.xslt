<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Account Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="account"/>
	<p>
	  The "Is visible" field specifies whether your account name,
	  description, and web site URL show up on the Micro Office
	  tenants page at http://www.microoffice.com/tenants.html
	  (shown <a
	  href="http://www.microoffice.com/tenants.html">here</a>). The
	  "External Name", "Description", and "Web Site URL" fields
	  specify the name, description, and URL that show up on the
	  Micro Office tenants page. You may choose to hide your name
	  by emailing service@microoffice.com and specifying that you
	  would like your name removed.
	</p>
	<p>
	  To change any of the above values, please email
	  service@microoffice.com and specify the change you would
	  like.
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Account Name
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Primary Contact Person
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="primary-contact-person-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Is visible on Micro Office web site?
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is-visible = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  External Name on Micro Office web site
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="external-name"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key">
	  Description
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="description"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Web Site URL
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="web-site"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

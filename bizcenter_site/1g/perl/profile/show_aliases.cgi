#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # fetch aliases linked to cust_acct
  my @alias_list;
  my $sql_query = "select * from alias_tbl where account_id = ? order by name";
  get_row_list($dbh, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \@alias_list);
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <alias-list>
---END_BLOCK---;
  for my $alias (@alias_list) {
    my $id = $alias->{"id"};
    my $name = HTML::Entities::encode($alias->{"name"});
    my $note = HTML::Entities::encode($alias->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <alias>
      <id>$id</id>
      <name>$name</name>
      <note>$note</note>
    </alias>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </alias-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_aliases.xslt", $cgi);
}

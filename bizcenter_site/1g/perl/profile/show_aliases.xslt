<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Aliases
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="alias-list"/>
	<p>
	  Micro Office uses aliases to identify your account for delivering mail and packages and for directing visitors and
	  callers. The aliases are also published in a directory at the front desk for visitors and messengers. To edit or
	  delete an alias, click on the name.
	</p>
	<p>
	  You can create a <a href="new_alias_input.cgi">new alias</a> or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="alias-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="3" class="standard_table_title">
	  Aliases
	  (<xsl:value-of select="count(alias)"/> total)
	  for <xsl:value-of select="../account-name"/>
	</td>
      </tr>
      <tr>
	<td class="standard_table_column_header">
	  Name
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Note
	</td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(alias) > 0">
	  <xsl:apply-templates select="alias"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="3" class="standard_table_body">
	      <div style="text-align: center">You have no aliases.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="alias">
    <tr valign="top">
      <td class="standard_table_body">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>edit_alias_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="name"/>
	</a>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="note"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

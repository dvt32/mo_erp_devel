#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # fetch bank accounts linked to cust_acct
  my (%bank_account_by_id, @bank_account_id_list);
  my $sql_query = "select * from bank_account_tbl where account_id = ? order by depository_name, bank_account_number";
  my @sql_param_list = ([$cust_acct_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%bank_account_by_id, \@bank_account_id_list);
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <bank-account-list>
---END_BLOCK---;
  for my $bank_account_id (@bank_account_id_list) {
    my $bank_account = $bank_account_by_id{$bank_account_id};
    my $depository_name = HTML::Entities::encode($bank_account->{"depository_name"});
    my $address_1 = HTML::Entities::encode($bank_account->{"address_1"});
    my $address_2 = HTML::Entities::encode($bank_account->{"address_2"} || "");
    my $city = HTML::Entities::encode($bank_account->{"city"});
    my $state = HTML::Entities::encode($bank_account->{"state"});
    my $zip_code = HTML::Entities::encode($bank_account->{"zip_code"});
    my $country = HTML::Entities::encode($bank_account->{"country"});
    my $bank_account_type = HTML::Entities::encode($bank_account->{"bank_account_type"});
    my $number_length;
    $number_length = length($bank_account->{"bank_routing_number"});
    my $bank_routing_number = "*" x ($number_length - 4);
    $bank_routing_number .= substr($bank_account->{"bank_routing_number"}, $number_length - 4);
    $number_length = length($bank_account->{"bank_account_number"});
    my $bank_account_number = "*" x ($number_length - 4);
    $bank_account_number .= substr($bank_account->{"bank_account_number"}, $number_length - 4);
    my $notes = HTML::Entities::encode($bank_account->{"notes"} || "");
    my $bank_account_customer_name = HTML::Entities::encode($bank_account->{"bank_account_customer_name"});
    my $bank_account_ownership_type = HTML::Entities::encode($bank_account->{"bank_account_ownership_type"});
    my $bank_account_nickname = HTML::Entities::encode($bank_account->{"bank_account_nickname"} || "");
    my $is_primary = $bank_account->{"is_primary"} ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
    <bank-account>
      <id>$bank_account_id</id>
      <depository_name>$depository_name</depository_name>
      <address_1>$address_1</address_1>
      <address_2>$address_2</address_2>
      <city>$city</city>
      <state>$state</state>
      <zip_code>$zip_code</zip_code>
      <country>$country</country>
      <bank_account_type>$bank_account_type</bank_account_type>
      <bank_routing_number>$bank_routing_number</bank_routing_number>
      <bank_account_number>$bank_account_number</bank_account_number>
      <notes>$notes</notes>
      <bank_account_customer_name>$bank_account_customer_name</bank_account_customer_name>
      <bank_account_ownership_type>$bank_account_ownership_type</bank_account_ownership_type>
      <bank_account_nickname>$bank_account_nickname</bank_account_nickname>
      <is_primary>$is_primary</is_primary>
    </bank-account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </bank-account-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_bank_accounts.xslt", $cgi);
}

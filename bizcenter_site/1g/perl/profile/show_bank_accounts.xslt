<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Bank Accounts
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="bank-account-list"/>
	<p>
	  You can register a <a href="new_bank_account_input.cgi">new bank account</a> or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="bank-account-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="11" class="standard_table_title">
	  Bank Accounts
	  for <xsl:value-of select="../account-name"/>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Depository Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account Type</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Nickname</td>
	<td class="standard_table_spacer_column"></td>
	<td>Notes</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(bank-account) > 0">
	  <xsl:apply-templates select="bank-account"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="11" class="standard_table_row_odd">
	      <div style="text-align: center">You have no bank accounts.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="bank-account">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="depository_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="bank_account_type"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">edit_bank_account_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="bank_account_number"/>
	</a>
	<xsl:if test="is_primary = 1">
	  (primary)
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="bank_account_nickname"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="notes"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

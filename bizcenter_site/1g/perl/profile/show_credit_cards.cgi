#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # fetch credit cards linked to cust_acct
  my (%credit_card_by_id, @credit_card_id_list);
  my $sql_query = "select * from credit_card_tbl where account_id = ? and person_id = ? order by card_number";
  my @sql_param_list = ([$cust_acct_id, SQL_INTEGER], [$cust_pers->{"id"}, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%credit_card_by_id, \@credit_card_id_list);
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $first_name = HTML::Entities::encode($cust_pers->{"first_name"});
  my $last_name = HTML::Entities::encode($cust_pers->{"last_name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <person>
    <first_name>$first_name</first_name>
    <last_name>$last_name</last_name>
  </person>
  <credit-card-list>
---END_BLOCK---;
  for my $credit_card_id (@credit_card_id_list) {
    my $credit_card = $credit_card_by_id{$credit_card_id};
    my $number_length = length($credit_card->{"card_number"});
    my $card_number = "*" x ($number_length - 4);
    $card_number .= substr($credit_card->{"card_number"}, $number_length - 4);
    my $card_expiration_month = HTML::Entities::encode($credit_card->{"card_expiration_month"});
    my $card_expiration_year = HTML::Entities::encode($credit_card->{"card_expiration_year"});
    my $is_primary = $credit_card->{"is_primary"} ? 1 : 0;
    my $card_nickname = HTML::Entities::encode($credit_card->{"card_nickname"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <credit-card>
      <id>$credit_card_id</id>
      <card_nickname>$card_nickname</card_nickname>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <is_primary>$is_primary</is_primary>
    </credit-card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </credit-card-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_credit_cards.xslt", $cgi);
}

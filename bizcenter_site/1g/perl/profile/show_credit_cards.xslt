<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Credit Cards
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="credit-card-list"/>
	<p>
	  You can register a <a href="new_credit_card_input.cgi">new credit card</a> or return to the <a href="/1g/index.cgi">main menu</a>. Please note we charge a 3% convenience fee when you use credit cards.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit-card-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="7" class="standard_table_title">
	  Credit Cards
	  for
	  <xsl:value-of select="../person/first_name"/><xsl:text> </xsl:text><xsl:value-of select="../person/last_name"/>
	  (<xsl:value-of select="../account-name"/>)
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Card Nickname</td>
	<td class="standard_table_spacer_column"></td>
	<td>Card Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Expiration</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(credit-card) > 0">
	  <xsl:apply-templates select="credit-card"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="7" class="standard_table_row_odd">
	      <div style="text-align: center">You have no credit cards.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="credit-card">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="card_nickname"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">edit_credit_card_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="card_number"/>
	</a>
	<xsl:if test="is_primary = 1">
	  (primary)
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

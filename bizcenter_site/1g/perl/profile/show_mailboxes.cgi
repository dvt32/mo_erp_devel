#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # fetch mailboxes linked to cust_acct
  my @mailbox_list;
  my $sql_query = "select * from mailbox_tbl where account_id = ? order by mailbox_number";
  get_row_list($dbh, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \@mailbox_list);
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <mailbox-list>
---END_BLOCK---;
  for my $mailbox (@mailbox_list) {
    my $id = $mailbox->{"id"};
    my $number = HTML::Entities::encode($mailbox->{"mailbox_number"});
    my $note = HTML::Entities::encode($mailbox->{"note"} || "");
    my $last_checked_dt = $mailbox->{"last_checked_dt"};
    my $bool_mail_present = $mailbox->{"bool_mail_present"};
    $generated_xml .= <<"---END_BLOCK---;";
    <mailbox>
      <id>$id</id>
      <number>$number</number>
      <note>$note</note>
      <last_checked_dt>$last_checked_dt</last_checked_dt>
      <bool_mail_present>$bool_mail_present</bool_mail_present>
    </mailbox>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </mailbox-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_mailboxes.xslt", $cgi);
}

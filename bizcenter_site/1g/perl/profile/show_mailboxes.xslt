<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Mailboxes
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="mailbox-list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="mailbox-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="5" class="standard_table_title">
	  Mailboxes
	  (<xsl:value-of select="count(mailbox)"/> total)
	  for <xsl:value-of select="../account-name"/>
	</td>
      </tr>
      <tr>
	<td class="standard_table_column_header">
	  Number
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Last Checked
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Has Mail
	</td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(mailbox) > 0">
	  <xsl:apply-templates select="mailbox"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="5" class="standard_table_body">
	      <div style="text-align: center">You have no mailboxes.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="mailbox">
    <tr valign="top">
      <td class="standard_table_body" style="text-align: center;">
	<xsl:value-of select="number"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="substring(last_checked_dt, 1, 16)"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body" style="text-align: center;">
	<xsl:choose>
	  <xsl:when test="bool_mail_present = 1">
	    Yes
	  </xsl:when>
	  <xsl:otherwise>
	    No
	  </xsl:otherwise>
	</xsl:choose>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # fetch cust_pers rows linked to cust_acct
  my @cust_pers_list;
  my $sql_query = "select * from cust_pers_tbl where cust_acct_id = ? order by last_name, first_name, middle_name";
  get_row_list($dbh, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \@cust_pers_list);
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <person-list>
---END_BLOCK---;
  for my $cust_pers (@cust_pers_list) {
    my $id = $cust_pers->{"id"};
    my $last_name = HTML::Entities::encode($cust_pers->{"last_name"});
    my $first_name = HTML::Entities::encode($cust_pers->{"first_name"});
    my $middle_name = HTML::Entities::encode($cust_pers->{"middle_name"} || "");
    my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
    my $is_active = $cust_pers->{"is_active"} ? 1 : 0;
    my $register_with_building = $cust_pers->{"register_with_building"} ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$id</id>
      <last-name>$last_name</last-name>
      <first-name>$first_name</first-name>
      <middle-name>$middle_name</middle-name>
      <email>$email</email>
      <is-active>$is_active</is-active>
      <is-registered>$register_with_building</is-registered>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_persons.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Persons
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="person-list"/>
	<p>
	  To edit or delete a person, click on the name. The active field specifies whether the person is still an active
	  employee. The registered field specifies whether the person should have 24 hour access to the building.
	</p>
	<p>
	  You can create a <a href="new_person_input.cgi">new person</a> or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="person-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="10" class="standard_table_title">
	  Persons
	  (<xsl:value-of select="count(person)"/> total)
	  for <xsl:value-of select="../account-name"/>
	</td>
      </tr>
      <tr>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  #
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Name
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Email
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Active?
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Registered?
	</td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(person) > 0">
	  <xsl:apply-templates select="person"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="10" class="standard_table_body">
	      <div style="text-align: center">You have no persons.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="person">
    <tr valign="top">
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body" style="text-align: right;">
	<xsl:value-of select="position()"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>edit_person_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="last-name"/>,
	  <xsl:value-of select="first-name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="middle-name"/>
	</a>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="email"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body" style="text-align: center;">
	<xsl:choose>
	  <xsl:when test="is-active = 1">
	    Yes
	  </xsl:when>
	  <xsl:otherwise>
	    No
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body" style="text-align: center;">
	<xsl:choose>
	  <xsl:when test="is-registered = 1">
	    Yes
	  </xsl:when>
	  <xsl:otherwise>
	    No
	  </xsl:otherwise>
	</xsl:choose>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_list;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # fetch cards linked to cust_acct
  my @card_list;
  my $sql_query = "select * from proximity_card_tbl where account_id = ? order by number";
  get_row_list($dbh, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \@card_list);
  # fetch access levels
  my %access_level_by_id;
  get_row_by_id_map($dbh, "select * from access_level_tbl", undef, \%access_level_by_id);
  # fetch linked cust_pers rows.
  my %cust_pers_by_id;
  $sql_query = "select cust_pers_tbl.* from cust_pers_tbl, proximity_card_tbl where proximity_card_tbl.account_id = ? and proximity_card_tbl.person_id = cust_pers_tbl.id";
  get_row_by_id_map($dbh, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \%cust_pers_by_id);
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <card-list>
---END_BLOCK---;
  for my $card (@card_list) {
    my $id = $card->{"id"};
    my $number = HTML::Entities::encode($card->{"number"});
    my $status = HTML::Entities::encode($card->{"status"});
    my $access_level_id = $card->{"access_level_id"};
    my $access_level_name = "";
    if (defined($access_level_id)) {
      $access_level_name = HTML::Entities::encode($access_level_by_id{$access_level_id}->{"name"});
    }
    my $cust_pers_id = $card->{"person_id"};
    my $person_name = "";
    if (defined($cust_pers_id) && exists($cust_pers_by_id{$cust_pers_id})) {
      my $cust_pers = $cust_pers_by_id{$cust_pers_id};
      $person_name = HTML::Entities::encode($cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"});
    }
    $generated_xml .= <<"---END_BLOCK---;";
    <card>
      <id>$id</id>
      <number>$number</number>
      <status>$status</status>
      <access-level>$access_level_name</access-level>
      <person>$person_name</person>
    </card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </card-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_proximity_cards.xslt", $cgi);
}

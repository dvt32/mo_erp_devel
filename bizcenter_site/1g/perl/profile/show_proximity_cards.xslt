<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Proximity Cards
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="card-list"/>
	<p>
	  The number field specifies the four digit number on the
	  proximity card. The status can be active, lost, or
	  inactive. You cannot delete existing cards or add new cards
	  through this web interface. Note also that although you can
	  update status and linked person in this web interface,
	  changes to the statuses of cards (active/lost/inactive) do
	  not take effect until they are approved by Micro Office
	  staff. Changes are generally approved in a timely fashion,
	  but if you need an immediate change in status (because for
	  instance, a card was lost and you are concerned about
	  security), please update the status using this web
	  interface, then email <a
	  href="mailto:service@microoffice.com">service@microoffice.com</a>
	  with the card number or numbers that need immediate
	  attention.
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="card-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="7" class="standard_table_title">
	  Proximity Cards
	  (<xsl:value-of select="count(card)"/> total)
	  for <xsl:value-of select="../account-name"/>
	</td>
      </tr>
      <tr>
	<td class="standard_table_column_header">
	  Number
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Status
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Access Level
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Person
	</td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(card) > 0">
	  <xsl:apply-templates select="card"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="7" class="standard_table_body">
	      <div style="text-align: center">You have no proximity cards.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="card">
    <tr valign="top">
      <td class="standard_table_body">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>edit_proximity_card_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="number"/>
	</a>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="status"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="access-level"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="person"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

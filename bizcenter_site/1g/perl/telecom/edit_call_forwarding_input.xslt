<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Call Forwarding
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="call-forwarding"/>
	<ul>
	  <li>Call Forward Delay = number of seconds before call forwarding kicks in. Must be 0 or some number from 5 to 45. 0 means call forward immediately.</li>
	  <li>Forward To Number = telephone number where to send calls.</li>
	</ul>
	<p>
	  Enter the "Forward To" number as XXXYYYZZZZ. For example, to forward calls to (646) 201-5508, enter 6462015508 in the
	  "Forward To" field. Supported area codes are 212, 646, 917, 347, 508, 516, 518, 631, 201, 973, 732, 718, 561, 312,
	  630, 505, 217, 874, 845, 914, 612, 800, 855, 866, 877, 888, 415, 617, 510, 570, 203, 804, 310, 305, 650, 602, 484. To
	  request other area codes, email <a href="mailto:service@microoffice.com">service@microoffice.com</a>.
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="call-forwarding">
    <table>
      <tr><td>
	<form action="/telecom/edit_call_forwarding_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Call Forwarding for <xsl:value-of select="number"/>
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Number:
		</td>
		<td>
		  <xsl:value-of select="number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Call Forward Status:
		</td>
		<td>
                  <select name="do_call_forward">
		    <option>
		      <xsl:if test="do_call_forward = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>Off</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="do_call_forward = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>On</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Call Forward Delay:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">call_forward_delay</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="call_forward_delay"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Forward To Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">call_forward_dst_num</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="call_forward_dst_num"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

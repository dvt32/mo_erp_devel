#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # load phone number.
  my $phone_number_id = $cgi->param("id");
  if ((!defined($phone_number_id)) || ($phone_number_id !~ /^\d+$/)) {
    die "show_page: edit_call_forwarding_not_found.html\n";
  }
  my $phone_number = select_row($dbh_phone, "phone_number_tbl", $phone_number_id);
  # verify existence of phone number.
  if (!$phone_number) {
    die "show_page: edit_call_forwarding_not_found.html\n";
  }
  # verify ownership of phone number.
  if ($cust_acct_id != $phone_number->{"account_id"}) {
    die "show_page: edit_call_forwarding_not_owner.html\n";
  }
  # get the form fields.
  my $do_call_forward = $cgi->param("do_call_forward");
  my $call_forward_delay = $cgi->param("call_forward_delay");
  my $call_forward_dst_num = $cgi->param("call_forward_dst_num");
  # verify do_call_forward is 0 or 1.
  if ($do_call_forward !~ /^0|1$/) {
    die "show_page: edit_call_forwarding_bad_do_call_forward.html\n";
  }
  # verify valid delay.
  if ($call_forward_delay =~ /^\d+$/) {
    if (($call_forward_delay == 0) || (($call_forward_delay >= 5) && ($call_forward_delay <= 45))) {
      # all good.
    } else {
      die "show_page: edit_call_forwarding_bad_delay.html\n";
    }
  } else {
    die "show_page: edit_call_forwarding_bad_delay.html\n";
  }
  # verify format of "forward to" field.
  if ($call_forward_dst_num =~ /^(\d{3})(\d{3})(\d{4})$/) {
    my $area_code = $1;
    if ($area_code !~ /^(212|646|917|347|508|516|518|631|201|973|732|718|561|312|630|505|217|847|845|914|612|800|855|866|877|888|415|617|510|570|203|804|310|315|305|650|602|484)$/) {
      die "show_page: edit_call_forwarding_bad_area_code.html\n";
    }
  } elsif ($call_forward_dst_num eq "") {
    # OK to be blank.
  } else {
    die "show_page: edit_call_forwarding_bad_format.html\n";
  }
  my @sql_column_list;
  push(@sql_column_list, ["do_call_forward", $do_call_forward, SQL_BOOLEAN]);
  push(@sql_column_list, ["call_forward_delay", $call_forward_delay, SQL_INTEGER]);
  push(@sql_column_list, ["call_forward_dst_num", $call_forward_dst_num]);
  update_row($dbh_phone, "phone_number_tbl", \@sql_column_list, $phone_number_id);
  # populate information needed for display of page.
  my $number = HTML::Entities::encode($phone_number->{"phone_number"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <call-forwarding>
    <number>$number</number>
    <do_call_forward>$do_call_forward</do_call_forward>
    <call_forward_delay>$call_forward_delay</call_forward_delay>
    <call_forward_dst_num>$call_forward_dst_num</call_forward_dst_num>
  </call-forwarding>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_call_forwarding_result_success.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Call Forwarding
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="call-forwarding"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="call-forwarding">
    <p>
      You successfully made the following call forwarding updates:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Call Forward Status:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="do_call_forward = 1">
	      On
	    </xsl:when>
	    <xsl:otherwise>
	      Off
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Call Forward Delay:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="call_forward_delay"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Forward To Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="call_forward_dst_num"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

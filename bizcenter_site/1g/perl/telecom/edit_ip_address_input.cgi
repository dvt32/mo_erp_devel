#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  # check if ip address id is specified.
  my $ip_address_id = $cgi->param("id");
  if ((!defined($ip_address_id)) || ($ip_address_id !~ /^\d+$/)) {
    die "show_page: edit_ip_address_not_found.html\n";
  }
  # load ip address.
  my $ip_address = select_row($dbh, "ip_address_tbl", $ip_address_id);
  if (!$ip_address) {
    die "show_page: edit_ip_address_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $ip_address->{"account_id"}) {
    die "show_page: edit_ip_address_not_owner.html\n";
  }
  # fetch all person rows belonging to the cust_acct.
  my @person_list;
  my $sql_query = "select * from cust_pers_tbl where cust_acct_id = ? order by last_name, first_name, middle_name";
  get_row_list($dbh, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \@person_list);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $ip_address_field = HTML::Entities::encode($ip_address->{"ip_address"});
  my $note = HTML::Entities::encode($ip_address->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ip-address>
    <account-name>$account_name</account-name>
    <id>$ip_address_id</id>
    <ip-address>$ip_address_field</ip-address>
    <note>$note</note>
  </ip-address>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_ip_address_input.xslt", $cgi);
}

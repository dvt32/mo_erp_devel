#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_cust_acct;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # load ip address.
  my $ip_address_id = $cgi->param("id");
  if ((!defined($ip_address_id)) || ($ip_address_id !~ /^\d+$/)) {
    die "show_page: edit_ip_address_not_found.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  my $ip_address = select_row($dbh, "ip_address_tbl", $ip_address_id);
  if (!$ip_address) {
    $dbh->rollback();
    die "show_page: edit_ip_address_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $ip_address->{"account_id"}) {
    $dbh->rollback();
    die "show_page: edit_ip_address_not_owner.html\n";
  }
  # get the form fields.
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "ip_address_tbl", \@sql_column_list, $ip_address_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $ip_address_field = HTML::Entities::encode($ip_address->{"ip_address"});
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ip-address>
    <account-name>$account_name</account-name>
    <ip-address>$ip_address_field</ip-address>
    <note>$note</note>
  </ip-address>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_ip_address_result_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # check if phone number id is specified.
  my $phone_number_id = $cgi->param("id");
  if ((!defined($phone_number_id)) || ($phone_number_id !~ /^\d+$/)) {
    die "show_page: edit_phone_number_not_found.html\n";
  }
  # load phone number.
  my $phone_number = select_row($dbh_phone, "phone_number_tbl", $phone_number_id);
  if (!$phone_number) {
    die "show_page: edit_phone_number_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $phone_number->{"account_id"}) {
    die "show_page: edit_phone_number_not_owner.html\n";
  }
  # fetch all person rows belonging to the cust_acct.
  my @person_list;
  my $sql_query = "select * from cust_pers_tbl where cust_acct_id = ? order by last_name, first_name, middle_name";
  get_row_list($dbh, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \@person_list);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $number = HTML::Entities::encode($phone_number->{"phone_number"});
  my $extension = HTML::Entities::encode($phone_number->{"extension"});
  my $visibility = $phone_number->{"visibility"};
  my $person_id = defined($phone_number->{"person_id"}) ? $phone_number->{"person_id"} : "";
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <person-list>
---END_BLOCK---;
  for my $person (@person_list) {
    my $loop_person_id = defined($person->{"id"}) ? $person->{"id"} : "";
    my $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
  $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$loop_person_id</id>
      <name>$person_name</name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
  <phone-number>
    <account-name>$account_name</account-name>
    <id>$phone_number_id</id>
    <number>$number</number>
    <extension>$extension</extension>
    <visibility>$visibility</visibility>
    <person-id>$person_id</person-id>
  </phone-number>
  <visibility-list>
    <visibility>hidden</visibility>
    <visibility>visible</visibility>
    <visibility>emphasized</visibility>
  </visibility-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_phone_number_input.xslt", $cgi);
}

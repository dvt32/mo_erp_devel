<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Edit Phone Number
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="phone-number"/>
	<p>
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>edit_call_forwarding_input.cgi?id=</xsl:text>
	      <xsl:value-of select="phone-number/id"/>
	    </xsl:attribute>
	    <xsl:text>Edit call forwarding settings</xsl:text>
	  </a>
	  or
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>reset_voicemail_pin_input.cgi?id=</xsl:text>
	      <xsl:value-of select="phone-number/id"/>
	    </xsl:attribute>
	    <xsl:text>reset voicemail PIN</xsl:text>
	  </a>
	  or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="phone-number">
    <table>
      <tr><td>
	<form action="/telecom/edit_phone_number_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Phone Number for <xsl:value-of select="account-name"/>
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Number:
		</td>
		<td>
		  <xsl:value-of select="number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Extension:
		</td>
		<td>
		  <xsl:value-of select="extension"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Visibility:
		</td>
		<td>
                  <select name="visibility">
		    <xsl:for-each select="/page/visibility-list/visibility">
		      <option>
			<xsl:if test="/page/phone-number/visibility = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Person:
		</td>
		<td>
                  <select name="person_id">
		    <option value="">(unassigned)</option>
		    <xsl:for-each select="/page/person-list/person">
		      <option>
			<xsl:if test="/page/phone-number/person-id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use get_cust_acct;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # load phone number.
  my $phone_number_id = $cgi->param("id");
  if ((!defined($phone_number_id)) || ($phone_number_id !~ /^\d+$/)) {
    die "show_page: edit_phone_number_not_found.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  my $phone_number = select_row($dbh_phone, "phone_number_tbl", $phone_number_id);
  if (!$phone_number) {
    $dbh->rollback();
    die "show_page: edit_phone_number_not_found.html\n";
  }
  if ($cust_acct->{"id"} != $phone_number->{"account_id"}) {
    $dbh->rollback();
    die "show_page: edit_phone_number_not_owner.html\n";
  }
  # get the form fields.
  my $visibility = $cgi->param("visibility");
  if ($visibility !~ /^hidden|visible|emphasized$/) {
    die "show_page: edit_phone_number_bad_visibility.html";
  }
  my $person_id = $cgi->param("person_id") || "";
  my $person_name = "";
  if ($person_id eq "") {
    $person_id = undef;
  } else {
    if ($person_id =~ /^\d+$/) {
      # verify that the person_id is a person belonging to the cust_acct.
      my $person = select_row($dbh, "cust_pers_tbl", $person_id);
      if ((!$person) || ($person->{"cust_acct_id"} != $cust_acct_id)) {
	$dbh->rollback();
	die "show_page: edit_phone_number_bad_person.html\n";
      }
      # need person_name for display of result page.
      $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
    } else {
      $dbh->rollback();
      die "show_page: edit_phone_number_bad_person.html\n";
    }
  }
  # check that if the phone number is visible, it has a linked person.
  if ($visibility ne "hidden") {
    if (!defined($person_id)) {
      $dbh->rollback();
      die "show_page: edit_phone_number_missing_person.html\n";
    }
  }
  my @sql_column_list;
  push(@sql_column_list, ["visibility", $visibility]);
  push(@sql_column_list, ["person_id", $person_id, SQL_INTEGER]);
  update_row($dbh_phone, "phone_number_tbl", \@sql_column_list, $phone_number_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $number = HTML::Entities::encode($phone_number->{"phone_number"});
  my $extension = HTML::Entities::encode($phone_number->{"extension"});
  $person_name = HTML::Entities::encode($person_name);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <phone-number>
    <account-name>$account_name</account-name>
    <number>$number</number>
    <extension>$extension</extension>
    <visibility>$visibility</visibility>
    <person>$person_name</person>
  </phone-number>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_phone_number_result_success.xslt", $cgi);
}

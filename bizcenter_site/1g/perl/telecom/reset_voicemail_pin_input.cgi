#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # check if phone number id is specified.
  my $phone_number_id = $cgi->param("id");
  if ((!defined($phone_number_id)) || ($phone_number_id !~ /^\d+$/)) {
    die "show_page: reset_voicemail_pin_not_found.html\n";
  }
  # load phone number.
  my $phone_number = select_row($dbh, "phone_number_tbl", $phone_number_id);
  if (!$phone_number) {
    die "show_page: reset_voicemail_pin_not_found.html\n";
  }
  if ($cust_acct_id != $phone_number->{"account_id"}) {
    die "show_page: reset_voicemail_pin_not_owner.html\n";
  }
  # populate information needed for display of page.
  my $number = HTML::Entities::encode($phone_number->{"phone_number"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <voicemail-pin>
    <id>$phone_number_id</id>
    <number>$number</number>
  </voicemail-pin>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reset_voicemail_pin_input.xslt", $cgi);
}

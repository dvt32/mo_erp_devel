#!/usr/bin/perl

use strict;
local($^W) = 1;

use LWP::UserAgent;
use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;

my $url = "https://s1.ews.voip.nyc1.e-nt.net/servlet/com.broadsoft.clients.oam.servlets.Login";
my $login_username = "microoffice";
my $login_password = "p5YfMHUs1r";

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  # load phone number.
  my $phone_number_id = $cgi->param("id");
  if ((!defined($phone_number_id)) || ($phone_number_id !~ /^\d+$/)) {
    die "show_page: reset_voicemail_pin_not_found.html\n";
  }
  my $phone_number = select_row($dbh, "phone_number_tbl", $phone_number_id);
  # load url and credentials.
  my $phone_carrier_id = $phone_number->{"phone_carrier_id"};
  my $phone_carrier = select_row($dbh, "phone_carrier_tbl", $phone_carrier_id);
  # check that we can reset the voicemail PIN on the number.
  if ($phone_number->{"type"} ne "voice") {
    die "show_page: reset_voicemail_pin_bad_type.html\n";
  }
  # verify existence of phone number.
  if (!$phone_number) {
    die "show_page: reset_voicemail_pin_not_found.html\n";
  }
  # verify ownership of phone number.
  if ($cust_acct_id != $phone_number->{"account_id"}) {
    die "show_page: reset_voicemail_pin_not_owner.html\n";
  }
  # get the form fields.
  my $voicemail_pin = $cgi->param("voicemail_pin") || "";
  # verify format of pin.
  if ($voicemail_pin !~ /^\d{4,7}$/) {
    die "show_page: reset_voicemail_pin_bad_format.html\n";
  }
  # set up the user agent.
  my $user_agent = LWP::UserAgent->new();
  $user_agent->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.1) Gecko/20060111 Firefox/1.5.0.1");
  push(@{$user_agent->requests_redirectable }, "POST");
  $user_agent->cookie_jar({});
  my $cookie_jar = $user_agent->cookie_jar();
  my ($response, $response_content);
  # fill in credentials and submit the login form.
  my %param_hash =
    (
     "domain" => "",
     "UserID" => $login_username,
     "EnteredUserID" => $login_username,
     "Password" => $login_password,
     );
  $response = $user_agent->post($url, \%param_hash);
  # simulate the Javascript set cookie command.
  $response->base() =~ m{^(http[s]?://[^/]+/)}i;
  my $base_url = $1;
  $base_url =~ m{^http[s]?://([^/]+)/}i;
  my $cookie_domain = $1;
  $cookie_jar->set_cookie(0, "acceptCookies", "true", "/servlet/", $cookie_domain, undef, 1, 0, undef, 1, {});
  $cookie_jar->set_cookie(0, "acceptCookies", "true", "/Login/", $cookie_domain, undef, 1, 0, undef, 1, {});
  # simulate a click on the phone number. This is necessary because
  # the BroadSoft portal stupidly uses server-side state to keep track
  # of the phone number currently being operated on rather than the
  # smart way of passing the phone number in the URL.
  my $group = $phone_number->{"enterprise_group"};
  my $phone_number_key = $phone_number->{"phone_number"};
  $phone_number_key =~ s/-//g;
  $url = $base_url . "Group/Members/Modify/index.jsp?key=ENT-MicroOffice_634803%3A%3A" . $group . "%3A%3A" . $phone_number_key;
  $response = $user_agent->get($url);
  $url = $base_url . "User/Password/";
  %param_hash =
    (
     "buttonClicked" => "",
     "category" => "VoicePortal",
     "minPwdLen" => "3",
     "pwdType" => "VoicePortal",
     "newPassword" => $voicemail_pin,
     "newPasswordConfirm" => $voicemail_pin,
     "ok" => "OK",
     );
  $response = $user_agent->post($url, \%param_hash);
  # populate information needed for display of page.
  my $number = HTML::Entities::encode($phone_number->{"phone_number"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <voicemail-pin>
    <number>$number</number>
    <pin>$voicemail_pin</pin>
  </voicemail-pin>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reset_voicemail_pin_result_success.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: IP Addresses
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<xsl:apply-templates select="ip-address-list"/>
	<p>
	  You cannot delete existing IP addresses or add new IP
	  addresses through this web interface. To delete or add IP
	  addresses, please email <a
	  href="mailto:service@microoffice.com">service@microoffice.com</a>
	  with your request.
	</p>
<pre>
Other network details.

902 Broadway:
subnet mask: 255.255.255.192
gateway: 50.74.218.193
primary DNS: 24.29.99.35
secondary DNS: 24.29.99.36

36 W. 20th St.:
subnet mask: 255.255.255.248
gateway: 184.75.16.209
primary DNS: 24.29.99.35
secondary DNS: 24.29.99.36

122 W. 27th St.:
subnet mask: 255.255.255.224
gateway: 184.74.162.225
primary DNS: 24.29.99.35
secondary DNS: 24.29.99.36

1375 Broadway:
subnet mask: 255.255.255.128
gateway: 206.71.242.1
primary DNS: 207.172.3.8
secondary DNS: 207.172.3.9

new range of 162.208.108.x
subnet mask: 255.255.255.0
gateway: 162.208.108.1
primary DNS: 162.208.108.1
secondary DNS: 8.8.8.8

1601 Broadway:
subnet mask: 255.255.255.248
gateway: 50.74.183.153
primary DNS: 24.29.99.35
secondary DNS: 24.29.99.36
</pre>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ip-address-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="5" class="standard_table_title">
	  IP Addresses
	  (<xsl:value-of select="count(ip-address)"/> total)
	  for <xsl:value-of select="../account-name"/>
	</td>
      </tr>
      <tr>
	<td class="standard_table_column_header">
	  IP Address
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Note
	</td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(ip-address) > 0">
	  <xsl:apply-templates select="ip-address"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="5" class="standard_table_body">
	      <div style="text-align: center">You have no IP addresses.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="ip-address">
    <tr valign="top">
      <td class="standard_table_body">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>edit_ip_address_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="ip-address"/>
	</a>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="note"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

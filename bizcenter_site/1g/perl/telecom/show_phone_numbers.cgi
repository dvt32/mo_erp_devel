#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ bizcenter_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use get_cust_acct;
use get_row_list;
use get_row_by_id_map;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $cust_pers) = init_login_standard($instance_map);
  my $cust_acct = get_cust_acct($instance_map);
  my $cust_acct_id = $cust_acct->{"id"};
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # fetch phone numbers linked to cust_acct
  my @phone_number_list;
  my $sql_query = "select * from phone_number_tbl where account_id = ? order by phone_number";
  get_row_list($dbh_phone, $sql_query, [[$cust_acct_id, SQL_INTEGER]], \@phone_number_list);
  # fetch linked person rows.
  my %person_by_id;
  for my $phone_number (@phone_number_list) {
    my $person_id = $phone_number->{"person_id"};
    if (($person_id) && (!$person_by_id{$person_id})) {
      $person_by_id{$person_id} = select_row($dbh, "person_vw", $person_id);
    }
  }
  # populate information for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <phone-number-list>
---END_BLOCK---;
  for my $phone_number (@phone_number_list) {
    my $id = $phone_number->{"id"};
    my $number = HTML::Entities::encode($phone_number->{"phone_number"});
    my $extension = HTML::Entities::encode($phone_number->{"extension"});
    my $visibility = $phone_number->{"visibility"};
    my $person_id = $phone_number->{"person_id"};
    my $person_name = "";
    if (defined($person_id) && exists($person_by_id{$person_id})) {
      my $person = $person_by_id{$person_id};
      $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
    }
    $generated_xml .= <<"---END_BLOCK---;";
    <phone-number>
      <id>$id</id>
      <number>$number</number>
      <extension>$extension</extension>
      <visibility>$visibility</visibility>
      <person>$person_name</person>
    </phone-number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </phone-number-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_phone_numbers.xslt", $cgi);
}

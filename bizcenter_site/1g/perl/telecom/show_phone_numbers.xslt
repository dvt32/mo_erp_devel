<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Business Center :: Phone Numbers
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/bizcenter.css"/>
      </head>
      <body>
	<p>
	  <b><span class="attention_notice">Note: to edit call forwarding settings, first choose a phone number and click on
	  it. A new page will appear. At the bottom, there is a link to "Edit call forwarding settings."</span></b>
	</p>
	<xsl:apply-templates select="phone-number-list"/>
	<p>
	  To edit a phone number, click on the phone number. The extension field specifies the four digit extension that can be
	  used to dial numbers internally when in the office. The visibility field specifies whether the extension shows up on
	  the intercom directory at the front desk. You cannot delete existing phone numbers or add new phone numbers through
	  this web interface. To delete or add phone numbers, please email <a
	  href="mailto:service@microoffice.com">service@microoffice.com</a> with your request.
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="phone-number-list">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="7" class="standard_table_title">
	  Phone Numbers
	  (<xsl:value-of select="count(phone-number)"/> total)
	  for <xsl:value-of select="../account-name"/>
	</td>
      </tr>
      <tr>
	<td class="standard_table_column_header">
	  Number
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Extension
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Visibility
	</td>
	<td class="standard_table_column_header_spacer_column"><xsl:text> </xsl:text></td>
	<td class="standard_table_column_header">
	  Person
	</td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(phone-number) > 0">
	  <xsl:apply-templates select="phone-number"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="7" class="standard_table_body">
	      <div style="text-align: center">You have no phone numbers.</div>
	    </td>
	  </tr>	  
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="phone-number">
    <tr valign="top">
      <td class="standard_table_body">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>edit_phone_number_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="number"/>
	</a>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="extension"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body" style="text-align: center;">
	<xsl:value-of select="visibility"/>
      </td>
      <td class="standard_table_body_spacer_column"><xsl:text> </xsl:text></td>
      <td class="standard_table_body">
	<xsl:value-of select="person"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

<?php

use AD7six\Dsn\Wrapper\CakePHP\DbDsn;

class DATABASE_CONFIG {

/**
 * Define connections using environment variables
 *
 * @return void
 */
	public function __construct() {

		if (!env('DATABASE_DEFAULT'))
			throw new NotFoundException('Missing database configuration DATABASE_DEFAULT, define it in the .env file');
		if (!env('DATABASE_TELECOM'))
			throw new NotFoundException('Missing database configuration DATABASE_TELECOM, define it in the .env file');
		if (!env('DATABASE_PROVENDER'))
			throw new NotFoundException('Missing database configuration DATABASE_PROVENDER, define it in the .env file');
		if (!env('DATABASE_TEST_URL'))
			throw new NotFoundException('Missing database configuration DATABASE_TEST_URL, define it in the .env file');

		$this->default = DbDsn::parse(env('DATABASE_DEFAULT'));
		$this->telecom = DbDsn::parse(env('DATABASE_TELECOM'));
		$this->provender = DbDsn::parse(env('DATABASE_PROVENDER'));
		$this->test = DbDsn::parse(env('DATABASE_TEST_URL'));
	}

}

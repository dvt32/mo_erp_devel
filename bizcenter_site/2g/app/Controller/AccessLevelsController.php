<?php
App::uses('AppController', 'Controller');
/**
 * AccessLevels Controller
 *
 * @property AccessLevel $AccessLevel
 */
class AccessLevelsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccessLevel->recursive = 0;
		$this->set('accessLevels', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccessLevel->exists($id)) {
			throw new NotFoundException(__('Invalid access level'));
		}
		$options = array('conditions' => array('AccessLevel.' . $this->AccessLevel->primaryKey => $id));
		$this->set('accessLevel', $this->AccessLevel->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AccessLevel->create();
			if ($this->AccessLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The access level has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The access level could not be saved. Please, try again.'));
			}
		}
		$floors = $this->AccessLevel->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AccessLevel->exists($id)) {
			throw new NotFoundException(__('Invalid access level'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AccessLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The access level has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The access level could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AccessLevel.' . $this->AccessLevel->primaryKey => $id));
			$this->request->data = $this->AccessLevel->find('first', $options);
		}
		$floors = $this->AccessLevel->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AccessLevel->id = $id;
		if (!$this->AccessLevel->exists()) {
			throw new NotFoundException(__('Invalid access level'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AccessLevel->delete()) {
			$this->Session->setFlash(__('Access level deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Access level was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AccessLevel->recursive = 0;
		$this->set('accessLevels', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AccessLevel->exists($id)) {
			throw new NotFoundException(__('Invalid access level'));
		}
		$options = array('conditions' => array('AccessLevel.' . $this->AccessLevel->primaryKey => $id));
		$this->set('accessLevel', $this->AccessLevel->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AccessLevel->create();
			if ($this->AccessLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The access level has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The access level could not be saved. Please, try again.'));
			}
		}
		$floors = $this->AccessLevel->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AccessLevel->exists($id)) {
			throw new NotFoundException(__('Invalid access level'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AccessLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The access level has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The access level could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AccessLevel.' . $this->AccessLevel->primaryKey => $id));
			$this->request->data = $this->AccessLevel->find('first', $options);
		}
		$floors = $this->AccessLevel->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AccessLevel->id = $id;
		if (!$this->AccessLevel->exists()) {
			throw new NotFoundException(__('Invalid access level'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AccessLevel->delete()) {
			$this->Session->setFlash(__('Access level deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Access level was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

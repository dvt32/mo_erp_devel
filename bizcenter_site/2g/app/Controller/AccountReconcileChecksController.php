<?php
App::uses('AppController', 'Controller');
/**
 * AccountReconcileChecks Controller
 *
 * @property AccountReconcileCheck $AccountReconcileCheck
 */
class AccountReconcileChecksController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccountReconcileCheck->recursive = 0;
		$this->set('accountReconcileChecks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccountReconcileCheck->exists($id)) {
			throw new NotFoundException(__('Invalid account reconcile check'));
		}
		$options = array('conditions' => array('AccountReconcileCheck.' . $this->AccountReconcileCheck->primaryKey => $id));
		$this->set('accountReconcileCheck', $this->AccountReconcileCheck->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AccountReconcileCheck->create();
			if ($this->AccountReconcileCheck->save($this->request->data)) {
				$this->Session->setFlash(__('The account reconcile check has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account reconcile check could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AccountReconcileCheck->exists($id)) {
			throw new NotFoundException(__('Invalid account reconcile check'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AccountReconcileCheck->save($this->request->data)) {
				$this->Session->setFlash(__('The account reconcile check has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account reconcile check could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AccountReconcileCheck.' . $this->AccountReconcileCheck->primaryKey => $id));
			$this->request->data = $this->AccountReconcileCheck->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AccountReconcileCheck->id = $id;
		if (!$this->AccountReconcileCheck->exists()) {
			throw new NotFoundException(__('Invalid account reconcile check'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AccountReconcileCheck->delete()) {
			$this->Session->setFlash(__('Account reconcile check deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Account reconcile check was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AccountReconcileCheck->recursive = 0;
		$this->set('accountReconcileChecks', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AccountReconcileCheck->exists($id)) {
			throw new NotFoundException(__('Invalid account reconcile check'));
		}
		$options = array('conditions' => array('AccountReconcileCheck.' . $this->AccountReconcileCheck->primaryKey => $id));
		$this->set('accountReconcileCheck', $this->AccountReconcileCheck->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AccountReconcileCheck->create();
			if ($this->AccountReconcileCheck->save($this->request->data)) {
				$this->Session->setFlash(__('The account reconcile check has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account reconcile check could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AccountReconcileCheck->exists($id)) {
			throw new NotFoundException(__('Invalid account reconcile check'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AccountReconcileCheck->save($this->request->data)) {
				$this->Session->setFlash(__('The account reconcile check has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account reconcile check could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AccountReconcileCheck.' . $this->AccountReconcileCheck->primaryKey => $id));
			$this->request->data = $this->AccountReconcileCheck->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AccountReconcileCheck->id = $id;
		if (!$this->AccountReconcileCheck->exists()) {
			throw new NotFoundException(__('Invalid account reconcile check'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AccountReconcileCheck->delete()) {
			$this->Session->setFlash(__('Account reconcile check deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Account reconcile check was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

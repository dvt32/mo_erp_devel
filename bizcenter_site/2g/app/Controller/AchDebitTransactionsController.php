<?php
App::uses('AppController', 'Controller');
/**
 * AchDebitTransactions Controller
 *
 * @property AchDebitTransaction $AchDebitTransaction
 */
// class AchDebitTransactionsController extends AppController {

// /**
//  * index method
//  *
//  * @return void
//  */
// 	public function index() {
// 		$this->AchDebitTransaction->recursive = 0;
// 		$this->set('achDebitTransactions', $this->Paginator->paginate());
// 	}

// /**
//  * view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function view($id = null) {
// 		if (!$this->AchDebitTransaction->exists($id)) {
// 			throw new NotFoundException(__('Invalid ach debit transaction'));
// 		}
// 		$options = array('conditions' => array('AchDebitTransaction.' . $this->AchDebitTransaction->primaryKey => $id));
// 		$this->set('achDebitTransaction', $this->AchDebitTransaction->find('first', $options));
// 	}

// /**
//  * add method
//  *
//  * @return void
//  */
// 	public function add() {
// 		if ($this->request->is('post')) {
// 			$this->AchDebitTransaction->create();
// 			if ($this->AchDebitTransaction->save($this->request->data)) {
// 				$this->Session->setFlash(__('The ach debit transaction has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The ach debit transaction could not be saved. Please, try again.'));
// 			}
// 		}
// 		$customerAccounts = $this->AchDebitTransaction->CustomerAccount->find('list');
// 		$bankAccounts = $this->AchDebitTransaction->BankAccount->find('list');
// 		$electronicChecks = $this->AchDebitTransaction->ElectronicCheck->find('list');
// 		$receivePayments = $this->AchDebitTransaction->ReceivePayment->find('list');
// 		$this->set(compact('customerAccounts', 'bankAccounts', 'electronicChecks', 'receivePayments'));
// 	}

// /**
//  * edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function edit($id = null) {
// 		if (!$this->AchDebitTransaction->exists($id)) {
// 			throw new NotFoundException(__('Invalid ach debit transaction'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->AchDebitTransaction->save($this->request->data)) {
// 				$this->Session->setFlash(__('The ach debit transaction has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The ach debit transaction could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('AchDebitTransaction.' . $this->AchDebitTransaction->primaryKey => $id));
// 			$this->request->data = $this->AchDebitTransaction->find('first', $options);
// 		}
// 		$customerAccounts = $this->AchDebitTransaction->CustomerAccount->find('list');
// 		$bankAccounts = $this->AchDebitTransaction->BankAccount->find('list');
// 		$electronicChecks = $this->AchDebitTransaction->ElectronicCheck->find('list');
// 		$receivePayments = $this->AchDebitTransaction->ReceivePayment->find('list');
// 		$this->set(compact('customerAccounts', 'bankAccounts', 'electronicChecks', 'receivePayments'));
// 	}

// /**
//  * delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function delete($id = null) {
// 		$this->AchDebitTransaction->id = $id;
// 		if (!$this->AchDebitTransaction->exists()) {
// 			throw new NotFoundException(__('Invalid ach debit transaction'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->AchDebitTransaction->delete()) {
// 			$this->Session->setFlash(__('Ach debit transaction deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Ach debit transaction was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}

// /**
//  * admin_index method
//  *
//  * @return void
//  */
// 	public function admin_index() {
// 		$this->AchDebitTransaction->recursive = 0;
// 		$this->set('achDebitTransactions', $this->Paginator->paginate());
// 	}

// /**
//  * admin_view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_view($id = null) {
// 		if (!$this->AchDebitTransaction->exists($id)) {
// 			throw new NotFoundException(__('Invalid ach debit transaction'));
// 		}
// 		$options = array('conditions' => array('AchDebitTransaction.' . $this->AchDebitTransaction->primaryKey => $id));
// 		$this->set('achDebitTransaction', $this->AchDebitTransaction->find('first', $options));
// 	}

// /**
//  * admin_add method
//  *
//  * @return void
//  */
// 	public function admin_add() {
// 		if ($this->request->is('post')) {
// 			$this->AchDebitTransaction->create();
// 			if ($this->AchDebitTransaction->save($this->request->data)) {
// 				$this->Session->setFlash(__('The ach debit transaction has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The ach debit transaction could not be saved. Please, try again.'));
// 			}
// 		}
// 		$customerAccounts = $this->AchDebitTransaction->CustomerAccount->find('list');
// 		$bankAccounts = $this->AchDebitTransaction->BankAccount->find('list');
// 		$electronicChecks = $this->AchDebitTransaction->ElectronicCheck->find('list');
// 		$receivePayments = $this->AchDebitTransaction->ReceivePayment->find('list');
// 		$this->set(compact('customerAccounts', 'bankAccounts', 'electronicChecks', 'receivePayments'));
// 	}

// /**
//  * admin_edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_edit($id = null) {
// 		if (!$this->AchDebitTransaction->exists($id)) {
// 			throw new NotFoundException(__('Invalid ach debit transaction'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->AchDebitTransaction->save($this->request->data)) {
// 				$this->Session->setFlash(__('The ach debit transaction has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The ach debit transaction could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('AchDebitTransaction.' . $this->AchDebitTransaction->primaryKey => $id));
// 			$this->request->data = $this->AchDebitTransaction->find('first', $options);
// 		}
// 		$customerAccounts = $this->AchDebitTransaction->CustomerAccount->find('list');
// 		$bankAccounts = $this->AchDebitTransaction->BankAccount->find('list');
// 		$electronicChecks = $this->AchDebitTransaction->ElectronicCheck->find('list');
// 		$receivePayments = $this->AchDebitTransaction->ReceivePayment->find('list');
// 		$this->set(compact('customerAccounts', 'bankAccounts', 'electronicChecks', 'receivePayments'));
// 	}

// /**
//  * admin_delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_delete($id = null) {
// 		$this->AchDebitTransaction->id = $id;
// 		if (!$this->AchDebitTransaction->exists()) {
// 			throw new NotFoundException(__('Invalid ach debit transaction'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->AchDebitTransaction->delete()) {
// 			$this->Session->setFlash(__('Ach debit transaction deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Ach debit transaction was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Aliases Controller
 *
 * @property Alias $Alias
 */
class AliasesController extends AppController {

	public $paginate = array(
        'limit' => 10,
    );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Alias->recursive = 0;
		$this->set('aliases', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Alias->exists($id)) {
			throw new NotFoundException(__('Invalid alias'));
		}
		$options = array('conditions' => array('Alias.' . $this->Alias->primaryKey => $id));
		$this->set('alias', $this->Alias->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Alias->create();
			if ($this->Alias->save($this->request->data)) {
				$this->Session->setFlash(__('The alias has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The alias could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Alias->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Alias->exists($id)) {
			throw new NotFoundException(__('Invalid alias'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Alias->save($this->request->data)) {
				$this->Session->setFlash(__('The alias has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The alias could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Alias.' . $this->Alias->primaryKey => $id));
			$this->request->data = $this->Alias->find('first', $options);
		}
		$customerAccounts = $this->Alias->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Alias->id = $id;
		if (!$this->Alias->exists()) {
			throw new NotFoundException(__('Invalid alias'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Alias->delete()) {
			$this->Session->setFlash(__('Alias deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Alias was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Alias->recursive = 0;
		$this->set('aliases', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Alias->exists($id)) {
			throw new NotFoundException(__('Invalid alias'));
		}
		$options = array('conditions' => array('Alias.' . $this->Alias->primaryKey => $id));
		$this->set('alias', $this->Alias->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Alias->create();
			if ($this->Alias->save($this->request->data)) {
				$this->Session->setFlash(__('The alias has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The alias could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Alias->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Alias->exists($id)) {
			throw new NotFoundException(__('Invalid alias'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Alias->save($this->request->data)) {
				$this->Session->setFlash(__('The alias has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The alias could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Alias.' . $this->Alias->primaryKey => $id));
			$this->request->data = $this->Alias->find('first', $options);
		}
		$customerAccounts = $this->Alias->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Alias->id = $id;
		if (!$this->Alias->exists()) {
			throw new NotFoundException(__('Invalid alias'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Alias->delete()) {
			$this->Session->setFlash(__('Alias deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Alias was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

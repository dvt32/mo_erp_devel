<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
App::uses('CrudControllerTrait', 'Crud.Lib');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	use CrudControllerTrait;

/**
 * List of global controller components
 *
 * @var array
 */
	public $components = [
		'RequestHandler',
		'Session',
		// 'DebugKit.Toolbar',
		'Auth' => array(
			'loginAction' => array('controller' => 'users', 'action' => 'login'),
			'loginRedirect' => array('controller' => 'customers', 'action' => 'dashboard'),
			'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
			'authorize' => array('Controller'),
		),
		'Crud.Crud' => [
			'listeners' => [
				'Crud.Api',
				'Crud.ApiPagination',
				'Crud.ApiQueryLog'
			]
		],
		'Paginator' => ['settings' => ['paramType' => 'querystring', 'limit' => 30]],
		'Alert',
	];

/**
 * authorization check
 * @return boolean
 */
	public function isAuthorized($user) {

		// map allowed actions
		$primary_actions =  @$this->allowed['primary'] ?: array();
		$admin_actions =    @$this->allowed['admin'] ?: array();
		$normal_actions =   @$this->allowed['normal'] ?: array();

		// default flag to false
		$authorized = false;

		// set flag according to role and allowed
		switch($user['role']) {
			case ROLE_SUPER:     $authorized = true;                                       break;
			case ROLE_PRIMARY:   $authorized = in_array($this->action, $primary_actions);  break;
			case ROLE_ADMIN:     $authorized = in_array($this->action, $admin_actions);    break;
			default:
			case ROLE_NORMAL:    $authorized = in_array($this->action, $normal_actions);   break;
		}

		// authorized
		if ($authorized) {
			return true;
		}
		$this->Alert->error(sprintf("You are not authorized to access that section! Please contact your account's primary person to give you access by making you an admin."), "Unauthorized");

		// unauthorized (fallthrough)
		// throw new ForbiddenException('You are not authorized to access this action');
	}

/**
 * fetch the floor id of the logged in user
 * 
 */
	public function getUserFloorId() {

		// fetch logged in user's customer account id
		$cust_acct_id = $this->Auth->user('cust_acct_id');

		// fetch the floor id
		$this->uses[] = 'CustomerAccount';
		$customerAccount = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'fields' => array('primary_floor_id'),
			'contain' => array()
		));
		$floor_id = $customerAccount['CustomerAccount']['primary_floor_id'];

		return $floor_id;
	}

/**
 * 
 * 
 */
	public function getNotifications($floor_id = 0, $building_id = 0) {

		// show alert if user is logged in and associated with a floor
		if (!empty($floor_id)) {

			// fetch notifications related to the floor
			$this->uses[] = 'Notification';
			$notifications = $this->Notification->find('all', array(
				// 'fields' => array('DISTINCT Notification.id','name','description'),
				'contain' => array(),
				'conditions' => array(
					'Notification.is_active' => TRUE,              // active notification
					'OR' => array(
						'Notification.floor_id' =>    $floor_id,     // floor-wide notification
						'Notification.building_id' => $building_id,  // building-wide notification
					),
				),
			));
			return $notifications;
		}
	}

	public function beforeRender()
	{
		if ($next = $this->request->query('next'))
		{
			return $this->redirect($next);
		}
	}

	
/**
 * Sends an email via Mandrill.
 * 
 * @param array $emails list of email recipients
 * @param mix $send_at strtotime parseable string indicating when to schedule the email to send (e.g., '+4 days') or a UTC time (seconds since epoch) in the future
 * @return array $result result of the call to the Mandrill API via MandrillTransport
 */
	public function sendEmail($message = array(), $send_at = false) {

		// check required fields
		if (empty($message['to'])) {
			throw new Exception('Missing email recipient(s)');
		}
		elseif (empty($message['subject'])) {
			throw new Exception('Missing email subject');
		}
		elseif (empty($message['text']) && empty($message['html'])) {
			throw new Exception('Missing email body');
		}

		// set default from_email
		$message['from_email'] = @$message['from_email'] ?: 'no-reply@microoffice.com';

		// create mandrill object
		$mandrill = new Mandrill(MANDRILL_API_KEY);
		// var_dump($mandrill);

		// send immediately
		if (empty($send_at)) {
			$results = $mandrill->messages->send($message);
		}
		// schedule email to be sent
		else {

			// convert to time if $send_at is a string
			$time = is_int($send_at) ? $send_at : strtotime($send_at);

			// convert time to utc datetime
			$utc_datetime = gmdate(DATETIME_FORMAT, $time);

			// schedule message
			$results = $mandrill->messages->send($message, false, null, $utc_datetime);
		}
		// debug($results);

		// filter our successful sends, leaving just the failed sends
		$failed = array_filter($results,function($result){ return $result['status']==='invalid' || $result['status']==='rejected'; });

		// report failed sends if debug is on
		// debug($failed);

		return empty($failed);
	}

}

<?php
App::uses('AppController', 'Controller');
/**
 * BankAccounts Controller
 *
 * @property BankAccount $BankAccount
 */
class BankAccountsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('history', 'view', 'add', 'edit', 'delete'),
		'admin' =>    array('history', 'view', 'add', 'edit', 'delete'),
		'normal' =>   array(),
	);
	
	public $paginate = array(
		'limit' => 50,
		'order' => array('BankAccount.creation_dt' => 'desc')
	);
/**
 * history method
 * @author John Jung
 * @param $cust_acct_id from AuthComponent
 * @return void
 */
	public function history() {

		$customer_account_id = AuthComponent::user('cust_acct_id');

		$this->BankAccount->recursive = 0;

		$this->Paginator->settings = array(
			'BankAccount' => array(
				'conditions' => ['BankAccount.account_id' => $customer_account_id]
			)
		);

		$this->set('bankAccounts', $this->Paginator->paginate());

	}

// /**
//  * index method
//  *
//  * @return void
//  */
// 	public function index() {
// 		$this->BankAccount->recursive = 0;
// 		$this->set('bankAccounts', $this->Paginator->paginate());
// 	}

// /**
//  * view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function view($id = null) {
// 		if (!$this->BankAccount->exists($id)) {
// 			throw new NotFoundException(__('Invalid bank account'));
// 		}
// 		$options = array('conditions' => array('BankAccount.' . $this->BankAccount->primaryKey => $id));
// 		$this->set('bankAccount', $this->BankAccount->find('first', $options));
// 	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->BankAccount->create();

			$data = $this->request->data;
			$data['BankAccount']['is_active'] = true;

			if ($this->BankAccount->save($data)) {
				$this->Alert->success(__('The bank account has been saved'));
				$this->redirect(array('controller' => 'payments', 'action' => 'accounts'));
			} else {
				// debug($this->BankAccount->validationErrors);
				$this->Alert->error(__('The bank account could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data['BankAccount']['country'] = 'USA';
		}
		$customerAccounts = $this->BankAccount->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

// /**
//  * edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function edit($id = null) {
// 		if (!$this->BankAccount->exists($id)) {
// 			throw new NotFoundException(__('Invalid bank account'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->BankAccount->save($this->request->data)) {
// 				$this->Session->setFlash(__('The bank account has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The bank account could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('BankAccount.' . $this->BankAccount->primaryKey => $id));
// 			$this->request->data = $this->BankAccount->find('first', $options);
// 		}
// 		$customerAccounts = $this->BankAccount->CustomerAccount->find('list');
// 		$this->set(compact('customerAccounts'));
// 	}

// /**
//  * delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function delete($id = null) {
// 		$this->BankAccount->id = $id;
// 		if (!$this->BankAccount->exists()) {
// 			throw new NotFoundException(__('Invalid bank account'));
// 		}

// 		if ($this->request->is('post') || $this->request->is('put')) {
		
// 			// find credit card
// 			$ba = $this->BankAccount->find('first', array(
// 				'conditions' => array(
// 					'BankAccount.id' => $id
// 				)
// 			));

// 			// set as inactive
// 			$ba['BankAccount']['is_active'] = false;

// 		}

// 		if ($this->BankAccount->save($ba)) {
// 			$this->Session->setFlash(__('Bank Account deleted'));			
// 		} else {
// 			$this->Session->setFlash(__('Bank Account was not deleted'));
// 		}
// 		$this->redirect(array('controller' => 'payments', 'action' => 'accounts'));
// 	}

// /**
//  * admin_index method
//  *
//  * @return void
//  */
// 	public function admin_index() {
// 		$this->BankAccount->recursive = 0;
// 		$this->set('bankAccounts', $this->Paginator->paginate());
// 	}

// /**
//  * admin_view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_view($id = null) {
// 		if (!$this->BankAccount->exists($id)) {
// 			throw new NotFoundException(__('Invalid bank account'));
// 		}
// 		$options = array('conditions' => array('BankAccount.' . $this->BankAccount->primaryKey => $id));
// 		$this->set('bankAccount', $this->BankAccount->find('first', $options));
// 	}

// /**
//  * admin_add method
//  *
//  * @return void
//  */
// 	public function admin_add() {
// 		if ($this->request->is('post')) {
// 			$this->BankAccount->create();
// 			if ($this->BankAccount->save($this->request->data)) {
// 				$this->Session->setFlash(__('The bank account has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The bank account could not be saved. Please, try again.'));
// 			}
// 		}
// 		$customerAccounts = $this->BankAccount->CustomerAccount->find('list');
// 		$this->set(compact('customerAccounts'));
// 	}

// /**
//  * admin_edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_edit($id = null) {
// 		if (!$this->BankAccount->exists($id)) {
// 			throw new NotFoundException(__('Invalid bank account'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->BankAccount->save($this->request->data)) {
// 				$this->Session->setFlash(__('The bank account has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The bank account could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('BankAccount.' . $this->BankAccount->primaryKey => $id));
// 			$this->request->data = $this->BankAccount->find('first', $options);
// 		}
// 		$customerAccounts = $this->BankAccount->CustomerAccount->find('list');
// 		$this->set(compact('customerAccounts'));
// 	}

// /**
//  * admin_delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_delete($id = null) {
// 		$this->BankAccount->id = $id;
// 		if (!$this->BankAccount->exists()) {
// 			throw new NotFoundException(__('Invalid bank account'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->BankAccount->delete()) {
// 			$this->Session->setFlash(__('Bank account deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Bank account was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}
}

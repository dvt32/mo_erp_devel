<?php
App::uses('AppController', 'Controller');
/**
 * Cdrs Controller
 *
 * @property Cdr $Cdr
 */
class CdrsController extends AppController {

/**
 * authorization rules
 *
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('recording', 'account', 'view', 'add', 'edit', 'delete'),
		'admin' =>    array('recording', 'account', 'view', 'add', 'edit', 'delete'),
		'normal' =>   array('recording', 'account', 'view', 'add', 'edit', 'delete'),
	);

/**
 * Pagination rules
 *
 * @var array
 */
	public $paginate = array(
		'limit' => 20
	);

/**
 * recording method
 *
 * @return void
 */
/*
	public function recording() {

		// TO DOWNLOAD A RECORDING
		// http://puresource.microoffice.com/dav/recordings/6466528418/20130809155914.6466528418.6466605046.wav
		// /dav/recordings/phonenumberto/yyyymmdddhhmmss.phonenumberfrom.phonenumberto.wav

		$cpid = AuthComponent::user('cust_pers_id');
		$caid = AuthComponent::user('cust_acct_id');

		$this->paginate['conditions'] = array('Cdr.account_id' => 1538);

		$this->Cdr->recursive = 0;
    $this->set('cdrs', $this->Paginator->paginate());
	}
*/
	
/**
 * search for a call log record
 * @return void
 */
	public function recording() {

		// get customer account id from auth
		$caid = AuthComponent::user('cust_acct_id');

		// query from params
		$query = @$this->request->query['q'] ?: '';

		// check query threshold
		if (strlen($query) >= 5) {

			// wildcard
			$query = "%{$query}%";

			// filter conditions
			$conditions = array(
				array('OR' => array(
					'Cdr.dest_num ILIKE' => $query,
					'Cdr.src_num ILIKE' => $query,
				)),
				'Cdr.account_id' => $caid,
				'Cdr.start_stamp ILIKE' => $query,
				'Cdr.end_stamp ILIKE' => $query,
			);
		} else {
			$conditions = ['Cdr.account_id' => $caid];
		}

		$order = ['start_stamp DESC'];

		// fetch cdrs
		$this->Paginator->settings = ['Cdr' => compact('conditions','contain','limit','fields','order')];
		$cdrs = $this->Paginator->paginate();

		$this->set(compact('cdrs'));
	}

/**
 * method to download a voice recording directly.
 *
 * @param integer $id
 * @return void
 */
	public function download($id=null) {
		if (!$this->Cdr->exists($id)) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		$recording = $this->Cdr->findById($id);
		$originalTimeStamp = $recording['Cdr']['start_stamp'];

		// generate the path of the recording then pass it to the download_file function in order to retrieve it
		$timeDate = str_replace('-','',substr($originalTimeStamp, 0, strpos($recording['Cdr']['start_stamp'], ' ')));
		$timeHours = substr(str_replace(':','',substr($originalTimeStamp, strpos($originalTimeStamp, ' '))), 1, 6);
		$timeStamp = $timeDate.$timeHours;
		$recordingUrl = DAV_SERVER_HOST.'recordings/'.$recording['Cdr']['dest_num'].'/'.$timeStamp.'.'.$recording['Cdr']['dest_num'].'.'.$recording['Cdr']['src_num'].'.wav';
		// The function will return the filename after either downloading, or finding it on disk
		$fileUrl = download_file($recordingUrl, './files/');
		$fileName = substr(strrchr($fileUrl, '/'), 1);
		// Afterwards, send it to the user for download
		$this->response->file($fileUrl, array('name' => $fileName, 'download' => true));
		return $this->response;
	}

/**
 * account method
 *
 * @return void
 */
	public function account() {

		$cpid = AuthComponent::user('cust_pers_id');
		$caid = AuthComponent::user('cust_acct_id');

		$this->paginate['conditions'] = array('Cdr.account_id' => $caid);

		$this->Cdr->recursive = 0;
		$this->set('cdrs', $this->Paginator->paginate());
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Cdr->recursive = 0;
		$this->set('cdrs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cdr->exists($id)) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		$options = array('conditions' => array('Cdr.' . $this->Cdr->primaryKey => $id));
		$this->set('cdr', $this->Cdr->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cdr->create();
			if ($this->Cdr->save($this->request->data)) {
				$this->Session->setFlash(__('The cdr has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cdr could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Cdr->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cdr->exists($id)) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Cdr->save($this->request->data)) {
				$this->Session->setFlash(__('The cdr has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cdr could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cdr.' . $this->Cdr->primaryKey => $id));
			$this->request->data = $this->Cdr->find('first', $options);
		}
		$customerAccounts = $this->Cdr->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cdr->id = $id;
		if (!$this->Cdr->exists()) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Cdr->delete()) {
			$this->Session->setFlash(__('Cdr deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Cdr was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

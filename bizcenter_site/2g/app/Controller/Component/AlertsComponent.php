<?
App::uses('Component', 'Controller');
App::uses('CakeSession', 'Model/Datasource');

class AlertsComponent extends Component {

	public function alert($message, $heading = null, $type = null) {
		$element = 'alert';
		$params = compact('heading', 'type');
		CakeSession::write('Message.flash', compact('message', 'element', 'params'));
	}

	public function error($message, $heading = null) {
		$this->alert($message, $heading, 'error');
	}

	public function success($message, $heading = null) {
		$this->alert($message, $heading, 'success');
	}

	public function info($message, $heading = null) {
		$this->alert($message, $heading, 'info');
	}
}

<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * CreditCardPayments Controller
 *
 * @property CreditCardPayment $CreditCardPayment
 */
class CreditCardPaymentsController extends AppController {

/**
 * authorization rules
 *
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('add', 'cf', 'cctype'),
		'admin' =>    array('add', 'cf', 'cctype'),
		'normal' =>   array(),
	);

	public $uses = array('CreditCard', 'CreditCardPayment', 'CreditCardTransaction', 'Payment');


/**
 * cctype method
 * @author John Jung
 * @param creditcard type, subtotal
 * @return subtotal + convience charge
 */
	private function cf($cctype = null, $subtotal) {

		switch($cctype)
		{
			case 'Visa':
				$convenience_fee = ceil($subtotal * 3)/100;
				$subtotal += $convenience_fee;
				break;
			case 'Amex':
				$convenience_fee = ceil($subtotal * 3.5)/100;
				$subtotal += $convenience_fee;
				break;
			case 'Discover':
				$convenience_fee = ceil($subtotal * 3)/100;
				$subtotal += $convenience_fee;
				break;
			case 'Mastercard':
				$convenience_fee = ceil($subtotal * 3)/100;
				$subtotal += $convenience_fee;
				break;
			default:
				$convenience_fee = ceil($subtotal * 3)/100;
				$subtotal += $convenience_fee;
				break;
		}

		return round($convenience_fee, 2);

		// check if firstdata accepts this format
		// setlocale(LC_MONETARY, 'en_US');
		// return money_format('%i', $subtotal);


	}

/**
 * cctype method
 * @author John Jung
 * @param creditcard number
 * @return cardtype
 */
	private function cctype($ccnumber = null) {

		$cc = $ccnumber;

		if (preg_match('/^5[1-5]\d{14}$/', $cc) === 1) {
			$card_type = "Mastercard";
		} elseif (preg_match('/^4\d{15}$/', $cc) || preg_match('/^4\d{12}$/', $cc) === 1) {
			 $card_type = "Visa";
		} elseif (preg_match('/^3[47]\d{13}$/', $cc) === 1) {
			 $card_type = "Amex";
		} elseif (preg_match('/^3[68]\d{12}$/', $cc) || preg_match('/^30[0-5]\d{11}$/', $cc) === 1) {
			$card_type = "Diners";
		} elseif (preg_match('/^2131\d{11}$/', $cc) || preg_match('/^1800\d{11}$/', $cc) || preg_match('/^6011\d{12}$/', $cc) === 1) {
			$card_type = "Discover";
		} elseif (preg_match('/^3\d{15}$/', $cc) === 1) {
			$card_type = "Jcb";
		}

		return $card_type;

	}

/**
 * add method
 * @throws NotFoundException
 * @param Authcomponenet cust_acct_id
 * @return void
 */
	public function add($cid = null) {

		if ($this->request->is('post')) {

			$payment = $this->request->data;

			// validation
			// strip commas
			$payment['CreditCardPayment']['total_charge'] = str_replace(",", "", $payment['CreditCardPayment']['total_charge']);

			if (!is_numeric($payment['CreditCardPayment']['total_charge'])) {
				$this->Session->setFlash('The credit card payment amount must be a dollar amount! Example: 1234.56');
				$this->redirect(array('controller' => 'creditCardPayments', 'action' => 'add', $cid));
			}

			$credit_card_id = $payment['CreditCardPayment']['credit_card_id'];

			// find the credit card that it posted
			$account = $this->CreditCard->find('first', array(
				'conditions' => array('CreditCard.id' => $payment['CreditCardPayment']['credit_card_id']),
				'contain' => array(
					'CustomerAccount' => array(
						'fields' => array('id','name'),
						'PrimaryCustomer' => array(
							'fields' => array('id','first_name','last_name')
						)
					)
				)
			));

			// save payment data
			$savePayment = array(
				'CreditCardPayment' => array(
					'card_number' => $account['CreditCard']['card_number'],
					'card_expiration_month' => $account['CreditCard']['card_expiration_month'],
					'card_expiration_year' => $account['CreditCard']['card_expiration_year'],
					'card_security_code' => $account['CreditCard']['card_security_code'],
					'billing_name' => $account['CreditCard']['billing_name'],
					'billing_company' => $account['CreditCard']['billing_company'],
					'billing_address_num' => $account['CreditCard']['billing_address_num'],
					'billing_zip_code' => $account['CreditCard']['billing_zip_code'],
					'billing_address_1' => $account['CreditCard']['billing_address_1'],
					'billing_address_2' => $account['CreditCard']['billing_address_2'],
					'billing_city' => $account['CreditCard']['billing_city'],
					'billing_state' => $account['CreditCard']['billing_state'],
					'billing_country' => $account['CreditCard']['billing_country'],
					'billing_phone_number' => $account['CreditCard']['billing_phone_number'],
					'billing_fax_number' => $account['CreditCard']['billing_fax_number'],
					'billing_email_address' => $account['CreditCard']['billing_email_address'],
					'subtotal' => $payment['CreditCardPayment']['total_charge'],
					'processed' => true,
					'charge_date' => date(DATETIME_FORMAT),
					'card_type' => $this->cctype($account['CreditCard']['card_number']),
					'convenience_fee' => $this->cf($this->cctype($account['CreditCard']['card_number']), $payment['CreditCardPayment']['total_charge'])
				)
			);
			$remail = $savePayment['CreditCardPayment']['billing_email_address'];
			

			$this->CreditCardPayment->create();
			$x = $this->CreditCardPayment->save($savePayment);
			if ($x) {
				$x['CreditCardTransaction']['credit_card_payment_id'] = $x['CreditCardPayment']['id'];

				// send email receipt to finance
				$Email = new CakeEmail();
				$Email->config('default');
				$Email->from(
					array('service@microoffice.com' => 'Micro Office'))
				    ->to(EMAIL_FINANCE)
				    ->subject('Payment has been processed for '.$account['CustomerAccount']['name']."!")
			    	->send(
			    		"Company: ".$account['CustomerAccount']['name']."\r\n".
			    		"Account Contact: ".$savePayment['CreditCardPayment']['billing_name']."\r\n".
			    		"Email: ".$remail."\r\n".
			    		"CreditCard Name: ".$account['CreditCard']['card_nickname']."\r\n".
			    		"CreditCard Number: ".substr($savePayment['CreditCardPayment']['card_number'], 1, 1)."************".substr($savePayment['CreditCardPayment']['card_number'], -4)."\r\n".
			    		"CreditCard Type: ".$savePayment['CreditCardPayment']['card_type']."\r\n".
			    		"Convenience Fee: ".$savePayment['CreditCardPayment']['convenience_fee']."\r\n".
			    		"Subtotal: ".$savePayment['CreditCardPayment']['subtotal']."\r\n".
			    		"Total Charged: ".($savePayment['CreditCardPayment']['subtotal']+$savePayment['CreditCardPayment']['convenience_fee'])."\r\n".
			    		"Company ID: ".$account['CustomerAccount']['id']."\r\n".
			    		"Comments: ".$account['CreditCard']['note']
			    	);

		    	// send email receipt to customer
		    	$Email = new CakeEmail();
		    	$Email->config('default');
		    	$Email->from(
		    		array('service@microoffice.com' => 'Micro Office'))
		    	    ->to($remail)
		    	    ->subject('Thank you! Your Payment has been Processed!')
		        	->send(
		        		"Thank you for your payment!"."\r\n"."\r\n".
		        		"Company: ".$account['CustomerAccount']['name']."\r\n".
		        		"Account Contact: ".$savePayment['CreditCardPayment']['billing_name']."\r\n".
		        		"CreditCard Name: ".$account['CreditCard']['card_nickname']."\r\n".
		        		"CreditCard Number: ".substr($savePayment['CreditCardPayment']['card_number'], 1, 1)."************".substr($savePayment['CreditCardPayment']['card_number'], -4)."\r\n".
		        		"CreditCard Type: ".$savePayment['CreditCardPayment']['card_type']."\r\n".
		        		"Convenience Fee: ".$savePayment['CreditCardPayment']['convenience_fee']."\r\n".
		        		"Subtotal: ".$savePayment['CreditCardPayment']['subtotal']."\r\n".
		        		"Total Charged: ".($savePayment['CreditCardPayment']['subtotal']+$savePayment['CreditCardPayment']['convenience_fee'])."\r\n".
		        		"Comments: ".$account['CreditCard']['note']
		        	);


				$this->Session->setFlash(__('The credit card payment has been processed. You will receive an email with your receipt. '));
				$this->redirect(array('controller' => 'payments', 'action' => 'accounts'));
			}

			// payment could not be saved
			else {

				// first data response
				$transaction_error = @array_pop($this->CreditCardPayment->validationErrors['CreditCardTransaction']);
				
				// set flash message
				$this->Session->setFlash($transaction_error ?: 'The credit card payment could not be processed');
			}
		}

		// CustomerAccount id
		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		// list of CreditCards for this CustomerAccount
		$credit_cards = $this->CreditCard->find('list', array(
			'conditions' => array('CreditCard.account_id' => $caid),
			));

		$this->set('credit_cards', $credit_cards);
		$this->set('cid', $cid);
	}


}
<?php
App::uses('AppController', 'Controller');
/**
 * CreditCardTransactions Controller
 *
 * @property CreditCardTransaction $CreditCardTransaction
 */
class CreditCardTransactionsController extends AppController {

// /**
//  * index method
//  *
//  * @return void
//  */
// 	public function index() {
// 		$this->CreditCardTransaction->recursive = 0;
// 		$this->set('creditCardTransactions', $this->Paginator->paginate());
// 	}

// /**
//  * view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function view($id = null) {
// 		if (!$this->CreditCardTransaction->exists($id)) {
// 			throw new NotFoundException(__('Invalid credit card transaction'));
// 		}
// 		$options = array('conditions' => array('CreditCardTransaction.' . $this->CreditCardTransaction->primaryKey => $id));
// 		$this->set('creditCardTransaction', $this->CreditCardTransaction->find('first', $options));
// 	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CreditCardTransaction->create();
			if ($this->CreditCardTransaction->save($this->request->data)) {
				$this->Session->setFlash(__('The credit card transaction has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The credit card transaction could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->CreditCardTransaction->CustomerAccount->find('list');
		$creditCardPayments = $this->CreditCardTransaction->CreditCardPayment->find('list');
		$receivePayments = $this->CreditCardTransaction->ReceivePayment->find('list');
		$this->set(compact('customerAccounts', 'creditCardPayments', 'receivePayments'));
	}

// /**
//  * edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function edit($id = null) {
// 		if (!$this->CreditCardTransaction->exists($id)) {
// 			throw new NotFoundException(__('Invalid credit card transaction'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->CreditCardTransaction->save($this->request->data)) {
// 				$this->Session->setFlash(__('The credit card transaction has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The credit card transaction could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('CreditCardTransaction.' . $this->CreditCardTransaction->primaryKey => $id));
// 			$this->request->data = $this->CreditCardTransaction->find('first', $options);
// 		}
// 		$customerAccounts = $this->CreditCardTransaction->CustomerAccount->find('list');
// 		$creditCardPayments = $this->CreditCardTransaction->CreditCardPayment->find('list');
// 		$receivePayments = $this->CreditCardTransaction->ReceivePayment->find('list');
// 		$this->set(compact('customerAccounts', 'creditCardPayments', 'receivePayments'));
// 	}

// /**
//  * delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function delete($id = null) {
// 		$this->CreditCardTransaction->id = $id;
// 		if (!$this->CreditCardTransaction->exists()) {
// 			throw new NotFoundException(__('Invalid credit card transaction'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->CreditCardTransaction->delete()) {
// 			$this->Session->setFlash(__('Credit card transaction deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Credit card transaction was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}

// /**
//  * admin_index method
//  *
//  * @return void
//  */
// 	public function admin_index() {
// 		$this->CreditCardTransaction->recursive = 0;
// 		$this->set('creditCardTransactions', $this->Paginator->paginate());
// 	}

}

<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * CreditCards Controller
 *
 * @property CreditCard $CreditCard
 */
class CreditCardsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('history', 'index', 'view', 'add', 'edit', 'delete'),
		'admin' =>    array('history', 'index', 'view', 'add', 'edit', 'delete'),
		'normal' =>   array(),
	);

	public $paginate = array(
		'limit' => 50,
		'order' => array('CreditCard.creation_dt' => 'desc')
	);

/**
 * history method
 * @author John Jung
 * @param $cust_acct_id from AuthComponent
 * @return void
 */
	public function history() {

		$customer_account_id = AuthComponent::user('cust_acct_id');

		$this->CreditCard->recursive = 0;
		$this->paginate['conditions'] = array('CreditCard.account_id' => $customer_account_id);

		$this->set('creditCards', $this->Paginator->paginate());
		$this->set('_serialize', array('creditCards'));

	}

// /**
//  * index method
//  *
//  * @return void
//  */
// 	public function index() {
// 		$this->CreditCard->recursive = 0;
// 		$this->set('creditCards', $this->Paginator->paginate());
// 	}

// /**
//  * view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function view($id = null) {
// 		if (!$this->CreditCard->exists($id)) {
// 			throw new NotFoundException(__('Invalid credit card'));
// 		}
// 		$options = array('conditions' => array('CreditCard.' . $this->CreditCard->primaryKey => $id));
// 		$this->set('creditCard', $this->CreditCard->find('first', $options));
// 	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			

			$saveCreditCard = $this->request->data;

			// 4 digit year
			$saveCreditCard['CreditCard']['card_expiration_year'] = $saveCreditCard['CreditCard']['card_expiration_year']['year'];
			$saveCreditCard['CreditCard']['is_active'] = true;

			$notExpired = true;

			// check if it's an expired credit card
			if ($saveCreditCard['CreditCard']['card_expiration_year'] == date('Y')) {
				if ($saveCreditCard['CreditCard']['card_expiration_month'] <= date('m')) {
					$notExpired = false;
				}
			}

			if ($notExpired == true) {

				// find the credit card that it posted
				$this->uses[] = 'CustomerAccount';
				$account = $this->CustomerAccount->find('first', array(
					'conditions' => array('CustomerAccount.id' => AuthComponent::user('cust_acct_id')),
					'contain' => array('PrimaryCustomer')
				));

				// make sure that you can find the person
				if (!empty($account)) {

					$this->CreditCard->create();
					if ($this->CreditCard->save($saveCreditCard)) {

						// send email to finance
						$Email = new CakeEmail();
						$Email->config('default');
						$Email->from(
							array('service@microoffice.com' => 'Micro Office'))
						    ->to(EMAIL_FINANCE)
						    ->subject('New Credit Card for '.$account['CustomerAccount']['name']."!")
					    	->send(
					    		"Company: ".$account['CustomerAccount']['name']."\r\n".
					    		"Account Contact: ".$account['PrimaryCustomer']['full_name']."\r\n".
					    		"Email: ".$account['PrimaryCustomer']['email']."\r\n".
					    		"CreditCard Number: ".substr($saveCreditCard['CreditCard']['card_number'], 0, 3)."*****".substr($savePayment['CreditCard']['card_number'], -3)."\r\n".
					    		"CreditCard Expiration: ".$saveCreditCard['CreditCard']['card_expiration_month']." / ".$saveCreditCard['CreditCard']['card_expiration_year']."\r\n".
					    		"Company ID: ".AuthComponent::user('cust_acct_id')
					    	);


						$this->Alert->success(__('The credit card has been saved'));
						$this->redirect(array('controller' => 'payments', 'action' => 'accounts'));
					} else {
						$this->Alert->error(__('The credit card could not be saved. Please, try again.'));
					}

				}
			} else {
				$this->Session->setFlash(__('This is an expired credit card'));
			}

		}
		$customerAccounts = $this->CreditCard->CustomerAccount->find('list');
		$customers = $this->CreditCard->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

// /**
//  * edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function edit($id = null) {
// 		if (!$this->CreditCard->exists($id)) {
// 			throw new NotFoundException(__('Invalid credit card'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->CreditCard->save($this->request->data)) {
// 				$this->Session->setFlash(__('The credit card has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The credit card could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('CreditCard.' . $this->CreditCard->primaryKey => $id));
// 			$this->request->data = $this->CreditCard->find('first', $options);
// 		}
// 		$customerAccounts = $this->CreditCard->CustomerAccount->find('list');
// 		$customers = $this->CreditCard->Customer->find('list');
// 		$this->set(compact('customerAccounts', 'customers'));
// 	}

// /**
//  * delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function delete($id = null) {
// 		$this->CreditCard->id = $id;
// 		if (!$this->CreditCard->exists()) {
// 			throw new NotFoundException(__('Invalid credit card'));
// 		}

// 		if ($this->request->is('post') || $this->request->is('put')) {
		
// 			// find credit card
// 			$cc = $this->CreditCard->find('first', array(
// 				'conditions' => array(
// 					'CreditCard.id' => $id
// 				)
// 			));

// 			// set as inactive
// 			$cc['CreditCard']['is_active'] = false;

// 		}

// 		$this->CreditCard->id = $id;
// 		if ($this->CreditCard->saveField('is_active', false)) {
// 			$this->Session->setFlash(__('Credit card deleted'));			
// 		} else {
// 			$this->Session->setFlash(__('Credit card was not deleted'));
// 		}
// 		$this->redirect(array('controller' => 'payments', 'action' => 'accounts'));

// 	}

// /**
//  * admin_index method
//  *
//  * @return void
//  */
// 	public function admin_index() {
// 		$this->CreditCard->recursive = 0;
// 		$this->set('creditCards', $this->Paginator->paginate());
// 	}

// /**
//  * admin_view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_view($id = null) {
// 		if (!$this->CreditCard->exists($id)) {
// 			throw new NotFoundException(__('Invalid credit card'));
// 		}
// 		$options = array('conditions' => array('CreditCard.' . $this->CreditCard->primaryKey => $id));
// 		$this->set('creditCard', $this->CreditCard->find('first', $options));
// 	}

// /**
//  * admin_add method
//  *
//  * @return void
//  */
// 	public function admin_add() {
// 		if ($this->request->is('post')) {
// 			$this->CreditCard->create();
// 			if ($this->CreditCard->save($this->request->data)) {
// 				$this->Session->setFlash(__('The credit card has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The credit card could not be saved. Please, try again.'));
// 			}
// 		}
// 		$customerAccounts = $this->CreditCard->CustomerAccount->find('list');
// 		$customers = $this->CreditCard->Customer->find('list');
// 		$this->set(compact('customerAccounts', 'customers'));
// 	}

// /**
//  * admin_edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_edit($id = null) {
// 		if (!$this->CreditCard->exists($id)) {
// 			throw new NotFoundException(__('Invalid credit card'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->CreditCard->save($this->request->data)) {
// 				$this->Session->setFlash(__('The credit card has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The credit card could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('CreditCard.' . $this->CreditCard->primaryKey => $id));
// 			$this->request->data = $this->CreditCard->find('first', $options);
// 		}
// 		$customerAccounts = $this->CreditCard->CustomerAccount->find('list');
// 		$customers = $this->CreditCard->Customer->find('list');
// 		$this->set(compact('customerAccounts', 'customers'));
// 	}

// /**
//  * admin_delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_delete($id = null) {
// 		$this->CreditCard->id = $id;
// 		if (!$this->CreditCard->exists()) {
// 			throw new NotFoundException(__('Invalid credit card'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->CreditCard->delete()) {
// 			$this->Session->setFlash(__('Credit card deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Credit card was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}
}

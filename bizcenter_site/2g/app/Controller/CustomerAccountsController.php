<?php
App::uses('AppController', 'Controller');
/**
 * CustomerAccounts Controller
 *
 * @property CustomerAccount $CustomerAccount
 */
class CustomerAccountsController extends AppController {

/**
 * authorization rules
 *
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('index', 'view', 'employees', 'add', 'edit', 'deactivate'),
		'admin'   =>  array('index', 'view', 'employees', 'add', 'edit', 'deactivate'),
		'normal'  =>  array('index', 'view', 'employees'),
	);


	public $uses = array('CustomerAccount', 'ProximityCard', 'MoFile');

	public $paginate = array(
			'limit' => '20',
			'recursive' => -1,
	);

/**
 * index method
 * @author John Jung
 * @return void
 */
	public function index() {
		$query = @$this->request->query['q'] ?: '';
		if (strlen($query) >= 4) {
			$query = "%{$query}%";

			$conditions = array(
				'CustomerAccount.is_active' => true,
				'CustomerAccount.visible' => true,
				'CustomerAccount.name ILIKE' => $query,
			);
		}
		$contain = ['PrimaryCustomer' => ['id','full_name','email']];

		$this->Paginator->settings = array('CustomerAccount' => compact('conditions','contain'));
		$customerAccounts = $this->Paginator->paginate();
		
		$this->set('customerAccounts', $customerAccounts);
	}


/**
 * view basic customer information
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (empty($id)) {
			$this->redirect(array('action' => 'view', $this->Auth->user('cust_acct_id')));
		}

		if (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		// fetch customerAccount info
		$customerAccount = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $id),
			'contain' => array('PrimaryCustomer')
		));

		$this->set(compact('customerAccount'));
	}



/**
 * employees method
 * @author John Jung
 * @return void
 */
	public function employees($id = null) {

		// redirect to active user's customerAccount
		if (empty($id)) {
			return $this->redirect(array('action' => $this->action, $this->Auth->user('cust_acct_id')));
		}

		// check that id is valid
		if (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}

		// unless super, only allow users to view their own customerAccount employees
		if ($this->Auth->user('role') < ROLE_SUPER && $this->Auth->user('cust_acct_id') != $id) {
			throw new ForbiddenException('You are not authorized to view this page');
		}

		// fetch customers
		$customers = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $id),
			'contain' => array('Customer', 'PrimaryCustomer')
		));

		$this->set(compact('customers'));
	}


/**
 * Allow a customer to add themselves as a new customer account
 *
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			// reset the customerAccount model
			$this->CustomerAccount->create();

			// copy data
			$data = $this->request->data;

			// autofill data
			$data['PrimaryCustomer']['User']['confirm_password'] =
			$data['PrimaryCustomer']['User']['password'] = $password = random_string();

			$data['PrimaryCustomer']['User']['username'] = $data['PrimaryCustomer']['email'];
			$data['PrimaryCustomer']['nickname'] = $password; // temporarily store password
			$data['CustomerAccount']['is_visible'] = 1;
			if (empty($data['CustomerAccount']['external_name'])) {
				$data['CustomerAccount']['external_name'] = $data['CustomerAccount']['name'];
			}

			// if they are uploading an image, save it
			if ($data['Picture']['name'] != '') {
				$file = $this->MoFile->save(array(
					'filename' => $data['Picture']['data']['name'],
					'mime_type' => $data['Picture']['data']['type'],
					'namespace' => 'img/logo'
				));

				// resize the image before uploading
				// $image = new Imagick($data['Picture']['data']['tmp_name']);
				// $image->resizeImage(128,128, imagick::FILTER_UNDEFINED, 1);
				// $image->writeImage($data['Picture']['data']['tmp_name']);

				$uploadedFileURL = upload_file(DAV_SERVER_HOST.pathFromID($file['MoFile']['id'], $data['Picture']['data']));
				$data['CustomerAccount']['logo_url'] = $uploadedFileURL;
			}

			// atomic save CustomerAccount, Customer, User
			if ($this->CustomerAccount->saveAssociated($data,array('deep' => true))) {

				// save succeeded
				$this->Alert->success('Your customer account has been saved', 'Success');

				// redirect to the customer account that was just created
				return $this->redirect(array('controller' => 'customerAccounts', 'action' => 'view', $this->CustomerAccount->id));
			} else {
				//debug($this->CustomerAccount->validationErrors);
				// save failed
				$this->Alert->error('Your customer account could not be saved. Please, try again.', 'Error');
			}
		}

		// micro office floors (re-indexed for dropdown)
		$buildings = $this->CustomerAccount->Floor->Building->find('all', array('contain' => 'Floor'));
		$floors = array();
		foreach ($buildings as &$building) {
			$floors[$building['Building']['address_1']] = array();
			foreach ($building['Floor'] as $floor) {
				$floors[$building['Building']['address_1']][$floor['id']] = $floor['label'];
			}
		}

		// micro office services (e.g. full-time, virtual)
		$services = $this->CustomerAccount->Service->find('list');
		$this->set(compact('services', 'floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		// fallback to active user's id
		if (empty($id)) {
			$this->redirect(array('action' => 'edit', $this->Auth->user('cust_acct_id')));
		} elseif (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			// pointer to data
			$data =& $this->request->data;

			// limit fields saved to these
			$fieldList = 	array(
				'id',
				'name',
				'primary_contact_cust_pers_id',
				'external_name',
				'description',
				'web_site',
				'visible',
				'pkg_notify_email'
			);

			if ($data['Picture']['file']['error'] == UPLOAD_ERR_OK) {
				// save logo url
				$url = $this->CustomerAccount->uploadFile(
					$data['Picture']['file'],
					'company_logos',
					$data['CustomerAccount']['id']
				);
				if (!empty($url)) {
					$data['CustomerAccount']['logo_url'] = $url;
					$fieldList[] = 'logo_url';
				}
			}

			// save the customerAccount information
			if ($this->CustomerAccount->save($this->request->data, compact('fieldList'))) {
				$this->Alert->success(__('The customer account has been saved'));
			} else {
				// debug($this->CustomerAccount->validationErrors);
				$this->Alert->error(__('The customer account could not be saved. Please, try again.'));
			}
		}


		// display the image before/after editing
		$this->request->data = 
		$customerAccount = 
		$this->CustomerAccount->find('first',array('conditions'=>array('CustomerAccount.id'=>$id)));

		$pc = $this->request->data['CustomerAccount']['primary_contact_cust_pers_id'];
		$this->set('picturePath', $customerAccount['CustomerAccount']['logo_url'] ?: '/img/no-profile-pic.png');

		$customers = $this->CustomerAccount->Customer->find('list', 
			array(
				'conditions' => array('Customer.cust_acct_id' => $id),
				'fields' => array('Customer.id', 'Customer.full_name')
			)
		);
		$services = $this->CustomerAccount->Service->find('list');
		$floors = $this->CustomerAccount->Floor->find('list');
		$this->set(compact('customers', 'services', 'floors'));
		$this->set('pc', $pc);
	}

/**
 * deactivate method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function deactivate($id = null) {
		$this->CustomerAccount->id = $id;
		if (!$this->CustomerAccount->exists()) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CustomerAccount->deactivate()) {
			$this->Session->setFlash(__('Customer account deactivated'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Customer account was not deactivated'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CustomerAccount->recursive = 0;
		$this->set('customerAccounts', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		$options = array('conditions' => array('CustomerAccount.' . $this->CustomerAccount->primaryKey => $id));
		$this->set('customerAccount', $this->CustomerAccount->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CustomerAccount->create();
			if ($this->CustomerAccount->save($this->request->data)) {
				$this->Session->setFlash(__('The customer account has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer account could not be saved. Please, try again.'));
			}
		}
		$customers = $this->CustomerAccount->Customer->find('list');
		$services = $this->CustomerAccount->Service->find('list');
		$floors = $this->CustomerAccount->Floor->find('list');
		$this->set(compact('customers', 'services', 'floors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->CustomerAccount->save($this->request->data)) {
				$this->Session->setFlash(__('The customer account has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer account could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CustomerAccount.' . $this->CustomerAccount->primaryKey => $id));
			$this->request->data = $this->CustomerAccount->find('first', $options);
		}
		$customers = $this->CustomerAccount->Customer->find('list');
		$services = $this->CustomerAccount->Service->find('list');
		$floors = $this->CustomerAccount->Floor->find('list');
		$this->set(compact('customers', 'services', 'floors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CustomerAccount->id = $id;
		if (!$this->CustomerAccount->exists()) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CustomerAccount->delete()) {
			$this->Session->setFlash(__('Customer account deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Customer account was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

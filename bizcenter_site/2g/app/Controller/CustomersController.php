<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 */
class CustomersController extends AppController {

/**
 * Allow someone to join without signing in.
 *
 * @return void
 */
	public function beforeFilter() {
		$this->Auth->allow('join');
	}

/**
 * authorization rules
 *
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('search','dashboard','view','add','edit','invite','deactivate','invite','messages'),
		'admin' =>    array('search','dashboard','view','add','edit','invite','deactivate','invite','messages'),
		'normal' =>   array('search','dashboard','view','edit','messages'),
	);

/**
 * Pagination rules
 *
 * @var array
 */
	public $paginate = array(
		'limit' => 20,
		'recursive' => -1
	);

/**
 * This controller uses these models
 *
 * @var array
 */
	public $uses = array('Customer','CustomerAccount','Event','User', 'Notification', 'Mailbox', 'MailPackage', 'MoFile');

/**
 * search for a customer by name
 * @return void
 */
	public function search() {
		
		// check if query is set
		$query = @$this->request->query['q'] ?: '';

		// check if length meets threshold
		if (strlen($query) >= 4) {

			// wildcard
			$query = "%{$query}%";

			// filter conditions
			$conditions = array(
				'Customer.is_active' => true,
				'Customer.visibility' => 'visible',
				'Customer.full_name ILIKE' => $query,
			);

		} else {
			$conditions = ['Customer.id' => -1];
		}

		$contain = ['CustomerAccount' => ['fields' => ['id','name','is_active']]];
		$fields = ['id','email','first_name','last_name','full_name','work_phone_number'];

		$this->Paginator->settings = ['Customer' => compact('conditions','contain','limit','fields','order')];
		$customers = $this->Paginator->paginate();

		$this->set(compact('customers'));
	}

/**
 * dashboard method
 * @return void
 */
	public function dashboard() {

		// set user id
		$id = $this->Auth->user('id');

		// fetch user info
		$user = $this->Customer->find('first', array(
			'conditions' => array('Customer.user_id' => $id),
			'contain' => array(
				'CustomerAccount' => array(
					'MailPackage' => array(
						'order' => 'MailPackage.date DESC',
						'limit' => 1
					),
					'Mailbox.bool_mail_present',
					'Mailbox.mailbox_number',
					'IpAddress',
					'Floor.OfficeManager'
				),
			)
		));
		$roomMinutesRemaining = $this->CustomerAccount->roomMinutesRemaining($this->Auth->user('cust_acct_id'));
		$user['CustomerAccount']['room_minutes_remaining'] = $roomMinutesRemaining;
		$events = $this->Event->find('all', array(
			'limit' => 10,
			'orderBy' => 'Event.Modified DESC',
		));

		$last_logged_in = $this->Auth->user('last_logged_in');

		// get relevant notifications
		$this->uses[] = 'Notification';
		$notifications = $this->getNotifications($this->getUserFloorId());

		$alert = !empty($notifications) ? $notifications[0]['Notification']['name'] : null;
		$this->set(compact('notifications','alert','user','events','last_logged_in'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @author Aron Maeng
 * @return void
 */
	public function view($id = null) {
		if (empty($id)) {
			$this->redirect(array('action' => 'view', $this->Auth->user('cust_pers_id')));
		}

		// fallback to auth user
		$this->Customer->id = $id = empty($id) ? $this->Auth->user('cust_pers_id') : $id;

		// invalid customer check
		if (!$this->Customer->exists($id)) {
			throw new NotFoundException(__('Invalid customer'));
		}

		// fetch the customer
		$customer = $this->Customer->find('first', array(
			'conditions' => array('Customer.id' => $id),
			'contain' => array('CustomerAccount')
		));
		// Retrieve the profile picture then set it for displaying or point to the default for no picture.
		// if ($customer['Customer']['profile_picture_url'] != null) {
		// 	$remoteUrl = $customer['Customer']['profile_picture_url'];
		// 	//debug($remoteUrl);
		// 	// $downloadedFile = download_file($remoteUrl, './files/'.$customer['Customer']['id'].'profile');
		// 	$finfo = finfo_open(FILEINFO_MIME_TYPE);
		// 	$fileType = finfo_file($finfo, $downloadedFile);
		// 	if (strstr($fileType, '/', true) != 'image') {
		// 		$this->set('picturePath', 'no-profile-pic.png');
		// 	} else {
		// 		$fileExtension = substr($fileType, 1 + strpos($fileType, '/'));
		// 		$success = rename($downloadedFile, $downloadedFile.'.'.$fileExtension);
		// 		if (!$success) {
		// 			$this->set('picturePath', 'no-profile-pic.png');
		// 		} else {
		// 			$this->set('picturePath', $downloadedFile.'.'.$fileExtension);
		// 		}
		// 	}
		// } else {
		// 	$this->set('picturePath', 'no-profile-pic.png');
		// }
		$this->set('customer', $customer);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			// reset the customer model
			$this->Customer->create();

			$data = $this->request->data;
			//debug($data);exit;

			if (isset($data['Picture'])) {
				// save the record of the file
				$file = $this->MoFile->save(array(
					'filename' => $data['Picture']['data']['name'],
					'mime_type' => $data['Picture']['data']['type'],
					'namespace' => 'img/logo'
				));
				// resize the image
				// $image = new Imagick($data['Picture']['data']['tmp_name']);
				// $image->resizeImage(128,128, imagick::FILTER_UNDEFINED, 1);
				// $image->writeImage($data['Picture']['data']['tmp_name']);

				// upload the file then save the file's url within the customer entry
				$remoteUrl = DAV_SERVER_HOST.pathFromID($file['MoFile']['id']);
				$uploadedFileUrl = upload_file($remoteUrl, $this->data['Picture']['data']);
				$this->data['Customer']['profile_picture_url'] = $uploadedFileUrl;
			}

			if ($this->Customer->save($this->request->data)) {
				$this->Session->setFlash(__('The customer has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Customer->CustomerAccount->find('list');
		$users = $this->Customer->User->find('list');
		$this->set(compact('customerAccounts', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		// fallback redirect
		if (empty($id)) {
			$this->redirect(array('action' => 'edit', $this->Auth->user('cust_pers_id')));
		}

		// sanity check
		if (!$this->Customer->exists($id)) {
			throw new NotFoundException(__('Invalid customer'));
		}

		// check authorization
		if ($this->Auth->user('cust_pers_id') != $id) {
			if ($this->Auth->user('role') < ROLE_ADMIN) {
				// TODO check if customer is in customerAccount
				// throw new ForbiddenException('You are not authorized to access this action');
				$this->Alert->error('You are not authorized to edit another user\'s account information');
				$this->redirect(array('controller' => 'customerAccounts', 'action' => 'employees'));
			}
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			// point to data
			$data =& $this->request->data;

			// upload the file then save the url within the cust_pers_tbl row entry
			if ($data['Picture']['data']['error'] == UPLOAD_ERR_OK) {
				// save logo url
				$url = $this->CustomerAccount->uploadFile(
					$data['Picture']['data'],
					'profile_pics',
					$data['Customer']['id']
				);
				if (!empty($url)) {
					$data['Customer']['profile_picture_url'] = $url;
				}
			}
			$this->Customer->User->id = $data['User']['id'];
			$this->Customer->User->saveField('role', $data['User']['role']);

			// save the customer info
			if ($this->Customer->save($data)) {

				// refetch saved customer information
				$user = $this->User->find('first', array(
					'conditions' => array('User.id' => $this->Auth->user('id')),
					'fields' => array('id','username','last_logged_in','role'),
					'contain' => array(
						'Customer' => array(
							'fields' => array('id','cust_acct_id','is_active')
						)
					)
				));
				// reload variables into AuthComponent
				$this->Auth->login(array(
					'id' => $user['User']['id'],
					'username' => $user['User']['username'],
					'role' => $user['User']['role'],
					'cust_pers_id' => $user['Customer']['id'],
					'cust_acct_id' => $user['Customer']['cust_acct_id'],
					'last_logged_in' => $user['User']['last_logged_in'],
				));

				$this->Session->setFlash(__('The customer has been saved'));
				$this->redirect(array('controller' => 'customers', 'action' => 'edit', $this->Auth->user('cust_pers_id')));
			} else {
				$this->Session->setFlash(__('The customer could not be saved. Please, try again.'));
			}
		}
		// get request
		else {
			$this->request->data = $this->Customer->find('first', array(
				'conditions' => array('Customer.id' => $id),
				'contain' => array('User'))
			);
			unset($this->request->data['Customer']['password']);
		}

	}

/**
 * deactivate method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function deactivate($id = null) {
		$this->request->onlyAllow('post', 'delete');

		// delete self is never allowed
		if ($this->Auth->user('cust_pers_id') == $id) {
			throw new ForbiddenException("You are not allowed deactivate yourself");
		}

		// check that customer id is valid
		$this->Customer->id = $id;
		if (!$this->Customer->exists()) {
			throw new NotFoundException(__('Invalid customer'));
		}

		// get logged in user's role
		$active_role = $this->Auth->user('role');

		if ($active_role != ROLE_SUPER) {

			// get deactivate target's role
			$target = $this->Customer->find('first', array(
				'conditions' => array('Customer.id' => $id),
				'fields' => array(),
				'contain' => 'User.role'
			));
			$target_role = $target['User']['role'];

			// check that the target_role < active_role
			if ($target_role > $active_role) {
				throw new ForbiddenException('You are not authorized to deactivate that person');
			}
		}

		// deactivate the customer
		if ($this->Customer->deactivate()) {
			$this->Alert->success(__('Customer deactivated'));
			return $this->redirect(array('controller' => 'customerAccounts', 'action' => 'employees'));
		}

		$this->Alert->error(__('Customer was not deactivated'));
		return $this->redirect(array('controller' => 'customerAccounts', 'action' => 'employees'));
	}

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function makeContact() {

		// $this->autoRender('false');
		if ($this->request->is('post') || $this->request->is('put')) {

			$email = $this->request->data['Customer'];

			// send the email
			$Email = new CakeEmail('noreply');
			$Email->from(array($email['email'] => 'Prospective Customer from Contact Us Page'));
			$Email->to('jj@pith-studio.com');
			$Email->subject('Contact Us Form Email');
			$Email->send("Name: ".$email['name']."\r\n"."Email: ".$email['email']."\r\n"."Phone Number: ".$email['phone']."\r\n"."Comments: ".$email['comments']);
		}
		$this->Session->setFlash(__('Email sent. We\'ll get back to you soon!'));
		$this->redirect(array('controller' => 'pages',  'action' => 'display', 'contact'));
	}


/**
 * Messages and notifications
 *
 * @return void
 */
	public function messages() {

		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		$mailp = $this->MailPackage->find('all', array(
			'conditions' => array('MailPackage.customer_id' => $caid),
			'recursive' => -1
		));

		$this->set(compact('mailp'));
	}

/**
 * Invite a new customer to the current user's customerAccount
 *
 * @return void
 */
	public function invite() {

		$cust_acct_id = $this->Auth->user('cust_acct_id');

		if ($this->request->is('post')) {
			$data = $this->request->data;

			// autofill customer data
			$data['Customer']['cust_acct_id'] = $cust_acct_id;
			$data['Customer']['is_active'] = true;
			$data['User']['username'] = $data['Customer']['email'];

			// autofill user data
			$data['User']['role'] = $data['User']['is_admin'] ? ROLE_ADMIN : ROLE_NORMAL;
			$data['User']['password'] =
			$data['User']['confirm_password'] = random_string(20);

			
			// $this->uses[] = 'User';
			$this->uses[] = 'Token';

			// save the customer and user
			$this->Customer->create();
			if ($this->Customer->saveAssociated($data, array('deep' => true))) {
				$this->Token->save(array('Token' => array(
					'user_id' => $this->Customer->User->id,
					'type' => 'customerJoin'
				)));
				$this->Alert->success('New user has been invited');
				$this->request->data = array();
			} else {
				$this->Alert->error('New user could not be invited');
			}
		}

		$customers = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'Customer' => array(
					'conditions' => array(
						array('OR' => array(
							array('Customer.is_active' => false),
							array('Customer.is_active' => null),
						))
					),
					'fields' => array('id','email','full_name','is_active'),
					'User' => array(
						'fields' => array('id', 'username', 'last_logged_in'),
						// 'conditions' => array('User.last_logged_in' => null)
					)
				)
			)
		));
		// debug($customers);
		$this->set(compact('customers'));
	}

}

<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * ElectronicChecks Controller
 *
 * @property ElectronicCheck $ElectronicCheck
 */
class ElectronicChecksController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('view', 'add', 'edit', 'delete'),
		'admin' =>    array('view', 'add', 'edit', 'delete'),
		'normal' =>   array(),
	);

	public $uses = array('BankAccount', 'ElectronicCheck', 'Payment');	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ElectronicCheck->recursive = 0;
		$this->set('electronicChecks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ElectronicCheck->exists($id)) {
			throw new NotFoundException(__('Invalid electronic check'));
		}
		$options = array('conditions' => array('ElectronicCheck.' . $this->ElectronicCheck->primaryKey => $id));
		$this->set('electronicCheck', $this->ElectronicCheck->find('first', $options));
	}

/**
 * add method
 * @throws NotFoundException
 * @param Authcomponenet cust_acct_id
 * @return void
 * @return void
 */
	public function add($bid = null) {

		if ($this->request->is('post')) {

			$payment = $this->request->data;
			// find the bank account that it posted
			$account = $this->BankAccount->find('first', array(
				'conditions' => array('BankAccount.id' => $payment['ElectronicCheck']['bankaccount']),
				'contain' => array('CustomerAccount' => 'PrimaryCustomer')
			));
			$this->request->data('ElectronicCheck.remote_addr', $this->request->clientIp());

			$savePayment = array('ElectronicCheck' => array(
				'bank_account_name' => substr($account['BankAccount']['bank_account_nickname'], 0, 22),
				'bank_account_type' => $account['BankAccount']['bank_account_type'],
				'routing_number' => $account['BankAccount']['bank_routing_number'],
				'account_number' => $account['BankAccount']['bank_account_number'],
				'amount' => $payment['ElectronicCheck']['amount'],
				'debit_date' => $payment['ElectronicCheck']['debit_date'],
				'notes' => $payment['ElectronicCheck']['notes'],
				'remote_addr' => $this->request->clientIp(),
				'remote_host' => gethostbyaddr($_SERVER['REMOTE_ADDR'])
			));
			$remail = $account['CustomerAccount']['PrimaryCustomer']['email'];
			$this->ElectronicCheck->create();

			if ($this->ElectronicCheck->save($savePayment)) {

				$msg = "Company: ".$account['CustomerAccount']['name']."\r\n".
					"Primary Contact: ".$account['CustomerAccount']['PrimaryCustomer']['first_name']." ".$account['CustomerAccount']['PrimaryCustomer']['last_name']."\r\n".
					"Email: ".$remail."\r\n".
					"Bank Account: ".$account['BankAccount']['bank_account_nickname']."\r\n".
					"Bank Account Type: ".$account['BankAccount']['bank_account_type']."\r\n".
					"Bank Account Number: "."*****".substr($account['BankAccount']['bank_account_number'], -4)."\r\n".
					"Bank Routing Number: ".$account['BankAccount']['bank_routing_number']."\r\n".
					"Payment Amount: ".$savePayment['ElectronicCheck']['amount']."\r\n".
					"Debit Date: ".CakeTime::nice($savePayment['ElectronicCheck']['debit_date'])."\r\n".
					"Comments: ".$account['BankAccount']['notes'];
				// debug($msg);
				// exit;

				// send email receipt to customer
				$email_to_customer = new CakeEmail();
				$email_to_customer->config('default');
				// $email_to_customer->from(array('hello@pith-studio.com' => 'Micro Office')); // FIXME
				$email_to_customer->to($remail)
					->subject('Your payment has been processed')
					->send($msg);

				// send email to Jin
				$email_to_jin = new CakeEmail();
				$email_to_jin->config('default');
				// $email_to_jin->from(array('hello@pith-studio.com' => 'Micro Office')); // FIXME
				$email_to_jin->to('jin@microoffice.com')
					->subject('Payment made by ' . $account['CustomerAccount']['name'])
					->send($msg);

				$this->Alert->success(__('Your payment has been processed successfully'));
				$this->redirect(array('controller' => 'payments', 'action' => 'accounts'));
			} else {
				// debug($this->ElectronicCheck->validationErrors);
				// exit;
				// $errs = $this->ElectronicCheck->validationErrors;
				$this->Alert->error(__('Payment could not be processed'));
				// $this->redirect(array('controller' => 'electronicChecks', 'action' => 'add', $bid));
			}
		}
		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		$bankaccounts = $this->BankAccount->find('list', array(
			'conditions' => array('BankAccount.account_id' => $caid),
		));
		
		$this->set('bankaccounts', $bankaccounts);
		$this->set('bid', $bid);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ElectronicCheck->exists($id)) {
			throw new NotFoundException(__('Invalid electronic check'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ElectronicCheck->save($this->request->data)) {
				$this->Session->setFlash(__('The electronic check has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The electronic check could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ElectronicCheck.' . $this->ElectronicCheck->primaryKey => $id));
			$this->request->data = $this->ElectronicCheck->find('first', $options);
		}
		$customerAccounts = $this->ElectronicCheck->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ElectronicCheck->id = $id;
		if (!$this->ElectronicCheck->exists()) {
			throw new NotFoundException(__('Invalid electronic check'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ElectronicCheck->delete()) {
			$this->Session->setFlash(__('Electronic check deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Electronic check was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ElectronicCheck->recursive = 0;
		$this->set('electronicChecks', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ElectronicCheck->exists($id)) {
			throw new NotFoundException(__('Invalid electronic check'));
		}
		$options = array('conditions' => array('ElectronicCheck.' . $this->ElectronicCheck->primaryKey => $id));
		$this->set('electronicCheck', $this->ElectronicCheck->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ElectronicCheck->create();
			if ($this->ElectronicCheck->save($this->request->data)) {
				$this->Session->setFlash(__('The electronic check has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The electronic check could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->ElectronicCheck->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ElectronicCheck->exists($id)) {
			throw new NotFoundException(__('Invalid electronic check'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ElectronicCheck->save($this->request->data)) {
				$this->Session->setFlash(__('The electronic check has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The electronic check could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ElectronicCheck.' . $this->ElectronicCheck->primaryKey => $id));
			$this->request->data = $this->ElectronicCheck->find('first', $options);
		}
		$customerAccounts = $this->ElectronicCheck->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ElectronicCheck->id = $id;
		if (!$this->ElectronicCheck->exists()) {
			throw new NotFoundException(__('Invalid electronic check'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ElectronicCheck->delete()) {
			$this->Session->setFlash(__('Electronic check deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Electronic check was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

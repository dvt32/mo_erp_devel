<?php
App::uses('AppController', 'Controller');
/**
 * Employeeses Controller
 *
 * @property Employees $Employees
 */
class EmployeesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Employee->recursive = 0;
		$this->set('Employeeses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid Employee'));
		}
		$options = array('conditions' => array('Employees.' . $this->Employees->primaryKey => $id));
		$this->set('employees', $this->Employees->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Employees->create();
			if ($this->Employees->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		}
		$floors = $this->Employees->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Employees->exists($id)) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Employees->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Employees.' . $this->Employees->primaryKey => $id));
			$this->request->data = $this->Employees->find('first', $options);
		}
		$floors = $this->Employees->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Employees->id = $id;
		if (!$this->Employees->exists()) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Employees->delete()) {
			$this->Session->setFlash(__('Network switch deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Network switch was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Employees->recursive = 0;
		$this->set('Employeeses', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Employees->exists($id)) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		$options = array('conditions' => array('Employees.' . $this->Employees->primaryKey => $id));
		$this->set('Employees', $this->Employees->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Employees->create();
			if ($this->Employees->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		}
		$floors = $this->Employees->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Employees->exists($id)) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Employees->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Employees.' . $this->Employees->primaryKey => $id));
			$this->request->data = $this->Employees->find('first', $options);
		}
		$floors = $this->Employees->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Employees->id = $id;
		if (!$this->Employees->exists()) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Employees->delete()) {
			$this->Session->setFlash(__('Network switch deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Network switch was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

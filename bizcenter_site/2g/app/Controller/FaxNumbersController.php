<?php
App::uses('AppController', 'Controller');
/**
 * FaxNumbers Controller
 *
 * @property FaxNumber $FaxNumber
 */
class FaxNumbersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FaxNumber->recursive = 0;
		$this->set('faxNumbers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FaxNumber->exists($id)) {
			throw new NotFoundException(__('Invalid fax number'));
		}
		$options = array('conditions' => array('FaxNumber.' . $this->FaxNumber->primaryKey => $id));
		$this->set('faxNumber', $this->FaxNumber->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FaxNumber->create();
			if ($this->FaxNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The fax number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax number could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->FaxNumber->CustomerAccount->find('list');
		$customers = $this->FaxNumber->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FaxNumber->exists($id)) {
			throw new NotFoundException(__('Invalid fax number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The fax number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax number could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxNumber.' . $this->FaxNumber->primaryKey => $id));
			$this->request->data = $this->FaxNumber->find('first', $options);
		}
		$customerAccounts = $this->FaxNumber->CustomerAccount->find('list');
		$customers = $this->FaxNumber->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FaxNumber->id = $id;
		if (!$this->FaxNumber->exists()) {
			throw new NotFoundException(__('Invalid fax number'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxNumber->delete()) {
			$this->Session->setFlash(__('Fax number deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax number was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FaxNumber->recursive = 0;
		$this->set('faxNumbers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FaxNumber->exists($id)) {
			throw new NotFoundException(__('Invalid fax number'));
		}
		$options = array('conditions' => array('FaxNumber.' . $this->FaxNumber->primaryKey => $id));
		$this->set('faxNumber', $this->FaxNumber->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FaxNumber->create();
			if ($this->FaxNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The fax number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax number could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->FaxNumber->CustomerAccount->find('list');
		$customers = $this->FaxNumber->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FaxNumber->exists($id)) {
			throw new NotFoundException(__('Invalid fax number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The fax number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax number could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxNumber.' . $this->FaxNumber->primaryKey => $id));
			$this->request->data = $this->FaxNumber->find('first', $options);
		}
		$customerAccounts = $this->FaxNumber->CustomerAccount->find('list');
		$customers = $this->FaxNumber->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FaxNumber->id = $id;
		if (!$this->FaxNumber->exists()) {
			throw new NotFoundException(__('Invalid fax number'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxNumber->delete()) {
			$this->Session->setFlash(__('Fax number deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax number was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

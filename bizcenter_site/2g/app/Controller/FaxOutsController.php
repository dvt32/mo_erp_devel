<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');

/**
 * FaxOuts Controller
 *
 * @property FaxOut $FaxOut
 */
class FaxOutsController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow('faxageResponse');
	}

/**
 * authorization rules
 *
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('history', 'view', 'add', 'faxageResponse', 'edit', 'delete', 'getaNumber'),
		'admin' =>    array('history', 'view', 'add', 'faxageResponse', 'edit', 'delete', 'getaNumber'),
		'normal' =>   array('history', 'view', 'add', 'faxageResponse', 'edit', 'delete', 'getaNumber'), // getaNumber is blocked with a specific message.
	);

	public $uses = array('FaxOut', 'CustomerAccount', 'FaxNumber', 'MoFile', 'User', 'Customer');

	public $paginate = array(
		'limit' => '50',
		'order' => 'FaxOut.creation_dt DESC',
	);

/**
 * updateJob method
 * @author John Jung
 * @param $job_id
 * @return faxage array
 */
	private function updateJob($jobid) {
		// Faxage Creds
		$username = FAXAGE_USERNAME;
		$company = FAXAGE_COMPANYID;
		$password = FAXAGE_PSWD;

		$info = array(
			'username' => FAXAGE_USERNAME,
			'company' => FAXAGE_COMPANYID,
			'password' => FAXAGE_PSWD,
			'operation' => 'status',
			'jobid' => $jobid,
			'pagecount' => 1
		);
		$info = http_build_query($info);

		// SENDS TO FAXAGE
		$HttpSocket = new HttpSocket();

		// sends the post request
		$results = $HttpSocket->post(
		    'https://www.faxage.com/httpsfax.php',
		    $info
		);

		$line = $results->body();
		$update = array();

		foreach (explode("\t", $line) as $chunk) {
			array_push($update, $chunk);
		}

		$faxage = array();

		$faxage['Faxage']['job_id'] = @$update[0];
		$faxage['Faxage']['comm_id'] = @$update[1];
		$faxage['Faxage']['destname'] = @$update[2];
		$faxage['Faxage']['destnum'] = @$update[3];
		$faxage['Faxage']['shortstatus'] = @$update[4];
		$faxage['Faxage']['longstatus'] = @$update[5];
		$faxage['Faxage']['sendtime'] = @$update[6];
		$faxage['Faxage']['completetime'] = @$update[7];
		$faxage['Faxage']['xmittime'] = @$update[8];
		$faxage['Faxage']['pagecount'] = @$update[9];

		return $faxage;
	}


/**
 * history method
 * @author John Jung
 * @param $cust_acct_id from AuthComponent
 * @return void
 */
	public function history() {

		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');
		// filter conditions
		$conditions = array(
			'FaxOut.account_id' => $caid,
			'FaxOut.person_id' => $cpid
		);
		// fetch documents
		$this->Paginator->settings = array('FaxOut' => compact('conditions','contain'));
		$faxOuts = $this->Paginator->paginate();

		foreach ($faxOuts as $key => $value) {

			$jobstatus = $this->updateJob($value['FaxOut']['job_id']);
			// if toSave is true, update the db
			$toSave = 0;

			if ($value['FaxOut']['comm_id'] != $jobstatus['Faxage']['comm_id']) { 
				$value['FaxOut']['comm_id'] = $jobstatus['Faxage']['comm_id'];
				$toSave = 1;
			}
			if ($value['FaxOut']['short_status'] != $jobstatus['Faxage']['shortstatus']) { 
				$value['FaxOut']['short_status'] = $jobstatus['Faxage']['shortstatus'];
				$toSave = 1;
			}
			if ($value['FaxOut']['long_status'] != $jobstatus['Faxage']['longstatus']) { 
				$value['FaxOut']['long_status'] = $jobstatus['Faxage']['longstatus'];
				$toSave = 1;
			}
			if ($value['FaxOut']['complete_time'] != $jobstatus['Faxage']['completetime']) { 
				$value['FaxOut']['complete_time'] = $jobstatus['Faxage']['completetime'];
				$toSave = 1;
			}
			if ($value['FaxOut']['xmit_time'] != $jobstatus['Faxage']['xmittime']) { 
				$value['FaxOut']['xmit_time'] = $jobstatus['Faxage']['xmittime'];
				$toSave = 1;
			}
			if ($value['FaxOut']['page_count'] != $jobstatus['Faxage']['pagecount']) { 
				$value['FaxOut']['page_count'] = $jobstatus['Faxage']['pagecount'];
				$toSave = 1;
			}

			// if toSave is true, resave it
			if ($toSave === 1) {
				$value['FaxOut']['complete_time'] = null;
				$value['FaxOut']['xmit_time'] = null;
				if ($this->FaxOut->save($value)) {
					// $this->Session->setFlash(__('Fax Statuses Updated'));
				} else {
					$this->Session->setFlash(__('Fax Status Updates failed'));
				}
			}

		}

		$this->set('faxOuts', $faxOuts);
	}




/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FaxOut->recursive = 0;
		$this->set('faxOuts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FaxOut->exists($id)) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		$options = array('conditions' => array('FaxOut.' . $this->FaxOut->primaryKey => $id));
		$this->set('faxOut', $this->FaxOut->find('first', $options));
	}


/**
 * add method
 *
 * @return void
 */
	public function add($file_id = null) {

		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');
		// Get basic customer information
		$cust = $this->CustomerAccount->find('first', array(
				'conditions' => array('CustomerAccount.id' => $caid),
				'recursive' => 0
			)
		);

		// Check if they have a private fax number
		$faxnumber = $this->FaxNumber->find('first', array('conditions' => array('FaxNumber.account_id' => $caid)));
		
		// if they have a private fax number, use that
		if (!empty($faxnumber)) {
			$faxnumber = $faxnumber['FaxNumber']['fax_number'];
		} else {
			$faxnumber = PUBLIC_FAX_NUMBER;
		}

		if ($this->request->is('post')) {

			$faxing = $this->request->data;
			// debugged
			// array(
			// 	'subfile' => 'fax.pdf',
			// 	'FaxOut' => array(
			// 		'recipient_name' => 'John test',
			// 		'fax_number' => '6462531258',
			// 		'send_email_alert' => '0',
			// 		'short_status' => 'pending',
			// 		'file' => array(
			// 			'name' => 'fax.pdf',
			// 			'type' => 'application/pdf',
			// 			'tmp_name' => '/tmp/phpwPpHdd',
			// 			'error' => (int) 0,
			// 			'size' => (int) 1530569
			// 		)
			// 	)
			// )


			// START - SEND FAX RAW PHP

			$url = "https://www.faxage.com/httpsfax.php";
			$src = $faxnumber;
			$dst = $faxing['FaxOut']['fax_number'];
			$file = $faxing['FaxOut']['file'];

			// Faxage creds
			$username = FAXAGE_USERNAME;
			$company = FAXAGE_COMPANYID;
			$password = FAXAGE_PSWD;
			$recipname = $faxing['FaxOut']['recipient_name'];
			$url_callback = urlencode("http://puresource.microoffice.com/faxouts/faxageResponse");

			$tagname = $cust['CustomerAccount']['name'];
			$tagnumber = $faxnumber;

			// Handling the file
			$fileInfo = $file;
			$data = array(
				'filename' => $fileInfo['name'],
				'mime_type' => $fileInfo['type'],
				'namespace' => 'fax/out'
			);

			if ($file['size'] == 0) {
				$this->Session->setFlash(__('The fax could not be sent. No file was attached. '));
			} else {

				// Upload the file to the DAV Server
				// $file = $this->MoFile->save($data);
				// upload_file(DAV_SERVER_HOST.pathFromId($file['MoFile']['id']), $fileInfo);
				// $this->request->data['FaxOut']['file_id'] = $file['MoFile']['id'];
				$filetmpname = $file['tmp_name'];
				$fh = fopen($filetmpname, "r");
				$fdata = fread($fh, filesize($filetmpname));
				fclose($fh);

				$filename = $faxing['FaxOut']['file']['name'];
				$filename = urlencode($filename);

				$b64data = base64_encode($fdata);

				$post_data = "username=$username&company=$company&password=$password&";
				$post_data .= "callerid=$src&faxno=$dst&recipname=$recipname&operation=sendfax&";
				$post_data .= "tagname=$tagname&tagnumber=$tagnumber&faxfilenames[0]=$filename&";
				$post_data .= "faxfiledata[0]=$b64data&url_notify=$url_callback";


				// SENDS TO FAXAGE
				$HttpSocket = new HttpSocket();

				// sends the post request
				$results = $HttpSocket->post(
				    'https://www.faxage.com/httpsfax.php',
				    $post_data
				);

				// gets the jobID
				preg_match('/[0-9]+/' ,$results->body, $jobid);
				// debug($results->body);

				$this->request->data['FaxOut']['job_id'] = $jobid[0];
				$this->request->data['FaxOut']['send_time'] = $results->headers['Date'];
				// END - SEND FAX RAW PHP

				if ($this->request->data['FaxOut']['send_email_alert']) {
					$remail = $this->Customer->find('first', array(
						'conditions' => array('Customer.id' => AuthComponent::user('cust_pers_id'))));
					$remail = $remail['Customer']['email'];
				// if faxing-debug is on ("1")

				if (FAXING_DEBUG == 1) {
						// send email to debug email
						$Email = new CakeEmail();
						$Email->config('default');
						$Email->from(
							array('service@microoffice.com' => 'Fax Genie'))
						    ->to(FAXING_DEBUG_EMAIL)
						    ->subject('Your Fax is Processing!')
					    	->send("The fax Job ID is ".$this->request->data['FaxOut']['job_id']);
					} else {
						// send email to person
						$Email = new CakeEmail();
						$Email->config('default');
						$Email->from(
							array('service@microoffice.com' => 'Fax Genie'))
						    ->to($remail)
						    ->subject('Your Fax is Processing!')
					    	->send("Your fax Job ID is ".$this->request->data['FaxOut']['job_id']);
					}
				}					

				$this->FaxOut->create();
				$this->request->data['FaxOut']['person_id'] = AuthComponent::user('cust_pers_id');
				$this->request->data['FaxOut']['account_id'] = AuthComponent::user('cust_acct_id');
				$this->request->data['FaxOut']['complete_time'] = null;
				$this->request->data['FaxOut']['xmit_time'] = null;

				// debug($this->request->data);

				if ($this->FaxOut->save($this->request->data)) {

					$this->Session->setFlash(__('The fax has been sent'));
					$this->redirect(array('action' => 'history'));

				} else {
					$this->Session->setFlash(__('The fax could not be sent. Please, try again or contact Micro Office for assistance.'));
				}
			}
		}

		// If a file id gets passed
		$fid = $this->FaxOut->find('first', array('conditions' => array('FaxOut.file_id' => $file_id)));

	}



/**
 * faxageResponse method - faxage callback url
 *
 * @throws NotFoundException
 * @author John Jung
 * @param post jobid=105915248&commid=380384&destname=Test&destnum=(646)253-1258&shortstatus=success&longstatus=Success&sendtime=2013-08-09 21:07:16&completetime=2013-08-09 21:09:35&xmittime=00:01:52&pagecount=1&xmitpages=1
 * @return void
 */
	public function faxageResponse() {

		$this->autoRender = false;
		if($_SERVER['REQUEST_METHOD']=='POST'){

	    	if(!empty($_POST)){
				// receive the post, fit it into CakePHP array
				$faxes = file_get_contents('php://input');
				$faxes = urldecode($faxes);
				$faxdata['FaxOut'] = array();
				foreach (explode('&', $faxes) as $chunk) {
				    $param = explode("=", $chunk);

				    if ($param) {
				    	$faxdata['FaxOut'][urldecode($param[0])] = urldecode($param[1]);
				    }
				}
				// Find the fax by the job_id
				$faxjob = $this->FaxOut->find('first',
					array('conditions' => array('FaxOut.job_id' => $faxdata['FaxOut']['jobid']),
						'recursive' => -1
				));
				$isEmail = $faxjob['FaxOut']['send_email_alert'];

				// update the arrays
				$faxjob['FaxOut']['comm_id'] = $faxdata['FaxOut']['commid'];
				$faxjob['FaxOut']['short_status'] = $faxdata['FaxOut']['shortstatus'];
				$faxjob['FaxOut']['long_status'] = $faxdata['FaxOut']['longstatus'];
				$faxjob['FaxOut']['send_time'] = $faxdata['FaxOut']['sendtime'];
				$faxjob['FaxOut']['complete_time'] = $faxdata['FaxOut']['completetime'];
				$faxjob['FaxOut']['xmit_time'] = $faxdata['FaxOut']['xmittime'];
				$faxjob['FaxOut']['page_count'] = $faxdata['FaxOut']['pagecount'];

				// $this->FaxOut->set($faxjob);
				// debug($this->FaxOut->validates());
				debug($faxjob); die;

				// save it
				if ($this->FaxOut->save($faxjob)) {

					// If the email flag is true
					if ($isEmail) {
						// send email to person
						$Email = new CakeEmail();
						$Email->config('default');
						$Email->from(
							array('service@microoffice.com' => 'Micro Office Fax Genie'))
						    ->to(AuthComponent::user('username'))
						    ->subject('Your Fax Has Been Sent!')
					    	->send('Yay!');
					}
					return true;


				} else {
					// debug($faxjob); die;
					return false;
					// send email to tech support that fax didn't send
					$Email = new CakeEmail();
					$Email->config('default');
					$Email->from(
						array('hello@pith-studio.com' => 'Micro Office Fax Genie'))
					    ->to(EMAIL_FAXERROR)
					    ->subject('Boo, fax didn\'t save properly and something is terribly wrong.')
				    	->send(file_get_contents('php://input'));

				}
			}
		}
	}


/**
 * get a new number
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function getaNumber() {

		// non-admin tenants cannot order a fax number
		if ($this->Auth->user('role') < ROLE_ADMIN) {
			$this->Alert->error('Sorry, please contact your admin or primary account holder to order a private fax');
			$this->redirect(array('action' => 'add'));
		}

		$email = true;
		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		$account = $this->CustomerAccount->find('first', array('conditions' => array('CustomerAccount.id' => $caid)));

		// send email to person
		$Email = new CakeEmail();
		$Email->config('default');
		$Email->from(
			array('service@microoffice.com' => 'Micro Office Fax Genie'))
		    ->to(EMAIL_NEWFAXNUMBER)
		    ->subject('Someone has requested a fax Number')
	    	->send(
	    		"The person below has requested a new private fax number"."\r\n".
	    		"Company: ".$account['CustomerAccount']['name']."\r\n".
			    "Primary Contact: ".$account['PrimaryCustomer']['first_name']." ".$account['PrimaryCustomer']['last_name']."\r\n".
			    "Email: ".$account['PrimaryCustomer']['email']);


		if ($email) {
			$this->Session->setFlash(__('Your fax number will be sent to you via email '));
		} else {
			$this->Session->setFlash(__('Please Contact Micro Office for assistance '));
		}
		$this->redirect(array('action' => 'add'));
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FaxOut->exists($id)) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxOut->save($this->request->data)) {
				$this->Session->setFlash(__('The fax out has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax out could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxOut.' . $this->FaxOut->primaryKey => $id));
			$this->request->data = $this->FaxOut->find('first', $options);
		}
		$customerAccounts = $this->FaxOut->CustomerAccount->find('list');
		$customers = $this->FaxOut->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FaxOut->id = $id;
		if (!$this->FaxOut->exists()) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxOut->delete()) {
			$this->Session->setFlash(__('Fax out deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax out was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FaxOut->recursive = 0;
		$this->set('faxOuts', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FaxOut->exists($id)) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		$options = array('conditions' => array('FaxOut.' . $this->FaxOut->primaryKey => $id));
		$this->set('faxOut', $this->FaxOut->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FaxOut->create();
			if ($this->FaxOut->save($this->request->data)) {
				$this->Session->setFlash(__('The fax out has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax out could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->FaxOut->CustomerAccount->find('list');
		$customers = $this->FaxOut->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FaxOut->exists($id)) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxOut->save($this->request->data)) {
				$this->Session->setFlash(__('The fax out has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax out could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxOut.' . $this->FaxOut->primaryKey => $id));
			$this->request->data = $this->FaxOut->find('first', $options);
		}
		$customerAccounts = $this->FaxOut->CustomerAccount->find('list');
		$customers = $this->FaxOut->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FaxOut->id = $id;
		if (!$this->FaxOut->exists()) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxOut->delete()) {
			$this->Session->setFlash(__('Fax out deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax out was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

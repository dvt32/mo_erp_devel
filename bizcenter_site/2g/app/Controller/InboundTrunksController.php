<?php
App::uses('AppController', 'Controller');
/**
 * InboundTrunks Controller
 *
 * @property InboundTrunk $InboundTrunk
 */
class InboundTrunksController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InboundTrunk->recursive = 0;
		$this->set('inboundTrunks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->InboundTrunk->id = $id;
		if (!$this->InboundTrunk->exists()) {
			throw new NotFoundException(__('Invalid inbound trunk'));
		}
		$this->set('inboundTrunk', $this->InboundTrunk->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InboundTrunk->create();
			if ($this->InboundTrunk->save($this->request->data)) {
				$this->Session->setFlash(__('The inbound trunk has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inbound trunk could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->InboundTrunk->id = $id;
		if (!$this->InboundTrunk->exists()) {
			throw new NotFoundException(__('Invalid inbound trunk'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->InboundTrunk->save($this->request->data)) {
				$this->Session->setFlash(__('The inbound trunk has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inbound trunk could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->InboundTrunk->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->InboundTrunk->id = $id;
		if (!$this->InboundTrunk->exists()) {
			throw new NotFoundException(__('Invalid inbound trunk'));
		}
		if ($this->InboundTrunk->delete()) {
			$this->Session->setFlash(__('Inbound trunk deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Inbound trunk was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

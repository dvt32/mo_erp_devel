<?php
App::uses('AppController', 'Controller');
/**
 * IpAddresses Controller
 *
 * @property IpAddress $IpAddress
 */
class IpAddressesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->IpAddress->recursive = 0;
		$this->set('ipAddresses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->IpAddress->exists($id)) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		$options = array('conditions' => array('IpAddress.' . $this->IpAddress->primaryKey => $id));
		$this->set('ipAddress', $this->IpAddress->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->IpAddress->create();
			if ($this->IpAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The ip address has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip address could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->IpAddress->CustomerAccount->find('list');
		$buildings = $this->IpAddress->Building->find('list');
		$this->set(compact('customerAccounts', 'buildings'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->IpAddress->exists($id)) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->IpAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The ip address has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip address could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('IpAddress.' . $this->IpAddress->primaryKey => $id));
			$this->request->data = $this->IpAddress->find('first', $options);
		}
		$customerAccounts = $this->IpAddress->CustomerAccount->find('list');
		$buildings = $this->IpAddress->Building->find('list');
		$this->set(compact('customerAccounts', 'buildings'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->IpAddress->id = $id;
		if (!$this->IpAddress->exists()) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->IpAddress->delete()) {
			$this->Session->setFlash(__('Ip address deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ip address was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->IpAddress->recursive = 0;
		$this->set('ipAddresses', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->IpAddress->exists($id)) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		$options = array('conditions' => array('IpAddress.' . $this->IpAddress->primaryKey => $id));
		$this->set('ipAddress', $this->IpAddress->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->IpAddress->create();
			if ($this->IpAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The ip address has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip address could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->IpAddress->CustomerAccount->find('list');
		$buildings = $this->IpAddress->Building->find('list');
		$this->set(compact('customerAccounts', 'buildings'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->IpAddress->exists($id)) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->IpAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The ip address has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip address could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('IpAddress.' . $this->IpAddress->primaryKey => $id));
			$this->request->data = $this->IpAddress->find('first', $options);
		}
		$customerAccounts = $this->IpAddress->CustomerAccount->find('list');
		$buildings = $this->IpAddress->Building->find('list');
		$this->set(compact('customerAccounts', 'buildings'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->IpAddress->id = $id;
		if (!$this->IpAddress->exists()) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->IpAddress->delete()) {
			$this->Session->setFlash(__('Ip address deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ip address was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

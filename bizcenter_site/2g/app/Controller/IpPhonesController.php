<?php
App::uses('AppController', 'Controller');
/**
 * IpPhones Controller
 *
 * @property IpPhone $IpPhone
 */
class IpPhonesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->IpPhone->recursive = 0;
		$this->set('ipPhoneTbls', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone tbl'));
		}
		$this->set('ipPhoneTbl', $this->IpPhone->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->IpPhone->create();
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone tbl could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone tbl'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone tbl could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->IpPhone->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone tbl'));
		}
		if ($this->IpPhone->delete()) {
			$this->Session->setFlash(__('Ip phone tbl deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ip phone tbl was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->IpPhone->recursive = 0;
		$this->set('ipPhoneTbls', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone tbl'));
		}
		$this->set('ipPhoneTbl', $this->IpPhone->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->IpPhone->create();
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone tbl could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone tbl'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone tbl could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->IpPhone->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone tbl'));
		}
		if ($this->IpPhone->delete()) {
			$this->Session->setFlash(__('Ip phone tbl deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ip phone tbl was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

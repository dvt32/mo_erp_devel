<?php
App::uses('AppController', 'Controller');
/**
 * MailPackages Controller
 *
 * @property MailPackage $MailPackage
 */
class MailPackagesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MailPackage->recursive = 0;
		$this->set('mailPackages', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MailPackage->exists($id)) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		$options = array('conditions' => array('MailPackage.' . $this->MailPackage->primaryKey => $id));
		$this->set('mailPackage', $this->MailPackage->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MailPackage->create();
			if ($this->MailPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The mail package has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mail package could not be saved. Please, try again.'));
			}
		}
		$customers = $this->MailPackage->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MailPackage->exists($id)) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MailPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The mail package has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mail package could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MailPackage.' . $this->MailPackage->primaryKey => $id));
			$this->request->data = $this->MailPackage->find('first', $options);
		}
		$customers = $this->MailPackage->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MailPackage->id = $id;
		if (!$this->MailPackage->exists()) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MailPackage->delete()) {
			$this->Session->setFlash(__('Mail package deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Mail package was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MailPackage->recursive = 0;
		$this->set('mailPackages', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MailPackage->exists($id)) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		$options = array('conditions' => array('MailPackage.' . $this->MailPackage->primaryKey => $id));
		$this->set('mailPackage', $this->MailPackage->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->MailPackage->create();
			if ($this->MailPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The mail package has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mail package could not be saved. Please, try again.'));
			}
		}
		$customers = $this->MailPackage->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MailPackage->exists($id)) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MailPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The mail package has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mail package could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MailPackage.' . $this->MailPackage->primaryKey => $id));
			$this->request->data = $this->MailPackage->find('first', $options);
		}
		$customers = $this->MailPackage->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MailPackage->id = $id;
		if (!$this->MailPackage->exists()) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MailPackage->delete()) {
			$this->Session->setFlash(__('Mail package deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Mail package was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

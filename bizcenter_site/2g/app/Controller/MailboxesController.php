<?php
App::uses('AppController', 'Controller');
/**
 * Mailboxes Controller
 *
 * @property Mailbox $Mailbox
 */
class MailboxesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Mailbox->recursive = 0;
		$this->set('mailboxes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Mailbox->exists($id)) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		$options = array('conditions' => array('Mailbox.' . $this->Mailbox->primaryKey => $id));
		$this->set('mailbox', $this->Mailbox->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Mailbox->create();
			if ($this->Mailbox->save($this->request->data)) {
				$this->Session->setFlash(__('The mailbox has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mailbox could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Mailbox->CustomerAccount->find('list');
		$floors = $this->Mailbox->Floor->find('list');
		$this->set(compact('customerAccounts', 'floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Mailbox->exists($id)) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Mailbox->save($this->request->data)) {
				$this->Session->setFlash(__('The mailbox has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mailbox could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Mailbox.' . $this->Mailbox->primaryKey => $id));
			$this->request->data = $this->Mailbox->find('first', $options);
		}
		$customerAccounts = $this->Mailbox->CustomerAccount->find('list');
		$floors = $this->Mailbox->Floor->find('list');
		$this->set(compact('customerAccounts', 'floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Mailbox->id = $id;
		if (!$this->Mailbox->exists()) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Mailbox->delete()) {
			$this->Session->setFlash(__('Mailbox deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Mailbox was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Mailbox->recursive = 0;
		$this->set('mailboxes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Mailbox->exists($id)) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		$options = array('conditions' => array('Mailbox.' . $this->Mailbox->primaryKey => $id));
		$this->set('mailbox', $this->Mailbox->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Mailbox->create();
			if ($this->Mailbox->save($this->request->data)) {
				$this->Session->setFlash(__('The mailbox has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mailbox could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Mailbox->CustomerAccount->find('list');
		$floors = $this->Mailbox->Floor->find('list');
		$this->set(compact('customerAccounts', 'floors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Mailbox->exists($id)) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Mailbox->save($this->request->data)) {
				$this->Session->setFlash(__('The mailbox has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mailbox could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Mailbox.' . $this->Mailbox->primaryKey => $id));
			$this->request->data = $this->Mailbox->find('first', $options);
		}
		$customerAccounts = $this->Mailbox->CustomerAccount->find('list');
		$floors = $this->Mailbox->Floor->find('list');
		$this->set(compact('customerAccounts', 'floors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Mailbox->id = $id;
		if (!$this->Mailbox->exists()) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Mailbox->delete()) {
			$this->Session->setFlash(__('Mailbox deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Mailbox was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

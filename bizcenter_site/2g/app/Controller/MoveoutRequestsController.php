<?php
App::uses('AppController', 'Controller');
/**
 * MoveoutRequests Controller
 *
 * @property MoveoutRequest $MoveoutRequest
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MoveoutRequestsController extends AppController {

/**
 * authorization rules
 *
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('add', 'edit', 'view', 'closeoutAjax','sendReminders'),
		'admin'   =>  array('add', 'edit', 'view', 'closeoutAjax','sendReminders'),
		'normal'  =>  array('view','sendReminders'),
	);

/**
 * Models used by this controller
 * 
 * @var array
 */
	public $uses = array('MoveoutRequest','ProximityCard','CustomerAccount');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MoveoutRequest->recursive = 0;
		$this->set('moveoutCustomers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MoveoutRequest->exists($id)) {
			throw new NotFoundException(__('Invalid moveout customer'));
		}
		$options = array('conditions' => array('MoveoutRequest.' . $this->MoveoutRequest->primaryKey => $id));
		$this->set('moveoutCustomer', $this->MoveoutRequest->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		return $this->redirect(['action' => 'edit']);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {

		// fetch the move-out request associated with the customer account
		$cust_acct_id = AuthComponent::user('cust_acct_id');
		$moveoutrequest = $this->MoveoutRequest->find('first', array(
			'conditions' => compact('cust_acct_id')
		));

		// if no move-out request associated with this customer account
		if (!empty($moveoutrequest))
		{
			// grab the model id
			$id = $moveoutrequest['MoveoutRequest']['id'];
		}

		// POST|PUT request
		if ($this->request->is(array('post', 'put'))) {

			// gather the data
			$data = $this->request->data;

			// save the moveout request
			if ($this->MoveoutRequest->save($data)) {

				// report success
				$this->Alert->success(__('Your move-out request has been successfully updated')); // FIXME
				return $this->redirect(['controller' => 'customers', 'action' => 'dashboard']);
			}
			else
			{
				// report failure
				$this->Alert->error('Your move-out request could not be updated. Please review the form below and try again');

				var_dump($this->MoveoutRequest->validationErrors);
			}
		}
		else
		{
			// set the form data to match the existing move-out request
			if (!empty($id))
			{
				$this->request->data = $this->MoveoutRequest->find('first', [
					'conditions' => ['MoveoutRequest.id' => $id],
					'fields' => array(
						'id',
						'cust_acct_id',
						'move_out_date',
						'close_out_date',
						'security_deposit_address_line_1',
						'security_deposit_address_line_2',
						'security_deposit_city',
						'security_deposit_state',
						'security_deposit_zipcode',
						'is_using_moving_company',
						'security_deposit_notes',
						'security_deposit_payable_to',
						'num_keycards_returned',
						'num_metal_keys_returned'
					)
				]);
			}
		}
		$num_keycards = $this->ProximityCard->find('count', array(
			'conditions' => array(
				'account_id' => $cust_acct_id,
				'status' => 'active',
			)
		));

		// get the earliest close out date
		$earliest_close_out_date = $this->MoveoutRequest->getLicenseEndDate();
		
		// send variables to view
		$this->set(compact(
			'num_keycards',
			'earliest_close_out_date'
		));

		// set the view title
		$this->set('title_for_layout', 'Move-Out Request');
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MoveoutRequest->id = $id;
		if (!$this->MoveoutRequest->exists()) {
			throw new NotFoundException(__('Invalid moveout customer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MoveoutRequest->delete()) {
			$this->Session->setFlash(__('The moveout customer has been deleted.'));
		} else {
			$this->Session->setFlash(__('The moveout customer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * closeout ajax function
 *
 * 
 */
	public function closeoutAjax() {

		// POST request
		if ($this->request->is('post'))
		{
		$date = $this->request->data['date'];

		$moveout_date = $this->MoveoutRequest->getCloseOutDate($date);
		$this->set('moveout_date', $moveout_date);
		$this->set('_serialize', array('moveout_date'));

		}
	}

	/**
	 * Send email to all customer accounts with a move-out date 15 days from now
	 * 
	 * @return void
	 */
	private function emailAllFifteenDaysBeforeCloseOut()
	{
		$fifteen_days_from_now = date('Y-m-d', strtotime("+15 days"));
		$requests = $this->MoveoutRequest->find('list', array(
			'conditions' => array('MoveoutRequest.close_out_date' => $fifteen_days_from_now),
			'fields' => ['id','id']
		));
		// debug($requests);
		$results = null;
		foreach ($requests as $request_id)
		{
			$results[$request_id] = $this->MoveoutRequest->emailFifteenDaysBeforeCloseOut($request_id);
		}
		$this->set('results', $results);
		$this->set('_serialize', ['results']);
	}

	/**
	 * Email the primary and requester two days before move-out
	 */
	private function emailAllTwoDaysBeforeMoveOut()
	{
		$two_days_from_now = date('Y-m-d', strtotime("+2 days"));
		$requests = $this->MoveoutRequest->find('list', array(
			'conditions' => array('MoveoutRequest.move_out_date' => $two_days_from_now),
			'fields' => ['id','id']
		));
		// debug($requests);
		$results = null;
		foreach ($requests as $request_id)
		{
			$results[$request_id] = $this->MoveoutRequest->emailTwoDaysBeforeMoveOut($request_id);
		}
		$this->set('results', $results);
		$this->set('_serialize', ['results']);
	}

	/**
	 * Email the primary and requester five days before move out
	 */
	private function emailAllFiveDaysBeforeMoveOut()
	{
		$five_days_from_now = date('Y-m-d', strtotime("+5 days"));
		$requests = $this->MoveoutRequest->find('list', array(
			'conditions' => array('MoveoutRequest.move_out_date' => $five_days_from_now),
			'fields' => ['id','id']
		));
		// debug($requests);
		$results = null;
		foreach ($requests as $request_id)
		{
			$results[$request_id] = $this->MoveoutRequest->emailFiveDaysBeforeMoveOut($request_id);
		}
		$this->set('results', $results);
		$this->set('_serialize', ['results']);
	}

	/**
	 * Email the primary and requester the day of the move out
	 */
	private function emailAllDayOfMoveOut()
	{
		$today = date('Y-m-d');
		$requests = $this->MoveoutRequest->find('list', array(
			'conditions' => array('MoveoutRequest.move_out_date' => $today),
			'fields' => ['id','id']
		));
		// debug($requests);
		$results = null;
		foreach ($requests as $request_id)
		{
			$results[$request_id] = $this->MoveoutRequest->emailDayOfMoveOut($request_id);
		}
		$this->set('results', $results);
		$this->set('_serialize', ['results']);
	}

	private function emailAllDayOfAccountClose()
	{
		$today = date('Y-m-d');
		$requests = $this->MoveoutRequest->find('list', array(
			'conditions' => array('MoveoutRequest.close_out_date' => $today),
			'fields' => ['id','id']
		));
		// debug($requests);
		$results = null;
		foreach ($requests as $request_id)
		{
			$results[$request_id] = $this->MoveoutRequest->emailDayOfAccountClose($request_id);
		}
		$this->set('results', $results);
		$this->set('_serialize', ['results']);
	}

	/**
	 * Call this method to send all move-out-request-related reminder emails
	 */
	public function sendReminders($cron_key = null)
	{
		if (strcmp($cron_key, CRON_KEY) == 0)
		{
			$results['close_out_minus_15'] = $this->emailAllFifteenDaysBeforeCloseOut(); // billing
			$results['move_out_minus_5']   = $this->emailAllFiveDaysBeforeMoveOut();     // facilities
			$results['move_out_minus_2']   = $this->emailAllTwoDaysBeforeMoveOut();      // users
			$results['move_out_date']      = $this->emailAllDayOfMoveOut();              // users
			$results['close_out_date']     = $this->emailAllDayOfAccountClose();         // users
		}
		else
		{
			$results = false;
		}
		// var_dump($results);exit;
		$this->set('results', $results);
		$this->set('_serialize',$results);
	}
}

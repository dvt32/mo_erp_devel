<?php
App::uses('AppController', 'Controller');
/**
 * NetworkSwitches Controller
 *
 * @property NetworkSwitch $NetworkSwitch
 */
class NetworkSwitchesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->NetworkSwitch->recursive = 0;
		$this->set('networkSwitches', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->NetworkSwitch->exists($id)) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		$options = array('conditions' => array('NetworkSwitch.' . $this->NetworkSwitch->primaryKey => $id));
		$this->set('networkSwitch', $this->NetworkSwitch->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->NetworkSwitch->create();
			if ($this->NetworkSwitch->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		}
		$floors = $this->NetworkSwitch->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->NetworkSwitch->exists($id)) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->NetworkSwitch->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('NetworkSwitch.' . $this->NetworkSwitch->primaryKey => $id));
			$this->request->data = $this->NetworkSwitch->find('first', $options);
		}
		$floors = $this->NetworkSwitch->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->NetworkSwitch->id = $id;
		if (!$this->NetworkSwitch->exists()) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->NetworkSwitch->delete()) {
			$this->Session->setFlash(__('Network switch deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Network switch was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->NetworkSwitch->recursive = 0;
		$this->set('networkSwitches', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->NetworkSwitch->exists($id)) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		$options = array('conditions' => array('NetworkSwitch.' . $this->NetworkSwitch->primaryKey => $id));
		$this->set('networkSwitch', $this->NetworkSwitch->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->NetworkSwitch->create();
			if ($this->NetworkSwitch->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		}
		$floors = $this->NetworkSwitch->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->NetworkSwitch->exists($id)) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->NetworkSwitch->save($this->request->data)) {
				$this->Session->setFlash(__('The network switch has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The network switch could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('NetworkSwitch.' . $this->NetworkSwitch->primaryKey => $id));
			$this->request->data = $this->NetworkSwitch->find('first', $options);
		}
		$floors = $this->NetworkSwitch->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->NetworkSwitch->id = $id;
		if (!$this->NetworkSwitch->exists()) {
			throw new NotFoundException(__('Invalid network switch'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->NetworkSwitch->delete()) {
			$this->Session->setFlash(__('Network switch deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Network switch was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

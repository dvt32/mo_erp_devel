<?php
App::uses('AppController', 'Controller');
/**
 * OutboundTrunks Controller
 *
 * @property OutboundTrunk $OutboundTrunk
 */
class OutboundTrunksController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->OutboundTrunk->recursive = 0;
		$this->set('outboundTrunks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->OutboundTrunk->id = $id;
		if (!$this->OutboundTrunk->exists()) {
			throw new NotFoundException(__('Invalid outbound trunk'));
		}
		$this->set('outboundTrunk', $this->OutboundTrunk->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->OutboundTrunk->create();
			if ($this->OutboundTrunk->save($this->request->data)) {
				$this->Session->setFlash(__('The outbound trunk has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outbound trunk could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->OutboundTrunk->id = $id;
		if (!$this->OutboundTrunk->exists()) {
			throw new NotFoundException(__('Invalid outbound trunk'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->OutboundTrunk->save($this->request->data)) {
				$this->Session->setFlash(__('The outbound trunk has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outbound trunk could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->OutboundTrunk->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->OutboundTrunk->id = $id;
		if (!$this->OutboundTrunk->exists()) {
			throw new NotFoundException(__('Invalid outbound trunk'));
		}
		if ($this->OutboundTrunk->delete()) {
			$this->Session->setFlash(__('Outbound trunk deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Outbound trunk was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * BankAccounts Controller
 *
 * @property BankAccount $BankAccount
 */
class PaymentsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('accounts', 'payCreditCard', 'payBankAccount'),
		'admin' =>    array('accounts', 'payCreditCard', 'payBankAccount'),
		'normal' =>   array(),
	);

	public $uses = array('BankAccount', 'CreditCard', 'Payment');	

/**
 * accounts method
 *
 * @throws NotFoundException
 * @param Authcomponenet cust_acct_id
 * @return void
 */
	public function accounts() {

		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		$creditcards = $this->CreditCard->find('all', array(
			'conditions' => array(
				'CreditCard.account_id' => $caid,
				'NOT' => array(
					'CreditCard.is_active' => false
				)
			),
			'recursive' => -1
		));

		$bankaccounts = $this->BankAccount->find('all', array(
			'conditions' => array(
				'BankAccount.account_id' => $caid,
				'NOT' => array(
					'BankAccount.is_active' => false
				)
			),
			'recursive' => -1
		));
		$this->set('creditcards', $creditcards);
		$this->set('bankaccounts', $bankaccounts);

	}

}

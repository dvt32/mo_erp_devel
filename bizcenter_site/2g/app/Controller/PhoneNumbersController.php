<?php
App::uses('AppController', 'Controller');
/**
 * PhoneNumbers Controller
 *
 * @property PhoneNumber $PhoneNumber
 */
class PhoneNumbersController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('account', 'forward', 'unforward', 'view', 'add', 'edit', 'delete'),
		'admin' =>    array('account', 'forward', 'unforward', 'view', 'add', 'edit', 'delete'),
		'normal' =>   array('account', 'view', 'forward', 'unforward', 'add'),
	);

/**
 * account method
 * @author John Jung
 * @return void
 */
	public function account() {

		// $this->redirect(['action' => 'index']);

		$cust_acct_id = AuthComponent::user('cust_acct_id');

		$this->Paginator->settings = array('PhoneNumber' => array(
			'conditions' => array('account_id' => $cust_acct_id),
		));

		$phoneNumbers = $this->Paginator->paginate();
		$this->set('phoneNumbers', $phoneNumbers);
	}


/**
 * forward method
 * @author John Jung
 * @param id of the phonenumber
 * @return void
 */
	public function forward($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$cpid = AuthComponent::user('cust_pers_id');
			$caid = AuthComponent::user('cust_acct_id');

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['do_call_forward'] = true;
			$number['PhoneNumber']['call_forward_delay'] = $this->request->data['PhoneNumber']['call_forward_delay'];
			$number['PhoneNumber']['call_forward_dst_num'] = $this->request->data['PhoneNumber']['call_forward_dst_num'];

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'account'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}

		} else {
			$options = array('conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id));
			$this->request->data = $this->PhoneNumber->find('first', $options);
		}

	}



/**
 * unforward method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function unforward($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$cpid = AuthComponent::user('cust_pers_id');
			$caid = AuthComponent::user('cust_acct_id');

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['do_call_forward'] = false;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Call fowarding has been disabled'));
			} else {
				$this->Session->setFlash(__('Call fowarding has been disabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'account'));
		}

	}


/**
 * voiceon method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function voiceon($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$cpid = AuthComponent::user('cust_pers_id');
			$caid = AuthComponent::user('cust_acct_id');

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['is_voicemail_on'] = true;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Voicemail has been enabled'));
			} else {
				$this->Session->setFlash(__('Voicemail was not enabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'account'));
		}

	}


/**
 * voiceoff method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function voiceoff($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$cpid = AuthComponent::user('cust_pers_id');
			$caid = AuthComponent::user('cust_acct_id');

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['is_voicemail_on'] = false;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Voicemail has been disabled'));
			} else {
				$this->Session->setFlash(__('Voicemail was not disabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'account'));
		}

	}

/**
 * recordingon method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function recordingon($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$cpid = AuthComponent::user('cust_pers_id');
			$caid = AuthComponent::user('cust_acct_id');

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['is_recording_on'] = 1;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Recording has been enabled'));
			} else {
				$this->Session->setFlash(__('Recording was not enabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'account'));
		}

	}


/**
 * recordingoff method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function recordingoff($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$cpid = AuthComponent::user('cust_pers_id');
			$caid = AuthComponent::user('cust_acct_id');

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['is_recording_on'] = 0;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Recording has been disabled'));
			} else {
				$this->Session->setFlash(__('Recording was not disabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'account'));
		}

	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		// customer account id
		$cust_acct_id = AuthComponent::user('cust_acct_id');

		// limit phone numbers retrieved by customer account id
		$phoneNumbers = $this->PhoneNumber->find('all', array(
			'conditions' => array('account_id' => $cust_acct_id),
			'fields' => array('id','username','do_recording')
		));

		// send vars to view
		$this->set(compact('phoneNumbers'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		$options = array('conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id));
		$this->set('phoneNumber', $this->PhoneNumber->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PhoneNumber->create();
			if ($this->PhoneNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->PhoneNumber->CustomerAccount->find('list');
		$customers = $this->PhoneNumber->Customer->find('list');
		$floors = $this->PhoneNumber->Floor->find('list');
		$iads = $this->PhoneNumber->Iad->find('list');
		$ipPhones = $this->PhoneNumber->IpPhone->find('list');
		$inboundTrunks = $this->PhoneNumber->InboundTrunk->find('list');
		$outboundTrunks = $this->PhoneNumber->OutboundTrunk->find('list');
		$this->set(compact('customerAccounts', 'customers', 'floors', 'iads', 'ipPhones', 'inboundTrunks', 'outboundTrunks'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PhoneNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id));
			$this->request->data = $this->PhoneNumber->find('first', $options);
		}
		$customerAccounts = $this->PhoneNumber->CustomerAccount->find('list');
		$customers = $this->PhoneNumber->Customer->find('list');
		$floors = $this->PhoneNumber->Floor->find('list');
		$iads = $this->PhoneNumber->Iad->find('list');
		$ipPhones = $this->PhoneNumber->IpPhone->find('list');
		$inboundTrunks = $this->PhoneNumber->InboundTrunk->find('list');
		$outboundTrunks = $this->PhoneNumber->OutboundTrunk->find('list');
		$this->set(compact('customerAccounts', 'customers', 'floors', 'iads', 'ipPhones', 'inboundTrunks', 'outboundTrunks'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PhoneNumber->id = $id;
		if (!$this->PhoneNumber->exists()) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PhoneNumber->delete()) {
			$this->Session->setFlash(__('Phone number deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phone number was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PhoneNumber->recursive = 0;
		$this->set('phoneNumbers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		$options = array('conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id));
		$this->set('phoneNumber', $this->PhoneNumber->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PhoneNumber->create();
			if ($this->PhoneNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->PhoneNumber->CustomerAccount->find('list');
		$customers = $this->PhoneNumber->Customer->find('list');
		$floors = $this->PhoneNumber->Floor->find('list');
		$iads = $this->PhoneNumber->Iad->find('list');
		$ipPhones = $this->PhoneNumber->IpPhone->find('list');
		$inboundTrunks = $this->PhoneNumber->InboundTrunk->find('list');
		$outboundTrunks = $this->PhoneNumber->OutboundTrunk->find('list');
		$this->set(compact('customerAccounts', 'customers', 'floors', 'iads', 'ipPhones', 'inboundTrunks', 'outboundTrunks'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PhoneNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id));
			$this->request->data = $this->PhoneNumber->find('first', $options);
		}
		$customerAccounts = $this->PhoneNumber->CustomerAccount->find('list');
		$customers = $this->PhoneNumber->Customer->find('list');
		$floors = $this->PhoneNumber->Floor->find('list');
		$iads = $this->PhoneNumber->Iad->find('list');
		$ipPhones = $this->PhoneNumber->IpPhone->find('list');
		$inboundTrunks = $this->PhoneNumber->InboundTrunk->find('list');
		$outboundTrunks = $this->PhoneNumber->OutboundTrunk->find('list');
		$this->set(compact('customerAccounts', 'customers', 'floors', 'iads', 'ipPhones', 'inboundTrunks', 'outboundTrunks'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PhoneNumber->id = $id;
		if (!$this->PhoneNumber->exists()) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PhoneNumber->delete()) {
			$this->Session->setFlash(__('Phone number deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phone number was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

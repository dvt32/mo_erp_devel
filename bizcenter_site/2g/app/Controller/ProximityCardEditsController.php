<?php
App::uses('AppController', 'Controller');
/**
 * ProximityCardEdits Controller
 *
 * @property ProximityCardEdit $ProximityCardEdit
 */
class ProximityCardEditsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('index', 'view', 'add', 'edit', 'delete'),
		'admin' =>    array('index', 'view', 'add', 'edit', 'delete'),
		'normal' =>   array('index', 'view'),
	);

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProximityCardEdit->recursive = 0;
		$this->set('proximityCardEdits', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProximityCardEdit->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		$options = array('conditions' => array('ProximityCardEdit.' . $this->ProximityCardEdit->primaryKey => $id));
		$this->set('proximityCardEdit', $this->ProximityCardEdit->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		if ($this->request->is('post')) {
			$this->ProximityCardEdit->create();
			if ($this->ProximityCardEdit->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card edit has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->ProximityCardEdit->CustomerAccount->find('list');
		$customers = $this->ProximityCardEdit->Customer->find('list', array('fields' => array('Customer.id', 'Customer.last_name'), 'conditions' => array('Customer.cust_acct_id' => $caid)));
		$accessLevels = $this->ProximityCardEdit->AccessLevel->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProximityCardEdit->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ProximityCardEdit->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card edit has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProximityCardEdit.' . $this->ProximityCardEdit->primaryKey => $id));
			$this->request->data = $this->ProximityCardEdit->find('first', $options);
		}
		$customerAccounts = $this->ProximityCardEdit->CustomerAccount->find('list');
		$customers = $this->ProximityCardEdit->Customer->find('list');
		$accessLevels = $this->ProximityCardEdit->AccessLevel->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProximityCardEdit->id = $id;
		if (!$this->ProximityCardEdit->exists()) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProximityCardEdit->delete()) {
			$this->Session->setFlash(__('Proximity card edit deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Proximity card edit was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProximityCardEdit->recursive = 0;
		$this->set('proximityCardEdits', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProximityCardEdit->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		$options = array('conditions' => array('ProximityCardEdit.' . $this->ProximityCardEdit->primaryKey => $id));
		$this->set('proximityCardEdit', $this->ProximityCardEdit->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProximityCardEdit->create();
			if ($this->ProximityCardEdit->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card edit has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->ProximityCardEdit->CustomerAccount->find('list');
		$customers = $this->ProximityCardEdit->Customer->find('list');
		$accessLevels = $this->ProximityCardEdit->AccessLevel->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ProximityCardEdit->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ProximityCardEdit->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card edit has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProximityCardEdit.' . $this->ProximityCardEdit->primaryKey => $id));
			$this->request->data = $this->ProximityCardEdit->find('first', $options);
		}
		$customerAccounts = $this->ProximityCardEdit->CustomerAccount->find('list');
		$customers = $this->ProximityCardEdit->Customer->find('list');
		$accessLevels = $this->ProximityCardEdit->AccessLevel->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProximityCardEdit->id = $id;
		if (!$this->ProximityCardEdit->exists()) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProximityCardEdit->delete()) {
			$this->Session->setFlash(__('Proximity card edit deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Proximity card edit was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

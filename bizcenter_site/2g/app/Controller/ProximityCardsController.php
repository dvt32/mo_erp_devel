<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');
/**
 * ProximityCards Controller
 *
 * @property ProximityCard $ProximityCard
 */
class ProximityCardsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('account','disable','view','add','edit','delete'),
		'admin' =>    array('account','disable','view','add','edit','delete'),
		'normal' =>   array('account'),
	);

	public $uses = array('ProximityCard', 'Customer', 'CustomerAccount');

	public $paginate = array(
		'limit' => '50',
		'order' => 'ProximityCard.creation_dt DESC',
	);

/**
 * account method
 *
 * @return void
 */
	public function account() {
		// check auth for customer account id
		$cust_acct_id = AuthComponent::user('cust_acct_id');

		// fetch related models
		$this->Paginator->settings = array(
			'ProximityCard' => [
				'conditions' => ['ProximityCard.account_id' => $cust_acct_id],
				'contain' => [
					'Customer' => ['id','full_name'],
					'CustomerAccount',
					'Floor.short_global_label'
				]
			]
		);
		// fetch proximity cards for the customer account
		$proximityCards = $this->Paginator->paginate();

		// send variables to view
		$this->set('proximityCards', $proximityCards);
	}	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProximityCard->recursive = 0;
		$this->set('proximityCards', $this->Paginator->paginate());
	}


/**
 * disable method
 * @author John Jung
 * @throws NotFoundException
 * @param string http://192.168.216.67:18779/infinias/ia/People?Username=jjungPassword=pithstudio&Id=1394
 * @return {"success":true,"RecordId":0,"wrap":false,"logout":false,"errors":[]}
 */
	public function disable($pid= null) {

		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		// Proximity card doesn't exist
		if (!$this->ProximityCard->exists($pid)) {
			throw new NotFoundException(__('Invalid proximity card'));
		}

		// Check role and user
		// Make sure this customer can deactivate proximity cards.
		$disableCustomer = $this->ProximityCard->Customer->find('first', array(
			'ProximityCard.id' => $pid,
			'contains' => 'CustomerAccount'
		));

		$card_id = $disableCustomer['ProximityCard'][0]['intellim_id'];

		$info = array(
			'Username' => PROXIMITY_USERNAME,
			'Password' => PROXIMITY_PASSWORD,
			'Id' => $card_id,
		);
		$info = http_build_query($info);

		$editInfo['ProximityCard'] = $disableCustomer['ProximityCard'][0];

		$editInfo['ProximityCard']['status'] = 'inactive';

		if ($this->ProximityCard->save($editInfo)) {

			// SENDS TO LEMUR
			$HttpSocket = new HttpSocket();

			// sends the post request
			$results = $HttpSocket->delete(
			    'http://192.168.216.67:18779/infinias/ia/People',
			    $info
			);

			$this->Session->setFlash(__('The proximity card has been disabled'));
			$this->redirect(array('action' => 'account'));
			
		} else {
			$this->Session->setFlash(__('The proximity card could not be disabled. Please, contact service@microoffice.com'));
			$this->redirect(array('action' => 'account'));
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProximityCard->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card'));
		}
		$options = array('conditions' => array('ProximityCard.' . $this->ProximityCard->primaryKey => $id));
		$this->set('proximityCard', $this->ProximityCard->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		$email = true;
		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		$account = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $caid),
			'contain' => array(
				'Floor.OfficeManager',
				'PrimaryCustomer'
				),
			));

		// send email to Office Manager
		$Email = new CakeEmail();
		$Email->config('default');
		$Email->from(
			array('service@microoffice.com' => 'Someone wants a new access card'))
		    ->to($account['Floor']['OfficeManager']['email'])
		    ->subject('Someone has requested a new access card')
	    	->send(
	    		"The person below has requested an access card"."\r\n".
	    		"Company: ".$account['CustomerAccount']['name']."\r\n".
			    "Primary Contact: ".$account['PrimaryCustomer']['first_name']." ".$account['PrimaryCustomer']['last_name']."\r\n".
			    "Email: ".$account['PrimaryCustomer']['email']);


		if ($email) {
			$this->Session->setFlash(__(' Your request has been sent to your office manager'));
		} else {
			$this->Session->setFlash(__('Please Contact Micro Office for assistance '));
		}
		$this->redirect(array('action' => 'account'));
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProximityCard->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ProximityCard->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProximityCard.' . $this->ProximityCard->primaryKey => $id));
			$this->request->data = $this->ProximityCard->find('first', $options);
		}
		$customerAccounts = $this->ProximityCard->CustomerAccount->find('list');
		$customers = $this->ProximityCard->Customer->find('list');
		$accessLevels = $this->ProximityCard->AccessLevel->find('list');
		$floors = $this->ProximityCard->Floor->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels', 'floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProximityCard->id = $id;
		if (!$this->ProximityCard->exists()) {
			throw new NotFoundException(__('Invalid proximity card'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProximityCard->delete()) {
			$this->Session->setFlash(__('Proximity card deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Proximity card was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProximityCard->recursive = 0;
		$this->set('proximityCards', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProximityCard->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card'));
		}
		$options = array('conditions' => array('ProximityCard.' . $this->ProximityCard->primaryKey => $id));
		$this->set('proximityCard', $this->ProximityCard->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProximityCard->create();
			if ($this->ProximityCard->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->ProximityCard->CustomerAccount->find('list');
		$customers = $this->ProximityCard->Customer->find('list');
		$accessLevels = $this->ProximityCard->AccessLevel->find('list');
		$floors = $this->ProximityCard->Floor->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels', 'floors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ProximityCard->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ProximityCard->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProximityCard.' . $this->ProximityCard->primaryKey => $id));
			$this->request->data = $this->ProximityCard->find('first', $options);
		}
		$customerAccounts = $this->ProximityCard->CustomerAccount->find('list');
		$customers = $this->ProximityCard->Customer->find('list');
		$accessLevels = $this->ProximityCard->AccessLevel->find('list');
		$floors = $this->ProximityCard->Floor->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels', 'floors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProximityCard->id = $id;
		if (!$this->ProximityCard->exists()) {
			throw new NotFoundException(__('Invalid proximity card'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProximityCard->delete()) {
			$this->Session->setFlash(__('Proximity card deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Proximity card was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * ReceivePayments Controller
 *
 * @property ReceivePayment $ReceivePayment
 */
class ReceivePaymentsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('history', 'view', 'add', 'edit', 'delete'),
		'admin' =>    array('history', 'view', 'add', 'edit', 'delete'),
		'normal' =>   array(),
	);

/**
 * history method
 * @author John Jung
 * @param $cust_acct_id from AuthComponent
 * @return void
 */
	public function history() {

		$cust_acct_id = AuthComponent::user('cust_acct_id');

		// fetch receive payments
		$conditions = array('cust_acct_id' => $cust_acct_id);

		$this->Paginator->settings = ['ReceivePayment' => compact('conditions','contain')];
		$receivePayments = $this->Paginator->paginate();

		$this->set('receivePayments', $receivePayments);
	}

// /**
//  * index method
//  *
//  * @return void
//  */
// 	public function index() {
// 		$this->ReceivePayment->recursive = 0;
// 		$this->set('receivePayments', $this->Paginator->paginate());
// 	}

// /**
//  * view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function view($id = null) {
// 		if (!$this->ReceivePayment->exists($id)) {
// 			throw new NotFoundException(__('Invalid receive payment'));
// 		}
// 		$options = array('conditions' => array('ReceivePayment.' . $this->ReceivePayment->primaryKey => $id));
// 		$this->set('receivePayment', $this->ReceivePayment->find('first', $options));
// 	}

// /**
//  * add method
//  *
//  * @return void
//  */
// 	public function add() {
// 		if ($this->request->is('post')) {
// 			$this->ReceivePayment->create();
// 			if ($this->ReceivePayment->save($this->request->data)) {
// 				$this->Session->setFlash(__('The receive payment has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The receive payment could not be saved. Please, try again.'));
// 			}
// 		}
// 		$customerAccounts = $this->ReceivePayment->CustomerAccount->find('list');
// 		$employees = $this->ReceivePayment->Employee->find('list');
// 		$this->set(compact('customerAccounts', 'employees'));
// 	}

// /**
//  * edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function edit($id = null) {
// 		if (!$this->ReceivePayment->exists($id)) {
// 			throw new NotFoundException(__('Invalid receive payment'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->ReceivePayment->save($this->request->data)) {
// 				$this->Session->setFlash(__('The receive payment has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The receive payment could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('ReceivePayment.' . $this->ReceivePayment->primaryKey => $id));
// 			$this->request->data = $this->ReceivePayment->find('first', $options);
// 		}
// 		$customerAccounts = $this->ReceivePayment->CustomerAccount->find('list');
// 		$employees = $this->ReceivePayment->Employee->find('list');
// 		$this->set(compact('customerAccounts', 'employees'));
// 	}

// /**
//  * delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function delete($id = null) {
// 		$this->ReceivePayment->id = $id;
// 		if (!$this->ReceivePayment->exists()) {
// 			throw new NotFoundException(__('Invalid receive payment'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->ReceivePayment->delete()) {
// 			$this->Session->setFlash(__('Receive payment deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Receive payment was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}

// /**
//  * admin_index method
//  *
//  * @return void
//  */
// 	public function admin_index() {
// 		$this->ReceivePayment->recursive = 0;
// 		$this->set('receivePayments', $this->Paginator->paginate());
// 	}

// /**
//  * admin_view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_view($id = null) {
// 		if (!$this->ReceivePayment->exists($id)) {
// 			throw new NotFoundException(__('Invalid receive payment'));
// 		}
// 		$options = array('conditions' => array('ReceivePayment.' . $this->ReceivePayment->primaryKey => $id));
// 		$this->set('receivePayment', $this->ReceivePayment->find('first', $options));
// 	}

// /**
//  * admin_add method
//  *
//  * @return void
//  */
// 	public function admin_add() {
// 		if ($this->request->is('post')) {
// 			$this->ReceivePayment->create();
// 			if ($this->ReceivePayment->save($this->request->data)) {
// 				$this->Session->setFlash(__('The receive payment has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The receive payment could not be saved. Please, try again.'));
// 			}
// 		}
// 		$customerAccounts = $this->ReceivePayment->CustomerAccount->find('list');
// 		$employees = $this->ReceivePayment->Employee->find('list');
// 		$this->set(compact('customerAccounts', 'employees'));
// 	}

// /**
//  * admin_edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_edit($id = null) {
// 		if (!$this->ReceivePayment->exists($id)) {
// 			throw new NotFoundException(__('Invalid receive payment'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->ReceivePayment->save($this->request->data)) {
// 				$this->Session->setFlash(__('The receive payment has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The receive payment could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('ReceivePayment.' . $this->ReceivePayment->primaryKey => $id));
// 			$this->request->data = $this->ReceivePayment->find('first', $options);
// 		}
// 		$customerAccounts = $this->ReceivePayment->CustomerAccount->find('list');
// 		$employees = $this->ReceivePayment->Employee->find('list');
// 		$this->set(compact('customerAccounts', 'employees'));
// 	}

// /**
//  * admin_delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_delete($id = null) {
// 		$this->ReceivePayment->id = $id;
// 		if (!$this->ReceivePayment->exists()) {
// 			throw new NotFoundException(__('Invalid receive payment'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->ReceivePayment->delete()) {
// 			$this->Session->setFlash(__('Receive payment deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Receive payment was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}
}

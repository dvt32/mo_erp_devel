<?php
App::uses('AppController', 'Controller');
/**
 * RecurringCardTransactions Controller
 *
 * @property RecurringCardTransaction $RecurringCardTransaction
 */
class RecurringCardTransactionsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RecurringCardTransaction->recursive = 0;
		$this->set('recurringCardTransactions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RecurringCardTransaction->exists($id)) {
			throw new NotFoundException(__('Invalid recurring card transaction'));
		}
		$options = array('conditions' => array('RecurringCardTransaction.' . $this->RecurringCardTransaction->primaryKey => $id));
		$this->set('recurringCardTransaction', $this->RecurringCardTransaction->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RecurringCardTransaction->create();
			if ($this->RecurringCardTransaction->save($this->request->data)) {
				$this->Session->setFlash(__('The recurring card transaction has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recurring card transaction could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->RecurringCardTransaction->CustomerAccount->find('list');
		$employees = $this->RecurringCardTransaction->Employee->find('list');
		$this->set(compact('customerAccounts', 'employees'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RecurringCardTransaction->exists($id)) {
			throw new NotFoundException(__('Invalid recurring card transaction'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RecurringCardTransaction->save($this->request->data)) {
				$this->Session->setFlash(__('The recurring card transaction has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recurring card transaction could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RecurringCardTransaction.' . $this->RecurringCardTransaction->primaryKey => $id));
			$this->request->data = $this->RecurringCardTransaction->find('first', $options);
		}
		$customerAccounts = $this->RecurringCardTransaction->CustomerAccount->find('list');
		$employees = $this->RecurringCardTransaction->Employee->find('list');
		$this->set(compact('customerAccounts', 'employees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RecurringCardTransaction->id = $id;
		if (!$this->RecurringCardTransaction->exists()) {
			throw new NotFoundException(__('Invalid recurring card transaction'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RecurringCardTransaction->delete()) {
			$this->Session->setFlash(__('Recurring card transaction deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Recurring card transaction was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RecurringCardTransaction->recursive = 0;
		$this->set('recurringCardTransactions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RecurringCardTransaction->exists($id)) {
			throw new NotFoundException(__('Invalid recurring card transaction'));
		}
		$options = array('conditions' => array('RecurringCardTransaction.' . $this->RecurringCardTransaction->primaryKey => $id));
		$this->set('recurringCardTransaction', $this->RecurringCardTransaction->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RecurringCardTransaction->create();
			if ($this->RecurringCardTransaction->save($this->request->data)) {
				$this->Session->setFlash(__('The recurring card transaction has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recurring card transaction could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->RecurringCardTransaction->CustomerAccount->find('list');
		$employees = $this->RecurringCardTransaction->Employee->find('list');
		$this->set(compact('customerAccounts', 'employees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RecurringCardTransaction->exists($id)) {
			throw new NotFoundException(__('Invalid recurring card transaction'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RecurringCardTransaction->save($this->request->data)) {
				$this->Session->setFlash(__('The recurring card transaction has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recurring card transaction could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RecurringCardTransaction.' . $this->RecurringCardTransaction->primaryKey => $id));
			$this->request->data = $this->RecurringCardTransaction->find('first', $options);
		}
		$customerAccounts = $this->RecurringCardTransaction->CustomerAccount->find('list');
		$employees = $this->RecurringCardTransaction->Employee->find('list');
		$this->set(compact('customerAccounts', 'employees'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RecurringCardTransaction->id = $id;
		if (!$this->RecurringCardTransaction->exists()) {
			throw new NotFoundException(__('Invalid recurring card transaction'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RecurringCardTransaction->delete()) {
			$this->Session->setFlash(__('Recurring card transaction deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Recurring card transaction was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

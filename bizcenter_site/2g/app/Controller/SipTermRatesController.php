<?php
App::uses('AppController', 'Controller');
/**
 * SipTermRate Controller
 * @todo Make sure sets and views are connected properly
 * @property SipTermRate $SipTermRate
 */
class SipTermRatesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SipTermRate->recursive = 0;
		// debug($this->Paginator->paginate());
		$this->set('sipTermRates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->SipTermRate->id = $id;
		if (!$this->SipTermRate->exists()) {
			throw new NotFoundException(__('Invalid sip term rate'));
		}
		$this->set('sipTermRate', $this->SipTermRate->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SipTermRate->create();
			if ($this->SipTermRate->save($this->request->data)) {
				$this->Session->setFlash(__('The sip term rate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sip term rate could not be saved. Please, try again.'));
			}
		}
		$sipTermCarrs = $this->SipTermRate->SipTermCarr->find('list');
		$this->set(compact('sipTermCarrs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->SipTermRate->id = $id;
		if (!$this->SipTermRate->exists()) {
			throw new NotFoundException(__('Invalid sip term rate'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SipTermRate->save($this->request->data)) {
				$this->Session->setFlash(__('The sip term rate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sip term rate could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SipTermRate->read(null, $id);
		}
		$sipTermCarrs = $this->SipTermRate->SipTermCarr->find('list');
		$this->set(compact('sipTermCarrs'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SipTermRate->id = $id;
		if (!$this->SipTermRate->exists()) {
			throw new NotFoundException(__('Invalid sip term rate'));
		}
		if ($this->SipTermRate->delete()) {
			$this->Session->setFlash(__('Sip term rate deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Sip term rate was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

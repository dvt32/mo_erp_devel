<?php
App::uses('AppController', 'Controller');

class SudoController extends AppController {

	public $uses = array();

	public function beforeFilter() {
		$this->Auth->allow();
	}

	public function isAuthorized($user) {
		return false;
		// return Configure::read('debug') > 0;
	}

	public function pw($password) {
		$hashed = $this->Auth->password($password);
		debug($hashed);
		exit;
	}

	public function login() {

		$this->uses[] = 'CustomerAccount';
		$this->set('customerAccounts', $this->CustomerAccount->find('list', array(
			'conditions' => array('is_active' => 1),
			'order' => 'name ASC'
		)));

		$this->uses[] = 'Customer';
		$this->set('customers', $this->Customer->find('list', array('fields' => array('id', 'full_name'))));

		$this->uses[] = 'User';
		$this->set('users', $this->User->find('list', array('fields' => array('id','username'))));

		if ($this->request->is('post')) {
			$data = filter_recursive($this->request->data);

			if (count($data['User']) != 1) {
				$this->Alert->error('choose one field');
			}

			else {
				
				$key = array_keys($data['User']);
				$key = $key[0];

				switch ($key) {
					case 'cust_acct_id':
						$cust_acct_id = $data['User']['cust_acct_id'];
						break;
					case 'cust_pers_id':
						$this->Customer->id = $cust_pers_id = $data['User']['cust_pers_id'];
						$cust_acct_id = $this->Customer->field('cust_acct_id');
						break;
					case 'user_id':
						$user_id = $data['User']['user_id'];
						$this->Customer->id = $cust_pers_id = $this->Customer->lookupId('user_id', $user_id);
						$cust_acct_id = $this->Customer->field('cust_acct_id');
						break;
				}
				$account = $this->CustomerAccount->find('first', array(
					'conditions' => array('cust_acct_id' => $cust_acct_id),
					'fields' => array('id','primary_contact_cust_pers_id'),
					'contain' => array(
						'PrimaryCustomer' => array(
							'fields' => array('id','user_id','cust_acct_id'),
							'User' => array('id','username','role','last_logged_in')
						)
					)
				));

				$customerAccount = $account['CustomerAccount'];
				$customer = $account['PrimaryCustomer'];
				$user = $account['PrimaryCustomer']['User'];
				$this->Auth->login(array(
					'id' => $user['id'],
					'username' => $user['username'],
					'role' => $user['role'],
					'cust_pers_id' => $customer['id'],
					'cust_acct_id' => $customerAccount['id'],
					'last_logged_in' => $user['last_logged_in']
				));

				$this->redirect('/');
			}

		}
	}

	public function su($level = null) {

		$level = strtoupper($level);
		switch ($level) {
			case 'SUPER':
				$user = array(
					'id' => 4,
					'username' => 'super@test.com',
					'role' => 9,
					'cust_pers_id' => 6150,
					'cust_acct_id' => 1526,
					'last_logged_in' => '2013-08-13 15:03:13-04'
				);
				break;
			case 'PRIMARY':
				$user = array(
					'id' => 776,
					'username' => 'primary@test.com',
					'role' => 2,
					'cust_pers_id' => 74,
					'cust_acct_id' => 50,
					'last_logged_in' => date(DATETIME_FORMAT)
				);
				break;
			case 'ADMIN':
				$user = array(
					'id' => 31,
					'username' => 'admin@test.com',
					'role' => 1,
					'cust_pers_id' => 6176,
					'cust_acct_id' => 1526,
					'last_logged_in' => '2013-08-13 12:23:59-04'
				);
				break;
			default:
			case 'NORMAL':
				$user = array(
					'id' => 5,
					'username' => 'normal@test.com',
					'role' => 0,
					'cust_pers_id' => 4170,
					'cust_acct_id' => 1494,
					'last_logged_in' => '2013-08-13 16:24:41-04'
				);
				break;
		}
		$this->Auth->login($user);
		debug($user);
		$this->redirect('/');
	}


	public function reservations($room_id) {
		$this->uses[] = 'Reservation';
		$reservations = $this->Reservation->find('all', array(
			'conditions' => array('Reservation.room_id' => $room_id),
			'order' => array('Reservation.start_dt DESC'),
			'fields' => array('start_dt','end_dt'),
			'contain' => array()
		));
		debug($reservations);
		exit;
	}
	

	public function roomMinutesRemaining($id = null) {
		$this->uses[] = 'CustomerAccount';
		$results = $this->CustomerAccount->roomMinutesRemaining($id);
		debug($results);
		exit;
	}

	public function portCustomers() {

		$test = true;

		// model usage
		$this->uses[] = 'User';
		$this->uses[] = 'Customer';
		
		// fetch existing customers
		$customers = $this->Customer->find('all', array(
			'conditions' => array(
				'Customer.user_id' => null,
				array('not' => array(
					"Customer.email" => null,
					"Customer.password" => null
				))
			),
			'fields' => array('id','email','password','user_id'),
			'contain' => 'CustomerAccount.primary_contact_cust_pers_id',
			'limit' => 100
		));
		//debug($customers);
		

		foreach ($customers as $customer) {
			$role =
				($customer['CustomerAccount']['primary_contact_cust_pers_id'] == $customer['Customer']['id'])
				? ROLE_PRIMARY : ROLE_NORMAL;

			$user = array('User' => array(
				'username' => $customer['Customer']['email'],
				'password' => $customer['Customer']['password'], // User.beforeSave hashes pw
				'confirm_password' => $customer['Customer']['password'],
				'role' => $role // FIXME
			));
			//debug($user);
			
			// save the user
			$this->User->create();
			$saved_user = $this->User->save($user);

			//debug($saved_user);

			// add fk association
			if ($saved_user) {
				$this->Customer->id = $customer['Customer']['id'];
				$this->Customer->saveField('user_id', $this->User->id);
			 } else {
			        debug($customer['Customer']['id']);
			        debug($customer['Customer']['email']);
			   	//debug($this->User->validationErrors);
			}
		}

		$customer_count = $this->Customer->find('count');
		$user_count = $this->User->find('count');
		debug(compact('customer_count','user_count'));
		exit;
	}

}

<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Tasks Controller
 *
 * @property Task $Task
 */
class TasksController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow('ir');
		parent::beforeFilter();
	}

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('add','index','view','edit','delete','removal', 'send'),
		'admin' =>    array('add','index','view','edit','delete','removal', 'send'),
		'normal' =>   array('add','index','view','edit','delete','removal', 'send'),
	);
	
/**
 * pagination rules
 * 
 * @var array
 */
	public $paginate = array(
		// 'limit' => '50',
		// 'conditions' => array('Task.status' => 'Requested'),
		'order' => 'Task.creation_date ASC',
		'contain' => array('Employee','Floor')
	);

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// auth vars
		$cust_acct_id = $this->Auth->user('cust_acct_id');
		$cust_pers_id = $this->Auth->user('cust_pers_id');

		// status requested
		$status = @$this->request->named['status'];
		$status = !empty($status) ? $status : 'Open';

		// filter conditions
		$conditions = array('Task.status' => $status,
			'OR' => array(
				'Task.requestedby_cust_pers_id' => $cust_pers_id,
				'Task.requestedby_cust_acct_id' => $cust_acct_id,
		));

		
		// add pagination contains
		$contain = array(
			'Customer' => array('fields' => array('id','first_name','last_name', 'full_name')),
			'CustomerAccount' => array('fields' => array('id','name')),
			'Employee' => array('fields' => array('id','first_name','last_name','title')),
			'Floor' => array('fields' => array('id','short_global_label')),
		);
		// fetch tasks
		$this->Paginator->settings = array('Task' => compact('conditions','contain'));
		$tasks = $this->Paginator->paginate();

		// send to view
		$this->set(compact('tasks'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Task->exists($id)) {
			throw new NotFoundException(__('Invalid task'));
		}
		$options = array('conditions' => array('Task.' . $this->Task->primaryKey => $id));
		$this->set('task', $this->Task->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// add task
		if ($this->request->is('post')) {

			// reset the task model
			$this->Task->create();

			// copy the data
			$data = $this->request->data;

			// fetch from customer
			$user = $this->Task->Customer->User->find('first', array(
				'conditions' => array('User.id' => 1),
				'fields' => array('id'),
				'contain' => array('Customer.id','Customer.cust_acct_id')
			));
			$cust_pers_id = $this->Auth->user('cust_pers_id');
			$cust_acct_id = $this->Auth->user('cust_acct_id');

			// autofill columns
			$data['Task']['assigned_employee_id'] = 2762; // FIXME hardcoded zoila's employee_id
			$data['Task']['requestedby_cust_pers_id'] = $cust_pers_id;
			$data['Task']['requestedby_cust_acct_id'] = $cust_acct_id;
			$data['Task']['requested_completion_date'] = date(DATETIME_FORMAT,strtotime($data['Task']['requested_completion_date']));
			$data['Task']['status'] = 'Open';

			// attempt to save
			if ($this->Task->save($data)) {
				$task = $this->Task->find('first', array(
					'conditions' => array('Task.id' => $this->Task->id),
					'contain' => array(
						'Floor.short_global_label',
						'Customer.first_name',
						'Customer.last_name',
						'CustomerAccount.name',
					),
				));
				$this->send($task);

				$this->Alert->success('The task has been saved');
				$this->redirect(array('controller' => 'tasks', 'action' => 'index'));
			} else {
				$this->Alert->error('The task could not be saved. Please, try again.');
			}
		}

		// grab cust_acct_id from auth
		$cust_acct_id = $this->Auth->user('cust_acct_id');

		// fetch building-related floors
		$this->uses[] = 'CustomerAccount';
		$building = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'Floor' => array(
					'fields' => array('id','label','short_global_label','short_local_label'),
					'Building' => array('fields' => array('id','short_global_label','address_1','address_2'))
				)
			)
		));

		// fetch floors related to the logged in user
		$floors = $this->Task->Floor->find('list', array(
			'conditions' => array('Floor.building_id' => $building['Floor']['Building']['id'])
		));

		// request types, e.g., cleaning, maintenance
		$areas = $this->Task->areas;

		$this->set(compact('floors', 'areas'));
	}


/**
 * removal method
 *
 * @return void
 */
	public function removal() {

		// removal task
		if ($this->request->is('post')) {

			// reset the task model
			$this->Task->create();

			// copy the data
			$data = $this->request->data;

			// fetch from customer
			$user = $this->Task->Customer->User->find('first', array(
				'conditions' => array('User.id' => 1),
				'fields' => array('id'),
				'contain' => array('Customer.id','Customer.cust_acct_id')
			));
			$cust_pers_id = $this->Auth->user('cust_pers_id');
			$cust_acct_id = $this->Auth->user('cust_acct_id');

			// autofill columns
			$data['Task']['assigned_employee_id'] = 2762; // FIXME hardcoded zoila's employee_id
			$data['Task']['requestedby_cust_pers_id'] = $cust_pers_id;
			$data['Task']['requestedby_cust_acct_id'] = $cust_acct_id;
			$data['Task']['requested_completion_date'] = date(DATETIME_FORMAT,strtotime($data['Task']['requested_completion_date']) + 23*60*60 + 59*60 + 59);
			$data['Task']['area'] = 'Item Removal';
			$data['Task']['status'] = 'Requested';
			$data['Task']['jumble'] = random_string(15);

			// attempt to save
			if ($this->Task->save($data)) {
				$task = $this->Task->find('first', array(
					'conditions' => array('Task.id' => $this->Task->id),
					'contain' => array(
						'Floor.short_global_label',
						'Customer.full_name',
						'CustomerAccount.name',
					),
				));
				// $this->send($task);

				$this->Alert->success('Your request has been submitted');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Alert->error('The task could not be saved. Please, try again.');
			}
		}

		// grab cust_acct_id from auth
		$cust_acct_id = $this->Auth->user('cust_acct_id');

		// fetch building-related floors
		$this->uses[] = 'CustomerAccount';
		$building = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'Floor' => array(
					'fields' => array('id','label','short_global_label','short_local_label'),
					'Building' => array('fields' => array('id','short_global_label','address_1','address_2'))
				)
			)
		));

		// fetch floors related to the logged in user
		$floors = $this->Task->Floor->find('list', array(
			'conditions' => array('Floor.building_id' => $building['Floor']['Building']['id'])
		));

		// request types, e.g., cleaning, maintenance
		$areas = $this->Task->areas;

		$this->set(compact('floors', 'areas'));
	}

/**
 * send "new task requested" email to taskmaster (i.e., Zoila)
 * 
 * @return void
 */
	private function send($task) {

		// build message
		$host = FULL_BASE_URL;
		$task_id = $task['Task']['id'];

		// Tasks URL
		$url = "{$host}/tasks/view/{$task_id}";

		
		
		extract($task['Task']);

		if ($area == 'Item Removal') {

			// Item Removal URL
			$removalUrl = "{$host}/tasks/ir/{$jumble}";

			$msg =
			"Task:"."\r\n".
			"url: {$removalUrl}
			requested by: {$requested_completion_date}
			category: {$area}
			urgency: {$urgency}
			description: {$description}"."\r\n".
			"Requestor:"."\r\n".
			"customer: {$task['Customer']['full_name']}
			account: {$task['CustomerAccount']['name']}";

			// configure email
			$email = new CakeEmail();
			$email->config('default');
			$email->from (array('no-reply@microoffice.com' => 'MicroOffice'))
				->to('zoila@microoffice.com') // send email to zoila
				->subject('New Task Requested')
				->send($msg);

		} else {

			$msg =
			"Task:"."\r\n".
				"url: {$url}\n" .
				"requested by: {$requested_completion_date}\n" .
				"category: {$area}\n" .
				"urgency: {$urgency}\n" .
				"description: {$description}"."\r\n".
			    "Requestor:"."\r\n".
				"customer: {$task['Customer']['first_name']} {$task['Customer']['last_name']}\n" .
				"account: {$task['CustomerAccount']['name']}";

			// configure email
			$email = new CakeEmail();
			$email->config('default');
			$email->from (array('no-reply@microoffice.com' => 'MicroOffice'))
				->to('zoila@microoffice.com') // send email to zoila
				->subject('New Task Requested')
				->send($msg);
		}

	}

/**
 * send "new task requested" email to taskmaster (i.e., Zoila)
 * 
 * @return void
 */

	public function ir($jumble = null) {

		// null-check jumble
		if (empty($jumble)) {
			throw new NotFoundException('Invalid item removal request');
		}

		// fetch the task corresponding to the jumble
		$task = $this->Task->find('first', array(
			'conditions' => array('Task.jumble' => $jumble),
			'contain' => array(
				'CustomerAccount' => array('Floor' => 'OfficeManager'),
				'Floor' => 'Building'
			)
		));

		if (empty($task)) {
			throw new NotFoundException(__('Invalid item remove request'));
		}

		// debug($task);
		$this->set('task', $task);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->Task->exists($id)) {
			throw new NotFoundException(__('Invalid task'));
		}

		$task = $this->Task->find('first', array('conditions' => array('Task.id' => $id), 'contain' => array()));

		$authed = true;
		if ($this->Auth->user('role') != ROLE_SUPER) {
			if ($task['Task']['requestedby_cust_pers_id'] != $this->Auth->user('cust_pers_id')) {
				if ($task['Task']['requestedby_cust_acct_id'] != $this->Auth->user('cust_acct_id')) {
					throw new ForbiddenException('You are not authorized to access this action');
				}
			}
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Task->save($this->request->data)) {
				$this->Session->setFlash(__('The task has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The task could not be saved. Please, try again.'));
			}
		} else {

			// grab cust_acct_id from auth
			$cust_acct_id = $this->Auth->user('cust_acct_id');

			// fetch building-related floors
			$this->uses[] = 'CustomerAccount';
			$building = $this->CustomerAccount->find('first', array(
				'conditions' => array('CustomerAccount.id' => $cust_acct_id),
				'contain' => array(
					'Floor' => array(
						'fields' => array('id','label','short_global_label','short_local_label'),
						'Building' => array('fields' => array('id','short_global_label','address_1','address_2'))
					)
				)
			));

			// fetch task
			$this->request->data = $this->Task->find('first', array(
				'conditions' => array('Task.id' => $id)
			));

			// fetch floors related to the logged in user
			$floors = $this->Task->Floor->find('list', array(
				'conditions' => array('Floor.building_id' => $building['Floor']['Building']['id'])
			));

			// request types, e.g., cleaning, maintenance
			$areas = $this->Task->areas;

			$this->set(compact('floors', 'areas'));
		}


		// $employees = $this->Task->Employee->find('list');
		// $requestedbyEmployees = $this->Task->RequestedbyEmployee->find('list');
		// $requestedbyCustPers = $this->Task->RequestedbyCustPer->find('list');
		// $requestedbyCustAccts = $this->Task->RequestedbyCustAcct->find('list');
		// $floors = $this->Task->Floor->find('list');
		// $this->set(compact('employees', 'requestedbyEmployees', 'requestedbyCustPers', 'requestedbyCustAccts', 'floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Task->id = $id;
		if (!$this->Task->exists()) {
			throw new NotFoundException(__('Invalid task'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Task->delete()) {

			$this->Session->setFlash(__('Task deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Task was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

// /**
//  * admin_index method
//  *
//  * @return void
//  */
// 	public function admin_index() {
// 		$this->Task->recursive = 0;
// 		$this->set('tasks', $this->Paginator->paginate());
// 	}

// /**
//  * admin_view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_view($id = null) {
// 		if (!$this->Task->exists($id)) {
// 			throw new NotFoundException(__('Invalid task'));
// 		}
// 		$options = array('conditions' => array('Task.' . $this->Task->primaryKey => $id));
// 		$this->set('task', $this->Task->find('first', $options));
// 	}

// /**
//  * admin_add method
//  *
//  * @return void
//  */
// 	public function admin_add() {
// 		if ($this->request->is('post')) {
// 			$this->Task->create();
// 			if ($this->Task->save($this->request->data)) {
// 				$this->Session->setFlash(__('The task has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The task could not be saved. Please, try again.'));
// 			}
// 		}
// 		$employees = $this->Task->Employee->find('list');
// 		$requestedbyEmployees = $this->Task->RequestedbyEmployee->find('list');
// 		$requestedbyCustPers = $this->Task->RequestedbyCustPer->find('list');
// 		$requestedbyCustAccts = $this->Task->RequestedbyCustAcct->find('list');
// 		$floors = $this->Task->Floor->find('list');
// 		$this->set(compact('employees', 'requestedbyEmployees', 'requestedbyCustPers', 'requestedbyCustAccts', 'floors'));
// 	}

// /**
//  * admin_edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_edit($id = null) {
// 		if (!$this->Task->exists($id)) {
// 			throw new NotFoundException(__('Invalid task'));
// 		}
// 		if ($this->request->is('post') || $this->request->is('put')) {
// 			if ($this->Task->save($this->request->data)) {
// 				$this->Session->setFlash(__('The task has been saved'));
// 				$this->redirect(array('action' => 'index'));
// 			} else {
// 				$this->Session->setFlash(__('The task could not be saved. Please, try again.'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('Task.' . $this->Task->primaryKey => $id));
// 			$this->request->data = $this->Task->find('first', $options);
// 		}
// 		$employees = $this->Task->Employee->find('list');
// 		$requestedbyEmployees = $this->Task->RequestedbyEmployee->find('list');
// 		$requestedbyCustPers = $this->Task->RequestedbyCustPer->find('list');
// 		$requestedbyCustAccts = $this->Task->RequestedbyCustAcct->find('list');
// 		$floors = $this->Task->Floor->find('list');
// 		$this->set(compact('employees', 'requestedbyEmployees', 'requestedbyCustPers', 'requestedbyCustAccts', 'floors'));
// 	}

// /**
//  * admin_delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function admin_delete($id = null) {
// 		$this->Task->id = $id;
// 		if (!$this->Task->exists()) {
// 			throw new NotFoundException(__('Invalid task'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->Task->delete()) {
// 			$this->Session->setFlash(__('Task deleted'));
// 			$this->redirect(array('action' => 'index'));
// 		}
// 		$this->Session->setFlash(__('Task was not deleted'));
// 		$this->redirect(array('action' => 'index'));
// 	}
}

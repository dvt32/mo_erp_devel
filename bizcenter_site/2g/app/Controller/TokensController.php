<?php
App::uses('AppController', 'Controller');

/**
 * Tokens Controller
 *
 * @property Token $Token
 */
class TokensController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow('request', 'resetPassword', 'customerJoin');
	}

/**
 * sends an email with the appropriate token type
 * 
 */
	public function request($type = null) {

		if ($this->request->isPost()) {

			// copy data
			$data = $this->request->data;

			// find by username
			$this->uses[] = 'User';
			$this->User->id = $this->User->field('id', array('username ILIKE' => $data['User']['username']));

			// valid username?
			if ($this->User->exists()) {

				// set the associated user_id
				$data['User'] = array('id' => $this->User->id);
				$data['Token']['user_id'] = $this->User->id;

				// save the token (also sends the email)
				$token = $this->Token->save($data);
				$this->Alert->success('A password-reset link has been sent to your inbox. Check your spam folder if you do not receive the email after a few minutes', 'Email Sent');
				$this->redirect('/');
			} else {
				$this->Alert->error(__('Invalid email address')); // FIXME security vulnerability? tmi? throttle?
			}
		} else {
			$this->Alert->info(__("We'll send a password-reset link so you can regain access to your account.", ''));
		}
	}

/**
 * user clicks link in email that leads here
 * 
 * @param $jumble the unique identifier for the token
 */
	public function resetPassword($jumble = null) {
		// check that token is valid
		$token = $this->Token->findValid($jumble, $this->action);

		// invalid token
		if (empty($token)) {
			$this->set('valid_token', 0);
			$errors = "Possible Reasons: <ul> <li>link was typed incorrectly</li> <li>link has expired</li> <li>link has already been used</li> </ul>";
			$this->Alert->error($errors, 'Invalid Reset Password Link');
		}
		
		// valid token
		else {
			$this->set('valid_token', 1);

			// Form-data posted
			if ($this->request->isPost() || $this->request->isPut()) {

				// user data to be saved
				$user = array(
					'Token' => array(
						'id' => $token['Token']['id'],
						'is_spent' => true
					),
					'User' => array(
						'id' => $token['Token']['user_id'],
						'password' => $this->request->data['User']['password'],
						'confirm_password' => $this->request->data['User']['confirm_password'],
					)
				);

				// Validate new password+confirmation
				$this->uses[] = 'User';
				$saved = $this->User->save($user, array('fieldList' => array('password')));

				if ($saved) {

					// Mark token as used
					$marked = $this->Token->markAllUsed($user['User']['id'], 'resetPassword');

					// Report success
					$this->Alert->success("Login using new password", 'Changes saved');
					$this->redirect(array('controller' => 'users', 'action' => 'login'));
				} else {
					$this->Alert->error("Changes failed to save.");
				}
			}
		}
	}

	public function customerJoin($jumble = null) {
		// check that token is valid
		$token = $this->Token->findValid($jumble, $this->action);

		if (empty($token)) {
			$this->set('valid_token', false);
			$errors = "Possible Reasons: <ul> <li>link was typed incorrectly</li> <li>link has expired</li> <li>link has already been used</li> </ul>";
			$this->Alert->error($errors, 'Invalid invitation acceptance link');
		}

		else {
			$this->set('valid_token', true);
			if ($this->request->is('post') || $this->request->is('put')) {

				// copy request data
				$data = $this->request->data;

				// TODO define fieldList
				$fieldList = array(
					'User' => array('id','password','confirm_password'),
					'Customer' => array('id','first_name','last_name','is_active')
				);

				$this->uses[] = 'User';
				// debug($data); exit;

				// save the user
				if ($this->User->saveAssociated($data, compact('fieldList'))) {
					$this->Token->markAllUsed($data['User']['id'], 'customerJoin');
					$this->Alert->success("You're all set, sign in to get started", "Great!");
					$this->redirect(array('controller' => 'users', 'action' => 'login'));
				} else {
					debug($this->User->validationErrors);
					$this->Alert->error('Your information could not be saved');
				}
			} else {
				$this->uses[] = 'User';
				$user = $this->request->data = $this->User->find('first', array(
					'conditions' => array('User.id' => $token['Token']['user_id']),
					'fields' => array('id','username'),
					'contain' => array(
						'Customer' => array(
							'fields' => array('id','first_name','last_name'),
						)
					)
				));
			}
		}
	}

}

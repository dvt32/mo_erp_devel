<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property Customer $Customer
 */
class UsersController extends AppController {

/**
 * allow certain actions before authentication
 */
	public function beforeFilter() {
		$this->Auth->allow('add','login','logout', 'harlem', 'coalition', 'opauth_complete');
	}

	public function isAuthorized($user) {
		if ($user['role'] === ROLE_SUPER) {
			return true;
		}
		throw new ForbiddenException('You are not authorized to access this action.');
	}

	public $uses = array('User','CustomerAccount');


/**
 * login as existing user
 * 
 * @return void
 */
	public function login() {

		$user_id = $this->Auth->user('id');
		if (!empty($user_id)) {
			return $this->redirect(array('controller' => 'customers', 'action' => 'dashboard'));
		}

		if ($this->request->is('post')) {

			// copy data
			$data = $this->request->data;

			// find user
			$username = trim($data['User']['username']);
			$user = $this->User->find('first', array(
				'conditions' => array(
					'User.username ILIKE' => $username,
					'User.password' => $this->Auth->password($data['User']['password']),
				),
				'fields' => array('id','username','last_logged_in','role'),
				'contain' => array('Customer.cust_acct_id', 'Customer.id','Customer.is_active')
			));
			$this->uses = array("Customer", "CustomerAccount");
			//$this->User->query("SELECT * FROM cust_pers_tbl WHERE email = ?");
			$customer = $this->Customer->find('first', array("conditions" => array("Customer.email" => $username)));
			$is_active = $user['Customer']['is_active'] || $customer['Customer']['is_active'];
			//$this->log($customer);
			$this->User->id = @$user['User']['id'];

			if ($this->User->exists())
			{
				if ($is_active) {

					// write the timezone to config for use in various timezone-sensitive methods
					$timezone = $this->CustomerAccount->getTimezone($customer['Customer']['cust_acct_id']);

					// previous last_logged_in datetime
					$last_logged_in = $user['User']['last_logged_in'];

					// manually set auth info
					$this->Auth->login(array(
						'id' => $user['User']['id'],
						'username' => $user['User']['username'],
						'role' => $user['User']['role'],
						'cust_pers_id' => $customer['Customer']['id'],
						'cust_acct_id' => $customer['Customer']['cust_acct_id'],
						'last_logged_in' => $last_logged_in,
						'timezone' => $timezone
					));

					// update login time
					$this->User->updateLogin();

					// first login?
					if ($last_logged_in == null) {
						// redirect to the getting started guide
						return $this->redirect(array('controller' => 'pages', 'action' => 'start'));
					}

					// redirect to dashboard
					$this->redirect(array('controller' => 'customers', 'action' => 'dashboard'));
				}
				else
				{
					$this->Alert->error('Your account is inactive');
				}
			}
			else {
				
				if ($data['User']['password'] === strtoupper($data['User']['password']))
					$this->Alert->error('Password is case-sensitive. Please make sure your caps-lock is not on');
				else
					$this->Alert->error('Invalid username or password, please try again');
			}
		}
	}


/**
 * login as existing user
 * 
 * @return void
 */
	public function harlem() {

		$user_id = $this->Auth->user('id');
		$this->layout = 'harlem';
		if (!empty($user_id)) {
			return $this->redirect(array('controller' => 'customers', 'action' => 'dashboard'));
		}

		if ($this->request->is('post')) {

			// copy data
			$data = $this->request->data;

			// find user
			$username = $data['User']['username'];
			$user = $this->User->find('first', array(
				'conditions' => array(
					'User.username ILIKE' => $username,
					'User.password' => $this->Auth->password($data['User']['password']),
				),
				'fields' => array('id','username','last_logged_in','role'),
				'contain' => array('Customer.cust_acct_id', 'Customer.id','Customer.is_active')
			));
			$is_active = $user['Customer']['is_active'];
			$this->User->id = @$user['User']['id'];

			if ($this->User->exists()) {
				if ($is_active) {

					// previous last_logged_in datetime
					$last_logged_in = $user['User']['last_logged_in'];

					// manually set auth info
					$this->Auth->login(array(
						'brand' => 'harlem',
						'id' => $user['User']['id'],
						'username' => $user['User']['username'],
						'role' => $user['User']['role'],
						'cust_pers_id' => $user['Customer']['id'],
						'cust_acct_id' => $user['Customer']['cust_acct_id'],
						'last_logged_in' => $last_logged_in,
					));

					// update login time
					$this->User->updateLogin();

					// first login?
					if ($last_logged_in == null) {
						// redirect to the getting started guide
						return $this->redirect(array('controller' => 'pages', 'action' => 'start'));
					}

					// redirect to dashboard
					$this->redirect(array('controller' => 'customers', 'action' => 'dashboard'));
				} else {
					$this->Alert->error('Your account is inactive');
				}
			} else {
				$this->Alert->error('Invalid username or password, please try again');
			}
		}
	}


/**
 * login as existing user
 * 
 * @return void
 */
	public function coalition() {

		$user_id = $this->Auth->user('id');
		$this->layout = 'coalition';

		if (!empty($user_id)) {
			return $this->redirect(array('controller' => 'customers', 'action' => 'dashboard'));
		}

		if ($this->request->is('post')) {

			// copy data
			$data = $this->request->data;

			// find user
			$username = $data['User']['username'];
			$user = $this->User->find('first', array(
				'conditions' => array(
					'User.username ILIKE' => $username,
					'User.password' => $this->Auth->password($data['User']['password']),
				),
				'fields' => array('id','username','last_logged_in','role'),
				'contain' => array('Customer.cust_acct_id', 'Customer.id','Customer.is_active')
			));
			$is_active = $user['Customer']['is_active'];
			$this->User->id = @$user['User']['id'];

			if ($this->User->exists()) {
				if ($is_active) {

					// previous last_logged_in datetime
					$last_logged_in = $user['User']['last_logged_in'];

					// manually set auth info
					$this->Auth->login(array(
						'brand' => 'coalition',
						'id' => $user['User']['id'],
						'username' => $user['User']['username'],
						'role' => $user['User']['role'],
						'cust_pers_id' => $user['Customer']['id'],
						'cust_acct_id' => $user['Customer']['cust_acct_id'],
						'last_logged_in' => $last_logged_in,
					));

					// update login time
					$this->User->updateLogin();

					// first login?
					if ($last_logged_in == null) {
						// redirect to the getting started guide
						return $this->redirect(array('controller' => 'pages', 'action' => 'start'));
					}

					// redirect to dashboard
					$this->redirect(array('controller' => 'customers', 'action' => 'dashboard'));
				} else {
					$this->Alert->error('Your account is inactive');
				}
			} else {
				$this->Alert->error('Invalid username or password, please try again');
			}
		}
	}

/**
 * called from opauth plugin after opauth
 */
	public function opauth_complete() {

		// linkedin validation
		if ($this->data['validated'] == true) {

			// fetch user info per linkedin email
			$user = $this->User->find('first', array(
				'conditions' => array('User.username' => $this->data['auth']['info']['email']),
				'fields' => array('id','username','last_logged_in'),
				'contain' => array('Customer.cust_acct_id', 'Customer.id')
			));
			$this->User->id = @$user['User']['id'];

			if ($this->User->exists()) {

				// previous last_logged_in datetime
				$last_logged_in = $user['User']['last_logged_in'];

				// manually set auth info
				$this->Auth->login(array(
					'id' => $user['User']['id'],
					'username' => $user['User']['username'],
					'cust_pers_id' => $user['Customer']['id'],
					'cust_acct_id' => $user['Customer']['cust_acct_id'],
					'last_logged_in' => $last_logged_in
				));

				// update login time
				$this->User->updateLogin();

				// first login?
				if ($last_logged_in == null) {
					// redirect to the getting started guide
					return $this->redirect(array('controller' => 'pages', 'action' => 'start'));
				}

				// redirect to dashboard
				return $this->redirect(array('controller' => 'customers', 'action' => 'dashboard'));

			} else {

				// linkedin email does not match existing username
				$msg =
					"Your LinkedIn email does not match any email address in our records.
					Please sign in using the credentials in the MicroOffice activation email first,
					and change your email to match your LinkedIn.";
				$this->Alert->info($msg, 'No Matching Record');
				
				return $this->redirect(array('controller' => 'users', 'action' => 'login'));
			}

		} else {

			// linkedin validation did not work, redirect to login
			$this->Alert->error('Invalid username or password, please try again');
			return $this->redirect(array('controller' => 'users', 'action' => 'login'));
		}
   }


/**
 * user logout
 * 
 * @return void
 */
	public function logout() {
    $this->redirect($this->Auth->logout());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view() {
		$this->User->id = $this->Auth->user('id');

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {


			$fieldList = array('id','role');

			if ($this->User->save($this->request->data, array('fieldList' => $fieldList))) {

				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * Porting existing user credentials to user_tbl
 * 
 * @throws NotFoundException
 * @param string $id
 * @return void
 *
 *	public function user_port($id = null) {
 *		// Get all the current BizCenter users
 *		
 *		$oldinfo = $this->CustomerAccount->find('first', array(
 *			'conditions' => array('CustomerAccount.primary_contact_cust_pers_id' => 'Customer.id'),
 *			'contain' => array(
 *				'Customer' => array(
 *					'fields' => array('Customer.user_id', 'Customer.email', 'Customer.password', 'Customer.id'),
 *					)
 *				)
 *			)
 *		);
 *		// Need a way to define CustomerAccount.primary_contact_cust_pers_id => Role = 1, if ! then Role = 0
 *		$newinfo = $this->Customer->find('first', array(
 *			'conditions' => array('Customer.user_id' => NULL),
 *			'contain' => array(
 *				'User' => array(	
 *					'fields' => array('User.id', 'User.username', 'User.password', 'User.role'),
 *					)
 *				)
 *			);
 *
 *		$this->User->updateAll(
 *  	  		array('User.id' => $newinfo), 
 *   		array('User.id' => $oldinfo)
*/
}


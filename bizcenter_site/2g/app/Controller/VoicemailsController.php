<?php
App::uses('AppController', 'Controller');
/**
 * Voicemails Controller
 *
 * @property Voicemail $Voicemail
 */
class VoicemailsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('account', 'index', 'view', 'add', 'edit', 'delete'),
		'admin' =>    array('account', 'index', 'view', 'add', 'edit', 'delete'),
		'normal' =>   array('account', 'index', 'view', 'add', 'edit', 'delete'),
	);

	public $uses = array('Voicemail', 'PhoneNumber', 'CustomerAccount');

	public $paginate = array(
		'limit' => '20',
		'order' => 'Voicemail.created_epoch ASC',
	);

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$cpid = AuthComponent::user('cust_pers_id');
		$caid = AuthComponent::user('cust_acct_id');

		$numbers = $this->PhoneNumber->find('list', array(
			'conditions' => array('PhoneNumber.account_id' => $caid),
			'fields' => array('id','phone_number'),
			'recursive' => -1
		));
		$conditions = ['Voicemail.username' => $numbers];

		$this->Paginator->settings = array('Voicemail' => compact('conditions','contain'));
		$voicemails = $this->Paginator->paginate();
		$this->set(compact('voicemails'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Voicemail->exists($id)) {
			throw new NotFoundException(__('Invalid voicemail'));
		}
		$options = array('conditions' => array('Voicemail.' . $this->Voicemail->primaryKey => $id));
		$this->set('voicemail', $this->Voicemail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Voicemail->create();
			if ($this->Voicemail->save($this->request->data)) {
				$this->Session->setFlash(__('The voicemail has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The voicemail could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Voicemail->exists($id)) {
			throw new NotFoundException(__('Invalid voicemail'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Voicemail->save($this->request->data)) {
				$this->Session->setFlash(__('The voicemail has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The voicemail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Voicemail.' . $this->Voicemail->primaryKey => $id));
			$this->request->data = $this->Voicemail->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Voicemail->id = $id;
		if (!$this->Voicemail->exists()) {
			throw new NotFoundException(__('Invalid voicemail'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Voicemail->delete()) {
			$this->Session->setFlash(__('Voicemail deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Voicemail was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppModel', 'Model');
/**
 * AccountReconcileCheck Model
 *
 */
class AccountReconcileCheck extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'acct_reconcile_check_tbl';

}

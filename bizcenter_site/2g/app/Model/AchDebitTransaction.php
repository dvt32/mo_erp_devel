<?php
App::uses('AppModel', 'Model');
/**
 * AchDebitTransaction Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property BankAccount $BankAccount
 * @property ElectronicCheck $ElectronicCheck
 * @property ReceivePayment $ReceivePayment
 */
class AchDebitTransaction extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ach_debit_txn_tbl';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		// 'BankAccount' => array(
		// 	'className' => 'BankAccount',
		// 	'foreignKey' => 'customer_bank_id',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => ''
		// ),
		'ElectronicCheck' => array(
			'className' => 'ElectronicCheck',
			'foreignKey' => 'electronic_check_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ReceivePayment' => array(
			'className' => 'ReceivePayment',
			'foreignKey' => 'recv_payment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * Update the customer account balance after the transaction is saved
 * 
 * @return void
 */
	public function afterSave($created, $options = array()) {

		// customer account id
		$cust_acct_id = $this->data[$this->alias]['cust_acct_id'];

		// update the customer account balance
		$this->CustomerAccount->updateBalance($cust_acct_id);
	}

/**
 * Write the customer account id to session for afterDelete to read
 * 
 * @return boolean true
 */
	public function beforeDelete($cascade = true) {
		$cust_acct_id = $this->field('cust_acct_id');
		CakeSession::write('deleted.ach_debit_txn.cust_acct_id', $cust_acct_id);
		return true;
	}

/**
 * Read the customer account id from session and update the account balance
 * 
 * @return void
 */
	public function afterDelete() {
		$cust_acct_id = CakeSession::read('deleted.ach_debit_txn.cust_acct_id');
		$this->CustomerAccount->updateBalance($cust_acct_id);
	}
}
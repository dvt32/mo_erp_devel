<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	
/**
 * recursive 
 *
 * @var int
 */
	public $recursive = -1;
	
/**
 * behaviors used by model
 *
 * @var array
 */
	public $actsAs = ['Containable'];

/**
 * reverse lookup method
 * 
 * @return mix id
 */
	public function lookupId($field, $value) {
		$row = $this->find('first', array('conditions' => array($field => $value), $this->primaryKey));
		if (empty($row)) {
			return false;
		}
		return $row[$this->alias][$this->primaryKey];
	}


/**
 * Set MS Access info. This must be called by any child-class overloads.
 * 
 * @param array $options beforeSave options
 * @return boolean true
 */
	public function beforeSave($options = array()) {
		// generate a random integer
		$this->data[$this->alias]['ms_access_random_num'] = pow(-1,rand(0,3)) * mt_rand();
		$this->data[$this->alias]['ms_access_creation_dt'] = date(DATETIME_FORMAT);
		$this->data[$this->alias]['creation_dt'] = date(DATETIME_FORMAT);
		$this->data[$this->alias]['modifier_id'] = AuthComponent::user('id');
		return true;
	}

	/**
	 * Custom validation rule, compare a user-input date/time with a system-defined timestring value
	 * 
	 * @param array $check array with value to check (auto-passed by cakephp)
	 * @param string $format format to compare
	 * @param string $timestring string accepted by strtotime against which the pased value should be checked
	 * @return boolean true if the time is in the future, false otherwise
	 */
	public function compareTime($check, $operator = 'gt', $format = 'Y-m-d H:i:s', $timestring = 'now')
	{
		// unwrap the value
		$value = array_values($check);
		$value = $value[0];

		// date against which the value should be compared
		$control = date($format, strtotime($timestring));

		// make the comparison
		switch ($operator)
		{
			case 'eq':   return $value == $control;

			case 'gt':   return $value >  $control;
			case 'gte':  return $value >= $control;

			case 'lt':   return $value <  $control;
			case 'lte':  return $value <= $control;

			// return false if the operator is invalid
			default: return false;
		}

		// should never be reached
		throw new InvalidArgumentException('One or more of the arguments are invalid');
	}

	/**
	 * Custom validation rule, check if the date(time) given falls on a weekday
	 * 
	 * @param array $check array containing value to check (auto-passed by cakephp)
	 * @return return boolean true if the date(time) is a weekday
	 */
	public function isWeekday($check)
	{
		// unwrap the value
		$value = array_values($check);
		$value = $value[0];
		return in_array(date('w',strtotime($value)), [1,2,3,4,5]); // mon-fri
	}

/**
 * Sends an email via Mandrill.
 * 
 * @param array $emails list of email recipients
 * @param mix $send_at strtotime parseable string indicating when to schedule the email to send (e.g., '+4 days') or a UTC time (seconds since epoch) in the future
 * @return array $result result of the call to the Mandrill API via MandrillTransport
 */
	public function sendEmail($message = array(), $send_at = false) {

		// check required fields
		if (empty($message['to'])) {
			throw new Exception('Missing email recipient(s)');
		}
		elseif (empty($message['subject'])) {
			throw new Exception('Missing email subject');
		}
		elseif (empty($message['text']) && empty($message['html'])) {
			throw new Exception('Missing email body');
		}

		// set default from_email
		$message['from_email'] = @$message['from_email'] ?: 'no-reply@microoffice.com';

		// create mandrill object
		$mandrill = new Mandrill(MANDRILL_API_KEY);
		// var_dump($mandrill);

		// send immediately
		if (empty($send_at)) {
			$results = $mandrill->messages->send($message);
		}
		// schedule email to be sent
		else {

			// convert to time if $send_at is a string
			$time = is_int($send_at) ? $send_at : strtotime($send_at);

			// convert time to utc datetime
			$utc_datetime = gmdate(DATETIME_FORMAT, $time);

			// schedule message
			$results = $mandrill->messages->send($message, false, null, $utc_datetime);
		}
		// debug($results);

		// filter our successful sends, leaving just the failed sends
		$failed = array_filter($results,function($result){ return $result['status']==='invalid' || $result['status']==='rejected'; });

		// report failed sends if debug is on
		// debug($failed);

		return empty($failed);
	}


/**
 * matches method (custom validation rule)
 * 
 * @return void
 */
	public function matches($check, $ctrl) {
		return array_pop($check) === $this->data[$this->alias][$ctrl];
	}

/**
 * Makes sure date is in the future
 * 
 * @return void
 */
	public function debitDate() { 
		return ($this->data[$this->alias]['debit_date'] > date(DATETIME_FORMAT)) ? true : false; 
	}

/**
 * Check that the datetime is in the future
 * 
 * @return boolean
 */
	public function timeInFuture($check, $allowEqual = false) {
		$value = array_values($check);
		$value = $value[0];

		$now = date(DATETIME_FORMAT);
		$value = date(DATETIME_FORMAT, strtotime($value));

		return $allowEqual ? ($now <= $value) : ($now < $value);
	}

	public function todayOrFuture($check) {
		$value = array_values($check);
		$value = $value[0];

		$today = date('Y-m-d');
		$value = date('Y-m-d', strtotime($value));
		$cmp = strcmp($value, $today);
		return $cmp >= 0; 
	}

/**
 * matches method (custom validation rule)
 * 
 * @return void
 */
	public function compareDates() { 
		return ($this->data[$this->alias]['start_dt'] < $this->data[$this->alias]['end_dt']) ? true : false; 
	} 


/**
 * Upload a file to the server by moving the tmp file into the appropriate subdirectory of /webroot/files
 * 
 * @param array $file array containing file info
 * @param string $relative_path path relative to /webroot/files where file should be stored
 * @return mix url to the file uplod was successful, false otherwise
 */
	public function uploadFile($file, $relative_path = '', $filename) {

		// model alias
		$alias = $this->alias;
		$debug = Configure::read('debug') > 0;
		// debug($file);

		// check back 
		if (!empty($file)) {
			if ($file['error'] == UPLOAD_ERR_OK) {
				if ($file['size'] < MAX_FILE_UPLOAD_SIZE) {

					// build the full path the file
					$full_path = WWW_ROOT . 'files' . DS . $relative_path;

					// create the directory if it does not exist
					if (is_dir($full_path) || mkdir($full_path,0744,true)) {

						// move the tmp file into the path
						if (move_uploaded_file($file['tmp_name'], $full_path . DS . $filename)) {

							return FULL_BASE_URL . DS . 'files' . DS . $relative_path . DS . $filename; // UPLOAD SUCCESSFUL
							
						} else {
							debug("Error: failed to move tmp file ({$file['tmp_name']}) to full path ({$full_path})");
						}
					} else {
						debug("Error: failed to read/write directory({$full_path})");
					}
				} else {
					debug("Error: file size cannot exceed " . MAX_FILE_UPLOAD_SIZE);
				}
			} else {
				debug("Error: file upload error {$file['error']}");
			}
		} else {
			debug("Error: empty file upload");
		}
		return false;
	}
	
}

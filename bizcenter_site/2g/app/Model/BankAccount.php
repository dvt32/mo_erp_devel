<?php
App::uses('AppModel', 'Model');
/**
 * BankAccount Model
 *
 * @property CustomerAccount $CustomerAccount
 */
class BankAccount extends AppModel {


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'bank_account_tbl';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'bank_account_nickname';


/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'bank_account_seq';


/** 
 * validation rules
 * 
 * @var array
 */
	public $validate = array(
		'bank_account_nickname' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a bank account nickname'
			),
			'maxLength' => array(
				'rule' => array('maxLength', 32),
				'message' => array('Bank account name cannot exceed 32 characters')
			)
		),
		'depository_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the depository name'
			),
		),
		'bank_routing_number' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the bank routing number'
			),
		),
		'bank_account_number' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the bank account number'
			),
		),
		'bank_account_type' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please select the bank account type'
			),
		),
		'bank_account_customer_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the full name associated with this bank account'
			),
		),
		'address_1' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the street address of the billing address'
			),
		),
		'city' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the city of the billing address'
			),
		),
		'zip_code' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the zip code of the billing address'
			),
		),
		'country' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter the country of the billing address'
			),
		)
	);


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id'
		)
	);


	
	public function beforeSave($options = array()) {

		// MS access
		parent::beforeSave();

		$user_id = AuthComponent::user('id');
		if (empty($user_id)) {
			return false;
		}
		// $this->data['BankAccount']['person_id'] = AuthComponent::user('cust_pers_id');
		$this->data['BankAccount']['account_id'] = AuthComponent::user('cust_acct_id');
		return true;
	}
}

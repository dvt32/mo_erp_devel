<?php
App::uses('AppModel', 'Model');
/**
 * Building Model
 *
 * @property IpAddress $IpAddress
 * @property Floor $Floor
 */
class Building extends AppModel {



/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'building_tbl';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'address_1';

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'IpAddress' => array(
			'className' => 'IpAddress',
			'foreignKey' => 'building_id',
			'dependent' => false,
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'building_id',
			'dependent' => false,
		)
	);

/**
 * Concatenates the parts of a street address to a single string
 * 
 * @return array of results, with "name" field containing concatenated address
 */
	public function afterFind($results, $primary = false) {
		if (!empty($results)) {
			foreach ($results as &$result) {
				if (!isset($b['address_1']) || !isset($b['city']) || !isset($b['state']) || !isset($b['zip_code'])) {
					break;
				}
				$b = $result[$this->alias]; // buildinga
				$address = $b['address_1'] . ', ';
				$address .= !empty($b['address_2']) ? $b['address_2'] . ', ' : null;
				$address .= sprintf("%s, %s %s", $b['city'], $b['state'], $b['zip_code']);
				$result[$this->alias]['name'] = $address;
			}
		}
		return $results;
	}

}

<?php
App::uses('AppModel', 'Model');
/**
 * CreditCard Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Customer $Customer
 */
class CreditCard extends AppModel {



/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'credit_card_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'credit_card_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'card_nickname';


/** 
 * Credit Card Validations
 *
 *
 */

	public $validate = array(
		'card_number' => array(
			'rule'    => array('cc', array('visa', 'amex', 'bankcard', 'diners', 'disc', 'mc'), false, null),
			'message' => 'The credit card number you supplied was invalid.'
		),
		'card_nickname' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a label for this card'
			),
		),
		'card_security_code' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid security code'
			),
		),
		'card_expiration_month' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid month'
			),
		),
		'billing_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid billing name'
			),
		),
		'billing_address_1' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid address'
			),
		),
		'billing_city' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid city'
			),
		),
		'billing_zip_code' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid zipcode'
			),
		),
		'billing_country' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid country'
			),
		),
		'billing_email_address' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid email address'
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please enter a valid email address'
			),
			// 'isUnique' => array(
			// 	'rule' => 'isUnique',
			// 	'message' => 'That email address has already been registered'
			// )
		),

	);
	

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function beforeSave($options = array()) {

		// MS access
		parent::beforeSave();

		$user_id = AuthComponent::user('id');
		if (empty($user_id)) {
			return false;
		}
		// manually assign the id using select nextval (fix for the currval sql error)
		$nextval = $this->query("select nextval('credit_card_seq')");
		$this->data[$this->alias]['id'] = $nextval[0][0]['nextval'];
		$this->data['CreditCard']['person_id'] = AuthComponent::user('cust_pers_id');
		$this->data['CreditCard']['account_id'] = AuthComponent::user('cust_acct_id');
		return true;
	}


/**
 * cctype method
 * @author John Jung
 * @param creditcard number
 * @return cardtype
 */
	public function cctype($ccnumber = null) {

		$cc = $ccnumber;

		if (preg_match('/^5[1-5]\d{14}$/', $cc) === 1) {
			$card_type = "Mastercard";
		} elseif (preg_match('/^4\d{15}$/', $cc) || preg_match('/^4\d{12}$/', $cc) === 1) {
			 $card_type = "Visa";
		} elseif (preg_match('/^3[47]\d{13}$/', $cc) === 1) {
			 $card_type = "Amex";
		} elseif (preg_match('/^3[68]\d{12}$/', $cc) || preg_match('/^30[0-5]\d{11}$/', $cc) === 1) {
			$card_type = "Diners";
		} elseif (preg_match('/^2131\d{11}$/', $cc) || preg_match('/^1800\d{11}$/', $cc) || preg_match('/^6011\d{12}$/', $cc) === 1) {
			$card_type = "Discover";
		} elseif (preg_match('/^3\d{15}$/', $cc) === 1) {
			$card_type = "Jcb";
		}

		return $card_type;

	}

}

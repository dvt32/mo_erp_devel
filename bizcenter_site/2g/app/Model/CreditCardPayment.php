<?php
App::uses('AppModel', 'Model');
include 'lphp.php';


/**
 * CreditCardPayment Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property CreditCardTransaction $CreditCardTransaction
 */
class CreditCardPayment extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'credit_card_payment_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'credit_card_payment_seq';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CreditCardTransaction' => array(
			'className' => 'CreditCardTransaction',
			'foreignKey' => 'credit_card_payment_id',
			'dependent' => false,
		)
	);

	public function beforeSave($options = array()) {

		// ms access information
		parent::beforeSave();

		$data =& $this->data;
		// debug($data);
		// exit;
		$result = $this->firstDataApiCall($data);
		// exit;

		// set extra data
		$this->data['CreditCardPayment']['remote_host'] = gethostname();
		$this->data['CreditCardPayment']['remote_addr'] = $_SERVER["REMOTE_ADDR"];
		$this->data['CreditCardPayment']['cust_acct_id'] = AuthComponent::user('cust_acct_id');

		// continue the save
		if ($result[0] == true) {

			// Repeat data from CreditCardPayment
			$data['CreditCardTransaction']['cust_acct_id'] = AuthComponent::user('cust_acct_id');
			$data['CreditCardTransaction']['card_number'] = $data['CreditCardPayment']['card_number'];
			$data['CreditCardTransaction']['card_expiration_month'] = $data['CreditCardPayment']['card_expiration_month'];
			$data['CreditCardTransaction']['card_expiration_year'] = $data['CreditCardPayment']['card_expiration_year'];
			$data['CreditCardTransaction']['card_security_code'] = $data['CreditCardPayment']['card_security_code'];
			$data['CreditCardTransaction']['billing_name'] = $data['CreditCardPayment']['billing_name'];
			$data['CreditCardTransaction']['billing_company'] = $data['CreditCardPayment']['billing_company'];
			$data['CreditCardTransaction']['billing_address_num'] = $data['CreditCardPayment']['billing_address_num'];
			$data['CreditCardTransaction']['billing_address_1'] = $data['CreditCardPayment']['billing_address_1'];
			$data['CreditCardTransaction']['billing_address_2'] = $data['CreditCardPayment']['billing_address_2'];
			$data['CreditCardTransaction']['billing_city'] = $data['CreditCardPayment']['billing_city'];
			$data['CreditCardTransaction']['billing_state'] = $data['CreditCardPayment']['billing_state'];
			$data['CreditCardTransaction']['billing_zip_code'] = $data['CreditCardPayment']['billing_zip_code'];
			$data['CreditCardTransaction']['billing_country'] = $data['CreditCardPayment']['billing_country'];
			$data['CreditCardTransaction']['billing_phone_number'] = $data['CreditCardPayment']['billing_phone_number'];
			$data['CreditCardTransaction']['billing_fax_number'] = $data['CreditCardPayment']['billing_fax_number'];
			$data['CreditCardTransaction']['billing_email_address'] = $data['CreditCardPayment']['billing_email_address'];
			$data['CreditCardTransaction']['total_charge'] = $data['CreditCardPayment']['subtotal'] + $data['CreditCardPayment']['convenience_fee'];

			// Extra data from First Data
			$data['CreditCardTransaction']['response_avs'] = $result[1]['r_avs'];
			$data['CreditCardTransaction']['response_order_num'] = $result[1]['r_ordernum'];
			$data['CreditCardTransaction']['response_error'] = $result[1]['r_error'];
			$data['CreditCardTransaction']['response_approved'] = $result[1]['r_approved'];
			$data['CreditCardTransaction']['response_code'] = $result[1]['r_code'];
			$data['CreditCardTransaction']['response_message'] = $result[1]['r_message'];
			$data['CreditCardTransaction']['response_txn_dt'] = $result[1]['r_time'];
			$data['CreditCardTransaction']['response_reference_num'] = $result[1]['r_ref'];
			$data['CreditCardTransaction']['response_tdate'] = $result[1]['r_tdate'];
			$data['CreditCardTransaction']['ip_address'] = $_SERVER['REMOTE_ADDR'];
			$data['CreditCardTransaction']['processed_dt'] = date(DATETIME_FORMAT);

			return $this->CreditCardTransaction->save($data);
			}


			$data['CreditCardPayment']['transaction_json'] = $result;
			return true;

		}

/**
 * After the CreditCardPayment is saved, attempt to automatically process 
 * the payment and record the results in CreditCardTransaction
 * @return void
 */
	// public function afterSave($created, $options = array()) {

	// 	$data = $this->data;

	// 	// make call to first data api
	// 	$response = $this->firstDataApiCall($data);

	// 	if (!empty($response)) { // response from first data received

	// 		// mark payment as processed
	// 		$this->saveField('processed', true, array('validate' => false, 'callbacks' => false));

	// 		// convert xml response to associative array
	// 		$response = xml2array($response);

	// 		// build array to save CreditCardTransaction
	// 		$transaction = array('CreditCardTransaction' => array(
				
	// 			'cust_acct_id'           => $data['CreditCardPayment']['cust_acct_id'],

	// 			'card_number'            => $data['CreditCardPayment']['card_number'],
	// 			'card_expiration_month'  => $data['CreditCardPayment']['card_expiration_month'],
	// 			'card_expiration_year'   => $data['CreditCardPayment']['card_expiration_year'],
	// 			'card_security_code'     => $data['CreditCardPayment']['card_security_code'],

	// 			'billing_name'           => $data['CreditCardPayment']['billing_name'],
	// 			'billing_company'        => $data['CreditCardPayment']['billing_company'],
	// 			'billing_address_num'    => $data['CreditCardPayment']['billing_address_num'],
	// 			'billing_address_1'      => $data['CreditCardPayment']['billing_address_1'],
	// 			'billing_address_2'      => $data['CreditCardPayment']['billing_address_2'],
	// 			'billing_city'           => $data['CreditCardPayment']['billing_city'],
	// 			'billing_state'          => $data['CreditCardPayment']['billing_state'],
	// 			'billing_zip_code'       => $data['CreditCardPayment']['billing_zip_code'],
	// 			'billing_country'        => $data['CreditCardPayment']['billing_country'],
	// 			'billing_phone_number'   => $data['CreditCardPayment']['billing_phone_number'],
	// 			'billing_fax_number'     => $data['CreditCardPayment']['billing_fax_number'],
	// 			'billing_email_address'  => $data['CreditCardPayment']['billing_email_address'],

	// 			'total_charge'           => $data['CreditCardPayment']['subtotal'] + $data['CreditCardPayment']['convenience_fee'],

	// 			// 'queued_dt' => ??,
	// 			'processed_dt' =>            date(DATETIME_FORMAT),

	// 			// first data response
	// 			'response_avs' =>            @$response['r_avs'],
	// 			'response_order_num' =>      @$response['r_ordernum'],
	// 			'response_error' =>          @$response['r_error'],
	// 			'response_approved' =>       @$response['r_approved'],
	// 			'response_code' =>           @$response['r_code'],
	// 			'response_message' =>        @$response['r_message'],
	// 			'response_txn_dt' =>         @$response['r_time'],
	// 			'response_reference_num' =>  @$response['r_ref'],
	// 			'response_tdate' =>          @$response['r_tdate'],

	// 			'ip_address' =>              $_SERVER['REMOTE_ADDR'],
	// 			'credit_card_payment_id' =>  $this->id, // link the credit card transaction to this payment
	// 		));

	// 		// record the transaction
	// 		$transaction_saved = $this->CreditCardTransaction->save($transaction);
	// 	}

	// 	// update the customer account balance
	// 	$this->CustomerAccount->updateBalance($data[$this->alias]['cust_acct_id']);
	// }
	
/*
 * function to convert POST data from view into necessary information for API call
 *
 * Necessary Information from View:
 *
 *	'subtotal'
 *	'card_number'
 *	'card_expiration_month'
 *	'card_expiration_year'
 *	'billing_name'
 *	'billing_address_num'
 *	'billing_address_1'
 *	'billing_address_2'
 *	'billing_zip_code'
 *	'billing_city'
 *	'billing_state'
 *	'billing_country'
 *	'card_security_code'
 *	'billing_email_address'
 *	'card_currency' -> Currency type as noted here: https://firstdata.zendesk.com/entries/450214-supported-currencies
 *	'card_type' -> Credit Card type: "American Express", "Visa", "Mastercard", "Discover", "Diners Club", "JCB", "Gift Card", "PayPal"
 *
 * @author AM
 * @param $array POST data from view
 */
	public function constructFirstDataApiCall($array = null) {
		if ($array == null) {
			throw new Exception('Please pass in data');
		}
		$requestData = array();
		$data =& $array['CreditCardPayment'];
		$amount = $data['subtotal'] + $data['convenience_fee'];
		$ccNumber = $data['card_number'];
		$cardholderName = $data['billing_name'];
		// $client_ip = $_SERVER['REMOTE_ADDR'];
		$client_email = $data['billing_email_address'];
		
		$requestData = array(
			//specific meta information
			'cargs' => '-m 300 -s -S --cacert /etc/pki/tls/certs/curl-ca-bundle.crt --insecure',
			'host' => 'secure.linkpt.net',
			'port' => '1129',
			//'ip' => $client_ip,
			'keyfile' => '../app/Config/'.SSL_KEY_FILE,
			'configfile' => STORE_ID,
			'transactionorigin' => 'ECI',
			'terminaltype' => 'UNSPECIFIED',
			'tax' => '0.00',
			'shipping' => '0.00',
			//transaction information
			'ordertype' => 'SALE',
			/* This can be set to either LIVE, GOOD, DECLINE, or DUPLICATE. The latter values are used to return test result*/
			'transaction_result' => 'LIVE', 
			'subtotal' => $amount,
			'total_charge' => $amount,
			//card information
			'cardnumber' => $ccNumber,
			'cardexpmonth' => $data['card_expiration_month'],
			'cardexpyear' => (strlen($data['card_expiration_year'] == 2))
				? $data['card_expiration_year']
				: substr($data['card_expiration_year'], 2),
			'cvmvalue' => $data['card_security_code'],
			'cvmindicator' => 'provided',
			//billing information
			'name' => $cardholderName,
			'zip' => $data['billing_zip_code'],
			'addrnum' => $data['billing_address_num'],
			'address1' => $data['billing_address_1'],
			'address2' => $data['billing_address_2'],
			'city' => $data['billing_city'],
			'state' => $data['billing_state'],
			'country' => $data['billing_country'],
			'email' => $client_email
		);
		if (array_key_exists('billing_company', $data)) {
			$requestData['company'] = $data['billing_company'];
		}
		if (array_key_exists('billing_phone', $data)) {
			$requestData['phone'] = $data['billing_phone'];	
		}
		if (array_key_exists('billing_fax', $data)) {
			$requestData['fax'] = $data['billing_fax'];
		}
		if (array_key_exists('comments', $data)) {
			$requestData['comments'] = $data['comments'];
		}
		if (array_key_exists('marketing_source', $data)) {
			$requestData['referred'] = $data['marketing_source'];
		}		
		return $requestData;
	}
/*
 * function to execute the cURL portion of the FirstData API Call.
 *
 * @author AM
 * @param $array POST data from the view for add()
 * @return array will return an array with the first index as boolean true upon a successful transaction.
 */
	public function firstDataApiCall($array = null) {
		if ($array == null) {
			throw new Exception('Please pass in data');
		}
	
		$requestData = $this->constructFirstDataApiCall($array);
		//$requestData['debugging'] = true;	
		//$requestData['debug'] = true;
		$lphp = new lphp();
		$result = $lphp->curl_process($requestData);
		return array(true, $result);
	}

	private function mockFirstDataApiCall($array = null) {
		if (empty($array)) {
			throw new Exception('Please pass in data');
		}
		$request = $this->constructFirstDataApiCall($array);

		// non-00 penny amount mock-returns DECLINED
		$penny_amount = substr($array['CreditCardPayment']['subtotal'],-2);
		if ($penny_amount == '00') {
			$result =
				'<!-- APPROVED XML RESPONSE -->
				<r_csp>CSI</r_csp>
				<r_time>Mon Oct 1 17:17:35 2007</r_time>
				<r_ref>0280908040</r_ref>
				<r_error></r_error>
				<r_ordernum>CDA53434-4753453-255-13453C</r_ordernum>
				<r_message>APPROVED</r_message>
				<r_code>00845302354353453:YYYM:1005354353888:</r_code>
				<r_tdate>1191280650</r_tdate>
				<r_authresponse></r_authresponse>
				<r_approved>APPROVED</r_approved>
				<r_avs>YYYM</r_avs>';
		} else {
			$result =
				'<!-- DECLINED XML RESPONSE -->
				<r_csp></r_csp>
				<r_time>Mon Oct 1 00:56:16 2007</r_time>
				<r_ref></r_ref>
				<r_error>SGS-000001: D:Declined:XXUX:</r_error>
				<r_ordernum>CDA78C0B-47009A0F-319-17231C</r_ordernum>
				<r_message>DECLINED</r_message>
				<r_code></r_code>
				<r_tdate>1191221775</r_tdate>
				<r_authresponse></r_authresponse>
				<r_approved>DECLINED</r_approved>
				<r_avs></r_avs>';
		}
		return $result;
	}



/* EMAIL METHODS ---------------------------------------------------------------------------- */

	private function sendApprovedEmailToAccounting($account, $payment) {

		// send email receipt to finance
		$email = new CakeEmail();
		$email->config('default');
		$email->to(EMAIL_FINANCE)
			// ->from(array('service@microoffice.com' => 'MicroOffice'))
			->subject('Payment has been processed: ' . $account['CustomerAccount']['name'])
			->send(
				"Company: ".$account['CustomerAccount']['name']."\r\n".
				"Account Contact: ".$payment['CreditCardPayment']['billing_name']."\r\n".
				"Email: ".$remail."\r\n".
				"CreditCard Name: ".$account['CreditCard']['card_nickname']."\r\n".
				"CreditCard Number: ".substr($payment['CreditCardPayment']['card_number'], 3)."****".substr($payment['CreditCardPayment']['card_number'], -3)."\r\n".
				"CreditCard Type: ".$payment['CreditCardPayment']['card_type']."\r\n".
				"Convenience Fee: ".$payment['CreditCardPayment']['convenience_fee']."\r\n".
				"Subtotal: ".$payment['CreditCardPayment']['subtotal']."\r\n".
				"Total Charged: ".($payment['CreditCardPayment']['subtotal']+$payment['CreditCardPayment']['convenience_fee'])."\r\n".
				"Company ID: ".$account['CustomerAccount']['id']."\r\n".
				"Comments: ".$account['CreditCard']['note']
			);
	}

	private function sendApprovedEmailToTentant($account, $payment) {

		// send email receipt to customer
		$email = new CakeEmail();
		$email->config('default');
		$email->to($remail)
			// ->from(array('service@microoffice.com' => 'MicroOffice'))
			->subject('Thank you, your payment has been processed')
			->send(
				"Thank you for your payment!"."\r\n"."\r\n".
				"Company: ".$account['CustomerAccount']['name']."\r\n".
				"Account Contact: ".$payment['CreditCardPayment']['billing_name']."\r\n".
				"CreditCard Name: ".$account['CreditCard']['card_nickname']."\r\n".
				"CreditCard Number: "."*****".substr($payment['CreditCardPayment']['card_number'], -4)."\r\n".
				"CreditCard Type: ".$payment['CreditCardPayment']['card_type']."\r\n".
				"Convenience Fee: ".$payment['CreditCardPayment']['convenience_fee']."\r\n".
				"Subtotal: ".$payment['CreditCardPayment']['subtotal']."\r\n".
				"Total Charged: ".($payment['CreditCardPayment']['subtotal']+$payment['CreditCardPayment']['convenience_fee'])."\r\n".
				"Comments: ".$account['CreditCard']['note']
			);
	}

	private function sendDeclinedEmailToAccounting($account, $payment) {

		// send email receipt to finance
		$email = new CakeEmail();
		$email->config('default');
		$email->to(EMAIL_FINANCE)
			// ->from(array('service@microoffice.com' => 'MicroOffice'))
			->subject('Payment has been processed: ' . $account['CustomerAccount']['name'])
			->send(
				"Company: ".$account['CustomerAccount']['name']."\r\n".
				"Account Contact: ".$payment['CreditCardPayment']['billing_name']."\r\n".
				"Email: ".$remail."\r\n".
				"CreditCard Name: ".$account['CreditCard']['card_nickname']."\r\n".
				"CreditCard Number: ".substr($payment['CreditCardPayment']['card_number'], 3)."****".substr($payment['CreditCardPayment']['card_number'], -3)."\r\n".
				"CreditCard Type: ".$payment['CreditCardPayment']['card_type']."\r\n".
				"Convenience Fee: ".$payment['CreditCardPayment']['convenience_fee']."\r\n".
				"Subtotal: ".$payment['CreditCardPayment']['subtotal']."\r\n".
				"Total Charged: ".($payment['CreditCardPayment']['subtotal']+$payment['CreditCardPayment']['convenience_fee'])."\r\n".
				"Company ID: ".$account['CustomerAccount']['id']."\r\n".
				"Comments: ".$account['CreditCard']['note']
			);
	}
}



<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property User $User
 * @property MailPackage $MailPackage
 * @property ProximityCard $ProximityCard
 * @property CreditCard $CreditCard
 */
class Customer extends AppModel {

/**
 * overload constructor to set virtual field(s)
 *
 * @link http://book.cakephp.org/2.0/en/models/virtual-fields.html#virtual-fields-and-model-aliases
 */
	public function __construct($id = false, $table = null, $ds = null) {
	    parent::__construct($id, $table, $ds);
	    $this->virtualFields['full_name'] = sprintf('%s.first_name || \' \' || %s.last_name', $this->alias, $this->alias);
	}

/**
 * table this model uses
 *
 * @var string
 */
	public $useTable = 'cust_pers_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'person_seq';

/**
 * validation rules
 *
 * @var array
 */
	public $validate = array(
		'first_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter your first name'
			),
		),
		'last_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter your last name'
			),
		),
		'email' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid email address'
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please enter a valid email address'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'That email address has already been registered'
			)
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			
			// enabling this causes customers to be associated with null customerAccounts
			// 'conditions' => array('CustomerAccount.is_active' => true),
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		)
	);


/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ProximityCard' => array(
			'className' => 'ProximityCard',
			'foreignKey' => 'person_id',
			'dependent' => false,
		),
		'CreditCard' => array(
			'className' => 'CreditCard',
			'foreignKey' => 'person_id',
			'dependent' => false,
		),
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'requestedby_cust_pers_id',
			'dependent' => false
		),
		'PhoneNumber' => array(
			'className' => 'PhoneNumber',
			'foreignKey' => 'person_id',
			'dependent' => false,
		),
	);

/**
 * Deactivate this customer
 * 
 * @return boolean true if deactivation succeeded
 */
	public function deactivate() {

		// check that $this->id is set properly
		if ($this->exists()) {

			// deactivate this customer
			return $this->saveField('is_active', 0);
		}
		return false;
	}

/**
 * afterSave callback method. Updates the user login email if the cust_pers_tbl.email was changed.
 * 
 * @param boolean $created indicates whether or not a new record was created in this save
 * @return void
 */
	public function afterSave($created, $options = array()) {
		// only on update
		if (!$created) {
			$this->updateLoginEmail();
		}
	}

/**
 * Update the login email in the user_tbl
 * 
 * @return boolean true if user_tbl.username was updated, false otherwise
 */
	private function updateLoginEmail() {
		$email = @$this->data[$this->alias]['email'];

		// change login email in user_tbl
		if (!empty($email)) {
			$this->User->id = $this->field('user_id');
			return $this->User->saveField('username', $email);
		}

		return false;
	}
}

<?php
App::uses('AppModel', 'Model');
/**
 * CustomerAccount Model
 *
 * @property PrimaryContact $PrimaryContact
 * @property Service $Service
 * @property Floor $Floor
 * @property BankAccount $BankAccount
 * @property CreditCard $CreditCard
 * @property Mailbox $Mailbox
 * @property ProximityCard $ProximityCard
 * @property Reservation $Reservation
 * @property Alias $Alias
 * @property ProximityCardEdit $ProximityCardEdit
 * @property IpAddress $IpAddress
 * @property Customer $Customer
 */
class CustomerAccount extends AppModel {


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cust_acct_tbl';


/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'account_seq';

/**
 * Use id as the primary key
 * 
 * @var string
 */
	public $primaryKey = 'id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(

		// account name
		'name' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'A customer account with that name already exists.'
			)
		),
		
		// primary contact
		'primary_contact_cust_pers_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'on' => 'update',
				'message' => 'A primary contact is required'
			)
		),

		// website url
		'web_site' => array(
			'url' => array(
				'rule' => 'url',
				'required' => false,
				'allowEmpty' => true,
				'message' => 'Please enter properly formatted website url'
			)
		),
/*
		// tax id
		'tax_id' => array(
			'format' => array(
				'rule' => '/^([07][1-7]|1[0-6]|2[0-7]|[35][0-9]|[468][0-8]|9[0-589])-?\d{7}$/',
				'required' => false,
				'message' => 'Please enter a properly formatted tax id'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'A customer account with that tax id already exists',
			),
		),
*/

		// active flag
		'is_active' => array(
			'boolean' => array(
				'rule' => 'boolean',
			),
		),

		'service_id' => array(
			'numeric' => array(
				'rule' => 'numeric',
				'required' => true,
				'message' => 'Please select a service'
			)
		),

		// primary floor
		'primary_floor_id' => array(
			'numeric' => array(
				'rule' => 'numeric',
				'required' => true,
				'message' => 'Please select a floor'
			)
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PrimaryCustomer' => array(
			'className' => 'Customer',
			'foreignKey' => 'primary_contact_cust_pers_id',
		),
		'Service' => array(
			'className' => 'Service',
			'foreignKey' => 'service_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'primary_floor_id',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Alias' => array(
			'className' => 'Alias',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'cust_acct_id',
			'conditions' => array('Customer.is_active' => true),
			'dependent' => false,
		),
		'BankAccount' => array(
			'className' => 'BankAccount',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'CreditCard' => array(
			'className' => 'CreditCard',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Mailbox' => array(
			'className' => 'Mailbox',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'MailPackage' => array(
			'className' => 'MailPackage',
			'foreignKey' => 'customer_id',
			'dependent' => false,
		),
		'Reservation' => array(
			'className' => 'Reservation',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'IpAddress' => array(
			'className' => 'IpAddress',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'PhoneNumber' => array(
			'className' => 'PhoneNumber',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Cdr' => array(
			'className' => 'Cdr',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'requestedby_cust_acct_id',
			'dependent' => false,
		),

		'CreditCardTransaction' => array(
			'className' => 'CreditCardTransaction',
			'foreignKey' => 'cust_acct_id',
		),
		'CreditCardPayment' => array(
			'className' => 'CreditCardPayment',
			'foreignKey' => 'cust_acct_id'
		),

		'LicenseAgreement' => array(
			'className' => 'LicenseAgreement',
			'foreignKey' => 'account_id'
		)
	);

/**
 * Deactivate this customerAccount and associated customers
 * 
 * @return boolean true if deactivation succeeded
 */
	public function deactivate() {

		// check that $this->id is set properly
		if ($this->exists()) {

			// deactivate this customerAccount
			$account_deactivated = $this->saveField('is_active', 0);

			// deactivate all associated customers
			$customers_deactivated = $this->Customer->updateAll(
				array('Customer.is_active' => 0),
				array('Customer.cust_acct_id' => $this->id)
			);
			
			// whether deactivation succeeded
			return $account_deactivated && $customers_deactivated;
		}
		return false;
	}

/**
 * Calculates room minutes remaining
 * @return int
 */
	public function roomMinutesRemaining($id = null) {

		// fetch recurring room minutes per month
		$this->id = $id;
		$room_minutes = $this->field('room_minutes');

		// get room minutes used this month
		$minutes_used = $this->roomMinutesUsed($id);

		// calculate minutes remaining this month
		$minutes_remaining = $room_minutes - $minutes_used;

		$cust_acct_id = AuthComponent::user('cust_acct_id');
		
		return $minutes_remaining;
	}

/**
 * Fetch room minutes used this month
 * 
 * @param int $cust_acct_id id of the account to lookup
 * @return int $minutes_used minutes used this month
 */
	public function roomMinutesUsed($cust_acct_id = null) {

		// current year and month
		$year = date('Y');
		$month = date('m');

		$results = $this->query(
			"SELECT SUM( (EXTRACT(epoch FROM end_dt) - EXTRACT(epoch FROM start_dt)) / 60 ) AS minutes_used
			FROM reservation_tbl
			WHERE account_id = {$cust_acct_id}

			AND EXTRACT(year FROM end_dt) = {$year}
			AND EXTRACT(year FROM start_dt) = {$year}

			AND EXTRACT(month FROM end_dt) = {$month}
			AND EXTRACT(month FROM start_dt) = {$month}"
		);
		return $results[0][0]['minutes_used'] ?: 0;
	}

/**
 * Updates the CustomerAccount balance taking into account
 * BillingAmount, ReceivedPayment (check and cash), CreditCardTransaction, AchDebitTransaction values
 * NOTE: identical method found in Operations CustomerAccount model
 *
 * @param int $id customer account id
 * @return void
 */
	public function updateBalance($id = false) {

		// where clause for query
		$where_clause = '';
		if (!empty($id)) {
			if (is_array($id)) {
				$ids = implode(',',$id);
				$where_clause = "WHERE cust_acct_tbl.id IN ({$ids})";
			} else {
				$where_clause = "WHERE cust_acct_tbl.id={$id}";
			}
		}

		// define billing epoch
		$billing_epoch = BILLING_EPOCH;


		/*
		 *   sum(billing_amount.grand_total)
		 * - sum(recv_payment.amount) [checks]
		 * - sum(recv_payment.amount) [cash]
		 * - sum(credit_card_txn.total_charge)
		 * - sum(ach_debit_txn.amount)
		 * ___________________________________
		 * = cust_acct.balance
		 */
		$query = 
			"UPDATE cust_acct_tbl
			SET balance=(
				-- sum of billing amount grand total
				COALESCE((SELECT SUM(billing_amount_tbl.grand_total)
						FROM billing_amount_tbl
						WHERE billing_amount_tbl.cust_acct_id=cust_acct_tbl.id), 0)
				-
				-- sum of cash payments in recv_payments
				COALESCE((SELECT SUM (amount)
						FROM recv_payment_tbl
						WHERE recv_payment_tbl.cust_acct_id=cust_acct_tbl.id
						AND recv_payment_tbl.payment_method='cash'
						AND process_status='complete'
						AND recv_payment_tbl.recv_dt >= '{$billing_epoch}'), 0)
				-
				-- sum of check payments in recv_payments
				COALESCE((SELECT SUM (amount)
					FROM recv_payment_tbl
					WHERE recv_payment_tbl.cust_acct_id=cust_acct_tbl.id
					AND payment_method='check'
					AND process_status='complete'
					AND recv_payment_tbl.recv_dt >= '{$billing_epoch}'), 0)
				-
				-- sum of credit_card_txn_tbl.total_charge
				COALESCE((SELECT SUM (total_charge)
						FROM credit_card_txn_tbl
						WHERE credit_card_txn_tbl.cust_acct_id=cust_acct_tbl.id
						AND credit_card_txn_tbl.response_approved='APPROVED'
						AND credit_card_txn_tbl.response_txn_dt >= '{$billing_epoch}'), 0)
				-
				-- sum of ach_debit_txn_tbl
				COALESCE((SELECT SUM (amount)
						FROM ach_debit_txn_tbl
						WHERE ach_debit_txn_tbl.cust_acct_id=cust_acct_tbl.id
						AND ach_debit_txn_tbl.cancelled=false
						AND ach_debit_txn_tbl.processed_dt >= '{$billing_epoch}'), 0)
			) {$where_clause}";

		$this->query($query);
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 */
	public function getTimezone($id)
	{
		// set the model id
		$id = $id ?: $this->id;

		// fetch the building information
		$result = $this->find('first', array(
			'conditions' => ['CustomerAccount.id' => $id],
			'fields' => ['id','primary_floor_id'],
			'contain' => ['Floor' => ['id', 'Building.timezone']]
		));

		// return the timezone
		$timezone = $result['Floor']['Building']['timezone'];
		return $timezone;
	}
}

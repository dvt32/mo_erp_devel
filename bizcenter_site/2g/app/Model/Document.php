<?php
App::uses('AppModel', 'Model');
/**
 * Document Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Customer $Customer
 * @property File $File
 */
class Document extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'document_vw';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
		),
		// 'File' => array(
		// 	'className' => 'File',
		// 	'foreignKey' => 'file_id',
		// )
	);
}

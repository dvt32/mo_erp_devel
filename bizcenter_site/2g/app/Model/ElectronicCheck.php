<?php
App::uses('AppModel', 'Model');

/**
 * ElectronicCheck Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property AchDebitTransaction $AchDebitTransaction
 */
class ElectronicCheck extends AppModel {

/**
 * validation rules
 *
 * @var array
 */
	public $validate = array(
		'debit_date' => array(
			'future' => array(
				'rule' => array('todayOrFuture'),
				'message' => 'The Debit Date must be in the future'
			),
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a Debit Date'
			)
		),
		'amount' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter an amount'
			),
			'numeric' => array(
		        'rule'    => 'numeric',
		        'message' => 'Please supply a valid monetary amount.'
		    )
		)
	);


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'electronic_check_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'electronic_check_seq';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'AchDebitTransaction' => array(
			'className' => 'AchDebitTransaction',
			'foreignKey' => 'electronic_check_id',
			'dependent' => false,
		)
	);

	public function beforeSave($options = array()) {

		// MS access
		parent::beforeSave();

		$user_id = AuthComponent::user('id');
		if (empty($user_id)) {
			return false;
		}
		// $data['ElectronicCheck']['remote_host'] = gethostname();
		// $data['ElectronicCheck']['remote_addr'] = $_SERVER["REMOTE_ADDR"];
		$this->data['ElectronicCheck']['cust_acct_id'] = AuthComponent::user('cust_acct_id');
		return true;

	}

}

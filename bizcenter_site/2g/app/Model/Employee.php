<?php
App::uses('AppModel', 'Model');
/**
 * Employee Model
 *
 * @property Tax $Tax
 * @property DepositTbl $DepositTbl
 * @property Deposit $Deposit
 * @property EmailTbl $EmailTbl
 * @property EmployeeAbsenceTbl $EmployeeAbsenceTbl
 * @property EmployeeBankAccountTbl $EmployeeBankAccountTbl
 * @property EmployeeTimeTbl $EmployeeTimeTbl
 * @property RecurCardTxnTbl $RecurCardTxnTbl
 * @property ReceivePayment $ReceivePayment
 * @property Task $Task
 */
class Employee extends AppModel {

	/**
	 * 
	 * @link http://book.cakephp.org/2.0/en/models/virtual-fields.html#virtual-fields-and-model-aliases
	 */
	public function __construct($id = false, $table = null, $ds = null) {
	    parent::__construct($id, $table, $ds);
	    $this->virtualFields['full_name'] = sprintf('%s.first_name || \' \' || %s.last_name', $this->alias, $this->alias);
	}

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'employee_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'person_seq';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	// public $validate = array(
		
	// );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tax' => array(
			'className' => 'Tax',
			'foreignKey' => 'tax_id',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		// 'Deposit' => array(
		// 	'className' => 'Deposit',
		// 	'foreignKey' => 'employee_id',
		// 	'dependent' => false,
		// ),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'office_manager_id',
			'dependent' => false,
		),
		// 'EmployeeAbsenceTbl' => array(
		// 	'className' => 'EmployeeAbsenceTbl',
		// 	'foreignKey' => 'employee_id',
		// 	'dependent' => false,
		// ),
		// 'EmployeeBankAccountTbl' => array(
		// 	'className' => 'EmployeeBankAccountTbl',
		// 	'foreignKey' => 'employee_id',
		// 	'dependent' => false,
		// ),
		// 'EmployeeTimeTbl' => array(
		// 	'className' => 'EmployeeTimeTbl',
		// 	'foreignKey' => 'employee_id',
		// 	'dependent' => false,
		// ),
		// 'RecurCardTxnTbl' => array(
		// 	'className' => 'RecurCardTxnTbl',
		// 	'foreignKey' => 'employee_id',
		// 	'dependent' => false,
		// ),
		// 'ReceivePayment' => array(
		// 	'className' => 'ReceivePayment',
		// 	'foreignKey' => 'employee_id',
		// 	'dependent' => false,
		// ),
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'assigned_employee_id',
			'dependent' => false,
		)
	);
}

<?php
App::uses('AppModel', 'Model');
/**
 * EventTbl Model
 *
 */
class Event extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'event_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'event_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


/**
 * validation rules
 * 
 * @var array
 */
	public $validate = array(

		// event name
		'name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'An event name is required (including a date/time is recommended)'
			),
		),

		// event page url
		'event_page_url' => array(
			'url' => array(
				'rule' => 'url',
				'required' => false,
				'allowEmpty' => true,
				'message' => 'Please enter a valid event page url'
			)
		),

		// event page url
		'images_url' => array(
			'url' => array(
				'rule' => 'url',
				'required' => false,
				'allowEmpty' => true,
				'message' => 'Please enter a valid image url'
			)
		),

	);

/**
 * This model has no associations  
 */

}

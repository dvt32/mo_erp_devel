<?php
App::uses('AppModel', 'Model');
/**
 * FaxOut Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Customer $Customer
 * @property Job $Job
 * @property Comm $Comm
 */
class FaxOut extends AppModel {

	public $validate = array(
		'fax_number' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a fax number to send faxes'
			),
		),
		'recipient_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a Recipient Name'
			),
		)
	);

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'fax_out_tbl';
/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'fax_out_seq';	


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	public function beforeSave($options = array()) {

		// MS access
		parent::beforeSave();

		// $user_id = AuthComponent::user('id');
		// if (empty($user_id)) {
		// 	return false;
		// }
		
		return true;
	}


/**
 * method send fax
 *
 * @param array
 */
	public function faxageSend($param = array()) {

		// $param = array(
		// 	'username' => FAXAGE_USERNAME,
		// 	'company' => FAXAGE_COMPANYID,
		// 	'password' => FAXAGE_PSWD,
		// 	'url_notify' => FAXAGE_CALLBACK,
		// 	'callerid' => $src,
		// 	'faxno' => ,
		// 	'recipname' => ,
		// 	'faxfilenames' => array(
		// 		'faxfilenames' => ,
		// 		'faxfiledata' => 
		// 	)
		// );

		$param['username'] = FAXAGE_USERNAME;
		$param['company'] = FAXAGE_COMPANYID;
		$param['password'] = FAXAGE_PSWD;
		$param['url_notify'] = FAXAGE_CALLBACK;
		$param['callerid'] = PUBLIC_FAX_NUMBER;
		$param['operation'] = 'sendfax';

		// SENDS TO FAXAGE
		$HttpSocket = new HttpSocket();

		$param = http_build_query($param);

		// sends the post request
		$results = $HttpSocket->post(
		    'https://www.faxage.com/httpsfax.php',
		    $param
		);

		return $results;


	}


/**
 * updateJob method
 * @author John Jung
 * @param $job_id
 * @return faxage array
 */
	public function updateJob($jobid) {
		
		// Faxage Creds
		$username = FAXAGE_USERNAME;
		$company = FAXAGE_COMPANYID;
		$password = FAXAGE_PSWD;

		$info = array(
			'username' => FAXAGE_USERNAME,
			'company' => FAXAGE_COMPANYID,
			'password' => FAXAGE_PSWD,
			'operation' => 'status',
			'jobid' => $jobid,
			'pagecount' => 1
		);
		$info = http_build_query($info);

		// SENDS TO FAXAGE
		$HttpSocket = new HttpSocket();

		// sends the post request
		$results = $HttpSocket->post(
		    'https://www.faxage.com/httpsfax.php',
		    $info
		);

		$line = $results->body();

		$update = array();

		foreach (explode("\t", $line) as $chunk) {
			array_push($update, $chunk);
		}

		$faxage = array();

		$faxage['Faxage']['job_id'] = @$update[0];
		$faxage['Faxage']['comm_id'] = @$update[1];
		$faxage['Faxage']['destname'] = @$update[2];
		$faxage['Faxage']['destnum'] = @$update[3];
		$faxage['Faxage']['shortstatus'] = @$update[4];
		$faxage['Faxage']['longstatus'] = @$update[5];
		$faxage['Faxage']['sendtime'] = @$update[6];
		$faxage['Faxage']['completetime'] = @$update[7];
		$faxage['Faxage']['xmittime'] = @$update[8];
		$faxage['Faxage']['pagecount'] = @$update[9];

		return $faxage;

	}
}

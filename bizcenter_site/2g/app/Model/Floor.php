<?php
App::uses('AppModel', 'Model');
/**
 * Floor Model
 *
 * @property Building $Building
 * @property Customer $Customer
 * @property Room $Room
 * @property ProximityCard $ProximityCard
 * @property Mailbox $Mailbox
 * @property AccessLevel $AccessLevel
 */
class Floor extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'floor_tbl';

/**
 * Display field
 */
	public $displayField = 'short_global_label';


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		// 'number' => array(
		// 	'numeric' => array(
		// 		'rule' => array('numeric'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'building_id' => array(
		// 	'numeric' => array(
		// 		'rule' => array('numeric'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'label' => array(
		// 	'notempty' => array(
		// 		'rule' => array('notempty'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'ms_access_created' => array(
		// 	'datetime' => array(
		// 		'rule' => array('datetime'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'ms_access_random_num' => array(
		// 	'numeric' => array(
		// 		'rule' => array('numeric'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'bounding_polygon' => array(
		// 	'notempty' => array(
		// 		'rule' => array('notempty'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'short_global_label' => array(
		// 	'notempty' => array(
		// 		'rule' => array('notempty'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
	);

/**
 * belongsTo associations
 * @var array
 */
	public $belongsTo = array(
		'Building' => array(
			'className' => 'Building',
			'foreignKey' => 'building_id',
		),
		'OfficeManager' => array(
			'className' => 'Employee',
			'foreignKey' => 'office_manager_id',
			'fields' => array(
				'id','full_name','work_phone_number','email'
			)
		)
	);

/**
 * hasMany associations
 * @var array
 */
	public $hasMany = array(
		'NetworkSwitch' => array(
			'className' => 'NetworkSwitch',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'primary_floor_id',
			'dependent' => false,
		),
		'Room' => array(
			'className' => 'Room',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'Mailbox' => array(
			'className' => 'Mailbox',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'AccessLevel' => array(
			'className' => 'AccessLevel',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'PhoneNumber' => array(
			'className' => 'PhoneNumber',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
	);
}

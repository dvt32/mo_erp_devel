<?php
App::uses('AppModel', 'Model');
/**
 * InboundTrunk Model
 *
 * @property PhoneNumberTbl $PhoneNumberTbl
 */
class InboundTrunk extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'telecom';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'inbound_trunk_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'PhoneNumber' => array(
			'className' => 'PhoneNumber',
			'foreignKey' => 'inbound_trunk_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

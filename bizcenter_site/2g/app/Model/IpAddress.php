<?php
App::uses('AppModel', 'Model');
/**
 * IpAddress Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Building $Building
 */
class IpAddress extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ip_address_tbl';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'ip_address';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Building' => array(
			'className' => 'Building',
			'foreignKey' => 'building_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

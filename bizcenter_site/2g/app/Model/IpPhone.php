<?php
App::uses('AppModel', 'Model');
/**
 * IpPhone Model
 *
 */
class IpPhone extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'telecom';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ip_phone_tbl';

}

// /**
//  * hasMany associations
//  *
//  * @var array
//  */
// 	public $hasMany = array(
// 		'PhoneNumber' => array(
// 			'className' => 'PhoneNumber',
// 			'foreignKey' => 'ip_phone_id',
// 			'dependent' => false,
// 		),
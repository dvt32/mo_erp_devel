<?php

class LicenseAgreement extends AppModel {
	
	public $useTable = 'license_agreement_tbl';

	public $sequence = 'public.license_agreement_seq';

	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'dependent' => false
		)
	);
}

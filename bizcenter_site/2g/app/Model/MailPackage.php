<?php
App::uses('AppModel', 'Model');
/**
 * MailPackage Model
 *
 * @property Customer $Customer
 */
class MailPackage extends AppModel {


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mail_package_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 * @todo FIX THIS ASSOCIATION
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

<?php
App::uses('AppModel', 'Model');
/**
 * Mailbox Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Floor $Floor
 */
class Mailbox extends AppModel {


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mailbox_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

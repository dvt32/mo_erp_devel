<?php
App::uses('AppModel', 'Model');
/**
 * MOFile Model
 *
 * @property FaxIn $FaxIn
 * @property Scan $Scan
 * @property DocumentVw $DocumentVw
 * @property TaxDocument $TaxDocument
 * @property VoicecloudTxn $VoicecloudTxn
 */
class MoFile extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'file_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'file_seq';


	public function downloadFile($id) {
		$this->id = $id;
		if (!$this->exists()) {
			throw new NotFoundException('MoFile does not exist');
		}
		$this->find('first', array(
			'conditions' => array('MoFile.id' => $id)));
		$filePath = DAV_SERVER_HOST.pathFromId($id);
		debug($filePath);die;
		return download_file($filePath,'./files');
		unlink($filePath,'./files');
	}

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'FaxIn' => array(
			'className' => 'FaxIn',
			'foreignKey' => 'file_id',
			'dependent' => false,
		),
		'Scan' => array(
			'className' => 'Scan',
			'foreignKey' => 'file_id',
			'dependent' => false,
		),
		// 'Document' => array(
		// 	'className' => 'Document',
		// 	'foreignKey' => 'file_id',
		// 	'dependent' => false,
		// ),
		// 'TaxDocument' => array(
		// 	'className' => 'TaxDocument',
		// 	'foreignKey' => 'file_id',
		// 	'dependent' => false,
		// ),
		// 'VoicecloudTxn' => array(
		// 	'className' => 'VoicecloudTxn',
		// 	'foreignKey' => 'file_id',
		// 	'dependent' => false,
		// )
	);

}

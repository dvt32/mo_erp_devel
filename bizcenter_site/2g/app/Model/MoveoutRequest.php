<?php
App::uses('AppModel', 'Model');
/**
 * MoveoutRequest Model
 *
 * @property CustAcct $CustAcct
 */
class MoveoutRequest extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'moveout_request_tbl';

/**
 * Sequence used by this model
 * 
 * @var string
 */
	public $sequence = 'public.moveout_request_seq';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(

		// associated customer account id
		'cust_acct_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'A customer account id required'
			)
		),

		// move out date
		'move_out_date' => array(
			'date' => array(
				'rule' => array('date','ymd'),
				'message' => 'Invalid move-out date'
			),
			'compareTime' => array(
				'rule' => array('compareTime','gt', 'Y-m-d', 'now'), // time in the future
				'message' => 'You have selected an invalid move-out date',
				'on' => 'create'
			),
			'isWeekday' => array(
				'rule' => array('isWeekday'),
				'message' => 'Move-outs are only allowed during weekdays between 9am-5pm'
			)
		),

		// close out date
		'close_out_date' => array(
			'date' => array(
				'rule' => array('date','ymd'),
				'message' => 'Invalid close-out date'
			),
		),

		'num_keycards_returned' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber', true), // allow zero
				'message' => 'Invalid number of keycards returned'
			),
		),
		'num_metal_keys_returned' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber', true), // allow zero
				'message' => 'Invalid number of metal keys returned'
			),
		),
		'is_using_moving_company' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
		),
		'move_out_requester_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),

		// employee update flags
		'all_keycards_returned_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'all_keycards_returned_employee_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'all_metalkeys_returned_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'all_metalkeys_returned_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'coi_submitted_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'coi_submitted_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'coi_approved_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'coi_approved_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'moved_out_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'moved_out_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'security_deposit_sent_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
			),
		),
		'security_deposit_sent_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		),
		'Requester' => array(
			'className' => 'Customer',
			'foreignKey' => 'move_out_requester_id'
		)
	);

	public function beforeValidate($options = array()) {
		parent::beforeValidate($options);

		// check if the move out date is valid
		$is_move_out_date_valid = $this->isMoveOutDateValid(
			$this->data[$this->alias]['move_out_date'],
			$this->data[$this->alias]['is_using_moving_company'] // = is_coi_required
		);

		if (!$is_move_out_date_valid) {

			// stop the save operation if the move out date is invalid
			return false;
		}

		// continue the save operation
		return true;
	}

/**
 * Assign the cust_acct_id and cust_pers_id
 * 
 * @param array $options save options
 * @return boolean true if the save operation should continue
 */ 
	public function beforeSave($options = array())
	{
		// call the parent method
		parent::beforeSave($options);

		// assign customer account id
		$this->data['MoveoutRequest']['cust_acct_id'] = AuthComponent::user('cust_acct_id');

		// assign customer person id
		$this->data['MoveoutRequest']['move_out_requester_id'] = AuthComponent::user('cust_pers_id');

		// determine the account-close-out date
		$move_out_date = $this->data['MoveoutRequest']['move_out_date'];
		$this->data['MoveoutRequest']['close_out_date'] = $this->getCloseOutDate($move_out_date);

		return true;
	}

	public function afterSave($created, $options = array())
	{
		if ($created === true)
		{
			$this->emailMoveoutRequestSubmitted($this->id);
		}
	}

	public function getLicenseEndDate($cust_acct_id = null)
	{
		$cust_acct_id = $cust_acct_id ?: AuthComponent::user('cust_acct_id');
		$latest_agreement = $this->CustomerAccount->LicenseAgreement->find('first', array(
			'conditions' => ['account_id' => $cust_acct_id],
			'fields' => ['end_date'],
			'order' => 'end_date DESC',
			'limit' => 1
		));
		return $latest_agreement['LicenseAgreement']['end_date'];
	}


	/**
	 * Checks whether the close-out date is before the license agreement end date
	 * 
	 * @param string $close_out date    datestring of the close out date (calculated from move-out date)
	 * @return boolean                  true if the close-out date occurs before the license agreement end date
	 */
	public function beforeLicenseEndDate($check)
	{
		$value = array_values($check);
		$value = $value[0];

		$cust_acct_id = AuthComponent::user('cust_acct_id');
		$license_end_date = $this->getLicenseEndDate();
		return false;
	}

/**
 * Determine the account close-out date from the requested move-out date
 * 
 * @param string $move_out_date datestring in Y-m-d format of the requested move-out date
 * @return DateTime
 */
	public function getCloseOutDate($move_out_date)
	{
		// determine timezone?
		$timezone = new DateTimezone('America/New_York');

		// create move-out DateTime object
		$move_out_dt = new DateTime($move_out_date, $timezone);

		// default
		$is_before_cutoff = $move_out_dt->format('d') < '03' && $move_out_dt->format('H') < '17';

		// copy move-out DateTime object for modification
		$close_out_dt = clone $move_out_dt;

		// if the move-out date is after the cutoff...
		if ($is_before_cutoff !== true)
		{
			// ... close-out date is at the end of month thereafter
			$close_out_dt->modify('+1 month');
		}
		$year  = $close_out_dt->format('Y');
		$month = $close_out_dt->format('m');
		$day   = $close_out_dt->format('t');
		$close_out_dt->setDate($year, $month, $day);

		// format to datestring
		$close_out_date = $close_out_dt->format('Y-m-d');

		// return result
		return $close_out_date;
	}

	/**
	 * Checks if the move out date is valid, based on...
	 * - the current datetime vs the monthly cutoff,
	 * - the requested move-out date,
	 * - whether or not COI is required
	 * 
	 * @param string $move_out_date        datestring of requested move out date
	 * @param boolean $is_coi_required     whether or not a coi is required
	 * @param string $now                  datetimestring of now (testing only)
	 * @return boolean                     true if the move out date is valid
	 */
	public function isMoveOutDateValid($move_out_date, $is_coi_required)
	{
		// determine timezone?
		$timezone = new DateTimezone('America/New_York');

		// time travel
		$now = null; // '2014-05-08 16:30:00';
		$now_dt = new DateTime($now, $timezone);

		// create move-out DateTime object
		$move_out_dt = DateTime::createFromFormat('Y-m-d H:i:s', "{$move_out_date} 17:00:00", $timezone);

		// find the datetime diff
		$diff = $now_dt->diff($move_out_dt);

		// var_dump(compact('now_dt','move_out_dt','is_coi_required','diff'));

		if ($is_coi_required)
		{
			// if coi is required

			// move out date must be >= 3 days from now
			$is_valid = ($diff->invert == 0) && ($diff->days >= 3);

			if ($is_valid == false) {
				// error message
				$this->validationErrors['move_out_date']['invalid_date'] = 
					'If you are using a moving company, the move-out date must be at least 3 full business days from now';

				return false;
			}
		}
		else
		{
			// if coi is not required

			// move out date must be >= 1 day from now
			$is_valid = ($diff->invert == 0) && ($diff->days >= 1);

			if ($is_valid == false) {
				// error message
				$this->validationErrors['move_out_date']['invalid_date'] = 
					'If you are not using a moving company, the move-out date must be at least 1 full business day from now';

				return false;
			}
		}

		return true;
	}

	private function fetch($id = null)
	{
		// set the model id
		$this->id = $id = $id ?: $this->id;

		// fetch request
		$request = $this->find('first', array(
			'conditions' => ['MoveoutRequest.id' => $id],
			'contain' => array(
				'Requester' => ['id','full_name','email'],
				'CustomerAccount' => array(
					'fields' => ['id','name','primary_floor_id'],
					'PrimaryCustomer' => ['id','full_name','email'],
					'Floor'  => array(
						'fields' => ['id','short_global_label','label','office_manager_id'],
						'Building',
						'OfficeManager' => array(
							'fields' => ['id','full_name','email']
						),
					)
				)
			)
		));

		return array(
			'request'          => $request['MoveoutRequest'],
			'requester'        => $request['Requester'],
			'customerAccount'  => $request['CustomerAccount'],
			'primaryCustomer'  => $request['CustomerAccount']['PrimaryCustomer'],
			'floor'            => $request['CustomerAccount']['Floor'],
			'officeManager'    => $request['CustomerAccount']['Floor']['OfficeManager'],
			'building'         => $request['CustomerAccount']['Floor']['Building']
		);
	}

	public function emailMoveoutRequestSubmitted($id = null)
	{
		extract($this->fetch($id));

		// om, billing, facilities, sales
		$this->sendEmail(array(
			'to' => array(
				['name' => $officeManager['full_name'], 'email' => $officeManager['email']],
				['email' => EMAIL_BILLING],
				['email' => EMAIL_FACILITIES],
				['email' => EMAIL_SALES],
			),
			'subject' => 'Notification: Member Move Out Requested',
			'text' =>
				"Account Name: {$customerAccount['name']}\n
				Name of Person Requesting: {$requester['full_name']}\n
				Date of Request: {$request['created']}\n
				Location: {$floor['short_global_label']}\n
				Requested Move-Out Date: {$request['move_out_date']}"
		));

		if (!$request['is_using_moving_company'])
		{
			$email_body = "Thanks for submitting a Move Out Request. We’re sorry to see you go, but hope that your next steps are exciting ones.\n
					Regarding the move out date requested, our Operations Team will check the necessary calendars and send you a response in 7-10 business days.\n
					Please note that if all the criteria in your license agreement are met, your security deposit will be returned to you within 30-60 business days from the date of account closing. If you have any questions, please contact service@microoffice.com. \n
					We’re always improving. Please let us know your thoughts by completing the Exit Survey at www.MicroOffice.com/exitsurvey. \n
					For your records, the information that you entered in the request is below.\n\n\n

					Notification: Member Move Out Requested\n
					Account Name: {$customerAccount['name']}\n
					Name of Person Requesting: {$requester['full_name']}\n
					Date of Request: {$request['created']}\n
					Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
					Requested Move-Out Date: {$request['move_out_date']}\n\n

					Cordially,\n
					The MicroOffice Team";

			// primaryCustomer
			$sent = $this->sendEmail(array(
				'to' => array(
					['name' => $primaryCustomer['full_name'], 'email' => $primaryCustomer['email']]
				),
				'subject' => 'Move-Out Request Confirmation',
				'text' => "{$primaryCustomer['full_name']},\n\n$email_body"
			));
			
			if ($primaryCustomer['id'] != $requester['id'])
			{
				// requester
				$sent += $this->sendEmail(array(
					'to' => array(
						['name' => $requester['full_name'], 'email' => $requester['email']]
					),
					'subject' => 'Move-Out Request Confirmation',
					'text' =>
						"{$requester['full_name']},\n\n$email_body"
				));
			}

			return $sent;
		}
		else
		{
			$email_body =
				"Thank you for submitting your Certificate of Insurance. A member of our Operations Team will contact you when your COI is approved.\n
				For your reference, your original Move Out Request information is included below:\n\n\n

				Notification: Member Move Out Requested\n
				Account Name: {$customerAccount['name']}\n
				Name of Person Requesting: {$requester['full_name']}\n
				Date of Request: {$request['created']}\n
				Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
				Requested Move-Out Date: {$request['move_out_date']}\n\n

				Cordially,\n
				The MicroOffice Team";

			// TODO check if $primary_contact_cust_pers_id == $move_out_requester_id

			// primaryCustomer
			$sent = $this->sendEmail(array(
				'to' => array(
					['name' => $primaryCustomer['full_name'], 'email' => $primaryCustomer['email']]
				),
				'subject' => 'Move-Out Request Confirmation',
				'text' => "{$primaryCustomer['full_name']},\n\n$email_body"
			));
			
			if ($primaryCustomer['id'] != $requester['id'])
			{
				// requester
				$sent += $this->sendEmail(array(
					'to' => array(
						['name' => $requester['full_name'], 'email' => $requester['email']]
					),
					'subject' => 'Move-Out Request Confirmation',
					'text' =>
						"{$requester['full_name']},\n\n$email_body"
				));
			}

			return $sent;
		}
	}

	public function emailTwoDaysBeforeMoveOut($id = null)
	{
		extract($this->fetch($id));

		$email_body = 
			"This is a reminder that your Move Out Date is approaching in two days.\n\n
			Please familiarize yourself with the Package Removal policy. Please also note that this policy varies by building,
			and that building fines issued for violations will be passed to your account. For details, please visit
			www.MicroOffice.com/help, and click on “6. Package Removals and Move Out Policy.”\n
			Notification: Member Move Out Requested\n
			Account Name: {$customerAccount['name']}\n
			Name of Person Requesting: {$requester['full_name']}\n
			Date of Request: {$request['created']}\n
			Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
			Requested Move-Out Date: {$request['move_out_date']}\n\n

			Cordially,\n
			The MicroOffice Team";

		// requester
		
		// primaryCustomer
		$sent = $this->sendEmail(array(
			'to' => array(
				['name' => $primaryCustomer['full_name'], 'email' => $primaryCustomer['email']]
			),
			'subject' => 'Two-Day Move-Out Reminder',
			'text' => "{$primaryCustomer['full_name']},\n\n$email_body"
		));
		
		if ($primaryCustomer['id'] != $requester['id'])
		{
			// requester
			$sent += $this->sendEmail(array(
				'to' => array(
					['name' => $requester['full_name'], 'email' => $requester['email']]
				),
				'subject' => 'Move-Out Request Confirmation',
				'text' =>
					"{$requester['full_name']},\n\n$email_body"
			));
		}

		return $sent;

	}

	public function emailFifteenDaysBeforeCloseOut($id = null)
	{
		extract($this->fetch($id));

		$sent = $this->sendEmail(array(
			'to' => array(['email' => EMAIL_BILLING]),
			'subject' => 'Notification: Account Closing in 15 Days',
			'text' =>
				"Please prepare to return security deposit.\n\n
				Account Name: {$customerAccount['name']}\n
				Name of Person Requesting: {$requester['full_name']}\n
				Date of Request: {$request['created']}\n
				Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
				Requested Move-Out Date: {$request['move_out_date']}"
		));
		return $sent;
	}

	public function emailFiveDaysBeforeMoveOut($id = null)
	{
		extract($this->fetch($id));

		$sent = $this->sendEmail(array(
			'to' => array(['email' => EMAIL_FACILITIES]),
			'subject' => 'Notification: Move-Out Date in 5 Days',
			'text' =>
				"Account Name: {$customerAccount['name']}\n
				Name of Person Requesting: {$requester['full_name']}\n
				Date of Request: {$request['created']}\n
				Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
				Requested Move-Out Date: {$request['move_out_date']}"
		));
		return $sent;
	}

	public function emailDayOfMoveOut($id = null)
	{
		extract($this->fetch($id));

		$email_body = 
			"Just a reminder, today is your Move Out Day.\n\n
			If applicable, don’t forget to pick up your Item Removal Passes from your Community Manager, 
			and please be sure to use the freight elevators.\n
			For other details regarding your move out, visit www.MicroOffice.com/help, and click on “6. 
			Package Removals and Move Out Policy.” Please also note that this policy varies by building, 
			and that building fines issued for violations will be passed to your account .\n
			Please note that if all the criteria in your license agreement are met, your security 
			deposit will be returned to you within 30-60 business days from the date of account closing. 
			If you have any questions, please contact service@microoffice.com.\n
			We’re always improving. Please let us know your thoughts by completing the Exit Survey at 
			www.MicroOffice.com/exitsurvey.\n\n
			Notification: Member Move Out Requested\n
			Account Name: {$customerAccount['name']}\n
			Name of Person Requesting: {$requester['full_name']}\n
			Date of Request: {$request['created']}\n
			Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
			Requested Move-Out Date: {$request['move_out_date']}\n\n
			Cordially,\n
			The MicroOffice Team";

		// primaryCustomer
		$sent = $this->sendEmail(array(
			'to' => array(
				['name' => $primaryCustomer['full_name'], 'email' => $primaryCustomer['email']]
			),
			'subject' => 'Move-Out Reminder',
			'text' => "{$primaryCustomer['full_name']},\n\n$email_body"
		));
		
		if ($primaryCustomer['id'] != $requester['id'])
		{
			// requester
			$sent += $this->sendEmail(array(
				'to' => array(
					['name' => $requester['full_name'], 'email' => $requester['email']]
				),
				'subject' => 'Move-Out Request Confirmation',
				'text' =>
					"{$requester['full_name']},\n\n$email_body"
			));
		}

		return $sent;
	}

	public function emailDayOfAccountClose($id = null)
	{
		extract($this->fetch($id));

		$email_body = 
			"Your account has been closed. As a courtesy, we will hold mail and packages for 30 days. 
			Please feel free to pick up your mail and packages during normal business hours, Monday 
			through Friday, from 9:00 am until 5:00 pm. After 30 days, all mail and packages will be 
			returned to sender, or discarded.\n
			Please note that if all the criteria in your license agreement are met, your security deposit 
			will be returned to you within 30-60 business days from the date of account closing. If you 
			have any questions, please contact service@microoffice.com.\n
			We’re always improving. Please let us know your thoughts by completing the Exit Survey at 
			www.MicroOffice.com/exitsurvey. \n\n
			Notification: Member Move Out Requested\n
			Account Name: {$customerAccount['name']}\n
			Name of Person Requesting: {$requester['full_name']}\n
			Date of Request: {$request['created']}\n
			Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
			Requested Move-Out Date: {$request['move_out_date']}\n\n
			Cordially,\n
			The MicroOffice Team";

		// primaryCustomer
		$sent = $this->sendEmail(array(
			'to' => array(
				['name' => $primaryCustomer['full_name'], 'email' => $primaryCustomer['email']]
			),
			'subject' => 'Move-Out Request Confirmation',
			'text' => "{$primaryCustomer['full_name']},\n\n$email_body"
		));
		
		if ($primaryCustomer['id'] != $requester['id'])
		{
			// requester
			$sent += $this->sendEmail(array(
				'to' => array(
					['name' => $requester['full_name'], 'email' => $requester['email']]
				),
				'subject' => 'Move-Out Request Confirmation',
				'text' =>
					"{$requester['full_name']},\n\n$email_body"
			));
		}

		return $sent;
	}
}

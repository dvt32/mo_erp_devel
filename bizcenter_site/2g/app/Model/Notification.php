<?php
App::uses('AppModel', 'Model');
/**
 * Notification Model
 *
 * @property Room $Room
 * @property Floor $Floor
 * @property Building $Building
 */
class Notification extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'notification_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'notification_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'A notification heading is required'
			)
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Room' => array(
			'className' => 'Room',
			'foreignKey' => 'room_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
		),
		'Building' => array(
			'className' => 'Building',
			'foreignKey' => 'building_id',
		)
	);
}

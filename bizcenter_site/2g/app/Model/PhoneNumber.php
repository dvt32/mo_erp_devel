<?php
App::uses('AppModel', 'Model');
/**
 * PhoneNumber Model
 *
 * @property Account $Account
 * @property Person $Person
 * @property Floor $Floor
 * @property Iad $Iad
 * @property IpPhone $IpPhone
 * @property BlockOutboundCaller $BlockOutboundCaller
 * @property SipRegisterUser $SipRegisterUser
 * @property InboundTrunk $InboundTrunk
 * @property OutboundTrunk $OutboundTrunk
 */
class PhoneNumber extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'telecom';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'phone_number_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed


// /**
//  * hasMany associations
//  *
//  * @var array
//  */
// 	public $hasMany = array(
// 		'Voicemail' => array(
// 			'className' => 'Voicemail',
// 			'foreignKey' => 'username',
// 			'finderQuery' => 'SELECT Voicemail.* FROM voicemail'
// 		)
// 	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
		),
		// 'Iad' => array(
		// 	'className' => 'Iad',
		// 	'foreignKey' => 'iad_id',
		// ),
		// 'IpPhone' => array(
		// 	'className' => 'IpPhone',
		// 	'foreignKey' => 'ip_phone_id',
		// ),
		// 'InboundTrunk' => array(
		// 	'className' => 'InboundTrunk',
		// 	'foreignKey' => 'inbound_trunk_id',
		// ),
		// 'OutboundTrunk' => array(
		// 	'className' => 'OutboundTrunk',
		// 	'foreignKey' => 'outbound_trunk_id',
		// )
	);

	
	public function beforeSave($options = array()) {
		// set ms access
		parent::beforeSave();
		
		$user_id = AuthComponent::user('id');
		if (empty($user_id)) {
			return false;
		}
		$this->data['PhoneNumber']['account_id'] = AuthComponent::user('cust_acct_id');

		return true;
	}
}

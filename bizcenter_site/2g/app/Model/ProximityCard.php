<?php
App::uses('AppModel', 'Model');
/**
 * ProximityCard Model
 *
 * @property Account $Account
 * @property Person $Person
 * @property AccessLevel $AccessLevel
 * @property Floor $Floor
 */
class ProximityCard extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'proximity_card_tbl';

// Original JSON array from the get requests
// columns created in the db is labeled below
// {
//     "FirstName": "Aiden", 
//     "LastName": "Darne",
//     "CardNumber": "1-42369", // card_number
//     "CardStatus": "Inactive",
//     "EmployeeId": "bioth", // intellim_employee_id
//     "UserRole": "None", // user_role
//     "InGroup": false, // in_group
//     "IsGroupMember": false, // is_group_member
//     "MiddleInitial": "", 
//     "IsCredentialHolder": true, // is_credential_holder
//     "Id": 1053 // THIS IS THE ID USED TO SEND THE DELETE REQUEST FOR DISABLING PROXIMITY CARDS (intellim_id)
// }



/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
		),
		'AccessLevel' => array(
			'className' => 'AccessLevel',
			'foreignKey' => 'access_level_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
		)
	);
}

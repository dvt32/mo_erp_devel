<?php
App::uses('AppModel', 'Model');
/**
 * ProximityCardEdit Model
 *
 * @property Account $Account
 * @property Person $Person
 * @property AccessLevel $AccessLevel
 */
class ProximityCardEdit extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'proximity_card_edit_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AccessLevel' => array(
			'className' => 'AccessLevel',
			'foreignKey' => 'access_level_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


/**
 * Explicitly add a virtual field called full_name which just combines the first and last name
 * 
 * @return array $results
 */
	public function afterFind($results, $primary = false) {
		if (!empty($results)) {
			foreach ($results as &$result) {
				if (isset($result[$this->alias]['first_name']) && $result[$this->alias]['last_name']) {
					$first_name = @$result[$this->alias]['first_name'];
					$last_name = @$result[$this->alias]['last_name'];
					$result[$this->alias]['full_name'] = "{$first_name} {$last_name}";
				}
			}
		}
		return $results;
	}
}

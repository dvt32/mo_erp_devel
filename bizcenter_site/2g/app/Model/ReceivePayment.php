<?php
App::uses('AppModel', 'Model');
/**
 * ReceivePayment Model
 *
 * @property CustAcct $CustAcct
 * @property Employee $Employee
 * @property Deposit $Deposit
 */
class ReceivePayment extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'recv_payment_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		// 'Deposit' => array(
		// 	'className' => 'Deposit',
		// 	'foreignKey' => 'deposit_id',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => ''
		// )
	);
}

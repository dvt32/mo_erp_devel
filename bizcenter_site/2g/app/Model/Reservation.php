<?php
App::uses('AppModel', 'Model');
/**
 * Reservation Model
 *
 * @property Room $Room
 * @property CustomerAccount $CustomerAccount
 */
class Reservation extends AppModel {



/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'reservation_tbl';


/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'reservation_seq';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Room' => array(
			'className' => 'Room',
			'foreignKey' => 'room_id',
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
		)
	);

/**
 * User must be logged in to book a reservation.
 * This function sets the following fields:
 * 		$this->data['Reservation']['account_id']
 * 
 * @return boolean true if the user 
 */
	public function beforeValidate($options = array()) {
		parent::beforeValidate($options);

		$this->data[$this->alias]['account_id'] = AuthComponent::user('cust_acct_id');

		$duration = @$this->data[$this->alias]['duration'];
		if (!empty($duration)) {

			$reservation =& $this->data[$this->alias];

			$start_date = @$reservation['start_date'];
			$start_time = @$reservation['start_time'];

			if (empty($start_date) || empty($start_time))
				return false;

			// convert start datetime to format
			$timestring = $start_date . '-' . implode('-', $start_time);

			// create datetime for start_dt
			$start_time = DateTime::createFromFormat('Y-m-d-h-i-a', $timestring);
			$end_time = clone $start_time;
			$end_time->modify("+{$duration} minutes");


			// save datetimes
			$reservation['start_dt'] = $start_time->format('Y-m-d H:i:sO');
			$reservation['end_dt'] = $end_time->format('Y-m-d H:i:sO');
		}
		// debug($reservation);exit;

		return true;
	}

/**
 * Check that the logged in user's customerAccount has enough conference room minutes
 * 
 * @return boolean true if the difference of room_minutes and reservation_duration >= 0
 */
	public function beforeSave($options = array()) {

		// set ms access info
		parent::beforeSave($options);

		// check that user has enough conference room to book reservation
		// return $this->enoughTimeLeft();
	}


/**
 * validation rules
 * 
 * @var array
 */
	public $validate = array(

		// booker info
		'cust_acct_id' => array('required' => 'notEmpty'),
		'cust_pers_id' => array('required' => 'notEmpty'),

		// room
		'room_id' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please choose a room to reserve'
			)
		),

		// start date
		'start_dt' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please choose when your reservation starts'
			),
			'validTimeInterval' => array(
				'rule' => 'validTimeInterval',
				'message' => 'Start and end times seem to be reversed'
			),
			'noCollisions' => array(
				'rule' => 'noCollisions',
				'message' => 'Someone else has already booked the room for that time'
			),
			'enoughTimeLeft' => array(
				'rule' => 'enoughTimeLeft',
				'message' => 'You do not have enough conference room minutes remaining.'
			),
			'maxReservationDuration' => array(
				'rule' => 'maxReservationDuration',
				'message' => 'You cannot book a single reservation exceeding 6 hours'
			)
		),

		// end date
		'end_dt' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please choose when your reservation ends'
			)
		),
	);

/**
 * (custom validation rule)
 * check that logged in customer has enough conference room time remaining to book the reservation
 * this function sets the following field(s):
 * 		$this->data['Reservation']['duration']
 * 		$this->data['Reservation']['difference']
 * @return boolean
 */
	public function enoughTimeLeft() {

		// fetch room minutes
		$cust_acct_id = AuthComponent::user('cust_acct_id');
		$customerAccount = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'fields' => array('room_minutes'),
			'contain' => array()
		));

		// prep vars for calculation
		$room_minutes = $customerAccount['CustomerAccount']['room_minutes'];
		$end_time = strtotime($this->data['Reservation']['end_dt']);
		$start_time = strtotime($this->data['Reservation']['start_dt']);
		$duration = ($end_time - $start_time) / 60;


		// set duration field for use by afterSave
		$this->data['Reservation']['duration'] = $duration;

		// calculate the difference between room minutes and duration of the reservation
		$difference = $room_minutes - $duration;

		// set difference field for use by afterSave
		$this->data['Reservation']['difference'] = $difference;

		// debug(compact('room_minutes','end_time','start_time','duration','difference'));

		return $difference >= 0;
	}

/**
 * (custom validation rule)
 * check that time is in the future
 * @return boolean
 */
	public function timeInFuture($check, $allowEqual = false) {
		$value = array_values($check);
		$value = $value[0];
		return $allowEqual ? strtotime($value) >= time() : strtotime($value) > time();
	}

/**
 * (custom validation rule)
 * Checks that the start-date precedes the end-date
 * @return boolean
 */
	public function validTimeInterval() {
		return $this->data[$this->alias]['start_dt'] < $this->data[$this->alias]['end_dt'];
	}

/**
 * (custom validation rule)
 * Checks to make sure it's business hours
 */
	public function startBusinessHours() {

		$start = date("H", strtotime($this->data[$this->alias]['start_dt']));
		if ($start < 9) {
			return false;
		}

		return true;
	}


/**
 * (custom validation rule)
 * Checks to make sure it's business hours
 */
	public function endBusinessHours() {

		$end = date("H", strtotime($this->data[$this->alias]['end_dt']));
		
		if ($end > 18) {
			return false;
		}

		return true;
	}

/**
 * (custom validation rule)
 * Checks that the duration of the reservation does not exceed the given value
 * @return boolean true if the reservation duration is less than the max allowed
 */
	public function maxReservationDuration() {
		$end_time = strtotime($this->data[$this->alias]['end_dt']);
		$start_time = strtotime($this->data[$this->alias]['start_dt']);
		return ($end_time - $start_time) < MAX_RESERVATION_DURATION_IN_SECONDS;
	}


/**
 * Calculate reservation duration
 * 
 * 
 */
	public function duration($id = null) {
		$this->id = $id ?: $this->id;
		$end_time = strtotime($this->data[$this->alias]['end_dt']);
		$start_time = strtotime($this->data[$this->alias]['start_dt']);
	}

/**
 * (custom validation rule)
 * Checks for collisions between the requested time interval and existing reservations
 * 
 * @return boolean
 */
	public function noCollisions() {

		// check for collisions
		$id = @$this->data[$this->alias]['id'];
		$room_id = $this->data[$this->alias]['room_id'];
		$start = $this->data[$this->alias]['start_dt'];
		$end   = $this->data[$this->alias]['end_dt'];

		// set conditions
		$conditions = array(
			// same room
			'Reservation.room_id' => $room_id,
			// check range overlap
			'Reservation.start_dt <' => $end,
			'Reservation.end_dt >' => $start,
		);

		// exclude this reservation
		if (!empty($id)) {
			$conditions['Reservation.id !='] = $id;
		}

		// find collisions
		$collisions = $this->find('count', array(
			'conditions' => $conditions,
			'fields' => array('id','start_dt','end_dt'),
			'contain' => array()
		));

		return $collisions === 0;
	}

	/**
	 * Takes js calendar format and converts to db-saveable timestamp+timezone format
	 * @param string $str format used by calendar e.g., 'Fri Sep 06 2013 20:00:00 GMT-0400 (Eastern Daylight Time)'
	 * @return formatted timestamp+timezone e.g., '2013-09-06 20:00:00-04'
	 */
	public function convertCalendarFormat($str) {
		$datetime = substr($str,0,24);
		$timezone = substr($str,28,5);
		$str = $datetime . ' ' . $timezone;
		$datetime = DateTime::createFromFormat('D M d Y H:i:s O', $str);
		$formatted = substr($datetime->format('Y-m-d H:i:sO'),0,22);
		return $formatted;
	}
}

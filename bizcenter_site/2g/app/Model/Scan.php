<?php
App::uses('AppModel', 'Model');
/**
 * Scan Model
 *
 * @property Account $Account
 * @property Person $Person
 * @property File $File
 */
class Scan extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'scan_tbl';

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		// 'ProximityCard' => array(
		// 	'className' => 'ProximityCard',
		// 	'foreignKey' => 'cust_pers_id',
		// 	'dependent' => false,
		// )
	);


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'File' => array(
			'className' => 'MoFile',
			'foreignKey' => 'file_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

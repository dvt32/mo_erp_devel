<?php
App::uses('AppModel', 'Model');
/**
 * Service Model
 *
 * @property CustomerAccount $CustomerAccount
 */
class Service extends AppModel {


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'service_tbl';



/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'service_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

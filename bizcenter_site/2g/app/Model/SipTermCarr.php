<?php
App::uses('AppModel', 'Model');
/**
 * SipTermCarr Model
 *
 * @property SipTermRateExceptTbl $SipTermRateExceptTbl
 * @property SipTermRateTbl $SipTermRateTbl
 */
class SipTermCarr extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'telecom';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sip_term_carr_tbl';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'SipTermRateExcept' => array(
			'className' => 'SipTermRateExcept',
			'foreignKey' => 'sip_term_carr_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'SipTermRate' => array(
			'className' => 'SipTermRate',
			'foreignKey' => 'sip_term_carr_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

<?php
App::uses('AppModel', 'Model');
/**
 * SipTermRateExcept Model
 *
 * @property SipTermCarr $SipTermCarr
 */
class SipTermRateExcept extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'telecom';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sip_term_rate_except_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SipTermCarr' => array(
			'className' => 'SipTermCarr',
			'foreignKey' => 'sip_term_carr_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

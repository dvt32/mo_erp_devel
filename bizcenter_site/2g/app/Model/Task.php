<?php
App::uses('AppModel', 'Model');
/**
 * Task Model
 *
 * @property Employee $Employee
 * @property RequestedbyEmployee $RequestedbyEmployee
 * @property RequestedbyCustPers $RequestedbyCustPers
 * @property RequestedbyCustAcct $RequestedbyCustAcct
 * @property Floor $Floor
 */
class Task extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'task_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'task_seq';


/**
 * Validations
 *
 */
 public $validate = array(

 	// requested completion date
 	'requested_completion_date' => array(
		'notEmpty' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Please choose a requested completion date'
		),
    'timeInFuture' => array(
        'rule' => array('timeInFuture'),
        'message' => 'The requested completion date must be in the future'
    ),
  ),
  // task description
  'description' => array(
  	'notEmpty' => array(
  		'rule' => 'notEmpty',
  		'required' => true,
  		'message' => 'A description of what you\'d like done is required'
  	)
  ),
  // task description
  'floor_id' => array(
  	'notEmpty' => array(
  		'rule' => 'notEmpty',
  		'required' => true,
  		'message' => 'Please choose a floor'
  	)
  ),
  // task description
  'area' => array(
  	'notEmpty' => array(
  		'rule' => 'notEmpty',
  		'required' => true,
  		'message' => 'Please try your best to Categorize your Request'
  	)
  ),
);

/**
 * static list of task categories
 */
	public $areas = array(
		'Cleaning' =>     'Cleaning',
		'Billing' =>      'Billing',
		'General Office' =>   'General Office',
		'Internet' =>     'Internet',
		'Maintenance' =>  'Maintenance',
		'Phone' =>        'Phone',
	);

/**
 * static list of task statuses
 */
	public $statuses = array(
		'requested' => 'Requested',
		'assigned' => 'Assigned',
		'completed' => 'Completed',
		'cancelled' => 'Cancelled',
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'assigned_employee_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'requestedby_cust_pers_id'
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'requestedby_cust_acct_id'
		)
	);

/**
 * (custom validation rule)
 * Disables item removal requests for the same day after 3pm
 * 
 * @return 
 */
	public function beforeValidate($options = array()) {
		if (isset($this->data[$this->alias]['requested_completion_date'])) {
			$requested = date('Y-m-d', strtotime($this->data[$this->alias]['requested_completion_date']));
			$today = date('Y-m-d');

			// if the task area (category) is "Item Removal"
			if (strcmp($this->data[$this->alias]['area'], "Item Removal") === 0) {
				$this->validator()->remove('requested_completion_date','timeInFuture');


				// check if the requested removal day is today
				if ($requested === $today) {

					// check if it is after 3pm
					$now = date(DATETIME_FORMAT);
					$cutoff = date(DATETIME_FORMAT, strtotime(ITEM_REMOVAL_CUTOFF));


					// check that this item removal request beats the cutoff
					$beat_same_day_cutoff = $now < $cutoff;

					if (!$beat_same_day_cutoff) {
						$this->validationErrors['requested_completion_date'] = 
							"Item removal requests cannot be processed after 3pm for the same day";
						// unset($this->validate['requested_completion_date']);
						return false;
					}
				}
			}
		}
		return true;
	}

/**
 * manually assign the id using select nextval (fix for the currval sql error)
 * 
 */
	public function beforeSave($options = array()) {
		parent::beforeSave();
		$nextval = $this->query("select nextval('task_seq')");
		$this->data[$this->alias]['id'] = $nextval[0][0]['nextval'];
		// debug($this->data[$this->alias]['id']);
		return true;
	}
}

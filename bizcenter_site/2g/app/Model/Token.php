<?php
App::uses('CakeEmail', 'Network/Email');

/**
 * Token Model
 * Used for all user-specific email communications,
 * e.g., forgot password, email address confirmation
 * 
 * @property User $User
 */
class Token extends AppModel {

	public $useTable = "token_tbl";

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'token_seq';


/**
 * validation rules
 * 
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true
			)
		),
		'expires' => 'datetime',
		'jumble' => 'isUnique',
	);

/**
 * Generates a strongly random string. Guaranteed to be unique.
 * @param $len desired length of string
 * @return Random url-safe string of desired length
 */
	public function generateJumble($len = 16) {
		do {
			$fp = @fopen('/dev/urandom','rb');
			$jumble = '';
			if ($fp !== FALSE) {
				$bytes = ((int) ($len * 3 / 4)) + 1;
				$jumble .= @fread($fp, $bytes);
				@fclose($fp);
			} else {
				// fallback
				$times = ((int) ($len / 56)) + 1;
				while ($times--) {
					$jumble .= sha1(mt_rand());
				}
			}
			$jumble = base64_encode($jumble); // convert from binary to string
			$jumble = strtr($jumble, '+/', '-_'); // replace non-url chars
			$jumble = str_replace('=', ' ', $jumble); // remove '=' from the end
			$jumble = substr($jumble, 0, $len); // truncate to desired length
		} while ($this->find('count', array('conditions' => array('jumble' => $jumble))) > 0);
		return $jumble;
	}

/**
 * Finds a valid token given a jumble-type combination.
 * A token is valid iff the expiration date has not passed, and it has not been used.
 * 
 * @return $token A valid token or null if no such valid token exists
 */
	public function findValid($jumble, $type) {
		// fetch the requested token
		$requested_token = $this->findByJumbleAndType($jumble, $type);

		// continue iff the requested token is found
		if (!empty($requested_token)) {
			// continue iff the requested token is unused and not expired
			if (empty($requested_token['Token']['is_spent']) && $requested_token['Token']['expires'] > date(DATETIME_FORMAT)) {
				// a user has only one valid token of a particular type
				$valid_token = $this->find(
					'first',
					array(
						'order' => 'Token.created DESC',
						'conditions' => array(
							'Token.user_id' => $requested_token['Token']['user_id'],
							'Token.type' => $requested_token['Token']['type']
						),
						'fields' => 'Token.id'
					)
				);

				// return the token iff the requested token is the valid token
				if ($requested_token['Token']['id'] === $valid_token['Token']['id']) {
					return $requested_token;
				}
			}
		}
		return null;
	}

/**
 * Marks all tokens with a particular user-and-type combo as used, 
 * i.e., each user only has one valid token of a particular type,
 */
	public function markAllUsed($user_id, $type = 'resetPassword') {
		$fields = array('Token.is_spent' => true);
		$conditions = array('Token.user_id' => $user_id, 'Token.type' => $type);
		return $this->updateAll($fields, $conditions);
	}

/* CALLBACK METHODS ------------------------------------ */

	public function beforeValidate($options = array()) {

		// generate and set the jumble
		if (!isset($this->data['Token']['jumble'])) {
			$this->jumble = $this->data['Token']['jumble'] = $this->generateJumble();
		}

		// set the token expiration datetime
		if (!isset($this->data['Token']['expires'])) {
			$this->expires = $this->data['Token']['expires'] = date(DATETIME_FORMAT, strtotime('+1 month'));
		}

		// set the associated user_id
		// if (!isset($this->data['Token']['user_id'])) {
		// 	if (isset($this->data['User']['id'])) {
		// 		$this->data['Token']['user_id'] = $this->data['User']['id'];
		// 	} elseif (isset($this->data['User']['username'])) {
		// 		$this->User->id = $user_id = $this->User->lookupId('username',$this->data['User']['username']);
		// 		if ($this->User->exists()) {
		// 			$this->data['Token']['user_id'] = $user_id;
		// 		}
		// 	}
		// }
		return true;
	}

	public function beforeSave($options = array()) {
		$this->data[$this->alias]['is_spent'] = 0;
		return true;
	}

	public function afterSave($created, $options = array()) {
		// token saved?
		if ($created) {
			// fetch just created token info
			$token = $this->find('first', [
				'conditions' => [$this->alias . '.id' => $this->id],
				'contain' => 'User.username'
			]);

			$this->send($token['Token']['type'], $token['Token']['jumble'], $token['User']['username']);
		}
	}


/* ASSOCIATIONS ---------------------------------------- */

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
	);

/**
 * Send an email with a token link
 */
	private function send($type, $jumble, $username) {

		// configure email
		$email = new CakeEmail();
		$email->config(EMAIL_CONFIG)->to($username);
		$email->subject('BizCenter Request');

		// build url
		$host = FULL_BASE_URL;
		$url = "{$host}/tokens/{$type}/{$jumble}";

		// if (Configure::read('debug') > 0) {
		// 	echo $url;
		// 	exit;
		// } else {
		// 	// send email
		// 	$email->send($url);
		// }
		$email->send($url);
	}

}


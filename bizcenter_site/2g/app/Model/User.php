<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Customer $Customer
 * @property ProximityCard $ProximityCard
 */
class User extends AppModel {

/**
 * table this model uses
 * 
 * @var string
 */
	public $useTable = 'user_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'user_seq';

/**
 * hash the password before saving the user
 * 
 * @return boolean
 */
	public function beforeSave($options = array()) {

		// check that password field is trying to be saved
		if (isset($this->data[$this->alias]['password'])) {

			// hash the password using settings in core
			$password = AuthComponent::password($this->data[$this->alias]['password']);
			
			// set the data to be saved
			$this->data[$this->alias]['password'] = $password;
		}

    return true; // must be true to continue save
  }

/**
 * updates the date time of last_login
 * 
 * @return void
 */
  public function updateLogin() {

    // temporarily store modified time
    $modified = $this->field('modified');
    
    // update last login time
    $this->saveField('last_logged_in', date(DATETIME_FORMAT));

    // restore modified time
    $this->saveField('modified', $modified);
  }

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(

		// email address
		'username' => array(
			'required' => array(
				'rule' => 'notEmpty',
        'required' => true,
				'message' => 'Email address is required'
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please enter a valid email address'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'That email address has already been registered'
			),
		),

		// password
		'password' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'Password is required'
			),
			// 'minLength' => array(
			// 	'rule' => array('minLength',8),
			// 	'message' => 'Password must be at least 8 characters in length'
			// ),
		),

    // confirm password
    'confirm_password' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'required' => true,
        'message' => 'Please make sure you typed in both passwords correctly. ',
      ),
      'matches' => array(
        'rule' => array('matches','password'),
        'message' => 'Please make sure you typed in both passwords correctly. ',
      ),
    ),

    // current password    
    'current_password' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'required' => true,
        'on' => 'update',
      ),
      'matchesCurrent' => array(
        'rule' => 'matchesCurrentPassword',
        'on' => 'update',
        'matches' => 'The password you entered does not match your existing password'
      )
    ),
	);

/**
 * checks if the current_password field matches the stored value
 * 
 * @return boolean
 */
  public function matchesCurrentPassword($check, $ctrl) {
    $value = array_values($check);
    $value = $value[0];
    return $this->verify($this->data['User']['id'], $value);
  }

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'user_id',
		),
	);
/**
 * hasOne associations
 *
 * @var array
 */
	public $hasMany = array(
		'Token' => array(
			'className' => 'Token',
			'foreignKey' => 'user_id',
		),
	);

}

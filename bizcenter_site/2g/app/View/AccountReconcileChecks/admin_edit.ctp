<div class="accountReconcileChecks form">
<?php echo $this->Form->create('AccountReconcileCheck'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Account Reconcile Check'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('bank_number');
		echo $this->Form->input('account_number');
		echo $this->Form->input('is_void');
		echo $this->Form->input('check_number');
		echo $this->Form->input('check_amount');
		echo $this->Form->input('issue_date_of_check');
		echo $this->Form->input('additional_data');
		echo $this->Form->input('processed_dt');
		echo $this->Form->input('notes');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AccountReconcileCheck.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AccountReconcileCheck.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Account Reconcile Checks'), array('action' => 'index')); ?></li>
	</ul>
</div>

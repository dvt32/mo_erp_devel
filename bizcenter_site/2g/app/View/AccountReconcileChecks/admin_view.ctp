<div class="accountReconcileChecks view">
<h2><?php echo __('Account Reconcile Check'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Number'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['bank_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Account Number'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['account_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Void'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['is_void']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Check Number'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['check_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Check Amount'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['check_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Issue Date Of Check'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['issue_date_of_check']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Additional Data'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['additional_data']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Processed Dt'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['processed_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($accountReconcileCheck['AccountReconcileCheck']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Account Reconcile Check'), array('action' => 'edit', $accountReconcileCheck['AccountReconcileCheck']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Account Reconcile Check'), array('action' => 'delete', $accountReconcileCheck['AccountReconcileCheck']['id']), null, __('Are you sure you want to delete # %s?', $accountReconcileCheck['AccountReconcileCheck']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Account Reconcile Checks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account Reconcile Check'), array('action' => 'add')); ?> </li>
	</ul>
</div>

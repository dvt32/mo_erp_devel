<div class="accountReconcileChecks index">
	<h2><?php echo __('Account Reconcile Checks'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_number'); ?></th>
			<th><?php echo $this->Paginator->sort('account_number'); ?></th>
			<th><?php echo $this->Paginator->sort('is_void'); ?></th>
			<th><?php echo $this->Paginator->sort('check_number'); ?></th>
			<th><?php echo $this->Paginator->sort('check_amount'); ?></th>
			<th><?php echo $this->Paginator->sort('issue_date_of_check'); ?></th>
			<th><?php echo $this->Paginator->sort('additional_data'); ?></th>
			<th><?php echo $this->Paginator->sort('processed_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($accountReconcileChecks as $accountReconcileCheck): ?>
	<tr>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['id']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['bank_number']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['account_number']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['is_void']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['check_number']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['check_amount']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['issue_date_of_check']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['additional_data']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['processed_dt']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['notes']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($accountReconcileCheck['AccountReconcileCheck']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $accountReconcileCheck['AccountReconcileCheck']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $accountReconcileCheck['AccountReconcileCheck']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $accountReconcileCheck['AccountReconcileCheck']['id']), null, __('Are you sure you want to delete # %s?', $accountReconcileCheck['AccountReconcileCheck']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Account Reconcile Check'), array('action' => 'add')); ?></li>
	</ul>
</div>

<div class="achDebitTransactions form">
<?php echo $this->Form->create('AchDebitTransaction'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Ach Debit Transaction'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('amount');
		echo $this->Form->input('customer_name');
		echo $this->Form->input('customer_bank_id');
		echo $this->Form->input('customer_bank_acct_num');
		echo $this->Form->input('customer_bank_acct_type');
		echo $this->Form->input('company_bank_acct_num');
		echo $this->Form->input('company_bank_acct_type');
		echo $this->Form->input('start_date');
		echo $this->Form->input('prenote_date');
		echo $this->Form->input('last_collection_date');
		echo $this->Form->input('create_prenote');
		echo $this->Form->input('hold_transfer');
		echo $this->Form->input('queued_dt');
		echo $this->Form->input('processed_dt');
		echo $this->Form->input('notes');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('cancelled');
		echo $this->Form->input('electronic_check_id');
		echo $this->Form->input('recv_payment_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AchDebitTransaction.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AchDebitTransaction.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Ach Debit Transactions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('controller' => 'bank_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Electronic Checks'), array('controller' => 'electronic_checks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Electronic Check'), array('controller' => 'electronic_checks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('controller' => 'receive_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('controller' => 'receive_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>

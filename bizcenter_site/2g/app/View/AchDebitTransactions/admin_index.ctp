<div class="achDebitTransactions index">
	<h2><?php echo __('Ach Debit Transactions'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('cust_acct_id'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_name'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_bank_id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_bank_acct_num'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_bank_acct_type'); ?></th>
			<th><?php echo $this->Paginator->sort('company_bank_acct_num'); ?></th>
			<th><?php echo $this->Paginator->sort('company_bank_acct_type'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('prenote_date'); ?></th>
			<th><?php echo $this->Paginator->sort('last_collection_date'); ?></th>
			<th><?php echo $this->Paginator->sort('create_prenote'); ?></th>
			<th><?php echo $this->Paginator->sort('hold_transfer'); ?></th>
			<th><?php echo $this->Paginator->sort('queued_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('processed_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('cancelled'); ?></th>
			<th><?php echo $this->Paginator->sort('electronic_check_id'); ?></th>
			<th><?php echo $this->Paginator->sort('recv_payment_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($achDebitTransactions as $achDebitTransaction): ?>
	<tr>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($achDebitTransaction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $achDebitTransaction['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['amount']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['customer_name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($achDebitTransaction['BankAccount']['bank_account_nickname'], array('controller' => 'bank_accounts', 'action' => 'view', $achDebitTransaction['BankAccount']['id'])); ?>
		</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['customer_bank_acct_num']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['customer_bank_acct_type']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['company_bank_acct_num']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['company_bank_acct_type']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['prenote_date']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['last_collection_date']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['create_prenote']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['hold_transfer']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['queued_dt']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['processed_dt']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['notes']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($achDebitTransaction['AchDebitTransaction']['cancelled']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($achDebitTransaction['ElectronicCheck']['id'], array('controller' => 'electronic_checks', 'action' => 'view', $achDebitTransaction['ElectronicCheck']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($achDebitTransaction['ReceivePayment']['id'], array('controller' => 'receive_payments', 'action' => 'view', $achDebitTransaction['ReceivePayment']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $achDebitTransaction['AchDebitTransaction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $achDebitTransaction['AchDebitTransaction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $achDebitTransaction['AchDebitTransaction']['id']), null, __('Are you sure you want to delete # %s?', $achDebitTransaction['AchDebitTransaction']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ach Debit Transaction'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('controller' => 'bank_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Electronic Checks'), array('controller' => 'electronic_checks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Electronic Check'), array('controller' => 'electronic_checks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('controller' => 'receive_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('controller' => 'receive_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="">
	<h2><?= __('Ach Debit Transactions') ?></h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('id') ?></th>
			<th><?= $this->Paginator->sort('cust_acct_id') ?></th>
			<th><?= $this->Paginator->sort('amount') ?></th>
			<th><?= $this->Paginator->sort('customer_name') ?></th>
			<th><?= $this->Paginator->sort('customer_bank_acct_num') ?></th>
			<th><?= $this->Paginator->sort('customer_bank_acct_type') ?></th>
			<th><?= $this->Paginator->sort('company_bank_acct_num') ?></th>
			<th><?= $this->Paginator->sort('company_bank_acct_type') ?></th>
			<th><?= $this->Paginator->sort('queued_dt') ?></th>
			<th><?= $this->Paginator->sort('processed_dt') ?></th>
			<th><?= $this->Paginator->sort('creation_dt') ?></th>
			<th><?= $this->Paginator->sort('cancelled') ?></th>
			<th><?= $this->Paginator->sort('electronic_check_id') ?></th>
			<th><?= $this->Paginator->sort('recv_payment_id') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<? foreach ($achDebitTransactions as $achDebitTransaction): ?>
	<tr>
		<td><?= $this->Html->link($achDebitTransaction['AchDebitTransaction']['id'], array('controller' => 'achDebitTransactions', 'action' => 'view', $achDebitTransaction['AchDebitTransaction']['id'])) ?> </td>
		<td><?= $this->Html->link($achDebitTransaction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $achDebitTransaction['CustomerAccount']['id'])) ?></td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['amount']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['customer_name']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['customer_bank_acct_num']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['customer_bank_acct_type']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['company_bank_acct_num']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['company_bank_acct_type']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['queued_dt']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['processed_dt']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['creation_dt']) ?>&nbsp;</td>
		<td><?= h($achDebitTransaction['AchDebitTransaction']['cancelled']) ?>&nbsp;</td>
		<td><?= $this->Html->link($achDebitTransaction['ElectronicCheck']['id'], array('controller' => 'electronic_checks', 'action' => 'view', $achDebitTransaction['ElectronicCheck']['id'])) ?> </td>
		<td><?= $this->Html->link($achDebitTransaction['ReceivePayment']['id'], array('controller' => 'receive_payments', 'action' => 'view', $achDebitTransaction['ReceivePayment']['id'])) ?> </td>
		<td class="actions">
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $achDebitTransaction['AchDebitTransaction']['id'])) ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $achDebitTransaction['AchDebitTransaction']['id']), null, __('Are you sure you want to delete # %s?', $achDebitTransaction['AchDebitTransaction']['id'])) ?>
		</td>
	</tr>
<? endforeach ?>
	</table>
	<p>
	<?= $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Ach Debit Transaction'), array('action' => 'add')) ?></li>
		<li><?= $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Bank Accounts'), array('controller' => 'bank_accounts', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Electronic Checks'), array('controller' => 'electronic_checks', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Electronic Check'), array('controller' => 'electronic_checks', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Receive Payments'), array('controller' => 'receive_payments', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Receive Payment'), array('controller' => 'receive_payments', 'action' => 'add')) ?> </li>
	</ul>
</div>

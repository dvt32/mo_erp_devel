<div class="achDebitTransactions view">
<h2><?php echo __('Ach Debit Transaction'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($achDebitTransaction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $achDebitTransaction['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Name'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['customer_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($achDebitTransaction['BankAccount']['bank_account_nickname'], array('controller' => 'bank_accounts', 'action' => 'view', $achDebitTransaction['BankAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Bank Acct Num'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['customer_bank_acct_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Bank Acct Type'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['customer_bank_acct_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Bank Acct Num'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['company_bank_acct_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Bank Acct Type'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['company_bank_acct_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prenote Date'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['prenote_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Collection Date'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['last_collection_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create Prenote'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['create_prenote']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hold Transfer'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['hold_transfer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Queued Dt'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['queued_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Processed Dt'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['processed_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cancelled'); ?></dt>
		<dd>
			<?php echo h($achDebitTransaction['AchDebitTransaction']['cancelled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Electronic Check'); ?></dt>
		<dd>
			<?php echo $this->Html->link($achDebitTransaction['ElectronicCheck']['id'], array('controller' => 'electronic_checks', 'action' => 'view', $achDebitTransaction['ElectronicCheck']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Receive Payment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($achDebitTransaction['ReceivePayment']['id'], array('controller' => 'receive_payments', 'action' => 'view', $achDebitTransaction['ReceivePayment']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ach Debit Transaction'), array('action' => 'edit', $achDebitTransaction['AchDebitTransaction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ach Debit Transaction'), array('action' => 'delete', $achDebitTransaction['AchDebitTransaction']['id']), null, __('Are you sure you want to delete # %s?', $achDebitTransaction['AchDebitTransaction']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ach Debit Transactions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ach Debit Transaction'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('controller' => 'bank_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Electronic Checks'), array('controller' => 'electronic_checks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Electronic Check'), array('controller' => 'electronic_checks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('controller' => 'receive_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('controller' => 'receive_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>

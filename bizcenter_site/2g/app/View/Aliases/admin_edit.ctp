<div class="aliases form">
<?php echo $this->Form->create('Alias'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Alias'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('customer_account_id');
		echo $this->Form->input('name');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Alias.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Alias.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Aliases'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

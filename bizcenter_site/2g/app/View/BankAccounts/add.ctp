<?
$bank_account_types = array_combine(array('checking','savings'),array('checking','savings'));
$bank_account_ownership_types = array_combine(array('Personal','Business'),array('Personal','Business'));
?>

<div class="span12">
	<div class="bankAccounts form">
	<?= $this->Form->create('BankAccount') ?>
		<fieldset>
		<div class="row-fluid">
			<div class="span12">
				<legend><?= __('Add Bank Account') ?></legend>
			</div>
		</div>
		<div class="row-fluid">

			<div class="span6">
				<h3>Bank Account Information</h3>
				<?= $this->Form->input('bank_account_nickname', array('maxlength' => 32)) ?>
				<?= $this->Form->input('depository_name', array('label' => 'Bank Name')) ?>
				<?= $this->Form->input('bank_routing_number') ?>
				<?= $this->Form->input('bank_account_number') ?>
				<?= $this->Form->input('bank_account_type', array('empty' => '-- choose one --', 'options' => $bank_account_types)) ?>
				<?= $this->Form->input('bank_account_ownership_type', array('empty' => '-- choose one --', 'options' => $bank_account_ownership_types)) ?>
			</div>
		
			<div class="span6">
				<h3>Billing Address</h3>
				<?= $this->Form->input('bank_account_customer_name') ?>
				<?= $this->Form->input('address_1', array('label' => 'Billing Address 1')) ?>
				<?= $this->Form->input('address_2', array('label' => 'Billing Address 2')) ?>
				<?= $this->Form->input('city') ?>
				<?= $this->Form->input('state') ?>
				<?= $this->Form->input('zip_code') ?>
				<?= $this->Form->input('country') ?>
				<?= $this->Form->input('is_primary', array('label' => 'Make this my Primary Account')) ?>
			</div>

		</div>
		</fieldset>
	<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')) ?>
	<?= $this->Form->end() ?>
	</div>
</div>

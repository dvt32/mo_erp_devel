<div class="bankAccounts form">
<?php echo $this->Form->create('BankAccount'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Bank Account'); ?></legend>
	<?php
		echo $this->Form->input('customer_account_id');
		echo $this->Form->input('depository_name');
		echo $this->Form->input('address_1');
		echo $this->Form->input('address_2');
		echo $this->Form->input('city');
		echo $this->Form->input('state');
		echo $this->Form->input('zip_code');
		echo $this->Form->input('bank_account_type');
		echo $this->Form->input('bank_routing_number');
		echo $this->Form->input('bank_account_number');
		echo $this->Form->input('notes');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('bank_account_customer_name');
		echo $this->Form->input('bank_account_ownership_type');
		echo $this->Form->input('bank_account_nickname');
		echo $this->Form->input('is_primary');
		echo $this->Form->input('country');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

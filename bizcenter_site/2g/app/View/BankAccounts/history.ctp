<div class="bankAccounts index">
	<h2><?php echo __('Bank Accounts'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('customer_account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('depository_name'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_type'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_routing_number'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_number'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_customer_name'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_ownership_type'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_nickname'); ?></th>
			<th><?php echo $this->Paginator->sort('is_primary'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($bankAccounts as $bankAccount): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($bankAccount['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $bankAccount['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($bankAccount['BankAccount']['depository_name']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['bank_account_type']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['bank_routing_number']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['bank_account_number']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['notes']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['bank_account_customer_name']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['bank_account_ownership_type']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['bank_account_nickname']); ?>&nbsp;</td>
		<td><?php echo h($bankAccount['BankAccount']['is_primary']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $bankAccount['BankAccount']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $bankAccount['BankAccount']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $bankAccount['BankAccount']['id']), null, __('Are you sure you want to delete # %s?', $bankAccount['BankAccount']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="bankAccounts view">
<h2><?php echo __('Bank Account'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($bankAccount['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $bankAccount['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Depository Name'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['depository_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address 1'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address 2'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip Code'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Type'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['bank_account_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Routing Number'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['bank_routing_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Number'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['bank_account_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Customer Name'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['bank_account_customer_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Ownership Type'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['bank_account_ownership_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Nickname'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['bank_account_nickname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Primary'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['is_primary']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($bankAccount['BankAccount']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bank Account'), array('action' => 'edit', $bankAccount['BankAccount']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Bank Account'), array('action' => 'delete', $bankAccount['BankAccount']['id']), null, __('Are you sure you want to delete # %s?', $bankAccount['BankAccount']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

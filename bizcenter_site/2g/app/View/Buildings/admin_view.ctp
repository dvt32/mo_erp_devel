<div class="buildings view">
<h2><?php echo __('Building'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($building['Building']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address 1'); ?></dt>
		<dd>
			<?php echo h($building['Building']['address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address 2'); ?></dt>
		<dd>
			<?php echo h($building['Building']['address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($building['Building']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($building['Building']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip Code'); ?></dt>
		<dd>
			<?php echo h($building['Building']['zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($building['Building']['country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($building['Building']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($building['Building']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($building['Building']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($building['Building']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($building['Building']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Global Label'); ?></dt>
		<dd>
			<?php echo h($building['Building']['short_global_label']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($building['Building']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Building'), array('action' => 'edit', $building['Building']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Building'), array('action' => 'delete', $building['Building']['id']), null, __('Are you sure you want to delete # %s?', $building['Building']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Building'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Ip Addresses'); ?></h3>
	<?php if (!empty($building['IpAddress'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Ip Address'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Hostname'); ?></th>
		<th><?php echo __('Isp Id'); ?></th>
		<th><?php echo __('Building Id'); ?></th>
		<th><?php echo __('Rir Owner Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($building['IpAddress'] as $ipAddress): ?>
		<tr>
			<td><?php echo $ipAddress['id']; ?></td>
			<td><?php echo $ipAddress['customer_account_id']; ?></td>
			<td><?php echo $ipAddress['ip_address']; ?></td>
			<td><?php echo $ipAddress['note']; ?></td>
			<td><?php echo $ipAddress['ms_access_creation_dt']; ?></td>
			<td><?php echo $ipAddress['ms_access_random_num']; ?></td>
			<td><?php echo $ipAddress['created']; ?></td>
			<td><?php echo $ipAddress['hostname']; ?></td>
			<td><?php echo $ipAddress['isp_id']; ?></td>
			<td><?php echo $ipAddress['building_id']; ?></td>
			<td><?php echo $ipAddress['rir_owner_id']; ?></td>
			<td><?php echo $ipAddress['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ip_addresses', 'action' => 'view', $ipAddress['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ip_addresses', 'action' => 'edit', $ipAddress['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ip_addresses', 'action' => 'delete', $ipAddress['id']), null, __('Are you sure you want to delete # %s?', $ipAddress['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Floors'); ?></h3>
	<?php if (!empty($building['Floor'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Building Id'); ?></th>
		<th><?php echo __('Label'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Bounding Polygon'); ?></th>
		<th><?php echo __('Short Global Label'); ?></th>
		<th><?php echo __('Short Local Label'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($building['Floor'] as $floor): ?>
		<tr>
			<td><?php echo $floor['id']; ?></td>
			<td><?php echo $floor['building_id']; ?></td>
			<td><?php echo $floor['label']; ?></td>
			<td><?php echo $floor['is_active']; ?></td>
			<td><?php echo $floor['note']; ?></td>
			<td><?php echo $floor['ms_access_creation_dt']; ?></td>
			<td><?php echo $floor['ms_access_random_num']; ?></td>
			<td><?php echo $floor['created']; ?></td>
			<td><?php echo $floor['bounding_polygon']; ?></td>
			<td><?php echo $floor['short_global_label']; ?></td>
			<td><?php echo $floor['short_local_label']; ?></td>
			<td><?php echo $floor['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'floors', 'action' => 'view', $floor['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'floors', 'action' => 'edit', $floor['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'floors', 'action' => 'delete', $floor['id']), null, __('Are you sure you want to delete # %s?', $floor['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

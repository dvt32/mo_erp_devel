<div class="buildings index">
	<h2><?php echo __('Buildings'); ?></h2>
	<table class="table">
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('address_1'); ?></th>
		<th><?php echo $this->Paginator->sort('address_2'); ?></th>
		<th><?php echo $this->Paginator->sort('city'); ?></th>
		<th><?php echo $this->Paginator->sort('state'); ?></th>
		<th><?php echo $this->Paginator->sort('zip_code'); ?></th>
		<th><?php echo $this->Paginator->sort('country'); ?></th>
		<th><?php echo $this->Paginator->sort('is_active'); ?></th>
		<th><?php echo $this->Paginator->sort('note'); ?></th>
		<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
		<th><?php echo $this->Paginator->sort('short_global_label'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($buildings as $building): ?>
	<tr>
		<td><?php echo h($building['Building']['id']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['address_1']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['address_2']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['city']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['state']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['zip_code']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['country']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['note']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($building['Building']['short_global_label']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $building['Building']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $building['Building']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $building['Building']['id']), null, __('Are you sure you want to delete # %s?', $building['Building']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Building'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="cdrs index">
	<h2><?php echo __('Cdrs'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('account_name'); ?></th>
			<th><?php echo $this->Paginator->sort('src_num'); ?></th>
			<th><?php echo $this->Paginator->sort('dest_num'); ?></th>
			<th><?php echo $this->Paginator->sort('duration'); ?></th>
			<th><?php echo $this->Paginator->sort('billsec'); ?></th>
			<th><?php echo $this->Paginator->sort('ef_billsec'); ?></th>
			<th><?php echo $this->Paginator->sort('ef_billmin'); ?></th>
			<th><?php echo $this->Paginator->sort('ef_rate'); ?></th>
			<th><?php echo $this->Paginator->sort('cost'); ?></th>
			<th><?php echo $this->Paginator->sort('start_stamp'); ?></th>
			<th><?php echo $this->Paginator->sort('answer_stamp'); ?></th>
			<th><?php echo $this->Paginator->sort('end_stamp'); ?></th>
			<th><?php echo $this->Paginator->sort('xml_cdr'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cdrs as $cdr): ?>
	<tr>
		<td><?php echo h($cdr['Cdr']['account_name']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['src_num']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['dest_num']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['duration']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['billsec']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['ef_billsec']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['ef_billmin']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['ef_rate']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['cost']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['start_stamp']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['answer_stamp']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['end_stamp']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['xml_cdr']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Cdr'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

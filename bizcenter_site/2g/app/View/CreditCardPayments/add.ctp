<div class="container">
	<fieldset>
		<legend><?= __('Make a Payment'); ?></legend>
		<div>
			
		</div>
		<div id="cc">
			<h3>Make a payment with your Credit Card</h3>
			<?= $this->Form->create('CreditCardPayment', array('controller' => 'CreditCardPayments', 'action' => 'add')); ?>
				<?= $this->Form->hidden('cust_acct_id', array('value' => AuthComponent::user('cust_acct_id'))) ?>
				<?= $this->Form->input('credit_card_id', array('options' => $credit_cards)) ?>
				<?= $this->Html->link('Add another Credit Card', array('controller' => 'creditCards', 'action' => 'add')) ?>
				<br>
				<div class="input-prepend input text">
					<label for="CreditCardPaymentTotalCharge">Amount to Charge</label>
					<span class="add-on">$</span>
					<input name="data[CreditCardPayment][total_charge]" class="span12" id="CreditCardPaymentTotalCharge" type="text" required="required">
				</div>
				<p>Please note that we will add a 3% - 4% convenience fee for credit cards.</p>
			</div>
			<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
		<?= $this->Form->end(); ?>
	</fieldset>
</div>
<script>
	if (!_.isNull(<?= $cid ?>)) {
		$("#CreditCardPaymentCreditCardId").val(<?= $cid ?>);
	}
</script>

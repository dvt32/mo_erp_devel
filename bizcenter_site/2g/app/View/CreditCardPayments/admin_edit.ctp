<div class="creditCardPayments form">
<?php echo $this->Form->create('CreditCardPayment'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Credit Card Payment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('card_number');
		echo $this->Form->input('card_expiration_month');
		echo $this->Form->input('card_expiration_year');
		echo $this->Form->input('card_security_code');
		echo $this->Form->input('billing_name');
		echo $this->Form->input('billing_company');
		echo $this->Form->input('billing_address_num');
		echo $this->Form->input('billing_zip_code');
		echo $this->Form->input('billing_address_1');
		echo $this->Form->input('billing_address_2');
		echo $this->Form->input('billing_city');
		echo $this->Form->input('billing_state');
		echo $this->Form->input('billing_country');
		echo $this->Form->input('billing_phone_number');
		echo $this->Form->input('billing_fax_number');
		echo $this->Form->input('billing_email_address');
		echo $this->Form->input('subtotal');
		echo $this->Form->input('charge_date');
		echo $this->Form->input('invoice_number');
		echo $this->Form->input('note');
		echo $this->Form->input('processed');
		echo $this->Form->input('cancelled');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('convenience_fee');
		echo $this->Form->input('remote_addr');
		echo $this->Form->input('remote_host');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CreditCardPayment.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('CreditCardPayment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Credit Card Payments'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Transactions'), array('controller' => 'credit_card_transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Transaction'), array('controller' => 'credit_card_transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>

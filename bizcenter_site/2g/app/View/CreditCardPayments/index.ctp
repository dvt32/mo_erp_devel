<div class="creditCardPayments index">
	<h2><?php echo __('Credit Card Payments'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('cust_acct_id'); ?></th>
			<th><?php echo $this->Paginator->sort('card_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_month'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_year'); ?></th>
			<th><?php echo $this->Paginator->sort('card_security_code'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_name'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_company'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_num'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_1'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_2'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_city'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_state'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_country'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_email_address'); ?></th>
			<th><?php echo $this->Paginator->sort('subtotal'); ?></th>
			<th><?php echo $this->Paginator->sort('charge_date'); ?></th>
			<th><?php echo $this->Paginator->sort('invoice_number'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('processed'); ?></th>
			<th><?php echo $this->Paginator->sort('cancelled'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('convenience_fee'); ?></th>
			<th><?php echo $this->Paginator->sort('remote_addr'); ?></th>
			<th><?php echo $this->Paginator->sort('remote_host'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($creditCardPayments as $creditCardPayment): ?>
	<tr>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($creditCardPayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $creditCardPayment['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['card_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['card_expiration_month']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['card_expiration_year']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['card_security_code']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_name']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_company']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_address_num']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_zip_code']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_address_1']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_address_2']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_city']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_state']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_country']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_fax_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['billing_email_address']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['subtotal']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['charge_date']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['invoice_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['note']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['processed']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['cancelled']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['convenience_fee']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['remote_addr']); ?>&nbsp;</td>
		<td><?php echo h($creditCardPayment['CreditCardPayment']['remote_host']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $creditCardPayment['CreditCardPayment']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $creditCardPayment['CreditCardPayment']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $creditCardPayment['CreditCardPayment']['id']), null, __('Are you sure you want to delete # %s?', $creditCardPayment['CreditCardPayment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Credit Card Payment'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Transactions'), array('controller' => 'credit_card_transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Transaction'), array('controller' => 'credit_card_transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="creditCardPayments view">
<h2><?php echo __('Credit Card Payment'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($creditCardPayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $creditCardPayment['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Number'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['card_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Month'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['card_expiration_month']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Year'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['card_expiration_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Security Code'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['card_security_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Name'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Company'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address Num'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_address_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Zip Code'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address 1'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address 2'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing City'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing State'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Country'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Phone Number'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Fax Number'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Email Address'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['billing_email_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subtotal'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['subtotal']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Charge Date'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['charge_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invoice Number'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['invoice_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Processed'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['processed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cancelled'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['cancelled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Convenience Fee'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['convenience_fee']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remote Addr'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['remote_addr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remote Host'); ?></dt>
		<dd>
			<?php echo h($creditCardPayment['CreditCardPayment']['remote_host']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Credit Card Payment'), array('action' => 'edit', $creditCardPayment['CreditCardPayment']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Credit Card Payment'), array('action' => 'delete', $creditCardPayment['CreditCardPayment']['id']), null, __('Are you sure you want to delete # %s?', $creditCardPayment['CreditCardPayment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Payments'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Payment'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Transactions'), array('controller' => 'credit_card_transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Transaction'), array('controller' => 'credit_card_transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Credit Card Transactions'); ?></h3>
	<?php if (!empty($creditCardPayment['CreditCardTransaction'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cust Acct Id'); ?></th>
		<th><?php echo __('Card Number'); ?></th>
		<th><?php echo __('Card Expiration Month'); ?></th>
		<th><?php echo __('Card Expiration Year'); ?></th>
		<th><?php echo __('Card Security Code'); ?></th>
		<th><?php echo __('Billing Name'); ?></th>
		<th><?php echo __('Billing Company'); ?></th>
		<th><?php echo __('Billing Address Num'); ?></th>
		<th><?php echo __('Billing Zip Code'); ?></th>
		<th><?php echo __('Billing Address 1'); ?></th>
		<th><?php echo __('Billing Address 2'); ?></th>
		<th><?php echo __('Billing City'); ?></th>
		<th><?php echo __('Billing State'); ?></th>
		<th><?php echo __('Billing Country'); ?></th>
		<th><?php echo __('Billing Phone Number'); ?></th>
		<th><?php echo __('Billing Fax Number'); ?></th>
		<th><?php echo __('Billing Email Address'); ?></th>
		<th><?php echo __('Total Charge'); ?></th>
		<th><?php echo __('Queued Dt'); ?></th>
		<th><?php echo __('Processed Dt'); ?></th>
		<th><?php echo __('Response Avs'); ?></th>
		<th><?php echo __('Response Order Num'); ?></th>
		<th><?php echo __('Response Error'); ?></th>
		<th><?php echo __('Response Approved'); ?></th>
		<th><?php echo __('Response Code'); ?></th>
		<th><?php echo __('Response Message'); ?></th>
		<th><?php echo __('Response Txn Dt'); ?></th>
		<th><?php echo __('Response Reference Num'); ?></th>
		<th><?php echo __('Response Tdate'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th><?php echo __('Ip Address'); ?></th>
		<th><?php echo __('Credit Card Payment Id'); ?></th>
		<th><?php echo __('Recv Payment Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($creditCardPayment['CreditCardTransaction'] as $creditCardTransaction): ?>
		<tr>
			<td><?php echo $creditCardTransaction['id']; ?></td>
			<td><?php echo $creditCardTransaction['cust_acct_id']; ?></td>
			<td><?php echo $creditCardTransaction['card_number']; ?></td>
			<td><?php echo $creditCardTransaction['card_expiration_month']; ?></td>
			<td><?php echo $creditCardTransaction['card_expiration_year']; ?></td>
			<td><?php echo $creditCardTransaction['card_security_code']; ?></td>
			<td><?php echo $creditCardTransaction['billing_name']; ?></td>
			<td><?php echo $creditCardTransaction['billing_company']; ?></td>
			<td><?php echo $creditCardTransaction['billing_address_num']; ?></td>
			<td><?php echo $creditCardTransaction['billing_zip_code']; ?></td>
			<td><?php echo $creditCardTransaction['billing_address_1']; ?></td>
			<td><?php echo $creditCardTransaction['billing_address_2']; ?></td>
			<td><?php echo $creditCardTransaction['billing_city']; ?></td>
			<td><?php echo $creditCardTransaction['billing_state']; ?></td>
			<td><?php echo $creditCardTransaction['billing_country']; ?></td>
			<td><?php echo $creditCardTransaction['billing_phone_number']; ?></td>
			<td><?php echo $creditCardTransaction['billing_fax_number']; ?></td>
			<td><?php echo $creditCardTransaction['billing_email_address']; ?></td>
			<td><?php echo $creditCardTransaction['total_charge']; ?></td>
			<td><?php echo $creditCardTransaction['queued_dt']; ?></td>
			<td><?php echo $creditCardTransaction['processed_dt']; ?></td>
			<td><?php echo $creditCardTransaction['response_avs']; ?></td>
			<td><?php echo $creditCardTransaction['response_order_num']; ?></td>
			<td><?php echo $creditCardTransaction['response_error']; ?></td>
			<td><?php echo $creditCardTransaction['response_approved']; ?></td>
			<td><?php echo $creditCardTransaction['response_code']; ?></td>
			<td><?php echo $creditCardTransaction['response_message']; ?></td>
			<td><?php echo $creditCardTransaction['response_txn_dt']; ?></td>
			<td><?php echo $creditCardTransaction['response_reference_num']; ?></td>
			<td><?php echo $creditCardTransaction['response_tdate']; ?></td>
			<td><?php echo $creditCardTransaction['notes']; ?></td>
			<td><?php echo $creditCardTransaction['ms_access_creation_dt']; ?></td>
			<td><?php echo $creditCardTransaction['ms_access_random_num']; ?></td>
			<td><?php echo $creditCardTransaction['creation_dt']; ?></td>
			<td><?php echo $creditCardTransaction['ip_address']; ?></td>
			<td><?php echo $creditCardTransaction['credit_card_payment_id']; ?></td>
			<td><?php echo $creditCardTransaction['recv_payment_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'credit_card_transactions', 'action' => 'view', $creditCardTransaction['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'credit_card_transactions', 'action' => 'edit', $creditCardTransaction['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'credit_card_transactions', 'action' => 'delete', $creditCardTransaction['id']), null, __('Are you sure you want to delete # %s?', $creditCardTransaction['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Credit Card Transaction'), array('controller' => 'credit_card_transactions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="creditCardTransactions form">
<?php echo $this->Form->create('CreditCardTransaction'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Credit Card Transaction'); ?></legend>
	<?php
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('card_number');
		echo $this->Form->input('card_expiration_month');
		echo $this->Form->input('card_expiration_year');
		echo $this->Form->input('card_security_code');
		echo $this->Form->input('billing_name');
		echo $this->Form->input('billing_company');
		echo $this->Form->input('billing_address_num');
		echo $this->Form->input('billing_zip_code');
		echo $this->Form->input('billing_address_1');
		echo $this->Form->input('billing_address_2');
		echo $this->Form->input('billing_city');
		echo $this->Form->input('billing_state');
		echo $this->Form->input('billing_country');
		echo $this->Form->input('billing_phone_number');
		echo $this->Form->input('billing_fax_number');
		echo $this->Form->input('billing_email_address');
		echo $this->Form->input('total_charge');
		echo $this->Form->input('queued_dt');
		echo $this->Form->input('processed_dt');
		echo $this->Form->input('response_avs');
		echo $this->Form->input('response_order_num');
		echo $this->Form->input('response_error');
		echo $this->Form->input('response_approved');
		echo $this->Form->input('response_code');
		echo $this->Form->input('response_message');
		echo $this->Form->input('response_txn_dt');
		echo $this->Form->input('response_reference_num');
		echo $this->Form->input('response_tdate');
		echo $this->Form->input('notes');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('ip_address');
		echo $this->Form->input('credit_card_payment_id');
		echo $this->Form->input('recv_payment_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Credit Card Transactions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Payments'), array('controller' => 'credit_card_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Payment'), array('controller' => 'credit_card_payments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('controller' => 'receive_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('controller' => 'receive_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="creditCardTransactions view">
<h2><?php echo __('Credit Card Transaction'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($creditCardTransaction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $creditCardTransaction['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Number'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['card_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Month'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['card_expiration_month']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Year'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['card_expiration_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Security Code'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['card_security_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Name'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Company'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address Num'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_address_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Zip Code'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address 1'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address 2'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing City'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing State'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Country'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Phone Number'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Fax Number'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Email Address'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['billing_email_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Charge'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['total_charge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Queued Dt'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['queued_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Processed Dt'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['processed_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Avs'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_avs']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Order Num'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_order_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Error'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_error']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Approved'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_approved']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Code'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Message'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_message']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Txn Dt'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_txn_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Reference Num'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_reference_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Tdate'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['response_tdate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($creditCardTransaction['CreditCardTransaction']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Credit Card Payment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($creditCardTransaction['CreditCardPayment']['id'], array('controller' => 'credit_card_payments', 'action' => 'view', $creditCardTransaction['CreditCardPayment']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Receive Payment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($creditCardTransaction['ReceivePayment']['id'], array('controller' => 'receive_payments', 'action' => 'view', $creditCardTransaction['ReceivePayment']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Credit Card Transaction'), array('action' => 'edit', $creditCardTransaction['CreditCardTransaction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Credit Card Transaction'), array('action' => 'delete', $creditCardTransaction['CreditCardTransaction']['id']), null, __('Are you sure you want to delete # %s?', $creditCardTransaction['CreditCardTransaction']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Transactions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Transaction'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Payments'), array('controller' => 'credit_card_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Payment'), array('controller' => 'credit_card_payments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('controller' => 'receive_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('controller' => 'receive_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>

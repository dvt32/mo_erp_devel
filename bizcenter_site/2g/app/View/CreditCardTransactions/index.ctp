<div class="creditCardTransactions index">
	<h2><?php echo __('Credit Card Transactions'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('cust_acct_id'); ?></th>
			<th><?php echo $this->Paginator->sort('card_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_month'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_year'); ?></th>
			<th><?php echo $this->Paginator->sort('card_security_code'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_name'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_company'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_num'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_1'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_2'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_city'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_state'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_country'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_email_address'); ?></th>
			<th><?php echo $this->Paginator->sort('total_charge'); ?></th>
			<th><?php echo $this->Paginator->sort('queued_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('processed_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('response_avs'); ?></th>
			<th><?php echo $this->Paginator->sort('response_order_num'); ?></th>
			<th><?php echo $this->Paginator->sort('response_error'); ?></th>
			<th><?php echo $this->Paginator->sort('response_approved'); ?></th>
			<th><?php echo $this->Paginator->sort('response_code'); ?></th>
			<th><?php echo $this->Paginator->sort('response_message'); ?></th>
			<th><?php echo $this->Paginator->sort('response_txn_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('response_reference_num'); ?></th>
			<th><?php echo $this->Paginator->sort('response_tdate'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ip_address'); ?></th>
			<th><?php echo $this->Paginator->sort('credit_card_payment_id'); ?></th>
			<th><?php echo $this->Paginator->sort('recv_payment_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($creditCardTransactions as $creditCardTransaction): ?>
	<tr>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($creditCardTransaction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $creditCardTransaction['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['card_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['card_expiration_month']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['card_expiration_year']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['card_security_code']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_name']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_company']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_address_num']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_zip_code']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_address_1']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_address_2']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_city']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_state']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_country']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_fax_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['billing_email_address']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['total_charge']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['queued_dt']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['processed_dt']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_avs']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_order_num']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_error']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_approved']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_code']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_message']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_txn_dt']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_reference_num']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['response_tdate']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['notes']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($creditCardTransaction['CreditCardTransaction']['ip_address']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($creditCardTransaction['CreditCardPayment']['id'], array('controller' => 'credit_card_payments', 'action' => 'view', $creditCardTransaction['CreditCardPayment']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($creditCardTransaction['ReceivePayment']['id'], array('controller' => 'receive_payments', 'action' => 'view', $creditCardTransaction['ReceivePayment']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $creditCardTransaction['CreditCardTransaction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $creditCardTransaction['CreditCardTransaction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $creditCardTransaction['CreditCardTransaction']['id']), null, __('Are you sure you want to delete # %s?', $creditCardTransaction['CreditCardTransaction']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Credit Card Transaction'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Card Payments'), array('controller' => 'credit_card_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card Payment'), array('controller' => 'credit_card_payments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('controller' => 'receive_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('controller' => 'receive_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>

<? $month_names = array(
	'01' => "January",
	'02' => "February",
	'03' => "March",
	'04' => "April",
	'05' => "May",
	'06' => "June",
	'07' => "July",
	'08' => "August",
	'09' => "September",
	'10' => "October",
	'11' => "November",
	'12' => "December"
); ?> 
<div class="span12">
		
		<div class="creditCards form">
		<? echo $this->Form->create('CreditCard'); ?>
			<fieldset>
			<div class="row-fluid">
				<div class="span12">
					<legend><? echo __('Add a New Credit Card'); ?></legend>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
				<?
					echo '<h3>Credit Card Information</h3>';
					echo $this->Form->input('card_nickname');
					echo $this->Form->input('card_number');
					echo $this->Form->input('card_security_code');
					echo $this->Form->input('card_expiration_month', array(
						'options' => $month_names,
						'empty' => '-- Choose a Month --'
					));
					echo $this->Form->input('card_expiration_year', array(
					   'type' => 'date',
					   'maxYear' => date('Y', strtotime('+ 10 years')),
					   'minYear' => date('Y'),
					   'dateFormat' => 'Y'
					));
					echo $this->Html->image('icons/payments/types.png');
					?>
				</div>
				<div class="span6">
				<?
					echo '<h3>Billing Address</h3>';
					echo $this->Form->input('billing_name');
					echo $this->Form->input('billing_address_1');
					echo $this->Form->input('billing_address_2');
					echo $this->Form->input('billing_city');
					echo $this->Form->input('billing_state');
					echo $this->Form->input('billing_zip_code');
					echo $this->Form->input('billing_country');
					echo $this->Form->input('billing_email_address');
					echo $this->Form->input('is_primary', array('label' => 'Make this my Primary Account'));
				?>
				</div>
			</div>
			</fieldset>
		<? echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
		<? echo $this->Form->end(); ?>
		</div>

	</div>
</div>

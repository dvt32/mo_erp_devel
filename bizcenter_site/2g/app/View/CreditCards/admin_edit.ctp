<div class="creditCards form">
<?php echo $this->Form->create('CreditCard'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Credit Card'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('customer_account_id');
		echo $this->Form->input('card_number');
		echo $this->Form->input('card_security_code');
		echo $this->Form->input('billing_name');
		echo $this->Form->input('billing_address_1');
		echo $this->Form->input('billing_address_2');
		echo $this->Form->input('billing_city');
		echo $this->Form->input('billing_state');
		echo $this->Form->input('billing_zip_code');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('billing_company');
		echo $this->Form->input('billing_country');
		echo $this->Form->input('card_expiration_month');
		echo $this->Form->input('card_expiration_year');
		echo $this->Form->input('billing_phone_number');
		echo $this->Form->input('billing_fax_number');
		echo $this->Form->input('billing_email_address');
		echo $this->Form->input('is_primary');
		echo $this->Form->input('card_nickname');
		echo $this->Form->input('billing_address_num');
		echo $this->Form->input('customer_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CreditCard.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('CreditCard.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Credit Cards'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

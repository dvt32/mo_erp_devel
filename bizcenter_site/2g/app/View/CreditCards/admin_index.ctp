<div class="creditCards index">
	<h2><?php echo __('Credit Cards'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('card_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_security_code'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_name'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_1'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_2'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_city'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_state'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_company'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_country'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_month'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_year'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_email_address'); ?></th>
			<th><?php echo $this->Paginator->sort('is_primary'); ?></th>
			<th><?php echo $this->Paginator->sort('card_nickname'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_address_num'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($creditCards as $creditCard): ?>
	<tr>
		<td><?php echo h($creditCard['CreditCard']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($creditCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $creditCard['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($creditCard['CreditCard']['card_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['card_security_code']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_name']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_address_1']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_address_2']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_city']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_state']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_zip_code']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['note']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['created']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_company']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_country']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['card_expiration_month']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['card_expiration_year']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_fax_number']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_email_address']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['is_primary']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['card_nickname']); ?>&nbsp;</td>
		<td><?php echo h($creditCard['CreditCard']['billing_address_num']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($creditCard['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $creditCard['Customer']['id'])); ?>
		</td>
		<td><?php echo h($creditCard['CreditCard']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $creditCard['CreditCard']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $creditCard['CreditCard']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $creditCard['CreditCard']['id']), null, __('Are you sure you want to delete # %s?', $creditCard['CreditCard']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Credit Card'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

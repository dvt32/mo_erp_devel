<div class="creditCards view">
<h2><?php echo __('Credit Card'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($creditCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $creditCard['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Number'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['card_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Security Code'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['card_security_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Name'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address 1'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address 2'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing City'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing State'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Zip Code'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Company'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Country'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Month'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['card_expiration_month']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Year'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['card_expiration_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Phone Number'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Fax Number'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Email Address'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_email_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Primary'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['is_primary']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Nickname'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['card_nickname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Address Num'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['billing_address_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($creditCard['Customer']['first_name'], array('controller' => 'customers', 'action' => 'view', $creditCard['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($creditCard['CreditCard']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Credit Card'), array('action' => 'edit', $creditCard['CreditCard']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Credit Card'), array('action' => 'delete', $creditCard['CreditCard']['id']), null, __('Are you sure you want to delete # %s?', $creditCard['CreditCard']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Cards'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<!-- 
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/dropzone.js"></script>
<link rel="stylesheet" type="text/css" href="{{ bizcenter_2g_assetprefix }}/css/libs/dropzone/dropzone.css" />
<link rel="stylesheet" type="text/css" href="{{ bizcenter_2g_assetprefix }}/css/libs/dropzone/basic.css" />
 -->
<? $paymethods = $this->RealWorld->paymentMethods() ?>
<? $services = $this->RealWorld->services() ?>
<? $range = range(0,8*60,30) // 0 to 4 hours, 30 minute intervals ?>
<? $minutes = array_combine($range,$range) ?>

<div class="span6 offset3">
	<h1>Customer Account Sign Up</h1>
	<?= $this->Form->create('CustomerAccount', array('inputDefaults' => array('class' => 'input-block-level'), 'type' => 'file')) ?>
		<p>Please fill out the form below so we can...</p>
		<ol>
			<li>Verify your email address</li>
			<li>Contact you to activate your account.</li>
		</ol>
		<fieldset>
			<legend>Primary Contact Information</legend>
			<?= $this->Form->input('PrimaryCustomer.first_name') ?>
			<?= $this->Form->input('PrimaryCustomer.last_name') ?>
			<?= $this->Form->input('PrimaryCustomer.email', array('label' => 'Email Address', 'required' => 'required')) ?>
			<?= $this->Form->input('PrimaryCustomer.work_phone') ?>
		</fieldset>
<!-- 
		<fieldset>
			<legend>Customer Account Information</legend>
			<p></p>
			<?= $this->Form->input('CustomerAccount.external_name') ?>
			<?= $this->Form->input('CustomerAccount.description', array('type' => 'textarea', 'maxlength' => '255')) ?>
			<?= $this->Form->input('Customer.email') ?>
		</fieldset>
 -->
		<fieldset>
			<legend>Customer Account Information</legend>

			<?= $this->Form->input('CustomerAccount.name', array('label' => 'Company Name')) ?>
			<?= $this->Form->input('CustomerAccount.external_name') ?>
			<?= $this->Form->input('CustomerAccount.description') ?>
			<?= $this->Form->input('CustomerAccount.web_site') ?>
			<?= $this->Form->input('CustomerAccount.note') ?>
			<?= $this->Form->input('CustomerAccount.is_active', array('label' => 'Account Active', 'checked' => 'checked')) ?>
			<?= $this->Form->input('CustomerAccount.taxid', array('label' => 'Tax ID', 'placeholder' => 'XX-XXXXXXX')) ?>
			<?= $this->Form->input('CustomerAccount.room_minutes', array('options' => $minutes)) ?>
			<?= $this->Form->input('CustomerAccount.pkg_notify_email') // people who are alerted when a package arrives ?>
			<?= $this->Form->input('CustomerAccount.preferred_payment_method', array('options' => $paymethods, 'empty' => '-- choose a payment method --')) ?>
			<?= $this->Form->input('CustomerAccount.service_id', array('options' => $services, 'empty' => '-- choose a service --')) ?>
			<?= $this->Form->input('CustomerAccount.primary_floor_id', array('options' => $floors, 'empty' => '-- choose a floor --')) ?>

			<?//= $this->Form->input('CustomerAccount.logo_url', array('label' => 'Company Logo Url')) ?>
			<?= $this->Form->input('Picture.data', array('label' => 'Company Logo', 'type' => 'file')) ?>

			<?= $this->Form->input('CustomerAccount.type', array('value' => 'customer', 'readonly' => 'readonly')) ?>
			<?= $this->Form->input('CustomerAccount.is_active', array('value' => 1, 'checked' => 'checked')) ?>
			<?= $this->Form->input('CustomerAccount.visible', array('value' => 1, 'checked' => 'checked')) ?>
		</fieldset>

		<?= $this->Form->button('Submit', array('class' => 'btn btn-primary btn-large pull-right')) ?>
	<?= $this->Form->end() ?>
	<!-- <h4>Upload your Company Logo</h4> -->
	<!-- <form id="my-awesome-dropzone" action="/target" class="dropzone"></form> -->
</div>

<div class="customerAccounts form">
<?php echo $this->Form->create('CustomerAccount'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Customer Account'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('primary_contact_id');
		echo $this->Form->input('is_active');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('external_name');
		echo $this->Form->input('description');
		echo $this->Form->input('web_site');
		echo $this->Form->input('visible');
		echo $this->Form->input('type');
		echo $this->Form->input('note');
		echo $this->Form->input('taxid');
		echo $this->Form->input('service_id');
		echo $this->Form->input('preferred_payment_method');
		echo $this->Form->input('room_minutes');
		echo $this->Form->input('floor_id');
		echo $this->Form->input('pkg_notify_email');
		echo $this->Form->input('company_pic_url');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CustomerAccount.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('CustomerAccount.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Services'), array('controller' => 'services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service'), array('controller' => 'services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('controller' => 'bank_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Cards'), array('controller' => 'credit_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card'), array('controller' => 'credit_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mailboxes'), array('controller' => 'mailboxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reservations'), array('controller' => 'reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reservation'), array('controller' => 'reservations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aliases'), array('controller' => 'aliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Alias'), array('controller' => 'aliases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Card Edits'), array('controller' => 'proximity_card_edits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('controller' => 'proximity_card_edits', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>

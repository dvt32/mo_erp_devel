<div class="customerAccounts index">
	<h2><?php echo __('Customer Accounts'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('primary_contact_id'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('external_name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('web_site'); ?></th>
			<th><?php echo $this->Paginator->sort('visible'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('taxid'); ?></th>
			<th><?php echo $this->Paginator->sort('service_id'); ?></th>
			<th><?php echo $this->Paginator->sort('preferred_payment_method'); ?></th>
			<th><?php echo $this->Paginator->sort('room_minutes'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('pkg_notify_email'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('company_pic_url'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($customerAccounts as $customerAccount): ?>
	<tr>
		<td><?php echo h($customerAccount['CustomerAccount']['id']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($customerAccount['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $customerAccount['Customer']['id'])); ?>
		</td>
		<td><?php echo h($customerAccount['CustomerAccount']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['created']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['external_name']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['description']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['web_site']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['visible']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['type']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['note']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['taxid']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($customerAccount['Service']['name'], array('controller' => 'services', 'action' => 'view', $customerAccount['Service']['id'])); ?>
		</td>
		<td><?php echo h($customerAccount['CustomerAccount']['preferred_payment_method']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['room_minutes']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($customerAccount['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $customerAccount['Floor']['id'])); ?>
		</td>
		<td><?php echo h($customerAccount['CustomerAccount']['pkg_notify_email']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['modified']); ?>&nbsp;</td>
		<td><?php echo h($customerAccount['CustomerAccount']['company_pic_url']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $customerAccount['CustomerAccount']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $customerAccount['CustomerAccount']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $customerAccount['CustomerAccount']['id']), null, __('Are you sure you want to delete # %s?', $customerAccount['CustomerAccount']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Services'), array('controller' => 'services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service'), array('controller' => 'services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('controller' => 'bank_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Cards'), array('controller' => 'credit_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card'), array('controller' => 'credit_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mailboxes'), array('controller' => 'mailboxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reservations'), array('controller' => 'reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reservation'), array('controller' => 'reservations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aliases'), array('controller' => 'aliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Alias'), array('controller' => 'aliases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Card Edits'), array('controller' => 'proximity_card_edits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('controller' => 'proximity_card_edits', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>

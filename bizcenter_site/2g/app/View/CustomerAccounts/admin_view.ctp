<div class="customerAccounts view">
<h2><?php echo __('Customer Account'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerAccount['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $customerAccount['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('External Name'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['external_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Web Site'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['web_site']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Visible'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['visible']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Taxid'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['taxid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Service'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerAccount['Service']['name'], array('controller' => 'services', 'action' => 'view', $customerAccount['Service']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Preferred Payment Method'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['preferred_payment_method']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Room Minutes'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['room_minutes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerAccount['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $customerAccount['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pkg Notify Email'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['pkg_notify_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Pic Url'); ?></dt>
		<dd>
			<?php echo h($customerAccount['CustomerAccount']['company_pic_url']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Customer Account'), array('action' => 'edit', $customerAccount['CustomerAccount']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Customer Account'), array('action' => 'delete', $customerAccount['CustomerAccount']['id']), null, __('Are you sure you want to delete # %s?', $customerAccount['CustomerAccount']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Services'), array('controller' => 'services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service'), array('controller' => 'services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bank Accounts'), array('controller' => 'bank_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Cards'), array('controller' => 'credit_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card'), array('controller' => 'credit_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mailboxes'), array('controller' => 'mailboxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reservations'), array('controller' => 'reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reservation'), array('controller' => 'reservations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aliases'), array('controller' => 'aliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Alias'), array('controller' => 'aliases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Card Edits'), array('controller' => 'proximity_card_edits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('controller' => 'proximity_card_edits', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Bank Accounts'); ?></h3>
	<?php if (!empty($customerAccount['BankAccount'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Depository Name'); ?></th>
		<th><?php echo __('Address 1'); ?></th>
		<th><?php echo __('Address 2'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('Zip Code'); ?></th>
		<th><?php echo __('Bank Account Type'); ?></th>
		<th><?php echo __('Bank Routing Number'); ?></th>
		<th><?php echo __('Bank Account Number'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Bank Account Customer Name'); ?></th>
		<th><?php echo __('Bank Account Ownership Type'); ?></th>
		<th><?php echo __('Bank Account Nickname'); ?></th>
		<th><?php echo __('Is Primary'); ?></th>
		<th><?php echo __('Country'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['BankAccount'] as $bankAccount): ?>
		<tr>
			<td><?php echo $bankAccount['id']; ?></td>
			<td><?php echo $bankAccount['customer_account_id']; ?></td>
			<td><?php echo $bankAccount['depository_name']; ?></td>
			<td><?php echo $bankAccount['address_1']; ?></td>
			<td><?php echo $bankAccount['address_2']; ?></td>
			<td><?php echo $bankAccount['city']; ?></td>
			<td><?php echo $bankAccount['state']; ?></td>
			<td><?php echo $bankAccount['zip_code']; ?></td>
			<td><?php echo $bankAccount['bank_account_type']; ?></td>
			<td><?php echo $bankAccount['bank_routing_number']; ?></td>
			<td><?php echo $bankAccount['bank_account_number']; ?></td>
			<td><?php echo $bankAccount['notes']; ?></td>
			<td><?php echo $bankAccount['ms_access_creation_dt']; ?></td>
			<td><?php echo $bankAccount['ms_access_random_num']; ?></td>
			<td><?php echo $bankAccount['created']; ?></td>
			<td><?php echo $bankAccount['bank_account_customer_name']; ?></td>
			<td><?php echo $bankAccount['bank_account_ownership_type']; ?></td>
			<td><?php echo $bankAccount['bank_account_nickname']; ?></td>
			<td><?php echo $bankAccount['is_primary']; ?></td>
			<td><?php echo $bankAccount['country']; ?></td>
			<td><?php echo $bankAccount['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'bank_accounts', 'action' => 'view', $bankAccount['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'bank_accounts', 'action' => 'edit', $bankAccount['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'bank_accounts', 'action' => 'delete', $bankAccount['id']), null, __('Are you sure you want to delete # %s?', $bankAccount['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bank Account'), array('controller' => 'bank_accounts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Credit Cards'); ?></h3>
	<?php if (!empty($customerAccount['CreditCard'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Card Number'); ?></th>
		<th><?php echo __('Card Security Code'); ?></th>
		<th><?php echo __('Billing Name'); ?></th>
		<th><?php echo __('Billing Address 1'); ?></th>
		<th><?php echo __('Billing Address 2'); ?></th>
		<th><?php echo __('Billing City'); ?></th>
		<th><?php echo __('Billing State'); ?></th>
		<th><?php echo __('Billing Zip Code'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Billing Company'); ?></th>
		<th><?php echo __('Billing Country'); ?></th>
		<th><?php echo __('Card Expiration Month'); ?></th>
		<th><?php echo __('Card Expiration Year'); ?></th>
		<th><?php echo __('Billing Phone Number'); ?></th>
		<th><?php echo __('Billing Fax Number'); ?></th>
		<th><?php echo __('Billing Email Address'); ?></th>
		<th><?php echo __('Is Primary'); ?></th>
		<th><?php echo __('Card Nickname'); ?></th>
		<th><?php echo __('Billing Address Num'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['CreditCard'] as $creditCard): ?>
		<tr>
			<td><?php echo $creditCard['id']; ?></td>
			<td><?php echo $creditCard['customer_account_id']; ?></td>
			<td><?php echo $creditCard['card_number']; ?></td>
			<td><?php echo $creditCard['card_security_code']; ?></td>
			<td><?php echo $creditCard['billing_name']; ?></td>
			<td><?php echo $creditCard['billing_address_1']; ?></td>
			<td><?php echo $creditCard['billing_address_2']; ?></td>
			<td><?php echo $creditCard['billing_city']; ?></td>
			<td><?php echo $creditCard['billing_state']; ?></td>
			<td><?php echo $creditCard['billing_zip_code']; ?></td>
			<td><?php echo $creditCard['note']; ?></td>
			<td><?php echo $creditCard['ms_access_creation_dt']; ?></td>
			<td><?php echo $creditCard['ms_access_random_num']; ?></td>
			<td><?php echo $creditCard['created']; ?></td>
			<td><?php echo $creditCard['billing_company']; ?></td>
			<td><?php echo $creditCard['billing_country']; ?></td>
			<td><?php echo $creditCard['card_expiration_month']; ?></td>
			<td><?php echo $creditCard['card_expiration_year']; ?></td>
			<td><?php echo $creditCard['billing_phone_number']; ?></td>
			<td><?php echo $creditCard['billing_fax_number']; ?></td>
			<td><?php echo $creditCard['billing_email_address']; ?></td>
			<td><?php echo $creditCard['is_primary']; ?></td>
			<td><?php echo $creditCard['card_nickname']; ?></td>
			<td><?php echo $creditCard['billing_address_num']; ?></td>
			<td><?php echo $creditCard['customer_id']; ?></td>
			<td><?php echo $creditCard['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'credit_cards', 'action' => 'view', $creditCard['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'credit_cards', 'action' => 'edit', $creditCard['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'credit_cards', 'action' => 'delete', $creditCard['id']), null, __('Are you sure you want to delete # %s?', $creditCard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Credit Card'), array('controller' => 'credit_cards', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Mailboxes'); ?></h3>
	<?php if (!empty($customerAccount['Mailbox'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Mailbox Number'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Last Checked Dt'); ?></th>
		<th><?php echo __('Is Mailbox Present'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['Mailbox'] as $mailbox): ?>
		<tr>
			<td><?php echo $mailbox['id']; ?></td>
			<td><?php echo $mailbox['customer_account_id']; ?></td>
			<td><?php echo $mailbox['mailbox_number']; ?></td>
			<td><?php echo $mailbox['ms_access_creation_dt']; ?></td>
			<td><?php echo $mailbox['ms_access_random_num']; ?></td>
			<td><?php echo $mailbox['created']; ?></td>
			<td><?php echo $mailbox['note']; ?></td>
			<td><?php echo $mailbox['floor_id']; ?></td>
			<td><?php echo $mailbox['last_checked_dt']; ?></td>
			<td><?php echo $mailbox['is_mailbox_present']; ?></td>
			<td><?php echo $mailbox['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'mailboxes', 'action' => 'view', $mailbox['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'mailboxes', 'action' => 'edit', $mailbox['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'mailboxes', 'action' => 'delete', $mailbox['id']), null, __('Are you sure you want to delete # %s?', $mailbox['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Proximity Cards'); ?></h3>
	<?php if (!empty($customerAccount['ProximityCard'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Number'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Access Level Id'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Technology'); ?></th>
		<th><?php echo __('Site Code'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['ProximityCard'] as $proximityCard): ?>
		<tr>
			<td><?php echo $proximityCard['id']; ?></td>
			<td><?php echo $proximityCard['number']; ?></td>
			<td><?php echo $proximityCard['type']; ?></td>
			<td><?php echo $proximityCard['status']; ?></td>
			<td><?php echo $proximityCard['customer_account_id']; ?></td>
			<td><?php echo $proximityCard['customer_id']; ?></td>
			<td><?php echo $proximityCard['note']; ?></td>
			<td><?php echo $proximityCard['ms_access_creation_dt']; ?></td>
			<td><?php echo $proximityCard['ms_access_random_num']; ?></td>
			<td><?php echo $proximityCard['created']; ?></td>
			<td><?php echo $proximityCard['access_level_id']; ?></td>
			<td><?php echo $proximityCard['floor_id']; ?></td>
			<td><?php echo $proximityCard['technology']; ?></td>
			<td><?php echo $proximityCard['site_code']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proximity_cards', 'action' => 'view', $proximityCard['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proximity_cards', 'action' => 'edit', $proximityCard['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proximity_cards', 'action' => 'delete', $proximityCard['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Reservations'); ?></h3>
	<?php if (!empty($customerAccount['Reservation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Room Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['Reservation'] as $reservation): ?>
		<tr>
			<td><?php echo $reservation['id']; ?></td>
			<td><?php echo $reservation['room_id']; ?></td>
			<td><?php echo $reservation['customer_account_id']; ?></td>
			<td><?php echo $reservation['created']; ?></td>
			<td><?php echo $reservation['end']; ?></td>
			<td><?php echo $reservation['note']; ?></td>
			<td><?php echo $reservation['ms_access_creation_dt']; ?></td>
			<td><?php echo $reservation['ms_access_random_num']; ?></td>
			<td><?php echo $reservation['start']; ?></td>
			<td><?php echo $reservation['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'reservations', 'action' => 'view', $reservation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'reservations', 'action' => 'edit', $reservation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'reservations', 'action' => 'delete', $reservation['id']), null, __('Are you sure you want to delete # %s?', $reservation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Reservation'), array('controller' => 'reservations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Aliases'); ?></h3>
	<?php if (!empty($customerAccount['Alias'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['Alias'] as $alias): ?>
		<tr>
			<td><?php echo $alias['id']; ?></td>
			<td><?php echo $alias['customer_account_id']; ?></td>
			<td><?php echo $alias['name']; ?></td>
			<td><?php echo $alias['note']; ?></td>
			<td><?php echo $alias['ms_access_creation_dt']; ?></td>
			<td><?php echo $alias['ms_access_random_num']; ?></td>
			<td><?php echo $alias['created']; ?></td>
			<td><?php echo $alias['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'aliases', 'action' => 'view', $alias['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'aliases', 'action' => 'edit', $alias['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'aliases', 'action' => 'delete', $alias['id']), null, __('Are you sure you want to delete # %s?', $alias['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Alias'), array('controller' => 'aliases', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Proximity Card Edits'); ?></h3>
	<?php if (!empty($customerAccount['ProximityCardEdit'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Number'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Person Id'); ?></th>
		<th><?php echo __('Action Requested'); ?></th>
		<th><?php echo __('Cause'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Operator'); ?></th>
		<th><?php echo __('Access Level Id'); ?></th>
		<th><?php echo __('Completed'); ?></th>
		<th><?php echo __('Rejected'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['ProximityCardEdit'] as $proximityCardEdit): ?>
		<tr>
			<td><?php echo $proximityCardEdit['id']; ?></td>
			<td><?php echo $proximityCardEdit['number']; ?></td>
			<td><?php echo $proximityCardEdit['customer_account_id']; ?></td>
			<td><?php echo $proximityCardEdit['person_id']; ?></td>
			<td><?php echo $proximityCardEdit['action_requested']; ?></td>
			<td><?php echo $proximityCardEdit['cause']; ?></td>
			<td><?php echo $proximityCardEdit['notes']; ?></td>
			<td><?php echo $proximityCardEdit['ms_access_creation_dt']; ?></td>
			<td><?php echo $proximityCardEdit['ms_access_random_num']; ?></td>
			<td><?php echo $proximityCardEdit['created']; ?></td>
			<td><?php echo $proximityCardEdit['operator']; ?></td>
			<td><?php echo $proximityCardEdit['access_level_id']; ?></td>
			<td><?php echo $proximityCardEdit['completed']; ?></td>
			<td><?php echo $proximityCardEdit['rejected']; ?></td>
			<td><?php echo $proximityCardEdit['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proximity_card_edits', 'action' => 'view', $proximityCardEdit['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proximity_card_edits', 'action' => 'edit', $proximityCardEdit['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proximity_card_edits', 'action' => 'delete', $proximityCardEdit['id']), null, __('Are you sure you want to delete # %s?', $proximityCardEdit['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('controller' => 'proximity_card_edits', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Ip Addresses'); ?></h3>
	<?php if (!empty($customerAccount['IpAddress'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Ip Address'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Hostname'); ?></th>
		<th><?php echo __('Isp Id'); ?></th>
		<th><?php echo __('Building Id'); ?></th>
		<th><?php echo __('Rir Owner Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['IpAddress'] as $ipAddress): ?>
		<tr>
			<td><?php echo $ipAddress['id']; ?></td>
			<td><?php echo $ipAddress['customer_account_id']; ?></td>
			<td><?php echo $ipAddress['ip_address']; ?></td>
			<td><?php echo $ipAddress['note']; ?></td>
			<td><?php echo $ipAddress['ms_access_creation_dt']; ?></td>
			<td><?php echo $ipAddress['ms_access_random_num']; ?></td>
			<td><?php echo $ipAddress['created']; ?></td>
			<td><?php echo $ipAddress['hostname']; ?></td>
			<td><?php echo $ipAddress['isp_id']; ?></td>
			<td><?php echo $ipAddress['building_id']; ?></td>
			<td><?php echo $ipAddress['rir_owner_id']; ?></td>
			<td><?php echo $ipAddress['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ip_addresses', 'action' => 'view', $ipAddress['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ip_addresses', 'action' => 'edit', $ipAddress['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ip_addresses', 'action' => 'delete', $ipAddress['id']), null, __('Are you sure you want to delete # %s?', $ipAddress['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Customers'); ?></h3>
	<?php if (!empty($customerAccount['Customer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Salutation'); ?></th>
		<th><?php echo __('First Name'); ?></th>
		<th><?php echo __('Middle Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Nickname'); ?></th>
		<th><?php echo __('Email1'); ?></th>
		<th><?php echo __('Work Phone Number'); ?></th>
		<th><?php echo __('Home Phone Number'); ?></th>
		<th><?php echo __('Cell Phone Number'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Register With Building'); ?></th>
		<th><?php echo __('Other Phone Number'); ?></th>
		<th><?php echo __('Directory 1'); ?></th>
		<th><?php echo __('Directory 2'); ?></th>
		<th><?php echo __('Directory 3'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Work Fax Number'); ?></th>
		<th><?php echo __('Work Address 1'); ?></th>
		<th><?php echo __('Work Address 2'); ?></th>
		<th><?php echo __('Work City'); ?></th>
		<th><?php echo __('Work State'); ?></th>
		<th><?php echo __('Work Zip Code'); ?></th>
		<th><?php echo __('Work Country'); ?></th>
		<th><?php echo __('Home Address 1'); ?></th>
		<th><?php echo __('Home Address 2'); ?></th>
		<th><?php echo __('Home City'); ?></th>
		<th><?php echo __('Home State'); ?></th>
		<th><?php echo __('Home Zip Code'); ?></th>
		<th><?php echo __('Home Country'); ?></th>
		<th><?php echo __('Birthday'); ?></th>
		<th><?php echo __('Company'); ?></th>
		<th><?php echo __('Visibility'); ?></th>
		<th><?php echo __('Suffix'); ?></th>
		<th><?php echo __('Password Dep'); ?></th>
		<th><?php echo __('Email2'); ?></th>
		<th><?php echo __('Email3'); ?></th>
		<th><?php echo __('Inbound Fax Alert Cutoff Dt'); ?></th>
		<th><?php echo __('Enable Cc Pay'); ?></th>
		<th><?php echo __('Receive Email'); ?></th>
		<th><?php echo __('Fax Sender Name'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Profile Pic Url'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customerAccount['Customer'] as $customer): ?>
		<tr>
			<td><?php echo $customer['id']; ?></td>
			<td><?php echo $customer['customer_account_id']; ?></td>
			<td><?php echo $customer['salutation']; ?></td>
			<td><?php echo $customer['first_name']; ?></td>
			<td><?php echo $customer['middle_name']; ?></td>
			<td><?php echo $customer['last_name']; ?></td>
			<td><?php echo $customer['nickname']; ?></td>
			<td><?php echo $customer['email1']; ?></td>
			<td><?php echo $customer['work_phone_number']; ?></td>
			<td><?php echo $customer['home_phone_number']; ?></td>
			<td><?php echo $customer['cell_phone_number']; ?></td>
			<td><?php echo $customer['ms_access_creation_dt']; ?></td>
			<td><?php echo $customer['ms_access_random_num']; ?></td>
			<td><?php echo $customer['created']; ?></td>
			<td><?php echo $customer['is_active']; ?></td>
			<td><?php echo $customer['note']; ?></td>
			<td><?php echo $customer['register_with_building']; ?></td>
			<td><?php echo $customer['other_phone_number']; ?></td>
			<td><?php echo $customer['directory_1']; ?></td>
			<td><?php echo $customer['directory_2']; ?></td>
			<td><?php echo $customer['directory_3']; ?></td>
			<td><?php echo $customer['title']; ?></td>
			<td><?php echo $customer['work_fax_number']; ?></td>
			<td><?php echo $customer['work_address_1']; ?></td>
			<td><?php echo $customer['work_address_2']; ?></td>
			<td><?php echo $customer['work_city']; ?></td>
			<td><?php echo $customer['work_state']; ?></td>
			<td><?php echo $customer['work_zip_code']; ?></td>
			<td><?php echo $customer['work_country']; ?></td>
			<td><?php echo $customer['home_address_1']; ?></td>
			<td><?php echo $customer['home_address_2']; ?></td>
			<td><?php echo $customer['home_city']; ?></td>
			<td><?php echo $customer['home_state']; ?></td>
			<td><?php echo $customer['home_zip_code']; ?></td>
			<td><?php echo $customer['home_country']; ?></td>
			<td><?php echo $customer['birthday']; ?></td>
			<td><?php echo $customer['company']; ?></td>
			<td><?php echo $customer['visibility']; ?></td>
			<td><?php echo $customer['suffix']; ?></td>
			<td><?php echo $customer['password_dep']; ?></td>
			<td><?php echo $customer['email2']; ?></td>
			<td><?php echo $customer['email3']; ?></td>
			<td><?php echo $customer['inbound_fax_alert_cutoff_dt']; ?></td>
			<td><?php echo $customer['enable_cc_pay']; ?></td>
			<td><?php echo $customer['receive_email']; ?></td>
			<td><?php echo $customer['fax_sender_name']; ?></td>
			<td><?php echo $customer['user_id']; ?></td>
			<td><?php echo $customer['profile_pic_url']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'customers', 'action' => 'view', $customer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'customers', 'action' => 'edit', $customer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'customers', 'action' => 'delete', $customer['id']), null, __('Are you sure you want to delete # %s?', $customer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

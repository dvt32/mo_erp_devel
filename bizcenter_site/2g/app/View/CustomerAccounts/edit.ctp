<script src="{{ bizcenter_2g_assetprefix }}/js/libs/dropzone.js"></script>
<link rel="stylesheet" type="text/css" href="{{ bizcenter_2g_assetprefix }}/css/libs/dropzone/dropzone.css" />
<link rel="stylesheet" type="text/css" href="{{ bizcenter_2g_assetprefix }}/css/libs/dropzone/basic.css" />
<div class="row-fluid">
	<div class="span6 offset3">
		<h2>Edit Company Info</h2>
		<?= $this->Form->create('CustomerAccount', array('type' => 'file', 'inputDefaults' => array('class' => 'input-block-level'))) ?>
		<fieldset>
			<?= $this->Form->input('id') ?>
			<?= $this->Html->image($picturePath, array('class' => 'thumbnail-medium')) ?>
			<?= $this->Form->input('Picture.file', array('label' => 'Company Logo', 'type' => 'file', 'accept' => 'image/*')) ?>
			<?= $this->Form->input('name', array('disabled' => true)) ?>
			<?= $this->Form->input('primary_contact_cust_pers_id', array('label' => 'Primary Contact', 'options' => $customers, 'value' => $pc)) ?>
			<?= $this->Form->input('external_name') ?>
			<?= $this->Form->input('description') ?>
			<label>
				Web Site
				<a href="#" class="help" data-toggle="popover" data-placement="bottom" data-content="Make sure to enter in the FULL URL starting for links to redirect properly. For example: http://www.google.com" title="" data-original-title="Please Note">
					<?= $this->Html->image('icons/flat_miniset/png/256/info.png', array('style' => 'height:1.5em;')) ?>
				</a>
			</label>
			<?= $this->Form->input('web_site', array('label' => false)) ?>
			<?= $this->Form->input('visible') ?>
			<?= $this->Form->input('pkg_notify_email', array('label' => 'Package Arrived Notification Email', 'type' => 'text')) ?>
		</fieldset>
		<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
		<?= $this->Form->end(); ?>
		<? $role = AuthComponent::user('role') ?>
		<? if ($role >=  1): ?>
		<ul class="nav nav-pills">
		  <li><?= $this->Html->link('Move-Out Request', ['controller' => 'moveoutRequests', 'action' => 'edit']) ?></li>
		</ul>
		<? endif ?>
	</div>
</div>

<div class="customers index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/company.png' , array('style' => 'height:3em;')) ?>
		<?= __('People at this Company'); ?>
	</h2>
	<table class="table">
	<tr>
			<th><?= h('Primary') ?></th>
			<th><?= h('First Name') ?></th>
			<th><?= h('Last Name') ?></th>
			<th><?= h('Email') ?></th>
			<th><?= h('Join Date') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<? $pc = $customers['PrimaryCustomer']['id'] ?>
	<? foreach ($customers['Customer'] as $customer): ?>
	<tr>
		<? if($pc == $customer['id'] ) : ?>
		<td><?= $this->Html->image('icons/flat_miniset/png/256/star.png' , array('style' => 'height:1.5em;')) ?></td>
		<? else: ?>
		<td></td>
		<? endif; ?>
		<td>
			<?= h($customer['first_name']) ?>
		</td>
		<td><?= h($customer['last_name']) ?>&nbsp;</td>
		<td><?= h($customer['email']) ?>&nbsp;</td>
		<td><?= h($customer['creation_dt']) ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('View'), array('controller' => 'customers','action' => 'view', $customer['id'])) ?>
			<?= $this->Html->link(__('Edit'), array('controller' => 'customers','action' => 'edit', $customer['id'])) ?>
			<?= $this->Form->postLink(__('Deactivate'), array('controller' => 'customers','action' => 'deactivate', $customer['id']), null, __('Are you sure you want to deactivate "%s"?', $customer['first_name'])) ?>
		</td>
	</tr>
<? endforeach ?>
	</table>
</div>

<? // only show add employees button if role is admin+ ?>
<? if (AuthComponent::user('role') >= ROLE_ADMIN): ?>
	<div class="row-fluid">
		<?= $this->Html->link(__('Add Employees'), array('controller' => 'customers', 'action' => 'invite'), array('class' => 'btn btn-primary')) ?>
	</div>
<? endif ?>

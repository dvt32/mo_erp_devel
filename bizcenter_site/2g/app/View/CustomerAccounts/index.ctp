
<div class="customerAccounts index">
	<?//= $this->Html->image('icons/dashboard/collaborate.jpg', array('style' => 'height:10em;')) ?>
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/book.png' , array('style' => 'height:3em;')) ?>
		<?= __('Tenant Directory') ?>
	</h2>
	<ul class="nav nav-tabs">
		<li class="active"><?= $this->Html->link('By Company', '#') ?></li>
		<li><?= $this->Html->link('By Person', array('controller' => 'customers', 'action' => 'search')) ?></li>
	</ul>
	<div class="centered">
		<?= $this->Form->create('CustomerAccount', array('type' => 'get', 'class' => 'form-search')) ?>
			<div class="input-append">
				<input name="q" placeholder="Search by company name" type="text" id="CustomerAccountQ" class="span12 search-query" style="min-height:41px!important;" >
				<button class="btn btn-success" type="submit">Search</button>
			</div>
		<?= $this->Form->end() ?>
	</div>

	<table class="table">
		<thead>
			<tr>
				<th>&nbsp;</th><!-- customerAccount logo -->
				<th><?= $this->Paginator->sort('name') ?></th>
				<th><?= $this->Paginator->sort('primary_contact_id') ?></th>
				<th><?= $this->Paginator->sort('description') ?></th>
			</tr>
		</thead>
		<tbody>
			<? foreach ($customerAccounts as $customerAccount): ?>
				<tr>
					<td>
						<? if (!empty($customerAccount['CustomerAccount']['logo_url'])): ?>
							<?= $this->Html->image(trim($customerAccount['CustomerAccount']['logo_url']), array('alt' => $customerAccount['CustomerAccount']['name'], 'class' => 'thumbnail-small')) ?>
						<? else: ?>
							<?= $this->Html->image('no-profile-pic.png', array('alt' => 'No Profile Pic', 'class' => 'thumbnail-xxsmall')) ?>
						<? endif ?>
					</td>
					<td>
						<?= $this->Html->link(h($customerAccount['CustomerAccount']['name']), array('action' => 'view', $customerAccount['CustomerAccount']['id'])) ?>&nbsp;
					</td>
					<td><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?></td>
					<td><?= h($customerAccount['CustomerAccount']['description']) ?>&nbsp;</td>
				</tr>
			<? endforeach ?>
		</tbody>
	</table>

	<p> <?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?>	</p>

	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
	
</div>


<? $logo_url = $customerAccount['CustomerAccount']['logo_url'] ?: "/img/no-profile-pic.png" ?>
<? $profile_pic_url = $customerAccount['PrimaryCustomer']['profile_picture_url'] ?: FULL_BASE_URL . DS ."img" . DS ."no-profile-pic.png" ?>

<!-- container -->
<div class="row-fluid">

	<!-- left sidebar -->
	<div class="span4">
		<!-- customer account info -->
		<h2><?= __('Customer Account') ?></h2>
		<dl>
			<dd><img src="<?= $logo_url ?>" class="thumbnail-large"></dd>
			<dt><?= __('Name') ?></dt>
			<?
				if (empty($customerAccount['CustomerAccount']['external_name'])) {
					echo $customerAccount['CustomerAccount']['name'];
				} else {
					echo $customerAccount['CustomerAccount']['external_name'];
				}
			?>
			&nbsp;
			<dt><?= __('Description') ?></dt>
			<dd><?= h($customerAccount['CustomerAccount']['description']) ?>&nbsp;</dd>
			<dt><?= __('Web Site') ?></dt>
			<dd><a href="<?= h($customerAccount['CustomerAccount']['web_site']) ?>"><?= h($customerAccount['CustomerAccount']['web_site']) ?></a>&nbsp;</dd>
		</dl>
	</div>

	<!-- right body -->
	<div class="span8">
		<div class="row-fluid">
			<!-- primary contact info -->
			<h3>Primary Contact</h3>
			<dl>
				<? if (AuthComponent::user('id')): ?>
					<dd><?= $this->Html->image($profile_pic_url, array('class' => 'thumbnail-medium')) ?></dd>
				<? else: ?>
					<dd><span class="muted">Please login to view profile pics</span></dd>
				<? endif ?>
				<dt>Name</dt>
				<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?>&nbsp;</dd>
				<dt>Email</dt>
				<dd>(hidden)<?php // $customerAccount['PrimaryCustomer']['email'] ?></dd>
				<dt>Work Phone Number</dt>
				<dd>(hidden)<?php // $customerAccount['PrimaryCustomer']['work_phone_number'] ?></dd>
			</dl>
		</div>
	</div>

</div>

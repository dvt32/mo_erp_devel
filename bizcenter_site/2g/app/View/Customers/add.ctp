<script src="{{ bizcenter_2g_assetprefix }}/js/libs/dropzone.js"></script>
<link rel="stylesheet" type="text/css" href="{{ bizcenter_2g_assetprefix }}/css/libs/dropzone/dropzone.css" />
<link rel="stylesheet" type="text/css" href="{{ bizcenter_2g_assetprefix }}/css/libs/dropzone/basic.css" />
<div class="span6 offset3">
<?= $this->Form->create('Customer',
	array(
		'inputDefaults' => array('class' => 'input-block-level'),
		'type' => 'file')) ?>
	<fieldset>
		<legend><?= __('Add Customer') ?></legend>
		<?= $this->Form->input('Customer.customer_account_id') ?>

		<?= $this->Form->input('Customer.salutation') ?>
		<?= $this->Form->input('Customer.first_name') ?>
		<?= $this->Form->input('Customer.middle_name') ?>
		<?= $this->Form->input('Customer.last_name') ?>
		<?= $this->Form->input('Customer.nickname') ?>
	</fieldset>

	<fieldset>
		<legend>Contact Info</legend>
		<?= $this->Form->input('Customer.email1') ?>
		<?= $this->Form->input('Customer.work_phone_number') ?>
		<?= $this->Form->input('Customer.home_phone_number') ?>
		<?= $this->Form->input('Customer.cell_phone_number') ?>

		<?= $this->Form->input('Customer.is_active') ?>

		<?= $this->Form->input('Customer.note') ?>
		<?= $this->Form->input('Customer.register_with_building') ?>
		<?= $this->Form->input('Customer.other_phone_number') ?>

		<?= $this->Form->input('Customer.directory_1') ?>
		<?= $this->Form->input('Customer.directory_2') ?>
		<?= $this->Form->input('Customer.directory_3') ?>

		<?= $this->Form->input('Customer.title') ?>

		<?= $this->Form->input('Customer.work_fax_number') ?>
		<?= $this->Form->input('Customer.work_address_1') ?>
		<?= $this->Form->input('Customer.work_address_2') ?>
		<?= $this->Form->input('Customer.work_city') ?>
		<?= $this->Form->input('Customer.work_state') ?>
		<?= $this->Form->input('Customer.work_zip_code') ?>
		<?= $this->Form->input('Customer.work_country') ?>
		<?= $this->Form->input('Customer.home_address_1') ?>
		<?= $this->Form->input('Customer.home_address_2') ?>
		<?= $this->Form->input('Customer.home_city') ?>
		<?= $this->Form->input('Customer.home_state') ?>
		<?= $this->Form->input('Customer.home_zip_code') ?>
		<?= $this->Form->input('Customer.home_country') ?>
		<?= $this->Form->input('Customer.company') ?>

		<?= $this->Form->input('Customer.visibility') ?>

		<?= $this->Form->input('Customer.suffix') ?>

		<?= $this->Form->input('Customer.email2') ?>
		<?= $this->Form->input('Customer.email3') ?>
		<?= $this->Form->input('Customer.receive_email') ?>
		<?= $this->Form->input('Customer.fax_sender_name') ?>
		<?= $this->Form->input('Customer.user_id') ?>
		<?= $this->Form->input('Customer.profile_pic_url') ?>
		<?= $this->Form->input('Picture.data', array('type' => 'file')); ?>
</fieldset>
	<?= $this->Form->button('Submit', array('class' => 'btn btn-primary')) ?>
<?= $this->Form->end() ?>
<h4>Upload a profile picture</h4>
<form id="my-awesome-dropzone" action="/target" class="dropzone"></form>
</div>

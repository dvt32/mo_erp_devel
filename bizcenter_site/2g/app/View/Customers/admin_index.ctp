<div class="customers index">
	<h2><?php echo __('Customers'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('salutation'); ?></th>
			<th><?php echo $this->Paginator->sort('first_name'); ?></th>
			<th><?php echo $this->Paginator->sort('middle_name'); ?></th>
			<th><?php echo $this->Paginator->sort('last_name'); ?></th>
			<th><?php echo $this->Paginator->sort('nickname'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('work_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('home_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('cell_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('register_with_building'); ?></th>
			<th><?php echo $this->Paginator->sort('other_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('directory_1'); ?></th>
			<th><?php echo $this->Paginator->sort('directory_2'); ?></th>
			<th><?php echo $this->Paginator->sort('directory_3'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('work_fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('work_address_1'); ?></th>
			<th><?php echo $this->Paginator->sort('work_address_2'); ?></th>
			<th><?php echo $this->Paginator->sort('work_city'); ?></th>
			<th><?php echo $this->Paginator->sort('work_state'); ?></th>
			<th><?php echo $this->Paginator->sort('work_zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('work_country'); ?></th>
			<th><?php echo $this->Paginator->sort('home_address_1'); ?></th>
			<th><?php echo $this->Paginator->sort('home_address_2'); ?></th>
			<th><?php echo $this->Paginator->sort('home_city'); ?></th>
			<th><?php echo $this->Paginator->sort('home_state'); ?></th>
			<th><?php echo $this->Paginator->sort('home_zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('home_country'); ?></th>
			<th><?php echo $this->Paginator->sort('birthday'); ?></th>
			<th><?php echo $this->Paginator->sort('company'); ?></th>
			<th><?php echo $this->Paginator->sort('visibility'); ?></th>
			<th><?php echo $this->Paginator->sort('suffix'); ?></th>
			<th><?php echo $this->Paginator->sort('password'); ?></th>
			<th><?php echo $this->Paginator->sort('email2'); ?></th>
			<th><?php echo $this->Paginator->sort('email3'); ?></th>
			<th><?php echo $this->Paginator->sort('inbound_fax_alert_cutoff_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('enable_cc_pay'); ?></th>
			<th><?php echo $this->Paginator->sort('receive_email'); ?></th>
			<th><?php echo $this->Paginator->sort('fax_sender_name'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($customers as $customer): ?>
	<tr>
		<td><?php echo h($customer['Customer']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($customer['CustomerAccount']['name'], array('controller' => 'customerAccounts', 'action' => 'view', $customer['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($customer['Customer']['salutation']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['first_name']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['middle_name']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['nickname']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['email']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['cell_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['note']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['register_with_building']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['other_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['directory_1']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['directory_2']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['directory_3']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['title']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_fax_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_address_1']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_address_2']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_city']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_state']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_zip_code']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_country']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_address_1']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_address_2']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_city']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_state']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_zip_code']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_country']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['birthday']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['company']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['visibility']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['suffix']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['password']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['email2']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['email3']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['inbound_fax_alert_cutoff_dt']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['enable_cc_pay']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['receive_email']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['fax_sender_name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($customer['User']['id'], array('controller' => 'users', 'action' => 'view', $customer['User']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $customer['Customer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $customer['Customer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $customer['Customer']['id']), null, __('Are you sure you want to delete # %s?', $customer['Customer']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Customer'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mail Packages'), array('controller' => 'mail_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mail Package'), array('controller' => 'mail_packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Cards'), array('controller' => 'credit_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card'), array('controller' => 'credit_cards', 'action' => 'add')); ?> </li>
	</ul>
</div>

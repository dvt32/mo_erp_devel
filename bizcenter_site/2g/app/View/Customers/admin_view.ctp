<div class="customers view">
<h2><?php echo __('Customer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customer['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $customer['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Salutation'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['salutation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['first_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Middle Name'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['middle_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nickname'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['nickname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email1'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['email1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Phone Number'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home Phone Number'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['home_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cell Phone Number'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['cell_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Register With Building'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['register_with_building']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Other Phone Number'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['other_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Directory 1'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['directory_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Directory 2'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['directory_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Directory 3'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['directory_3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Fax Number'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Address 1'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Address 2'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work City'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work State'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Zip Code'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Country'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['work_country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home Address 1'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['home_address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home Address 2'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['home_address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home City'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['home_city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home State'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['home_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home Zip Code'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['home_zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home Country'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['home_country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Birthday'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['birthday']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Visibility'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['visibility']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Suffix'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['suffix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password Dep'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['password_dep']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email2'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['email2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email3'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['email3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inbound Fax Alert Cutoff Dt'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['inbound_fax_alert_cutoff_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Enable Cc Pay'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['enable_cc_pay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Receive Email'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['receive_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax Sender Name'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['fax_sender_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customer['User']['id'], array('controller' => 'users', 'action' => 'view', $customer['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Profile Pic Url'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['profile_pic_url']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Customer'), array('action' => 'edit', $customer['Customer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Customer'), array('action' => 'delete', $customer['Customer']['id']), null, __('Are you sure you want to delete # %s?', $customer['Customer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mail Packages'), array('controller' => 'mail_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mail Package'), array('controller' => 'mail_packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Credit Cards'), array('controller' => 'credit_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Credit Card'), array('controller' => 'credit_cards', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Mail Packages'); ?></h3>
	<?php if (!empty($customer['MailPackage'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Recipient Email'); ?></th>
		<th><?php echo __('Email Message'); ?></th>
		<th><?php echo __('Carrier'); ?></th>
		<th><?php echo __('Package Type'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Sender'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customer['MailPackage'] as $mailPackage): ?>
		<tr>
			<td><?php echo $mailPackage['id']; ?></td>
			<td><?php echo $mailPackage['date']; ?></td>
			<td><?php echo $mailPackage['customer_id']; ?></td>
			<td><?php echo $mailPackage['recipient_email']; ?></td>
			<td><?php echo $mailPackage['email_message']; ?></td>
			<td><?php echo $mailPackage['carrier']; ?></td>
			<td><?php echo $mailPackage['package_type']; ?></td>
			<td><?php echo $mailPackage['ms_access_creation_dt']; ?></td>
			<td><?php echo $mailPackage['ms_access_random_num']; ?></td>
			<td><?php echo $mailPackage['created']; ?></td>
			<td><?php echo $mailPackage['sender']; ?></td>
			<td><?php echo $mailPackage['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'mail_packages', 'action' => 'view', $mailPackage['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'mail_packages', 'action' => 'edit', $mailPackage['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'mail_packages', 'action' => 'delete', $mailPackage['id']), null, __('Are you sure you want to delete # %s?', $mailPackage['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Mail Package'), array('controller' => 'mail_packages', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Proximity Cards'); ?></h3>
	<?php if (!empty($customer['ProximityCard'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Number'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Access Level Id'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Technology'); ?></th>
		<th><?php echo __('Site Code'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customer['ProximityCard'] as $proximityCard): ?>
		<tr>
			<td><?php echo $proximityCard['id']; ?></td>
			<td><?php echo $proximityCard['number']; ?></td>
			<td><?php echo $proximityCard['type']; ?></td>
			<td><?php echo $proximityCard['status']; ?></td>
			<td><?php echo $proximityCard['customer_account_id']; ?></td>
			<td><?php echo $proximityCard['customer_id']; ?></td>
			<td><?php echo $proximityCard['note']; ?></td>
			<td><?php echo $proximityCard['ms_access_creation_dt']; ?></td>
			<td><?php echo $proximityCard['ms_access_random_num']; ?></td>
			<td><?php echo $proximityCard['created']; ?></td>
			<td><?php echo $proximityCard['access_level_id']; ?></td>
			<td><?php echo $proximityCard['floor_id']; ?></td>
			<td><?php echo $proximityCard['technology']; ?></td>
			<td><?php echo $proximityCard['site_code']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proximity_cards', 'action' => 'view', $proximityCard['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proximity_cards', 'action' => 'edit', $proximityCard['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proximity_cards', 'action' => 'delete', $proximityCard['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Credit Cards'); ?></h3>
	<?php if (!empty($customer['CreditCard'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Card Number'); ?></th>
		<th><?php echo __('Card Security Code'); ?></th>
		<th><?php echo __('Billing Name'); ?></th>
		<th><?php echo __('Billing Address 1'); ?></th>
		<th><?php echo __('Billing Address 2'); ?></th>
		<th><?php echo __('Billing City'); ?></th>
		<th><?php echo __('Billing State'); ?></th>
		<th><?php echo __('Billing Zip Code'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Billing Company'); ?></th>
		<th><?php echo __('Billing Country'); ?></th>
		<th><?php echo __('Card Expiration Month'); ?></th>
		<th><?php echo __('Card Expiration Year'); ?></th>
		<th><?php echo __('Billing Phone Number'); ?></th>
		<th><?php echo __('Billing Fax Number'); ?></th>
		<th><?php echo __('Billing Email Address'); ?></th>
		<th><?php echo __('Is Primary'); ?></th>
		<th><?php echo __('Card Nickname'); ?></th>
		<th><?php echo __('Billing Address Num'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customer['CreditCard'] as $creditCard): ?>
		<tr>
			<td><?php echo $creditCard['id']; ?></td>
			<td><?php echo $creditCard['customer_account_id']; ?></td>
			<td><?php echo $creditCard['card_number']; ?></td>
			<td><?php echo $creditCard['card_security_code']; ?></td>
			<td><?php echo $creditCard['billing_name']; ?></td>
			<td><?php echo $creditCard['billing_address_1']; ?></td>
			<td><?php echo $creditCard['billing_address_2']; ?></td>
			<td><?php echo $creditCard['billing_city']; ?></td>
			<td><?php echo $creditCard['billing_state']; ?></td>
			<td><?php echo $creditCard['billing_zip_code']; ?></td>
			<td><?php echo $creditCard['note']; ?></td>
			<td><?php echo $creditCard['ms_access_creation_dt']; ?></td>
			<td><?php echo $creditCard['ms_access_random_num']; ?></td>
			<td><?php echo $creditCard['created']; ?></td>
			<td><?php echo $creditCard['billing_company']; ?></td>
			<td><?php echo $creditCard['billing_country']; ?></td>
			<td><?php echo $creditCard['card_expiration_month']; ?></td>
			<td><?php echo $creditCard['card_expiration_year']; ?></td>
			<td><?php echo $creditCard['billing_phone_number']; ?></td>
			<td><?php echo $creditCard['billing_fax_number']; ?></td>
			<td><?php echo $creditCard['billing_email_address']; ?></td>
			<td><?php echo $creditCard['is_primary']; ?></td>
			<td><?php echo $creditCard['card_nickname']; ?></td>
			<td><?php echo $creditCard['billing_address_num']; ?></td>
			<td><?php echo $creditCard['customer_id']; ?></td>
			<td><?php echo $creditCard['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'credit_cards', 'action' => 'view', $creditCard['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'credit_cards', 'action' => 'edit', $creditCard['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'credit_cards', 'action' => 'delete', $creditCard['id']), null, __('Are you sure you want to delete # %s?', $creditCard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Credit Card'), array('controller' => 'credit_cards', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

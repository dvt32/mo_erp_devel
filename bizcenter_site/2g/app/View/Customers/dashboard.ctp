  <!-- NOTIFICATION BAR -->
  <? if (!empty($notifications)): ?>
    <div class="row-fluid">
      <? foreach ($notifications as $notification): ?>
        <div class="alert alert-block">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4><?= $notification['Notification']['name'] ?></h4>
          <p><?= $notification['Notification']['description'] ?></p>
        </div>
      <? endforeach ?>
    </div>
  <? endif ?>

  <!-- heading -->
  <div class="row-fluid">
    <h1>Dashboard</h1>
    <p class="lead">Welcome back, <?= $user['Customer']['first_name']." ".$user['Customer']['last_name']?>!</p>
  </div>
  <? if(!empty($user['CustomerAccount']['MailPackage'])): ?>
    <div class="row-fluid" style="margin-bottom:2em;">
      <div>
        <? $pkg = $user['CustomerAccount']['MailPackage'][0] ?>
        <? $last_received = CakeTime::timeAgoInWords($pkg['date']) ?>
        <? $sender = $pkg['sender'] ?>
        <?= $this->Html->image('icons/dashboard/others/gift.png', array(
            'style' => 'height:3em;padding:1ex;',
            'class' => 'pull-left',
            'title' => $pkg['email_message']
          ))
        ?>
        <p> Last package received <br />
        <?=
          CakeTime::wasWithinLast('24 hours', $last_received) ?
          "<strong>{$last_received}</strong> from <strong>{$sender}</strong>" :
          "{$last_received} from {$sender}"
        ?>
        </p>
      </div>
    </div>
  <? endif ?>

  <div class="row-fluid">
    <div class="span9">
      <div class="row-fluid">

        <div class="row-fluid">

          <!-- MY ACCOUNT -->
          <div class="span4 centered">
          <?= $this->Html->image('icons/flat_miniset/png/256/company.png' , array('style' => 'height:5em;')) ?>
            <h3>My Account</h3>
            <ul class="nav nav-pills nav-stacked">
              <li><?= $this->Html->link('Account Settings', array('controller' => 'customers', 'action' => 'edit', $user['Customer']['id'])) ?></li>
              <li><?= $this->Html->link('Company Settings', array('controller' => 'customerAccounts', 'action' => 'edit', $user['Customer']['cust_acct_id'])) ?></li>
              <li><?= $this->Html->link('Employees', array('controller' => 'customerAccounts', 'action' => 'employees', $user['Customer']['cust_acct_id'])) ?></li>
              <li><?= $this->Html->link('Proximity Cards', array('controller' => 'proximityCards', 'action' => 'account')) ?></li>
            </ul>
          </div>

          <!-- BILLING -->
          <div class="span4 centered">
          <?= $this->Html->image('icons/dashboard/billing.png', array('style' => 'height:5em;')) ?>
            <h3>Billing</h3>
            <ul class="nav nav-pills nav-stacked">
              <li><?= $this->Html->link('Invoices and Payments', array('controller' => 'receivePayments', 'action' => 'history')) ?></li>
              <li><?= $this->Html->link('Manage Payment Accounts', array('controller' => 'payments', 'action' => 'accounts')) ?></li>
            </ul>
          </div>

          <!-- CONFERENCE ROOMS -->
          <div class="span4 centered">
          <?= $this->Html->image('icons/flat_miniset/png/256/calendar.png', array('style' => 'height:5em;')) ?>
            <h3>Conference Rooms</h3>
            <ul class="nav nav-pills nav-stacked">
              <li><?= $this->Html->link('Schedule & Reservations', array('controller' => 'reservations', 'action' => 'book')) ?></li>
              <li><?= $this->Html->link('My Reservations', array('controller' => 'reservations', 'action' => 'upcoming')) ?></li>
            </ul>
            <?= $this->Html->image('icons/dashboard/others/conference2.png' , array('style' => 'height:3em;')) ?>
            Remaining Minutes: <?= $user['CustomerAccount']['room_minutes_remaining'] ?>
          </div>
        </div>

        <div class="row-fluid" style="padding-top: 2em;">
          <!-- PHONES -->
          <div class="span4 centered">
            <?= $this->Html->image('icons/dashboard/telephone.png' , array('style' => 'height:5em;')) ?>
            <h3>Phones</h3>
            <ul class="nav nav-pills nav-stacked">
              <li><?= $this->Html->link('Your Numbers', array('controller' => 'phoneNumbers', 'action' => 'account')) ?></li>
              <li><?= $this->Html->link('Call Log', array('controller' => 'Cdrs', 'action' => 'recording')) ?></li>
              <li><?= $this->Html->link('Voicemails', array('controller' => 'voicemails', 'action' => 'index')) ?></li>
            </ul>
          </div>

          <!-- TASKS -->
          <div class="span4 centered">
            <?= $this->Html->image('icons/dashboard/tasks2.png', array('style' => 'height:5em;')) ?>
            <h3>Tasks</h3>
            <ul class="nav nav-pills nav-stacked">
              <li><?= $this->Html->link('Request New Task', array('controller' => 'tasks', 'action' => 'add')) ?></li>
              <li><?= $this->Html->link('Tasks I\'ve Requested', array('controller' => 'tasks', 'action' => 'index')) ?></li>
            </ul>
          </div>

          <!-- SCAN & FAX -->
          <div class="span4 centered">
            <?= $this->Html->image('icons/flat_miniset/png/256/copy2.png', array('style' => 'height:5em;')) ?>
            <h3>Scan &amp; Fax</h3>
            <ul class="nav nav-pills nav-stacked">
              <li><?= $this->Html->link('Unclaimed Faxes', array('controller' => 'faxIns', 'action' => 'index')) ?></li>
              <li><?= $this->Html->link('Unclaimed Scans', array('controller' => 'scans', 'action' => 'index')) ?></li>
              <li><?= $this->Html->link('My Documents', array('controller' => 'documents', 'action' => 'account')) ?></li>
              <li><?= $this->Html->link('Send Fax', array('controller' => 'faxOuts', 'action' => 'add')) ?></li>
              <li><?= $this->Html->link('Sent Faxes', array('controller' => 'faxOuts', 'action' => 'history')) ?></li>
            </ul>
          </div>
        </div>

      </div>
    </div>

    <!-- EVENTS SIDEBAR -->
    <div class="span3">
      <h3>Events</h3>
        <!-- BUILD EVENTS LIST -->
        <? if (!empty($events)): ?>
          <? foreach ($events as $event): ?>
            <? $eventsList[] = $this->element('event', compact('event')) ?>
          <? endforeach ?>
          <?= $this->Html->nestedList($eventsList, array('class' => 'unstyled')) ?>
        <? else: ?>
          No Upcoming Events
        <? endif ?>
    </div>

  </div>

  <div class="footer">
    <div class="span12">
      <h3>Network</h3>
      <dl>
        <dt>Wifi Network</dt>
          <dd>
            <?= $this->Html->image('icons/flat_miniset/png/256/antenna2.png' , array('style' => 'height:2em;')) ?>
            <?= MICROOFFICE_SSID ?>
          </dd>
        <dt>Wifi Password</dt>
          <dd>
            <?= $this->Html->image('icons/flat_miniset/png/256/password.png' , array('style' => 'height:2em;')) ?>
            <?= MICROOFFICE_SSPW ?>
          </dd>
      </dl>
      <dl>
        <? if (!empty($user['CustomerAccount']['IpAddress'])) : ?>
          <dt>Your Static IP Address:</dt>
          <? foreach ($user['CustomerAccount']['IpAddress'] as $ip_address) : ?>
            <dd><?= $ip_address['ip_address'] ?></dd>
          <? endforeach; ?>
        <? endif; ?>

        <dt>Your Last Login</dt>
        <dd><?= CakeTime::timeAgoInWords($last_logged_in) ?></dd>

        <dt>
          Your Office Manager is:
        </dt>
        <dd>
          <?= $this->Html->image('icons/flat_miniset/png/256/front_desk.png' , array('style' => 'height:2em;')) ?>
          <a href="mailto:<?= $user['CustomerAccount']['Floor']['OfficeManager']['email'] ?>">
            <?= $user['CustomerAccount']['Floor']['OfficeManager']['full_name'] ?>
          </a>
        </dd>
      </dl>
    </div>
  </div>
</div>


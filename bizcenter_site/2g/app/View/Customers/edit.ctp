<? $profile_pic_url = $this->request->data['Customer']['profile_picture_url'] ?: FULL_BASE_URL . DS . "img" . DS . "no-profile-pic.png" ?>
<div class="row-fluid">
	<div class="span8 offset2">
		<?= $this->Form->create('Customer', array('type' => 'file')) ?>
			<fieldset>
				<h2>
					<?= $this->Html->image('icons/flat_miniset/png/256/gear.png' , array('style' => 'height:3em;')) ?>
					<?= __('Edit My Account Settings') ?>
				</h2>
				<?= $this->Form->input('id') ?>
				<?= $this->Form->input('salutation') ?>
				<?= $this->Form->input('first_name') ?>
				<?= $this->Form->input('middle_name') ?>
				<?= $this->Form->input('last_name') ?>
				<?= $this->Form->input('suffix') ?>
				<?= $this->Form->input('nickname') ?>
				<?= $this->Form->input('email') ?>
				<?= $this->Form->input('User.id') ?>
				<?= $this->Form->input('User.role', array('options' => array(0 => 'Normal User', 1 => 'Admin User'))) ?>
				<?= $this->Html->image($profile_pic_url, array('alt' => 'picture', 'class' => 'thumbnail-medium')) ?>
				<?= $this->Form->input('Picture.data', array('type' => 'file', 'label' => 'Upload a profile picture', 'accept' => 'image/*')) ?>
			<br />
			</fieldset>
		<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

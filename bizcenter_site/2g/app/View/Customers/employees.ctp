<div class="customers index">
	<h2><?php echo __('Employees'); ?></h2>
	<table class="table">
	<tr>
		<th><?php echo 'user_id'; ?></th>
		<th><?php echo 'first_name'; ?></th>
		<th><?php echo 'middle_name'; ?></th>
		<th><?php echo 'last_name'; ?></th>
		<th><?php echo 'nickname'; ?></th>
		<th><?php echo 'email1'; ?></th>
		<th><?php echo 'work_phone_number'; ?></th>
		<th><?php echo 'home_phone_number'; ?></th>
		<th><?php echo 'cell_phone_number'; ?></th>
		<th><?php echo 'created'; ?></th>
		<th><?php echo 'is_active'; ?></th>
		<th><?php echo 'register_with_building'; ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($customers as $customer): ?>
	<tr>
		<td><?php echo ($customer['Customer']['id']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['first_name']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['middle_name']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['nickname']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['email1']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['work_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['home_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['cell_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['created']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($customer['Customer']['register_with_building']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $customer['Customer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $customer['Customer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $customer['Customer']['id']), null, __('Are you sure you want to delete %s?', $customer['Customer']['first_name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Employee'), array('action' => 'add_employee')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
	</ul>
</div>

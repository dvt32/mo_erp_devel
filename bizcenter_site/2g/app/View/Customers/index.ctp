<div class="customers index">
	<h2><?= __('Tenant Directory by Person'); ?></h2>
	<ul class="nav nav-tabs">
		<li><?= $this->Html->link('Company', array('controller' => 'customerAccounts', 'action' => 'index')) ?></li>
		<li class="active"><?= $this->Html->link('Person', '#') ?></li>
	</ul>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('customer_account_id', 'Company'); ?></th>
			<th><?= $this->Paginator->sort('first_name'); ?></th>
			<th><?= $this->Paginator->sort('last_name'); ?></th>
			<th><?= $this->Paginator->sort('nickname'); ?></th>
			<th><?= $this->Paginator->sort('email1'); ?></th>
	</tr>
	<?php foreach ($customers as $customer): ?>
	<tr>
		<td>
			<?= $this->Html->link($customer['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $customer['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($customer['Customer']['first_name'], array('controller' => 'customers', 'action' => 'view', $customer['Customer']['id'])); ?>
		</td>
		<td><?= h($customer['Customer']['last_name']); ?>&nbsp;</td>
		<td><?= h($customer['Customer']['nickname']); ?>&nbsp;</td>
		<td><?= h($customer['Customer']['email']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
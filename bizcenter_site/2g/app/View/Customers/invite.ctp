<div class="span6 offset3">

	<div class="row-fluid">
		<?= $this->Form->create('Customer', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>
				<legend>Invite User to BizCenter</legend>
				<?= $this->Session->flash() ?>
				<?= $this->Form->input('Customer.first_name') ?>
				<?= $this->Form->input('Customer.last_name') ?>
				<?= $this->Form->input('Customer.email', array('label' => 'Email Address')) ?>
				<?= $this->Form->input('User.is_admin', array('type' => 'checkbox', 'label' => 'Admin Privileges')) ?>
			</fieldset>
			<?= $this->Form->button('Invite', array('class' => 'btn btn-primary pull-right')) ?>
		<?= $this->Form->end() ?>
	</div>

	<div class="row-fluid">
		<? $i = 0 ?>
		<? if (!empty($customers['Customer'])): ?>
			<h3>Pending User Invitations</h3>
			<ul>
				<? foreach ($customers['Customer'] as $customer): ?>
					<? if (!empty($customer['User'])): ?>
						<li> <?= $customer['email'] ?> - <span class="muted"><?= $customer['full_name'] ?></span> </li>
						<? $i++ ?>
					<? endif ?>
				<? endforeach ?>
			</ul>
		<? endif ?>
		<? if ($i == 0): ?>
			No pending invitations
		<? endif ?>
	</div>

</div>

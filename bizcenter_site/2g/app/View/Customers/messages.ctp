
<div class="row-fluid">
	<div class="span12">
		<div class="centered">
			<h1>Your Notifications</h1>
		</div>

		<h3>Packages</h3>
		<table class="table">
			<tr>
				<th>
					Carrier
				</th>
				<th>
					Package Type
				</th>
				<th>
					Sender
				</th>
				<th>
					Date Received
				</th>
			</tr>
			<?php foreach ($mailp as $mp['MailPackage']): ?>
			<tr>
				<td><?= $mp['MailPackage']['MailPackage']['carrier'] ?></td>
				<td><?= $mp['MailPackage']['MailPackage']['package_type'] ?></td>
				<td><?= $mp['MailPackage']['MailPackage']['sender'] ?></td>
				<td><?= CakeTime::timeAgoInWords($mp['MailPackage']['MailPackage']['date']) ?></td>
			</tr>
			<?php endforeach ?>
			
		</table>
	</div>
</div>
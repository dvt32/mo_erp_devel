<div class="customers index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/book.png' , array('style' => 'height:3em;')) ?>
		<?= __('Tenant Directory') ?>
	</h2>
	<ul class="nav nav-tabs">
		<li><?= $this->Html->link('By Company', array('controller' => 'customerAccounts', 'action' => 'index')) ?></li>
		<li class="active"><?= $this->Html->link('By Person', '#') ?></li>
	</ul>
	<div class="centered">
	<?= $this->Form->create('Custoemr', array('type' => 'get', 'class' => 'form-search')) ?>
		<div class="input-append">
			<input name="q" placeholder="Search by person name" class="span12 search-query" style="min-height:41px!important;" type="text" id="CustoemrQ">
			<button class="btn btn-success" type="submit">Search</button>
		</div>
	<?= $this->Form->end() ?>
	</div>

	<? if(!empty($customers)): ?>
		<table class="table">
			<thead>
				<tr>
					<th><?= $this->Paginator->sort('customer_account_id', 'Company') ?></th>
					<th><?= $this->Paginator->sort('full_name') ?></th>
					<th><?= $this->Paginator->sort('email') ?></th>
					<th><?= $this->Paginator->sort('work_phone_number') ?></th>
				</tr>
			</thead>
			<tbody>
				<? foreach ($customers as $customer): ?>
					<tr>
						<td><?= $customer['CustomerAccount']['name'] ?></td>
						<td> <?= $this->Html->link($customer['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customer['Customer']['id'])) ?> </td>
						<td><?= h($customer['Customer']['email']) ?>&nbsp;</td>
						<td><?= h($customer['Customer']['work_phone_number']) ?>&nbsp;</td>
					</tr>
				<? endforeach ?>
			</tbody>
		</table>
		<p> <?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )) ?>	</p>
		<div class="paging">
			<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
			<?= $this->Paginator->numbers(array('separator' => '')) ?>
			<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
		</div>
	<? else: ?>
		<div class="hero-unit">
			<h1></h1>
		</div>
	<? endif ?>
</div>

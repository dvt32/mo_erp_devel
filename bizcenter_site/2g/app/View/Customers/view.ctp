<? $picturePath = @$customer['Customer']['profile_picture_url'] ?: FULL_BASE_URL . DS . 'img' . DS . 'no-profile-pic.png' ?>

<div class="customers view">
	<h2><?= __('Customer') ?></h2>
	<dl>
    <dd><?= $this->Html->image($picturePath, array('class' => 'thumbnail-medium')) ?>&nbsp;</dd>
		<dt><?= __('Company') ?></dt>
		<dd><?= $this->Html->link($customer['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $customer['CustomerAccount']['id'])) ?>&nbsp;</dd>
		<dt><?= __('Name') ?></dt>
		<dd><?= h($customer['Customer']['full_name']) ?>&nbsp;</dd>
		<dt><?= __('Nickname') ?></dt>
		<dd><?= h($customer['Customer']['nickname']) ?>&nbsp;</dd>
		<dt><?= __('Email') ?></dt>
		<dd>(hidden)<?php // h($customer['Customer']['email']) ?>&nbsp;</dd>
		<dt><?= __('Work Phone Number') ?></dt>
		<dd>(hidden)<?php // h($customer['Customer']['work_phone_number']) ?>&nbsp;</dd>
	</dl>
</div>

<div class="documents index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/attachment.png' , array('style' => 'height:3em;')) ?>
		<?= __('Documents') ?>
	</h2>
	<?= $this->Session->flash() ?>
	<table class="table">
	<thead>
		<tr>
				<th><?= $this->Paginator->sort('type') ?></th>
				<th><?= $this->Paginator->sort('filename') ?></th>
				<th><?= $this->Paginator->sort('file_size', 'Size') ?></th>
				<th><?= $this->Paginator->sort('file_timestamp', 'Timestamp') ?></th>
				<th><?= $this->Paginator->sort('note') ?></th>
				<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($documents as $document): ?>
		<tr>
			<td><?= h($document['Document']['type']) ?>&nbsp;</td>
			<td><?= $this->Html->link($document['Document']['filename'], array('controller' => 'moFiles', 'action' => 'download', $document['Document']['file_id'])) ?>&nbsp;</td>
			<td><?= h($document['Document']['file_size']) ?>&nbsp;</td>
			<td><?= h($document['Document']['file_timestamp']) ?>&nbsp;</td>
			<td><?= h($document['Document']['note']) ?>&nbsp;</td>
			<td>
				<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $document['Document']['id']), null, __("Are you sure you want to delete '%s'? This action cannot be undone.", $document['Document']['filename']) ) ?>
			</td>
		</tr>
	<? endforeach ?>
	</tbody>
	</table>
	<p>
	<?
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>


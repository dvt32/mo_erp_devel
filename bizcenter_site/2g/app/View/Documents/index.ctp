<div class="documents index">
	<h2>
		<?= __('Documents') ?>
	</h2>
	<table class="table">
	<thead>
		<tr>
				<th><?= $this->Paginator->sort('id') ?></th>
				<th><?= $this->Paginator->sort('type') ?></th>
				<th><?= $this->Paginator->sort('account_id') ?></th>
				<th><?= $this->Paginator->sort('person_id') ?></th>
				<th><?= $this->Paginator->sort('file_id') ?></th>
				<th><?= $this->Paginator->sort('filename') ?></th>
				<th><?= $this->Paginator->sort('file_size') ?></th>
				<th><?= $this->Paginator->sort('file_timestamp') ?></th>
				<th><?= $this->Paginator->sort('note') ?></th>
				<th class="actions"><?= __('Actions') ?></th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($documents as $document): ?>
		<tr>
			<td><?= h($document['Document']['id']) ?>&nbsp;</td>
			<td><?= h($document['Document']['type']) ?>&nbsp;</td>
			<td>
				<?= $this->Html->link($document['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $document['CustomerAccount']['id'])) ?>
			</td>
			<td>
				<?= $this->Html->link($document['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $document['Customer']['id'])) ?>
			</td>
			<td><?= h($document['Document']['file_id']) ?>&nbsp;</td>
			<td><?= h($document['Document']['filename']) ?>&nbsp;</td>
			<td><?= h($document['Document']['file_size']) ?>&nbsp;</td>
			<td><?= h($document['Document']['file_timestamp']) ?>&nbsp;</td>
			<td><?= h($document['Document']['note']) ?>&nbsp;</td>
			<td class="actions">
				<?= $this->Html->link(__('View'), array('action' => 'view', $document['Document']['id'])) ?>
				<?= $this->Html->link(__('Edit'), array('action' => 'edit', $document['Document']['id'])) ?>
				<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $document['Document']['id']), null, __('Are you sure you want to delete # %s?', $document['Document']['id'])) ?>
			</td>
		</tr>
	<? endforeach ?>
	</tbody>
	</table>
	<p>
	<?
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Document'), array('action' => 'add')) ?></li>
		<li><?= $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')) ?> </li>
	</ul>
</div>

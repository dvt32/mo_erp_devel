
<div class="Payment form">
	<?= $this->Form->create('ElectronicCheck') ?>
		<fieldset>
			<legend>Make a Payment</legend>
			<div id="ba">
				<h3>Make a payment with your Bank Account</h3>

				<!-- bank account -->
				<?= $this->Form->input('ElectronicCheck.bankaccount') ?>
				<?= $this->Html->link('Add another Bank Account', array('controller' => 'bankAccounts', 'action' => 'add')) ?>

				<?= $this->Form->input('ElectronicCheck.debit_date', array('type' => 'text','class' => 'debitdate', 'readonly')) ?>
				<p>Debit Date must be at least 24 hours in advance</p>
				<?= $this->Form->input('ElectronicCheck.notes') ?>

				<?= $this->Form->input('ElectronicCheck.invoice_number') ?>
				<?= $this->Form->input('ElectronicCheck.amount') ?>
				<p>Example: 1500.00</p>
				
			</div>
		</fieldset>
		<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
	<?= $this->Form->end(); ?>
</div>

<script type="text/javascript">
	if (!_.isNull(<?= $bid ?>)) {
		$("#ElectronicCheckBankaccount").val(<?= $bid ?>);
	}
	// Datepicker
	$(".debitdate").datetimepicker({
	  format: 'yyyy-mm-dd',
	  autoclose: true,
	  todayBtn: true,
	  forceParse: false,
	  minView: 2
	});
</script>

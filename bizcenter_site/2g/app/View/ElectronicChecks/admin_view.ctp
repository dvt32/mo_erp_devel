<div class="electronicChecks view">
<h2><?php echo __('Electronic Check'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($electronicCheck['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $electronicCheck['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Name'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['bank_account_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Type'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['bank_account_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Routing Number'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['routing_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Account Number'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['account_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Debit Date'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['debit_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invoice Number'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['invoice_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Processed'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['processed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cancelled'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['cancelled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remote Addr'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['remote_addr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remote Host'); ?></dt>
		<dd>
			<?php echo h($electronicCheck['ElectronicCheck']['remote_host']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Electronic Check'), array('action' => 'edit', $electronicCheck['ElectronicCheck']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Electronic Check'), array('action' => 'delete', $electronicCheck['ElectronicCheck']['id']), null, __('Are you sure you want to delete # %s?', $electronicCheck['ElectronicCheck']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Electronic Checks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Electronic Check'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ach Debit Transactions'), array('controller' => 'ach_debit_transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ach Debit Transaction'), array('controller' => 'ach_debit_transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Ach Debit Transactions'); ?></h3>
	<?php if (!empty($electronicCheck['AchDebitTransaction'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cust Acct Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Customer Name'); ?></th>
		<th><?php echo __('Customer Bank Id'); ?></th>
		<th><?php echo __('Customer Bank Acct Num'); ?></th>
		<th><?php echo __('Customer Bank Acct Type'); ?></th>
		<th><?php echo __('Company Bank Acct Num'); ?></th>
		<th><?php echo __('Company Bank Acct Type'); ?></th>
		<th><?php echo __('Start Date'); ?></th>
		<th><?php echo __('Prenote Date'); ?></th>
		<th><?php echo __('Last Collection Date'); ?></th>
		<th><?php echo __('Create Prenote'); ?></th>
		<th><?php echo __('Hold Transfer'); ?></th>
		<th><?php echo __('Queued Dt'); ?></th>
		<th><?php echo __('Processed Dt'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th><?php echo __('Cancelled'); ?></th>
		<th><?php echo __('Electronic Check Id'); ?></th>
		<th><?php echo __('Recv Payment Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($electronicCheck['AchDebitTransaction'] as $achDebitTransaction): ?>
		<tr>
			<td><?php echo $achDebitTransaction['id']; ?></td>
			<td><?php echo $achDebitTransaction['cust_acct_id']; ?></td>
			<td><?php echo $achDebitTransaction['amount']; ?></td>
			<td><?php echo $achDebitTransaction['customer_name']; ?></td>
			<td><?php echo $achDebitTransaction['customer_bank_id']; ?></td>
			<td><?php echo $achDebitTransaction['customer_bank_acct_num']; ?></td>
			<td><?php echo $achDebitTransaction['customer_bank_acct_type']; ?></td>
			<td><?php echo $achDebitTransaction['company_bank_acct_num']; ?></td>
			<td><?php echo $achDebitTransaction['company_bank_acct_type']; ?></td>
			<td><?php echo $achDebitTransaction['start_date']; ?></td>
			<td><?php echo $achDebitTransaction['prenote_date']; ?></td>
			<td><?php echo $achDebitTransaction['last_collection_date']; ?></td>
			<td><?php echo $achDebitTransaction['create_prenote']; ?></td>
			<td><?php echo $achDebitTransaction['hold_transfer']; ?></td>
			<td><?php echo $achDebitTransaction['queued_dt']; ?></td>
			<td><?php echo $achDebitTransaction['processed_dt']; ?></td>
			<td><?php echo $achDebitTransaction['notes']; ?></td>
			<td><?php echo $achDebitTransaction['ms_access_creation_dt']; ?></td>
			<td><?php echo $achDebitTransaction['ms_access_random_num']; ?></td>
			<td><?php echo $achDebitTransaction['creation_dt']; ?></td>
			<td><?php echo $achDebitTransaction['cancelled']; ?></td>
			<td><?php echo $achDebitTransaction['electronic_check_id']; ?></td>
			<td><?php echo $achDebitTransaction['recv_payment_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ach_debit_transactions', 'action' => 'view', $achDebitTransaction['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ach_debit_transactions', 'action' => 'edit', $achDebitTransaction['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ach_debit_transactions', 'action' => 'delete', $achDebitTransaction['id']), null, __('Are you sure you want to delete # %s?', $achDebitTransaction['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ach Debit Transaction'), array('controller' => 'ach_debit_transactions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

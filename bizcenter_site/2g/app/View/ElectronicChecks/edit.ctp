<div class="electronicChecks form">
<?php echo $this->Form->create('ElectronicCheck'); ?>
	<fieldset>
		<legend><?php echo __('Edit Electronic Check'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('bank_account_name');
		echo $this->Form->input('bank_account_type');
		echo $this->Form->input('routing_number');
		echo $this->Form->input('account_number');
		echo $this->Form->input('amount');
		echo $this->Form->input('debit_date');
		echo $this->Form->input('notes');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('invoice_number');
		echo $this->Form->input('processed');
		echo $this->Form->input('cancelled');
		echo $this->Form->input('remote_addr');
		echo $this->Form->input('remote_host');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ElectronicCheck.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ElectronicCheck.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Electronic Checks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ach Debit Transactions'), array('controller' => 'ach_debit_transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ach Debit Transaction'), array('controller' => 'ach_debit_transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="electronicChecks index">
	<h2><?php echo __('Electronic Checks'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('cust_acct_id'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_name'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_type'); ?></th>
			<th><?php echo $this->Paginator->sort('routing_number'); ?></th>
			<th><?php echo $this->Paginator->sort('account_number'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('debit_date'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('invoice_number'); ?></th>
			<th><?php echo $this->Paginator->sort('processed'); ?></th>
			<th><?php echo $this->Paginator->sort('cancelled'); ?></th>
			<th><?php echo $this->Paginator->sort('remote_addr'); ?></th>
			<th><?php echo $this->Paginator->sort('remote_host'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($electronicChecks as $electronicCheck): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($electronicCheck['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $electronicCheck['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['bank_account_name']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['bank_account_type']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['routing_number']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['account_number']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['amount']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['debit_date']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['notes']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['invoice_number']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['processed']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['cancelled']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['remote_addr']); ?>&nbsp;</td>
		<td><?php echo h($electronicCheck['ElectronicCheck']['remote_host']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $electronicCheck['ElectronicCheck']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $electronicCheck['ElectronicCheck']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $electronicCheck['ElectronicCheck']['id']), null, __('Are you sure you want to delete # %s?', $electronicCheck['ElectronicCheck']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Electronic Check'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ach Debit Transactions'), array('controller' => 'ach_debit_transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ach Debit Transaction'), array('controller' => 'ach_debit_transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>

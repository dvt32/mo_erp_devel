<?=
  // user logged in
  $this->Html->nestedList(array(
    $this->Html->link('Dashboard', array('controller' => 'customers', 'action' => 'dashboard')),
    $this->Html->link('Tenant Directory', array('controller' => 'customerAccounts', 'action' => 'index')),
    $this->Html->link('Contact', array('controller' => 'pages', 'action' => 'display' ,'contact')),
		$this->Html->link('Brokers', 'http://www.microoffice.com/brokers.html'),
	),
	array('class' => 'nav pull-left uppercase'));



<div class="event" style="margin-bottom:2em;">
	<div class="event-title">
		<strong><?= $this->Html->link($event['Event']['name'], $event['Event']['event_page_url']) ?></strong>
	</div>

	<? if (!empty($event['Event']['images_url'])): ?>
		<div class="event-picture pull-left">
			<?= $this->Html->image($event['Event']['images_url'], array('class' => 'thumbnail-small')) ?>
		</div>
	<? endif ?>
	
	<div class="event-description">
		<?= substr($event['Event']['description'],0,140) ?>
	</div>

</div>


<ul class="nav pull-right">
  <? if (AuthComponent::user('id') == null): ?>
    <li><?= $this->Html->link('Log In', array('controller' => 'users', 'action' => 'login')) ?></li>
    <!-- <li><?= $this->Html->link('Join', array('controller' => 'users', 'action' => 'add')) ?></li> -->
  <? else: ?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= AuthComponent::user('username') ?> <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><?= $this->Html->link('Dashboard', array('controller' => 'customers', 'action' => 'dashboard')) ?></li>
      <li><?= $this->Html->link('Messages', array('controller' => 'customers','action' => 'messages')) ?></li>
      <li><?= $this->Html->link('Log Out', array('controller' => 'users', 'action' => 'logout')) ?></li>
    </ul>
  </li>
  <? endif ?>
</ul>


<div class="alert alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<?= empty($heading) ? "{$message}" : "<h4>{$heading}</h4> {$message}" ?>
</div>

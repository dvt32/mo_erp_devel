<div class="row">
	<div class="span12 text-center pagination">
		<ul>
			<?= !$this->Paginator->hasPrev() ? '<li><a class="disabled">&laquo</a></li>' : $this->Paginator->prev('&laquo;', array('escape' => false, 'tag' => 'li')) ?>
			<?= $this->Paginator->numbers(array('separator' => false, 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active', 'modulus' => @$modulus ?: 9)) ?>
			<?= !$this->Paginator->hasNext() ? '<li><a class="disabled">&raquo</a></li>' : $this->Paginator->next('&raquo;', array('escape' => false, 'tag' => 'li')) ?>
		</ul>
	</div>
</div>

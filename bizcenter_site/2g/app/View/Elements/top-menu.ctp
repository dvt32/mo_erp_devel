<?

$id = AuthComponent::user('id');

if (!empty($id)) {
	echo $this->element('customer-menu');
} else {
	echo $this->element('guest-menu');
}

<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

?>

<?php
	$bg = array('bg1.jpg', 'bg2.jpg', 'bg3.jpg', 'bg4.jpg', 'bg5.jpg'); 
	$i = rand(0, count($bg)-1); // generate random number size of the array
	$selectedBg = "$bg[$i]"; // set var iable equal to which random filename was chosen
?>

<style>
	.message {
		padding-top: 5em;
		background-color: rgba(255,255,255,0.8); /* CSS3 */zoom:1; /* required for the filters */
		 *background-color: transparent ; /* IE7 only */
		 background-color: transparent\9 ; /* IE8 only */
		-ms-filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#80FFFFFF, endColorstr=#80FFFFFF) ; /* IE8 */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#80FFFFFF, endColorstr=#80FFFFFF); /* IE7 */

		padding-left: 2em;
		padding-right: 2em;
		padding-bottom: 5em;
	}

	body {
		margin:0;
		padding:0;

		background: url('/img/bg/error/<?= $selectedBg; ?>') no-repeat center center fixed; 
		

		/* resize background to fit */
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='.myBackground.jpg', sizingMethod='scale');
		-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='myBackground.jpg', sizingMethod='scale')";
	}

</style>

<?php
if (Configure::read('debug') > 0 ):
	echo $this->element('exception_stack_trace');
endif; ?>

<? if (Configure::read('debug') == 0 ): ?>
<div class="row-fluid">
	<div class="span12 message">
		<div class="centered row-fluid">
			<h1>Uh Oh, nothing found! </h1>
			<p class="lead">
				Please contact <a href="mailto:service@microoffice.com">Micro Office Support</a> for any additional support.
			</p>
			<p class="lead">Let's go</p>
			<a href="/" class="btn btn-primary btn-large">
				Home
			</a>
		</div>
	</div>
</div>
<? endif; ?>


<div class="faxIns index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/open_folder.png' , array('style' => 'height:2em;')) ?>
		<?= __('Public Faxes') ?>
	</h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('file_id') ?></th>
			<th><?= $this->Paginator->sort('file_size') ?></th>
			<th><?= $this->Paginator->sort('creation_dt', 'Date Received') ?></th>
			<th><?= $this->Paginator->sort('filename') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<?php foreach ($faxIns as $faxIn): ?>
	<tr>
		<td><?= h($faxIn['FaxIn']['file_id']) ?>&nbsp;</td>
		<td><?= h($faxIn['FaxIn']['file_size']) ?>&nbsp;</td>
		<td><?= h($faxIn['FaxIn']['creation_dt']) ?>&nbsp;</td>
		<td><?= $this->Html->link($faxIn['FaxIn']['filename'], array(
			'controller' => 'moFiles', 
			'action' => 'view', $faxIn['FaxIn']['file_id']));?></td>
		<td class="actions">
			<?= $this->Html->link($this->Html->image('icons/flat_miniset/png/256/download.png' , array('style' => 'height:1.5em;')), array(
				'controller' => 'faxIns', 
				'action' => 'claim', $faxIn['FaxIn']['file_id']), array('escape' => false)) ?>
			<?= $this->Html->link(__('Claim'), array('action' => 'claim', $faxIn['FaxIn']['id'])) ?>
		</td>
	</tr>
<?php endforeach ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

<div class="faxMachines index">
	<h2><?php echo __('Fax Machines'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('person_id'); ?></th>
			<th><?php echo $this->Paginator->sort('mac_address'); ?></th>
			<th><?php echo $this->Paginator->sort('email_action'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($faxMachines as $faxMachine): ?>
	<tr>
		<td><?php echo h($faxMachine['FaxMachine']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($faxMachine['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxMachine['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($faxMachine['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxMachine['Customer']['id'])); ?>
		</td>
		<td><?php echo h($faxMachine['FaxMachine']['mac_address']); ?>&nbsp;</td>
		<td><?php echo h($faxMachine['FaxMachine']['email_action']); ?>&nbsp;</td>
		<td><?php echo h($faxMachine['FaxMachine']['note']); ?>&nbsp;</td>
		<td><?php echo h($faxMachine['FaxMachine']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $faxMachine['FaxMachine']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $faxMachine['FaxMachine']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $faxMachine['FaxMachine']['id']), null, __('Are you sure you want to delete # %s?', $faxMachine['FaxMachine']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Fax Machine'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

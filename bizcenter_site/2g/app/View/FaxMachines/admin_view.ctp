<div class="faxMachines view">
<h2><?php echo __('Fax Machine'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($faxMachine['FaxMachine']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxMachine['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxMachine['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxMachine['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxMachine['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mac Address'); ?></dt>
		<dd>
			<?php echo h($faxMachine['FaxMachine']['mac_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Action'); ?></dt>
		<dd>
			<?php echo h($faxMachine['FaxMachine']['email_action']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($faxMachine['FaxMachine']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxMachine['FaxMachine']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($faxMachine['FaxMachine']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxMachine['FaxMachine']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Fax Machine'), array('action' => 'edit', $faxMachine['FaxMachine']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Fax Machine'), array('action' => 'delete', $faxMachine['FaxMachine']['id']), null, __('Are you sure you want to delete # %s?', $faxMachine['FaxMachine']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Fax Machines'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fax Machine'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

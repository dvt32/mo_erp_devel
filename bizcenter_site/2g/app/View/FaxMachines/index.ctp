<div class="faxMachines index">
	<h2><?= __('Fax Machines'); ?></h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('id'); ?></th>
			<th><?= $this->Paginator->sort('account_id'); ?></th>
			<th><?= $this->Paginator->sort('person_id'); ?></th>
			<th><?= $this->Paginator->sort('mac_address'); ?></th>
			<th><?= $this->Paginator->sort('email_action'); ?></th>
			<th><?= $this->Paginator->sort('note'); ?></th>
			
			
			<th><?= $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($faxMachines as $faxMachine): ?>
	<tr>
		<td><?= h($faxMachine['FaxMachine']['id']); ?>&nbsp;</td>
		<td>
			<?= $this->Html->link($faxMachine['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxMachine['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($faxMachine['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxMachine['Customer']['id'])); ?>
		</td>
		<td><?= h($faxMachine['FaxMachine']['mac_address']); ?>&nbsp;</td>
		<td><?= h($faxMachine['FaxMachine']['email_action']); ?>&nbsp;</td>
		<td><?= h($faxMachine['FaxMachine']['note']); ?>&nbsp;</td>
		<td><?= h($faxMachine['FaxMachine']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('View'), array('action' => 'view', $faxMachine['FaxMachine']['id'])); ?>
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $faxMachine['FaxMachine']['id'])); ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $faxMachine['FaxMachine']['id']), null, __('Are you sure you want to delete # %s?', $faxMachine['FaxMachine']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Fax Machine'), array('action' => 'add')); ?></li>
		<li><?= $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?= $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="faxNumbers form">
<?php echo $this->Form->create('FaxNumber'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Fax Number'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('account_id');
		echo $this->Form->input('person_id');
		echo $this->Form->input('fax_number');
		echo $this->Form->input('email_action');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('cc_email');
		echo $this->Form->input('in_device_action');
		echo $this->Form->input('in_device_target');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FaxNumber.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('FaxNumber.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Fax Numbers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

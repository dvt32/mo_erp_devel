<div class="faxNumbers index">
	<h2><?php echo __('Fax Numbers'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('person_id'); ?></th>
			<th><?php echo $this->Paginator->sort('fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('email_action'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('cc_email'); ?></th>
			<th><?php echo $this->Paginator->sort('in_device_action'); ?></th>
			<th><?php echo $this->Paginator->sort('in_device_target'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($faxNumbers as $faxNumber): ?>
	<tr>
		<td><?php echo h($faxNumber['FaxNumber']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($faxNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxNumber['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($faxNumber['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxNumber['Customer']['id'])); ?>
		</td>
		<td><?php echo h($faxNumber['FaxNumber']['fax_number']); ?>&nbsp;</td>
		<td><?php echo h($faxNumber['FaxNumber']['email_action']); ?>&nbsp;</td>
		<td><?php echo h($faxNumber['FaxNumber']['note']); ?>&nbsp;</td>
		<td><?php echo h($faxNumber['FaxNumber']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($faxNumber['FaxNumber']['cc_email']); ?>&nbsp;</td>
		<td><?php echo h($faxNumber['FaxNumber']['in_device_action']); ?>&nbsp;</td>
		<td><?php echo h($faxNumber['FaxNumber']['in_device_target']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $faxNumber['FaxNumber']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $faxNumber['FaxNumber']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $faxNumber['FaxNumber']['id']), null, __('Are you sure you want to delete # %s?', $faxNumber['FaxNumber']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Fax Number'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<style type="text/css">
	#subfile {
		min-height:31px!important;
	}
</style>
<div class="faxOuts form">
<?php echo $this->Form->create('FaxOut', array('type' => 'file', 'class' => 'dropzone')); ?>
	<fieldset>
	<h2>
		<?= $this->Html->image('icons/dashboard/paper-plane.png' , array('style' => 'height:2em;')) ?>
		Send a Fax
	</h2>
	<?php
		echo $this->Form->input('recipient_name');
		echo $this->Form->input('fax_number');
		echo $this->Form->input('send_email_alert');
		echo $this->Form->hidden('short_status', array('value' => 'pending'));
	?>
	<br />
	<div>
		<p>Attachment</p>
	</div>
	<br />
		<input type="file" name="data[FaxOut][file]" id="FaxOutFile" />

		<div class="input-append" style="display:none;">
		    <input type="text" name="subfile" id="subfile" class="input-large" readonly="true" onclick="$('#FaxOutFile').click();">
		    <a class="btn" onclick="$('#FaxOutFile').click();">Browse</a>
		</div>
	</fieldset>
	<br />
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
<br />
<p>
	Do you want a private, dedicated fax number so that fax responses are delivered straight to your email, with notification the instant the fax comes in? If you get a lot of faxes or if you get faxes at arbitrary times in the day without prior warning from people, the private, dedicated fax number can be a helpful time-saver. Email <a href="mailto:service@microoffice.com">service@microoffice.com</a> to sign up for the private, dedicated fax service. There is a monthly fee.
	<?= $this->Html->link('Get a Fax Number', array('controller' => 'faxOuts', 'action' => 'getaNumber'), array('class' => 'btn btn-info')) ?>
</p>
<script>

	$('#FaxOutFile').change(function(){
		var filename = $('#FaxOutFile').val();
		filename = filename.slice(12);
	     $('#subfile').val(filename);
	});

</script>

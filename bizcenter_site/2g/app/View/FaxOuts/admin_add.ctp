<div class="faxOuts form">
<?php echo $this->Form->create('FaxOut'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Fax Out'); ?></legend>
	<?php
		echo $this->Form->input('account_id');
		echo $this->Form->input('person_id');
		echo $this->Form->input('job_id');
		echo $this->Form->input('comm_id');
		echo $this->Form->input('recipient_name');
		echo $this->Form->input('fax_number');
		echo $this->Form->input('short_status');
		echo $this->Form->input('long_status');
		echo $this->Form->input('send_time');
		echo $this->Form->input('complete_time');
		echo $this->Form->input('xmit_time');
		echo $this->Form->input('page_count');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('send_email_alert');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Fax Outs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="faxOuts index">
	<h2><?php echo __('Fax Outs'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('person_id'); ?></th>
			<th><?php echo $this->Paginator->sort('job_id'); ?></th>
			<th><?php echo $this->Paginator->sort('comm_id'); ?></th>
			<th><?php echo $this->Paginator->sort('recipient_name'); ?></th>
			<th><?php echo $this->Paginator->sort('fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('short_status'); ?></th>
			<th><?php echo $this->Paginator->sort('long_status'); ?></th>
			<th><?php echo $this->Paginator->sort('send_time'); ?></th>
			<th><?php echo $this->Paginator->sort('complete_time'); ?></th>
			<th><?php echo $this->Paginator->sort('xmit_time'); ?></th>
			<th><?php echo $this->Paginator->sort('page_count'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('send_email_alert'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($faxOuts as $faxOut): ?>
	<tr>
		<td><?php echo h($faxOut['FaxOut']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($faxOut['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxOut['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($faxOut['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxOut['Customer']['id'])); ?>
		</td>
		<td><?php echo h($faxOut['FaxOut']['job_id']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['comm_id']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['recipient_name']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['fax_number']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['short_status']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['long_status']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['send_time']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['complete_time']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['xmit_time']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['page_count']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($faxOut['FaxOut']['send_email_alert']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $faxOut['FaxOut']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $faxOut['FaxOut']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $faxOut['FaxOut']['id']), null, __('Are you sure you want to delete # %s?', $faxOut['FaxOut']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Fax Out'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="floors form">
<?php echo $this->Form->create('Floor'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Floor'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('building_id');
		echo $this->Form->input('label');
		echo $this->Form->input('is_active');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('bounding_polygon');
		echo $this->Form->input('short_global_label');
		echo $this->Form->input('short_local_label');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Floor.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Floor.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rooms'), array('controller' => 'rooms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Room'), array('controller' => 'rooms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mailboxes'), array('controller' => 'mailboxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>

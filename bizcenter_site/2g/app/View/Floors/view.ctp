<div class="floors view">
<h2><?php echo __('Floor'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Building Id'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['building_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Label'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['label']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bounding Polygon'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['bounding_polygon']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Global Label'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['short_global_label']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Local Label'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['short_local_label']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($floor['Floor']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Floor'), array('action' => 'edit', $floor['Floor']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Floor'), array('action' => 'delete', $floor['Floor']['id']), null, __('Are you sure you want to delete # %s?', $floor['Floor']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rooms'), array('controller' => 'rooms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Room'), array('controller' => 'rooms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mailboxes'), array('controller' => 'mailboxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Customers'); ?></h3>
	<?php if (!empty($floor['Customer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Salutation'); ?></th>
		<th><?php echo __('First Name'); ?></th>
		<th><?php echo __('Middle Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Nickname'); ?></th>
		<th><?php echo __('Email1'); ?></th>
		<th><?php echo __('Work Phone Number'); ?></th>
		<th><?php echo __('Home Phone Number'); ?></th>
		<th><?php echo __('Cell Phone Number'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Register With Building'); ?></th>
		<th><?php echo __('Other Phone Number'); ?></th>
		<th><?php echo __('Directory 1'); ?></th>
		<th><?php echo __('Directory 2'); ?></th>
		<th><?php echo __('Directory 3'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Work Fax Number'); ?></th>
		<th><?php echo __('Work Address 1'); ?></th>
		<th><?php echo __('Work Address 2'); ?></th>
		<th><?php echo __('Work City'); ?></th>
		<th><?php echo __('Work State'); ?></th>
		<th><?php echo __('Work Zip Code'); ?></th>
		<th><?php echo __('Work Country'); ?></th>
		<th><?php echo __('Home Address 1'); ?></th>
		<th><?php echo __('Home Address 2'); ?></th>
		<th><?php echo __('Home City'); ?></th>
		<th><?php echo __('Home State'); ?></th>
		<th><?php echo __('Home Zip Code'); ?></th>
		<th><?php echo __('Home Country'); ?></th>
		<th><?php echo __('Birthday'); ?></th>
		<th><?php echo __('Company'); ?></th>
		<th><?php echo __('Visibility'); ?></th>
		<th><?php echo __('Suffix'); ?></th>
		<th><?php echo __('Password Dep'); ?></th>
		<th><?php echo __('Email2'); ?></th>
		<th><?php echo __('Email3'); ?></th>
		<th><?php echo __('Inbound Fax Alert Cutoff Dt'); ?></th>
		<th><?php echo __('Enable Cc Pay'); ?></th>
		<th><?php echo __('Receive Email'); ?></th>
		<th><?php echo __('Fax Sender Name'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Profile Pic Url'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($floor['Customer'] as $customer): ?>
		<tr>
			<td><?php echo $customer['id']; ?></td>
			<td><?php echo $customer['customer_account_id']; ?></td>
			<td><?php echo $customer['salutation']; ?></td>
			<td><?php echo $customer['first_name']; ?></td>
			<td><?php echo $customer['middle_name']; ?></td>
			<td><?php echo $customer['last_name']; ?></td>
			<td><?php echo $customer['nickname']; ?></td>
			<td><?php echo $customer['email1']; ?></td>
			<td><?php echo $customer['work_phone_number']; ?></td>
			<td><?php echo $customer['home_phone_number']; ?></td>
			<td><?php echo $customer['cell_phone_number']; ?></td>
			<td><?php echo $customer['ms_access_creation_dt']; ?></td>
			<td><?php echo $customer['ms_access_random_num']; ?></td>
			<td><?php echo $customer['created']; ?></td>
			<td><?php echo $customer['is_active']; ?></td>
			<td><?php echo $customer['note']; ?></td>
			<td><?php echo $customer['register_with_building']; ?></td>
			<td><?php echo $customer['other_phone_number']; ?></td>
			<td><?php echo $customer['directory_1']; ?></td>
			<td><?php echo $customer['directory_2']; ?></td>
			<td><?php echo $customer['directory_3']; ?></td>
			<td><?php echo $customer['title']; ?></td>
			<td><?php echo $customer['work_fax_number']; ?></td>
			<td><?php echo $customer['work_address_1']; ?></td>
			<td><?php echo $customer['work_address_2']; ?></td>
			<td><?php echo $customer['work_city']; ?></td>
			<td><?php echo $customer['work_state']; ?></td>
			<td><?php echo $customer['work_zip_code']; ?></td>
			<td><?php echo $customer['work_country']; ?></td>
			<td><?php echo $customer['home_address_1']; ?></td>
			<td><?php echo $customer['home_address_2']; ?></td>
			<td><?php echo $customer['home_city']; ?></td>
			<td><?php echo $customer['home_state']; ?></td>
			<td><?php echo $customer['home_zip_code']; ?></td>
			<td><?php echo $customer['home_country']; ?></td>
			<td><?php echo $customer['birthday']; ?></td>
			<td><?php echo $customer['company']; ?></td>
			<td><?php echo $customer['visibility']; ?></td>
			<td><?php echo $customer['suffix']; ?></td>
			<td><?php echo $customer['password_dep']; ?></td>
			<td><?php echo $customer['email2']; ?></td>
			<td><?php echo $customer['email3']; ?></td>
			<td><?php echo $customer['inbound_fax_alert_cutoff_dt']; ?></td>
			<td><?php echo $customer['enable_cc_pay']; ?></td>
			<td><?php echo $customer['receive_email']; ?></td>
			<td><?php echo $customer['fax_sender_name']; ?></td>
			<td><?php echo $customer['user_id']; ?></td>
			<td><?php echo $customer['profile_pic_url']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'customers', 'action' => 'view', $customer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'customers', 'action' => 'edit', $customer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'customers', 'action' => 'delete', $customer['id']), null, __('Are you sure you want to delete # %s?', $customer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Rooms'); ?></h3>
	<?php if (!empty($floor['Room'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Short Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Is Shared Floor'); ?></th>
		<th><?php echo __('Room Type'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($floor['Room'] as $room): ?>
		<tr>
			<td><?php echo $room['id']; ?></td>
			<td><?php echo $room['name']; ?></td>
			<td><?php echo $room['short_name']; ?></td>
			<td><?php echo $room['note']; ?></td>
			<td><?php echo $room['ms_access_creation_dt']; ?></td>
			<td><?php echo $room['ms_access_random_num']; ?></td>
			<td><?php echo $room['created']; ?></td>
			<td><?php echo $room['is_active']; ?></td>
			<td><?php echo $room['floor_id']; ?></td>
			<td><?php echo $room['is_shared_floor']; ?></td>
			<td><?php echo $room['room_type']; ?></td>
			<td><?php echo $room['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'rooms', 'action' => 'view', $room['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'rooms', 'action' => 'edit', $room['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rooms', 'action' => 'delete', $room['id']), null, __('Are you sure you want to delete # %s?', $room['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Room'), array('controller' => 'rooms', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Proximity Cards'); ?></h3>
	<?php if (!empty($floor['ProximityCard'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Number'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Access Level Id'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Technology'); ?></th>
		<th><?php echo __('Site Code'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($floor['ProximityCard'] as $proximityCard): ?>
		<tr>
			<td><?php echo $proximityCard['id']; ?></td>
			<td><?php echo $proximityCard['number']; ?></td>
			<td><?php echo $proximityCard['type']; ?></td>
			<td><?php echo $proximityCard['status']; ?></td>
			<td><?php echo $proximityCard['customer_account_id']; ?></td>
			<td><?php echo $proximityCard['customer_id']; ?></td>
			<td><?php echo $proximityCard['note']; ?></td>
			<td><?php echo $proximityCard['ms_access_creation_dt']; ?></td>
			<td><?php echo $proximityCard['ms_access_random_num']; ?></td>
			<td><?php echo $proximityCard['created']; ?></td>
			<td><?php echo $proximityCard['access_level_id']; ?></td>
			<td><?php echo $proximityCard['floor_id']; ?></td>
			<td><?php echo $proximityCard['technology']; ?></td>
			<td><?php echo $proximityCard['site_code']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proximity_cards', 'action' => 'view', $proximityCard['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proximity_cards', 'action' => 'edit', $proximityCard['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proximity_cards', 'action' => 'delete', $proximityCard['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Mailboxes'); ?></h3>
	<?php if (!empty($floor['Mailbox'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Mailbox Number'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Last Checked Dt'); ?></th>
		<th><?php echo __('Is Mailbox Present'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($floor['Mailbox'] as $mailbox): ?>
		<tr>
			<td><?php echo $mailbox['id']; ?></td>
			<td><?php echo $mailbox['customer_account_id']; ?></td>
			<td><?php echo $mailbox['mailbox_number']; ?></td>
			<td><?php echo $mailbox['ms_access_creation_dt']; ?></td>
			<td><?php echo $mailbox['ms_access_random_num']; ?></td>
			<td><?php echo $mailbox['created']; ?></td>
			<td><?php echo $mailbox['note']; ?></td>
			<td><?php echo $mailbox['floor_id']; ?></td>
			<td><?php echo $mailbox['last_checked_dt']; ?></td>
			<td><?php echo $mailbox['is_mailbox_present']; ?></td>
			<td><?php echo $mailbox['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'mailboxes', 'action' => 'view', $mailbox['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'mailboxes', 'action' => 'edit', $mailbox['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'mailboxes', 'action' => 'delete', $mailbox['id']), null, __('Are you sure you want to delete # %s?', $mailbox['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Access Levels'); ?></h3>
	<?php if (!empty($floor['AccessLevel'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($floor['AccessLevel'] as $accessLevel): ?>
		<tr>
			<td><?php echo $accessLevel['id']; ?></td>
			<td><?php echo $accessLevel['name']; ?></td>
			<td><?php echo $accessLevel['ms_access_creation_dt']; ?></td>
			<td><?php echo $accessLevel['ms_access_random_num']; ?></td>
			<td><?php echo $accessLevel['created']; ?></td>
			<td><?php echo $accessLevel['note']; ?></td>
			<td><?php echo $accessLevel['floor_id']; ?></td>
			<td><?php echo $accessLevel['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'access_levels', 'action' => 'view', $accessLevel['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'access_levels', 'action' => 'edit', $accessLevel['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'access_levels', 'action' => 'delete', $accessLevel['id']), null, __('Are you sure you want to delete # %s?', $accessLevel['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

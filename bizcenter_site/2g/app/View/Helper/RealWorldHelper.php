<?
App::uses('AppHelper','View/Helper');

/**
 * Provides static real-world data for use in formHelper and related data-display
 */

class RealWorldHelper extends AppHelper {

/**
 * Returns a static array of the 50 states
 * 
 * @return array
 */
	public function states() {
		static $states = array(
			"AL"=>'Alabama',
			"AK"=>'Alaska',
			"AZ"=>'Arizona',
			"AR"=>'Arkansas',
			"CA"=>'California',
			"CO"=>'Colorado',
			"CT"=>'Connecticut',
			"DE"=>'Delaware',
			"DC"=>'District Of Columbia',
			"FL"=>'Florida',
			"GA"=>'Georgia',
			"HI"=>'Hawaii',
			"ID"=>'Idaho',
			"IL"=>'Illinois',
			"IN"=>'Indiana',
			"IA"=>'Iowa',
			"KS"=>'Kansas',
			"KY"=>'Kentucky',
			"LA"=>'Louisiana',
			"ME"=>'Maine',
			"MD"=>'Maryland',
			"MA"=>'Massachusetts',
			"MI"=>'Michigan',
			"MN"=>'Minnesota',
			"MS"=>'Mississippi',
			"MO"=>'Missouri',
			"MT"=>'Montana',
			"NE"=>'Nebraska',
			"NV"=>'Nevada',
			"NH"=>'New Hampshire',
			"NJ"=>'New Jersey',
			"NM"=>'New Mexico',
			"NY"=>'New York',
			"NC"=>'North Carolina',
			"ND"=>'North Dakota',
			"OH"=>'Ohio',
			"OK"=>'Oklahoma',
			"OR"=>'Oregon',
			"PA"=>'Pennsylvania',
			"RI"=>'Rhode Island',
			"SC"=>'South Carolina',
			"SD"=>'South Dakota',
			"TN"=>'Tennessee',
			"TX"=>'Texas',
			"UT"=>'Utah',
			"VT"=>'Vermont',
			"VA"=>'Virginia',
			"WA"=>'Washington',
			"WV"=>'West Virginia',
			"WI"=>'Wisconsin',
			"WY"=>'Wyoming',
		);
		return $states;
	}

	public function paymentMethods() {
		static $methods = array(
			'check'        => 'Check',
			'credit_card'  => 'Credit Card',
			'bank_account' => 'Bank Account',
			'cash'         => 'Cash',
		);
		return $methods;
	}

	public function services() {
		static $services = array(
			'0'     => 'Full-Time',
			'1'     => 'Part-Time',
			'2'      => 'Virtual',
		);
		return $services;
	}

	public function salutations() {
		static $salutations = array(
			'' => 'None',
			'Mr.' => 'Mr.',
			'Mrs.' => 'Mrs.',
			'Ms.' => 'Ms.',
		);
		return $salutations;
	}

}

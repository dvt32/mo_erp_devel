<div class="iads index">
	<h2><?php echo __('Iads'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('manufacturer'); ?></th>
			<th><?php echo $this->Paginator->sort('model'); ?></th>
			<th><?php echo $this->Paginator->sort('num_ports'); ?></th>
			<th><?php echo $this->Paginator->sort('mac_address'); ?></th>
			<th><?php echo $this->Paginator->sort('gateway_name'); ?></th>
			<th><?php echo $this->Paginator->sort('ip_address'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('device_name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($iads as $iad): ?>
	<tr>
		<td><?php echo h($iad['Iad']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($iad['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $iad['Floor']['id'])); ?>
		</td>
		<td><?php echo h($iad['Iad']['manufacturer']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['model']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['num_ports']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['mac_address']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['gateway_name']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['ip_address']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['note']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($iad['Iad']['device_name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $iad['Iad']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $iad['Iad']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $iad['Iad']['id']), null, __('Are you sure you want to delete # %s?', $iad['Iad']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Iad'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('controller' => 'phone_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="ipAddresses index">
	<h2><?php echo __('Ip Addresses'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('ip_address'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('hostname'); ?></th>
			<th><?php echo $this->Paginator->sort('isp_id'); ?></th>
			<th><?php echo $this->Paginator->sort('building_id'); ?></th>
			<th><?php echo $this->Paginator->sort('rir_owner_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($ipAddresses as $ipAddress): ?>
	<tr>
		<td><?php echo h($ipAddress['IpAddress']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ipAddress['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $ipAddress['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($ipAddress['IpAddress']['ip_address']); ?>&nbsp;</td>
		<td><?php echo h($ipAddress['IpAddress']['note']); ?>&nbsp;</td>
		<td><?php echo h($ipAddress['IpAddress']['created']); ?>&nbsp;</td>
		<td><?php echo h($ipAddress['IpAddress']['hostname']); ?>&nbsp;</td>
		<td><?php echo h($ipAddress['IpAddress']['isp_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ipAddress['Building']['address_1'], array('controller' => 'buildings', 'action' => 'view', $ipAddress['Building']['id'])); ?>
		</td>
		<td><?php echo h($ipAddress['IpAddress']['rir_owner_id']); ?>&nbsp;</td>
		<td><?php echo h($ipAddress['IpAddress']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $ipAddress['IpAddress']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ipAddress['IpAddress']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ipAddress['IpAddress']['id']), null, __('Are you sure you want to delete # %s?', $ipAddress['IpAddress']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('controller' => 'buildings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Building'), array('controller' => 'buildings', 'action' => 'add')); ?> </li>
	</ul>
</div>

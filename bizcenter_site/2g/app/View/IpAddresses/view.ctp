<div class="ipAddresses view">
<h2><?php echo __('Ip Address'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ipAddress['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $ipAddress['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hostname'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['hostname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Isp Id'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['isp_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Building'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ipAddress['Building']['address_1'], array('controller' => 'buildings', 'action' => 'view', $ipAddress['Building']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rir Owner Id'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['rir_owner_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ip Address'), array('action' => 'edit', $ipAddress['IpAddress']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ip Address'), array('action' => 'delete', $ipAddress['IpAddress']['id']), null, __('Are you sure you want to delete # %s?', $ipAddress['IpAddress']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('controller' => 'buildings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Building'), array('controller' => 'buildings', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="ipPhoneTbls index">
	<h2><?php echo __('Ip Phone Tbls'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('manufacturer'); ?></th>
			<th><?php echo $this->Paginator->sort('model'); ?></th>
			<th><?php echo $this->Paginator->sort('mac_address'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ownership'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($ipPhoneTbls as $ipPhoneTbl): ?>
	<tr>
		<td><?php echo h($ipPhoneTbl['IpPhone']['id']); ?>&nbsp;</td>
		<td><?php echo h($ipPhoneTbl['IpPhone']['manufacturer']); ?>&nbsp;</td>
		<td><?php echo h($ipPhoneTbl['IpPhone']['model']); ?>&nbsp;</td>
		<td><?php echo h($ipPhoneTbl['IpPhone']['mac_address']); ?>&nbsp;</td>
		<td><?php echo h($ipPhoneTbl['IpPhone']['note']); ?>&nbsp;</td>
		<td><?php echo h($ipPhoneTbl['IpPhone']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($ipPhoneTbl['IpPhone']['ownership']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $ipPhoneTbl['IpPhone']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ipPhoneTbl['IpPhone']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ipPhoneTbl['IpPhone']['id']), null, __('Are you sure you want to delete # %s?', $ipPhoneTbl['IpPhone']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ip Phone Tbl'), array('action' => 'add')); ?></li>
	</ul>
</div>

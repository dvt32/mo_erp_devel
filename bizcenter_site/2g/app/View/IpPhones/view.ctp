<div class="ipPhoneTbls view">
<h2><?php  echo __('Ip Phone Tbl'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Manufacturer'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['manufacturer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mac Address'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['mac_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ownership'); ?></dt>
		<dd>
			<?php echo h($ipPhoneTbl['IpPhone']['ownership']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ip Phone Tbl'), array('action' => 'edit', $ipPhoneTbl['IpPhone']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ip Phone Tbl'), array('action' => 'delete', $ipPhoneTbl['IpPhone']['id']), null, __('Are you sure you want to delete # %s?', $ipPhoneTbl['IpPhone']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Phone Tbls'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Phone Tbl'), array('action' => 'add')); ?> </li>
	</ul>
</div>

<!DOCTYPE html>
<html lang="en">
<head>
  <?= $this->Html->charset() ?>
  <title><?= $title_for_layout ?> </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Pith Studio">

  <?= $this->Html->meta('icon') ?>
  <?= $this->Html->css('libs/bootstrap.custom') ?>
  <?= $this->Html->css('libs/datetimepicker') ?>
  <?= $this->Html->css('libs/bootstrap-responsive.custom') ?>
  <?= $this->Html->css('style') ?>
  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('libs/script') ?>

  <link href='http://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
  <style> span.cursive {font-family: 'Satisfy', cursive; } </style>

  <!-- Google Analytics -->
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44586039-1', 'microoffice.com');
  ga('send', 'pageview');

  </script>
  <!-- Google Analytics -->

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]> <script src="{{ bizcenter_2g_assetprefix }}/js/html5shiv.js"></script> <![endif]-->
<!-- 
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="/ico/apple-touch-icon-57-precomposed.png">
 -->  
  <link rel="shortcut icon" href="/ico/favicon.png">

  <!-- libs -->
  <script src="{{ bizcenter_2g_assetprefix }}/js/libs/jquery.min.js"></script>
  <script src="{{ bizcenter_2g_assetprefix }}/js/libs/underscore-min.js"></script>
  <script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap-datetimepicker.min.js"></script>
</head>

<body>

<div id="wrap">
  <!-- Fixed navbar -->
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">

        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <a class="brand" href="/">
          <img src="{{ bizcenter_2g_assetprefix }}/img/logos/harlem_small.png" alt="MicroOffice" style="height:2em;" />
        </a>

        <div class="nav-collapse collapse" style="padding-top:2px!important;">
          <?= $this->element('top-menu') ?>
          <?= $this->element('login-menu') ?>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>

  <div class="container">

    <!-- PAGE CONTENT -->
    <div class="row-fluid">
      <?= $this->Session->flash() ?>
      <?= $this->fetch('content') ?>
    </div>

  </div>

  <div id="push"></div>
</div>

<div id="footer">
  <div class="container">
    <?= $this->element('sql_dump'); ?>
    <p class="muted credit">
      <span class="pull-left">
        &copy; 2014 MicroOffice
        <a href="mailto:service@microoffice.com">Feedback</a>
      </span>
      <span class="pull-right">
        <span class="cursive">crafted by</span>
        <a href="http://www.pith-studio.com/" style="color: #456">Pith Studio</a>
      </span>
    </p>
  </div>
</div>


<!-- Le javascript -->
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-transition.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-alert.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-modal.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-dropdown.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-scrollspy.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-tab.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-tooltip.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-popover.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-button.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-collapse.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-carousel.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap/bootstrap-typeahead.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    // Datepicker
    $(".form_datetime").datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      autoclose: true,
      todayBtn: true,
      showMeridian: true,
      minuteStep: 15,
      forceParse: false
    });

    // popovers
    $('.help').popover({trigger:'hover'});
</script>
 
</body>
</html>

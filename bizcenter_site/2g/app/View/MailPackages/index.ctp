<div class="mailPackages index">
	<h2><?php echo __('Mail Packages'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('recipient_email'); ?></th>
			<th><?php echo $this->Paginator->sort('email_message'); ?></th>
			<th><?php echo $this->Paginator->sort('carrier'); ?></th>
			<th><?php echo $this->Paginator->sort('package_type'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('sender'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mailPackages as $mailPackage): ?>
	<tr>
		<td><?php echo h($mailPackage['MailPackage']['id']); ?>&nbsp;</td>
		<td><?php echo h($mailPackage['MailPackage']['date']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($mailPackage['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $mailPackage['Customer']['id'])); ?>
		</td>
		<td><?php echo h($mailPackage['MailPackage']['recipient_email']); ?>&nbsp;</td>
		<td><?php echo h($mailPackage['MailPackage']['email_message']); ?>&nbsp;</td>
		<td><?php echo h($mailPackage['MailPackage']['carrier']); ?>&nbsp;</td>
		<td><?php echo h($mailPackage['MailPackage']['package_type']); ?>&nbsp;</td>
		<td><?php echo h($mailPackage['MailPackage']['created']); ?>&nbsp;</td>
		<td><?php echo h($mailPackage['MailPackage']['sender']); ?>&nbsp;</td>
		<td><?php echo h($mailPackage['MailPackage']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $mailPackage['MailPackage']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mailPackage['MailPackage']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mailPackage['MailPackage']['id']), null, __('Are you sure you want to delete # %s?', $mailPackage['MailPackage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mail Package'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="mailboxes form">
<?php echo $this->Form->create('Mailbox'); ?>
	<fieldset>
		<legend><?php echo __('Add Mailbox'); ?></legend>
	<?php
		echo $this->Form->input('customer_account_id');
		echo $this->Form->input('mailbox_number');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('note');
		echo $this->Form->input('floor_id');
		echo $this->Form->input('last_checked_dt');
		echo $this->Form->input('is_mailbox_present');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Mailboxes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

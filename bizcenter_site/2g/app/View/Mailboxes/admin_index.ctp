<div class="mailboxes index">
	<h2><?php echo __('Mailboxes'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('mailbox_number'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('last_checked_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('is_mailbox_present'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mailboxes as $mailbox): ?>
	<tr>
		<td><?php echo h($mailbox['Mailbox']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($mailbox['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $mailbox['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($mailbox['Mailbox']['mailbox_number']); ?>&nbsp;</td>
		<td><?php echo h($mailbox['Mailbox']['created']); ?>&nbsp;</td>
		<td><?php echo h($mailbox['Mailbox']['note']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($mailbox['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $mailbox['Floor']['id'])); ?>
		</td>
		<td><?php echo h($mailbox['Mailbox']['last_checked_dt']); ?>&nbsp;</td>
		<td><?php echo h($mailbox['Mailbox']['is_mailbox_present']); ?>&nbsp;</td>
		<td><?php echo h($mailbox['Mailbox']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $mailbox['Mailbox']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mailbox['Mailbox']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mailbox['Mailbox']['id']), null, __('Are you sure you want to delete # %s?', $mailbox['Mailbox']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="mailboxes view">
<h2><?php echo __('Mailbox'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mailbox['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $mailbox['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mailbox Number'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['mailbox_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mailbox['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $mailbox['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Checked Dt'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['last_checked_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Mailbox Present'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['is_mailbox_present']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mailbox'), array('action' => 'edit', $mailbox['Mailbox']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mailbox'), array('action' => 'delete', $mailbox['Mailbox']['id']), null, __('Are you sure you want to delete # %s?', $mailbox['Mailbox']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mailboxes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

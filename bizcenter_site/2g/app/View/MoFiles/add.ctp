<div class="files form">
<?php echo $this->Form->create('File'); ?>
	<fieldset>
		<legend><?php echo __('Add File Tbl'); ?></legend>
	<?php
		echo $this->Form->input('filename');
		echo $this->Form->input('mime_type');
		echo $this->Form->input('note');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('namespace');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List File Tbls'), array('action' => 'index')); ?></li>
	</ul>
</div>

<div class="files form">
<?php echo $this->Form->create('File'); ?>
	<fieldset>
		<legend><?php echo __('Edit File Tbl'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('filename');
		echo $this->Form->input('mime_type');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('namespace');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('File.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('File.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List File Tbls'), array('action' => 'index')); ?></li>
	</ul>
</div>

<div class="files view">
<h2><?php echo __('File Tbl'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($file['File']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filename'); ?></dt>
		<dd>
			<?php echo h($file['File']['filename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mime Type'); ?></dt>
		<dd>
			<?php echo h($file['File']['mime_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($file['File']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($file['File']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Namespace'); ?></dt>
		<dd>
			<?php echo h($file['File']['namespace']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit File Tbl'), array('action' => 'edit', $file['File']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete File Tbl'), array('action' => 'delete', $file['File']['id']), null, __('Are you sure you want to delete # %s?', $file['File']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List File Tbls'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New File Tbl'), array('action' => 'add')); ?> </li>
	</ul>
</div>

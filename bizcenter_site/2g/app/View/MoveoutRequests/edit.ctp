<div class="row-fluid">
	<div class="span8 offset2">
		<?= $this->Session->flash() ?>

		<?= $this->Form->create('MoveoutRequest', ['inputDefaults' => ['class' => 'input-block-level']]) ?>
			<?= $this->Form->input('id') ?>
		<fieldset>
			<legend><?= __('Move-Out Form') ?></legend>
			<ol>

				<li class="row-fluid">
					<!-- requested move out date -->
					<div class="span6">
						<?= $this->Form->input('move_out_date', ['type' => 'text', 'class' => 'form_date input-medium']) ?>
					</div>
					<div class="span6">
						<?= $this->Form->input('close_out_date', ['type' => 'text', 'class' => 'input-medium', 'readonly', 'id' => 'closeOutDate']) ?>
						<? if ($earliest_close_out_date > date('Y-m-d')): ?>
							<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="myModalLabel">Notice</h3>
								</div>
								<div class="modal-body">
									<p>
										The move-out date you have selected results in a close-out date of
										<strong id="closeOutDateSpan"></strong>.
										Agreeing to a close-out date prior to
										<strong><?= @$earliest_close_out_date ?></strong>
										will cause you to forfeit your security deposit.
									</p>
								</div>
								<div class="modal-footer">
									<!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> -->
									<?= $this->Html->link('Cancel', '/', ['class' => 'btn']) ?>
									<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">I Understand</button>
								</div>
							</div>
						<? endif ?>
					</div>
				</li>

				<li>
					<!-- moving company -->
					<label>
						Will you be using a moving company?<br />
						If so, please review the following
						(<?= $this->Html->link('Package Removals and Move Out Policy', 'https://sites.google.com/a/microoffice.com/tenant-wiki/moving', ['target' => '_blank']) ?>)
					</label>
					<?= $this->Form->input('is_using_moving_company', array(
						'label' => 'Are you using a moving company?',
						'options' => [1 => 'Yes', 0 => 'No'],
						'type' => 'radio',
						'legend' => false
					)) ?>
					<p>
					</p>
				</li>

				<li>
					<!-- security deposit address -->
					<label>Where should the security deposit be sent?</label>

					<!-- TODO display the primary contact's address -->
					<div class="form-group">
						<?= $this->Form->input('security_deposit_payable_to', ['label' => false, 'placeholder' => 'Payable To']) ?>
					</div>
					<div class="form-group">
						<?= $this->Form->input('security_deposit_address_line_1', ['label' => false, 'placeholder' => 'Address Line 1']) ?>
						<?= $this->Form->input('security_deposit_address_line_2', ['label' => false, 'placeholder' => 'Address Line 2']) ?>
					</div>
					<div class="form-group">
						<?= $this->Form->input('security_deposit_city',     ['label' => false, 'div' => 'span7', 'placeholder' => 'City']) ?>
						<?= $this->Form->input('security_deposit_state',    ['label' => false, 'div' => 'span2', 'placeholder' => 'State', 'options' => $this->RealWorld->states(), 'value' => 'NY']) ?>
						<?= $this->Form->input('security_deposit_zipcode', ['label' => false, 'div' => 'span3', 'placeholder' => 'Zip Code']) ?>
					</div>
					<div class="form-group">
						<?= $this->Form->input('security_deposit_notes',    [
							'type' => 'textarea',
							'rows' => '2',
							'placeholder' => 'Place any notes relating to the security deposit here'
						]) ?>
					</div>
				</li>

				<li>
					<label>How many keycards have you returned?</label>
					<!-- keycards -->
					<div class="input-append">
						<?= $this->Form->input('num_keycards_returned', ['label' => false, 'class' => 'input-mini', 'div' => false]) ?>
						<span class="add-on">of <?= $num_keycards ?></span>
					</div>
					<!-- metalkeys -->
					<?= $this->Form->input('num_metal_keys_returned', ['label' => 'How many metal keys have you returned?', 'class' => 'input-mini']) ?>
				</li>

				<li>
					<?= $this->Form->input('notes', [
						'type' => 'textarea',
						'rows' => '2',
						'label' => 'Additional Notes',
						'placeholder' => 'Place any additional notes relating to your move-out request here',
					]) ?>
				</li>

			</ol>

		</fieldset>

		<?= $this->Form->button('Submit', ['class' => 'btn btn-primary']) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

<script type="text/javascript">
	// update the move-out-request close-out date when move-out date changes
	$('#MoveoutRequestMoveOutDate').change(function() {
		// build json array
		var moveOutDate =  { "date" : $("#MoveoutRequestMoveOutDate").val() };

		$.ajax({
			url: "/moveoutRequests/closeoutAjax",
			type: "post",
			dataType: "json",
			data: moveOutDate,
			success: function(d) {
				dt = d.moveout_date
				$('#closeOutDate').toggle('highlight').val(dt).toggle('highlight');
				$('#closeOutDateSpan').empty().append(dt);
				// compare dt to license_end_date
				// if the dt < license_end_date
				var license_end_date = "<?= $earliest_close_out_date ?>";
				console.log(dt);
				console.log(license_end_date);
				if (dt < license_end_date)
					// then display the modal
					$('#myModal').modal();
			}
		});
	});
</script>

<div class="moveoutCustomers index">
	<h2><?= __('Moveout Customers'); ?></h2>
	<table class="table table-striped table-condensed`">
		<tr>
			<thead>
				<th><?= $this->Paginator->sort('id'); ?></th>
				<th><?= $this->Paginator->sort('cust_acct_id'); ?></th>
				<th><?= $this->Paginator->sort('move_out_date'); ?></th>
				<th><?= $this->Paginator->sort('close_out_date'); ?></th>
				<th><?= $this->Paginator->sort('security_deposit_address_line_1'); ?></th>
				<th><?= $this->Paginator->sort('security_deposit_address_line_2'); ?></th>
				<th><?= $this->Paginator->sort('security_deposit_city'); ?></th>
				<th><?= $this->Paginator->sort('security_deposit_state'); ?></th>
				<th><?= $this->Paginator->sort('security_deposit_zipcode'); ?></th>
				<th><?= $this->Paginator->sort('num_keycards_returned'); ?></th>
				<th><?= $this->Paginator->sort('num_metal_keys_returned'); ?></th>
				<th><?= $this->Paginator->sort('is_using_moving_company'); ?></th>
				<th><?= $this->Paginator->sort('move_out_requester_id'); ?></th>
				<th><?= $this->Paginator->sort('created'); ?></th>
				<th><?= $this->Paginator->sort('modified'); ?></th>
				<th><?= $this->Paginator->sort('all_keycards_returned_dt'); ?></th>
				<th><?= $this->Paginator->sort('all_keycards_returned_employee_id'); ?></th>
				<th><?= $this->Paginator->sort('all_metalkeys_returned_dt'); ?></th>
				<th><?= $this->Paginator->sort('all_metalkeys_returned_employee_id'); ?></th>
				<th><?= $this->Paginator->sort('coi_submitted_dt'); ?></th>
				<th><?= $this->Paginator->sort('coi_submitted_employee_id'); ?></th>
				<th><?= $this->Paginator->sort('coi_approved_dt'); ?></th>
				<th><?= $this->Paginator->sort('coi_approved_employee_id'); ?></th>
				<th><?= $this->Paginator->sort('moved_out_dt'); ?></th>
				<th><?= $this->Paginator->sort('moved_out_employee_id'); ?></th>
				<th><?= $this->Paginator->sort('security_deposit_sent_dt'); ?></th>
				<th><?= $this->Paginator->sort('security_deposit_sent_employee_id'); ?></th>
				<th class="actions"><?= __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($moveoutCustomers as $moveoutCustomer): ?>
				<tr>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['id']); ?>&nbsp;</td>
					<td>
						<?= $this->Html->link($moveoutCustomer['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $moveoutCustomer['CustomerAccount']['id'])); ?>
					</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['move_out_date']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['close_out_date']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['security_deposit_address_line_1']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['security_deposit_address_line_2']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['security_deposit_city']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['security_deposit_state']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['security_deposit_zipcode']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['num_keycards_returned']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['num_metal_keys_returned']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['is_using_moving_company']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['move_out_requester_id']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['created']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['modified']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['all_keycards_returned_dt']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['all_keycards_returned_employee_id']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['all_metalkeys_returned_dt']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['all_metalkeys_returned_employee_id']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['coi_submitted_dt']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['coi_submitted_employee_id']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['coi_approved_dt']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['coi_approved_employee_id']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['moved_out_dt']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['moved_out_employee_id']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['security_deposit_sent_dt']); ?>&nbsp;</td>
					<td><?= h($moveoutCustomer['MoveoutCustomer']['security_deposit_sent_employee_id']); ?>&nbsp;</td>
					<td class="actions">
						<?= $this->Html->link(__('View'), array('action' => 'view', $moveoutCustomer['MoveoutCustomer']['id'])); ?>
						<?= $this->Html->link(__('Edit'), array('action' => 'edit', $moveoutCustomer['MoveoutCustomer']['id'])); ?>
						<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $moveoutCustomer['MoveoutCustomer']['id']), null, __('Are you sure you want to delete # %s?', $moveoutCustomer['MoveoutCustomer']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?= $this->element('paging') ?>

</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Moveout Customer'), array('action' => 'add')); ?></li>
		<li><?= $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

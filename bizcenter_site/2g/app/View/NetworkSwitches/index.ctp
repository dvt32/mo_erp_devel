<div class="networkSwitches index">
	<h2><?php echo __('Network Switches'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('mac_address'); ?></th>
			<th><?php echo $this->Paginator->sort('model'); ?></th>
			<th><?php echo $this->Paginator->sort('version'); ?></th>
			<th><?php echo $this->Paginator->sort('ip_address'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('password'); ?></th>
			<th><?php echo $this->Paginator->sort('port'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($networkSwitches as $networkSwitch): ?>
	<tr>
		<td><?php echo h($networkSwitch['NetworkSwitch']['id']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['mac_address']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['model']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['version']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['ip_address']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['notes']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['created']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['username']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['password']); ?>&nbsp;</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['port']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($networkSwitch['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $networkSwitch['Floor']['id'])); ?>
		</td>
		<td><?php echo h($networkSwitch['NetworkSwitch']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $networkSwitch['NetworkSwitch']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $networkSwitch['NetworkSwitch']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $networkSwitch['NetworkSwitch']['id']), null, __('Are you sure you want to delete # %s?', $networkSwitch['NetworkSwitch']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Network Switch'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

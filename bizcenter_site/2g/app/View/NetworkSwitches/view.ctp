<div class="networkSwitches view">
<h2><?php echo __('Network Switch'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mac Address'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['mac_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Version'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['version']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Port'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['port']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($networkSwitch['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $networkSwitch['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($networkSwitch['NetworkSwitch']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Network Switch'), array('action' => 'edit', $networkSwitch['NetworkSwitch']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Network Switch'), array('action' => 'delete', $networkSwitch['NetworkSwitch']['id']), null, __('Are you sure you want to delete # %s?', $networkSwitch['NetworkSwitch']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Network Switches'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Network Switch'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="span6 offset3">
<?= $this->Form->create('Notification', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
	<fieldset>
		<legend><?= __('New Notification') ?></legend>
		<?= $this->Form->input('name', array('label' => 'Notification Heading')) ?>
		<?= $this->Form->input('description') ?>
	</fieldset>
	<fieldset>
		<legend><?= __('Location') ?></legend>
		<div class="row-fluid">
			<div class="first span6">
				<?= $this->Form->input('building_id', array('empty' => '-- choose a building')) ?>
			</div>
			<div class="second span6">
				<?= $this->Form->input('floor_id', array('empty' => '-- choose a floor --')) ?>
			</div>
		</div>
	</fieldset>
	<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')) ?>
<?= $this->Form->end() ?>
</div>

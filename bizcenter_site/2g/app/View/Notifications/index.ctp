<div class="notifications index">
	<h2><?= __('Notifications') ?></h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('id') ?></th>
			<th><?= $this->Paginator->sort('name') ?></th>
			<th><?= $this->Paginator->sort('description') ?></th>
			<th><?= $this->Paginator->sort('room_id') ?></th>
			<th><?= $this->Paginator->sort('floor_id') ?></th>
			<th><?= $this->Paginator->sort('building_id') ?></th>
			<th><?= $this->Paginator->sort('created') ?></th>
			<th><?= $this->Paginator->sort('modified') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<?php foreach ($notifications as $notification): ?>
	<tr>
		<td><?= h($notification['Notification']['id']) ?>&nbsp;</td>
		<td><?= h($notification['Notification']['name']) ?>&nbsp;</td>
		<td><?= h($notification['Notification']['description']) ?>&nbsp;</td>
		<td>
			<?= $this->Html->link($notification['Room']['name'], array('controller' => 'rooms', 'action' => 'view', $notification['Room']['id'])) ?>
		</td>
		<td>
			<?= $this->Html->link($notification['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $notification['Floor']['id'])) ?>
		</td>
		<td>
			<?= $this->Html->link($notification['Building']['address_1'], array('controller' => 'buildings', 'action' => 'view', $notification['Building']['id'])) ?>
		</td>
		<td><?= h($notification['Notification']['created']) ?>&nbsp;</td>
		<td><?= h($notification['Notification']['modified']) ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('View'), array('action' => 'view', $notification['Notification']['id'])) ?>
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $notification['Notification']['id'])) ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $notification['Notification']['id']), null, __('Are you sure you want to delete # %s?', $notification['Notification']['id'])) ?>
		</td>
	</tr>
<?php endforeach ?>
	</table>
	<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )) ?>	</p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Notification'), array('action' => 'add')) ?></li>
		<li><?= $this->Html->link(__('List Rooms'), array('controller' => 'rooms', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('List Buildings'), array('controller' => 'buildings', 'action' => 'index')) ?> </li>
	</ul>
</div>

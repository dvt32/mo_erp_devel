<div class="outboundTrunks form">
<?php echo $this->Form->create('OutboundTrunk'); ?>
	<fieldset>
		<legend><?php echo __('Edit Outbound Trunk'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('trunk_name');
		echo $this->Form->input('trunk_type');
		echo $this->Form->input('location');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('OutboundTrunk.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('OutboundTrunk.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Outbound Trunks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('controller' => 'phone_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
	</ul>
</div>

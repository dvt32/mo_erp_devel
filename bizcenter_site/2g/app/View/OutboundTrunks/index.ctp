<div class="outboundTrunks index">
	<h2><?php echo __('Outbound Trunks'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('trunk_name'); ?></th>
			<th><?php echo $this->Paginator->sort('trunk_type'); ?></th>
			<th><?php echo $this->Paginator->sort('location'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($outboundTrunks as $outboundTrunk): ?>
	<tr>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['id']); ?>&nbsp;</td>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['trunk_name']); ?>&nbsp;</td>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['trunk_type']); ?>&nbsp;</td>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['location']); ?>&nbsp;</td>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['note']); ?>&nbsp;</td>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($outboundTrunk['OutboundTrunk']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $outboundTrunk['OutboundTrunk']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $outboundTrunk['OutboundTrunk']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $outboundTrunk['OutboundTrunk']['id']), null, __('Are you sure you want to delete # %s?', $outboundTrunk['OutboundTrunk']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Outbound Trunk'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('controller' => 'phone_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
	</ul>
</div>

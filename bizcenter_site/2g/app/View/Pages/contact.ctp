<style type="text/css">

	#map-canvas { 
		height: 25em; 
		width: 100%; 
	}
	.show-more:hover { 
		cursor:pointer; 
	}

</style>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script>
<div class="span12">
	<div class="span12 centered">
		<h1>Contact Us</h1>
		<br />
	</div>
	<div class="row-fluid">
		<div class="span6">
			<div id="map-canvas" class="unstyled"></div>
			<ul class="pull-left unstyled">
				<li>Service Desk: (212) 359-4233</li>
				<li>Sales Line: (646) 201-5508</li>
			</ul>
		</div>
		<div class="span6">
			
			<?= $this->Form->create('Customer', array('action' => 'makeContact')); ?>
				<fieldset>
				<?
					echo $this->Form->input('name', array('placeholder' => 'Full Name'));
					echo $this->Form->input('email', array('placeholder' => 'Email Address'));
					echo $this->Form->input('phone', array('placeholder' => 'Phone Number'));
					echo $this->Form->input('comments', array('placeholder' => 'Questions or Comments', 'type' => 'textarea', 'label' => 'How can we help?'));
				?>
				</fieldset>
			<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
			<?= $this->Form->end(); ?>
		</div>
		<div class="span12 centered">
			<h3>Interested in hosting or sponsoring a future event</h3>
			<p>
				Find out how we can address your office space needs! For some of the most commonly asked questions about our services, consult the Presales FAQ.
			</p>
		</div>
		<div class="span12 centered">
			<h3>Want to upgrade or change your space?</h3>
			<p>
				Awesome! We're happy to accomodate your co-working needs. Please contact us at <a href="mailto:sales@microofficecom">sales@microoffice.com</a> and one of our friendly sales reps will contact you.
			</p>
		</div>
	</div>
</div>
<script type="text/javascript">

	var centered = new google.maps.LatLng('40.7484881', '-73.980795');

	function initialize() {
		var mapOptions = {
		  center: centered,
		  zoom: 11,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
		
		var locations = [
							{
								"name": "Chelsea Offices",
								"nystyle": "between 6th Ave. and 7th Ave.",
								"address1": "122 W 27th St",
								"address2": "New York, NY 10001",
								"lat": "40.745794",
								"lng": -73.992311
							},
							{
								"name": "Midtown Offices",
								"nystyle": "between 37th Street and 38th Street",
								"address1": "1375 Broadway",
								"address2": "New York, NY 10018",
								"lat": "40.752979",
								"lng": -73.987859
							},
							{
								"name": "Union Square Offices",
								"nystyle": "between 20th Street and 21st Street",
								"address1": "902 Broadway",
								"address2": "New York, NY 10010",
								"lat": "40.739762",
								"lng": -73.989297
							},
							{
								"name": "Times Square Offices",
								"nystyle": "between 48th Street and 49th Street",
								"address1": "1601 Broadway",
								"address2": "New York, NY 10010",
								"lat": "40.760748",
								"lng": -73.985156
							},
							{
								"name": "The Harlem Garage",
								"nystyle": "118th Street and Frederick Douglass Blvd",
								"address1": "Coming this Fall",
								"address2": "",
								"lat": "40.806501",
								"lng": -73.954174
							}
						];

	   $.each(locations, function(k, o) {

			var contentString = '<div class="gmaps_content">'+
			'<strong class="firstHeading">'+o.name+'</strong>'+
			  '<div id="bodyContent">'+
				'<p>'+o.nystyle+'</p>'+
				'<p>'+o.address1+'</p>'+
				'<p>'+o.address2+'</p>'+
			  '</div>'+
			'</div>';

			var infowindow = new google.maps.InfoWindow({
			  content: contentString
			});

			var marker;
			marker = new google.maps.Marker({
			  position: new google.maps.LatLng(o.lat, o.lng),
			  map: map,
			  title: o.name
			});

			google.maps.event.addListener(marker, 'click', function() {
			  infowindow.open(map,marker);
			});
			google.maps.event.addListener(marker, 'mouseout', function() {
			  setTimeout(function() { infowindow.close() }, 3000);
			});
			$('.gmaps_content').mouseout(function() {
			  infowindow.open(map,marker);
			});
		});
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
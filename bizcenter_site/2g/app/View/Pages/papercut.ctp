
<div class="span12">
	<div class="row-fluid">
		<div class="span6 offset3">
			<h1><center>Shared Printing</h1></center>
			<img src="{{ bizcenter_2g_assetprefix }}/img/bg/start.jpg" alt="start" style="width:100%;" />
			<p>
			<h3>To print to our networked printers...</h3>

			<p class="lead">Install Papercut NG: Client can be downloaded for <a href="{{ bizcenter_2g_assetprefix }}/files/papercutclient/win.zip">Windows</a>, <a href="{{ bizcenter_2g_assetprefix }}/files/papercutclient/mac.zip">Mac</a>, and <a href="{{ bizcenter_2g_assetprefix }}/files/papercutclient/linux.zip">Linux</a>.</p>

			<ol>
				<b>Run the client then take following steps</b>

			<li>Accept the end user license agreement</li>
			<li>Choose folder to install to (Hint: unless you are very tech savvy, just let it install to the folder it specifies)</li>
			<li>Choose your form of install</li>
			<ul style="list-style-type:lower-alpha;">
				<li>Local Install (For all users) - this is for computers that may have multiple users such as domain log in or guest accounts that WILL print. </li>
				<li>Local Install (my account only) - this is if your user on the computer is the only person that should be printing. </li>
			</ul>
			<li>Press Install</li>
			<li>On the final page it says “Launch client and verify my identity (Recommended)” make sure that is clicked and press finish.</li>
			<li>Log in using the username and password provided to you by your Community Manager.</li>
			<li>If you’ve taken all of the steps correctly, on the upper right hand side of your screen a popup should appear which shows your Papercut balance.</li></ol>

			
			<p class="lead">Install Printers: To install the printers needed, follow these directions for Windows.</p>
			<ol>
			<li>Visit your control panel and under hardware and sound select view devices and printers</li>
			<li>Select “add a printer”</li>
			<li>The printers available for your use will be listed there. Select the one you’d like and press “next”.</li>
			<li>If the driver for the printer is unavailable on your computer it will ask for your permission to download it. Press “install driver”.</li>
			<li>You can choose to set it as a default printer if you’d like.</li>
		</ol>

			After taking this step, you are now all set and able to print on our system! Keep in mind that your computer can only print if you are logged into Papercut. If you’ve chosen to not remember your password it will ask you each time you attempt to print.
			
		
            </p>
			<p>
			</div>
			
			<p></p>
			<div class="span6 centered offset5">
			<p><a href="mailto:kyle@coalitionspace.com?subject=I need help with Papercut!" class="btn btn-primary">Need Help?</a></p>
			</div>				
		</div>
	</div>
</div>

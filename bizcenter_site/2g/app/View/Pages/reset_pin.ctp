<div class="row-fluid">
	<div class="span12">
		<h1>How to Reset your PIN Number</h1>
		<p class="lead">
			To edit a phone number, click on the phone number. The extension field specifies the four digit extension that can be used to dial numbers internally when in the office. The visibility field specifies whether the extension shows up on the intercom directory at the front desk. You cannot delete existing phone numbers or add new phone numbers through this web interface. To delete or add phone numbers, please email service@microoffice.com with your request.
		</p>
	</div>
</div>

<div class="span12">
	<div class="row-fluid">
		<div class="span6 centered offset3">
			<h1>Getting Started</h1>
			<p class="lead">Welcome to MicroOffice!</p>
			<img src="{{ bizcenter_2g_assetprefix }}/img/bg/start.jpg" alt="start" style="width:100%;" />
			<p>
				We're happy to have you join the Coalition.	
			</p>
			<p>The BizCenter is your control panel to make your stay here relaxed and efficient. You can add employees, make payments, schedule conference rooms, request tasks, etc...</p>
			<p>Please feel free to go through the sections on the left to help you get acquainted. If you have any questions, please visit the <a href="https://sites.google.com/a/microoffice.com/tenant-wiki">Help Pages</a> or contact your neighborhood friendly Office Manager!

          </a>
            </p>
			<p>
			</div>
			<div class="span12 offset3">
				<h2>Connect with us!</h2>			
				<!-- AddThis Follow BEGIN -->
			<div class="addthis_toolbox addthis_32x32_style addthis_default_style">
			<a class="addthis_button_facebook_follow" addthis:userid="microoffice"></a>
			<a class="addthis_button_twitter_follow" addthis:userid="microofficenyc"></a>
			<a class="addthis_button_linkedin_follow" addthis:userid="774009" addthis:usertype="company"></a>
			</div>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51fd379d642d860e"></script>
			<!-- AddThis Follow END -->
			</div>
			<p></p>
			<div class="span6 centered offset5">
			<p><a href="/customers/dashboard" class="btn btn-primary">Start Working</a></p>
			</div>				
		</div>
	</div>
</div>

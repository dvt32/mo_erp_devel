<div class="index">
	<h2><?= __('Manage Bank Accounts and Credit Cards'); ?></h2>
	<? if (empty($creditcards)) : ?>
		<div class="hero-unit">
			<h2>You currently have no credit cards with Micro Office</h2>
		</div>
	<? else: ?>
	<h3>Credit Cards</h3>
	<table class="table">
	<tr>
			<th><?= h('Account Name'); ?></th>
			<th><?= h('Card Number'); ?></th>
			<th><?= h('Account Added'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php  foreach ($creditcards as $creditcard): ?>
	<tr>
		<td>&nbsp;<?= $creditcard['CreditCard']['card_nickname'] ?></td>
		<td>&nbsp;<?= "*****".substr($creditcard['CreditCard']['card_number'], -4) ?></td>
		<td>&nbsp;<?= $creditcard['CreditCard']['creation_dt'] ?></td>
		<td>
			<ul class="nav nav-pills">
				<li>
					<?= $this->Html->link(__('Make Payment'), array('controller' => 'creditCardPayments','action' => 'add', $creditcard['CreditCard']['id'])); ?>
				</li>
				<li>
					<?= $this->Form->postLink('Delete', array(
						'controller' => 'creditCards', 'action' => 'delete', $creditcard['CreditCard']['id']));
					?>
				</li>
			</ul>
		</td>
		
	</tr>
	<?php endforeach; ?>
	</table>
	
	<? endif; ?>

	<br />

	<? if (empty($bankaccounts)) : ?>
		<div class="hero-unit">
			<h2>You currently have no bank accounts with Micro Office</h2>
		</div>
	<? else: ?>
	<h3>Bank Accounts</h3>
	<table class="table">
	<tr>
			<th><?= h('Account Name'); ?></th>
			<th><?= h('Account Type'); ?></th>
			<th><?= h('Account Number'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php  foreach ($bankaccounts as $bankaccount): ?>
	<tr>
		<td>&nbsp;<?= $bankaccount['BankAccount']['bank_account_nickname'] ?></td>
		<td>&nbsp;<?= $bankaccount['BankAccount']['bank_account_ownership_type'] ?></td>
		<td>&nbsp;<?= "*****".substr($bankaccount['BankAccount']['bank_account_number'], -4) ?></td>
		<td>
			<ul class="nav nav-pills">
				<li>
					<?php echo $this->Html->link(__('Make Payment'), array('controller' => 'electronicChecks','action' => 'add', $bankaccount['BankAccount']['id'])); ?>

				</li>
				<li>
					<?= $this->Form->postLink('Delete', array(
						'controller' => 'bankAccounts', 'action' => 'delete', $bankaccount['BankAccount']['id']));
					?>
				</li>
			</ul>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	
	<? endif; ?>
</div>
<div class="actions">
	<h3><?= __('Add New Accounts'); ?></h3>
	<ul class="nav nav-pills">
		<li><?= $this->Html->link(__('New Bank Account'), array('controller' => 'bankAccounts','action' => 'add')); ?></li>
		<li><?= $this->Html->link(__('New Credit Card Account'), array('controller' => 'creditCards', 'action' => 'add')); ?> </li>
	</ul>
</div>

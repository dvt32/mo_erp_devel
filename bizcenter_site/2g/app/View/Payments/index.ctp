<div class="index">
	<h2><?php echo __('Invoices and Payments'); ?></h2>

	<? if (!isset($payments)) : ?>

		<div class="hero-unit">
			<h2>You currently have no payments</h2>
		</div>

	<? else: ?>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('Invoice'); ?></th>
			<th><?php echo $this->Paginator->sort('date_sent'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php // foreach ($bankAccounts as $bankAccount): ?>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="actions"></td>
	</tr>
	<?php //endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>

	<? endif; ?>
</div>
<div class="row-fluid container">
	<div class="span12">
		<div class="row">
			<div class="span6">
				<p class="lead">
					If you would like to mail a paper check, please send it to:
				</p>
				<address>
				Micro Office Management LLC <br>
				1375 Broadway, Suite 389 <br>
				New York, NY 10018 <br>
				</address>
			</div>
			<div class="span6">
				<p>
				Please make your check payable to "Micro Office Management LLC", and please write your account number and invoice number(s) on your check. Your account number is shown on your invoice(s).
				</p>

				<p>
				Note that we have electronic check payment capability, which is faster, cheaper, easier, and less prone to loss, misdirection, or bank error. Please consider using our electronic check payment feature instead of mailing a paper check. You will save a stamp, an envelope, a check, and a trip to the mailbox or post office, and you will avoid worry that the check gets lost in the mail.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Make a New Payment'), array('action' => 'add')); ?></li>
	</ul>
</div>

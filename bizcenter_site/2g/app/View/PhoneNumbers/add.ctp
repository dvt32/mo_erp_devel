<div class="phoneNumbers form">
<?php echo $this->Form->create('PhoneNumber'); ?>
	<fieldset>
		<legend><?php echo __('Add Phone Number'); ?></legend>
	<?php
		echo $this->Form->input('account_id');
		echo $this->Form->input('person_id');
		echo $this->Form->input('floor_id');
		echo $this->Form->input('phone_number');
		echo $this->Form->input('purpose');
		echo $this->Form->input('visibility');
		echo $this->Form->input('domain');
		echo $this->Form->input('extension');
		echo $this->Form->input('device_type');
		echo $this->Form->input('iad_id');
		echo $this->Form->input('ip_phone_id');
		echo $this->Form->input('device_port');
		echo $this->Form->input('is_custom_inbound_dialplan');
		echo $this->Form->input('is_custom_outbound_dialplan');
		echo $this->Form->input('do_call_forward');
		echo $this->Form->input('call_forward_delay');
		echo $this->Form->input('call_forward_dst_num');
		echo $this->Form->input('do_simring');
		echo $this->Form->input('simring_dst_num_list');
		echo $this->Form->input('block_outbound_caller_id');
		echo $this->Form->input('is_voicemail_on');
		echo $this->Form->input('voicemail_box_num');
		echo $this->Form->input('voicemail_pin');
		echo $this->Form->input('sip_register_user_id');
		echo $this->Form->input('sip_register_password');
		echo $this->Form->input('voicemail_as_email');
		echo $this->Form->input('email_addr_for_vm');
		echo $this->Form->input('is_active');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('modification_dt');
		echo $this->Form->input('voicemail_delay');
		echo $this->Form->input('fax_as_email');
		echo $this->Form->input('email_addr_for_fax');
		echo $this->Form->input('hold_music');
		echo $this->Form->input('emit_debug_msg');
		echo $this->Form->input('inbound_trunk_id');
		echo $this->Form->input('outbound_trunk_id');
		echo $this->Form->input('sip_profile');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Iads'), array('controller' => 'iads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Iad'), array('controller' => 'iads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Phones'), array('controller' => 'ip_phones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Phone'), array('controller' => 'ip_phones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inbound Trunks'), array('controller' => 'inbound_trunks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inbound Trunk'), array('controller' => 'inbound_trunks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Outbound Trunks'), array('controller' => 'outbound_trunks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outbound Trunk'), array('controller' => 'outbound_trunks', 'action' => 'add')); ?> </li>
	</ul>
</div>

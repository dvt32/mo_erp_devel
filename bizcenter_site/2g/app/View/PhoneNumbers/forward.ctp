<div class="phoneNumbers form">
<?php echo $this->Form->create('PhoneNumber'); ?>
	<fieldset>
		<legend><?php echo __('Edit Phone Number'); ?></legend>
	<?php

		echo $this->Form->hidden('id');
	?>
	
	<label>
		Delay Time
		<a href="#" class="help" data-toggle="popover" data-placement="bottom" data-content="Delay time is the number of seconds your landline phone will ring before it forwards your call to your call forward number you specify below. " title="" data-original-title="Delay Time">
			<?= $this->Html->image('icons/flat_miniset/png/256/info.png', array('style' => 'height:1.5em;')) ?>
		</a>
	</label>
	
	<?
		echo $this->Form->input('call_forward_delay', array(
			'label' => '',
		));
	?>
	<?
		echo $this->Form->input('call_forward_dst_num', array('label' => 'Call Forwarding Number'));
		
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

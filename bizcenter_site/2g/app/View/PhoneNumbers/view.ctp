<div class="phoneNumbers view">
<h2><?php echo __('Phone Number'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phoneNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $phoneNumber['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phoneNumber['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $phoneNumber['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phoneNumber['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $phoneNumber['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone Number'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Purpose'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['purpose']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Visibility'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['visibility']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Domain'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['domain']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Extension'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['extension']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Device Type'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['device_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Iad'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phoneNumber['Iad']['id'], array('controller' => 'iads', 'action' => 'view', $phoneNumber['Iad']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Phone'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phoneNumber['IpPhone']['id'], array('controller' => 'ip_phones', 'action' => 'view', $phoneNumber['IpPhone']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Device Port'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['device_port']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Custom Inbound Dialplan'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['is_custom_inbound_dialplan']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Custom Outbound Dialplan'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['is_custom_outbound_dialplan']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Do Call Forward'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['do_call_forward']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Call Forward Delay'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['call_forward_delay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Call Forward Dst Num'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['call_forward_dst_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Do Simring'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['do_simring']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Simring Dst Num List'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['simring_dst_num_list']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Block Outbound Caller Id'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['block_outbound_caller_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Voicemail On'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['is_voicemail_on']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Voicemail Box Num'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['voicemail_box_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Voicemail Pin'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['voicemail_pin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sip Register User Id'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['sip_register_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sip Register Password'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['sip_register_password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Voicemail As Email'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['voicemail_as_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Addr For Vm'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['email_addr_for_vm']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modification Dt'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['modification_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Voicemail Delay'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['voicemail_delay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax As Email'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['fax_as_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Addr For Fax'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['email_addr_for_fax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hold Music'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['hold_music']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Emit Debug Msg'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['emit_debug_msg']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inbound Trunk'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phoneNumber['InboundTrunk']['id'], array('controller' => 'inbound_trunks', 'action' => 'view', $phoneNumber['InboundTrunk']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Outbound Trunk'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phoneNumber['OutboundTrunk']['id'], array('controller' => 'outbound_trunks', 'action' => 'view', $phoneNumber['OutboundTrunk']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sip Profile'); ?></dt>
		<dd>
			<?php echo h($phoneNumber['PhoneNumber']['sip_profile']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Phone Number'), array('action' => 'edit', $phoneNumber['PhoneNumber']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Phone Number'), array('action' => 'delete', $phoneNumber['PhoneNumber']['id']), null, __('Are you sure you want to delete # %s?', $phoneNumber['PhoneNumber']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Iads'), array('controller' => 'iads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Iad'), array('controller' => 'iads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Phones'), array('controller' => 'ip_phones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Phone'), array('controller' => 'ip_phones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inbound Trunks'), array('controller' => 'inbound_trunks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inbound Trunk'), array('controller' => 'inbound_trunks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Outbound Trunks'), array('controller' => 'outbound_trunks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outbound Trunk'), array('controller' => 'outbound_trunks', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="proximityCardEdits form">
<?php echo $this->Form->create('ProximityCardEdit'); ?>
	<fieldset>
		<legend><?php echo __('Proximity Card Request'); ?></legend>
	<?php
		echo $this->Form->input('person_id', array('options' => $customers));
		echo $this->Form->input('action_requested', array('options' => array('activate', 'deactivate')));
		echo $this->Form->input('cause', array('options' => array('lost card', 'damaged card', 'defective card', 'additional card for account')));
		echo $this->Form->input('notes');
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

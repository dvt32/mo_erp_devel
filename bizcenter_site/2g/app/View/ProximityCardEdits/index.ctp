<div class="proximityCardEdits index">
	<h2><?php echo __('Proximity Card Edits'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('number'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('person_id'); ?></th>
			<th><?php echo $this->Paginator->sort('action_requested'); ?></th>
			<th><?php echo $this->Paginator->sort('cause'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('operator'); ?></th>
			<th><?php echo $this->Paginator->sort('access_level_id'); ?></th>
			<th><?php echo $this->Paginator->sort('completed'); ?></th>
			<th><?php echo $this->Paginator->sort('rejected'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($proximityCardEdits as $proximityCardEdit): ?>
	<tr>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['id']); ?>&nbsp;</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['number']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($proximityCardEdit['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCardEdit['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($proximityCardEdit['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCardEdit['Customer']['id'])); ?>
		</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['action_requested']); ?>&nbsp;</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['cause']); ?>&nbsp;</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['notes']); ?>&nbsp;</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['operator']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($proximityCardEdit['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCardEdit['AccessLevel']['id'])); ?>
		</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['completed']); ?>&nbsp;</td>
		<td><?php echo h($proximityCardEdit['ProximityCardEdit']['rejected']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $proximityCardEdit['ProximityCardEdit']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $proximityCardEdit['ProximityCardEdit']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $proximityCardEdit['ProximityCardEdit']['id']), null, __('Are you sure you want to delete # %s?', $proximityCardEdit['ProximityCardEdit']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>

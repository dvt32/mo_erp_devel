<div class="proximityCardEdits view">
<h2><?php echo __('Proximity Card Edit'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCardEdit['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCardEdit['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCardEdit['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCardEdit['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action Requested'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['action_requested']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cause'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['cause']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Operator'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['operator']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Access Level'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCardEdit['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCardEdit['AccessLevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Completed'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['completed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rejected'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['rejected']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proximity Card Edit'), array('action' => 'edit', $proximityCardEdit['ProximityCardEdit']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proximity Card Edit'), array('action' => 'delete', $proximityCardEdit['ProximityCardEdit']['id']), null, __('Are you sure you want to delete # %s?', $proximityCardEdit['ProximityCardEdit']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Card Edits'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="proximityCards index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/key.png' , array('style' => 'height:3em;')) ?>
		<?= __('Proximity Cards') ?>
	</h2>
	<ul class="unstyled">
	<li>
		<?= $this->Html->link(__('Proximity Card Request'), array('controller' => 'proximityCards','action' => 'add')) ?>
	</li>
</ul>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('person_id') ?></th>
			<th><?= $this->Paginator->sort('number', 'Card Number') ?></th>
			<th><?= $this->Paginator->sort('account_id') ?></th>
			<th><?= $this->Paginator->sort('floor_id') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<? foreach ($proximityCards as $proximityCard): ?>
	<tr>
		<td>
			<?= $this->Html->link($proximityCard['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $proximityCard['Customer']['id'])) ?>
		</td>
		<td><?= h($proximityCard['ProximityCard']['number']) ?>&nbsp;</td>
		<td>
			<?= $this->Html->link($proximityCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCard['CustomerAccount']['id'])) ?>
		</td>
		<td>
			<?= h($proximityCard['Floor']['short_global_label']) ?>
		</td>
		<? if($proximityCard['ProximityCard']['status'] == "active"): ?>
		<td class="actions">
			<?= $this->Html->link(__('Deactivate'), array('action' => 'disable', $proximityCard['ProximityCard']['id']), null, __('Are you sure you want to deactivate %s\'s Proximity Card? Changes will be effective immediately. ', $proximityCard['Customer']['full_name'])) ?>
		</td>
		<? else :?>
		<td>
			Disabled
		</td>
		<? endif; ?>
	</tr>
<? endforeach ?>
	</table>
	<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?></p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>
<br>

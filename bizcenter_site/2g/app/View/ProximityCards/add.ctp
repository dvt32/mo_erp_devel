<div class="proximityCards form">
<?php echo $this->Form->create('ProximityCard'); ?>
	<fieldset>
		<legend><?php echo __('Add Proximity Card'); ?></legend>
	<?php
		echo $this->Form->input('number');
		echo $this->Form->input('type');
		echo $this->Form->input('status');
		echo $this->Form->input('account_id', array('type' => 'text'));
		echo $this->Form->input('person_id', array('type' => 'text'));
		echo $this->Form->input('note');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('access_level_id');
		echo $this->Form->input('floor_id');
		echo $this->Form->input('technology');
		echo $this->Form->input('site_code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

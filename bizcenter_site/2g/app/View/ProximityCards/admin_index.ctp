<div class="proximityCards index">
	<h2><?php echo __('Proximity Cards'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('number'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('person_id'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('access_level_id'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('technology'); ?></th>
			<th><?php echo $this->Paginator->sort('site_code'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($proximityCards as $proximityCard): ?>
	<tr>
		<td><?php echo h($proximityCard['ProximityCard']['id']); ?>&nbsp;</td>
		<td><?php echo h($proximityCard['ProximityCard']['number']); ?>&nbsp;</td>
		<td><?php echo h($proximityCard['ProximityCard']['type']); ?>&nbsp;</td>
		<td><?php echo h($proximityCard['ProximityCard']['status']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($proximityCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCard['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($proximityCard['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCard['Customer']['id'])); ?>
		</td>
		<td><?php echo h($proximityCard['ProximityCard']['note']); ?>&nbsp;</td>
		<td><?php echo h($proximityCard['ProximityCard']['creation_dt']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($proximityCard['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCard['AccessLevel']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($proximityCard['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $proximityCard['Floor']['id'])); ?>
		</td>
		<td><?php echo h($proximityCard['ProximityCard']['technology']); ?>&nbsp;</td>
		<td><?php echo h($proximityCard['ProximityCard']['site_code']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $proximityCard['ProximityCard']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $proximityCard['ProximityCard']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $proximityCard['ProximityCard']['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['ProximityCard']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="span12">
	<div class="row-fluid">
		<div class="hero-unit">
			<h1>Proximity Card has been Disabled</h1>
			<p class="lead">
				Please return the disabled Proximity Card to your Office Manager.
			</p>
			<p>If the Proximity Card is not returned within 30 days, you will be charged $25.</p>
			<div class="centered">
			<a href="{{ bizcenter_2g_urlprefix }}/customers/dashboard" class="btn btn-primary">
				Back to Dashboard
			</a>
			</div>
		</div>
	</div>
</div>

<div class="proximityCards index">
	<h2><?= __('Proximity Cards') ?></h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('number') ?></th>
			<th><?= $this->Paginator->sort('type') ?></th>
			<th><?= $this->Paginator->sort('status') ?></th>
			<th><?= $this->Paginator->sort('account_id') ?></th>
			<th><?= $this->Paginator->sort('person_id') ?></th>
			<th><?= $this->Paginator->sort('note') ?></th>
			<th><?= $this->Paginator->sort('creation_dt') ?></th>
			<th><?= $this->Paginator->sort('access_level_id') ?></th>
			<th><?= $this->Paginator->sort('floor_id') ?></th>
			<th><?= $this->Paginator->sort('technology') ?></th>
			<th><?= $this->Paginator->sort('site_code') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<? foreach ($proximityCards as $proximityCard): ?>
	<tr>
		<td><?= $this->Html->link($proximityCard['ProximityCard']['number'], array('controller' => 'proximitycards', 'action' => 'view', $proximityCard['ProximityCard']['id'])) ?>&nbsp;</td>
		<td><?= h($proximityCard['ProximityCard']['type']) ?>&nbsp;</td>
		<td><?= h($proximityCard['ProximityCard']['status']) ?>&nbsp;</td>
		<td>
			<?= $this->Html->link($proximityCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCard['CustomerAccount']['id'])) ?>
		</td>
		<td>
			<?= $this->Html->link($proximityCard['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCard['Customer']['id'])) ?>
		</td>
		<td><?= h($proximityCard['ProximityCard']['note']) ?>&nbsp;</td>
		<td><?= h($proximityCard['ProximityCard']['creation_dt']) ?>&nbsp;</td>
		<td>
			<?= $this->Html->link($proximityCard['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCard['AccessLevel']['id'])) ?>
		</td>
		<td>
			<?= $this->Html->link($proximityCard['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $proximityCard['Floor']['id'])) ?>
		</td>
		<td><?= h($proximityCard['ProximityCard']['technology']) ?>&nbsp;</td>
		<td><?= h($proximityCard['ProximityCard']['site_code']) ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $proximityCard['ProximityCard']['id'])) ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $proximityCard['ProximityCard']['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['ProximityCard']['id'])) ?>
		</td>
	</tr>
<? endforeach ?>
	</table>
	<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?></p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Proximity Card'), array('action' => 'add')) ?></li>
		<li><?= $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')) ?> </li>
	</ul>
</div>

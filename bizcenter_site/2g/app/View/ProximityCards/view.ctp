<div class="proximityCards view">
<h2><?php echo __('Proximity Card'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCard['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCard['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCard['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Access Level'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCard['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCard['AccessLevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCard['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $proximityCard['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Technology'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['technology']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Site Code'); ?></dt>
		<dd>
			<?php echo h($proximityCard['ProximityCard']['site_code']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proximity Card'), array('action' => 'edit', $proximityCard['ProximityCard']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proximity Card'), array('action' => 'delete', $proximityCard['ProximityCard']['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['ProximityCard']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

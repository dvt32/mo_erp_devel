<div class="receivePayments form">
<?php echo $this->Form->create('ReceivePayment'); ?>
	<fieldset>
		<legend><?php echo __('Add Receive Payment'); ?></legend>
	<?php
		echo $this->Form->input('amount');
		echo $this->Form->input('payment_method');
		echo $this->Form->input('notify_email');
		echo $this->Form->input('check_name');
		echo $this->Form->input('check_number');
		echo $this->Form->input('bank_account_name');
		echo $this->Form->input('bank_account_type');
		echo $this->Form->input('bank_routing_number');
		echo $this->Form->input('bank_account_number');

		echo $this->Form->input('card_number');
		echo $this->Form->input('card_expiration_month');
		echo $this->Form->input('card_expiration_year');
		echo $this->Form->input('card_security_code');
		echo $this->Form->input('card_bill_name');
		echo $this->Form->input('card_bill_company');
		echo $this->Form->input('card_bill_address_num');
		echo $this->Form->input('card_bill_zip_code');
		echo $this->Form->input('card_bill_address_1');
		echo $this->Form->input('card_bill_address_2');
		echo $this->Form->input('card_bill_city');
		echo $this->Form->input('card_bill_state');
		echo $this->Form->input('card_bill_country');
		echo $this->Form->input('card_bill_phone_number');
		echo $this->Form->input('card_bill_fax_number');
		echo $this->Form->input('card_bill_email_address');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

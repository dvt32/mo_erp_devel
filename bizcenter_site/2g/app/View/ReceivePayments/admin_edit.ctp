<div class="receivePayments form">
<?php echo $this->Form->create('ReceivePayment'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Receive Payment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('employee_id');
		echo $this->Form->input('amount');
		echo $this->Form->input('recv_dt');
		echo $this->Form->input('payment_method');
		echo $this->Form->input('process_txn_after_dt');
		echo $this->Form->input('process_mode');
		echo $this->Form->input('process_status');
		echo $this->Form->input('notify_email');
		echo $this->Form->input('client_hostname');
		echo $this->Form->input('client_ip_addr');
		echo $this->Form->input('invoice_number');
		echo $this->Form->input('note');
		echo $this->Form->input('is_reconciled');
		echo $this->Form->input('payment_destination');
		echo $this->Form->input('check_name');
		echo $this->Form->input('check_number');
		echo $this->Form->input('bank_account_name');
		echo $this->Form->input('bank_account_type');
		echo $this->Form->input('bank_routing_number');
		echo $this->Form->input('bank_account_number');
		echo $this->Form->input('card_number');
		echo $this->Form->input('card_expiration_month');
		echo $this->Form->input('card_expiration_year');
		echo $this->Form->input('card_security_code');
		echo $this->Form->input('card_bill_name');
		echo $this->Form->input('card_bill_company');
		echo $this->Form->input('card_bill_address_num');
		echo $this->Form->input('card_bill_zip_code');
		echo $this->Form->input('card_bill_address_1');
		echo $this->Form->input('card_bill_address_2');
		echo $this->Form->input('card_bill_city');
		echo $this->Form->input('card_bill_state');
		echo $this->Form->input('card_bill_country');
		echo $this->Form->input('card_bill_phone_number');
		echo $this->Form->input('card_bill_fax_number');
		echo $this->Form->input('card_bill_email_address');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('deposit_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ReceivePayment.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ReceivePayment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

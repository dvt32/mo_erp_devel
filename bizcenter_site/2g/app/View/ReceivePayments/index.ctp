<div class="receivePayments index">
	<h2><?php echo __('Receive Payments'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('cust_acct_id'); ?></th>
			<th><?php echo $this->Paginator->sort('employee_id'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('recv_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('payment_method'); ?></th>
			<th><?php echo $this->Paginator->sort('process_txn_after_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('process_mode'); ?></th>
			<th><?php echo $this->Paginator->sort('process_status'); ?></th>
			<th><?php echo $this->Paginator->sort('notify_email'); ?></th>
			<th><?php echo $this->Paginator->sort('client_hostname'); ?></th>
			<th><?php echo $this->Paginator->sort('client_ip_addr'); ?></th>
			<th><?php echo $this->Paginator->sort('invoice_number'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('is_reconciled'); ?></th>
			<th><?php echo $this->Paginator->sort('payment_destination'); ?></th>
			<th><?php echo $this->Paginator->sort('check_name'); ?></th>
			<th><?php echo $this->Paginator->sort('check_number'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_name'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_type'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_routing_number'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_account_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_month'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_year'); ?></th>
			<th><?php echo $this->Paginator->sort('card_security_code'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_name'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_company'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_address_num'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_address_1'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_address_2'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_city'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_state'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_country'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_email_address'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('deposit_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($receivePayments as $receivePayment): ?>
	<tr>
		<td><?php echo h($receivePayment['ReceivePayment']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($receivePayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $receivePayment['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($receivePayment['Employee']['title'], array('controller' => 'employees', 'action' => 'view', $receivePayment['Employee']['id'])); ?>
		</td>
		<td><?php echo h($receivePayment['ReceivePayment']['amount']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['recv_dt']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['payment_method']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['process_txn_after_dt']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['process_mode']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['process_status']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['notify_email']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['client_hostname']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['client_ip_addr']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['invoice_number']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['note']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['is_reconciled']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['payment_destination']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['check_name']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['check_number']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['bank_account_name']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['bank_account_type']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['bank_routing_number']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['bank_account_number']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_number']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_expiration_month']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_expiration_year']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_security_code']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_name']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_company']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_address_num']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_zip_code']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_address_1']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_address_2']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_city']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_state']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_country']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_fax_number']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['card_bill_email_address']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($receivePayment['ReceivePayment']['deposit_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $receivePayment['ReceivePayment']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $receivePayment['ReceivePayment']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $receivePayment['ReceivePayment']['id']), null, __('Are you sure you want to delete # %s?', $receivePayment['ReceivePayment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="receivePayments view">
<h2><?php echo __('Receive Payment'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($receivePayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $receivePayment['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($receivePayment['Employee']['title'], array('controller' => 'employees', 'action' => 'view', $receivePayment['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recv Dt'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['recv_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Payment Method'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['payment_method']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Process Txn After Dt'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['process_txn_after_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Process Mode'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['process_mode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Process Status'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['process_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notify Email'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['notify_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client Hostname'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['client_hostname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client Ip Addr'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['client_ip_addr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invoice Number'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['invoice_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Reconciled'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['is_reconciled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Payment Destination'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['payment_destination']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Check Name'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['check_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Check Number'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['check_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Name'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['bank_account_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Type'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['bank_account_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Routing Number'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['bank_routing_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bank Account Number'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['bank_account_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Number'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Month'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_expiration_month']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Year'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_expiration_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Security Code'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_security_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Name'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Company'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Address Num'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_address_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Zip Code'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Address 1'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Address 2'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill City'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill State'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Country'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Phone Number'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Fax Number'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Email Address'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['card_bill_email_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deposit Id'); ?></dt>
		<dd>
			<?php echo h($receivePayment['ReceivePayment']['deposit_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Receive Payment'), array('action' => 'edit', $receivePayment['ReceivePayment']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Receive Payment'), array('action' => 'delete', $receivePayment['ReceivePayment']['id']), null, __('Are you sure you want to delete # %s?', $receivePayment['ReceivePayment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Receive Payments'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Receive Payment'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

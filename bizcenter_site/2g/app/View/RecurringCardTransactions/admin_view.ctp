<div class="recurringCardTransactions view">
<h2><?php echo __('Recurring Card Transaction'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($recurringCardTransaction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $recurringCardTransaction['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($recurringCardTransaction['Employee']['title'], array('controller' => 'employees', 'action' => 'view', $recurringCardTransaction['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Process Mode'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['process_mode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Frequency'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['frequency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Next Date'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['next_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Number'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Month'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_expiration_month']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Expiration Year'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_expiration_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Security Code'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_security_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Name'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Company'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Address Num'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_address_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Zip Code'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Address 1'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Address 2'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill City'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill State'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Country'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Phone Number'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Fax Number'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Card Bill Email Address'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_email_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($recurringCardTransaction['RecurringCardTransaction']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Recurring Card Transaction'), array('action' => 'edit', $recurringCardTransaction['RecurringCardTransaction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Recurring Card Transaction'), array('action' => 'delete', $recurringCardTransaction['RecurringCardTransaction']['id']), null, __('Are you sure you want to delete # %s?', $recurringCardTransaction['RecurringCardTransaction']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Recurring Card Transactions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Recurring Card Transaction'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

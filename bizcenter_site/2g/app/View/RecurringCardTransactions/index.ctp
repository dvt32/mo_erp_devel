<div class="recurringCardTransactions index">
	<h2><?php echo __('Recurring Card Transactions'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('employee_id'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('process_mode'); ?></th>
			<th><?php echo $this->Paginator->sort('frequency'); ?></th>
			<th><?php echo $this->Paginator->sort('next_date'); ?></th>
			<th><?php echo $this->Paginator->sort('card_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_month'); ?></th>
			<th><?php echo $this->Paginator->sort('card_expiration_year'); ?></th>
			<th><?php echo $this->Paginator->sort('card_security_code'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_name'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_company'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_address_num'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_address_1'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_address_2'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_city'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_state'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_country'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('card_bill_email_address'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($recurringCardTransactions as $recurringCardTransaction): ?>
	<tr>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($recurringCardTransaction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $recurringCardTransaction['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($recurringCardTransaction['Employee']['title'], array('controller' => 'employees', 'action' => 'view', $recurringCardTransaction['Employee']['id'])); ?>
		</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['amount']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['process_mode']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['frequency']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['next_date']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_number']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_expiration_month']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_expiration_year']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_security_code']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_name']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_company']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_address_num']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_zip_code']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_address_1']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_address_2']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_city']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_state']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_country']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_phone_number']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_fax_number']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['card_bill_email_address']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['note']); ?>&nbsp;</td>
		<td><?php echo h($recurringCardTransaction['RecurringCardTransaction']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $recurringCardTransaction['RecurringCardTransaction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $recurringCardTransaction['RecurringCardTransaction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $recurringCardTransaction['RecurringCardTransaction']['id']), null, __('Are you sure you want to delete # %s?', $recurringCardTransaction['RecurringCardTransaction']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Recurring Card Transaction'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

<style type="text/css">
	#RoomReservations td,
	#RoomReservations th
	{
		text-align: center;
	}
	.tpassed {
		background: #eee;
	}
</style>

<?
// populate durations dropdown
$durations = array_self_combine(range(15,180,15));
array_walk($durations,function(&$duration) {
	$h = (int) ($duration / 60);
	$m = (int) ($duration % 60);
	$duration = sprintf("%d hours, %02d mins",$h,$m);
});

$datetime = DateTime::createFromFormat('Y-m-d', $date);
$datetime_clone = clone $datetime;
$disabled = strcmp(date('Y-m-d'),$datetime->format('Y-m-d')) > 0;
$cust_acct_id = AuthComponent::user('cust_acct_id');
?>

<div class="row-fluid" style="margin-bottom:1.5em;">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/clock.png' , array('style' => 'height:3em;')) ?>
		<?php echo __('Reservations'); ?>
	</h2>
	<h3><?= "{$floor}" ?></h3>
	<span class="big"><?= $datetime->format('F j, Y') ?></span>
	<br />
	<?= $this->Html->link('<<',    array('action' => 'book', $floor_id, $datetime_clone->modify('-1 day')->format('Y-m-d'))) ?>
	<?= $this->Html->link('Today', array('action' => 'book', $floor_id, date('Y-m-d'))) ?>
	<?= $this->Html->link('>>',    array('action' => 'book', $floor_id, $datetime_clone->modify('+2 day')->format('Y-m-d'))) ?>
</div>
<div class="row-fluid">
	<?= $this->Session->flash() ?>
	<table id="RoomReservations" class="table table-condensed table-bordered" style="width:100%;">
		<thead>
			<tr>
				<th>Time Slot</th>
				<? foreach ($rooms as $room): ?>
					<th><?= $room ?></th>
				<? endforeach ?>
			</tr>
		</thead>
		<tbody>
			<? foreach ($slots as $time => $slot): ?>
				<tr>
					<th style="width:0; padding-right:1em;"><?= date('g:ia', $time) ?></th>
					<? foreach ($rooms as $room_id => $room): ?>
						<? $reservation = @$slot[$room_id] ?>
						<? $hour = date('h',$time); $min = date('i',$time); $mer = date('a',$time) ?>
					<? if (!empty($reservation)): ?>
						<? if ($reservation['Reservation']['is_start']): ?>
							<td class="booked" style="border-top:#70803F 2px solid;">
							<?// var_dump($reservation['Reservation']['account_id']) ?>
							<? $duration = $reservation['Reservation']['duration'] ?>
							<? $authed = $reservation['Reservation']['account_id'] == $cust_acct_id ?>
							<? if ($authed && $time > time()): ?>
								<?= $this->Html->link(
										$reservation['CustomerAccount']['name'],
										'#ReservationBookForm',
										array(
											'class' => 'autofill',
											'data-id' => $reservation['Reservation']['id'],
											'data-hour' => $hour,
											'data-min' => $min,
											'data-mer' => $mer,
											'data-room' => $room_id,
											'data-dur' => $duration
										)
									)
								?>
								<?=
								$this->Form->postLink(
									__(' &times '),
									array('action' => 'remove', $reservation['Reservation']['id'], '?' => ['next' => $_SERVER['REQUEST_URI']]),
									array('escape' => false, 'class' =>'pull-right', 'style' => 'width:1.5em;background:#A4BA5D'),
									__('Are you sure you want to delete your %s reservation?', date('g:ia',$time))
								)
								?>
							<? else: ?>
								<?= $reservation['CustomerAccount']['name'] ?>
							<? endif ?>
							</td>
						<? else: ?>
							<td class="booked">&nbsp;</td>
						<? endif ?>
						<? else: ?>
							<td class="avail">
								<? if ($time > $btime): ?>
									<?= $this->Html->link('reserve', '#ReservationBookForm',
										array(
											'class' => 'autofill',
											'data-id' => '',
											'data-hour' => $hour,
											'data-min' => $min,
											'data-mer' => $mer,
											'data-room' => $room_id
										)
									)	 ?>
								<? else: ?>
									<a class="tpassed">time has passed</a>
								<? endif ?>
							</td>
						<? endif ?>
					<? endforeach ?>
				</tr>
			<? endforeach ?>
		</tbody>
	</table>
</div>
<div class="row-fluid" style="margin-top:2em;">
	<div class="span2">
		<section class="module">
			<div class="module-head"><b><?= $building ?></b></div>
			<div class="module-body">
				<ul class="nav nav-pills nav-stacked">
					<? foreach ($floors as $fid => $floor_label): ?>
						<li class="<?= $fid == $floor_id ? 'active' : '' ?>"><?= $this->Html->link($floor_label, array('action' => 'book', $fid, $datetime->format('Y-m-d'))) ?></li>
					<? endforeach ?>
				</ul>
			</div>
		</section>
	</div>
	<div class="span10">
		<section class="module">
			<div class="module-head"><b>View Another Day</b></div>
			<div class="module-body">
				<div class="text-center">
					<?= $this->Html->link('Today', array('action' => 'book', $floor_id, date('Y-m-d'))) ?>
				</div>
				<div class="row-fluid">
					<? $linkFormat = Router::url(array('controller' => $this->request->controller, 'action' => $this->request->action, $floor_id)) . '/%s'; ?>
					<div class="span4"><?= $this->element('mini-calendar', array('linkFormat' => $linkFormat, 'month' => $datetime->format('m')-1)) ?></div>
					<div class="span4"><?= $this->element('mini-calendar', array('linkFormat' => $linkFormat, 'month' => $datetime->format('m')+0, 'current' => $datetime->format('U'))) ?></div>
					<div class="span4"><?= $this->element('mini-calendar', array('linkFormat' => $linkFormat, 'month' => $datetime->format('m')+1)) ?></div>
				</div>
			</div>
		</section>
	</div>
</div>

<!-- Modal -->
<div id="reservationModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Reserve a room</h3>
  </div>
  <div class="modal-body">
    <section class="module" id="ReserveRoomSection">
		<div class="module-body">
			<?= $this->Form->create('Reservation', array('inputDefaults' => array('class' => 'input-block-level', 'disabled' => $disabled))) ?>
				<? if ($disabled): ?>
					<div class="row-fluid">
						<div class="alert alert-error">
							<strong>Sorry!</strong> You cannot reserve a room in the past
						</div>
					</div>
				<? endif ?>
				<div class="row-fluid">
					<div class="span12"><?= $this->Form->input('Reservation.room_id', array('empty' => '-- room --')) ?></div>
				</div>
				<div class="row-fluid">
					<div class="span5">
						<div class="datetime">
							<?= $this->Form->hidden('Reservation.start_date', array('value' => $date, 'div' => false)) ?>
							<?= $this->Form->input('Reservation.start_time', array('type' => 'time', 'div' => false, 'interval' => '15')) ?>
						</div>
						<?= $this->Form->input('Reservation.duration', array('options' => $durations)) ?>
					</div>
					<div class="span7">
						<?= $this->Form->input('Reservation.note', array('rows' => 4)) ?>
					</div>
				</div>
				<?= $this->Form->hidden('Reservation.end_date', array('value' => $date)) ?>
				<?= $this->Form->hidden('Reservation.id') ?>
		</div>
	</section>
  </div>
  <div class="modal-footer">
    	<?= $this->Form->button('Reserve Room', array('class' => 'btn btn-primary', 'disabled' => $disabled)) ?>
		<?= $this->Form->button('Clear', array('class' => 'btn-link pull-right', 'type' => 'reset', 'id' => 'resClear')) ?>
	<?= $this->Form->end() ?>
  </div>
</div>
 

<?= $this->Html->script('libs/chosen/chosen.jquery.min') ?>
<?= $this->Html->css('chosen') ?>
<script type="text/javascript">

$(".chosen").chosen();

// autofill form fields when a reservation timeslot is clicked
$(".autofill").click(function() {
	$("#reservationModal").modal('show');
	$("#ReserveRoomSection").hide().fadeIn('slow');
	$("#ReservationId").val($(this).attr("data-id"));
	$("#ReservationStartTimeHour").val($(this).attr("data-hour"));
	$("#ReservationStartTimeMin").val($(this).attr("data-min"));
	$("#ReservationStartTimeMeridian").val($(this).attr("data-mer"));
	$("#ReservationDuration").val($(this).attr("data-dur"));
	$("#ReservationRoomId").val($(this).attr("data-room"));
});

$('#resClear').click(function() {
	$("#reservationModal").modal('hide');
});
</script>
<style>
	tbody td { border:1px #eee solid; }
	td.booked {
		font-size:small;
		background:#c6e170;
		border-bottom:1px #c6e170 solid;
	}
	div.datetime select { width:auto!important; display:inline-block!important; }
	td.avail:hover { background:#eee; }
	td.avail a { display:block; height:100%; color:transparent; }
	td.avail a:hover { color:rgb(0, 136, 204); cursor:pointer; }
</style>


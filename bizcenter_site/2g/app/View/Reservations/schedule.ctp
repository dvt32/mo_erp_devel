<!-- calendar -->
<link href="{{ bizcenter_2g_assetprefix }}/js/libs/calendar/fullcalendar.css" rel="stylesheet" />
<link href="{{ bizcenter_2g_assetprefix }}/js/libs/calendar/fullcalendar.print.css" rel="stylesheet" />
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/calendar/jquery-ui.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/calendar/fullcalendar.min.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/dateformat.min.js"></script>
<script src="{{ bizcenter_2g_assetprefix }}/js/libs/pith/dateparsing.min.js.></script>
<!-- /calendar -->

<style>

	#calendar_reservations {
		width: 100%;
		margin: 0 auto;
	}
	#red-add-button {
		margin-top: 7em;
	}
</style>

<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<div class="">
				<div class="span6">
					<h2>
						<?= $this->Html->image('icons/flat_miniset/png/256/clock.png' , array('style' => 'height:3em;')) ?>
						<?= __('Reservations'); ?>
					</h2>
					<a href="{{ bizcenter_2g_urlprefix }}/reservations/book" class="btn btn-success">Classic Calendar</a>
					<p class="lead">
						You have <?= $roomMinutesRemaining  ?> minutes remaining.
					</p>
					<p>
						Click on the calendar below to view more information.
						<a href="#" class="help" data-toggle="popover" data-placement="bottom" data-content="To make a reservation, click and hold on the desired timeslots on the calendar. Drag and let go, to confirm the appointment. Click MAKE RESERVATION to confirm the appointment. " title="" data-original-title="Help">
							<?= $this->Html->image('icons/flat_miniset/png/256/info.png', array('style' => 'height:1.5em;')) ?>
						</a>
					</p>
				</div>

				<!-- upcoming reservations -->
				<div class="span6">
					<h3>Your Upcoming Appointments</h3>
					<? foreach ($myWeek[0]['Reservation'] as $reservation): ?>
						<? $start_time = dttotime($reservation['start_dt']) ?>
						<? $end_time = dttotime($reservation['end_dt']) ?>
						<? $dur_mins = ($end_time - $start_time) / 60 ?>

					<ul class="unstyled">
						<li><?= date('m/d/Y g:ia', $start_time) ?> (<?= $dur_mins ?> minutes)</li>
						<li><?= $rooms[$reservation['room_id']] ?></li>
					</ul>
					<? endforeach; ?>
				</div>

				<!-- calendar -->
				<div class="container">
					<div class="row-fluid">
						<div class="span9">
							<h1>Calendar</h1>
							<div id='calendar_reservations'></div>	
						</div>
						<div class="span3">
							<h3>Info</h3>
							<?= $this->Form->create('Reservation', array()) ?>
								<?= $this->Form->input('Reservation.room_id') ?>
							<?= $this->Form->end() ?>
							<div id="res-view"></div>
						</div>
					</div>
				</div>
				<!-- /calendar -->

				<!-- res-modal -->
				<div id="res-add" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header">
						<h3 id="mod-head-add">Add a Reservation</h3>
						<h3 id="mod-head-edit">Edit this Reservation</h3>
						<div id="alert-bar" class="alert" style="display:none;">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						  <strong id="alert-msg"></strong>
						</div>
					</div>
					<div class="modal-body">
						<form id="ReservationAdd" method="post">
						<? 
							echo $this->Form->hidden('id', array('name' => 'data[Reservation][id]'));
							echo $this->Form->input('room_id', array('name' => 'data[Reservation][room_id]'));
							echo $this->Form->input('start_dt', array('type' => 'text', 'name' => 'data[Reservation][start_dt]', 'class' => 'form_datetime', 'readonly'));
							echo $this->Form->input('end_dt', array('type' => 'text', 'name' => 'data[Reservation][end_dt]', 'class' => 'form_datetime', 'readonly'));
							echo $this->Form->input('note', array('name' => 'data[Reservation][note]'));
						?>
						</form>
					</div>
					<div class="modal-footer">
						<button id="ReservationDeleteButton" type="button" class="btn btn-danger" style="display:none;">Delete</button>
						<button id="ReservationAddButton" type="button" class="btn btn-info">Make Reservation</button>
						<button id="res-add-close-btn" type="button" class="btn">Close</button>
					</div>
				</div>
				<!-- /res-modal -->

				<!-- actions -->
				<div class="span9">
					<div class="actions">
						<h3><?= __('Actions'); ?></h3>
						<ul>
							<li><?= $this->Html->link(__('New Reservation'), array('action' => 'add')); ?></li>
							<li><?= $this->Html->link(__('List Rooms'), array('controller' => 'rooms', 'action' => 'index')); ?> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
		
		// DECLARE GLOBAL VARIABLES
		var evt = [];
		var reservations;
		var myCompany;
		var roomID = <?= (isset($current_room)) ? $current_room : "$('#ReservationRoomId').val()" ; ?>;

		jQuery.format.date.defaultLongDateFormat = "dd/MM/yyyy HH:mm:ss";

		function loadCal() {
			evt = [];
			// LOAD DATA
			$.getJSON("/reservations/month/"+ roomID +".json", function(d) {
				reservations = d;
				_.each(d.reservations, function(num, key) {
					if (<?=AuthComponent::user('cust_acct_id') ?> == num.CustomerAccount.id) {
						myCompany = num.CustomerAccount.name;
					}
					var res = [];
					res = {
						title: num.CustomerAccount.name,
						start: num.Reservation.start_dt,
						end: num.Reservation.end_dt,
						start_nice: num.Reservation.start_nice,
						end_nice: num.Reservation.end_nice,
						roomID: num.Reservation.room_id,
						allDay: false,
						note: num.Reservation.note,
						color: ((<?=AuthComponent::user('cust_acct_id') ?> == num.CustomerAccount.id) ? '#00E5A7' : '#072740'),
						editable: ((<?=AuthComponent::user('cust_acct_id') ?> == num.CustomerAccount.id) ? true : false),
						disableResizing: false,
						resid: num.Reservation.id
						// url: "/reservations/view/"+num.Reservation.id
					};

					evt.push(res);
				});
				// console.log(evt);
				calendar_reservations.fullCalendar( 'refetchEvents' );

			});
			return evt;

		}
		
		// LOAD CALENDAR
		var calendar_reservations = $('#calendar_reservations').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaWeek,agendaDay'
			},
			selectable: true,
			selectHelper: true,
			timeFormat: 'h:mm TT',
			slotMinutes: 15,
			allDaySlot:false,
			firstHour: '7',
			defaultView: 'agendaWeek',

			// CREATE RESERVATION
			select: function(start, end, allDay) {

				// change room 
				$('#room_id').val($('#ReservationRoomId').val());

				// Disable delete button
				$('#ReservationDeleteButton').hide();

				$('#res-add').modal('show');
				$('#mod-head-add').show();
				$('#mod-head-edit').hide();
				$("input[name='data[Reservation][start_dt]']").val(start);
				$("input[name='data[Reservation][end_dt]']").val(end);

				// ON BUTTON CLICK CREATE THE RESERVATION
				$("#ReservationAddButton").click(function() {
					var title = "Pith Studio";
					// SAVE THE APPOINTMENT
					$.ajax({
						type: "POST",
						url: "/reservations/addajax",
						data: $("#ReservationAdd").serialize(),
						beforeSend: function() {
							$('#ReservationAddButton').attr("disabled");
						},
						success: function(d) {
							$('#ReservationAddButton').removeAttr("disabled");	
							if (d == 1) {
								// location.reload();
								window.open("/reservations/schedule/"+$('#room_id').val(), "_self");
							} else {
								// say it failed
								$('#alert-bar').show('fast');
								$('#alert-msg').empty();
								$('#alert-msg').append("The reservation slot is not available");
							}
						}

					});
				});

			},
			editable: false,
			events: loadCal(roomID),
			disableDragging: true,

			// VIEW A RESERVATION
			eventClick: function(calEvent, jsEvent, view) {

				// enable delete button
				$('#ReservationDeleteButton').show();

				// EDIT MY EVENT
				if (calEvent.editable == true) {

					$('#res-add').modal('show');
					$('#mod-head-edit').show();
					$('#mod-head-add').hide();

					$("input[name='data[Reservation][id]']").val(calEvent.resid);
					$("input[name='data[Reservation][start_dt]']").val(calEvent.start);
					$("input[name='data[Reservation][end_dt]']").val(calEvent.end);
					$("select[name='data[Reservation][room_id]']").val(calEvent.roomID); 

					// ON BUTTON CLICK EDIT THE RESERVATION
				    $("#ReservationAddButton").click(function() {

						// SAVE THE APPOINTMENT
						$.ajax({
							type: "POST",
							url: "/reservations/editajax/"+calEvent.resid,
							data: $("#ReservationAdd").serialize(),
							beforeSend: function() {
								$('#ReservationAddButton').attr("disabled");
							},
							// If the ajax succeeds
							success: function(d) {
								$('#ReservationAddButton').removeAttr("disabled");	
								if (d == 1) {
 									location.reload();
								} else {
									// say it failed
									$('#alert-bar').show('fast');
									$('#alert-msg').empty();
									$('#alert-msg').append("The reservation slot is not available");
								}

							}
						});
					});

					// DELETE EVENT
					// ON BUTTON CLICK EDIT THE RESERVATION
					$("#ReservationDeleteButton").click(function() {
						$.ajax({
							type: "POST",
							url: "/reservations/delete/"+calEvent.resid,
							data: null,
							beforeSend: function() {
								$('#ReservationDeleteButton').attr("disabled");
							},
							success: function(d) {
								$('#ReservationAddButton').removeAttr("disabled");	
								if (d == 1) {
 									location.reload();
								} else {
									// say it failed
									$('#alert-bar').show('fast');
									$('#alert-msg').empty();
									$('#alert-msg').append("This reservation cannot be deleted");
								}
								$('#res-add').modal('hide');
							}
						});
					});

				// SOMEONE ELSE's EVENT
				} else {

					$('#res-view').show();
					$("#res-add").hide();
					$("#red-add-button").show();
					$("#res-view").empty();
					$("#res-view").append("<h3>View Reservation</h3>");
					$("#res-view").append("<dl id='evt-view'>");
					$("#evt-view").append("<dt>Event Title</dt><dd>"+calEvent.note+"</dd>");
					$("#evt-view").append("<dt>Start</dt><dd>"+calEvent.start_nice+"</dd>");
					$("#evt-view").append("<dt>End</dt><dd>"+calEvent.end_nice+"</dd>");

				}
			}
		});
		
		// ADD-RESERVATION BUTTON
		$('#red-add-button').click(function() {
			$('#res-add').modal('show');
		});
		// Close the res-add modal
		$('#res-add-close-btn').click(function() {
			$('#res-add').modal('hide');
			location.reload();
		});
		
		// If no JS, show the actions
		$('.actions').hide();

		// on change of the room options
		$("#ReservationRoomId").change(function() {

			roomID = $('#ReservationRoomId').val();
			calendar_reservations.fullCalendar('removeEventSource', evt);
            calendar_reservations.fullCalendar('addEventSource', loadCal());
            calendar_reservations.fullCalendar('refetchEvents');
			calendar_reservations.fullCalendar('rerenderEvents');

		});

		// Clear alerta
		$('#res-add-close-btn').click(function() {
			$('#alert-bar').hide();
			$('#alert-msg').empty();
		});


		// set room to scheduled room
		$(document).ready(function() {
			$('#ReservationRoomId').val(roomID);
		});


</script>


<div class="reservations index">
	<h2><?= __('Reservations'); ?></h2>
	<div class="actions">
		<?= $this->Html->link('Schedule & Reservations', array('controller' => 'reservations', 'action' => 'schedule')) ?>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('room_id'); ?></th>
				<th><?= $this->Paginator->sort('start_dt'); ?></th>
				<th>Duration</th>
				<th><?= $this->Paginator->sort('end_dt'); ?></th>
				<th><?= $this->Paginator->sort('note'); ?></th>
			</tr>
		</thead>
		<tbody>
			<? foreach ($reservations as $reservation): ?>
			<? $start_time = dttotime($reservation['Reservation']['start_dt']) ?>
			<? $end_time = dttotime($reservation['Reservation']['end_dt']) ?>
			<? $dur_mins = ($end_time - $start_time) / 60 ?>
			<tr>
				<td><?= "{$reservation['Room']['name']} ({$reservation['Room']['Floor']['label']})" ?> </td>
				<td><?= date('m/d/Y g:ia', $start_time) ?>&nbsp;</td>
				<td> <?= $dur_mins ?> minutes</td>
				<td><?= date('m/d/Y g:ia', $end_time) ?>&nbsp;</td>
				<td><?= h($reservation['Reservation']['note']); ?>&nbsp;</td>
			</tr>
			<? endforeach; ?>
		</tbody>
	</table>
	<?= $this->element('paging') ?>
</div>


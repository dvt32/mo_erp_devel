<div class="reservations view">
<h2><?php echo __('Reservation'); ?></h2>
	<dl>
		<dt><?php echo __('Room'); ?></dt>
		<dd>
			<?php echo h($reservation['Room']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($reservation['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $reservation['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start'); ?></dt>
		<dd>
			<?php echo h($reservation['Reservation']['start_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End'); ?></dt>
		<dd>
			<?php echo h($reservation['Reservation']['end_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($reservation['Reservation']['note']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Reservation'), array('action' => 'edit', $reservation['Reservation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Reservation'), array('action' => 'delete', $reservation['Reservation']['id']), null, __('Are you sure you want to delete # %s?', $reservation['Reservation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('New Reservation'), array('action' => 'schedule')); ?> </li>
	</ul>
</div>

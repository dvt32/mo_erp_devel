<div class="scanners view">
<h2><?php echo __('Scanner'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mac Address'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['mac_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Version'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['version']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($scanner['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $scanner['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($scanner['Scanner']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Scanner'), array('action' => 'edit', $scanner['Scanner']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Scanner'), array('action' => 'delete', $scanner['Scanner']['id']), null, __('Are you sure you want to delete # %s?', $scanner['Scanner']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Scanners'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scanner'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

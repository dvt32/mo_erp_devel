<div class="scans index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/curriculum.png' , array('style' => 'height:3em;')) ?>
		<?= __('Scans') ?>
	</h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('file_size') ?></th>
			<th><?= $this->Paginator->sort('file_timestamp', 'Scanned') ?></th>
			<th><?= $this->Paginator->sort('filename') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<?php foreach ($scans as $scan): ?>
	<tr>
		<td><?= h($scan['Scan']['file_size']) ?>&nbsp;</td>
		<td><?= h($scan['Scan']['file_timestamp']) ?>&nbsp;</td>
		<td><?= $this->Html->link($scan['Scan']['filename'], array(
			'controller' => 'moFiles', 
			'action' => 'view', $scan['Scan']['file_id'])); ?>&nbsp;
		</td>
		<td class="actions">
			<?= $this->Html->link($this->Html->image('icons/flat_miniset/png/256/download.png' , array('style' => 'height:1.5em;')), array(
				'controller' => 'moFiles', 
				'action' => 'download', $scan['Scan']['file_id']), array('escape' => false)) ?>
			<?= $this->Html->link(__('Claim'), array('action' => 'claim', $scan['Scan']['id'])) ?>
		</td>
	</tr>
<?php endforeach ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

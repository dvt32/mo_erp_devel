<div class="services view">
<h2><?php echo __('Service'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($service['Service']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($service['Service']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($service['Service']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($service['Service']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($service['Service']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($service['Service']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($service['Service']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($service['Service']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Service'), array('action' => 'edit', $service['Service']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Service'), array('action' => 'delete', $service['Service']['id']), null, __('Are you sure you want to delete # %s?', $service['Service']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Services'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Customer Accounts'); ?></h3>
	<?php if (!empty($service['CustomerAccount'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Primary Contact Id'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('External Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Web Site'); ?></th>
		<th><?php echo __('Visible'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Taxid'); ?></th>
		<th><?php echo __('Service Id'); ?></th>
		<th><?php echo __('Preferred Payment Method'); ?></th>
		<th><?php echo __('Room Minutes'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Pkg Notify Email'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Company Pic Url'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($service['CustomerAccount'] as $customerAccount): ?>
		<tr>
			<td><?php echo $customerAccount['id']; ?></td>
			<td><?php echo $customerAccount['name']; ?></td>
			<td><?php echo $customerAccount['primary_contact_id']; ?></td>
			<td><?php echo $customerAccount['is_active']; ?></td>
			<td><?php echo $customerAccount['ms_access_creation_dt']; ?></td>
			<td><?php echo $customerAccount['ms_access_random_num']; ?></td>
			<td><?php echo $customerAccount['created']; ?></td>
			<td><?php echo $customerAccount['external_name']; ?></td>
			<td><?php echo $customerAccount['description']; ?></td>
			<td><?php echo $customerAccount['web_site']; ?></td>
			<td><?php echo $customerAccount['visible']; ?></td>
			<td><?php echo $customerAccount['type']; ?></td>
			<td><?php echo $customerAccount['note']; ?></td>
			<td><?php echo $customerAccount['taxid']; ?></td>
			<td><?php echo $customerAccount['service_id']; ?></td>
			<td><?php echo $customerAccount['preferred_payment_method']; ?></td>
			<td><?php echo $customerAccount['room_minutes']; ?></td>
			<td><?php echo $customerAccount['floor_id']; ?></td>
			<td><?php echo $customerAccount['pkg_notify_email']; ?></td>
			<td><?php echo $customerAccount['modified']; ?></td>
			<td><?php echo $customerAccount['company_pic_url']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'customer_accounts', 'action' => 'view', $customerAccount['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'customer_accounts', 'action' => 'edit', $customerAccount['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'customer_accounts', 'action' => 'delete', $customerAccount['id']), null, __('Are you sure you want to delete # %s?', $customerAccount['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="sipTermCarr form">
<?php echo $this->Form->create('SipTermCarr'); ?>
	<fieldset>
		<legend><?php echo __('Add Sip Term Carr'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('domestic_format');
		echo $this->Form->input('domestic_gateway_name');
		echo $this->Form->input('domestic_proxy');
		echo $this->Form->input('intl_format');
		echo $this->Form->input('intl_gateway_name');
		echo $this->Form->input('intl_proxy');
		echo $this->Form->input('quality');
		echo $this->Form->input('is_up');
		echo $this->Form->input('last_uptime_check');
		echo $this->Form->input('last_rate_refresh');
		echo $this->Form->input('is_active');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sip Term Carr'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sip Term Rate Excepts'), array('controller' => 'sip_term_rate_excepts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate Except'), array('controller' => 'sip_term_rate_excepts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Rates'), array('controller' => 'sip_term_rates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate'), array('controller' => 'sip_term_rates', 'action' => 'add')); ?> </li>
	</ul>
</div>

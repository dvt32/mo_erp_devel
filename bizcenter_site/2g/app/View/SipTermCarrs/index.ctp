<div class="sipTermCarr index">
	<h2><?php echo __('Sip Term Carr'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('domestic_format'); ?></th>
			<th><?php echo $this->Paginator->sort('domestic_gateway_name'); ?></th>
			<th><?php echo $this->Paginator->sort('domestic_proxy'); ?></th>
			<th><?php echo $this->Paginator->sort('intl_format'); ?></th>
			<th><?php echo $this->Paginator->sort('intl_gateway_name'); ?></th>
			<th><?php echo $this->Paginator->sort('intl_proxy'); ?></th>
			<th><?php echo $this->Paginator->sort('quality'); ?></th>
			<th><?php echo $this->Paginator->sort('is_up'); ?></th>
			<th><?php echo $this->Paginator->sort('last_uptime_check'); ?></th>
			<th><?php echo $this->Paginator->sort('last_rate_refresh'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($sipTermCarrs as $sipTermCarr): ?>
	<tr>
		<td><?php echo h($sipTermCarr['SipTermCarr']['id']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['name']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['domestic_format']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['domestic_gateway_name']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['domestic_proxy']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['intl_format']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['intl_gateway_name']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['intl_proxy']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['quality']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['is_up']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['last_uptime_check']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['last_rate_refresh']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($sipTermCarr['SipTermCarr']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $sipTermCarr['SipTermCarr']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $sipTermCarr['SipTermCarr']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $sipTermCarr['SipTermCarr']['id']), null, __('Are you sure you want to delete # %s?', $sipTermCarr['SipTermCarr']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sip Term Carr'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Sip Term Rate Excepts'), array('controller' => 'sip_term_rate_excepts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate Except'), array('controller' => 'sip_term_rate_excepts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Rates'), array('controller' => 'sip_term_rates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate'), array('controller' => 'sip_term_rates', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="sipTermCarr view">
<h2><?php  echo __('Sip Term Carr'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Domestic Format'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['domestic_format']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Domestic Gateway Name'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['domestic_gateway_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Domestic Proxy'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['domestic_proxy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Intl Format'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['intl_format']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Intl Gateway Name'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['intl_gateway_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Intl Proxy'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['intl_proxy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quality'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['quality']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Up'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['is_up']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Uptime Check'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['last_uptime_check']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Rate Refresh'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['last_rate_refresh']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($sipTermCarr['SipTermCarr']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sip Term Carr'), array('action' => 'edit', $sipTermCarr['SipTermCarr']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sip Term Carr'), array('action' => 'delete', $sipTermCarr['SipTermCarr']['id']), null, __('Are you sure you want to delete # %s?', $sipTermCarr['SipTermCarr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Carr'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Carr'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Rate Excepts'), array('controller' => 'sip_term_rate_excepts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate Except'), array('controller' => 'sip_term_rate_excepts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Rates'), array('controller' => 'sip_term_rates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate'), array('controller' => 'sip_term_rates', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Sip Term Rate Excepts'); ?></h3>
	<?php if (!empty($sipTermCarr['SipTermRateExcept'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sip Term Carr Id'); ?></th>
		<th><?php echo __('Prefix'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('First Interval'); ?></th>
		<th><?php echo __('Subsequent Interval'); ?></th>
		<th><?php echo __('Rate'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($sipTermCarr['SipTermRateExcept'] as $sipTermRateExcept): ?>
		<tr>
			<td><?php echo $sipTermRateExcept['id']; ?></td>
			<td><?php echo $sipTermRateExcept['sip_term_carr_id']; ?></td>
			<td><?php echo $sipTermRateExcept['prefix']; ?></td>
			<td><?php echo $sipTermRateExcept['description']; ?></td>
			<td><?php echo $sipTermRateExcept['first_interval']; ?></td>
			<td><?php echo $sipTermRateExcept['subsequent_interval']; ?></td>
			<td><?php echo $sipTermRateExcept['rate']; ?></td>
			<td><?php echo $sipTermRateExcept['ms_access_creation_dt']; ?></td>
			<td><?php echo $sipTermRateExcept['ms_access_random_num']; ?></td>
			<td><?php echo $sipTermRateExcept['creation_dt']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sip_term_rate_excepts', 'action' => 'view', $sipTermRateExcept['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sip_term_rate_excepts', 'action' => 'edit', $sipTermRateExcept['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sip_term_rate_excepts', 'action' => 'delete', $sipTermRateExcept['id']), null, __('Are you sure you want to delete # %s?', $sipTermRateExcept['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sip Term Rate Except'), array('controller' => 'sip_term_rate_excepts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Sip Term Rates'); ?></h3>
	<?php if (!empty($sipTermCarr['SipTermRate'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sip Term Carr Id'); ?></th>
		<th><?php echo __('Prefix'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('First Interval'); ?></th>
		<th><?php echo __('Subsequent Interval'); ?></th>
		<th><?php echo __('Rate'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($sipTermCarr['SipTermRate'] as $sipTermRate): ?>
		<tr>
			<td><?php echo $sipTermRate['id']; ?></td>
			<td><?php echo $sipTermRate['sip_term_carr_id']; ?></td>
			<td><?php echo $sipTermRate['prefix']; ?></td>
			<td><?php echo $sipTermRate['description']; ?></td>
			<td><?php echo $sipTermRate['first_interval']; ?></td>
			<td><?php echo $sipTermRate['subsequent_interval']; ?></td>
			<td><?php echo $sipTermRate['rate']; ?></td>
			<td><?php echo $sipTermRate['ms_access_creation_dt']; ?></td>
			<td><?php echo $sipTermRate['ms_access_random_num']; ?></td>
			<td><?php echo $sipTermRate['creation_dt']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sip_term_rates', 'action' => 'view', $sipTermRate['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sip_term_rates', 'action' => 'edit', $sipTermRate['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sip_term_rates', 'action' => 'delete', $sipTermRate['id']), null, __('Are you sure you want to delete # %s?', $sipTermRate['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sip Term Rate'), array('controller' => 'sip_term_rates', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

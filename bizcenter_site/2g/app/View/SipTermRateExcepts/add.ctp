<div class="sipTermRateExcept form">
<?php echo $this->Form->create('SipTermRateExcept'); ?>
	<fieldset>
		<legend><?php echo __('Add Sip Term Rate Except'); ?></legend>
	<?php
		echo $this->Form->input('sip_term_carr_id');
		echo $this->Form->input('prefix');
		echo $this->Form->input('description');
		echo $this->Form->input('first_interval');
		echo $this->Form->input('subsequent_interval');
		echo $this->Form->input('rate');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sip Term Rate Except'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sip Term Carrs'), array('controller' => 'sip_term_carrs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Carr'), array('controller' => 'sip_term_carrs', 'action' => 'add')); ?> </li>
	</ul>
</div>

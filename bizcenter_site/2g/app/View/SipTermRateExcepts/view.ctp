<div class="sipTermRateExcept view">
<h2><?php  echo __('Sip Term Rate Except'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sip Term Carr'); ?></dt>
		<dd>
			<?php echo $this->Html->link($sipTermRateExcept['SipTermCarr']['name'], array('controller' => 'sip_term_carrs', 'action' => 'view', $sipTermRateExcept['SipTermCarr']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefix'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['prefix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Interval'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['first_interval']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subsequent Interval'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['subsequent_interval']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rate'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($sipTermRateExcept['SipTermRateExcept']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sip Term Rate Except'), array('action' => 'edit', $sipTermRateExcept['SipTermRateExcept']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sip Term Rate Except'), array('action' => 'delete', $sipTermRateExcept['SipTermRateExcept']['id']), null, __('Are you sure you want to delete # %s?', $sipTermRateExcept['SipTermRateExcept']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Rate Except'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate Except'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Carrs'), array('controller' => 'sip_term_carrs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Carr'), array('controller' => 'sip_term_carrs', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="sipTermRate view">
<h2><?php  echo __('Sip Term Rate'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sip Term Carr'); ?></dt>
		<dd>
			<?php echo $this->Html->link($sipTermRate['SipTermCarr']['name'], array('controller' => 'sip_term_carrs', 'action' => 'view', $sipTermRate['SipTermCarr']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefix'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['prefix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Interval'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['first_interval']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subsequent Interval'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['subsequent_interval']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rate'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($sipTermRate['SipTermRate']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sip Term Rate'), array('action' => 'edit', $sipTermRate['SipTermRate']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sip Term Rate'), array('action' => 'delete', $sipTermRate['SipTermRate']['id']), null, __('Are you sure you want to delete # %s?', $sipTermRate['SipTermRate']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Rate'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Rate'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sip Term Carrs'), array('controller' => 'sip_term_carrs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Carr'), array('controller' => 'sip_term_carrs', 'action' => 'add')); ?> </li>
	</ul>
</div>

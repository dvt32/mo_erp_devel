<div class="row-fluid">
	<?= $this->Form->create('User', array('class' => 'form-signin', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'input-block-level') )) ?>
		<legend>Sudo Login</legend>
		<?= $this->Form->input('cust_acct_id', array('empty' => ' -- Account --', 'options' => $customerAccounts)) ?>
		<?= $this->Form->input('cust_pers_id', array('empty' => ' -- Customer --', 'options' => $customers)) ?>
		<?= $this->Form->input('user_id',      array('empty' => ' -- Username --', 'options' => $users)) ?>
		<?= $this->Form->button('Sudo Login', array('class' => 'btn btn-block')) ?>
	<?= $this->Form->end() ?>
</div>

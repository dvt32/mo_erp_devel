<div class="row-fluid">
	<div class="span6 offset3">
		<?= $this->Form->create('Task', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>
				<legend>
					<?= $this->Html->image('icons/flat_miniset/png/256/push_pin.png' , array('style' => 'height:3em;')) ?>
					Request a Task
				</legend>
				<!-- visible -->
				<?= $this->Form->input('Task.description', array('placeholder' => 'Please provide a brief description of the problem or task.')) ?>
				<?= $this->Form->input('Task.requested_completion_date', array('type' => 'text', 'readonly')) ?>
				<?= $this->Form->input('Task.floor_id') ?>
				<?= $this->Form->input('Task.area', array('label' => 'Category / Area', 'options' => $areas, 'empty' => '-- choose a category --')) ?>

			</fieldset>
			<?= $this->Form->button('Submit', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
		<br>
		<?= $this->Html->link('Request item removal', array('controller' => 'tasks', 'action' => 'removal')); ?>
	</div>
</div>
<script>
// Datepicker
$("#TaskRequestedCompletionDate").datetimepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  todayBtn: true,
  forceParse: false,
  minView: 2
});
</script>
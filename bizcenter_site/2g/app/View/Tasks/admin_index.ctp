<div class="tasks index">
	<h2><?php echo __('Tasks'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('employee_id'); ?></th>
			<th><?php echo $this->Paginator->sort('requestedby_employee_id'); ?></th>
			<th><?php echo $this->Paginator->sort('requestedby_cust_pers_id'); ?></th>
			<th><?php echo $this->Paginator->sort('requestedby_cust_acct_id'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_date'); ?></th>
			<th><?php echo $this->Paginator->sort('requested_completion_date'); ?></th>
			<th><?php echo $this->Paginator->sort('completion_date'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('urgency'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('area'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tasks as $task): ?>
	<tr>
		<td><?php echo h($task['Task']['id']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['description']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($task['Employee']['title'], array('controller' => 'employees', 'action' => 'view', $task['Employee']['id'])); ?>
		</td>
		<td><?php echo h($task['Task']['requestedby_employee_id']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['requestedby_cust_pers_id']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['requestedby_cust_acct_id']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['creation_date']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['requested_completion_date']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['completion_date']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['status']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['urgency']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['note']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['created']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($task['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $task['Floor']['id'])); ?>
		</td>
		<td><?php echo h($task['Task']['area']); ?>&nbsp;</td>
		<td><?php echo h($task['Task']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $task['Task']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $task['Task']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $task['Task']['id']), null, __('Are you sure you want to delete # %s?', $task['Task']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Task'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="row-fluid">
	<div class="span6 offset3">
		<?= $this->Form->create('Task', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>
				<legend>Edit Task</legend>
				
				<!-- visible -->
				<?= $this->Form->input('Task.id') ?>
				<?= $this->Form->input('Task.description', array('placeholder' => 'Please provide a brief description of the problem or task.')) ?>
				<?= $this->Form->input('Task.requested_completion_date', array('type' => 'text', 'class' => 'form_datetime')) ?>
				<?= $this->Form->input('Task.floor_id') ?>
				<?= $this->Form->input('Task.area', array('label' => 'Category / Area', 'options' => $areas, 'empty' => '-- choose a category --')) ?>
				
				<!-- hidden -->
				<?= $this->Form->hidden('Task.requestedby_cust_pers_id') ?>
				<?= $this->Form->hidden('Task.requestedby_cust_acct_id') ?>
				<?= $this->Form->hidden('Task.status', array('value' => 'requested')) ?>
			</fieldset>
			<?= $this->Form->button('Submit', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

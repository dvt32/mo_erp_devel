<div class="tasks index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/checkmark.png' , array('style' => 'height:2em;')) ?>
		<?= __('Tasks') ?>
	</h2>
	<table class="table">
	<tr>
		<th></th>
		<th><?= $this->Paginator->sort('requestedby_cust_pers_id', 'Person') ?></th>
		<th><?= $this->Paginator->sort('creation_date', 'Date Requested') ?></th>
		<th><?= $this->Paginator->sort('area', 'Request Type') ?></th>
		<th><?= $this->Paginator->sort('requested_completion_date', 'Requested Completion Date') ?></th>
		<th><?= $this->Paginator->sort('status') ?></th>
	</tr>
	<? foreach ($tasks as $task): ?>
	<tr>
		<td>
			<div><?= $this->Form->postLink(__('Cancel this request'), array('action' => 'delete', $task['Task']['id']), null, __('Are you sure you want to delete this task?', $task['Task']['id'])) ?></div>
		</td>
		<td><?= h($task['Customer']['full_name']) ?>&nbsp;</td>
		<td><?= h($task['Task']['creation_date']) ?>&nbsp;</td>
		<td><?= h($task['Task']['area']) ?>&nbsp;</td>
		<td><?= h($task['Task']['requested_completion_date']) ?>&nbsp;</td>
		<td>
			<? if (strcasecmp('completed', $task['Task']['status'])): ?>
				
			<? endif ?>
			<?= h($task['Task']['status']) ?>&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="7" style="border-top:none;">
			<p><strong>Description: </strong><?= h($task['Task']['description']) ?>&nbsp;</p>
		</td>
	</tr>
<? endforeach ?>
	</table>
	<p> <?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?>	</p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>

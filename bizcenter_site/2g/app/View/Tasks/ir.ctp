
<style type="text/css">
	<?php
		$bg = array('bg1.jpg', 'bg2.jpg', 'bg3.jpg', 'bg4.jpg', 'bg5.jpg', 'bg6.jpg', 'bg7.jpg'); 
		$i = rand(0, count($bg)-1); // generate random number size of the array
		$selectedBg = "$bg[$i]"; // set var iable equal to which random filename was chosen
	?>

	body {
		margin:0;
		padding:0;
		color: #000;

		background: url('/img/bg/itemRemoval/<?= $selectedBg ?>') no-repeat center center fixed; 

		/* resize background to fit */
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='.myBackground.jpg', sizingMethod='scale');
		-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='myBackground.jpg', sizingMethod='scale')";
	}

	.tbox {
		background-color: rgba(255,255,255,0.8); /* CSS3 */zoom:1; /* required for the filters */
		 *background-color: transparent ; /* IE7 only */
		 background-color: transparent\9 ; /* IE8 only */
		-ms-filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#80FFFFFF, endColorstr=#80FFFFFF) ; /* IE8 */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#80FFFFFF, endColorstr=#80FFFFFF); /* IE7 */

		padding-left: 2em;
		padding-right: 2em;
		padding-bottom: 2em;
	}


</style>
<div class="row-fluid">
	<div class="span12 tbox">
		<div class="row-fluid">
			<div class="span12">
				<h1>Offical Item Removal Form</h1>
				<h2>PRINTED ON: <?= CakeTime::nice(date("U")) ?></h2>
			</div>
			<div class="span6">
				<h3><?= $task['CustomerAccount']['name'] ?></h3>
				<dl>
					<dt>Move Out Date</dt>
					<dd><?= CakeTime::format($task['Task']['requested_completion_date'], '%Y-%m-%d') ?></dd>
					<dt>Building</dt>
					<dt><address>
						<?= $task['Floor']['Building']['address_1'] ?><br />
						<?= $task['Floor']['Building']['city']." ".$task['Floor']['Building']['state']." ".$task['Floor']['Building']['zip_code']  ?>
					</address></dt>
					<dt>Floor</dt>
					<dd><?= $task['Floor']['label'] ?></dd>
					<dt>Office Manager</dt>
					<dd><?= $task['CustomerAccount']['Floor']['OfficeManager']['full_name'] ?></dd>
				</dl>
				<strong>Description</strong>
				<p class="lead">
					<?= $task['Task']['description'] ?>
				</p>

			</div>

		</div>
		<div>
			<div class="pull-right">
				<img src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl=<?= substr(date("D"), 0,2).$task['Task']['jumble'].random_string(30) ?>&amp;choe=UTF-8" alt="QR code">
			</div>
			<?= substr(date("D"), 0,2).$task['Task']['jumble'].random_string(120) ?>
		</div>
	</div>
</div>

<script>
// Location tracking
var x=document.getElementById("demo");
function getLocation() {if (navigator.geolocation) {navigator.geolocation.getCurrentPosition(showPosition); } else{x.innerHTML="Geolocation is not supported by this browser.";} } function showPosition(position) {x.innerHTML="Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude; } getLocation(); </script>
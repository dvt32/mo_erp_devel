<div class="row-fluid">
	<div class="span6 offset3">
		<?= $this->Form->create('Task', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>
				<legend>Request Item Removal Slip</legend>
				<?= $this->Session->flash() ?>
				<!-- visible -->
				<?= $this->Form->input('Task.description', array('placeholder' => 'Please provide a list of items that will be removed')) ?>
				<?= $this->Form->input('Task.requested_completion_date', array('type' => 'text', 'readonly', 'label' => 'Requested Removal Date')) ?>
				<?= $this->Form->input('Task.floor_id') ?>
			</fieldset>
			<?= $this->Form->button('Submit', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
		<br>
		<?= $this->Html->link('Request other task', array('controller' => 'tasks', 'action' => 'add')); ?>
	</div>
</div>

<script>
// Datepicker
$("#TaskRequestedCompletionDate").datetimepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  todayBtn: true,
  forceParse: false,
  minView: 2
});
</script>

<div class="tasks view">
<h2><?php echo __('Task'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($task['Task']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($task['Task']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($task['Employee']['title'], array('controller' => 'employees', 'action' => 'view', $task['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requestedby Employee Id'); ?></dt>
		<dd>
			<?php echo h($task['Task']['requestedby_employee_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requestedby Cust Pers Id'); ?></dt>
		<dd>
			<?php echo h($task['Task']['requestedby_cust_pers_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requestedby Cust Acct Id'); ?></dt>
		<dd>
			<?php echo h($task['Task']['requestedby_cust_acct_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Date'); ?></dt>
		<dd>
			<?php echo h($task['Task']['creation_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requested Completion Date'); ?></dt>
		<dd>
			<?php echo h($task['Task']['requested_completion_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Completion Date'); ?></dt>
		<dd>
			<?php echo h($task['Task']['completion_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($task['Task']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Urgency'); ?></dt>
		<dd>
			<?php echo h($task['Task']['urgency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($task['Task']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($task['Task']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($task['Task']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($task['Task']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($task['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $task['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Area'); ?></dt>
		<dd>
			<?php echo h($task['Task']['area']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($task['Task']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Task'), array('action' => 'edit', $task['Task']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Task'), array('action' => 'delete', $task['Task']['id']), null, __('Are you sure you want to delete # %s?', $task['Task']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tasks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Task'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

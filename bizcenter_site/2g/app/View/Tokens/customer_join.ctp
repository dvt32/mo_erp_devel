<? $disabled = $valid_token ? '' : 'disabled' ?>

<!-- customers/join -->
<div class="row-fluid">
	<div class="span6 offset3">
		<?= $this->Form->create('Customer', array('inputDefaults' => array('class' => 'input-block-level', 'disabled' => $disabled))) ?>
			<fieldset>
				<?= $this->Session->flash() ?>
				<legend>Register for BizCenter</legend>

				<!-- customer information -->
				<?= $this->Form->input('Customer.id') ?>
				<?= $this->Form->input('Customer.first_name') ?>
				<?= $this->Form->input('Customer.last_name') ?>
				<?= $this->Form->hidden('Customer.is_active', array('value' => true)) ?>

				<!-- user information -->
			</fieldset>
			<fieldset>
				<legend>Login Information</legend>
				<?= $this->Form->input('User.id') ?>
				<?= $this->Form->input('User.password', array('type' => 'password')) ?>
				<?= $this->Form->input('User.confirm_password', array('type' => 'password')) ?>

			</fieldset>
			<?= $this->Form->button('Join', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

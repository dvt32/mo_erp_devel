<div class="row-fluid">
	<?= $this->Form->create('Token', array('class' => 'centered-form', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'input-block-level') )) ?>
		<h2 class="centered-form-heading align-center"><?= __('Forgot Your Password?') ?></h2>
		<?= $this->Session->flash() ?>
		<?= $this->Form->input('User.username', array('placeholder' => 'Email Address', 'autofocus' => 'autofocus')) ?>
		<?= $this->Form->hidden('Token.type', array('value' => 'resetPassword')) ?>
		<button id="login-button" class="btn btn-large btn-block btn-primary" type="submit"><?= __('Submit') ?></button>
		<br />
		<div class="align-center">
			Remember your password?
			<?= $this->Html->link('Back to login', array('controller' => 'users', 'action' => 'login')) ?></a>
		</div>
	</form>
</div>

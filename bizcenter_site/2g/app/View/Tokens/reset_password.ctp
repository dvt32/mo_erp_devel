<div class="row-fluid">
	<?= $this->Form->create('Token', array('class' => 'centered-form', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'input-block-level', 'disabled' => $valid_token ? false : '1') )) ?>
		<h2 class="centered-form-heading align-center"><?= __('New Password') ?></h2>
		<?= $this->Session->flash() ?>
		<?= $this->Form->input('User.password', array('placeholder' => 'New Password')) ?>
		<?= $this->Form->input('User.confirm_password', array('placeholder' => 'Confirm Password', 'type' => 'password')) ?>
		<?= $this->Form->button(__('Submit'), array('class' => "btn btn-large btn-block btn-primary", 'disabled' => $valid_token ? false : '1')) ?>
		<br />
		<div class="align-center">
			<?= $this->Html->link(__('Remember your password? '), array('controller' => 'users', 'action' => 'login')) ?></a>
		</div>
	</form>
</div>

<div class="users form">
  <?= $this->Form->create('User') ?>
  	<fieldset>
  		<legend>User Info</legend>
  		<?= $this->Form->input('User.username', array('placeholder' => 'Email Address', 'label' => 'Email Address')) ?>
      <?= $this->Form->input('User.password', array('placeholder' => 'Password')) ?>
      <?= $this->Form->input('User.confirm_password', array('type' => 'password', 'placeholder' => 'Retype Password')) ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')) ?>
  <?= $this->Form->end() ?>
</div>

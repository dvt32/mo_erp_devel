<div class="users index">
	<h2><?= __('Users') ?></h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('username') ?></th>
			<th><?= $this->Paginator->sort('role') ?></th>
			<th><?= $this->Paginator->sort('created') ?></th>
			<th><?= $this->Paginator->sort('modified') ?></th>
			<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?= $this->Html->link($user['User']['username'], array('action' => 'view', $user['User']['id'])) ?></td>
		<td><?= h($user['User']['role']) ?>&nbsp;</td>
		<td><?= h($user['User']['created']) ?>&nbsp;</td>
		<td><?= h($user['User']['modified']) ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])) ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])) ?>
		</td>
	</tr>
<?php endforeach ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New User'), array('action' => 'add')) ?></li>
		<li><?= $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')) ?> </li>
	</ul>
</div>

<div class="users form">
<?= $this->Form->create('User') ?>
	<fieldset>
		<legend><?= __('Edit User Settings') ?></legend>
		<?= $this->Form->input('username') ?>
	</fieldset>
	<?= $this->Form->button(__('Save'), array("class" => "btn btn-primary")) ?>
<?= $this->Form->end() ?>
</div>
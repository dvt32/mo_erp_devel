<div class="row-fluid">
  <?= $this->Form->create('User', array('class' => 'form-signin', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'input-block-level') )) ?>
    <div class="centered">
      <img src="{{ bizcenter_2g_assetprefix }}/img/logo1.jpg">
    </div>
    <h3>Welcome Back</h3>
    <?= $this->Session->flash() ?>
    <?= $this->Form->input('username', array('placeholder' => __('Email Address'), 'autofocus' => '1')) ?>
    <?= $this->Form->input('password', array('placeholder' => __('Password'))) ?>
    <?= $this->Form->button('Sign In', array('class' => 'btn btn-large btn-block btn-primary', 'id' => 'login-button')) ?>
    <div class="text-center"> or </div>
    <a href="{{ bizcenter_2g_urlprefix }}/auth/linkedin" id="linkedin-button" class="btn btn-large btn-block btn-info" type="submit"> Sign In with LinkedIn </a>
    <br />
    <div>
      <div>
        Forgot your password?
        <?= $this->Html->link('Recover Password', array('controller' => 'tokens', 'action' => 'request', 'resetPassword')) ?>
      </div>
    </div>
  </form>
</div>

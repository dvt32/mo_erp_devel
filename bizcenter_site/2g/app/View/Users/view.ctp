<div class="users view">
<h2><?= __('User') ?></h2>
	<dl>
		<dt><?= __('Id') ?></dt>
		<dd><?= h($user['User']['id']) ?>&nbsp;</dd>
		<dt><?= __('Password') ?></dt>
		<dd><?= h($user['User']['password']) ?>&nbsp;</dd>
		<dt><?= __('Role') ?></dt>
		<dd><?= h($user['User']['role']) ?>&nbsp;</dd>
		<dt><?= __('Username') ?></dt>
		<dd><?= h($user['User']['username']) ?>&nbsp;</dd>
		<dt><?= __('Created') ?></dt>
		<dd><?= h($user['User']['created']) ?>&nbsp;</dd>
		<dt><?= __('Modified') ?></dt>
		<dd><?= h($user['User']['modified']) ?>&nbsp;</dd>
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul>
		<li><?= $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])) ?> </li>
		<li><?= $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])) ?> </li>
		<li><?= $this->Html->link(__('List Users'), array('action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New User'), array('action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')) ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?= __('Related Proximity Cards') ?></h3>
	<? if (!empty($user['ProximityCard'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?= __('Id') ?></th>
		<th><?= __('Number') ?></th>
		<th><?= __('Type') ?></th>
		<th><?= __('Status') ?></th>
		<th><?= __('Customer Account Id') ?></th>
		<th><?= __('Customer Id') ?></th>
		<th><?= __('Note') ?></th>
		<th><?= __('Ms Access Creation Dt') ?></th>
		<th><?= __('Ms Access Random Num') ?></th>
		<th><?= __('Created') ?></th>
		<th><?= __('Access Level Id') ?></th>
		<th><?= __('Floor Id') ?></th>
		<th><?= __('Technology') ?></th>
		<th><?= __('Site Code') ?></th>
		<th class="actions"><?= __('Actions') ?></th>
	</tr>
	<? foreach ($user['ProximityCard'] as $proximityCard): ?>
		<tr>
			<td><?= $proximityCard['id'] ?></td>
			<td><?= $proximityCard['number'] ?></td>
			<td><?= $proximityCard['type'] ?></td>
			<td><?= $proximityCard['status'] ?></td>
			<td><?= $proximityCard['customer_account_id'] ?></td>
			<td><?= $proximityCard['customer_id'] ?></td>
			<td><?= $proximityCard['note'] ?></td>
			<td><?= $proximityCard['ms_access_creation_dt'] ?></td>
			<td><?= $proximityCard['ms_access_random_num'] ?></td>
			<td><?= $proximityCard['created'] ?></td>
			<td><?= $proximityCard['access_level_id'] ?></td>
			<td><?= $proximityCard['floor_id'] ?></td>
			<td><?= $proximityCard['technology'] ?></td>
			<td><?= $proximityCard['site_code'] ?></td>
			<td class="actions">
				<?= $this->Html->link(__('View'), array('controller' => 'proximity_cards', 'action' => 'view', $proximityCard['id'])) ?>
				<?= $this->Html->link(__('Edit'), array('controller' => 'proximity_cards', 'action' => 'edit', $proximityCard['id'])) ?>
				<?= $this->Form->postLink(__('Delete'), array('controller' => 'proximity_cards', 'action' => 'delete', $proximityCard['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['id'])) ?>
			</td>
		</tr>
	<? endforeach ?>
	</table>
<? endif ?>

	<div class="actions">
		<ul>
			<li><?= $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')) ?> </li>
		</ul>
	</div>
</div>

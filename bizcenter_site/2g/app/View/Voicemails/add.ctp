<div class="voicemails form">
<?php echo $this->Form->create('Voicemail'); ?>
	<fieldset>
		<legend><?php echo __('Add Voicemail'); ?></legend>
	<?php
		echo $this->Form->input('created_epoch');
		echo $this->Form->input('read_epoch');
		echo $this->Form->input('username');
		echo $this->Form->input('domain');
		echo $this->Form->input('cid_name');
		echo $this->Form->input('cid_number');
		echo $this->Form->input('in_folder');
		echo $this->Form->input('file_path');
		echo $this->Form->input('message_len');
		echo $this->Form->input('flags');
		echo $this->Form->input('read_flags');
		echo $this->Form->input('forwarded_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Voicemails'), array('action' => 'index')); ?></li>
	</ul>
</div>

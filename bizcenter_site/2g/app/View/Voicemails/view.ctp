<div class="voicemails view">
<h2><?php echo __('Voicemail'); ?></h2>
	<dl>
		<dt><?php echo __('Created Epoch'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['created_epoch']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Read Epoch'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['read_epoch']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Domain'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['domain']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Uuid'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['uuid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cid Name'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['cid_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cid Number'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['cid_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('In Folder'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['in_folder']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Path'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['file_path']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Message Len'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['message_len']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Flags'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['flags']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Read Flags'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['read_flags']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Forwarded By'); ?></dt>
		<dd>
			<?php echo h($voicemail['Voicemail']['forwarded_by']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Voicemail'), array('action' => 'edit', $voicemail['Voicemail']['uuid'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Voicemail'), array('action' => 'delete', $voicemail['Voicemail']['uuid']), null, __('Are you sure you want to delete # %s?', $voicemail['Voicemail']['uuid'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Voicemails'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Voicemail'), array('action' => 'add')); ?> </li>
	</ul>
</div>

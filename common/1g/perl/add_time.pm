use strict;
local($^W) = 1;

use days_in_month;

# (year, month, day, hour, minute, second, amount, unit).
# year is four digit year (e.g. 2005).
# month is 1 to 12.
# day is 1 to 31.
# hour is 0 to 23.
# minute is 0 to 59.
# second is 0 to 59.
# amount is an integer.
# unit is "day", "hour", or "minute".
sub add_time($$$$$$$$) {
  my ($year, $month, $day, $hour, $minute, $second, $amount, $unit) = @_;
  if ($amount > 0) {
    if ($unit eq "minute") {
      $minute += $amount;
      if ($minute > 59) {
	$amount = int($minute/60);
	$unit = "hour";
	$minute %= 60;
      }
    }
    if ($unit eq "hour") {
      $hour += $amount;
      if ($hour > 23) {
	$amount = int($hour/24);
	$unit = "day";
	$hour %= 24;
      }
    }
    if ($unit eq "day") {
      $day += $amount;
      my $days_in_month;
      $days_in_month = days_in_month($month, $year);
      while($day > $days_in_month) {
	$day -= $days_in_month;
	if (++$month > 12) {
	  $year++;
	  $month = 1;
	}
	$days_in_month = days_in_month($month, $year);
      }
    }
  } elsif ($amount < 0) {
    $amount = -$amount;
    if ($unit eq "minute") {
      $minute -= $amount;
      if ($minute < 0) {
	$amount = int(-$minute/60) + 1;
	$unit = "hour";
	$minute %= 60;
      }
    }
    if ($unit eq "hour") {
      $hour -= $amount;
      if ($hour < 0) {
	$amount = int(-$hour/24) + 1;
	$unit = "day";
	$hour %= 24;
      }
    }
    if ($unit eq "day") {
      $day -= $amount;
      while($day < 1) {
	if (--$month < 1) {
	  $year--;
	  $month = 12;
	}
	$day += days_in_month($month, $year);
      }
    }
  }
  return ($year, $month, $day, $hour, $minute, $second);
}

1;

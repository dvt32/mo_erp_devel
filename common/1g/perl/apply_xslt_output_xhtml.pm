use strict;
local($^W) = 1;

use get_xml_parser;
use get_xslt_processor;

sub apply_xslt_output_xhtml($$$;$) {
  my ($xml_input_stream, $xslt_file, $cgi, $mime_type) = @_;
  my $xml_parser = get_xml_parser();
  my $xslt_processor = get_xslt_processor();
  my $xml_document = $xml_parser->parse_string($xml_input_stream);
  my $xslt_document = $xml_parser->parse_file($xslt_file);
  my $stylesheet = $xslt_processor->parse_stylesheet($xslt_document);
  my $transformed_document = $stylesheet->transform($xml_document);
  my $serialized_output = $stylesheet->output_string($transformed_document);
  $mime_type ||= "text/html; charset=utf-8";
  print $cgi->header("-type" => $mime_type);
  print $serialized_output;
}

1;

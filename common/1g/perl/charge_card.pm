use strict;
local($^W) = 1;

use lpperl;
use get_state_map;
use get_database_handle;
use get_row_by_id_map;

# transaction_type: optional; defaults to SALE; can be SALE, PREAUTH, POSTAUTH, VOID, CREDIT, CALCSHIPPING, or CALCTAX.
# transaction_result: optional; defaults to LIVE; can be LIVE, GOOD, DECLINE, or DUPLICATE.
# order_id: optional; must be unique; if not set, gateway will assign one.
# purchase_order_num: optional, but required for purchasing card transactions.
# client_ip_address: optional.
# subtotal: optional; xxx.xx.
# tax: optional; xxx.xx.
# shipping_charge: optional; xxx.xx.
# total_charge: required; xxx.xx or xxx.
# card_number: credit card number (with or without spaces and dashes).
# card_expiration_month: two digit month (must be padded).
# card_expiration_year: two or four digit year (must be padded); if two digit, assumed to be 20XX.
# card_security_code: optional; three or four digit code on the back of the card.
# billing_name: customer's name on the payment account.
# billing_address_num: optional; numeric portion of the customer's billing street address.
# billing_zip_code: billing zip or postal code.
# billing_company: optional.
# billing_address_1: optional.
# billing_address_2: optional.
# billing_city: optional.
# billing_state: optional; state, province, or territory. If state, can be either the full state name or the two letter state code (postal code).
# billing_country: optional; can be either full country name or two letter country code.
# billing_phone_number: optional.
# billing_fax_number: optional.
# billing_email_address: optional.
# comments: optional.
# marketing_source: optional.
# --- following two fields do not seem to propagate:
# invoice_num: optional; invoice number of this order; not sure if this field actually works.
# account_num: optional; account number of the customer; not sure if this field actually works.
sub charge_card($%) {
  my ($instance_map, %param_map) = @_;
  my $linkpoint = new LPPERL();
  my %order_map =
    (
     # --- curl command line arguments
     "cargs"        => "-m 300 -s -S --cacert /etc/pki/tls/certs/curl-ca-bundle.crt --insecure",
     # --- connection details.
     "host"         => "secure.linkpt.net",
     "port"         => "1129",
     # keyfile: change this to the name and location of your certificate file.
     "keyfile"      => "../resource/1001232429.pem",
     # configfile: change this to your store number.
     "configfile"   => "1001232429",

     # --- transaction details.
     # transactionorigin: for credit card retail txns, set to RETAIL, for Mail order/telephone order, set to MOTO, for e-commerce, leave out or set to ECI.
     "transactionorigin" => "ECI",
     # taxexempt: required for Level 2 purchasing card transactions only.
     "taxexempt"    => "N",
     # terminaltype: set terminaltype to POS for an electronic cash register or integrated POS system, STANDALONE for a point-of-sale credit card terminal,
     # UNATTENDED for a self-service station, or UNSPECIFIED for e-commerce or other applications.
     "terminaltype" => "UNSPECIFIED",

     # --- payment/price details.
     # tax: required for Level 2 purchasing card transactions only. If tax is $0.00 for a purchasing card transaction, set this field to 0.
     "tax"          => "0.00",
     );
  # transaction_type: The type of transaction. The possible values are SALE, PREAUTH (for an Authorize Only transaction), POSTAUTH (for a Forced Ticket or
  # Ticket Only transaction), VOID, CREDIT, CALCSHIPPING (for shipping charges calculations) and CALCTAX (for sales tax calculations).
  if ($param_map{"transaction_type"}) {
    my $transaction_type = uc($param_map{"transaction_type"});
    if (grep {$transaction_type eq $_} (qw(SALE PREAUTH POSTAUTH VOID CREDIT))) {
      $order_map{"ordertype"} = $transaction_type;
    } else {
      die "invalid transaction_type: $transaction_type";
    }
  } else {
    # default to SALE.
    $order_map{"ordertype"} = "SALE";
  }
  # transaction_result: LIVE tells gateway to actually put through the operation (a live transaction). GOOD, DECLINE, DUPLICATE are for test transactions.
  if ($param_map{"transaction_result"}) {
    my $transaction_result = uc($param_map{"transaction_result"});
    if (grep {$transaction_result eq $_} (qw(LIVE GOOD DECLINE DUPLICATE))) {
      $order_map{"result"} = $transaction_result;
    } else {
      die "invalid transaction_result: $transaction_result";
    }
  } else {
    # default to LIVE.
    $order_map{"result"} = "LIVE";
  }
  # order_id: Order ID number must be unique. If not set, gateway will assign one. e.g. 12345678-A345.
  if ($param_map{"order_id"}) {
    $order_map{"oid"} = $param_map{"order_id"};
  }
  # invoice_num: optional; invoice number of this order; e.g. 134.
#  if ($param_map{"invoice_num"}) {
#    $order_map{"invoicenumber"} = $param_map{"invoice_num"};
#  }
  # purchase_order_num: Required for Level 2 purchasing card transactions. e.g. 09876543-Q1234.
  if ($param_map{"purchase_order_num"}) {
    $order_map{"ponumber"} = $param_map{"purchase_order_num"};
  }
  # account_num: optional; account number of the customer; e.g. 105.
#  if ($param_map{"account_num"}) {
#    $order_map{"custid"} = $param_map{"account_num"};
#  }
  # client_ip_address: e.g. 140.247.21.21.
  if ($param_map{"client_ip_address"}) {
    my $ip_address = $param_map{"client_ip_address"};
    if ($ip_address =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) {
      $order_map{"ip"} = $ip_address;
    } else {
      die "bad client_ip_address: $ip_address";
    }
  }
  # subtotal: e.g. 152.68.
  if ($param_map{"subtotal"}) {
    if ($param_map{"subtotal"} =~ /^\d+\.\d\d$/) {
      $order_map{"subtotal"} = $param_map{"subtotal"};
    } else {
      die "subtotal must follow format #+.## where # represents a digit";
    }
  }
  # tax: e.g. 152.68.
  if ($param_map{"tax"}) {
    if ($param_map{"tax"} =~ /^\d+\.\d\d$/) {
      $order_map{"tax"} = $param_map{"tax"};
    } else {
      die "tax must follow format #+.## where # represents a digit";
    }
  }
  # shipping_charge: e.g. 152.68.
  if ($param_map{"shipping_charge"}) {
    if ($param_map{"shipping_charge"} =~ /^\d+\.\d\d$/) {
      $order_map{"shipping"} = $param_map{"shipping_charge"};
    } else {
      die "shipping_charge must follow format #+.## where # represents a digit";
    }
  }
  warn "DEBUG1: total_charge: ".$param_map{"total_charge"};
  # total_charge: e.g. 152.68.
  if ($param_map{"total_charge"}) {
    if ($param_map{"total_charge"} =~ /^\d+\.\d\d$/) {
      $order_map{"chargetotal"} = $param_map{"total_charge"};
    } elsif ($param_map{"total_charge"} =~ /^\d+$/) {
      $order_map{"chargetotal"} = $param_map{"total_charge"} . ".00";
    } else {
      die "total_charge must follow format #+.## or #+ where # represents a digit";
    }
  } else {
    die "must specify total_charge";
  }
  # card_number: credit card number (with or without spaces and dashes).
  # card_expiration_month: two digit month (must be padded).
  # card_expiration_year: two digit year (must be padded).
  if (($param_map{"card_number"}) && ($param_map{"card_expiration_month"}) && ($param_map{"card_expiration_year"})) {
    $order_map{"cardnumber"} = $param_map{"card_number"};
    my $expiration_month = $param_map{"card_expiration_month"};
    if (($expiration_month =~ /^\d\d$/) && ($expiration_month >= 1) && ($expiration_month <= 12)) {
      $order_map{"cardexpmonth"} = $expiration_month;
    } else {
      die "bad card_expiration_month: $expiration_month";
    }
    my $expiration_year = $param_map{"card_expiration_year"};
    if ($expiration_year =~ /^20(\d\d)$/) {
      $expiration_year = $1;
    } elsif ($expiration_year =~ /^\d\d$/) {
      # format is ok, so do nothing.
    } else {
      die "bad card_expiration_year: $expiration_year";
    }
    $order_map{"cardexpyear"} = $expiration_year;
  } else {
    die "must specify card_number, card_expiration_month, and card_expiration_year";
  }
  # card_security_code: optional; three or four digit code on the back of the card.
  # CVM is the three-digit security code usually found on the signature line on the back of the card.
  if ((defined($param_map{"card_security_code"})) && ($param_map{"card_security_code"} ne "")) {
    $order_map{"cvmvalue"} = $param_map{"card_security_code"};
    $order_map{"cvmindicator"} = "provided";
  }
  # cvm_indicator: Indicates whether CVM was supplied and, if not, why. The possible values are "provided", "not_provided", "illegible", "not_present", and
  # "no_imprint". Required if card_security_code specified, but this library uses a default value of "provided" if omitted.
  if ($param_map{"cvm_indicator"}) {
    $order_map{"cvmindicator"} = $param_map{"cvm_indicator"};
  }
  # billing_name: customer's name on the payment account.
  # billing_zip_code: billing zip or postal code.
  if (($param_map{"billing_name"}) && ($param_map{"billing_zip_code"})) {
    $order_map{"name"} = $param_map{"billing_name"};
    $order_map{"zip"} = $param_map{"billing_zip_code"};
  } else {
    die "must specify billing_name and billing_zip_code";
  }
  # billing_address_num: numeric portion of the customer's billing street address.
  if ($param_map{"billing_address_num"}) {
    $order_map{"addrnum"} = $param_map{"billing_address_num"};
  }
  # billing_company: If the company name has an ampersand (&) in it, replace the & with the word "and" or the HTML escape character &amp;.
  if ($param_map{"billing_company"}) {
    my $billing_company = $param_map{"billing_company"};
    $billing_company =~ s/&(?!amp;)/&amp;/gi;
    $order_map{"company"} = $billing_company;
  }
  if ($param_map{"billing_address_1"}) {
    $order_map{"address1"} = $param_map{"billing_address_1"};
  }
  if ($param_map{"billing_address_2"}) {
    $order_map{"address2"} = $param_map{"billing_address_2"};
  }
  if ($param_map{"billing_city"}) {
    $order_map{"city"} = $param_map{"billing_city"};
  }
  # billing_state: for international addresses, enter province or territory. For US states, use the full name or one of the US State Codes.
  if ($param_map{"billing_state"}) {
    my $state_map = get_state_map();
    if (exists($state_map->{$param_map{"billing_state"}})) {
      $order_map{"state"} = $state_map->{$param_map{"billing_state"}};
    } else {
      $order_map{"state"} = $param_map{"billing_state"};
    }
  }
  # billing_country: If passed, can be either the full country name or a valid two character country code.
  if ($param_map{"billing_country"}) {
    my $dbh = get_database_handle($instance_map, "main");
    my (%country_by_id);
    my $sql_query = "select * from country_tbl";
    get_row_by_id_map($dbh, $sql_query, undef, \%country_by_id);
    my %country_iso_2_by_name;
    for my $country (values(%country_by_id)) {
      $country_iso_2_by_name{$country->{"name"}} = $country->{"iso_2"};
    }
    my $billing_country = $param_map{"billing_country"};
    if (length($billing_country) == 2) {
      $order_map{"country"} = $billing_country;
    } elsif (exists($country_iso_2_by_name{$billing_country})) {
      $order_map{"country"} = $country_iso_2_by_name{$billing_country};
    } else {
      die "bad billing_country: $billing_country";
    }
  }
  if ($param_map{"billing_phone_number"}) {
    $order_map{"phone"} = $param_map{"billing_phone_number"};
  }
  if ($param_map{"billing_fax_number"}) {
    $order_map{"fax"} = $param_map{"billing_fax_number"};
  }
  if ($param_map{"billing_email_address"}) {
    $order_map{"email"} = $param_map{"billing_email_address"};
  }
  # comments: comments about order.
  if ($param_map{"comments"}) {
    $order_map{"comments"} = $param_map{"comments"};
  }
  # marketing_source: marketing source of the order.
  if ($param_map{"marketing_source"}) {
    $order_map{"referred"} = $param_map{"marketing_source"};
  }
  my %response_map = $linkpoint->curl_process(\%order_map);
  return %response_map;
}

1;

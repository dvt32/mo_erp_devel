use strict;
local($^W) = 1;

# collapse multiple whitespace characters into one
sub collapse_whitespace {
  my $value = shift;
  $value =~ s/\s+/ /;
  return $value;
}

1;

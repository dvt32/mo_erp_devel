use strict;
local($^W) = 1;

use lead_pad;

# (year, month, day, hour, minute, second).
# year is four digit year (e.g. 2005).
# month is 1 to 12.
# day is 1 to 31.
# hour is 0 to 23.
# minute is 0 to 59.
# second is 0 to 59.
sub components_to_timestamp($$$$$$) {
  my ($year, $month, $day, $hour, $minute, $second) = @_;
  ($year, $month, $day, $hour, $minute, $second) =
    (lead_pad($year, 4), lead_pad($month, 2), lead_pad($day, 2), lead_pad($hour, 2), lead_pad($minute, 2), lead_pad($second, 2));
  my $timestamp = "$year-$month-$day $hour:$minute:$second";
  return $timestamp;
}

1;

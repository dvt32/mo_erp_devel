use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use get_cgi;
use get_id;
use serialize_hash;
use select_row;
use insert_row;
use get_public_id_cookie_name;
use get_database_handle;

sub create_session($$$;$) {
  my ($instance_map, $user_session_key, $user_id, $is_persistent) = @_;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $session_id = get_id($dbh, "session_seq");
  my $session = {};
  $session->{$user_session_key} = $user_id;
  my $payload = serialize_hash($session);
  my $remote_addr = $cgi->remote_addr() || undef;
  my $remote_host = $cgi->remote_host() || undef;
  my (@sql_column_list);
  push(@sql_column_list, ["id", $session_id, SQL_INTEGER]);
  push(@sql_column_list, ["payload", $payload]);
  push(@sql_column_list, ["remote_addr", $remote_addr]);
  push(@sql_column_list, ["remote_host", $remote_host]);
  insert_row($dbh, "session_tbl", \@sql_column_list);
  my $row = select_row($dbh, "session_tbl", $session_id);
  my $public_id = $row->{"public_id"};
  my $public_id_cookie_name = get_public_id_cookie_name();
  my $cookie;
  if ($is_persistent) {
    $cookie = $cgi->cookie
      (
       "-name" => $public_id_cookie_name,
       "-value" => $public_id,
       "-expires" => "+7d", # expire in 7 days.
       "-path" => "/",
       "-domain" => ".microoffice.com",
       "-secure" => 0,
      );
  } else {
    $cookie = $cgi->cookie
      (
       "-name" => $public_id_cookie_name,
       "-value" => $public_id,
       "-path" => "/",
       "-domain" => ".microoffice.com",
       "-secure" => 0,
      );
  }
  return $cookie;
}

1;

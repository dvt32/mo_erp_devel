use strict;
local($^W) = 1;

sub credit_card_type($) {
  my ($card_number) = @_;
  my $card_type;
  if ($card_number =~ /^5[1-5]\d{14}$/) {
    $card_type = "mastercard";
  } elsif (($card_number =~ /^4\d{12}$/) || ($card_number =~ /^4\d{15}$/)) {
    $card_type = "visa";
  } elsif ($card_number =~ /^3[47]\d{13}$/) {
    $card_type = "amex";
  } elsif (($card_number =~ /^30[0-5]\d{11}$/) || ($card_number =~ /^3[68]\d{12}$/)) {
    # diners club/carte blanche
    $card_type = "diners";
  } elsif ($card_number =~ /^6011\d{12}$/) {
    $card_type = "discover";
  } elsif (($card_number =~ /^3\d{15}$/) || ($card_number =~ /^2131\d{11}$/) || ($card_number =~ /^1800\d{11}$/)) {
    $card_type = "jcb";
  }
  return $card_type;
}

1;

use strict;
local($^W) = 1;

use lead_pad;

# see man page on ctime(3).
# ctime has format "Wed Jun 30 21:49:08 1993".
sub ctime_to_timestamp($) {
  my $ctime = shift;
  return undef if (!$ctime);
  my %month_map =
    (
     "Jan" => "01",
     "Feb" => "02",
     "Mar" => "03",
     "Apr" => "04",
     "May" => "05",
     "Jun" => "06",
     "Jul" => "07",
     "Aug" => "08",
     "Sep" => "09",
     "Oct" => "10",
     "Nov" => "11",
     "Dec" => "12",
     );
  $ctime =~ /^(\w{3}) (\w{3}) (\d+) (\d\d):(\d\d):(\d\d) (\d+)$/;
  my $month_name = $2;
  my $month = $month_map{$month_name};
  my $day = lead_pad($3, 2);
  my $hour = $4;
  my $minute = $5;
  my $second = $6;
  my $year = $7;
  my $timestamp = qq{$year-$month-$day $hour:$minute:$second};
  return $timestamp;
}

1;

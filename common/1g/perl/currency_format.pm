use strict;
local($^W) = 1;

sub currency_format($) {
	my $amount = sprintf "%.2f", shift @_;
  # add one comma each time through the do-nothing loop
  1 while $amount =~ s/^(-?\d+)(\d\d\d)/$1,$2/;
  # put the dollar sign in the right place
  $amount =~ s/^(-?)/$1\$/;
	return $amount;
}

1;
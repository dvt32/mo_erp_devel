use strict;
local($^W) = 1;

use days_since_1_bc;

# (month, day, year).
# month is 1 to 12.
# day is 1 to 31.
# year is four digit year (e.g. 2005).
# returns 0 through 6 (0 is Sunday, 6 is Saturday).
sub day_of_week($$$) {
  my ($month, $day, $year) = @_;
  my ($day_of_week, $dec_31_day_of_week);

  $dec_31_day_of_week = 5; # Dec 31, 1BC was Friday
  $day_of_week = (days_since_1_bc($month, $day, $year) + $dec_31_day_of_week) % 7;
  return $day_of_week;
}

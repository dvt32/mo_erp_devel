use strict;
local($^W) = 1;

use is_leap_year;

# (month, day, year).
# month is 1 to 12.
# day is 1 to 31.
# year is four digit year (e.g. 2005).
sub day_of_year($$$) {
  my ($month, $day, $year) = @_;
  # DinM         = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
  my (@day_list) = ( 0, 31, 59, 90,120,151,181,212,243,273,304,334,365);
  my $leap_year_bump = 0;
  if (($month > 2) && (is_leap_year($year))) {
    $leap_year_bump = 1;
  }
  return ($day_list[$month - 1] + $day + $leap_year_bump);
}

1;

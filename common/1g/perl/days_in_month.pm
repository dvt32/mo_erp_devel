use strict;
local($^W) = 1;

use is_leap_year;

# (month, year).
# month is 1 to 12.
# year is four digit year (e.g. 2005).
sub days_in_month {
  my ($month, $year) = @_;
  my (@days_in_month_list) = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
  if (is_leap_year($year)) {
    $days_in_month_list[2] = 29;
  }
  return $days_in_month_list[$month];
}

1;

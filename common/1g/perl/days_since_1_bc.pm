use strict;
local($^W) = 1;

use day_of_year;

# (month, day, year).
# month is 1 to 12.
# day is 1 to 31.
# year is four digit year (e.g. 2005).
sub days_since_1_bc($$$) {
  my ($month, $day, $year) = @_;
  my ($Ny, $N4, $N100, $N400, $day_of_year, $days_since_1_bc);
  my ($cc, $yy);

  $year =~ /(\d{2})(\d{2})/;
  ($cc, $yy) = ($1, $2);

  # Number of full years since Dec 31, 1BC (counting the year 0000).
  $Ny = $year;

  # Number of full 4th years (incl. 0000) since Dec 31, 1BC
  $N4 = ($Ny - 1) / 4 + 1;
  if ($year == 0) {
    $N4 = 0;
  }

  # Number of full 100th years (incl. 0000)
  $N100 = $cc + 1;
  if ($yy == 0) {
    $N100--;
  }
  if ($year == 0) {
    $N100 = 0;
  }

  # Number of full 400th years (incl. 0000)
  $N400 = ($N100 - 1) / 4 + 1;
  if ($year == 0) {
    $N400 = 0;
  }

  $day_of_year = day_of_year($month, $day, $year);
  $days_since_1_bc = $Ny*365 + $N4 - $N100 + $N400 + $day_of_year;

  return $days_since_1_bc;
}

1;

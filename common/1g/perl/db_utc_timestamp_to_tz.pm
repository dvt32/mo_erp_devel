use strict;
local($^W) = 1;

use DateTime::Format::Strptime;

# converts a UTC timestamp in the format YYYY-mm-dd HH:MM:SS
# to a time in the provided time zone.

sub db_utc_timestamp_to_tz($$) {
  my ($timestamp, $time_zone) = @_;

  my $pattern = '%Y-%m-%d %T';
  my $parser = DateTime::Format::Strptime->new(pattern => $pattern,
                                               locale => 'en_US',
                                               time_zone => 'UTC',
                                               on_error => 'croak');

  my $parsed = $parser->parse_datetime($timestamp);
  my $converted = $parsed->clone()->set_time_zone($time_zone);

  return $parser->format_datetime($converted);
}

1;

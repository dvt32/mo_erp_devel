use strict;
local($^W) = 1;

use LWP::UserAgent;
use JSON;

# decrypt a row in the database.
# pass in the table name, row ID, and employee ID of the accessing employee.
sub decrypt_row($$$) {
  my ($tbl_name, $row_id, $employee_id) = @_;
  # check for invalid characters in parameters
  if ($tbl_name !~ /^[\w_]+$/) {
    die "bad table name: $tbl_name";
  }
  if ($row_id !~ /^\d+$/) {
    die "bad row id: $row_id";
  }
  if ($employee_id !~ /^\d+$/) {
    die "bad employee id: $employee_id";
  }
  # retrieve decrypted values of encrypted fields
  my $user_agent = LWP::UserAgent->new();
  my $crypto_url = "{{ crypto_base_url }}/decrypt_row?tbl=$tbl_name&id=$row_id&request_user_id=$employee_id";
  my $response = $user_agent->get($crypto_url);
  my $decrypted_row = JSON::decode_json($response->content());
  return $decrypted_row;
}

1;

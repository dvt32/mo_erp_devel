use strict;
local($^W) = 1;

use lead_pad;
use select_row;
use delete_row;
use get_dav_handle;
use get_database_handle;

# pass in instance_map and file_id.
# throws exception if could not delete.
sub delete_file($$) {
  my ($instance_map, $file_id) = @_;
  my $dbh = get_database_handle($instance_map, "main");
  my ($dav, $base_url, $max_file_id) = get_dav_handle($instance_map, "library");
  if ($file_id !~ /^\d+$/) {
    die "bad file id";
  } elsif (($file_id < 1) || ($file_id > $max_file_id)) {
    die "file id out of range";
  }
  if (select_row($dbh, "file_tbl", $file_id)) {
    delete_row($dbh, "file_tbl", $file_id);
  } else {
    die "file_tbl row not found with id $file_id";
  }
  my $padded_id = lead_pad($file_id, 6);
  if ($padded_id !~ /^(\d\d)(\d\d)(\d\d)$/) {
    die "problem pattern matching file id";
  }
  my $file_url = $base_url . "/$1/$2/$3";
  $dav->delete($file_url) or die "couldn't delete $file_url: " . $dav->message();
}

1;

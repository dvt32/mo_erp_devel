use strict;
local($^W) = 1;

use DBI qw(:sql_types);

sub delete_row($$$) {
  my ($dbh, $table_name, $row_id) = @_;
  my $sth = $dbh->prepare("delete from $table_name where id = ?");
  $sth->bind_param(1, $row_id, SQL_INTEGER);
  my $rv = $sth->execute();
  $sth->finish();
  return $rv;
}

1;

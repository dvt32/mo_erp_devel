use strict;
local($^W) = 1;

use url_decode;

sub deserialize_array($) {
  my $serialized_array = shift;
  if (!$serialized_array) {
    return ();
  }
  return map {url_decode($_)} (split(/\|/, $serialized_array));
}

1;

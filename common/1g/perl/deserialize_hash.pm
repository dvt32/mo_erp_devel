use strict;
local($^W) = 1;

use url_decode;

sub deserialize_hash($) {
  my $serialized_hash = shift;
  my $deserialized_hash = {};
  my @hash_pair_list = split(/\n/, $serialized_hash);
  if ((scalar(@hash_pair_list) > 0) && (!$hash_pair_list[-1])) {
    # if the last pair is empty, pop it off the list.
    pop(@hash_pair_list);
  }
  for my $hash_pair (@hash_pair_list) {
    my ($encoded_key, $encoded_value) = split(/=/, $hash_pair);
    my $key = url_decode($encoded_key);
    my $value = url_decode($encoded_value);
    $deserialized_hash->{$key} = $value;
  }
  return $deserialized_hash;
}

1;

use strict;
local($^W) = 1;

use serialize_array;

sub die_apply_xslt($$) {
  my ($generated_xml, $xslt_file) = @_;
  my $param_string = serialize_array($generated_xml, $xslt_file);
  die "apply_xslt: $param_string\n";
}

1;

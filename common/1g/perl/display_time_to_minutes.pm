use strict;
local($^W) = 1;

# returns undef if display time is in invalid format.
sub display_time_to_minutes($) {
  my $display_time = shift;
  my $minutes_since_midnight;
  if ($display_time =~ /^(\d{1,2}):(\d{1,2})$/) {
    $minutes_since_midnight = $1 * 60 + $2;
  } elsif ($display_time =~ /^(\d{1,2}):(\d{1,2})\s+(am|pm)$/i) {
    my $hour = $1;
    my $minute = $2;
    my $am_pm = $3;
    if ($hour == 12) {
      $hour = 0;
    }
    $minutes_since_midnight = $hour * 60 + $minute;
    if ($am_pm =~ /^pm$/i) {
      $minutes_since_midnight += 12*60;
    }
  }
  return $minutes_since_midnight;
}

1;

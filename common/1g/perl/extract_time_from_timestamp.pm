use strict;
local($^W) = 1;

# returns undef if the format is unrecognized.
sub extract_time_from_timestamp($) {
  my $timestamp = shift;
  my $time;
  if ($timestamp =~ / (\d\d):(\d\d)/) {
    $time = "$1:$2";
  }
  return $time;
}

1;

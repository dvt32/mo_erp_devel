use strict;
local($^W) = 1;

sub generate_random_password(;$) {
  my $password_length = shift;
  if (!$password_length) {
    $password_length = 8 + int(rand(3));
  }
  my $char_pool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  my $pool_length = length($char_pool);
  my $generated_password = "";
  for(my $i=0; $i<$password_length; $i++) {
    my $offset = int(rand($pool_length));
    $generated_password .= substr($char_pool, $offset, 1);
  }
  return $generated_password;
}

1;

use strict;
local($^W) = 1;

use MIME::QuotedPrint qw(encode_qp);
use Encode qw(encode);

# salutation
# first_name
# middle_name
# last_name
# suffix
# organization
# title
# note
# tel_work_voice
# tel_home_voice
# tel_cell_voice
# tel_work_fax
# work_address_1
# work_address_2
# work_city
# work_state
# work_zip_code
# work_country
# home_address_1
# home_address_2
# home_city
# home_state
# home_zip_code
# home_country
# bday (YYYYMMDD)
# email_address
sub generate_vcard(%) {
  my %param_map = @_;
  $param_map{"salutation"} ||= "";
  $param_map{"middle_name"} ||= "";
  $param_map{"suffix"} ||= "";
  my @name_part_list = ();
  push(@name_part_list, $param_map{"first_name"});
  if ($param_map{"middle_name"}) {
    push(@name_part_list, $param_map{"middle_name"});
  }
  push(@name_part_list, $param_map{"last_name"});
  my $vcard_full_name = join(" ", @name_part_list);
  my $vcard_content = "";
  $vcard_content .= <<"---END_BLOCK---;";
BEGIN:VCARD
VERSION:2.1
N:$param_map{"last_name"};$param_map{"first_name"};$param_map{"middle_name"};$param_map{"salutation"};$param_map{"suffix"}
FN:$vcard_full_name
---END_BLOCK---;
  if ($param_map{"organization"}) {
    $vcard_content .= <<"---END_BLOCK---;";
ORG:$param_map{"organization"}
---END_BLOCK---;
  }
  if ($param_map{"title"}) {
    $vcard_content .= <<"---END_BLOCK---;";
TITLE:$param_map{"title"}
---END_BLOCK---;
  }
  if ($param_map{"note"}) {
    my $encoded_note = encode_qp(encode("UTF-8", $param_map{"note"}), "\n", 1);
    $encoded_note =~ s/=?\n*$//gis;
    $vcard_content .= <<"---END_BLOCK---;";
NOTE;ENCODING=QUOTED-PRINTABLE:$encoded_note
---END_BLOCK---;
  }
  if ($param_map{"tel_work_voice"}) {
    $vcard_content .= <<"---END_BLOCK---;";
TEL;WORK;VOICE:$param_map{"tel_work_voice"}
---END_BLOCK---;
  }
  if ($param_map{"tel_home_voice"}) {
    $vcard_content .= <<"---END_BLOCK---;";
TEL;HOME;VOICE:$param_map{"tel_home_voice"}
---END_BLOCK---;
  }
  if ($param_map{"tel_cell_voice"}) {
    $vcard_content .= <<"---END_BLOCK---;";
TEL;CELL;VOICE:$param_map{"tel_cell_voice"}
---END_BLOCK---;
  }
  if ($param_map{"tel_work_fax"}) {
    $vcard_content .= <<"---END_BLOCK---;";
TEL;WORK;FAX:$param_map{"tel_work_fax"}
---END_BLOCK---;
  }
  if ($param_map{"work_address_1"} || $param_map{"work_city"}) {
    my $address = ";;";
    $address .= $param_map{"work_address_1"} || "";
    if ($param_map{"work_address_2"}) {
      $address .= "\n";
      $address .= $param_map{"work_address_2"};
    }
    $address .= ";";
    $address .= $param_map{"work_city"};
    $address .= ";";
    $address .= $param_map{"work_state"};
    $address .= ";";
    $address .= $param_map{"work_zip_code"};
    $address .= ";";
    if ($param_map{"work_country"}) {
      $address .= $param_map{"work_country"};
    }
    my $encoded_address = encode_qp(encode("UTF-8", $address), "\n", 1);
    $encoded_address =~ s/=?\n*$//gis;
    my $label_address = "";
    $label_address .= $param_map{"work_address_1"} || "";
    if ($param_map{"work_address_2"}) {
      $label_address .= "\n";
      $label_address .= $param_map{"work_address_2"} || "";
    }
    $label_address .= "\n";
    $label_address .= $param_map{"work_city"};
    $label_address .= ", ";
    $label_address .= $param_map{"work_state"};
    $label_address .= " ";
    $label_address .= $param_map{"work_zip_code"};
    if ($param_map{"work_country"}) {
      $label_address .= "\n";
      $label_address .= $param_map{"work_country"} || "";
    }
    my $encoded_label_address = encode_qp(encode("UTF-8", $label_address), "\n", 1);
    $encoded_label_address =~ s/=?\n*$//gis;
    $vcard_content .= <<"---END_BLOCK---;";
ADR;WORK;ENCODING=QUOTED-PRINTABLE:$encoded_address
LABEL;WORK;ENCODING=QUOTED-PRINTABLE:$encoded_label_address
---END_BLOCK---;
  }
  if ($param_map{"home_address_1"} || $param_map{"home_city"}) {
    my $address = ";;";
    $address .= $param_map{"home_address_1"} || "";
    if ($param_map{"home_address_2"}) {
      $address .= "\n";
      $address .= $param_map{"home_address_2"};
    }
    $address .= ";";
    $address .= $param_map{"home_city"};
    $address .= ";";
    $address .= $param_map{"home_state"};
    $address .= ";";
    $address .= $param_map{"home_zip_code"};
    $address .= ";";
    if ($param_map{"home_country"}) {
      $address .= $param_map{"home_country"};
    }
    my $encoded_address = encode_qp(encode("UTF-8", $address), "\n", 1);
    $encoded_address =~ s/=?\n*$//gis;
    my $label_address = "";
    $label_address .= $param_map{"home_address_1"} || "";
    if ($param_map{"home_address_2"}) {
      $label_address .= "\n";
      $label_address .= $param_map{"home_address_2"} || "";
    }
    $label_address .= "\n";
    $label_address .= $param_map{"home_city"};
    $label_address .= ", ";
    $label_address .= $param_map{"home_state"};
    $label_address .= " ";
    $label_address .= $param_map{"home_zip_code"};
    if ($param_map{"home_country"}) {
      $label_address .= "\n";
      $label_address .= $param_map{"home_country"} || "";
    }
    my $encoded_label_address = encode_qp(encode("UTF-8", $label_address), "\n", 1);
    $encoded_label_address =~ s/=?\n*$//gis;
    $vcard_content .= <<"---END_BLOCK---;";
ADR;HOME;ENCODING=QUOTED-PRINTABLE:$encoded_address
LABEL;HOME;ENCODING=QUOTED-PRINTABLE:$encoded_label_address
---END_BLOCK---;
  }
  if ($param_map{"bday"}) {
    $vcard_content .= <<"---END_BLOCK---;";
BDAY:$param_map{"bday"}
---END_BLOCK---;
  }
  if ($param_map{"email_address"}) {
    $vcard_content .= <<"---END_BLOCK---;";
EMAIL;PREF;INTERNET:$param_map{"email_address"}
---END_BLOCK---;
  }
  $vcard_content .= <<"---END_BLOCK---;";
END:VCARD
---END_BLOCK---;
#  $vcard_content =~ s/\n/\r\n/gis; # PINE needs CRLF line endings
  return $vcard_content;
}

1;

use strict;
local($^W) = 1;

use HTML::Entities ();
use get_month_name;
use day_of_week;
use days_in_month;
use lead_pad;

# month_number = 1 to 12.
sub generate_xml_for_calendar_month($$) {
  my ($year, $month_number) = @_;
  my $padded_month_number = lead_pad($month_number, 2);
  my $month_name = HTML::Entities::encode(get_month_name($month_number));
  my $first_day_of_week = day_of_week($month_number, 1, $year);
  my $num_days = days_in_month($month_number, $year);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<calendar-month>
  <month-number>$month_number</month-number>
  <padded-month-number>$padded_month_number</padded-month-number>
  <month-name>$month_name</month-name>
  <year>$year</year>
  <week-list>
    <week>
---END_BLOCK---;
  if ($first_day_of_week != 0) {
    for(my $i = 0; $i < $first_day_of_week; $i++) {
      $generated_xml .= <<"---END_BLOCK---;";
      <day-blank-cell/>
---END_BLOCK---;
    }
  }
  my $day_of_week = $first_day_of_week;
  for(my $i = 1; $i <= $num_days; $i++) {
    if ($day_of_week == 7) {
      $generated_xml .= <<"---END_BLOCK---;";
    </week>
    <week>
---END_BLOCK---;
      $day_of_week = 0;
    }
    my $day_number = $i;
    my $padded_day_number = lead_pad($day_number, 2);
    $generated_xml .= <<"---END_BLOCK---;";
      <day>
        <day-number>$day_number</day-number>
        <padded-day-number>$padded_day_number</padded-day-number>
      </day>
---END_BLOCK---;
    $day_of_week++;
  }
  if ($day_of_week != 7) {
    for(my $i = $day_of_week; $i < 7; $i++) {
      $generated_xml .= <<"---END_BLOCK---;";
      <day-blank-cell/>
---END_BLOCK---;
    }
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </week>
  </week-list>
</calendar-month>
---END_BLOCK---;
  return $generated_xml;
}

1;

use strict;
local($^W) = 1;

use select_row;

sub get_building_id_from_floor_id($$) {
  my $dbh = shift;
  my $floor_id = shift;
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  my $building_id;
  if ($floor) {
    $building_id = $floor->{"building_id"};
  }
  return $building_id;
}

1;

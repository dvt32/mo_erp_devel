use strict;
local($^W) = 1;

sub get_building_id_given_ip_address($) {
  my $ip_address = shift;
  my $building_id = undef;
  my %building_id_by_ip_address =
    ("162.208.108.1" => 2,
     "162.208.109.1" => 3,
     "50.74.218.194" => 3,
    );
  if (exists($building_id_by_ip_address{$ip_address})) {
    $building_id = int($building_id_by_ip_address{$ip_address});
  }
  return $building_id;
}

1;

use strict;
local($^W) = 1;

use CGI;

sub get_cgi($) {
  my $instance_map = shift;
  if (!$instance_map->{"cgi"}) {
    $instance_map->{"cgi"} = CGI->new();
  }
  return $instance_map->{"cgi"};
}

1;

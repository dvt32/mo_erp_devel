use strict;
local($^W) = 1;

sub get_cgi_input_charset($) {
  my $instance_map = shift;
  return "UTF-8";
}

1;

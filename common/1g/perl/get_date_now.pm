use strict;
local($^W) = 1;

use lead_pad;

# $pad = pad date components to 2 digits each (true or false);
sub get_date_now(;$) {
  my $pad = shift;
  # determine the date and time now.
  my ($year, $month, $day) = (localtime(time()))[5, 4, 3];
  $year += 1900;
  $month++;
  if ($pad) {
    $month = lead_pad($month, 2);
    $day = lead_pad($day, 2);
  }
  return ($year, $month, $day);
}

1;

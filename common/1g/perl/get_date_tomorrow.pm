use strict;
local($^W) = 1;

use lead_pad;
use days_in_month;

# $pad = pad date components to 2 digits each (true or false);
sub get_date_tomorrow(;$) {
  my $pad = shift;
  # determine the date and time now.
  my ($year, $month, $day) = (localtime(time()))[5, 4, 3];
  $year += 1900;
  $month++;
  # now calculate tomorrow.
  $day++;
  if ($day > days_in_month($month, $year)) {
    $day = 1;
    $month++;
    if ($month > 12) {
      $month = 1;
      $year++;
    }
  }
  if ($pad) {
    $month = lead_pad($month, 2);
    $day = lead_pad($day, 2);
  }
  return ($year, $month, $day);
}

1;

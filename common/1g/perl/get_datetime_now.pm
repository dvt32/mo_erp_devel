use strict;
local($^W) = 1;

use lead_pad;

# $pad = pad timestamp components to 2 digits each. Can be "all" or just "time".
sub get_datetime_now(;$) {
  my $pad = shift;
  # determine the date and time now.
  my ($year, $month, $day, $hour, $minute, $second) = (localtime(time()))[5, 4, 3, 2, 1, 0];
  $year += 1900;
  $month++;
  if (($pad) && ($pad eq "all")) {
    $month = lead_pad($month, 2);
    $day = lead_pad($day, 2);
  }
  if (($pad) && (($pad eq "all") || ($pad eq "time"))) {
    $hour = lead_pad($hour, 2);
    $minute = lead_pad($minute, 2);
    $second = lead_pad($second, 2);
  }
  return ($year, $month, $day, $hour, $minute, $second);
}

1;

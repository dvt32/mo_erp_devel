use strict;
local($^W) = 1;

# day_number is 0 to 6, where 0 = Sunday, 1 = Monday, ..., 6 = Saturday.
sub get_day_name($) {
  my $day_number = shift;
  my @day_name_list = (qw(Sunday Monday Tuesday Wednesday Thursday Friday Saturday));
  my $day_name = $day_name_list[$day_number];
  return $day_name;
}

1;

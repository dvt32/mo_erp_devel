use strict;
local($^W) = 1;

use lead_pad;
use get_dav_handle;

# don't use this function for large files because it reads the file into memory. Instead use output_file, which sends the file
# directly to STDOUT.
# pass in instance map, file id, and a reference to scalar where file data should be stored.
sub get_file($$$) {
  my ($instance_map, $file_id, $file_data_ref) = @_;
  my ($dav, $base_url, $max_file_id) = get_dav_handle($instance_map, "library");
  if ($file_id !~ /^\d+$/) {
    die "bad file id";
  } elsif (($file_id < 1) || ($file_id > $max_file_id)) {
    die "file id out of range";
  }
  my $padded_id = lead_pad($file_id, 6);
  if ($padded_id !~ /^(\d\d)(\d\d)(\d\d)$/) {
    die "problem pattern matching file id";
  }
  my $file_url = $base_url . "/$1/$2/$3";
  $dav->get("-url" => $file_url, "-to" => $file_data_ref) or die "couldn't get $file_url: " . $dav->message();
}

1;

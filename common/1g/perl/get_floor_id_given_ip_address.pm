use strict;
local($^W) = 1;

sub get_floor_id_given_ip_address($) {
  my $ip_address = shift;
  my $floor_id = undef;
  my %floor_id_by_ip_address =
    ("162.208.108.1" => 2,
     "162.208.109.1" => 3,
     "50.74.218.194" => 3,
    );
  if (exists($floor_id_by_ip_address{$ip_address})) {
    $floor_id = int($floor_id_by_ip_address{$ip_address});
  }
  return $floor_id;
}

1;

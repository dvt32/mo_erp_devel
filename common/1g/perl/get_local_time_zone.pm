use strict;
local($^W) = 1;

use POSIX;
use epoch_time_to_local_datetime;

# get the name of the local time zone.
# accepts either a single parameter or 6 parameters.
# if single param, it must be the time in seconds since the epoch.
# if 6 params:
#   year is four digit year (e.g. 2006).
#   month is 1 - 12.
#   day is 1 - 31.
#   hour is 0 - 23.
#   minute is 0 - 59.
#   second is 0 - 59.
# returns time zone (e.g. EST, EDT, ...)
sub get_local_time_zone {
  my @param_list = @_;
  my ($year, $month, $day, $hour, $minute, $second);
  if (scalar(@param_list) == 1) {
    ($year, $month, $day, $hour, $minute, $second) = epoch_time_to_local_datetime($param_list[0]);
  } elsif (scalar(@param_list) == 6) {
    ($year, $month, $day, $hour, $minute, $second) = @param_list;
  } else {
    die "bad number of arguments to get_local_time_zone_name";
  }
  # convert datetime to UNIX values.
  $month--;
  $year -= 1900;
  my $time_zone = POSIX::strftime("%Z", $second, $minute, $hour, $day, $month, $year);
  return $time_zone;
}

1;

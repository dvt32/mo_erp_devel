use strict;
local($^W) = 1;

# month_number is 1 to 12.
sub get_month_name($) {
  my $month_number = shift;
  my @month_name_list = (qw(January February March April May June July August September October November December));
  my $month_name = $month_name_list[$month_number - 1];
  return $month_name;
}

1;

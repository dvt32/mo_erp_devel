use strict;
local($^W) = 1;

use days_in_month;

# month is 1 to 12.
# day is 1 to 31.
sub get_next_date($$$) {
  my ($year, $month, $day) = @_;
  if ($day >= days_in_month($month, $year)) {
    if ($month == 12) {
      $year++;
      $month = 1;
      $day = 1;
    } else {
      $month++;
      $day = 1;
    }
  } else {
    $day++;
  }
  return ($year, $month, $day);
}

1;

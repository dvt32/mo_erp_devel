use strict;
local($^W) = 1;

use days_in_month;

# month is 1 to 12.
# day is 1 to 31.
sub get_previous_date($$$) {
  my ($year, $month, $day) = @_;
  if ($day == 1) {
    if ($month == 1) {
      $year--;
      $month = 12;
      $day = 31;
    } else {
      $month--;
      $day = days_in_month($month, $year);
    }
  } else {
    $day--;
  }
  return ($year, $month, $day);
}

1;

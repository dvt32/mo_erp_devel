use strict;
local($^W) = 1;

use DBD::Pg qw(:pg_types);

# sql_param_list is undef if no params or a ref to an array. Each element of the array is either a scalar with the param value or a ref to an array as follows:
#   (column_value, sql_data_type)
#   column_value is the value of the column.
#   sql_data_type is SQL_INTEGER, SQL_BOOLEAN, or other constant from DBI, or a hash ref with driver-specific options. It can also be the string "pg_bytea".
# row_by_id_map is a ref to a hash.
# id_list is optional. If specified, it is a ref to an array where the id values of the rows are stored in the order they are returned by the database
#   query.
sub get_row_by_id_map($$$$;$) {
  my ($dbh, $sql_query, $sql_param_list, $row_by_id_map, $id_list) = @_;
  my $sth = $dbh->prepare($sql_query);
  my $force_pg_server_prepare = 0;
  if ($sql_param_list) {
    for(my $i = 0; $i < scalar(@$sql_param_list); $i++) {
      my $column_value = $sql_param_list->[$i];
      if (ref($column_value)) {
	my $sql_data_type;
	($column_value, $sql_data_type) = @$column_value;
	if (lc($sql_data_type) eq "pg_bytea") {
	  $sql_data_type = { "pg_type" => DBD::Pg::PG_BYTEA };
	  # bytea requires pg_server_prepare set to true.
	  # if not, data will swamp connection and you will get out of memory errors from the driver.
	  $force_pg_server_prepare = 1;
	}
	$sth->bind_param($i + 1, $column_value, $sql_data_type);
      } else {
	$sth->bind_param($i + 1, $column_value);
      }
    }
  }
  if ($force_pg_server_prepare) {
    $sth->{"pg_server_prepare"} = 1;
  }
  $sth->execute();
  if ($id_list) {
    while(my $row = $sth->fetchrow_hashref()) {
      my $row_id = $row->{"id"};
      $row_by_id_map->{$row_id} = $row;
      push(@$id_list, $row_id);
    }
  } else {
    while(my $row = $sth->fetchrow_hashref()) {
      $row_by_id_map->{$row->{"id"}} = $row;
    }
  }
  $sth->finish();
}

1;

use strict;
local($^W) = 1;

use get_cgi;
use get_public_id_cookie_name;
use deserialize_hash;
use select_row_with_query;
use update_with_statement;
use get_database_handle;

sub get_session($) {
  my $instance_map = shift;
  if (!$instance_map->{"session"}) {
    my $cgi = get_cgi($instance_map);
    my $dbh = get_database_handle($instance_map, "main");
    my $public_id_cookie_name = get_public_id_cookie_name();
    my $public_id = $cgi->cookie($public_id_cookie_name);
    if ($public_id) {
      # sessions can be a maximum of 7 days old before being considered expired.
      my $max_age = "7 days";
      my $sql_query = "select * from session_tbl where public_id = ? and (now() - update_dt < interval '$max_age') and is_active";
      my @sql_param_list = ($public_id);
      if (my $row = select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
	my $sql_statement = "update session_tbl set update_dt = now() where public_id = ?";
	@sql_param_list = ($public_id);
	update_with_statement($dbh, $sql_statement, \@sql_param_list);
	$instance_map->{"session"} = deserialize_hash($row->{"payload"});
      }
    }
  }
  return $instance_map->{"session"};
}

1;

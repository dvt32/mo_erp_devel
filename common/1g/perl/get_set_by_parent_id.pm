use strict;
local($^W) = 1;

use DBI qw(:sql_types);

sub get_set_by_parent_id($$$$) {
  my ($dbh, $table_name, $parent_id, $value_list) = @_;
  my $sth = $dbh->prepare("select * from $table_name where parent_id = ?");
  $sth->bind_param(1, $parent_id, SQL_INTEGER);
  $sth->execute();
  while(my $row = $sth->fetchrow_hashref()) {
    push(@$value_list, $row->{"element_value"});
  }
  $sth->finish();
}

1;

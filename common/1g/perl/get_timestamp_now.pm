use strict;
local($^W) = 1;

use lead_pad;

sub get_timestamp_now() {
  # determine the date and time now.
  my ($year, $month, $day, $hour, $minute, $second) = (localtime(time()))[5, 4, 3, 2, 1, 0];
  $year += 1900;
  $month++;
  $month = lead_pad($month, 2);
  $day = lead_pad($day, 2);
  $hour = lead_pad($hour, 2);
  $minute = lead_pad($minute, 2);
  $second = lead_pad($second, 2);
  my $timestamp = "$year-$month-$day $hour:$minute:$second";
  return $timestamp;
}

1;

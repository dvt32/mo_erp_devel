use strict;
local($^W) = 1;

use XML::LibXML;
use get_web_root_dir;

sub get_xml_parser() {
  my $xml_parser = XML::LibXML->new();
  $xml_parser->no_network(1);
  my $dtd_dir = get_web_root_dir() . "/dtd";
  $xml_parser->load_catalog("file://$dtd_dir/catalog.xml");
  return $xml_parser;
}

1;

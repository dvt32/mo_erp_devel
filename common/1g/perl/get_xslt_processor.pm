use strict;
local($^W) = 1;

use XML::LibXSLT;

sub get_xslt_processor() {
  my $xslt_processor = XML::LibXSLT->new();
  return $xslt_processor;
}

1;

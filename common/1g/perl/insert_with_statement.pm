use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use DBD::Pg qw(:pg_types);

# sql_param_list is a ref to an array of elements. Each element is either a column value or a ref to an array like so:
#   (column_value, sql_data_type)
#   column_value is the value.
#   sql_data_type is SQL_INTEGER, SQL_BOOLEAN, or other constant from DBI, or a hash ref with driver-specific options. It can also be the string "pg_bytea".
sub insert_with_statement($$$) {
  my ($dbh, $sql_statement, $sql_param_list) = @_;
  my $sth = $dbh->prepare($sql_statement);
  my $force_pg_server_prepare = 0;
  for(my $i = 0; $i < scalar(@$sql_param_list); $i++) {
    my ($column_value, $sql_data_type);
    if (ref($sql_param_list->[$i])) {
      ($column_value, $sql_data_type) = @{$sql_param_list->[$i]};
    } else {
      $column_value = $sql_param_list->[$i];
    }
    if ($sql_data_type) {
      if (!ref($sql_data_type)) {
	if ($sql_data_type =~ /^pg_/i) {
	  if (lc($sql_data_type) eq "pg_bytea") {
	    $sql_data_type = { "pg_type" => DBD::Pg::PG_BYTEA };
	    # bytea requires pg_server_prepare set to true.
	    # if not, data will swamp connection and you will get out of memory errors from the driver.
	    $force_pg_server_prepare = 1;
	  }
	}
      }
      $sth->bind_param($i + 1, $column_value, $sql_data_type);
    } else {
      $sth->bind_param($i + 1, $column_value);
    }
  }
  if ($force_pg_server_prepare) {
    $sth->{"pg_server_prepare"} = 1;
  }
  $sth->execute();
  $sth->finish();
}

1;

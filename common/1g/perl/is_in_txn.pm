use strict;
local($^W) = 1;

# pass in dbh. returns 1 | 0 depending on whether inside a database transaction or not. There are currently two methods for
# determining whether a transaction is active. The first is via the AutoCommit property of the database handle. When AutoCommit
# is 1, no transaction is active because DBI commits each statement individually. When AutoCommit is 0, a transaction is
# active. The second method of determining whether a transaction is active is via the ping method of the database handle. When
# ping is 3, a transaction is active. When ping is 1, there is no transaction. Consult DBD::Pg for other values of ping. With
# the ping method, a database handle is not considered to be part of a transaction until the first query or statement is
# executed after a begin_work() call. ping will report no transaction immediately after a begin_work() call but before any
# query or statement has been executed.
sub is_in_txn($;$) {
  my ($dbh, $method) = @_;
  $method ||= 0;
  my $result_val;
  if ($method == 0) {
    if ($dbh->{"AutoCommit"} == 1) {
      $result_val = 0;
    } elsif ($dbh->{"AutoCommit"} == 0) {
      $result_val = 1;
    } else {
      die qq{unknown value for DBI->{"AutoCommit"}: $dbh->{"AutoCommit"}};
    }
  } elsif ($method == 1) {
    my $ping = $dbh->ping();
    if ($ping == 3) {
      $result_val = 1;
    } elsif ($ping == 1) {
      $result_val = 0;
    } else {
      die "unknown value for DBD::Pg->ping(): $ping";
    }
  } else {
    die "unknown method $method in is_in_txn";
  }
  return $result_val;
}

1;

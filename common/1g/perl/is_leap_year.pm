use strict;
local($^W) = 1;

sub is_leap_year($) {
  my ($year) = @_;
  return 0 unless $year % 4 == 0;
  return 1 unless $year % 100 == 0;
  return 0 unless $year % 400 == 0;
  return 1;
}

1;

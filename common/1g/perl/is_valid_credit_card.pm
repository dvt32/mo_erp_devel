use strict;
local($^W) = 1;

sub is_valid_credit_card($) {
  my ($card_number) = @_;
  my $return_value = 0;
  if (
      ($card_number =~ /^5[1-5]\d{14}$/) || # mastercard
      ($card_number =~ /^4\d{12}$/) || # visa
      ($card_number =~ /^4\d{15}$/) || # visa
      ($card_number =~ /^3[47]\d{13}$/) || # amex
      ($card_number =~ /^30[0-5]\d{11}$/) || # diners club/carte blanche
      ($card_number =~ /^3[68]\d{12}$/) || # diners club/carte blanche
      ($card_number =~ /^6011\d{12}$/) || # discover
      ($card_number =~ /^3\d{15}$/) || # jcb
      ($card_number =~ /^2131\d{11}$/) || # jcb
      ($card_number =~ /^1800\d{11}$/) # jcb
     ) {
    my $reversed_card_number = reverse($card_number);
    my $digit_sum = 0;
    for(my $i = 0; $i < length($reversed_card_number); $i++) {
      if ($i % 2 == 0) {
	$digit_sum += substr($reversed_card_number, $i, 1);
      } else {
	my $doubled_digit = substr($reversed_card_number, $i, 1) * 2;
	if ($doubled_digit >= 10) {
	  $doubled_digit = int($doubled_digit/10) + ($doubled_digit % 10);
	}
	$digit_sum += $doubled_digit;
      }
    }
    if ($digit_sum % 10 == 0) {
      $return_value = 1;
    }
  }
  return $return_value;
}

1;

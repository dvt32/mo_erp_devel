use strict;
local($^W) = 1;

use HTML::Entities ();
use get_web_root_dir;
use apply_xslt_output_xhtml;
use get_database_handle;
use deserialize_array;
use get_cgi;
use show_page;

sub main_exception_wrapper($) {
  my $main_ref = shift;
  my %instance_map;
  eval {
    $main_ref->(\%instance_map);
  };
  my $exception = $@;
  if ($exception) {
    # remove trailing newline from exception message.
    chomp($exception);
  }
  my ($cgi, $dbh) = (get_cgi(\%instance_map), get_database_handle(\%instance_map, "main"));
  if (($dbh) && ($dbh->{"Active"})) {
    # if database is still active, disconnect from it.
    eval { $dbh->disconnect(); };
  }
  # if exception, perform appropriate action.
  if ((!$exception) || ($exception eq "return")) {
    # do nothing. just exit because the thrower of exception has presumably already displayed output.
  } elsif (($cgi) && ($exception =~ /^show_page: (.+)$/)) {
    my $filename = $1;
    if (-e $filename) {
      show_page(\%instance_map, $filename);
    } else {
      require Carp;
      Carp::confess("File does not exist for show_page: $filename");
    }
  } elsif ($exception =~ /^apply_xslt: (.+)$/) {
    my ($generated_xml, $xslt_file) = deserialize_array($1);
    apply_xslt_output_xhtml($generated_xml, $xslt_file, $cgi);
  } else {
    # unknown exception, so display error page and then propagate the exception so it shows up in server logs.
    my $error_xslt = get_web_root_dir() . "/error/error.xslt";
    my $encoded_exception = HTML::Entities::encode($exception);
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <exception>$encoded_exception</exception>
</page>
---END_BLOCK---;
    apply_xslt_output_xhtml($generated_xml, $error_xslt, $cgi);
    die $exception;
  }
}

1;

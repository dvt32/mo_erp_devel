use strict;
local($^W) = 1;

use display_time_to_minutes;
use minutes_to_display_time;

sub military_time_to_am_pm($) {
  my $time = shift;
  return minutes_to_display_time(display_time_to_minutes($time), 1);
}

1;

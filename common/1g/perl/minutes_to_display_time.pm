use strict;
local($^W) = 1;

use lead_pad;

sub minutes_to_display_time($;$) {
  my $minutes_since_midnight = shift;
  my $show_am_pm = shift;
  my $hour = int($minutes_since_midnight / 60);
  my $minutes_after_hour = lead_pad($minutes_since_midnight % 60, 2);
  my $display_time;
  if ($show_am_pm) {
    my $am_pm = "AM";
    if ($hour > 11) {
      $hour -= 12;
      $am_pm = "PM";
    }
    if ($hour == 0) {
      $hour = 12;
    }
    $hour = lead_pad($hour, 2);
    $display_time = "$hour:$minutes_after_hour $am_pm";
  } else {
    $hour = lead_pad($hour, 2);
    $display_time = "$hour:$minutes_after_hour";
  }
  return $display_time;
}

1;

use strict;
local($^W) = 1;

# Set up a statement handle for a loop that will insert multiple rows.
# sql_column_list is a ref to an array of refs to arrays, each of which looks like so:
#   (column_name, sql_data_type)
#   sql_data_type is SQL_INTEGER, SQL_BOOLEAN, or other constant from DBI, or a hash ref with driver-specific options.
# Example sql_column_list entries:
#   ("room_id", SQL_INTEGER)
#   ("note")
# Returns the statement handle. call $sth->execute(@param_list) repeatedly, then $sth->finish() when done.
sub multi_insert_sth($$$) {
  my ($dbh, $table_name, $sql_column_list) = @_;
  my @column_name_list = map {$_->[0]} @$sql_column_list;
  my @sql_data_type_list = map {$_->[1]} @$sql_column_list;
  my $column_names = join(", ", @column_name_list);
  my $placeholders = join(", ", map {"?"} @column_name_list);
  my $sth = $dbh->prepare("insert into $table_name ($column_names) values ($placeholders)");
  # need true server side prepare or else no performance benefit because DBI would be emulating the prepare. turn off server side prepare if you want to see the values for debugging purposes.
  $sth->{"pg_server_prepare"} = 1;
  for(my $i = 0; $i < scalar(@sql_data_type_list); $i++) {
    if (my $sql_data_type = $sql_data_type_list[$i]) {
      $sth->bind_param($i + 1, undef, $sql_data_type);
    }
  }
  return $sth;
}

1;

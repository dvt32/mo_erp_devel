use strict;
local($^W) = 1;

# strip leading and trailing whitespace and collapse multiple whitespace characters into one
sub normalize_whitespace {
  my $value = shift;
  $value =~ s/^\s+//;
  $value =~ s/\s+$//;
  $value =~ s/\s+/ /;
  return $value;
}

1;

use strict;
local($^W) = 1;

use get_cgi;
use lead_pad;
use select_row;
use get_dav_handle;
use get_database_handle;

# if filename is not specified, defaults to filename stored in file_tbl.
# if mime_type is not specified, defaults to mime_type stored in file_tbl.
# if is_attachment unspecified, defaults to false. is_attachment is useful for "Save As" links whereas no attachment means view
#   in-line in browser. If is_attachment is true, then filename is good to specify (although not mandatory).
sub output_file($$;$$$) {
  my ($instance_map, $file_id, $filename, $mime_type, $is_attachment) = @_;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my ($dav, $base_url, $max_file_id) = get_dav_handle($instance_map, "library");
  if ($file_id !~ /^\d+$/) {
    die "bad file id";
  } elsif (($file_id < 1) || ($file_id > $max_file_id)) {
    die "file id out of range";
  }
  my $padded_id = lead_pad($file_id, 6);
  if ($padded_id !~ /^(\d\d)(\d\d)(\d\d)$/) {
    die "problem pattern matching file id";
  }
  my $file_url = $base_url . "/$1/$2/$3";
  my $file = select_row($dbh, "file_tbl", $file_id) or die "couldn't load file row ($file_id)";
  $filename ||= $file->{"filename"};
  $mime_type ||= $file->{"mime_type"};
  my @dav_option_list = split(/,/, $dav->options($file_url));
  if (!grep {$_ eq "GET"} @dav_option_list) {
    die "couldn't propfind $file_url: " . $dav->message();
  }
  # print out data to stdout with specified MIME type.
  if ($is_attachment) {
    print $cgi->header
      ("-type" => $mime_type,
       "-content-disposition" => qq{attachment; filename="$filename"});
  } else {
    print $cgi->header("-type" => $mime_type);
  }
  $dav->get("-url" => $file_url, "-to" => \*STDOUT, "-callback" => sub {}) or die "couldn't get $file_url: " . $dav->message();
}

1;

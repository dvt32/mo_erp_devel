use strict;
local($^W) = 1;

# accepts string with monetary amount.
# returns dollars (>1 digit) and cents (2 digits). If error, throws exception.
sub parse_dollars_cents($) {
  my ($amount) = @_;
  my ($amount_dollars, $amount_cents);
  $amount =~ s/[,\$]//g;
  $amount =~ s/^\s+//;
  $amount =~ s/\s+$//;
  if ($amount =~ /^(\d+)(\.\d\d)?$/) {
    $amount_dollars = $1;
    $amount_cents = $2;
    if ($amount_cents) {
      $amount_cents =~ s/^\.//;
    } else {
      $amount_cents = "00";
    }
  } else {
    die "bad monetary amount";
  }
  return ($amount_dollars, $amount_cents);
}

1;

use strict;
local($^W) = 1;

use lead_pad;
use is_in_txn;
use insert_row;
use get_dav_handle;
use get_database_handle;

# args: ($dbh, $namespace, $filename, $mime_type, $file_data_ref, $note)
#   make sure $file_data_ref is a reference to a scalar and not just something that mostly appears to be a
#     scalar. ref($file_data_ref) must return "SCALAR" or "SCALAR(addr)". If this is not true, HTTP::DAV->put(...) will barf.
#   $note is optional.
#   returns $file_id.
sub put_file($$$$$;$) {
  my ($instance_map, $namespace, $filename, $mime_type, $file_data_ref, $note) = @_;
  my $dbh = get_database_handle($instance_map, "main");
  my ($dav, $base_url, $max_file_id) = get_dav_handle($instance_map, "library");
  my $is_in_txn = is_in_txn($dbh);
  if (!$is_in_txn) {
    $dbh->begin_work();
  }
  my $file_id;
  eval {
    my @sql_column_list;
    push(@sql_column_list, ["namespace", $namespace]);
    push(@sql_column_list, ["filename", $filename]);
    push(@sql_column_list, ["mime_type", $mime_type]);
    push(@sql_column_list, ["note", $note]) if ($note);
    $file_id = insert_row($dbh, "file_tbl", \@sql_column_list, "file_seq");
    if (($file_id < 1) || ($file_id > $max_file_id)) {
      die "file id out of range";
    }
    my $padded_id = lead_pad($file_id, 6);
    if ($padded_id !~ /^(\d\d)(\d\d)(\d\d)$/) {
      die "problem pattern matching file id";
    }
    my @dir_tree = ($1, $2);
    my $file_url = $base_url . "/$1/$2/$3";
    for my $dir (@dir_tree) {
      if (!$dav->cwd($dir)) {
	$dav->mkcol($dir) or die "couldn't mkcol $dir: " . $dav->message();
	$dav->cwd($dir);
      }
    }
    $dav->put("-local" => $file_data_ref, "-url" => $file_url) or die "couldn't put $file_url: " . $dav->message();
  };
  if ($@) {
    if (!$is_in_txn) {
      $dbh->rollback();
    }
    die $@;
  }
  if (!$is_in_txn) {
    $dbh->commit();
  }
  return $file_id;
}

1;

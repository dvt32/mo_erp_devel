use strict;
local($^W) = 1;

use IO::File;

sub read_file($) {
  my $filename = shift;
  local($/) = undef;
  my $file_handle = IO::File->new();
  $file_handle->open($filename);
  my $file_data = <$file_handle>;
  $file_handle->close();
  return $file_data;
}

1;

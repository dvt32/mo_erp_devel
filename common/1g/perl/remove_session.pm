use strict;
local($^W) = 1;

use get_cgi;
use update_with_statement;
use get_public_id_cookie_name;
use get_database_handle;

sub remove_session($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $public_id_cookie_name = get_public_id_cookie_name();
  my $public_id = $cgi->cookie($public_id_cookie_name);
  my $sql_statement = "update session_tbl set is_active = false where public_id = ?";
  my @sql_param_list = ($public_id);
  update_with_statement($dbh, $sql_statement, \@sql_param_list);
  my $cookie = $cgi->cookie
    ("-name" => $public_id_cookie_name,
     "-value" => "",
     "-path" => "/",
     "-domain" => ".microoffice.com",
     "-expires" => "-1d",
     "-secure" => 1,);
  return $cookie;
}

1;

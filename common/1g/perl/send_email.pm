use strict;
local($^W) = 1;

use Mail::Sendmail qw();

sub send_email(@) {
  my %param_hash = @_;
  my %mail_arg_hash =
    ("From" => $param_hash{"from"},
     "To" => $param_hash{"to"},
     "Message" => $param_hash{"message"},
     "SMTP" => "localhost",
     );
  if ($param_hash{"subject"}) {
    $mail_arg_hash{"Subject"} = $param_hash{"subject"};
  }
  Mail::Sendmail::sendmail(%mail_arg_hash);
}

1;

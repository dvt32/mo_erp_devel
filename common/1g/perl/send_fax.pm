use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use MIME::Base64 ();
use Encode ();
use select_row_with_query;
use insert_row;
use call_faxage_api;

my $default_sending_fax_number = "646-253-1258";
my $default_caller_id = "6462531258";
my $account_id_of_corporate = 1;

# send a fax using FaxAge. parameters:
#   dbh: database handle
#   db_charset: character set of database for internationalization.
#   sending_person: can be from person_vw, cust_pers_tbl, employee_tbl.
#   sending_account_type: customer or corporate.
#   recipient_name: name of the recipient. should be 32 or fewer characters.
#   fax_number: target fax number. should be 10 digits.
#   filename_list: reference to array containing filenames. NOTE: extensions are important! They are used to determine file format.
#   file_content_list: ref to array containing file data. Do not encode it beforehand. This function encodes it in base 64.
#   send_email_alert: 1 | 0. If 1, send email alert to person_vw.email after fax succeeds or fails.
# returns job ID if successful. If unsuccessful, throws exception with error message.
sub send_fax {
  my %param_map = @_;
  my $dbh = $param_map{"dbh"};
  if (!$dbh) {
    die "missing dbh";
  }
  my $db_charset = $param_map{"db_charset"};
  if (!$db_charset) {
    die "missing db_charset";
  }
  my $sending_person = $param_map{"sending_person"};
  my $sending_account_type = $param_map{"sending_account_type"};
  my $account_id = $sending_person->{"account_id"};
  if (!$account_id) {
    if ($sending_account_type eq "customer") {
      $account_id = $sending_person->{"cust_acct_id"};
    } elsif ($sending_account_type eq "corporate") {
      $account_id = $account_id_of_corporate;
    } else {
      die "unknown sending account type: $sending_account_type";
    }
  }
  my $recipient_name = $param_map{"recipient_name"};
  # strip out tabs.
  $recipient_name =~ s/\t//g;
  my $fax_number = $param_map{"fax_number"};
  if ($fax_number !~ /^\d{10}$/) {
    die "bad fax number ($fax_number)";
  }
  my $filename_list = $param_map{"filename_list"};
  my $file_content_list = $param_map{"file_content_list"};
  my $send_email_alert = $param_map{"send_email_alert"};
  # issue request to FaxAge.
  my $sender_name;
  if (defined($sending_person->{"fax_sender_name"})) {
    $sender_name = $sending_person->{"fax_sender_name"};
  } else {
    $sender_name = $sending_person->{"first_name"} . " " . $sending_person->{"last_name"};
  }
  # sending fax number shows up in header and station ID, but not in caller ID (ANI).
  my $sending_fax_number = $sending_person->{"work_fax_number"};
  if (!$sending_fax_number) {
    # if the person has no work_fax_number setting and he has a private fax number, use the private fax number.
    # this will use the first available in the unusual case that the person has multiple private fax numbers.
    my $sql_query = "select * from fax_number_tbl where person_id = ?";
    my @sql_param_list = ([$sending_person->{"id"}, SQL_INTEGER]);
    my $fax_number_row = select_row_with_query($dbh, $sql_query, \@sql_param_list);
    if ($fax_number_row) {
      $sending_fax_number = $fax_number_row->{"fax_number"};
    } else {
      $sending_fax_number = $default_sending_fax_number;
    }
  }
  # Caller ID is the phone number (ANI) that shows up on caller ID of the recipient's fax machine. Should be 10 digits, no
  # punctuation or non-numeric characters. Can have extra 1 (11th digit) as prefix, in which case 1 is stripped off.
  my $caller_id = $sending_fax_number;
  $caller_id =~ s/[^\d]//g;
  if ($caller_id =~ /^1?(\d{10})$/) {
    $caller_id = $1;
  } else {
    $caller_id = $default_caller_id;
  }
  # FaxAge expects the tagnumber to be in the exact format 1.XXX.XXX.XXXX (with the periods and prefix 1)
  my $faxage_tagnumber = $sending_fax_number;
  $faxage_tagnumber =~ s/[^\d]//g;
  if ($faxage_tagnumber =~ /^1?(\d{3})(\d{3})(\d{4})$/) {
    $faxage_tagnumber = "1.$1.$2.$3";
  }
  %param_map =
    (
     "recipname" => $recipient_name,
     "faxno" => $fax_number,
     "operation" => "sendfax",
     "tagname" => $sender_name,
     "tagnumber" => $faxage_tagnumber,
     "callerid" => $caller_id,
     "em_notify" => 1,
    );
  for(my $i = 0; $i < scalar(@$file_content_list); $i++) {
    $param_map{"faxfilenames[$i]"} = $filename_list->[$i];
    $param_map{"faxfiledata[$i]"} = MIME::Base64::encode_base64($file_content_list->[$i]);
  }
  my $response_content = call_faxage_api(%param_map);
  my $job_id;
  if ($response_content =~ /^JOBID: (\d+)$/) {
    $job_id = $1;
    # insert new row.
    my (@sql_column_list);
    push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
    push(@sql_column_list, ["person_id", $sending_person->{"id"}, SQL_INTEGER]);
    push(@sql_column_list, ["job_id", $job_id, SQL_INTEGER]);
    push(@sql_column_list, ["recipient_name", Encode::encode($db_charset, $recipient_name)]);
    push(@sql_column_list, ["fax_number", $fax_number]);
    push(@sql_column_list, ["send_email_alert", $send_email_alert, SQL_BOOLEAN]);
    insert_row($dbh, "fax_out_tbl", \@sql_column_list);
  } else {
    my $error_message = $response_content;
    die "$error_message\n";
  }
  return $job_id;
}

1;

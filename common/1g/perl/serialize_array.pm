use strict;
local($^W) = 1;

use url_encode;

sub serialize_array(@) {
  return join("|", map {url_encode($_)} @_);
}

1;

use strict;
local($^W) = 1;

use url_encode;

sub serialize_hash($) {
  my $hash = shift;
  my $serialized_hash = "";
  for my $key (sort(keys(%$hash))) {
    my $value = $hash->{$key};
    my $encoded_key = url_encode($key);
    my $encoded_value = url_encode($value);
    $serialized_hash .= "$encoded_key=$encoded_value\n";
  }
  return $serialized_hash;
}

1;

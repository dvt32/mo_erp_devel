use strict;
local($^W) = 1;

use IO::File;
use get_cgi;

sub show_page($$;$) {
  my $instance_map = shift;
  my $filename = shift;
  my $cookie = shift;
  if (!-e $filename) {
    die "$filename does not exist";
  }  
  local($/) = undef;
  my $file_handle = IO::File->new();
  my $file_content;
  $file_handle->open($filename);
  $file_content = <$file_handle>;
  $file_handle->close();
  my $cgi = get_cgi($instance_map);
  if ($cookie) {
    print $cgi->header("-type" => "text/html; charset=utf-8", "-cookie" => $cookie,);
  } else {
    print $cgi->header("-type" => "text/html; charset=utf-8");
  }
  print $file_content;
}

1;

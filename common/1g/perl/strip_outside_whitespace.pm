use strict;
local($^W) = 1;

sub strip_outside_whitespace {
  my $value = shift;
  $value =~ s/^\s+//;
  $value =~ s/\s+$//;
  return $value;
}

1;

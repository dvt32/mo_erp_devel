use strict;
local($^W) = 1;

# timestamp has format "yyyy-mm-dd hh:mm:ss"
# strips leading zeroes off all date components.
sub timestamp_to_components($) {
  my $timestamp = shift;
  $timestamp =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/;
  my ($year, $month, $day, $hour, $minute, $second) = (int($1), int($2), int($3), int($4), int($5), int($6));
  return ($year, $month, $day, $hour, $minute, $second);
}

1;

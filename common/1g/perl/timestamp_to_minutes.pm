use strict;
local($^W) = 1;

use display_time_to_minutes;
use extract_time_from_timestamp;

sub timestamp_to_minutes($) {
  my $timestamp = shift;
  my $minutes = display_time_to_minutes(extract_time_from_timestamp($timestamp));
  return $minutes;
}

1;

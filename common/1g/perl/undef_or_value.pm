use strict;
local($^W) = 1;

# if value is defined and is not the empty string or totally white space, return the value.
# otherwise, return undef.
sub undef_or_value($) {
  my $value = shift;
  my $return_value;
  if ((defined($value)) && ($value ne "") && ($value !~ /^\s*$/)) {
    $return_value = $value;
  } else {
    $return_value = undef;
  }
  return $return_value;
}

1;

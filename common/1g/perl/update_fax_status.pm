use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use select_row_with_query;
use update_row;
use call_faxage_api;

# update fax_out_tbl with data from FaxAge. positional parameters:
#   dbh: database handle
#   fax_out_by_job_id: ref to hash of (job id, fax_out_tbl row) pairs. NOTE: key is JOB ID, not fax_out_tbl ID.
# Updates fax_out_tbl in database and in-memory fax_out_by_job_id map if successful. If unsuccessful, throws exception with
#   error message.
sub update_fax_status($$) {
  my ($dbh, $fax_out_by_job_id) = @_;
  my %param_map =
    (
     "operation" => "status",
     "pagecount" => 1,
    );
  my $response_content = call_faxage_api(%param_map);
  if ($response_content =~ /^ERR06/) {
    # no jobs in FaxAge status list. No problem, just move on.
  } elsif ($response_content =~ /^ERR/) {
    # other error. need to display it.
    die $response_content;
  } else {
    # no errors. Response comprised of rows of tab-delimited status records.
    my @status_record_list = split(/\n/, $response_content);
    my $sql_query = "select * from fax_out_tbl where job_id = ?";
    %param_map =
      (
       "operation" => "clear",
      );
    for my $status_record_line (@status_record_list) {
      my ($job_id, $comm_id, $recipient_name, $fax_number, $short_status, $long_status, $send_time, $complete_time, $xmit_time, $page_count) = split(/\t/, $status_record_line);
      my @sql_param_list = ([$job_id, SQL_INTEGER]);
      my $fax_out = select_row_with_query($dbh, $sql_query, \@sql_param_list);
      if (!$fax_out) {
	next;
      }
      $page_count = undef if ($page_count =~ /^\s*$/);
      my @sql_column_list;
      push(@sql_column_list, ["comm_id", $comm_id]);
      push(@sql_column_list, ["short_status", $short_status]);
      push(@sql_column_list, ["long_status", $long_status]);
      push(@sql_column_list, ["send_time", $send_time]);
      if ($short_status ne "pending") {
	push(@sql_column_list, ["complete_time", $complete_time]);
	push(@sql_column_list, ["xmit_time", $xmit_time]);
	push(@sql_column_list, ["page_count", $page_count, SQL_INTEGER]);
      }
      # update the sent fax record in the database using the information from FaxAge.
      update_row($dbh, "fax_out_tbl", \@sql_column_list, $fax_out->{"id"});
      # if not pending, clear the sent fax record from the list on the FaxAge servers.
      if ($short_status ne "pending") {
	$param_map{"jobid"} = $job_id;
	$response_content = call_faxage_api(%param_map);
      }
      if ($fax_out = $fax_out_by_job_id->{$job_id}) {
	# update our in-memory record.
	$fax_out->{"comm_id"} = $comm_id;
	$fax_out->{"short_status"} = $short_status;
	$fax_out->{"long_status"} = $long_status;
	$fax_out->{"send_time"} = $send_time;
	if ($short_status ne "pending") {
	  $fax_out->{"complete_time"} = $complete_time;
	  $fax_out->{"xmit_time"} = $xmit_time;
	  $fax_out->{"page_count"} = $page_count;
	}
      }
    }
  }
}

1;

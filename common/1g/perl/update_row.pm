use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use DBD::Pg qw(:pg_types);

# sql_column_list is a ref to an array that looks like so:
#   (column_name, column_value, sql_data_type)
#   column_value is the value or a ref to an array, first element is literal text to be inserted.
#   sql_data_type is SQL_INTEGER, SQL_BOOLEAN, or other constant from DBI, or a hash ref with driver-specific options. It can also be the string "pg_bytea".
sub update_row($$$$) {
  my ($dbh, $table_name, $sql_column_list, $row_id) = @_;
  my $update_column_assignments = join(", ", map {$_->[0] . " = " . (ref($_->[1]) eq "ARRAY" ? $_->[1]->[0] : "?")} @$sql_column_list);
  my $sth = $dbh->prepare("update $table_name set $update_column_assignments where id = ?");
  my $bind_column_number = 1;
  my $force_pg_server_prepare = 0;
  for(my $column_list_index = 0; $column_list_index < scalar(@$sql_column_list); $column_list_index++) {
    my ($column_name, $column_value, $sql_data_type) = @{$sql_column_list->[$column_list_index]};
    if (ref($column_value) eq "ARRAY") {
      next;
    } else {
      if (defined($sql_data_type)) {
	if (!ref($sql_data_type)) {
	  if ($sql_data_type =~ /^pg_/i) {
	    if (lc($sql_data_type) eq "pg_bytea") {
	      $sql_data_type = { "pg_type" => DBD::Pg::PG_BYTEA };
	      # bytea requires pg_server_prepare set to true.
	      # if not, data will swamp connection and you will get out of memory errors from the driver.
	      $force_pg_server_prepare = 1;
	    }
	  }
	}
	$sth->bind_param($bind_column_number, $column_value, $sql_data_type);
      } else {
	$sth->bind_param($bind_column_number, $column_value);
      }
      $bind_column_number++;
    }
  }
  $sth->bind_param($bind_column_number, $row_id, SQL_INTEGER);
  if ($force_pg_server_prepare) {
    $sth->{"pg_server_prepare"} = 1;
  }
  my $rv = $sth->execute();
  $sth->finish();
  return $rv;
}

1;

use strict;
local($^W) = 1;

sub url_decode($) {
  my $value = shift;
  $value =~ tr/+/ /;
  $value =~ s/%(..)/chr(hex($1))/egs;
  return $value;
}

1;

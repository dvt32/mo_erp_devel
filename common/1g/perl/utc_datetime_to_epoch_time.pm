use strict;
local($^W) = 1;

use Date::Calc qw(Date_to_Time);

# converts datetime (GMT/UTC) to epoch time (seconds since the epoch, always GMT/UTC).
# year is four digit year (e.g. 2006).
# month is 1 - 12.
# day is 1 - 31.
# hour is 0 - 23.
# minute is 0 - 59.
# second is 0 - 59.
# returns number of seconds since the epoch (always GMT/UTC).
sub utc_datetime_to_epoch_time {
  my ($year, $month, $day, $hour, $minute, $second) = @_;
  return Date_to_Time($year, $month, $day, $hour, $minute, $second);
}

1;

use strict;
local($^W) = 1;

use epoch_time_to_local_datetime;
use utc_datetime_to_epoch_time;

# converts datetime (GMT/UTC) to datetime (local time zone).
# year is four digit year (e.g. 2006).
# month is 1 - 12.
# day is 1 - 31.
# hour is 0 - 23.
# minute is 0 - 59.
# second is 0 - 59.
sub utc_datetime_to_local_datetime {
  my ($year, $month, $day, $hour, $minute, $second) = @_;
  return epoch_time_to_local_datetime(utc_datetime_to_epoch_time($year, $month, $day, $hour, $minute, $second));
}

1;

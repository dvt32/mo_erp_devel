include $(repo_root)/common/func.mk

# restart Apache using either service (sysvinit) or systemctl (systemd).
# examines use_systemd variable to see if we should use systemd.
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
ifeq ($(linux_distribution),debian)
define restart_apache
if [ -z "$(use_systemd)" ] || [ "$(use_systemd)" = "0" ]; then \
  service apache2 restart; \
else \
  systemctl restart apache2; \
fi; \
:
endef
else ifeq ($(linux_distribution),centos)
define restart_apache
  systemctl restart httpd; \
:
endef
endif

# restart Apache if DO_APACHE_RESTART exists in unique tmpdir.
#
# parameters:
# $1 = path to unique tmpdir
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
define restart_apache_if_needed
if [ -e "$(1)/DO_APACHE_RESTART" ]; then \
  rm -f "$(1)/DO_APACHE_RESTART"; \
  echo Restart apache2; \
  $(call restart_apache); \
fi; \
:
endef

# install an Apache site on Debian.
# check if Apache conf files changed; if changed, restart Apache; if no change, delete conf file in tmpdir.
# cmp test will detect change or missing file.
#
# parameters:
# $1 = path to Apache site configuration file
# $2 = site name; we assume conf file name is <sitename>.conf
# $3 = path to unique tmpdir
# $4 = path to system properties file for variable substitution
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)

ifeq ($(linux_distribution),debian)
define install_apache_site
$(call copy_subst,"$(1)","$(3)/$(2).conf",root.root,0644,,"$(4)"); \
if ! cmp -s "$(3)/$(2).conf" /etc/apache2/sites-available/$(2).conf; then \
  echo Apache conf files changed, so updating files and then restarting Apache; \
  mv -f "$(3)/$(2).conf" /etc/apache2/sites-available/$(2).conf; \
  a2ensite $(2); \
  touch "$(3)/DO_APACHE_RESTART"; \
else \
  rm -f "$(3)/$(2).conf"; \
fi; \
:
endef
else ifeq ($(linux_distribution),centos)
define install_apache_site
$(call copy_subst,"$(1)","$(3)/$(2).conf",root.root,0644,,"$(4)"); \
if ! cmp -s "$(3)/$(2).conf" /etc/httpd/conf.d/$(2).conf; then \
  echo Apache conf files changed, so updating files and then restarting Apache; \
  mv -f "$(3)/$(2).conf" /etc/httpd/conf.d/$(2).conf; \
  touch "$(3)/DO_APACHE_RESTART"; \
else \
  rm -f "$(3)/$(2).conf"; \
fi; \
:
endef
endif

# remove an Apache site on Debian. flag Apache for restart if needed.
#
# parameters:
# $1 = site name; we assume conf file name is <sitename>.conf
# $2 = path to unique tmpdir
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
ifeq ($(linux_distribution),debian)
define rm_apache_site
if [ -e /etc/apache2/sites-enabled/$(1).conf ]; then \
  a2dissite $(1); \
  touch "$(2)/DO_APACHE_RESTART"; \
fi; \
rm -f /etc/apache2/sites-available/$(1).conf; \
:
endef
else ifeq ($(linux_distribution),centos)
define rm_apache_site
if [ -e /etc/httpd/conf.d/$(1).conf ]; then \
  touch "$(2)/DO_APACHE_RESTART"; \
fi; \
rm -f /etc/apache2/sites-available/$(1).conf; \
:
endef
endif

# enable Apache modules (and restart Apache if needed).
#
# parameters:
# $1 = list of Apache modules to enable
# $2 = path to unique tmpdir
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
define enable_apache_modules
for apache_module in $(1); do \
  if [ ! -e "/etc/apache2/mods-enabled/$$apache_module.load" ]; then \
    a2enmod $$apache_module; \
    touch "$(2)/DO_APACHE_RESTART"; \
  fi; \
done; \
:
endef

.PHONY: apache_pkg apache_start_on_boot apache_wsgi_pkgs ssl_certs web_postgresql_cert

apache_pkg:
ifeq ($(linux_distribution),centos)
	@$(call install_packages,httpd)
else ifeq ($(linux_distribution),debian)
	@$(call install_packages,apache2)
# we do not use default site, so disable it
	@if [ -e "/etc/apache2/sites-enabled/000-default.conf" ]; then \
		a2dissite 000-default; \
	fi
else
	$(error unsupported distribution in apache_pkg rule: $(linux_distribution))
endif

apache_start_on_boot: apache_pkg
ifeq ($(linux_distribution),centos)
	systemctl enable httpd
else ifeq ($(linux_distribution),debian)
	@if [ -z "$(use_systemd)" ] || [ "$(use_systemd)" = "0" ]; then \
	  update-rc.d apache2 defaults; \
	  update-rc.d apache2 enable; \
	else \
	  if [ -z "$(wildcard /etc/rc3.d/S*apache2)" ]; then \
	    systemctl enable apache2; \
	  fi; \
	fi
else
	$(error unsupported distribution in apache_start_on_boot rule: $(linux_distribution))
endif

apache_wsgi_pkgs:
# on centos, only prepares for mod_wsgi install, doesn't actually install it (need venv to do that)
ifeq ($(linux_distribution),centos)
	@$(call install_packages,httpd httpd-devel)
else ifeq ($(linux_distribution),debian)
	@$(call install_packages,apache2 libapache2-mod-wsgi-py3)
else
	$(error unsupported distribution in apache_wsgi_pkgs rule: $(linux_distribution))
endif

ssl_certs:
ifeq ($(is_production_deployment),1)
ifeq ($(secret_base_dir),)
	$(error ssl_certs requires secret_base_dir on production server (i.e. when is_production_deployment is 1))
endif
ifeq ($(linux_distribution),centos)
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.crt)
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.key)
	@$(call check_file_exist,$(secret_base_dir)/ssl/$(intermediate_cert_filename))
	@$(call install_packages,mod_ssl)
	@cp -a $(secret_base_dir)/ssl/microoffice.com.crt /etc/pki/tls/certs/
	@cp -a $(secret_base_dir)/ssl/microoffice.com.key /etc/pki/tls/private/
	@cp -a $(secret_base_dir)/ssl/$(intermediate_cert_filename) /etc/pki/tls/certs/
	@chown root.root /etc/pki/tls/certs/microoffice.com.crt /etc/pki/tls/certs/$(intermediate_cert_filename) /etc/pki/tls/private/microoffice.com.key
	@chmod 0444 /etc/pki/tls/certs/microoffice.com.crt /etc/pki/tls/certs/$(intermediate_cert_filename)
	@chmod 0400 /etc/pki/tls/private/microoffice.com.key
else ifeq ($(linux_distribution),debian)
ifeq ($(intermediate_cert_filename),)
	$(error ssl_certs requires intermediate_cert_filename system property)
endif
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.crt)
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.key)
	@$(call check_file_exist,$(secret_base_dir)/ssl/$(intermediate_cert_filename))
	@cp -a $(secret_base_dir)/ssl/microoffice.com.crt /etc/ssl/certs/
	@cp -a $(secret_base_dir)/ssl/microoffice.com.key /etc/ssl/private/
	@cp -a $(secret_base_dir)/ssl/$(intermediate_cert_filename) /etc/ssl/certs/
	@chown root.root /etc/ssl/certs/microoffice.com.crt /etc/ssl/certs/$(intermediate_cert_filename) /etc/ssl/private/microoffice.com.key
	@chmod 0444 /etc/ssl/certs/microoffice.com.crt /etc/ssl/certs/$(intermediate_cert_filename)
	@chmod 0400 /etc/ssl/private/microoffice.com.key
else
	$(error unsupported distribution in ssl_certs rule: $(linux_distribution))
endif
endif

ssl_certs_devel:
ifeq ($(secret_base_dir),)
	$(error ssl_certs requires secret_base_dir on production server (i.e. when is_production_deployment is 1))
endif
ifeq ($(linux_distribution),centos)
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.crt)
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.key)
	@$(call check_file_exist,$(secret_base_dir)/ssl/$(intermediate_cert_filename))
	@$(call install_packages,mod_ssl)
	@cp -a $(secret_base_dir)/ssl/microoffice.com.crt /etc/pki/tls/certs/
	@cp -a $(secret_base_dir)/ssl/microoffice.com.key /etc/pki/tls/private/
	@cp -a $(secret_base_dir)/ssl/$(intermediate_cert_filename) /etc/pki/tls/certs/
	@chown root.root /etc/pki/tls/certs/microoffice.com.crt /etc/pki/tls/certs/$(intermediate_cert_filename) /etc/pki/tls/private/microoffice.com.key
	@chmod 0444 /etc/pki/tls/certs/microoffice.com.crt /etc/pki/tls/certs/$(intermediate_cert_filename)
	@chmod 0400 /etc/pki/tls/private/microoffice.com.key
else ifeq ($(linux_distribution),debian)
ifeq ($(intermediate_cert_filename),)
	$(error ssl_certs requires intermediate_cert_filename system property)
endif
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.crt)
	@$(call check_file_exist,$(secret_base_dir)/ssl/microoffice.com.key)
	@$(call check_file_exist,$(secret_base_dir)/ssl/$(intermediate_cert_filename))
	@cp -a $(secret_base_dir)/ssl/microoffice.com.crt /etc/ssl/certs/
	@cp -a $(secret_base_dir)/ssl/microoffice.com.key /etc/ssl/private/
	@cp -a $(secret_base_dir)/ssl/$(intermediate_cert_filename) /etc/ssl/certs/
	@chown root.root /etc/ssl/certs/microoffice.com.crt /etc/ssl/certs/$(intermediate_cert_filename) /etc/ssl/private/microoffice.com.key
	@chmod 0444 /etc/ssl/certs/microoffice.com.crt /etc/ssl/certs/$(intermediate_cert_filename)
	@chmod 0400 /etc/ssl/private/microoffice.com.key
else
	$(error unsupported distribution in ssl_certs rule: $(linux_distribution))
endif


web_postgresql_cert:
ifeq ($(is_production_deployment),1)
ifeq ($(secret_base_dir),)
	$(error postgresql_web_cert requires secret_base_dir on production server (i.e. when is_production_deployment is 1))
endif
ifeq ($(linux_distribution),centos)
	@$(call check_file_exist,$(secret_base_dir)/dbconf/root.crt)
	@mkdir -p /var/www/.postgresql
	@cp -a $(secret_base_dir)/dbconf/root.crt /var/www/.postgresql/
	@chmod 0755 /var/www/.postgresql
	@chown root.root /var/www/.postgresql/root.crt
	@chmod 0444 /var/www/.postgresql/root.crt
else ifeq ($(linux_distribution),debian)
	@$(call check_file_exist,$(secret_base_dir)/dbconf/root.crt)
	@mkdir -p /var/www/.postgresql
	@cp -a $(secret_base_dir)/dbconf/root.crt /var/www/.postgresql/
	@chmod 0755 /var/www/.postgresql
	@chown root.root /var/www/.postgresql/root.crt
	@chmod 0444 /var/www/.postgresql/root.crt
else
	$(error unsupported distribution in postgresql_web_cert rule: $(linux_distribution))
endif
endif

include $(repo_root)/common/func.mk

.PHONY: iptables_pkg iptables_conf firewalld_direct_xml

iptables_pkg:
ifeq ($(is_production_deployment),1)
ifeq ($(linux_distribution),debian)
	@$(call install_packages,iptables-persistent)
else
	$(error iptables_pkg requires debian distribution)
endif
endif

iptables_conf:
ifeq ($(is_production_deployment),1)
ifeq ($(linux_distribution),debian)
	@$(call check_file_exist,$(secret_base_dir)/iptables/rules.v4)
# if iptables rules do not exist, copy them
ifeq ("$(wildcard /etc/iptables/rules.v4)","")
	@echo ..copy persistent iptables rules to /etc/iptables/rules.v4
	@$(call copy_file,$(secret_base_dir)/iptables/rules.v4,/etc/iptables/rules.v4,root.root,0644)
	@iptables-restore < /etc/iptables/rules.v4
else
# if iptables rules previously existed, check that they match and stop make if no match
	@if ! cmp -s $(secret_base_dir)/iptables/rules.v4 /etc/iptables/rules.v4; then \
	  echo /etc/iptables/rules.v4 changed. Please review for differences. If new rules.v4 should take precedence, simply delete old.; \
	  exit 1; \
	fi
endif
else
	$(error iptables_conf requires debian distribution)
endif
endif

firewalld_direct_xml:
ifeq ($(is_production_deployment),1)
ifeq ($(linux_distribution),centos)
	@$(call check_file_exist,$(secret_base_dir)/firewalld/direct.xml.in)
# if firewalld rules do not exist, copy them
ifeq ("$(wildcard /etc/firewalld/direct.xml.in)","")
	@echo ..copy firewalld direct.xml.in to /etc/firewalld/direct.xml.in
	@$(call copy_file,$(secret_base_dir)/firewalld/direct.xml.in,/etc/firewalld/direct.xml.in,root.root,0644)
	@grep -v '^#' /etc/firewalld/direct.xml.in > /etc/firewalld/direct.xml
	@echo check iptables-save, systemctl restart firewalld, and then check if any rules missing (e.g. fail2ban)
else
# if firewalld rules previously existed, check that they match and stop make if no match
	@if ! cmp -s $(secret_base_dir)/firewalld/direct.xml.in /etc/firewalld/direct.xml.in; then \
	  echo /etc/firewalld/direct.xml.in changed. Please review for differences. If new direct.xml.in should take precedence, simply delete old.; \
	  exit 1; \
	fi
endif
else
	$(error firewalld_direct_xml requires centos distribution)
endif
endif

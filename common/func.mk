# common functions.
#
# Two reasons this file exists:
# First, it's more organized to keep functions in one place.
# Second, GNU Make does not export functions properly. Export changes variables to simply defined instead of recursively defined.
# Therefore, parameters stop working when function definitions are exported.


# check if a file exists, and if not, exit with error.
#
# params:
# $1 = path to file to check for existence
define check_file_exist
if [ ! -f $(1) ]; then echo "make: *** required file $(1) does not exist." >&2; exit 1; fi
endef

# check that every system property in a list of system properties exists and has some nonblank value.
# exit with error if any are missing or blank.
#
# params:
# $1 = list of system properties
define check_system_properties
$(foreach prop,$(1),$(if $($(prop)),,$(error missing $(prop) system property)))
endef

# patch a file safely.
#
# params:
# (file to be patched, patch file, [optional file to store 1/0 depending on whether patch applied])
#
# global variables:
# _quiet: squelch output
#
# need to make it all executed in one shell because caller might need continuity
define safe_patch
$(call check_file_exist,$(1)); \
$(call check_file_exist,$(2)); \
if [ -n "$(3)" ]; then \
	patch_count_file="$(3)"; \
	if `patch --dry-run -sr - -Np0 $(1) < $(2) > /dev/null`; then \
		patch -b -sr - -Np0 $(1) < $(2); \
		echo 1 > $$patch_count_file; \
	else \
		:; $(if $(_quiet),,echo "patch `basename $(2)` already applied to `basename $(1)`" >&2;) \
		echo 0 > $$patch_count_file; \
	fi; \
else \
	if `patch --dry-run -sr - -Np0 $(1) < $(2) > /dev/null`; then \
		patch -b -sr - -Np0 $(1) < $(2); \
	else \
		:; $(if $(_quiet),,echo "patch `basename $(2)` already applied to `basename $(1)`" >&2;) \
	fi; \
fi; \
:
endef

# given a string containing space separated list of packages, check which are already installed and only install missing ones.
# need to make lines continue for single shell instance with state since MKTEMPDIR has to persist between lines.
define install_packages
MKTEMPDIR=`/bin/mktemp -d`; \
echo $(1) | tr -s ' ' '\n' | sort | uniq > $$MKTEMPDIR/pkgs_needed.txt; \
if [ "$(linux_distribution)" = "centos" ]; then \
	yum -q list installed $(1) 2> /dev/null | tail -n +2 | awk -F. '{print $$1; }' | sort > $$MKTEMPDIR/pkgs_present.txt; \
elif [ "$(linux_distribution)" = "debian" ]; then \
	dpkg-query -l $(1) 2> /dev/null | grep ^ii | awk '{print $$2;}' | cut -d':' -f1 | sort > $$MKTEMPDIR/pkgs_present.txt; \
else \
	echo "unsupported Linux distribution: $(linux_distribution)" >&2; \
	exit 1; \
fi; \
PKG_LIST=`comm -23 $$MKTEMPDIR/pkgs_needed.txt $$MKTEMPDIR/pkgs_present.txt`; \
rm -f $$MKTEMPDIR/pkgs_needed.txt $$MKTEMPDIR/pkgs_present.txt; \
rmdir $$MKTEMPDIR; \
if [ -n "$$PKG_LIST" ]; then \
	if [ "$(linux_distribution)" = "centos" ]; then \
		yum -y install $$PKG_LIST; \
	elif [ "$(linux_distribution)" = "debian" ]; then \
		apt-get update; \
		DEBIAN_FRONTEND=noninteractive apt-get -y install $$PKG_LIST; \
	fi; \
fi; \
:
endef

# given a string containing space separated list of packages, check that all are installed and error if missing any.
# need to make lines continue with backslash for single shell instance in order to preserve state between lines.
define check_packages_installed
if [ "$(linux_distribution)" = "centos" ]; then \
  echo check_packages_installed not implemented for CentOS yet; \
  exit 1; \
elif [ "$(linux_distribution)" = "debian" ]; then \
  MISSING_PKG=""; \
  for pkg in $(1); do \
    if ! dpkg-query -W $$pkg > /dev/null 2>&1; then \
      if [ -z "$$MISSING_PKG" ]; then \
        echo -n "Debian packages needed: $$pkg"; \
      else \
        echo -n " $$pkg"; \
      fi; \
      MISSING_PKG=1; \
    fi; \
  done; \
  if [ -n "$$MISSING_PKG" ]; then \
    echo ""; \
    exit 1; \
  fi; \
fi; \
:
endef

# copy a file and perform chown and chmod (if desired)
#
# parameters:
# $1 = source file
# $2 = target file (new copy)
# $3 = chown (user.group, user, group, or blank to refrain from chown)
# $4 = chmod (u+rx or other symbolic, 0700 or numeric, or blank to refrain from chmod)
# $5 = preserve time; if not empty, use time from source file; if empty, time will be time of the copy
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
define copy_file
cp -f $1 $2; \
if [ "$3" != "" ]; then chown $3 $2; fi; \
if [ "$4" != "" ]; then chmod $4 $2; fi; \
if [ "$5" != "" ]; then touch -r $1 $2; fi; \
:
endef

# copy a file, perform chown and chmod (if desired), and add to list for later batch operation (such as substitute properties)
#
# parameters:
# $1 = source file
# $2 = target file (new copy)
# $3 = chown (user.group, user, group, or blank to refrain from chown)
# $4 = chmod (u+rx or other symbolic, 0700 or numeric, or blank to refrain from chmod)
# $5 = preserve time; if not empty, use time from source file; if empty, time will be time of the copy
#
# global variables:
# _file_list: temporary variable used to append files for substprop.py or other batch operation
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
define copy_enqueue
cp -f $1 $2; \
if [ "$3" != "" ]; then chown $3 $2; fi; \
if [ "$4" != "" ]; then chmod $4 $2; fi; \
if [ "$5" != "" ]; then touch -r $1 $2; fi; \
$(eval _file_list += $2) \
:
endef

# perform substitution on all files in _file_list
#
# parameters:
# $1 = properties file
# $2 = make variables in space-separated list
#
# global variables:
# _file_list: contains list of files
define subst_batch
$(repo_root)/tools/substprop.py --makefile --preservetime $1$(foreach makevar,$2, --setproperty $(makevar)=$($(makevar)))$(_file_list)
endef

# perform substitution on all files listed in a file who filename is passed as a parameter
#
# parameters:
# $1 = properties file
# $2 = file containing list of target files, one to a line
define subst_batch_from_file
$(repo_root)/tools/substprop.py --makefile --preservetime --listinfile $1 $2
endef

# copy a file, perform chown and chmod (if desired), and substitute properties
# use when substitute has to happen right after copy
#
# parameters:
# $1 = source file
# $2 = target file (new copy)
# $3 = chown (user.group, user, group, or blank to refrain from chown)
# $4 = chmod (u+rx or other symbolic, 0700 or numeric, or blank to refrain from chmod)
# $5 = preserve time; if not empty, use time from source file; if empty, time will be time of the copy
# $6 = properties file
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
define copy_subst
cp -f $1 $2; \
if [ "$3" != "" ]; then chown $3 $2; fi; \
if [ "$4" != "" ]; then chmod $4 $2; fi; \
$(repo_root)/tools/substprop.py --makefile $6 $2; \
if [ "$5" != "" ]; then touch -r $1 $2; fi; \
:
endef


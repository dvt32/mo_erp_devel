# Linux distribution we are running on
ifneq ($(wildcard /etc/os-release),)
linux_distribution := $(patsubst "%",%,$(patsubst ID=%,%,$(shell grep ^ID= /etc/os-release)))
else
ifneq ($(wildcard /etc/centos-release),)
linux_distribution := centos
endif
endif

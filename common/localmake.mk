# export all variables set in system.properties file to sub-make instances
.EXPORT_ALL_VARIABLES:

# inhibit creation of __pycache__ and .py[co] files during build.
PYTHONDONTWRITEBYTECODE := anything

local_subdir_path := $(word 2,$(cwd_components))
local_basedir := /srv/localmake/$(local_subdir_path)
system_properties := $(local_basedir)/system.properties
# pull in system.properties variables so they are visible to this make instance and sub-make instances
ifeq ($(wildcard $(system_properties)),)
$(error system.properties file not found at $(system_properties))
endif
-include $(system_properties)


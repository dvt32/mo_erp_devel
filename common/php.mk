# common defines for php code.

makedirectory:
	@mkdir -p $(INSTALLDIR)
	@mkdir -p $(INSTALLDIR)/tmp
	@for dir in cache logs sessions tests; do \
		mkdir -p $(INSTALLDIR)/tmp/$$dir; \
	done

# eof

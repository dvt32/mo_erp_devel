include $(repo_root)/common/func.mk

.PHONY: postgresql_repo postgresql_client_dev_pkgs

# APT key for PostgreSQL APT repo
postgresql_apt_key_fingerprint := B97B 0AFC AA1A 47F0 44F2  44A0 7FCC 7D46 ACCC 4CF8

postgresql_repo:
ifeq ($(linux_distribution),centos)
	@$(call check_system_properties,postgresql_yum_repo_url postgresql_yum_repo_file)
	@if [ ! -e /etc/yum.repos.d/$(postgresql_yum_repo_file) ]; then \
	  yum -y install $(postgresql_yum_repo_url); \
	fi
else ifeq ($(linux_distribution),debian)
# if APT key for PostgreSQL APT repo is not in list of trusted keys, add it
# grep returns 0 if found, 1 if not found
	@apt-key finger | grep -q "$(postgresql_apt_key_fingerprint)"; \
	if [ $$? -eq 1 ]; then \
	  $(call install_packages,wget); \
	  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -; \
	fi
# if PostgreSQL APT repo is not in sources, add it
# cmp returns 0 if same, 1 if different, 2 if trouble (e.g, missing file)
	@cmp -s $(repo_root)/database/conf/apt-stretch-pgdg.list /etc/apt/sources.list.d/pgdg.list; \
	if [ $$? -gt 0 ]; then \
	  echo add PostgreSQL APT repo to /etc/apt/sources.list.d/pgdg.list; \
	  cp -a $(repo_root)/database/conf/apt-stretch-pgdg.list /etc/apt/sources.list.d/pgdg.list; \
	  apt-get update; \
	fi
else
	$(error unsupported distribution in postgresql_repo rule: $(linux_distribution))
endif

postgresql_client_dev_pkgs:
ifeq ($(linux_distribution),centos)
	@$(call check_system_properties,postgresql_devel_pkg_name)
	@$(call install_packages,$(postgresql_devel_pkg_name))
else ifeq ($(linux_distribution),debian)
	@$(call install_packages,postgresql-client libpq-dev gcc)
else
	$(error unsupported distribution in postgresql_repo rule: $(linux_distribution))
endif

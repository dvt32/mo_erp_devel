from singleton import Singleton
import datetime

class DebugState(metaclass=Singleton):
  pass

debug_state = DebugState()

def get_debug_file(debug_file_path='/tmp/debug.txt', file_open_mode='a'):
  debug_file = getattr(debug_state, '_debug_file', None)
  if debug_file is None:
    debug_file = debug_state._debug_file = open(debug_file_path, file_open_mode)
  return debug_file

def log_msg(debug_message, emit_newline=True, emit_timestamp=True):
  debug_file = get_debug_file()
  if emit_timestamp:
    debug_file.write(str(datetime.datetime.now()) + ' ')
  debug_file.write(debug_message)
  if emit_newline and not debug_message.endswith('\n'):
    debug_file.write('\n')
  debug_file.flush()

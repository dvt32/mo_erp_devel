include $(repo_root)/common/func.mk

# install Python virtualenv and set it up with packages from PyPI.
# have to invoke bash explicitly so that we can source activate and run pip in the virtualenv.
#
# parameters:
# $1 = path to virtualenv
# $2 = list of packages to install with pip
#
# end with colon so that use is similar to other define blocks that are shell scripts (i.e. backslash at end is consistent)
ifeq ($(linux_distribution),centos)
define install_virtualenv
if [ ! -e $(1) ] || [ ! -e $(1)/bin/activate ]; then pyvenv --without-pip $(1); fi; \
/bin/bash -c "wget https://bootstrap.pypa.io/get-pip.py -O $(1)/get-pip.py"; \
/bin/bash -c " source $(1)/bin/activate && python $(1)/get-pip.py && pip install -q --upgrade pip setuptools && pip install -q --upgrade $(2)"; \
:
endef
else ifeq ($(linux_distribution),debian)
define install_virtualenv
if [ ! -e $(1) ] || [ ! -e $(1)/bin/activate ]; then pyvenv $(1); fi; \
/bin/bash -c "source $(1)/bin/activate && pip install -q --upgrade pip setuptools && pip install -q --upgrade $(2)"; \
:
endef
endif

define install_pip_pkg_from_src
/bin/bash -c "source $(1)/bin/activate && pip install -q --upgrade --no-binary $(2) $(2)"; \
:
endef

.PHONY: python_dev_pkgs jinja2_pkg

python_dev_pkgs:
ifeq ($(linux_distribution),debian)
	@$(call install_packages,python3-all-dev python3-venv gcc)
else ifeq ($(linux_distribution),centos)
	@$(call install_packages,python34-devel gcc)
endif

# Jinja2 is required for substprop.py
jinja2_pkg:
	@$(call install_packages,python3-jinja2)


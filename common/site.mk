# common targets for site directories

.PHONY: $(subdirs)
.PHONY: install
.PHONY: clean

all:
	@for subdir in $(subdirs); do $(MAKE) -C $$subdir; done

install:
	@for subdir in $(subdirs); do $(MAKE) -C $$subdir install; done

clean:
	@for subdir in $(subdirs); do $(MAKE) -C $$subdir clean; done

# eof

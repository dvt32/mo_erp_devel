from flask import request, redirect, url_for, render_template
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import SubmitField, PasswordField, HiddenField, StringField, SelectField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms import validators, ValidationError
import psycopg2
import psycopg2.extras
import database
import pycurl
import io
from urllib.parse import urlencode
import json
import re
import smtplib
from email.mime.text import MIMEText
import email.utils
import email.header

API_TOKEN = "{{ pipedrive_api_key }}"
referrer_field_hash = '{{ pipedrive_referrer_field_hash }}'
landing_field_hash = '{{ pipedrive_landing_field_hash }}'
location_field_hash = '{{ pipedrive_location_field_hash }}'
how_did_you_hear_hash = '{{ pipedrive_other_referrer_field_hash }}'
comments_field_hash = '{{ pipedrive_comments_field_hash }}'

# 'lead_in' stage for each pipeline
stage_id_dict = {'NY':{{ pipedrive_lead_stage_id_ny }}, 'CH':{{ pipedrive_lead_stage_id_ch }}, 'BO':{{ pipedrive_lead_stage_id_bo }}}

# new lead email addresses
new_lead_email_dict = {'NY':'{{ contact_email_ny }}', 'CH':'{{ contact_email_ch }}', 'BO':'{{ contact_email_bo }}'}

class contactForm(FlaskForm):
  name = StringField('Name*', validators=[validators.InputRequired()])
  email = StringField('Email*', validators=[validators.InputRequired(), validators.Email()])
  phone = StringField('Phone')
  city = SelectField('Location*')
  referrer = StringField('How Did You Hear About Us')
  comments = TextAreaField('Comments')
  http_referrer = HiddenField('HTTP_REFERRER')
  http_landing = HiddenField('LANDING PAGE')
  submit = SubmitField('Submit')

def get_form():
  form = contactForm(meta={'csrf':False})
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  # GET request
  curlObj.setopt(pycurl.HTTPGET,1)
  curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
  curlObj.setopt(pycurl.TIMEOUT,15)
  curlObj.setopt(pycurl.HEADER,0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  curlObj.setopt(pycurl.URL, 'https://api.pipedrive.com/v1/dealFields/{{ pipedrive_location_field_id }}?api_token=' + API_TOKEN)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  # note that it has to be cast to string because the validation will end up comparing a string value to an integer value otherwise (e.g. 22 != '22')
  form.city.choices = [(str(x['id']), x['label']) for x in res['data']['options']]
  return form

def show_form():
  form = get_form()
  return render_template('coalitionform.html', form=form)

def parse_form():
  form = get_form()
  if not form.validate_on_submit():
    return render_template('coalitionform.html', form=form)

  # Search the list of tuples for form.city.data (location_id) and return list of matches as a list... This can definitely be more readable, worth fixing later
  chosen_location = [v[1] for i, v in enumerate(form.city.choices) if v[0] == form.city.data][0]
  match = re.search(r', (NY|BO|CH)', chosen_location)

  if match is not None:
    chosen_city = match.group(0)[2:]
    stage_id = stage_id_dict[chosen_city]

  # setup curl object
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST,1)
  curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
  curlObj.setopt(pycurl.TIMEOUT,15)
  curlObj.setopt(pycurl.HEADER,0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)

  # validate reCaptcha
  curlObj.setopt(pycurl.URL, 'https://www.google.com/recaptcha/api/siteverify')
  response = request.form["g-recaptcha-response"]
  query = urlencode({'secret': '{{ recaptchaSecretKey }}', 'response': response})
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  print('Attempted to validate recaptcha')
  print(res)

  # deal with robot submitting form case
  if not {{ is_production_deployment }}:
    res['success'] = 'Force validation on development.'

  if not res['success']:
    curlObj.close()
    errors = ['Failed to validate reCaptcha, please make sure you click the box!']
    return render_template('coalitionform.html', form=form, errors=errors)

  # Create new person
  responseIO = io.BytesIO()
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  data = {'name': form.name.data, 'email': form.email.data, 'phone': form.phone.data }
  curlObj.setopt(pycurl.URL, 'https://api.pipedrive.com/v1/persons?api_token=' + API_TOKEN)
  query = urlencode(data)
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  person_id = res['data']['id']

  print('Attempted to create new person:')
  print(res)

  # Create new deal
  responseIO = io.BytesIO()
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  deal_data = {'title': 'Coalitionspace lead: ' + form.name.data, 'person_id': person_id, referrer_field_hash: form.http_referrer.data, landing_field_hash: form.http_landing.data, location_field_hash:form.city.data, how_did_you_hear_hash:form.referrer.data, comments_field_hash:form.comments.data }

  # if we don't set stage_id it defaults to Chicago. This specifies the specific 'Pipeline'
  if stage_id:
    deal_data['stage_id'] = stage_id
  curlObj.setopt(pycurl.URL, 'https://api.pipedrive.com/v1/deals?api_token=' + API_TOKEN)
  query = urlencode(deal_data)
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  print('Attempted to create new deal')
  print(res)

  curlObj.close()

  #connect to database and add new customer data into database
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  args = (form.name.data, form.email.data, form.phone.data, chosen_location, form.referrer.data + ', ' + form.http_referrer.data, form.comments.data)
  curdict.execute(''' INSERT INTO prospective_customer_tbl(first_name, email_address, work_phone_number, desired_location, marketing_source, notes) VALUES(%s, %s, %s, %s, %s, %s) ''', args)
  print('inserted a row into prospective_customer_tbl')

  # send email to the customer and microoffice email address
  mail_body = '''New Lead (available in pipedrive)

  Received Submission:
  first name: %s
  email address: %s
  phone number: %s
  location: %s
  how did you find us: %s
  Original Referrer: %s
  comments: %s''' % (form.name.data, form.email.data, form.phone.data, chosen_location, form.referrer.data, form.http_referrer.data, form.comments.data)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = '{{ contact_mail_from }}'
  if chosen_city:
    mail_msg['To'] = new_lead_email_dict[chosen_city]
  else:
    mail_msg['To'] = new_lead_email_dict['CH']

  mail_msg['Subject'] = email.header.Header('New Lead - Form Completion - Available on Pipedrive', 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()

  conn.commit()
  return 'Successfully submitted data. Check pipedrive.'

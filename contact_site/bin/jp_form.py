from flask import request, abort, redirect, url_for, flash, render_template, make_response
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import SubmitField, PasswordField, HiddenField, StringField, SelectField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms import validators
import psycopg2
import psycopg2.extras
import pycurl
import io
from urllib.parse import urlencode
import json
import smtplib
from email.mime.text import MIMEText
import email.utils
import email.header


class JapanCustomerInfo(FlaskForm):
  first_name = StringField('First Name', validators=[validators.InputRequired()])
  last_name = StringField('Last Name', validators=[validators.InputRequired()])
  company_name = StringField('Company Name', render_kw={'placeholder':'Company Name'})
  email_addr = StringField('Email Address', validators=[validators.InputRequired(), validators.Email()])
  work_phone = StringField('Work Phone Number', render_kw={'placeholder':'Add Country/Area Code'})
  desired_location = SelectField('Desired Office Location', choices=[])
  comment = TextAreaField('message')
  submitForm = SubmitField('Submit')

def parse_contact1():
  form = JapanCustomerInfo(request.form, csrf_enabled=False)
  conn = psycopg2.connect(database='{{ contact_db_name }}', user='{{ contact_db_username }}', password='{{ contact_db_password }}', host='{{ contact_db_host }}', sslmode='{{ contact_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('''SELECT B.city, B.address_1, F.label, F.id, F.building_label, F.short_local_label FROM floor_vw F, building_vw B WHERE F.building_id = B.id and B.is_active and F.is_active and B.type = 'officesuite' ORDER BY B.city, B.address_1, F.label''')
  choices = [('', '---Please Select A Location---')]
  choices.extend([(str(bf['building_label'] + ' ' +bf['short_local_label']), str(bf['city'] + ' - ' +bf['address_1'] + ', ' +bf['label'])) for bf in curdict])
  form.desired_location.choices = choices
  # response from recaptcha to indicate it is a human or robot
  response = request.form["g-recaptcha-response"]
  # if customer didn't use recaptcha, go back to the page
  if not response:
    errorMes = 'Please indicate that you are not robot and submit the form again. Thanks.'
    webpage = '{{ jpcontact_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  # using recaptcha to verify if it a human
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST,1)
  curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
  curlObj.setopt(pycurl.TIMEOUT,15)
  curlObj.setopt(pycurl.HEADER,0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  curlObj.setopt(pycurl.URL, 'https://www.google.com/recaptcha/api/siteverify')
  query = urlencode({'secret': '{{ recaptchaSecretKey }}', 'response': response})
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  curlObj.close()
  # # deal with robot submitting form case
  if not res['success']:
    errorMes = 'You cannot submit the form as a robot. Please go back to the main page.'
    webpage = '{{ jpcontact_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  if not form.validate_on_submit():
    errorMes = 'First name, last name and email address are required fields. Please make sure you filled in the three fields correctly.'
    webpage = '{{ jpcontact_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  #add new customer data into database
  curdict.execute(''' INSERT INTO prospective_customer_tbl(first_name, last_name, email_address, company, work_phone_number, desired_location, notes) VALUES(%s, %s, %s, %s, %s, %s, %s) ''', (form.first_name.data, form.last_name.data, form.email_addr.data, form.company_name.data, form.work_phone.data, form.desired_location.data, form.comment.data))
  conn.commit()
  # send email to the customer and microoffice email address
  mail_body = '''{{ jpcontact_mail_body }} \n

  Received Submission:
  first name: %s
  last name: %s
  email address: %s
  company name: %s
  phone number: %s
  desired location: %s
  comment: %s''' %(form.first_name.data, form.last_name.data, form.email_addr.data, form.company_name.data, form.work_phone.data, form.desired_location.data, form.comment.data)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = '{{ jpcontact_mail_from }}'
  mail_msg['To'] = form.email_addr.data
  mail_msg['Cc'] = '{{ jpcontact_mail_cc }}'
  mail_msg['Subject'] = email.header.Header('{{ jpcontact_mail_subject }}', 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()
  values = [['First Name: ', form.first_name.data], ['Last Name: ', form.last_name.data], ['Email Address: ', form.email_addr.data], ['Company Name: ', form.company_name.data], ['Work Phone Number: ', form.work_phone.data], ['Desired Office Location: ', form.desired_location.data], ['Message: ', form.comment.data]]
  return render_template("jpcongrat.html", values = values)
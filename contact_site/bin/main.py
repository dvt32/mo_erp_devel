import sys
sys.path.append('/var/www/sites/{{ contact_fqdn }}/lib')
from flask import request, redirect, url_for, render_template, escape
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import SubmitField, PasswordField, HiddenField, StringField, SelectField, TextAreaField, SelectMultipleField, widgets
from wtforms.fields.html5 import EmailField
from wtforms import validators, ValidationError
import psycopg2
import psycopg2.extras
import pycurl
import database
import io
from urllib.parse import urlencode
import json
import re
import smtplib
from email.mime.text import MIMEText
import email.utils
import email.header
import argparse
from itsdangerous import URLSafeTimedSerializer, SignatureExpired, BadSignature

API_TOKEN = "{{ pipedrive_api_key }}"
referrer_field_hash = '{{ pipedrive_referrer_field_hash }}'
landing_field_hash = '{{ pipedrive_landing_field_hash }}'
location_field_hash = '{{ pipedrive_location_field_hash }}'
how_did_you_hear_hash = '{{ pipedrive_other_referrer_field_hash }}'
comments_field_hash = '{{ pipedrive_comments_field_hash }}'

# 'lead_in' stage for each pipeline
stage_id_dict = {'NY':{{ pipedrive_lead_stage_id_ny }}, 
'CH':{{ pipedrive_lead_stage_id_ch }}, 
'BO':{{ pipedrive_lead_stage_id_bo }}}

def get_pipedrive_choices(field_id):
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  # GET request
  curlObj.setopt(pycurl.HTTPGET,1)
  curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
  curlObj.setopt(pycurl.TIMEOUT,15)
  curlObj.setopt(pycurl.HEADER,0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  curlObj.setopt(pycurl.URL, 'https://api.pipedrive.com/v1/dealFields/' + field_id + '?api_token=' + API_TOKEN)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  # note that it has to be cast to string because the validation will end up comparing a string value to an integer value otherwise (e.g. 22 != '22')
  return [(str(x['id']), x['label']) for x in res['data']['options']]

field_choices = [('referrer', 'Referrer'), ('referrer_other', 'Referrer (Other)'), ('location', 'Location (Building)'), ('comments', 'Comments')]

def getField(field):
  if field == 'referrer':
    choices = get_pipedrive_choices('{{ pipedrive_referrer_field_id }}')
    return SelectField('How Did You Hear About Us?', validators=[validators.Optional()], choices=choices, render_kw={'placeholder': 'How did you hear about us?'})
  elif field == 'referrer_other':
    return StringField('How Did You Hear About Us?', validators=[validators.Optional()], render_kw={'placeholder': 'How did you hear about us?'})
  elif field == 'location':
    choices = get_pipedrive_choices('{{ pipedrive_location_field_id }}')
    return SelectField('Location', validators=[validators.InputRequired()], choices=choices, render_kw={'placeholder': 'Which location are you interested in?'} )
  elif field == 'comments':
    return TextAreaField('Comments', validators=[validators.Optional()], render_kw={'placeholder': 'Any additional comments?'} )
  else:
    return None

class contactFormBase(FlaskForm):
  form_id = HiddenField('form_id')
  http_referer = HiddenField('HTTP_REFERER')
  http_landing = HiddenField('LANDING PAGE')
  first_name = StringField('First Name', validators=[validators.InputRequired()], render_kw={'placeholder': 'First Name'})
  last_name = StringField('Last Name', validators=[validators.InputRequired()], render_kw={'placeholder': 'Last Name'})
  email = StringField('Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder': 'Email'})
  phone = StringField('Phone Number', validators=[validators.InputRequired()], render_kw={'placeholder': 'Phone Number'})
  submitForm = SubmitField('Submit')

def get_form():
  form = contactFormBase(meta={'csrf':False})
  return form

def is_email(string):
  if not re.match(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', string):
    return False
  return True
  
class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class FormCreator(FlaskForm):
  fields = MultiCheckboxField('Fields', choices=field_choices, validators=[validators.Optional()])
  style = SelectField('Style', choices=[('horizontal', 'Horizontal'), ('vertical', 'Vertical')], validators=[validators.InputRequired()])
  dst_emails = StringField('Destination Emails for Form Responses (Comma Separated)', validators=[validators.InputRequired(), validators.Email()])
  submitForm = SubmitField('Create Form')
  
  def validate_dest_emails(form, field):
    emails = field.data
    emalis.split(',')
    for email in emails:
      email.strip()
      if not is_email(email):
        raise ValidationError('Must all be valid emails')
    return True
  
  
def form_creator():
  if request.method == 'GET':
    form = FormCreator()
    return render_template('form_creator.html', form=form)
  elif request.method == 'POST':
    form = FormCreator()
    if not form.validate_on_submit():
      return render_template('form_creator.html', form=form)
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('INSERT INTO contact_form_tbl (fields, dst_emails) VALUES (%s, %s) RETURNING *', (form.fields.data, [form.dst_emails.data]))
    form_entry = curdict.fetchone()
    serializer = URLSafeTimedSerializer('{{ contact_itsdangerous_secret_key }}')
    form_token = serializer.dumps(form_entry['id'], salt='contact_form_id')
    class ContactForm(contactFormBase):
      pass
    for field in form.fields.data:
      setattr(ContactForm, field, getField(field))
    contact_form = ContactForm(meta={'csrf':False})
    contact_form.form_id.data = form_token
    contact_form_html = render_template('contactform.html', form=contact_form, style=form.style.data)
    conn.commit()
    return escape(contact_form_html)

def show_form():
  form = get_form()
  return render_template('contact.html', form=form, style='horizontal')
  
def submit_form():
  form = get_form()
  if not form.validate_on_submit():
    flash('It seems something went wrong with your submission. Please go back and try submitting again.', 'error')
    return redirect('contactform_result.html')
  conn = database.get_connection()
  serializer = URLSafeTimedSerializer('{{ contact_itsdangerous_secret_key }}')
  form_id = serializer.loads(form.form_id.data, salt='contact_form_id')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM contact_form_tbl WHERE id = %s', (form_id,))
  form_entry = curdict.fetchone()
  if not form_entry:
    flash('It seems something went wrong with your submission. Please go back and try submitting again.', 'error')
    return render_template('contactform_result.html')
  class ContactForm(contactFormBase):
    pass
  for field in form_entry['fields']:
    setattr(ContactForm, field, getField(field))
  form = ContactForm(meta={'csrf':False})
  
  # setup curl object
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST,1)
  curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
  curlObj.setopt(pycurl.TIMEOUT,15)
  curlObj.setopt(pycurl.HEADER,0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)

  # validate reCaptcha
  curlObj.setopt(pycurl.URL, 'https://www.google.com/recaptcha/api/siteverify')
  response = request.form["g-recaptcha-response"]
  query = urlencode({'secret': '{{ recaptchaSecretKey }}', 'response': response})
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  
  if not res['success']:
    curlObj.close()
    flash('Failed to validate reCaptcha, please make sure you click the box!', 'error')
    return render_template('contactform_result.html', form=form)
    
  # Create new person
  responseIO = io.BytesIO()
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  data = {'name': form.first_name.data + ' ' + form.last_name.data, 'email': form.email.data, 'phone': form.phone.data }
  curlObj.setopt(pycurl.URL, 'https://api.pipedrive.com/v1/persons?api_token=' + API_TOKEN)
  query = urlencode(data)
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  person_id = res['data']['id']

  # Create new deal
  responseIO = io.BytesIO()
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  deal_data = {'title': 'CoalitionSpace lead: ' + form.first_name.data + ' ' + form.last_name.data, 'person_id': person_id, referrer_field_hash: form.http_referer.data, landing_field_hash: form.http_landing.data }
  if hasattr(form, 'location'):
    deal_data[location_field_hash] = form.location.data
    chosen_location = [v[1] for i, v in enumerate(form.location.choices) if v[0] == form.location.data][0]
    match = re.search(r', (NY|BO|CH)', chosen_location)

    if match is not None:
      chosen_city = match.group(0)[2:]
      stage_id = stage_id_dict[chosen_city]
    # if we don't set stage_id it defaults to Chicago. This specifies the specific 'Pipeline'
    if stage_id:
      deal_data['stage_id'] = stage_id
      
  if hasattr(form, 'comments'):
    deal_data[comments_field_hash] = form.comments.data
  if hasattr(form, 'referrer'):
    deal_data[how_did_you_hear_hash] = form.referrer.data
  elif hasattr(form, 'referrer_other'):
    deal_data[how_did_you_hear_hash] = form.referrer_other.data  
  
  # Search the list of tuples for form.city.data (location_id) and return list of matches as a list... This can definitely be more readable, worth fixing later
    
  curlObj.setopt(pycurl.URL, 'https://api.pipedrive.com/v1/deals?api_token=' + API_TOKEN)
  query = urlencode(deal_data)
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  curlObj.close()

  curdict.execute('INSERT INTO contact_form_response_tbl (http_referer, form_id) VALUES (%s, %s) RETURNING *', (form.http_referer.data, form_id))
  form_response = curdict.fetchone()
  for field in form:
    curdict.execute('INSERT INTO contact_form_response_val_tbl(response_id, field_name, field_value) VALUES (%s, %s, %s)', (form_response['id'], field.name, field.data))
  # send member email
  mail_body = '''New Lead (available in pipedrive)

  Received Submission:
  first name: %s
  last name: %s
  email address: %s
  phone number: %s''' % (form.first_name.data, form.last_name.data, form.email.data, form.phone.data)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = 'service@microoffice.com'
  mail_msg['To'] = form_entry['dst_emails'][0]
  mail_subject = 'New Lead - Form Completion - Available on Pipedrive'
  mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.starttls()
  mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
  mailer.send_message(mail_msg)
  mailer.quit()
  conn.commit()
  flash('Thank you for interest in Coalition Spaces! Your contact information was submitted successfully. </br><a href="%s">Return</a>' % form.http_referer.data, 'recent')
  return render_template('contactform_result.html', )
  

from flask import request, redirect, url_for, render_template
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import SubmitField, PasswordField, HiddenField, StringField, SelectField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms import validators
import psycopg2
import psycopg2.extras
import database
import pycurl
import io
from urllib.parse import urlencode
import json
import smtplib
from email.mime.text import MIMEText
import email.utils
import email.header

# form from microoffce.com
class moForm(FlaskForm):
  first_name = StringField('First Name', validators=[validators.InputRequired()])
  last_name = StringField('Last Name', validators=[validators.InputRequired()])
  email_address = StringField('Email Address', validators=[validators.InputRequired(), validators.Email()])
  phone_number = StringField('Phone Number')
# form from coalitionspace.com
class coalitionForm(FlaskForm):
  first_name = StringField('First Name', validators=[validators.InputRequired()])
  last_name = StringField('Last Name', validators=[validators.InputRequired()])
  your_email = StringField('Email Address', validators=[validators.InputRequired(), validators.Email()])
  tel_234 = StringField('Phone Number')
  city = SelectField('Location', choices=[("New York", "New York"), ("Boston", "Boston"), ("Chicago", "Chicago")])
  text_960 = StringField('How Did You Find Us')
  your_comment = TextAreaField('Special Notes')

# parse form from microoffice.com
def parse_moform():
  form = moForm(request.form, csrf_enabled=False)
  # response from recaptcha to indicate it is a human or robot
  response = request.form["g-recaptcha-response"]
  # if customer didn't use recaptcha, go back to the page
  if not response:
    errorMes = 'Please indicate that you are not robot and submit the form again. Thanks.'
    webpage = '{{ microoffice_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  # using recaptcha to verify if it a human
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST,1)
  curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
  curlObj.setopt(pycurl.TIMEOUT,15)
  curlObj.setopt(pycurl.HEADER,0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  curlObj.setopt(pycurl.URL, 'https://www.google.com/recaptcha/api/siteverify')
  query = urlencode({'secret': '{{ recaptchaSecretKey }}', 'response': response})
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  curlObj.close()
  # deal with robot submitting form case
  if not res['success']:
    errorMes = 'You cannot submit the form as a robot. Please go back to the main page.'
    webpage = '{{ microoffice_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  # deal with unvalidated form
  if not form.validate_on_submit():
    errorMes = 'First name, last name and email address are required fields. Please make sure you filled in the three fields correctly.'
    webpage = '{{ microoffice_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  #connect to database and add new customer data into database
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute(''' INSERT INTO prospective_customer_tbl(first_name, last_name, email_address, work_phone_number) VALUES(%s, %s, %s, %s) ''', (form.first_name.data, form.last_name.data, form.email_address.data, form.phone_number.data))
  conn.commit()
  # send email to the customer and microoffice email address
  mail_body = '''We have received your sumbitted information. We will contact you soon. - MicroOffice

  Received Submission:
  first name: %s
  last name: %s
  email address: %s
  phone number: %s''' %(form.first_name.data, form.last_name.data, form.email_address.data, form.phone_number.data)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = '{{ contact_mail_from }}'
  mail_msg['To'] = form.email_address.data
  mail_msg['Cc'] = '{{ contact_mail_cc }}'
  mail_msg['Subject'] = email.header.Header('Submit information on microoffice.com - Verify email', 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()
  # successfully submit form
  values = [['First Name: ', form.first_name.data], ['Last Name: ', form.last_name.data], ['Email Address: ', form.email_address.data], ['Phone Number: ', form.phone_number.data]]
  return render_template("congrat.html", values = values)
  
# parse form from coalition
def parse_coalitionForm():
  form = coalitionForm(request.form, csrf_enabled=False)
  # response from recaptcha to indicate it is a human or robot
  response = request.form["g-recaptcha-response"]
  # if customer didn't use recaptcha, go back to the page
  if not response:
    errorMes = 'Please indicate that you are not robot and submit the form again. Thanks.'
    webpage = '{{ coalitionsapce_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  # using recaptcha to verify if it a human
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST,1)
  curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
  curlObj.setopt(pycurl.TIMEOUT,15)
  curlObj.setopt(pycurl.HEADER,0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write)
  curlObj.setopt(pycurl.URL, 'https://www.google.com/recaptcha/api/siteverify')
  query = urlencode({'secret': '{{ recaptchaSecretKey }}', 'response': response})
  curlObj.setopt(pycurl.POSTFIELDS,query)
  curlObj.perform()
  res = json.loads(responseIO.getvalue().decode('utf-8'))
  curlObj.close()
  # deal with robot submitting form case
  if not res['success']:
    errorMes = 'You cannot submit the form as a robot. Please go back to the main page.'
    webpage = '{{ coalitionsapce_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  # deal with unvalidated form
  if not form.validate_on_submit():
    errorMes = 'First name, last name and email address are required fields. Please make sure you filled in the three fields correctly.'
    webpage = '{{ coalitionsapce_form_url }}'
    return render_template('error.html', errorMes = errorMes, webpage = webpage)
  #connect to database and add new customer data into database
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute(''' INSERT INTO prospective_customer_tbl(first_name, last_name, email_address, work_phone_number, desired_location, marketing_source, notes) VALUES(%s, %s, %s, %s, %s, %s, %s) ''', (form.first_name.data, form.last_name.data, form.your_email.data, form.tel_234.data, form.city.data, form.text_960.data, form.your_comment.data))
  conn.commit()
  # send email to the customer and microoffice email address
  mail_body = '''We have received your sumbitted information. We will contact you soon. - Coalition Space

  Received Submission:
  first name: %s
  last name: %s
  email address: %s
  phone number: %s
  desired location: %s
  how did you find us: %s
  comments: %s''' %(form.first_name.data, form.last_name.data, form.your_email.data, form.tel_234.data, form.city.data, form.text_960.data, form.your_comment.data)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = '{{ contact_mail_from }}'
  mail_msg['To'] = form.your_email.data
  mail_msg['Cc'] = '{{ contact_mail_cc }}'
  mail_msg['Subject'] = email.header.Header('Submit information on coalition.com - Verify email', 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()
  # successfully submit form
  values = [['First Name: ', form.first_name.data], ['Last Name: ', form.last_name.data], ['Email Address: ', form.your_email.data], ['Phone Number: ', form.tel_234.data], ['Desired Location', form.city.data], ['How Did You Find Us', form.text_960.data], ['Special Notes: ', form.your_comment.data]]
  return render_template("congrat.html", values = values)

 
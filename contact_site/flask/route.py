import sys
sys.path.append('/var/www/sites/{{ contact_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ contact_fqdn }}/lib')
from flask import Flask, render_template, g
from flask_cors import cross_origin, CORS
import app_index
import jp_form
import microoffice_form
import contact_form
import main

app = Flask(__name__)
app.secret_key = '{{ contact_flask_secret_key }}'
app.debug = {{ contact_flask_app_debug }}

@app.context_processor
def define_static_url():
    return dict(static_base_url='{{ static_base_url }}')
    
@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@app.route('/')
def index():
  return app_index.show_index()

@app.route('/parse_contact1', methods = ["POST"])
def jp_contact1():
  return jp_form.parse_contact1()

# receive form from microoffice.com
@app.route('/microoffice/main', methods = ["POST"])
def contact_microoffice():
  return microoffice_form.parse_moform()

# receive form form coalitionspace.com
@app.route('/coalitionspace/main', methods = ["POST"])
def contact_coalition():
  return microoffice_form.parse_coalitionForm()

@app.route('/coalitionspace/form', methods = ["GET"])
def show_coalition_form():
  return contact_form.show_form()

@app.route('/coalitionspace/form/submit', methods = ["POST"])
def parse_coalition_form():
  return contact_form.parse_form()
  
@app.route('/submit_form', methods = ["POST"])
def submit_form():
  return main.submit_form()
  
@app.route('/contact_form', methods = ["GET"])
def show_contact_form():
  return main.show_form()

@app.route('/form_creator', methods=["POST", "GET"])
def form_creator():
  return main.form_creator()
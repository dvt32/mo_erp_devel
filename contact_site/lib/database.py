from flask import g
import psycopg2

def get_connection():
  conn = getattr(g, '_database', None)
  if conn is None:
    conn = g._database = psycopg2.connect(database='{{ contact_db_name }}', user='{{ contact_db_username }}', password='{{ contact_db_password }}', host='{{ contact_db_host }}', sslmode='{{ contact_db_sslmode }}')
  return conn

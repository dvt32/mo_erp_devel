from flask import render_template

def show_index():
  return render_template('index.html')

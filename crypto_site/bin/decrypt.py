from flask import request, Response
import gnupg
import json
import warnings
import database

def decrypt_row():
  if request.method == 'GET':
    request_dict = request.args
  elif request.method == 'POST':
    request_dict = request.form
  else:
    output = 'Unsupported request method: {}'.format(request.method)
    warnings.warn(output)
    # 400 Bad Request
    response = Response(output, status=400, mimetype='text/plain')
    return response
  tbl_name = request_dict.get('tbl')
  row_id = request_dict.get('id')
  request_user_id = request_dict.get('request_user_id')
  conn = database.get_connection()
  cur = conn.cursor()
  # the query on information_schema.tables checks whether the table exists AND the current user has access, which is too specific. 
  #cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = %s and table_name = %s LIMIT 1', ('public', tbl_name))
  # check that table or view exists (also protects against SQL injection)
  cur.execute('SELECT 1 FROM pg_tables WHERE schemaname = %s AND tablename = %s UNION SELECT 1 FROM pg_views WHERE schemaname = %s AND viewname = %s LIMIT 1', ('public', tbl_name, 'public', tbl_name))
  if cur.fetchone() is None:
    output = 'Table {} does not exist.'.format(tbl_name)
    warnings.warn(output)
    # 400 Bad Request
    response = Response(output, status=400, mimetype='text/plain')
    return response
  # check that we have access to table
  cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = %s and table_name = %s LIMIT 1', ('public', tbl_name))
  if cur.fetchone() is None:
    output = 'mo_cerberus does not have access to table {}.'.format(tbl_name)
    warnings.warn(output)
    # 400 Bad Request
    response = Response(output, status=400, mimetype='text/plain')
    return response
  # no need to quote tbl_name because we check above that there is a table with matching name
  cur.execute('SELECT crypt_json FROM ' + tbl_name + ' WHERE id = %s', (row_id,))
  row = cur.fetchone()
  if row is None:
    output = 'Table {} has no row with id {}.'.format(tbl_name, row_id)
    warnings.warn(output)
    # 400 Bad Request
    response = Response(output, status=400, mimetype='text/plain')
    return response
  crypt_json = row[0]
  cur.close()
  conn.close()
  crypt_column_dict = {}
  if crypt_json is not None:
    if crypt_json['columns'] is not None:
      gpg = gnupg.GPG()
      gpg.encode = 'utf-8'
      for key in crypt_json['columns'].keys():
        crypt_value = crypt_json['columns'][key]['crypt_value']
        decrypted_data = gpg.decrypt(crypt_value)
        # decrypted_data.ok will be True and decrypted_data.status will be 'decryption ok' if successful.
        # otherwise False and 'bad passphrase' or 'decryption failed' or some other message.
        if not decrypted_data.ok:
          output = 'Unable to decrypt - check that passphrase was preset.'
          warnings.warn(output)
          # 400 Bad Request
          response = Response(output, status=400, mimetype='text/plain')
          return response
        decrypted_string = str(decrypted_data)
        crypt_column_dict[key] = decrypted_string
  output = json.dumps(crypt_column_dict)
  # 200 OK
  response = Response(output, status=200, mimetype='text/plain')
  return response

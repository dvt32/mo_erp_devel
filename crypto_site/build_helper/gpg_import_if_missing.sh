#!/bin/bash

# need 2>/dev/null because gnupg complains "failed to re-lookup public key" with --dry-run.
# need head -1 because otherwise two fingerprints/keygrips separated by space: 1. secret key, 2. secret subkey.
fingerprint=`gpg --batch --with-colons --fingerprint --import-options import-show --dry-run --import < /var/www/.gnupg/postgres.seckey 2>/dev/null | grep ^fpr: | sed -r 's/^fpr:+(\w+):/\1/g' | head -1`
keygrip=`gpg --batch --with-colons --with-keygrip --import-options import-show --dry-run --import < /var/www/.gnupg/postgres.seckey 2>/dev/null | grep ^grp: | sed -r 's/^grp:+(\w+):/\1/g' | head -1`
# 2>/dev/null squelches "error reading key: No public key"
key_grep=`gpg --list-keys --with-keygrip $fingerprint 2>/dev/null | grep $keygrip`
if [ "$key_grep" == "" ]; then
  # key is not present, so import it
  echo ..import private key into gpg
  gpg --batch --import < /var/www/.gnupg/postgres.seckey
  echo ..give the key ultimate trust
  echo "$fingerprint:6:" | gpg --import-ownertrust
fi
subkey_keygrip=`gpg --fingerprint --with-colons --with-keygrip $fingerprint | grep ^grp: | sed -r 's/^grp:+(\w+):/\1/g' | tail -1`
sed -i s/"{{ gpg_keygrip }}"/"$subkey_keygrip"/g /var/www/.gnupg/preset_passphrase.sh

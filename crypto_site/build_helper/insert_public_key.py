#!/var/www/sites/crypto.microoffice.com/venv/bin/python3
#!{{ crypto_basedir }}/venv/bin/python3
import psycopg2.extras
import sys
import re

def main():
  tmpdir = sys.argv[1]
  with open(tmpdir + '/fingerprint.txt', 'r') as fingerprint:
    for line in fingerprint:
      if 'sub' in line:
        size_type = re.match(r'(\D|\s)*(\d*)([A-Z])\/.*', line)
        fprint = re.match(r'.*=\s(.*)', next(fingerprint))
        size = size_type.group(2)
        type = size_type.group(3)
        if type == 'R':
          type = 'rsa'
        elif type == 'G':
          type = 'dsa'
        fprint = fprint.group(1)
        break;
  with open(tmpdir + '/public_key', 'rb') as key:
    public_key = key.read()
  conn = psycopg2.connect(database='microoffice_devel', user='mo_admin', password='crimsonadmin', host='localhost', sslmode='allow')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('INSERT INTO public_key_tbl (public_key, key_type, key_size, fingerprint) VALUES (%s, %s, %s, %s) RETURNING *', (public_key, type, size, fprint))
  key_entry = curdict.fetchone()
  curdict.execute('UPDATE config_setting_tbl SET value = %s WHERE name = %s', (key_entry['id'], 'public_key_id'))
  conn.commit()
  return

if __name__ == "__main__":
  main()

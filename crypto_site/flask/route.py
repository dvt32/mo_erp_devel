import sys
sys.path.append('/var/www/sites/{{ crypto_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ crypto_fqdn }}/lib')
from flask import Flask, render_template, g
import app_index
import decrypt

app = Flask(__name__)
app.secret_key = '{{ crypto_flask_secret_key }}'
app.debug = {{ crypto_flask_app_debug }}

@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@app.route('/')
def index():
  return app_index.show_index()

@app.route('/decrypt_row', methods=['GET', 'POST'])
def decrypt_row():
  return decrypt.decrypt_row()


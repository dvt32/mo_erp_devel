from flask import g
import psycopg2

def get_connection():
  conn = getattr(g, '_database', None)
  if conn is None:
    conn = g._database = psycopg2.connect(database='{{ crypto_db_name }}', user='{{ crypto_db_username }}', password='{{ crypto_db_password }}', host='{{ crypto_db_hostname }}', sslmode='{{ crypto_db_sslmode }}')
  return conn

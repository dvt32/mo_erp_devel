#!/bin/bash

# kill any existing gpg-agent instances
pkill gpg-agent
# start up new instance of gpg-agent
gpg-agent --homedir /var/www/.gnupg --daemon --allow-preset-passphrase
echo `dialog --stdout --passwordbox "Password:" 0 0` | /usr/lib/gnupg2/gpg-preset-passphrase --preset {{ gpg_keygrip }}

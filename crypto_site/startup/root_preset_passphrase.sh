#!/bin/sh

# temporarily change owner of /dev/pts/X to www-data to allow dialog command to function correctly
chown www-data `tty`
# call dialog to fetch passphrase and then pipe it into gpg-agent
su -s /bin/bash -c /var/www/.gnupg/preset_passphrase.sh www-data
# now change owner of pts back to root
chown root `tty`

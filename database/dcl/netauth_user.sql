GRANT INSERT, SELECT, UPDATE ON netauth_device_guest_tbl, netauth_device_transient_tbl, netauth_device_pin_tbl, netauth_reg_code_tbl, netauth_access_code_tbl, cust_acct_tbl TO mo_netauth;
GRANT SELECT, UPDATE ON cust_pers_tbl TO mo_netauth;
GRANT SELECT ON state_tbl, floor_tbl, building_tbl, building_link_tbl TO mo_netauth;
GRANT INSERT, SELECT, UPDATE, DELETE ON netauth_device_cust_tbl, netauth_device_id_tbl, netauth_code_id_tbl, external_daypass_user_tbl TO mo_netauth;
GRANT USAGE ON netauth_device_seq, netauth_code_seq TO mo_netauth;


GRANT INSERT, SELECT, UPDATE ON external_daypass_user_tbl, netauth_access_code_tbl, netauth_device_transient_tbl TO mo_operator;
GRANT USAGE ON netauth_code_seq, netauth_device_seq TO mo_operator;
GRANT INSERT, SELECT, UPDATE, DELETE on netauth_code_id_tbl, netauth_device_id_tbl TO mo_operator;

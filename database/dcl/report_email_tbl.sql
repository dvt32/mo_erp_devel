GRANT SELECT ON TABLE report_type_tbl TO mo_operator;
GRANT SELECT ON TABLE report_type_tbl TO mo_report;

GRANT SELECT,INSERT,DELETE ON TABLE report_email_tbl TO mo_operator;
GRANT SELECT ON TABLE report_email_tbl TO mo_report;
-- necessary to view emails for each employee
GRANT SELECT ON TABLE employee_tbl TO mo_report;

CREATE ROLE sp_update_pmt_vault;
-- Permissions for validate_pmt_vault_card
GRANT SELECT ON state_tbl TO sp_update_pmt_vault;
GRANT SELECT, UPDATE ON pmt_vault_card_tbl TO sp_update_pmt_vault;
GRANT SELECT, UPDATE ON pmt_vault_ach_tbl  TO sp_update_pmt_vault;

ALTER FUNCTION deactivate_pmt_vault ( integer) OWNER TO sp_update_pmt_vault;
REVOKE ALL ON FUNCTION deactivate_pmt_vault(pmt_vault_id INT) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION deactivate_pmt_vault(integer) TO mo_apply;

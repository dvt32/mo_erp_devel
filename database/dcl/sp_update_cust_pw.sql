-- Create role for function and set ownership (for security definer clause)
CREATE ROLE sp_update_cust_pers;
GRANT SELECT, UPDATE ON cust_pers_tbl TO sp_update_cust_pers;
GRANT SELECT ON config_setting_tbl TO sp_update_cust_pers;
GRANT SELECT ON public_key_tbl TO sp_update_cust_pers;
ALTER FUNCTION update_cust_pw(text, text, text) OWNER TO sp_update_cust_pers;
-- allow mo_apply to execute
REVOKE ALL ON FUNCTION update_cust_pw(text, text, text) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION update_cust_pw(text, text, text) TO mo_apply;

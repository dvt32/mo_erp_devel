CREATE TYPE bank_account_function_type AS ENUM ('checking', 'savings', 'money_market');
CREATE TYPE bank_account_ownership_type AS ENUM ('personal', 'business');
CREATE TYPE ach_txn_status_type AS ENUM ('pending', 'initiated', 'completed', 'failed', 'canceled');
CREATE SEQUENCE ach_credit_txn_seq;
CREATE TABLE ach_credit_txn_tbl (
  id integer DEFAULT nextval('ach_credit_txn_seq'::regclass) NOT NULL PRIMARY KEY,
  payor_company_id integer NOT NULL REFERENCES company_tbl (id),
  source_routing_number character(9) NOT NULL,
  source_account_number character varying(17) NOT NULL,
  -- payee_name: Chase allows 32 chars, CitiBank allows 22 chars
  payee_name varchar(32) NOT NULL,
  payee_table_column varchar(127),
  payee_fk_id integer,
  payee_routing_number character(9) NOT NULL,
  payee_account_number character varying(17) NOT NULL,
  payee_account_function bank_account_function_type NOT NULL,
  payee_account_ownership bank_account_ownership_type NOT NULL,
  amount money NOT NULL,
  scheduled_date date NOT NULL,
  -- note: Chase allows 80 chars, CitiBank allows 75 chars
  note varchar(80),
  -- Zero or more email addresses separated by commas. Send ACH credit transaction result to the email address(es).
  email text,
  requester_id integer NOT NULL REFERENCES employee_tbl (id),
  notify_requester boolean DEFAULT true NOT NULL,
  approver_id integer REFERENCES employee_tbl (id),
  notify_approver boolean DEFAULT false NOT NULL,
  status ach_txn_status_type DEFAULT 'pending' NOT NULL,
  when_paid date,
  txn_number varchar(32),
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE ach_credit_txn_tbl IS 'ACH credit transactions (i.e. payments to vendors, contractors, and other business partners)';
COMMENT ON COLUMN ach_credit_txn_tbl.payor_company_id IS 'The company in the Micro Office family that issued the payment';
COMMENT ON COLUMN ach_credit_txn_tbl.source_routing_number IS 'The routing number of one of our bank accounts to send money from';
COMMENT ON COLUMN ach_credit_txn_tbl.source_account_number IS 'The account number of one of our bank accounts to send money from';
COMMENT ON COLUMN ach_credit_txn_tbl.payee_name IS 'Legal name of the payee';
COMMENT ON COLUMN ach_credit_txn_tbl.payee_table_column IS 'Table and column that link to this row, in <table>.<column> format; can be vendor_tbl.id, govt_filing.id, or other table and column, or can be NULL if no linkage';
COMMENT ON COLUMN ach_credit_txn_tbl.payee_fk_id IS 'Foreign key ID of linked table; can be NULL';
COMMENT ON COLUMN ach_credit_txn_tbl.payee_routing_number IS 'The routing number of the bank account to send money to';
COMMENT ON COLUMN ach_credit_txn_tbl.payee_account_number IS 'The account number of the bank account to send money to';
COMMENT ON COLUMN ach_credit_txn_tbl.payee_account_function IS 'checking | savings | money_market';
COMMENT ON COLUMN ach_credit_txn_tbl.payee_account_ownership IS 'personal | business';
COMMENT ON COLUMN ach_credit_txn_tbl.email IS 'Zero or more email addresses separated by commas. Send ACH credit transaction result to the email address(es).';
COMMENT ON COLUMN ach_credit_txn_tbl.requester_id IS 'ID of the employee who is requesting the ACH credit';
COMMENT ON COLUMN ach_credit_txn_tbl.notify_requester IS 'true | false: send an email to requester when ACH credit is processed';
COMMENT ON COLUMN ach_credit_txn_tbl.approver_id IS 'ID of the employee who approved the ACH credit';
COMMENT ON COLUMN ach_credit_txn_tbl.notify_approver IS 'true | false: send an email to approver when ACH credit is processed';
COMMENT ON COLUMN ach_credit_txn_tbl.status IS 'pending | initiated | completed | failed';
COMMENT ON COLUMN ach_credit_txn_tbl.when_paid IS 'Date when the ACH credit transaction was processed';
COMMENT ON COLUMN ach_credit_txn_tbl.txn_number IS 'Transaction number or reference number supplied by the bank for the transaction';

-- validate column values in ach_credit_txn_tbl
CREATE FUNCTION validate_ach_credit_txn() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  -- strip all spaces in source_routing_number
  NEW.source_routing_number = strip_space(NEW.source_routing_number);
  -- if operation is INSERT or source_routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.source_routing_number IS DISTINCT FROM OLD.source_routing_number)) THEN
    IF NEW.source_routing_number IS NULL THEN
      RAISE EXCEPTION 'source_routing_number is required';
    END IF;
    IF NEW.source_routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'source_routing_number must be 9 digits; you entered %', NEW.source_routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.source_routing_number) THEN
      RAISE EXCEPTION 'source_routing_number not found in FedACH Participants; you entered %', NEW.source_routing_number;
    END IF;
  END IF;
  -- strip all spaces in source_account_number
  NEW.source_account_number = strip_space(NEW.source_account_number);
  -- if operation is INSERT or source_account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.source_account_number IS DISTINCT FROM OLD.source_account_number)) THEN
    IF NEW.source_account_number IS NULL THEN
      RAISE EXCEPTION 'source_account_number is required';
    END IF;
    IF NEW.source_account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'source_account_number must be numeric; you entered %', NEW.source_account_number;
    END IF;
  END IF;
  -- strip all leading and trailing spaces in payee_name
  NEW.payee_name = strip_collapse_space(NEW.payee_name);
  -- strip all spaces in payee_table_column
  NEW.payee_table_column = strip_space(NEW.payee_table_column);
  -- strip all spaces in payee_routing_number
  NEW.payee_routing_number = strip_space(NEW.payee_routing_number);
  -- if operation is INSERT or payee_routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.payee_routing_number IS DISTINCT FROM OLD.payee_routing_number)) THEN
    IF NEW.payee_routing_number IS NULL THEN
      RAISE EXCEPTION 'payee_routing_number is required';
    END IF;
    IF NEW.payee_routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'payee_routing_number must be 9 digits; you entered %', NEW.payee_routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.payee_routing_number) THEN
      RAISE EXCEPTION 'payee_routing_number not found in FedACH Participants; you entered %', NEW.payee_routing_number;
    END IF;
  END IF;
  -- strip all spaces in payee_account_number
  NEW.payee_account_number = strip_space(NEW.payee_account_number);
  -- if operation is INSERT or payee_account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.payee_account_number IS DISTINCT FROM OLD.payee_account_number)) THEN
    IF NEW.payee_account_number IS NULL THEN
      RAISE EXCEPTION 'payee_account_number is required';
    END IF;
    IF NEW.payee_account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'payee_account_number must be numeric; you entered %', NEW.payee_account_number;
    END IF;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  NEW.email = strip_space(lower(NEW.email));
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  -- strip all spaces in txn_number
  NEW.txn_number = strip_space(NEW.txn_number);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in ach_credit_txn_tbl
CREATE FUNCTION mask_ach_credit_txn() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.source_account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.source_account_number IS DISTINCT FROM OLD.source_account_number) THEN
      IF length(NEW.source_account_number) > 4 THEN
        NEW.source_account_number = left(NEW.source_account_number, 1) || repeat('*', length(NEW.source_account_number) - 4) || right(NEW.source_account_number, 3);
      END IF;
    END IF;
  END IF;
  IF (NEW.payee_account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.payee_account_number IS DISTINCT FROM OLD.payee_account_number) THEN
      IF length(NEW.payee_account_number) > 4 THEN
        NEW.payee_account_number = left(NEW.payee_account_number, 1) || repeat('*', length(NEW.payee_account_number) - 4) || right(NEW.payee_account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON ach_credit_txn_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON ach_credit_txn_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON ach_credit_txn_tbl FOR EACH ROW EXECUTE PROCEDURE validate_ach_credit_txn();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON ach_credit_txn_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('source_account_number', 'payee_account_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON ach_credit_txn_tbl FOR EACH ROW EXECUTE PROCEDURE mask_ach_credit_txn();

-- set up audit log for ach_credit_txn_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON ach_credit_txn_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON ach_credit_txn_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('ach_credit_txn_tbl');

CREATE VIEW ach_credit_txn_vw AS
  SELECT ach_credit_txn_tbl.id,
    ach_credit_txn_tbl.payor_company_id,
    company_tbl.name AS payor_company_name,
    ach_credit_txn_tbl.source_routing_number,
    ach_credit_txn_tbl.source_account_number,
    ach_credit_txn_tbl.payee_name,
    ach_credit_txn_tbl.payee_routing_number,
    ach_credit_txn_tbl.payee_account_number,
    ach_credit_txn_tbl.amount,
    ach_credit_txn_tbl.scheduled_date,
    ach_credit_txn_tbl.note,
    ach_credit_txn_tbl.email,
    (requester_employee_tbl.last_name)::text || ', '::text || (requester_employee_tbl.first_name)::text AS requester_name,
    ach_credit_txn_tbl.notify_requester,
    (approver_employee_tbl.last_name)::text || ', '::text || (approver_employee_tbl.first_name)::text AS approver_name,
    ach_credit_txn_tbl.notify_approver,
    ach_credit_txn_tbl.status,
    ach_credit_txn_tbl.when_paid,
    ach_credit_txn_tbl.txn_number,
    ach_credit_txn_tbl.payee_table_column,
    ach_credit_txn_tbl.payee_fk_id
  FROM (((ach_credit_txn_tbl
    JOIN employee_tbl requester_employee_tbl ON ((ach_credit_txn_tbl.requester_id = requester_employee_tbl.id)))
    JOIN employee_tbl approver_employee_tbl ON ((ach_credit_txn_tbl.approver_id = approver_employee_tbl.id)))
    JOIN company_tbl ON ((ach_credit_txn_tbl.payor_company_id = company_tbl.id)));

--DROP VIEW ach_credit_txn_vw;
--DROP TABLE ach_credit_txn_tbl;
--DROP SEQUENCE ach_credit_txn_seq;
--DROP FUNCTION validate_ach_credit_txn();
--DROP FUNCTION mask_ach_credit_txn();
--DROP TYPE bank_account_function_type;
--DROP TYPE bank_account_ownership_type;
--DROP TYPE ach_txn_status_type;

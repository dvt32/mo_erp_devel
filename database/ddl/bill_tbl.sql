CREATE SEQUENCE bill_seq;
CREATE SEQUENCE bill_list_id_seq;

CREATE TABLE bill_tbl (
  id integer DEFAULT nextval('bill_seq'::regclass) NOT NULL PRIMARY KEY,
  -- which company (entity) on our side that the bill belongs to (and thus which QuickBooks company file it goes in)
  company_id integer NOT NULL REFERENCES company_tbl (id),
  -- QB: TxnID. Not visible in UI. NOT NULL in QB.
  qb_list_id varchar(36) DEFAULT nextval('bill_list_id_seq'::regclass) NOT NULL,
  unique(company_id, qb_list_id),
  -- QB: EditSequence. Not visible in UI. NOT NULL in QB.
  qb_edit_sequence varchar(16),
  -- QB: Name
  name character varying(41) NOT NULL,
  -- QB: IsActive
  is_active boolean DEFAULT true NOT NULL,
  -- QB: CompanyName
  company_name character varying(41),
  -- QB: Salutation. Mr. | Miss | Mrs. | Ms. | Dr.
  salutation character varying(15),
  -- QB: FirstName
  first_name character varying(25),
  -- QB: MiddleName
  middle_name character varying(5),
  -- QB: LastName
  last_name character varying(25),
  -- QB: JobTitle
  job_title character varying(41),
  -- QB: BillAddress.Addr1, OpenSync: BillAddress_Addr1
  bill_address_addr_1 varchar(41),
  -- QB: BillAddress.Addr2, OpenSync: BillAddress_Addr2
  bill_address_addr_2 varchar(41),
  -- QB: BillAddress.Addr3, OpenSync: BillAddress_Addr3
  bill_address_addr_3 varchar(41),
  -- QB: BillAddress.Addr4, OpenSync: BillAddress_Addr4
  bill_address_addr_4 varchar(41),
  -- QB: BillAddress.Addr5, OpenSync: BillAddress_Addr5
  bill_address_addr_5 varchar(41),
  -- QB: BillAddress.City, OpenSync: BillAddress_City
  bill_address_city varchar(31),
  -- QB: BillAddress.State, OpenSync: BillAddress_State
  bill_address_state varchar(21),
  -- QB: BillAddress.PostalCode, OpenSync: BillAddress_PostalCode
  bill_address_postal_code varchar(13),
  -- QB: BillAddress.Country, OpenSync: BillAddress_Country
  bill_address_country varchar(31),
  -- QB: BillAddress.Note, OpenSync: BillAddress_Note
  bill_address_note varchar(41),
  -- QB: ShipAddress.Addr1, OpenSync: ShipAddress_Addr1
  ship_address_addr_1 varchar(41),
  -- QB: ShipAddress.Addr2, OpenSync: ShipAddress_Addr2
  ship_address_addr_2 varchar(41),
  -- QB: ShipAddress.Addr3, OpenSync: ShipAddress_Addr3
  ship_address_addr_3 varchar(41),
  -- QB: ShipAddress.Addr4, OpenSync: ShipAddress_Addr4
  ship_address_addr_4 varchar(41),
  -- QB: ShipAddress.Addr5, OpenSync: ShipAddress_Addr5
  ship_address_addr_5 varchar(41),
  -- QB: ShipAddress.City, OpenSync: ShipAddress_City
  ship_address_city varchar(31),
  -- QB: ShipAddress.State, OpenSync: ShipAddress_State
  ship_address_state varchar(21),
  -- QB: ShipAddress.PostalCode, OpenSync: ShipAddress_PostalCode
  ship_address_postal_code varchar(13),
  -- QB: ShipAddress.Country, OpenSync: ShipAddress_Country
  ship_address_country varchar(31),
  -- QB: ShipAddress.Note, OpenSync: ShipAddress_Note
  ship_address_note varchar(41),
  -- QB: Phone. Maps to Main Phone field in UI.
  phone character varying(21),
  -- QB: Fax
  fax character varying(21),
  -- QB: Email. In QB, seems to be max length 1023.
  email text,
  -- QB: NameOnCheck
  name_on_check varchar(41),
  -- QB: AccountNumber. This is the account number assigned by the bill to us.
  account_number varchar(99),
  -- QB: Notes. OpenSync only syncs first note. In QB, seems to be max length 4095.
  notes text,
  -- QB: BillTypeRefListID, OpenSync: BillTypeRef_ListID. Read-only for employees.
  bill_type_ref_list_id varchar(36),
  -- QB: BillTypeRefFullName, OpenSync: BillTypeRef_FullName
  bill_type_ref_full_name varchar(159),
  -- QB: TermsRefListID, OpenSync: TermsRef_ListID
  terms_ref_list_id varchar(36),
  -- QB: TermsRefFullName, OpenSync: TermsRef_FullName
  terms_ref_full_name varchar(31),
  -- QB: CreditLimit
  credit_limit numeric(10, 2),
  -- QB: BillTaxIdent
  bill_tax_ident varchar(15),
  -- QB: Balance. Not visible in form, but used for reports.
  balance numeric(10, 2),
  -- What action to take with this row during sync: add | update | delete. Can be NULL.
  sync_action varchar(6),
  -- if sync resulted in change to this row, then sync_local_update_ts is updated to current time
  sync_local_update_ts timestamp with time zone,
  -- if sync resulted in change to row in sync peer, then sync_remote_update_ts is updated to current time
  sync_remote_update_ts timestamp with time zone,
  -- electronic payment method: ach_push | wire | paypal | ach_pull | credit_card. Can be NULL.
  epay_method varchar(11),
  -- 9 digit routing number when epay_method is ach_push or wire. This is the bill bank account that we send money to.
  epay_push_routing_number varchar(9),
  -- bank account number (up to 17 digits) when epay_method is ach_push or wire. This is the bill bank account that we send money to.
  epay_push_account_number varchar(17),
  -- 9 digit routing number when epay_method is ach_pull. This is one of our bank accounts that bills pull money from.
  epay_pull_routing_number varchar(9),
  -- bank account number (up to 17 digits) when epay_method is ach_pull. This is one of our bank accounts that bills pull money from.
  epay_pull_account_number varchar(17),
  -- if epay_method is credit_card, then epay_card_number is the card used to pay (bill charges this card)
  epay_card_number varchar(16),
  -- epay_card_expire_date should be YYYY-MM-DD. If day is unknown, use first day of the month (1).
  epay_card_expire_date date,
  -- when epay_method is a PULL type (ach_pull or credit_card), epay_pull_statement_pattern can be a regex that describes what
  -- the charges look like on our bank statements or credit card statements. Otherwise should be NULL.
  epay_pull_statement_pattern varchar(255),
  -- epay email when epay_method is paypal
  epay_email varchar(255),
  -- URL of the bill web site
  web_site_url text,
  -- usual periodic payment amount. NULL means variable amount.
  usual_pmt_amt numeric(10, 2),
  -- usual payment amount in months 1 - 12 (Jan - Dec) for bills that fluctuate seasonally. NULL means variable amount.
  usual_pmt_amt_01 numeric(10, 2),
  usual_pmt_amt_02 numeric(10, 2),
  usual_pmt_amt_03 numeric(10, 2),
  usual_pmt_amt_04 numeric(10, 2),
  usual_pmt_amt_05 numeric(10, 2),
  usual_pmt_amt_06 numeric(10, 2),
  usual_pmt_amt_07 numeric(10, 2),
  usual_pmt_amt_08 numeric(10, 2),
  usual_pmt_amt_09 numeric(10, 2),
  usual_pmt_amt_10 numeric(10, 2),
  usual_pmt_amt_11 numeric(10, 2),
  usual_pmt_amt_12 numeric(10, 2),
  -- usual payment period (month | quarter | biannual | year). NULL means no usual amount with periodicity.
  usual_pmt_period varchar(8),
  -- 1 - 31
  pmt_day_of_month integer,
  -- start month when usual period is quarter, biannual, or year. NULL when usual_period is month or NULL.
  pmt_month_in_year varchar(9),
  -- high | medium | low | NULL. high priority bills get paid quickly.
  pmt_priority_level varchar(6),
  -- crypt_passwd is for the bill to log in to our bill web site
  -- 123 is max length of crypt(3) password with SHA-512. see http://www.akkadia.org/drepper/SHA-crypt.txt.
  -- format: $6$rounds=<n>$salt$digest
  --         \./\___..___/\_.._/\_.._/
  --          3     16      18    86
  -- $6$ specifies the SHA-512 algorithm
  -- <n> in rounds=<n> should be replaced with a positive integer from 1000 to 999999999. as of 2015, recommend 10000
  -- salt is a 16 character [a-zA-Z0-9/+.-] string containing the salt
  -- digest is the base64-encoded digest of the 64 byte SHA-512 digest (max is 86 bytes instead of 88 bytes because no padding)
  crypt_passwd character varying (123),
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE bill_tbl IS 'Bills with fields that can be synchronized with QuickBooks';
COMMENT ON COLUMN bill_tbl.company_id IS 'Company ID links to the company (entity) on our side that has a relationship with the bill';
COMMENT ON COLUMN bill_tbl.qb_list_id IS 'List ID is primary key in each company file, but there could be two rows with same List ID if they are in different QB company files';
COMMENT ON COLUMN bill_tbl.name IS 'Name is the label we give to the bill (will usually be the actual name of the bill, but does not have to be)';
COMMENT ON COLUMN bill_tbl.company_name IS 'If the bill is a corporate entity or partnership rather than an individual, the company name is the name of that entity';
COMMENT ON COLUMN bill_tbl.name_on_check IS 'Sometimes bills want payment to be made out to a particular name that is not the same as the company name or person name; if so, the name_on_check field should be set to that payee';
COMMENT ON COLUMN bill_tbl.account_number IS 'Account number is assigned by the bill to us (does not refer to our identifier of the bill)';
COMMENT ON COLUMN bill_tbl.bill_type_ref_full_name IS 'Type of the bill: Utility | Credit Card | Construction | Bank. Can be NULL if bill type is unknown or uncategorized.';
COMMENT ON COLUMN bill_tbl.bill_tax_ident IS 'Tax ID (EIN/SSN) of the bill';
COMMENT ON COLUMN bill_tbl.balance IS 'Current open balance with the bill; not writable or updatable; only readable from QB';
COMMENT ON COLUMN bill_tbl.sync_action IS 'Action to take when synchronizing: add | update | delete. Can be NULL if no sync action being initiated from this database to QuickBooks.';
COMMENT ON COLUMN bill_tbl.sync_local_update_ts IS 'If a sync resulted in a change to this local database, sync_local_update_ts tells when that update happened';
COMMENT ON COLUMN bill_tbl.sync_remote_update_ts IS 'If a sync resulted in a change to the remote database (QuickBooks), sync_remote_update_ts tells when that update happened';
COMMENT ON COLUMN bill_tbl.epay_method IS 'Method of electronic payment: ach_push | wire | paypal | ach_pull | credit_card. Can be NULL if no electronic payment.';
COMMENT ON COLUMN bill_tbl.epay_push_routing_number IS 'Bank routing number when epay_method is ach_push or wire. This is the bill bank account that we send money to.';
COMMENT ON COLUMN bill_tbl.epay_push_account_number IS 'Bank account number when epay_method is ach_push or wire. This is the bill bank account that we send money to.';
COMMENT ON COLUMN bill_tbl.epay_pull_routing_number IS 'Bank routing number when epay_method is ach_pull. This is one of our bank accounts that bills pull money from.';
COMMENT ON COLUMN bill_tbl.epay_pull_account_number IS 'Bank account number when epay_method is ach_pull. This is one of our bank accounts that bills pull money from.';
COMMENT ON COLUMN bill_tbl.epay_card_number IS 'Credit card number used if epay_method is credit_card';
COMMENT ON COLUMN bill_tbl.epay_card_expire_date IS 'Expiration date of card. Use 1 as day if no day on card.';
COMMENT ON COLUMN bill_tbl.epay_pull_statement_pattern IS 'When epay_method is a PULL type (ach_pull or credit_card), epay_pull_statement_pattern can be a regex that describes what the charges look like on our bank statements or credit card statements. Otherwise should be NULL.';
COMMENT ON COLUMN bill_tbl.epay_email IS 'Payment email when epay_method is paypal';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt IS 'Usual periodic amount due every month, quarter, 6 months, or year';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_01 IS 'Usual amount due in January for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_02 IS 'Usual amount due in February for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_03 IS 'Usual amount due in March for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_04 IS 'Usual amount due in April for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_05 IS 'Usual amount due in May for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_06 IS 'Usual amount due in June for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_07 IS 'Usual amount due in July for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_08 IS 'Usual amount due in August for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_09 IS 'Usual amount due in September for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_10 IS 'Usual amount due in October for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_11 IS 'Usual amount due in November for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_amt_12 IS 'Usual amount due in December for seasonally fluctuating bills';
COMMENT ON COLUMN bill_tbl.usual_pmt_period IS 'Usual recurrence period: month | quarter | biannual | year';
COMMENT ON COLUMN bill_tbl.pmt_day_of_month IS 'Which day of the month (1 - 31) the recurring amount is due when usual_amount and usual_period are both not null';
COMMENT ON COLUMN bill_tbl.pmt_month_in_year IS 'Start month of the cycle when usual_period is quarter or biannual, or the month in every year when usual_period is year: January | February | March | April | May | June | July | August | September | October | November | December';
COMMENT ON COLUMN bill_tbl.pmt_priority_level IS 'How important is timely payment of the bill bills: high | medium | low. Can be NULL if unknown or uncategorized.';

-- validate column values in bill_tbl
CREATE FUNCTION validate_bill() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
  epay_method_entered boolean = false;
BEGIN
  IF (NEW.email IS NOT NULL) THEN
    -- strip all spaces
    NEW.email = regexp_replace(NEW.email, '\s+', '', 'g');
    IF (NEW.email = '') THEN
      NEW.email = NULL;
    ELSIF (NEW.email !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'email must be a valid email address; you entered %', NEW.email;
    END IF;
  END IF;
  -- strip all spaces in bill_tax_ident
  NEW.bill_tax_ident = regexp_replace(NEW.bill_tax_ident, '\s+', '', 'g');
  IF NEW.bill_tax_ident = '' THEN
    NEW.bill_tax_ident = NULL;
  END IF;
  -- if bill_tax_ident IS NOT NULL, check next step to see if we should validate
  IF (NEW.bill_tax_ident IS NOT NULL) THEN
    -- if operation is INSERT or bill_tax_ident changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate if so
    IF ((TG_OP = 'INSERT') OR (NEW.bill_tax_ident IS DISTINCT FROM OLD.bill_tax_ident)) THEN
      IF (NEW.bill_tax_ident !~* '^\d[0-9-]+\d$') THEN
        RAISE EXCEPTION 'bill_tax_ident must contain only numbers and dashes; you entered %', NEW.bill_tax_ident;
      END IF;
    END IF;
  END IF;
  -- strip all spaces in sync_action
  NEW.sync_action = regexp_replace(NEW.sync_action, '\s+', '', 'g');
  IF NEW.sync_action = '' THEN
    NEW.sync_action = NULL;
  END IF;
  -- if sync_action IS NOT NULL, validate
  IF (NEW.sync_action IS NOT NULL) THEN
    IF (NEW.sync_action NOT IN ('add', 'update', 'delete')) THEN
      RAISE EXCEPTION 'sync_action must be NULL or one of add, update, or delete; you entered %', NEW.sync_action;
    END IF;
  END IF;
  -- strip all spaces in epay_method
  NEW.epay_method = regexp_replace(NEW.epay_method, '\s+', '', 'g');
  IF NEW.epay_method = '' THEN
    NEW.epay_method = NULL;
  END IF;
  -- if epay_method IS NOT NULL, validate format
  IF (NEW.epay_method IS NOT NULL) THEN
    IF (NEW.epay_method NOT IN ('ach_push', 'wire', 'paypal', 'ach_pull', 'credit_card')) THEN
      RAISE EXCEPTION 'epay_method must be NULL or one of ach_push, wire, paypal, ach_pull, or credit_card; you entered %', NEW.epay_method;
    END IF;
  END IF;
  -- strip all spaces in epay_push_routing_number
  NEW.epay_push_routing_number = regexp_replace(NEW.epay_push_routing_number, '\s+', '', 'g');
  IF (NEW.epay_push_routing_number = '') THEN
    NEW.epay_push_routing_number = NULL;
  END IF;
  -- if epay_push_routing_number IS NOT NULL, validate
  IF (NEW.epay_push_routing_number IS NOT NULL) THEN
    -- if operation is INSERT or epay_push_routing_number changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate in that case
    IF ((TG_OP = 'INSERT') OR (NEW.epay_push_routing_number IS DISTINCT FROM OLD.epay_push_routing_number)) THEN
      IF NEW.epay_push_routing_number !~ '^\d{9}$' THEN
        RAISE EXCEPTION 'epay_push_routing_number must be 9 digits; you entered %', NEW.epay_push_routing_number;
      END IF;
      IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.epay_push_routing_number) THEN
        RAISE EXCEPTION 'epay_push_routing_number not found in FedACH Participants; you entered %', NEW.epay_push_routing_number;
      END IF;
    END IF;
  END IF;
  -- strip all spaces in epay_push_account_number
  NEW.epay_push_account_number = regexp_replace(NEW.epay_push_account_number, '\s+', '', 'g');
  IF (NEW.epay_push_account_number = '') THEN
    NEW.epay_push_account_number = NULL;
  END IF;
  -- if epay_push_account_number IS NOT NULL, validate
  IF (NEW.epay_push_account_number IS NOT NULL) THEN
    -- if operation is INSERT or epay_push_account_number changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate in that case
    IF ((TG_OP = 'INSERT') OR (NEW.epay_push_account_number IS DISTINCT FROM OLD.epay_push_account_number)) THEN
      IF NEW.epay_push_account_number !~ '^\d+$' THEN
        RAISE EXCEPTION 'epay_push_account_number must be numeric; you entered %', NEW.epay_push_account_number;
      END IF;
    END IF;
  END IF;
  -- if epay_push_routing_number or epay_push_account_number are specified, then both must be specified
  IF (((NEW.epay_push_routing_number IS NOT NULL) AND (NEW.epay_push_account_number IS NULL)) OR ((NEW.epay_push_routing_number IS NULL) AND (NEW.epay_push_account_number IS NOT NULL))) THEN
    RAISE EXCEPTION 'epay_push_account_number and epay_push_routing_number must both be NULL or must both be specified; you only entered one';
  END IF;
  -- if epay_push_routing_number and epay_push_account_number are specified, check that epay_method is epay_push or wire
  IF ((NEW.epay_push_routing_number IS NOT NULL) AND (NEW.epay_push_account_number IS NOT NULL)) THEN
    epay_method_entered = true;
    IF (NEW.epay_method IS NULL) THEN
      RAISE EXCEPTION 'epay_method must be ach_push or wire if you specify epay_push_account_number and epay_push_routing_number; you did not enter a value for epay_method';
    ELSIF (NEW.epay_method NOT IN ('ach_push', 'wire')) THEN
      RAISE EXCEPTION 'epay_method must be ach_push or wire if you specify epay_push_account_number and epay_push_routing_number; you entered epay_method of %', NEW.epay_method;
    END IF;
  END IF;
  -- strip all spaces in epay_pull_routing_number
  NEW.epay_pull_routing_number = regexp_replace(NEW.epay_pull_routing_number, '\s+', '', 'g');
  IF (NEW.epay_pull_routing_number = '') THEN
    NEW.epay_pull_routing_number = NULL;
  END IF;
  -- if epay_pull_routing_number IS NOT NULL, validate
  IF (NEW.epay_pull_routing_number IS NOT NULL) THEN
    -- if operation is INSERT or epay_pull_routing_number changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate in that case
    IF ((TG_OP = 'INSERT') OR (NEW.epay_pull_routing_number IS DISTINCT FROM OLD.epay_pull_routing_number)) THEN
      IF NEW.epay_pull_routing_number !~ '^\d{9}$' THEN
        RAISE EXCEPTION 'epay_pull_routing_number must be 9 digits; you entered %', NEW.epay_pull_routing_number;
      END IF;
      IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.epay_pull_routing_number) THEN
        RAISE EXCEPTION 'epay_pull_routing_number not found in FedACH Participants; you entered %', NEW.epay_pull_routing_number;
      END IF;
    END IF;
  END IF;
  -- strip all spaces in epay_pull_account_number
  NEW.epay_pull_account_number = regexp_replace(NEW.epay_pull_account_number, '\s+', '', 'g');
  IF (NEW.epay_pull_account_number = '') THEN
    NEW.epay_pull_account_number = NULL;
  END IF;
  -- if epay_pull_account_number IS NOT NULL, validate
  IF (NEW.epay_pull_account_number IS NOT NULL) THEN
    -- if operation is INSERT or epay_pull_account_number changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate in that case
    IF ((TG_OP = 'INSERT') OR (NEW.epay_pull_account_number IS DISTINCT FROM OLD.epay_pull_account_number)) THEN
      IF NEW.epay_pull_account_number !~ '^\d+$' THEN
        RAISE EXCEPTION 'epay_pull_account_number must be numeric; you entered %', NEW.epay_pull_account_number;
      END IF;
    END IF;
  END IF;
  -- if epay_pull_routing_number or epay_pull_account_number are specified, then both must be specified
  IF (((NEW.epay_pull_routing_number IS NOT NULL) AND (NEW.epay_pull_account_number IS NULL)) OR ((NEW.epay_pull_routing_number IS NULL) AND (NEW.epay_pull_account_number IS NOT NULL))) THEN
    RAISE EXCEPTION 'epay_pull_account_number and epay_pull_routing_number must both be NULL or must both be specified; you only entered one';
  END IF;
  -- if epay_pull_routing_number and epay_pull_account_number are specified, check that epay_method is epay_pull
  IF ((NEW.epay_pull_routing_number IS NOT NULL) AND (NEW.epay_pull_account_number IS NOT NULL)) THEN
    epay_method_entered = true;
    IF (NEW.epay_method IS NULL) THEN
      RAISE EXCEPTION 'epay_method must be ach_pull if you specify epay_pull_account_number and epay_pull_routing_number; you did not enter a value for epay_method';
    ELSIF (NEW.epay_method != 'ach_pull') THEN
      RAISE EXCEPTION 'epay_method must be ach_pull if you specify epay_pull_account_number and epay_pull_routing_number; you entered epay_method of %', NEW.epay_method;
    END IF;
  END IF;
  -- strip all spaces in epay_card_number
  NEW.epay_card_number = regexp_replace(NEW.epay_card_number, '\s+', '', 'g');
  IF (NEW.epay_card_number = '') THEN
    NEW.epay_card_number = NULL;
  END IF;
  -- if epay_card_number IS NOT NULL, validate
  IF (NEW.epay_card_number IS NOT NULL) THEN
    -- if operation is INSERT or epay_card_number changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate in that case
    IF ((TG_OP = 'INSERT') OR (NEW.epay_card_number IS DISTINCT FROM OLD.epay_card_number)) THEN
      IF NEW.epay_card_number !~ '^\d{13,16}$' THEN
        RAISE EXCEPTION 'epay_card_number must be 13 to 16 digits; you entered %', NEW.epay_card_number;
      END IF;
      -- check validity of credit card number using Luhn algorithm
      IF (NOT is_valid_cc_luhn(NEW.epay_card_number)) THEN
        RAISE EXCEPTION 'epay_card_number is not a valid credit card number; you entered %', NEW.epay_card_number;
      END IF;
    END IF;
  END IF;
  -- if epay_card_number is specified, check that epay_method is credit_card
  IF (NEW.epay_card_number IS NOT NULL) THEN
    epay_method_entered = true;
    IF (NEW.epay_method IS NULL) THEN
      RAISE EXCEPTION 'epay_method must be credit_card if you specify epay_card_number; you did not enter a value for epay_method';
    ELSIF (NEW.epay_method != 'credit_card') THEN
      RAISE EXCEPTION 'epay_method must be credit_card if you specify epay_card_number; you entered epay_method of %', NEW.epay_method;
    END IF;
  END IF;
  -- if epay_method IS NOT NULL, but no epay details entered, then raise exception
  IF ((NEW.epay_method IS NOT NULL) AND (NOT epay_method_entered)) THEN
    RAISE EXCEPTION 'you must specify epay details (routing and account information, or credit card details) if you specify epay_method; if you do not want epay, make epay_method NULL';
  END IF;
  -- strip all spaces in epay_pull_statement_pattern
  NEW.epay_pull_statement_pattern = regexp_replace(NEW.epay_pull_statement_pattern, '\s+', '', 'g');
  IF (NEW.epay_pull_statement_pattern = '') THEN
    NEW.epay_pull_statement_pattern = NULL;
  END IF;
  -- if epay_pull_statement_pattern IS NOT NULL, check that epay_method was a PULL method (ach_pull or credit_card)
  IF (NEW.epay_pull_statement_pattern IS NOT NULL) THEN
    IF (NEW.epay_method IS NULL) THEN
      RAISE EXCEPTION 'epay_method must be ach_pull or credit_card if you specify epay_pull_statement_pattern; you did not enter a value for epay_method';
    ELSIF (NEW.epay_method NOT IN ('ach_pull', 'credit_card')) THEN
      RAISE EXCEPTION 'epay_method must be ach_pull or credit_card if you specify epay_pull_statement_pattern; you entered epay_method of %', NEW.epay_method;
    END IF;
  END IF;
  IF (NEW.epay_email IS NOT NULL) THEN
    -- strip all spaces
    NEW.epay_email = regexp_replace(NEW.epay_email, '\s+', '', 'g');
    IF (NEW.epay_email = '') THEN
      NEW.epay_email = NULL;
    ELSIF (NEW.epay_email !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'epay_email must be a valid email address; you entered %', NEW.epay_email;
    END IF;
  END IF;
  -- if usual_pmt_period IS NOT NULL, validate format
  IF (NEW.usual_pmt_period IS NOT NULL) THEN
    IF (NEW.usual_pmt_period NOT IN ('month', 'quarter', 'biannual', 'year')) THEN
      RAISE EXCEPTION 'usual_pmt_period must be NULL or one of month, quarter, biannual, or year; you entered %', NEW.usual_pmt_period;
    END IF;
  END IF;
  -- if pmt_day_of_month IS NOT NULL, validate
  IF (NEW.pmt_day_of_month IS NOT NULL) THEN
    IF (NEW.pmt_day_of_month < 1) OR (NEW.pmt_day_of_month > 31) THEN
      RAISE EXCEPTION 'pmt_day_of_month must be in the range 1 to 31; you entered %', NEW.pmt_day_of_month;
    END IF;
  END IF;
  -- if pmt_month_in_year IS NOT NULL, validate
  IF (NEW.pmt_month_in_year IS NOT NULL) THEN
    IF (NEW.pmt_month_in_year NOT IN ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')) THEN
      RAISE EXCEPTION 'pmt_month_in_year must be one of January, February, March, April, May, June, July, August, September, October, November, or December; you entered %', NEW.pmt_month_in_year;
    END IF;
  END IF;
  -- if pmt_priority_level IS NOT NULL, validate format
  IF (NEW.pmt_priority_level IS NOT NULL) THEN
    IF (NEW.pmt_priority_level NOT IN ('high', 'medium', 'low')) THEN
      RAISE EXCEPTION 'pmt_priority_level must be NULL or one of high, medium, or low; you entered %', NEW.pmt_priority_level;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

-- mask sensitive fields in bill_tbl
CREATE FUNCTION mask_bill() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.bill_tax_ident IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.bill_tax_ident IS DISTINCT FROM OLD.bill_tax_ident) THEN
      -- mask the tax id by keeping first digit and last two digits and changing remaining numbers to '*'
      NEW.bill_tax_ident = left(NEW.bill_tax_ident, 1) || regexp_replace(substr(NEW.bill_tax_ident, 2, length(NEW.bill_tax_ident) - 3), '\d', '*', 'g') || right(NEW.bill_tax_ident, 2);
    END IF;
  END IF;
  IF (NEW.epay_push_routing_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.epay_push_routing_number IS DISTINCT FROM OLD.epay_push_routing_number) THEN
      NEW.epay_push_routing_number = left(NEW.epay_push_routing_number, 1) || repeat('*', 5) || right(NEW.epay_push_routing_number, 3);
    END IF;
  END IF;
  IF (NEW.epay_push_account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.epay_push_account_number IS DISTINCT FROM OLD.epay_push_account_number) THEN
      IF length(NEW.epay_push_account_number) > 4 THEN
        NEW.epay_push_account_number = left(NEW.epay_push_account_number, 1) || repeat('*', length(NEW.epay_push_account_number) - 4) || right(NEW.epay_push_account_number, 3);
      END IF;
    END IF;
  END IF;
  IF (NEW.epay_pull_routing_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.epay_pull_routing_number IS DISTINCT FROM OLD.epay_pull_routing_number) THEN
      NEW.epay_pull_routing_number = left(NEW.epay_pull_routing_number, 1) || repeat('*', 5) || right(NEW.epay_pull_routing_number, 3);
    END IF;
  END IF;
  IF (NEW.epay_pull_account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.epay_pull_account_number IS DISTINCT FROM OLD.epay_pull_account_number) THEN
      IF length(NEW.epay_pull_account_number) > 4 THEN
        NEW.epay_pull_account_number = left(NEW.epay_pull_account_number, 1) || repeat('*', length(NEW.epay_pull_account_number) - 4) || right(NEW.epay_pull_account_number, 3);
      END IF;
    END IF;
  END IF;
  IF (NEW.epay_card_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.epay_card_number IS DISTINCT FROM OLD.epay_card_number) THEN
      NEW.epay_card_number = left(NEW.epay_card_number, 1) || repeat('*', length(NEW.epay_card_number) - 5) || right(NEW.epay_card_number, 4);
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON bill_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON bill_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON bill_tbl FOR EACH ROW EXECUTE PROCEDURE validate_bill();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON bill_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('bill_tax_ident', 'epay_push_routing_number', 'epay_push_account_number', 'epay_pull_routing_number', 'epay_pull_account_number', 'epay_card_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON bill_tbl FOR EACH ROW EXECUTE PROCEDURE mask_bill();

--DROP TABLE bill_tbl;
--DROP SEQUENCE bill_list_id_seq;
--DROP SEQUENCE bill_seq;
--DROP FUNCTION validate_bill();
--DROP FUNCTION mask_bill();

CREATE SEQUENCE building_seq;

CREATE TABLE building_tbl (
  id integer DEFAULT nextval('building_seq'::regclass) NOT NULL PRIMARY KEY,
  address_1 character varying(41) NOT NULL,
  address_2 character varying(41),
  city character varying(31) NOT NULL,
  state character varying(21),
  zip_code character varying(13),
  country character varying(31) NOT NULL,
  short_global_label character varying(31) NOT NULL,
  type character varying(60),
  longitude double precision,
  latitude double precision,
  timezone varchar(45),
  croissant_id varchar(32),
  owner_company_id integer REFERENCES company_tbl (id),
  share_company_id integer REFERENCES company_tbl (id),
  is_active boolean DEFAULT true NOT NULL,
  note text,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255),
  nickname character varying(32),
  essensys_location_id int
);

COMMENT ON TABLE building_tbl IS 'Buildings used, owned, or operated by Micro Office';
COMMENT ON COLUMN building_tbl.short_global_label IS 'Label that uniquely identifies the building and includes a prefix for state followed by an abbreviated address';
COMMENT ON COLUMN building_tbl.type IS 'officesuite or datacenter';
COMMENT ON COLUMN building_tbl.timezone IS 'Time zone in Unix zoneinfo format (e.g. America/New_York)';
COMMENT ON COLUMN building_tbl.owner_company_id IS 'Which Micro Office entity owns the building. Can be NULL if building is not owned by Micro Office entity.';
COMMENT ON COLUMN building_tbl.share_company_id IS 'Which Micro Office entity operates a shared office in the building. Can be NULL if Micro Office entity does not operate a shared office in the building.';

CREATE VIEW building_vw AS
 SELECT building_tbl.id,
    building_tbl.short_global_label,
    building_tbl.address_1,
    building_tbl.address_2,
    building_tbl.city,
    building_tbl.state,
    building_tbl.zip_code,
    building_tbl.country,
    building_tbl.type,
    owner_company_tbl.short_name AS owner_short_name,
    share_company_tbl.short_name AS share_short_name,
    building_tbl.timezone,
    building_tbl.is_active,
    building_tbl.croissant_id,
    building_tbl.note
   FROM ((building_tbl
     LEFT JOIN company_tbl owner_company_tbl ON (building_tbl.owner_company_id = owner_company_tbl.id))
     LEFT JOIN company_tbl share_company_tbl ON (building_tbl.share_company_id = share_company_tbl.id));

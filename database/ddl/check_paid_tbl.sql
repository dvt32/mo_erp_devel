CREATE SEQUENCE check_paid_seq;
CREATE TABLE check_paid_tbl (
  id integer DEFAULT nextval('check_paid_seq'::regclass) NOT NULL PRIMARY KEY,
  payor_company_id integer NOT NULL REFERENCES company_tbl (id),
  source_routing_number character(9) NOT NULL,
  source_account_number character varying(17) NOT NULL,
  -- payee_name: Chase allows 32 chars, CitiBank allows 22 chars
  payee_name varchar(32) NOT NULL,
  payee_table_column varchar(127),
  payee_fk_id integer,
  amount money NOT NULL,
  check_number integer,
  check_date date NOT NULL,
  -- note: Chase allows 80 chars.
  note varchar(80),
  -- Zero or more email addresses separated by commas. Send check paid transaction result to the email address(es).
  email text,
  -- when did the check clear (money debited from our account)?
  cleared_date date,
  -- was a stop placed on the check? true or false. when cleared_date is NULL and is_stopped is false, check is in transit or
  -- has not yet been deposited by the recipient.
  is_stopped boolean DEFAULT false NOT NULL,
  requester_id integer NOT NULL REFERENCES employee_tbl (id),
  notify_requester boolean DEFAULT true NOT NULL,
  approver_id integer REFERENCES employee_tbl (id),
  notify_approver boolean DEFAULT false NOT NULL,
  txn_number varchar(32),
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE check_paid_tbl IS 'Check paid transactions (i.e. payments to vendors, contractors, and other business partners)';
COMMENT ON COLUMN check_paid_tbl.payor_company_id IS 'The company in the Micro Office family that issued the payment';
COMMENT ON COLUMN check_paid_tbl.source_routing_number IS 'The routing number of one of our bank accounts to send money from';
COMMENT ON COLUMN check_paid_tbl.source_account_number IS 'The account number of one of our bank accounts to send money from';
COMMENT ON COLUMN check_paid_tbl.payee_name IS 'Legal name of the payee';
COMMENT ON COLUMN check_paid_tbl.payee_table_column IS 'Table and column that link to this row, in <table>.<column> format; can be vendor_tbl.id, govt_filing.id, or other table and column, or can be NULL if no linkage';
COMMENT ON COLUMN check_paid_tbl.payee_fk_id IS 'Foreign key ID of linked table; can be NULL';
COMMENT ON COLUMN check_paid_tbl.email IS 'Zero or more email addresses separated by commas. Send ACH credit transaction result to the email address(es).';
COMMENT ON COLUMN check_paid_tbl.cleared_date IS 'Date when the check cleared (i.e. money debited from our account)';
COMMENT ON COLUMN check_paid_tbl.is_stopped IS 'true | false: a stop been placed on the check; when cleared_date is NULL and is_stopped is false, check is in transit or has not yet been deposited by the recipient.';
COMMENT ON COLUMN check_paid_tbl.requester_id IS 'ID of the employee who is requesting the ACH credit';
COMMENT ON COLUMN check_paid_tbl.notify_requester IS 'true | false: send an email to requester when ACH credit is processed';
COMMENT ON COLUMN check_paid_tbl.approver_id IS 'ID of the employee who approved the ACH credit';
COMMENT ON COLUMN check_paid_tbl.notify_approver IS 'true | false: send an email to approver when ACH credit is processed';
COMMENT ON COLUMN check_paid_tbl.txn_number IS 'Transaction number or reference number supplied by the bank for the transaction';

-- validate column values in check_paid_tbl
CREATE FUNCTION validate_check_paid() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  -- strip all spaces in source_routing_number
  NEW.source_routing_number = strip_space(NEW.source_routing_number);
  -- if operation is INSERT or source_routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.source_routing_number IS DISTINCT FROM OLD.source_routing_number)) THEN
    IF NEW.source_routing_number IS NULL THEN
      RAISE EXCEPTION 'source_routing_number is required';
    END IF;
    IF NEW.source_routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'source_routing_number must be 9 digits; you entered %', NEW.source_routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.source_routing_number) THEN
      RAISE EXCEPTION 'source_routing_number not found in FedACH Participants; you entered %', NEW.source_routing_number;
    END IF;
  END IF;
  -- strip all spaces in source_account_number
  NEW.source_account_number = strip_space(NEW.source_account_number);
  -- if operation is INSERT or source_account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.source_account_number IS DISTINCT FROM OLD.source_account_number)) THEN
    IF NEW.source_account_number IS NULL THEN
      RAISE EXCEPTION 'source_account_number is required';
    END IF;
    IF NEW.source_account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'source_account_number must be numeric; you entered %', NEW.source_account_number;
    END IF;
  END IF;
  -- strip all leading and trailing spaces in payee_name
  NEW.payee_name = strip_collapse_space(NEW.payee_name);
  -- strip all spaces in payee_table_column
  NEW.payee_table_column = strip_space(NEW.payee_table_column);
  NEW.note = strip_collapse_space(NEW.note);
  NEW.email = strip_space(lower(NEW.email));
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  -- strip all spaces in txn_number
  NEW.txn_number = strip_space(NEW.txn_number);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in check_paid_tbl
CREATE FUNCTION mask_check_paid() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.source_account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.source_account_number IS DISTINCT FROM OLD.source_account_number) THEN
      IF length(NEW.source_account_number) > 4 THEN
        NEW.source_account_number = left(NEW.source_account_number, 1) || repeat('*', length(NEW.source_account_number) - 4) || right(NEW.source_account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON check_paid_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON check_paid_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON check_paid_tbl FOR EACH ROW EXECUTE PROCEDURE validate_check_paid();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON check_paid_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('source_account_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON check_paid_tbl FOR EACH ROW EXECUTE PROCEDURE mask_check_paid();

-- set up audit log for check_paid_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON check_paid_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON check_paid_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('check_paid_tbl');

CREATE VIEW check_paid_vw AS
  SELECT check_paid_tbl.id,
    check_paid_tbl.payor_company_id,
    company_tbl.name AS payor_company_name,
    check_paid_tbl.source_routing_number,
    check_paid_tbl.source_account_number,
    check_paid_tbl.payee_name,
    check_paid_tbl.amount,
    check_paid_tbl.check_number,
    check_paid_tbl.check_date,
    check_paid_tbl.note,
    check_paid_tbl.email,
    check_paid_tbl.cleared_date,
    check_paid_tbl.is_stopped,
    (requester_employee_tbl.last_name)::text || ', '::text || (requester_employee_tbl.first_name)::text AS requester_name,
    check_paid_tbl.notify_requester,
    (approver_employee_tbl.last_name)::text || ', '::text || (approver_employee_tbl.first_name)::text AS approver_name,
    check_paid_tbl.notify_approver,
    check_paid_tbl.txn_number,
    check_paid_tbl.payee_table_column,
    check_paid_tbl.payee_fk_id
  FROM (((check_paid_tbl
    JOIN employee_tbl requester_employee_tbl ON ((check_paid_tbl.requester_id = requester_employee_tbl.id)))
    JOIN employee_tbl approver_employee_tbl ON ((check_paid_tbl.approver_id = approver_employee_tbl.id)))
    JOIN company_tbl ON ((check_paid_tbl.payor_company_id = company_tbl.id)));

--DROP VIEW check_paid_vw;
--DROP TABLE check_paid_tbl;
--DROP SEQUENCE check_paid_seq;
--DROP FUNCTION validate_check_paid();
--DROP FUNCTION mask_check_paid();

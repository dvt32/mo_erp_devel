CREATE TYPE company_activity_type AS ENUM ('own_property', 'share_office', 'other');
CREATE SEQUENCE company_seq;
CREATE TABLE company_tbl (
  id integer DEFAULT nextval(('company_seq'::text)::regclass) NOT NULL,
  name character varying(64) NOT NULL,
  short_name character varying(64) NOT NULL,
  ownership numeric NOT NULL,
  tax_id character varying(10) NOT NULL,
  building_id integer REFERENCES building_tbl (id),
  activity company_activity_type NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

-- validate column values in company_tbl
CREATE FUNCTION validate_company() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.name = strip_collapse_space(NEW.name);
  NEW.short_name = strip_collapse_space(NEW.short_name);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in company_tbl
CREATE FUNCTION mask_company() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.tax_id IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.tax_id IS DISTINCT FROM OLD.tax_id) THEN
      -- mask the tax id by keeping first digit and last two digits and changing remaining numbers to '*'
      NEW.tax_id = left(NEW.tax_id, 1) || regexp_replace(substr(NEW.tax_id, 2, length(NEW.tax_id) - 3), '\d', '*', 'g') || right(NEW.tax_id, 2);
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON company_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON company_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON company_tbl FOR EACH ROW EXECUTE PROCEDURE validate_company();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON company_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('tax_id');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON company_tbl FOR EACH ROW EXECUTE PROCEDURE mask_company();

CREATE VIEW company_vw AS
  SELECT company_tbl.id,
  company_tbl.name,
  company_tbl.short_name,
  company_tbl.ownership,
  company_tbl.tax_id,
  building_tbl.short_global_label AS building_short_name,
  company_tbl.building_id,
  company_tbl.activity,
  company_tbl.note,
  company_tbl.is_active,
  company_tbl.crypt_json,
  company_tbl.created_ts,
  company_tbl.created_user,
  company_tbl.modified_ts,
  company_tbl.modified_user
  FROM (company_tbl
    LEFT JOIN building_tbl ON company_tbl.building_id = building_tbl.id);

--DROP VIEW company_vw;
--DROP TABLE company_tbl;
--DROP SEQUENCE company_seq;
--DROP FUNCTION validate_company();
--DROP FUNCTION mask_company();
--DROP TYPE company_activity_type;

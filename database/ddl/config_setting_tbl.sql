CREATE TABLE config_setting_tbl (
  name varchar(255) NOT NULL PRIMARY KEY,
  value text NOT NULL,
  note text,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE config_setting_tbl IS 'Configuration settings for application';
CREATE TRIGGER update_modified_config_setting BEFORE UPDATE ON config_setting_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created_config_setting BEFORE INSERT ON config_setting_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE config_setting_tbl;

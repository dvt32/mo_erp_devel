CREATE SEQUENCE contact_form_seq;

CREATE TABLE contact_form_tbl (
  id integer DEFAULT nextval('contact_form_seq'::regclass) NOT NULL PRIMARY KEY,
  dst_emails text[] NOT NULL,
  fields text[] NOT NULL,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE contact_form_tbl IS 'Table to store contact form ids information.';
COMMENT ON COLUMN contact_form_tbl.dst_emails IS 'Email to send contact form results to.';
COMMENT ON COLUMN contact_form_tbl.fields IS 'Fields beyond the basic required ones in this form.';

CREATE TRIGGER update_modified BEFORE UPDATE ON contact_form_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON contact_form_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON contact_form_tbl FOR EACH ROW EXECUTE PROCEDURE validate_contact_form_email();

CREATE FUNCTION validate_contact_form_email() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
  e varchar;
BEGIN
  FOREACH e in ARRAY NEW.dst_emails
  LOOP
    e = strip_collapse_space(e);
    IF (e IS NOT NULL) AND (e !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'dst_emails must be a valid email address; you entered %', e;
    END IF;
  END LOOP;
  RETURN NEW;
END;
$$;



CREATE SEQUENCE contact_form_response_seq;

CREATE TABLE contact_form_response_tbl (
  id integer DEFAULT nextval('contact_form_response_seq'::regclass) NOT NULL PRIMARY KEY,
  form_id integer REFERENCES contact_form_tbl (id),
  http_referer text,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE contact_form_response_tbl IS 'table mapping responses to form ids.';

CREATE TRIGGER update_modified BEFORE UPDATE ON contact_form_response_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON contact_form_response_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();


CREATE TABLE contact_form_response_val_tbl (
  response_id integer REFERENCES contact_form_response_tbl (id) NOT NULL,
  field_name text NOT NULL,
  field_value text NOT NULL,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE contact_form_response_val_tbl IS 'generic table mapping field name to field values for form response ids.';

CREATE TRIGGER update_modified BEFORE UPDATE ON contact_form_response_val_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON contact_form_response_val_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE contact_form_response_val_tbl;
--DROP TABLE contact_form_response_tbl;
--DROP SEQUENCE contact_form_response_seq;
--DROP FUNCTION validate_contact_form_email;
--DROP TABLE contact_form_tbl;
--DROP SEQUENCE contact_form_seq;
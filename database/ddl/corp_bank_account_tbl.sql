-- run corp_pmt_account.sql commands first.

CREATE TABLE corp_bank_account_tbl (
  id integer DEFAULT nextval('corp_pmt_account_seq'::regclass) NOT NULL PRIMARY KEY,
  company_id integer NOT NULL REFERENCES company_tbl (id),
  nickname varchar(64),
  -- 31 max length of bank in QuickBooks
  depository_name varchar(31),
  address_1 varchar(41),
  address_2 varchar(41),
  city varchar(31),
  state varchar(21),
  zip_code varchar(13),
  country varchar(31),
  -- contact details of main contact person at the bank
  first_name varchar(25),
  last_name varchar(25),
  job_title varchar(25),
  phone_number varchar(21),
  fax_number varchar(21),
  email varchar(64),
  -- account_type: checking or savings
  account_type character varying(8) NOT NULL REFERENCES enum_bank_account_type_tbl (account_type),
  routing_number character(9) NOT NULL,
  account_number character varying(17) NOT NULL,
  is_active boolean DEFAULT true NOT NULL,
  note text,
  view_id_list integer[],
  edit_id_list integer[],
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE corp_bank_account_tbl IS 'Corporate bank accounts for Micro Office entities';
COMMENT ON COLUMN corp_bank_account_tbl.company_id IS 'Which Micro Office entity controls and owns the bank account';
COMMENT ON COLUMN corp_bank_account_tbl.nickname IS 'Nickname of the bank account';
COMMENT ON COLUMN corp_bank_account_tbl.depository_name IS 'Name of the bank';
COMMENT ON COLUMN corp_bank_account_tbl.address_1 IS 'Address of branch where account was opened';
COMMENT ON COLUMN corp_bank_account_tbl.first_name IS 'First name of contact person at bank';
COMMENT ON COLUMN corp_bank_account_tbl.account_type IS 'checking or savings';
COMMENT ON COLUMN corp_bank_account_tbl.routing_number IS 'ACH routing number';
COMMENT ON COLUMN corp_bank_account_tbl.account_number IS 'Bank account number';
COMMENT ON COLUMN corp_bank_account_tbl.view_id_list IS 'List of ID values of employees who can view this bank account.';
COMMENT ON COLUMN corp_bank_account_tbl.edit_id_list IS 'List of ID values of employees who can edit this bank account. Can edit most of the fields, apart from metadata fields or crypt_json.';

-- validate column values in corp_bank_account_tbl
CREATE FUNCTION validate_corp_bank_account() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar := '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.nickname = strip_collapse_space(NEW.nickname);
  NEW.depository_name = strip_collapse_space(NEW.depository_name);
  NEW.address_1 = strip_collapse_space(NEW.address_1);
  NEW.address_2 = strip_collapse_space(NEW.address_2);
  NEW.city = strip_collapse_space(NEW.city);
  NEW.state = strip_collapse_space(NEW.state);
  NEW.zip_code = strip_collapse_space(NEW.zip_code);
  NEW.country = strip_collapse_space(NEW.country);
  NEW.first_name = strip_collapse_space(NEW.first_name);
  NEW.last_name = strip_collapse_space(NEW.last_name);
  NEW.job_title = strip_collapse_space(NEW.job_title);
  NEW.phone_number = strip_collapse_space(NEW.phone_number);
  NEW.fax_number = strip_collapse_space(NEW.fax_number);
  NEW.email = strip_space(NEW.email);
  IF ((NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern)) THEN
    RAISE EXCEPTION 'email must be a valid email address; you entered %', NEW.email;
  END IF;
  NEW.account_type = strip_collapse_space(NEW.account_type);
  NEW.routing_number = strip_collapse_space(NEW.routing_number);
  -- if operation is INSERT or routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.routing_number IS DISTINCT FROM OLD.routing_number)) THEN
    IF NEW.routing_number IS NULL THEN
      RAISE EXCEPTION 'routing_number is required';
    END IF;
    IF NEW.routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'routing_number must be 9 digits; you entered %', NEW.routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.routing_number) THEN
      RAISE EXCEPTION 'routing_number not found in FedACH Participants; you entered %', NEW.routing_number;
    END IF;
  END IF;
  NEW.account_number = strip_collapse_space(NEW.account_number);
  -- if operation is INSERT or account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number)) THEN
    IF NEW.account_number IS NULL THEN
      RAISE EXCEPTION 'account_number is required';
    END IF;
    IF NEW.account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'account_number must be numeric; you entered %', NEW.account_number;
    END IF;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in corp_bank_account_tbl
CREATE FUNCTION mask_corp_bank_account() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number) THEN
      IF length(NEW.account_number) > 4 THEN
        NEW.account_number = left(NEW.account_number, 1) || repeat('*', length(NEW.account_number) - 4) || right(NEW.account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('corp_pmt_account_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('corp_pmt_account_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('corp_pmt_account_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE validate_corp_bank_account();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('account_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON corp_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE mask_corp_bank_account();

-- set up audit log for corp_bank_account_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON corp_bank_account_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON corp_bank_account_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('corp_bank_account_tbl');

--DROP TABLE corp_bank_account_tbl;
--DROP FUNCTION validate_corp_bank_account();
--DROP FUNCTION mask_corp_bank_account();

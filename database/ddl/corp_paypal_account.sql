-- run corp_pmt_account.sql commands first.

CREATE TABLE corp_paypal_account_tbl (
  id integer DEFAULT nextval('corp_pmt_account_seq'::regclass) NOT NULL PRIMARY KEY,
  company_id integer NOT NULL REFERENCES company_tbl (id),
  nickname varchar(64) NOT NULL,
  email character varying(127) NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  view_id_list integer[],
  edit_id_list integer[],
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE corp_paypal_account_tbl IS 'PayPal accounts for Micro Office entities.';
COMMENT ON COLUMN corp_paypal_account_tbl.company_id IS 'Which Micro Office entity controls and owns the PayPal account.';
COMMENT ON COLUMN corp_paypal_account_tbl.nickname IS 'Nickname of the PayPal account.';
COMMENT ON COLUMN corp_paypal_account_tbl.email IS 'Email address associated with PayPal account.';
COMMENT ON COLUMN corp_paypal_account_tbl.view_id_list IS 'List of ID values of employees who can view this PayPal account.';
COMMENT ON COLUMN corp_paypal_account_tbl.edit_id_list IS 'List of ID values of employees who can edit this PayPal account. Can edit most of the fields, apart from metadata fields or crypt_json.';

-- validate column values in corp_paypal_account_tbl
CREATE FUNCTION validate_corp_paypal_account() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar := '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.nickname = strip_collapse_space(NEW.nickname);
  NEW.email = strip_space(NEW.email);
  IF NEW.email !~* email_regexp_pattern THEN
    RAISE EXCEPTION 'email must be a valid email address; you entered %', NEW.email;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON corp_paypal_account_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('corp_pmt_account_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON corp_paypal_account_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('corp_pmt_account_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON corp_paypal_account_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('corp_pmt_account_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON corp_paypal_account_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON corp_paypal_account_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON corp_paypal_account_tbl FOR EACH ROW EXECUTE PROCEDURE validate_corp_paypal_account();

-- set up audit log for corp_paypal_account_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON corp_paypal_account_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON corp_paypal_account_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('corp_paypal_account_tbl');

--DROP TABLE corp_paypal_account_tbl;
--DROP FUNCTION validate_corp_paypal_account();

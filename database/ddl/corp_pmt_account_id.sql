CREATE SEQUENCE corp_pmt_account_seq;

CREATE TABLE corp_pmt_account_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  -- Name of table that uses the ID from this row: corp_bank_account_tbl | corp_card_account_tbl | corp_paypal_account_tbl
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE corp_pmt_account_id_tbl IS 'Common table to reserve ID values for corp_x_account_tbl tables.';
COMMENT ON COLUMN corp_pmt_account_id_tbl.tbl_name IS 'Name of table that uses the ID from this row.';

--DROP SEQUENCE corp_pmt_account_seq;

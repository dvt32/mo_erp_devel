CREATE VIEW corp_pmt_account_vw AS
 SELECT corp_bank_account_tbl.id,
    company_tbl.id AS company_id,
    company_tbl.short_name AS company_short_name,
    company_tbl.name AS company_long_name,
    corp_bank_account_tbl.nickname,
    corp_bank_account_tbl.account_type,
    corp_bank_account_tbl.routing_number,
    corp_bank_account_tbl.account_number,
    NULL AS email,
    corp_bank_account_tbl.is_active,
    corp_bank_account_tbl.note,
    corp_bank_account_tbl.crypt_json,
    corp_bank_account_tbl.created_ts,
    corp_bank_account_tbl.created_user,
    corp_bank_account_tbl.modified_ts,
    corp_bank_account_tbl.modified_user
   FROM corp_bank_account_tbl,
    company_tbl
  WHERE (corp_bank_account_tbl.company_id = company_tbl.id)
UNION ALL
 SELECT corp_paypal_account_tbl.id,
    company_tbl.id AS company_id,
    company_tbl.short_name AS company_short_name,
    company_tbl.name AS company_long_name,
    corp_paypal_account_tbl.nickname,
    NULL AS account_type,
    NULL AS routing_number,
    NULL AS account_number,
    corp_paypal_account_tbl.email,
    corp_paypal_account_tbl.is_active,
    corp_paypal_account_tbl.note,
    corp_paypal_account_tbl.crypt_json,
    corp_paypal_account_tbl.created_ts,
    corp_paypal_account_tbl.created_user,
    corp_paypal_account_tbl.modified_ts,
    corp_paypal_account_tbl.modified_user
   FROM corp_paypal_account_tbl,
    company_tbl
  WHERE (corp_paypal_account_tbl.company_id = company_tbl.id);

--DROP VIEW corp_pmt_account_vw;

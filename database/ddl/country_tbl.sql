CREATE TABLE country_tbl (
  name character varying(80) NOT NULL UNIQUE,
  iso_2 character(2) NOT NULL UNIQUE,
  iso_3 character(3) NOT NULL UNIQUE,
  iso_num character(3) NOT NULL PRIMARY KEY,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE country_tbl IS 'All the countries in the world, along with ISO-3166-1 alpha-2, alpha-3, and numeric codes';
COMMENT ON COLUMN country_tbl.name IS 'Name of the country';
COMMENT ON COLUMN country_tbl.iso_2 IS 'ISO 3166-1 alpha-2 two letter country code';
COMMENT ON COLUMN country_tbl.iso_3 IS 'ISO 3166-1 alpha-3 three letter country code';
COMMENT ON COLUMN country_tbl.iso_num IS 'ISO 3166-1 three digit numeric country code';

--DROP TABLE country_tbl;

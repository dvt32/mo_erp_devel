CREATE EXTENSION IF NOT EXISTS plperl WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plperl IS 'PL/Perl procedural language';
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;
COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';
CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';

-- Filter out duplicates in an array and return an array with only unique elements
CREATE FUNCTION anyarray_uniq(with_array anyarray) RETURNS anyarray
    LANGUAGE plpgsql
    AS $$
DECLARE
  -- The variable used to track iteration over "with_array".
  loop_offset integer;
  -- The array to be returned by this function.
  return_array with_array%TYPE := '{}';
BEGIN
  IF with_array IS NULL THEN
    RETURN NULL;
  END IF;
  -- Iterate over each element in "concat_array".
  FOR loop_offset IN ARRAY_LOWER(with_array, 1)..ARRAY_UPPER(with_array, 1) LOOP
    IF NOT with_array[loop_offset] = ANY(return_array) THEN
      return_array = ARRAY_APPEND(return_array, with_array[loop_offset]);
    END IF;
  END LOOP;
  RETURN return_array;
END;
$$;

-- check if credit card number is valid using Luhn algorithm
CREATE FUNCTION is_valid_cc_luhn(card_number varchar) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
  card_length integer;
  is_even_length integer;
  digit integer;
  checksum integer;
BEGIN
  card_length = length(card_number);
  IF (card_length = 0) THEN
    RETURN false;
  END IF;
  is_even_length = 1 - mod(card_length, 2);
  checksum = 0;
  FOR i IN 1..card_length LOOP
    digit = substr(card_number, i, 1)::integer;
    -- we take mod of i + is_even_length because what we actually want is to double those digits that are in even positions
    -- counting from the right, not counting from the left, but for convenience we are looping through from the left
    IF (mod(i + is_even_length, 2) = 0) THEN
      digit = digit * 2;
      -- if doubling the digit causes it to become a two digit number, add up the individual digits
      IF (digit > 9) THEN
        digit = 1 + mod(digit, 10);
      END IF;
    END IF;
    checksum = checksum + digit;
  END LOOP;
  IF (mod(checksum, 10) = 0) THEN
    RETURN true;
  END IF;
  RETURN false;
END;
$$;

-- generate created_ts and created_user upon INSERT
-- discard values for modified_ts and modified_user on INSERT
CREATE FUNCTION insert_created() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.created_ts := now();
  NEW.created_user := current_user;
  -- prevent the user from setting values for modified_ts and modified_user
  NEW.modified_ts := NULL;
  NEW.modified_user := NULL;
  RETURN NEW;
END;
$$;

-- update modified_ts and modified_user upon UPDATE
-- protect created_ts and created_user from change on UPDATE
CREATE FUNCTION update_modified() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  -- discard any changes to created_ts and created_user by overwriting them with old values
  -- thus they always remain at the initial values upon INSERT
  NEW.created_ts := OLD.created_ts;
  NEW.created_user := OLD.created_user;
  -- discard any changes to modified_ts and modified_user by overwriting them with old values
  -- thus any changes to just modified_ts and modified_user do not result in treating the row as actually modified
  NEW.modified_ts := OLD.modified_ts;
  NEW.modified_user := OLD.modified_user;
  -- if any columns changed, update modified_ts and modified_user
  IF row(NEW.*) IS DISTINCT FROM row(OLD.*) THEN
    NEW.modified_ts := now();
    NEW.modified_user := current_user;
  END IF;
  RETURN NEW;
END;
$$;

-- encrypt certain columns in a table (passed as parameters when creating this trigger) and store values in crypt_json column
/*
Contents of crypt_json column:
{
  "columns" : {
    "field1" : {
      "public_key_id" : 1, (reference to public_key_tbl.id)
      "crypt_value" : "..." (armored data value encrypted with public key),
    },
    "field2" : {
      "symmetric_key_id" : 2, (reference to symmetric_key_tbl.id),
      "crypt_value" : "..." (armored data value encrypted with symmetric key),
    },
    ...
  },
}
*/
CREATE OR REPLACE FUNCTION crypt_row() RETURNS trigger
    LANGUAGE plperl
    AS $$
  my @columns_to_crypt;
  if ($_TD->{"argc"} > 0) {
    @columns_to_crypt = @{$_TD->{"args"}};
  } else {
    die "no columns to crypt specified in trigger " . $_TD->{"name"};
  }
  # count of how many crypted columns were modified
  my $mod_count = 0;
  for my $column (@columns_to_crypt) {
    if (!exists($_TD->{"new"}{$column})) {
      die qq{non-existent column ($column) specified in trigger $_TD->{"name"}};
    }
    my $old_value = $_TD->{"old"}{$column};
    my $new_value = $_TD->{"new"}{$column};
    # comparing $old_value to $new_value will result in a warning if either is undef
    # so first check if defined status different; if so, increment mod_count
    # if defined status is same, then either both are defined or both are undefined
    # if both undefined, then both are same and mod_count should not be incremented
    # if old_value is defined (meaning both are defined), then compare old_value and new_value and increment mod_count if different
    if ((defined($old_value) != defined($new_value)) ||
        (defined($old_value) && ($old_value ne $new_value))) {
      $mod_count++;
    }
  }
  # if none of the crypted columns were modified, we have no work to do
  # check if crypt_json was modified, and if so, revert crypt_json if user lacks column UPDATE privilege
  my ($result_value, $row);
  if (!$mod_count) {
    my $old_value = $_TD->{"old"}{"crypt_json"};
    my $new_value = $_TD->{"new"}{"crypt_json"};
    if ((defined($old_value) != defined($new_value)) ||
        (defined($old_value) && ($old_value ne $new_value))) {
      my $table_name = $_TD->{"table_name"};
      $result_value = spi_exec_query("select has_column_privilege('$table_name', 'crypt_json', 'UPDATE')");
      $row = $result_value->{"rows"}[0];
      # check if user has column UPDATE privilege on crypt_json
      if (!$row->{"has_column_privilege"}) {
        # crypt_json column was modified; revert to old value since end user did not have permission to modify crypt_json
        $_TD->{"new"}{"crypt_json"} = $_TD->{"old"}{"crypt_json"};
        return "MODIFY";
      }
    }
    # either no change to crypt_json or user has column UPDATE privilege, so proceed with update
    return;
  }
  my $crypt_json;
  if ($_TD->{"event"} eq "UPDATE") {
    if (exists($_TD->{"old"}{"crypt_json"})) {
      $crypt_json = $_TD->{"old"}{"crypt_json"};
    } else {
      die qq{no crypt_json column in table $_TD->{"table_name"} during call to trigger $_TD->{"name"}};
    }
  }
  use JSON;
  my $crypt_hash;
  if (defined($crypt_json) && ($crypt_json =~ /^\s*{.*}\s*$/s)) {
    $crypt_hash = JSON::decode_json($crypt_json);
  } else {
    $crypt_hash = { "columns" => {} };
  }
  $result_value = spi_exec_query("select value from config_setting_tbl where name = 'public_key_id'");
  $row = $result_value->{"rows"}[0];
  if (!$row) {
    die "public_key_id in config_setting_tbl is missing; please define it";
  }
  my $public_key_id = int($row->{"value"});
  if (!$public_key_id) {
    die "public_key_id in config_setting_tbl is invalid; please define it as a row ID";
  }
  # check if public key exists
  $result_value = spi_exec_query("select 1 as public_key_exists from public_key_tbl where id = $public_key_id");
  if ($result_value->{"processed"} == 0) {
    die "could not find public_key_tbl row with ID $public_key_id";
  }
  my $query_plan = spi_prepare("select armor(pgp_pub_encrypt(\$1, (select public_key from public_key_tbl where id = $public_key_id))) as armored_ciphertext", "text");
  for my $column (@columns_to_crypt) {
    if (!defined($_TD->{"new"}{$column})) {
      # if new value of column is undef (i.e. NULL) and crypt_json had old value, delete old value
      if (exists($crypt_hash->{"columns"}->{$column})) {
        delete $crypt_hash->{"columns"}->{$column};
      }
    } else {
      # if new value of column is same as old value, skip to next column because the new value is likely masked (not full contents)
      # we do not want to overwrite crypted value with masked value (which would lose data)
      if (defined($_TD->{"old"}{$column}) && ($_TD->{"new"}{$column} eq $_TD->{"old"}{$column})) {
        next;
      }
      $result_value = spi_exec_prepared($query_plan, $_TD->{"new"}{$column});
      $row = $result_value->{"rows"}[0];
      $crypt_hash->{"columns"}->{$column} = {
        "public_key_id" => $public_key_id,
        "crypt_value" => $row->{"armored_ciphertext"},
      };
    }
  }
  # if all encrypted columns are NULL, then set crypt_json to NULL as well
  if (($crypt_hash->{"columns"}) && (scalar(keys(%{$crypt_hash->{"columns"}})) > 0)) {
    $_TD->{"new"}{"crypt_json"} = JSON::encode_json($crypt_hash);
  } else {
    $_TD->{"new"}{"crypt_json"} = undef;
  }
  spi_freeplan($query_plan);
  return "MODIFY";
$$;

-- strip leading and trailing spaces and collapse multiple spaces in middle to single space
-- optional 2nd argument specifies whether to set value to NULL if it ends up being the empty string (''); defaults to true
CREATE FUNCTION strip_collapse_space(textvar text, blank_to_null boolean DEFAULT true) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN
  textvar = regexp_replace(textvar, '^\s+', '', 'g');
  textvar = regexp_replace(textvar, '\s+$', '', 'g');
  textvar = regexp_replace(textvar, '\s+', ' ', 'g');
  IF (blank_to_null AND textvar = '') THEN
    textvar = NULL;
  END IF;
  RETURN textvar;
END;
$$;

-- strip all spaces
-- optional 2nd argument specifies whether to set value to NULL if it ends up being the empty string (''); defaults to true
CREATE FUNCTION strip_space(textvar text, blank_to_null boolean DEFAULT true) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN
  textvar = regexp_replace(textvar, '\s+', '', 'g');
  IF (blank_to_null AND textvar = '') THEN
    textvar = NULL;
  END IF;
  RETURN textvar;
END;
$$;

-- reserve an ID value in id reservation table for coordination between multiple tables sharing one pool of ID values
-- first argument is the table containing ID values: id, tbl_name
-- second argument is the table that wants to use the ID values (must have id column)
CREATE FUNCTION reserve_id() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  reservation_tbl text := TG_ARGV[0];
BEGIN
  EXECUTE format('INSERT INTO %I (id, tbl_name) VALUES ($1, $2)', reservation_tbl) USING NEW.id, TG_TABLE_NAME;
  RETURN NEW;
END;
$$;

-- change an ID reservation in id reservation table for coordination between multiple tables sharing one pool of ID values
-- first argument is the table containing ID values: id, tbl_name
-- second argument is the table that wants to use the ID values (must have id column)
CREATE FUNCTION change_id_reservation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  reservation_tbl text := TG_ARGV[0];
BEGIN
  IF OLD.id != NEW.id THEN
    EXECUTE format('UPDATE %I SET id = $2, tbl_name = $3, created_ts = now() WHERE id = $1', reservation_tbl) USING OLD.id, NEW.id, TG_TABLE_NAME;
  END IF;
  RETURN NEW;
END;
$$;

-- remove/free ID reservation value in id reservation table for coordination between multiple tables sharing one pool of ID values
-- first argument is the table containing ID values: id, tbl_name
CREATE FUNCTION free_id_reservation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  reservation_tbl text := TG_ARGV[0];
BEGIN
  EXECUTE format('DELETE FROM %I WHERE id = $1', reservation_tbl) USING OLD.id;
  RETURN OLD;
END;
$$;

-- Determine the current time at time zone for a desk
CREATE FUNCTION desk_local_now(desk_id integer) RETURNS date
  LANGUAGE plpgsql
  AS $$
BEGIN
  RETURN now() AT TIME ZONE (SELECT timezone FROM building_tbl WHERE id IN (SELECT building_id FROM floor_tbl WHERE id IN (SELECT floor_id FROM desk_tbl WHERE id = desk_id)));
END;
$$;


-- Sets row in pmt_vault_card_tbl or pmt_vault_ach_tbl to inactive
-- This function is exposed to mo_apply so that direct update permissions are not required
CREATE FUNCTION deactivate_pmt_vault(pmt_vault_id INT) RETURNS void
  LANGUAGE plpgsql
  AS $$
BEGIN
  UPDATE pmt_vault_card_tbl SET is_active = FALSE WHERE id = $1;
  UPDATE pmt_vault_ach_tbl SET is_active = FALSE WHERE id = $1;
END;
$$ SECURITY DEFINER;

-- Updates row in cust_pers_tbl password, hashed password
-- this function is exposed to mo_apply so that direct update permissions are not required
CREATE FUNCTION update_cust_pw(passwd text, crypt_passwd text, email text) RETURNS void
  LANGUAGE plpgsql
  AS $$
BEGIN
  UPDATE cust_pers_tbl SET pay_ws_passwd = $1, pay_ws_cryptpasswd = $2 WHERE cust_pers_tbl.email = $3;
END;
$$ SECURITY DEFINER;

-- deal with lack of = operator for (polygon, polygon) in PostgreSQL 9.4
-- may become unnecessary in future version of PostgreSQL if (=, polygon, polygon) operator added as built-in
-- see http://www.postgresql.org/docs/9.4/static/functions-geometry.html

CREATE FUNCTION cmp_polygons(polygon, polygon) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
BEGIN
  IF $1 IS NULL OR $2 IS NULL THEN
    RETURN NULL;
  END IF;
  IF $1 ~= $2 THEN
    RETURN TRUE;
  END IF;
  RETURN FALSE;
END;
$_$;

CREATE OPERATOR = (
    PROCEDURE = cmp_polygons,
    LEFTARG = polygon,
    RIGHTARG = polygon,
    COMMUTATOR = =,
    NEGATOR = <>
);

-- deal with lack of = operator for (xml, xml) in PostgreSQL 9.4
-- may become unnecessary in future version of PostgreSQL if (=, xml, xml) operator added as built-in
-- see http://www.postgresql.org/docs/9.5/static/datatype-xml.html

CREATE FUNCTION cmp_xml(xml, xml) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
BEGIN
  IF $1 IS NULL OR $2 IS NULL THEN
    RETURN NULL;
  END IF;
  IF xmlserialize(DOCUMENT $1 AS text) = xmlserialize(DOCUMENT $2 AS text) THEN
    RETURN TRUE;
  END IF;
  RETURN FALSE;
END;
$_$;

CREATE OPERATOR = (
    PROCEDURE = cmp_xml,
    LEFTARG = xml,
    RIGHTARG = xml,
    COMMUTATOR = =,
    NEGATOR = <>
);

-- validate column values in cust_pers_tbl
CREATE FUNCTION validate_cust_pers()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  NEW.email = strip_collapse_space(lower(NEW.email));
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be NULL or a valid email address; you entered %', NEW.email;
  END IF;
  NEW.pay_ws_passwd = strip_space(NEW.pay_ws_passwd);
  RETURN NEW;
END;
$function$

CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON cust_pers_tbl FOR EACH ROW EXECUTE PROCEDURE validate_cust_pers();

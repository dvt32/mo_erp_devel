CREATE SEQUENCE daypass_seq;

CREATE TABLE daypass_tbl (
    id integer DEFAULT nextval('daypass_seq'::regclass) NOT NULL PRIMARY KEY,
    cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
    date_of_service date,
    is_redeemed boolean DEFAULT false,
    note text,
    num_people integer,
    floor_id integer NOT NULL REFERENCES floor_tbl (id),
    txn_id integer REFERENCES pmt_vault_txn_tbl (id),
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

CREATE TRIGGER update_modified BEFORE UPDATE ON daypass_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON daypass_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

CREATE VIEW daypass_vw AS
 SELECT d.id,
    d.date_of_service,
    d.is_redeemed,
    d.floor_id,
    d.num_people,
    d.note,
    p.email,
    (((p.first_name)::text || ' '::text) || (p.last_name)::text) AS name
   FROM ((daypass_tbl d
     LEFT JOIN cust_acct_tbl c ON ((d.cust_acct_id = c.id)))
     LEFT JOIN cust_pers_tbl p ON ((c.primary_contact_cust_pers_id = p.id)));

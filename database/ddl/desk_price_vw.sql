CREATE SEQUENCE desk_price_seq;

CREATE TABLE desk_price_tbl (
    id integer DEFAULT nextval('desk_price_seq'::regclass) NOT NULL PRIMARY KEY,
    desk_id integer NOT NULL,
    price numeric(8,2) NOT NULL,
    internet_revenue numeric(8,2) DEFAULT 0 NOT NULL,
    internet_lines INT DEFAULT 1 NOT NULL,
    phone_revenue numeric(8,2) DEFAULT 0 NOT NULL,
    phone_lines INT DEFAULT 1 NOT NULL,
    portal_lines INT DEFAULT 1 NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    account_id integer NOT NULL,
    is_monthly boolean DEFAULT false NOT NULL,
    note text,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255),
    is_active boolean DEFAULT true NOT NULL
);

CREATE TRIGGER update_modified BEFORE UPDATE ON desk_price_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON desk_price_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

ALTER TABLE ONLY desk_price_tbl
    ADD CONSTRAINT desk_price_tbl_desk_id_fkey FOREIGN KEY (desk_id) REFERENCES desk_tbl(id);

-- desk_price_vw gets active desk_price_tbl rows which have start_date/end_date between today
-- complications occur in retrieving increase_date
-- solved by using a subquery for each case (next price exists / does not exist)
CREATE VIEW desk_price_vw AS
  SELECT
    desks_with_increase_date.id,
    desks_with_increase_date.desk_id,
    desks_with_increase_date.price,
    desks_with_increase_date.start_date,
    desks_with_increase_date.end_date,
    desks_with_increase_date.account_id,
    desks_with_increase_date.increase_date,
    desks_with_increase_date.is_monthly,
    desks_with_increase_date.internet_revenue,
    desks_with_increase_date.phone_revenue
  FROM (
    SELECT DISTINCT ON (current_price.id)
      current_price.id,
      current_price.desk_id,
      current_price.price,
      current_price.start_date,
      current_price.end_date,
      current_price.account_id,
      next_price.start_date AS increase_date,
      current_price.is_monthly,
      current_price.internet_revenue,
      current_price.phone_revenue
    FROM
      desk_price_tbl current_price,
      desk_price_tbl next_price
    WHERE
      desk_local_now(current_price.desk_id) BETWEEN current_price.start_date AND current_price.end_date
      AND next_price.start_date >= current_price.end_date
      AND current_price.desk_id = next_price.desk_id
      AND current_price.account_id = next_price.account_id
      AND current_price.is_active
      AND next_price.is_active
    ORDER BY current_price.id, next_price.start_date
  ) desks_with_increase_date
  UNION ALL
  SELECT
    current_price.id,
    current_price.desk_id,
    current_price.price,
    current_price.start_date,
    current_price.end_date,
    current_price.account_id,
    NULL::date AS increase_date,
    current_price.is_monthly,
    current_price.internet_revenue,
    current_price.phone_revenue
  FROM
    desk_price_tbl current_price
  WHERE
    desk_local_now(current_price.desk_id) BETWEEN current_price.start_date AND current_price.end_date
    AND current_price.is_active
    AND (NOT (EXISTS ( SELECT 1
      FROM
        desk_price_tbl next_price
      WHERE
        next_price.start_date >= current_price.end_date
        AND next_price.desk_id = current_price.desk_id
        AND next_price.account_id = current_price.account_id
        AND next_price.is_active)));

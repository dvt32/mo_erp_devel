CREATE SEQUENCE desk_sale_interaction_seq;

CREATE TABLE desk_sale_interaction_tbl (
    id integer DEFAULT nextval('desk_sale_interaction_seq'::regclass) NOT NULL PRIMARY KEY,
    desk_id integer NOT NULL,
    employee_id integer NOT NULL,
    date date NOT NULL,
    note text NOT NULL
);

ALTER TABLE ONLY desk_sale_interaction_tbl
    ADD CONSTRAINT desk_sale_interaction_tbl_desk_id_fkey FOREIGN KEY (desk_id) REFERENCES desk_tbl(id);

ALTER TABLE ONLY desk_sale_interaction_tbl
    ADD CONSTRAINT desk_sale_interaction_tbl_employee_id_fkey FOREIGN KEY (employee_id) REFERENCES employee_tbl(id);

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE desk_sale_interaction_tbl TO mo_operator;
GRANT USAGE ON SEQUENCE desk_sale_interaction_seq TO mo_operator;

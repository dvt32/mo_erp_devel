CREATE TYPE unit_system_type AS ENUM ('imperial', 'metric');

CREATE TABLE desk_tbl (
    id integer DEFAULT nextval('space_seq'::regclass) NOT NULL PRIMARY KEY,
    floor_id integer NOT NULL REFERENCES floor_tbl (id),
    assigned_number integer NOT NULL,
    account_id integer,
    num_people integer NOT NULL,
    is_enclosed boolean DEFAULT false NOT NULL,
    list_price numeric(8,2) DEFAULT 495 NOT NULL,
    when_available date,
    bounding_polygon polygon NOT NULL,
    -- imperial (inches) or metric (meters) units for bounding_polygon
    unit_system unit_system_type DEFAULT 'imperial' NOT NULL,
    note text,
    waiting_account integer,
    current_price numeric(8,2) DEFAULT 0 NOT NULL,
    start_date date,
    end_date date,
    internet_telephone_revenue numeric(8,2) DEFAULT 0 NOT NULL,
    increase_date date,
    cust_pers_id integer,
    modifier_id integer,
    modified timestamp with time zone,
    is_active boolean DEFAULT true NOT NULL,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255),
    reviewed_by_manager boolean default false not null
);

--
-- Name: desk_tbl_floor_id_key; Type: CONSTRAINT; Schema: public; Owner: wchao; Tablespace:
--

ALTER TABLE ONLY desk_tbl
    ADD CONSTRAINT desk_tbl_floor_id_key UNIQUE (floor_id, assigned_number);

--
-- Name: update_modified_desk; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_desk BEFORE UPDATE ON desk_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_desk; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_desk BEFORE INSERT ON desk_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

CREATE TRIGGER reserve_id BEFORE INSERT ON desk_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('space_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON desk_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('space_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON desk_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('space_id_tbl');

--DROP TABLE desk_tbl;
--DROP TYPE unit_system_type;

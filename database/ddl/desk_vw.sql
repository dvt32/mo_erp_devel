CREATE VIEW desk_vw AS
 SELECT desk_tbl.id,
    desk_tbl.floor_id,
    desk_tbl.assigned_number,
    desk_price_vw.account_id,
    desk_tbl.num_people,
    desk_tbl.is_enclosed,
    desk_tbl.list_price,
    desk_tbl.when_available,
    desk_tbl.bounding_polygon,
    desk_tbl.note,
    desk_tbl.is_active,
    desk_tbl.waiting_account,
        CASE
            WHEN (desk_price_vw.price IS NULL) THEN (0)::numeric(8,2)
            ELSE desk_price_vw.price
        END AS current_price,
    desk_price_vw.start_date,
    desk_price_vw.end_date,
    desk_price_vw.increase_date,
    ((desk_price_vw.phone_revenue + desk_price_vw.phone_revenue))::numeric(8,2) AS internet_telephone_revenue,
    desk_tbl.cust_pers_id,
    desk_price_vw.internet_revenue,
    desk_price_vw.phone_revenue
   FROM (desk_tbl
     LEFT JOIN desk_price_vw ON ((desk_price_vw.desk_id = desk_tbl.id)));

CREATE OR REPLACE VIEW desk_area_vw AS
 SELECT desk_tbl.id,
    desk_tbl.floor_id,
    floor_tbl.short_global_label,
    desk_tbl.assigned_number,
    desk_price_tbl.account_id,
    account_vw.name AS account_name,
    desk_tbl.is_enclosed,
    desk_tbl.list_price,
    desk_tbl.bounding_polygon,
    (area(path(desk_tbl.bounding_polygon)) / (144)::double precision) AS desk_area,
    ((floor_tbl.base_rev_psf / 12) * (area(path(desk_tbl.bounding_polygon)) / (144)::double precision)) AS computed_price
   FROM ((desk_tbl
     JOIN floor_tbl ON ((desk_tbl.floor_id = floor_tbl.id)))
     LEFT JOIN desk_price_tbl ON (((desk_tbl.id = desk_price_tbl.desk_id) AND ((desk_local_now(desk_tbl.id) BETWEEN desk_price_tbl.start_date AND desk_price_tbl.end_date) AND desk_price_tbl.is_active)))
     LEFT JOIN account_vw ON ((desk_price_tbl.account_id = account_vw.id)))
  WHERE ((desk_tbl.is_active IS TRUE) AND (floor_tbl.is_active IS TRUE));

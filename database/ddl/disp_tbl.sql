CREATE SEQUENCE disp_seq;

CREATE TABLE disp_tbl (
    id integer DEFAULT nextval('disp_seq'::regclass) NOT NULL PRIMARY KEY,
    tbl_name character varying(64),
    columns character varying(128),
    filter character varying(128),
    order_by character varying(32),
    asc_desc character varying(4),
    lim integer,
    rows_per_page integer,
    page integer
);

GRANT SELECT,INSERT,UPDATE ON TABLE disp_tbl TO mo_operator;
GRANT SELECT,USAGE ON SEQUENCE disp_seq TO mo_operator;

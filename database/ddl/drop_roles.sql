-- drop all roles except postgres
do $$
declare rolename text;
begin
  for rolename in select rolname from pg_roles where rolname <> 'postgres'
  loop
    execute 'DROP ROLE ' || quote_ident(rolename);
  end loop;
end
$$;

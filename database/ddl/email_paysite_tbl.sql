CREATE SEQUENCE email_paysite_seq;

CREATE TABLE email_paysite_tbl (
  id integer DEFAULT nextval('email_paysite_seq'::regclass) NOT NULL PRIMARY KEY,
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  from_addr text NOT NULL,
  to_addr text NOT NULL,
  subject text NOT NULL,
  plain_text text NOT NULL,
  html_text text,
  type text,
  attach bytea[],
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE email_paysite_tbl IS 'Store emails sent to users on creation and updating of recurring payments and scheduled payments, for easy website rendering and reference';

CREATE FUNCTION validate_email_paysite() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  NEW.from_addr = strip_collapse_space(NEW.from_addr);
  IF (NEW.from_addr IS NOT NULL) AND (NEW.from_addr !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'from email must be zero or more valid email addresses separated by commas; you entered %', NEW.from_addr;
  END IF;
  NEW.to_addr = strip_collapse_space(NEW.to_addr);
  IF (NEW.to_addr IS NOT NULL) AND (NEW.to_addr !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'to email must be zero or more valid email addresses separated by commas; you entered %', NEW.to_addr;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON email_paysite_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON email_paysite_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON email_paysite_tbl FOR EACH ROW EXECUTE PROCEDURE validate_email_paysite();

CREATE VIEW email_paysite_vw AS
 SELECT email_paysite_tbl.id,
    floor_tbl.short_global_label AS floor_label,
    email_paysite_tbl.cust_acct_id,
    cust_acct_tbl.name AS cust_acct_name,
    email_paysite_tbl.from_addr,
    email_paysite_tbl.to_addr,
    email_paysite_tbl.subject,
    email_paysite_tbl.plain_text,
    email_paysite_tbl.html_text,
    email_paysite_tbl.type,
    email_paysite_tbl.attach,
    email_paysite_tbl.created_ts,
    email_paysite_tbl.created_user,
    email_paysite_tbl.modified_ts,
    email_paysite_tbl.modified_user
   FROM (email_paysite_tbl
     JOIN cust_acct_tbl ON ((email_paysite_tbl.cust_acct_id = cust_acct_tbl.id))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id)));

--DROP VIEW email_paysite_vw;
--DROP TABLE email_paysite_tbl;
--DROP FUNCTION validate_email_paysite();
--DROP SEQUENCE email_paysite_seq;
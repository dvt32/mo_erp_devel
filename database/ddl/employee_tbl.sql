CREATE TABLE employee_tbl (
    id integer DEFAULT nextval('person_seq'::regclass) NOT NULL PRIMARY KEY,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(32),
    last_name character varying(48) NOT NULL,
    nickname character varying(32),
    email character varying(64),
    work_phone_number character varying(32),
    work_fax_number character varying(32),
    home_phone_number character varying(32),
    cell_phone_number character varying(32),
    other_phone_number character varying(32),
    title character varying(80),
    company character varying(128),
    work_address_1 character varying(128),
    work_address_2 character varying(128),
    work_city character varying(64),
    work_state character varying(32),
    work_zip_code character varying(16),
    work_country character varying(64),
    home_address_1 character varying(128),
    home_address_2 character varying(128),
    home_city character varying(64),
    home_state character varying(32),
    home_zip_code character varying(16),
    home_country character varying(64),
    birthday date,
    start_date date,
    end_date date,
    tax_id character varying(16),
    is_active boolean DEFAULT true NOT NULL,
    note text,
    email2 character varying(64),
    email3 character varying(64),
    type character varying(16) DEFAULT 'employee'::character varying NOT NULL,
    in_directory boolean DEFAULT true NOT NULL,
    password character varying(32),
    suffix character varying(32),
    allowed_ip_addr character varying(15),
    phone_pin character varying(16) DEFAULT ''::character varying NOT NULL,
    vacation_days integer DEFAULT 5 NOT NULL,
    has_assigned_task boolean DEFAULT false NOT NULL,
    windows_logon_name character varying(20),
    windows_domain_name character varying(15),
    windows_upn_suffix character varying(255),
    hash character(60),
    salt character(29),
    modifier_id integer,
    reload_roles boolean DEFAULT false NOT NULL,
    modified timestamp with time zone,
    in_pay_period_report boolean DEFAULT true NOT NULL,
    do_email_reminder boolean DEFAULT true NOT NULL,
    time_zone character varying(32) DEFAULT 'America/New_York'::character varying NOT NULL,
    pg_username character varying(63),
    pg_password character varying(63),
    crypt_json jsonb,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255),
    UNIQUE (windows_logon_name, windows_domain_name)
);

COMMENT ON COLUMN employee_tbl.start_date IS 'Date the employee started employment';
COMMENT ON COLUMN employee_tbl.end_date IS 'Date the employee ended employment';
COMMENT ON COLUMN employee_tbl.created_user IS 'User who created the row';
COMMENT ON COLUMN employee_tbl.modified_user IS 'User who last modified the row';

-- validate column values in employee_tbl
CREATE FUNCTION validate_employee() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  IF (NEW.email IS NOT NULL) THEN
    -- strip all spaces
    NEW.email = regexp_replace(NEW.email, '\s+', '', 'g');
    IF (NEW.email = '') THEN
      NEW.email = NULL;
    ELSIF (NEW.email !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'email must be a valid email address; you entered %', NEW.email;
    END IF;
  END IF;
  IF (NEW.email2 IS NOT NULL) THEN
    -- strip all spaces
    NEW.email2 = regexp_replace(NEW.email2, '\s+', '', 'g');
    IF (NEW.email2 = '') THEN
      NEW.email2 = NULL;
    ELSIF (NEW.email2 !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'email2 must be a valid email address; you entered %', NEW.email2;
    END IF;
  END IF;
  IF (NEW.email3 IS NOT NULL) THEN
    -- strip all spaces
    NEW.email3 = regexp_replace(NEW.email3, '\s+', '', 'g');
    IF (NEW.email3 = '') THEN
      NEW.email3 = NULL;
    ELSIF (NEW.email3 !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'email3 must be a valid email address; you entered %', NEW.email3;
    END IF;
  END IF;
  -- strip all spaces in tax_id
  NEW.tax_id = regexp_replace(NEW.tax_id, '\s+', '', 'g');
  IF NEW.tax_id = '' THEN
    NEW.tax_id = NULL;
  END IF;
  -- if tax_id IS NOT NULL, check next step to see if we should validate
  IF (NEW.tax_id IS NOT NULL) THEN
    -- if operation is INSERT or tax_id changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate if so
    IF ((TG_OP = 'INSERT') OR (NEW.tax_id IS DISTINCT FROM OLD.tax_id)) THEN
      IF (NEW.tax_id !~* '^\d[0-9-]+\d$') THEN
        RAISE EXCEPTION 'tax_id must contain only numbers and dashes; you entered %', NEW.tax_id;
      END IF;
    END IF;
  END IF;
  -- strip all spaces in pg_password
  NEW.pg_password = regexp_replace(NEW.pg_password, '\s+', '', 'g');
  IF NEW.pg_password = '' THEN
    NEW.pg_password = NULL;
  END IF;
  RETURN NEW;
END;
$$;

-- mask sensitive fields in employee_tbl
CREATE FUNCTION mask_employee() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.birthday IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.birthday IS DISTINCT FROM OLD.birthday) THEN
      -- mask the year of the birthdate by making year 1900
      NEW.birthday = make_date(1900, date_part('month', NEW.birthday)::int, date_part('day', NEW.birthday)::int)::date;
    END IF;
  END IF;
  IF (NEW.tax_id IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.tax_id IS DISTINCT FROM OLD.tax_id) THEN
      -- mask the tax id by keeping first digit and last two digits and changing remaining numbers to '*'
      NEW.tax_id = left(NEW.tax_id, 1) || regexp_replace(substr(NEW.tax_id, 2, length(NEW.tax_id) - 3), '\d', '*', 'g') || right(NEW.tax_id, 2);
    END IF;
  END IF;
  IF (NEW.pg_password IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.pg_password IS DISTINCT FROM OLD.pg_password) THEN
      -- mask pg_password by setting it to '***'
      NEW.pg_password = '***';
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified_employee BEFORE UPDATE ON employee_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created_employee BEFORE INSERT ON employee_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_employee BEFORE INSERT OR UPDATE ON employee_tbl FOR EACH ROW EXECUTE PROCEDURE validate_employee();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt_employee BEFORE INSERT OR UPDATE ON employee_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('birthday', 'tax_id', 'pg_password');
CREATE TRIGGER z2_mask_employee BEFORE INSERT OR UPDATE ON employee_tbl FOR EACH ROW EXECUTE PROCEDURE mask_employee();

--DROP FUNCTION validate_employee();
--DROP FUNCTION mask_employee();
--DROP TABLE employee_tbl;

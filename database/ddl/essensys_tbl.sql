CREATE TABLE essensys_license_tbl (
    id integer NOT NULL,
    account_id integer,
    account_name character varying(255),
    status character varying(64),
    location_id integer,
    items text,
    start_date timestamp with time zone,
    end_date timestamp with time zone
);


ALTER TABLE ONLY essensys_license_tbl
    ADD CONSTRAINT essensys_license_tbl_pkey PRIMARY KEY (id);

CREATE VIEW essensys_virtual_license_vw AS
SELECT    essensys_license_tbl.id,
          account_id,
          account_name,
          building_tbl.id AS building_id
FROM      essensys_license_tbl
LEFT JOIN building_tbl
ON        building_tbl.essensys_location_id = essensys_license_tbl.location_id
WHERE	  now() BETWEEN start_date AND end_date
AND	  items ILIKE '%virtual%'
AND       status ILIKE '%activated%';

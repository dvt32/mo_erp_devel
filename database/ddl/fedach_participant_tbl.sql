CREATE TABLE fedach_participant_tbl (
    -- The institution's routing number
    routing_number character(9) NOT NULL,
    -- Main office or branch O=main B=branch
    office_code character(1) NOT NULL,
    -- Servicing Fed's main office routing number
    servicing_frb_number character(9) NOT NULL,
    -- The code indicating the ABA number to be used to route or send ACH items to the RFI
    -- 0 = Institution is a Federal Reserve Bank
    -- 1 = Send items to customer routing number
    -- 2 = Send items to customer using new routing number field
    record_type_code character(1) NOT NULL,
    -- date of last change to CRF information
    change_date date,
    -- Institution's new routing number resulting from a merger or renumber
    new_routing_number character(9),
    -- Commonly used abbreviated name
    customer_name varchar(36) NOT NULL,
    -- Delivery address
    address varchar(36),
    -- City name in the delivery address
    city varchar(20),
    -- State code of the state in the delivery address
    state_code character(2),
    -- Zip code in the delivery address
    zip_code character(9),
    -- Phone number of the CRF
    phone_number character(10),
    -- Code is based on the customer's receiver code
    -- 1 = Receives Gov/Comm
    institution_status_code character(1),
    -- 1 = Current view
    data_view_code character(1),
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    modified_ts timestamp with time zone,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_user character varying(255)
);

ALTER TABLE fedach_participant_tbl OWNER TO wchao;

--
-- Name: fedach_participant_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: wchao; Tablespace:
--

ALTER TABLE ONLY fedach_participant_tbl
    ADD CONSTRAINT fedach_participant_tbl_pkey PRIMARY KEY (routing_number);

--
-- Name: update_modified_fedach_participant; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_fedach_participant BEFORE UPDATE ON fedach_participant_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_fedach_participant; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_fedach_participant BEFORE INSERT ON fedach_participant_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE fedach_participant_tbl;

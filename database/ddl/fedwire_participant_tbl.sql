CREATE TABLE fedwire_participant_tbl (
    routing_number character(9) NOT NULL,
    telegraphic_name varchar(18) NOT NULL,
    customer_name varchar(36) NOT NULL,
    state_abbrev character(2),
    city varchar(25),
    funds_transfer_eligible boolean NOT NULL,
    funds_settlement_only boolean NOT NULL,
    book_entry_securities_transfer_eligible boolean NOT NULL,
    date_of_last_revision date,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    modified_ts timestamp with time zone,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_user character varying(255)
);

ALTER TABLE fedwire_participant_tbl OWNER TO wchao;

--
-- Name: fedwire_participant_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: wchao; Tablespace:
--

ALTER TABLE ONLY fedwire_participant_tbl
    ADD CONSTRAINT fedwire_participant_tbl_pkey PRIMARY KEY (routing_number);

--
-- Name: update_modified_fedwire_participant; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_fedwire_participant BEFORE UPDATE ON fedwire_participant_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_fedwire_participant; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_fedwire_participant BEFORE INSERT ON fedwire_participant_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE fedwire_participant_tbl;

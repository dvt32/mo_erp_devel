CREATE SEQUENCE floor_seq;

CREATE TABLE floor_tbl (
    id integer DEFAULT nextval('floor_seq'::regclass) NOT NULL,
    building_id integer NOT NULL,
    label character varying(32) NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    bounding_polygon path NOT NULL,
    short_global_label character varying(64) NOT NULL,
    short_local_label character varying(16),
    office_manager_id integer,
    map_height integer,
    map_width integer,
    dom_payment_due integer,
    dom_billing_cycle_starts integer,
    base_rev_psf money,
    vo_monthly_fee numeric(5,2),
    vo_setup_fee numeric(5,2),
    vo_phone_fee numeric(5,2),
    tax_rate numeric(5,3),
    has_vo_service boolean DEFAULT false NOT NULL,
    pmt_notify_email text,
    has_daypass_service boolean DEFAULT false NOT NULL,
    daypass_fee numeric(5,2),
    vo_mail_forwarding_fee numeric(5,2),
    vo_mail_scanning_fee numeric(5,2)
);

ALTER TABLE ONLY floor_tbl
    ADD CONSTRAINT floor_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);

ALTER TABLE ONLY floor_tbl
    ADD CONSTRAINT floor_tbl_pkey PRIMARY KEY (id);

COMMENT ON COLUMN floor_tbl.office_manager_id IS 'office_manager_id is a link to employee_tbl';

-- Validate emails are comma separated and stripped/lower cased
CREATE FUNCTION validate_floor() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  emails_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  NEW.pmt_notify_email = strip_space(lower(NEW.pmt_notify_email));
  IF (NEW.pmt_notify_email IS NOT NULL) AND (NEW.pmt_notify_email !~* emails_regexp_pattern) THEN
    RAISE EXCEPTION 'pmt_notify_email must be zero or more valid email addresses separated by commas; you entered %', NEW.pmt_notify_email;
  END IF;
  RETURN NEW;
END;
$$;
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON floor_tbl FOR EACH ROW EXECUTE PROCEDURE validate_floor();

CREATE SEQUENCE investor_seq;

CREATE TABLE investor_tbl (
    id integer DEFAULT nextval(('investor_seq'::text)::regclass) NOT NULL PRIMARY KEY,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(32),
    last_name character varying(64) NOT NULL,
    -- investor_name is the name on the promissory note; can be corporate name
    investor_name character varying(80) NOT NULL,
    tax_id_number character varying(32),
    address_1 character varying(128),
    address_2 character varying(128),
    city character varying(64),
    state character varying(32),
    zip_code character varying(16),
    country character varying(80),
    work_phone_number character varying(32),
    work_fax_number character varying(32),
    home_phone_number character varying(32),
    cell_phone_number character varying(32),
    email character varying(64),
    -- person who referred this investor
    referrer character varying(64),
    note text,
    email2 character varying(64),
    email3 character varying(64),
    is_active boolean DEFAULT true NOT NULL,
    password character varying(32),
    primary_bank_account_id integer REFERENCES investor_bank_account_tbl (id),
    class_a_units numeric(12,4),
    initial_capital_contribution money,
    crypt_json jsonb,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

ALTER TABLE ONLY investor_tbl ADD CONSTRAINT investor_tbl_email_key UNIQUE (email);
COMMENT ON TABLE investor_tbl IS 'Equity and debt investors';
COMMENT ON COLUMN investor_tbl.referrer IS 'person who referred this investor';
COMMENT ON COLUMN investor_tbl.created_user IS 'User who created the row';
COMMENT ON COLUMN investor_tbl.modified_user IS 'User who last modified the row';

-- validate column values in investor_tbl
CREATE FUNCTION validate_investor() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  IF (NEW.email IS NOT NULL) THEN
    -- strip all spaces
    NEW.email = regexp_replace(NEW.email, '\s+', '', 'g');
    IF (NEW.email = '') THEN
      NEW.email = NULL;
    ELSIF (NEW.email !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'email must be a valid email address; you entered %', NEW.email;
    END IF;
  END IF;
  IF (NEW.email2 IS NOT NULL) THEN
    -- strip all spaces
    NEW.email2 = regexp_replace(NEW.email2, '\s+', '', 'g');
    IF (NEW.email2 = '') THEN
      NEW.email2 = NULL;
    ELSIF (NEW.email2 !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'email2 must be a valid email address; you entered %', NEW.email2;
    END IF;
  END IF;
  IF (NEW.email3 IS NOT NULL) THEN
    -- strip all spaces
    NEW.email3 = regexp_replace(NEW.email3, '\s+', '', 'g');
    IF (NEW.email3 = '') THEN
      NEW.email3 = NULL;
    ELSIF (NEW.email3 !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'email3 must be a valid email address; you entered %', NEW.email3;
    END IF;
  END IF;
  -- strip all spaces in tax_id_number
  NEW.tax_id_number = regexp_replace(NEW.tax_id_number, '\s+', '', 'g');
  IF NEW.tax_id_number = '' THEN
    NEW.tax_id_number = NULL;
  END IF;
  -- if tax_id_number IS NOT NULL, check next step to see if we should validate
  IF (NEW.tax_id_number IS NOT NULL) THEN
    -- if operation is INSERT or tax_id_number changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate if so
    IF ((TG_OP = 'INSERT') OR (NEW.tax_id_number IS DISTINCT FROM OLD.tax_id_number)) THEN
      IF (NEW.tax_id_number !~* '^\d[0-9-]+\d$') THEN
        RAISE EXCEPTION 'tax_id_number must contain only numbers and dashes; you entered %', NEW.tax_id_number;
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

-- mask sensitive fields in investor_tbl
CREATE FUNCTION mask_investor() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.tax_id_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.tax_id_number IS DISTINCT FROM OLD.tax_id_number) THEN
      -- mask the tax id by keeping first digit and last two digits and changing remaining numbers to '*'
      NEW.tax_id_number = left(NEW.tax_id_number, 1) || regexp_replace(substr(NEW.tax_id_number, 2, length(NEW.tax_id_number) - 3), '\d', '*', 'g') || right(NEW.tax_id_number, 2);
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified_investor BEFORE UPDATE ON investor_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created_investor BEFORE INSERT ON investor_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_investor BEFORE INSERT OR UPDATE ON investor_tbl FOR EACH ROW EXECUTE PROCEDURE validate_investor();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt_investor BEFORE INSERT OR UPDATE ON investor_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('tax_id_number');
CREATE TRIGGER z2_mask_investor BEFORE INSERT OR UPDATE ON investor_tbl FOR EACH ROW EXECUTE PROCEDURE mask_investor();

CREATE OR REPLACE VIEW investor_vw AS
  SELECT investor_tbl.id,
    investor_tbl.first_name,
    investor_tbl.last_name,
    investor_tbl.investor_name,
    investor_tbl.tax_id_number,
    investor_tbl.class_a_units,
    investor_tbl.is_active,
    investor_tbl.crypt_json,
    investor_tbl.created_ts,
    investor_tbl.created_user,
    investor_tbl.modified_ts,
    investor_tbl.modified_user
  FROM investor_tbl;

--DROP FUNCTION validate_investor();
--DROP FUNCTION mask_investor();
--DROP TABLE investor_tbl;
--DROP SEQUENCE investor_seq;

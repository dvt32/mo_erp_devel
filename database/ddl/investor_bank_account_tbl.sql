CREATE SEQUENCE investor_bank_account_seq;

CREATE TABLE investor_bank_account_tbl (
    id integer DEFAULT nextval('investor_bank_account_seq'::regclass) NOT NULL PRIMARY KEY,
    investor_id integer NOT NULL REFERENCES investor_tbl (id),
    depository_name character varying(48) NOT NULL,
    address_1 character varying(128),
    address_2 character varying(128),
    city character varying(64),
    state character varying(32),
    zip_code character varying(16),
    country character varying(64),
    account_type character varying(16) NOT NULL,
    routing_number character(9) NOT NULL,
    account_number character varying(17) NOT NULL,
    owner_name character varying(128) NOT NULL,
    owner_type character varying(16) NOT NULL,
    account_nickname character varying(32),
    note text,
    crypt_json jsonb,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

COMMENT ON TABLE investor_bank_account_tbl IS 'Investor bank accounts';
COMMENT ON COLUMN investor_bank_account_tbl.account_type IS 'checking | savings';
COMMENT ON COLUMN investor_bank_account_tbl.owner_type IS 'business | personal';

-- validate column values in investor_bank_account_tbl
CREATE FUNCTION validate_investor_bank_account() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.depository_name = strip_collapse_space(NEW.depository_name);
  NEW.address_1 = strip_collapse_space(NEW.address_1);
  NEW.address_2 = strip_collapse_space(NEW.address_2);
  NEW.city = strip_collapse_space(NEW.city);
  NEW.state = strip_collapse_space(NEW.state);
  NEW.zip_code = strip_collapse_space(NEW.zip_code);
  NEW.country = strip_collapse_space(NEW.country);
  NEW.account_type = strip_collapse_space(NEW.account_type);
  NEW.routing_number = strip_collapse_space(NEW.routing_number);
  -- if operation is INSERT or routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.routing_number IS DISTINCT FROM OLD.routing_number)) THEN
    IF NEW.routing_number IS NULL THEN
      RAISE EXCEPTION 'routing_number is required';
    END IF;
    IF NEW.routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'routing_number must be 9 digits; you entered %', NEW.routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.routing_number) THEN
      RAISE EXCEPTION 'routing_number not found in FedACH Participants; you entered %', NEW.routing_number;
    END IF;
  END IF;
  NEW.account_number = strip_collapse_space(NEW.account_number);
  -- if operation is INSERT or account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number)) THEN
    IF NEW.account_number IS NULL THEN
      RAISE EXCEPTION 'account_number is required';
    END IF;
    IF NEW.account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'account_number must be numeric; you entered %', NEW.account_number;
    END IF;
  END IF;
  NEW.owner_name = strip_collapse_space(NEW.owner_name);
  NEW.owner_type = strip_collapse_space(NEW.owner_type);
  NEW.account_nickname = strip_collapse_space(NEW.account_nickname);
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in investor_bank_account_tbl
CREATE FUNCTION mask_investor_bank_account() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number) THEN
      IF length(NEW.account_number) > 4 THEN
        NEW.account_number = left(NEW.account_number, 1) || repeat('*', length(NEW.account_number) - 4) || right(NEW.account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER insert_created BEFORE INSERT ON investor_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER update_modified BEFORE UPDATE ON investor_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON investor_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE validate_investor_bank_account();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON investor_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('account_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON investor_bank_account_tbl FOR EACH ROW EXECUTE PROCEDURE mask_investor_bank_account();

-- set up audit log for investor_bank_account_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON investor_bank_account_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON investor_bank_account_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('investor_bank_account_tbl');

--DROP FUNCTION validate_investor_bank_account();
--DROP FUNCTION mask_investor_bank_account();
--DROP TABLE investor_bank_account_tbl;
--DROP SEQUENCE investor_bank_account_seq;

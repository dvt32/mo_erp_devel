CREATE SEQUENCE ip_address_seq;
ALTER TABLE ip_address_seq OWNER TO wchao;

CREATE TABLE ip_address_tbl (
    id integer DEFAULT nextval(('ip_address_seq'::text)::regclass) NOT NULL PRIMARY KEY,
    account_id integer,
    -- There can be multiple instances of the same IP address when the addresses are private RFC 1918 addresses or local IPv6
    -- addresses. The tuple (ip_address, private_ns) is unique.
    ip_address inet NOT NULL,
    -- The DNS hostname associated with the IP address. Separate with commas if there are multiple.
    hostname character varying(255),
    -- rir_owner_id specifies the owner of the IP address block as shown in ARIN or other registry
    rir_owner_id integer REFERENCES isp_tbl (id),
    -- isp_id specifies the ISP that delivers bandwidth to the building where the IP address block resides
    isp_id integer REFERENCES isp_tbl (id),
    -- building_id specifies the building where the IP address block resides
    building_id integer NOT NULL REFERENCES building_tbl (id),
    -- setting group_name to same value across multiple rows signifies that two or more IP addresses are related
    group_name character varying(255) DEFAULT '' NOT NULL,
    -- private_ns allows for multiple rows with same ip_address for private RFC 1918 IPv4 or local IPv6 addresses by
    -- distinguishing them in different namespaces. leave blank if not needed.
    private_ns character varying(255) DEFAULT '' NOT NULL,
    note text,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

ALTER TABLE ip_address_tbl OWNER TO wchao;

--
-- Name: ip_address_tbl_ip_address_private_ns_key; Type: CONSTRAINT; Schema: public; Owner: wchao; Tablespace:
--

ALTER TABLE ONLY ip_address_tbl
    ADD CONSTRAINT ip_address_tbl_ip_address_private_ns_key UNIQUE (ip_address, private_ns);

COMMENT ON COLUMN ip_address_tbl.ip_address IS 'There can be multiple instances of the same IP address when the addresses are private RFC 1918 addresses or local IPv6 addresses. The tuple (ip_address, private_ns) is unique.';
COMMENT ON COLUMN ip_address_tbl.hostname IS 'The DNS hostname associated with the IP address. Separate with commas if there are multiple.';
COMMENT ON COLUMN ip_address_tbl.rir_owner_id IS 'rir_owner_id specifies the owner of the IP address block as shown in ARIN or other registry';
COMMENT ON COLUMN ip_address_tbl.isp_id IS 'isp_id specifies the ISP that delivers bandwidth to the building where the IP address block resides';
COMMENT ON COLUMN ip_address_tbl.building_id IS 'building_id specifies the building where the IP address block resides';
COMMENT ON COLUMN ip_address_tbl.group_name IS 'setting group_name to same value across multiple rows signifies that two or more IP addresses are related';
COMMENT ON COLUMN ip_address_tbl.private_ns IS 'private_ns allows for multiple rows with same ip_address for private RFC 1918 IPv4 or local IPv6 addresses by distinguishing them in different namespaces. leave NULL if not needed.';

--
-- Name: update_modified_ip_address; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_ip_address BEFORE UPDATE ON ip_address_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_ip_address; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_ip_address BEFORE INSERT ON ip_address_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE ip_address_tbl;
--DROP SEQUENCE ip_address_seq;

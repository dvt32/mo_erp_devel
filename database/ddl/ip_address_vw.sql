CREATE VIEW ip_address_vw AS
 SELECT ip_address_tbl.id,
    building_tbl.short_global_label AS building_name,
    isp_tbl_a.short_name AS isp_name,
    isp_tbl_b.short_name AS rir_name,
    ip_address_tbl.ip_address,
    ('IPv'::text || family(ip_address_tbl.ip_address)) AS ip_family,
    account_vw.name AS account_name,
    ip_address_tbl.hostname,
    ip_address_tbl.note,
    ip_address_tbl.private_ns,
    ip_address_tbl.group_name,
    ip_address_tbl.created_ts AS created_dt,
    ip_address_tbl.created_user AS created_user,
    ip_address_tbl.modified_ts AS modified_dt,
    ip_address_tbl.modified_user AS modified_user
   FROM ((((ip_address_tbl
     LEFT JOIN account_vw ON ((account_vw.id = ip_address_tbl.account_id)))
     LEFT JOIN isp_tbl isp_tbl_a ON ((isp_tbl_a.id = ip_address_tbl.isp_id)))
     LEFT JOIN building_tbl ON ((building_tbl.id = ip_address_tbl.building_id)))
     LEFT JOIN isp_tbl isp_tbl_b ON ((ip_address_tbl.rir_owner_id = isp_tbl_b.id)));


ALTER TABLE ip_address_vw OWNER TO wchao;

--
-- Name: ip_address_vw; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON TABLE ip_address_vw FROM PUBLIC;
REVOKE ALL ON TABLE ip_address_vw FROM wchao;
GRANT ALL ON TABLE ip_address_vw TO wchao;

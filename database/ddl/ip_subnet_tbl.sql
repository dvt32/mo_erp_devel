CREATE SEQUENCE ip_subnet_seq;
ALTER TABLE ip_subnet_seq OWNER TO wchao;

CREATE TABLE ip_subnet_tbl (
    id integer DEFAULT nextval(('ip_subnet_seq'::text)::regclass) NOT NULL PRIMARY KEY,
    -- IP address subnets can overlap. The more specific subnets override broader subnets. There can be multiple instances of
    -- the same IP address subnet when the addresses are private RFC 1918 addresses or local IPv6 addresses.
    -- The tuple (ip_subnet, private_ns) is unique.
    ip_subnet cidr NOT NULL,
    account_id integer,
    -- rir_owner_id specifies the owner of the IP address block as shown in ARIN or other registry
    rir_owner_id integer REFERENCES isp_tbl (id),
    -- isp_id specifies the ISP that delivers bandwidth to the building where the IP address block resides
    isp_id integer REFERENCES isp_tbl (id),
    -- building_id specifies the building where the IP address block resides
    building_id integer NOT NULL REFERENCES building_tbl (id),
    -- private_ns allows for multiple rows with same ip_subnet for private RFC 1918 IPv4 or local IPv6 addresses by
    -- distinguishing them in different namespaces. leave blank if not needed.
    private_ns character varying(255) DEFAULT '' NOT NULL,
    note text,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

ALTER TABLE ip_subnet_tbl OWNER TO wchao;

--
-- Name: ip_subnet_tbl_ip_subnet_private_ns_key; Type: CONSTRAINT; Schema: public; Owner: wchao; Tablespace:
--

ALTER TABLE ONLY ip_subnet_tbl
    ADD CONSTRAINT ip_subnet_tbl_ip_subnet_private_ns_key UNIQUE (ip_subnet, private_ns);


--
-- Name: update_modified_ip_subnet; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_ip_subnet BEFORE UPDATE ON ip_subnet_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_ip_subnet; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_ip_subnet BEFORE INSERT ON ip_subnet_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE ip_subnet_tbl;
--DROP SEQUENCE ip_subnet_seq;

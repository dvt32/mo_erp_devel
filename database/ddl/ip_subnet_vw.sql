CREATE VIEW ip_subnet_vw AS
 SELECT ip_subnet_tbl.id,
    building_tbl.short_global_label AS building_name,
    isp_tbl_a.short_name AS isp_name,
    isp_tbl_b.short_name AS rir_name,
    ip_subnet_tbl.ip_subnet,
    ('IPv'::text || family(ip_subnet_tbl.ip_subnet)) AS ip_family,
    account_vw.name AS account_name,
    ip_subnet_tbl.note,
    ip_subnet_tbl.private_ns,
    ip_subnet_tbl.created_ts AS created_dt,
    ip_subnet_tbl.created_user AS created_user,
    ip_subnet_tbl.modified_ts AS modified_dt,
    ip_subnet_tbl.modified_user AS modified_user
   FROM ((((ip_subnet_tbl
     LEFT JOIN account_vw ON ((account_vw.id = ip_subnet_tbl.account_id)))
     LEFT JOIN isp_tbl isp_tbl_a ON ((isp_tbl_a.id = ip_subnet_tbl.isp_id)))
     LEFT JOIN building_tbl ON ((building_tbl.id = ip_subnet_tbl.building_id)))
     LEFT JOIN isp_tbl isp_tbl_b ON ((ip_subnet_tbl.rir_owner_id = isp_tbl_b.id)));


ALTER TABLE ip_subnet_vw OWNER TO wchao;

--
-- Name: ip_subnet_vw; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON TABLE ip_subnet_vw FROM PUBLIC;
REVOKE ALL ON TABLE ip_subnet_vw FROM wchao;
GRANT ALL ON TABLE ip_subnet_vw TO wchao;

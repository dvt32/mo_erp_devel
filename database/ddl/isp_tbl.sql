CREATE SEQUENCE isp_seq;
ALTER TABLE isp_seq OWNER TO wchao;

CREATE TABLE isp_tbl (
    id integer DEFAULT nextval(('isp_seq'::text)::regclass) NOT NULL PRIMARY KEY,
    name character varying(64) NOT NULL,
    short_name character varying(32) NOT NULL,
    note text,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

ALTER TABLE isp_tbl OWNER TO wchao;

--
-- Name: update_modified_isp; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_isp BEFORE UPDATE ON isp_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_isp; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_isp BEFORE INSERT ON isp_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE isp_tbl;
--DROP SEQUENCE isp_seq;

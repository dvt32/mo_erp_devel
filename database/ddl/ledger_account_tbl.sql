CREATE SEQUENCE ledger_account_seq;
ALTER TABLE ledger_account_seq OWNER TO wchao;

CREATE TYPE ledger_account_type AS ENUM ('accounts_payable', 'accounts_receivable', 'bank', 'credit_card', 'cost_of_goods_sold', 'equity', 'other_expense', 'other_income', 'expense', 'fixed_asset', 'income', 'long_term_liability', 'other_asset', 'other_current_asset', 'other_current_liability');

CREATE TABLE ledger_account_tbl (
    id integer DEFAULT nextval(('ledger_account_seq'::text)::regclass) NOT NULL PRIMARY KEY,
    -- namespace for chart of accounts in order to allow for multiple charts of accounts so that different entities can have
    -- different charts of accounts.
    chart_ns character varying(255) NOT NULL,
    -- if this is a subsidiary chart of accounts that feeds into a parent chart of accounts for reporting, specify the parent ns.
    -- NULL means there is no parent chart of accounts (i.e. either this is standalone or it is a top level chart of accounts).
    -- Loops are checked for and are not permitted.
    parent_chart_ns character varying(255),
    -- parent_id is a link to ledger_account_tbl row (on id) in the current chart of accounts. parent_id points to a source
    -- ledger account and indicates that the current ledger account is a child of the source. If parent_id is NULL and alias_id
    -- is also NULL, then this row is a top level ledger account in a top level chart of accounts. Subsidiary charts of accounts
    -- (those with parent_chart_ns) cannot have new top level ledger accounts (i.e. parent_id must be NOT NULL if alias_id is
    -- NULL). This is so that reporting at the top level chart of accounts captures all data. If parent_id is NOT NULL, then type
    -- must be NULL because type always inherits from the parent. Loops are checked for and are not permitted.
    parent_id integer,
    -- alias_id is a link to ledger_account_tbl row (on id) in the direct parent chart of accounts. alias_id points to a source
    -- ledger account and indicates that the current ledger account is a reference to the source. If alias_id is NOT NULL, then
    -- parent_id must be NULL, type must also be NULL because type is the same as the alias source, name may be NULL, and the
    -- current chart of accounts must have alias links to every parent row of the alias target.
    -- Loops are impossible because this_row.alias_id must be aliased_row.id where this_row.parent_chart_ns = aliased_row.chart_ns.
    alias_id integer,
    -- account number used to identify this ledger account (e.g. 1000 for cash, 1040 for fixed asset, 4030 for rental income).
    -- can (and should) be NULL if alias_id is NOT NULL. a NULL account_num means use the same account_num as the source row.
    -- if alias_id is NOT NULL, can still specify account_num to override the alias link, but should avoid doing so.
    account_num integer,
    -- name can be NULL if alias_id is NOT NULL. a NULL name means use the same name as the ledger account in alias_id.
    -- if alias_id is NULL, then name cannot be NULL.
    name character varying(255),
    -- expense, income, fixed_asset, bank, etc.... must be NOT NULL for top level ledger accounts that are canonical (not alias).
    -- must be NULL for child ledger accounts (parent_id is NOT NULL) and aliases (alias_id is NOT NULL).
    type ledger_account_type,
    note text,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

ALTER TABLE ledger_account_tbl OWNER TO wchao;

--
-- Name: ledger_account_internal_vw; Type: VIEW; Schema: public; Owner: wchao
--

CREATE VIEW ledger_account_internal_vw AS
WITH RECURSIVE ledger_tree_query (id, chart_ns, parent_chart_ns, parent_id, alias_id, account_num, name, type, note, id_path, chart_ns_path, parent_id_path, alias_id_path, account_num_path, name_path, type_path, is_cycle) AS
(
  -- nonrecursive portion of query: top level ledger accounts in top level charts of accounts
  SELECT id, chart_ns, parent_chart_ns, parent_id, alias_id, account_num, name, type, note,
    ARRAY[id], ARRAY[CAST(chart_ns AS text)], ARRAY[parent_id], ARRAY[alias_id], ARRAY[account_num], ARRAY[CAST(name AS text)],
    ARRAY[type], false
  FROM ledger_account_tbl
  WHERE parent_id IS NULL AND alias_id IS NULL
  --
  UNION
  -- recursive portion of query
  SELECT join_tbl.id, join_tbl.chart_ns, join_tbl.parent_chart_ns, join_tbl.parent_id, join_tbl.alias_id, join_tbl.account_num,
    join_tbl.name, join_tbl.type, join_tbl.note,
    id_path || join_tbl.id, chart_ns_path || CAST(join_tbl.chart_ns AS text), parent_id_path || join_tbl.parent_id,
    alias_id_path || join_tbl.alias_id, account_num_path || join_tbl.account_num, name_path || CAST(join_tbl.name AS text),
    type_path || join_tbl.type, join_tbl.id = ANY(id_path)
  FROM ledger_account_tbl AS join_tbl
    INNER JOIN ledger_tree_query AS ledger_parent
    ON (join_tbl.parent_id = ledger_parent.id OR join_tbl.alias_id = ledger_parent.id)
  WHERE NOT is_cycle
  --
)
SELECT id, chart_ns, parent_chart_ns, parent_id, alias_id, account_num, name, type, note, id_path, chart_ns_path, parent_id_path, alias_id_path, account_num_path, name_path, type_path, is_cycle FROM ledger_tree_query;

ALTER TABLE ledger_account_internal_vw OWNER TO wchao;

--
-- Name: ledger_account_short_vw; Type: VIEW; Schema: public; Owner: wchao
--

CREATE VIEW ledger_account_short_vw AS
SELECT id, array_to_string(anyarray_uniq(chart_ns_path), ' > ') as chart_lineage, chart_ns, array_to_string(account_num_path, '-') as account_num, ledger_account_expanded_name(alias_id, name_path) AS expanded_name, type_path[1] AS type, note FROM ledger_account_internal_vw;

ALTER TABLE ledger_account_short_vw OWNER TO wchao;

--
-- Name: anyarray_uniq(anyarray); Type: FUNCTION; Schema: public; Owner: wchao
--

DROP FUNCTION IF EXISTS anyarray_uniq(anyarray);
CREATE FUNCTION anyarray_uniq(with_array anyarray) RETURNS anyarray
  LANGUAGE plpgsql
  AS $$
DECLARE
  -- The variable used to track iteration over "with_array".
  loop_offset integer;
  -- The array to be returned by this function.
  return_array with_array%TYPE := '{}';
BEGIN
  IF with_array IS NULL THEN
    RETURN NULL;
  END IF;
  -- Iterate over each element in "concat_array".
  FOR loop_offset IN ARRAY_LOWER(with_array, 1)..ARRAY_UPPER(with_array, 1) LOOP
    IF NOT with_array[loop_offset] = ANY(return_array) THEN
      return_array = ARRAY_APPEND(return_array, with_array[loop_offset]);
    END IF;
  END LOOP;
  RETURN return_array;
END;
$$;

--
-- Name: ledger_account_expanded_name(integer, anyarray); Type: FUNCTION; Schema: public; Owner: wchao
--

DROP FUNCTION IF EXISTS ledger_account_expanded_name(integer, anyarray);
CREATE FUNCTION ledger_account_expanded_name(alias_id integer, name_path anyarray) RETURNS text
  LANGUAGE plpgsql
  AS $$
DECLARE
  -- The array to be returned by this function (but joined as a string).
  return_array name_path%TYPE := '{}';
BEGIN
  IF name_path IS NULL THEN
    RETURN NULL;
  END IF;
  IF cardinality(name_path) <= 1 THEN
    RETURN array_to_string(name_path, '');
  END IF;
  IF alias_id IS NULL THEN
    -- if not an alias mapping, then return all components of the name
    return_array = name_path;
  ELSE
    -- if an alias mapping, then check if final component of the name is non-NULL, and if so, replace the penultimate component.
    -- alias mappings can have a new name, and the final component (if not NULL) is that new name.
    return_array = name_path[1:array_upper(name_path, 1) - 1];
    IF name_path[array_upper(name_path, 1)] IS NOT NULL THEN
      return_array[array_upper(return_array, 1)] = name_path[array_upper(name_path, 1)];
    END IF;
  END IF;
  RETURN array_to_string(return_array, ' : ');
END;
$$;

--
-- Name: update_modified_ledger_account; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_ledger_account BEFORE UPDATE ON ledger_account_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_ledger_account; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_ledger_account BEFORE INSERT ON ledger_account_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP VIEW ledger_account_short_vw;
--DROP VIEW ledger_account_internal_vw;
--DROP TABLE ledger_account_tbl;
--DROP TYPE ledger_account_type;
--DROP SEQUENCE ledger_account_seq;

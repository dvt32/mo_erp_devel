CREATE VIEW list_price_by_account_vw AS 
SELECT account_id, cust_acct_tbl.name, sum(list_price) FROM desk_tbl, cust_acct_tbl WHERE desk_tbl.account_id = cust_acct_tbl.id AND account_id IS NOT NULL GROUP BY account_id, cust_acct_tbl.name ORDER BY account_id;

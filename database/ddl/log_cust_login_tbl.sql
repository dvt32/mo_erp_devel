CREATE SEQUENCE log_cust_login_seq;

CREATE TABLE log_cust_login_tbl (
  id integer DEFAULT nextval('log_cust_login_seq'::regclass) NOT NULL PRIMARY KEY,
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  cust_pers_id integer NOT NULL REFERENCES cust_pers_tbl (id),
  email text NOT NULL,
  ip_address inet,
  user_agent text,
  login_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE log_cust_login_tbl IS 'Store customer login events for easy troubleshooting by allowing us to contact users based on errors we see in the error log.';

CREATE FUNCTION validate_log_cust_login() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  IF ((NEW.cust_acct_id IS NOT NULL) AND (NEW.cust_pers_id IS NOT NULL)) THEN
    IF NOT EXISTS (SELECT 1 FROM cust_pers_tbl WHERE id = NEW.cust_pers_id AND cust_acct_id = NEW.cust_acct_id) THEN
      RAISE EXCEPTION 'mismatch between cust_acct_id and cust_pers_id; you entered cust_acct_id % and cust_pers_id %', NEW.cust_acct_id, NEW.cust_pers_id;
    END IF;
  END IF;
  NEW.email = strip_collapse_space(NEW.email);
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON log_cust_login_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON log_cust_login_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON log_cust_login_tbl FOR EACH ROW EXECUTE PROCEDURE validate_log_cust_login();

CREATE VIEW log_cust_login_vw AS
 SELECT log_cust_login_tbl.id,
    floor_tbl.short_global_label AS floor_label,
    log_cust_login_tbl.cust_acct_id,
    cust_acct_tbl.name AS cust_acct_name,
    log_cust_login_tbl.cust_pers_id,
    cust_pers_tbl.last_name || ', ' || cust_pers_tbl.first_name AS cust_pers_name,
    log_cust_login_tbl.email,
    log_cust_login_tbl.ip_address,
    log_cust_login_tbl.user_agent,
    log_cust_login_tbl.login_ts,
    log_cust_login_tbl.created_ts,
    log_cust_login_tbl.created_user,
    log_cust_login_tbl.modified_ts,
    log_cust_login_tbl.modified_user
   FROM (log_cust_login_tbl
     JOIN cust_acct_tbl ON ((log_cust_login_tbl.cust_acct_id = cust_acct_tbl.id))
     JOIN cust_pers_tbl ON ((log_cust_login_tbl.cust_pers_id = cust_pers_tbl.id))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id)));

--DROP VIEW log_cust_login_vw;
--DROP TABLE log_cust_login_tbl;
--DROP FUNCTION validate_log_cust_login();
--DROP SEQUENCE log_cust_login_seq;

CREATE SEQUENCE log_page_access_seq;

CREATE TABLE log_page_access_tbl (
  id integer DEFAULT nextval('log_page_access_seq'::regclass) NOT NULL PRIMARY KEY,
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  cust_pers_id integer NOT NULL REFERENCES cust_pers_tbl (id),
  ip_address inet,
  user_agent text,
  request_uri text NOT NULL,
  http_referer text,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE log_page_access_tbl IS 'Store page access events for easy troubleshooting by allowing us to contact users based on errors we see in the error log.';
COMMENT ON COLUMN log_page_access_tbl.ip_address IS 'IP address of the remote user.';
COMMENT ON COLUMN log_page_access_tbl.request_uri IS 'The URI that the user requested.';
COMMENT ON COLUMN log_page_access_tbl.http_referer IS 'The URL of the page the user was previously on, before visiting the current page.';

CREATE FUNCTION validate_log_page_access() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF ((NEW.cust_acct_id IS NOT NULL) AND (NEW.cust_pers_id IS NOT NULL)) THEN
    IF NOT EXISTS (SELECT 1 FROM cust_pers_tbl WHERE id = NEW.cust_pers_id AND cust_acct_id = NEW.cust_acct_id) THEN
      RAISE EXCEPTION 'mismatch between cust_acct_id and cust_pers_id; you entered cust_acct_id % and cust_pers_id %', NEW.cust_acct_id, NEW.cust_pers_id;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON log_page_access_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON log_page_access_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON log_page_access_tbl FOR EACH ROW EXECUTE PROCEDURE validate_log_page_access();

CREATE VIEW log_page_access_vw AS
 SELECT log_page_access_tbl.id,
    floor_tbl.short_global_label AS floor_label,
    log_page_access_tbl.cust_acct_id,
    cust_acct_tbl.name AS cust_acct_name,
    log_page_access_tbl.cust_pers_id,
    cust_pers_tbl.last_name || ', ' || cust_pers_tbl.first_name AS cust_pers_name,
    log_page_access_tbl.ip_address,
    log_page_access_tbl.user_agent,
    log_page_access_tbl.request_uri,
    log_page_access_tbl.http_referer,
    log_page_access_tbl.created_ts,
    log_page_access_tbl.created_user,
    log_page_access_tbl.modified_ts,
    log_page_access_tbl.modified_user
   FROM (log_page_access_tbl
     JOIN cust_acct_tbl ON ((log_page_access_tbl.cust_acct_id = cust_acct_tbl.id))
     JOIN cust_pers_tbl ON ((log_page_access_tbl.cust_pers_id = cust_pers_tbl.id))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id)));

--DROP VIEW log_page_access_vw;
--DROP TABLE log_page_access_tbl;
--DROP FUNCTION validate_log_page_access();
--DROP SEQUENCE log_page_access_seq;

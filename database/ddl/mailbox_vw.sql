CREATE VIEW mailbox_vw AS
 SELECT mailbox_tbl.id,
    mailbox_tbl.floor_id,
    floor_tbl.short_global_label AS floor_label,
    mailbox_tbl.account_id,
    cust_acct_tbl.name AS account_name,
    mailbox_tbl.mailbox_number,
    mailbox_tbl.note,
    mailbox_tbl.last_checked_dt,
    mailbox_tbl.bool_mail_present,
    mailbox_tbl.creation_dt
   FROM (mailbox_tbl JOIN floor_tbl ON (mailbox_tbl.floor_id = floor_tbl.id) LEFT JOIN cust_acct_tbl ON (mailbox_tbl.account_id = cust_acct_tbl.id));

--DROP VIEW mailbox_vw;

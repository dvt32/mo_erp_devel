CREATE VIEW missing_contract_vw AS
 SELECT desk_tbl.id,
    desk_tbl.floor_id,
    floor_tbl.short_global_label,
    desk_tbl.assigned_number,
    desk_tbl.account_id,
    cust_acct_tbl.name AS account_name,
    cust_acct_tbl.room_minutes,
    desk_tbl.is_enclosed,
    desk_tbl.current_price,
    desk_tbl.list_price,
    desk_tbl.start_date,
    desk_tbl.end_date,
    (area(path(desk_tbl.bounding_polygon)) / (144)::double precision) AS desk_area
   FROM ((desk_tbl
     JOIN floor_tbl ON ((desk_tbl.floor_id = floor_tbl.id)))
     LEFT JOIN cust_acct_tbl ON ((desk_tbl.account_id = cust_acct_tbl.id)))
  WHERE (
    (desk_tbl.is_active IS TRUE)
    AND (cust_acct_tbl.is_active IS TRUE)
    AND (desk_tbl.end_date IS NULL));

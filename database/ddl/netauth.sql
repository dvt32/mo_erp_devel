CREATE TABLE building_link_tbl (
  building_id integer NOT NULL REFERENCES building_tbl (id),
  name varchar(255) NOT NULL,
  public_ip_addrs inet[],
  client_ip_addr inet,
  ssid varchar(32),
  password varchar(255),
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255),
  UNIQUE (building_id, name)
);

COMMENT ON TABLE building_link_tbl IS 'Table to store external IP address of each link at each building.';
COMMENT ON COLUMN building_link_tbl.name IS 'Name of the link (e.g. rcn, vzfios, twccoax, etc.).';
COMMENT ON COLUMN building_link_tbl.public_ip_addrs IS 'External IP addresses of the router.';

CREATE FUNCTION validate_building_link() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.name = strip_collapse_space(NEW.name);
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON building_link_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON building_link_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON building_link_tbl FOR EACH ROW EXECUTE PROCEDURE validate_building_link();

CREATE SEQUENCE leadsource_seq;
CREATE TYPE leadsource_type AS ENUM ('croissant');
CREATE TABLE leadsource_tbl (
  id integer DEFAULT nextval('leadsource_seq'::regclass) NOT NULL PRIMARY KEY,
  company leadsource_type,
  username character varying(64) NOT NULL,
  hashed_passwd character varying(60),
  known_ip_addrs inet[],
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);
CREATE TRIGGER update_modified BEFORE UPDATE ON leadsource_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON leadsource_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

CREATE TABLE external_daypass_user_tbl (
  id integer DEFAULT nextval('account_seq'::regclass) NOT NULL PRIMARY KEY,
  leadsource_id integer REFERENCES leadsource_tbl (id),
  first_name varchar(64),
  last_name varchar(64),
  company varchar(64),
  building_id integer REFERENCES building_tbl (id),
  email text,
  external_id varchar(32),
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

CREATE FUNCTION validate_external_daypass_user() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
NEW.first_name = strip_collapse_space(NEW.first_name);
NEW.last_name = strip_collapse_space(NEW.last_name);
NEW.company = strip_collapse_space(NEW.company);
NEW.email = strip_collapse_space(NEW.email);
IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
  RAISE EXCEPTION 'guest_email must be a valid email address; you entered %', NEW.email;
END IF;
RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON external_daypass_user_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON external_daypass_user_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON external_daypass_user_tbl FOR EACH ROW EXECUTE PROCEDURE validate_external_daypass_user();

CREATE SEQUENCE netauth_device_seq;

CREATE TABLE netauth_device_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE netauth_device_id_tbl IS 'Common table to reserve ID values for netauth_device_cust_tbl, netauth_device_guest_tbl, and netauth_device_transient_tbl';
COMMENT ON COLUMN netauth_device_id_tbl.tbl_name IS 'Name of table that uses the ID from this row';

CREATE TABLE netauth_device_cust_tbl (
  id integer DEFAULT nextval('netauth_device_seq'::regclass) NOT NULL PRIMARY KEY,
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  cust_pers_id integer REFERENCES cust_pers_tbl (id),
  -- user-assigned label for the device. not required, but highly recommended.
  label varchar(64),
  -- all lowercase, 12 hexadecimal digits.
  mac_addr varchar(12) NOT NULL,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255),
  UNIQUE (cust_acct_id, mac_addr)
);

COMMENT ON TABLE netauth_device_cust_tbl IS 'Network authorization table with list of devices allowed to get online.';
COMMENT ON COLUMN netauth_device_cust_tbl.label IS 'User-assigned label for the device. Not required, but highly recommended.';

CREATE FUNCTION validate_netauth_device_cust() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.label = strip_collapse_space(NEW.label);
  NEW.mac_addr = strip_collapse_space(NEW.mac_addr);
  IF (NEW.mac_addr !~* '^[0-9a-f]{12}$') THEN
    RAISE EXCEPTION 'mac_addr must be 12 lowercase hex digits; you entered %', NEW.mac_addr;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON netauth_device_cust_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON netauth_device_cust_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON netauth_device_cust_tbl FOR EACH ROW EXECUTE PROCEDURE validate_netauth_device_cust();
CREATE TRIGGER reserve_id BEFORE INSERT ON netauth_device_cust_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('netauth_device_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON netauth_device_cust_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('netauth_device_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON netauth_device_cust_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('netauth_device_id_tbl');

CREATE TABLE netauth_device_pin_tbl (
  netauth_device_id integer NOT NULL REFERENCES netauth_device_cust_tbl (id),
  building_id integer NOT NULL REFERENCES building_tbl (id),
  UNIQUE (netauth_device_id, building_id)
);

COMMENT ON TABLE netauth_device_pin_tbl IS 'Network authorization table for pinned devices that should be allowed online without portal login.';
COMMENT ON COLUMN netauth_device_pin_tbl.building_id IS 'Which building is the pinned device active at?';

-- workflow:
--   1. Guest tries to go online, is redirected to netauth login page with link at bottom saying "fill out guest request if you are a guest".
--   2. Click to fill out a guest request, then submit that form.
--   3. Our site sends an email to the member and to the guest.
--   4. When the member clicks to activate the Internet, cust_activated set to true, activated_ts set to now, and expire_ts set to 15 minutes or 2 hours depending on whether guest has already verified email. Then guest can go online. Member has the option to activate the Internet under pending guest requests on the netauth site if member did not receive an email.
--   5. When the guest clicks link to verify email, guest's Internet access is extended to two hours. If guest verifies email before member clicks activate link, then wait until activation before granting Internet access to guest.

CREATE TABLE netauth_device_guest_tbl (
  id integer DEFAULT nextval('netauth_device_seq'::regclass) NOT NULL PRIMARY KEY,
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  cust_pers_email text NOT NULL,
  building_id integer REFERENCES building_tbl (id),
  guest_first_name varchar(64),
  guest_last_name varchar(64),
  guest_company varchar(64),
  guest_email text NOT NULL,
  -- all lowercase, 12 hexadecimal digits
  mac_addr varchar(12),
  label varchar(64),
  cust_activated_ts timestamp with time zone,
  guest_verified_ts timestamp with time zone,
  expire_ts timestamp with time zone,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE netauth_device_guest_tbl IS 'Cached/generated list of allowed guest devices at each building. Only for guest devices of our registered customers.';
COMMENT ON COLUMN netauth_device_guest_tbl.mac_addr IS 'MAC address: needed for MAC filtering.';
COMMENT ON COLUMN netauth_device_guest_tbl.cust_activated_ts IS 'When did the customer activate Internet for the guest?';
COMMENT ON COLUMN netauth_device_guest_tbl.guest_verified_ts IS 'When did the guest confirm their email address?';
COMMENT ON COLUMN netauth_device_guest_tbl.expire_ts IS 'For guest access, when does access end';

CREATE FUNCTION validate_netauth_device_guest() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  NEW.cust_pers_email = strip_collapse_space(NEW.cust_pers_email);
  IF (NEW.cust_pers_email IS NOT NULL) AND (NEW.cust_pers_email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'cust_pers_email must be a valid email address; you entered %', NEW.cust_pers_email;
  END IF;
  NEW.guest_first_name = strip_collapse_space(NEW.guest_first_name);
  NEW.guest_last_name = strip_collapse_space(NEW.guest_last_name);
  NEW.guest_company = strip_collapse_space(NEW.guest_company);
  NEW.guest_email = strip_collapse_space(NEW.guest_email);
  IF (NEW.guest_email IS NOT NULL) AND (NEW.guest_email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'guest_email must be a valid email address; you entered %', NEW.guest_email;
  END IF;
  NEW.mac_addr = strip_collapse_space(NEW.mac_addr);
  IF (NEW.mac_addr !~* '^[0-9a-f]{12}$') THEN
    RAISE EXCEPTION 'mac_addr must be 12 lowercase hex digits; you entered %', NEW.mac_addr;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON netauth_device_guest_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON netauth_device_guest_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON netauth_device_guest_tbl FOR EACH ROW EXECUTE PROCEDURE validate_netauth_device_guest();
CREATE TRIGGER reserve_id BEFORE INSERT ON netauth_device_guest_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('netauth_device_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON netauth_device_guest_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('netauth_device_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON netauth_device_guest_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('netauth_device_id_tbl');


CREATE TABLE netauth_device_transient_tbl (
  id integer DEFAULT nextval('netauth_device_seq'::regclass) NOT NULL PRIMARY KEY,
  external_daypass_user_id integer NOT NULL REFERENCES external_daypass_user_tbl (id),
  label varchar(64),
  building_id integer REFERENCES building_tbl (id),
  -- all lowercase, 12 hexadecimal digits
  mac_addr varchar(12),
  croissant_visit_id varchar(32),
  activated_ts timestamp with time zone,
  expire_ts timestamp with time zone,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE netauth_device_transient_tbl IS 'Cached/generated list of allowed devices at each building for temporary users (daypass).';
COMMENT ON COLUMN netauth_device_transient_tbl.mac_addr IS 'MAC address: needed for MAC filtering.';
COMMENT ON COLUMN netauth_device_transient_tbl.activated_ts IS 'When did the customer activate Internet?';
COMMENT ON COLUMN netauth_device_transient_tbl.expire_ts IS 'For guest access, when does access end';


CREATE FUNCTION validate_netauth_device_transient() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  NEW.mac_addr = strip_collapse_space(NEW.mac_addr);
  IF (NEW.mac_addr !~* '^[0-9a-f]{12}$') THEN
    RAISE EXCEPTION 'mac_addr must be 12 lowercase hex digits; you entered %', NEW.mac_addr;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON netauth_device_transient_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON netauth_device_transient_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON netauth_device_transient_tbl FOR EACH ROW EXECUTE PROCEDURE validate_netauth_device_transient();
CREATE TRIGGER reserve_id BEFORE INSERT ON netauth_device_transient_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('netauth_device_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON netauth_device_transient_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('netauth_device_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON netauth_device_transient_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('netauth_device_id_tbl');


CREATE TABLE netauth_code_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE netauth_code_id_tbl IS 'Common table to reserve ID values for netauth codes';
COMMENT ON COLUMN netauth_code_id_tbl.tbl_name IS 'Name of table that uses the ID from this row';

CREATE SEQUENCE netauth_code_seq;

CREATE TABLE netauth_reg_code_tbl (
  id integer DEFAULT nextval('netauth_code_seq'::regclass) NOT NULL PRIMARY KEY,
  cust_pers_id integer NOT NULL REFERENCES cust_pers_tbl (id),
  -- alphanumeric, 6 characters
  activation_code varchar(6),
  code_expire_ts timestamp with time zone,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

CREATE TRIGGER update_modified BEFORE UPDATE ON netauth_reg_code_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON netauth_reg_code_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER reserve_id BEFORE INSERT ON netauth_reg_code_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('netauth_code_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON netauth_reg_code_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('netauth_code_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON netauth_reg_code_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('netauth_code_id_tbl');

COMMENT ON TABLE netauth_reg_code_tbl IS 'Codes for registering your account for a password on the netauth site.';
COMMENT ON COLUMN netauth_reg_code_tbl.activation_code IS 'Alphanumeric 6 character code';
COMMENT ON COLUMN netauth_reg_code_tbl.code_expire_ts IS 'How long is this code for registration/password setting valid';
COMMENT ON COLUMN netauth_reg_code_tbl.cust_pers_id IS 'Login is by a person basis';

CREATE TYPE rel_end AS ENUM ('day', 'week', 'year');

CREATE TABLE netauth_access_code_tbl (
  id integer DEFAULT nextval('netauth_code_seq'::regclass) NOT NULL PRIMARY KEY,
  netauth_device_id integer REFERENCES netauth_device_id_tbl (id),
  -- alphanumeric, 6 characters
  activation_code varchar(6),
  duration interval,
  relative_end rel_end,
  code_expire_ts timestamp with time zone,
  not_before_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

CREATE TRIGGER update_modified BEFORE UPDATE ON netauth_access_code_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON netauth_access_code_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER reserve_id BEFORE INSERT ON netauth_access_code_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('netauth_code_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON netauth_access_code_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('netauth_code_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON netauth_access_code_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('netauth_code_id_tbl');

COMMENT ON TABLE netauth_access_code_tbl IS 'Access codes for netauth internet access.';
COMMENT ON COLUMN netauth_access_code_tbl.netauth_device_id IS 'If linked to a netauth device entry, then this is the code sent to the person to activate their device.';
COMMENT ON COLUMN netauth_access_code_tbl.activation_code IS 'Alphanumeric 6 character code';
COMMENT ON COLUMN netauth_access_code_tbl.duration IS 'Duration of internet access this code grants';
COMMENT ON COLUMN netauth_access_code_tbl.relative_end IS 'Duration of internet access that is just, the end of the day or week or month, etc.';
COMMENT ON COLUMN netauth_access_code_tbl.code_expire_ts IS 'When the code expires';

CREATE VIEW netauth_devices_vw AS
 SELECT netauth_device_cust_tbl.id,
    'cust'::character varying(10) AS device_type,
    netauth_device_cust_tbl.cust_acct_id,
    netauth_device_cust_tbl.label,
    NULL AS cust_pers_email,
    NULL AS building_id,
    NULL AS guest_first_name,
    NULL AS guest_last_name, 
    NULL AS guest_company,
    NULL AS guest_email,
    netauth_device_cust_tbl.mac_addr,
    NULL AS cust_activated_ts,
    NULL AS guest_verified_ts,
    NULL AS croissant_visit_id,
    NULL AS expire_ts,
    netauth_device_cust_tbl.created_ts,
    NULL::integer AS external_daypass_user_id,
    NULL::timestamp AS activated_ts
    FROM netauth_device_cust_tbl
UNION ALL 
  SELECT netauth_device_guest_tbl.id,
    'guest'::character varying(10) AS device_type,
    netauth_device_guest_tbl.cust_acct_id,
    netauth_device_guest_tbl.label,
    netauth_device_guest_tbl.cust_pers_email,
    netauth_device_guest_tbl.building_id,
    netauth_device_guest_tbl.guest_first_name,
    netauth_device_guest_tbl.guest_last_name,
    netauth_device_guest_tbl.guest_company,
    netauth_device_guest_tbl.guest_email,
    netauth_device_guest_tbl.mac_addr,
    netauth_device_guest_tbl.cust_activated_ts,
    netauth_device_guest_tbl.guest_verified_ts,
    NULL AS croissant_visit_id,
    netauth_device_guest_tbl.expire_ts,
    netauth_device_guest_tbl.created_ts,
    NULL::integer AS external_daypass_user_id,
    NULL::timestamp AS activated_ts
    FROM netauth_device_guest_tbl
UNION ALL
  SELECT netauth_device_transient_tbl.id,
    'transient'::character varying(10) AS device_type,
    NULL AS cust_acct_id,
    netauth_device_transient_tbl.label,
    NULL AS cust_pers_email,
    NULL AS building_id,
    NULL AS guest_first_name,
    NULL AS guest_last_name, 
    NULL AS guest_company,
    NULL AS guest_email,
    netauth_device_transient_tbl.mac_addr,
    NULL AS cust_activated_ts,
    NULL AS guest_verified_ts,
    netauth_device_transient_tbl.croissant_visit_id,
    netauth_device_transient_tbl.expire_ts,
    netauth_device_transient_tbl.created_ts,
    netauth_device_transient_tbl.external_daypass_user_id,
    netauth_device_transient_tbl.activated_ts
    FROM netauth_device_transient_tbl;

DROP VIEW building_vw;
CREATE VIEW building_vw AS
 SELECT building_tbl.id,
    building_tbl.short_global_label,
    building_tbl.address_1,
    building_tbl.address_2,
    building_tbl.city,
    building_tbl.state,
    building_tbl.zip_code,
    building_tbl.country,
    building_tbl.type,
    owner_company_tbl.short_name AS owner_short_name,
    share_company_tbl.short_name AS share_short_name,
    building_tbl.timezone,
    building_tbl.is_active,
    building_tbl.croissant_id,
    building_tbl.note
   FROM ((building_tbl
     LEFT JOIN company_tbl owner_company_tbl ON (building_tbl.owner_company_id = owner_company_tbl.id))
     LEFT JOIN company_tbl share_company_tbl ON (building_tbl.share_company_id = share_company_tbl.id));

--DROP VIEW netauth_devices_vw;
--DROP TABLE netauth_access_code_tbl;
--DROP TYPE rel_end;
--DROP TABLE netauth_reg_code_tbl;
--DROP SEQUENCE netauth_code_seq;
--DROP TABLE netauth_code_id_tbl;
--DROP TABLE netauth_device_transient_tbl;
--DROP FUNCTION validate_netauth_device_transient();
--DROP TABLE netauth_device_guest_tbl;
--DROP FUNCTION validate_netauth_device_guest();
--DROP TABLE netauth_device_pin_tbl;
--DROP TABLE netauth_device_cust_tbl;
--DROP FUNCTION validate_netauth_device_cust();
--DROP TABLE netauth_device_id_tbl;
--DROP SEQUENCE netauth_device_seq;
--DROP TABLE external_daypass_user_tbl;
--DROP FUNCTION validate_external_daypass_user();
--DROP TABLE leadsource_tbl;
--DROP SEQUENCE leadsource_seq;
--DROP FUNCTION validate_building_link();
--DROP TABLE building_link_tbl;
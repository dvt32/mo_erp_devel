CREATE SEQUENCE nmi_vault_update_seq;

CREATE TABLE nmi_vault_update_tbl (
  id integer DEFAULT nextval('nmi_vault_update_seq'::regclass) NOT NULL PRIMARY KEY,
  request text,
  response text,
  checkaba character(9),
  checkaccount varchar(17),
  ccnumber varchar(16),
  cvv varchar(4),
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE nmi_vault_update_tbl IS 'Log NMI vault add/update/delete operations';
COMMENT ON COLUMN nmi_vault_update_tbl.request IS 'HTTP request parameters sent to NMI, sanitized to remove sensitive info';
COMMENT ON COLUMN nmi_vault_update_tbl.response IS 'HTTP response body from NMI';
COMMENT ON COLUMN nmi_vault_update_tbl.checkaba IS 'Masked ABA routing number';
COMMENT ON COLUMN nmi_vault_update_tbl.checkaccount IS 'Masked bank account number';
COMMENT ON COLUMN nmi_vault_update_tbl.ccnumber IS 'Masked ccnumber';
COMMENT ON COLUMN nmi_vault_update_tbl.cvv IS 'Masked cvv';

-- validate column values in nmi_vault_update_tbl
CREATE FUNCTION validate_nmi_vault_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.checkaba = strip_space(NEW.checkaba);
  -- if operation is INSERT or checkaba changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF NEW.checkaba IS NOT NULL THEN
    IF ((TG_OP = 'INSERT') OR (NEW.checkaba IS DISTINCT FROM OLD.checkaba)) THEN
      IF NEW.checkaba !~ '^\d{9}$' THEN
        RAISE EXCEPTION 'checkaba must be 9 digits; you entered %', NEW.checkaba;
      END IF;
      IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.checkaba) THEN
        RAISE EXCEPTION 'routing_number not found in FedACH Participants; you entered %', NEW.checkaba;
      END IF;
    END IF;
  END IF;
  NEW.checkaccount = strip_space(NEW.checkaccount);
  -- if operation is INSERT or checkaccount changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF NEW.checkaccount IS NOT NULL THEN
    IF ((TG_OP = 'INSERT') OR (NEW.checkaccount IS DISTINCT FROM OLD.checkaccount)) THEN
      IF NEW.checkaccount !~ '^\d+$' THEN
        RAISE EXCEPTION 'checkaccount must be numeric; you entered %', NEW.checkaccount;
      END IF;
    END IF;
  END IF;
  NEW.ccnumber = strip_space(NEW.ccnumber);
  -- if operation is INSERT or ccnumber changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF NEW.ccnumber IS NOT NULL THEN
    IF ((TG_OP = 'INSERT') OR (NEW.ccnumber IS DISTINCT FROM OLD.ccnumber)) THEN
      IF NEW.ccnumber !~ '^\d{13,16}$' THEN
        RAISE EXCEPTION 'ccnumber must be 13 to 16 digits; you entered %', NEW.ccnumber;
      END IF;
      -- check validity of credit card number using Luhn algorithm
      IF (NOT is_valid_cc_luhn(NEW.ccnumber)) THEN
        RAISE EXCEPTION 'ccnumber is not a valid credit card number; you entered %', NEW.ccnumber;
      END IF;
    END IF;
  END IF;
  NEW.cvv = strip_space(NEW.cvv);
  -- if cvv IS NOT NULL, check next step to see if we should validate
  IF (NEW.cvv IS NOT NULL) THEN
    -- if operation is INSERT or cvv changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate if so
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      IF NEW.cvv !~ '^\d{3,4}$' THEN
        RAISE EXCEPTION 'cvv must be 3 or 4 digits; you entered %', NEW.cvv;
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

-- mask sensitive fields in nmi_vault_update_tbl
CREATE FUNCTION mask_nmi_vault_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.checkaba IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.checkaba IS DISTINCT FROM OLD.checkaba) THEN
      NEW.checkaba = left(NEW.checkaba, 1) || repeat('*', 5) || right(NEW.checkaba, 3);
    END IF;
  END IF;
  IF (NEW.checkaccount IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.checkaccount IS DISTINCT FROM OLD.checkaccount) THEN
      IF length(NEW.checkaccount) > 4 THEN
        NEW.checkaccount = left(NEW.checkaccount, 1) || repeat('*', length(NEW.checkaccount) - 4) || right(NEW.checkaccount, 3);
      END IF;
    END IF;
  END IF;
  IF (NEW.ccnumber IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.ccnumber IS DISTINCT FROM OLD.ccnumber) THEN
      NEW.ccnumber = left(NEW.ccnumber, 1) || repeat('*', length(NEW.ccnumber) - 5) || right(NEW.ccnumber, 4);
    END IF;
  END IF;
  IF (NEW.cvv IS NOT NULL) THEN
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      NEW.cvv = '****';
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON nmi_vault_update_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON nmi_vault_update_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON nmi_vault_update_tbl FOR EACH ROW EXECUTE PROCEDURE validate_nmi_vault_update();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON nmi_vault_update_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('checkaba', 'checkaccount', 'ccnumber', 'cvv');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON nmi_vault_update_tbl FOR EACH ROW EXECUTE PROCEDURE mask_nmi_vault_update();

--DROP TABLE nmi_vault_update_tbl;
--DROP FUNCTION validate_nmi_vault_update();
--DROP FUNCTION mask_nmi_vault_update();
--DROP SEQUENCE nmi_vault_update_seq;

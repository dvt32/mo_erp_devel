CREATE SEQUENCE op_recur_pmt_ach_seq;

CREATE TABLE op_recur_pmt_ach_tbl (
    id integer DEFAULT nextval(('op_recur_pmt_ach_seq'::text)::regclass) NOT NULL PRIMARY KEY,
    account_id integer NOT NULL REFERENCES cust_acct_tbl (id),
    -- account_type: checking or savings
    account_type character varying(8) NOT NULL REFERENCES enum_bank_account_type_tbl (account_type),
    -- owned_by: business or personal
    owned_by character varying(8) NOT NULL REFERENCES enum_bank_account_owned_by_tbl (owned_by),
    -- name of the person or company that owns the money in the bank account
    registered_name character varying(64),
    routing_number character(9) NOT NULL,
    account_number character varying(17) NOT NULL,
    -- 1 - 28. Not 29, 30, 31 because of confusion for customers in leap years and variable number of days in different months
    day_of_month smallint NOT NULL,
    -- 1 = monthly, 3 = quarterly, 6 = biannually, 12 = annually
    cycle_period smallint DEFAULT 1 NOT NULL,
    amount numeric(8,2) NOT NULL,
    -- zero or more email addresses to send notice of charge and advance warning/reminder (if turned on)
    cc_emails text,
    -- also send emails to primary contact or not, depending on value here
    do_email_to_primary boolean DEFAULT TRUE NOT NULL,
    -- how many days before the charge to send a reminder of the upcoming charge. can be 1 - 28.
    reminder_days smallint,
    -- NULL if start immediately. If date here, do not charge before start_date.
    start_date date,
    -- NULL if repeat endlessly. If date here, do not charge after end_date.
    end_date date,
    -- set on_hold to TRUE to place all charges on hold until future action on our part. Defaults to FALSE.
    on_hold boolean DEFAULT FALSE NOT NULL,
    crypt_json jsonb,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    modified_ts timestamp with time zone,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_user character varying(255)
);

COMMENT ON TABLE op_recur_pmt_ach_tbl IS 'Recurring ACH payments managed by Micro Office employees';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.account_id IS 'link to cust_acct_tbl.id';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.account_type IS 'checking or savings';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.owned_by IS 'business or personal';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.registered_name IS 'Name of the person or entity that owns the bank account';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.routing_number IS 'ACH routing number';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.account_number IS 'Bank account number';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.day_of_month IS 'What day of the month to debit funds. Can be 1 - 28.';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.cycle_period IS '1 = monthly, 3 = quarterly, 6 = biannually, 12 = annually. default is 1 (monthly).';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.cc_emails IS 'Comma-separated list of email addresses to send advance warnings and notification of debit. Email is also sent to primary contact if do_email_to_primary is true. Can be NULL.';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.do_email_to_primary IS 'True/false send email to primary contact on account';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.reminder_days IS 'How many days before day_of_month to send a reminder. Can be NULL or 0 to indicate no reminder.';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.start_date IS 'If not NULL, then no transactions will be charged before start_date';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.end_date IS 'If not NULL, then no transactions will be charged after end_date';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.on_hold IS 'When true, means recurring transaction should not be processed going forward, until set back to false';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.created_user IS 'User who created the row';
COMMENT ON COLUMN op_recur_pmt_ach_tbl.modified_user IS 'User who last modified the row';

CREATE VIEW op_recur_pmt_ach_vw AS
 SELECT op_recur_pmt_ach_tbl.id,
    cust_acct_tbl.name,
    op_recur_pmt_ach_tbl.account_type,
    op_recur_pmt_ach_tbl.owned_by,
    op_recur_pmt_ach_tbl.registered_name,
    op_recur_pmt_ach_tbl.routing_number,
    op_recur_pmt_ach_tbl.account_number,
    op_recur_pmt_ach_tbl.day_of_month,
    op_recur_pmt_ach_tbl.cycle_period,
    op_recur_pmt_ach_tbl.amount,
    op_recur_pmt_ach_tbl.cc_emails,
    op_recur_pmt_ach_tbl.do_email_to_primary,
    op_recur_pmt_ach_tbl.reminder_days,
    op_recur_pmt_ach_tbl.start_date,
    op_recur_pmt_ach_tbl.end_date,
    op_recur_pmt_ach_tbl.on_hold,
    op_recur_pmt_ach_tbl.created_ts,
    op_recur_pmt_ach_tbl.modified_ts,
    op_recur_pmt_ach_tbl.created_user,
    op_recur_pmt_ach_tbl.modified_user
   FROM op_recur_pmt_ach_tbl,
    cust_acct_tbl
  WHERE (op_recur_pmt_ach_tbl.account_id = cust_acct_tbl.id);

-- validate column values in op_recur_pmt_ach_tbl
CREATE FUNCTION validate_op_recur_pmt_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  IF (NEW.account_type != 'checking') AND (NEW.account_type != 'savings') THEN
    RAISE EXCEPTION 'account_type must be either checking or savings (all lowercase); you entered %', NEW.account_type;
  END IF;
  IF (NEW.owned_by != 'business') AND (NEW.owned_by != 'personal') THEN
    RAISE EXCEPTION 'owned_by must be either business or personal (all lowercase); you entered %', NEW.owned_by;
  END IF;
  IF (NEW.day_of_month < 1) OR (NEW.day_of_month > 28) THEN
    RAISE EXCEPTION 'day_of_month must be in the range 1 to 28; you entered %', NEW.day_of_month;
  END IF;
  IF (NEW.cycle_period < 1) OR (NEW.cycle_period > 12) THEN
    RAISE EXCEPTION 'cycle_period must be in the range 1 to 12; you entered %', NEW.cycle_period;
  END IF;
  IF (NEW.amount <= 0) THEN
    RAISE EXCEPTION 'amount must be greater than 0; you entered %', NEW.amount;
  END IF;
  -- strip all spaces
  NEW.cc_emails = regexp_replace(NEW.cc_emails, '\s+', '', 'g');
  IF (NEW.cc_emails = '') THEN
    NEW.cc_emails = NULL;
  END IF;
  IF (NEW.cc_emails IS NOT NULL) THEN
    IF (NEW.cc_emails !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'cc_emails must be zero or more valid email addresses separated by commas; you entered %', NEW.cc_emails;
    END IF;
  END IF;
  IF (NEW.reminder_days < 1) OR (NEW.reminder_days > 28) THEN
    RAISE EXCEPTION 'reminder_days must be in the range 1 to 28; you entered %', NEW.reminder_days;
  END IF;
  IF (NEW.start_date > NEW.end_date) THEN
    RAISE EXCEPTION 'start_date cannot be after end_date; you entered start_date % and end_date %', NEW.start_date, NEW.end_date;
  END IF;
  -- strip all spaces in routing_number
  NEW.routing_number = regexp_replace(NEW.routing_number, '\s+', '', 'g');
  IF (NEW.routing_number = '') THEN
    NEW.routing_number = NULL;
  END IF;
  -- if operation is INSERT or routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.routing_number IS DISTINCT FROM OLD.routing_number)) THEN
    IF NEW.routing_number IS NULL THEN
      RAISE EXCEPTION 'routing_number is required';
    END IF;
    IF NEW.routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'routing_number must be 9 digits; you entered %', NEW.routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.routing_number) THEN
      RAISE EXCEPTION 'routing_number not found in FedACH Participants; you entered %', NEW.routing_number;
    END IF;
  END IF;
  -- strip all spaces in account_number
  NEW.account_number = regexp_replace(NEW.account_number, '\s+', '', 'g');
  IF (NEW.account_number = '') THEN
    NEW.account_number = NULL;
  END IF;
  -- if operation is INSERT or account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number)) THEN
    IF NEW.account_number IS NULL THEN
      RAISE EXCEPTION 'account_number is required';
    END IF;
    IF NEW.account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'account_number must be numeric; you entered %', NEW.account_number;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

-- mask sensitive fields in op_recur_pmt_ach_tbl
CREATE FUNCTION mask_op_recur_pmt_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.routing_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.routing_number IS DISTINCT FROM OLD.routing_number) THEN
      NEW.routing_number = left(NEW.routing_number, 1) || repeat('*', 5) || right(NEW.routing_number, 3);
    END IF;
  END IF;
  IF (NEW.account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number) THEN
      IF length(NEW.account_number) > 4 THEN
        NEW.account_number = left(NEW.account_number, 1) || repeat('*', length(NEW.account_number) - 4) || right(NEW.account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified_op_recur_pmt_ach BEFORE UPDATE ON op_recur_pmt_ach_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created_op_recur_pmt_ach BEFORE INSERT ON op_recur_pmt_ach_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_op_recur_pmt_ach BEFORE INSERT OR UPDATE ON op_recur_pmt_ach_tbl FOR EACH ROW EXECUTE PROCEDURE validate_op_recur_pmt_ach();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt_op_recur_pmt_ach BEFORE INSERT OR UPDATE ON op_recur_pmt_ach_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('routing_number', 'account_number');
CREATE TRIGGER z2_mask_op_recur_pmt_ach BEFORE INSERT OR UPDATE ON op_recur_pmt_ach_tbl FOR EACH ROW EXECUTE PROCEDURE mask_op_recur_pmt_ach();

--DROP VIEW op_recur_pmt_ach_vw;
--DROP TABLE op_recur_pmt_ach_tbl;
--DROP SEQUENCE op_recur_pmt_ach_seq;
--DROP FUNCTION validate_op_recur_pmt_ach();
--DROP FUNCTION mask_op_recur_pmt_ach();

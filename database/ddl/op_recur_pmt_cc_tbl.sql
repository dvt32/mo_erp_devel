CREATE SEQUENCE op_recur_pmt_cc_seq;

CREATE TABLE op_recur_pmt_cc_tbl (
    id integer DEFAULT nextval(('op_recur_pmt_cc_seq'::text)::regclass) NOT NULL PRIMARY KEY,
    account_id integer NOT NULL REFERENCES cust_acct_tbl (id),
    card_number varchar(16) NOT NULL,
    -- expire date should be YYYY-MM-DD. If day is unknown, use first day of the month (1).
    expire_date date NOT NULL,
    -- CVV/CVC: 3 digits for Visa/MC, 4 for AMEX
    cvv varchar(4),
    billing_name varchar(64) NOT NULL,
    billing_company varchar(64),
    billing_address_1 varchar(128) NOT NULL,
    billing_address_2 varchar(128),
    -- billing_address_num is the numeric portion of the street address. Computers have a hard time parsing it, so enter it here.
    billing_address_num varchar(16),
    billing_city varchar(48) NOT NULL,
    -- if billing_country is US, billing_state must be valid two character abbreviation in capital letters
    billing_state varchar(64),
    billing_zip_code varchar(20) NOT NULL,
    -- if country is left blank, it will default to US. This is the 2 character ISO 3166-1 alpha-2 country code.
    billing_country character(2) DEFAULT 'US' NOT NULL REFERENCES country_tbl (iso_2),
    billing_phone_number varchar(32),
    -- zero or more email addresses to send notice of charge and advance warning/reminder (if turned on)
    billing_emails text,
    -- also send emails to primary contact or not, depending on value here
    do_email_to_primary boolean DEFAULT TRUE NOT NULL,
    -- 1 - 28. Not 29, 30, 31 because of confusion for customers in leap years and variable number of days in different months
    day_of_month smallint NOT NULL,
    -- 1 = monthly, 3 = quarterly, 6 = biannually, 12 = annually
    cycle_period smallint DEFAULT 1 NOT NULL,
    amount numeric(8,2) NOT NULL,
    -- how many days before the charge to send a reminder of the upcoming charge. can be 1 - 28.
    reminder_days smallint,
    -- NULL if start immediately. If date here, do not charge before start_date.
    start_date date,
    -- NULL if repeat endlessly. If date here, do not charge after end_date.
    end_date date,
    -- set on_hold to TRUE to place all charges on hold until future action on our part. Defaults to FALSE.
    on_hold boolean DEFAULT FALSE NOT NULL,
    crypt_json jsonb,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    modified_ts timestamp with time zone,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_user character varying(255)
);

COMMENT ON TABLE op_recur_pmt_cc_tbl IS 'Recurring credit card payments managed by Micro Office employees';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.account_id IS 'link to cust_acct_tbl.id';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.expire_date IS 'Expiration date of card. Use 1 as day if no day on card.';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.cvv IS 'Card verification value. 3 digits for Visa/MC/Discover, 4 digits for AMEX.';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.billing_address_num IS 'Address number portion of address_1. This is because it is sometimes hard for the computer to parse the address number.';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.billing_country IS 'ISO 3166-1 alpha-2 country code. Defaults to US.';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.billing_emails IS 'Comma-separated list of email addresses to send advance warnings and notification of credit card charge. Email is also sent to primary contact if do_email_to_primary is true. Can be NULL. The first email in this list is used as the billing email on file with the credit card company (if one is requested by the merchant processor).';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.day_of_month IS 'What day of the month to debit funds. Can be 1 - 28.';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.cycle_period IS '1 = monthly, 3 = quarterly, 6 = biannually, 12 = annually. default is 1 (monthly).';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.do_email_to_primary IS 'True/false send email to primary contact on account';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.reminder_days IS 'How many days before day_of_month to send a reminder. Can be NULL or 0 to indicate no reminder.';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.start_date IS 'If not NULL, then no transactions will be charged before start_date';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.end_date IS 'If not NULL, then no transactions will be charged after end_date';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.on_hold IS 'When true, means recurring transaction should not be processed going forward, until set back to false';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.created_user IS 'User who created the row';
COMMENT ON COLUMN op_recur_pmt_cc_tbl.modified_user IS 'User who last modified the row';

CREATE VIEW op_recur_pmt_cc_vw AS
 SELECT op_recur_pmt_cc_tbl.id,
    cust_acct_tbl.name,
    op_recur_pmt_cc_tbl.card_number,
    op_recur_pmt_cc_tbl.expire_date,
    op_recur_pmt_cc_tbl.billing_name,
    op_recur_pmt_cc_tbl.billing_company,
    op_recur_pmt_cc_tbl.billing_address_1,
    op_recur_pmt_cc_tbl.billing_address_2,
    op_recur_pmt_cc_tbl.billing_address_num,
    op_recur_pmt_cc_tbl.billing_city,
    op_recur_pmt_cc_tbl.billing_state,
    op_recur_pmt_cc_tbl.billing_zip_code,
    op_recur_pmt_cc_tbl.billing_country,
    op_recur_pmt_cc_tbl.billing_phone_number,
    op_recur_pmt_cc_tbl.billing_emails,
    op_recur_pmt_cc_tbl.do_email_to_primary,
    op_recur_pmt_cc_tbl.day_of_month,
    op_recur_pmt_cc_tbl.cycle_period,
    op_recur_pmt_cc_tbl.amount,
    op_recur_pmt_cc_tbl.reminder_days,
    op_recur_pmt_cc_tbl.start_date,
    op_recur_pmt_cc_tbl.end_date,
    op_recur_pmt_cc_tbl.on_hold,
    op_recur_pmt_cc_tbl.created_ts,
    op_recur_pmt_cc_tbl.modified_ts,
    op_recur_pmt_cc_tbl.created_user,
    op_recur_pmt_cc_tbl.modified_user
   FROM op_recur_pmt_cc_tbl,
    cust_acct_tbl
  WHERE (op_recur_pmt_cc_tbl.account_id = cust_acct_tbl.id);

-- validate column values in op_recur_pmt_cc_tbl
CREATE FUNCTION validate_op_recur_pmt_cc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  NEW.billing_state = strip_collapse_space(NEW.billing_state);
  -- if country is US and state is not NULL, check that state is valid US state
  IF ((NEW.billing_country = 'US') AND (NEW.billing_state IS NOT NULL)) THEN
    IF NOT EXISTS (SELECT 1 FROM state_tbl WHERE abbreviation = NEW.billing_state) THEN
      RAISE EXCEPTION 'Please enter a valid 2 letter US state abbreviation in all caps; you entered %', NEW.billing_state;
    END IF;
  END IF;
  IF (NEW.billing_emails IS NOT NULL) THEN
    -- strip all spaces
    NEW.billing_emails = regexp_replace(NEW.billing_emails, '\s+', '', 'g');
    IF (NEW.billing_emails = '') THEN
      NEW.billing_emails = NULL;
    ELSIF (NEW.billing_emails !~* email_regexp_pattern) THEN
      RAISE EXCEPTION 'billing_emails must be zero or more valid email addresses separated by commas; you entered %', NEW.billing_emails;
    END IF;
  END IF;
  IF (NEW.day_of_month < 1) OR (NEW.day_of_month > 28) THEN
    RAISE EXCEPTION 'day_of_month must be in the range 1 to 28; you entered %', NEW.day_of_month;
  END IF;
  IF (NEW.cycle_period < 1) OR (NEW.cycle_period > 12) THEN
    RAISE EXCEPTION 'cycle_period must be in the range 1 to 12; you entered %', NEW.cycle_period;
  END IF;
  IF (NEW.amount <= 0) THEN
    RAISE EXCEPTION 'amount must be greater than 0; you entered %', NEW.amount;
  END IF;
  IF (NEW.reminder_days < 1) OR (NEW.reminder_days > 28) THEN
    RAISE EXCEPTION 'reminder_days must be in the range 1 to 28; you entered %', NEW.reminder_days;
  END IF;
  IF (NEW.start_date > NEW.end_date) THEN
    RAISE EXCEPTION 'start_date cannot be after end_date; you entered start_date % and end_date %', NEW.start_date, NEW.end_date;
  END IF;
  -- if operation is INSERT or card_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.card_number IS DISTINCT FROM OLD.card_number)) THEN
    IF NEW.card_number IS NULL THEN
      RAISE EXCEPTION 'card_number is required';
    END IF;
    IF NEW.card_number !~ '^\d{13,16}$' THEN
      RAISE EXCEPTION 'card_number must be 13 to 16 digits; you entered %', NEW.card_number;
    END IF;
    -- check validity of credit card number using Luhn algorithm
    IF (NOT is_valid_cc_luhn(NEW.card_number)) THEN
      RAISE EXCEPTION 'card_number is not a valid credit card number; you entered %', NEW.card_number;
    END IF;
  END IF;
  -- strip all spaces in cvv
  NEW.cvv = regexp_replace(NEW.cvv, '\s+', '', 'g');
  IF (NEW.cvv = '') THEN
    NEW.cvv = NULL;
  END IF;
  -- if cvv IS NOT NULL, check next step to see if we should validate
  IF (NEW.cvv IS NOT NULL) THEN
    -- if operation is INSERT or cvv changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate if so
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      IF NEW.cvv !~ '^\d{3,4}$' THEN
        RAISE EXCEPTION 'cvv must be 3 or 4 digits; you entered %', NEW.cvv;
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

-- mask sensitive fields in op_recur_pmt_cc_tbl
CREATE FUNCTION mask_op_recur_pmt_cc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.card_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.card_number IS DISTINCT FROM OLD.card_number) THEN
      NEW.card_number = left(NEW.card_number, 1) || repeat('*', length(NEW.card_number) - 5) || right(NEW.card_number, 4);
    END IF;
  END IF;
  IF (NEW.cvv IS NOT NULL) THEN
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      NEW.cvv = '****';
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified_op_recur_pmt_cc BEFORE UPDATE ON op_recur_pmt_cc_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created_op_recur_pmt_cc BEFORE INSERT ON op_recur_pmt_cc_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_op_recur_pmt_cc BEFORE INSERT OR UPDATE ON op_recur_pmt_cc_tbl FOR EACH ROW EXECUTE PROCEDURE validate_op_recur_pmt_cc();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt_op_recur_pmt_cc BEFORE INSERT OR UPDATE ON op_recur_pmt_cc_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('card_number', 'cvv');
CREATE TRIGGER z2_mask_op_recur_pmt_cc BEFORE INSERT OR UPDATE ON op_recur_pmt_cc_tbl FOR EACH ROW EXECUTE PROCEDURE mask_op_recur_pmt_cc();

--DROP VIEW op_recur_pmt_cc_vw;
--DROP TABLE op_recur_pmt_cc_tbl;
--DROP SEQUENCE op_recur_pmt_cc_seq;
--DROP FUNCTION validate_op_recur_pmt_cc();
--DROP FUNCTION mask_op_recur_pmt_cc();

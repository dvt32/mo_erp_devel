CREATE SEQUENCE password_seq;

CREATE TABLE password_tbl (
  id integer DEFAULT nextval('password_seq'::regclass) NOT NULL PRIMARY KEY,
  name varchar(255) NOT NULL,
  url text,
  phone_number varchar(21),
  account_number varchar(255),
  username varchar(255),
  password varchar(255),
  pin varchar(16),
  email varchar(255),
  sec_quest_1 text,
  sec_ans_1 text,
  sec_quest_2 text,
  sec_ans_2 text,
  sec_quest_3 text,
  sec_ans_3 text,
  sec_quest_4 text,
  sec_ans_4 text,
  sec_quest_5 text,
  sec_ans_5 text,
  sec_quest_6 text,
  sec_ans_6 text,
  sec_quest_7 text,
  sec_ans_7 text,
  note text,
  needs_update boolean DEFAULT false NOT NULL,
  view_id_list integer[],
  comment_id_list integer[],
  edit_id_list integer[],
  owner_id integer NOT NULL REFERENCES employee_tbl (id),
  table_column varchar(127),
  fk_id integer,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE password_tbl IS 'Passwords for vendors and other business partners through web sites, phone systems, and other modes of interaction. A single web site can have multiple passwords for different accounts.';
COMMENT ON COLUMN password_tbl.name IS 'Friendly name for the password entry';
COMMENT ON COLUMN password_tbl.account_number IS 'Account number that is associated with the account that the password is good for. Can mean different things for different vendors. E.g. for merchant processors, could be store number.';
COMMENT ON COLUMN password_tbl.pin IS 'PIN, if one is needed. Sometimes needed for phone interactions (or occasionally for web sites as well).';
COMMENT ON COLUMN password_tbl.sec_quest_1 IS 'Security question #1. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_ans_1 IS 'Security answer #1. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_quest_2 IS 'Security question #2. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_ans_2 IS 'Security answer #2. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_quest_3 IS 'Security question #3. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_ans_3 IS 'Security answer #3. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_quest_4 IS 'Security question #4. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_ans_4 IS 'Security answer #4. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_quest_5 IS 'Security question #5. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_ans_5 IS 'Security answer #5. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_quest_6 IS 'Security question #6. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_ans_6 IS 'Security answer #6. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_quest_7 IS 'Security question #7. Can be NULL.';
COMMENT ON COLUMN password_tbl.sec_ans_7 IS 'Security answer #7. Can be NULL.';
COMMENT ON COLUMN password_tbl.needs_update IS 'Flag that indicates whether the password needs to be updated, perhaps because current one does not work';
COMMENT ON COLUMN password_tbl.view_id_list IS 'List of ID values of employees who can view this password. An employee who can view can set the needs_update flag (but cannot clear it).';
COMMENT ON COLUMN password_tbl.comment_id_list IS 'List of ID values of employees who can comment on this password, meaning they can edit the note column and can set the needs_update flag (but cannot clear it).';
COMMENT ON COLUMN password_tbl.edit_id_list IS 'List of ID values of employees who can edit this password. Can edit most of the fields, apart from metadata fields or crypt_json.';
COMMENT ON COLUMN password_tbl.owner_id IS 'ID value of the employee that owns this password';
COMMENT ON COLUMN password_tbl.table_column IS 'Table and column that link to this row, in <table>.<column> format; can be vendor_tbl.id, govt_filing.id, or other table and column, or can be NULL if no linkage';
COMMENT ON COLUMN password_tbl.fk_id IS 'Foreign key ID of linked table; can be NULL';

-- validate column values in password_tbl
CREATE FUNCTION validate_password_tbl() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar := '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.name = strip_collapse_space(NEW.name);
  NEW.url = strip_collapse_space(NEW.url);
  NEW.phone_number = strip_collapse_space(NEW.phone_number);
  NEW.account_number = strip_collapse_space(NEW.account_number);
  NEW.username = strip_collapse_space(NEW.username);
  NEW.password = strip_collapse_space(NEW.password);
  NEW.pin = strip_collapse_space(NEW.pin);
  NEW.email = strip_space(NEW.email);
  IF ((NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern)) THEN
    RAISE EXCEPTION 'email must be a valid email address; you entered %', NEW.email;
  END IF;
  NEW.sec_quest_1 = strip_collapse_space(NEW.sec_quest_1);
  NEW.sec_quest_2 = strip_collapse_space(NEW.sec_quest_2);
  NEW.sec_quest_3 = strip_collapse_space(NEW.sec_quest_3);
  NEW.sec_quest_4 = strip_collapse_space(NEW.sec_quest_4);
  NEW.sec_quest_5 = strip_collapse_space(NEW.sec_quest_5);
  NEW.sec_quest_6 = strip_collapse_space(NEW.sec_quest_6);
  NEW.sec_quest_7 = strip_collapse_space(NEW.sec_quest_7);
  NEW.sec_ans_1 = strip_collapse_space(NEW.sec_ans_1);
  NEW.sec_ans_2 = strip_collapse_space(NEW.sec_ans_2);
  NEW.sec_ans_3 = strip_collapse_space(NEW.sec_ans_3);
  NEW.sec_ans_4 = strip_collapse_space(NEW.sec_ans_4);
  NEW.sec_ans_5 = strip_collapse_space(NEW.sec_ans_5);
  NEW.sec_ans_6 = strip_collapse_space(NEW.sec_ans_6);
  NEW.sec_ans_7 = strip_collapse_space(NEW.sec_ans_7);
  -- for each question and answer, check that either both have values or both are NULL
  IF (((NEW.sec_quest_1 IS NOT NULL) AND (NEW.sec_ans_1 IS NULL)) OR ((NEW.sec_quest_1 IS NULL) AND (NEW.sec_ans_1 IS NOT NULL))) THEN
    RAISE EXCEPTION 'security question and answer #1 must both be blank or answered; you entered just one';
  END IF;
  IF (((NEW.sec_quest_2 IS NOT NULL) AND (NEW.sec_ans_2 IS NULL)) OR ((NEW.sec_quest_2 IS NULL) AND (NEW.sec_ans_2 IS NOT NULL))) THEN
    RAISE EXCEPTION 'security question and answer #2 must both be blank or answered; you entered just one';
  END IF;
  IF (((NEW.sec_quest_3 IS NOT NULL) AND (NEW.sec_ans_3 IS NULL)) OR ((NEW.sec_quest_3 IS NULL) AND (NEW.sec_ans_3 IS NOT NULL))) THEN
    RAISE EXCEPTION 'security question and answer #3 must both be blank or answered; you entered just one';
  END IF;
  IF (((NEW.sec_quest_4 IS NOT NULL) AND (NEW.sec_ans_4 IS NULL)) OR ((NEW.sec_quest_4 IS NULL) AND (NEW.sec_ans_4 IS NOT NULL))) THEN
    RAISE EXCEPTION 'security question and answer #4 must both be blank or answered; you entered just one';
  END IF;
  IF (((NEW.sec_quest_5 IS NOT NULL) AND (NEW.sec_ans_5 IS NULL)) OR ((NEW.sec_quest_5 IS NULL) AND (NEW.sec_ans_5 IS NOT NULL))) THEN
    RAISE EXCEPTION 'security question and answer #5 must both be blank or answered; you entered just one';
  END IF;
  IF (((NEW.sec_quest_6 IS NOT NULL) AND (NEW.sec_ans_6 IS NULL)) OR ((NEW.sec_quest_6 IS NULL) AND (NEW.sec_ans_6 IS NOT NULL))) THEN
    RAISE EXCEPTION 'security question and answer #6 must both be blank or answered; you entered just one';
  END IF;
  IF (((NEW.sec_quest_7 IS NOT NULL) AND (NEW.sec_ans_7 IS NULL)) OR ((NEW.sec_quest_7 IS NULL) AND (NEW.sec_ans_7 IS NOT NULL))) THEN
    RAISE EXCEPTION 'security question and answer #7 must both be blank or answered; you entered just one';
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  NEW.table_column = strip_space(NEW.table_column);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in password_tbl
CREATE FUNCTION mask_password_tbl() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.password IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.password IS DISTINCT FROM OLD.password) THEN
      -- mask password by setting it to '***'
      NEW.password = '***';
    END IF;
  END IF;
  IF (NEW.pin IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.pin IS DISTINCT FROM OLD.pin) THEN
      -- mask pin by setting it to '***'
      NEW.pin = '***';
    END IF;
  END IF;
  IF (NEW.sec_ans_1 IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.sec_ans_1 IS DISTINCT FROM OLD.sec_ans_1) THEN
      -- mask security answer by setting it to '***'
      NEW.sec_ans_1 = '***';
    END IF;
  END IF;
  IF (NEW.sec_ans_2 IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.sec_ans_2 IS DISTINCT FROM OLD.sec_ans_2) THEN
      -- mask security answer by setting it to '***'
      NEW.sec_ans_2 = '***';
    END IF;
  END IF;
  IF (NEW.sec_ans_3 IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.sec_ans_3 IS DISTINCT FROM OLD.sec_ans_3) THEN
      -- mask security answer by setting it to '***'
      NEW.sec_ans_3 = '***';
    END IF;
  END IF;
  IF (NEW.sec_ans_4 IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.sec_ans_4 IS DISTINCT FROM OLD.sec_ans_4) THEN
      -- mask security answer by setting it to '***'
      NEW.sec_ans_4 = '***';
    END IF;
  END IF;
  IF (NEW.sec_ans_5 IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.sec_ans_5 IS DISTINCT FROM OLD.sec_ans_5) THEN
      -- mask security answer by setting it to '***'
      NEW.sec_ans_5 = '***';
    END IF;
  END IF;
  IF (NEW.sec_ans_6 IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.sec_ans_6 IS DISTINCT FROM OLD.sec_ans_6) THEN
      -- mask security answer by setting it to '***'
      NEW.sec_ans_6 = '***';
    END IF;
  END IF;
  IF (NEW.sec_ans_7 IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.sec_ans_7 IS DISTINCT FROM OLD.sec_ans_7) THEN
      -- mask security answer by setting it to '***'
      NEW.sec_ans_7 = '***';
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON password_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON password_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON password_tbl FOR EACH ROW EXECUTE PROCEDURE validate_password_tbl();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON password_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('password', 'pin', 'sec_ans_1', 'sec_ans_2', 'sec_ans_3', 'sec_ans_4', 'sec_ans_5', 'sec_ans_6', 'sec_ans_7');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON password_tbl FOR EACH ROW EXECUTE PROCEDURE mask_password_tbl();

-- set up audit log for password_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON password_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON password_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('password_tbl');

--DROP TABLE password_tbl;
--DROP SEQUENCE password_seq;
--DROP FUNCTION validate_password_tbl();
--DROP FUNCTION mask_password_tbl();

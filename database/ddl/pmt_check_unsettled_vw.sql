CREATE VIEW pmt_check_unsettled_vw AS
 SELECT pmt_nmi_txn_vw.id,
    pmt_nmi_txn_vw.pmt_method,
    pmt_nmi_txn_vw.floor_label,
    pmt_nmi_txn_vw.cust_acct_id,
    pmt_nmi_txn_vw.name,
    pmt_nmi_txn_vw.amount,
    pmt_nmi_txn_vw.scheduled_date,
    pmt_nmi_txn_vw.action_list
   FROM pmt_nmi_txn_vw
  WHERE (((pmt_nmi_txn_vw.action_list)::text ~~ '%|sale|APPROVED%'::text) AND ((pmt_nmi_txn_vw.action_list)::text !~ similar_escape('%\|(settle|void|check_return)\|%'::text, NULL::text)));

CREATE SEQUENCE pmt_nmi_post_txn_seq;
CREATE TABLE pmt_nmi_post_txn_tbl (
  id integer DEFAULT nextval('pmt_nmi_post_txn_seq'::regclass) NOT NULL PRIMARY KEY,
  -- type: sale, auth, credit, validate, offline, capture, void, refund, or update
  type character varying(8) NOT NULL,
  -- Username assigned to merchant account
  username character varying(24) NOT NULL,
  -- abbreviated credit card number (first number plus last three numbers); required for cc txns
  ccnumber character varying(24),
  -- credit card expiration date in MMYY format; required for cc txns
  ccexp character(4),
  -- name on the customer's ACH account; required for ACH txns
  checkname character varying(255),
  -- customer's bank routing number (abbreviated: first number plus last three numbers); required for ACH txns
  checkaba character(9),
  -- customer's bank account number (abbreviated: first number plus last three numbers); required for ACH txns
  checkaccount character varying(17),
  -- business or personal; required for ACH txns
  account_holder_type character varying(8),
  -- checking or savings; required for ACH txns
  account_type character varying(8),
  -- standard entry class code of the ACH transaction (PPD, WEB, TEL, or CCD); required for ACH txns
  sec_code character(3),
  -- amount to be charged; for type = validate, amount must be omitted or set to 0.00
  amount numeric(8,2),
  -- the transaction currency; format: ISO 4217; usually will be blank or USD
  currency character(3),
  -- type of payment; default is creditcard; can be creditcard or check
  payment character varying(10),
  -- if using multiple MIDs, route to processor (processor_id is obtained under Settings->Load Balancing in the Control Panel)
  processor_id character varying(64),
  -- specify authorization code; for use with "offline" action only
  authorization_code character varying(64),
  -- sets the time in seconds for duplication transaction checking on supported processors; set to 0 to disable duplicate chkg
  dup_seconds integer,
  -- set payment descriptor on supported processors
  descriptor character varying(64),
  -- set payment descriptor phone on supported processors
  descriptor_phone character varying(64),
  -- set payment descriptor address on supported processors
  descriptor_address character varying(64),
  -- set payment descriptor city on supported processors
  descriptor_city character varying(64),
  -- set payment descriptor state on supported processors
  descriptor_state character varying(64),
  -- set payment descriptor postal on supported processors
  descriptor_postal character varying(64),
  -- set payment descriptor country on supported processors
  descriptor_country character varying(64),
  -- set payment descriptor mcc on supported processors
  descriptor_mcc character varying(64),
  -- set payment descriptor merchant id on supported processors
  descriptor_merchant_id character varying(64),
  -- set payment descriptor url on supported processors
  descriptor_url character varying(64),
  -- should be set to 'recurring' to mark payment as a recurring transaction
  billing_method character varying(9),
  -- order description; legacy variable includes orderdescription
  order_description character varying(64),
  -- order id
  orderid character varying(64),
  -- IP address of cardholder; this field is recommended; format: xxx.xxx.xxx.xxx
  ipaddress character varying(64),
  -- total tax amount
  tax numeric(8,2),
  -- total shipping amount
  shipping numeric(8,2),
  -- original purchase order
  ponumber character varying(64),
  -- cardholder's first name; legacy variable includes firstname
  first_name character varying(64),
  -- cardholder's last name; legacy variable includes lastname
  last_name character varying(64),
  -- cardholder's company
  company character varying(64),
  -- card billing address
  address1 character varying(80),
  -- card billing address, line 2
  address2 character varying(80),
  -- card billing city
  city character varying(64),
  -- card billing state; format: CC
  state character(2),
  -- card billing zip code
  zip character varying(16),
  -- card billing country; country codes are as shown in ISO 3166; format: CC
  country character(2),
  -- billing phone number
  phone character varying(64),
  -- billing fax number
  fax character varying(64),
  -- billing email address
  email character varying(64),
  -- customer's social security number; checked against bad check writers database if check verification is enabled
  social_security_number character(9),
  -- driver's license number
  drivers_license_number character varying(64),
  -- driver's license date of birth
  drivers_license_dob date,
  -- the state that issued the customer's driver's license
  drivers_license_state character varying(64),
  -- shipping first name
  shipping_firstname character varying(64),
  -- shipping last name
  shipping_lastname character varying(64),
  -- shipping company
  shipping_company character varying(64),
  -- shipping address
  shipping_address1 character varying(64),
  -- shipping address, line 2
  shipping_address2 character varying(64),
  -- shipping city
  shipping_city character varying(64),
  -- shipping state
  shipping_state character varying(64),
  -- shipping zip code
  shipping_zip character varying(64),
  -- shipping country; country codes are as shown in ISO 3166; format: CC
  shipping_country character(2),
  -- shipping email address
  shipping_email character varying(64),
  -- you can pass custom information in up to 20 fields; format: merchant_defined_field_1=Value
  merchant_defined_fields text ARRAY[20],
  -- if set to true, when the customer is charged, they will be sent a transaction receipt; Values: 'true' or 'false'
  customer_receipt character varying(5),
  /* Recurring specific fields */
  -- recurring action to be processed; Values: add_subscription
  recurring character varying(64),
  -- create a subscription tied to a Plan ID if the sale/auth transaction is successful
  plan_id character varying(64),
  -- the number of payments before the recurring plan is complete; Note: Use '0' for 'until canceled'
  plan_payments character varying(64),
  -- the plan amount to be charged each billing cycle; Format: x.xx
  plan_amount character varying(64),
  -- how often, in days, to charge the customer; cannot be set with 'month_frequency' or 'day_of_month'
  day_frequency character varying(64),
  -- how often, in months, to charge the customer; cannot be set with 'day_frequency'; must be set with 'day_of_month'; Values: 1 trough 24
  month_frequency character varying(64),
  -- the day that the customer will be charged; cannot be set with 'day_frequency'; must be set with 'month_frequency'; Values: 1 through 31 - for months without 29, 30, or 31 days, the charge will be on the last day
  day_of_month character varying(64),
  -- the first day that the customer will be charged; Format: YYYYMMDD
  start_date character varying(64),
  /* Customer Vault specific fields */
  -- Associate payment information with a Customer Vault record if the transaction is successful; Valuds: 'add_customer' or 'update_customer'
  customer_vault character varying(64),
  -- Specifies a customer vault id; if not set, the payment gateway will randomly generate a customer vault id
  customer_vault_id character varying(64),
  /* Level III specific order fields */
  -- Freight or shipping amount included in the transaction amount; Default: '0.00'; Format: x.xx
  -- already in main section above
  --shipping character varying(64),
  -- The sales tax, included in the transaction amount, associated with the purchase; setting tax equal to '-1' indicates an order that is exempt from sales tax; Default: '0.00'; Format: x.xx
  -- already in main section above
  --tax character varying(64),
  -- Purchase order number supplied by cardholder
  -- already in main section above
  --ponumber character varying(64),
  -- Identifier assigned by the merchant; This defaults to gateway transaction id
  --orderid character varying(64),
  -- Shipping country (e.g. US); Format: CC
  -- already in main section above
  --shipping_country character varying(64),
  -- Postal/ZIP code of the address where purchased goods will be delivered; this field can be identical to the 'ship_from_postal' if the customer is present and takes immediate possession of the goods 
  shipping_postal character varying(64),
  -- Postal/ZIP code of the address from where purchased goods are being shipped; defaults to merchant profile postal code
  ship_from_postal character varying(64),
  -- 4 character international description code of the overall goods or services being supplied; the acquirer or processor will provide a list of current codes
  summary_commodity_code character varying(64),
  -- Purchase order date, defaults to the date of the transaction; Format: YYMMDD
  order_date character(6),
  /* Level III specific line item detail fields */
  -- see pmt_nmi_level3_line_item_tbl
  --
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_nmi_post_txn_tbl IS 'NMI/SoluPay payment transaction';
COMMENT ON COLUMN pmt_nmi_post_txn_tbl.ccnumber IS 'Credit card number. Either this or checkaccount should be specified, but not both.';

CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_nmi_post_txn_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_nmi_post_txn_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_nmi_post_txn_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_nmi_post_txn();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON pmt_nmi_post_txn_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('ccnumber', 'checkaba', 'checkaccount', 'social_security_number', 'drivers_license_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON pmt_nmi_post_txn_tbl FOR EACH ROW EXECUTE PROCEDURE mask_pmt_nmi_post_txn();

CREATE TABLE pmt_nmi_level3_line_item_tbl (
  pmt_nmi_post_txn_id integer NOT NULL REFERENCES pmt_nmi_post_txn_tbl (id),
  -- ordinal position in the list of ordered items
  ordinal integer NOT NULL,
  -- Merchant defined description code of the item being purchased
  item_product_code varchar(64),
  -- Description of the item(s) being supplied
  item_description varchar(64),
  -- International description code of the individual good or service being supplied; The acquirer or processor will provide a list of current codes
  item_commodity_code varchar(64),
  -- Code for units of measurement as used in international trade; Default: 'EACH'
  item_unit_of_measure varchar(64),
  -- Unit cost of item purchased, may contain up to 4 decimal places
  item_unit_cost varchar(64),
  -- Quantity of the item(s) being purchased; Default: '1'
  item_quantity varchar(64),
  -- Purchase amount associated with the item; Defaults to: 'item_unit_cost' x 'item_quantity' rounded to the nearest penny
  item_total_amount varchar(64),
  -- Amount of tax on specific item; amount should not be included in 'total_amount'; Default: '0.00'
  item_tax_amount varchar(64),
  -- Percentage representing the value-added tax applied; Default: '0.00'
  item_tax_rate varchar(64),
  -- Discount amount which can have been applied by the merchant on the sale of the specific item; Amount should not be included in 'total_amount'
  item_discount_amount varchar(64),
  -- Discount rate for the line item; 1% = 1.00; Default: '0.00'
  item_discount_rate varchar(64),
  -- Type of value-added taxes that are being used
  item_tax_type varchar(64),
  -- Tax identification number of the merchant that reported the alternate tax amount
  item_alternate_tax_id varchar(64),
  UNIQUE (pmt_nmi_post_txn_id, ordinal)
);

--DROP TABLE pmt_nmi_level3_line_item_tbl;
--DROP TABLE pmt_nmi_post_txn_tbl;
--DROP SEQUENCE pmt_nmi_post_txn_seq;

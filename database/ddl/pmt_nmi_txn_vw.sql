CREATE SEQUENCE pmt_nmi_txn_seq;

CREATE TABLE pmt_nmi_txn_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  -- Name of table that uses the ID from this row: 'pmt_nmi_txn_ach_tbl' or 'pmt_nmi_txn_card_tbl'
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE pmt_nmi_txn_id_tbl IS 'Common table to reserve ID values for pmt_nmi_txn_x_tbl tables';
COMMENT ON COLUMN pmt_nmi_txn_id_tbl.tbl_name IS 'Name of table that uses the ID from this row';

CREATE TYPE nmi_txn_purpose_type AS ENUM ('service', 'confirm');

CREATE TABLE pmt_nmi_txn_ach_tbl (
  id integer DEFAULT nextval('pmt_nmi_txn_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to cust_acct_tbl.id
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  -- account_type: checking or savings
  account_type character varying(8) NOT NULL REFERENCES enum_bank_account_type_tbl (account_type),
  -- owned_by: business or personal
  owned_by character varying(8) NOT NULL REFERENCES enum_bank_account_owned_by_tbl (owned_by),
  -- name of the person or company that owns the money in the bank account
  registered_name character varying(64),
  routing_number character(9) NOT NULL,
  account_number character varying(17) NOT NULL,
  -- amount to be charged
  amount money NOT NULL,
  -- scheduled_date: date to debit the customer, in local time of the customer
  scheduled_date date NOT NULL,
  note text,
  -- Zero or more email addresses separated by commas. Send ACH debit transaction result to the email address(es).
  email text,
  -- if hold true, then do not process the transaction yet.
  hold boolean NOT NULL DEFAULT false,
  -- Link to pmt_vault_txn_tbl.id. Can be NULL if NMI transaction did not originate from vault transaction.
  pmt_vault_txn_id integer UNIQUE REFERENCES pmt_vault_txn_tbl (id),
  -- txn_purpose: service or confirm. service means payment/credit related to service. confirm means payment method confirmation.
  txn_purpose nmi_txn_purpose_type DEFAULT 'service' NOT NULL,
  -- Link to pmt_method_confirm_tbl.id. Must be NULL if not a confirm transaction and non-NULL if a confirm transaction.
  pmt_method_confirm_id integer REFERENCES pmt_method_confirm_tbl (id),
  -- order description; legacy variable includes orderdescription
  order_description character varying(255),
  -- 1 = Transaction Approved, 2 = Transaction Declined, 3 = Error in transaction data or system error
  response char(1),
  -- Textual response
  responsetext text,
  -- Transaction authorization code.
  authcode varchar(255),
  -- Payment gateway transaction id.
  transactionid varchar(255),
  -- order id
  orderid character varying(255),
  -- Result Code Table
  -- 100 Transaction was approved.
  -- 200 Transaction was declined by processor.
  -- 201 Do not honor.
  -- 202 Insufficient funds.
  -- 203 Over limit.
  -- 204 Transaction not allowed.
  -- 220 Incorrect payment information.
  -- 221 No such card issuer.
  -- 222 No card number on file with issuer.
  -- 223 Expired card.
  -- 224 Invalid expiration date.
  -- 225 Invalid card security code.
  -- 240 Call issuer for further information.
  -- 250 Pick up card.
  -- 251 Lost card.
  -- 252 Stolen card.
  -- 253 Fraudulent card.
  -- 260 Declined with further instructions available. (See response text)
  -- 261 Declined-Stop all recurring payments.
  -- 262 Declined-Stop this recurring program.
  -- 263 Declined-Update cardholder data available.
  -- 264 Declined-Retry in a few days.
  -- 300 Transaction was rejected by gateway.
  -- 400 Transaction error returned by processor.
  -- 410 Invalid merchant configuration.
  -- 411 Merchant account is inactive.
  -- 420 Communication error.
  -- 421 Communication error with issuer.
  -- 430 Duplicate transaction at processor.
  -- 440 Processor format error.
  -- 441 Invalid transaction information.
  -- 460 Processor feature not available.
  -- 461 Unsupported card type.
  response_code char(3),
  -- complete request body
  request_body text,
  request_posted_ts timestamp with time zone,
  -- complete response body
  response_body text,
  -- transaction XML as returned via query API call to NMI Solupay
  txn_xml xml,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_nmi_txn_ach_tbl IS 'NMI/SoluPay ACH payment transaction using Direct Post API';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.cust_acct_id IS 'Link to cust_acct_tbl.id';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.account_type IS 'checking or savings';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.owned_by IS 'business or personal';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.registered_name IS 'Name of the person or entity that has beneficial ownership of the bank account';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.routing_number IS 'ACH bank routing number';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.account_number IS 'Bank account number';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.scheduled_date IS 'date to debit the customer, in local time of the customer';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.email IS 'Zero or more email addresses separated by commas. Send ACH debit transaction result to the email address(es).';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.pmt_vault_txn_id IS 'Link to pmt_vault_txn_tbl.id';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.txn_purpose IS 'service or confirm. service means payment/credit related to service. confirm means payment method confirmation.';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.pmt_method_confirm_id IS 'Link to pmt_method_confirm_tbl.id. Must be NULL if not a confirm transaction and non-NULL if a confirm transaction.';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.response IS '1 = Transaction Approved, 2 = Transaction Declined, 3 = Error in transaction data or system error';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.responsetext IS 'Textual response';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.transactionid IS 'Payment gateway transaction ID';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.orderid IS 'The original order id passed in the transaction request';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.response_code IS 'Result Code Table.
100 Transaction was approved.
200 Transaction was declined by processor.
201 Do not honor.
202 Insufficient funds.
203 Over limit.
204 Transaction not allowed.
220 Incorrect payment information.
221 No such card issuer.
222 No card number on file with issuer.
223 Expired card.
224 Invalid expiration date.
225 Invalid card security code.
240 Call issuer for further information.
250 Pick up card.
251 Lost card.
252 Stolen card.
253 Fraudulent card.
260 Declined with further instructions available. (See response text)
261 Declined-Stop all recurring payments.
262 Declined-Stop this recurring program.
263 Declined-Update cardholder data available.
264 Declined-Retry in a few days.
300 Transaction was rejected by gateway.
400 Transaction error returned by processor.
410 Invalid merchant configuration.
411 Merchant account is inactive.
420 Communication error.
421 Communication error with issuer.
430 Duplicate transaction at processor.
440 Processor format error.
441 Invalid transaction information.
460 Processor feature not available.
461 Unsupported card type.';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.request_body IS 'complete request body';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.response_body IS 'complete response body';
COMMENT ON COLUMN pmt_nmi_txn_ach_tbl.txn_xml IS 'transaction XML as returned by query API call to NMI Solupay';

-- validate column values in pmt_nmi_txn_ach_tbl
CREATE FUNCTION validate_pmt_nmi_txn_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  -- disallow update of posted transaction if user is not a member of update_posted_txn role.
  IF NOT pg_has_role('update_posted_txn', 'member') AND (TG_OP = 'UPDATE') AND OLD.request_posted_ts IS NOT NULL THEN
    RAISE EXCEPTION 'row id % has already posted; update not allowed', OLD.id;
  END IF;
  NEW.registered_name = strip_collapse_space(NEW.registered_name);
  NEW.routing_number = strip_space(NEW.routing_number);
  -- if operation is INSERT or routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.routing_number IS DISTINCT FROM OLD.routing_number)) THEN
    IF NEW.routing_number IS NULL THEN
      RAISE EXCEPTION 'routing_number is required';
    END IF;
    IF NEW.routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'routing_number must be 9 digits; you entered %', NEW.routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.routing_number) THEN
      RAISE EXCEPTION 'routing_number not found in FedACH Participants; you entered %', NEW.routing_number;
    END IF;
  END IF;
  NEW.account_number = strip_space(NEW.account_number);
  -- if operation is INSERT or account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number)) THEN
    IF NEW.account_number IS NULL THEN
      RAISE EXCEPTION 'account_number is required';
    END IF;
    IF NEW.account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'account_number must be numeric; you entered %', NEW.account_number;
    END IF;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  NEW.email = strip_collapse_space(NEW.email);
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  IF ((NEW.txn_purpose = 'service') AND (NEW.pmt_method_confirm_id IS NOT NULL)) THEN
    RAISE EXCEPTION 'when txn_purpose is service, pmt_method_confirm_id must be NULL';
  END IF;
  IF ((NEW.txn_purpose = 'confirm') AND (NEW.pmt_method_confirm_id IS NULL)) THEN
    RAISE EXCEPTION 'when txn_purpose is confirm, pmt_method_confirm_id must be non-NULL';
  END IF;
  RETURN NEW;
END;
$$;

-- restrict delete of posted transactions to members of delete_posted_txn
CREATE FUNCTION delete_pmt_nmi_txn_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF OLD.request_posted_ts IS NOT NULL AND NOT pg_has_role('delete_posted_txn', 'member') THEN
    RAISE EXCEPTION 'row id % has already posted; delete not allowed', OLD.id;
  END IF;
  -- either transaction not posted or user is member of delete_posted_txn, so proceed with delete
  RETURN OLD;
END;
$$;

-- mask sensitive fields in pmt_nmi_txn_ach_tbl
CREATE FUNCTION mask_pmt_nmi_txn_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number) THEN
      IF length(NEW.account_number) > 4 THEN
        NEW.account_number = left(NEW.account_number, 1) || repeat('*', length(NEW.account_number) - 4) || right(NEW.account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('pmt_nmi_txn_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('pmt_nmi_txn_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('pmt_nmi_txn_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_nmi_txn_ach();
CREATE TRIGGER delete_row BEFORE DELETE ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE delete_pmt_nmi_txn_ach();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('routing_number', 'account_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON pmt_nmi_txn_ach_tbl FOR EACH ROW EXECUTE PROCEDURE mask_pmt_nmi_txn_ach();

-- set up audit log for pmt_nmi_txn_ach_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_nmi_txn_ach_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_nmi_txn_ach_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_nmi_txn_ach_tbl');

CREATE TABLE pmt_nmi_txn_card_tbl (
  id integer DEFAULT nextval('pmt_nmi_txn_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to cust_acct_tbl.id
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  card_number varchar(16) NOT NULL,
  -- expire date should be YYYY-MM-DD. If day is unknown, use first day of the month (1).
  expire_date date NOT NULL,
  -- CVV/CVC: 3 digits for Visa/MC, 4 for AMEX
  cvv varchar(4),
  billing_first_name varchar(64) NOT NULL,
  billing_last_name varchar(64) NOT NULL,
  billing_company varchar(64),
  billing_address_1 varchar(128) NOT NULL,
  billing_address_2 varchar(128),
  -- billing_address_num is the numeric portion of the street address. Computers have a hard time parsing it, so enter it here.
  billing_address_num varchar(16),
  billing_city varchar(48) NOT NULL,
  -- if billing_country is US, billing_state must be valid two character abbreviation in capital letters
  billing_state varchar(64),
  billing_zip_code varchar(20) NOT NULL,
  -- if country is left blank, it will default to US. This is the 2 character ISO 3166-1 alpha-2 country code.
  billing_country character(2) DEFAULT 'US' NOT NULL REFERENCES country_tbl (iso_2),
  -- amount to be charged (total including all taxes and convenience fees)
  amount money NOT NULL,
  -- convenience fee portion of amount
  convenience_fee money DEFAULT 0.00 NOT NULL,
  -- scheduled_date: date to charge the customer, in local time of the customer
  scheduled_date date NOT NULL,
  note text,
  -- Zero or more email addresses separated by commas. Send card charge transaction result to the email address(es).
  email text,
  hold boolean NOT NULL DEFAULT false,
  -- Link to pmt_vault_txn_tbl.id. Can be NULL if NMI transaction did not originate from vault transaction.
  pmt_vault_txn_id integer UNIQUE REFERENCES pmt_vault_txn_tbl (id),
  -- order description; legacy variable includes orderdescription
  order_description character varying(255),
  -- 1 = Transaction Approved, 2 = Transaction Declined, 3 = Error in transaction data or system error
  response char(1),
  -- Textual response
  responsetext text,
  -- Transaction authorization code.
  authcode varchar(255),
  -- Payment gateway transaction id.
  transactionid varchar(255),
  -- AVS Response Codes
  -- X   Exact match, 9-character numeric ZIP
  -- Y   Exact match, 5-character numeric ZIP
  -- D   Exact match, 5-character numeric ZIP
  -- M   Exact match, 5-character numeric ZIP
  -- A   Address match only
  -- B   Address match only
  -- W   9-character numeric ZIP match only
  -- Z   5-character ZIP match only
  -- P   5-character ZIP match only
  -- L   5-character ZIP match only
  -- N   No address or ZIP match only
  -- C   No address or ZIP match only
  -- U   Address unavailable
  -- G   Non-U.S. issuer does not participate
  -- I   Non-U.S. issuer does not participate
  -- R   Issuer system unavailable
  -- E   Not a mail/phone order
  -- S   Service not supported
  -- O   AVS not available
  -- B   AVS not available
  avsresponse char(1),
  -- CVV Response Codes
  -- M   CVV2/CVC2 match
  -- N   CVV2/CVC2 no match
  -- P   Not processed
  -- S   Merchant has indicated that CVV2/CVC2 is not present on card
  -- U   Issuer is not certified and/or has not provided Visa encryption keys
  cvvresponse char(1),
  -- order id
  orderid character varying(255),
  -- Result Code Table
  -- 100 Transaction was approved.
  -- 200 Transaction was declined by processor.
  -- 201 Do not honor.
  -- 202 Insufficient funds.
  -- 203 Over limit.
  -- 204 Transaction not allowed.
  -- 220 Incorrect payment information.
  -- 221 No such card issuer.
  -- 222 No card number on file with issuer.
  -- 223 Expired card.
  -- 224 Invalid expiration date.
  -- 225 Invalid card security code.
  -- 240 Call issuer for further information.
  -- 250 Pick up card.
  -- 251 Lost card.
  -- 252 Stolen card.
  -- 253 Fraudulent card.
  -- 260 Declined with further instructions available. (See response text)
  -- 261 Declined-Stop all recurring payments.
  -- 262 Declined-Stop this recurring program.
  -- 263 Declined-Update cardholder data available.
  -- 264 Declined-Retry in a few days.
  -- 300 Transaction was rejected by gateway.
  -- 400 Transaction error returned by processor.
  -- 410 Invalid merchant configuration.
  -- 411 Merchant account is inactive.
  -- 420 Communication error.
  -- 421 Communication error with issuer.
  -- 430 Duplicate transaction at processor.
  -- 440 Processor format error.
  -- 441 Invalid transaction information.
  -- 460 Processor feature not available.
  -- 461 Unsupported card type.
  response_code char(3),
  -- complete request body
  request_body text,
  request_posted_ts timestamp with time zone,
  -- complete response body
  response_body text,
  -- transaction XML as returned via query API call to NMI Solupay
  txn_xml xml,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_nmi_txn_card_tbl IS 'NMI/SoluPay credit card payment transaction using Direct Post API';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.cust_acct_id IS 'Link to cust_acct_tbl.id';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.expire_date IS 'Expiration date of card. Use 1 as day if no day on card.';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.cvv IS 'Card verification value. 3 digits for Visa/MC/Discover, 4 digits for AMEX.';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.billing_address_num IS 'Address number portion of address_1. This is because it is sometimes hard for the computer to parse the address number.';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.billing_country IS 'ISO 3166-1 alpha-2 two character country code. Defaults to US.';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.amount IS 'Total amount charged (including surcharges and fees)';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.convenience_fee IS 'Convenience fee portion of amount (not added on top of amount)';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.scheduled_date IS 'date to charge the customer, in local time of the customer';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.email IS 'Zero or more email addresses separated by commas. Send card charge transaction result to the email address(es).';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.pmt_vault_txn_id IS 'Link to pmt_vault_txn_tbl.id';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.response IS '1 = Transaction Approved, 2 = Transaction Declined, 3 = Error in transaction data or system error';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.responsetext IS 'Textual response';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.transactionid IS 'Payment gateway transaction ID';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.orderid IS 'The original order id passed in the transaction request';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.response_code IS 'Result Code Table.
100 Transaction was approved.
200 Transaction was declined by processor.
201 Do not honor.
202 Insufficient funds.
203 Over limit.
204 Transaction not allowed.
220 Incorrect payment information.
221 No such card issuer.
222 No card number on file with issuer.
223 Expired card.
224 Invalid expiration date.
225 Invalid card security code.
240 Call issuer for further information.
250 Pick up card.
251 Lost card.
252 Stolen card.
253 Fraudulent card.
260 Declined with further instructions available. (See response text)
261 Declined-Stop all recurring payments.
262 Declined-Stop this recurring program.
263 Declined-Update cardholder data available.
264 Declined-Retry in a few days.
300 Transaction was rejected by gateway.
400 Transaction error returned by processor.
410 Invalid merchant configuration.
411 Merchant account is inactive.
420 Communication error.
421 Communication error with issuer.
430 Duplicate transaction at processor.
440 Processor format error.
441 Invalid transaction information.
460 Processor feature not available.
461 Unsupported card type.';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.request_body IS 'complete request body';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.response_body IS 'complete response body';
COMMENT ON COLUMN pmt_nmi_txn_card_tbl.txn_xml IS 'transaction XML as returned by query API call to NMI Solupay';

-- validate column values in pmt_nmi_txn_card_tbl
CREATE FUNCTION validate_pmt_nmi_txn_card() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  -- disallow update of posted transaction if user is not a member of update_posted_txn role.
  IF NOT pg_has_role('update_posted_txn', 'member') AND (TG_OP = 'UPDATE') AND OLD.request_posted_ts IS NOT NULL THEN
    RAISE EXCEPTION 'row id % has already posted; update not allowed', OLD.id;
  END IF;
  NEW.card_number = strip_space(NEW.card_number);
  -- if operation is INSERT or card_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.card_number IS DISTINCT FROM OLD.card_number)) THEN
    IF NEW.card_number IS NULL THEN
      RAISE EXCEPTION 'card_number is required';
    END IF;
    IF NEW.card_number !~ '^\d{13,16}$' THEN
      RAISE EXCEPTION 'card_number must be 13 to 16 digits; you entered %', NEW.card_number;
    END IF;
    -- check validity of credit card number using Luhn algorithm
    IF (NOT is_valid_cc_luhn(NEW.card_number)) THEN
      RAISE EXCEPTION 'card_number is not a valid credit card number; you entered %', NEW.card_number;
    END IF;
  END IF;
  NEW.cvv = strip_space(NEW.cvv);
  -- if cvv IS NOT NULL, check next step to see if we should validate
  IF (NEW.cvv IS NOT NULL) THEN
    -- if operation is INSERT or cvv changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate if so
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      IF NEW.cvv !~ '^\d{3,4}$' THEN
        RAISE EXCEPTION 'cvv must be 3 or 4 digits; you entered %', NEW.cvv;
      END IF;
    END IF;
  END IF;
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.billing_first_name = strip_collapse_space(NEW.billing_first_name);
  NEW.billing_last_name = strip_collapse_space(NEW.billing_last_name);
  NEW.billing_company = strip_collapse_space(NEW.billing_company);
  NEW.billing_address_1 = strip_collapse_space(NEW.billing_address_1);
  NEW.billing_address_2 = strip_collapse_space(NEW.billing_address_2);
  NEW.billing_address_num = strip_collapse_space(NEW.billing_address_num);
  NEW.billing_city = strip_collapse_space(NEW.billing_city);
  NEW.billing_state = strip_collapse_space(NEW.billing_state);
  -- if country is US and state is not NULL, check that state is valid US state
  IF ((NEW.billing_country = 'US') AND (NEW.billing_state IS NOT NULL)) THEN
    IF NOT EXISTS (SELECT 1 FROM state_tbl WHERE abbreviation = NEW.billing_state) THEN
      RAISE EXCEPTION 'Please enter a valid 2 letter US state abbreviation in all caps; you entered %', NEW.billing_state;
    END IF;
  END IF;
  NEW.billing_zip_code = strip_collapse_space(NEW.billing_zip_code);
  NEW.billing_country = strip_collapse_space(NEW.billing_country);
  NEW.note = strip_collapse_space(NEW.note);
  NEW.email = strip_collapse_space(NEW.email);
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  RETURN NEW;
END;
$$;

-- restrict delete of posted transactions to members of delete_posted_txn
CREATE FUNCTION delete_pmt_nmi_txn_card() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF OLD.request_posted_ts IS NOT NULL AND NOT pg_has_role('delete_posted_txn', 'member') THEN
    RAISE EXCEPTION 'row id % has already posted; delete not allowed', OLD.id;
  END IF;
  -- either transaction not posted or user is member of delete_posted_txn, so proceed with delete
  RETURN OLD;
END;
$$;

-- mask sensitive fields in pmt_nmi_txn_card_tbl
CREATE FUNCTION mask_pmt_nmi_txn_card() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.card_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.card_number IS DISTINCT FROM OLD.card_number) THEN
      NEW.card_number = left(NEW.card_number, 6) || repeat('*', length(NEW.card_number) - 10) || right(NEW.card_number, 4);
    END IF;
  END IF;
  IF (NEW.cvv IS NOT NULL) THEN
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      NEW.cvv = '****';
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('pmt_nmi_txn_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('pmt_nmi_txn_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('pmt_nmi_txn_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_nmi_txn_card();
CREATE TRIGGER delete_row BEFORE DELETE ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE delete_pmt_nmi_txn_card();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('card_number', 'cvv');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON pmt_nmi_txn_card_tbl FOR EACH ROW EXECUTE PROCEDURE mask_pmt_nmi_txn_card();

-- set up audit log for pmt_nmi_txn_card_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_nmi_txn_card_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_nmi_txn_card_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_nmi_txn_card_tbl');

CREATE TYPE nmi_txn_action_type AS ENUM ('auth', 'capture', 'sale', 'settle', 'check_return', 'check_late_return', 'void', 'refund', 'credit');
CREATE TYPE nmi_txn_action_source_type AS ENUM ('api', 'internal', 'quickbooks', 'virtual_terminal');

CREATE TABLE pmt_nmi_txn_action_tbl (
  -- Link to pmt_nmi_txn_vw.id.
  pmt_nmi_txn_id integer NOT NULL REFERENCES pmt_nmi_txn_id_tbl (id),
  amount money NOT NULL,
  action_type nmi_txn_action_type NOT NULL,
  action_ts timestamp with time zone NOT NULL,
  success boolean NOT NULL,
  ip_address inet,
  source nmi_txn_action_source_type,
  username varchar(16),
  response_text varchar(255),
  -- 0 or 9-10 digit number
  batch_id varchar(16) NOT NULL,
  -- empty or a number
  processor_batch_id varchar(16),
  -- Result Code Table
  -- 100 Transaction was approved.
  -- 200 Transaction was declined by processor.
  -- 201 Do not honor.
  -- 202 Insufficient funds.
  -- 203 Over limit.
  -- 204 Transaction not allowed.
  -- 220 Incorrect payment information.
  -- 221 No such card issuer.
  -- 222 No card number on file with issuer.
  -- 223 Expired card.
  -- 224 Invalid expiration date.
  -- 225 Invalid card security code.
  -- 240 Call issuer for further information.
  -- 250 Pick up card.
  -- 251 Lost card.
  -- 252 Stolen card.
  -- 253 Fraudulent card.
  -- 260 Declined with further instructions available. (See response text)
  -- 261 Declined-Stop all recurring payments.
  -- 262 Declined-Stop this recurring program.
  -- 263 Declined-Update cardholder data available.
  -- 264 Declined-Retry in a few days.
  -- 300 Transaction was rejected by gateway.
  -- 400 Transaction error returned by processor.
  -- 410 Invalid merchant configuration.
  -- 411 Merchant account is inactive.
  -- 420 Communication error.
  -- 421 Communication error with issuer.
  -- 430 Duplicate transaction at processor.
  -- 440 Processor format error.
  -- 441 Invalid transaction information.
  -- 460 Processor feature not available.
  -- 461 Unsupported card type.
  response_code varchar(3) NOT NULL,
  -- always seems to be empty
  processor_response_text varchar(255),
  -- seems to be empty or 1 or 2 character response code
  processor_response_code varchar(8),
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255),
  -- tuple (pmt_nmi_txn_id, action_ts) is unique across rows
  -- (i.e. no two rows linked to the same payment transaction will have the same action timestamp)
  UNIQUE (pmt_nmi_txn_id, action_ts)
);

COMMENT ON TABLE pmt_nmi_txn_action_tbl IS 'NMI/SoluPay transaction subsequent actions';
COMMENT ON COLUMN pmt_nmi_txn_action_tbl.pmt_nmi_txn_id IS 'Link to pmt_nmi_txn_vw.id';
COMMENT ON COLUMN pmt_nmi_txn_action_tbl.action_type IS 'sale | settle | check_return | check_late_return | void | refund';
COMMENT ON COLUMN pmt_nmi_txn_action_tbl.action_ts IS 'transaction -> action -> date element, expressed as timestamp in UTC';
COMMENT ON COLUMN pmt_nmi_txn_action_tbl.source IS 'api | internal | quickbooks | virtual_terminal';
COMMENT ON COLUMN pmt_nmi_txn_action_tbl.username IS 'NMI username used to process transaction';

CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_nmi_txn_action_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_nmi_txn_action_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

-- set up audit log for pmt_nmi_txn_action_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_nmi_txn_action_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_nmi_txn_action_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_nmi_txn_action_tbl');

CREATE VIEW pmt_nmi_txn_vw AS
 SELECT pmt_nmi_txn_ach_tbl.id,
    'ach'::character varying(4) AS pmt_method,
    floor_tbl.short_global_label AS floor_label,
    pmt_nmi_txn_ach_tbl.cust_acct_id,
    cust_acct_tbl.name,
    building_tbl.nmi_username,
    pmt_nmi_txn_ach_tbl.amount,
    pmt_nmi_txn_ach_tbl.amount AS prefee_amount,
    NULL AS convenience_fee,
    NULL AS convenience_pct,
    pmt_nmi_txn_ach_tbl.scheduled_date,
    pmt_nmi_txn_ach_tbl.scheduled_date::timestamp at time zone building_tbl.timezone AS scheduled_ts_utc,
    pmt_nmi_txn_ach_tbl.note,
    pmt_nmi_txn_ach_tbl.hold,
    pmt_nmi_txn_ach_tbl.pmt_vault_txn_id,
    pmt_nmi_txn_ach_tbl.txn_purpose,
    pmt_nmi_txn_ach_tbl.pmt_method_confirm_id,
    pmt_nmi_txn_ach_tbl.account_type,
    pmt_nmi_txn_ach_tbl.owned_by,
    pmt_nmi_txn_ach_tbl.registered_name,
    pmt_nmi_txn_ach_tbl.routing_number,
    pmt_nmi_txn_ach_tbl.account_number,
    NULL AS card_number,
    NULL AS expire_date,
    NULL AS cvv,
    NULL AS billing_first_name,
    NULL AS billing_last_name,
    NULL AS billing_company,
    NULL AS billing_address_1,
    NULL AS billing_address_2,
    NULL AS billing_address_num,
    NULL AS billing_city,
    NULL AS billing_state,
    NULL AS billing_zip_code,
    NULL AS billing_country,
    pmt_nmi_txn_ach_tbl.email,
    pmt_nmi_txn_ach_tbl.response,
    pmt_nmi_txn_ach_tbl.responsetext,
    pmt_nmi_txn_ach_tbl.authcode,
    pmt_nmi_txn_ach_tbl.transactionid,
    NULL AS avsresponse,
    NULL AS cvvresponse,
    pmt_nmi_txn_ach_tbl.response_code,
    array_agg(to_char(pmt_nmi_txn_action_tbl.action_ts, 'YYYYMMDD') || '|' || pmt_nmi_txn_action_tbl.action_type::text || '|' || replace(CASE WHEN pmt_nmi_txn_action_tbl.response_text IS NULL THEN '' ELSE pmt_nmi_txn_action_tbl.response_text END,' ','_')) AS action_list,
    pmt_nmi_txn_ach_tbl.request_body,
    pmt_nmi_txn_ach_tbl.request_posted_ts,
    pmt_nmi_txn_ach_tbl.response_body,
    pmt_nmi_txn_ach_tbl.txn_xml,
    floor_tbl.id AS floor_id,
    pmt_nmi_txn_ach_tbl.crypt_json,
    pmt_nmi_txn_ach_tbl.created_ts,
    pmt_nmi_txn_ach_tbl.created_user,
    pmt_nmi_txn_ach_tbl.modified_ts,
    pmt_nmi_txn_ach_tbl.modified_user
   FROM ((pmt_nmi_txn_ach_tbl
     JOIN cust_acct_tbl ON ((pmt_nmi_txn_ach_tbl.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id))
     JOIN building_tbl ON (floor_tbl.building_id = building_tbl.id)
     LEFT JOIN pmt_nmi_txn_action_tbl ON (pmt_nmi_txn_ach_tbl.id = pmt_nmi_txn_action_tbl.pmt_nmi_txn_id))
  GROUP BY pmt_nmi_txn_ach_tbl.id, floor_tbl.short_global_label, cust_acct_tbl.name, building_tbl.nmi_username, building_tbl.timezone, floor_tbl.id
UNION ALL
 SELECT pmt_nmi_txn_card_tbl.id,
    'card'::character varying(4) AS pmt_method,
    floor_tbl.short_global_label AS floor_label,
    pmt_nmi_txn_card_tbl.cust_acct_id,
    cust_acct_tbl.name,
    building_tbl.nmi_username,
    pmt_nmi_txn_card_tbl.amount,
    (pmt_nmi_txn_card_tbl.amount - pmt_nmi_txn_card_tbl.convenience_fee) AS prefee_amount,
    pmt_nmi_txn_card_tbl.convenience_fee,
    to_char((CASE WHEN pmt_nmi_txn_card_tbl.convenience_fee IS NULL THEN NULL ELSE CASE WHEN (pmt_nmi_txn_card_tbl.amount - pmt_nmi_txn_card_tbl.convenience_fee = 0::money) THEN 0 ELSE pmt_nmi_txn_card_tbl.convenience_fee/(pmt_nmi_txn_card_tbl.amount - pmt_nmi_txn_card_tbl.convenience_fee)*100 END END)::numeric(4,2), '90D99') || '%' AS convenience_pct,
    pmt_nmi_txn_card_tbl.scheduled_date,
    pmt_nmi_txn_card_tbl.scheduled_date::timestamp at time zone building_tbl.timezone AS scheduled_ts_utc,
    pmt_nmi_txn_card_tbl.note,
    pmt_nmi_txn_card_tbl.hold,
    pmt_nmi_txn_card_tbl.pmt_vault_txn_id,
    'service' AS txn_purpose,
    NULL AS pmt_method_confirm_id,
    NULL AS account_type,
    NULL AS owned_by,
    NULL AS registered_name,
    NULL AS routing_number,
    NULL AS account_number,
    pmt_nmi_txn_card_tbl.card_number,
    pmt_nmi_txn_card_tbl.expire_date,
    pmt_nmi_txn_card_tbl.cvv,
    pmt_nmi_txn_card_tbl.billing_first_name,
    pmt_nmi_txn_card_tbl.billing_last_name,
    pmt_nmi_txn_card_tbl.billing_company,
    pmt_nmi_txn_card_tbl.billing_address_1,
    pmt_nmi_txn_card_tbl.billing_address_2,
    pmt_nmi_txn_card_tbl.billing_address_num,
    pmt_nmi_txn_card_tbl.billing_city,
    pmt_nmi_txn_card_tbl.billing_state,
    pmt_nmi_txn_card_tbl.billing_zip_code,
    pmt_nmi_txn_card_tbl.billing_country,
    pmt_nmi_txn_card_tbl.email,
    pmt_nmi_txn_card_tbl.response,
    pmt_nmi_txn_card_tbl.responsetext,
    pmt_nmi_txn_card_tbl.authcode,
    pmt_nmi_txn_card_tbl.transactionid,
    pmt_nmi_txn_card_tbl.avsresponse,
    pmt_nmi_txn_card_tbl.cvvresponse,
    pmt_nmi_txn_card_tbl.response_code,
    array_agg(to_char(pmt_nmi_txn_action_tbl.action_ts, 'YYYYMMDD') || '|' || pmt_nmi_txn_action_tbl.action_type::text || '|' || replace(CASE WHEN pmt_nmi_txn_action_tbl.response_text IS NULL THEN '' ELSE pmt_nmi_txn_action_tbl.response_text END,' ','_')) AS action_list,
    pmt_nmi_txn_card_tbl.request_body,
    pmt_nmi_txn_card_tbl.request_posted_ts,
    pmt_nmi_txn_card_tbl.response_body,
    pmt_nmi_txn_card_tbl.txn_xml,
    floor_tbl.id AS floor_id,
    pmt_nmi_txn_card_tbl.crypt_json,
    pmt_nmi_txn_card_tbl.created_ts,
    pmt_nmi_txn_card_tbl.created_user,
    pmt_nmi_txn_card_tbl.modified_ts,
    pmt_nmi_txn_card_tbl.modified_user
   FROM ((pmt_nmi_txn_card_tbl
     JOIN cust_acct_tbl ON ((pmt_nmi_txn_card_tbl.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id))
     JOIN building_tbl ON (floor_tbl.building_id = building_tbl.id)
     LEFT JOIN pmt_nmi_txn_action_tbl ON (pmt_nmi_txn_card_tbl.id = pmt_nmi_txn_action_tbl.pmt_nmi_txn_id))
  GROUP BY pmt_nmi_txn_card_tbl.id, floor_tbl.short_global_label, cust_acct_tbl.name, building_tbl.nmi_username, building_tbl.timezone, floor_tbl.id;

--DROP VIEW pmt_nmi_txn_vw;
--DROP TABLE pmt_nmi_txn_action_tbl;
--DROP TYPE nmi_txn_action_type;
--DROP TYPE nmi_txn_action_source_type;
--DROP TABLE pmt_nmi_txn_card_tbl;
--DROP FUNCTION validate_pmt_nmi_txn_card();
--DROP FUNCTION mask_pmt_nmi_txn_card();
--DROP TABLE pmt_nmi_txn_ach_tbl;
--DROP FUNCTION validate_pmt_nmi_txn_ach();
--DROP FUNCTION mask_pmt_nmi_txn_ach();
--DROP TABLE pmt_nmi_txn_id_tbl;
--DROP SEQUENCE pmt_nmi_txn_seq;

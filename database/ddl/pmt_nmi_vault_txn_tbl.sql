CREATE SEQUENCE pmt_nmi_vault_txn_seq;
CREATE TABLE pmt_nmi_vault_txn_tbl (
  id integer DEFAULT nextval('pmt_nmi_vault_txn_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to cust_acct_tbl.id
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  amount numeric(8,2) NOT NULL,
  scheduled_date date NOT NULL,
  note text,
  hold boolean NOT NULL DEFAULT false,
  -- Order description. Legacy variable includes: orderdescription.
  order_description varchar(255),
  -- 1 = Transaction Approved, 2 = Transaction Declined, 3 = Error in transaction data or system error
  response char(1),
  -- Textual response
  responsetext text,
  -- Transaction authorization code.
  authcode varchar(255),
  -- Payment gateway transaction id.
  transactionid varchar(255),
  -- AVS Response Codes
  -- X   Exact match, 9-character numeric ZIP
  -- Y   Exact match, 5-character numeric ZIP
  -- D   Exact match, 5-character numeric ZIP
  -- M   Exact match, 5-character numeric ZIP
  -- A   Address match only
  -- B   Address match only
  -- W   9-character numeric ZIP match only
  -- Z   5-character ZIP match only
  -- P   5-character ZIP match only
  -- L   5-character ZIP match only
  -- N   No address or ZIP match only
  -- C   No address or ZIP match only
  -- U   Address unavailable
  -- G   Non-U.S. issuer does not participate
  -- I   Non-U.S. issuer does not participate
  -- R   Issuer system unavailable
  -- E   Not a mail/phone order
  -- S   Service not supported
  -- O   AVS not available
  -- B   AVS not available
  avsresponse char(1),
  -- CVV Response Codes
  -- M   CVV2/CVC2 match
  -- N   CVV2/CVC2 no match
  -- P   Not processed
  -- S   Merchant has indicated that CVV2/CVC2 is not present on card
  -- U   Issuer is not certified and/or has not provided Visa encryption keys
  cvvresponse char(1),
  -- The original order id passed in the transaction request.
  orderid varchar(255),
  -- Result Code Table
  -- 100 Transaction was approved.
  -- 200 Transaction was declined by processor.
  -- 201 Do not honor.
  -- 202 Insufficient funds.
  -- 203 Over limit.
  -- 204 Transaction not allowed.
  -- 220 Incorrect payment information.
  -- 221 No such card issuer.
  -- 222 No card number on file with issuer.
  -- 223 Expired card.
  -- 224 Invalid expiration date.
  -- 225 Invalid card security code.
  -- 240 Call issuer for further information.
  -- 250 Pick up card.
  -- 251 Lost card.
  -- 252 Stolen card.
  -- 253 Fraudulent card.
  -- 260 Declined with further instructions available. (See response text)
  -- 261 Declined-Stop all recurring payments.
  -- 262 Declined-Stop this recurring program.
  -- 263 Declined-Update cardholder data available.
  -- 264 Declined-Retry in a few days.
  -- 300 Transaction was rejected by gateway.
  -- 400 Transaction error returned by processor.
  -- 410 Invalid merchant configuration.
  -- 411 Merchant account is inactive.
  -- 420 Communication error.
  -- 421 Communication error with issuer.
  -- 430 Duplicate transaction at processor.
  -- 440 Processor format error.
  -- 441 Invalid transaction information.
  -- 460 Processor feature not available.
  -- 461 Unsupported card type.
  response_code char(3),
  -- complete request body
  request_body text,
  request_posted_ts timestamp with time zone,
  -- complete response body
  response_body text,
  -- transaction XML as returned via query API call to NMI Solupay
  txn_xml xml,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_nmi_vault_txn_tbl IS 'NMI/SoluPay payment transaction using vault ID';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.cust_acct_id IS 'Link to cust_acct_tbl.id';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.response IS '1 = Transaction Approved, 2 = Transaction Declined, 3 = Error in transaction data or system error';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.responsetext IS 'Textual response';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.authcode IS 'Transaction authorization code';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.transactionid IS 'Payment gateway transaction ID';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.avsresponse IS 'AVS Response Codes.
X   Exact match, 9-character numeric ZIP
Y   Exact match, 5-character numeric ZIP
D   Exact match, 5-character numeric ZIP
M   Exact match, 5-character numeric ZIP
A   Address match only
B   Address match only
W   9-character numeric ZIP match only
Z   5-character ZIP match only
P   5-character ZIP match only
L   5-character ZIP match only
N   No address or ZIP match only
C   No address or ZIP match only
U   Address unavailable
G   Non-U.S. issuer does not participate
I   Non-U.S. issuer does not participate
R   Issuer system unavailable
E   Not a mail/phone order
S   Service not supported
O   AVS not available
B   AVS not available';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.cvvresponse IS 'CVV Response Codes.
M   CVV2/CVC2 match
N   CVV2/CVC2 no match
P   Not processed
S   Merchant has indicated that CVV2/CVC2 is not present on card
U   Issuer is not certified and/or has not provided Visa encryption keys';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.orderid IS 'The original order id passed in the transaction request';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.response_code IS 'Result Code Table.
100 Transaction was approved.
200 Transaction was declined by processor.
201 Do not honor.
202 Insufficient funds.
203 Over limit.
204 Transaction not allowed.
220 Incorrect payment information.
221 No such card issuer.
222 No card number on file with issuer.
223 Expired card.
224 Invalid expiration date.
225 Invalid card security code.
240 Call issuer for further information.
250 Pick up card.
251 Lost card.
252 Stolen card.
253 Fraudulent card.
260 Declined with further instructions available. (See response text)
261 Declined-Stop all recurring payments.
262 Declined-Stop this recurring program.
263 Declined-Update cardholder data available.
264 Declined-Retry in a few days.
300 Transaction was rejected by gateway.
400 Transaction error returned by processor.
410 Invalid merchant configuration.
411 Merchant account is inactive.
420 Communication error.
421 Communication error with issuer.
430 Duplicate transaction at processor.
440 Processor format error.
441 Invalid transaction information.
460 Processor feature not available.
461 Unsupported card type.';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.request_body IS 'complete request body';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.response_body IS 'complete response body';
COMMENT ON COLUMN pmt_nmi_vault_txn_tbl.txn_xml IS 'transaction XML as returned by query API call to NMI Solupay';

CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_nmi_vault_txn_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_nmi_vault_txn_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

-- set up audit log for pmt_nmi_vault_txn_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_nmi_vault_txn_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_nmi_vault_txn_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_nmi_vault_txn_tbl');

CREATE VIEW pmt_nmi_vault_txn_vw AS
 SELECT pmt_nmi_vault_txn_tbl.id,
    floor_tbl.short_global_label AS floor_label,
    pmt_nmi_vault_txn_tbl.cust_acct_id,
    cust_acct_tbl.name AS cust_acct_name,
    building_tbl.nmi_username,
    trim(to_char(pmt_nmi_vault_txn_tbl.amount, '999990.99')) AS amount,
    pmt_nmi_vault_txn_tbl.scheduled_date,
    pmt_nmi_vault_txn_tbl.note,
    pmt_nmi_vault_txn_tbl.hold,
    pmt_nmi_vault_txn_tbl.response,
    pmt_nmi_vault_txn_tbl.responsetext,
    pmt_nmi_vault_txn_tbl.authcode,
    pmt_nmi_vault_txn_tbl.transactionid,
    pmt_nmi_vault_txn_tbl.avsresponse,
    pmt_nmi_vault_txn_tbl.cvvresponse,
    pmt_nmi_vault_txn_tbl.orderid,
    pmt_nmi_vault_txn_tbl.response_code,
    pmt_nmi_vault_txn_tbl.request_body,
    pmt_nmi_vault_txn_tbl.request_posted_ts,
    pmt_nmi_vault_txn_tbl.response_body,
    pmt_nmi_vault_txn_tbl.txn_xml,
    pmt_nmi_vault_txn_tbl.created_ts,
    pmt_nmi_vault_txn_tbl.modified_ts
   FROM ((pmt_nmi_vault_txn_tbl
     JOIN cust_acct_tbl ON ((pmt_nmi_vault_txn_tbl.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id))
     JOIN building_tbl ON (floor_tbl.building_id = building_tbl.id));

CREATE VIEW pmt_upcoming_nmi_vault_txn_vw AS
 SELECT pmt_nmi_vault_txn_tbl.id,
    floor_tbl.short_global_label AS floor_label,
    pmt_nmi_vault_txn_tbl.cust_acct_id,
    cust_acct_tbl.name AS cust_acct_name,
    building_tbl.nmi_username,
    pmt_nmi_vault_txn_tbl.amount,
    pmt_nmi_vault_txn_tbl.scheduled_date,
    pmt_nmi_vault_txn_tbl.note,
    pmt_nmi_vault_txn_tbl.hold,
    pmt_nmi_vault_txn_tbl.created_ts,
    pmt_nmi_vault_txn_tbl.modified_ts
   FROM ((pmt_nmi_vault_txn_tbl
     JOIN cust_acct_tbl ON ((pmt_nmi_vault_txn_tbl.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id))
     JOIN building_tbl ON (floor_tbl.building_id = building_tbl.id))
   WHERE pmt_nmi_vault_txn_tbl.scheduled_date > current_date;

--DROP VIEW pmt_upcoming_nmi_vault_txn_vw;
--DROP VIEW pmt_nmi_vault_txn_vw;
--DROP TABLE pmt_nmi_vault_txn_tbl;
--DROP SEQUENCE pmt_nmi_vault_txn_seq;

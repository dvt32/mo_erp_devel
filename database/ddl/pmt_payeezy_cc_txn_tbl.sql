CREATE SEQUENCE pmt_payeezy_cc_txn_seq;
CREATE TABLE pmt_payeezy_cc_txn_tbl (
  id integer DEFAULT nextval('pmt_payeezy_cc_txn_seq'::regclass) NOT NULL PRIMARY KEY,
  account_id integer REFERENCES cust_acct_tbl (id),
  -- ExactID in REST/SOAP, gateway_id in JSON
  gateway_id varchar(10) NOT NULL,
  -- Transaction_Type in REST/SOAP, transaction_type in JSON
  -- Populated with a two-digit string indicator. The indicator identifies the transaction type. Descriptions of these transaction types can be found here. Supported values include:
  -- 00 = Purchase
  -- 01 = Pre-Authorization
  -- 02 = Pre-Authorization Completion
  -- 03 = Forced Post
  -- 04 = Refund
  -- 05 = Pre-Authorization Only
  -- 07 = PayPal Order
  -- 13 = Void
  -- 32 = Tagged Pre-Authorization Completion
  -- 33 = Tagged Void
  -- 34 = Tagged Refund
  -- 83 = CashOut (ValueLink, v9 or higher end point only)
  -- 85 = Activation (ValueLink, v9 or higher end point only)
  -- 86 = Balance Inquiry (ValueLink, v9 or higher end point only)
  -- 88 = Reload (ValueLink, v9 or higher end point only)
  -- 89 = Deactivation (ValueLink, v9 or higher end point only)
  transaction_type char(2) NOT NULL,
  -- DollarAmount in REST/SOAP, amount in JSON. can be NULL for certain transaction types.
  amount numeric(8,2),
  -- Card_Number in REST/SOAP, cc_number in JSON
  -- The customer''s credit card number. Not used for tagged transaction types.
  cc_number varchar(16),
  -- Transaction_Tag in REST/SOAP, transaction_tag in JSON
  -- A unique identifier to associate with a tagged transaction. Only for tagged transaction types. Maximum Length is 10 Digits.
  transaction_tag varchar(10),
  -- Authorization_Num in REST/SOAP, authorization_num in JSON
  -- This is the authorization number returned by the cardholder’s financial institution when a transaction has been approved. This value needs to be sent when sending various transaction types such as preauthorization completion, void, or tagged transaction.
  authorization_num varchar(8),
  -- Expiry_Date in REST/SOAP, cc_expiry in JSON
  -- The credit card expiry date in the format mmyy. Property for manually entering expiry date. If Track1 or Track2 is populated, there is no need to set this field.
  cc_expiry char(4),
  -- CardHoldersName in REST/SOAP, cardholder_name in JSON
  -- The customer’s name. The following characters will be stripped from this field: ; ` " / % as well as --
  cardholder_name varchar(30),
  -- CVDCode in REST/SOAP, cvd_code in JSON
  -- This is the 0, 3, or 4-digit code on the back of the credit card sometimes called the CVV2 or CVD value.
  cvd_code varchar(4),
  -- CVD_Presence_Ind in REST/SOAP, cvd_presence_ind in JSON
  -- The number indicates how the CVV2 value should be handled when processing. The value must be either null or the integer 0, 1, 2, or 9. Note that null defaults to 0.
  -- Null or 0 = Not Supported (Default)
  -- 1 = Value provided by Cardholder
  -- 2 = Value provided on card is Illegible
  -- 9 = Cardholder states data is not available
  cvd_presence_ind char(1),
  -- Reference_No in REST/SOAP, reference_no in JSON
  -- A merchant defined value that can be used to internally identify the transaction. This value is passed through to the Payeezy Gateway unmodified, and may be searched in First Data Payeezy Gateway Real-time Payment Manager (RPM). It is not passed on to the financial institution. The following characters will be stripped from this field: ; ` " / % as well as -- (2 consecutive dashes).
  -- NOTE: For non-international transactions, DO NOT USE the following characters: pipe (|), caret (^), percent symbol (%), backslash (\), or forward slash (/).
  -- For international transactions DO NOT USE the following punctuation: caret (^), backslash (\), openbracket ([), closed bracket (]), tilde (~) or accent key (`). If used the transaction will reject for Response Reason Code 225 (Invalid field data)
  -- For two stage transactions (pre-auth/completion), the reference number utilized at the time of pre-authorization will be utilized at
  -- time of transaction completion. An override of this value is not supported. The consistent usage of values enables a merchant to achieve the best interchange rates and least re-authorizations. Typically, the reference number field is the purchase order number or unique sequence value associated to a given transaction suite.
  reference_no varchar(20),
  -- ZipCode in REST/SOAP, zip_code in JSON
  -- Customer zip code used for qualifying transactions, only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data Payeezy Gateway only supports Level II processing for Visa and MasterCard. It does not support American Express Level II processing.
  zip_code varchar(10),
  -- Tax1Amount in REST/SOAP, tax1_amount in JSON
  -- Tax value included in total amount, only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the PST amount.
  -- Payeezy Gateway only supports Level II processing for Visa and MasterCard. It does not support American Express Level II processing.
  tax1_amount numeric(7,2),
  -- Tax1Number in REST/SOAP, tax1_number in JSON
  -- Registered number associated with the tax value. Used for reference or government claims purposes and only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the PST number.
  -- Payeezy Gateway only supports Level II processing for Visa and MasterCard. It does not support American Express Level II processing.
  tax1_number varchar(20),
  -- Tax2Amount in REST/SOAP, tax2_amount in JSON
  -- Tax value included in total amount, only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the GST amount.
  -- Payeezy Gateway only supports Level II processing for Visa and MasterCard. It does not support American Express Level II processing.
  tax2_amount numeric(7,2),
  -- Tax2Number in REST/SOAP, tax2_number in JSON
  -- Registered number associated with the tax value. Used for reference or government claims purposes and only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the PST number.
  -- Payeezy Gateway only supports Level II processing for Visa and MasterCard. It does not support American Express Level II processing.
  tax2_number varchar(20),
  -- Customer_Ref in REST/SOAP, customer_ref in JSON
  -- A merchant defined value that can be used to internally identify the transaction. This value is passed through to the Payeezy Gateway unmodified, and may be searched in First Data Payeezy Gateway Real-time Payment Manager (RPM). It is passed on to the financial institution. The following characters will be stripped from this field: ; ` " / % as well as -- (2 consecutive dashes).
  customer_ref varchar(20),
  -- Reference_3 in REST/SOAP, reference_3 in JSON
  -- A merchant defined value that can be used to internally identify the transaction. This value is passed through to the Payeezy Gateway unmodified. It is not searchable and is not passed on to the financial institution. The following characters will be stripped from this field: ; ` " / % as well as -- (2 consecutive dashes). Also used to pass on the Foreign Access Code for ValueLink transactions.
  reference_3 varchar(30),
  -- Client_IP in REST/SOAP, client_ip in JSON
  -- This is the IP address of the customer (i.e. client browser) connecting to the merchant. This value is stored for fraud investigation. It is not passed on to the financial institution.
  client_ip varchar(15),
  -- user_name in REST/SOAP, user_name in JSON
  -- This is the user_name of the user processing the transaction. This field is visible in the Real Time Payment manager as the "User ID" and defaults to "API-(ExactID)."
  user_name varchar(30),
  -- PartialRedemption in REST/SOAP, partial_redemption in JSON
  -- Submit true to allow partial redemptions, false otherwise. A partial redemption will be returned if only a portion of the requested funds are available. For example, if a transaction is submitted for $100, but only $80 is available on the customer's card, the $80 will be authorized or captured when this property is set to true. This property can be used for all types of pre-authorization and purchase transactions.
  partial_redemption boolean,
  -- TransarmorToken in REST/SOAP, transarmor_token in JSON
  -- Used to submit a Transarmor token for transaction processing rather than using a credit card number for processing. If using this property, CardType must also be populated.
  -- Note that when a terminal is set up with TransArmor, a value will be returned in this property each time a transaction is submitted using a regular card number.
  -- This property can only be used with the v9 or higher endpoint of the API.
  transarmor_token varchar(250),
  -- CardType in REST/SOAP, credit_card_type in JSON
  -- CardType value associated with a TransArrmor token, PayPal transaction, or if submitting a ValueLink transaction, this property's value is required to be "Gift." Required for TransArmor transactions, PayPal transactions, and ValueLink transactions that submit a card number.
  -- Note that when a terminal is set up with TransArmor, a value will be returned in this property each time a transaction is submitted using a regular card number.
  -- Possible values include "American Express", "Visa", "Mastercard", "Discover", "Diners Club", "JCB", "Gift", "PayPal"
  -- This property can only be used with the v9 or higher endpoint of the API.
  credit_card_type varchar(16),
  -- Address.Address1 in REST/SOAP, address.address1 in JSON
  address1 varchar(30),
  -- Address.Address2 in REST/SOAP, address.address2 in JSON
  address2 varchar(28),
  -- Address.City in REST/SOAP, address.city in JSON
  city varchar(20),
  -- Address.State in REST/SOAP, address.state in JSON. if country_code is US, state must be valid US state.
  state char(2),
  -- Address.Zip in REST/SOAP, address.zip in JSON
  zip varchar(10),
  -- Address.CountryCode in REST/SOAP, address.country_code in JSON. ISO 3166-1 alpha-2 country code.
  country_code char(2) REFERENCES country_tbl (iso_2),
  -- Address.PhoneNumber in REST/SOAP, address.phone_number in JSON
  -- Non digits will be removed before processing
  phone_number varchar(14),
  -- Address.PhoneType in REST/SOAP, address.phone_type in JSON
  -- Only the following values are accepted:
  -- H = Home
  -- W = Work
  -- D = Day
  -- N = Night
  -- PhoneType is required when the PhoneNumber field is populated in a transaction request. Otherwise, it is optional.
  phone_type char(1),
  -- response properties
  -- Transaction_Error in REST/SOAP, transaction_error in JSON
  -- This property indicates that there was an error during the processing of the transaction. Please refer to eCommerce Response Codes (ETG Codes) for further information.
  transaction_error boolean,
  -- Transaction_Approved in REST/SOAP, transaction_approved in JSON
  -- This property indicates that the bank approved a transaction and there are no pending errors. If further information is required, please check the Optional Response properties.
  transaction_approved boolean,
  -- Exact_Resp_code in REST/SOAP, exact_resp_code in JSON
  -- This property indicates the processing status of the transaction. Please refer to the section on Exception Handling for further information. The Transaction_Error property will return True if this property is not “00”.
  exact_resp_code char(2),
  -- Exact_Message in REST/SOAP, exact_message in JSON
  -- Message that accompanies the Exact_Resp_code.
  exact_message varchar(50),
  -- Bank_Resp_code in REST/SOAP
  -- This is a 2 or 3 digit code, provided by the financial institution, indicating the approval status of a transaction. The meaning of these codes is defined by the various financial institutions and is not under the control of the Global Gateway e4 web service API or Gateway. Please refer to the Transaction_Approved property for the approval status of a transaction.
  bank_resp_code varchar(3),
  -- Bank_Message in REST/SOAP
  -- A message provided by the financial institution describing the Response code above.
  bank_message varchar(80),
  -- Bank_Resp_code_2 in REST/SOAP
  -- A secondary response provided returned by the financial institution.
  bank_resp_code_2 varchar(2),
  -- Transaction_Tag in REST/SOAP
  -- A unique identifier to associate with a tagged transaction. This value overrides any value sent for the Request Property of the same name.
  transaction_tag_resp varchar(10),
  -- Authorization_Num in REST/SOAP
  -- This is the authorization number returned by the cardholder’s financial institution when a transaction has been approved. This value overrides any value sent for the Request Property of the same name.
  authorization_num_resp varchar(8),
  -- SequenceNo in REST/SOAP
  -- A digit sequentially incremented number generated by Global Gateway e4 and passed through to the financial institution. It is also passed back to the client in the transaction response. This number can be used for tracking and audit purposes.
  sequence_no varchar(50),
  -- AVS in REST/SOAP
  -- Supported AVS Results:
  -- X = exact match, 9 digit zip
  -- Y = exact match, 5 digit zip
  -- A = address match only
  -- W = 9 digit zip match only
  -- Z = 5 digit zip match only
  -- N = no address or zip match
  -- U = address unavailable
  -- G = non-North American issuer, does not participate
  -- R = issuer system unavailable
  -- E = not a Mail\Phone order
  -- S = service not supported
  -- Q = Bill to address did not pass edit checks
  -- D = International street address and postal code match
  -- B = International street address match, postal code not verified due to incompatable formats
  -- C = International street address and postal code not verified due to incompatable formats
  -- P = International postal code match, street address not verified due to incompatable format
  -- 1 = Cardholder name matches
  -- 2 = Cardholder name, billing address, and postal code match
  -- 3 = Cardholder name and billing postal code match
  -- 4 = Cardholder name and billing address match
  -- 5 = Cardholder name incorrect, billing address and postal code match
  -- 6 = Cardholder name incorrect, billing postal code matches
  -- 7 = Cardholder name incorrect, billing address matches
  -- 8 = Cardholder name, billing address, and postal code are all incorrect
  avs_resp char(1),
  -- CVV2 in REST/SOAP
  -- The CVV2 authentication code returned from the bank. Note: the value is null if CVV2 is not supported.
  -- Supported CVV Results:
  -- M = CVV2 / CVC2/CVD Match.
  -- N = CVV2 / CVC2/CVD No Match.
  -- P = Not Processed.
  -- S = Merchant has indicated that CVV2 / CVC2/CVD is not present on the card.
  -- U = Issuer is not certified and / or has not provided Visa encryption keys.
  cvv2_resp char(1),
  -- Retrieval_Ref_No in REST/SOAP
  -- The reference number returned with an AVS Result.
  retrieval_ref_no varchar(13),
  -- MerchantName in REST/SOAP
  -- Returned by Global Gateway e4 upon successful Authentication.
  merchant_name varchar(50),
  -- MerchantAddress in REST/SOAP
  -- Returned by Global Gateway e4 upon successful Authentication.
  merchant_address varchar(50),
  -- MerchantCity in REST/SOAP
  -- Returned by Global Gateway e4 upon successful Authentication.
  merchant_city varchar(25),
  -- MerchantProvince in REST/SOAP
  -- Returned by Global Gateway e4 upon successful Authentication.
  merchant_province varchar(64),
  -- MerchantCountry in REST/SOAP
  -- Returned by Global Gateway e4 upon successful Authentication.
  merchant_country varchar(50),
  -- MerchantPostal in REST/SOAP
  -- Returned by Global Gateway e4 upon successful Authentication.
  merchant_postal varchar(12),
  -- MerchantURL in REST/SOAP
  -- Returned by Global Gateway e4 upon successful Authentication.
  merchant_url varchar(25),
  -- CTR in REST/SOAP
  -- Displays the bank required Customer Transaction Record. This information must be displayed to the customer upon completion of all transactions, Approved or Declined.
  ctr text,
  -- AmexFraud in REST/SOAP
  -- Support (AF) Amex Fraud Mitigation Reply Format Indicators
  -- Indicator Response Fields: 5digit code one digit each in the order of; Zip Code, Street, Name, Telephone, Email
  -- Indicator Codes:
  -- Y = Match
  -- N = No Match
  -- S = Service Unavailable
  -- U = Unchecked
  -- R = Retry
  -- <space> = Data Not Sent
  -- When a merchant is sending in an Amex message, these fields will be returned if they were supplied in the message fields and Address Verification/Validation was requested.
  -- This record will be returned (when supplied by Amex) if a merchant certified/recertified for First Data Compass platform after September 2010 to receive Amex Enhanced Fraud Responses
  -- This property is only applicable to the v16 or higher endpoint.
  amex_fraud varchar(250),
  -- card type in response
  card_type_resp varchar(16),
  hold_txn boolean DEFAULT false NOT NULL,
  -- complete REST/SOAP XML or JSON for request
  request text,
  request_posted_ts timestamp with time zone,
  -- complete REST/SOAP XML or JSON for response
  response text,
  -- free-form notes (not submitted to gateway)
  note text,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_payeezy_cc_txn_tbl IS 'Payeezy credit card payment transaction';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.account_id IS 'Link to customer table on cust_acct_tbl.id';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.gateway_id IS 'ExactID in REST/SOAP, gateway_id in JSON. Used as login to identify which terminal/location/department is processing the transaction.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.transaction_type IS '
Populated with a two-digit string indicator. The indicator identifies the transaction type. Descriptions of these transaction types can be found here. Supported values include:
00 = Purchase
01 = Pre-Authorization
02 = Pre-Authorization Completion
03 = Forced Post
04 = Refund
05 = Pre-Authorization Only
07 = PayPal Order
13 = Void
32 = Tagged Pre-Authorization Completion
33 = Tagged Void
34 = Tagged Refund
83 = CashOut (ValueLink, v9 or higher end point only)
85 = Activation (ValueLink, v9 or higher end point only)
86 = Balance Inquiry (ValueLink, v9 or higher end point only)
88 = Reload (ValueLink, v9 or higher end point only)
89 = Deactivation (ValueLink, v9 or higher end point only)
';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.transaction_tag IS 'A unique identifier to associate with a tagged transaction. Only for tagged transaction types. Maximum Length is 10 Digits.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.authorization_num IS 'This is the authorization number returned by the cardholder’s financial institution when a transaction has been approved. This value needs to be sent when sending various transaction types such as preauthorization completion, void, or tagged transaction.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.cc_expiry IS 'The credit card expiry date in the format mmyy.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.cvd_code IS 'This is the 0, 3, or 4-digit code on the back of the credit card sometimes called the CVV2 or CVD value.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.cvd_presence_ind IS 'The number indicates how the CVV2 value should be handled when processing. The value must be either null or the integer 0, 1, 2, or 9. Note that null defaults to 0.
Null or 0 = Not Supported (Default)
1 = Value provided by Cardholder
2 = Value provided on card is Illegible
9 = Cardholder states data is not available
';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.reference_no IS 'A merchant defined value that can be used to internally identify the transaction. This value is passed through to the Payeezy Gateway unmodified, and may be searched in First Data Payeezy Gateway Real-time Payment Manager (RPM). It is not passed on to the financial institution. The following characters will be stripped from this field: ; ` " / % as well as -- (2 consecutive dashes).';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.tax1_amount IS 'Tax value included in total amount, only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the PST amount.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.tax1_number IS 'Registered number associated with the tax value. Used for reference or government claims purposes and only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the PST number.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.tax2_amount IS 'Tax value included in total amount, only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the GST amount.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.tax2_number IS 'Registered number associated with the tax value. Used for reference or government claims purposes and only applicable to merchants passing level 2 (Corporate Credit Card - Level II) data. For Canadian merchants this field is the PST number.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.customer_ref IS 'A merchant defined value that can be used to internally identify the transaction. This value is passed through to the Payeezy Gateway unmodified, and may be searched in First Data Payeezy Gateway Real-time Payment Manager (RPM). It is passed on to the financial institution. The following characters will be stripped from this field: ; ` " / % as well as -- (2 consecutive dashes).';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.reference_3 IS 'A merchant defined value that can be used to internally identify the transaction. This value is passed through to the Payeezy Gateway unmodified. It is not searchable and is not passed on to the financial institution. The following characters will be stripped from this field: ; ` " / % as well as -- (2 consecutive dashes). Also used to pass on the Foreign Access Code for ValueLink transactions.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.client_ip IS 'This is the IP address of the customer (i.e. client browser) connecting to the merchant. This value is stored for fraud investigation. It is not passed on to the financial institution.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.user_name IS 'This is the user_name of the user processing the transaction. This field is visible in the Real Time Payment manager as the "User ID" and defaults to "API-(ExactID)."';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.partial_redemption IS 'Submit true to allow partial redemptions, false otherwise. A partial redemption will be returned if only a portion of the requested funds are available. For example, if a transaction is submitted for $100, but only $80 is available on the customer''s card, the $80 will be authorized or captured when this property is set to true. This property can be used for all types of pre-authorization and purchase transactions.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.transarmor_token IS 'Used to submit a Transarmor token for transaction processing rather than using a credit card number for processing. If using this property, CardType must also be populated.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.credit_card_type IS 'CardType value associated with a TransArrmor token, PayPal transaction, or if submitting a ValueLink transaction, this property''s value is required to be "Gift." Required for TransArmor transactions, PayPal transactions, and ValueLink transactions that submit a card number. Possible values include "American Express", "Visa", "Mastercard", "Discover", "Diners Club", "JCB", "Gift", "PayPal".';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.phone_number IS 'Non digits will be removed before processing';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.phone_type IS 'Only the following values are accepted: H = Home, W = Work, D = Day, N = Night. PhoneType is required when the PhoneNumber field is populated in a transaction request. Otherwise, it is optional.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.transaction_error IS 'This property indicates that there was an error during the processing of the transaction. Please refer to eCommerce Response Codes (ETG Codes) for further information.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.transaction_approved IS 'This property indicates that the bank approved a transaction and there are no pending errors. If further information is required, please check the Optional Response properties.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.exact_resp_code IS 'This property indicates the processing status of the transaction. Please refer to the section on Exception Handling for further information. The Transaction_Error property will return True if this property is not “00”.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.exact_message IS 'Message that accompanies the Exact_Resp_code.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.bank_resp_code IS 'This is a 2 or 3 digit code, provided by the financial institution, indicating the approval status of a transaction. The meaning of these codes is defined by the various financial institutions and is not under the control of the Global Gateway e4 web service API or Gateway. Please refer to the Transaction_Approved property for the approval status of a transaction.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.bank_message IS 'A message provided by the financial institution describing the Response code above.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.bank_resp_code_2 IS 'A secondary response provided returned by the financial institution.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.transaction_tag_resp IS 'A unique identifier to associate with a tagged transaction. This value overrides any value sent for the Request Property of the same name.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.authorization_num_resp IS 'This is the authorization number returned by the cardholder’s financial institution when a transaction has been approved. This value overrides any value sent for the Request Property of the same name.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.sequence_no IS 'A digit sequentially incremented number generated by Global Gateway e4 and passed through to the financial institution. It is also passed back to the client in the transaction response. This number can be used for tracking and audit purposes.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.avs_resp IS '
X = exact match, 9 digit zip
Y = exact match, 5 digit zip
A = address match only
W = 9 digit zip match only
Z = 5 digit zip match only
N = no address or zip match
U = address unavailable
G = non-North American issuer, does not participate
R = issuer system unavailable
E = not a Mail\Phone order
S = service not supported
Q = Bill to address did not pass edit checks
D = International street address and postal code match
B = International street address match, postal code not verified due to incompatable formats
C = International street address and postal code not verified due to incompatable formats
P = International postal code match, street address not verified due to incompatable format
1 = Cardholder name matches
2 = Cardholder name, billing address, and postal code match
3 = Cardholder name and billing postal code match
4 = Cardholder name and billing address match
5 = Cardholder name incorrect, billing address and postal code match
6 = Cardholder name incorrect, billing postal code matches
7 = Cardholder name incorrect, billing address matches
8 = Cardholder name, billing address, and postal code are all incorrect
';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.cvv2_resp IS 'The CVV2 authentication code returned from the bank. Note: the value is null if CVV2 is not supported.
Supported CVV Results:
M = CVV2 / CVC2/CVD Match.
N = CVV2 / CVC2/CVD No Match.
P = Not Processed.
S = Merchant has indicated that CVV2 / CVC2/CVD is not present on the card.
U = Issuer is not certified and / or has not provided Visa encryption keys.
';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.retrieval_ref_no IS 'The reference number returned with an AVS Result.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.merchant_name IS 'Returned by Global Gateway e4 upon successful Authentication.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.merchant_address IS 'Returned by Global Gateway e4 upon successful Authentication.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.merchant_city IS 'Returned by Global Gateway e4 upon successful Authentication.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.merchant_province IS 'Returned by Global Gateway e4 upon successful Authentication.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.merchant_country IS 'Returned by Global Gateway e4 upon successful Authentication.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.merchant_postal IS 'Returned by Global Gateway e4 upon successful Authentication.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.merchant_url IS 'Returned by Global Gateway e4 upon successful Authentication.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.ctr IS 'Displays the bank required Customer Transaction Record. This information must be displayed to the customer upon completion of all transactions, Approved or Declined.';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.amex_fraud IS '
Support (AF) Amex Fraud Mitigation Reply Format Indicators
Indicator Response Fields: 5digit code one digit each in the order of; Zip Code, Street, Name, Telephone, Email
Indicator Codes:
Y = Match
N = No Match
S = Service Unavailable
U = Unchecked
R = Retry
<space> = Data Not Sent
When a merchant is sending in an Amex message, these fields will be returned if they were supplied in the message fields and Address Verification/Validation was requested.
This record will be returned (when supplied by Amex) if a merchant certified/recertified for First Data Compass platform after September 2010 to receive Amex Enhanced Fraud Responses
This property is only applicable to the v16 or higher endpoint.
';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.card_type_resp IS 'card type returned by the gateway';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.request IS 'complete REST/SOAP XML or JSON for request';
COMMENT ON COLUMN pmt_payeezy_cc_txn_tbl.response IS 'complete REST/SOAP XML or JSON for response';

-- validate column values in pmt_payeezy_cc_txn_tbl
CREATE FUNCTION validate_pmt_payeezy_cc_txn() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  cc_expiry_month integer;
BEGIN
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.gateway_id = strip_collapse_space(NEW.gateway_id);
  IF NEW.transaction_type !~ '^\d\d$' THEN
    RAISE EXCEPTION 'transaction_type must be 2 digits; you entered %', NEW.transaction_type;
  END IF;
  -- if operation is INSERT or cc_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.cc_number IS DISTINCT FROM OLD.cc_number)) THEN
    NEW.cc_number = strip_collapse_space(NEW.cc_number);
    IF NEW.cc_number = '' THEN
      NEW.cc_number = NULL;
    END IF;
    IF NEW.cc_number IS NOT NULL THEN
      IF NEW.cc_number !~ '^\d{13,16}$' THEN
        RAISE EXCEPTION 'cc_number must be 13 to 16 digits; you entered %', NEW.cc_number;
      END IF;
      -- check validity of credit card number using Luhn algorithm
      IF (NOT is_valid_cc_luhn(NEW.cc_number)) THEN
        RAISE EXCEPTION 'cc_number is not a valid credit card number; you entered %', NEW.cc_number;
      END IF;
    END IF;
  END IF;
  NEW.transaction_tag = strip_collapse_space(NEW.transaction_tag);
  NEW.cc_expiry = strip_collapse_space(NEW.cc_expiry);
  IF NEW.cc_expiry = '' THEN
    NEW.cc_expiry = NULL;
  END IF;

  -- if operation is INSERT or cvd_code changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.cvd_code IS DISTINCT FROM OLD.cvd_code)) THEN
    NEW.cvd_code = strip_collapse_space(NEW.cvd_code);
    IF NEW.cvd_code = '' THEN
      NEW.cvd_code = NULL;
    END IF;
    IF (NEW.cvd_code IS NOT NULL) AND (NEW.cvd_code !~ '^\d{3,4}$') THEN
      RAISE EXCEPTION 'cvd_code must be 3 or 4 digits; you entered %', NEW.cvd_code;
    END IF;
  END IF;
  NEW.cvd_presence_ind = strip_collapse_space(NEW.cvd_presence_ind);
  IF NEW.cvd_presence_ind = '' THEN
    NEW.cvd_presence_ind = NULL;
  END IF;
  IF NEW.cc_number IS NULL THEN
    IF NEW.cc_expiry IS NOT NULL THEN
      RAISE EXCEPTION 'cc_expiry should be blank when cc_number is blank';
    END IF;
    IF NEW.cvd_code IS NOT NULL THEN
      RAISE EXCEPTION 'cvd_code should be blank when cc_number is blank';
    END IF;
    IF NEW.cvd_presence_ind IS NOT NULL THEN
      RAISE EXCEPTION 'cvd_presence_ind should be blank when cc_number is blank';
    END IF;
  ELSE
    IF NEW.cc_expiry !~ '^\d{4}$' THEN
      RAISE EXCEPTION 'cc_expiry must be 4 digits in mmyy format; you entered %', NEW.cc_expiry;
    END IF;
    cc_expiry_month = cast(left(NEW.cc_expiry, 2) AS integer);
    IF (cc_expiry_month < 1) OR (cc_expiry_month > 12) THEN
      RAISE EXCEPTION 'cc_expiry must be 4 digits in mmyy format and mm must be 01 through 12; you entered %', NEW.cc_expiry;
    END IF;
    IF (NEW.cvd_presence_ind IS NOT NULL) AND (NEW.cvd_presence_ind !~ '^\d$') THEN
      RAISE EXCEPTION 'cvd_presence_ind must be one digit; you entered %', NEW.cvd_presence_ind;
    END IF;
  END IF;
  NEW.reference_no = strip_collapse_space(NEW.reference_no);
  NEW.zip_code = strip_collapse_space(NEW.zip_code);
  NEW.tax1_number = strip_collapse_space(NEW.tax1_number);
  NEW.tax2_number = strip_collapse_space(NEW.tax2_number);
  NEW.customer_ref = strip_collapse_space(NEW.customer_ref);
  NEW.reference_3 = strip_collapse_space(NEW.reference_3);
  NEW.client_ip = strip_collapse_space(NEW.client_ip);
  NEW.user_name = strip_collapse_space(NEW.user_name);
  NEW.transarmor_token = strip_collapse_space(NEW.transarmor_token);
  NEW.credit_card_type = strip_collapse_space(NEW.credit_card_type);
  NEW.address1 = strip_collapse_space(NEW.address1);
  NEW.address2 = strip_collapse_space(NEW.address2);
  NEW.city = strip_collapse_space(NEW.city);
  NEW.state = strip_collapse_space(NEW.state);
  -- if country is US and state is not NULL, check that state is valid US state
  IF ((NEW.country_code = 'US') AND (NEW.state IS NOT NULL)) THEN
    IF NOT EXISTS (SELECT 1 FROM state_tbl WHERE abbreviation = NEW.state) THEN
      RAISE EXCEPTION 'Please enter a valid 2 letter US state abbreviation in all caps; you entered %', NEW.state;
    END IF;
  END IF;
  NEW.zip = strip_collapse_space(NEW.zip);
  NEW.country_code = strip_collapse_space(NEW.country_code);
  NEW.phone_number = strip_collapse_space(NEW.phone_number);
  IF NEW.phone_number = '' THEN
    NEW.phone_number = NULL;
  END IF;
  IF NEW.phone_number !~ '^\d+$' THEN
    RAISE EXCEPTION 'phone_number must be only digits; you entered %', NEW.phone_number;
  END IF;
  NEW.phone_type = strip_collapse_space(NEW.phone_type);
  IF NEW.phone_type = '' THEN
    NEW.phone_type = NULL;
  END IF;
  IF NEW.phone_number IS NULL THEN
    IF NEW.phone_type IS NOT NULL THEN
      RAISE EXCEPTION 'phone_type must be blank when phone_number is blank';
    END IF;
  ELSE
    IF NEW.phone_type IS NULL THEN
      RAISE EXCEPTION 'must specify phone_type when you specify phone_number';
    END IF;
  END IF;
  NEW.exact_resp_code = strip_collapse_space(NEW.exact_resp_code);
  NEW.exact_message = strip_collapse_space(NEW.exact_message);
  NEW.bank_resp_code = strip_collapse_space(NEW.bank_resp_code);
  NEW.bank_message = strip_collapse_space(NEW.bank_message);
  NEW.bank_resp_code_2 = strip_collapse_space(NEW.bank_resp_code_2);
  NEW.transaction_tag_resp = strip_collapse_space(NEW.transaction_tag_resp);
  NEW.authorization_num_resp = strip_collapse_space(NEW.authorization_num_resp);
  NEW.sequence_no = strip_collapse_space(NEW.sequence_no);
  NEW.avs_resp = strip_collapse_space(NEW.avs_resp);
  NEW.cvv2_resp = strip_collapse_space(NEW.cvv2_resp);
  NEW.retrieval_ref_no = strip_collapse_space(NEW.retrieval_ref_no);
  NEW.merchant_name = strip_collapse_space(NEW.merchant_name);
  NEW.merchant_address = strip_collapse_space(NEW.merchant_address);
  NEW.merchant_city = strip_collapse_space(NEW.merchant_city);
  NEW.merchant_province = strip_collapse_space(NEW.merchant_province);
  NEW.merchant_country = strip_collapse_space(NEW.merchant_country);
  NEW.merchant_postal = strip_collapse_space(NEW.merchant_postal);
  NEW.merchant_url = strip_collapse_space(NEW.merchant_url);
  NEW.ctr = strip_collapse_space(NEW.ctr);
  NEW.amex_fraud = strip_collapse_space(NEW.amex_fraud);
  NEW.card_type_resp = strip_collapse_space(NEW.card_type_resp);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in pmt_payeezy_cc_txn_tbl
CREATE FUNCTION mask_pmt_payeezy_cc_txn() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.cc_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.cc_number IS DISTINCT FROM OLD.cc_number) THEN
      NEW.cc_number = left(NEW.cc_number, 2) || repeat('*', length(NEW.cc_number) - 6) || right(NEW.cc_number, 4);
    END IF;
  END IF;
  IF (NEW.cvd_code IS NOT NULL) THEN
    IF ((TG_OP = 'INSERT') OR (NEW.cvd_code IS DISTINCT FROM OLD.cvd_code)) THEN
      NEW.cvd_code = '****';
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_payeezy_cc_txn_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_payeezy_cc_txn_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_payeezy_cc_txn_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_payeezy_cc_txn();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON pmt_payeezy_cc_txn_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('cc_number', 'cvd_code');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON pmt_payeezy_cc_txn_tbl FOR EACH ROW EXECUTE PROCEDURE mask_pmt_payeezy_cc_txn();

--DROP TABLE pmt_payeezy_cc_txn_tbl;
--DROP SEQUENCE pmt_payeezy_cc_txn_seq;
--DROP FUNCTION validate_pmt_payeezy_cc_txn();
--DROP FUNCTION mask_pmt_payeezy_cc_txn();

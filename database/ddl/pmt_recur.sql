CREATE SEQUENCE pmt_recur_seq;

CREATE TABLE pmt_recur_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  -- Name of table that uses the ID from this row: 'pmt_recur_month_tbl'
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE pmt_recur_id_tbl IS 'Common table to reserve ID values for pmt_recur_x_tbl tables';
COMMENT ON COLUMN pmt_recur_id_tbl.tbl_name IS 'Name of table that uses the ID from this row';

-- recur once a month, full amount due, possibly with cap.
CREATE TABLE pmt_recur_month_tbl (
  id integer DEFAULT nextval('pmt_recur_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to pmt_vault_vw.id. pmt_vault_id_tbl allocates ID values for pmt_vault_ach_tbl and pmt_vault_card_tbl.
  pmt_vault_id integer NOT NULL REFERENCES pmt_vault_id_tbl (id),
  -- max amount to pay. can be NULL if unlimited, up to full amount due.
  max_amount money,
  -- which day of the month the debit/charge should occur on. Can be 1 - 28.
  day_of_month integer NOT NULL,
  -- start date of recurrence. first recurrence will occur on yyyy-mm-dd, where yyyy-mm is the later of current date or start_date,
  -- possibly with mm one month later if dd has passed. in local time of customer.
  start_date date,
  -- end date of recurrence. no recurrence will happen after end date. if dd of end date is same as day_of_month, then that final
  -- recurrence will occur. in local time of customer.
  end_date date,
  -- Zero or more email addresses separated by commas. Send payment transaction result to the email address(es).
  email text,
  -- one email will be sent on the day of the transaction describing the result. If a reminder email is desired one or more days
  -- before the transaction, enter it here. NULL if no prior email desired.
  remind_days_before integer,
  note text,
  internal_note text,
  hold boolean NOT NULL DEFAULT false,
  -- which merchant processor should handle this vault transaction (nmi, payeezy, etc.)
  processor varchar(255) DEFAULT 'nmi'::character varying NOT NULL REFERENCES merchant_processor_tbl (name),
  -- do not delete pmt_recur_month_tbl rows; instead, set is_active to false.
  -- that way rows in pmt_vault_txn_vw that link to pmt_recur_vw do not dangle.
  is_active boolean NOT NULL DEFAULT true,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_recur_month_tbl IS 'Vault transaction table stores link to vault and details about payment transaction';
COMMENT ON COLUMN pmt_recur_month_tbl.pmt_vault_id IS 'Link to pmt_vault_vw.id. pmt_vault_id_tbl allocates ID values for pmt_vault_ach_tbl and pmt_vault_card_tbl.';
COMMENT ON COLUMN pmt_recur_month_tbl.max_amount IS 'max amount to pay. can be NULL if unlimited, up to full amount due.';
COMMENT ON COLUMN pmt_recur_month_tbl.day_of_month IS 'which day of the month the debit/charge should occur on. Can be 1 - 28.';
COMMENT ON COLUMN pmt_recur_month_tbl.start_date IS 'start date of recurrence. first recurrence will occur on yyyy-mm-dd, where yyyy-mm is the later of current date or start_date, possibly with mm one month later if dd has passed. in local time of customer.';
COMMENT ON COLUMN pmt_recur_month_tbl.end_date IS 'end date of recurrence. no recurrence will happen after end date. if dd of end date is same as day_of_month, then that final recurrence will occur. in local time of customer.';
COMMENT ON COLUMN pmt_recur_month_tbl.email IS 'Zero or more email addresses separated by commas. Send payment transaction result to the email address(es).';
COMMENT ON COLUMN pmt_recur_month_tbl.remind_days_before IS 'one email will be sent on the day of the transaction describing the result. If a reminder email is desired one or more days before the transaction, enter it here. NULL if no prior email desired.';
COMMENT ON COLUMN pmt_recur_month_tbl.internal_note IS 'Note that is NOT user-visible, but instead visible only to Micro Office.';
COMMENT ON COLUMN pmt_recur_month_tbl.processor IS 'Which merchant processor should handle this vault payment transaction; valid values are in merchant_processor_tbl.name';
COMMENT ON COLUMN pmt_recur_month_tbl.is_active IS 'Do not delete pmt_recur_month_tbl rows; instead, set is_active to false. That way rows in pmt_vault_txn_vw that link to pmt_recur_vw do not dangle.';

-- validate column values in pmt_recur_month_tbl
CREATE FUNCTION validate_pmt_recur_month() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  IF (NEW.max_amount IS NOT NULL) AND (NEW.max_amount <= 0::money) THEN
    RAISE EXCEPTION 'max_amount must be greater than 0 or NULL; you entered %', NEW.max_amount;
  END IF;
  IF (NEW.day_of_month < 1) OR (NEW.day_of_month > 28) THEN
    RAISE EXCEPTION 'day_of_month must be greater than 0 and less than 29; you entered %', NEW.day_of_month;
  END IF;
  IF (NEW.start_date IS NOT NULL) AND (NEW.end_date IS NOT NULL) AND (NEW.start_date > NEW.end_date) THEN
    RAISE EXCEPTION 'start_date cannot be after end_date; you entered start_date % and end_date %', start_date, end_date;
  END IF;
  NEW.email = strip_collapse_space(NEW.email);
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  IF (NEW.remind_days_before IS NOT NULL) AND (NEW.remind_days_before < 1) OR (NEW.remind_days_before > 15) THEN
    RAISE EXCEPTION 'remind_days_before must be NULL or greater than 0 and less than 16; you entered %', NEW.remind_days_before;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON pmt_recur_month_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('pmt_recur_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON pmt_recur_month_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('pmt_recur_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON pmt_recur_month_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('pmt_recur_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_recur_month_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_recur_month_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_recur_month_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_recur_month();

-- set up audit log for pmt_recur_month_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_recur_month_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_recur_month_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_recur_month_tbl');

CREATE VIEW pmt_recur_vw AS
 SELECT pmt_recur_month_tbl.id,
    'month'::character varying(6) AS frequency,
    pmt_recur_month_tbl.pmt_vault_id,
    pmt_vault_vw.pmt_method,
    pmt_vault_vw.floor_label,
    pmt_vault_vw.cust_acct_id,
    pmt_vault_vw.name,
    pmt_recur_month_tbl.max_amount,
    pmt_recur_month_tbl.day_of_month,
    pmt_recur_month_tbl.start_date,
    pmt_recur_month_tbl.end_date,
    pmt_recur_month_tbl.email,
    pmt_recur_month_tbl.remind_days_before,
    pmt_recur_month_tbl.note,
    pmt_recur_month_tbl.internal_note,
    pmt_recur_month_tbl.hold,
    pmt_recur_month_tbl.processor,
    pmt_recur_month_tbl.is_active,
    building_tbl.nmi_username AS processor_username,
    'pmt_nmi_txn_' || pmt_vault_vw.pmt_method || '_tbl'::character varying AS txn_tbl_name,
    pmt_vault_vw.account_type,
    pmt_vault_vw.owned_by,
    pmt_vault_vw.registered_name,
    pmt_vault_vw.routing_number,
    pmt_vault_vw.account_number,
    pmt_vault_vw.card_number,
    pmt_vault_vw.expire_date,
    pmt_vault_vw.cvv,
    pmt_vault_vw.billing_first_name,
    pmt_vault_vw.billing_last_name,
    pmt_vault_vw.billing_company,
    pmt_vault_vw.billing_address_1,
    pmt_vault_vw.billing_address_2,
    pmt_vault_vw.billing_address_num,
    pmt_vault_vw.billing_city,
    pmt_vault_vw.billing_state,
    pmt_vault_vw.billing_zip_code,
    pmt_vault_vw.billing_country,
    building_tbl.timezone AS building_timezone,
    pmt_recur_month_tbl.start_date::timestamp at time zone building_tbl.timezone AS start_date_utc,
    pmt_recur_month_tbl.end_date::timestamp at time zone building_tbl.timezone AS end_date_utc,
    pmt_vault_vw.crypt_json,
    pmt_recur_month_tbl.created_ts,
    pmt_recur_month_tbl.created_user,
    pmt_recur_month_tbl.modified_ts,
    pmt_recur_month_tbl.modified_user
   FROM ((((pmt_recur_month_tbl
     JOIN pmt_vault_vw ON (pmt_recur_month_tbl.pmt_vault_id = pmt_vault_vw.id))
     JOIN cust_acct_tbl ON ((pmt_vault_vw.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id)))
     JOIN building_tbl ON (floor_tbl.building_id = building_tbl.id));

--DROP VIEW pmt_recur_vw;
--DROP TABLE pmt_recur_month_tbl;
--DROP FUNCTION validate_pmt_recur_month();
--DROP TABLE pmt_recur_id_tbl;
--DROP SEQUENCE pmt_recur_seq;

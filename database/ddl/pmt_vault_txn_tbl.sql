CREATE SEQUENCE pmt_vault_txn_seq;

CREATE TABLE merchant_processor_tbl (
  name varchar(255) NOT NULL PRIMARY KEY,
  note text
);

COMMENT ON TABLE merchant_processor_tbl IS 'Merchant processors we use to process credit/debit/ACH transactions';

CREATE TABLE pmt_vault_txn_tbl (
  id integer DEFAULT nextval('pmt_vault_txn_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to pmt_vault_vw.id. pmt_vault_id_tbl allocates ID values for pmt_vault_ach_tbl and pmt_vault_card_tbl.
  pmt_vault_id integer NOT NULL REFERENCES pmt_vault_id_tbl (id),
  amount money NOT NULL,
  -- convenience fee portion of amount. Can be NULL for ACH payments. Should be non-NULL number (>= 0) for card payments.
  convenience_fee money,
  -- scheduled_date: date to debit/charge the customer, in local time of the customer
  scheduled_date date NOT NULL,
  -- which merchant processor should handle this vault transaction (nmi, payeezy, etc.)
  processor varchar(255) DEFAULT 'nmi'::character varying NOT NULL REFERENCES merchant_processor_tbl (name),
  note text,
  hold boolean NOT NULL DEFAULT false,
  -- Zero or more email addresses separated by commas. Send payment transaction result to the email address(es).
  email text,
  -- if this transaction came from a recurrence rule, link to the recurrence rule in pmt_recur_vw.
  pmt_recur_id integer REFERENCES pmt_recur_id_tbl (id),
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_vault_txn_tbl IS 'Vault transaction table stores link to vault and details about payment transaction';
COMMENT ON COLUMN pmt_vault_txn_tbl.pmt_vault_id IS 'Link to pmt_vault_vw.id. pmt_vault_id_tbl allocates ID values for pmt_vault_ach_tbl and pmt_vault_card_tbl.';
COMMENT ON COLUMN pmt_vault_txn_tbl.convenience_fee IS 'Convenience fee portion of amount (not added on top of amount). Can be NULL for ACH payments. Should be non-NULL number (>= 0) for card payments.';
COMMENT ON COLUMN pmt_vault_txn_tbl.scheduled_date IS 'date to process the payment transaction, in local time of the customer';
COMMENT ON COLUMN pmt_vault_txn_tbl.processor IS 'Which merchant processor should handle this vault payment transaction; valid values are in merchant_processor_tbl.name';
COMMENT ON COLUMN pmt_vault_txn_tbl.email IS 'Zero or more email addresses separated by commas. Send payment transaction result to the email address(es).';
COMMENT ON COLUMN pmt_vault_txn_tbl.pmt_recur_id IS 'If this transaction came from a recurrence rule, link to the recurrence rule in pmt_recur_vw.';

-- validate column values in pmt_vault_txn_tbl
CREATE FUNCTION validate_pmt_vault_txn() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
  pmt_method varchar;
BEGIN
  -- disallow update of posted transaction if user is not a member of update_posted_txn role.
  IF NOT pg_has_role('update_posted_txn', 'member') AND (TG_OP = 'UPDATE') THEN
    IF EXISTS (SELECT 1 FROM pmt_nmi_txn_vw WHERE pmt_vault_txn_id = OLD.id AND request_posted_ts IS NOT NULL) THEN
      RAISE EXCEPTION 'row id % has already posted; update not allowed', OLD.id;
    END IF;
  END IF;
  IF NOT EXISTS (SELECT 1 FROM pmt_vault_vw WHERE id = NEW.pmt_vault_id) THEN
    RAISE EXCEPTION 'row with id not found in pmt_vault_vw; you entered a pmt_vault_id of %', NEW.pmt_vault_id;
  END IF;
  SELECT pmt_vault_vw.pmt_method INTO pmt_method FROM pmt_vault_vw WHERE id = NEW.pmt_vault_id;
  IF NEW.processor = 'nmi' THEN
    IF pmt_method NOT IN ('ach', 'card') THEN
      RAISE EXCEPTION 'processor nmi does not support pmt_method %', pmt_method;
    END IF;
  ELSIF NEW.processor = 'payeezy' THEN
    IF pmt_method NOT IN ('card') THEN
      RAISE EXCEPTION 'processor payeezy does not support pmt_method %', pmt_method;
    END IF;
  END IF;
  IF pmt_method = 'ach' THEN
    IF NEW.convenience_fee IS NOT NULL THEN
      IF NEW.convenience_fee = 0::money THEN
        NEW.convenience_fee = NULL;
      ELSE
        RAISE EXCEPTION 'when pmt_method = "ach", convenience_fee must be NULL';
      END IF;
    END IF;
  ELSIF pmt_method = 'card' THEN
    IF NEW.convenience_fee IS NULL THEN
      NEw.convenience_fee = 0::money;
    END IF;
  END IF;
  NEW.email = strip_collapse_space(NEW.email);
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

-- restrict delete of posted transactions to members of delete_posted_txn
CREATE FUNCTION delete_pmt_vault_txn() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF NOT pg_has_role('delete_posted_txn', 'member') THEN
    IF EXISTS (SELECT 1 FROM pmt_nmi_txn_vw WHERE pmt_vault_txn_id = OLD.id AND request_posted_ts IS NOT NULL) THEN
      RAISE EXCEPTION 'row id % has already posted; delete not allowed', OLD.id;
    END IF;
  END IF;
  -- either transaction not posted or user is member of delete_posted_txn, so proceed with delete
  RETURN OLD;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_vault_txn_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_vault_txn_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_vault_txn_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_vault_txn();
CREATE TRIGGER delete_row BEFORE DELETE ON pmt_vault_txn_tbl FOR EACH ROW EXECUTE PROCEDURE delete_pmt_vault_txn();

-- set up audit log for pmt_vault_txn_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_vault_txn_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_vault_txn_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_vault_txn_tbl');

CREATE VIEW pmt_vault_txn_vw AS
 SELECT pmt_vault_txn_tbl.id,
    pmt_vault_txn_tbl.pmt_vault_id,
    pmt_vault_vw.pmt_method,
    pmt_vault_vw.floor_label,
    pmt_vault_vw.cust_acct_id,
    pmt_vault_vw.name,
    pmt_vault_txn_tbl.amount,
    (CASE WHEN pmt_vault_txn_tbl.convenience_fee IS NULL THEN pmt_vault_txn_tbl.amount ELSE pmt_vault_txn_tbl.amount - pmt_vault_txn_tbl.convenience_fee END) AS prefee_amount,
    pmt_vault_txn_tbl.convenience_fee,
    to_char((CASE WHEN pmt_vault_txn_tbl.convenience_fee IS NULL THEN NULL ELSE CASE WHEN (pmt_vault_txn_tbl.amount - pmt_vault_txn_tbl.convenience_fee = 0::money) THEN 0 ELSE pmt_vault_txn_tbl.convenience_fee/(pmt_vault_txn_tbl.amount - pmt_vault_txn_tbl.convenience_fee)*100 END END)::numeric(4,2), '90D99') || '%' AS convenience_pct,
    pmt_vault_txn_tbl.scheduled_date,
    pmt_vault_txn_tbl.scheduled_date::timestamp at time zone building_tbl.timezone AS scheduled_ts_utc,
    building_tbl.timezone AS building_timezone,
    pmt_vault_txn_tbl.note,
    pmt_vault_txn_tbl.hold,
    pmt_vault_txn_tbl.pmt_recur_id,
    pmt_vault_txn_tbl.processor,
    building_tbl.nmi_username AS processor_username,
    'pmt_nmi_txn_' || pmt_vault_vw.pmt_method || '_tbl'::character varying AS txn_tbl_name,
    pmt_nmi_txn_vw.id AS pmt_txn_fk_id,
    pmt_vault_vw.account_type,
    pmt_vault_vw.owned_by,
    pmt_vault_vw.registered_name,
    pmt_vault_vw.routing_number,
    pmt_vault_vw.account_number,
    pmt_vault_vw.card_number,
    pmt_vault_vw.expire_date,
    pmt_vault_vw.cvv,
    pmt_vault_vw.billing_first_name,
    pmt_vault_vw.billing_last_name,
    pmt_vault_vw.billing_company,
    pmt_vault_vw.billing_address_1,
    pmt_vault_vw.billing_address_2,
    pmt_vault_vw.billing_address_num,
    pmt_vault_vw.billing_city,
    pmt_vault_vw.billing_state,
    pmt_vault_vw.billing_zip_code,
    pmt_vault_vw.billing_country,
    pmt_vault_vw.crypt_json,
    pmt_vault_txn_tbl.email,
    pmt_nmi_txn_vw.response,
    pmt_nmi_txn_vw.responsetext,
    pmt_nmi_txn_vw.authcode,
    pmt_nmi_txn_vw.transactionid,
    pmt_nmi_txn_vw.avsresponse,
    pmt_nmi_txn_vw.cvvresponse,
    pmt_nmi_txn_vw.response_code,
    pmt_nmi_txn_vw.action_list,
    pmt_nmi_txn_vw.request_body,
    pmt_nmi_txn_vw.request_posted_ts,
    pmt_nmi_txn_vw.response_body,
    pmt_vault_txn_tbl.created_ts,
    pmt_vault_txn_tbl.created_user,
    pmt_vault_txn_tbl.modified_ts,
    pmt_vault_txn_tbl.modified_user
   FROM (((((pmt_vault_txn_tbl
     JOIN pmt_vault_vw ON (pmt_vault_txn_tbl.pmt_vault_id = pmt_vault_vw.id))
     JOIN cust_acct_tbl ON ((pmt_vault_vw.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id)))
     JOIN building_tbl ON (floor_tbl.building_id = building_tbl.id))
     LEFT JOIN pmt_nmi_txn_vw ON ((pmt_vault_txn_tbl.id = pmt_nmi_txn_vw.pmt_vault_txn_id)))
  WHERE ((pmt_vault_txn_tbl.processor)::text = 'nmi'::text);

--DROP VIEW pmt_vault_txn_vw;
--DROP TABLE pmt_vault_txn_tbl;
--DROP FUNCTION validate_pmt_vault_txn();
--DROP TABLE merchant_processor_tbl;
--DROP SEQUENCE pmt_vault_txn_seq;

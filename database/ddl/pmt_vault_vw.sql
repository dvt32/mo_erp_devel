CREATE SEQUENCE pmt_vault_seq;

CREATE TABLE pmt_vault_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  -- Name of table that uses the ID from this row: 'pmt_vault_ach_tbl' or 'pmt_vault_card_tbl'
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE pmt_vault_id_tbl IS 'Common table to reserve ID values for pmt_vault_x_tbl tables';
COMMENT ON COLUMN pmt_vault_id_tbl.tbl_name IS 'Name of table that uses the ID from this row';

CREATE TABLE pmt_vault_ach_tbl (
  id integer DEFAULT nextval('pmt_vault_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to cust_acct_tbl.id
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  -- account_type: checking or savings
  account_type character varying(8) NOT NULL REFERENCES enum_bank_account_type_tbl (account_type),
  -- owned_by: business or personal
  owned_by character varying(8) NOT NULL REFERENCES enum_bank_account_owned_by_tbl (owned_by),
  -- name of the person or company that owns the money in the bank account
  registered_name character varying(64),
  routing_number character(9) NOT NULL,
  account_number character varying(17) NOT NULL,
  -- two sub-$1 deposits are made to the account (and then debited later) to verify control
  confirmed_ts timestamp with time zone,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_vault_ach_tbl IS 'Vault for encrypted customer bank accounts';
COMMENT ON COLUMN pmt_vault_ach_tbl.cust_acct_id IS 'Link to cust_acct_tbl.id';
COMMENT ON COLUMN pmt_vault_ach_tbl.account_type IS 'checking or savings';
COMMENT ON COLUMN pmt_vault_ach_tbl.owned_by IS 'business or personal';
COMMENT ON COLUMN pmt_vault_ach_tbl.registered_name IS 'Name of the person or entity that has beneficial ownership of the bank account';
COMMENT ON COLUMN pmt_vault_ach_tbl.routing_number IS 'ACH bank routing number';
COMMENT ON COLUMN pmt_vault_ach_tbl.account_number IS 'Bank account number';
COMMENT ON COLUMN pmt_vault_ach_tbl.confirmed_ts IS 'Timestamp of when the bank account was confirmed via entry of correct two deposits. NULL if not yet confirmed.';

-- validate column values in pmt_vault_ach_tbl
CREATE FUNCTION validate_pmt_vault_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.registered_name = strip_collapse_space(NEW.registered_name);
  NEW.routing_number = strip_space(NEW.routing_number);
  -- if operation is INSERT or routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.routing_number IS DISTINCT FROM OLD.routing_number)) THEN
    IF NEW.routing_number IS NULL THEN
      RAISE EXCEPTION 'routing_number is required';
    END IF;
    IF NEW.routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'routing_number must be 9 digits; you entered %', NEW.routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.routing_number) THEN
      RAISE EXCEPTION 'routing_number not found in FedACH Participants; you entered %', NEW.routing_number;
    END IF;
  END IF;
  NEW.account_number = strip_space(NEW.account_number);
  -- if operation is INSERT or account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number)) THEN
    IF NEW.account_number IS NULL THEN
      RAISE EXCEPTION 'account_number is required';
    END IF;
    IF NEW.account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'account_number must be numeric; you entered %', NEW.account_number;
    END IF;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in pmt_vault_ach_tbl
CREATE FUNCTION mask_pmt_vault_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number) THEN
      IF length(NEW.account_number) > 4 THEN
        NEW.account_number = left(NEW.account_number, 1) || repeat('*', length(NEW.account_number) - 4) || right(NEW.account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('pmt_vault_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('pmt_vault_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('pmt_vault_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_vault_ach();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('routing_number', 'account_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON pmt_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE mask_pmt_vault_ach();

-- set up audit log for pmt_vault_ach_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_vault_ach_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_vault_ach_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_vault_ach_tbl');

CREATE TABLE pmt_vault_card_tbl (
  id integer DEFAULT nextval('pmt_vault_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to cust_acct_tbl.id
  cust_acct_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  card_number varchar(16) NOT NULL,
  -- expire date should be YYYY-MM-DD. If day is unknown, use first day of the month (1).
  expire_date date NOT NULL,
  -- CVV/CVC: 3 digits for Visa/MC, 4 for AMEX
  cvv varchar(4),
  billing_first_name varchar(64) NOT NULL,
  billing_last_name varchar(64) NOT NULL,
  billing_company varchar(64),
  billing_address_1 varchar(128) NOT NULL,
  billing_address_2 varchar(128),
  -- billing_address_num is the numeric portion of the street address. Computers have a hard time parsing it, so enter it here.
  billing_address_num varchar(16),
  billing_city varchar(48) NOT NULL,
  -- if billing_country is US, billing_state must be valid two character abbreviation in capital letters
  billing_state varchar(64),
  billing_zip_code varchar(20) NOT NULL,
  -- if country is left blank, it will default to US. This is the 2 character ISO 3166-1 alpha-2 country code.
  billing_country character(2) DEFAULT 'US' NOT NULL REFERENCES country_tbl (iso_2),
  note text,
  is_active boolean DEFAULT true NOT NULL,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_vault_card_tbl IS 'Vault for encrypted customer bank accounts';
COMMENT ON COLUMN pmt_vault_card_tbl.cust_acct_id IS 'Link to cust_acct_tbl.id';
COMMENT ON COLUMN pmt_vault_card_tbl.expire_date IS 'Expiration date of card. Use 1 as day if no day on card.';
COMMENT ON COLUMN pmt_vault_card_tbl.cvv IS 'Card verification value. 3 digits for Visa/MC/Discover, 4 digits for AMEX.';
COMMENT ON COLUMN pmt_vault_card_tbl.billing_address_num IS 'Address number portion of address_1. This is because it is sometimes hard for the computer to parse the address number.';
COMMENT ON COLUMN pmt_vault_card_tbl.billing_country IS 'ISO 3166-1 alpha-2 two character country code. Defaults to US.';

-- validate column values in pmt_vault_card_tbl
CREATE FUNCTION validate_pmt_vault_card() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.card_number = strip_space(NEW.card_number);
  -- if operation is INSERT or card_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.card_number IS DISTINCT FROM OLD.card_number)) THEN
    IF NEW.card_number IS NULL THEN
      RAISE EXCEPTION 'card_number is required';
    END IF;
    IF NEW.card_number !~ '^\d{13,16}$' THEN
      RAISE EXCEPTION 'card_number must be 13 to 16 digits; you entered %', NEW.card_number;
    END IF;
    -- check validity of credit card number using Luhn algorithm
    IF (NOT is_valid_cc_luhn(NEW.card_number)) THEN
      RAISE EXCEPTION 'card_number is not a valid credit card number; you entered %', NEW.card_number;
    END IF;
  END IF;
  NEW.cvv = strip_space(NEW.cvv);
  -- if cvv IS NOT NULL, check next step to see if we should validate
  IF (NEW.cvv IS NOT NULL) THEN
    -- if operation is INSERT or cvv changed during UPDATE, validate
    -- it could be masked from last update, in which case it would be same, so do not validate if so
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      IF NEW.cvv !~ '^\d{3,4}$' THEN
        RAISE EXCEPTION 'cvv must be 3 or 4 digits; you entered %', NEW.cvv;
      END IF;
    END IF;
  END IF;
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.billing_first_name = strip_collapse_space(NEW.billing_first_name);
  NEW.billing_last_name = strip_collapse_space(NEW.billing_last_name);
  NEW.billing_company = strip_collapse_space(NEW.billing_company);
  NEW.billing_address_1 = strip_collapse_space(NEW.billing_address_1);
  NEW.billing_address_2 = strip_collapse_space(NEW.billing_address_2);
  NEW.billing_address_num = strip_collapse_space(NEW.billing_address_num);
  NEW.billing_city = strip_collapse_space(NEW.billing_city);
  NEW.billing_state = strip_collapse_space(NEW.billing_state);
  -- if country is US and state is not NULL, check that state is valid US state
  IF ((NEW.billing_country = 'US') AND (NEW.billing_state IS NOT NULL)) THEN
    IF NOT EXISTS (SELECT 1 FROM state_tbl WHERE abbreviation = NEW.billing_state) THEN
      RAISE EXCEPTION 'Please enter a valid 2 letter US state abbreviation in all caps; you entered %', NEW.billing_state;
    END IF;
  END IF;
  NEW.billing_zip_code = strip_collapse_space(NEW.billing_zip_code);
  NEW.billing_country = strip_collapse_space(NEW.billing_country);
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in pmt_vault_card_tbl
CREATE FUNCTION mask_pmt_vault_card() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.card_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.card_number IS DISTINCT FROM OLD.card_number) THEN
      NEW.card_number = left(NEW.card_number, 6) || repeat('*', length(NEW.card_number) - 10) || right(NEW.card_number, 4);
    END IF;
  END IF;
  IF (NEW.cvv IS NOT NULL) THEN
    IF ((TG_OP = 'INSERT') OR (NEW.cvv IS DISTINCT FROM OLD.cvv)) THEN
      NEW.cvv = '****';
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('pmt_vault_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('pmt_vault_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('pmt_vault_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_vault_card();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('card_number', 'cvv');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON pmt_vault_card_tbl FOR EACH ROW EXECUTE PROCEDURE mask_pmt_vault_card();

-- set up audit log for pmt_vault_card_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_vault_card_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_vault_card_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_vault_card_tbl');

CREATE TABLE pmt_vault_pref_tbl (
  pmt_vault_id integer NOT NULL PRIMARY KEY REFERENCES pmt_vault_id_tbl (id),
  pref_rank integer,
  amount_limit money,
  email_address text,
  phone_number character varying(255),
  sms_number character varying(255),
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_vault_pref_tbl IS 'Preference details for vault entries in pmt_vault_vw';
COMMENT ON COLUMN pmt_vault_pref_tbl.pmt_vault_id IS 'Link to pmt_vault_vw.id';
COMMENT ON COLUMN pmt_vault_pref_tbl.pref_rank IS 'Preference rank of payment method. Starts with 1 and goes up. Can be NULL if no preference ranking. No two pmt_vault_vw rows with same cust_acct_id can have same preference rank (except that NULL is allowed multiple times).';
COMMENT ON COLUMN pmt_vault_pref_tbl.amount_limit IS 'Limit of charge/debit per billing cycle';
COMMENT ON COLUMN pmt_vault_pref_tbl.email_address IS 'Zero or more email addresses separated by commas. Send notifications of charge/debit on the linked payment method to the email address(es).';
COMMENT ON COLUMN pmt_vault_pref_tbl.phone_number IS 'Zero or more voice phone numbers separated by commas. Call the telephone numbers and read (via speech synthesis) the notifications of charge/debit on the linked payment method.';
COMMENT ON COLUMN pmt_vault_pref_tbl.sms_number IS 'Zero or more SMS/text phone numbers separated by commas. Send SMS/text message to the phone numbers notifying of charge/debit on the linked payment method.';

-- validate column values in pmt_vault_pref_tbl
CREATE FUNCTION validate_pmt_vault_pref() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
  _cust_acct_id integer;
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pmt_vault_vw WHERE id = NEW.pmt_vault_id) THEN
    RAISE EXCEPTION 'row with id not found in pmt_vault_vw; you entered a pmt_vault_id of %', NEW.pmt_vault_id;
  END IF;
  IF ((TG_OP = 'INSERT') OR (NEW.pref_rank IS DISTINCT FROM OLD.pref_rank)) THEN
    IF (NEW.pref_rank IS NOT NULL) THEN
      IF NEW.pref_rank < 1 THEN
        RAISE EXCEPTION 'pref_rank must be 1 or higher, or can be NULL; you entered %', NEW.pref_rank;
      END IF;
      -- check that no two pmt_vault_pref_tbl rows that link to a single customer have same pref_rank
      SELECT cust_acct_id INTO _cust_acct_id FROM pmt_vault_vw WHERE id = NEW.pmt_vault_id;
      IF EXISTS (SELECT 1 FROM pmt_vault_pref_tbl JOIN pmt_vault_vw ON pmt_vault_pref_tbl.pmt_vault_id = pmt_vault_vw.id WHERE pmt_vault_vw.cust_acct_id = _cust_acct_id AND pmt_vault_vw.id != NEW.pmt_vault_id AND pmt_vault_pref_tbl.pref_rank = NEW.pref_rank) THEN
        RAISE EXCEPTION 'no two pmt_vault_pref_tbl rows can have the same pref_rank for a single customer (cust_acct_id); look in pmt_vault_vw for other rows with cust_acct_id = % and see which one(s) have the same pref_rank, then fix your entry', _cust_acct_id;
      END IF;
    END IF;
  END IF;
  IF NEW.amount_limit IS NOT NULL THEN
    IF NEW.amount_limit < 1 THEN
      RAISE EXCEPTION 'amount_limit must be at least $1, or can be NULL; you entered %', NEW.amount_limit;
    END IF;
  END IF;
  NEW.email_address = strip_collapse_space(NEW.email_address);
  IF (NEW.email_address IS NOT NULL) AND (NEW.email_address !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email_address must be zero or more valid email addresses separated by commas; you entered %', NEW.email_address;
  END IF;
  NEW.phone_number = strip_collapse_space(NEW.phone_number);
  NEW.sms_number = strip_collapse_space(NEW.sms_number);
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_vault_pref_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_vault_pref_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_vault_pref_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_vault_pref();

-- set up audit log for pmt_vault_pref_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_vault_pref_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_vault_pref_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_vault_pref_tbl');

CREATE SEQUENCE pmt_method_confirm_seq;

CREATE TABLE pmt_method_confirm_tbl (
  id integer DEFAULT nextval('pmt_method_confirm_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to pmt_vault_vw.id
  pmt_vault_id integer NOT NULL REFERENCES pmt_vault_ach_tbl (id),
  num_confirm_attempts integer DEFAULT 0 NOT NULL,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE pmt_method_confirm_tbl IS 'Store data to allow for confirmation of bank accounts and other payment methods.';
COMMENT ON COLUMN pmt_method_confirm_tbl.pmt_vault_id IS 'Link to pmt_vault_vw.id.';
COMMENT ON COLUMN pmt_method_confirm_tbl.num_confirm_attempts IS 'Number of times the user has previously attempted to confirm the amounts credited.';

-- validate column values in pmt_method_confirm_tbl
CREATE FUNCTION validate_pmt_method_confirm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.num_confirm_attempts < 0) THEN
    RAISE EXCEPTION 'num_confirm_attempts must be 0 or greater; you entered %', NEW.num_confirm_attempts;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON pmt_method_confirm_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON pmt_method_confirm_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON pmt_method_confirm_tbl FOR EACH ROW EXECUTE PROCEDURE validate_pmt_method_confirm();

-- set up audit log for pmt_method_confirm_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON pmt_method_confirm_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON pmt_method_confirm_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('pmt_method_confirm_tbl');

CREATE VIEW pmt_vault_vw AS
 SELECT pmt_vault_ach_tbl.id,
    'ach'::character varying(4) AS pmt_method,
    floor_tbl.short_global_label AS floor_label,
    pmt_vault_ach_tbl.cust_acct_id,
    cust_acct_tbl.name,
    pmt_vault_pref_tbl.pref_rank,
    pmt_vault_pref_tbl.amount_limit,
    pmt_vault_ach_tbl.account_type,
    pmt_vault_ach_tbl.owned_by,
    pmt_vault_ach_tbl.registered_name,
    pmt_vault_ach_tbl.routing_number,
    pmt_vault_ach_tbl.account_number,
    pmt_vault_ach_tbl.confirmed_ts,
    NULL AS card_number,
    NULL AS expire_date,
    NULL AS cvv,
    NULL AS billing_first_name,
    NULL AS billing_last_name,
    NULL AS billing_company,
    NULL AS billing_address_1,
    NULL AS billing_address_2,
    NULL AS billing_address_num,
    NULL AS billing_city,
    NULL AS billing_state,
    NULL AS billing_zip_code,
    NULL AS billing_country,
    pmt_vault_pref_tbl.email_address,
    pmt_vault_pref_tbl.phone_number,
    pmt_vault_pref_tbl.sms_number,
    pmt_vault_ach_tbl.note,
    pmt_vault_ach_tbl.is_active,
    pmt_vault_ach_tbl.crypt_json,
    pmt_vault_ach_tbl.created_ts,
    pmt_vault_ach_tbl.created_user,
    pmt_vault_ach_tbl.modified_ts,
    pmt_vault_ach_tbl.modified_user
   FROM ((pmt_vault_ach_tbl
     JOIN cust_acct_tbl ON ((pmt_vault_ach_tbl.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id))
     LEFT JOIN pmt_vault_pref_tbl ON (pmt_vault_ach_tbl.id = pmt_vault_pref_tbl.pmt_vault_id))
UNION ALL
 SELECT pmt_vault_card_tbl.id,
    'card'::character varying(4) AS pmt_method,
    floor_tbl.short_global_label AS floor_label,
    pmt_vault_card_tbl.cust_acct_id,
    cust_acct_tbl.name,
    pmt_vault_pref_tbl.pref_rank,
    pmt_vault_pref_tbl.amount_limit,
    NULL AS account_type,
    NULL AS owned_by,
    NULL AS registered_name,
    NULL AS routing_number,
    NULL AS account_number,
    NULL AS confirmed_ts,
    pmt_vault_card_tbl.card_number,
    pmt_vault_card_tbl.expire_date,
    pmt_vault_card_tbl.cvv,
    pmt_vault_card_tbl.billing_first_name,
    pmt_vault_card_tbl.billing_last_name,
    pmt_vault_card_tbl.billing_company,
    pmt_vault_card_tbl.billing_address_1,
    pmt_vault_card_tbl.billing_address_2,
    pmt_vault_card_tbl.billing_address_num,
    pmt_vault_card_tbl.billing_city,
    pmt_vault_card_tbl.billing_state,
    pmt_vault_card_tbl.billing_zip_code,
    pmt_vault_card_tbl.billing_country,
    pmt_vault_pref_tbl.email_address,
    pmt_vault_pref_tbl.phone_number,
    pmt_vault_pref_tbl.sms_number,
    pmt_vault_card_tbl.note,
    pmt_vault_card_tbl.is_active,
    pmt_vault_card_tbl.crypt_json,
    pmt_vault_card_tbl.created_ts,
    pmt_vault_card_tbl.created_user,
    pmt_vault_card_tbl.modified_ts,
    pmt_vault_card_tbl.modified_user
   FROM ((pmt_vault_card_tbl
     JOIN cust_acct_tbl ON ((pmt_vault_card_tbl.cust_acct_id = cust_acct_tbl.id)))
     JOIN floor_tbl ON ((cust_acct_tbl.primary_floor_id = floor_tbl.id))
     LEFT JOIN pmt_vault_pref_tbl ON (pmt_vault_card_tbl.id = pmt_vault_pref_tbl.pmt_vault_id));

--DROP VIEW pmt_vault_vw;
--DROP TABLE pmt_method_confirm_tbl;
--DROP SEQUENCE pmt_method_confirm_seq;
--DROP FUNCTION validate_pmt_method_confirm();
--DROP TABLE pmt_vault_pref_tbl;
--DROP FUNCTION validate_pmt_vault_pref();
--DROP TABLE pmt_vault_ach_tbl;
--DROP FUNCTION validate_pmt_vault_ach();
--DROP FUNCTION mask_pmt_vault_ach();
--DROP TABLE pmt_vault_card_tbl;
--DROP FUNCTION validate_pmt_vault_card();
--DROP FUNCTION mask_pmt_vault_card();
--DROP TABLE pmt_vault_id_tbl;
--DROP SEQUENCE pmt_vault_seq;

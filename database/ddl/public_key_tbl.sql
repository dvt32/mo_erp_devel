CREATE TYPE public_key_type AS ENUM ('rsa', 'dsa');
CREATE SEQUENCE public_key_seq;
CREATE TABLE public_key_tbl (
  id integer DEFAULT nextval('public_key_seq'::regclass) NOT NULL PRIMARY KEY,
  public_key bytea NOT NULL,
  key_type public_key_type NOT NULL,
  key_size integer NOT NULL,
  fingerprint text NOT NULL,
  note text,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);
COMMENT ON TABLE public_key_tbl IS 'Public keys';
COMMENT ON COLUMN public_key_tbl.public_key IS 'Public key';
COMMENT ON COLUMN public_key_tbl.key_type IS 'Type of key: rsa or dsa';
COMMENT ON COLUMN public_key_tbl.key_size IS 'Size of public key in bits';
COMMENT ON COLUMN public_key_tbl.fingerprint IS 'Fingerprint of key as reported by --fingerprint in GnuPG';

--DROP TABLE public_key_tbl;
--DROP SEQUENCE public_key_seq;
--DROP TYPE public_key_type;

CREATE SEQUENCE recording_email_seq;

CREATE TABLE recording_email_tbl (
  id integer DEFAULT nextval('recording_email_seq'::regclass) NOT NULL PRIMARY KEY,
  account_id integer NOT NULL REFERENCES cust_acct_tbl (id),
  recording_dir character varying(32) NOT NULL,
  email text NOT NULL,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE recording_email_tbl IS 'Table of customers who want recordings emailed';

ALTER TABLE ONLY recording_email_tbl
    ADD CONSTRAINT recording_email_tbl_recording_dir_key UNIQUE (recording_dir);


-- Validate emails are comma separated and stripped/lower cased
CREATE FUNCTION validate_recording_email() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  emails_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  NEW.email = strip_space(lower(NEW.email));
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* emails_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON recording_email_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON recording_email_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_recording_email BEFORE INSERT OR UPDATE ON recording_email_tbl FOR EACH ROW EXECUTE PROCEDURE validate_recording_email();

-- set up audit log for recording_email_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON recording_email_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON recording_email_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('recording_email_tbl');

CREATE VIEW recording_email_vw AS
 SELECT recording_email_tbl.id,
    recording_email_tbl.account_id,
    cust_acct_tbl.name,
    recording_email_tbl.recording_dir,
    recording_email_tbl.email,
    recording_email_tbl.created_ts,
    recording_email_tbl.created_user,
    recording_email_tbl.modified_ts,
    recording_email_tbl.modified_user
   FROM recording_email_tbl
     JOIN cust_acct_tbl ON recording_email_tbl.account_id = cust_acct_tbl.id;

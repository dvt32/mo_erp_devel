CREATE VIEW recv_payment_check_vw AS
 SELECT recv_payment_tbl.id,
    recv_payment_tbl.cust_acct_id,
    cust_acct_tbl.name AS cust_acct_name,
    floor_tbl.short_global_label AS floor_label,
    recv_payment_tbl.employee_id,
    employee_tbl.last_name || ', ' || employee_tbl.first_name AS employee_name,
    recv_payment_tbl.amount,
    recv_payment_tbl.check_name,
    recv_payment_tbl.check_number,
    recv_payment_tbl.process_txn_after_dt,
    recv_payment_tbl.invoice_number,
    recv_payment_tbl.note,
    recv_payment_tbl.recv_dt,
    recv_payment_tbl.entered_in_qb,
    recv_payment_tbl.is_reconciled,
    recv_payment_tbl.deposit_id
   FROM (((recv_payment_tbl
     JOIN cust_acct_tbl ON recv_payment_tbl.cust_acct_id = cust_acct_tbl.id)
     JOIN employee_tbl ON recv_payment_tbl.employee_id = employee_tbl.id)
     JOIN floor_tbl ON cust_acct_tbl.primary_floor_id = floor_tbl.id)
 WHERE recv_payment_tbl.payment_method = 'check';

--DROP VIEW recv_payment_check_vw;

CREATE SEQUENCE report_email_seq;
CREATE SEQUENCE report_type_seq;

CREATE TABLE report_type_tbl (
    id integer DEFAULT nextval('report_type_seq'::regclass) NOT NULL PRIMARY KEY,
    name character varying(32),
    description character varying(128)
);

CREATE TABLE report_email_tbl (
    employee_id integer NOT NULL references employee_tbl(id),
    building_id integer NOT NULL references building_tbl(id),
    report_type_id integer NOT NULL references report_type_tbl(id)
);

ALTER TABLE ONLY report_email_tbl
    ADD CONSTRAINT report_email_tbl_pkey PRIMARY KEY (employee_id, building_id, report_type_id);

CREATE SEQUENCE revenue_type_seq;
CREATE SEQUENCE revenue_seq;

-- Represents services offered such as internet and phone
CREATE TABLE revenue_type_tbl (
    id integer DEFAULT nextval('revenue_type_seq'::regclass) NOT NULL PRIMARY KEY,
    description character varying(32),
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

-- Represents list price for services by building
CREATE TABLE revenue_list_price_tbl (
    id integer DEFAULT nextval('revenue_seq'::regclass) NOT NULL PRIMARY KEY,
    building_id integer NOT NULL,
    revenue_type_id integer NOT NULL,
    amount numeric(8,2) NOT NULL,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);


CREATE TRIGGER update_modified BEFORE UPDATE ON revenue_type_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON revenue_type_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER update_modified BEFORE UPDATE ON revenue_list_price_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON revenue_list_price_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

ALTER TABLE ONLY revenue_list_price_tbl
    ADD CONSTRAINT revenue_list_price_tbl_building_id_fk FOREIGN KEY (building_id) REFERENCES building_tbl(id);
ALTER TABLE ONLY revenue_list_price_tbl
    ADD CONSTRAINT revenue_list_price_tbl_revenue_type_id_fk FOREIGN KEY (revenue_type_id) REFERENCES revenue_type_tbl(id);
ALTER TABLE ONLY revenue_list_price_tbl
    ADD CONSTRAINT revenue_list_price_tbl_unique_building_id_revenue_id UNIQUE (building_id, revenue_type_id);

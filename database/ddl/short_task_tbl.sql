CREATE SEQUENCE short_task_seq;
CREATE TABLE short_task_tbl (
  id integer DEFAULT nextval('short_task_seq'::regclass) NOT NULL PRIMARY KEY,
  description text,
  input_note text,
  assigned_to integer[],
  depends_on integer[],
  result_note text,
  due_date date,
  completed_date date,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE short_task_tbl IS 'Short bite-sized tasks.';
COMMENT ON COLUMN short_task_tbl.description IS 'Description of the task.';
COMMENT ON COLUMN short_task_tbl.input_note IS 'Notes on what is needed before or during the performance of the task.';
COMMENT ON COLUMN short_task_tbl.assigned_to IS 'Array of employee IDs (from employee_tbl), in order of supervisor to implementer.';
COMMENT ON COLUMN short_task_tbl.depends_on IS 'Array of ID values of other tasks from short_task_tbl that the current task depends on.';
COMMENT ON COLUMN short_task_tbl.result_note IS 'Notes on results of the task.';
COMMENT ON COLUMN short_task_tbl.due_date IS 'Date when task is due. Can be NULL if no set due date.';
COMMENT ON COLUMN short_task_tbl.completed_date IS 'Date when task completed. NULL if not completed.';

-- validate column values in short_task_tbl
CREATE FUNCTION validate_short_task() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  employee_id integer;
  short_task_id integer;
BEGIN
  -- strip all leading and trailing spaces; set to NULL if empty string
  NEW.description = strip_collapse_space(NEW.description);
  NEW.input_note = strip_collapse_space(NEW.input_note);
  NEW.result_note = strip_collapse_space(NEW.result_note);
  IF (NEW.assigned_to IS NOT NULL) THEN
    FOREACH employee_id IN ARRAY NEW.assigned_to
    LOOP
      IF NOT EXISTS (SELECT 1 FROM employee_tbl WHERE employee_tbl.id = employee_id) THEN
        RAISE EXCEPTION 'employee_id of % in assigned_to field does not exist', employee_id;
      END IF;
    END LOOP;
  END IF;
  IF (NEW.depends_on IS NOT NULL) THEN
    FOREACH short_task_id IN ARRAY NEW.depends_on
    LOOP
      IF NOT EXISTS (SELECT 1 FROM short_task_tbl WHERE short_task_tbl.id = short_task_id) THEN
        RAISE EXCEPTION 'depends_on: short_task with ID % does not exist', short_task_id;
      END IF;
      IF (short_task_id = NEW.id) THEN
        RAISE EXCEPTION 'short_task with ID % cannot depend on itself (depends_on cannot contain ID of this task)', NEW.id;
      END IF;
    END LOOP;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON short_task_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON short_task_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON short_task_tbl FOR EACH ROW EXECUTE PROCEDURE validate_short_task();

-- set up audit log for short_task_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON short_task_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON short_task_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('short_task_tbl');

CREATE VIEW short_task_vw AS
  SELECT t1.id,
    t1.description,
    t1.input_note,
    array_to_string(array_agg(left(first_name,3) || left(last_name,3)), ',') as assigned_to,
    t1.depends_on,
    t1.result_note,
    t1.due_date,
    t1.completed_date
  FROM (select *, unnest(assigned_to) as employee_id from short_task_tbl) as t1
    JOIN employee_tbl on t1.employee_id = employee_tbl.id
  GROUP BY t1.id, t1.description, t1.input_note, t1.depends_on, t1.result_note, t1.due_date, t1.completed_date;

CREATE VIEW short_task_by_employee_vw AS
  SELECT t1.id,
    t1.description,
    t1.input_note,
    left(first_name,3) || left(last_name,3) as assigned_to,
    t1.depends_on,
    t1.result_note,
    t1.due_date,
    t1.completed_date
  FROM (select *, unnest(assigned_to) as employee_id from short_task_tbl) as t1
    JOIN employee_tbl on t1.employee_id = employee_tbl.id;

--DROP VIEW short_task_by_employee_vw;
--DROP VIEW short_task_vw;
--DROP TABLE short_task_tbl;
--DROP SEQUENCE short_task_seq;

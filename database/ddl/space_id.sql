CREATE SEQUENCE space_seq;

CREATE TABLE space_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  -- Name of table that uses the ID from this row: desk_tbl | room_tbl
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE space_id_tbl IS 'Common table to reserve ID values for desk/room tables.';
COMMENT ON COLUMN space_id_tbl.tbl_name IS 'Name of table that uses the ID from this row.';

--DROP SEQUENCE space_seq;

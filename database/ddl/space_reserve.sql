CREATE SEQUENCE space_reserve_seq;

CREATE TABLE space_reserve_tbl (
  id integer DEFAULT nextval('space_reserve_seq'::regclass) NOT NULL PRIMARY KEY,
  space_id integer NOT NULL REFERENCES space_id_tbl (id),
  account_id integer NOT NULL,
  start_ts timestamp with time zone NOT NULL,
  end_ts timestamp with time zone NOT NULL,
  note text,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE space_reserve_tbl IS 'Space reservations with desk, office, conference room support and also time zone support.';
COMMENT ON COLUMN space_reserve_tbl.space_id IS 'Link to space_vw.id.';
COMMENT ON COLUMN space_reserve_tbl.account_id IS 'Link to account_vw.id.';
COMMENT ON COLUMN space_reserve_tbl.start_ts IS 'Start timestamp of reservation in UTC.';
COMMENT ON COLUMN space_reserve_tbl.end_ts IS 'End timestamp of reservation in UTC.';

-- validate column values in space_reserve_tbl
CREATE FUNCTION validate_space_reserve() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF NEW.start_ts >= NEW.end_ts THEN
    RAISE EXCEPTION 'end_ts must be a timestamp occurring after start_ts; you entered start_ts % and end_ts %', NEW.start_ts, NEW.end_ts;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

--DROP TABLE space_reserve_tbl;
--DROP FUNCTION validate_space_reserve();
--DROP SEQUENCE space_reserve_seq;

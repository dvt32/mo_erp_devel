CREATE VIEW space_vw AS
 SELECT room_tbl.id,
    room_tbl.name,
    room_tbl.short_name,
    room_tbl.floor_id,
    room_tbl.is_shared_floor,
    room_tbl.room_type,
    room_tbl.note,
    room_tbl.is_active,
    room_tbl.created_ts,
    room_tbl.created_user,
    room_tbl.modified_ts,
    room_tbl.modified_user
   FROM room_tbl
UNION ALL
 SELECT desk_tbl.id,
    desk_tbl.assigned_number::text AS name,
    desk_tbl.assigned_number::text AS short_name,
    desk_tbl.floor_id,
    false AS is_shared_floor,
    'desk' AS room_type,
    desk_tbl.note,
    desk_tbl.is_active,
    desk_tbl.created_ts,
    desk_tbl.created_user,
    desk_tbl.modified_ts,
    desk_tbl.modified_user
   FROM desk_tbl;

--DROP VIEW space_vw;

CREATE TABLE state_tbl (
  name character varying(80) NOT NULL UNIQUE,
  abbreviation character(2) NOT NULL PRIMARY KEY
);

COMMENT ON TABLE state_tbl IS 'States in the USA';
COMMENT ON COLUMN state_tbl.name IS 'Name of the state';
COMMENT ON COLUMN state_tbl.abbreviation IS '2 character abbreviation of the state';

--DROP TABLE state_tbl;

CREATE SEQUENCE symmetric_key_seq;
CREATE TABLE symmetric_key_tbl (
  id integer DEFAULT nextval('symmetric_key_seq'::regclass) NOT NULL PRIMARY KEY,
  encrypted_key bytea NOT NULL,
  key_size integer NOT NULL,
  public_key_id integer NOT NULL REFERENCES public_key_tbl (id),
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);
COMMENT ON TABLE symmetric_key_tbl IS 'Symmetric encryption keys';
COMMENT ON COLUMN symmetric_key_tbl.encrypted_key IS 'Symmetric key, encrypted using a public key';
COMMENT ON COLUMN symmetric_key_tbl.key_size IS 'Size of key in bits';
COMMENT ON COLUMN symmetric_key_tbl.public_key_id IS 'Public key used to encrypt the symmetric key, linked to public_key_tbl.id';

--DROP TABLE symmetric_key_tbl;
--DROP SEQUENCE symmetric_key_seq;

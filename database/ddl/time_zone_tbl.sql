CREATE TABLE time_zone_tbl (
    tzname character varying(255) NOT NULL UNIQUE,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

--
-- Name: update_modified_time_zone; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_time_zone BEFORE UPDATE ON time_zone_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_time_zone; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_time_zone BEFORE INSERT ON time_zone_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--DROP TABLE time_zone_tbl;

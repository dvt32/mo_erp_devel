--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: utc_reservation_tbl; Type: TABLE; Schema: public; Owner: shasan; Tablespace: 
--

CREATE TABLE utc_reservation_tbl (
    id integer DEFAULT nextval(('reservation_seq'::text)::regclass) NOT NULL,
    room_id integer NOT NULL,
    account_id integer NOT NULL,
    start_dt timestamp with time zone NOT NULL,
    end_dt timestamp with time zone NOT NULL,
    note text,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255),
    CONSTRAINT utc_reservation_tbl_end_dt_check CHECK ((date_part('timezone'::text, end_dt) = (0)::double precision)),
    CONSTRAINT utc_reservation_tbl_start_dt_check CHECK ((date_part('timezone'::text, start_dt) = (0)::double precision))
);


ALTER TABLE utc_reservation_tbl OWNER TO shasan;

--
-- Name: utc_reservation_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: shasan; Tablespace: 
--

ALTER TABLE ONLY utc_reservation_tbl
    ADD CONSTRAINT utc_reservation_tbl_pkey PRIMARY KEY (id);


--
-- Name: insert_created; Type: TRIGGER; Schema: public; Owner: shasan
--

CREATE TRIGGER insert_created BEFORE INSERT ON utc_reservation_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();


--
-- Name: update_modified; Type: TRIGGER; Schema: public; Owner: shasan
--

CREATE TRIGGER update_modified BEFORE UPDATE ON utc_reservation_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();


--
-- Name: fk_room_id; Type: FK CONSTRAINT; Schema: public; Owner: shasan
--

ALTER TABLE ONLY utc_reservation_tbl
    ADD CONSTRAINT fk_room_id FOREIGN KEY (room_id) REFERENCES room_tbl(id);


--
-- Name: utc_reservation_tbl; Type: ACL; Schema: public; Owner: shasan
--

REVOKE ALL ON TABLE utc_reservation_tbl FROM PUBLIC;
REVOKE ALL ON TABLE utc_reservation_tbl FROM shasan;
GRANT ALL ON TABLE utc_reservation_tbl TO shasan;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE utc_reservation_tbl TO mo_apply;


--
-- PostgreSQL database dump complete
--


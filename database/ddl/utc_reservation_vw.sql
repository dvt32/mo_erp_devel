--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: utc_reservation_vw; Type: VIEW; Schema: public; Owner: shasan
--

CREATE VIEW utc_reservation_vw AS
 SELECT utc_reservation_tbl.id,
    floor_tbl.short_global_label AS floor_name,
    room_tbl.short_name AS room_name,
    room_tbl.id AS room_id,
    account_vw.name AS account_name,
    account_vw.id AS account_id,
    utc_reservation_tbl.start_dt,
    utc_reservation_tbl.end_dt,
    utc_reservation_tbl.note,
    utc_reservation_tbl.creation_dt
   FROM (((utc_reservation_tbl
     JOIN room_tbl ON ((utc_reservation_tbl.room_id = room_tbl.id)))
     JOIN floor_tbl ON ((room_tbl.floor_id = floor_tbl.id)))
     JOIN account_vw ON ((utc_reservation_tbl.account_id = account_vw.id)));


ALTER TABLE utc_reservation_vw OWNER TO shasan;

--
-- Name: utc_reservation_vw; Type: ACL; Schema: public; Owner: shasan
--

REVOKE ALL ON TABLE utc_reservation_vw FROM PUBLIC;
REVOKE ALL ON TABLE utc_reservation_vw FROM shasan;
GRANT ALL ON TABLE utc_reservation_vw TO shasan;
GRANT SELECT ON TABLE utc_reservation_vw TO mo_apply;


--
-- PostgreSQL database dump complete
--

CREATE SEQUENCE vend_bill_img_seq;

CREATE TABLE vend_bill_img_tbl (
  id integer DEFAULT nextval('vend_bill_img_seq'::regclass) NOT NULL PRIMARY KEY,
  vendor_id integer NOT NULL REFERENCES vendor_tbl (id),
  date date NOT NULL,
  note text,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE vend_bill_img_tbl IS 'Vendor bill scanned images';
COMMENT ON COLUMN vend_bill_img_tbl.vendor_id IS 'Link to vendor_tbl.id';

CREATE TRIGGER update_modified BEFORE UPDATE ON vend_bill_img_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON vend_bill_img_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

-- set up audit log for vend_bill_img_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON vend_bill_img_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON vend_bill_img_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('vend_bill_img_tbl');

CREATE VIEW vend_bill_img_vw AS
 SELECT vend_bill_img_tbl.id,
    vend_bill_img_tbl.vendor_id,
    vendor_tbl.name,
    vend_bill_img_tbl.date,
    vend_bill_img_tbl.note,
    vend_bill_img_tbl.created_ts,
    vend_bill_img_tbl.created_user,
    vend_bill_img_tbl.modified_ts,
    vend_bill_img_tbl.modified_user
   FROM vend_bill_img_tbl
     JOIN vendor_tbl ON vend_bill_img_tbl.vendor_id = vendor_tbl.id;

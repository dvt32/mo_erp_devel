CREATE SEQUENCE vend_vault_seq;

CREATE TABLE vend_vault_id_tbl (
  id integer NOT NULL PRIMARY KEY,
  -- Name of table that uses the ID from this row: vend_vault_ach_tbl | vend_vault_card_tbl | vend_vault_paypal_tbl
  tbl_name character varying(64),
  created_ts timestamp with time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE vend_vault_id_tbl IS 'Common table to reserve ID values for vend_vault_x_tbl tables';
COMMENT ON COLUMN vend_vault_id_tbl.tbl_name IS 'Name of table that uses the ID from this row';

CREATE TABLE vend_vault_ach_tbl (
  id integer DEFAULT nextval('vend_vault_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to vendor_tbl.id
  vendor_id integer NOT NULL REFERENCES vendor_tbl (id),
  -- account_type: checking or savings
  account_type character varying(8) NOT NULL REFERENCES enum_bank_account_type_tbl (account_type),
  -- owned_by: business or personal
  owned_by character varying(8) NOT NULL REFERENCES enum_bank_account_owned_by_tbl (owned_by),
  -- name of the person or company that owns the money in the bank account
  registered_name character varying(32),
  routing_number character(9) NOT NULL,
  account_number character varying(17) NOT NULL,
  -- two sub-$1 deposits are made to the account (and then debited later) to verify control
  confirmed_ts timestamp with time zone,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE vend_vault_ach_tbl IS 'Vault for encrypted vendor bank accounts';
COMMENT ON COLUMN vend_vault_ach_tbl.vendor_id IS 'Link to vendor_tbl.id';
COMMENT ON COLUMN vend_vault_ach_tbl.account_type IS 'checking or savings';
COMMENT ON COLUMN vend_vault_ach_tbl.owned_by IS 'business or personal';
COMMENT ON COLUMN vend_vault_ach_tbl.registered_name IS 'Name of the person or entity that has beneficial ownership of the bank account';
COMMENT ON COLUMN vend_vault_ach_tbl.routing_number IS 'ACH bank routing number';
COMMENT ON COLUMN vend_vault_ach_tbl.account_number IS 'Bank account number';
COMMENT ON COLUMN vend_vault_ach_tbl.confirmed_ts IS 'Timestamp of when the bank account was confirmed via entry of correct two deposits. NULL if not yet confirmed.';

-- validate column values in vend_vault_ach_tbl
CREATE FUNCTION validate_vend_vault_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.registered_name = strip_collapse_space(NEW.registered_name);
  NEW.routing_number = strip_space(NEW.routing_number);
  -- if operation is INSERT or routing_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.routing_number IS DISTINCT FROM OLD.routing_number)) THEN
    IF NEW.routing_number IS NULL THEN
      RAISE EXCEPTION 'routing_number is required';
    END IF;
    IF NEW.routing_number !~ '^\d{9}$' THEN
      RAISE EXCEPTION 'routing_number must be 9 digits; you entered %', NEW.routing_number;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM fedach_participant_tbl WHERE routing_number = NEW.routing_number) THEN
      RAISE EXCEPTION 'routing_number not found in FedACH Participants; you entered %', NEW.routing_number;
    END IF;
  END IF;
  NEW.account_number = strip_space(NEW.account_number);
  -- if operation is INSERT or account_number changed during UPDATE, validate
  -- it could be masked from last update, in which case it would be same, so do not validate if so
  IF ((TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number)) THEN
    IF NEW.account_number IS NULL THEN
      RAISE EXCEPTION 'account_number is required';
    END IF;
    IF NEW.account_number !~ '^\d+$' THEN
      RAISE EXCEPTION 'account_number must be numeric; you entered %', NEW.account_number;
    END IF;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

-- mask sensitive fields in vend_vault_ach_tbl
CREATE FUNCTION mask_vend_vault_ach() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.account_number IS NOT NULL) THEN
    IF (TG_OP = 'INSERT') OR (NEW.account_number IS DISTINCT FROM OLD.account_number) THEN
      IF length(NEW.account_number) > 4 THEN
        NEW.account_number = left(NEW.account_number, 1) || repeat('*', length(NEW.account_number) - 4) || right(NEW.account_number, 3);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('vend_vault_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('vend_vault_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('vend_vault_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE validate_vend_vault_ach();
-- trigger name starts with z1_ to sort second-last so trigger executes second-last
CREATE TRIGGER z1_crypt BEFORE INSERT OR UPDATE ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE crypt_row('account_number');
CREATE TRIGGER z2_mask BEFORE INSERT OR UPDATE ON vend_vault_ach_tbl FOR EACH ROW EXECUTE PROCEDURE mask_vend_vault_ach();

-- set up audit log for vend_vault_ach_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON vend_vault_ach_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON vend_vault_ach_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('vend_vault_ach_tbl');

CREATE TYPE paypal_recipient_type AS ENUM ('email', 'phone', 'paypal_id');

CREATE TABLE vend_vault_paypal_tbl (
  id integer DEFAULT nextval('vend_vault_seq'::regclass) NOT NULL PRIMARY KEY,
  -- Link to vendor_tbl.id
  vendor_id integer NOT NULL REFERENCES vendor_tbl (id),
  recipient_type paypal_recipient_type NOT NULL,
  receiver character varying(127) NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  crypt_json jsonb,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE vend_vault_paypal_tbl IS 'Vault for encrypted vendor bank accounts';
COMMENT ON COLUMN vend_vault_paypal_tbl.vendor_id IS 'Link to vendor_tbl.id';
COMMENT ON COLUMN vend_vault_paypal_tbl.recipient_type IS 'email | phone | paypay_id';
COMMENT ON COLUMN vend_vault_paypal_tbl.receiver IS 'email or phone number or PayPal ID';

-- validate column values in vend_vault_paypal_tbl
CREATE FUNCTION validate_vend_vault_paypal() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';
BEGIN
  NEW.receiver = strip_space(NEW.receiver);
  IF NEW.recipient_type = 'email' THEN
    IF NEW.receiver !~* email_regexp_pattern THEN
      RAISE EXCEPTION 'receiver must be a valid email address when recipient_type is email; you entered %', NEW.receiver;
    END IF;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

CREATE TRIGGER reserve_id BEFORE INSERT ON vend_vault_paypal_tbl FOR EACH ROW EXECUTE PROCEDURE reserve_id('vend_vault_id_tbl');
CREATE TRIGGER change_id_reservation BEFORE UPDATE ON vend_vault_paypal_tbl FOR EACH ROW EXECUTE PROCEDURE change_id_reservation('vend_vault_id_tbl');
CREATE TRIGGER free_id_reservation AFTER DELETE ON vend_vault_paypal_tbl FOR EACH ROW EXECUTE PROCEDURE free_id_reservation('vend_vault_id_tbl');
CREATE TRIGGER update_modified BEFORE UPDATE ON vend_vault_paypal_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON vend_vault_paypal_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON vend_vault_paypal_tbl FOR EACH ROW EXECUTE PROCEDURE validate_vend_vault_paypal();

-- set up audit log for vend_vault_paypal_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON vend_vault_paypal_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON vend_vault_paypal_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('vend_vault_paypal_tbl');

CREATE VIEW vend_vault_vw AS
 SELECT vend_vault_ach_tbl.id,
    'ach'::character varying(8) AS pmt_method,
    vend_vault_ach_tbl.vendor_id,
    vendor_tbl.name,
    vend_vault_ach_tbl.account_type,
    vend_vault_ach_tbl.owned_by,
    vend_vault_ach_tbl.registered_name,
    vend_vault_ach_tbl.routing_number,
    vend_vault_ach_tbl.account_number,
    vend_vault_ach_tbl.confirmed_ts,
    NULL AS recipient_type,
    NULL AS receiver,
    vend_vault_ach_tbl.note,
    vend_vault_ach_tbl.is_active,
    vend_vault_ach_tbl.crypt_json,
    vend_vault_ach_tbl.created_ts,
    vend_vault_ach_tbl.created_user,
    vend_vault_ach_tbl.modified_ts,
    vend_vault_ach_tbl.modified_user
   FROM (vend_vault_ach_tbl
     JOIN vendor_tbl ON (vend_vault_ach_tbl.vendor_id = vendor_tbl.id))
UNION ALL
 SELECT vend_vault_paypal_tbl.id,
    'paypal'::character varying(8) AS pmt_method,
    vend_vault_paypal_tbl.vendor_id,
    vendor_tbl.name,
    NULL AS account_type,
    NULL AS owned_by,
    NULL AS registered_name,
    NULL AS routing_number,
    NULL AS account_number,
    NULL AS confirmed_ts,
    vend_vault_paypal_tbl.recipient_type,
    vend_vault_paypal_tbl.receiver,
    vend_vault_paypal_tbl.note,
    vend_vault_paypal_tbl.is_active,
    vend_vault_paypal_tbl.crypt_json,
    vend_vault_paypal_tbl.created_ts,
    vend_vault_paypal_tbl.created_user,
    vend_vault_paypal_tbl.modified_ts,
    vend_vault_paypal_tbl.modified_user
   FROM (vend_vault_paypal_tbl
     JOIN vendor_tbl ON (vend_vault_paypal_tbl.vendor_id = vendor_tbl.id));

--DROP VIEW vend_vault_vw;
--DROP TABLE vend_vault_paypal_tbl;
--DROP FUNCTION validate_vend_vault_paypal();
--DROP TYPE paypal_recipient_type;
--DROP TABLE vend_vault_ach_tbl;
--DROP FUNCTION validate_vend_vault_ach();
--DROP FUNCTION mask_vend_vault_ach();
--DROP TABLE vend_vault_id_tbl;
--DROP SEQUENCE vend_vault_seq;

CREATE SEQUENCE virtual_office_seq;

CREATE TABLE virtual_office_tbl (
 id integer DEFAULT nextval('virtual_office_seq'::regclass) NOT NULL PRIMARY KEY,
 account_id int references cust_acct_tbl(id),
 -- location for virtual office
 floor_id int references floor_tbl(id),
 -- floor_tbl.vo_setup_fee
 setup_fee numeric(8,2),
 -- floor_tbl.vo_monthly_fee
 monthly_fee numeric(8,2),
 has_phone boolean default false,
 -- floor_tbl.vo_phone_fee * (1 + floor_tbl.tax_rate)
 phone_fee numeric(8,2),
 has_mail_forwarding boolean default false,
 -- floor_tbl.vo_mail_forwarding_fee
 mail_forwarding_fee numeric(8,2),
 -- weekly, biweekly, monthly
 mail_forwarding_frequency varchar(32),
 has_mail_scanning boolean default false,
 -- floor_tbl.vo_mail_scanning_fee
 mail_scanning_fee numeric(8,2),
 start_date date NOT NULL,
 end_date date,
 mailbox text,
 billing_company text,
 note text,
 initial_pmt_txn_id int references pmt_vault_txn_tbl(id),
 recur_id int references pmt_recur_month_tbl(id),
 created_ts timestamp with time zone DEFAULT now() NOT NULL,
 created_user character varying(255) DEFAULT "current_user"() NOT NULL,
 modified_ts timestamp with time zone,
 modified_user character varying(255)
);

CREATE TRIGGER update_modified BEFORE UPDATE ON virtual_office_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON virtual_office_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

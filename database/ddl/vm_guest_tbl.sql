CREATE SEQUENCE vm_guest_seq;
ALTER TABLE vm_guest_seq OWNER TO wchao;

--
-- Name: vm_guest_tbl; Type: TABLE; Schema: public; Owner: wchao; Tablespace: 
--

CREATE TABLE vm_guest_tbl (
    id integer DEFAULT nextval('vm_guest_seq'::regclass) NOT NULL PRIMARY KEY,
    vm_host_id integer NOT NULL REFERENCES vm_host_tbl (id),
    virt_type character varying(16) NOT NULL,
    operating_system character varying(16) NOT NULL,
    name character varying(64) NOT NULL,
    fqdn_list text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    note text,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

--
-- Name: vm_guest_tbl_vm_host_id_name_key; Type: CONSTRAINT; Schema: public; Owner: wchao; Tablespace: 
--

ALTER TABLE ONLY vm_guest_tbl
    ADD CONSTRAINT vm_guest_tbl_vm_host_id_name_key UNIQUE (vm_host_id, name);


--
-- Name: update_modified_vm_guest; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_vm_guest BEFORE UPDATE ON vm_guest_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_vm_guest; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_vm_guest BEFORE INSERT ON vm_guest_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--
-- Name: vm_guest_vw; Type: VIEW; Schema: public; Owner: wchao
--

CREATE VIEW vm_guest_vw AS
 SELECT vm_guest_tbl.id,
    floor_tbl.short_global_label AS floor_name,
    vm_host_tbl.name AS vm_host_name,
    vm_guest_tbl.virt_type,
    vm_guest_tbl.operating_system,
    vm_guest_tbl.name,
    vm_guest_tbl.fqdn_list,
    vm_guest_tbl.description,
    vm_guest_tbl.note,
    vm_guest_tbl.created_ts,
    vm_guest_tbl.modified_ts
   FROM ((vm_guest_tbl
     JOIN vm_host_tbl ON ((vm_guest_tbl.vm_host_id = vm_host_tbl.id)))
     JOIN floor_tbl ON ((vm_host_tbl.floor_id = floor_tbl.id)));

--DROP VIEW vm_guest_vw;
--DROP TABLE vm_guest_tbl;
--DROP SEQUENCE vm_guest_seq;

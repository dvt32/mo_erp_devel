CREATE SEQUENCE vm_host_seq;
ALTER TABLE vm_host_seq OWNER TO wchao;

--
-- Name: vm_host_tbl; Type: TABLE; Schema: public; Owner: wchao; Tablespace: 
--

CREATE TABLE vm_host_tbl (
    id integer DEFAULT nextval('vm_host_seq'::regclass) NOT NULL PRIMARY KEY,
    floor_id integer NOT NULL REFERENCES floor_tbl (id),
    name character varying(64) NOT NULL,
    fqdn_list text DEFAULT ''::text NOT NULL,
    operating_system character varying(16) NOT NULL,
    cpu character varying(32) DEFAULT ''::character varying NOT NULL,
    speed integer DEFAULT 0 NOT NULL,
    sockets integer DEFAULT 1 NOT NULL,
    cores integer DEFAULT 1 NOT NULL,
    threads integer DEFAULT 1 NOT NULL,
    is_vtx boolean DEFAULT true NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    note text,
    created_ts timestamp with time zone DEFAULT now() NOT NULL,
    created_user character varying(255) DEFAULT "current_user"() NOT NULL,
    modified_ts timestamp with time zone,
    modified_user character varying(255)
);

--
-- Name: COLUMN vm_host_tbl.operating_system; Type: COMMENT; Schema: public; Owner: wchao
--

COMMENT ON COLUMN vm_host_tbl.operating_system IS 'centos6, centos7, debian8, win2012, etc.';


--
-- Name: COLUMN vm_host_tbl.cpu; Type: COMMENT; Schema: public; Owner: wchao
--

COMMENT ON COLUMN vm_host_tbl.cpu IS 'Intel i7-3770, Intel i7-3770K, etc.';


--
-- Name: COLUMN vm_host_tbl.speed; Type: COMMENT; Schema: public; Owner: wchao
--

COMMENT ON COLUMN vm_host_tbl.speed IS 'CPU speed in MHz';


--
-- Name: COLUMN vm_host_tbl.sockets; Type: COMMENT; Schema: public; Owner: wchao
--

COMMENT ON COLUMN vm_host_tbl.sockets IS '# of CPU sockets in the machine';


--
-- Name: COLUMN vm_host_tbl.cores; Type: COMMENT; Schema: public; Owner: wchao
--

COMMENT ON COLUMN vm_host_tbl.cores IS '# of cores per socket';


--
-- Name: COLUMN vm_host_tbl.threads; Type: COMMENT; Schema: public; Owner: wchao
--

COMMENT ON COLUMN vm_host_tbl.threads IS '# of threads per core';


--
-- Name: COLUMN vm_host_tbl.is_vtx; Type: COMMENT; Schema: public; Owner: wchao
--

COMMENT ON COLUMN vm_host_tbl.is_vtx IS 'Is machine VT-x capable?';


--
-- Name: update_modified_vm_host; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER update_modified_vm_host BEFORE UPDATE ON vm_host_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();

--
-- Name: insert_created_vm_host; Type: TRIGGER; Schema: public; Owner: wchao
--

CREATE TRIGGER insert_created_vm_host BEFORE INSERT ON vm_host_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();

--
-- Name: vm_host_vw; Type: VIEW; Schema: public; Owner: wchao
--

CREATE VIEW vm_host_vw AS
 SELECT vm_host_tbl.id,
    floor_tbl.short_global_label AS floor_name,
    vm_host_tbl.name,
    vm_host_tbl.fqdn_list,
    vm_host_tbl.operating_system,
    vm_host_tbl.cpu,
    vm_host_tbl.speed,
    vm_host_tbl.sockets,
    vm_host_tbl.cores,
    vm_host_tbl.threads,
    vm_host_tbl.is_vtx,
    vm_host_tbl.description,
    vm_host_tbl.note,
    vm_host_tbl.created_ts,
    vm_host_tbl.modified_ts
   FROM (vm_host_tbl
     JOIN floor_tbl ON ((vm_host_tbl.floor_id = floor_tbl.id)));

--DROP VIEW vm_host_vw;
--DROP TABLE vm_host_tbl;
--DROP SEQUENCE vm_host_seq;

CREATE SEQUENCE voicemail_box_seq;

CREATE TABLE voicemail_box_tbl (
  id integer DEFAULT nextval('voicemail_box_seq'::regclass) NOT NULL PRIMARY KEY,
  account_id integer NOT NULL,
  person_id integer,
  box_number varchar(32) NOT NULL UNIQUE,
  email text,
  note text,
  created_ts timestamp with time zone DEFAULT now() NOT NULL,
  created_user character varying(255) DEFAULT "current_user"() NOT NULL,
  modified_ts timestamp with time zone,
  modified_user character varying(255)
);

COMMENT ON TABLE voicemail_box_tbl IS 'Voicemail boxes';
COMMENT ON COLUMN voicemail_box_tbl.account_id IS 'Link to account_vw.id';
COMMENT ON COLUMN voicemail_box_tbl.person_id IS 'Link to person_vw.id';
COMMENT ON COLUMN voicemail_box_tbl.box_number IS 'Unique name/number of voicemail box, e.g. 2125551212 or 2125551212x201';
COMMENT ON COLUMN voicemail_box_tbl.email IS 'Zero or more email addresses where we should send the voicemail';

-- validate column values in voicemail_box_tbl
CREATE FUNCTION validate_voicemail_box() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  email_regexp_pattern CONSTANT varchar = '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}(,[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,})*$';
BEGIN
  IF NOT EXISTS (SELECT 1 FROM account_vw WHERE id = NEW.account_id) THEN
    RAISE EXCEPTION 'account_id not found in account_vw; you entered %', NEW.account_id;
  END IF;
  IF (NEW.person_id IS NOT NULL) THEN
    IF NOT EXISTS (SELECT 1 FROM person_vw WHERE id = NEW.person_id) THEN
      RAISE EXCEPTION 'person_id not found in person_vw; you entered %', NEW.person_id;
    END IF;
  END IF;
  NEW.box_number = strip_collapse_space(NEW.box_number);
  NEW.email = strip_collapse_space(NEW.email);
  IF (NEW.email IS NOT NULL) AND (NEW.email !~* email_regexp_pattern) THEN
    RAISE EXCEPTION 'email must be zero or more valid email addresses separated by commas; you entered %', NEW.email;
  END IF;
  NEW.note = strip_collapse_space(NEW.note);
  RETURN NEW;
END;
$$;

CREATE TRIGGER update_modified BEFORE UPDATE ON voicemail_box_tbl FOR EACH ROW EXECUTE PROCEDURE update_modified();
CREATE TRIGGER insert_created BEFORE INSERT ON voicemail_box_tbl FOR EACH ROW EXECUTE PROCEDURE insert_created();
CREATE TRIGGER validate_row BEFORE INSERT OR UPDATE ON voicemail_box_tbl FOR EACH ROW EXECUTE PROCEDURE validate_voicemail_box();

-- set up audit log for voicemail_box_tbl
-- to remove audit log without dropping table:
-- DROP TRIGGER IF EXISTS audit_trigger_row ON voicemail_box_tbl
-- DROP TRIGGER IF EXISTS audit_trigger_stm ON voicemail_box_tbl
-- both triggers are automatically dropped when table is dropped
SELECT audit.audit_table('voicemail_box_tbl');

CREATE VIEW voicemail_box_vw AS
 SELECT voicemail_box_tbl.id,
    voicemail_box_tbl.account_id,
    account_vw.name AS account_name,
    voicemail_box_tbl.person_id,
    person_vw.last_name || ', ' || person_vw.first_name AS person_name,
    voicemail_box_tbl.box_number,
    voicemail_box_tbl.email,
    voicemail_box_tbl.note,
    voicemail_box_tbl.created_ts,
    voicemail_box_tbl.created_user,
    voicemail_box_tbl.modified_ts,
    voicemail_box_tbl.modified_user
   FROM (voicemail_box_tbl
     JOIN account_vw ON (voicemail_box_tbl.account_id = account_vw.id)
     LEFT JOIN person_vw ON (voicemail_box_tbl.person_id = person_vw.id));

--DROP VIEW voicemail_box_vw;
--DROP TABLE voicemail_box_tbl;
--DROP FUNCTION validate_voicemail_box();
--DROP SEQUENCE voicemail_box_seq;

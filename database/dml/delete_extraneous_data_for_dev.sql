-- delete search rankings since they occupy lots of space and are not currently used
delete from bing_query_tbl;
delete from bing_search_result_tbl;
delete from google_query_tbl;
delete from google_search_result_tbl;
delete from yahoo_search_result_tbl;
delete from yahoo_query_tbl;
-- search results by users
delete from result_list_tbl;
delete from result_item_tbl;

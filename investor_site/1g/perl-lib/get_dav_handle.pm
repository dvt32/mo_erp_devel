use strict;
local($^W) = 1;

use HTTP::DAV;
use is_production_env;

sub get_dav_handle($$) {
  my ($instance_map, $handle_name) = @_;
  if (!$instance_map->{"dav"}) {
    $instance_map->{"dav"} = {};
  }
  my $dav_map;
  if (!($dav_map = $instance_map->{"dav"}->{$handle_name})) {
    my ($base_url, $username, $password, $max_file_id);
    if ($handle_name eq "library") {
      $base_url = "{{ dav_base_url }}";
      $username = "{{ dav_username }}";
      $password = "{{ dav_password }}";
      $max_file_id = 999999;
    } else {
      die "get_dav_handle: bad handle name ($handle_name)\n";
    }
    my $dav_handle = HTTP::DAV->new();
    $dav_handle->credentials("-user" => $username, "-pass" => $password, "-url" => $base_url);
    $dav_handle->open("-url" => $base_url) or die "couldn't open $base_url: " . $dav_handle->message();
    $dav_map = {};
    $instance_map->{"dav"}->{$handle_name} = $dav_map;
    ($dav_map->{"dav_handle"}, $dav_map->{"dav_base_url"}, $dav_map->{"dav_max_file_id"}) = ($dav_handle, $base_url, $max_file_id);
  }
  return ($dav_map->{"dav_handle"}, $dav_map->{"dav_base_url"}, $dav_map->{"dav_max_file_id"});
}

1;

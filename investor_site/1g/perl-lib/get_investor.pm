use strict;
local($^W) = 1;

use get_cgi;
use get_session;
use select_row;
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use get_database_handle;

sub get_investor($) {
  my $instance_map = shift;
  if (!$instance_map->{"investor"}) {
    my $session;
    if (($session = get_session($instance_map)) && ($session->{"investor_id"})) {
      my $dbh = get_database_handle($instance_map, "main");
      my $investor;
      if ($investor = select_row($dbh, "investor_tbl", $session->{"investor_id"})) {
	if ((!$investor->{"is_active"}) || (!$investor->{"password"})) {
	  undef $investor;
	} else {
	  $instance_map->{"investor"} = $investor;
	}
      }
    }
  }
  return $instance_map->{"investor"};
}

1;

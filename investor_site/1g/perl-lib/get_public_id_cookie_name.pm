use strict;
local($^W) = 1;

use lib "{{ investor_commondir }}";
use is_production_env;

sub get_public_id_cookie_name() {
  if (is_production_env()) {
    return "investor.microoffice.com:public_id";
  } else {
    return "investor-devel.microoffice.com:public_id";
  }
}

1;

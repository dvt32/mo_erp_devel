use strict;
local($^W) = 1;

use get_set_by_parent_id;
use lib "{{ investor_commondir }}";
use get_database_handle;

# get the list of roles for an investor.
# pass in the database handle and investor ID.
# returns a reference to an array of roles.
sub get_role_list($$) {
  my ($instance_map, $investor_id) = @_;
  if (!$instance_map->{"role_list"}) {
    my $dbh = get_database_handle($instance_map, "main");
    my @role_list;
    get_set_by_parent_id($dbh, "ss_investor_x_role_tbl", $investor_id, \@role_list);
    $instance_map->{"role_list"} = \@role_list;
  }
  return $instance_map->{"role_list"};
}

1;

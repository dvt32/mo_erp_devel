use strict;
local($^W) = 1;

my $web_root_dir = "{{ investor_basedir }}/1g";

sub get_web_root_dir() {
  return $web_root_dir;
}

1;

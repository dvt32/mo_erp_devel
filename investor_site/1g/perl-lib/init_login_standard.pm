use strict;
local($^W) = 1;

use get_cgi;
use get_investor;
use get_web_root_dir;
use lib "{{ investor_commondir }}";
use get_database_handle;

sub init_login_standard($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = (get_cgi($instance_map), get_database_handle($instance_map, "main"), get_investor($instance_map));
  if (!$investor) {
    my $web_root_dir = get_web_root_dir();
    die "show_page: $web_root_dir/auth/login_must_login.html\n";
  }
  return ($cgi, $dbh, $investor);
}

1;

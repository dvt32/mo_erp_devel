#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Encode ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_cgi_input_charset;
use get_database_charset;
use update_row;
use show_page;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my ($old_password, $new_password, $new_password_2) = map {Encode::decode($cgi_input_charset, $cgi->param($_))} (qw(old_password new_password new_password_2));
  # user entered a bad old password. display error.
  if ((!$old_password) || ($old_password ne $investor->{"password"})) {
    die "show_page: change_password_bad_old_password.html\n";
  }
  # user did not enter a new password.
  if ((!$new_password) && (!$new_password_2)) {
    die "show_page: change_password_must_enter_new_password.html\n";
  }
  # user did not enter matching new passwords.
  if ($new_password ne $new_password_2) {
    die "show_page: change_password_new_password_mismatch.html\n";
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["password", Encode::encode($db_charset, $new_password)]);
  update_row($dbh, "investor_tbl", \@sql_column_list, $investor->{"id"});
  # display success page.
  show_page($instance_map, "change_password_success.html");
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use Encode ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use get_cgi_input_charset;
use get_database_charset;
use show_page;
use select_row_with_query;
use send_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email"));
  # user did not enter email. display error.
  if (!$email) {
    die "show_page: forgot_password_must_enter_email.html\n";
  }
  # make email lowercase to avoid case problems.
  $email = lc($email);
  my $db_charset = get_database_charset($instance_map, "main");
  my $sql_query = "select * from investor_tbl where email = ? and is_active";
  my @sql_param_list = (Encode::encode($db_charset, $email));
  my $investor = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  # no row with matching email found. display error. 
  if (!$investor) {
    die "show_page: forgot_password_not_found.html\n";
  }
  # no assigned password.
  if (!$investor->{"password"}) {
    die "show_page: forgot_password_no_password.html\n";
  }
  # found matching email. send password in email and display success page.
  my $email = $investor->{"email"};
  my $password = $investor->{"password"};
  # send password via SMTP.
  my $message .= <<"---END_BLOCK---;";
Your password for the Micro Office Solutions investor center (https://investor.microoffice.com) is $password.

We advise you to change the password to something that is both secure (hard to guess for other people) and easy for you to remember.
---END_BLOCK---;
  send_email
    ("from" => "Micro Office Solutions <investor_relations\@microoffice.com>",
     "to" => $email,
     "subject" => "Your password request for Micro Office investor center.",
     "message" => $message);
  show_page($instance_map, "forgot_password_found.html");
}

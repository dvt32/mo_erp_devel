#!/usr/bin/perl

use strict;
local($^W) = 1;

use Encode ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use get_cgi_input_charset;
use get_database_charset;
use create_session;
use select_row_with_query;
use show_page;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email"));
  my $password = Encode::decode($cgi_input_charset, $cgi->param("password"));
  my $is_persistent = $cgi->param("is_persistent");
  # user did not enter email. display error.
  if (!$email) {
    die "show_page: login_must_enter_email.html\n";
  }
  # make email lowercase to avoid case problems.
  $email = lc($email);
  my $db_charset = get_database_charset($instance_map, "main");
  my $sql_query = "select * from investor_tbl where email = ? and length(password) > 0 and is_active";
  my @sql_param_list = (Encode::encode($db_charset, $email));
  my $investor = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  # no row with matching email found, or investor does not have login account, or bad password. display error.
  if ((!$investor) || (!$investor->{"password"}) || ($password ne $investor->{"password"})) {
    die "show_page: login_result_bad.html\n";
  }
  # found matching email and password. display success page.
  my $cookie = create_session($instance_map, "investor_id", $investor->{"id"}, $is_persistent);
  show_page($instance_map, "login_result_good.html", $cookie);
}

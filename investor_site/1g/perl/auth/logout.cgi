#!/usr/bin/perl

use strict;
local($^W) = 1;

use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use show_page;
use get_session;
use remove_session;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  if (!get_session($instance_map)) {
    die "show_page: login_must_login.html\n";
  }
  my $cookie = remove_session($instance_map);
  show_page($instance_map, "logout.html", $cookie);
}

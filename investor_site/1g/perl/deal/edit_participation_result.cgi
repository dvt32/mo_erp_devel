#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use select_row_with_query;
use insert_row;
use update_row;
use get_date_now;
use get_month_name;
use enqueue_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # get CGI parameters.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $investor_participation_id = $cgi->param("id");
  $investor_participation_id =~ s/^\s+//;
  $investor_participation_id =~ s/\s+$//;
  $investor_participation_id =~ s/\s+/ /;
  my $investor_participation;
  if ($investor_participation_id eq "") {
    # OK if blank.
  } elsif ($investor_participation_id =~ m{^\d+$}) {
    # OK if numeric, but check that row is valid.
    $investor_participation = select_row($dbh, "investor_participation_tbl", $investor_participation_id);
    if (!$investor_participation) {
      die "show_page: edit_participation_not_found.html\n";
    } elsif ($investor->{"id"} != $investor_participation->{"investor_id"}) {
      die "show_page: edit_participation_not_owner.html\n";
    }
  } else {
    # not OK if investor_participation_id is not a number.
    die "show_page: edit_participation_not_found.html\n";
  }
  my $investor_deal_id = $cgi->param("investor_deal_id");
  $investor_deal_id =~ s/^\s+//;
  $investor_deal_id =~ s/\s+$//;
  $investor_deal_id =~ s/\s+/ /;
  my $investor_deal;
  if ($investor_deal_id =~ m{^\d+$}) {
    # check that row is valid.
    $investor_deal = select_row($dbh, "investor_deal_tbl", $investor_deal_id);
    if (!$investor_deal) {
      die "show_page: edit_participation_deal_not_found.html\n";
    }
    $investor_deal_id = $investor_deal->{"id"};
  } else {
    # not OK if investor_deal_id is blank or not a number.
    die "show_page: edit_participation_deal_not_found.html\n";
  }
  my $amount = $cgi->param("amount");
  $amount =~ s/^\s+//;
  $amount =~ s/\s+$//;
  $amount =~ s/\s+/ /;
  if ($amount !~ m{^\d+$}) {
    die "show_page: edit_participation_bad_amount.html\n";
  }
  my $note = $cgi->param("note");
  $note =~ s/^\s+//;
  $note =~ s/\s+$//;
  $note =~ s/\s+/ /;
  my @sql_column_list;
  my $db_charset = get_database_charset($instance_map, "main");
  if ($investor_participation) {
    push(@sql_column_list, ["amount", $amount, SQL_INTEGER]);
    push(@sql_column_list, ["note", $note || undef]);
    push(@sql_column_list, ["submission_dt", ["now()"]]);
    update_row($dbh, "investor_participation_tbl", \@sql_column_list, $investor_participation->{"id"});
  } else {
    push(@sql_column_list, ["investor_id", $investor->{"id"}, SQL_INTEGER]);
    push(@sql_column_list, ["investor_deal_id", $investor_deal->{"id"}, SQL_INTEGER]);
    push(@sql_column_list, ["amount", $amount, SQL_INTEGER]);
    if ($note) {
      push(@sql_column_list, ["note", $note]);
    }
    push(@sql_column_list, ["submission_dt", ["now()"]]);
    insert_row($dbh, "investor_participation_tbl", \@sql_column_list);
  }
  # temporarily turn off warnings about undefined values.
  local($^W) = 0;
  # send an email to administrator.
  my $email_message = <<"---END_BLOCK---;";
Deal Name: $investor_deal->{"name"}
Amount: $amount
Note: $note
---END_BLOCK---;
  # turn warnings back on.
  local($^W) = 1;
  enqueue_email
    ("dbh" => $dbh,
     "investor_id" => $investor->{"id"},
     "from_hdr" => "Micro Office investor notification <investor_relations\@microoffice.com>",
     "to_hdr" => "investor_relations\@microoffice.com",
     "subject_hdr" => qq{Investor participation update ($investor->{"investor_name"}, ID #$investor->{"id"})},
     "message_body" => $email_message,
    );
  # populate information needed for display of page.
  my $investor_deal_name = HTML::Entities::encode($investor_deal->{"name"});
  my $terms = HTML::Entities::encode($investor_deal->{"terms"});
  my $max_amount = HTML::Entities::encode($investor_deal->{"max_amount"} || "");
  my $offer_date = HTML::Entities::encode($investor_deal->{"offer_date"});
  my $deadline_date = HTML::Entities::encode($investor_deal->{"deadline_date"} || "");
  my $investor_deal_note = HTML::Entities::encode($investor_deal->{"note"} || "");
  my $nda_required = $investor_deal->{"nda_required"} ? 1 : 0;
  my $investor_id = $investor->{"id"};
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor_deal>
    <id>$investor_deal_id</id>
    <name>$investor_deal_name</name>
    <terms>$terms</terms>
    <max_amount>$max_amount</max_amount>
    <offer_date>$offer_date</offer_date>
    <deadline_date>$deadline_date</deadline_date>
    <note>$investor_deal_note</note>
    <nda_required>$nda_required</nda_required>
  </investor_deal>
  <investor_participation>
    <investor_id>$investor_id</investor_id>
    <investor_deal_id>$investor_deal_id</investor_deal_id>
    <amount>$amount</amount>
    <note>$note</note>
  </investor_participation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_participation_result.xslt", $cgi);
}


<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Edit Participation
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	</p>
	<p>
	  Thank you for your interest. The deal you viewed was:
	</p>
	<xsl:apply-templates select="investor_deal"/>
	<p>
	  You entered the following values for your participation interest:
	</p>
	<xsl:apply-templates select="investor_participation"/>
	<p>
	  Go to other <a href="/1g/deal/show_upcoming.cgi">upcoming deals</a> or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor_deal">
    <table>
      <tr>
	<td>Deal Name:</td>
	<td><xsl:value-of select="name"/></td>
      </tr>
      <tr>
	<td>Terms:</td>
	<td><xsl:value-of select="terms"/></td>
      </tr>
      <xsl:if test="deadline_date != ''">
	<tr>
	  <td>Deadline Date:</td>
	  <td><xsl:value-of select="deadline_date"/></td>
	</tr>
      </xsl:if>
      <xsl:if test="note != ''">
	<tr>
	  <td>Note:</td>
	  <td><xsl:value-of select="note"/></td>
	</tr>
      </xsl:if>
    </table>
  </xsl:template>

  <xsl:template match="investor_participation">
    <table>
      <tr>
	<td>Amount:</td>
	<td>$<xsl:value-of select="format-number(amount, '###,###,###')"/></td>
      </tr>
      <tr>
	<td>Note:</td>
	<td><xsl:value-of select="note"/></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

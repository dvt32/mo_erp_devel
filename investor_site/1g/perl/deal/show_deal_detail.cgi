#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use select_row_with_query;
use get_date_now;
use get_month_name;
use get_web_root_dir;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # get CGI parameters.
  my $investor_deal_id = $cgi->param("id");
  # load row.
  my $investor_deal = select_row($dbh, "investor_deal_tbl", $investor_deal_id);
  if (!$investor_deal) {
    die "show_page: show_deal_detail_not_found.html\n";
  }
  if (!$investor_deal->{"is_active"}) {
    die "show_page: show_deal_detail_not_active.html\n";
  }
  $investor_deal_id = $investor_deal->{"id"};
  my ($sql_query, @sql_param_list);
  $sql_query = "select * from investor_participation_tbl where investor_id = ? and investor_deal_id = ?";
  push(@sql_param_list, [$investor->{"id"}, SQL_INTEGER]);
  push(@sql_param_list, [$investor_deal_id, SQL_INTEGER]);
  my $investor_participation = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if ($investor_deal->{"nda_required"}) {
    if ((!$investor_participation) || (!$investor_participation->{"nda_signed_dt"})) {
      # NDA is required and not signed, so display it for signing now.
      present_nda($cgi, $investor, $investor_deal);
      return;
    }
  }
  # Either NDA not required or it has been signed, so OK to display details of the deal.

  # populate information needed for display of page.
  my $investor_deal_name = HTML::Entities::encode($investor_deal->{"name"});
  my $terms = HTML::Entities::encode($investor_deal->{"terms"});
  my $max_amount = HTML::Entities::encode($investor_deal->{"max_amount"} || "");
  my $offer_date = HTML::Entities::encode($investor_deal->{"offer_date"});
  my $deadline_date = HTML::Entities::encode($investor_deal->{"deadline_date"} || "");
  my $note = HTML::Entities::encode($investor_deal->{"note"} || "");
  my $nda_required = $investor_deal->{"nda_required"} ? 1 : 0;
  my $deal_packet_filename = get_web_root_dir() . "/resource/deal/$investor_deal_id.zip";
  my $deal_packet_exists = (-e $deal_packet_filename) ? 1 : 0;
  my $deal_packet_size = -s $deal_packet_filename;
  
  my $investor_participation_id = "";
  my $investor_id = $investor->{"id"};
  my $nda_signed_dt = "";
  my $nda_ip_address = "";
  my $amount = "";
  my $submission_dt = "";
  my $investor_participation_note = "";
  if ($investor_participation) {
    $investor_participation_id = $investor_participation->{"id"};
    $nda_signed_dt = $investor_participation->{"nda_signed_dt"};
    $nda_ip_address = $investor_participation->{"nda_ip_address"};
    $amount = $investor_participation->{"amount"};
    $submission_dt = $investor_participation->{"submission_dt"} || "";
    $investor_participation_note = HTML::Entities::encode($investor_participation->{"note"} || "");
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor_deal>
    <id>$investor_deal_id</id>
    <name>$investor_deal_name</name>
    <terms>$terms</terms>
    <max_amount>$max_amount</max_amount>
    <offer_date>$offer_date</offer_date>
    <deadline_date>$deadline_date</deadline_date>
    <note>$note</note>
    <nda_required>$nda_required</nda_required>
    <deal_packet_exists>$deal_packet_exists</deal_packet_exists>
    <deal_packet_size>$deal_packet_size</deal_packet_size>
  </investor_deal>
  <investor_participation>
    <id>$investor_participation_id</id>
    <investor_id>$investor_id</investor_id>
    <investor_deal_id>$investor_deal_id</investor_deal_id>
    <nda_signed_dt>$nda_signed_dt</nda_signed_dt>
    <nda_ip_address>$nda_ip_address</nda_ip_address>
    <amount>$amount</amount>
    <submission_dt>$submission_dt</submission_dt>
    <note>$investor_participation_note</note>
  </investor_participation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_deal_detail.xslt", $cgi);
}

sub present_nda {
  my ($cgi, $investor, $investor_deal) = @_;
  # NDA is required and not signed, so display it for signing now.
  my ($today_year, $today_month, $today_day) = get_date_now();
  $today_month = get_month_name($today_month);
  my $investor_deal_id = $investor_deal->{"id"};
  my $investor_deal_name = HTML::Entities::encode($investor_deal->{"name"});
  my $terms = HTML::Entities::encode($investor_deal->{"terms"});
  my $max_amount = HTML::Entities::encode($investor_deal->{"max_amount"} || "");
  my $offer_date = HTML::Entities::encode($investor_deal->{"offer_date"});
  my $deadline_date = HTML::Entities::encode($investor_deal->{"deadline_date"} || "");
  my $note = HTML::Entities::encode($investor_deal->{"note"} || "");
  my $nda_required = $investor_deal->{"nda_required"} ? 1 : 0;
  my $first_name = HTML::Entities::encode($investor->{"first_name"});
  my $last_name = HTML::Entities::encode($investor->{"last_name"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <month>$today_month</month>
    <day>$today_day</day>
    <year>$today_year</year>
  </today>
  <investor_deal>
    <id>$investor_deal_id</id>
    <name>$investor_deal_name</name>
    <terms>$terms</terms>
    <max_amount>$max_amount</max_amount>
    <offer_date>$offer_date</offer_date>
    <deadline_date>$deadline_date</deadline_date>
    <note>$note</note>
    <nda_required>$nda_required</nda_required>
  </investor_deal>
  <investor>
    <first_name>$first_name</first_name>
    <last_name>$last_name</last_name>
  </investor>
</page>
---END_BLOCK---;
  apply_xslt_output_xhtml($generated_xml, "sign_nda_input.xslt", $cgi);
}

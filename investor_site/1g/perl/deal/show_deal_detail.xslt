<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Deal Detail
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	</p>
	<xsl:if test="investor_participation/nda_signed_dt != ''">
	  <p>
	    You signed the nondisclosure agreement <xsl:value-of select="investor_participation/nda_signed_dt"/>.
	  </p>
	</xsl:if>
	<xsl:apply-templates select="investor_deal"/>
	<p>
	  <span class="inverse">Your Participation (nonbinding, and can be changed):</span><br/>
	  <form>
	    <xsl:attribute name="method">post</xsl:attribute>
	    <xsl:attribute name="action">/deal/edit_participation_result.cgi</xsl:attribute>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="investor_participation/id"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">investor_deal_id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="investor_participation/investor_deal_id"/></xsl:attribute>
	    </input>
	    <table>
	      <tr>
		<td>How much are you interested in buying?</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">amount</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="investor_participation/amount"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td>Notes/comments from you:</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">60</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="investor_participation/note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td>Last updated:</td>
		<td>
		  <xsl:choose>
		    <xsl:when test="investor_participation/submission_dt = ''">
		      Never
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="investor_participation/submission_dt"/>
		    </xsl:otherwise>
		  </xsl:choose>
		</td>
	      </tr>
	      <tr>
		<td></td>
		<td>
		  <input type="submit" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </form>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor_deal">
    <table>
      <tr>
	<td>Deal Name:</td>
	<td><xsl:value-of select="name"/></td>
      </tr>
      <tr>
	<td>Terms:</td>
	<td><xsl:value-of select="terms"/></td>
      </tr>
      <tr>
	<td>Total Funding Amount (across all investors):</td>
	<td>$<xsl:value-of select="format-number(max_amount, '###,###,###')"/></td>
      </tr>
      <xsl:if test="deadline_date != ''">
	<tr>
	  <td>Deadline Date:</td>
	  <td><xsl:value-of select="deadline_date"/></td>
	</tr>
      </xsl:if>
      <xsl:if test="note != ''">
	<tr>
	  <td>Note:</td>
	  <td><xsl:value-of select="note"/></td>
	</tr>
      </xsl:if>
      <xsl:if test="deal_packet_exists = 1">
	<tr>
	  <td>Information Packet:</td>
	  <td>
	    <a>
	      <xsl:attribute name="href">
		<xsl:text>/deal/download_deal_packet.cgi?id=</xsl:text>
		<xsl:value-of select="id"/>
	      </xsl:attribute>
	      <xsl:text>click to download due diligence and information packet</xsl:text>
	    </a>
	    <xsl:text> </xsl:text>
	    (<xsl:value-of select="format-number(deal_packet_size, '###,###,###')"/> bytes)
	  </td>
	</tr>
      </xsl:if>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

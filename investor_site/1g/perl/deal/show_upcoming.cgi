#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # fetch active deals under consideration now.
  my $sql_query = "select * from investor_deal_tbl where is_active order by offer_date";
  my (%investor_deal_by_id, @investor_deal_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%investor_deal_by_id, \@investor_deal_id_list);
  # populate information needed for display of page.
  my $first_name = HTML::Entities::encode($investor->{"first_name"});
  my $middle_name = HTML::Entities::encode($investor->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($investor->{"last_name"});
  my $investor_name = HTML::Entities::encode($investor->{"investor_name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor>
    <id>$investor->{"id"}</id>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <investor_name>$investor_name</investor_name>
  </investor>
  <investor_deal_list>
---END_BLOCK---;
  for my $investor_deal_id (@investor_deal_id_list) {
    my $investor_deal = $investor_deal_by_id{$investor_deal_id};
    my $investor_deal_name = HTML::Entities::encode($investor_deal->{"name"});
    my $terms = HTML::Entities::encode($investor_deal->{"terms"});
    my $max_amount = HTML::Entities::encode($investor_deal->{"max_amount"} || "");
    my $offer_date = HTML::Entities::encode($investor_deal->{"offer_date"});
    my $deadline_date = HTML::Entities::encode($investor_deal->{"deadline_date"} || "");
    my $note = HTML::Entities::encode($investor_deal->{"note"} || "");
    my $nda_required = $investor_deal->{"nda_required"} ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
    <investor_deal>
      <id>$investor_deal_id</id>
      <name>$investor_deal_name</name>
      <terms>$terms</terms>
      <max_amount>$max_amount</max_amount>
      <offer_date>$offer_date</offer_date>
      <deadline_date>$deadline_date</deadline_date>
      <note>$note</note>
      <nda_required>$nda_required</nda_required>
    </investor_deal>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </investor_deal_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_upcoming.xslt", $cgi);
}

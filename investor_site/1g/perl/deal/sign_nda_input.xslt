<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Nondisclosure Agreement
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	</p>
	<p>
	  The deal you requested requires a nondisclosure agreement. Please review the terms below and indicate your acceptance
	  by typing your first and last name, checking the box that says "I Agree", and clicking submit. Note that your IP
	  address and the date and time of your acceptance will be recorded.
	</p>
        <h1>
          Confidentiality Agreement - MOM as discloser
        </h1>
<p>
This Confidentiality Agreement (the "Agreement"), dated as of
<xsl:value-of select="today/month"/>
<xsl:text> </xsl:text>
<xsl:value-of select="today/day"/>,
<xsl:text> </xsl:text>
<xsl:value-of select="today/year"/>,
is made by and between Micro Office Management LLC ("MOM"), a limited liability company organized and existing under the laws
of the State of New York and doing business at 1375 Broadway, Floor 3, New York, NY 10018, and
<xsl:value-of select="investor/first_name"/>
<xsl:text> </xsl:text>
<xsl:value-of select="investor/last_name"/>
("Recipient").
</p>
<p>
MOM and Recipient are sometimes individually referred to herein as "Party" and collectively referred to herein as the "Parties".
</p>
<p>
In order to protect from disclosure the Confidential Information (as defined below) which MOM may make available to
Recipient in connection with
<xsl:value-of select="investor_deal/name"/>
and such other projects as the Parties might contemplate from time to time during the
Disclosure Period, as that term is defined below, (the "Project(s)"), and in consideration of the mutual covenants contained in
this Agreement, the Parties agree as follows:
</p>
<ol>
  <li>Definitions
  <ol type="A">
    <li>"Affiliates" means, with respect to each Party, any corporation, firm, partnership or other entity or person which directly or indirectly controls or is controlled by, or is under common control with that Party. For purposes of this definition, "control" (including with correlative meaning, the terms "controlled by" and "under common control with") shall be presumed to exist if one of the following conditions is met: (a) in the case of corporate entities, direct or indirect ownership of at least fifty percent (50%) of the stock or shares having the right to vote for the election of directors of such Party or any direct or indirect parent of such Party; and (b) in the case of non-corporate entities, direct or indirect ownership of at least fifty percent (50%) of the equity interest with the power to direct the management and policies of such non-corporate entities.</li>
    <li>"Confidential Information" means, other than Exempt Information (as defined below), all information disclosed to Recipient by or on behalf of MOM or learned or observed by the Recipient during the Disclosure Period (as defined below) relating to (i) MOM's business or business plans, including, but not limited to, suppliers, customers, prospective customers, contractors, cost and pricing data, software products, programming techniques, all proprietary information, know-how, trade secrets, technical and non-technical materials, products, specifications, processes, sales and marketing plans and strategies, designs, and any such information developed by Recipient for and on behalf of MOM, (ii) information of any third parties, and (iii) any discussions and proceedings relating to any of the foregoing information, whether disclosed in oral, electronic, visual, written or any other form. Confidential Information includes, without limitation, the terms and conditions of this Agreement. Confidential Information shall also include information of MOM and/or its Affiliates that MOM or its Affiliates would consider confidential or proprietary under the circumstances. The fact that MOM may have marked or identified as confidential or proprietary any specific information shall be indicative that MOM believes such information to be confidential or proprietary, but the failure to so mark information shall not conclusively determine that such information is or is not considered confidential information by MOM or its Affiliates.</li>
    <li>"Disclosure Period" means the period during which MOM may disclose Confidential Information to Recipient. The Disclosure Period shall commence on the Effective Date (as defined below), and shall expire two (2) years after such date, unless the Disclosure Period is either extended or terminated earlier in writing by the Parties, in which case the Disclosure Period shall expire on the date agreed by the Parties in such writing.</li>
    <li>"Effective Date" means <xsl:value-of select="today/month"/><xsl:text> </xsl:text>
 <xsl:value-of select="today/day"/>,<xsl:text> </xsl:text>
 <xsl:value-of select="today/year"/>.</li>
    <li>"Exempt Information" means that Confidential Information shall not include information which Recipient can demonstrate: (a) was lawfully in its possession and reduced to writing prior to the time of disclosure by or on behalf of MOM and which is not subject to another obligation of confidentiality; (b) is or becomes generally available to the public through no fault, omission, or other act of Recipient or any of its Affiliates; (c) is obtained from a third party lawfully entitled to possession of such Confidential Information and under no obligation of confidentiality to MOM or its Affiliates; or (d) was independently developed by or for the Recipient without reference to, aid from or reliance upon the Confidential Information of MOM and/or its Affiliates.</li>
    <li>"Permitted User" means an individual who: (a) in the case of a corporate Recipient, is a director, officer, consultant, contractor, agent or employee of Recipient or any of its Affiliates, or in the case of an individual Recipient, is the individual himself or herself; (b) is party to an effective confidentiality agreement with Recipient, which would protect the Confidential Information in a manner consistent with the terms of this Agreement; and (c) has a need to know the Confidential Information in connection with the Project.</li>
    <li>"Person" means any natural person, corporation, general or limited partnership, joint venture, proprietorship, trust, union, association, governmental authority or other entity.</li>
  </ol>
  </li>
  <li>Treatment of Confidential Information
  <ol type="A">
    <li>Recipient shall treat the Confidential Information as strictly confidential and proprietary. Recipient shall safeguard the confidential and proprietary nature of the Confidential Information with at least the same degree of care as it holds its own confidential or proprietary information, but in no event shall the degree of care for the Confidential Information be less than a reasonable degree of care.</li>
    <li>Recipient may use the Confidential Information only in connection with the applicable Project, and for no other purpose whatsoever. Recipient shall not use the Confidential Information for the personal benefit of itself or a Permitted User, or for the benefit of any third party.</li>
    <li>Recipient shall not disclose (directly or indirectly) any Confidential Information to, or permit it to be accessed by, any Person except a Permitted User. Recipient shall cause any Permitted User to whom Confidential Information is disclosed to abide by the confidentiality provisions of this Agreement. In the event Recipient becomes aware of any breach of the confidentiality and non-use obligations contained in this Section by it or any Permitted User, Recipient shall promptly notify MOM of such breach and all facts known to Recipient regarding same.</li>
    <li>If Recipient is requested to disclose the Confidential Information or the substance of this Agreement in connection with a legal or administrative proceeding or otherwise to comply with a requirement under applicable law, Recipient shall give MOM prompt notice of such request so that MOM may seek an appropriate protective order or other remedy, or waive compliance with the relevant provisions of this Agreement. If MOM seeks a protective order or other remedy, Recipient shall promptly cooperate with and reasonably assist MOM in such efforts. If MOM fails to obtain a protective order or waives compliance with the relevant provisions of this Agreement, Recipient shall disclose only that portion of Confidential Information which its legal counsel determines it is required to disclose.</li>
    <li>Upon the written request of MOM, Recipient shall promptly return or destroy, at MOM's option, all Confidential Information of MOM and/or its Affiliates (including all copies in whatever medium provided to, or made by, any Permitted User); provided, however, that, subject to the terms of this Agreement, Recipient shall be entitled to retain one archival copy of such Confidential Information for so long as necessary and for the sole purpose of determining its obligations under this Agreement. Notwithstanding anything to the contrary in this Agreement, Recipient shall not be required to destroy any computer files stored securely by Recipient that are created during automatic system back-up. Subject to Recipient's return or destruction of Confidential Information, Recipient shall continue to be bound by its obligations of confidentiality and non-use under this Agreement for a period of five (5) years from the end of the Disclosure Period.</li>
  </ol>
  </li>
  <li>General Provisions
  <ol type="A">
    <li>Each Party represents and warrants to the other Party that it has the legal power and authority to enter into this Agreement.</li>
    <li>Neither this Agreement, nor either Party's performance under it, will: (a) transfer to the Recipient, or create in the Recipient, any proprietary right, title, interest or claim in or to any of MOM's Confidential Information; (b) obligate either Party to enter into any other agreement or undertaking of any nature whatsoever with the other Party; (c) prohibit either Party from entering into any other agreement with any other party, if doing so will not violate such Party's obligations hereunder; or (d) be construed as granting a license to its Confidential Information to the Recipient.</li>
    <li>This Agreement sets forth the entire understanding between the parties as to its subject matter, and supersedes all prior agreements and understanding relating to such subject matter. This Agreement may only be modified in a writing signed by a duly authorized agent for each respective Party and specifically referring hereto.</li>
    <li>This Agreement shall be governed by, and construed and enforced in accordance with the laws of the State of New York, excluding its conflict of law rules.</li>
    <li>The Parties acknowledge that, except as expressly set forth herein: (a) neither Party has made any representation, warranty or promise to the other, express or implied, upon which either is entitled to rely in any way; and (b) the Parties specifically waive and disclaim any reliance, dependence or action based on any written or verbal statement or promise made by either Party to the other.</li>
    <li>Neither the rights nor the obligations of either Party hereunder may be assigned or delegated, in whole or in part, without the prior written consent of the other Party. Any such assignment or delegation without the prior written consent of the other Party shall be null and void and of no effect.</li>
    <li>This Agreement may be executed in one or more counterparts, each of which shall be deemed an original, but all of which together shall constitute one and the same instrument.</li>
    <li>If any term of this Agreement or the application thereof shall be deemed invalid or unenforceable, the remainder of this Agreement shall be unaffected thereby and each remaining term of this Agreement shall be valid and enforced to the fullest extent permitted by law.</li>
    <li>The failure of either Party to insist upon the strict observation or performance of any provision of this Agreement, or to exercise any right or remedy shall not impair or waive any such right or remedy in the future. Every right and remedy given by this Agreement to the Parties may be exercised from time to time as often as appropriate.</li>
    <li>All notices given hereunder shall be in writing and shall be sent to the Parties hereto at the addresses set forth above or to such other address as a Party may provide.</li>
  </ol>
  </li>
</ol>
<p>
IN WITNESS WHEREOF, this Agreement has been duly executed and delivered as of the Effective Date.
</p>
<p>
<strong><em>Note: before clicking Proceed, print a copy of this page for your records.</em></strong>
</p>
<p>
Recipient: <xsl:value-of select="investor/first_name"/>
<xsl:text> </xsl:text>
<xsl:value-of select="investor/last_name"/><br/>
<form>
  <xsl:attribute name="method">post</xsl:attribute>
  <xsl:attribute name="action">/deal/sign_nda_result.cgi</xsl:attribute>
<input>
  <xsl:attribute name="type">hidden</xsl:attribute>
  <xsl:attribute name="name">investor_deal_id</xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="investor_deal/id"/></xsl:attribute>
</input>
Type your first and last name as your signature: <input type="text" name="investor_name"/><br/>
<input type="checkbox" name="i_agree"/>I Agree<br/>
<input type="submit" value="Proceed"/>
</form>
</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

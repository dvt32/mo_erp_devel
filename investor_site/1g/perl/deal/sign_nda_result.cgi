#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use select_row_with_query;
use insert_row;
use update_row;
use get_date_now;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # get CGI parameters.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $investor_deal_id = $cgi->param("investor_deal_id");
  my $investor_name = Encode::decode($cgi_input_charset, $cgi->param("investor_name"));
  $investor_name =~ s/^\s+//;
  $investor_name =~ s/\s+$//;
  $investor_name =~ s/\s+/ /;
  my $i_agree = $cgi->param("i_agree");
  # check that form was filled out.
  if ($investor_name ne $investor->{"first_name"} . " " . $investor->{"last_name"}) {
    die "show_page: sign_nda_signature_mismatch.html\n";
  }
  if (!$i_agree) {
    die "show_page: sign_nda_did_not_agree.html\n";
  }
  # load investor deal row.
  my $investor_deal = select_row($dbh, "investor_deal_tbl", $investor_deal_id);
  if (!$investor_deal) {
    die "show_page: sign_nda_deal_not_found.html\n";
  }
  if (!$investor_deal->{"is_active"}) {
    die "show_page: sign_nda_deal_not_active.html\n";
  }
  if (!$investor_deal->{"nda_required"}) {
    die "show_page: sign_nda_not_required.html\n";
  }
  my ($sql_query, @sql_param_list);
  $sql_query = "select * from investor_participation_tbl where investor_id = ? and investor_deal_id = ?";
  push(@sql_param_list, [$investor->{"id"}, SQL_INTEGER]);
  push(@sql_param_list, [$investor_deal_id, SQL_INTEGER]);
  my $investor_participation = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if (($investor_participation) && ($investor_participation->{"nda_signed_dt"})) {
    # already signed NDA, so display error.
    die "show_page: sign_nda_already_signed.html\n";
  }
  my @sql_column_list;
  my $db_charset = get_database_charset($instance_map, "main");
  my $remote_client_ip_address = $cgi->remote_addr();
  if ($investor_participation) {
    push(@sql_column_list, ["nda_signed_dt", ["now()"]]);
    push(@sql_column_list, ["nda_ip_address", $remote_client_ip_address]);
    update_row($dbh, "investor_participation_tbl", \@sql_column_list, $investor_participation->{"id"});
  } else {
    push(@sql_column_list, ["investor_id", $investor->{"id"}, SQL_INTEGER]);
    push(@sql_column_list, ["investor_deal_id", $investor_deal->{"id"}, SQL_INTEGER]);
    push(@sql_column_list, ["nda_signed_dt", ["now()"]]);
    push(@sql_column_list, ["nda_ip_address", $remote_client_ip_address]);
    insert_row($dbh, "investor_participation_tbl", \@sql_column_list);
  }
  # populate information needed for display of page.
  my ($today_year, $today_month, $today_day) = get_date_now();
  $today_month = get_month_name($today_month);
  my $investor_deal_name = HTML::Entities::encode($investor_deal->{"name"});
  my $terms = HTML::Entities::encode($investor_deal->{"terms"});
  my $max_amount = HTML::Entities::encode($investor_deal->{"max_amount"} || "");
  my $offer_date = HTML::Entities::encode($investor_deal->{"offer_date"});
  my $deadline_date = HTML::Entities::encode($investor_deal->{"deadline_date"} || "");
  my $note = HTML::Entities::encode($investor_deal->{"note"} || "");
  my $nda_required = $investor_deal->{"nda_required"} ? 1 : 0;
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <month>$today_month</month>
    <day>$today_day</day>
    <year>$today_year</year>
  </today>
  <ip_address>$remote_client_ip_address</ip_address>
  <investor_deal>
    <id>$investor_deal_id</id>
    <name>$investor_deal_name</name>
    <terms>$terms</terms>
    <max_amount>$max_amount</max_amount>
    <offer_date>$offer_date</offer_date>
    <deadline_date>$deadline_date</deadline_date>
    <note>$note</note>
    <nda_required>$nda_required</nda_required>
  </investor_deal>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "sign_nda_result.xslt", $cgi);
}


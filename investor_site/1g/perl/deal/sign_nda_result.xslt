<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Sign NDA
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	</p>
<p>
Thank you. You signed the NDA for
<xsl:value-of select="investor_deal/name"/>
on
<xsl:value-of select="today/month"/>
<xsl:text> </xsl:text>
<xsl:value-of select="today/day"/>,
<xsl:text> </xsl:text>
<xsl:value-of select="today/year"/>
from IP address
<xsl:value-of select="ip_address"/>.
</p>
<p>
You may proceed now to the
<a>
  <xsl:attribute name="href">/deal/show_deal_detail.cgi?id=<xsl:value-of select="investor_deal/id"/></xsl:attribute>
  deal detail page for 
  <xsl:value-of select="investor_deal/name"/>
</a>.
</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

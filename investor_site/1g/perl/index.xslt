<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>Micro Office Solutions :: Investor Center</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css" />
	<link rel="icon" type="image/x-icon" href="/1g/favicon.ico" />
	<link rel="shortcut icon" type="image/x-icon" href="/1g/favicon.ico" />
      </head>
      <body>
	<p><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
	<p>Welcome, <xsl:value-of select="investor/name"/>. Here are your menu options:</p>
	<ul>
	  <li>
	    Online Security:
	    <a href="/1g/auth/logout.cgi">Log Out</a>
	    &middot;
	    <a href="/1g/auth/change_password_input.html">Change Password</a>
	  </li>
	  <li>
	    Profile:
	    <a href="/1g/profile/edit_profile_input.cgi">Edit Profile</a>
	    &middot;
	    <a href="/1g/profile/show_bank_accounts.cgi">Bank Accounts</a>
	  </li>
	  <li>
	    <a href="/1g/payment/show_payments.cgi">Payment History</a>
	  </li>
	  <li>
	    <a href="/1g/tax/show_document_list.cgi">Tax Documents</a>
	  </li>
	  <li>
	    <a href="/1g/deal/show_upcoming.cgi">Upcoming Deals</a>
	  </li>
	</ul>
	<p>Have suggestions or comments? Send feedback to <a href="mailto:investor_relations@microoffice.com">investor_relations@microoffice.com</a>.</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use IO::File;
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_web_root_dir;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  my $company_id = $cgi->param("id");
  if (!$company_id) {
    die "show_page: download_debt_registry_missing_company_id.html\n";
  }
  my $company = select_row($dbh, "company_tbl", $company_id);
  my $spreadsheet_filename = get_web_root_dir() . "/resource/debt_registry/$company_id.xls";
  if ((!$company) || (!-e $spreadsheet_filename)) {
    warn "download_debt_registry.cgi: company_id ($company_id) filename ($spreadsheet_filename)";
    die "show_page: download_debt_registry_not_found.html\n";
  }
  my $company_short_name = lc($company->{"short_name"});
  my $fh = IO::File->new();
  $fh->open($spreadsheet_filename);
  local($/) = undef;
  my $spreadsheet = <$fh>;
  $fh->close();
  # print out data to stdout
  print $cgi->header
    ("-type" => "application/vnd.ms-excel",
     "-content-disposition" => "attachment; filename=debt_registry_$company_short_name.xls");
  print $spreadsheet;
}

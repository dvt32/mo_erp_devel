#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  my $sql_query = "select * from investor_payment_tbl where investor_id = ? order by txn_date, reference_number";
  my @sql_param_list = ([$investor->{"id"}, SQL_INTEGER]);
  my @investor_payment_list;
  get_row_list($dbh, $sql_query, \@sql_param_list, \@investor_payment_list);
  # populate information needed for display of page.
  my $first_name = HTML::Entities::encode($investor->{"first_name"});
  my $middle_name = HTML::Entities::encode($investor->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($investor->{"last_name"});
  my $investor_name = HTML::Entities::encode($investor->{"investor_name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor>
    <id>$investor->{"id"}</id>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <investor_name>$investor_name</investor_name>
  </investor>
  <investor-payment-list>
---END_BLOCK---;
  for my $investor_payment (@investor_payment_list) {
    my $amount = $investor_payment->{"amount"};
    my $payment_method = $investor_payment->{"payment_method"};
    my $payment_status = $investor_payment->{"payment_status"};
    my $txn_date = $investor_payment->{"txn_date"};
    my $reference_number = HTML::Entities::encode($investor_payment->{"reference_number"} || "");
    my $note = HTML::Entities::encode($investor_payment->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <investor-payment>
      <amount>$amount</amount>
      <payment_method>$payment_method</payment_method>
      <payment_status>$payment_status</payment_status>
      <txn_date>$txn_date</txn_date>
      <reference_number>$reference_number</reference_number>
      <note>$note</note>
    </investor-payment>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </investor-payment-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_payments.xslt", $cgi);
}

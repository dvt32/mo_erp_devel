<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Show Payment History
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	</p>
	<xsl:apply-templates select="investor"/>
	<xsl:apply-templates select="investor-payment-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor">
    <p>
      Welcome, <xsl:value-of select="investor_name"/>. Here are the payments made to you:
    </p>
  </xsl:template>

  <xsl:template match="investor-payment-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Ref. Num.</td>
	<td class="standard_table_spacer_column"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Status</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="investor-payment"/>
      <tr class="standard_table_footer_row">
	<td class="standard_table_side_padding"></td>
	<td></td>
	<td class="standard_table_spacer_column"></td>
	<td style="text-align: right">Total:</td>
	<td class="standard_table_spacer_column"></td>
	<td>$<xsl:value-of select="format-number(sum(investor-payment/amount), '0.00')"/></td>
	<td class="standard_table_spacer_column"></td>
	<td></td>
	<td class="standard_table_spacer_column"></td>
	<td></td>
	<td class="standard_table_side_padding"></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="investor-payment">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td><xsl:value-of select="txn_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:if test="reference_number != ''">
	  <xsl:value-of select="payment_method"/><xsl:text> #</xsl:text><xsl:value-of select="reference_number"/>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td style="text-align: right">$<xsl:value-of select="format-number(amount, '0.00')"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="payment_status"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

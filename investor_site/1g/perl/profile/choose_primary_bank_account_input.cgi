#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  my $sql_query = "select * from investor_bank_account_tbl where investor_id = ? order by routing_number, account_number";
  my @sql_param_list = ([$investor->{"id"}, SQL_INTEGER]);
  my (%investor_bank_account_by_id, @investor_bank_account_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%investor_bank_account_by_id, \@investor_bank_account_id_list);
  # populate information needed for display of page.
  my $primary_bank_account_id = $investor->{"primary_bank_account_id"} || "";
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <primary_bank_account_id>$primary_bank_account_id</primary_bank_account_id>
  <investor_bank_account_list>
---END_BLOCK---;
  for my $investor_bank_account_id (@investor_bank_account_id_list) {
    my $investor_bank_account = $investor_bank_account_by_id{$investor_bank_account_id};
    my $depository_name = HTML::Entities::encode($investor_bank_account->{"depository_name"});
    my $routing_number = HTML::Entities::encode($investor_bank_account->{"routing_number"});
    my $account_number = HTML::Entities::encode($investor_bank_account->{"account_number"});
    my $account_nickname = HTML::Entities::encode($investor_bank_account->{"account_nickname"} || "");
    my $note = HTML::Entities::encode($investor_bank_account->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <investor_bank_account>
      <id>$investor_bank_account_id</id>
      <depository_name>$depository_name</depository_name>
      <routing_number>$routing_number</routing_number>
      <account_number>$account_number</account_number>
      <account_nickname>$account_nickname</account_nickname>
      <note>$note</note>
    </investor_bank_account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </investor_bank_account_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "choose_primary_bank_account_input.xslt", $cgi);
}

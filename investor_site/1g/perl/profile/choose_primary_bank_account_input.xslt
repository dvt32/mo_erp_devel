<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Choose Primary Bank Account
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/profile/edit_profile_input.cgi">Edit Profile</a>
	  |
	  <a href="/1g/profile/show_bank_accounts.cgi">Bank Accounts</a>
	</p>
	<xsl:apply-templates select="investor_bank_account_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor_bank_account_list">
    <table>
      <tr><td>
	<form action="/1g/profile/choose_primary_bank_account_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Choose Primary Bank Account
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Primary Bank Account:
		</td>
		<td>
		  <select name="id">
		    <option value="">[---]</option>
		    <xsl:for-each select="investor_bank_account">
		      <option>
			<xsl:if test="/page/primary_bank_account_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="account_nickname"/>
			(<xsl:value-of select="depository_name"/>
			routing num <xsl:value-of select="routing_number"/>
			account num <xsl:value-of select="account_number"/>)
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # get form input and validate it.
  my $primary_bank_account_id = $cgi->param("id");
  if (!$primary_bank_account_id) {
    die "show_page: choose_primary_bank_account_missing_selection.html\n";
  }
  # update row.
  $dbh->begin_work();
  my $investor_bank_account = select_row($dbh, "investor_bank_account_tbl", $primary_bank_account_id);
  if ((!$investor_bank_account) || ($investor_bank_account->{"investor_id"} != $investor->{"id"})) {
    $dbh->rollback();
    die "show_page: choose_primary_bank_account_not_found.html\n";
  }
  my @sql_column_list;
  push(@sql_column_list, ["primary_bank_account_id", $primary_bank_account_id, SQL_INTEGER]);
  update_row($dbh, "investor_tbl", \@sql_column_list, $investor->{"id"});
  $dbh->commit();
  # populate information needed for display of page.
  my $depository_name = HTML::Entities::encode($investor_bank_account->{"depository_name"});
  my $account_type = HTML::Entities::encode($investor_bank_account->{"account_type"});
  my $routing_number = HTML::Entities::encode($investor_bank_account->{"routing_number"});
  my $account_number = HTML::Entities::encode($investor_bank_account->{"account_number"});
  my $account_nickname = HTML::Entities::encode($investor_bank_account->{"account_nickname"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <bank_account>
    <depository_name>$depository_name</depository_name>
    <account_type>$account_type</account_type>
    <routing_number>$routing_number</routing_number>
    <account_number>$account_number</account_number>
    <account_nickname>$account_nickname</account_nickname>
  </bank_account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "choose_primary_bank_account_result.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Choose Primary Bank Account (success)
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/profile/edit_profile_input.cgi">Edit Profile</a>
	  |
	  <a href="/1g/profile/show_bank_accounts.cgi">Bank Accounts</a>
	</p>
	<xsl:apply-templates select="bank_account"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="bank_account">
    <p>
      You successfully chose the following bank account as your primary bank account:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Depository Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="depository_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Routing Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="routing_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account Nickname:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_nickname"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # get form input and validate it.
  my $investor_bank_account_id = $cgi->param("id");
  if (!$investor_bank_account_id) {
    die "show_page: delete_bank_account_not_found.html\n";
  }
  # delete row.
  $dbh->begin_work();
  my $investor_bank_account = select_row($dbh, "investor_bank_account_tbl", $investor_bank_account_id);
  if ((!$investor_bank_account) || ($investor_bank_account->{"investor_id"} != $investor->{"id"})) {
    $dbh->rollback();
    die "show_page: delete_bank_account_not_found.html\n";
  }
  delete_row($dbh, "investor_bank_account_tbl", $investor_bank_account_id);
  $dbh->commit();
  # populate information needed for display of page.
  my $depository_name = HTML::Entities::encode($investor_bank_account->{"depository_name"});
  my $address_1 = HTML::Entities::encode($investor_bank_account->{"address_1"} || "");
  my $address_2 = HTML::Entities::encode($investor_bank_account->{"address_2"} || "");
  my $city = HTML::Entities::encode($investor_bank_account->{"city"} || "");
  my $state = HTML::Entities::encode($investor_bank_account->{"state"} || "");
  my $zip_code = HTML::Entities::encode($investor_bank_account->{"zip_code"} || "");
  my $country = HTML::Entities::encode($investor_bank_account->{"country"} || "");
  my $account_type = HTML::Entities::encode($investor_bank_account->{"account_type"});
  my $routing_number = HTML::Entities::encode($investor_bank_account->{"routing_number"});
  my $account_number = HTML::Entities::encode($investor_bank_account->{"account_number"});
  my $owner_name = HTML::Entities::encode($investor_bank_account->{"owner_name"});
  my $owner_type = HTML::Entities::encode($investor_bank_account->{"owner_type"});
  my $account_nickname = HTML::Entities::encode($investor_bank_account->{"account_nickname"} || "");
  my $note = HTML::Entities::encode($investor_bank_account->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <bank_account>
    <depository_name>$depository_name</depository_name>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <account_type>$account_type</account_type>
    <routing_number>$routing_number</routing_number>
    <account_number>$account_number</account_number>
    <owner_name>$owner_name</owner_name>
    <owner_type>$owner_type</owner_type>
    <account_nickname>$account_nickname</account_nickname>
    <note>$note</note>
  </bank_account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_bank_account.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # get CGI parameters.
  my $investor_bank_account_id = $cgi->param("id");
  # load row.
  my $investor_bank_account = select_row($dbh, "investor_bank_account_tbl", $investor_bank_account_id);
  if ((!$investor_bank_account) || ($investor_bank_account->{"investor_id"} != $investor->{"id"})) {
    die "show_page: edit_bank_account_not_found.html\n";
  }
  # load states.
  my @state_list;
  my $sql_query = "select * from state_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@state_list);
  # load countries.
  my @country_list;
  $sql_query = "select * from country_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@country_list);
  # populate information needed for display of page.
  my $depository_name = HTML::Entities::encode($investor_bank_account->{"depository_name"});
  my $address_1 = HTML::Entities::encode($investor_bank_account->{"address_1"} || "");
  my $address_2 = HTML::Entities::encode($investor_bank_account->{"address_2"} || "");
  my $city = HTML::Entities::encode($investor_bank_account->{"city"} || "");
  my $state = HTML::Entities::encode($investor_bank_account->{"state"} || "");
  my $zip_code = HTML::Entities::encode($investor_bank_account->{"zip_code"} || "");
  my $country = HTML::Entities::encode($investor_bank_account->{"country"} || "");
  my $account_type = HTML::Entities::encode($investor_bank_account->{"account_type"});
  my $routing_number = HTML::Entities::encode($investor_bank_account->{"routing_number"});
  my $account_number = HTML::Entities::encode($investor_bank_account->{"account_number"});
  my $owner_name = HTML::Entities::encode($investor_bank_account->{"owner_name"});
  my $owner_type = HTML::Entities::encode($investor_bank_account->{"owner_type"});
  my $account_nickname = HTML::Entities::encode($investor_bank_account->{"account_nickname"} || "");
  my $note = HTML::Entities::encode($investor_bank_account->{"note"} || "");
  # generate derived values.
  my $state_other = "";
  if (!grep {$_->{"name"} eq $state} @state_list) {
    $state_other = $state;
    $state = "";
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor_bank_account>
    <id>$investor_bank_account_id</id>
    <depository_name>$depository_name</depository_name>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <state_other>$state_other</state_other>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <account_type>$account_type</account_type>
    <routing_number>$routing_number</routing_number>
    <account_number>$account_number</account_number>
    <owner_name>$owner_name</owner_name>
    <owner_type>$owner_type</owner_type>
    <account_nickname>$account_nickname</account_nickname>
    <note>$note</note>
  </investor_bank_account>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <account_type_list>
    <account_type>checking</account_type>
    <account_type>savings</account_type>
  </account_type_list>
  <owner_type_list>
    <owner_type>personal</owner_type>
    <owner_type>business</owner_type>
  </owner_type_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_bank_account_input.xslt", $cgi);
}

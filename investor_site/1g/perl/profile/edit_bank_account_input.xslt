<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Edit Bank Account
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/profile/edit_profile_input.cgi">Edit Profile</a>
	  |
	  <a href="/1g/profile/show_bank_accounts.cgi">Bank Accounts</a>
	</p>
	<xsl:apply-templates select="investor_bank_account"/>
	<p>
	  Here are some locations where bank routing numbers and bank account numbers usually appear on checks:
	</p>
	<p>
	  <img src="/1g/img/check_bottom_one.gif" alt="routing number and account number example 1" style="vertical-align: middle"/>
	  <span style="vertical-align: middle"> - or - </span>
	  <img src="/1g/img/check_bottom_two.gif" alt="routing number and account number example 2" style="vertical-align: middle"/>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor_bank_account">
    <table>
      <tr><td>
	<form action="/1g/profile/edit_bank_account_result.cgi" method="post">
	  <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  </input>
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Bank Account
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Depository Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">depository_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="depository_name"/></xsl:attribute>
		  </input>
		  (name of your bank)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Address 1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">address_1</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="address_1"/></xsl:attribute>
		  </input>
		  (address of your bank)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Address 2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">address_2</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="address_2"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  City:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">city</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="city"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  State:
		</td>
		<td>
		  <select name="state">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/state-list/state">
		      <option>
			<xsl:if test="/page/investor_bank_account/state = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  or Province/Territory:
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">state_other</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="state_other"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Zip Code/Postal Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">zip_code</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="zip_code"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Country:
		</td>
		<td>
		  <select name="country">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/country-list/country">
		      <option>
			<xsl:if test="/page/investor_bank_account/country = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  (note only United States supported at present)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account Type:
		</td>
		<td>
		  <select name="account_type">
		    <xsl:for-each select="/page/account_type_list/account_type">
		      <option>
			<xsl:if test="/page/investor_bank_account/account_type = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Routing Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">routing_number</xsl:attribute>
		    <xsl:attribute name="size">9</xsl:attribute>
		    <xsl:attribute name="maxlength">9</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="routing_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">account_number</xsl:attribute>
		    <xsl:attribute name="size">17</xsl:attribute>
		    <xsl:attribute name="maxlength">17</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="account_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Owner Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">owner_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="owner_name"/></xsl:attribute>
		  </input>
		  (beneficiary of the account, either your name or perhaps a company name)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Owner Type:
		</td>
		<td>
		  <select name="owner_type">
		    <xsl:for-each select="/page/owner_type_list/owner_type">
		      <option>
			<xsl:if test="/page/investor_bank_account/owner_type = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  (indicates whether bank account is owned by you personally or a business)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account Nickname:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">account_nickname</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="account_nickname"/></xsl:attribute>
		  </input>
		  (short nickname you will later use to refer to this account)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		  If you like, you may alternatively
		  <a>
		    <xsl:attribute name="href">/profile/delete_bank_account.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
		    <xsl:text>Delete Bank Account</xsl:text>
		  </a>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;
use get_row_list;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # load states.
  my @state_list;
  my $sql_query = "select * from state_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@state_list);
  # load countries.
  my @country_list;
  $sql_query = "select * from country_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@country_list);
  # populate information needed for display of page.
  my $first_name = HTML::Entities::encode($investor->{"first_name"});
  my $middle_name = HTML::Entities::encode($investor->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($investor->{"last_name"});
  my $investor_name = HTML::Entities::encode($investor->{"investor_name"});
  my $address_1 = HTML::Entities::encode($investor->{"address_1"});
  my $address_2 = HTML::Entities::encode($investor->{"address_2"} || "");
  my $city = HTML::Entities::encode($investor->{"city"});
  my $state = HTML::Entities::encode($investor->{"state"});
  my $zip_code = HTML::Entities::encode($investor->{"zip_code"});
  my $country = HTML::Entities::encode($investor->{"country"} || "");
  my $work_phone_number = HTML::Entities::encode($investor->{"work_phone_number"} || "");
  my $work_fax_number = HTML::Entities::encode($investor->{"work_fax_number"} || "");
  my $home_phone_number = HTML::Entities::encode($investor->{"home_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($investor->{"cell_phone_number"} || "");
  my $email = HTML::Entities::encode($investor->{"email"});
  # generate derived values.
  my $state_other = "";
  if (!grep {$_->{"name"} eq $state} @state_list) {
    $state_other = $state;
    $state = "";
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor>
    <id>$investor->{"id"}</id>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <investor_name>$investor_name</investor_name>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <state_other>$state_other</state_other>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <work_phone_number>$work_phone_number</work_phone_number>
    <work_fax_number>$work_fax_number</work_fax_number>
    <home_phone_number>$home_phone_number</home_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <email>$email</email>
  </investor>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_profile_input.xslt", $cgi);
}

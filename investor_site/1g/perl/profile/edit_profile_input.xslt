<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Edit Profile
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/profile/show_bank_accounts.cgi">Bank Accounts</a>
	  |
	  <a href="/1g/profile/new_bank_account_input.cgi">New Bank Account</a>
	</p>
	<xsl:apply-templates select="investor"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor">
    <table>
      <tr><td>
	<form action="/1g/profile/edit_profile_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Profile
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  First Name:
		</td>
		<td>
		  <xsl:value-of select="first_name"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Middle Name:
		</td>
		<td>
		  <xsl:value-of select="middle_name"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Last Name:
		</td>
		<td>
		  <xsl:value-of select="last_name"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Registered Name:
		</td>
		<td>
		  <xsl:value-of select="investor_name"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Address 1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">address_1</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="address_1"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Address 2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">address_2</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="address_2"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  City:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">city</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="city"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  State:
		</td>
		<td>
		  <select name="state">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/state-list/state">
		      <option>
			<xsl:if test="/page/investor/state = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  or Province/Territory:
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">state_other</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="state_other"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Zip Code/Postal Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">zip_code</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="zip_code"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Country:
		</td>
		<td>
		  <select name="country">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/country-list/country">
		      <option>
			<xsl:if test="/page/investor/country = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Work Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">work_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="work_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Fax:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">work_fax_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="work_fax_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Home Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">home_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="home_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Cell Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">cell_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="cell_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email*:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="email"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  * note that changing email will also change your login
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use is_valid_email;
use select_row;
use select_row_with_query;
use update_row;
use strip_outside_whitespace;
use enqueue_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that address_1 was specified.
  my $address_1 = Encode::decode($cgi_input_charset, $cgi->param("address_1"));
  if ((!$address_1) || ($address_1 =~ /^\s*$/)) {
    die "show_page: edit_profile_missing_address.html\n";
  }
  $address_1 = strip_outside_whitespace($address_1);
  # verify that city was specified.
  my $city = Encode::decode($cgi_input_charset, $cgi->param("city"));
  if ((!$city) || ($city =~ /^\s*$/)) {
    die "show_page: edit_profile_missing_address.html\n";
  }
  $city = strip_outside_whitespace($city);
  # verify that state or state_other was specified.
  my $state = Encode::decode($cgi_input_charset, $cgi->param("state"));
  my $state_other = Encode::decode($cgi_input_charset, $cgi->param("state_other"));
  if (((!$state) || ($state =~ /^\s*$/)) &&
      ((!$state_other) || ($state_other =~ /^\s*$/))) {
    die "show_page: edit_profile_missing_address.html\n";
  }
  if ($state_other) {
    $state = $state_other;
    $state_other = "";
  }
  $state = strip_outside_whitespace($state);
  # verify that zip code was specified.
  my $zip_code = Encode::decode($cgi_input_charset, $cgi->param("zip_code"));
  if ((!$zip_code) || ($zip_code =~ /^\s*$/)) {
    die "show_page: edit_profile_missing_address.html\n";
  }
  $zip_code = strip_outside_whitespace($zip_code);
  # verify that country was specified.
  my $country = Encode::decode($cgi_input_charset, $cgi->param("country"));
  if ((!$country) || ($country =~ /^\s*$/)) {
    die "show_page: edit_profile_missing_address.html\n";
  }
  $country = strip_outside_whitespace($country);
  # verify that email was specified and is the proper format.
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email"));
  if ((!$email) || ($email =~ /^\s*$/)) {
    die "show_page: edit_profile_missing_email.html\n";
  } elsif (!is_valid_email($email)) {
    die "show_page: edit_profile_bad_email.html\n";
  }
  # strip surrounding whitespace and make email lowercase to avoid case issues.
  $email = lc(strip_outside_whitespace($email));
  # get the remaining form fields.
  my $address_2 = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("address_2")) || "");
  my $work_phone_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("work_phone_number")) || "");
  my $work_fax_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("work_fax_number")) || "");
  my $home_phone_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("home_phone_number")) || "");
  my $cell_phone_number = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("cell_phone_number")) || "");
  # begin transaction of updating the rows.
  $dbh->begin_work();
  # wrap in an eval to catch exceptions.
  eval {
    # raise an exception if there is a database error (such as transaction error).
    local $dbh->{"RaiseError"} = 1;
    # if new email differs from old email, check that new email is not a duplicate of somebody else's email.
    if ($investor->{"email"} ne $email) {
      my $sql_query = "select * from investor_tbl where email = ?";
      my @sql_param_list = ($email);
      if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
	die "duplicate_email\n";
      }
    }
    # issue the update command to the database.
    my $db_charset = get_database_charset($instance_map, "main");
    my (@sql_column_list);
    push(@sql_column_list, ["address_1", Encode::encode($db_charset, $address_1)]);
    push(@sql_column_list, ["address_2", Encode::encode($db_charset, $address_2)]);
    push(@sql_column_list, ["city", Encode::encode($db_charset, $city)]);
    push(@sql_column_list, ["state", Encode::encode($db_charset, $state)]);
    push(@sql_column_list, ["zip_code", Encode::encode($db_charset, $zip_code)]);
    push(@sql_column_list, ["country", Encode::encode($db_charset, $country)]);
    push(@sql_column_list, ["work_phone_number", Encode::encode($db_charset, $work_phone_number)]);
    push(@sql_column_list, ["work_fax_number", Encode::encode($db_charset, $work_fax_number)]);
    push(@sql_column_list, ["home_phone_number", Encode::encode($db_charset, $home_phone_number)]);
    push(@sql_column_list, ["cell_phone_number", Encode::encode($db_charset, $cell_phone_number)]);
    push(@sql_column_list, ["email", Encode::encode($db_charset, $email)]);
    update_row($dbh, "investor_tbl", \@sql_column_list, $investor->{"id"});
    # temporarily turn off warnings about undefined values.
    local($^W) = 0;
    # send an email to administrator.
    my $email_message = <<"---END_BLOCK---;";
==Old investor profile==
Address 1: $investor->{"address_1"}
Address 2: $investor->{"address_2"}
City: $investor->{"city"}
State: $investor->{"state"}
Zip Code: $investor->{"zip_code"}
Work Phone: $investor->{"work_phone_number"}
Work Fax: $investor->{"work_fax_number"}
Home Phone: $investor->{"home_phone_number"}
Cell Phone: $investor->{"cell_phone_number"}
Email: $investor->{"email"}

==New investor profile==
Address 1: $address_1
Address 2: $address_2
City: $city
State: $state
Zip Code: $zip_code
Work Phone: $work_phone_number
Work Fax: $work_fax_number
Home Phone: $home_phone_number
Cell Phone: $cell_phone_number
Email: $email
---END_BLOCK---;
    # turn warnings back on.
    local($^W) = 1;
    enqueue_email
      ("dbh" => $dbh,
       "investor_id" => $investor->{"id"},
       "from_hdr" => "Micro Office investor notification <investor_relations\@microoffice.com>",
       "to_hdr" => "investor_relations\@microoffice.com",
       "subject_hdr" => qq{Investor profile update ($investor->{"investor_name"}, ID #$investor->{"id"})},
       "message_body" => $email_message,
      );
    # commit the transaction.
    $dbh->commit();
  };
  my $transaction_error = $@;
  if ($transaction_error) {
    # remove trailing newline from exception message.
    chomp($transaction_error);
    # now rollback to undo the incomplete changes but do it in an eval{} as it may also fail.
    eval { $dbh->rollback(); };
  }
  # if error, display appropriate page.
  if (!$transaction_error) {
    # do nothing. continue with script.
  } elsif ($transaction_error eq "duplicate_email") {
    die "show_page: edit_profile_duplicate_email.html\n";
  } else {
    die $transaction_error;
  }
  # populate information needed for display of page.
  $address_1 = HTML::Entities::encode($address_1);
  $address_2 = HTML::Entities::encode($address_2);
  $city = HTML::Entities::encode($city);
  $state = HTML::Entities::encode($state);
  $zip_code = HTML::Entities::encode($zip_code);
  $country = HTML::Entities::encode($country);
  $work_phone_number = HTML::Entities::encode($work_phone_number);
  $work_fax_number = HTML::Entities::encode($work_fax_number);
  $home_phone_number = HTML::Entities::encode($home_phone_number);
  $cell_phone_number = HTML::Entities::encode($cell_phone_number);
  $email = HTML::Entities::encode($email);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor>
    <id>$investor->{"id"}</id>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <work_phone_number>$work_phone_number</work_phone_number>
    <work_fax_number>$work_fax_number</work_fax_number>
    <home_phone_number>$home_phone_number</home_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <email>$email</email>
  </investor>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_profile_result_success.xslt", $cgi);
}

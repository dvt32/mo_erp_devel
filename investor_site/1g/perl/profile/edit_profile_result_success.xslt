<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Edit Profile
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<xsl:apply-templates select="investor"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor">
    <p>
      You successfully updated your profile:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  State:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Work Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="work_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Fax:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="work_fax_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Home Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="home_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Cell Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cell_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # load states.
  my @state_list;
  my $sql_query = "select * from state_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@state_list);
  # load countries.
  my @country_list;
  $sql_query = "select * from country_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@country_list);
  # populate information needed for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <bank_account/>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <account_type_list>
    <account_type>checking</account_type>
    <account_type>savings</account_type>
  </account_type_list>
  <owner_type_list>
    <owner_type>personal</owner_type>
    <owner_type>business</owner_type>
  </owner_type_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_bank_account_input.xslt", $cgi);
}

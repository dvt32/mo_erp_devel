#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use insert_row;
use enqueue_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # get form input and validate it.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $depository_name = Encode::decode($cgi_input_charset, $cgi->param("depository_name"));
  if (!$depository_name) {
    die "show_page: edit_bank_account_missing_depository_name.html\n";
  }
  my $address_1 = Encode::decode($cgi_input_charset, $cgi->param("address_1"));
  my $address_2 = Encode::decode($cgi_input_charset, $cgi->param("address_2"));
  my $city = Encode::decode($cgi_input_charset, $cgi->param("city"));
  my $state = Encode::decode($cgi_input_charset, $cgi->param("state"));
  my $state_other = Encode::decode($cgi_input_charset, $cgi->param("state_other"));
  if ($state_other) {
    $state = $state_other;
    $state_other = "";
  }
  my $zip_code = Encode::decode($cgi_input_charset, $cgi->param("zip_code"));
  my $country = Encode::decode($cgi_input_charset, $cgi->param("country"));
  if ($country ne "United States") {
    die "show_page: edit_bank_account_bad_country.html\n";
  }
  my $account_type = $cgi->param("account_type");
  if ($account_type !~ /^checking|savings$/) {
    die "show_page: edit_bank_account_bad_account_type.html\n";
  }
  my $routing_number = $cgi->param("routing_number");
  if ($routing_number !~ /^\d{9}$/) {
    die "show_page: edit_bank_account_bad_routing_number.html\n";
  }
  my $account_number = $cgi->param("account_number");
  if ($account_number !~ /^\d{1,17}$/) {
    die "show_page: edit_bank_account_bad_account_number.html\n";
  }
  my $owner_name = Encode::decode($cgi_input_charset, $cgi->param("owner_name"));
  if (!$owner_name) {
    die "show_page: edit_bank_account_missing_owner_name.html\n";
  }
  my $owner_type = $cgi->param("owner_type");
  if ($owner_type !~ /^personal|business$/) {
    die "show_page: edit_bank_account_bad_owner_type.html\n";
  }
  my $account_nickname = Encode::decode($cgi_input_charset, $cgi->param("account_nickname"));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["investor_id", $investor->{"id"}, SQL_INTEGER]);
  push(@sql_column_list, ["depository_name", Encode::encode($db_charset, $depository_name)]);
  push(@sql_column_list, ["address_1", Encode::encode($db_charset, $address_1) || undef]);
  push(@sql_column_list, ["address_2", Encode::encode($db_charset, $address_2) || undef]);
  push(@sql_column_list, ["city", Encode::encode($db_charset, $city) || undef]);
  push(@sql_column_list, ["state", Encode::encode($db_charset, $state) || undef]);
  push(@sql_column_list, ["zip_code", Encode::encode($db_charset, $zip_code) || undef]);
  push(@sql_column_list, ["country", Encode::encode($db_charset, $country) || undef]);
  push(@sql_column_list, ["account_type", $account_type]);
  push(@sql_column_list, ["routing_number", $routing_number]);
  push(@sql_column_list, ["account_number", $account_number]);
  push(@sql_column_list, ["owner_name", Encode::encode($db_charset, $owner_name)]);
  push(@sql_column_list, ["owner_type", $owner_type]);
  push(@sql_column_list, ["account_nickname", Encode::encode($db_charset, $account_nickname) || undef]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  insert_row($dbh, "investor_bank_account_tbl", \@sql_column_list);
  # temporarily turn off warnings about undefined values.
  local($^W) = 0;
  # send an email to administrator.
  my $email_message = <<"---END_BLOCK---;";
==New bank account==
Depository Name: $depository_name
Address 1: $address_1
Address 2: $address_2
City: $city
State: $state
Zip Code: $zip_code
Country: $country
Account Type: $account_type
Routing Number: $routing_number
Account Number: $account_number
Owner Name: $owner_name
Owner Type: $owner_type
Account Nickname: $account_nickname
Note: $note
---END_BLOCK---;
  # turn warnings back on.
  local($^W) = 1;
  enqueue_email
    ("dbh" => $dbh,
     "investor_id" => $investor->{"id"},
     "from_hdr" => "Micro Office investor notification <investor_relations\@microoffice.com>",
     "to_hdr" => "investor_relations\@microoffice.com",
     "subject_hdr" => qq{Investor bank account insertion ($investor->{"investor_name"}, ID #$investor->{"id"})},
     "message_body" => $email_message,
    );
  # populate information needed for display of page.
  $depository_name = HTML::Entities::encode($depository_name);
  $address_1 = HTML::Entities::encode($address_1);
  $address_2 = HTML::Entities::encode($address_2);
  $city = HTML::Entities::encode($city);
  $state = HTML::Entities::encode($state);
  $zip_code = HTML::Entities::encode($zip_code);
  $country = HTML::Entities::encode($country);
  $owner_name = HTML::Entities::encode($owner_name);
  $account_nickname = HTML::Entities::encode($account_nickname);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <bank_account>
    <depository_name>$depository_name</depository_name>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <account_type>$account_type</account_type>
    <routing_number>$routing_number</routing_number>
    <account_number>$account_number</account_number>
    <owner_name>$owner_name</owner_name>
    <owner_type>$owner_type</owner_type>
    <account_nickname>$account_nickname</account_nickname>
    <note>$note</note>
  </bank_account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_bank_account_result.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Bank Accounts
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/profile/edit_profile_input.cgi">Edit Profile</a>
	  |
	  <a href="/1g/profile/new_bank_account_input.cgi">New Bank Account</a>
	</p>
	<xsl:apply-templates select="investor_bank_account_list"/>
	<p>
	  To edit a bank account, click on its account number. To delete a bank account, click on the account number to edit
	  the bank account and then click on the link that says "Delete Bank Account". To create a new bank account, click on
	  "New Bank Account" above.
	  <xsl:if test="count(investor_bank_account_list/investor_bank_account) > 0">
	    If there is a star next to a bank account, then that is your primary bank account for ACH transactions. If no bank
	    accounts are starred, you should <a href="/1g/profile/choose_primary_bank_account_input.cgi">choose a primary bank
	    account</a> so that ACH transactions can take place. If you do not choose a primary bank account, no ACH
	    transactions will take place. Instead, payments will be mailed to you via check to the address listed in your
	    profile.
	  </xsl:if>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor_bank_account_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
          Bank Accounts
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td></td>
	<td class="standard_table_spacer_column"></td>
	<td>Depository</td>
	<td class="standard_table_spacer_column"></td>
	<td>Routing Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Nickname</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(investor_bank_account) > 0">
	  <xsl:apply-templates select="investor_bank_account"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="13" align="center">You have no bank accounts registered. Click "New Bank Account" above to register one.</td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="investor_bank_account">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:if test="is_primary = '1'">
	  *
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="depository_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="routing_number"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/profile/edit_bank_account_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="account_number"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="account_nickname"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="note"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;
use output_file;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  my $tax_document_id = $cgi->param("id");
  if (!$tax_document_id) {
    die "show_page: show_document_bad_id.html\n";
  }
  my $tax_document = select_row($dbh, "tax_document_tbl", $tax_document_id);
  if ($investor->{"id"} != $tax_document->{"investor_id"}) {
    die "show_page: show_document_not_owner.html\n";
  }
  my $file_id = $tax_document->{"file_id"};
  output_file($instance_map, $file_id, undef, undef, 1);
}

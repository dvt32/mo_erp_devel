#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "{{ investor_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $investor) = init_login_standard($instance_map);
  # fetch tax documents.
  my $sql_query = "select * from tax_document_tbl where investor_id = ?";
  my @sql_param_list = ([$investor->{"id"}, SQL_INTEGER]);
  my %tax_document_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%tax_document_by_id);
  # fetch company rows.
  $sql_query = "select * from company_tbl";
  my %company_by_id;
  get_row_by_id_map($dbh, $sql_query, undef, \%company_by_id);
  # organize tax documents by form name, then company, then year.
  my %tax_document_tree;
  for my $id (keys(%tax_document_by_id)) {
    my $tax_document = $tax_document_by_id{$id};
    my $form_name = $tax_document->{"form_name"};
    my $company_id = $tax_document->{"company_id"};
    my $company_short_name = $company_by_id{$company_id}->{"short_name"};
    my $tax_year = $tax_document->{"tax_year"};
    $tax_document_tree{$form_name}->{$company_short_name}->{$tax_year} = $id;
  }
  # populate information needed for display of page.
  my $first_name = HTML::Entities::encode($investor->{"first_name"});
  my $middle_name = HTML::Entities::encode($investor->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($investor->{"last_name"});
  my $investor_name = HTML::Entities::encode($investor->{"investor_name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <investor>
    <id>$investor->{"id"}</id>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <investor_name>$investor_name</investor_name>
  </investor>
  <tax-documents>
---END_BLOCK---;
  for my $form_name (sort(keys(%tax_document_tree))) {
    my $encoded_form_name = HTML::Entities::encode($form_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <form name="$encoded_form_name">
---END_BLOCK---;
    my $tax_document_subtree_company = $tax_document_tree{$form_name};
    for my $company_name (sort(keys(%$tax_document_subtree_company))) {
      my $encoded_company_name = HTML::Entities::encode($company_name);
      $generated_xml .= <<"---END_BLOCK---;";
      <company name="$encoded_company_name">
---END_BLOCK---;
      my $tax_document_subtree_year = $tax_document_subtree_company->{$company_name};
      for my $tax_year (sort(keys(%$tax_document_subtree_year))) {
	my $tax_document_id = $tax_document_subtree_year->{$tax_year};
	my $tax_document = $tax_document_by_id{$tax_document_id};
	$generated_xml .= <<"---END_BLOCK---;";
        <tax-document>
          <id>$tax_document_id</id>
          <tax_year>$tax_year</tax_year>
        </tax-document>
---END_BLOCK---;
      }
      $generated_xml .= <<"---END_BLOCK---;";
      </company>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
    </form>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </tax-documents>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_document_list.xslt", $cgi);
}

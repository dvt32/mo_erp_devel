<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Investor Center :: Tax Documents
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/investor_center.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	</p>
	<xsl:apply-templates select="investor"/>
	<xsl:apply-templates select="tax-documents"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="investor">
    <p>
      Welcome, <xsl:value-of select="investor_name"/>. Here are the tax documents available to you:
    </p>
  </xsl:template>

  <xsl:template match="tax-documents">
    <ul>
      <xsl:apply-templates select="form"/>
    </ul>
  </xsl:template>

  <xsl:template match="form">
    <li><xsl:value-of select="@name"/></li>
    <ul>
      <xsl:apply-templates select="company"/>
    </ul>
  </xsl:template>

  <xsl:template match="company">
    <li>
      <xsl:value-of select="@name"/>:
      <xsl:apply-templates select="tax-document"/>
    </li>
  </xsl:template>

  <xsl:template match="tax-document">
    <a>
      <xsl:attribute name="href">
	<xsl:text>/tax/show_document.cgi?id=</xsl:text>
	<xsl:value-of select="id"/>
      </xsl:attribute>
      <xsl:value-of select="tax_year"/>
    </a>
    <xsl:if test="position() != last()">
      <xsl:text> &middot; </xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

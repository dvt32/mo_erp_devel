use strict;
local($^W) = 1;

my $web_root_dir = "/var/www/sites/kiosk.microoffice.com";

sub get_web_root_dir() {
  return $web_root_dir;
}

1;

use strict;
local($^W) = 1;

sub is_production_env() {
  my $http_server_name = $ENV{"SERVER_NAME"};
  if ($http_server_name) {
    $http_server_name =~ s/\..*$//;
  } else {
    die "could not get server name to determine whether we are in production environment";
  }
  if ($http_server_name =~ /^kiosk$/) {
    return 1;
  } else {
    return 0;
  }
}

1;

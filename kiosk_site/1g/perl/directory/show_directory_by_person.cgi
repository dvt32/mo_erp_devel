#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "../lib";
use lib "/var/www/cgi-bin/kiosk.microoffice.com-securelib";
use lib "/var/www/cgi-bin/web-sharedlib";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use apply_xslt;
use get_building_id_given_ip_address;
use get_row_by_id_map;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $remote_client_ip_address = $cgi->remote_addr();
  my $building_id = get_building_id_given_ip_address($remote_client_ip_address);
  if (!defined($building_id)) {
    die "show_page: show_directory_unknown_ip_address.html\n";
  }
  # get all cust_acct rows in the building.
  my %cust_acct_by_id;
  my $sql_query = "select * from cust_acct_tbl where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
  get_row_by_id_map($dbh, $sql_query, [[$building_id, SQL_INTEGER]], \%cust_acct_by_id);
  # select all cust_pers rows in the building.
  my @cust_pers_list;
  $sql_query = "select * from cust_pers_tbl where is_active and cust_acct_id in (select id from cust_acct_tbl where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)) order by last_name, first_name, middle_name";
  get_row_list($dbh, $sql_query, [[$building_id, SQL_INTEGER]], \@cust_pers_list);
  # select all visible phone numbers for people in the building.
  my (%phone_number_by_id, %phone_number_id_list_by_person_id);
  $sql_query = "select id, account_id, person_id, phone_number from phone_number_tbl where visibility <> ?";
  my $dbh_phone = get_database_handle($instance_map, "phone");
  get_row_by_id_map($dbh_phone, $sql_query, ["hidden"], \%phone_number_by_id);
  for my $phone_number (values(%phone_number_by_id)) {
    if ((!$phone_number->{"account_id"}) || (!$cust_acct_by_id{$phone_number->{"account_id"}})) {
      delete $phone_number_by_id{$phone_number->{"id"}};
      next;
    }
    if (defined($phone_number->{"person_id"})) {
      push(@{$phone_number_id_list_by_person_id{$phone_number->{"person_id"}}}, $phone_number->{"id"});
    }
  }
  # sort the phone number id lists in by_person_id.
  for my $phone_number_id_list (values(%phone_number_id_list_by_person_id)) {
    @{$phone_number_id_list} = sort {$phone_number_by_id{$a}->{"phone_number"} cmp $phone_number_by_id{$b}->{"phone_number"}} @{$phone_number_id_list};
  }
  my @directory_entry_list;
  my $directory_entry;
  for my $cust_pers (@cust_pers_list) {
    my $cust_pers_id = $cust_pers->{"id"};
    my $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    my $cust_acct_id = $cust_pers->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = $cust_acct->{"name"};
    my $phone_number_id_list = $phone_number_id_list_by_person_id{$cust_pers_id};
    if (defined($phone_number_id_list) && (scalar(@$phone_number_id_list) > 0)) {
      if (has_external_phone_number($cust_pers)) {
	my $phone_number_id = $phone_number_id_list->[0];
	my $phone_number = $phone_number_by_id{$phone_number_id};
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"account_name"} = $account_name;
	$directory_entry->{"desk_phone_num"} = $phone_number->{"phone_number"};
	$directory_entry->{"external_phone_numbers"} = render_external_phone_number($cust_pers);
	push(@directory_entry_list, $directory_entry);
      } else {
	my $phone_number_id = $phone_number_id_list->[0];
	my $phone_number = $phone_number_by_id{$phone_number_id};
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"account_name"} = $account_name;
	$directory_entry->{"desk_phone_num"} = $phone_number->{"phone_number"};
	$directory_entry->{"external_phone_numbers"} = "";
	push(@directory_entry_list, $directory_entry);
      }
      for(my $j = 1; $j < @$phone_number_id_list; $j++) {
	my $phone_number_id = $phone_number_id_list->[$j];
	my $phone_number = $phone_number_by_id{$phone_number_id};
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"account_name"} = $account_name;
	$directory_entry->{"desk_phone_num"} = $phone_number->{"phone_number"};
	$directory_entry->{"external_phone_numbers"} = "";
	push(@directory_entry_list, $directory_entry);
      }
    } else {
      if (has_external_phone_number($cust_pers)) {
	$directory_entry = {};
	$directory_entry->{"person_name"} = $person_name;
	$directory_entry->{"account_name"} = $account_name;
	$directory_entry->{"desk_phone_num"} = "";
	$directory_entry->{"external_phone_numbers"} = render_external_phone_number($cust_pers);
	push(@directory_entry_list, $directory_entry);
      }
    }
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <row-list>
---END_BLOCK---;
  for $directory_entry (@directory_entry_list) {
    my $person_name = HTML::Entities::encode($directory_entry->{"person_name"});
    my $account_name = HTML::Entities::encode($directory_entry->{"account_name"});
    my $desk_phone_num = $directory_entry->{"desk_phone_num"};
    $desk_phone_num =~ s/^(\d{3})(\d{3})(\d{4})$/$1-$2-$3/;
    my $external_phone_numbers = HTML::Entities::encode($directory_entry->{"external_phone_numbers"});
    $generated_xml .= <<"---END_BLOCK---;";
    <row>
      <person-name>$person_name</person-name>
      <account-name>$account_name</account-name>
      <desk_phone_num>$desk_phone_num</desk_phone_num>
      <external-phone-numbers>$external_phone_numbers</external-phone-numbers>
    </row>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </row-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet.
  my $serialized_output = apply_xslt
    ("xml_document" => $generated_xml,
     "xslt_file" => "show_directory_by_person.xslt");
  print $cgi->header
    ("-type" => "text/html",
     "-expires" => "-1m",
     "-cache_control" => "no-cache");
  print $serialized_output;
}

sub has_external_phone_number($) {
  my $cust_pers = shift;
  my $has_number = 0;
  for(my $i = 1; $i <= 3; $i++) {
    if (my $phone_number_type = $cust_pers->{"directory_$i"}) {
      if (my $phone_number = $cust_pers->{$phone_number_type . "_phone_number"}) {
	if ($phone_number !~ /^\s*$/) {
	  $has_number = 1;
	}
      }
    }
  }
  return $has_number;
}

sub render_external_phone_number($) {
  my $cust_pers = shift;
  my @phone_number_list;
  for(my $i = 1; $i <= 3; $i++) {
    if (my $phone_number_type = $cust_pers->{"directory_$i"}) {
      if (my $phone_number = $cust_pers->{$phone_number_type . "_phone_number"}) {
	if (($phone_number !~ /^\s*$/) && (!grep {$_ eq $phone_number} @phone_number_list)) {
	  push(@phone_number_list, $phone_number);
	}
      }
    }
  }
  return join(" / ", @phone_number_list);
}


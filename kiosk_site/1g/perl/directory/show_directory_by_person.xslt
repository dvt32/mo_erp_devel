<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Kiosk :: Tenant Directory
	</title>
	<link rel="Stylesheet" type="text/css" href="/css/kiosk.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
	<p>
	  Return to the <a href="/index.cgi">kiosk menu</a>.
	</p>
	<p>
	  <img src="/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="row-list">
    <table cellspacing="0" cellpadding="0" border="0" class="directory_table">
      <tr class="directory_header">
	<td class="directory_side_padding"></td>
	<td>Person</td>
	<td class="directory_spacer_column"></td>
	<td>Company</td>
	<td class="directory_spacer_column"></td>
	<td>Desk Phone</td>
	<td class="directory_spacer_column"></td>
	<td>External Phone Number(s)</td>
	<td class="directory_side_padding"></td>
      </tr>
      <xsl:apply-templates/>
      <tr class="directory_footer">
	<td class="directory_side_padding"></td>
	<td colspan="7">Visit https://bizcenter.microoffice.com to update tenant details (note the https, which denotes SSL for secure communications)</td>
	<td class="directory_side_padding"></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="row">
    <tr>
      <xsl:choose>
	<!-- For some reason the position() function returns everything multiplied by 2. -->
	<xsl:when test="position() mod 4 = 0">
	  <xsl:attribute name="class">directory_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">directory_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="directory_side_padding"></td>
      <td><xsl:value-of select="person-name"/></td>
      <td class="directory_spacer_column"></td>
      <td><xsl:value-of select="account-name"/></td>
      <td class="directory_spacer_column"></td>
      <td><xsl:value-of select="desk_phone_num"/></td>
      <td class="directory_spacer_column"></td>
      <td><xsl:value-of select="external-phone-numbers"/></td>
      <td class="directory_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

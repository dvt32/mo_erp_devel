<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Kiosk :: Error
        </title>
        <link rel="Stylesheet" type="text/css" href="/css/kiosk.css"/>
      </head>
      <body>
        <p>
          <a href="/index.cgi">Main menu</a>
        </p>
	<p>
	  The following error occurred:
	</p>
	<p>
	  <pre>
	    <xsl:value-of select="exception"/>
	  </pre>
	</p>
	<p>
	  If you do not know what to do, please consult a system administrator.
	</p>
        <p>
          <img src="/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

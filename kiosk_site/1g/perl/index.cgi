#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "lib";
use lib "/var/www/cgi-bin/kiosk.microoffice.com-securelib";
use lib "/var/www/cgi-bin/web-sharedlib";
use main_exception_wrapper;
use get_cgi;
use apply_xslt_output_xhtml;
use get_database_handle;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "index.xslt", $cgi);
}

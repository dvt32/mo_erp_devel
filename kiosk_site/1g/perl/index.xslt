<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>Micro Office Solutions :: Kiosk</title>
	<link rel="Stylesheet" type="text/css" href="/css/kiosk.css" />
	<link rel="icon" type="image/x-icon" href="/favicon.ico" />
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
      </head>
      <body>
	<p><img src="/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
	<p>Services</p>
	<ul>
	  <li><a href="/meet/show_schedule.cgi">Conference Room Schedules</a></li>
	  <li><a href="/directory/show_directory_by_person.cgi">Tenant Directory (sorted by person)</a></li>
	  <li><a href="/directory/show_directory_by_company.cgi">Tenant Directory (sorted by company)</a></li>
	</ul>
	<p>Have suggestions or comments? Send feedback to service@microoffice.com.</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

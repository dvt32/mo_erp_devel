#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "../lib";
use lib "/var/www/cgi-bin/kiosk.microoffice.com-securelib";
use lib "/var/www/cgi-bin/web-sharedlib";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use select_row_with_query;
use minutes_to_display_time;
use timestamp_to_minutes;
use day_of_week;
use get_day_name;
use get_month_name;
use lead_pad;
use strip_outside_whitespace;
use get_row_list;
use insert_row;
use serialize_array;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify the format of the date parameter and parse the date.
  my $reservation_date = $cgi->param("date");
  if (!$reservation_date) {
    die "show_page: reserve_room_bad_date.html\n";
  }
  if ($reservation_date !~ /^(\d{4})-(\d\d)-(\d\d)$/) {
    die "show_page: reserve_room_bad_date.html\n";
  }
  # verify that the start time is valid.
  my $start_time = $cgi->param("time");
  if ((!defined($start_time)) || ($start_time eq "")) {
    die "show_page: reserve_room_bad_start_time.html\n";
  }
  if ($start_time !~ /^\d+$/) {
    die "show_page: reserve_room_bad_start_time.html\n";
  }
  if (($start_time < 0) || ($start_time > 24 * 60)) {
    die "show_page: reserve_room_bad_start_time.html\n";
  }
  # verify that the duration is valid.
  my $duration = $cgi->param("duration");
  if (!$duration) {
    die "show_page: reserve_room_bad_duration.html\n";
  }
  if ($duration !~ /^\d+$/) {
    die "show_page: reserve_room_bad_duration.html\n";
  }
  if (($duration < 0) || ($duration > 4 * 60)) {
    die "show_page: reserve_room_bad_duration.html\n";
  }
  # verify that the start time plus the duration do not exceed midnight (i.e. cross over into a new day).
  if (($start_time + $duration) > 24 * 60) {
    die "show_page: reserve_room_day_boundary_error.html\n";
  }
  # verify that the room was specified.
  my $room_id = $cgi->param("room_id");
  if ((!defined($room_id)) || ($room_id eq "")) {
    die "show_page: reserve_room_bad_room.html\n";
  }
  # verify that email and password were specified.
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email"));
  my $password = Encode::decode($cgi_input_charset, $cgi->param("password"));
  if ((!$email) || (!($email = strip_outside_whitespace($email))) || (!$password)) {
    die "show_page: reserve_room_credentials_missing.html\n";
  }
  # make emails case-insensitive by forcing them to lowercase.
  $email = lc($email);
  # verify that (email, password) pair exists in database.
  my $db_charset = get_database_charset($instance_map, "main");
  my $sql_query = "select * from cust_pers_tbl where email = ? and password = ? and is_active and length(password) > 0";
  my @sql_param_list = (Encode::encode($db_charset, $email), Encode::encode($db_charset, $password));
  my $cust_pers = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if (!$cust_pers) {
    die "show_page: reserve_room_bad_credentials.html\n";
  }
  # verify that either (1) the specified room is in the same building as the cust_acct and the room has shared floor set to
  # true, or (2) the specified room is on the same floor as the cust_acct.
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_pers->{"cust_acct_id"});
  my $cust_acct_id = $cust_acct->{"id"};
  if ((!$cust_acct) || (!$cust_acct->{"is_active"})) {
    die "show_page: reserve_room_bad_account.html\n";
  }
  my $room = select_row($dbh, "room_tbl", $room_id);
  if ($room) {
    if ($room->{"is_shared_floor"}) {
      my $room_floor = select_row($dbh, "floor_tbl", $room->{"floor_id"});
      my $account_floor = select_row($dbh, "floor_tbl", $cust_acct->{"primary_floor_id"});
      if ($room_floor->{"building_id"} != $account_floor->{"building_id"}) {
	die "show_page: reserve_room_unauthorized_room.html\n";
      }
    } else {
      if ($room->{"floor_id"} != $cust_acct->{"primary_floor_id"}) {
	die "show_page: reserve_room_unauthorized_room.html\n";
      }
    }
  } else {
    die "show_page: reserve_room_bad_room.html\n";
  }
  my $start_dt = $reservation_date . " " . minutes_to_display_time($start_time) . ":00";
  my $end_dt = $reservation_date . " " . minutes_to_display_time($start_time + $duration) . ":00";
  # get the note.
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of inserting the reservation row.
  $dbh->begin_work();
  # wrap in an eval to catch exceptions.
  eval {
    # raise an exception if there is a database error (such as transaction error).
    local $dbh->{"RaiseError"} = 1;
    # there is an overlapping reservation in the same room. roll back the transaction and display an error.
    $sql_query = "select * from reservation_tbl where room_id = ? and (start_dt, end_dt) overlaps (timestamp ?, timestamp ?)";
    if (select_row_with_query($dbh, $sql_query, [[$room_id, SQL_INTEGER], $start_dt, $end_dt])) {
      die "overlap\n";
    }
    # compute amount of time booked so far during the month of the reservation.
    $reservation_date =~ /^(\d{4})-(\d\d)-(\d\d)$/;
    my $reservation_year = $1;
    my $reservation_month = $2;
    my $year_upper_bound = $reservation_year;
    my $month_upper_bound = $reservation_month + 1;
    if ($month_upper_bound > 12) {
      $year_upper_bound++;
      $month_upper_bound = 1;
    }
    $month_upper_bound = lead_pad($month_upper_bound, 2);
    # check how much time has been booked already in the month of the proposed reservation.
    my @reservation_list;
    $sql_query = "select * from reservation_tbl where account_id = ? and start_dt >= timestamp ? and start_dt < timestamp ?";
    @sql_param_list = ([$cust_acct_id, SQL_INTEGER], "$reservation_year-$reservation_month-01 00:00:00", "$year_upper_bound-$month_upper_bound-01 00:00:00");
    get_row_list($dbh, $sql_query, \@sql_param_list, \@reservation_list);
    my $time_used = 0;
    for my $reservation (@reservation_list) {
      $time_used += timestamp_to_minutes($reservation->{"end_dt"}) - timestamp_to_minutes($reservation->{"start_dt"});
    }
    # now add in the proposed reservation.
    my $previously_used_time = $time_used;
    $time_used += timestamp_to_minutes($end_dt) - timestamp_to_minutes($start_dt);
    my $time_allocated = $cust_acct->{"room_minutes"};
    if ($time_used > $time_allocated) {
      die "out_of_time: ($previously_used_time,$time_allocated)\n";
    }
    # no problems, so insert the new reservation row.
    my @sql_column_list;
    push(@sql_column_list, ["room_id", $room_id, SQL_INTEGER]);
    push(@sql_column_list, ["account_id", $cust_acct_id, SQL_INTEGER]);
    push(@sql_column_list, ["start_dt", ["timestamp ?", $start_dt]]);
    push(@sql_column_list, ["end_dt", ["timestamp ?", $end_dt]]);
    push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
    insert_row($dbh, "reservation_tbl", \@sql_column_list);
    # commit the transaction.
    $dbh->commit();
  };
  my $transaction_err = $@;
  if ($transaction_err) {
    # remove trailing newline from exception message.
    chomp($transaction_err);
    # now rollback to undo the incomplete changes but do it in an eval{} as it may also fail.
    eval { $dbh->rollback(); };
  }
  # if error, display appropriate page.
  if (!$transaction_err) {
    # do nothing. continue with script.
  } elsif ($transaction_err eq "overlap") {
    die "show_page: reserve_room_overlap_error.html\n";
  } elsif ($transaction_err =~ /^out_of_time: \((\d+),(\d+)\)/) {
    my $time_used = $1/60;
    my $time_allocated = $2/60;
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <error>
    <time_used>$time_used</time_used>
    <time_allocated>$time_allocated</time_allocated>
  </error>
</page>
---END_BLOCK---;
    my $serialized_array = serialize_array($generated_xml, "reserve_room_out_of_time.xslt");
    die "apply_xslt: $serialized_array\n";
  } else {
    # throw the exception up the chain.
    die $transaction_err;
  }
  # determine reservation information needed for display of page.
  $reservation_date =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  my ($year, $month, $day) = ($1, $2, $3);
  my $day_name = get_day_name(day_of_week($month, $day, $year));
  my $month_name = get_month_name($month);
  my $day_number = $day;
  my $start_time_display = minutes_to_display_time($start_time, 1);
  my $end_time_display = minutes_to_display_time($start_time + $duration, 1);
  my $room_name = HTML::Entities::encode($room->{"short_name"});
  $note = HTML::Entities::encode($note);
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <room-reservation>
    <account-name>$account_name</account-name>
    <day-name>$day_name</day-name>
    <month-name>$month_name</month-name>
    <day-number>$day_number</day-number>
    <year>$year</year>
    <start-time>$start_time_display</start-time>
    <end-time>$end_time_display</end-time>
    <room-name>$room_name</room-name>
    <note>$note</note>
  </room-reservation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reserve_room_success.xslt", $cgi);
}

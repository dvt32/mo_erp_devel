<IfModule mod_ssl.c>
<VirtualHost {{ member_ipv4_addr }}:443>
  ServerName {{ member_https_server_name }}
  ServerAdmin webmaster@microoffice.com
  ErrorLog ${APACHE_LOG_DIR}/member-error.log
  CustomLog ${APACHE_LOG_DIR}/member-access.log combined
  SSLEngine on
  SSLCertificateFile /etc/ssl/certs/microoffice.com.crt
  SSLCertificateKeyFile /etc/ssl/private/microoffice.com.key
  SSLCertificateChainFile /etc/ssl/certs/{{ intermediate_cert_filename }}
  # HSTS for 1 year
  Header always set Strict-Transport-Security "max-age=31536000"
  <FilesMatch "\.(cgi|shtml|phtml|php)$">
    SSLOptions +StdEnvVars
  </FilesMatch>
  <Directory /usr/lib/cgi-bin>
    SSLOptions +StdEnvVars
  </Directory>
  BrowserMatch "MSIE [2-6]" nokeepalive ssl-unclean-shutdown downgrade-1.0 force-response-1.0
  # MSIE 7 and newer should be able to use keepalive
  BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
  DocumentRoot /var/www/sites/{{ member_fqdn }}
  WSGIDaemonProcess {{ member_wsgi_group_name }}v4 python-home=/var/www/sites/{{ member_fqdn }}/venv
  WSGIScriptAlias / /var/www/sites/{{ member_fqdn }}/application.wsgi
  <Directory /var/www/sites/{{ member_fqdn }}>
    Options All ExecCGI -Indexes
    AllowOverride All
    Order deny,allow
    Allow from all
    WSGIProcessGroup {{ member_wsgi_group_name }}v4
    WSGIApplicationGroup %{GLOBAL}
    WSGIScriptReloading {{ member_wsgi_script_reloading }}
  </Directory>
</VirtualHost>
<VirtualHost [{{ member_ipv6_addr }}]:443>
  ServerName {{ member_https_server_name }}
  ServerAdmin webmaster@microoffice.com
  ErrorLog ${APACHE_LOG_DIR}/member-error.log
  CustomLog ${APACHE_LOG_DIR}/member-access.log combined
  SSLEngine on
  SSLCertificateFile /etc/ssl/certs/microoffice.com.crt
  SSLCertificateKeyFile /etc/ssl/private/microoffice.com.key
  SSLCertificateChainFile /etc/ssl/certs/{{ intermediate_cert_filename }}
  # HSTS for 1 year
  Header always set Strict-Transport-Security "max-age=31536000"
  <FilesMatch "\.(cgi|shtml|phtml|php)$">
    SSLOptions +StdEnvVars
  </FilesMatch>
  <Directory /usr/lib/cgi-bin>
    SSLOptions +StdEnvVars
  </Directory>
  BrowserMatch "MSIE [2-6]" nokeepalive ssl-unclean-shutdown downgrade-1.0 force-response-1.0
  # MSIE 7 and newer should be able to use keepalive
  BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
  DocumentRoot /var/www/sites/{{ member_fqdn }}
  WSGIDaemonProcess {{ member_wsgi_group_name }}v6 python-home=/var/www/sites/{{ member_fqdn }}/venv
  WSGIScriptAlias / /var/www/sites/{{ member_fqdn }}/application.wsgi
  <Directory /var/www/sites/{{ member_fqdn }}>
    Options All ExecCGI -Indexes
    AllowOverride All
    Order deny,allow
    Allow from all
    WSGIProcessGroup {{ member_wsgi_group_name }}v6
    WSGIApplicationGroup %{GLOBAL}
    WSGIScriptReloading {{ member_wsgi_script_reloading }}
  </Directory>
</VirtualHost>
</IfModule>

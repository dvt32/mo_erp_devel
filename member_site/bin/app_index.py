import psycopg2
import psycopg2.extras
from flask import g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, HiddenField, IntegerField, BooleanField, RadioField
from wtforms.fields.html5 import DateField, DecimalField
from wtforms_components import If
from wtforms import validators, ValidationError
import ach
import card
import decimal
import datetime
import tzconvert
import database
import json
from flask import render_template
import log_access

def show_index():
  log_access.log_page()
  return render_template('index.html') 


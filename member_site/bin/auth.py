from flask import request, abort, redirect, url_for, flash, render_template
import flask_login
from flask_wtf import FlaskForm
from wtforms.fields.html5 import EmailField
from wtforms import PasswordField, HiddenField, BooleanField, SubmitField
from wtforms.validators import InputRequired, Email
import warnings
import psycopg2
import psycopg2.extras
import database
from user import User
import cryptpw
import debugutil

class LoginForm(FlaskForm):
  next = HiddenField('next')
  email = EmailField('Email', validators=[InputRequired(), Email()], render_kw={'placeholder':'Email'})
  password = PasswordField('Password', validators=[InputRequired()], render_kw={'placeholder': 'Password'})
  remember_me = BooleanField('Remember Me', description="Remember me", default=True)
  submitForm = SubmitField('Login')

def login():
  form = LoginForm(request.form)
  if request.method == 'GET':
    form.next.data = request.args.get('next') or '/'
    return render_template('login.html', form=form)
  if not form.validate_on_submit():
    return render_template('login.html', form=form)
  next = form.next.data or '/'
  email = form.email.data.strip().lower()
  password = form.password.data.strip()
  remember_me = form.remember_me.data
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE lower(email) = %s', (email,))
  cust_pers = curdict.fetchone()
  if not cust_pers:
    flash('It seems you\'re not registered for an account on our member site, or perhaps you\'re using a different email than the one we have in our database. Please click the register link at the bottom to do so.', 'error')
    return render_template('login.html', form=form)
  curdict.execute('INSERT INTO log_cust_login_tbl (email, ip_address, user_agent, cust_acct_id, cust_pers_id) VALUES (%s, %s, %s, %s, %s)', (email, request.remote_addr, request.user_agent.string, cust_pers['cust_acct_id'], cust_pers['id']))
  conn.commit()
# uncomment to output login details to /tmp/debug.txt
#  key_list = ['id', 'cust_acct_id', 'email', 'member_ws_cryptpasswd']
#  debugutil.log_msg(', '.join([key + ': ' + str(cust_pers[key]) for key in key_list]))
  if cust_pers and cryptpw.check_password(password, cust_pers['member_ws_cryptpasswd']):
    user = User()
    user.id = cust_pers['id']
    user.cust_acct_id = cust_pers['cust_acct_id']
    user.email = cust_pers['email'].strip()
    # if number of bcrypt rounds has increased, update the crypted password
    if not cryptpw.hash_param_match_default(cust_pers['member_ws_cryptpasswd']):
      update_cryptpasswd(conn, password, cust_pers['id'])
    flask_login.login_user(user)
    # need to check if next is valid and user has authorization
    # TODO: perform check here
    # if not valid, return abort(403)
    return redirect(next or url_for('index'))
  return render_template('bad_login.html')

def update_cryptpasswd(conn, password, cust_pers_id):
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  hashed_password = cryptpw.hash_password(password)
  curdict.execute('UPDATE cust_pers_tbl SET member_ws_passwd = %s, member_ws_cryptpasswd = %s WHERE id = %s', (password, hashed_password, cust_pers_id))
  conn.commit()


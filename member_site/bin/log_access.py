from flask import g, request
import database
import psycopg2.extras
import database

def log_page(do_commit=True):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('INSERT INTO log_page_access_tbl (cust_acct_id, cust_pers_id, ip_address, user_agent, request_uri, http_referer) VALUES (%s, %s, %s, %s, %s, %s)', (g.user.cust_acct_id, g.user.id, request.remote_addr, request.user_agent.string, request.url, request.referrer))
  if do_commit:
    conn.commit()

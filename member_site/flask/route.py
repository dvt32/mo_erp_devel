import sys
sys.path.append('/var/www/sites/{{ member_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ member_fqdn }}/lib')
from flask import Flask
from flask import request, abort, redirect, url_for, flash, render_template
from flask import g, send_from_directory
import flask_login
import warnings
import psycopg2
import psycopg2.extras
import os
from user import User
import database
import auth
import app_index
import log_access

app = Flask(__name__)
app.secret_key = '{{ member_flask_secret_key }}'
app.debug = {{ member_flask_app_debug }}

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
# name of function to run when login_required is triggered and no user is logged in
login_manager.login_view = 'login'

@app.context_processor
def define_static_url():
    return dict(static_base_url='{{ static_base_url }}')

@app.before_request
def before_request():
  g.user = flask_login.current_user
@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@login_manager.user_loader
def user_loader(id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE id = %s', (id,))
  cust_pers = curdict.fetchone()
  if not cust_pers:
    return
  user = User()
  user.id = id
  user.cust_acct_id = cust_pers['cust_acct_id']
  user.email = cust_pers['email'].strip()
  return user

@app.route('/')
@flask_login.login_required
def index():
  return app_index.show_index()

@app.route('/favicon.ico')
def favicon():
  return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/login', methods=['GET', 'POST'])
def login():
  return auth.login()

@app.route('/logout')
@flask_login.login_required
def logout():
  log_access.log_page()
  flask_login.logout_user()
  return render_template('logout.html')

@app.route('/instrument/<action>')
def instrument(action):
  return "OK"


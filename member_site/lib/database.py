from flask import g
import psycopg2

def get_connection():
  conn = getattr(g, '_database', None)
  if conn is None:
    conn = g._database = psycopg2.connect(database='{{ main_database_name }}', user='{{ main_database_member_username }}', password='{{ main_database_member_password }}', host='{{ main_database_hostname }}', sslmode='{{ main_database_sslmode }}')
  return conn

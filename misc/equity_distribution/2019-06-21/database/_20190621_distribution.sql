--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _20190621_distribution_tbl; Type: TABLE; Schema: public; Owner: wchao
--

CREATE TABLE public._20190621_distribution_tbl (
    id integer,
    first_name character varying(32),
    last_name character varying(64),
    investor_name character varying(80),
    email character varying(64),
    class_a_units numeric(12,4),
    ownership_fraction numeric(13,12),
    inital_cap money,
    distribution_amount money
);


ALTER TABLE public._20190621_distribution_tbl OWNER TO wchao;

--
-- Data for Name: _20190621_distribution_tbl; Type: TABLE DATA; Schema: public; Owner: wchao
--

COPY public._20190621_distribution_tbl (id, first_name, last_name, investor_name, email, class_a_units, ownership_fraction, inital_cap, distribution_amount) FROM stdin;
48	Sheetal	Sharma	Sheetal Sharma	sheetal@hoplitecapital.com	475.7488	0.000475748800	$2,000.00	$299.72
27	Amish	Majmundar	Amish Majmundar	amajmundar@gmail.com	7567.1004	0.007567100397	$16,750.00	$4,767.27
28	Jean	Chao	Chi Yun Chao Revocable Trust	chaojean@hotmail.com	7876.6657	0.007876665697	$14,000.00	$4,962.30
31	Chen	Lin	Chen Lin	chen.lin@gmail.com	12334.8616	0.012334861595	$20,000.00	$7,770.96
18	Arvind	Krishnamurthy	Arvind Krishnamurthy	akrishnamurthy@mba2003.hbs.edu	2858.5436	0.002858543599	$10,875.00	$1,800.88
33	Rajesh	Ramanan	Rajesh Ramanan	rtramanan@gmail.com	1233.4862	0.001233486200	$2,000.00	$777.10
7	Michael	Gellert	Michael Gellert	mgellert@windcrestpartners.com	103070.6911	0.103070691059	$242,993.00	$64,934.54
5	Pavninder	Singh	Pavninder Singh	psingh@mba2005.hbs.edu	57347.5128	0.057347512777	$19,807.69	$36,128.93
26	Neelam	Modi	Neelam Modi	neelam.modi@gmail.com	9118.1711	0.009118171096	$18,154.54	$5,744.45
17	Anne	Lahar	Anne Lahar	phillahar@gmail.com	6719.9315	0.006719931497	$14,500.00	$4,233.56
56	Ravi	Krishnamurthy	Ravi Krishnamurthy	mskravi@yahoo.com	521.9463	0.000521946300	$1,363.64	$328.83
9	Ranvir	Gujral	RSG Investments LLC	ranvir@gmail.com	4440.5502	0.004440550198	$7,200.00	$2,797.55
14	Alastair	Onglingswan	Alastair Onglingswan	misobrilliant@gmail.com	12334.8616	0.012334861595	$20,000.00	$7,770.96
2	Erica	Dewan	Erica Dewan	edewan@me.com	58889.3705	0.058889370476	$22,307.69	$37,100.30
10	Jeffrey	Kuo	Jeffrey Kuo	jeffreyckuo@gmail.com	2158.6008	0.002158600799	$3,500.00	$1,359.92
86	Esther	Chao	Esther Chao	esther.chao@gmail.com	12544.8555	0.012544855495	$4,188.21	$7,903.26
3	Wellie	Chao	Wellie Chao	wchao@microoffice.com	467148.7622	0.467148762013	$29,615.38	$294,303.72
4	Jerry	Chao	Jih Kwin Chao Trust #1	jkchaous@gmail.com	53888.2015	0.053888201478	$17,991.03	$33,949.57
65	Flora	Lutz	Flora Lutz	florachao@gmail.com	18817.2833	0.018817283292	$6,282.31	$11,854.89
15	Munish	Puri	Munish Puri	munpuri@gmail.com	82786.6110	0.082786610967	$30,000.00	$52,155.56
13	Sharmil	Modi	Sharmil Modi	modi@post.harvard.edu	12476.4193	0.012476419295	$26,068.18	$7,860.14
8	Virali	Gokaldas	Virali Gokaldas	virali.modiparekh@gmail.com	6167.4308	0.006167430798	$10,000.00	$3,885.48
29	Amar	Goel	On the Side Investments LLC	amar.goel@pubmatic.com	58889.3705	0.058889370476	$22,307.69	$37,100.30
105	Spencer	Rothschild	Barrio Foods Consulting	srothschild@coalitionspace.com	333.0241	0.000333024100	$1,400.00	$209.81
\.


--
-- Name: TABLE _20190621_distribution_tbl; Type: ACL; Schema: public; Owner: wchao
--

GRANT SELECT ON TABLE public._20190621_distribution_tbl TO orion_wchao;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _20190621_equity_ownership_fraction_tbl; Type: TABLE; Schema: public; Owner: wchao
--

CREATE TABLE public._20190621_equity_ownership_fraction_tbl (
    id integer,
    total_units numeric(12,4),
    ownership_fraction numeric(13,12)
);


ALTER TABLE public._20190621_equity_ownership_fraction_tbl OWNER TO wchao;

--
-- Data for Name: _20190621_equity_ownership_fraction_tbl; Type: TABLE DATA; Schema: public; Owner: wchao
--

COPY public._20190621_equity_ownership_fraction_tbl (id, total_units, ownership_fraction) FROM stdin;
48	1000000.0004	0.000475748800
27	1000000.0004	0.007567100397
28	1000000.0004	0.007876665697
31	1000000.0004	0.012334861595
18	1000000.0004	0.002858543599
33	1000000.0004	0.001233486200
7	1000000.0004	0.103070691059
5	1000000.0004	0.057347512777
26	1000000.0004	0.009118171096
17	1000000.0004	0.006719931497
56	1000000.0004	0.000521946300
9	1000000.0004	0.004440550198
14	1000000.0004	0.012334861595
2	1000000.0004	0.058889370476
10	1000000.0004	0.002158600799
86	1000000.0004	0.012544855495
3	1000000.0004	0.467148762013
4	1000000.0004	0.053888201478
65	1000000.0004	0.018817283292
15	1000000.0004	0.082786610967
13	1000000.0004	0.012476419295
8	1000000.0004	0.006167430798
29	1000000.0004	0.058889370476
105	1000000.0004	0.000333024100
\.


--
-- PostgreSQL database dump complete
--


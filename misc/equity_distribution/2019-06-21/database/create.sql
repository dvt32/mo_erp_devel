CREATE TABLE _20190621_equity_ownership_fraction_tbl AS SELECT id FROM investor_tbl WHERE class_a_units IS NOT NULL;
ALTER TABLE _20190621_equity_ownership_fraction_tbl ADD COLUMN total_units numeric(12,4);
ALTER TABLE _20190621_equity_ownership_fraction_tbl ADD COLUMN ownership_fraction numeric(13,12);
UPDATE _20190621_equity_ownership_fraction_tbl SET total_units = (SELECT SUM(class_a_units) FROM investor_tbl);
UPDATE _20190621_equity_ownership_fraction_tbl SET ownership_fraction = (SELECT class_a_units/total_units FROM investor_tbl WHERE investor_tbl.id = _20190621_equity_ownership_fraction_tbl.id);
CREATE TABLE _20190621_distribution_tbl AS SELECT investor_tbl.id, first_name, last_name, investor_name, email, class_a_units, ownership_fraction, initial_capital_contribution AS inital_cap, (630000*ownership_fraction)::money AS distribution_amount FROM investor_tbl NATURAL JOIN _20190621_equity_ownership_fraction_tbl;

#!{{ local_basedir }}/venv/bin/python3
"""Send email payment advice to equity investors for 2019-09-18 distribution."""

import sys, argparse
import smtplib, ssl, email.utils
import psycopg2, psycopg2.extras
import jinja2
import traceback
import locale

def main(argv):
  argparser = argparse.ArgumentParser(description='Send email payment advice to equity investors for 2019-09-18 distribution.')
  argparser.add_argument('investor_payment_id', help='ID value of row in investor_payment_tbl', type=int)
  argparser.add_argument('--dryrun', help='Dry run; do not actually send emails.', action='store_true')
  args = argparser.parse_args()

  db_hostname = "{{ db_hostname }}"
  db_database = "{{ db_database }}"
  db_username = "{{ db_username }}"
  db_password = "{{ db_password }}"
  db_sslmode = "{{ db_sslmode }}"
  
  smtp_server = "{{ smtp_server }}"
  smtp_port = {{ smtp_port }}
  smtp_username = "{{ smtp_username }}"
  smtp_password = "{{ smtp_password }}"

  db_conn = psycopg2.connect(database=db_database, user=db_username, password=db_password, host=db_hostname, sslmode=db_sslmode)
  smtp_conn = smtplib.SMTP(smtp_server, smtp_port)
  # create SSL context
  ssl_context = ssl.create_default_context()
  from_addr = "{{ from_addr }}"
  bcc_addr = "{{ bcc_addr }}"
  if args.dryrun:
    dry_run = True
  else:
    dry_run = False
  try:
    smtp_conn.starttls(context=ssl_context)
    smtp_conn.login(smtp_username, smtp_password)
    curdict = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM investor_payment_tbl WHERE id = %s', (args.investor_payment_id,))
    locale.setlocale(locale.LC_ALL, '')
    for row in curdict:
      # convert to mutable dictionary so we can add entries.
      investor_payment = dict(row)
      acct_num = str(investor_payment['recv_account_number'])
      investor_payment['trunc_acct_num'] = acct_num[0] + '*' * (len(acct_num) - 4) + acct_num[-3:]
      investor_payment['formatted_amount'] = locale.currency(investor_payment['amount'], grouping=True)
      to_addr = investor_payment['email']
      subject = render_tmpl('{{ local_basedir }}/share/email_subject.txt', investor_payment)
      body = render_tmpl('{{ local_basedir }}/share/email_body.txt', investor_payment)
      send_text_mail(smtp_conn, from_addr, to_addr, subject, body, bcc_addr=bcc_addr, dry_run=dry_run)
  except Exception as e:
    traceback.print_exc(file=sys.stdout)
  finally:
    db_conn.close()
    smtp_conn.quit()

def render_tmpl(tmpl_filename, prop_dict):
  """
  Render a Jinja template file with values for variables.

  Args:
    tmpl_filename (str): Filename of the Jinja template.
    prop_dict (dict): Dictionary containing the values that are referenced in the Jinja template.

  Returns:
    str: rendered template with values in place of Jinja variables.
  """
  with open(tmpl_filename, 'r') as tmpl_file:
    file_content = tmpl_file.read()
  template = jinja2.Template(file_content)
  return template.render(prop_dict)

def send_text_mail(smtp_conn, from_addr, to_addr, subject, body, date=None, cc_addr=None, bcc_addr=None, dry_run=False):
  """
  Send a plain text email using an established SMTP connection.

  Args:
    smtp_conn (smtplib.SMTP): SMTP connection instance created with smtplib.SMTP(smtp_server, smtp_port). Call smtp_conn.login() before passing.
    from_addr (str): The from address; can be just the email address or "First Last" <flast@mydomain.com>.
    to_addr (str): Comma-separated list of email recipients; shows up in To header of email.
    subject (str): Subject header of the email.
    body (str): Body of the email.
    date (:obj:`str`, optional): date in RFC2822 format; if omitted, uses current date and time in local time zone.
    cc_addr (:obj:`str`, optional): Comma-separated list of email recipients who will be copied; shows up in Cc header of email.
    bcc_addr (:obj:`str`, optional): Comma-separated list of email recipients who will be blind copied; does not show in header of email.
    dry_run (:obj:`bool`, optional): defaults to False; if True, does not actually send email, but instead prints the email to stdout.
  """
  if date is None:
    # generate RFC2822 date from current time in local time zone.
    date = email.utils.formatdate(localtime=True)
  header_list = []
  header_list.append("From: " + from_addr)
  header_list.append("To: " + to_addr)
  if cc_addr:
    header_list.append("Cc: " + cc_addr)
  header_list.append("Date: " + date)
  header_list.append("Subject: " + subject)
  message_body = "\n".join(header_list) + "\n\n" + body
  if dry_run:
    print(message_body)
    print("-"*80)
  else:
    # include to, cc, bcc in recipients. Include headers for to and cc (but not bcc) in message.
    recipient_list = [to_addr]
    if cc_addr:
      recipient_list += [cc_addr]
    if bcc_addr:
      recipient_list += [bcc_addr]
    smtp_conn.sendmail(from_addr, recipient_list, message_body)

if __name__ == "__main__":
  main(sys.argv[1:])

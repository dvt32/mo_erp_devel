include $(repo_root)/common/apache.mk
include $(repo_root)/common/firewall.mk
include $(repo_root)/common/func.mk
include $(repo_root)/common/postgresql.mk
include $(repo_root)/common/python.mk

.PHONY: websetup install clean

# libcurl4-openssl-dev needed by pycurl
deb_packages := libcurl4-openssl-dev incron
# Apache modules rewrite and ssl needed for SSL; headers needed for HSTS (in conjunction with SSL)
apache_modules := rewrite ssl headers
# pycurl needed for solupay library. jinja2 needed despite jinja2_pkg dependency because we need jinja2 in pip
netauth_pip_packages := Flask Flask-Login pycurl email_validator bcrypt jinja2 pytz python-dateutil Flask-WTF WTForms-components flask-cors

netauth_pip_pkgs_from_src := psycopg2

websetup: apache_pkg apache_start_on_boot ssl_certs web_postgresql_cert apache_wsgi_pkgs iptables_pkg iptables_conf python_dev_pkgs jinja2_pkg postgresql_repo postgresql_client_dev_pkgs
	$(call check_system_properties,netauth_basedir netauth_ipv4_addr netauth_fqdn netauth_wsgi_group_name netauth_wsgi_script_reloading)
ifneq ($(linux_distribution),debian)
	$(error only debian distribution is supported in websetup-netauth target)
endif
ifeq ($(is_production_deployment), 1)
ifeq ($(secret_base_dir),)
	$(error websetup-netauth requires secret_base_dir on production server (i.e. when is_production_deployment is 1))
else
	@$(call check_file_exist,$(secret_base_dir)/dbconf/root.crt)
endif
	$(call check_system_properties,netauth_ipv6_addr)
	@mkdir -p $(netauth_basedir)
# install required packages in APT repos
	@$(call install_packages,$(deb_packages))
# set up Python virtualenv with packages from PyPI
	@echo ..create Python virtualenv for netauth and install Python packages with pip
	@$(call install_virtualenv,$(netauth_basedir)/venv,$(netauth_pip_packages))
	@$(call install_pip_pkg_from_src,$(netauth_basedir)/venv,$(netauth_pip_pkgs_from_src))
# PostgreSQL SSL root certificate
	@echo ..copy PostgreSQL root.crt
	@mkdir -p $(netauth_basedir)/.postgresql
	@$(call copy_file,$(secret_base_dir)/dbconf/root.crt,$(netauth_basedir)/.postgresql/root.crt,root.root,0444)
# create tmpdir to store files to propagate state across shell lines
	$(eval $@_tmpdir := $(shell mktemp -d))
	@$(call install_apache_site,"$(CURDIR)/apacheconf/production/netauth.conf",netauth,$($@_tmpdir),$(system_properties))
	@$(call install_apache_site,"$(CURDIR)/apacheconf/production/netauth-ssl.conf",netauth-ssl,$($@_tmpdir),$(system_properties))
	@$(call enable_apache_modules,$(apache_modules),$($@_tmpdir))
	@$(call restart_apache_if_needed,$($@_tmpdir))
	@rmdir "$($@_tmpdir)"
	@echo Create DNS mapping from $(netauth_fqdn) to IP address \($(netauth_ipv4_addr)\) and \($(netauth_ipv6_addr)\).
else
# development deployment
# install required packages in APT repos
	@$(call install_packages,$(deb_packages))
# set up Python virtualenv with packages from PyPI
	@mkdir -p $(netauth_basedir)
	@echo ..create Python virtualenv for netauth and install Python packages with pip
	@$(call install_virtualenv,$(netauth_basedir)/venv,$(netauth_pip_packages))
	@$(call install_pip_pkg_from_src,$(netauth_basedir)/venv,$(netauth_pip_pkgs_from_src))
# create tmpdir to store files to propagate state across shell lines
	$(eval $@_tmpdir := $(shell mktemp -d))
	@$(call install_apache_site,"$(CURDIR)/apacheconf/devel/netauth.conf",netauth,$($@_tmpdir),$(system_properties))
	@$(call enable_apache_modules,$(apache_modules),$($@_tmpdir))
	@$(call restart_apache_if_needed,$($@_tmpdir))
	@rmdir "$($@_tmpdir)"
	@echo Create DNS mapping from $(netauth_fqdn) to IP address \($(netauth_ipv4_addr)\).
endif

install: jinja2_pkg
	$(call check_system_properties,netauth_basedir netauth_base_url netauth_db_host netauth_db_sslmode netauth_db_name netauth_db_username netauth_db_password netauth_flask_secret_key static_base_url pay_base_url outbound_mail_server register_attempt_email netauth_itsdangerous_secret_key)
	$(eval _file_list:=)
	@echo ..create deployment directories for command line clients
# use copy_file instead of copy_enqueue for share resources because they rely on Jinja for templating during operations
	@for i in bin lib share templates; do mkdir -p $(netauth_basedir)/$$i; chown www-data.www-data $(netauth_basedir)/$$i; done
	@echo ..create web deployment directories for netauth site
	@$(call copy_enqueue,$(CURDIR)/flask/application.wsgi,$(netauth_basedir)/application.wsgi,www-data.www-data,0755)
	@$(call copy_enqueue,$(CURDIR)/flask/route.py,$(netauth_basedir)/route.py,www-data.www-data,0755)
	@$(call copy_enqueue,$(CURDIR)/daemon/error_monitor.py,$(netauth_daemon_basedir)/bin/error_monitor.py,root.root,0755)
# create tmpdir to store files to propagate state across shell lines
	@$(eval $@_tmpdir := $(shell mktemp -d))
ifeq ($(is_production_deployment),1)
	@$(call copy_subst,$(CURDIR)/daemon/netauth_error_monitor,$($@_tmpdir)/netauth_error_monitor,root.root,0644,,$(repo_root)/system.properties)
	@cmp -s $($@_tmpdir)/netauth_error_monitor /etc/incron.d/netauth_error_monitor; \
	if [ $$? -gt 0 ]; then \
	  mv -f $($@_tmpdir)/netauth_error_monitor /etc/incron.d/netauth_error_monitor; \
	else \
	  rm -f $($@_tmpdir)/netauth_error_monitor; \
	fi
endif
	@rmdir "$($@_tmpdir)"
	@$(foreach file,$(wildcard $(CURDIR)/lib/*.py),$(call copy_enqueue,$(file),$(netauth_basedir)/lib/$(notdir $(file)),www-data.www-data,0755);)
	@$(foreach file,$(wildcard $(CURDIR)/bin/*.py),$(call copy_enqueue,$(file),$(netauth_basedir)/bin/$(notdir $(file)),www-data.www-data,0755);)
	@$(call copy_enqueue,$(repo_root)/payment/nmi/lib/tzconvert.py,$(netauth_basedir)/lib/tzconvert.py,www-data.www-data,0755)
# use copy_file (not copy_enqueue) for email templates because Jinja invoked at run-time
	@$(foreach file,$(wildcard $(CURDIR)/share/*),$(call copy_file,$(file),$(netauth_basedir)/share/$(notdir $(file)),www-data.www-data,0755);)
	@$(foreach file,$(wildcard $(repo_root)/pay_site/share/*),$(call copy_file,$(file),$(netauth_basedir)/share/$(notdir $(file)),www-data.www-data,0755);)
	@$(foreach file,$(wildcard $(repo_root)/payment/nmi/share/*),$(call copy_file,$(file),$(netauth_basedir)/share/$(notdir $(file)),www-data.www-data,0755);)
	@$(call copy_enqueue,$(repo_root)/pay_site/lib/cryptpw.py,$(netauth_basedir)/lib/cryptpw.py,www-data.www-data,0755)
# use copy_file (not copy_enqueue) for HTML templates because Jinja invoked at run-time rather than build-time
	@$(foreach file,$(wildcard $(CURDIR)/templates/*),$(call copy_file,$(file),$(netauth_basedir)/templates/$(notdir $(file)),www-data.www-data,0755);)
	@echo ..substitute properties in copied files
	@$(call subst_batch,$(system_properties))

clean:
	$(call check_system_properties,build_cache_dir netauth_basedir)
	@rm -fr $(netauth_basedir)
	@$(call rm_apache_site,netauth,$($@_tmpdir))
	@$(call rm_apache_site,netauth-ssl,$($@_tmpdir))
	@$(call restart_apache_if_needed,$($@_tmpdir))
	@rmdir "$($@_tmpdir)"

from flask import request, abort, session, redirect, url_for, flash, render_template, g
import flask_login
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, HiddenField, SelectField
from wtforms.fields.html5 import EmailField
from wtforms_components import If
from wtforms import validators, ValidationError
from email_validator import validate_email, EmailNotValidError
import psycopg2
import psycopg2.extras
import tzconvert
import database
import datetime
import calendar
import smtplib
import random
import decimal
import cryptpw
from user import User
from email.mime.text import MIMEText
import email.utils
import email.header
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired
import jinja2
import forms
import ip_sync
import string
import re
from urllib.parse import urlparse, urljoin

def is_safe_url(target):
  ref_url = urlparse(request.host_url)
  test_url = urlparse(urljoin(request.host_url, target))
  return test_url.scheme in ('http', 'https') and \
    ref_url.netloc == test_url.netloc

def login():
  form = forms.LoginForm()
  if request.method == 'GET':
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM building_link_tbl WHERE %s = ANY(public_ip_addrs)', (request.remote_addr,))
    if re.match('.*{{ netauth_base_url }}.*', request.url_root) == None:
      redirected = True
    else:
      redirected = False
    ip_sync.refresh_ip()
    return render_template('login.html', form=form, redirected=redirected)
  if not form.validate_on_submit():
    return render_template('login.html', form=form)
  form.email.data = form.email.data.strip().lower()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE email = %s', (form.email.data,))
  cust_pers = curdict.fetchone()
  if not cust_pers:
    flash('It seems you\'re not registered as a customer with us. You can request guest access from a member, or register with us at the front desk.', 'error')
    return render_template('login.html', form=form)
  if cust_pers['pay_ws_passwd'] == None:
    flash('It seems you\'re not registered for a website account with us. Please click the register link at the bottom to do so.', 'error')
    return render_template('login.html', form=form)
  if cust_pers and cryptpw.check_password(form.password.data, cust_pers['pay_ws_cryptpasswd']):
    user = User()
    user.id = cust_pers['id']
    user.cust_acct_id = cust_pers['cust_acct_id']
    user.email = cust_pers['email'].strip()
    curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (cust_pers['cust_acct_id'],))
    cust_acct = curdict.fetchone()
    if cust_acct['create_user_public'] == True:
      user.can_create_account = True
    elif cust_acct['primary_contact_cust_pers_id'] == cust_pers['id']:
      user.can_create_account = True
    else:
      user.can_create_account = False
    # if number of bcrypt rounds has increased, update the crypted password
    if not cryptpw.hash_param_match_default(cust_pers['pay_ws_cryptpasswd']):
      update_cryptpasswd(conn, form.password.data, cust_pers['id'])
    flask_login.login_user(user)
    next_url = request.values.get('next')
    if not is_safe_url(next_url):
      return flask.abort(400)
    return redirect(next_url or url_for('show_devices'))
  flash('Incorrect password. Try again.', 'error')
  return render_template('login.html', form=form)

def update_cryptpasswd(conn, password, cust_pers_id):
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  hashed_password = cryptpw.hash_password(password)
  curdict.execute('UPDATE cust_pers_tbl SET pay_ws_passwd = %s, pay_ws_cryptpasswd = %s WHERE id = %s', (password, hashed_password, cust_pers_id))
  conn.commit()
  
class RegisterForm(FlaskForm):
  first_name = StringField('First Name', validators=[validators.InputRequired()], render_kw={'placeholder':'First Name'})
  last_name = StringField('Last Name', validators=[validators.InputRequired()], render_kw={'placeholder':'Last Name'})
  building = SelectField('Building', validators=[validators.InputRequired()])
  email = EmailField('Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder':'Email'})
  submitForm = SubmitField('Register')

def get_register_form(**kwargs):
  form = RegisterForm(**kwargs)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT building_tbl.*, state_tbl.abbreviation FROM building_tbl JOIN state_tbl ON building_tbl.state = state_tbl.name WHERE building_tbl.is_active = %s AND building_tbl.type = %s', (True, 'officesuite'))
  form.building.choices = [(str(building['id']), building['address_1'] + ', ' + building['city'] + ', ' + building['abbreviation'] +
  ' ' + building['zip_code']) for building in curdict]
  return form
  
def register():
  if not g.user.can_create_account:
    flash('You do not have sufficient permissions to add employee accounts for your company.', 'error')
    return redirect('/')
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (g.user.cust_acct_id,))
  cust_acct = curdict.fetchone()
  company = cust_acct['name']
  if request.method == 'GET':
    email_addr = request.args.get('email')
    if email_addr:
      email_addr = email_addr.strip().lower()
      try:
        validated_email = validate_email(email_addr)
        email_addr = validated_email['email']
      except EmailNotValidError as e:
        flash(str(e), 'error')
        return render_template('register.html')
      form = get_register_form(email=email_addr)
      form.email.render_kw={'readonly': 'true'}
      return render_template("register.html", form=form, company=company)
    else:
      form = get_register_form()
      return render_template("register.html", form=form, company=company)
  form = get_register_form()
  if not form.validate_on_submit():
    return render_template("register.html", form=form, company=company)
  email_addr = form.email.data.strip().lower()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE email = %s', (email_addr,))
  cust_pers = curdict.fetchone()
  if not cust_pers:
    curdict.execute('INSERT INTO cust_pers_tbl (first_name, last_name, email, cust_acct_id) VALUES (%s, %s, %s, %s) RETURNING *', (form.first_name.data, form.last_name.data, email_addr, g.user.cust_acct_id))
    cust_pers = curdict.fetchone()
  else:
    flash('The person you wish to add already has an account with us! We\'ve sent them an email with instructions for how to reset their password.', 'recent')
  code = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
  curdict.execute('UPDATE netauth_reg_code_tbl SET code_expire_ts = now() WHERE cust_pers_id = %s', (cust_pers['id'],))
  curdict.execute("INSERT INTO netauth_reg_code_tbl (cust_pers_id, activation_code, code_expire_ts) VALUES (%s, %s, now() + interval '2 hours')", (cust_pers['id'], code))
  conn.commit()
  mail_body = '%s has added an employee account for you on our netauth site. Please visit {{ netauth_base_url }}/confirm_email and enter the code %s to create a password for your account.' % (g.user.name, code)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = 'billing@microoffice.com'
  mail_msg['To'] = email_addr
  mail_msg['Subject'] = email.header.Header('Register for Coalition Websites - Verify email', 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.starttls()
  mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
  mailer.send_message(mail_msg)
  mailer.quit()
  return render_template("register_new_thanks.html", email=email_addr, name=form.first_name.data + " " + form.last_name.data)
  
class ConfirmRegForm(FlaskForm):
  code = StringField('Registration Code', validators=[validators.InputRequired(), validators.Regexp(r'^([0-9A-Z]{6})$')], render_kw={'placeholder': 'Registration Code', 'maxlength':'6', 'minlength':'6'})
  submitForm = SubmitField('Submit')
  
class PasswordForm(FlaskForm):
  token = HiddenField('Token', validators=[validators.InputRequired()])
  password = PasswordField('New Password', validators=[validators.InputRequired()], render_kw={'placeholder': 'New Password'})
  confirm = PasswordField('Confirm Password', validators=[validators.InputRequired(), validators.EqualTo('password', message='Passwords do not match.')], render_kw={'placeholder': 'Confirm New Password'})
  submitForm = SubmitField('Submit')

class ForgotPasswordForm(FlaskForm):
  company = StringField('Company', validators=[validators.InputRequired()], render_kw={'placeholder': 'Company'})
  email = EmailField('Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder':'Email'})
  submitForm = SubmitField('Send Recovery Email')
  
def forgot_passwd():
  if request.method == "GET":
    form = ForgotPasswordForm()
    return render_template('forgot_password.html', form=form)
  else:
    form = ForgotPasswordForm()
    email_addr = form.email.data.strip().lower()
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM cust_pers_tbl WHERE trim(both ' ' from lower(email)) = %s', (email_addr,))
    cust_pers = curdict.fetchone()
    if cust_pers:
      if cust_pers['email'] != email_addr:
        curdict.execute('UPDATE cust_pers_tbl SET email = %s WHERE id = %s', (email_addr, cust_pers['id']))
      code = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
      curdict.execute('UPDATE netauth_reg_code_tbl SET code_expire_ts = now() WHERE cust_pers_id = %s', (cust_pers['id'],))
      curdict.execute("INSERT INTO netauth_reg_code_tbl (cust_pers_id, activation_code, code_expire_ts) VALUES (%s, %s, now() + interval '2 hours')", (cust_pers['id'], code))
      conn.commit()
      mail_body = 'This email is being sent to verify that you are the owner of %s. Please visit {{ netauth_base_url }}/confirm_email and enter the code %s to change the password for your account.' % (email_addr, code)
      mail_msg = MIMEText(mail_body)
      mail_msg['Date'] = email.utils.formatdate(localtime=True)
      mail_msg['From'] = 'billing@microoffice.com'
      mail_msg['To'] = email_addr
      mail_msg['Subject'] = email.header.Header('Reset Password for Coalition Websites - Verify email', 'utf-8')
      mailer = smtplib.SMTP('{{ outbound_mail_server }}')
      mailer.starttls()
      mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
      mailer.send_message(mail_msg)
      mailer.quit()
    else:
      mail_body = '''User %s, who works at %s, attempted to reset password on netauth.microoffice.com, but could not find cust_pers_tbl row. 
  ''' % (email_addr, form.company.data)
      mail_msg = MIMEText(mail_body)
      mail_msg['Date'] = email.utils.formatdate(localtime=True)
      mail_msg['From'] = 'billing@microoffice.com'
      mail_msg['To'] = '{{ register_attempt_email }}'
      mail_msg['Subject'] = email.header.Header('Register attempt on netauth.microoffice.com', 'utf-8')
      mailer = smtplib.SMTP('{{ outbound_mail_server }}')
      mailer.starttls()
      mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
      mailer.send_message(mail_msg)
      mailer.quit()
    return render_template("register_thanks.html", email=email_addr)
      
  
def confirm_email():
  if request.method == "GET":
    form = ConfirmRegForm()
    return render_template('register_confirm.html', form=form)
  elif request.method == "POST":
    form = ConfirmRegForm()
    if not form.validate_on_submit():
      return render_template('register_confirm.html', form=form)
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM netauth_reg_code_tbl WHERE activation_code = %s AND code_expire_ts > now()', (form.code.data,))
    code = curdict.fetchone()
    if not code:
      flash ('Registration Code is Invalid or Expired. You can try <a href="/register">registering again</a>.', 'error')
      return render_template('register_confirm.html', form=form)
    serializer = URLSafeTimedSerializer('{{ netauth_itsdangerous_secret_key }}')
    form_token = serializer.dumps(code['cust_pers_id'], salt='set_password')
    session['pwd_token'] = form_token
    return redirect( url_for('set_password') )

def setpasswd():
  form = PasswordForm()
  if request.method == 'GET':
    if 'pwd_token' not in session:
      flash('You must be registered for an account before setting a password.', 'error')
      return render_template("password.html", form=form)
    else:
      form.token.data = session['pwd_token']
      return render_template("password.html", form=form)
  if not form.validate_on_submit():
    if form.token.data == None:
      flash('You must be registered for an account before setting a password.', 'error')
    return render_template("password.html", form=form)
  token = form.token.data
  serializer = URLSafeTimedSerializer('{{ netauth_itsdangerous_secret_key }}')
  try:
    # form tokens from the set password form can exist for 120 minutes and then the user must request a new token
    cust_pers_id = serializer.loads(token, salt='set_password', max_age=7200)
  except BadSignature as e:
    flash(str(e), 'error')
    return render_template('password')
  password = form.password.data
  confirm_password = form.confirm.data
  hashed_password = cryptpw.hash_password(password)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('UPDATE cust_pers_tbl SET pay_ws_passwd = %s, pay_ws_cryptpasswd = %s WHERE id = %s', (password, hashed_password, cust_pers_id))
  conn.commit()
  del session['pwd_token']
  flash('You have successfully set your password. You may now login.', 'recent')
  return redirect( url_for("login") )

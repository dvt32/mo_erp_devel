from flask import request, session, abort, redirect, url_for, flash, render_template, g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, BooleanField, HiddenField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms_components import If
from wtforms import validators, ValidationError
from email_validator import validate_email, EmailNotValidError
import psycopg2
import psycopg2.extras
import tzconvert
import database
import datetime
import calendar
import smtplib
import random
import decimal
from email.mime.text import MIMEText
import email.utils
import email.header
from itsdangerous import URLSafeTimedSerializer, SignatureExpired, BadSignature
import jinja2
import forms
import json
import ip_sync
import pycurl
import urllib
import urllib.parse
import io
import string
import pytz


def get_router_url():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM building_link_tbl WHERE %s = ANY(public_ip_addrs)', (request.remote_addr,))
  router_ip = curdict.fetchone()
  if router_ip:
    router_url = "https://router-b" + str(router_ip['building_id']) + "c.microoffice.com"
  else:
    router_url = "NONE"
  return router_url

def get_devices():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM netauth_device_cust_tbl WHERE cust_acct_id = %s', (g.user.cust_acct_id,))
  dev_list = [dict(row) for row in curdict]
  for dev in dev_list:
    curdict.execute('SELECT * FROM netauth_device_pin_tbl WHERE netauth_device_id = %s', (dev['id'],))
    pin = curdict.fetchone()
    if pin:
      curdict.execute('SELECT building_tbl.*, state_tbl.abbreviation FROM building_tbl JOIN state_tbl ON building_tbl.state = state_tbl.name WHERE building_tbl.is_active = %s AND building_tbl.type = %s AND building_tbl.id = %s', (True, 'officesuite', pin['building_id']))
      building = curdict.fetchone()
      build = building['address_1'] + ', ' + building['city']
    else:
      build = "All"
    dev['building'] = build
    if dev['cust_pers_id']:
      curdict.execute('SELECT * FROM cust_pers_tbl WHERE id = %s', (g.user.id,))
      dev['cust_pers'] = dict(curdict.fetchone())
  return dev_list

def get_requests():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT netauth_device_guest_tbl.* FROM netauth_device_guest_tbl, netauth_access_code_tbl WHERE netauth_device_guest_tbl.cust_acct_id = %s AND (netauth_device_guest_tbl.cust_activated_ts IS NULL OR netauth_device_guest_tbl.guest_verified_ts IS NULL) AND netauth_device_guest_tbl.id = netauth_access_code_tbl.netauth_device_id AND netauth_access_code_tbl.code_expire_ts > now()', (g.user.cust_acct_id,))
  requests = [dict(row) for row in curdict]
  serializer = URLSafeTimedSerializer('{{ netauth_itsdangerous_secret_key }}')
  for request in requests:
    cust_token = serializer.dumps(request['id'], salt='activate_guest')
    request['token'] = cust_token
  return requests

def get_guests():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM netauth_device_guest_tbl WHERE cust_acct_id = %s AND expire_ts > now() AND cust_activated_ts IS NOT NULL AND guest_verified_ts IS NOT NULL', (g.user.cust_acct_id,))
  guest_list = [dict(row) for row in curdict]
  for guest in guest_list:
    curdict.execute('SELECT building_tbl.*, state_tbl.abbreviation FROM building_tbl JOIN state_tbl ON building_tbl.state = state_tbl.name WHERE building_tbl.is_active = %s AND building_tbl.type = %s AND building_tbl.id = %s', (True, 'officesuite', guest['building_id']))
    building = curdict.fetchone()
    build = building['address_1'] + ', ' + building['city']
    guest['building'] = build
    guest['expire_ts'] = tzconvert.utc_to_building_datetime(guest['expire_ts'], guest['building_id'], conn)
  return guest_list

def show():
  devices = get_devices()
  requests = get_requests()
  guests = get_guests()
  router_url = get_router_url()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s AND is_network_enabled = %s', (g.user.cust_acct_id, False))
  inactive = curdict.fetchone()
  if inactive:
    flash('Your network access has been suspended. Please see a Micro Office staff member for further details, or email memberservices@coalitionspace.com', 'error')
  ip_sync.refresh_ip()
  return render_template("devices.html", devices=devices, requests=requests, guests=guests, router_url=router_url, can_create_account=g.user.can_create_account)

def get_device_form(formtype, **kwargs):
  form = formtype(**kwargs)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT building_tbl.*, state_tbl.abbreviation FROM building_tbl JOIN state_tbl ON building_tbl.state = state_tbl.name WHERE building_tbl.is_active = %s AND building_tbl.type = %s', (True, 'officesuite'))
  form.building.choices = [(str(building['id']), building['address_1'] + ', ' + building['city'] + ', ' + building['abbreviation'] +
  ' ' + building['zip_code']) for building in curdict]
  form.building.choices.insert(0, ('All', 'All'))
  return form

def new_device():
  router_url = get_router_url()
  form = get_device_form(forms.DeviceForm)
  if (request.args):
    mac = request.args['mac']
    form.mac_address.data = mac
  return render_template("add.html", form=form, router_url=router_url)

def add_device():
  router_url = get_router_url()
  form = get_device_form(forms.DeviceForm)
  if not form.validate_on_submit():
    return render_template("add.html", form=form, router_url=router_url)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT COUNT(*) FROM netauth_device_cust_tbl WHERE cust_acct_id = %s', (g.user.cust_acct_id,))
  count = int(curdict.fetchone()[0])
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (g.user.cust_acct_id,))
  cust_acct = curdict.fetchone()
  if count >= int(cust_acct['max_net_device']):
    flash('Sorry, you have already reached the maximum device limit of ' + str(cust_acct['max_net_device']) + '. Please delete a current device if you wish to add another.', 'error')
    return redirect( url_for('show_devices') )
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s AND is_active = %s', (g.user.cust_acct_id, False))
  inactive = curdict.fetchone()
  if inactive:
    flash('You customer account has been suspended, so you cannot add any new devices. Please see a Micro Office staff member for further details.', 'error')
    return redirect( url_for('show_devices') )
  curdict.execute('SELECT * FROM netauth_device_cust_tbl WHERE mac_addr = %s', (form.mac_address.data.replace(':', ''),))
  exists = curdict.fetchone()
  if exists:
    flash('You\'ve already registered this device under the name %s! You can edit or delete it from this page.' % exists['label'], 'recent')
    return redirect( url_for('show_devices') )
  curdict.execute('INSERT INTO netauth_device_cust_tbl (cust_acct_id, cust_pers_id, label, mac_addr) VALUES (%s, %s, %s, %s) RETURNING *', (g.user.cust_acct_id, g.user.id, form.nickname.data, form.mac_address.data.replace(':', '')))
  device = curdict.fetchone()
  if form.building.data != "All":
    curdict.execute('INSERT INTO netauth_device_pin_tbl (netauth_device_id, building_id) VALUES (%s, %s)', (device['id'], form.building.data))
  conn.commit()
  ip_sync.refresh_ip()
  flash(device['id'], 'device_id')
  return redirect( url_for('show_devices') )
  
def quick_add():
  router_url = get_router_url()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT COUNT(*) FROM netauth_device_cust_tbl WHERE cust_acct_id = %s', (g.user.cust_acct_id,))
  count = int(curdict.fetchone()[0])
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (g.user.cust_acct_id,))
  cust_acct = curdict.fetchone()
  if count >= int(cust_acct['max_net_device']):
    flash('Sorry, you have already reached the maximum device limit of ' + str(cust_acct['max_net_device']) + '. Please delete a current device if you wish to add another.', 'error')
    return json.dumps("DONE")
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s AND is_active = %s', (g.user.cust_acct_id, False))
  inactive = curdict.fetchone()
  if inactive:
    flash('You customer account has been suspended, so you cannot add any new devices. Please see a Micro Office staff member for further details.', 'error')
    return json.dumps("DONE")
  # if there is no mac address?? TODO:
  curdict.execute('SELECT * FROM netauth_device_cust_tbl WHERE mac_addr = %s', (request.args['mac'].replace(':', ''),))
  exists = curdict.fetchone()
  if exists:
    flash('You\'ve already registered this device under the name %s! You can edit or delete it from this page.' % exists['label'], 'recent')
    return json.dumps("DONE")
  curdict.execute('INSERT INTO netauth_device_cust_tbl (cust_acct_id, cust_pers_id, label, mac_addr) VALUES (%s, %s, %s, %s) RETURNING *', (g.user.cust_acct_id, g.user.id, "device_" + request.args['mac'].replace(':', '')[-4:], request.args['mac'].replace(':', '')))
  conn.commit()
  device = curdict.fetchone()
  ip_sync.refresh_ip()
  flash(device['id'], 'device_id')
  return json.dumps("DONE")

def delete_device():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM netauth_device_cust_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, request.args['id']))
  device = curdict.fetchone()
  if not device:
    flash('You are unathorized to delete this device', 'error')
    return json.dumps("unauthorized")
  curdict.execute('DELETE FROM netauth_device_cust_tbl WHERE id = %s', (request.args['id'],))
  conn.commit()
  flash(device['label'] + ": " + device['mac_addr'], 'deleted')
  ip_sync.refresh_ip()
  return json.dumps("deleted")

def edit_device():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM netauth_device_cust_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, request.args['id']))
  device = curdict.fetchone()
  if not device:
    flash('You are unathorized to edit this device', 'error')
    return redirect( url_for('show_devices') )
  curdict.execute('SELECT * FROM netauth_device_pin_tbl WHERE netauth_device_id = %s', (device['id'],))
  pin = curdict.fetchone()
  if pin:
    building_id = pin['building_id']
  else:
    building_id = "All"
  form = get_device_form(forms.EditDeviceForm, device_id=device['id'], nickname=device['label'], mac_address=device['mac_addr'], building=building_id)
  form.mac_address.render_kw = {'readonly': 'true'}
  return render_template("edit.html", form=form)

def update_device():
  form = get_device_form(forms.EditDeviceForm)
  if not form.validate_on_submit():
    return render_template("edit.html", form=form)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM netauth_device_cust_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, form.device_id.data))
  device = curdict.fetchone()
  if not device:
    flash('You are unathorized to edit this device', 'error')
  curdict.execute('UPDATE netauth_device_cust_tbl SET label = %s WHERE id = %s', (form.nickname.data, device['id']))
  curdict.execute('SELECT * FROM netauth_device_pin_tbl WHERE netauth_device_id = %s', (device['id'],))
  pin = curdict.fetchone()
  if form.building.data == "All" and pin:
    curdict.execute('DELETE FROM netauth_device_pin_tbl WHERE netauth_device_id = %s', (device['id'],))
  if form.building.data != "All":
    if pin:
      curdict.execute('UPDATE netauth_device_pin_tbl SET building_id = %s WHERE netauth_device_id = %s', (form.building.data, device['id']))
    elif not pin:
      curdict.execute('INSERT INTO netauth_device_pin_tbl (netauth_device_id, building_id) VALUES (%s, %s)', (device['id'], form.building.data))
  conn.commit()
  ip_sync.refresh_ip()
  flash(device['id'], 'device_id')
  return redirect( url_for('show_devices') )

def delete_guest():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM netauth_device_guest_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, request.args['id']))
  guest = curdict.fetchone()
  if not guest:
    flash('Could not find guest device.', 'error')
    return json.dumps("not found")
  curdict.execute('UPDATE netauth_device_guest_tbl SET expire_ts = now() WHERE id = %s', (request.args['id'],))
  conn.commit()
  flash(guest['guest_first_name'] + " " + guest['guest_last_name'] + ": " + guest['mac_addr'], 'deleted_guest')
  return json.dumps("deleted")

def guest():
  if request.method == 'GET':
    form = forms.GuestForm()
    return render_template('guest.html', form=form)
  else: # POST
    form = forms.GuestForm()
    if not form.validate_on_submit():
      return render_template("guest.html", form=form)
    member_email = form.member_email.data.strip().lower()
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM cust_pers_tbl WHERE trim(both ' ' from lower(email)) = %s', (member_email,))
    cust_pers = curdict.fetchone()
    if not cust_pers: # couldn't find member with that email
      name = form.first_name.data + " " + form.last_name.data
      mail_body = '''User attempted to request guest access from netauth.microoffice.com, but could not find cust_pers_tbl row.

  Name: %s
  Member Email: %s
  ''' % (name, form.guest_email.data)
      mail_msg = MIMEText(mail_body)
      mail_msg['Date'] = email.utils.formatdate(localtime=True)
      mail_msg['From'] = 'billing@microoffice.com'
      mail_msg['To'] = '{{ register_attempt_email }}'
      mail_subject = 'Guest Access Attempt on netauth.microoffice.com'
      mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
      mailer = smtplib.SMTP('{{ outbound_mail_server }}')
      mailer.starttls()
      mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
      mailer.send_message(mail_msg)
      mailer.quit()
      flash("Sorry, we couldn't find a member with the email address %s. Please check with the member and try again." % member_email, 'error')
      return render_template("guest_thanks.html", member_email=member_email, guest_email=form.guest_email.data, success=False)
    else:
      curdict.execute('SELECT COUNT(*) FROM netauth_device_guest_tbl WHERE cust_acct_id = %s AND expire_ts > now()', (cust_pers['cust_acct_id'],))
      count = int(curdict.fetchone()[0])
      curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (cust_pers['cust_acct_id'],))
      cust_acct = curdict.fetchone()
      if count >= int(cust_acct['max_guest_device']):
        flash('Sorry, this member has already reached the maximum simultaneous guest limit of ' + str(cust_acct['max_guest_device']) + '. Please ask them to delete curent guest device in order to add yours.', 'error')
        return render_template("guest_thanks.html", member_email=member_email, guest_email=form.guest_email.data, success=False)
      guest_request = create_guest(form, cust_pers['cust_acct_id'], cust_pers['email'], False)
      if not guest_request:
        return render_template('guest.html', form=form)
      serializer = URLSafeTimedSerializer('{{ netauth_itsdangerous_secret_key }}')
      cust_token = serializer.dumps(guest_request['id'], salt='activate_guest')
      # send member email
      mail_body = 'This email is being sent to allow network access to %s %s. Please visit {{ netauth_base_url }}/activate_guest/%s to grant them network access.' % (form.first_name.data, form.last_name.data, cust_token)
      mail_msg = MIMEText(mail_body)
      mail_msg['Date'] = email.utils.formatdate(localtime=True)
      mail_msg['From'] = 'service@microoffice.com'
      mail_msg['To'] = member_email
      mail_subject = 'Allow Guest Access on Micro Office Network for %s %s' % (form.first_name.data, form.last_name.data)
      mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
      mailer = smtplib.SMTP('{{ outbound_mail_server }}')
      mailer.starttls()
      mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
      mailer.send_message(mail_msg)
      mailer.quit()
      return render_template("guest_thanks.html", member_email=member_email, guest_email=form.guest_email.data, success=True)

# takes in form (first name, last name, company, guest email), cust_acct_id, member_email
# returns dict_row of netauth_device_guest_tbl row, or False on error, which the calling function should handle
def create_guest(form, cust_acct_id, member_email, activated):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT netauth_device_guest_tbl.* FROM netauth_device_guest_tbl, netauth_access_code_tbl WHERE netauth_device_guest_tbl.guest_email = %s AND netauth_access_code_tbl.code_expire_ts > now() AND netauth_device_guest_tbl.cust_acct_id = %s AND netauth_device_guest_tbl.expire_ts > now() AND netauth_device_guest_tbl.id = netauth_access_code_tbl.netauth_guest_id', (form.guest_email.data, cust_acct_id))
  prev_guest_req = curdict.fetchone()
  if prev_guest_req:
    flash("This customer account already has an active request for this email.", 'error')
    return False
  curdict.execute("INSERT INTO netauth_device_guest_tbl (cust_acct_id, cust_pers_email, guest_first_name, guest_last_name, guest_company, guest_email) VALUES (%s, %s, %s, %s, %s, %s) RETURNING *", (cust_acct_id, member_email, form.first_name.data, form.last_name.data, form.company.data, form.guest_email.data))
  guest_request = curdict.fetchone()
  if activated:
    curdict.execute("UPDATE netauth_device_guest_tbl SET cust_activated_ts = now() WHERE id = %s RETURNING *", (guest_request['id'],))
    guest_request = curdict.fetchone()
  # insert code into table connected to guest request
  code = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
  curdict.execute("INSERT INTO netauth_access_code_tbl (netauth_guest_id, activation_code, duration, code_expire_ts) VALUES (%s, %s, %s, now() + interval '12 hours')", (guest_request['id'], code, '8 hours'))
  conn.commit()
  # send guest email
  mail_body = 'This email is being sent to grant you guest network access at Micro Office. To register, on the DEVICE YOU WISH TO GRANT ACCESS TO please visit {{ netauth_base_url }}/register_guest and enter the code %s.' % (code,)
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = 'service@microoffice.com'
  mail_msg['To'] = form.guest_email.data
  mail_subject = 'Register Guest Access on Micro Office Network for %s %s' % (form.first_name.data, form.last_name.data)
  mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.starttls()
  mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
  mailer.send_message(mail_msg)
  mailer.quit()
  return guest_request
  
def request_guest():
  if request.method == 'GET':
    form = forms.GuestRequestForm()
    return render_template('request_guest.html', form=form)
  else: # POST
    form = forms.GuestRequestForm()
    if not form.validate_on_submit():
      return render_template("request_guest.html", form=form)
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT COUNT(*) FROM netauth_device_guest_tbl WHERE cust_acct_id = %s AND expire_ts > now()', (g.user.cust_acct_id,))
    count = int(curdict.fetchone()[0])
    curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (g.user.cust_acct_id,))
    cust_acct = curdict.fetchone()
    if count >= int(cust_acct['max_guest_device']):
      flash("Sorry, you've already reached the maximum simultaneous guest limit of " + str(cust_acct['max_guest_device']) + ". Please delete a curent guest device in order to add a new one.", 'error')
      return render_template("guest_thanks.html", member_email=member_email, guest_email=form.guest_email.data, success=False)
    guest_request = create_guest(form, g.user.cust_acct_id, g.user.email, True)
    if not guest_request: # some error creating the guest happened, which will be handled with a flash message
      return render_template('request_guest.html', form=form)
    mail_body = 'You have just requested network access for %s %s. We\'ve sent them an email with instructions for how to register their device.' % (form.first_name.data, form.last_name.data)
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = 'service@microoffice.com'
    mail_msg['To'] = g.user.email
    mail_subject = 'Requested Guest Access on Micro Office Network for %s %s' % (form.first_name.data, form.last_name.data)
    mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.starttls()
    mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
    mailer.send_message(mail_msg)
    mailer.quit()
    flash('Network Access Requested for %s %s' % (form.first_name.data, form.last_name.data), 'recent')
    return redirect( url_for('show_devices') )

# checks if the customer account is allowed to add a device
def check_guest_allowed(cust_acct_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  # check if we've exceeded the max amount of devices
  curdict.execute('SELECT COUNT(*) FROM netauth_device_guest_tbl WHERE cust_acct_id = %s AND expire_ts > now()', (cust_acct_id,))
  count = int(curdict.fetchone()[0])
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (cust_acct_id,))
  cust_acct = curdict.fetchone()
  if count >= int(cust_acct['max_guest_device']):
    flash('Sorry, this customer account has already reached the maximum simultaneous guest limit of ' + str(cust_acct['max_guest_device']) + '. They must delete a curent guest device in order to add another.', 'error')
    return False
  # check if network enabled
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s AND is_network_enabled = %s', (cust_acct_id, False))
  inactive = curdict.fetchone()
  if inactive:
    flash('This customer account\'s network access has been suspended, so they cannot add any guest devices.', 'error')
    return False
  return True  
  
def register_guest(): #for the guest
  router_url = get_router_url()
  if router_url == "NONE":
    flash('It seems you aren\'t currently connected to our WiFi, so we cannot properly grant this device a network access. If you are on your phone, you are probably connected to cellular data. If you wish to grant access to your COMPUTER, please visit this site there.', 'error')
    return render_template('register_guest.html')
  if request.method == "GET":
    form = forms.GuestRegisterForm()
    # for http redirect fetching of mac address, when javascript doesn't work
    if (request.args):
      mac = request.args['mac']
      form.mac_address.data = mac
    return render_template('register_guest.html', form=form, router_url=router_url)
  # POST
  form = forms.GuestRegisterForm()
  if not form.validate_on_submit():
    return render_template("register_guest.html", form=form, router_url=router_url)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SELECT * FROM netauth_access_code_tbl WHERE activation_code = '%s' AND code_expire_ts > now()" % (form.activation_code.data))
  code_entry = curdict.fetchone()
  if not code_entry:
    flash('Invalid or Expired Activation Code. Have your member request a new one or go <a href="/guest">here</a> to register yourself.', 'error')
    return render_template("register_guest.html", form=form, router_url=router_url)
  # get unactivated guest requests, check if you can't find this one
  curdict.execute('SELECT * FROM netauth_device_guest_tbl WHERE id = %s AND guest_verified_ts IS NULL', (code_entry['netauth_device_id'],))
  guest_req = curdict.fetchone()
  if not guest_req:
    flash('Could not find guest request. Have your member request a new one or go <a href="/guest">here</a> to register yourself.', 'error')
    return render_template("register_guest.html", form=form, router_url=router_url)
  # check for max devices and network allowed
  if not check_guest_allowed(guest_req['cust_acct_id']):
    return render_template("register_guest.html", form=form, router_url=router_url)
  # update entry, verify guest
  curdict.execute('SELECT * FROM building_link_tbl WHERE %s = ANY(public_ip_addrs)', (request.remote_addr,))
  router_ip = curdict.fetchone()
  nickname = form.nickname.data or guest_req['guest_first_name'] + '\'s dev_' + form.mac_address.data.replace(':', '')[-4:]
  curdict.execute("UPDATE netauth_device_guest_tbl SET (mac_addr, guest_verified_ts, building_id, label) = (%s, now(), %s, %s) WHERE id = %s RETURNING *", (form.mac_address.data.replace(':', ''), router_ip['building_id'], nickname, guest_req['id'],))
  if guest_req['cust_activated_ts'] == None:
    cust_activated = False
  else:
    curdict.execute("UPDATE netauth_device_guest_tbl SET expire_ts = cust_activated_ts + interval %s WHERE id = %s RETURNING *", (code_entry['duration'], guest_req['id'],))
    cust_activated = True
  guest_req = curdict.fetchone()
  curdict.execute('UPDATE netauth_access_code_tbl SET code_expire_ts = now() WHERE id = %s', (code_entry['id'],))
  conn.commit()
  ip_sync.refresh_ip()
  this_guest = dict(guest_req)
  return render_template('activated.html', cust_activated=cust_activated, this_guest=this_guest)
  
def activate_guest(token): # for our customer
  serializer = URLSafeTimedSerializer('{{ netauth_itsdangerous_secret_key }}')
  try:
    # guest access tokens
    request_id = serializer.loads(token, salt='activate_guest', max_age=7200)
  except SignatureExpired as e:
    flash(str(e), 'error')
    return render_template('activated.html')
  except BadSignature as e:
    flash(str(e), 'error')
    return render_template('activated.html')
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  # find the current request
  curdict.execute('SELECT netauth_device_guest_tbl.* FROM netauth_device_guest_tbl, netauth_access_code_tbl WHERE netauth_device_guest_tbl.id = %s AND netauth_device_guest_tbl.cust_acct_id = %s and netauth_device_guest_tbl.id = netauth_access_code_tbl.netauth_guest_id AND netauth_access_code_tbl.code_expire_ts > now()', (request_id, g.user.cust_acct_id))
  guest_req = curdict.fetchone()
  if guest_req['cust_activated_ts'] != None:
    flash("You've already activated this guest device!", "recent")
    return render_template('activated.html', guest=guest)
  if not guest_req:
    flash('Could not find guest request entry under your account. Either you are logged in as the wrong user, or the request has expired.')
    return render_template('activated.html')
  # check for max devices and network allowed
  if not check_guest_allowed(g.user.cust_acct_id):
    return render_template('activated.html')
  curdict.execute('UPDATE netauth_device_guest_tbl SET cust_activated_ts = now() WHERE id = %s', (guest_req['id'],))
  # update depending on who has confirmed so far
  if not guest_req['guest_verified_ts']:
    curdict.execute("UPDATE netauth_device_guest_tbl SET expire_ts = now() + interval '15 minutes' WHERE id = %s", (guest_req['id'],))
  else:
    curdict.execute("UPDATE netauth_device_guest_tbl SET expire_ts = now() + interval '8 hours' WHERE id = %s", (guest_req['id'],))
  conn.commit()
  ip_sync.refresh_ip()
  guest = dict(guest_req)
  return render_template('activated.html', guest=guest)

def deactivate_guest():
  router_url = get_router_url()
  if router_url == "NONE":
    flash('It seems you aren\'t currently connected to our WiFi, so we cannot properly remove this device from network access. However, the member for whom you are a guest can always remove guests at any time.', 'error')
    return render_template('deactivate_guest.html')
  if request.method == "GET":
    form = forms.GuestDeactivateForm()
    # for http redirect fetching of mac address, when javascript doesn't work
    if (request.args):
      mac = request.args['mac']
      form.mac_address.data = mac
    return render_template('deactivate_guest.html', form=form, router_url=router_url)
  # POST
  form = forms.GuestDeactivateForm()
  if not form.validate_on_submit():
    return render_template("deactivate_guest.html", form=form, router_url=router_url)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  # get unactivated guest requests, check if you can't find this one
  curdict.execute('SELECT * FROM netauth_device_guest_tbl WHERE mac_addr = %s AND expire_ts > now()', (form.mac_address.data.replace(':', ''),))
  guest_dev = curdict.fetchone()
  if not guest_dev:
    flash("Could not find guest device. It either doesn't exist or has already expired.", 'error')
    return render_template("deactivate_guest.html", form=form, router_url=router_url)
  # check for max devices and network allowed
  # update entry, verify guest
  curdict.execute("UPDATE netauth_device_guest_tbl SET expire_ts = now() WHERE id = %s RETURNING *", (guest_dev['id'],))
  conn.commit()
  ip_sync.refresh_ip()
  flash('Guest Device %s Removed.' % (guest_dev['mac_addr']), 'recent')
  return redirect( url_for('login') )

def temporary(): #for the temporary user
  router_url = get_router_url()
  if router_url == "NONE":
    flash('It seems you aren\'t currently connected to our WiFi, so we cannot properly grant this device a network access. If you are on your phone, you are probably connected to cellular data. If you wish to grant access to your COMPUTER, please visit this site there.', 'error')
    return render_template('temporary_add.html')
  if request.method == "GET":
    form = forms.TemporaryAccessForm()
    # for http redirect fetching of mac address, when javascript doesn't work
    if (request.args):
      mac = request.args['mac']
      form.mac_address.data = mac
    return render_template('temporary_add.html', form=form, router_url=router_url)
  # POST
  form = forms.TemporaryAccessForm()
  if not form.validate_on_submit():
    return render_template("temporary_add.html", form=form, router_url=router_url)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SELECT * FROM netauth_access_code_tbl WHERE activation_code = '%s' AND code_expire_ts > now()" % (form.activation_code.data.upper()))
  code_entry = curdict.fetchone()
  if not code_entry:
    flash('Invalid or Expired Activation Code. Please request a new one from the front desk.', 'error')
    return render_template("temporary_add.html", form=form, router_url=router_url)
  if code_entry['not_before_ts'] > datetime.datetime.now(pytz.timezone('UTC')):
    flash('This code is not yet valid. It will be valid after %s. Please request a new one from the front desk.' % (code_entry['not_before_ts']), 'error')
    return render_template("temporary_add.html", form=form, router_url=router_url)
  # get unactivated guest requests, check if you can't find this one
  curdict.execute('SELECT external_daypass_user_tbl.* FROM external_daypass_user_tbl, netauth_device_transient_tbl WHERE netauth_device_transient_tbl.id = %s AND external_daypass_user_tbl.id = netauth_device_transient_tbl.external_daypass_user_id', (code_entry['netauth_device_id'],))
  temp_user = curdict.fetchone()
  if not temp_user:
    flash('Could not find user associated with access code. Please request a new one at the front desk.', 'error')
    return render_template("temporary_add.html", form=form, router_url=router_url)
  curdict.execute('SELECT * FROM netauth_device_transient_tbl WHERE mac_addr = %s and expire_ts > now()', (form.mac_address.data.replace(':', ''),))
  already_in_mac = curdict.fetchone()
  if already_in_mac:
    flash('This device is already registered under this account for internet access right now.', 'error')
    return render_template("temporary_add.html", form=form, router_url=router_url)
  curdict.execute('SELECT * FROM netauth_device_transient_tbl WHERE netauth_device_transient_tbl.id = %s', (code_entry['netauth_device_id'],))
  temp_dev = curdict.fetchone()
  if not temp_dev:
    flash('Could not create device for access code, please ask for another at the front desk.', 'error')
    return render_template("temporary_add.html", form=form, router_url=router_url)
  curdict.execute('SELECT * FROM building_link_tbl WHERE %s = ANY(public_ip_addrs)', (request.remote_addr,))
  router_ip = curdict.fetchone()
  curdict.execute('SELECT * FROM building_tbl WHERE id = %s', (router_ip['building_id'],))
  building = curdict.fetchone()
  curdict.execute('SELECT * FROM building_tbl WHERE id = %s', (temp_dev['building_id'],))
  dev_building = curdict.fetchone()
  if building and dev_building and building['id'] != dev_building['id']:
    flash('This code is valid for %s, but you seem to be at %s. Please check with the front desk if you think this is wrong.' % (building['address_1'] + ', ' + building['city'] + ', ' + building['abbreviation'] +
    ' ' + building['zip_code'], dev_building['address_1'] + ', ' + dev_building['city'] + ', ' + dev_building['abbreviation'] +
    ' ' + dev_building['zip_code']), 'error')
    return render_template("temporary_add.html", form=form, router_url=router_url)
  if form.nickname.data:
    nickname = form.nickname.data
  elif temp_user['first_name']:
    nickname =  temp_user['first_name'] + '\'s dev_' + form.mac_address.data.replace(':', '')[-4:]
  else:
    nickname = 'dev_' + form.mac_address.data.replace(':', '')[-4:]
  if code_entry['duration']:
    expire = datetime.datetime.now(pytz.timezone('UTC')) + code_entry['duration']
  elif code_entry['relative_end']:
    if code_entry['relative_end'] == 'day':
      expire = datetime.datetime.now(pytz.timezone(building['timezone']))
      expire = expire.replace(hour=23,minute=59,second=59)
      expire = expire.astimezone(pytz.timezone('UTC'))
    elif code_entry['relative_end'] == 'week':
      expire = datetime.datetime.now(pytz.timezone(building['timezone']))
      expire = expire.replace(hour=23,minute=59,second=59)
      expire += datetime.timedelta(days=7)
      expire = expire.astimezone(pytz.timezone('UTC'))
    elif code_entry['relative_end'] == 'month':
      expire = datetime.datetime.now(pytz.timezone(building['timezone']))
      expire = expire.replace(hour=23,minute=59,second=59)
      expire += datetime.timedelta(months=1)
      expire = expire.astimezone(pytz.timezone('UTC'))
  else:
    expire = datetime.datetime.now(pytz.timezone(building['timezone']))
    expire = expire.replace(hour=23,minute=59,second=59)
    expire = expire.astimezone(pytz.timezone('UTC'))
  curdict.execute("UPDATE netauth_device_transient_tbl SET (label, building_id, mac_addr, activated_ts, expire_ts) = (%s, %s, %s, now(), %s) WHERE id = %s RETURNING *", (nickname, router_ip['building_id'], form.mac_address.data.replace(':', ''), expire, temp_dev['id']))
  temp_dev = curdict.fetchone()
  curdict.execute('UPDATE netauth_access_code_tbl SET (code_expire_ts, netauth_device_id) = (now(), %s) WHERE id = %s', (temp_dev['id'], code_entry['id'],))
  expire = expire.astimezone(pytz.timezone(building['timezone']))
  session['expire'] = expire
  session['timezone'] = building['timezone']
  conn.commit()
  ip_sync.refresh_ip()
  if not temp_user['email']:
    session['external_user'] = temp_user['id']
    return redirect( url_for('new_external') )
  return render_template('temporary_activated.html', expire=expire)
  
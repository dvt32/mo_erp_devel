from flask import request, abort, session, redirect, url_for, flash, render_template, g, jsonify
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, HiddenField
from wtforms.fields.html5 import EmailField
from wtforms_components import If
from wtforms import validators, ValidationError
from email_validator import validate_email, EmailNotValidError
import psycopg2
import psycopg2.extras
import tzconvert
import pytz
import datetime
import database
import smtplib
import cryptpw
from email.mime.text import MIMEText
import email.utils
import email.header
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired
import forms
import ip_sync
import pprint

def is_client_allowed():
  pp = pprint.PrettyPrinter(indent=4)
  pp.pprint(request.__dict__)
  pp.pprint(request.authorization)
  pp.pprint(request.headers)
  pp.pprint(request.data)
  pp.pprint(request.values)
  pp.pprint(request.get_json())
  pp.pprint(request.get_json(force=True))
  auth = request.authorization
  if not auth:
    return False
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM leadsource_tbl WHERE username = %s', (auth.username,))
  client = curdict.fetchone()
  g.client = client
  if client and cryptpw.check_password(auth.password, client['hashed_passwd']):
    return True
  else:
    return False

def check_in():
  if not is_client_allowed():
    return jsonify({'error': 'Client authorization not valid'})
  data = request.get_json()
  if not data:
    return jsonify({'error': 'no data received'})
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM building_tbl WHERE croissant_id = %s', (data['place_id'],))
  building = curdict.fetchone()
  curdict.execute('SELECT * FROM external_daypass_user_tbl WHERE external_id = %s', (data['user_id'],))
  external_user = curdict.fetchone()
  if not external_user:
    curdict.execute('INSERT INTO external_daypass_user_tbl (leadsource_id, external_id, building_id) VALUES (%s, %s, %s) RETURNING *', (g.client['id'], data['user_id'], building['id'])) 
    external_user = curdict.fetchone()
  expire = datetime.datetime.now(pytz.timezone(building['timezone']))
  expire = expire.replace(hour=23,minute=59,second=59)
  expire = expire.astimezone(pytz.timezone('UTC'))
  not_before = datetime.datetime.now(pytz.timezone('UTC'))
  codes = []
  for i in range(2):
    code = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
    codes.append(code)
    curdict.execute("INSERT INTO netauth_device_transient_tbl (external_daypass_user_id, building_id, visit_id) VALUES (%s, %s, %s) RETURNING *", (external_user['id'], building['id'], data['visit_id']))
    dummy_dev = curdict.fetchone()
    curdict.execute("INSERT INTO netauth_access_code_tbl (netauth_device_id, activation_code, relative_end, code_expire_ts, not_before_ts) VALUES (%s, %s, 'day', %s, %s)", (dummy_dev['id'], code, expire, not_before))
  conn.commit()
  return jsonify({'access_codes' : codes })

def check_out():
  if not is_client_allowed():
    return jsonify({'error': 'Client authorization not valid'})
  data = request.get_json()
  if not data:
    return jsonify({'error': 'no data received'})
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM building_tbl WHERE croissant_id = %s', (data['place_id'],))
  building = curdict.fetchone()
  curdict.execute('SELECT * FROM external_daypass_user_tbl WHERE external_id = %s', (data['user_id'],))
  external_user = curdict.fetchone()
  if not external_user:
    return jsonify({'error' : 'User not found'})
  curdict.execute('UPDATE netauth_device_transient_tbl SET expire_ts = now() WHERE visit_id = %s', (data['visit_id'],))
  conn.commit()
  ip_sync.refresh_ip(building['id'])
  return jsonify({'success'})

def new_external():
  form = forms.NewExternalForm()
  expire = session['expire'].replace(tzinfo=pytz.timezone('UTC')).astimezone(pytz.timezone(session['timezone']))
  if request.method == 'GET':
    if not session['external_user']:
      return render_template('temporary_activated.html', expire=expire)
    return render_template('new_external.html', form=form)
  else: # POST
    if not session['external_user']:
      flash('Could not link information to user. Internet access is still granted though.', 'error')
      return render_template('temporary_activated.html', expire=expire)
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    if form.first_name.data:
      curdict.execute('UPDATE external_daypass_user_tbl SET first_name = %s WHERE id = %s', (form.first_name.data, session['external_user']))
    if form.last_name.data:
      curdict.execute('UPDATE external_daypass_user_tbl SET last_name = %s WHERE id = %s', (form.last_name.data, session['external_user']))
    if form.company.data:
      curdict.execute('UPDATE external_daypass_user_tbl SET company = %s WHERE id = %s', (form.company.data, session['external_user']))
    if form.email.data:
      curdict.execute('UPDATE external_daypass_user_tbl SET email = %s WHERE id = %s', (form.email.data, session['external_user']))
    conn.commit()
    return render_template('temporary_activated.html', expire=expire)
    
  
  
  
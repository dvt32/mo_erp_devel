from flask import request, session, abort, redirect, url_for, flash, render_template, g
import psycopg2
import psycopg2.extras
import database
import datetime
import calendar
from itsdangerous import URLSafeTimedSerializer, TimedSerializer, SignatureExpired, BadSignature
import json
import pycurl
import urllib
import urllib.parse
import io

def refresh_ip(building_id=None):
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST, 1)
  # set connect timeout to 15 seconds
  curlObj.setopt(pycurl.CONNECTTIMEOUT, 15)
  # set timeout to 15 seconds
  curlObj.setopt(pycurl.TIMEOUT, 15)
  curlObj.setopt(pycurl.HEADER, 0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION, responseIO.write)
  if building_id:
    url = router_url = "https://router-b" + building_id + "n.microoffice.com/update_ip"
  else:
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM building_link_tbl WHERE %s = ANY(public_ip_addrs)', (request.remote_addr,))
    router_ip = curdict.fetchone()
    if router_ip:
      url = router_url = "https://router-b" + str(router_ip['building_id']) + "n.microoffice.com/update_ip"
    else:
      url = None
  if url == None:
    flash('Could not refresh network access rules', 'error')
    result = "NOT SURE WHERE TO SEND REQUEST"
  else:
    curlObj.setopt(pycurl.URL, url)
    try:
      curlObj.perform()
      data = responseIO.getvalue().decode('utf-8')
      result = json.loads(data)
    except:
      flash('Could not refresh network access rules', 'error')
      result = "FAILED"
  return result

import sys
sys.path.append('/var/www/sites/{{ netauth_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ netauth_fqdn }}/lib')
from flask import Flask
from flask import request, session, abort, redirect, url_for, flash, render_template, g
from user import User
import psycopg2
import psycopg2.extras
import flask_login
import database
import auth
import devices
import external
import ip_sync #DELETE

app = Flask(__name__)
app.secret_key = '{{ netauth_flask_secret_key }}'
app.debug = {{ netauth_flask_app_debug }}

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
# name of function to run when login_required is triggered and no user is logged in
login_manager.login_view = 'login'

@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@app.context_processor
def define_static_url():
  return dict(static_base_url='{{ static_base_url }}')

@app.before_request
def before_request():
  g.user = flask_login.current_user

@login_manager.user_loader
def user_loader(id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE id = %s', (id,))
  cust_pers = curdict.fetchone()
  if not cust_pers:
    return
  user = User()
  user.id = id
  user.cust_acct_id = cust_pers['cust_acct_id']
  user.email = cust_pers['email'].strip()
  user.name = cust_pers['first_name'] + ' ' + cust_pers['last_name']
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (cust_pers['cust_acct_id'],))
  cust_acct = curdict.fetchone()
  if cust_acct['create_user_public'] == True:
    user.can_create_account = True
  elif cust_acct['primary_contact_cust_pers_id'] == cust_pers['id']:
    user.can_create_account = True
  else:
    user.can_create_account = False
  return user

@app.route('/login', methods=['GET', 'POST'])
def login():
  return auth.login()

@app.route('/logout')
@flask_login.login_required
def logout():
  flask_login.logout_user()
  return redirect(url_for('login'))

@app.route('/')
@flask_login.login_required
def show_devices():
  return devices.show()

@app.route('/register', methods=['GET', 'POST'])
@flask_login.login_required
def register():
  return auth.register()

@app.route('/guest', methods=['GET'])
def guest():
  return devices.guest()

@app.route('/add_guest', methods=['POST'])
def add_guest():
  return devices.guest()

@app.route('/confirm_email', methods=['GET', 'POST'])
def confirm_email():
  return auth.confirm_email()

@app.route('/forgot_password', methods=['GET','POST'])
def forgot_password():
  return auth.forgot_passwd()
  
@app.route('/set_password', methods=['GET','POST'])
def set_password():
  return auth.setpasswd()

@app.route('/add_device', methods=['POST'])
@flask_login.login_required
def add_device():
  return devices.add_device()
  
@app.route('/quick_add', methods=['POST'])
@flask_login.login_required
def quick_add():
  return devices.quick_add()

@app.route('/new_device')
@flask_login.login_required
def new_device():
  return devices.new_device()

@app.route('/delete_device', methods=["POST"])
@flask_login.login_required
def delete_device():
  return devices.delete_device()

@app.route('/delete_guest', methods=["POST"])
@flask_login.login_required
def delete_guest():
  return devices.delete_guest()

@app.route('/edit_device')
@flask_login.login_required
def edit_device():
  return devices.edit_device()

@app.route('/update_device', methods=["POST"])
@flask_login.login_required
def update_device():
  return devices.update_device()

@app.route('/activate_guest/<token>')
@flask_login.login_required
def activate_guest(token):
  return devices.activate_guest(token)
  
@app.route('/request_guest', methods=["GET", "POST"])
@flask_login.login_required
def request_guest():
  return devices.request_guest()
  
@app.route('/register_guest', methods=["GET", "POST"])
def register_guest():
  return devices.register_guest()
  
@app.route('/deactivate_guest', methods=["GET", "POST"])
def deactivate_guest():
  return devices.deactivate_guest()
  
@app.route('/temporary', methods=["GET", "POST"])
def temporary():
  return devices.temporary()
  
@app.route('/refresh') #DELETE
def refresh():
  return ip_sync.refresh_ip()

@app.route('/external_check_in', methods=['GET', 'POST'])
def external_check_in():
  return external.check_in()

@app.route('/external_check_out', methods=['GET', 'POST'])
def external_check_out():
  return external.check_out()

@app.route('/new_external', methods=['GET', 'POST'])
def new_external():
  return external.new_external()

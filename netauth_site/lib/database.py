from flask import g
import psycopg2

def get_connection():
  conn = getattr(g, '_database', None)
  if conn is None:
    conn = g._database = psycopg2.connect(database='{{ netauth_db_name }}', user='{{ netauth_db_username }}', password='{{ netauth_db_password }}', host='{{ netauth_db_host }}', sslmode='{{ netauth_db_sslmode }}')
  return conn

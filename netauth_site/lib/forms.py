from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, PasswordField, SelectField
from wtforms.fields.html5 import EmailField
from wtforms_components import If
from wtforms import validators, ValidationError


class GuestRequestForm(FlaskForm):
  first_name = StringField('First Name', validators=[validators.InputRequired()], render_kw={'placeholder':'First Name'})
  last_name = StringField('Last Name', validators=[validators.InputRequired()], render_kw={'placeholder':'Last Name'})
  company = StringField('Company', validators=[validators.InputRequired()], render_kw={'placeholder': 'Company'})
  guest_email = EmailField('Guest Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder':'Guest\'s Email'})
  submitForm = SubmitField('Request Access')
  
class NewExternalForm(FlaskForm):
  first_name = StringField('First Name', validators=[validators.Optional()], render_kw={'placeholder':'First Name'})
  last_name = StringField('Last Name', validators=[validators.Optional()], render_kw={'placeholder':'Last Name'})
  company = StringField('Company', validators=[validators.Optional()], render_kw={'placeholder': 'Company'})
  email = EmailField('Email', validators=[validators.Optional(), validators.Email()], render_kw={'placeholder':'Email'})
  submitForm = SubmitField('Save Information')
  
class GuestForm(GuestRequestForm):
  member_email = EmailField('Member Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder':'Member\'s Email'})
  guest_email = EmailField('Your Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder':'Your Email'})

class LoginForm(FlaskForm):
  email = EmailField('Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder': 'Email'})
  password = PasswordField('Password', validators=[validators.InputRequired()], render_kw={'placeholder': 'Password'})
  submitForm = SubmitField('Login')

class GuestDeactivateForm(FlaskForm):
  mac_address = StringField('MAC Address', validators=[validators.InputRequired(), validators.Regexp(r'^([0-9A-Fa-f]{2}[:-]?){5}([0-9A-Fa-f]{2})$')], render_kw={'placeholder': 'MAC Address'})
  submitForm = SubmitField('Deactivate')
  
class GuestRegisterForm(GuestDeactivateForm):
  activation_code = StringField('Activation Code', validators=[validators.InputRequired(), validators.Regexp(r'^([0-9A-Z]{6})$')], render_kw={'placeholder': 'Activation Code', 'maxlength':'6', 'minlength':'6'})
  nickname = StringField('Nickname', validators=[validators.Optional()], render_kw={'placeholder': 'Nickname for device'})
  submitForm = SubmitField('Register')

class TemporaryAccessForm(GuestRegisterForm):
  submitForm = SubmitField('Get Access')
  
class DeviceForm(FlaskForm):
  nickname = StringField('Nickname', validators=[validators.InputRequired()], render_kw={'placeholder': 'Nickname for device'})
  mac_address = StringField('MAC Address', validators=[validators.InputRequired(), validators.Regexp(r'^([0-9A-Fa-f]{2}[:-]?){5}([0-9A-Fa-f]{2})$')], render_kw={'placeholder': 'MAC Address'})
  building = SelectField('Pin to Specific Location', validators=[validators.InputRequired()], default='All')
  add = SubmitField('Add Device')

class EditDeviceForm(DeviceForm):
  device_id = HiddenField('Device ID', validators=[validators.InputRequired()])
  edit = SubmitField('Submit')

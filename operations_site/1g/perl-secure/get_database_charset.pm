use strict;
local($^W) = 1;

sub get_database_charset($$) {
  my ($instance_map, $handle_name) = @_;
  my $charset;
  if ($handle_name eq "main") {
    $charset = "utf8"; # UTF-8
  } elsif ($handle_name eq "phone") {
    $charset = "iso-8859-15"; # ISO-8859-15 AKA Latin-9
  } else {
    die "get_database_charset: bad handle name ($handle_name)\n";
  }
  return $charset;
}

1;

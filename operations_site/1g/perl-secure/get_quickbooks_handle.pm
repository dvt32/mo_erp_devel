use strict;
local($^W) = 1;

use SOAP::Lite;
use is_production_env;

# Ideally, we would not need a manually specified WSDL file, but
# QuickBooks RDS is retarded and does not return the FQDN of the RDS
# server host.
my $wsdl_file_name = "file:QBXMLRemote.wsdl";
# If and when Intuit fixes the hostname FQDN issue, we can
# retrieve the WSDL from QuickBooks RDS directly. Uncomment the
# following line to do so.
#my $service_wsdl_address = "https://hephaestus.loc2.microoffice.us:3790/QBXMLRemote?wsdl";
my $app_id = "";
my $app_name = "Micro Office Operations Center";
my $login_name = "{{ quickbooks_login_name }}";
my $password = "{{ quickbooks_password }}";
#my $qb_file_name = "M:\\finance\\QuickBooks\\Application Data\\Micro Office Management.QBW";
#my $req_file_mod = "multiUser";
my $qb_file_name = "C:\\Documents and Settings\\wchao\\My Documents\\Micro Office Management.QBW";
my $req_file_mod = "singleUser";

sub get_quickbooks_handle($$) {
  my ($instance_map, $handle_name) = @_;
  if (!$instance_map->{"quickbooks"}) {
    $instance_map->{"quickbooks"} = {};
  }
  my $quickbooks_map;
  if (!($quickbooks_map = $instance_map->{"quickbooks"}->{$handle_name})) {
    my ($client_stub, $ticket);
    if ($handle_name eq "main") {
      $client_stub = SOAP::Lite->service($wsdl_file_name);
      my $serializer = $client_stub->serializer();
      $serializer->typelookup->{"openMode"} = [1, \&SOAP::Serializer::openModeTest, 'as_openMode'];
      $ticket = $client_stub->OpenConnectionAndBeginSession
	($login_name, $password, $app_id, $app_name, $qb_file_name, $req_file_mod);
    } else {
      die "get_quickbooks_handle: bad handle name ($handle_name)\n";
    }
    $quickbooks_map = {};
    $instance_map->{"quickbooks"}->{$handle_name} = $quickbooks_map;
    ($quickbooks_map->{"client_stub"}, $quickbooks_map->{"ticket"}) = ($client_stub, $ticket);
  }
  return ($quickbooks_map->{"client_stub"}, $quickbooks_map->{"ticket"});
}

sub SOAP::Serializer::openModeTest {
  my $value = shift;
  if ($value =~ /^(dontCare|singleUser|multiUser)$/) {
    return 1;
  } else {
    return 0;
  }
}

sub SOAP::Serializer::as_openMode {
  my $self = shift;
  my($value, $name, $type, $attr) = @_;
  return [$name, {'xsi:type' => 's0:openMode', %$attr}, $value];
}

1;

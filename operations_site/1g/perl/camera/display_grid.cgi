#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my @camera_id_list = $cgi->param("camera_id");
  my $fps = $cgi->param("fps");
  # load all cameras.
  my (%camera_by_id);
  my $sql_query = "select * from camera_tbl order by id";
  get_row_by_id_map($dbh, $sql_query, undef, \%camera_by_id);
  for my $camera_id (@camera_id_list) {
    if (!exists($camera_by_id{$camera_id})) {
      die "show_page: display_grid_camera_not_found.html\n";
    }
  }
  if (!@camera_id_list) {
    die "show_page: display_grid_no_cameras_selected.html\n";
  } elsif (scalar(@camera_id_list) > 15) {
    die "show_page: display_grid_too_many_cameras.html\n";
  }
  # populate information needed for display of page.
  my $client_ip_addr = $cgi->remote_addr();
  $client_ip_addr =~ /^(\d+\.\d+\.\d+)/;
  my $client_subnet = $1;
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <fps>$fps</fps>
---END_BLOCK---;
  # give selected camera urls and grid placement numbers (1-9).
  for my $camera_id (@camera_id_list) {
    my $camera = $camera_by_id{$camera_id};
    next if (!$camera);
    my $url = "http://" . $camera->{"lan_ip_addr"};
    if ($camera->{"lan_port"} != 80) {
      $url .= ":" . $camera->{"lan_port"};
    }
    $url = HTML::Entities::encode($url);
    my $floor = select_row($dbh, "floor_tbl", $camera->{"floor_id"});
    my $floor_name = HTML::Entities::encode($floor->{"short_global_label"});
    my $description = HTML::Entities::encode($camera->{"description"});
    if ($url) {
      $generated_xml .= <<"---END_BLOCK---;";
  <camera>
    <id>$camera_id</id>
    <url>$url</url>
    <floor-name>$floor_name</floor-name>
    <description>$description</description>
  </camera>
---END_BLOCK---;
    }
  }
  $generated_xml .= <<"---END_BLOCK---;";
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "display_grid.xslt", $cgi);
}

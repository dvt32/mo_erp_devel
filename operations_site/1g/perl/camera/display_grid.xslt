<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Cameras
	</title>
	<script type="text/javascript" src="/1g/js/camera.js"> /* this is necessary for the declaration of JS with our Perl HTML processor */ var dummy_variable; </script>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> | <a href="/1g/camera/select_cameras.cgi">Grid Selection</a> | [Note: Click titles for full-screen views.]
	</p>
	<xsl:apply-templates select="camera"/>
	<p style="clear: both"><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="camera">
    <div class="grid">
      <div class="link">
	<a>
	  <xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
	  Camera ID #<xsl:value-of select="id"/>: <xsl:value-of select="description"/>
	</a>
      </div>
      <script type="text/javascript">
	create_camera('<xsl:value-of select="url"/>','<xsl:value-of select="../fps"/>')
      </script>
    </div>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

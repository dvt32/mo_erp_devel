<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Live Footage of Cameras
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> 
	  |
	  <a href="/1g/camera/show_archived_cameras.cgi">Archived Footage</a>
	</p>
	<p>You have chosen to view live camera footage. Please select one or more cameras to view live footage from.</p>
	<form action="/1g/camera/display_grid.cgi" method="post" autocomplete="off">
	  Hold Ctrl and click to select multiple:
	  <br />
	  <xsl:apply-templates select="camera-list"/>
	  <xsl:text> </xsl:text>
	  <select name="fps">
	    <option value="1">1 frame per second</option>
	    <option value="2" selected="selected">2 frames per second</option>
	    <option value="3">3 frames per second</option>
	    <option value="5">5 frames per second</option>
	    <option value="10">10 frames per second</option>
	  </select>
	  <xsl:text> </xsl:text>
	  <input type="submit" name="submitButton" value="Submit"/>
	</form>
	<xsl:for-each select="camera-list/floor">
	  <p><b><xsl:value-of select="floor-name" /></b></p>
	  <p>
	    <xsl:for-each select="camera">
	      <a>
		<xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
		<xsl:attribute name="class">bare_link</xsl:attribute>
		<xsl:text>Camera ID #</xsl:text><xsl:value-of select="id"/>
	      </a>
	      <xsl:text> (</xsl:text><xsl:value-of select="mac_address"/><xsl:text>)</xsl:text>
	      <xsl:text>: </xsl:text><xsl:value-of select="description"/>
	      <br />
	    </xsl:for-each>
	  </p>
	</xsl:for-each>
	<p><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="camera-list">
    <select>
      <xsl:attribute name="name">camera_id</xsl:attribute>
      <xsl:attribute name="multiple">1</xsl:attribute>
      <xsl:attribute name="size">10</xsl:attribute>
      <xsl:for-each select="floor/camera">
	<option>
	  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="../floor-name"/>, <xsl:text>Camera ID #</xsl:text><xsl:value-of select="id"/>
	</option>
      </xsl:for-each>
    </select>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

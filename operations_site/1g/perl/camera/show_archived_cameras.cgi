#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use global_config;

my $ftp_username = global_config::lookup("ftp_username");
my $ftp_password = global_config::lookup("ftp_password");

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:security/view_cameras");
  # load cameras at the specified floor.
  my (%camera_by_id, @camera_id_list);
  my $sql_query = "select * from camera_tbl order by floor_id, id";
  get_row_by_id_map($dbh, $sql_query, undef, \%camera_by_id, \@camera_id_list);
  # populate information needed for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <camera-list>
---END_BLOCK---;
  my ($floor_id, $floor, $floor_name);
  for my $camera_id (@camera_id_list) {
    my $camera = $camera_by_id{$camera_id};
    next if (!$camera->{"lan_ip_addr"});
    next if ((!$camera->{"storage_server"}) || (!$camera->{"storage_base_path"}));
    my $mac_address = HTML::Entities::encode($camera->{"mac_address"});
    my $url = "ftp://" . $ftp_username . ":" . $ftp_password . "\@" . $camera->{"storage_server"} . $camera->{"storage_base_path"} . "/" . $camera->{"mac_address"};
    $url = HTML::Entities::encode($url);
    my $description = HTML::Entities::encode($camera->{"description"});
    my $storage_server = HTML::Entities::encode($camera->{"storage_server"});
    my $storage_base_path = HTML::Entities::encode($camera->{"storage_base_path"});
    # are we changing floors in our listing of cameras by floor?
    if ((!defined($floor_id)) || ($floor_id != $camera->{"floor_id"})) {
      # if so, and we are not on the first floor_id, end previous floor item
      if (defined($floor_id)) {
	$generated_xml .= <<"---END_BLOCK---;";
    </floor>
---END_BLOCK---;
      }
      $floor_id = $camera->{"floor_id"};
      $floor = select_row($dbh, "floor_tbl", $floor_id);
      $floor_name = HTML::Entities::encode($floor->{"short_global_label"});
      $generated_xml .= <<"---END_BLOCK---;";
    <floor>
      <floor-name>$floor_name</floor-name>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      <camera>
        <id>$camera_id</id>
        <mac_address>$mac_address</mac_address>
        <url>$url</url>
        <description>$description</description>
        <storage_server>$storage_server</storage_server>
        <storage_base_path>$storage_base_path</storage_base_path>
      </camera>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </floor>
  </camera-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_archived_cameras.xslt", $cgi);
}

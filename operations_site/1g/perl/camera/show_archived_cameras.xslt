<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Archived Footage of Cameras
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> 
	  |
	  <a href="/1g/camera/select_cameras.cgi">Live Footage</a>
	</p>
	<p>You have chosen to download archived camera footage. Please select a camera to see archived files for that camera.</p>
	<p>After selecting a camera, you will see a list of AVI files. <span style="font-weight: bold;">MAKE SURE TO RIGHT CLICK THE AVI FILE YOU WANT AND CHOOSE EITHER "SAVE LINK AS" (Firefox) or "SAVE TARGET AS" (Internet Explorer)!</span></p>
	<xsl:for-each select="camera-list/floor">
	  <p><b><xsl:value-of select="floor-name" /></b></p>
	  <p>
	    <xsl:for-each select="camera">
	      <a>
		<xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
		<xsl:attribute name="class">bare_link</xsl:attribute>
		<xsl:text>Camera ID #</xsl:text><xsl:value-of select="id"/>
	      </a>
	      <xsl:text> (</xsl:text><xsl:value-of select="mac_address"/><xsl:text>)</xsl:text>
	      <xsl:text>: </xsl:text><xsl:value-of select="description"/>
	      <br />
	    </xsl:for-each>
	  </p>
	</xsl:for-each>
	<p><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="camera-list">
    <select>
      <xsl:attribute name="name">camera_id</xsl:attribute>
      <xsl:attribute name="multiple">1</xsl:attribute>
      <xsl:attribute name="size">10</xsl:attribute>
      <xsl:for-each select="floor/camera">
	<option>
	  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="../floor-name"/>, <xsl:text>Camera ID #</xsl:text><xsl:value-of select="id"/>
	</option>
      </xsl:for-each>
    </select>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

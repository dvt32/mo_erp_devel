#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use decrypt_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:corporate/company");
  my $employee_id = $employee->{"id"};
  my $company_id = $cgi->param("id");
  my $company;
  if ((!$company_id) || (!($company = select_row($dbh, "company_vw", $company_id)))) {
    die "company not found: $company_id";
  }
  my $field_list_string = <<"---END_BLOCK---;";
id
name
short_name
ownership
tax_id
building_short_name
building_id
activity
note
---END_BLOCK---;
  my @field_list = split(/\n/, $field_list_string);
  # retrieve decrypted values of encrypted fields
  my $decrypted_column_map = decrypt_row("company_tbl", $company_id, $employee_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <company>
---END_BLOCK---;
  for my $field_name (@field_list) {
    my $field_value = HTML::Entities::encode(defined($company->{$field_name}) ? $company->{$field_name} : "");
    $generated_xml .= <<"---END_BLOCK---;";
    <$field_name>$field_value</$field_name>
---END_BLOCK---;
  }
  # --- plaintext versions of encrypted fields
  $generated_xml .= <<"---END_BLOCK---;";
    <plaintext_column_list>
---END_BLOCK---;
  for my $field_name (sort(keys(%$decrypted_column_map))) {
    my $field_value = HTML::Entities::encode($decrypted_column_map->{$field_name});
    $generated_xml .= <<"---END_BLOCK---;";
      <$field_name>$field_value</$field_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </plaintext_column_list>
---END_BLOCK---;
  $generated_xml .= <<"---END_BLOCK---;";
  </company>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "company_detail.xslt", $cgi);
}

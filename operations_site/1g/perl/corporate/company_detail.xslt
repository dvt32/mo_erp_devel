<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Company Detail
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <xsl:apply-templates select="company"/>
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="company">
    <p>Company Detail</p>
    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
      <tr>
	<td style="padding: 4px; ">
	  ID:
	</td>
	<td>
	  <xsl:value-of select="id"/>
	</td>
      </tr>
      <tr>
	<td style="padding: 4px; ">
	  Name:
	</td>
	<td>
	  <xsl:value-of select="name"/>
	</td>
      </tr>
      <tr>
	<td style="padding: 4px; ">
	  Short Name:
	</td>
	<td>
	  <xsl:value-of select="short_name"/>
	</td>
      </tr>
      <tr>
	<td style="padding: 4px; ">
	  Ownership:
	</td>
	<td>
	  <xsl:value-of select="ownership"/>
	</td>
      </tr>
      <tr>
	<td style="padding: 4px; ">
	  Tax ID:
	</td>
	<td>
	  <xsl:value-of select="plaintext_column_list/tax_id"/>
	</td>
      </tr>
      <tr>
	<td style="padding: 4px; ">
	  Building:
	</td>
	<td>
	  <xsl:value-of select="building_short_name"/>
	</td>
      </tr>
      <tr>
	<td style="padding: 4px; ">
	  Activity:
	</td>
	<td>
	  <xsl:value-of select="activity"/>
	</td>
      </tr>
      <tr>
	<td style="padding: 4px; ">
	  Note:
	</td>
	<td>
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

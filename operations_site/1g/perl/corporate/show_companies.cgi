#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:corporate/company");
  my (%company_by_id, @company_id_list);
  my $sql_query = "select * from company_vw order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%company_by_id, \@company_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <company_list>
---END_BLOCK---;
  for my $company_id (@company_id_list) {
    my $company = $company_by_id{$company_id};
    my $name = HTML::Entities::encode($company->{"name"});
    my $short_name = HTML::Entities::encode($company->{"short_name"});
    my $ownership = HTML::Entities::encode($company->{"ownership"});
    my $tax_id = HTML::Entities::encode($company->{"tax_id"});
    my $building_short_name = HTML::Entities::encode($company->{"building_short_name"});
    my $building_id = HTML::Entities::encode($company->{"building_id"});
    my $activity = HTML::Entities::encode($company->{"activity"});
    my $note = HTML::Entities::encode($company->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <company>
      <id>$company_id</id>
      <name>$name</name>
      <short_name>$short_name</short_name>
      <ownership>$ownership</ownership>
      <tax_id>$tax_id</tax_id>
      <building_short_name>$building_short_name</building_short_name>
      <building_id>$building_id</building_id>
      <activity>$activity</activity>
      <note>$note</note>
    </company>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </company_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_companies.xslt", $cgi);
}

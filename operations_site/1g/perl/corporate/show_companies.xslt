<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Companies
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="company_list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="company_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="17" class="standard_table_title">
	  Companies
	</td>
      </tr>
      <tr class="standard_table_column_heading_row" valign="top">
	<td class="standard_table_side_padding"></td>
	<td>Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Short Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Own</td>
	<td class="standard_table_spacer_column"></td>
	<td>Tax ID</td>
	<td class="standard_table_spacer_column"></td>
	<td>Building</td>
	<td class="standard_table_spacer_column"></td>
	<td>Activity</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(company) > 0">
	  <xsl:apply-templates select="company"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="15">No companies.</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="company">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a class="bare_link">
	  <xsl:attribute name="href">
	    <xsl:text>/1g/corporate/company_detail.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:value-of select="name"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="short_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="ownership"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="tax_id"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="building_short_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="activity"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

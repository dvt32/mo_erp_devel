<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Applications
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/windows_logon_name != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a Windows domain account. Please ask your administrator to set up a Windows domain account
	    for you or to update the database with your Windows domain account information.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
    <li>
      Applications (RDP = remote desktop protocol) (nemo hostnames are if you are outside the office)
      <ul>
	<li>
	  General Desktop:
	  Chicago RDP desktopchi.microoffice.com
	  &middot;
	  New York RDP desktopny.microoffice.com
	</li>
	<li>
	  Design:
	  Adobe Creative Suite 6 RDP adobecs6.microoffice.local (nemo.microoffice.com:9944)
	  &middot;
	  Autodesk Building Design Suite 2016 RDP abds2016.microoffice.local
	  &middot;
	  AutoCAD Architecture 2016 RDP aca2016.microoffice.local (nemo.microoffice.com:9955)
	  &middot;
	  AutoCAD 2015 RDP autocad2015.microoffice.local
	  &middot;
	  Rhino 5 RDP rhino5.microoffice.local
	  &middot;
	  Solidworks 2014 RDP solidworks2014.microoffice.local
	</li>
	<li>
	  Accounting:
	  QuickBooks 2014 RDP qb2014.microoffice.local (nemo.microoffice.com:2014)
	  &middot;
	  QuickBooks 2015 RDP qb2015.microoffice.local
	</li>
	<li>
	  Network/Sysadmin:
	  TeamViewer 9 RDP teamviewer9.microoffice.local (nemo.microoffice.com:3399)
	  &middot;
	  TeamViewer 10 RDP teamviewer10.microoffice.local
	</li>
      </ul>
    </li>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Exchange Server Account Setup
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/windows_logon_name != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a Windows domain account. Please ask your administrator to set up a Windows domain account
	    for you or to update the database with your Windows domain account information.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<p>There are two formats for logon credentials:</p>
<ol>
  <li>
    User Principal Name (UPN) looks like this: user@domain.com. Example: jsmith@microoffice.com. Your UPN is
    <xsl:value-of select="windows_logon_name"/>@<xsl:value-of select="windows_upn_suffix"/>.
  </li>
  <li>
    Down-Level Logon Name looks like this: DOMAIN\user. Example: MICROOFFICE\jsmith. Your Down-Level Logon Name is
    <xsl:value-of select="upcase_windows_domain_name"/>\<xsl:value-of select="windows_logon_name"/>.
  </li>
</ol>
<p>
To access your email via the web, visit one of the following sites in your web browser:
</p>
<ol>
  <li><a href="http://mail.microoffice.com">mail.microoffice.com</a></li>
  <li><a href="http://exchange.microoffice.com/owa">exchange.microoffice.com/owa</a></li>
</ol>
<h3>Smartphone setup</h3>
<p>
  Server: exchange.microoffice.com
</p>
<p>
  Sometimes the username is
  <xsl:value-of select="windows_logon_name"/>
  and sometimes it is your Down-Level Logon Name
  (<xsl:value-of select="upcase_windows_domain_name"/>\<xsl:value-of select="windows_logon_name"/>),
  depending on whether the phone asks for the domain separately. If it asks for the domain separately, then enter
  <xsl:value-of select="windows_logon_name"/> as the username and <xsl:value-of select="upcase_windows_domain_name"/>
  as the domain. If it does not ask for the domain separately, then enter
  <xsl:value-of select="upcase_windows_domain_name"/>\<xsl:value-of select="windows_logon_name"/>
  as the username. Your email address is <xsl:value-of select="email"/>.
</p>
<h3>Outlook 2010/2013 setup</h3>
<p>
  If you are using a domain-managed computer, you hardly have to do any configuration because Outlook will automatically set
  itself up when you enter your email address the first time you start up Outlook. If you log in to Windows with
  <xsl:value-of select="upcase_windows_domain_name"/>\<xsl:value-of select="windows_logon_name"/> or with
  <xsl:value-of select="windows_logon_name"/>@<xsl:value-of select="windows_upn_suffix"/>, then you are using a
  domain-managed computer and you just need to enter your email address and Outlook will configure itself automatically. If you
  are not using a domain-managed computer, then Google for "Outlook 2010 setup exchange account" or "Outlook 2013 setup
  exchange account" and use the information below. Here are the specific details you will need:
</p>
<ul>
  <li>Server: exchange.microoffice.com</li>
  <li>User Name: <xsl:value-of select="windows_logon_name"/>@<xsl:value-of select="windows_upn_suffix"/></li>
</ul>
<p>
  At the settings dialog, click on More Settings. Then go to the Connection tab and make sure that the box labeled "Connect to
  Microsoft Exchange using HTTP" is checked (check it if it is not). Next, click on the "Exchange Proxy Settings" button.
</p>
<p>
  Find the text input box that is labeled "Use this URL to connect to my proxy server for Exchange:"
  <br/>
  The https:// part is already there, so just enter "exchange.microoffice.com" (not including the quotes).
</p>
<p>
  Check the box that says "Only connect to proxy servers that have this principal name in their certificate:", then enter the
  following in the text box that appears afterwards:
  <br/>
  msstd:exchange.microoffice.com
</p>
<p>
  Proxy authentication settings should be set to "Basic Authentication".
</p>
<h3>Junk e-mail / spam filter</h3>
<p>
If you start getting a lot of spam, here is the procedure for turning on server-side spam filtering:
</p>
<ol>
  <li>Go to the Home tab in Outlook, click on Rules, then Create Rule.</li>
  <li>Click the Advanced Options button in the lower right of the "Create Rule" dialog window.</li>
  <li>Check the box to the left of "with specific words in the message header".</li>
  <li>In the bottom pane, click the "specific words" in blue.</li>
  <li>Type the following exactly, matching uppercase and lowercase, but not including the surrounding quotes: "X-Spam-Status: Yes". Then click the Add button on the right, then click the OK button on the bottom.</li>
  <li>Click the Next button.</li>
  <li>Check the box to the left of "move it to the specified folder".</li>
  <li>Next, check the box to the left of "stop processing more rules".</li>
  <li>In the lower pane, click the "specified" in blue.</li>
  <li>Choose your Junk E-Mail folder and click the OK button.</li>
  <li>Click the Next button.</li>
  <li>On the screen that asks "Are there any exceptions?", just click the Next button.</li>
  <li>For the name, type spam. Then click the Finish button.</li>
</ol>
<p>
Be sure to periodically check your Junk E-Mail folder just in case there is legitimate email. The spam filter misclassifies
some legitimate email as spam, so check periodically to avoid missing important emails. Also, if you later add rules to your
mailbox, be sure to move the spam filter up to the top of the rule list because otherwise your rule will run before the spam
filter rule (and thus spam might first get copied to a folder before getting classified as spam).
</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

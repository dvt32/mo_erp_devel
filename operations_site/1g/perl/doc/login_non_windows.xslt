<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Log In on Non-Windows Machine
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/windows_logon_name != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a Windows domain account. Please ask your administrator to set up a Windows domain account
	    for you or to update the database with your Windows domain account information.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
Log In on Non-Windows Machine (Mac or Chromebook, etc.)
</h3>
<p>
  On domain-managed Windows machines, if you use Internet Explorer or Chrome, login is automatic. If you use Firefox on a domain-managed Windows machines, follow these instructions to enable SPNEGO so that you can get automatic sign-in:
  <ol>
    <li>Navigate to the URL <code>about:config</code>.</li>
    <li>Click past the warning of harmful consequences.</li>
    <li>Type <code>negotiate-auth</code> into the filter at the top of the page, in order to remove most of the irrelevant settings from the list.</li>
    <li>Double-click on <code>network.negotiate-auth.trusted-uris</code>. A dialog box for editing the value should appear.</li>
    <li>Enter <code>https://operations.microoffice.com</code>, then click OK.</li>
    <li>Type <code>automatic-ntlm-auth</code> into the filter at the top of the page, in order to remove most of the irrelevant settings from the list.</li>
    <li>Double-click on <code>network.automatic-ntlm-auth.trusted-uris</code>. A dialog box for editing the value should appear.</li>
    <li>Enter <code>https://operations.microoffice.com</code>, then click OK.</li>
  </ol>
</p>
<p>
  If you are not on a domain-managed Windows (for instance, if you use a Mac, Chromebook, or a Windows machine not on our domain), then log in using the following credentials:
  <ul>
    <li>Username: <xsl:value-of select="windows_logon_name"/>@<xsl:value-of select="upcase_windows_domain_name"/>.LOCAL</li> (make sure to type the part after the at (@) sign in all capital letters)
    <li>Password: (your Windows domain password, which you use for Outlook/Exchange email login)</li>
  </ul>
</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

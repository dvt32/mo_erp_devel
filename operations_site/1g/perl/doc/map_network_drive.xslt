<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Map Network Drive
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/windows_logon_name != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a Windows domain account. Please ask your administrator to set up a Windows domain account
	    for you or to update the database with your Windows domain account information.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
File server access
</h3>
<p>
You can only access the file server in one of the following two situations:
</p>
<ol>
  <li>You are physically located at one of our offices.</li>
  <li>You are remote and have <a href="vpn_access.cgi">VPN turned on</a>.</li>
</ol>
<p>
  If you log in to Windows with <xsl:value-of select="upcase_windows_domain_name"/>\<xsl:value-of select="windows_logon_name"/>
  or with <xsl:value-of select="windows_logon_name"/>@<xsl:value-of select="windows_upn_suffix"/>, then you are using a
  domain-managed computer and you should perform the following steps:
  <ol>
    <li>
      Go to Windows file explorer (not Internet explorer) and right click on Computer, then choose "Map network drive".
    </li>
    <li>
      Choose M: for the drive. For folder, type "\\microoffice\files\monyc" (without the quotes).
    </li>
    <li>
      Click on Finish and you should then see an M: drive in Windows file explorer.
    </li>
  </ol>
</p>
<p>
  If you log in to Windows with other credentials, then either you are not using a domain-managed computer (you might be on a
  workgroup), or you are domain-managed on a different company's domain. In that case, you should perform the following steps:
  <ol>
    <li>
      Go to Windows file explorer (not Internet explorer) and right click on Computer, then choose "Map network drive".
    </li>
    <li>
      Choose M: for the drive. For folder, type "\\juno.sna8.microoffice.com\monyc" (without the quotes).
    </li>
    <li>
      Click the checkbox that says "Connect using different credentials" and enter
      <xsl:value-of select="windows_logon_name"/>@<xsl:value-of select="windows_upn_suffix"/>
      and your password. Then click on Finish. You can Google "Windows map network drive" if you have problems.
    </li>
  </ol>
</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

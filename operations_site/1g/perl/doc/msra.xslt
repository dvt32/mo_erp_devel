<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Microsoft Remote Assistance (msra)
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:apply-templates select="employee"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
Microsoft Remote Assistance (msra)
</h3>
<p>
  Microsoft Remote Assistance is a program built in to Windows 7 and Windows 8 that allows one person to help another by
  viewing and controlling the other person's computer over the network. You can either be the helper (person providing help) or
  the helpee (person who needs the help).
</p>
<h4>If you are the helpee (you need help), perform the following actions to invite the helper to view and/or control your computer:</h4>
<p>
<ol>
  <li>Click the start button (Windows 7) or open the start menu (Windows 8) and then type msra and press Enter.</li>
  <li>Choose "Invite someone you trust to help you".</li>
  <li>If you have Outlook installed already on the computer, choose "Use e-mail to send an invitation." Otherwise choose "Save this invitation as a file" and then open up a web browser and go to your email in the browser and attach the msra invitation.</li>
  <li>In the email, do not forget to include the code provided by msra. The helper will need that code to connect to your computer.</li>
</ol>
</p>
<h4>If you are the helper and want to help someone on their computer, perform the following actions:</h4>
<p>
<ol>
  <li>Wait for the msra invitation from the person who needs help. It should arrive via email.</li>
  <li>Open the attachment in the invite. WARNING: be careful to scrutinize the email and only open the attachment if it is from the person who needs help, not if it is from some strange person.</li>
  <li>Enter the msra code supplied by the person who needs help.</li>
  <li>If you want to control the remote computer (as opposed to merely view the screen), click the upper left button to request control.</li>
</ol>
</p>
<p>
  If msra does not work, it is possible there is a firewall issue. In that case, consider using <a href="screenconnect.cgi">ScreenConnect</a> instead of msra.
</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

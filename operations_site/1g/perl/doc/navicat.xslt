<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Navicat Connection
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/windows_logon_name != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a Windows domain account. Please ask your administrator to set up a Windows domain account
	    for you or to update the database with your Windows domain account information.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
Navicat connection
</h3>
<p>
  We have a company database that stores lots of information about our business. To get direct access, you can either use
  Navicat or Microsoft Access. The benefit is that you can manipulate large amounts of data in spreadsheet-like format. If you
  use Navicat, no driver installation is necessary. Instead, start up Navicat (if it is installed on your computer), or remote
  desktop to navicat.microoffice.local. We use PostgreSQL as our database back-end. To set up the Navicat connection, click the
  connection button in the upper left and choose PostgresQL. Then, fill out the fields below and click OK.
</p>
<p>
  <ul>
    <li>Connection Name: <xsl:value-of select="windows_logon_name"/>@kleene.int</li>
    <li>Host Name/IP Address: kleene.int.microoffice.com</li>
    <li>Port: 5432</li>
    <li>Initial Database: microoffice</li>
    <li>User Name: <xsl:value-of select="windows_logon_name"/></li>
    <li>Password: (enter your Windows domain password, which is the same as your Outlook password)</li>
  </ul>
</p>
<p>
  Click the Test Connection button to check if you entered the settings correctly.
</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

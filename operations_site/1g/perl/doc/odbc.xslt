<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: ODBC
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/pg_username != '' and employee/pg_password != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a PostgreSQL account. Please ask your administrator to set up a PostgreSQL account
	    for you or to update the database with your PostgreSQL credentials.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
Open Database Connectivity (ODBC) access
</h3>
<p>
  We have a company database that stores lots of information about our business. To get direct access, you can either use Navicat
  or Microsoft Access. The benefit is that you can manipulate large amounts of data in spreadsheet-like format. If you use
  Navicat, no driver installation is necessary. Instead, just remote desktop to navicat.microoffice.local. We use PostgreSQL as
  our database back-end. If you use Microsoft Access, first, you need to install the PostgreSQL ODBC driver, available here: <a
  href="http://www.postgresql.org/ftp/odbc/versions/msi/">http://www.postgresql.org/ftp/odbc/versions/msi/</a>. Look for the
  latest version of the ODBC driver. You may already have it installed. To check, go to uninstall programs and look for psqlODBC.
</p>
<p>
  After installing the PostgreSQL ODBC driver, you need to configure an ODBC data source. To do this, type ODBC at the start menu or start page and, if prompted with two options, choose the 64 bit option for configure ODBC data source. If you already see a data source named MicroOffice, then you are done. Otherwise, under the User DSN tab, click the Add button to add a new User Data Source. Type the following for the field values:
</p>
<style>
span.hidden_password:hover {
  color: black;
}
span.hidden_password {
  color: white;
}
</style>
<p>
  <ul>
    <li>Data Source: MicroOffice (uppercase M and O, but lowercase everything else)</li>
    <li>Database: microoffice</li>
    <li>Server: kleene.snc2.microoffice.com</li>
    <li>User Name: <xsl:value-of select="pg_username"/></li>
    <li>Description: Micro Office database</li>
    <li>SSL Mode: disable</li>
    <li>Port: 5432</li>
    <li>Password: <span class="hidden_password"><xsl:value-of select="pg_password"/></span> (mouse over to see password)</li>
  </ul>
</p>
<p>
  Click the Test button to check if you entered the settings correctly.
</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Remote Desktop Access
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/windows_logon_name != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a Windows domain account. Please ask your administrator to set up a Windows domain account
	    for you or to update the database with your Windows domain account information.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
Remote desktop access
</h3>
<p>
  We have a remote desktop gateway that allows you to access computers
  inside the office from outside. You can access it from a PC, Mac or
  smartphone.
</p>
<p>
  Here is how to access it from a PC or Mac. On Mac, you should first download Microsoft Remote Desktop Connection Client.
<ol>
  <li>Open the "Remote Desktop Connection" program on your computer outside the office.</li>
  <li>For "Computer", enter the host name of the computer you wish to connect to.</li>
  <li>On the lower left, click "Show Options".</li>
  <li>Click on the "Advanced" tab near the upper right.</li>
  <li>Under the section labeled "Connect from anywhere", click the "Setting" button.</li>
  <li>Click the radio button labeled "Use these RD Gateway server settings".</li>
  <li>Enter rdgateway.microoffice.com for Server name.</li>
  <li>Change Logon method to "Ask for password (NTLM)".</li>
  <li>Click OK.</li>
  <li>Click Connect.</li>
  <li>
    Log in as <xsl:value-of select="windows_logon_name"/>@<xsl:value-of select="windows_upn_suffix"/>
    (or <xsl:value-of select="upcase_windows_domain_name"/>\<xsl:value-of select="windows_logon_name"/>) and your password.
  </li>
</ol>
</p>
<p>
  Here is how to access remote desktop from a smartphone.
<ol>
  <li>Download Microsoft Remote Desktop from the App Store (iPhone) or Play Store (Android).</li>
  <li>Create a host and enter the host name of the computer you wish to connect to.</li>
  <li>Add a user named <xsl:value-of select="upcase_windows_domain_name"/>\<xsl:value-of select="windows_logon_name"/>.</li>
  <li>Add a gateway rdgateway.microoffice.com (note the .com at the end, not .local)</li>
  <li>You should then be able to connect.</li>
</ol>
</p>
<p>
Here are the host names of some useful computers you can connect to:
<ul>
  <li>autocad2019.microoffice.local - AutoCAD 2019 for designing and working with CAD drawing files</li>
  <li>adobecc2018.microoffice.local - Adobe Creative Cloud 2018 applications (Photoshop, Illustrator, etc.)</li>
  <li>qb2016.microoffice.local - QuickBooks 2016 server</li>
  <li>cloudpc.microoffice.local - Cloud PC with Microsoft Office (Word, Excel, Powerpoint, OneNote, etc.), Adobe Acrobat, Navicat, and other productivity software</li>
</ul>
  On some of the computers, you'll be able to use a full Outlook client and get <a href="map_network_drive.cgi">file server access</a>, and also access to OneNote.
</p>
<p>
  To find out the name of your PC, go to the start menu and type
  System. Click on the program that says System, not the one that says
  System Information or System Configuration. There should be a line
  that reads “Computer name”. Append “.microoffice.local” to that
  name. It might say qb2016, for instance. Your full hostname in that
  case would “qb2016.microoffice.local”.
</p>
<p>
Sometimes it is helpful to be able to use or access local resources (such as printers or your local hard drive) while connected to the remote machine. You might be connected to a remote computer and want to print something to your local printer. Or, you might be connected to a remote computer and want to conveniently copy a file from that remote computer to your local computer. Here is how you can do that:
</p>
<ol>
  <li>
    Start the Remote Desktop Connection program (in Windows 10, press the Start key on the keyboard and start typing remote desktop).
  </li>
  <li>
    If you do not see any tabs at the top of the Remote Desktop Connection window, find the button that says "Show Options" in the lower left and click it.
  </li>
  <li>
    Click the "Local Resources" tab.
  </li>
  <li>
    Under "Local devices and resources", check the boxes for Printers if you want to be able to print to your local printer and for Clipboard if you want to be able to copy and paste between your local machine and the remote machine. Usually both boxes are checked, so you do not have to do anything.
  </li>
  <li>
    Click the button that says "More...".
  </li>
  <li>
    Expand the plus sign to the left of Drives.
  </li>
  <li>
    Choose the drives you want to use on the remote computer. Usually this will be C:, but it might also be a USB flash drive if you have one plugged in.
  </li>
  <li>
    Click OK.
  </li>
  <li>
    Go back to the "General" tab.
  </li>
  <li>
    Enter the computer and your username, then click on the Connect at the bottom of the window.
  </li>
</ol>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: ScreenConnect Usage
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:apply-templates select="employee"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
ScreenConnect usage
</h3>
<p>
  ScreenConnect is a program that lets you give help to another or receive help from another. There are two modes in
  ScreenConnect: Host and Guest. Connect as Host if you are the person providing help (i.e. you want to view and take control
  of somebody else's computer). Connect as Guest if you are the person who needs help (i.e. you want to give somebody control
  over your computer). Do not give someone else (especially not an outside person) your login, as that may give them access to
  control our computers.
</p>
<h4>To connect as Host, perform the following actions:</h4>
<p>
<ol>
  <li>In your web browser, visit https://screenconnect.microoffice.com (note the <em>https</em> prefix, which ensures security -- regular http will not work).</li>
  <li>If you are not logged in, click the "Login" link in the upper right and then log in with your ScreenConnect credentials (not the same as your Windows domain credentials). If you do not have ScreenConnect credentials, but would like to view someone else's screen to help them, email Wellie to get a login. Note that if you are looking for help <em>from</em> someone, what you want is to connect as a guest (see below for Guest mode).</li>
  <li>On the upper left of your screen, you should see a section labeled "Support" in red background. Just below that, you can see a list of support sessions and a button labeled "Create Session". Click "Create Session" if you want to start a new support session.</li>
  <li>A dialog will pop up asking you to name the support session. Name it something unique that will differentiate the support session from others, perhaps the name of the person you are helping or the building, floor, and computer name of the target.</li>
  <li>The Join Mode option has four choices. Usually you should use either Invitation Only or Secure Code, although you may experiment with the others if you like. If you choose Invitation Only, be sure you click on the Invite drop-down button so that you can get the invitation to the person you want to help.</li>
  <li>Then click on the Join button so you can actually open up the remote control display.</li>
  <li>After you are done with the support session, don't forget to end the session.</li>
</ol>
</p>
<h4>To connect as Guest, perform the following actions:</h4>
<p>
<ol>
  <li>If the technician sent you an invite via email or instant message, just click on the URL. Be careful not to click on links from people you do not trust.</li>
  <li>If the technician gave you a code, then open a web browser and visit https://screenconnect.microoffice.com (note the <em>https</em> prefix, which ensures security -- regular http will not work).</li>
</ol>
</p>
<h4>To install the unattended access agent, perform the following actions:</h4>
<p>
<ol>
  <li>In your web browser, visit https://screenconnect.microoffice.com (note the <em>https</em> prefix, which ensures security -- regular http will not work).</li>
  <li>If you are not logged in, click the "Login" link in the upper right and then log in with your credentials.</li>
  <li>On the lower left you should see a section labeled "Access" in red background. Below that there is a button labeled "Build Installer". Click that. You will get a file you can then copy over to the machine that needs remote unattended access. You should run that program or installer (as an administrator) on the machine needing remote unattended access.</li>
</ol>
</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

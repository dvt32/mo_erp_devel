#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # populate information needed for display of page.
  my $windows_logon_name = HTML::Entities::encode($employee->{"windows_logon_name"} || "");
  my $windows_domain_name = HTML::Entities::encode($employee->{"windows_domain_name"} || "");
  my $upcase_windows_domain_name = uc($windows_domain_name);
  my $windows_upn_suffix = HTML::Entities::encode($employee->{"windows_upn_suffix"} || "");
  my $email = HTML::Entities::encode($employee->{"email"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <employee>
    <windows_logon_name>$windows_logon_name</windows_logon_name>
    <windows_domain_name>$windows_domain_name</windows_domain_name>
    <upcase_windows_domain_name>$upcase_windows_domain_name</upcase_windows_domain_name>
    <windows_upn_suffix>$windows_upn_suffix</windows_upn_suffix>
    <email>$email</email>
  </employee>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "vpn_access.xslt", $cgi);
}

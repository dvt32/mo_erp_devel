<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: VPN Access
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:choose>
	  <xsl:when test="employee/windows_logon_name != ''">
	    <xsl:apply-templates select="employee"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Sorry, you do not have a Windows domain account. Please ask your administrator to set up a Windows domain account
	    for you or to update the database with your Windows domain account information.
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee">
<h3>
Virtual Private Network (VPN) access
</h3>
<p>
  Google for "Windows 7 VPN setup", "Windows 8 VPN setup", or "Windows 10 VPN setup", depending on what version of Windows you are running. Use the
  following settings:
  <ul>
    <li>username: <xsl:value-of select="windows_logon_name"/></li>
    <li>
      domain: <xsl:value-of select="upcase_windows_domain_name"/>
      (if that gives you problems, try <xsl:value-of select="upcase_windows_domain_name"/>.local)
    </li>
    <li>VPN server: winvpn.microoffice.com</li>
  </ul>
</p>
<!--
<p>
  Also, please configure the following:
  <a href="http://ferhatindi.com/2012/03/using-your-own-internet-during-vpn-connection/">
    http://ferhatindi.com/2012/03/using-your-own-internet-during-vpn-connection/
  </a>
</p>
<p>
  If you don't, all your traffic will first flow through our 1375 Broadway location, which isn't a big deal for us since we
  have ample bandwidth, but it will make your traffic needlessly delayed unless you uncheck the box so that your traffic
  doesn't get proxied by our VPN server.
</p>
-->
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

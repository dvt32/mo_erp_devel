#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use decrypt_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $employee_id = $employee->{"id"};
  my $password_id = $cgi->param("id");
  my $password;
  if ((!$password_id) || (!($password = select_row($dbh, "password_tbl", $password_id)))) {
    die "password not found: $password_id";
  }
  # check if employee is owner or in viewer, editor, commentor lists
  my $employee_has_access = 0;
  if ($employee_id == $password->{"owner_id"}) {
    $employee_has_access = 1;
  }
  for my $list_name (qw(view comment edit)) {
    if ($password->{$list_name."_id_list"}) {
      if (grep {$_ == $employee_id} @{$password->{$list_name."_id_list"}}) {
	$employee_has_access = 1;
      }
    }
  }
  if (!$employee_has_access) {
    die "employee ($employee_id) lacks access to password ($password_id)";
  }
  # fetch employees to generate list
  my $sql_query = "select * from employee_tbl where is_active order by last_name, first_name";
  my (%employee_by_id, @employee_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  my $field_list_string = <<"---END_BLOCK---;";
id
name
url
phone_number
account_number
username
password
pin
email
sec_quest_1
sec_ans_1
sec_quest_2
sec_ans_2
sec_quest_3
sec_ans_3
sec_quest_4
sec_ans_4
sec_quest_5
sec_ans_5
sec_quest_6
sec_ans_6
sec_quest_7
sec_ans_7
note
needs_update
---END_BLOCK---;
  my @field_list = split(/\n/, $field_list_string);
  # retrieve decrypted values of encrypted fields
  my $decrypted_column_map = decrypt_row("password_tbl", $password_id, $employee_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <password>
---END_BLOCK---;
  for my $field_name (@field_list) {
    my $field_value = HTML::Entities::encode(defined($password->{$field_name}) ? $password->{$field_name} : "");
    $generated_xml .= <<"---END_BLOCK---;";
    <$field_name>$field_value</$field_name>
---END_BLOCK---;
  }
  # --- plaintext versions of encrypted fields
  $generated_xml .= <<"---END_BLOCK---;";
    <plaintext_column_list>
---END_BLOCK---;
  for my $field_name (sort(keys(%$decrypted_column_map))) {
    my $field_value = HTML::Entities::encode($decrypted_column_map->{$field_name});
    $generated_xml .= <<"---END_BLOCK---;";
      <$field_name>$field_value</$field_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </plaintext_column_list>
---END_BLOCK---;
  # --- list of employees with view access
  $generated_xml .= <<"---END_BLOCK---;";
    <viewer_list>
---END_BLOCK---;
  my @view_id_list;
  if ($password->{"view_id_list"}) {
    @view_id_list = @{$password->{"view_id_list"}};
  }
  for my $_employee_id (@view_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
    $generated_xml .= <<"---END_BLOCK---;";
      <viewer>
        <employee_id>$_employee_id</employee_id>
        <employee_name>$_employee_name</employee_name>
      </viewer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </viewer_list>
---END_BLOCK---;
  # --- list of employees with comment access
  $generated_xml .= <<"---END_BLOCK---;";
    <commenter_list>
---END_BLOCK---;
  my @comment_id_list;
  if ($password->{"comment_id_list"}) {
    @comment_id_list = @{$password->{"comment_id_list"}};
  }
  for my $_employee_id (@comment_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
    $generated_xml .= <<"---END_BLOCK---;";
      <commenter>
        <employee_id>$_employee_id</employee_id>
        <employee_name>$_employee_name</employee_name>
      </commenter>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </commenter_list>
---END_BLOCK---;
  # --- list of employees with edit access
  $generated_xml .= <<"---END_BLOCK---;";
    <editor_list>
---END_BLOCK---;
  my @edit_id_list;
  if ($password->{"edit_id_list"}) {
    @edit_id_list = @{$password->{"edit_id_list"}};
  }
  for my $_employee_id (@edit_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
    $generated_xml .= <<"---END_BLOCK---;";
      <editor>
        <employee_id>$_employee_id</employee_id>
        <employee_name>$_employee_name</employee_name>
      </editor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </editor_list>
---END_BLOCK---;
  my $owner = $employee_by_id{$password->{"owner_id"}};
  my $owner_name = HTML::Entities::encode($owner->{"last_name"} . ", " . $owner->{"first_name"});
  $generated_xml .= <<"---END_BLOCK---;";
    <owner_id>$password->{"owner_id"}</owner_id>
    <owner_name>$owner_name</owner_name>
  </password>
  <employee_list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <name>$_employee_name</name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_password_input.xslt", $cgi);
}

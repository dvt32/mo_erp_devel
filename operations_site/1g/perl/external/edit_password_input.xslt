<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Password
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <xsl:apply-templates select="password"/>
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="password">
    <table>
      <tr><td>
	<form action="/1g/external/edit_password_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Password
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URL:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">url</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="url"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">account_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="account_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Username:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">username</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="username"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Password:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">password</xsl:attribute>
		    <xsl:attribute name="name">password</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="password"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		    <xsl:attribute name="maxlength">32</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/password != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/password"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Pin:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">pin</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="pin"/></xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/pin != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/pin"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="email"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Question #1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_quest_1</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_quest_1"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Answer #1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_ans_1</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_ans_1"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/sec_ans_1 != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/sec_ans_1"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Question #2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_quest_2</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_quest_2"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Answer #2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_ans_2</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_ans_2"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/sec_ans_2 != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/sec_ans_2"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Question #3:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_quest_3</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_quest_3"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Answer #3:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_ans_3</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_ans_3"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/sec_ans_3 != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/sec_ans_3"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Question #4:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_quest_4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_quest_4"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Answer #4:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_ans_4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_ans_4"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/sec_ans_4 != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/sec_ans_4"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Question #5:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_quest_5</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_quest_5"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Answer #5:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_ans_5</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_ans_5"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/sec_ans_5 != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/sec_ans_5"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Question #6:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_quest_6</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_quest_6"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Answer #6:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_ans_6</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_ans_6"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/sec_ans_6 != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/sec_ans_6"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Question #7:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_quest_7</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_quest_7"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Security Answer #7:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sec_ans_7</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sec_ans_7"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  <xsl:if test="plaintext_column_list/sec_ans_7 != ''">
		    curr val: <xsl:value-of select="plaintext_column_list/sec_ans_7"/>
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Has Edit Access:
		</td>
		<td>
                  <select name="edit_id_list" multiple="multiple" size="8">
		    <xsl:for-each select="/page/employee_list/employee">
		      <option>
			<xsl:if test="/page/password/editor_list/editor/employee_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

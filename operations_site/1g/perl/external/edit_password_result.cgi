#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;
use normalize_whitespace;



use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_role_list;
use get_timestamp_now;
use put_file;
use delete_file;
use update_row;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $employee_id = $employee->{"id"};
  my $password_id = $cgi->param("id");
  my $password;
  if ((!$password_id) || (!($password = select_row($dbh, "password_tbl", $password_id)))) {
    die "password not found: $password_id";
  }
  # check if employee is owner or in editor list
  my $employee_has_access = 0;
  if ($employee_id == $password->{"owner_id"}) {
    $employee_has_access = 1;
  }
  for my $list_name (qw(edit)) {
    if ($password->{$list_name."_id_list"}) {
      if (grep {$_ == $employee_id} @{$password->{$list_name."_id_list"}}) {
	$employee_has_access = 1;
      }
    }
  }
  if (!$employee_has_access) {
    die "you lack access to password ($password_id)";
  }
  # get the form fields.
  my @field_name_list = (qw(name url phone_number account_number username password pin email));
  for(my $i = 1; $i <= 7; $i++) {
    push(@field_name_list, ("sec_quest_$i", "sec_ans_$i"));
  }
  push(@field_name_list, (qw(note edit_id_list)));
  my %field_map;
  for my $field_name (@field_name_list) {
    $field_map{$field_name} = Encode::decode($cgi_input_charset, normalize_whitespace($cgi->param($field_name)));
  }
  # enforce required fields
  for my $field_name (qw(name)) {
    if (!$field_map{$field_name}) {
      die "show_page: edit_password_missing_$field_name.html\n";
    }
  }
  # validate email
  if (!is_valid_email($field_map{"email"})) {
    die "show_page: edit_password_bad_email.html\n";
  }
  # validate edit_id_list
  for my $edit_id ($field_map{"edit_id_list"}) {
  }




  # get the form fields.
  my $expense_id = $cgi->param("id");
  my $expense;
  if ((!$expense_id) || (!($expense = select_row($dbh, "expense_tbl", $expense_id)))) {
    die "show_page: edit_expense_bad_id.html\n";
  }
  # check if expense has been reimbursed or already reconciled.
  if ((($expense->{"is_reimbursed"}) || ($expense->{"is_reconciled"})) && (!grep {$_ eq "role:finance/view_all_expenses"} @$role_list)) {
    die "show_page: edit_expense_already_processed.html\n";
  }
  my $person;
  if ((grep {$_ eq "role:finance/view_all_expenses"} @$role_list) && ($cgi->param("employee_id"))) {
    $employee_id = $cgi->param("employee_id");
  }
  if (!($person = select_row($dbh, "employee_tbl", $employee_id))) {
    die "show_page: edit_expense_bad_employee.html\n";
  }
  my $item_name = Encode::decode($cgi_input_charset, $cgi->param("item_name"));
  if (!$item_name) {
    die "show_page: edit_expense_missing_item_name.html\n";
  }
  my $vendor_name = Encode::decode($cgi_input_charset, $cgi->param("vendor_name"));
  if (!$vendor_name) {
    die "show_page: edit_expense_missing_vendor_name.html\n";
  }
  my $purchase_date_month = $cgi->param("month");
  if (($purchase_date_month < 1) || ($purchase_date_month > 12)) {
    die "show_page: edit_expense_bad_purchase_date.html\n";
  }
  my $purchase_date_day = $cgi->param("day");
  if (($purchase_date_day < 1) || ($purchase_date_day > 31)) {
    die "show_page: edit_expense_bad_purchase_date.html\n";
  }
  my $purchase_date_year = $cgi->param("year");
  if (($purchase_date_year < 2008) || ($purchase_date_year > 2099)) {
    die "show_page: edit_expense_bad_purchase_date.html\n";
  }
  my $purchase_date = $purchase_date_year . "-" . lead_pad($purchase_date_month, 2) . "-" . lead_pad($purchase_date_day, 2);
  my $ledger_account_id = $cgi->param("ledger_account_id");
  my $ledger_account;
  if ((!$ledger_account_id) || ($ledger_account_id !~ /^\d+$/) ||
      (!($ledger_account = select_row($dbh, "ledger_account_tbl", $ledger_account_id)))) {
    die "show_page: edit_expense_bad_ledger_account.html\n";
  }
  my $locations = Encode::decode($cgi_input_charset, $cgi->param("locations"));
  if (!$locations) {
    die "show_page: edit_expense_missing_locations.html\n";
  }
  my $amount = $cgi->param("amount");
  if ((!$amount) || ($amount !~ /^\d+(\.\d\d)$/)) {
    die "show_page: edit_expense_bad_amount.html\n";
  }
  my $payer = $cgi->param("payer");
  if ((!$payer) || ($payer !~ /^(company|personal)$/)) {
    die "show_page: edit_expense_bad_payer.html\n";
  }
  my $payment_method = $cgi->param("payment_method");
  if ((!$payment_method) || ($payment_method !~ /^(bill|card|cash|check)$/)) {
    die "show_page: edit_expense_bad_payment_method.html\n";
  }
  my $payment_identifier = Encode::decode($cgi_input_charset, $cgi->param("payment_identifier"));
  my $receipt_fh = $cgi->upload("receipt_file");
  my $receipt_file_content;
  if ($receipt_fh) {
    local($/) = undef;
    $receipt_file_content = scalar(<$receipt_fh>);
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # update the expense. if needed, replace the uploaded receipt.
  $dbh->begin_work();
  my ($receipt_file_id, $receipt_filename);
  my $old_receipt_file_id = $expense->{"receipt_file_id"};
  if ($receipt_file_content) {
    my $timestamp_now = get_timestamp_now();
    $timestamp_now =~ s/[^\d]//g;
    $receipt_filename = "Expense Receipt $purchase_date - $timestamp_now.pdf";
    $receipt_file_id = put_file($instance_map, "/finance/expense", $receipt_filename, "application/pdf", \$receipt_file_content);
    if ($old_receipt_file_id) {
      delete_file($instance_map, $old_receipt_file_id);
    }
  } elsif ($old_receipt_file_id) {
    $receipt_file_id = $old_receipt_file_id;
    my $file = select_row($dbh, "file_tbl", $old_receipt_file_id);
    $receipt_filename = $file->{"filename"};
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["account_id", $account_id_of_corporate, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $employee_id, SQL_INTEGER]);
  push(@sql_column_list, ["item_name", Encode::encode($db_charset, $item_name)]);
  push(@sql_column_list, ["vendor_name", Encode::encode($db_charset, $vendor_name)]);
  push(@sql_column_list, ["purchase_date", $purchase_date]);
  push(@sql_column_list, ["ledger_account_id", $ledger_account_id, SQL_INTEGER]);
  push(@sql_column_list, ["locations", Encode::encode($db_charset, $locations)]);
  push(@sql_column_list, ["amount", $amount]);
  push(@sql_column_list, ["payer", $payer]);
  push(@sql_column_list, ["payment_method", $payment_method]);
  push(@sql_column_list, ["payment_identifier", $payment_identifier || undef]);
  if ($receipt_file_content) {
    push(@sql_column_list, ["receipt_file_id", $receipt_file_id]);
  }
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  update_row($dbh, "expense_tbl", \@sql_column_list, $expense_id);
  $dbh->commit();
  # populate information for display of page.
  my $person_name = HTML::Entities::encode($person->{"first_name"} . " " . $person->{"last_name"});
  $item_name = HTML::Entities::encode($item_name);
  $vendor_name = HTML::Entities::encode($vendor_name);
  my $ledger_account_name = HTML::Entities::encode($ledger_account->{"name"});
  $locations = HTML::Entities::encode($locations);
  $payment_identifier = HTML::Entities::encode($payment_identifier);
  $receipt_file_id ||= "";
  $receipt_filename = HTML::Entities::encode($receipt_filename || "");
  $note = HTML::Entities::encode($note);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <operation>edit</operation>
  <expense>
    <id>$expense_id</id>
    <person_name>$person_name</person_name>
    <item_name>$item_name</item_name>
    <vendor_name>$vendor_name</vendor_name>
    <purchase_date>$purchase_date</purchase_date>
    <ledger_account_name>$ledger_account_name</ledger_account_name>
    <locations>$locations</locations>
    <amount>$amount</amount>
    <payer>$payer</payer>
    <payment_method>$payment_method</payment_method>
    <payment_identifier>$payment_identifier</payment_identifier>
    <receipt_file_id>$receipt_file_id</receipt_file_id>
    <receipt_filename>$receipt_filename</receipt_filename>
    <note>$note</note>
  </expense>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_expense_result.xslt", $cgi);
}

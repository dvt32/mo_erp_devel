#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch passwords owned by employee or otherwise visible to employee
  my (%password_by_id, @password_id_list);
  my $sql_query = "select * from password_tbl where owner_id = ? or ? = any(view_id_list) or ? = any(comment_id_list) or ? = any(edit_id_list) order by name";
  my @sql_param_list;
  for(my $i = 0; $i < 4; $i++) {
    push(@sql_param_list, [$employee->{"id"}, SQL_INTEGER]);
  }
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%password_by_id, \@password_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <password_list>
---END_BLOCK---;
  for my $password_id (@password_id_list) {
    my $password = $password_by_id{$password_id};
    my $name = HTML::Entities::encode($password->{"name"});
    my $url = HTML::Entities::encode($password->{"url"} || "");
    my $phone_number = HTML::Entities::encode($password->{"phone_number"} || "");
    my $account_number = HTML::Entities::encode($password->{"account_number"} || "");
    my $username = HTML::Entities::encode($password->{"username"} || "");
    my $email = HTML::Entities::encode($password->{"email"} || "");
    my $note = HTML::Entities::encode($password->{"note"} || "");
    my $needs_update = $password->{"needs_update"};
    $generated_xml .= <<"---END_BLOCK---;";
    <password>
      <id>$password_id</id>
      <name>$name</name>
      <url>$url</url>
      <phone_number>$phone_number</phone_number>
      <account_number>$account_number</account_number>
      <username>$username</username>
      <email>$email</email>
      <note>$note</note>
      <needs_update>$needs_update</needs_update>
    </password>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </password_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_passwords.xslt", $cgi);
}

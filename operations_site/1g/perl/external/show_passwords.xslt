<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Passwords
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="password_list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="password_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
	  Passwords
	</td>
      </tr>
      <tr class="standard_table_column_heading_row" valign="top">
	<td class="standard_table_side_padding"></td>
	<td align="center">*</td>
	<td class="standard_table_spacer_column"></td>
	<td>Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Contact Point</td>
	<td class="standard_table_spacer_column"></td>
	<td>Credentials</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_spacer_column"></td>
	<td>X</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(password) > 0">
	  <xsl:apply-templates select="password"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="11">No passwords.</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="password">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="right"><xsl:if test="needs_update != 0">*</xsl:if></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a class="bare_link">
	  <xsl:attribute name="href">
	    <xsl:text>/1g/external/edit_password_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:value-of select="name"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="url"/>
	<xsl:if test="phone_number != ''"><br/>tel: <xsl:value-of select="phone_number"/></xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:if test="account_number != ''">acctnum: <xsl:value-of select="account_number"/><br/></xsl:if>
        <xsl:if test="username != ''">u: <xsl:value-of select="username"/><br/></xsl:if>
	<xsl:if test="email != ''">e: <xsl:value-of select="email"/><br/></xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/external/delete_password.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:text>X</xsl:text>
	</a>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

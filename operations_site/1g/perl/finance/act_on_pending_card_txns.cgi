#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_database_charset;
use select_row;
use insert_row;
use update_row;
use get_row_by_id_map;
use charge_card;
use apply_xslt_output_xhtml;
use get_timestamp_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/receive_payment/card");
  # get CGI input parameters.
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @recv_payment_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@recv_payment_id_list, $1);
    }
  }
  my $action = $cgi->param("submitButton");
  if ($action eq "Charge") {
    my $now_dt = get_timestamp_now();
    my @charged_row_list;
    for my $recv_payment_id (@recv_payment_id_list) {
      # begin database transaction so our select + charge + update is atomic.
      $dbh->begin_work();
      my $recv_payment = select_row($dbh, "recv_payment_tbl", $recv_payment_id);
      if ((!$recv_payment) || ($recv_payment->{"process_status"} ne "pending") || ($recv_payment->{"payment_method"} ne "card") || ($recv_payment->{"process_txn_after_dt"} gt $now_dt)) {
	$dbh->rollback();
	next;
      }
      my %linkpoint_order =
	(
	 "client_ip_address" => $recv_payment->{"client_ip_addr"} || "",

	 "total_charge" => $recv_payment->{"amount"},

	 "card_number" => $recv_payment->{"card_number"},
	 "card_expiration_month" => $recv_payment->{"card_expiration_month"},
	 "card_expiration_year" => $recv_payment->{"card_expiration_year"},
	 "card_security_code" => $recv_payment->{"card_security_code"},

	 "billing_name" => $recv_payment->{"card_bill_name"},
	 "billing_company" => $recv_payment->{"card_bill_company"} || "",
	 "billing_address_1" => $recv_payment->{"card_bill_address_1"},
	 "billing_address_2" => $recv_payment->{"card_bill_address_2"} || "",
	 "billing_city" => $recv_payment->{"card_bill_city"},
	 "billing_state" => $recv_payment->{"card_bill_state"},
	 "billing_country" => $recv_payment->{"card_bill_country"},
	 "billing_zip_code" => $recv_payment->{"card_bill_zip_code"},
	 "billing_address_num" => $recv_payment->{"card_bill_address_num"} || "",
	 "billing_phone_number" => $recv_payment->{"card_bill_phone_number"} || "",
	 "billing_fax_number" => $recv_payment->{"card_bill_fax_number"} || "",
	 "billing_email_address" => $recv_payment->{"card_bill_email_address"} || "",
	);
      my %linkpoint_response = charge_card($instance_map, %linkpoint_order);
      my $db_charset = get_database_charset($instance_map, "main");
      my @sql_column_list = ();
      push(@sql_column_list, ["cust_acct_id", $recv_payment->{"cust_acct_id"}, SQL_INTEGER]);
      push(@sql_column_list, ["card_number", Encode::encode($db_charset, $recv_payment->{"card_number"})]);
      push(@sql_column_list, ["card_expiration_month", Encode::encode($db_charset, $recv_payment->{"card_expiration_month"})]);
      push(@sql_column_list, ["card_expiration_year", Encode::encode($db_charset, $recv_payment->{"card_expiration_year"})]);
      push(@sql_column_list, ["card_security_code", Encode::encode($db_charset, $recv_payment->{"card_security_code"})]);
      push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $recv_payment->{"card_bill_name"})]);
      push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $recv_payment->{"card_bill_company"})]);
      push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $recv_payment->{"card_bill_address_num"})]);
      push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $recv_payment->{"card_bill_zip_code"})]);
      push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $recv_payment->{"card_bill_address_1"})]);
      push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $recv_payment->{"card_bill_address_2"})]);
      push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $recv_payment->{"card_bill_city"})]);
      push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $recv_payment->{"card_bill_state"})]);
      push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $recv_payment->{"card_bill_country"})]);
      push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $recv_payment->{"card_bill_phone_number"})]);
      push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $recv_payment->{"card_bill_fax_number"})]);
      push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $recv_payment->{"card_bill_email_address"})]);
      push(@sql_column_list, ["total_charge", $recv_payment->{"amount"}, SQL_DECIMAL]);
      push(@sql_column_list, ["recv_payment_id", $recv_payment_id, SQL_INTEGER]); # link to recv_payment_tbl
      push(@sql_column_list, ["processed_dt", ["now()"]]);
      push(@sql_column_list, ["response_avs", Encode::encode($db_charset, $linkpoint_response{"r_avs"})]);
      push(@sql_column_list, ["response_order_num", Encode::encode($db_charset, $linkpoint_response{"r_ordernum"})]);
      push(@sql_column_list, ["response_error", Encode::encode($db_charset, $linkpoint_response{"r_error"})]);
      push(@sql_column_list, ["response_approved", Encode::encode($db_charset, $linkpoint_response{"r_approved"})]);
      push(@sql_column_list, ["response_code", Encode::encode($db_charset, $linkpoint_response{"r_code"})]);
      push(@sql_column_list, ["response_message", Encode::encode($db_charset, $linkpoint_response{"r_message"})]);
      push(@sql_column_list, ["response_txn_dt", Encode::encode($db_charset, $linkpoint_response{"r_time"})]);
      push(@sql_column_list, ["response_reference_num", Encode::encode($db_charset, $linkpoint_response{"r_ref"})]);
      push(@sql_column_list, ["response_tdate", Encode::encode($db_charset, $linkpoint_response{"r_tdate"})]);
      insert_row($dbh, "credit_card_txn_tbl", \@sql_column_list);
      @sql_column_list = ();
      push(@sql_column_list, ["process_status", "complete"]);
      update_row($dbh, "recv_payment_tbl", \@sql_column_list, $recv_payment_id);
      $recv_payment->{"process_status"} = "complete";
      # commit the database transaction.
      $dbh->commit();
      # add to list of processed transactions for display later.
      $recv_payment->{"response_avs"} = $linkpoint_response{"r_avs"};
      $recv_payment->{"response_order_num"} = $linkpoint_response{"r_ordernum"};
      $recv_payment->{"response_error"} = $linkpoint_response{"r_error"};
      $recv_payment->{"response_approved"} = $linkpoint_response{"r_approved"};
      push(@charged_row_list, $recv_payment);
    }
    # fetch accounts.
    my %account_by_id;
    my $sql_query = "select * from account_vw";
    get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
    # fetch employees.
    my %employee_by_id;
    $sql_query = "select * from employee_tbl";
    get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
    # generate XML.
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <recv_payment_list>
---END_BLOCK---;
    for my $recv_payment (@charged_row_list) {
      my $recv_payment_id = $recv_payment->{"id"};
      my $account_id = $recv_payment->{"cust_acct_id"};
      my $account_name = "";
      if (defined($account_id)) {
	if (my $account = $account_by_id{$account_id}) {
	  $account_name = HTML::Entities::encode($account->{"name"});
	}
      }
      my $employee_id = $recv_payment->{"employee_id"};
      my $employee_name = "";
      if (defined($employee_id)) {
	if (my $_employee = $employee_by_id{$employee_id}) {
	  $employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
	}
      }
      my $card_number = HTML::Entities::encode($recv_payment->{"card_number"});
      my $card_expiration_month = HTML::Entities::encode($recv_payment->{"card_expiration_month"});
      my $card_expiration_year = HTML::Entities::encode($recv_payment->{"card_expiration_year"});
      my $card_bill_name = HTML::Entities::encode($recv_payment->{"card_bill_name"});
      my $card_bill_company = HTML::Entities::encode($recv_payment->{"card_bill_company"} || "");
      my $amount = HTML::Entities::encode($recv_payment->{"amount"});
      my $recv_dt = HTML::Entities::encode($recv_payment->{"recv_dt"});
      my $process_txn_after_dt = HTML::Entities::encode($recv_payment->{"process_txn_after_dt"});
      my $process_mode = HTML::Entities::encode($recv_payment->{"process_mode"});
      my $response_avs = HTML::Entities::encode($recv_payment->{"response_avs"} || "");
      my $response_order_num = HTML::Entities::encode($recv_payment->{"response_order_num"} || "");
      my $response_approved = HTML::Entities::encode($recv_payment->{"response_approved"} || "");
      my $response_error = HTML::Entities::encode($recv_payment->{"response_error"} || "");
      my $note = HTML::Entities::encode($recv_payment->{"note"} || "");
      $generated_xml .= <<"---END_BLOCK---;";
    <recv_payment>
      <id>$recv_payment_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <card_bill_name>$card_bill_name</card_bill_name>
      <card_bill_company>$card_bill_company</card_bill_company>
      <amount>$amount</amount>
      <recv_dt>$recv_dt</recv_dt>
      <process_txn_after_dt>$process_txn_after_dt</process_txn_after_dt>
      <process_mode>$process_mode</process_mode>
      <response_avs>$response_avs</response_avs>
      <response_order_num>$response_order_num</response_order_num>
      <response_approved>$response_approved</response_approved>
      <response_error>$response_error</response_error>
      <note>$note</note>
    </recv_payment>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
  </recv_payment_list>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "act_on_pending_card_txns_charge.xslt", $cgi);
  } elsif ($action eq "Delete") {
    my @cancelled_row_list;
    for my $recv_payment_id (@recv_payment_id_list) {
      my $recv_payment = select_row($dbh, "recv_payment_tbl", $recv_payment_id);
      if (($recv_payment) && ($recv_payment->{"process_status"} eq "pending") && ($recv_payment->{"payment_method"} eq "card")) {
	my @sql_column_list = ();
	push(@sql_column_list, ["process_status", "cancelled"]);
	update_row($dbh, "recv_payment_tbl", \@sql_column_list, $recv_payment_id);
	$recv_payment->{"process_status"} = "cancelled";
	push(@cancelled_row_list, $recv_payment);
      }
    }
    # fetch accounts.
    my %account_by_id;
    my $sql_query = "select * from account_vw";
    get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
    # fetch employees.
    my %employee_by_id;
    $sql_query = "select * from employee_tbl";
    get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
    # generate XML.
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <recv_payment_list>
---END_BLOCK---;
    for my $recv_payment (@cancelled_row_list) {
      my $recv_payment_id = $recv_payment->{"id"};
      my $account_id = $recv_payment->{"cust_acct_id"};
      my $account_name = "";
      if (defined($account_id)) {
	if (my $account = $account_by_id{$account_id}) {
	  $account_name = HTML::Entities::encode($account->{"name"});
	}
      }
      my $employee_id = $recv_payment->{"employee_id"};
      my $employee_name = "";
      if (defined($employee_id)) {
	if (my $_employee = $employee_by_id{$employee_id}) {
	  $employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
	}
      }
      my $card_number = HTML::Entities::encode($recv_payment->{"card_number"});
      my $card_expiration_month = HTML::Entities::encode($recv_payment->{"card_expiration_month"});
      my $card_expiration_year = HTML::Entities::encode($recv_payment->{"card_expiration_year"});
      my $card_bill_name = HTML::Entities::encode($recv_payment->{"card_bill_name"});
      my $card_bill_company = HTML::Entities::encode($recv_payment->{"card_bill_company"} || "");
      my $amount = HTML::Entities::encode($recv_payment->{"amount"});
      my $recv_dt = HTML::Entities::encode($recv_payment->{"recv_dt"});
      my $process_txn_after_dt = HTML::Entities::encode($recv_payment->{"process_txn_after_dt"});
      my $process_mode = HTML::Entities::encode($recv_payment->{"process_mode"});
      my $note = HTML::Entities::encode($recv_payment->{"note"} || "");
      $generated_xml .= <<"---END_BLOCK---;";
    <recv_payment>
      <id>$recv_payment_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <card_bill_name>$card_bill_name</card_bill_name>
      <card_bill_company>$card_bill_company</card_bill_company>
      <amount>$amount</amount>
      <recv_dt>$recv_dt</recv_dt>
      <process_txn_after_dt>$process_txn_after_dt</process_txn_after_dt>
      <process_mode>$process_mode</process_mode>
      <note>$note</note>
    </recv_payment>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
  </recv_payment_list>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "act_on_pending_card_txns_delete.xslt", $cgi);
  } else {
    die "show_page: act_on_pending_card_txns_unknown_action.html\n";
  }
}

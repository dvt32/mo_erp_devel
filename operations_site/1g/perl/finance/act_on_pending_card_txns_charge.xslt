<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Charged Credit Card Transactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
          <a href="/1g/index.cgi">Main menu</a>
	  <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
	    |
	    Credit/Debit Card Transactions:
	    <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
	    &middot;
	    <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
	    &middot;
	    <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
	  </xsl:if>
	</p>
	<xsl:apply-templates select="recv_payment_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="recv_payment_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="17" class="standard_table_title">
	  Charged Credit Card Transactions
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Card Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Expires</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Billing Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>AVS</td>
	<td class="standard_table_spacer_column"></td>
	<td>Approved?</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="recv_payment"/>
    </table>
  </xsl:template>

  <xsl:template match="recv_payment">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_bill_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="response_avs"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="response_approved = 'APPROVED'">
	    <xsl:value-of select="response_approved"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:attribute name="class">response_approved_not_approved</xsl:attribute>
	    <xsl:value-of select="response_approved"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
    <xsl:if test="response_error != ''">
      <tr>
	<xsl:choose>
	  <xsl:when test="position() mod 2 = 0">
	    <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	  </xsl:otherwise>
	</xsl:choose>
	<td class="standard_table_side_padding"></td>
	<td align="right"></td>
	<td class="standard_table_spacer_column"></td>
	<td colspan="13" class="response_error">
	  Response Error:
	  <xsl:value-of select="response_error"/>
	</td>
	<td class="standard_table_side_padding"></td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

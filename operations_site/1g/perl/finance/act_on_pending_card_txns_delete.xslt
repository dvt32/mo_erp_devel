<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Deleted Credit Card Transactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
          <a href="/1g/index.cgi">Main menu</a>
	  <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
	    |
	    Credit/Debit Card Transactions:
	    <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
	    &middot;
	    <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
	    &middot;
	    <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
	  </xsl:if>
	</p>
	<xsl:apply-templates select="recv_payment_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="recv_payment_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="23" class="standard_table_title">
	  Deleted Credit Card Transactions
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>ID</td>
	<td class="standard_table_spacer_column"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Card Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Expires</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Billing Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Received By</td>
	<td class="standard_table_spacer_column"></td>
	<td>Received Time</td>
	<td class="standard_table_spacer_column"></td>
	<td>Process Time</td>
	<td class="standard_table_spacer_column"></td>
	<td>Mode</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="recv_payment"/>
    </table>
  </xsl:template>

  <xsl:template match="recv_payment">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="id"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_bill_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="employee_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="substring(recv_dt, 1, 16)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="substring(process_txn_after_dt, 1, 16)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="process_mode"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_database_charset;
use select_row;
use insert_row;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/receive_payment/card");
  # get CGI input parameters.
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @credit_card_txn_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@credit_card_txn_id_list, $1);
    }
  }
  my $action = $cgi->param("submitButton");
  if ($action eq "Enter") {
    my @entered_row_list;
    for my $credit_card_txn_id (@credit_card_txn_id_list) {
      my $credit_card_txn = select_row($dbh, "credit_card_txn_tbl", $credit_card_txn_id);
      next if (!$credit_card_txn);
      # insert new row in recv_payment_tbl.
      my $db_charset = get_database_charset($instance_map, "main");
      my (@sql_column_list);
      push(@sql_column_list, ["cust_acct_id", $credit_card_txn->{"cust_acct_id"}, SQL_INTEGER]);
      push(@sql_column_list, ["employee_id", $employee->{"id"}, SQL_INTEGER]);
      push(@sql_column_list, ["amount", $credit_card_txn->{"total_charge"}, SQL_DECIMAL]);
      push(@sql_column_list, ["recv_dt", ["now()"]]);
      push(@sql_column_list, ["payment_method", "card"]);
      push(@sql_column_list, ["process_txn_after_dt", ["now()"]]);
      push(@sql_column_list, ["process_mode", "manual"]);
      push(@sql_column_list, ["notify_email", "accounts_receivable\@microoffice.com"]);
      push(@sql_column_list, ["process_status", "pending"]);
      push(@sql_column_list, ["card_number", Encode::encode($db_charset, $credit_card_txn->{"card_number"})]);
      push(@sql_column_list, ["card_expiration_month", Encode::encode($db_charset, $credit_card_txn->{"card_expiration_month"})]);
      push(@sql_column_list, ["card_expiration_year", Encode::encode($db_charset, $credit_card_txn->{"card_expiration_year"})]);
      push(@sql_column_list, ["card_security_code", Encode::encode($db_charset, $credit_card_txn->{"card_security_code"})]);
      push(@sql_column_list, ["card_bill_name", Encode::encode($db_charset, $credit_card_txn->{"billing_name"})]);
      push(@sql_column_list, ["card_bill_company", Encode::encode($db_charset, $credit_card_txn->{"billing_company"})]);
      push(@sql_column_list, ["card_bill_address_num", Encode::encode($db_charset, $credit_card_txn->{"billing_address_num"})]);
      push(@sql_column_list, ["card_bill_zip_code", Encode::encode($db_charset, $credit_card_txn->{"billing_zip_code"})]);
      push(@sql_column_list, ["card_bill_address_1", Encode::encode($db_charset, $credit_card_txn->{"billing_address_1"})]);
      push(@sql_column_list, ["card_bill_address_2", Encode::encode($db_charset, $credit_card_txn->{"billing_address_2"})]);
      push(@sql_column_list, ["card_bill_city", Encode::encode($db_charset, $credit_card_txn->{"billing_city"})]);
      push(@sql_column_list, ["card_bill_state", Encode::encode($db_charset, $credit_card_txn->{"billing_state"})]);
      push(@sql_column_list, ["card_bill_country", Encode::encode($db_charset, $credit_card_txn->{"billing_country"})]);
      push(@sql_column_list, ["card_bill_phone_number", Encode::encode($db_charset, $credit_card_txn->{"billing_phone_number"})]);
      push(@sql_column_list, ["card_bill_fax_number", Encode::encode($db_charset, $credit_card_txn->{"billing_fax_number"})]);
      push(@sql_column_list, ["card_bill_email_address", Encode::encode($db_charset, $credit_card_txn->{"billing_email_address"})]);
      insert_row($dbh, "recv_payment_tbl", \@sql_column_list);
      push(@entered_row_list, $credit_card_txn);
    }
    # fetch accounts.
    my %account_by_id;
    my $sql_query = "select * from account_vw";
    get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
    # populate information needed for display of page.
    # generate XML.
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <credit_card_txn_list>
---END_BLOCK---;
    for my $credit_card_txn (@entered_row_list) {
      my $credit_card_txn_id = $credit_card_txn->{"id"};
      my $account_id = $credit_card_txn->{"cust_acct_id"};
      my $account_name = "";
      if (defined($account_id)) {
	if (my $account = $account_by_id{$account_id}) {
	  $account_name = HTML::Entities::encode($account->{"name"});
	}
      }
      my $card_number = HTML::Entities::encode($credit_card_txn->{"card_number"});
      my $card_expiration_month = HTML::Entities::encode($credit_card_txn->{"card_expiration_month"});
      my $card_expiration_year = HTML::Entities::encode($credit_card_txn->{"card_expiration_year"});
      my $card_bill_name = HTML::Entities::encode($credit_card_txn->{"billing_name"});
      my $card_bill_company = HTML::Entities::encode($credit_card_txn->{"billing_company"} || "");
      my $amount = HTML::Entities::encode($credit_card_txn->{"total_charge"});
      $generated_xml .= <<"---END_BLOCK---;";
    <credit_card_txn>
      <id>$credit_card_txn_id</id>
      <account_name>$account_name</account_name>
      <amount>$amount</amount>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <card_bill_name>$card_bill_name</card_bill_name>
      <card_bill_company>$card_bill_company</card_bill_company>
    </credit_card_txn>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
  </credit_card_txn_list>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "act_on_processed_card_txns_enter.xslt", $cgi);
  } else {
    die "show_page: act_on_processed_card_txns_unknown_action.html\n";
  }
}

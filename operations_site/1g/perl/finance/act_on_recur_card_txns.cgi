#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_database_charset;
use select_row;
use insert_row;
use update_row;
use delete_row;
use get_row_by_id_map;
use apply_xslt_output_xhtml;
use get_date_now;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/receive_payment/card");
  # get CGI input parameters.
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @recur_card_txn_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@recur_card_txn_id_list, $1);
    }
  }
  my $action = $cgi->param("submitButton");
  if ($action eq "Enter") {
    my $advance_next_date = $cgi->param("advance_next_date") ? 1 : 0;
    my @entered_row_list;
    for my $recur_card_txn_id (@recur_card_txn_id_list) {
      my $recur_card_txn = select_row($dbh, "recur_card_txn_tbl", $recur_card_txn_id);
      next if (!$recur_card_txn);
      # insert new row in recv_payment_tbl.
      my $db_charset = get_database_charset($instance_map, "main");
      my (@sql_column_list);
      push(@sql_column_list, ["cust_acct_id", $recur_card_txn->{"account_id"}, SQL_INTEGER]);
      push(@sql_column_list, ["employee_id", $recur_card_txn->{"employee_id"}, SQL_INTEGER]);
      push(@sql_column_list, ["amount", $recur_card_txn->{"amount"}, SQL_DECIMAL]);
      push(@sql_column_list, ["recv_dt", ["now()"]]);
      push(@sql_column_list, ["payment_method", "card"]);
      push(@sql_column_list, ["process_txn_after_dt", ["now()"]]);
      push(@sql_column_list, ["process_mode", Encode::encode($db_charset, $recur_card_txn->{"process_mode"})]);
      push(@sql_column_list, ["notify_email", "accounts_receivable\@microoffice.com"]);
      push(@sql_column_list, ["process_status", "pending"]);
      push(@sql_column_list, ["card_number", Encode::encode($db_charset, $recur_card_txn->{"card_number"})]);
      push(@sql_column_list, ["card_expiration_month", Encode::encode($db_charset, $recur_card_txn->{"card_expiration_month"})]);
      push(@sql_column_list, ["card_expiration_year", Encode::encode($db_charset, $recur_card_txn->{"card_expiration_year"})]);
      push(@sql_column_list, ["card_security_code", Encode::encode($db_charset, $recur_card_txn->{"card_security_code"})]);
      push(@sql_column_list, ["card_bill_name", Encode::encode($db_charset, $recur_card_txn->{"card_bill_name"})]);
      push(@sql_column_list, ["card_bill_company", Encode::encode($db_charset, $recur_card_txn->{"card_bill_company"})]);
      push(@sql_column_list, ["card_bill_address_num", Encode::encode($db_charset, $recur_card_txn->{"card_bill_address_num"})]);
      push(@sql_column_list, ["card_bill_zip_code", Encode::encode($db_charset, $recur_card_txn->{"card_bill_zip_code"})]);
      push(@sql_column_list, ["card_bill_address_1", Encode::encode($db_charset, $recur_card_txn->{"card_bill_address_1"})]);
      push(@sql_column_list, ["card_bill_address_2", Encode::encode($db_charset, $recur_card_txn->{"card_bill_address_2"})]);
      push(@sql_column_list, ["card_bill_city", Encode::encode($db_charset, $recur_card_txn->{"card_bill_city"})]);
      push(@sql_column_list, ["card_bill_state", Encode::encode($db_charset, $recur_card_txn->{"card_bill_state"})]);
      push(@sql_column_list, ["card_bill_country", Encode::encode($db_charset, $recur_card_txn->{"card_bill_country"})]);
      push(@sql_column_list, ["card_bill_phone_number", Encode::encode($db_charset, $recur_card_txn->{"card_bill_phone_number"})]);
      push(@sql_column_list, ["card_bill_fax_number", Encode::encode($db_charset, $recur_card_txn->{"card_bill_fax_number"})]);
      push(@sql_column_list, ["card_bill_email_address", Encode::encode($db_charset, $recur_card_txn->{"card_bill_email_address"})]);
      push(@sql_column_list, ["note", Encode::encode($db_charset, $recur_card_txn->{"note"})]);
      insert_row($dbh, "recv_payment_tbl", \@sql_column_list);
      if ($advance_next_date) {
	# if advance_next_date checked, advance the next_date column for chosen rows.
	$recur_card_txn->{"next_date"} =~ /^(\d{4})-(\d\d)-(\d\d)$/;
	my ($year, $month, $day) = ($1, $2, $3);
	if (++$month > 12) {
	  $month = "1";
	  $year++;
	}
	$month = lead_pad($month, 2);
	my $new_next_date = "$year-$month-$day";
	@sql_column_list = ();
	push(@sql_column_list, ["next_date", $new_next_date]);
	update_row($dbh, "recur_card_txn_tbl", \@sql_column_list, $recur_card_txn_id);
	$recur_card_txn->{"next_date"} = $new_next_date;
      }
      push(@entered_row_list, $recur_card_txn);
    }
    # fetch accounts.
    my %account_by_id;
    my $sql_query = "select * from account_vw";
    get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
    # fetch employees.
    my %employee_by_id;
    $sql_query = "select * from employee_tbl";
    get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
    # populate information needed for display of page.
    my ($today_year, $today_month, $today_day) = get_date_now(1);
    my $today_date = "$today_year-$today_month-$today_day";
    # generate XML.
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <today_date>$today_date</today_date>
  <advance_next_date>$advance_next_date</advance_next_date>
  <recur_card_txn_list>
---END_BLOCK---;
    for my $recur_card_txn (@entered_row_list) {
      my $recur_card_txn_id = $recur_card_txn->{"id"};
      my $account_id = $recur_card_txn->{"account_id"};
      my $account_name = "";
      if (defined($account_id)) {
	if (my $account = $account_by_id{$account_id}) {
	  $account_name = HTML::Entities::encode($account->{"name"});
	}
      }
      my $employee_id = $recur_card_txn->{"employee_id"};
      my $employee_name = "";
      if (defined($employee_id)) {
	if (my $_employee = $employee_by_id{$employee_id}) {
	  $employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
	}
      }
      my $card_number = HTML::Entities::encode($recur_card_txn->{"card_number"});
      my $card_expiration_month = HTML::Entities::encode($recur_card_txn->{"card_expiration_month"});
      my $card_expiration_year = HTML::Entities::encode($recur_card_txn->{"card_expiration_year"});
      my $card_bill_name = HTML::Entities::encode($recur_card_txn->{"card_bill_name"});
      my $card_bill_company = HTML::Entities::encode($recur_card_txn->{"card_bill_company"} || "");
      my $amount = HTML::Entities::encode($recur_card_txn->{"amount"});
      my $process_mode = HTML::Entities::encode($recur_card_txn->{"process_mode"});
      my $frequency = HTML::Entities::encode($recur_card_txn->{"frequency"});
      my $next_date = HTML::Entities::encode($recur_card_txn->{"next_date"});
      my $note = HTML::Entities::encode($recur_card_txn->{"note"} || "");
      $generated_xml .= <<"---END_BLOCK---;";
    <recur_card_txn>
      <id>$recur_card_txn_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <amount>$amount</amount>
      <process_mode>$process_mode</process_mode>
      <frequency>$frequency</frequency>
      <next_date>$next_date</next_date>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <card_bill_name>$card_bill_name</card_bill_name>
      <card_bill_company>$card_bill_company</card_bill_company>
      <note>$note</note>
    </recur_card_txn>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
  </recur_card_txn_list>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "act_on_recur_card_txns_enter.xslt", $cgi);
  } elsif ($action eq "Delete") {
    my @deleted_row_list;
    for my $recur_card_txn_id (@recur_card_txn_id_list) {
      my $recur_card_txn = select_row($dbh, "recur_card_txn_tbl", $recur_card_txn_id);
      if ($recur_card_txn) {
	delete_row($dbh, "recur_card_txn_tbl", $recur_card_txn_id);
	push(@deleted_row_list, $recur_card_txn);
      }
    }
    # fetch accounts.
    my %account_by_id;
    my $sql_query = "select * from account_vw";
    get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
    # fetch employees.
    my %employee_by_id;
    $sql_query = "select * from employee_tbl";
    get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
    # generate XML.
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <recur_card_txn_list>
---END_BLOCK---;
    for my $recur_card_txn (@deleted_row_list) {
      my $recur_card_txn_id = $recur_card_txn->{"id"};
      my $account_id = $recur_card_txn->{"account_id"};
      my $account_name = "";
      if (defined($account_id)) {
	if (my $account = $account_by_id{$account_id}) {
	  $account_name = HTML::Entities::encode($account->{"name"});
	}
      }
      my $employee_id = $recur_card_txn->{"employee_id"};
      my $employee_name = "";
      if (defined($employee_id)) {
	if (my $_employee = $employee_by_id{$employee_id}) {
	  $employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
	}
      }
      my $card_number = HTML::Entities::encode($recur_card_txn->{"card_number"});
      my $card_expiration_month = HTML::Entities::encode($recur_card_txn->{"card_expiration_month"});
      my $card_expiration_year = HTML::Entities::encode($recur_card_txn->{"card_expiration_year"});
      my $card_bill_name = HTML::Entities::encode($recur_card_txn->{"card_bill_name"});
      my $card_bill_company = HTML::Entities::encode($recur_card_txn->{"card_bill_company"} || "");
      my $amount = HTML::Entities::encode($recur_card_txn->{"amount"});
      my $process_mode = HTML::Entities::encode($recur_card_txn->{"process_mode"});
      my $frequency = HTML::Entities::encode($recur_card_txn->{"frequency"});
      my $next_date = HTML::Entities::encode($recur_card_txn->{"next_date"});
      my $note = HTML::Entities::encode($recur_card_txn->{"note"} || "");
      $generated_xml .= <<"---END_BLOCK---;";
    <recur_card_txn>
      <id>$recur_card_txn_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <amount>$amount</amount>
      <process_mode>$process_mode</process_mode>
      <frequency>$frequency</frequency>
      <next_date>$next_date</next_date>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <card_bill_name>$card_bill_name</card_bill_name>
      <card_bill_company>$card_bill_company</card_bill_company>
      <note>$note</note>
    </recur_card_txn>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
  </recur_card_txn_list>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "act_on_recur_card_txns_delete.xslt", $cgi);
  } else {
    die "show_page: act_on_recur_card_txns_unknown_action.html\n";
  }
}

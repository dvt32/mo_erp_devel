#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI parameters.
  my $ach_debit_txn_id = $cgi->param("id");
  if ((!defined($ach_debit_txn_id)) || ($ach_debit_txn_id !~ /^\d+$/)) {
    die "show_page: cancel_ach_debit_txn_not_found.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the ACH debit transaction exists.
  my $ach_debit_txn = select_row($dbh, "ach_debit_txn_tbl", $ach_debit_txn_id);
  if (!$ach_debit_txn) {
    $dbh->rollback();
    die "show_page: cancel_ach_debit_txn_not_found.html\n";
  }
  my @sql_column_list = (["cancelled", 1, SQL_BOOLEAN]);
  update_row($dbh, "ach_debit_txn_tbl", \@sql_column_list, $ach_debit_txn_id);
  # commit the transaction.
  $dbh->commit();
  # load rows needed for display of results.
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $ach_debit_txn->{"cust_acct_id"});
  # populate information needed for display of page.
  my $account_name = "";
  if ($cust_acct) {
    $account_name = $cust_acct->{"name"};
  } else {
    $account_name = "(inactive account)";
  }
  $account_name = HTML::Entities::encode($account_name);
  my $amount = HTML::Entities::encode($ach_debit_txn->{"amount"});
  my $customer_name = HTML::Entities::encode($ach_debit_txn->{"customer_name"});
  my $customer_bank_id = HTML::Entities::encode($ach_debit_txn->{"customer_bank_id"});
  my $customer_bank_acct_num = HTML::Entities::encode($ach_debit_txn->{"customer_bank_acct_num"});
  my $customer_bank_acct_type = HTML::Entities::encode($ach_debit_txn->{"customer_bank_acct_type"});
  my $company_bank_acct_num = HTML::Entities::encode($ach_debit_txn->{"company_bank_acct_num"});
  my $company_bank_acct_type = HTML::Entities::encode($ach_debit_txn->{"company_bank_acct_type"});
  my $queued_dt = HTML::Entities::encode($ach_debit_txn->{"queued_dt"});
  my $notes = HTML::Entities::encode($ach_debit_txn->{"notes"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ach-debit-txn>
    <id>$ach_debit_txn_id</id>
    <account_name>$account_name</account_name>
    <amount>$amount</amount>
    <customer_name>$customer_name</customer_name>
    <customer_bank_id>$customer_bank_id</customer_bank_id>
    <customer_bank_acct_num>$customer_bank_acct_num</customer_bank_acct_num>
    <customer_bank_acct_type>$customer_bank_acct_type</customer_bank_acct_type>
    <company_bank_acct_num>$company_bank_acct_num</company_bank_acct_num>
    <company_bank_acct_type>$company_bank_acct_type</company_bank_acct_type>
    <queued_dt>$queued_dt</queued_dt>
    <notes>$notes</notes>
  </ach-debit-txn>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "cancel_ach_debit_txn_success.xslt", $cgi);
}

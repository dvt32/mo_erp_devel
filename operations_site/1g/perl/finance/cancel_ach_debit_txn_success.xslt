<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Cancel ACH Debit Transaction
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="ach-debit-txn"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ach-debit-txn">
    <p>
      You successfully cancelled the following ACH debit transaction:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Amount:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Bank Routing Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_bank_id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Bank Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_bank_acct_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Bank Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_bank_acct_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company Bank Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company_bank_acct_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company Bank Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company_bank_acct_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Queued Timestamp:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="queued_dt"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI parameters.
  my $electronic_check_id = $cgi->param("id");
  if ((!defined($electronic_check_id)) || ($electronic_check_id !~ /^\d+$/)) {
    die "show_page: cancel_electronic_check_not_found.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the electronic check exists.
  my $electronic_check = select_row($dbh, "electronic_check_tbl", $electronic_check_id);
  if (!$electronic_check) {
    $dbh->rollback();
    die "show_page: cancel_electronic_check_not_found.html\n";
  }
  my @sql_column_list = (["cancelled", 1, SQL_BOOLEAN]);
  update_row($dbh, "electronic_check_tbl", \@sql_column_list, $electronic_check_id);
  # commit the transaction.
  $dbh->commit();
  # load rows needed for display of results.
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $electronic_check->{"cust_acct_id"});
  # populate information needed for display of page.
  my $account_name = "";
  if ($cust_acct) {
    $account_name = $cust_acct->{"name"};
  } else {
    $account_name = "(inactive account)";
  }
  $account_name = HTML::Entities::encode($account_name);
  my $bank_account_name = HTML::Entities::encode($electronic_check->{"bank_account_name"});
  my $bank_account_type = HTML::Entities::encode($electronic_check->{"bank_account_type"});
  my $routing_number = HTML::Entities::encode($electronic_check->{"routing_number"});
  my $account_number = HTML::Entities::encode($electronic_check->{"account_number"});
  my $amount = HTML::Entities::encode($electronic_check->{"amount"});
  my $debit_date = HTML::Entities::encode($electronic_check->{"debit_date"});
  my $notes = HTML::Entities::encode($electronic_check->{"notes"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <electronic-check>
    <id>$electronic_check_id</id>
    <account_name>$account_name</account_name>
    <bank_account_name>$bank_account_name</bank_account_name>
    <bank_account_type>$bank_account_type</bank_account_type>
    <routing_number>$routing_number</routing_number>
    <account_number>$account_number</account_number>
    <amount>$amount</amount>
    <debit_date>$debit_date</debit_date>
    <notes>$notes</notes>
  </electronic-check>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "cancel_electronic_check_success.xslt", $cgi);
}

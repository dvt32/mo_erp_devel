<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Cancel Electronic Check
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="electronic-check"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="electronic-check">
    <p>
      You successfully cancelled the following electronic check:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bank Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="bank_account_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Routing Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="routing_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Amount:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Debit Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="debit_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

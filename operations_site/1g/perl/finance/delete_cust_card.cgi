#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/edit_cust_pmt_accts");
  # get input data.
  my $credit_card_id = $cgi->param("id");
  my $credit_card;
  if ((!$credit_card_id) || (!($credit_card = select_row($dbh, "credit_card_tbl", $credit_card_id)))) {
    die "show_page: delete_cust_card_bad_id.html\n";
  }
  # delete row in credit_card_tbl.
  delete_row($dbh, "credit_card_tbl", $credit_card_id);
  # populate information needed for display of page.
  my $account_id = $credit_card->{"account_id"};
  my $account = select_row($dbh, "account_vw", $account_id);
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $person_id = $credit_card->{"person_id"};
  my $person = select_row($dbh, "person_vw", $person_id);
  my $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
  my $card_nickname = HTML::Entities::encode($credit_card->{"card_nickname"} || "");
  my $card_number = $credit_card->{"card_number"};
  my $card_security_code = $credit_card->{"card_security_code"} || "";
  my $card_expiration_month = $credit_card->{"card_expiration_month"};
  my $card_expiration_year = $credit_card->{"card_expiration_year"};
  my $billing_name = HTML::Entities::encode($credit_card->{"billing_name"});
  my $billing_company = HTML::Entities::encode($credit_card->{"billing_company"} || "");
  my $billing_address_1 = HTML::Entities::encode($credit_card->{"billing_address_1"});
  my $billing_address_2 = HTML::Entities::encode($credit_card->{"billing_address_2"} || "");
  my $billing_city = HTML::Entities::encode($credit_card->{"billing_city"});
  my $billing_state = HTML::Entities::encode($credit_card->{"billing_state"});
  my $billing_zip_code = HTML::Entities::encode($credit_card->{"billing_zip_code"});
  my $billing_country = HTML::Entities::encode($credit_card->{"billing_country"});
  my $billing_phone_number = HTML::Entities::encode($credit_card->{"billing_phone_number"} || "");
  my $billing_fax_number = HTML::Entities::encode($credit_card->{"billing_fax_number"} || "");
  my $billing_email_address = HTML::Entities::encode($credit_card->{"billing_email_address"} || "");
  my $billing_address_num = HTML::Entities::encode($credit_card->{"billing_address_num"} || "");
  my $note = HTML::Entities::encode($credit_card->{"note"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <credit_card>
    <id>$credit_card_id</id>
    <account_name>$account_name</account_name>
    <person_name>$person_name</person_name>
    <card_nickname>$card_nickname</card_nickname>
    <card_number>$card_number</card_number>
    <card_security_code>$card_security_code</card_security_code>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <billing_address_num>$billing_address_num</billing_address_num>
    <note>$note</note>
  </credit_card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_cust_card.xslt", $cgi);
}

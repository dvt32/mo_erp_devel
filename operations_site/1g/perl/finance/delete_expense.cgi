#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;
use get_role_list;
use delete_row;
use delete_file;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $expense_id = $cgi->param("id");
  my $expense;
  if ((!$expense_id) || (!($expense = select_row($dbh, "expense_tbl", $expense_id)))) {
    die "show_page: delete_expense_not_found.html\n";
  }
  # get role list of current user.
  my $employee_id = $employee->{"id"};
  my $role_list = get_role_list($instance_map, $employee_id);
  # check that expense is owned by current user or that user can view all expenses.
  if ((!grep {$_ eq "role:finance/view_all_expenses"} @$role_list) && ($expense->{"person_id"} != $employee_id)) {
    die "show_page: delete_expense_not_owner.html\n";
  }
  # check that expense has not already been reimbursed.
  if ($expense->{"is_reimbursed"}) {
    die "show_page: delete_expense_already_reimbursed.html\n";
  }
  # check that expense has not already been reconciled.
  if ($expense->{"is_reconciled"}) {
    die "show_page: delete_expense_already_reconciled.html\n";
  }
  # load linked employee.
  my $person;
  if (!($person = select_row($dbh, "employee_tbl", $expense->{"person_id"}))) {
    die "show_page: delete_expense_bad_employee.html\n";
  }
  # load linked ledger account.
  my $ledger_account;
  if (!($ledger_account = select_row($dbh, "ledger_account_tbl", $expense->{"ledger_account_id"}))) {
    die "show_page: delete_expense_bad_ledger_account.html\n";
  }
  # delete expense.
  delete_row($dbh, "expense_tbl", $expense_id);
  # delete receipt (if any).
  if ($expense->{"receipt_file_id"}) {
    delete_file($instance_map, $expense->{"receipt_file_id"});
  }
  # populate information for display of page.
  my $person_name = HTML::Entities::encode($person->{"first_name"} . " " . $person->{"last_name"});
  my $item_name = HTML::Entities::encode($expense->{"item_name"});
  my $vendor_name = HTML::Entities::encode($expense->{"vendor_name"});
  my $purchase_date = $expense->{"purchase_date"};
  my $ledger_account_name = HTML::Entities::encode($ledger_account->{"name"});
  my $locations = HTML::Entities::encode($expense->{"locations"});
  my $amount = $expense->{"amount"};
  my $payer = HTML::Entities::encode($expense->{"payer"});
  my $payment_method = HTML::Entities::encode($expense->{"payment_method"});
  my $payment_identifier = HTML::Entities::encode($expense->{"payment_identifier"} || "");
  my $is_reconciled = $expense->{"is_reconciled"} ? 1 : 0;
  my $is_reimbursed = $expense->{"is_reimbursed"} ? 1 : 0;
  my $note = HTML::Entities::encode($expense->{"note"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <expense>
    <id>$expense_id</id>
    <person_name>$person_name</person_name>
    <item_name>$item_name</item_name>
    <vendor_name>$vendor_name</vendor_name>
    <purchase_date>$purchase_date</purchase_date>
    <ledger_account_name>$ledger_account_name</ledger_account_name>
    <locations>$locations</locations>
    <amount>$amount</amount>
    <payer>$payer</payer>
    <payment_method>$payment_method</payment_method>
    <payment_identifier>$payment_identifier</payment_identifier>
    <note>$note</note>
  </expense>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_expense.xslt", $cgi);
}

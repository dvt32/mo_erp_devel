#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Archive::Zip;
use Text::CSV_XS;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use select_row;
use get_file;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/view_all_expenses");
  # get CGI input parameters.
  my $id_csv = $cgi->param("id_csv");
  my @expense_id_list = split(/,/, $id_csv);
  # fetch all persons.
  my (%person_by_id);
  my $sql_query = "select * from person_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  # get all ledger accounts.
  my (%ledger_account_by_id);
  $sql_query = "select * from ledger_account_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%ledger_account_by_id);
  # generate CSV file of expenses.
  my ($zip_file_handle, $zip_file_as_string);
  open($zip_file_handle, ">", \$zip_file_as_string);
  my $zip_object = Archive::Zip->new();
  my $csv = Text::CSV_XS->new();
  my $csv_file_data = "";
  $csv->combine("ID", "Person", "Item", "Vendor", "Ledger Account", "Locations", "Buy Date", "Amount", "Pay Method", "Reim", "Note");
  $csv_file_data .= $csv->string();
  $csv_file_data .= "\n";
  for my $expense_id (@expense_id_list) {
    if (my $expense = select_row($dbh, "expense_tbl", $expense_id)) {
      my $file_id = $expense->{"receipt_file_id"};
      if ($file_id) {
	my $file_data;
	get_file($instance_map, $file_id, \$file_data);
	$zip_object->addString($file_data, "$expense_id.pdf");
      }
      my @column_list;
      push(@column_list, $expense_id);
      my $person_id = $expense->{"person_id"};
      my $person = $person_by_id{$person_id};
      my $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
      push(@column_list, $person_name);
      push(@column_list, $expense->{"item_name"});
      push(@column_list, $expense->{"vendor_name"});
      push(@column_list, $ledger_account_by_id{$expense->{"ledger_account_id"}}->{"name"});
      push(@column_list, $expense->{"locations"});
      push(@column_list, $expense->{"purchase_date"});
      push(@column_list, $expense->{"amount"});
      push(@column_list, $expense->{"payer"} . " " . $expense->{"payment_method"});
      my $is_reimbursed = "";
      if ($expense->{"is_reimbursed"}) {
	$is_reimbursed = "Yes";
      } elsif ($expense->{"payer"} eq "personal") {
	$is_reimbursed = "No";
      } else {
	$is_reimbursed = "--";
      }
      push(@column_list, $is_reimbursed);
      push(@column_list, $expense->{"note"});
      $csv->combine(@column_list);
      $csv_file_data .= $csv->string();
      $csv_file_data .= "\n";
    }
  }
  $zip_object->addString($csv_file_data, "expenses.csv");
  $zip_object->writeToFileHandle($zip_file_handle, 0);
  close($zip_file_handle);
  # print out data to stdout as a ZIP file.
  print $cgi->header
    ("-type" => "application/zip",
     "-content-disposition" => "attachment; filename=\"expenses.zip\"");
  print $zip_file_as_string;
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;
use update_row;
use lead_pad;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI input parameters.
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @ach_debit_txn_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@ach_debit_txn_id_list, $1);
    }
  }
  my $mark_as_processed = $cgi->param("mark_as_processed") ? 1 : 0;
  # determine today's date.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("all");
  my $now_dt = "$year-$month-$day $hour:$minute:$second";
  # begin database transaction so our updates are atomic.
  $dbh->begin_work();
  my @sql_column_list;
  # fetch ACH debit transactions and mark them as processed if mark_as_processed is true.
  my (%ach_debit_txn_by_id, @processed_ach_debit_txn_id_list);
  for my $ach_debit_txn_id (@ach_debit_txn_id_list) {
    my $ach_debit_txn = select_row($dbh, "ach_debit_txn_tbl", $ach_debit_txn_id);
    if ((!$ach_debit_txn) || (defined($ach_debit_txn->{"processed_dt"}))) {
      next;
    }
    if ($mark_as_processed) {
      @sql_column_list = ();
      push(@sql_column_list, ["processed_dt", $now_dt]);
      update_row($dbh, "ach_debit_txn_tbl", \@sql_column_list, $ach_debit_txn_id);
      $ach_debit_txn->{"processed_dt"} = $now_dt;
    }
    push(@processed_ach_debit_txn_id_list, $ach_debit_txn_id);
    $ach_debit_txn_by_id{$ach_debit_txn_id} = $ach_debit_txn;
  }
  # commit the database transaction.
  $dbh->commit();
  # print out data to stdout as a CSV file.
  print $cgi->header
    ("-type" => "application/vnd.ms-excel",
     "-content-disposition" => "attachment; filename=\"ach_debit_txn_list.csv\"");
  my @column_headings = ("Amount", "Customer Name", "Customer ID", "Customer Bank ID", "Customer Bank Acct No", "Customer Bank Acct Type", "Company Acct No", "Company Acct Type", "Start Date", "Prenote Date", "Last Collection Date", "Create Prenote", "Hold Transfer");
  my $csv_line;
  $csv_line = join(",", map {if (/,/) {qq{"$_"}} else {$_}} @column_headings);
  print $csv_line;
  print "\n";
  for my $ach_debit_txn_id (@processed_ach_debit_txn_id_list) {
    my $ach_debit_txn = $ach_debit_txn_by_id{$ach_debit_txn_id};
    my @column_values = ();
    push(@column_values, $ach_debit_txn->{"amount"});
    push(@column_values, $ach_debit_txn->{"customer_name"});
    push(@column_values, $ach_debit_txn->{"cust_acct_id"} || "");
    push(@column_values, $ach_debit_txn->{"customer_bank_id"});
    push(@column_values, $ach_debit_txn->{"customer_bank_acct_num"});
    push(@column_values, $ach_debit_txn->{"customer_bank_acct_type"});
    push(@column_values, $ach_debit_txn->{"company_bank_acct_num"});
    push(@column_values, $ach_debit_txn->{"company_bank_acct_type"});
    push(@column_values, $ach_debit_txn->{"start_date"} || "");
    push(@column_values, $ach_debit_txn->{"prenote_date"} || "");
    push(@column_values, $ach_debit_txn->{"last_collection_date"} || "");
    push(@column_values, $ach_debit_txn->{"create_prenote"} ? "Y" : "N");
    push(@column_values, $ach_debit_txn->{"hold_transfer"} ? "Y" : "N");
    $csv_line = join(",", map {if (($_) && (/,/)) {qq{"$_"}} else {defined($_) ? $_ : ""}} @column_values);
    print $csv_line;
    print "\n";    
  }
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use get_row_by_id_map;
use get_row_list;
use get_month_name;
use apply_xslt_output_xhtml;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/edit_cust_pmt_accts");
  # get input data.
  my $credit_card_id = $cgi->param("id");
  my $credit_card;
  if ((!$credit_card_id) || (!($credit_card = select_row($dbh, "credit_card_tbl", $credit_card_id)))) {
    die "show_page: edit_cust_card_bad_id.html\n";
  }
  # load accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # load persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  $sql_query = "select * from cust_pers_tbl order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id, \@cust_pers_id_list);
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # populate information needed for display of page.
  my $account_id = $credit_card->{"account_id"} || "";
  my $person_id = $credit_card->{"person_id"} || "";
  my $card_nickname = HTML::Entities::encode($credit_card->{"card_nickname"} || "");
  my $card_number = $credit_card->{"card_number"};
  my $card_security_code = $credit_card->{"card_security_code"} || "";
  my $card_expiration_month = $credit_card->{"card_expiration_month"};
  my $card_expiration_year = $credit_card->{"card_expiration_year"};
  my $billing_name = HTML::Entities::encode($credit_card->{"billing_name"});
  my $billing_company = HTML::Entities::encode($credit_card->{"billing_company"} || "");
  my $billing_address_1 = HTML::Entities::encode($credit_card->{"billing_address_1"});
  my $billing_address_2 = HTML::Entities::encode($credit_card->{"billing_address_2"} || "");
  my $billing_city = HTML::Entities::encode($credit_card->{"billing_city"});
  my $billing_state = $credit_card->{"billing_state"};
  my $billing_state_other = "";
  if (!grep {$billing_state eq $_->{"name"}} @state_list) {
    $billing_state_other = $billing_state;
    $billing_state = "";
  }
  $billing_state = HTML::Entities::encode($billing_state);
  $billing_state_other = HTML::Entities::encode($billing_state_other);
  my $billing_zip_code = HTML::Entities::encode($credit_card->{"billing_zip_code"});
  my $billing_country = HTML::Entities::encode($credit_card->{"billing_country"});
  my $billing_phone_number = HTML::Entities::encode($credit_card->{"billing_phone_number"} || "");
  my $billing_fax_number = HTML::Entities::encode($credit_card->{"billing_fax_number"} || "");
  my $billing_email_address = HTML::Entities::encode($credit_card->{"billing_email_address"} || "");
  my $billing_address_num = HTML::Entities::encode($credit_card->{"billing_address_num"} || "");
  my $note = HTML::Entities::encode($credit_card->{"note"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $account = $cust_acct_by_id{$cust_acct_id};
    my $name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <name>$name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <person-list>
---END_BLOCK---;
  for my $person_id (@cust_pers_id_list) {
    my $person = $cust_pers_by_id{$person_id};
    my $first_name = HTML::Entities::encode($person->{"first_name"});
    my $last_name = HTML::Entities::encode($person->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$person_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$padded_number</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <credit_card>
    <id>$credit_card_id</id>
    <account_id>$account_id</account_id>
    <person_id>$person_id</person_id>
    <card_nickname>$card_nickname</card_nickname>
    <card_number>$card_number</card_number>
    <card_security_code>$card_security_code</card_security_code>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_state_other>$billing_state_other</billing_state_other>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <billing_address_num>$billing_address_num</billing_address_num>
    <note>$note</note>
  </credit_card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_cust_card_input.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;
use is_valid_credit_card;
use undef_or_value;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/edit_cust_pmt_accts");
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $credit_card_id = $cgi->param("id");
  my $credit_card;
  if ((!$credit_card_id) || (!($credit_card = select_row($dbh, "credit_card_tbl", $credit_card_id)))) {
    die "show_page: edit_cust_card_bad_id.html\n";
  }
  my $account_id = $cgi->param("account_id");
  my $account;
  if ((!$account_id) || ($account_id !~ /^\d+$/) || (!($account = select_row($dbh, "account_vw", $account_id)))) {
    die "show_page: edit_cust_card_bad_account.html\n";
  }
  my $person_id = $cgi->param("person_id");
  my $person;
  if ((!$person_id) || ($person_id !~ /^\d+$/) || (!($person = select_row($dbh, "person_vw", $person_id)))) {
    die "show_page: edit_cust_card_bad_person.html\n";
  }
  if ($person->{"account_id"} != $account_id) {
    die "show_page: edit_cust_card_account_person_mismatch.html\n";
  }
  my $card_nickname = Encode::decode($cgi_input_charset, $cgi->param("card_nickname"));
  my $card_number = $cgi->param("card_number");
  if (!is_valid_credit_card($card_number)) {
    die "show_page: edit_cust_card_bad_card_number.html\n";
  }
  my $card_security_code = $cgi->param("card_security_code");
  if (($card_security_code) && ($card_security_code !~ /^\d+$/)) {
    die "show_page: edit_cust_card_bad_card_security_code.html\n";
  }
  my $card_expiration_month = $cgi->param("card_expiration_month");
  if (($card_expiration_month !~ /^\d\d$/) || (int($card_expiration_month) < 1) || (int($card_expiration_month) > 12)) {
    die "show_page: edit_cust_card_bad_card_expiration_month.html\n";
  }
  my $card_expiration_year = $cgi->param("card_expiration_year");
  if (($card_expiration_year !~ /^\d{4}$/) || ($card_expiration_year < 2000) || ($card_expiration_year > 3000)) {
    die "show_page: edit_cust_card_bad_card_expiration_year.html\n";
  }
  my $billing_name = Encode::decode($cgi_input_charset, $cgi->param("billing_name"));
  if (!$billing_name) {
    die "show_page: edit_cust_card_missing_billing_name.html\n";
  }
  my $billing_company = Encode::decode($cgi_input_charset, $cgi->param("billing_company"));
  my $billing_address_1 = Encode::decode($cgi_input_charset, $cgi->param("billing_address_1"));
  if (!$billing_address_1) {
    die "show_page: edit_cust_card_missing_billing_address.html\n";
  }
  my $billing_address_2 = Encode::decode($cgi_input_charset, $cgi->param("billing_address_2"));
  my $billing_city = Encode::decode($cgi_input_charset, $cgi->param("billing_city"));
  if (!$billing_city) {
    die "show_page: edit_cust_card_missing_billing_city.html\n";
  }
  my $billing_state = Encode::decode($cgi_input_charset, $cgi->param("billing_state"));
  my $billing_state_other = Encode::decode($cgi_input_charset, $cgi->param("billing_state_other"));
  if (($billing_state) && ($billing_state_other)) {
    die "show_page: edit_cust_card_state_and_province_specified.html\n";
  } elsif ((!$billing_state) && (!$billing_state_other)) {
    die "show_page: edit_cust_card_missing_billing_state.html\n";
  } elsif ($billing_state_other) {
    $billing_state = $billing_state_other;
    $billing_state_other = "";
  }
  my $billing_zip_code = Encode::decode($cgi_input_charset, $cgi->param("billing_zip_code"));
  if (!$billing_zip_code) {
    die "show_page: edit_cust_card_missing_billing_zip_code.html\n";
  }
  my $billing_country = Encode::decode($cgi_input_charset, $cgi->param("billing_country"));
  if (!$billing_country) {
    die "show_page: edit_cust_card_missing_billing_country.html\n";
  }
  my $billing_phone_number = Encode::decode($cgi_input_charset, $cgi->param("billing_phone_number"));
  my $billing_fax_number = Encode::decode($cgi_input_charset, $cgi->param("billing_fax_number"));
  my $billing_email_address = Encode::decode($cgi_input_charset, $cgi->param("billing_email_address"));
  my $billing_address_num = Encode::decode($cgi_input_charset, $cgi->param("billing_address_num"));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # update row in credit_card_tbl.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $person_id, SQL_INTEGER]);
  push(@sql_column_list, ["card_nickname", undef_or_value(Encode::encode($db_charset, $card_nickname))]);
  push(@sql_column_list, ["card_number", $card_number]);
  push(@sql_column_list, ["card_security_code", undef_or_value($card_security_code)]);
  push(@sql_column_list, ["card_expiration_month", $card_expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $card_expiration_year]);
  push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $billing_name)]);
  push(@sql_column_list, ["billing_company", undef_or_value(Encode::encode($db_charset, $billing_company))]);
  push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $billing_address_1)]);
  push(@sql_column_list, ["billing_address_2", undef_or_value(Encode::encode($db_charset, $billing_address_2))]);
  push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $billing_city)]);
  push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $billing_state)]);
  push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
  push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $billing_country)]);
  push(@sql_column_list, ["billing_phone_number", undef_or_value(Encode::encode($db_charset, $billing_phone_number))]);
  push(@sql_column_list, ["billing_fax_number", undef_or_value(Encode::encode($db_charset, $billing_fax_number))]);
  push(@sql_column_list, ["billing_email_address", undef_or_value(Encode::encode($db_charset, $billing_email_address))]);
  push(@sql_column_list, ["billing_address_num", undef_or_value(Encode::encode($db_charset, $billing_address_num))]);
  push(@sql_column_list, ["note", undef_or_value(Encode::encode($db_charset, $note))]);
  update_row($dbh, "credit_card_tbl", \@sql_column_list, $credit_card_id);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
  $card_nickname = HTML::Entities::encode($card_nickname || "");
  $billing_name = HTML::Entities::encode($billing_name);
  $billing_company = HTML::Entities::encode($billing_company || "");
  $billing_address_1 = HTML::Entities::encode($billing_address_1);
  $billing_address_2 = HTML::Entities::encode($billing_address_2 || "");
  $billing_city = HTML::Entities::encode($billing_city);
  $billing_state = HTML::Entities::encode($billing_state);
  $billing_zip_code = HTML::Entities::encode($billing_zip_code);
  $billing_country = HTML::Entities::encode($billing_country);
  $billing_phone_number = HTML::Entities::encode($billing_phone_number || "");
  $billing_fax_number = HTML::Entities::encode($billing_fax_number || "");
  $billing_email_address = HTML::Entities::encode($billing_email_address || "");
  $billing_address_num = HTML::Entities::encode($billing_address_num || "");
  $note = HTML::Entities::encode($note || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <credit_card>
    <id>$credit_card_id</id>
    <account_name>$account_name</account_name>
    <person_name>$person_name</person_name>
    <card_nickname>$card_nickname</card_nickname>
    <card_number>$card_number</card_number>
    <card_security_code>$card_security_code</card_security_code>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <billing_address_num>$billing_address_num</billing_address_num>
    <note>$note</note>
  </credit_card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_cust_card_result.xslt", $cgi);
}

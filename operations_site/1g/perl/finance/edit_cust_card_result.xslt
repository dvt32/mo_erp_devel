<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Customer Credit/Debit Card (result)
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Customer Credit/Debit Card (result)
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/show_cust_cards.cgi">Show Credit/Debit Cards</a>
	  |
	  <a href="/1g/finance/new_cust_card_input.cgi">New Credit/Debit Card</a>
<!--	  |
	  <a href="/1g/finance/show_cust_bank_accts.cgi">Show Bank Accounts</a>
	  |
	  <a href="/1g/finance/new_cust_bank_acct_input.cgi">New Bank Account</a> -->
        </p>
	<xsl:apply-templates select="credit_card"/>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit_card">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following credit/debit card:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new credit/debit card:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Person:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="person_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Card Nickname:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_nickname"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Card Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Card Security Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_security_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Expiration Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Company:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_company"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing State:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Fax Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_fax_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Email Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_email_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

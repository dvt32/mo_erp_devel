#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use get_role_list;
use sorted_tree_from_id_map;
use flatten_sorted_tree;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $expense_id = $cgi->param("id");
  my $expense;
  if ((!$expense_id) || (!($expense = select_row($dbh, "expense_tbl", $expense_id)))) {
    die "show_page: edit_expense_bad_id.html\n";
  }
  # fetch all employees to generate drop down list.
  my (%employee_by_id, @employee_id_list);
  my $sql_query = "select * from employee_tbl order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  # get role list of current user.
  my $employee_id = $employee->{"id"};
  my $role_list = get_role_list($instance_map, $employee_id);
  # check if expense has been reimbursed or reconciled.
  if ((($expense->{"is_reimbursed"}) || ($expense->{"is_reconciled"})) && (!grep {$_ eq "role:finance/view_all_expenses"} @$role_list)) {
    die "show_page: edit_expense_already_processed.html\n";
  }
  # get all ledger accounts.
  my (%ledger_account_by_id);
  $sql_query = "select * from ledger_account_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%ledger_account_by_id);
  # populate information for display of page.
  my $ledger_account_tree = sorted_tree_from_id_map(\%ledger_account_by_id, "name");
  my $sorted_ledger_account_list = flatten_sorted_tree($ledger_account_tree);
  my $receipt_filename = "";
  if ($expense->{"receipt_file_id"}) {
    my $file = select_row($dbh, "file_tbl", $expense->{"receipt_file_id"});
    if ($file->{"namespace"} eq "/finance/expense") {
      $receipt_filename = HTML::Entities::encode($file->{"filename"});
    } else {
      die "show_page: edit_expense_bad_receipt_file.html\n";
    }
  }
  # generate XML.
  my $account_id = $expense->{"account_id"};
  my $person_id = $expense->{"person_id"};
  $expense->{"purchase_date"} =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  my $purchase_date_year = $1;
  my $purchase_date_month = int($2);
  my $purchase_date_day = int($3);
  my $item_name = HTML::Entities::encode($expense->{"item_name"});
  my $vendor_name = HTML::Entities::encode($expense->{"vendor_name"});
  my $ledger_account_id = $expense->{"ledger_account_id"};
  my $locations = HTML::Entities::encode($expense->{"locations"});
  my $amount = $expense->{"amount"};
  my $payer = HTML::Entities::encode($expense->{"payer"});
  my $payment_method = HTML::Entities::encode($expense->{"payment_method"});
  my $payment_identifier = HTML::Entities::encode($expense->{"payment_identifier"} || "");
  my $receipt_file_id = $expense->{"receipt_file_id"} || "";
  my $is_reconciled = $expense->{"is_reconciled"} ? 1 : 0;
  my $is_reimbursed = $expense->{"is_reimbursed"} ? 1 : 0;
  my $note = HTML::Entities::encode($expense->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <employee-list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_first_name = HTML::Entities::encode($_employee->{"first_name"});
    my $_last_name = HTML::Entities::encode($_employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <first_name>$_first_name</first_name>
      <last_name>$_last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <ledger_account_list>
---END_BLOCK---;
  for my $node (@$sorted_ledger_account_list) {
    my $ledger_account = $node->{"value"};
    my $depth = $node->{"depth"};
    my $ledger_account_id = $ledger_account->{"id"};
    my $ledger_account_label = "."x(($depth-1)*4) . HTML::Entities::encode($ledger_account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <ledger_account>
      <id>$ledger_account_id</id>
      <label>$ledger_account_label</label>
    </ledger_account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ledger_account_list>
  <payer_list>
    <payer>company</payer>
    <payer>personal</payer>
  </payer_list>
  <payment_method_list>
    <payment_method>bill</payment_method>
    <payment_method>card</payment_method>
    <payment_method>cash</payment_method>
    <payment_method>check</payment_method>
  </payment_method_list>
  <expense>
    <id>$expense_id</id>
    <account_id>$account_id</account_id>
    <person_id>$person_id</person_id>
    <purchase_date_year>$purchase_date_year</purchase_date_year>
    <purchase_date_month>$purchase_date_month</purchase_date_month>
    <purchase_date_day>$purchase_date_day</purchase_date_day>
    <item_name>$item_name</item_name>
    <vendor_name>$vendor_name</vendor_name>
    <ledger_account_id>$ledger_account_id</ledger_account_id>
    <locations>$locations</locations>
    <amount>$amount</amount>
    <payer>$payer</payer>
    <payment_method>$payment_method</payment_method>
    <payment_identifier>$payment_identifier</payment_identifier>
    <receipt_filename>$receipt_filename</receipt_filename>
    <receipt_file_id>$receipt_file_id</receipt_file_id>
    <is_reconciled>$is_reconciled</is_reconciled>
    <is_reimbursed>$is_reimbursed</is_reimbursed>
    <note>$note</note>
  </expense>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_expense_input.xslt", $cgi);
}

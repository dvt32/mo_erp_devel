<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Expense
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/new_expense_input.cgi">New Expense</a>
	  |
	  <a href="/1g/finance/show_expenses.cgi">Recent Expenses</a>
        </p>
	<xsl:apply-templates select="expense"/>
	<p>
	  <a>
	    <xsl:attribute name="href">/1g/finance/delete_expense.cgi?id=<xsl:value-of select="expense/id"/></xsl:attribute>
	    Delete this expense
	  </a>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="expense">
    <table>
      <tr><td>
	<form action="/1g/finance/edit_expense_result.cgi" method="post" enctype="multipart/form-data">
	  <input type="hidden" name="id">
	    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  </input>
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Expense
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <xsl:if test="/page/role-list/role = 'role:finance/view_all_expenses'">
		<tr>
		  <td style="padding: 4px; ">
		    Employee:
		  </td>
		  <td>
		    <select name="employee_id">
		      <xsl:for-each select="/page/employee-list/employee">
			<option>
			  <xsl:if test="/page/expense/person_id = id">
			    <xsl:attribute name="selected">selected</xsl:attribute>
			  </xsl:if>
			  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			  <xsl:value-of select="last_name"/>
			  <xsl:text>, </xsl:text>
			  <xsl:value-of select="first_name"/>
			</option>
		      </xsl:for-each>
		    </select>
		  </td>
		</tr>
	      </xsl:if>
	      <tr>
		<td style="padding: 4px; ">
		  Item Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">item_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="item_name"/></xsl:attribute>
		  </input>
		  (what did you buy?)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Vendor Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">vendor_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="vendor_name"/></xsl:attribute>
		  </input>
		  (where did you buy it?)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Purchase Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>month</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/month-list/month">
		      <option>
			<xsl:if test="/page/expense/purchase_date_month = number">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select name="day">
		    <xsl:attribute name="name">
		      <xsl:text>day</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/day-list/day">
		      <option>
			<xsl:if test="/page/expense/purchase_date_day = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">
		      <xsl:text>year</xsl:text>
		    </xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="/page/expense/purchase_date_year"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Ledger Account:
		</td>
		<td>
		  <select name="ledger_account_id">
		    <xsl:for-each select="/page/ledger_account_list/ledger_account">
		      <option>
			<xsl:if test="/page/expense/ledger_account_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Location(s):
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">locations</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="locations"/></xsl:attribute>
		  </input>
		  (buildings this purchase benefits: can be all or one or multiple)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Amount:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">amount</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="amount"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Payer:
		</td>
		<td>
		  <select name="payer">
		    <xsl:for-each select="/page/payer_list/payer">
		      <option>
			<xsl:if test="/page/expense/payer = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Payment Method:
		</td>
		<td>
		  <select name="payment_method">
		    <xsl:for-each select="/page/payment_method_list/payment_method">
		      <option>
			<xsl:if test="/page/expense/payment_method = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Payment Identifier:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">payment_identifier</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="payment_identifier"/></xsl:attribute>
		  </input>
		  (card number or check number)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Receipt:
		</td>
		<td>
		  <input type="file" name="receipt_file"/>
		  <xsl:if test="receipt_file_id != ''">
		    (upload will replace existing receipt)
		  </xsl:if>
		</td>
	      </tr>
	      <xsl:if test="receipt_file_id != ''">
		<tr>
		  <td style="padding: 4px; ">
		  </td>
		  <td>
		    view existing receipt:
		    <a>
		      <xsl:attribute name="href">
			<xsl:text>/1g/finance/view_expense_receipt.cgi?id=</xsl:text>
			<xsl:value-of select="id"/>
		      </xsl:attribute>
		      <xsl:value-of select="receipt_filename"/>
		    </a>
		  </td>
		</tr>
	      </xsl:if>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

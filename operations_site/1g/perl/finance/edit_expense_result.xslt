<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Expense
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Expense
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/new_expense_input.cgi">New Expense</a>
	  |
	  <a href="/1g/finance/show_expenses.cgi">Recent Expenses</a>
        </p>
	<xsl:apply-templates select="expense"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="expense">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following expense:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new expense:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <xsl:if test="/page/role-list/role = 'role:finance/view_all_expenses'">
	<tr>
	  <td class="map_table_key">
	    Employee:
	  </td>
	  <td class="map_table_value">
	    <xsl:value-of select="person_name"/>
	  </td>
	</tr>
      </xsl:if>
      <tr>
	<td class="map_table_key">
	  Item Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="item_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Vendor Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="vendor_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Purchase Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="purchase_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Ledger Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="ledger_account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Locations:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="locations"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Amount:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Payer:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="payer"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Payment Method:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="payment_method"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Payment Identifier:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="payment_identifier"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Book:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="book"/>
	</td>
      </tr>
       <tr>
	<td class="map_table_key">
	  Class:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="class"/>
	</td>
      </tr>
       <tr>
	<td class="map_table_key">
	  Entered By:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="entered_by"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Receipt:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="receipt_file_id != ''">
	      <a>
		<xsl:attribute name="href">
		  <xsl:text>/1g/finance/view_expense_receipt.cgi?id=</xsl:text>
		  <xsl:value-of select="id"/>
		</xsl:attribute>
		<xsl:value-of select="receipt_filename"/>
	      </a>
	    </xsl:when>
	    <xsl:otherwise>
	      (none)
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

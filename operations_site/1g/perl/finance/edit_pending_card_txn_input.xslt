<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Operations Center :: Edit Pending Card Transaction
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
          <a href="/1g/index.cgi">Main menu</a>
	  <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
	    |
	    Credit/Debit Card Transactions:
	    <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
	    &middot;
	    <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
	    &middot;
	    <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
	  </xsl:if>
	</p>
	<xsl:apply-templates select="recv_payment"/>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="recv_payment">
    <form action="/1g/finance/edit_pending_card_txn_result.cgi" method="post">
      <input type="hidden" name="id">
	<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
      </input>
      <table cellspacing="0" cellpadding="10px" border="0" class="standard_table" style="white-space: nowrap;">
	<tr>
	  <td class="standard_table_title" colspan="2">
	    <xsl:text>Edit Pending Card Transaction</xsl:text>
	  </td>
	</tr>
	<tr valign="top">
	  <td colspan="2">
	    <span class="attention">
	      *Make tenant aware that there is an additional 3% fee for Visa/MC and 3.5% for AMEX,
	      <br />
	      except for virtual office. Add this processing fee to "Amount of Payment" below and
	      <br />
	      also add it to the QuickBooks invoice.
	    </span>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">When Tenant Gave Card Payment Details</span>:<br />
	    <select>
	      <xsl:attribute name="name">
		<xsl:text>month</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/month-list/month">
		<option>
		  <xsl:if test="/page/recv_payment/recv_dt_month = number">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		  <xsl:value-of select="name"/>
		</option>
	      </xsl:for-each>
	    </select>
	    <select name="day">
	      <xsl:attribute name="name">
		<xsl:text>day</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/day-list/day">
		<option>
		  <xsl:if test="/page/recv_payment/recv_dt_day = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">
		<xsl:text>year</xsl:text>
	      </xsl:attribute>
	      <xsl:attribute name="size">4</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="/page/recv_payment/recv_dt_year"/></xsl:attribute>
	    </input>
	    <xsl:text> at </xsl:text>
	    <select>
	      <xsl:attribute name="name">
		<xsl:text>hour</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/hour-list/hour">
		<option>
		  <xsl:if test="/page/recv_payment/recv_dt_hour = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	    <xsl:text>:</xsl:text>
	    <select name="minute">
	      <xsl:attribute name="name">
		<xsl:text>minute</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/minute-list/minute">
		<option>
		  <xsl:if test="/page/recv_payment/recv_dt_minute = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	  </td>
	  <td style="text-align: right;">
	    <span class="required_field">Received by</span>:
	    <br />
	    <select name="employee_id">
	      <xsl:for-each select="/page/employee-list/employee">
		<option>
		  <xsl:if test="/page/recv_payment/employee_id = id">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		  <xsl:value-of select="last_name"/>
		  <xsl:text>, </xsl:text>
		  <xsl:value-of select="first_name"/>
		</option>
	      </xsl:for-each>
	    </select>
	  </td>
	</tr>
	<tr>
	  <td valign="top">
	    <span class="required_field">Account</span>:<br />
	    <select>
	      <xsl:attribute name="name">
		<xsl:text>cust_acct_id</xsl:text>
	      </xsl:attribute>
	      <option value="">[---]</option>
	      <xsl:for-each select="/page/account-list/account">
		<option>
		  <xsl:if test="/page/recv_payment/cust_acct_id = id">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		  <xsl:value-of select="name"/><xsl:text>, </xsl:text><xsl:value-of select="primary-contact"/>
		</option>
	      </xsl:for-each>
	    </select>
	  </td>
	  <td style="text-align: right;">
	    <span class="required_field">Process Mode</span>:
	    <br />
	    <select name="process_mode">
	      <xsl:for-each select="/page/process_mode_list/process_mode">
		<option>
		  <xsl:if test="/page/recv_payment/process_mode = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">When to Charge Card</span>:<br />
	    <select>
	      <xsl:attribute name="name">
		<xsl:text>process_month</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/month-list/month">
		<option>
		  <xsl:if test="/page/recv_payment/process_txn_after_dt_month = number">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		  <xsl:value-of select="name"/>
		</option>
	      </xsl:for-each>
	    </select>
	    <select name="day">
	      <xsl:attribute name="name">
		<xsl:text>process_day</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/day-list/day">
		<option>
		  <xsl:if test="/page/recv_payment/process_txn_after_dt_day = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">
		<xsl:text>process_year</xsl:text>
	      </xsl:attribute>
	      <xsl:attribute name="size">4</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="/page/recv_payment/process_txn_after_dt_year"/></xsl:attribute>
	    </input>
	    <xsl:text> at </xsl:text>
	    <select>
	      <xsl:attribute name="name">
		<xsl:text>process_hour</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/hour-list/hour">
		<option>
		  <xsl:if test="/page/recv_payment/process_txn_after_dt_hour = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	    <xsl:text>:</xsl:text>
	    <select name="minute">
	      <xsl:attribute name="name">
		<xsl:text>process_minute</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/minute-list/minute">
		<option>
		  <xsl:if test="/page/recv_payment/process_txn_after_dt_minute = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	  </td>
	  <td style="text-align: right;">
	    <span class="required_field">Amount of Payment</span><span class="attention">*</span>:<br />$
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">amount</xsl:attribute>
	      <xsl:attribute name="size">6</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="amount"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">Card Number</span>
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_number</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_number"/></xsl:attribute>
	    </input>
	  </td>
	  <td style="text-align: right;">
	    Card Security Code:<br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_security_code</xsl:attribute>
	      <xsl:attribute name="size">3</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_security_code"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">Card Expiration Date</span>:
	    <br />
	    <select>
	      <xsl:attribute name="name">
		<xsl:text>card_expiration_month</xsl:text>
	      </xsl:attribute>
	      <xsl:for-each select="/page/month-list/month">
		<option>
		  <xsl:if test="number(/page/recv_payment/card_expiration_month) = number">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		  (<xsl:if test="number &lt; 10">0</xsl:if><xsl:value-of select="number"/>)
		  <xsl:value-of select="name"/>
		</option>
	      </xsl:for-each>
	    </select>
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">
		<xsl:text>card_expiration_year</xsl:text>
	      </xsl:attribute>
	      <xsl:attribute name="size">4</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_expiration_year"/></xsl:attribute>
	    </input>
	  </td>
	  <td style="text-align: right;">
	    <span class="required_field">Person Name on Card</span>:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_name</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_name"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    Company Name on Card:<br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_company</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_company"/></xsl:attribute>
	    </input>
	  </td>
	  <td style="text-align: right;">
	    Address 1 Number:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_address_num</xsl:attribute>
	      <xsl:attribute name="size">8</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_address_num"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">Address 1</span>:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_address_1</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_address_1"/></xsl:attribute>
	    </input>
	  </td>
	  <td style="text-align: right;">
	    Address 2:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_address_2</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_address_2"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">Zip Code</span>:<br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_zip_code</xsl:attribute>
	      <xsl:attribute name="size">10</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_zip_code"/></xsl:attribute>
	    </input>
	  </td>
	  <td style="text-align: right;">
	    <span class="required_field">City</span>:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_city</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_city"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">State</span>:<br />
	    <select name="card_bill_state">
	      <option value="">[---]</option>
	      <xsl:for-each select="/page/state-list/state">
		<option>
		  <xsl:if test="/page/recv_payment/card_bill_state = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	  </td>
	  <td style="text-align: right;">
	    or Province/Territory:<br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_state_other</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_state_other"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    <span class="required_field">Country</span>:<br />
	    <select name="card_bill_country">
	      <option value="">[---]</option>
	      <xsl:for-each select="/page/country-list/country">
		<option>
		  <xsl:if test="/page/recv_payment/card_bill_country = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	  </td>
	  <td style="text-align: right;">
	    Phone Number:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_phone_number</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_phone_number"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td>
	    Fax Number:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_fax_number</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_fax_number"/></xsl:attribute>
	    </input>
	  </td>
	  <td style="text-align: right;">
	    Email Address:
	    <br />
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">card_bill_email_address</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="card_bill_email_address"/></xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr valign="top">
	  <td colspan="2">
	    Note:<br />
	    <textarea>
	      <xsl:attribute name="name">note</xsl:attribute>
	      <xsl:attribute name="rows">4</xsl:attribute>
	      <xsl:attribute name="cols">40</xsl:attribute>
	      <!-- Need this blank note field; otherwise the surrounding textarea gets collapsed into a single tag. -->
	      <xsl:choose>
		<xsl:when test="note = ''">.</xsl:when>
		<xsl:otherwise>
		  <xsl:value-of select="note"/>
		</xsl:otherwise>
	      </xsl:choose>
	    </textarea>
	  </td>
	</tr>
	<tr valign="top">
	  <td colspan="2">
	    <span class="required_field">Required fields</span> are in red.
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="submit" name="submitButton" value="Save"/>
	  </td>
	</tr>
      </table>
    </form>    
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

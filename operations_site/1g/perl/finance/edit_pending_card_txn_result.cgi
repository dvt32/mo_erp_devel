#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use get_row_list;
use update_row;
use insert_row;
use enqueue_email;
use components_to_timestamp;
use is_valid_credit_card;
use get_timestamp_now;
use charge_card;
use strip_outside_whitespace;
use parse_dollars_cents;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/receive_payment/card");
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $recv_payment_id = $cgi->param("id");
  my $recv_payment;
  if ((!$recv_payment_id) || (!($recv_payment = select_row($dbh, "recv_payment_tbl", $recv_payment_id)))) {
    die "show_page: edit_pending_card_txn_not_found.html\n";
  }
  if ($recv_payment->{"process_status"} ne "pending") {
    die "show_page: edit_pending_card_txn_not_pending.html\n";
  }
  my ($month, $day, $year, $hour, $minute) = map {$cgi->param($_)} (qw(month day year hour minute));
  if (($month < 1) || ($month > 12) || ($day < 1) || ($day > 31) || ($year < 2008) || ($year > 3000) ||
      ($hour < 0) || ($hour > 23) || ($minute < 0) || ($minute > 59)) {
    die "show_page: edit_pending_card_txn_bad_time.html\n";
  }
  my $recv_dt = components_to_timestamp($year, $month, $day, $hour, $minute, 0);
  my $recv_employee_id = $cgi->param("employee_id");
  my $recv_employee;
  if ((!$recv_employee_id) || (!($recv_employee = select_row($dbh, "employee_tbl", $recv_employee_id)))) {
    die "show_page: edit_pending_card_txn_bad_employee.html\n";
  }
  my $cust_acct_id = $cgi->param("cust_acct_id");
  my $cust_acct;
  if ((!$cust_acct_id) || (!($cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id)))) {
    die "show_page: edit_pending_card_txn_bad_account.html\n";
  }
  my $process_month = $cgi->param("process_month");
  my $process_day = $cgi->param("process_day");
  my $process_year = $cgi->param("process_year");
  my $process_hour = $cgi->param("process_hour");
  my $process_minute = $cgi->param("process_minute");
  if ((!$process_month) || (!$process_day) || (!$process_year) ||
      ($process_month < 1) || ($process_month > 12) || ($process_day < 1) || ($process_day > 31) ||
      ($process_year < 2000) || ($process_year > 3000) || ($process_hour < 0) || ($process_hour > 23) ||
      ($process_minute < 0) || ($process_minute > 59)) {
    die "show_page: edit_pending_card_txn_bad_process_txn_after_dt.html\n";
  }
  my $process_txn_after_dt = components_to_timestamp($process_year, $process_month, $process_day, $process_hour, $process_minute, 0);
  my $amount = $cgi->param("amount") || "";
  eval {
    my ($amount_dollars, $amount_cents) = parse_dollars_cents($amount);
    $amount = "$amount_dollars.$amount_cents";
  };
  if ($@) {
    die "show_page: edit_pending_card_txn_bad_amount.html\n";
  }
  my $card_number = $cgi->param("card_number");
  if ((!$card_number) || (!is_valid_credit_card($card_number))) {
    die "show_page: edit_pending_card_txn_bad_card_number.html\n";
  }
  my $card_security_code = $cgi->param("card_security_code");
  if (($card_security_code) && ($card_security_code !~ /^\d{3,4}$/)) {
    die "show_page: edit_pending_card_txn_bad_card_security_code.html\n";
  }
  my $card_expiration_month = $cgi->param("card_expiration_month");
  if ((!$card_expiration_month) || ($card_expiration_month < 1) || ($card_expiration_month > 12)) {
    die "show_page: edit_pending_card_txn_bad_card_expiration_month.html\n";
  }
  $card_expiration_month = lead_pad(int($card_expiration_month), 2);
  my $card_expiration_year = $cgi->param("card_expiration_year");
  if ((!$card_expiration_year) || ($card_expiration_year < 2000) || ($card_expiration_year > 3000)) {
    die "show_page: edit_pending_card_txn_bad_card_expiration_year.html\n";
  }
  my $card_bill_name = Encode::decode($cgi_input_charset, $cgi->param("card_bill_name"));
  if (!$card_bill_name) {
    die "show_page: edit_pending_card_txn_missing_bill_name.html\n";
  }
  my $card_bill_company = Encode::decode($cgi_input_charset, $cgi->param("card_bill_company"));
  my $card_bill_address_num = Encode::decode($cgi_input_charset, $cgi->param("card_bill_address_num"));
  my $card_bill_address_1 = Encode::decode($cgi_input_charset, $cgi->param("card_bill_address_1"));
  if (!$card_bill_address_1) {
    die "show_page: edit_pending_card_txn_missing_bill_address_1.html\n";
  }
  my $card_bill_address_2 = Encode::decode($cgi_input_charset, $cgi->param("card_bill_address_2"));
  my $card_bill_city = Encode::decode($cgi_input_charset, $cgi->param("card_bill_city"));
  if (!$card_bill_city) {
    die "show_page: edit_pending_card_txn_missing_bill_city.html\n";
  }
  my $card_bill_state = Encode::decode($cgi_input_charset, $cgi->param("card_bill_state"));
  if (!$card_bill_state) {
    if (!($card_bill_state = Encode::decode($cgi_input_charset, $cgi->param("card_bill_state_other")))) {
      die "show_page: edit_pending_card_txn_missing_bill_state.html\n";
    }
  }
  my $card_bill_zip_code = Encode::decode($cgi_input_charset, $cgi->param("card_bill_zip_code"));
  if (!$card_bill_zip_code) {
    die "show_page: edit_pending_card_txn_missing_bill_zip_code.html\n";
  }
  my $card_bill_country = Encode::decode($cgi_input_charset, $cgi->param("card_bill_country"));
  if (!$card_bill_country) {
    die "show_page: edit_pending_card_txn_missing_bill_country.html\n";
  }
  my $card_bill_phone_number = Encode::decode($cgi_input_charset, $cgi->param("card_bill_phone_number"));
  my $card_bill_fax_number = Encode::decode($cgi_input_charset, $cgi->param("card_bill_fax_number"));
  my $card_bill_email_address = Encode::decode($cgi_input_charset, $cgi->param("card_bill_email_address"));
  my $process_mode = $cgi->param("process_mode");
  if ((!$process_mode) || ($process_mode !~ /^automatic|manual$/)) {
    die "show_page: edit_pending_card_txn_bad_process_mode.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  my $client_hostname = $cgi->remote_host() || undef;
  my $client_ip_addr = $cgi->remote_addr();
  my $timestamp_now = get_timestamp_now();
  my $process_status = (($process_mode eq "automatic") && ($process_txn_after_dt le $timestamp_now)) ? "complete" : "pending";
  # update existing row in recv_payment_tbl.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["employee_id", $recv_employee_id, SQL_INTEGER]);
  push(@sql_column_list, ["amount", $amount, SQL_DECIMAL]);
  push(@sql_column_list, ["recv_dt", $recv_dt]);
  push(@sql_column_list, ["payment_method", "card"]);
  push(@sql_column_list, ["process_txn_after_dt", $process_txn_after_dt]);
  push(@sql_column_list, ["process_mode", $process_mode]);
  push(@sql_column_list, ["notify_email", "accounts_receivable\@microoffice.com"]);
  push(@sql_column_list, ["process_status", $process_status]);
  push(@sql_column_list, ["client_hostname", $client_hostname]);
  push(@sql_column_list, ["client_ip_addr", $client_ip_addr]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  push(@sql_column_list, ["card_number", $card_number]);
  push(@sql_column_list, ["card_expiration_month", $card_expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $card_expiration_year]);
  push(@sql_column_list, ["card_security_code", $card_security_code]);
  push(@sql_column_list, ["card_bill_name", Encode::encode($db_charset, $card_bill_name)]);
  push(@sql_column_list, ["card_bill_company", Encode::encode($db_charset, $card_bill_company)]);
  push(@sql_column_list, ["card_bill_address_num", Encode::encode($db_charset, $card_bill_address_num)]);
  push(@sql_column_list, ["card_bill_zip_code", Encode::encode($db_charset, $card_bill_zip_code)]);
  push(@sql_column_list, ["card_bill_address_1", Encode::encode($db_charset, $card_bill_address_1)]);
  push(@sql_column_list, ["card_bill_address_2", Encode::encode($db_charset, $card_bill_address_2)]);
  push(@sql_column_list, ["card_bill_city", Encode::encode($db_charset, $card_bill_city)]);
  push(@sql_column_list, ["card_bill_state", Encode::encode($db_charset, $card_bill_state)]);
  push(@sql_column_list, ["card_bill_country", Encode::encode($db_charset, $card_bill_country)]);
  push(@sql_column_list, ["card_bill_phone_number", Encode::encode($db_charset, $card_bill_phone_number)]);
  push(@sql_column_list, ["card_bill_fax_number", Encode::encode($db_charset, $card_bill_fax_number)]);
  push(@sql_column_list, ["card_bill_email_address", Encode::encode($db_charset, $card_bill_email_address)]);
  update_row($dbh, "recv_payment_tbl", \@sql_column_list, $recv_payment_id);
  my %credit_card_txn_response;
  if ($process_status eq "complete") {
    my %linkpoint_order =
      (
       "client_ip_address" => $client_ip_addr,

       "total_charge" => $amount,

       "card_number" => $card_number,
       "card_expiration_month" => $card_expiration_month,
       "card_expiration_year" => $card_expiration_year,
       "card_security_code" => $card_security_code,

       "billing_name" => $card_bill_name,
       "billing_company" => $card_bill_company || "",
       "billing_address_1" => $card_bill_address_1,
       "billing_address_2" => $card_bill_address_2 || "",
       "billing_city" => $card_bill_city,
       "billing_state" => $card_bill_state,
       "billing_country" => $card_bill_country,
       "billing_zip_code" => $card_bill_zip_code,
       "billing_address_num" => $card_bill_address_num || "",
       "billing_phone_number" => $card_bill_phone_number || "",
       "billing_fax_number" => $card_bill_fax_number || "",
       "billing_email_address" => $card_bill_email_address || "",
       );
    my %linkpoint_response = charge_card($instance_map, %linkpoint_order);
    # create row in credit_card_txn_tbl if transaction is being processed immediately.
    @sql_column_list = ();
    push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
    push(@sql_column_list, ["card_number", $card_number]);
    push(@sql_column_list, ["card_expiration_month", $card_expiration_month]);
    push(@sql_column_list, ["card_expiration_year", $card_expiration_year]);
    push(@sql_column_list, ["card_security_code", $card_security_code]);
    push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $card_bill_name)]);
    push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $card_bill_company)]);
    push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $card_bill_address_num)]);
    push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $card_bill_zip_code)]);
    push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $card_bill_address_1)]);
    push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $card_bill_address_2)]);
    push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $card_bill_city)]);
    push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $card_bill_state)]);
    push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $card_bill_country)]);
    push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $card_bill_phone_number)]);
    push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $card_bill_fax_number)]);
    push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $card_bill_email_address)]);
    push(@sql_column_list, ["total_charge", $amount, SQL_DECIMAL]);
    push(@sql_column_list, ["recv_payment_id", $recv_payment_id, SQL_INTEGER]); # link to recv_payment_tbl
    push(@sql_column_list, ["processed_dt", ["now()"]]);
    push(@sql_column_list, ["response_avs", $linkpoint_response{"r_avs"}]);
    push(@sql_column_list, ["response_order_num", $linkpoint_response{"r_ordernum"}]);
    push(@sql_column_list, ["response_error", $linkpoint_response{"r_error"}]);
    push(@sql_column_list, ["response_approved", $linkpoint_response{"r_approved"}]);
    push(@sql_column_list, ["response_code", $linkpoint_response{"r_code"}]);
    push(@sql_column_list, ["response_message", $linkpoint_response{"r_message"}]);
    push(@sql_column_list, ["response_txn_dt", $linkpoint_response{"r_time"}]);
    push(@sql_column_list, ["response_reference_num", $linkpoint_response{"r_ref"}]);
    push(@sql_column_list, ["response_tdate", $linkpoint_response{"r_tdate"}]);
    insert_row($dbh, "credit_card_txn_tbl", \@sql_column_list);
    %credit_card_txn_response =
      (
       "response_avs" => $linkpoint_response{"r_avs"},
       "response_error" => $linkpoint_response{"r_error"},
       "response_approved" => $linkpoint_response{"r_approved"},
       "response_code" => $linkpoint_response{"r_code"},
       "response_message" => $linkpoint_response{"r_message"},
      );
  }
  # get name to display instead of id
  my $account_name = $cust_acct->{"name"};
  my $obscured_card_number = "*"x(length($card_number) - 3) . substr($card_number, length($card_number) - 3);
  if ($process_status eq "complete") {
    # enqueue the receipt email.
    my $receipt_email_message = <<"---END_BLOCK---;";
$account_name (ID #$cust_acct_id) has paid \$$amount via credit or debit card provided to $recv_employee->{"first_name"} $recv_employee->{"last_name"}. The card number was $obscured_card_number.

The payment was processed immediately and the result was:
AVS: $credit_card_txn_response{"response_avs"}
Error: $credit_card_txn_response{"response_error"}
Approved: $credit_card_txn_response{"response_approved"}
Code: $credit_card_txn_response{"response_code"}
Message: $credit_card_txn_response{"response_message"}

If the transaction was not successful, please get in touch with the tenant to try again.
---END_BLOCK---;
    if ($note) {
      $receipt_email_message .= <<"---END_BLOCK---;";
Additional operator note: $note
---END_BLOCK---;
    }
    enqueue_email
      ("dbh" => $dbh,
       "account_id" => $cust_acct_id,
       "employee_id" => $recv_employee_id,
       "from_hdr" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
       "to_hdr" => "accounts_receivable\@microoffice.com",
       "subject_hdr" => "Payment of \$$amount from $account_name (ID #$cust_acct_id) by credit/debit card",
       "message_body" => $receipt_email_message,
      );
  }
  # populate information needed for display of page.
  $account_name = HTML::Entities::encode($account_name);
  my $first_name = HTML::Entities::encode($recv_employee->{"first_name"});
  my $last_name = HTML::Entities::encode($recv_employee->{"last_name"});
  $note = HTML::Entities::encode($note || "");
  my $response_avs = HTML::Entities::encode($credit_card_txn_response{"response_avs"} || "");
  my $response_error = HTML::Entities::encode($credit_card_txn_response{"response_error"} || "");
  my $response_approved = HTML::Entities::encode($credit_card_txn_response{"response_approved"} || "");
  my $response_code = HTML::Entities::encode($credit_card_txn_response{"response_code"} || "");
  my $response_message = HTML::Entities::encode($credit_card_txn_response{"response_message"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <recv_payment>
    <cust_acct_id>$cust_acct_id</cust_acct_id>
    <account_name>$account_name</account_name>
    <card_number>$obscured_card_number</card_number>
    <amount>$amount</amount>
    <employee>
      <id>$recv_employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
    <note>$note</note>
    <recv_dt>$recv_dt</recv_dt>
    <process_txn_after_dt>$process_txn_after_dt</process_txn_after_dt>
    <process_mode>$process_mode</process_mode>
    <process_status>$process_status</process_status>
    <response_avs>$response_avs</response_avs>
    <response_error>$response_error</response_error>
    <response_approved>$response_approved</response_approved>
    <response_code>$response_code</response_code>
    <response_message>$response_message</response_message>
  </recv_payment>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_pending_card_txn_result.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Operations Center :: Edit Pending Card Transaction (result)
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
          <a href="/1g/index.cgi">Main menu</a>
	  <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
	    |
	    Credit/Debit Card Transactions:
	    <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
	    &middot;
	    <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
	    &middot;
	    <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
	  </xsl:if>
	</p>
        <xsl:apply-templates select="recv_payment"/>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="recv_payment">
    <table class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="2">
	  Edit Pending Card Transaction (result)
        </td>
      </tr>
      <tr><td colspan="2">You've successfully edited the following card transaction:</td></tr>
      <tr>
	<td><b>Account name: </b></td>
	<td> <xsl:value-of select="account_name"/> (ID #<xsl:value-of select="cust_acct_id"/>)</td>
      </tr>
      <tr>
	<td><b>Card number: </b></td>
	<td> <xsl:value-of select="card_number"/></td>
      </tr>
      <tr>
	<td><b>Amount of payment: </b></td>
	<td>$<xsl:value-of select="amount"/></td>
      </tr>
      <tr>
	<td><b>Received by: </b></td>
	<td><xsl:value-of select="employee/first_name"/><xsl:text> </xsl:text><xsl:value-of select="employee/last_name"/></td>
      </tr>
      <xsl:if test="note != ''">
	<tr valign="top">
	  <td><b>Note: </b></td>
	  <td><xsl:value-of select="note"/></td>
	</tr>
      </xsl:if>
      <tr>
	<td><b>Time received: </b></td>
	<td><xsl:value-of select="recv_dt"/></td>
      </tr>
      <tr>
	<td><b>When to charge card: </b></td>
	<td><xsl:value-of select="process_txn_after_dt"/></td>
      </tr>
      <tr>
	<td><b>Process mode: </b></td>
	<td><xsl:value-of select="process_mode"/></td>
      </tr>
      <tr>
	<td><b>Process status: </b></td>
	<td><xsl:value-of select="process_status"/></td>
      </tr>
      <xsl:if test="process_status = 'complete'">
	<tr valign="top">
	  <td><b>Response AVS: </b></td>
	  <td><xsl:value-of select="response_avs"/></td>
	</tr>
	<tr valign="top">
	  <td><b>Response error: </b></td>
	  <td><xsl:value-of select="response_error"/></td>
	</tr>
	<tr valign="top">
	  <td><b>Response approved: </b></td>
	  <td><xsl:value-of select="response_approved"/></td>
	</tr>
	<tr valign="top">
	  <td><b>Response code: </b></td>
	  <td><xsl:value-of select="response_code"/></td>
	</tr>
	<tr valign="top">
	  <td><b>Response message: </b></td>
	  <td><xsl:value-of select="response_message"/></td>
	</tr>
      </xsl:if>
    </table>
    <p>
      <xsl:choose>
	<xsl:when test="process_status = 'complete'">
	  Check the transaction response above. If it succeeded, receive payment into QuickBooks. If it failed, contact the customer to try again.
	</xsl:when>
	<xsl:when test="process_status = 'pending' and process_mode = 'automatic'">
	  The payment was queued for automatic processing on or after <xsl:value-of select="process_txn_after_dt"/>. You will receive an email when the payment has been automatically processed.
	</xsl:when>
	<xsl:when test="process_status = 'pending' and process_mode = 'manual'">
	  The payment must be manually processed on or after <xsl:value-of select="process_txn_after_dt"/>. You should check the list of pending card transactions to manually release the card transaction for processing. You will receive a reminder email if manual card transactions have not been processed as of the day after the date when they can be processed.
	</xsl:when>
      </xsl:choose>
    </p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

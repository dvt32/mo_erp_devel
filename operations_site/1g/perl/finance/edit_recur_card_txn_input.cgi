#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use get_row_by_id_map;
use get_row_list;
use get_month_name;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/receive_payment/card");
  # get input data.
  my $recur_card_txn_id = $cgi->param("id");
  my $recur_card_txn;
  if ((!$recur_card_txn_id) || (!($recur_card_txn = select_row($dbh, "recur_card_txn_tbl", $recur_card_txn_id)))) {
    die "show_page: edit_recur_card_txn_not_found.html\n";
  }
  # load accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # load persons.
  my (%cust_pers_by_id);
  $sql_query = "select * from cust_pers_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment/card");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # populate information needed for display of page.
  $recur_card_txn->{"next_date"} =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  my ($next_date_year, $next_date_month, $next_date_day) = ($1, int($2), int($3));
  my $frequency = $recur_card_txn->{"frequency"};
  my $account_id = $recur_card_txn->{"account_id"};
  my $process_mode = $recur_card_txn->{"process_mode"};
  my $employee_id = $recur_card_txn->{"employee_id"} || "";
  my $amount = $recur_card_txn->{"amount"};
  my $card_number = $recur_card_txn->{"card_number"};
  my $card_security_code = $recur_card_txn->{"card_security_code"};
  my $card_expiration_month = int($recur_card_txn->{"card_expiration_month"});
  my $card_expiration_year = $recur_card_txn->{"card_expiration_year"};
  my $card_bill_name = HTML::Entities::encode($recur_card_txn->{"card_bill_name"});
  my $card_bill_company = HTML::Entities::encode($recur_card_txn->{"card_bill_company"} || "");
  my $card_bill_address_num = HTML::Entities::encode($recur_card_txn->{"card_bill_address_num"} || "");
  my $card_bill_address_1 = HTML::Entities::encode($recur_card_txn->{"card_bill_address_1"});
  my $card_bill_address_2 = HTML::Entities::encode($recur_card_txn->{"card_bill_address_2"} || "");
  my $card_bill_city = HTML::Entities::encode($recur_card_txn->{"card_bill_city"});
  my $card_bill_state = HTML::Entities::encode($recur_card_txn->{"card_bill_state"});
  my $card_bill_state_other = "";
  if (!grep {$card_bill_state eq $_->{"name"}} @state_list) {
    $card_bill_state_other = $card_bill_state;
    $card_bill_state = "";
  }
  $card_bill_state = HTML::Entities::encode($card_bill_state);
  $card_bill_state_other = HTML::Entities::encode($card_bill_state_other);
  my $card_bill_zip_code = HTML::Entities::encode($recur_card_txn->{"card_bill_zip_code"});
  my $card_bill_country = HTML::Entities::encode($recur_card_txn->{"card_bill_country"});
  my $card_bill_phone_number = HTML::Entities::encode($recur_card_txn->{"card_bill_phone_number"} || "");
  my $card_bill_fax_number = HTML::Entities::encode($recur_card_txn->{"card_bill_fax_number"} || "");
  my $card_bill_email_address = HTML::Entities::encode($recur_card_txn->{"card_bill_email_address"} || "");
  my $note = HTML::Entities::encode($recur_card_txn->{"note"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <account-list>
---END_BLOCK---;
  for my $_cust_acct_id (@cust_acct_id_list) {
    my $_cust_acct = $cust_acct_by_id{$_cust_acct_id};
    my $name = HTML::Entities::encode($_cust_acct->{"name"});
    my $primary_contact_cust_pers_id = $_cust_acct->{"primary_contact_cust_pers_id"};
    my $primary_contact = $cust_pers_by_id{$primary_contact_cust_pers_id};
    my $primary_contact_name = ($primary_contact->{"first_name"} || "") . " " . ($primary_contact->{"last_name"} || ""); 
    $primary_contact_name = HTML::Entities::encode($primary_contact_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$_cust_acct_id</id>
      <name>$name</name>
      <primary-contact>$primary_contact_name</primary-contact>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <employee-list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $employee = $employee_by_id{$_employee_id};
    my $first_name = HTML::Entities::encode($employee->{"first_name"});
    my $last_name = HTML::Entities::encode($employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <process_mode_list>
    <process_mode>automatic</process_mode>
    <process_mode>manual</process_mode>
  </process_mode_list>
  <frequency_list>
    <frequency>never</frequency>
    <frequency>monthly</frequency>
  </frequency_list>
  <recur_card_txn>
    <id>$recur_card_txn_id</id>
    <next_date_year>$next_date_year</next_date_year>
    <next_date_month>$next_date_month</next_date_month>
    <next_date_day>$next_date_day</next_date_day>
    <frequency>$frequency</frequency>
    <account_id>$account_id</account_id>
    <process_mode>$process_mode</process_mode>
    <employee_id>$employee_id</employee_id>
    <amount>$amount</amount>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <card_bill_name>$card_bill_name</card_bill_name>
    <card_bill_company>$card_bill_company</card_bill_company>
    <card_bill_address_num>$card_bill_address_num</card_bill_address_num>
    <card_bill_address_1>$card_bill_address_1</card_bill_address_1>
    <card_bill_address_2>$card_bill_address_2</card_bill_address_2>
    <card_bill_city>$card_bill_city</card_bill_city>
    <card_bill_state>$card_bill_state</card_bill_state>
    <card_bill_state_other>$card_bill_state_other</card_bill_state_other>
    <card_bill_zip_code>$card_bill_zip_code</card_bill_zip_code>
    <card_bill_country>$card_bill_country</card_bill_country>
    <card_bill_phone_number>$card_bill_phone_number</card_bill_phone_number>
    <card_bill_fax_number>$card_bill_fax_number</card_bill_fax_number>
    <card_bill_email_address>$card_bill_email_address</card_bill_email_address>
    <note>$note</note>
  </recur_card_txn>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_recur_card_txn_input.xslt", $cgi);
}

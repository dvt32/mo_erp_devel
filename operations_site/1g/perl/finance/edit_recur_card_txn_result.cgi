#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;
use is_valid_credit_card;
use strip_outside_whitespace;
use parse_dollars_cents;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/receive_payment/card");
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $recur_card_txn_id = $cgi->param("id");
  my $recur_card_txn;
  if ((!$recur_card_txn_id) || (!($recur_card_txn = select_row($dbh, "recur_card_txn_tbl", $recur_card_txn_id)))) {
    die "show_page: edit_recur_card_txn_not_found.html\n";
  }
  my ($next_date_month, $next_date_day, $next_date_year) = map {$cgi->param($_)} (qw(next_date_month next_date_day next_date_year));
  my $next_date = $next_date_year . "-" . lead_pad($next_date_month, 2) . "-" . lead_pad($next_date_day, 2);
  my ($now_date_year, $now_date_month, $now_date_day) = get_date_now(1);
  my $now_date = "$now_date_year-$now_date_month-$now_date_day";
  if (($next_date_month < 1) || ($next_date_month > 12) || ($next_date_day < 1) || ($next_date_day > 31) ||
      ($next_date_year < 2008) || ($next_date_year > 3000) || ($next_date lt $now_date)) {
    die "show_page: edit_recur_card_txn_bad_next_date.html\n";
  }
  my $frequency = $cgi->param("frequency");
  if ($frequency !~ /^never|monthly$/) {
    die "show_page: edit_recur_card_txn_bad_frequency.html\n";
  }
  my $account_id = $cgi->param("account_id");
  my $account;
  if ((!$account_id) || (!($account = select_row($dbh, "cust_acct_tbl", $account_id)))) {
    die "show_page: edit_recur_card_txn_bad_account.html\n";
  }
  my $process_mode = $cgi->param("process_mode");
  if ((!$process_mode) || ($process_mode !~ /^automatic|manual$/)) {
    die "show_page: edit_recur_card_txn_bad_process_mode.html\n";
  }
  my $recv_employee_id = $cgi->param("employee_id");
  my $recv_employee;
  if ((!$recv_employee_id) || (!($recv_employee = select_row($dbh, "employee_tbl", $recv_employee_id)))) {
    die "show_page: edit_recur_card_txn_bad_employee.html\n";
  }
  my $amount = $cgi->param("amount") || "";
  eval {
    my ($amount_dollars, $amount_cents) = parse_dollars_cents($amount);
    $amount = "$amount_dollars.$amount_cents";
  };
  if ($@) {
    die "show_page: edit_recur_card_txn_bad_amount.html\n";
  }
  my $card_number = $cgi->param("card_number");
  if ((!$card_number) || (!is_valid_credit_card($card_number))) {
    die "show_page: edit_recur_card_txn_bad_card_number.html\n";
  }
  my $card_security_code = $cgi->param("card_security_code");
  if (($card_security_code) && ($card_security_code !~ /^\d{3,4}$/)) {
    die "show_page: edit_recur_card_txn_bad_card_security_code.html\n";
  }
  my $card_expiration_month = $cgi->param("card_expiration_month");
  if ((!$card_expiration_month) || ($card_expiration_month < 1) || ($card_expiration_month > 12)) {
    die "show_page: edit_recur_card_txn_bad_card_expiration_month.html\n";
  }
  $card_expiration_month = lead_pad(int($card_expiration_month), 2);
  my $card_expiration_year = $cgi->param("card_expiration_year");
  if ((!$card_expiration_year) || ($card_expiration_year < 2000) || ($card_expiration_year > 3000)) {
    die "show_page: edit_recur_card_txn_bad_card_expiration_year.html\n";
  }
  my $card_bill_name = Encode::decode($cgi_input_charset, $cgi->param("card_bill_name"));
  if (!$card_bill_name) {
    die "show_page: edit_recur_card_txn_missing_bill_name.html\n";
  }
  my $card_bill_company = Encode::decode($cgi_input_charset, $cgi->param("card_bill_company"));
  my $card_bill_address_num = Encode::decode($cgi_input_charset, $cgi->param("card_bill_address_num"));
  my $card_bill_address_1 = Encode::decode($cgi_input_charset, $cgi->param("card_bill_address_1"));
  if (!$card_bill_address_1) {
    die "show_page: edit_recur_card_txn_missing_bill_address_1.html\n";
  }
  my $card_bill_address_2 = Encode::decode($cgi_input_charset, $cgi->param("card_bill_address_2"));
  my $card_bill_city = Encode::decode($cgi_input_charset, $cgi->param("card_bill_city"));
  if (!$card_bill_city) {
    die "show_page: edit_recur_card_txn_missing_bill_city.html\n";
  }
  my $card_bill_state = Encode::decode($cgi_input_charset, $cgi->param("card_bill_state"));
  if (!$card_bill_state) {
    if (!($card_bill_state = Encode::decode($cgi_input_charset, $cgi->param("card_bill_state_other")))) {
      die "show_page: edit_recur_card_txn_missing_bill_state.html\n";
    }
  }
  my $card_bill_zip_code = Encode::decode($cgi_input_charset, $cgi->param("card_bill_zip_code"));
  if (!$card_bill_zip_code) {
    die "show_page: edit_recur_card_txn_missing_bill_zip_code.html\n";
  }
  my $card_bill_country = Encode::decode($cgi_input_charset, $cgi->param("card_bill_country"));
  if (!$card_bill_country) {
    die "show_page: edit_recur_card_txn_missing_bill_country.html\n";
  }
  my $card_bill_phone_number = Encode::decode($cgi_input_charset, $cgi->param("card_bill_phone_number"));
  my $card_bill_fax_number = Encode::decode($cgi_input_charset, $cgi->param("card_bill_fax_number"));
  my $card_bill_email_address = Encode::decode($cgi_input_charset, $cgi->param("card_bill_email_address"));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # update existing row in recur_card_txn_tbl.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["employee_id", $recv_employee_id, SQL_INTEGER]);
  push(@sql_column_list, ["amount", $amount, SQL_DECIMAL]);
  push(@sql_column_list, ["process_mode", $process_mode]);
  push(@sql_column_list, ["next_date", $next_date]);
  push(@sql_column_list, ["frequency", $frequency]);
  push(@sql_column_list, ["card_number", $card_number]);
  push(@sql_column_list, ["card_expiration_month", $card_expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $card_expiration_year]);
  push(@sql_column_list, ["card_security_code", $card_security_code]);
  push(@sql_column_list, ["card_bill_name", Encode::encode($db_charset, $card_bill_name)]);
  push(@sql_column_list, ["card_bill_company", Encode::encode($db_charset, $card_bill_company)]);
  push(@sql_column_list, ["card_bill_address_num", Encode::encode($db_charset, $card_bill_address_num)]);
  push(@sql_column_list, ["card_bill_zip_code", Encode::encode($db_charset, $card_bill_zip_code)]);
  push(@sql_column_list, ["card_bill_address_1", Encode::encode($db_charset, $card_bill_address_1)]);
  push(@sql_column_list, ["card_bill_address_2", Encode::encode($db_charset, $card_bill_address_2)]);
  push(@sql_column_list, ["card_bill_city", Encode::encode($db_charset, $card_bill_city)]);
  push(@sql_column_list, ["card_bill_state", Encode::encode($db_charset, $card_bill_state)]);
  push(@sql_column_list, ["card_bill_country", Encode::encode($db_charset, $card_bill_country)]);
  push(@sql_column_list, ["card_bill_phone_number", Encode::encode($db_charset, $card_bill_phone_number)]);
  push(@sql_column_list, ["card_bill_fax_number", Encode::encode($db_charset, $card_bill_fax_number)]);
  push(@sql_column_list, ["card_bill_email_address", Encode::encode($db_charset, $card_bill_email_address)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "recur_card_txn_tbl", \@sql_column_list, $recur_card_txn_id);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $employee_name = HTML::Entities::encode($recv_employee->{"first_name"} . " " . $recv_employee->{"last_name"});
  $card_bill_name = HTML::Entities::encode($card_bill_name || "");
  $card_bill_company = HTML::Entities::encode($card_bill_company || "");
  $card_bill_address_num = HTML::Entities::encode($card_bill_address_num || "");
  $card_bill_zip_code = HTML::Entities::encode($card_bill_zip_code || "");
  $card_bill_address_1 = HTML::Entities::encode($card_bill_address_1 || "");
  $card_bill_address_2 = HTML::Entities::encode($card_bill_address_2 || "");
  $card_bill_city = HTML::Entities::encode($card_bill_city || "");
  $card_bill_state = HTML::Entities::encode($card_bill_state || "");
  $card_bill_country = HTML::Entities::encode($card_bill_country || "");
  $card_bill_phone_number = HTML::Entities::encode($card_bill_phone_number || "");
  $card_bill_fax_number = HTML::Entities::encode($card_bill_fax_number || "");
  $card_bill_email_address = HTML::Entities::encode($card_bill_email_address || "");
  $note = HTML::Entities::encode($note || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <operation>edit</operation>
  <recur_card_txn>
    <next_date>$next_date</next_date>
    <frequency>$frequency</frequency>
    <account_id>$account_id</account_id>
    <account_name>$account_name</account_name>
    <process_mode>$process_mode</process_mode>
    <employee_name>$employee_name</employee_name>
    <amount>$amount</amount>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <card_bill_name>$card_bill_name</card_bill_name>
    <card_bill_company>$card_bill_company</card_bill_company>
    <card_bill_address_num>$card_bill_address_num</card_bill_address_num>
    <card_bill_zip_code>$card_bill_zip_code</card_bill_zip_code>
    <card_bill_address_1>$card_bill_address_1</card_bill_address_1>
    <card_bill_address_2>$card_bill_address_2</card_bill_address_2>
    <card_bill_city>$card_bill_city</card_bill_city>
    <card_bill_state>$card_bill_state</card_bill_state>
    <card_bill_country>$card_bill_country</card_bill_country>
    <card_bill_phone_number>$card_bill_phone_number</card_bill_phone_number>
    <card_bill_fax_number>$card_bill_fax_number</card_bill_fax_number>
    <card_bill_email_address>$card_bill_email_address</card_bill_email_address>
    <note>$note</note>
  </recur_card_txn>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_recur_card_txn_result.xslt", $cgi);
}

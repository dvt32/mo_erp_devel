<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Operations Center :: 
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Recurring Card Transaction (result)
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Recurring Card Transaction (result)
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
          <a href="/1g/index.cgi">Main menu</a>
	  <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
	    |
	    Credit/Debit Card Transactions:
	    <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
	    &middot;
	    <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
	    &middot;
	    <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
	  </xsl:if>
	</p>
        <xsl:apply-templates select="recur_card_txn"/>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="recur_card_txn">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following recurring credit/debit card transaction:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new recurring credit/debit card transaction:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="2">
	  Edit Recurring Card Transaction (result)
        </td>
      </tr>
      <tr>
	<td><b>Next Date: </b></td>
	<td><xsl:value-of select="next_date"/></td>
      </tr>
      <tr>
	<td><b>Frequency: </b></td>
	<td><xsl:value-of select="frequency"/></td>
      </tr>
      <tr>
	<td><b>Account name: </b></td>
	<td><xsl:value-of select="account_name"/> (ID #<xsl:value-of select="account_id"/>)</td>
      </tr>
      <tr>
	<td><b>Process mode: </b></td>
	<td><xsl:value-of select="process_mode"/></td>
      </tr>
      <tr>
	<td><b>Received by: </b></td>
	<td><xsl:value-of select="employee_name"/></td>
      </tr>
      <tr>
	<td><b>Amount of payment: </b></td>
	<td>$<xsl:value-of select="amount"/></td>
      </tr>
      <tr>
	<td><b>Card number: </b></td>
	<td><xsl:value-of select="card_number"/></td>
      </tr>
      <tr>
	<td><b>Card security code: </b></td>
	<td><xsl:value-of select="card_security_code"/></td>
      </tr>
      <tr>
	<td><b>Card expiration date: </b></td>
	<td><xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/></td>
      </tr>
      <tr>
	<td><b>Person name on card: </b></td>
	<td><xsl:value-of select="card_bill_name"/></td>
      </tr>
      <tr>
	<td><b>Company name on card: </b></td>
	<td><xsl:value-of select="card_bill_company"/></td>
      </tr>
      <tr>
	<td><b>Address 1 number: </b></td>
	<td><xsl:value-of select="card_bill_address_num"/></td>
      </tr>
      <tr>
	<td><b>Address 1: </b></td>
	<td><xsl:value-of select="card_bill_address_1"/></td>
      </tr>
      <tr>
	<td><b>Address 2: </b></td>
	<td><xsl:value-of select="card_bill_address_2"/></td>
      </tr>
      <tr>
	<td><b>Zip Code: </b></td>
	<td><xsl:value-of select="card_bill_zip_code"/></td>
      </tr>
      <tr>
	<td><b>City: </b></td>
	<td><xsl:value-of select="card_bill_city"/></td>
      </tr>
      <tr>
	<td><b>State: </b></td>
	<td><xsl:value-of select="card_bill_state"/></td>
      </tr>
      <tr>
	<td><b>Country: </b></td>
	<td><xsl:value-of select="card_bill_country"/></td>
      </tr>
      <tr>
	<td><b>Phone Number: </b></td>
	<td><xsl:value-of select="card_bill_phone_number"/></td>
      </tr>
      <tr>
	<td><b>Fax Number: </b></td>
	<td><xsl:value-of select="card_bill_fax_number"/></td>
      </tr>
      <tr>
	<td><b>Email Address: </b></td>
	<td><xsl:value-of select="card_bill_email_address"/></td>
      </tr>
      <tr valign="top">
	<td><b>Note: </b></td>
	<td><xsl:value-of select="note"/></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

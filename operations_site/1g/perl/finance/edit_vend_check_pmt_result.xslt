<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Vendor Check Payment
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Vendor Check Payment
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/new_vend_ach_pmt_input.cgi">New Vendor ACH Payment</a>
	  |
	  <a href="/1g/finance/new_vend_check_pmt_input.cgi">New Vendor Check Payment</a>
        </p>
	<xsl:apply-templates select="check_paid"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="check_paid">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following vendor check payment:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new vendor check payment:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  ID:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Payor Company ID:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="payor_company_id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Payor Company Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="payor_company_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Source Routing Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="source_routing_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Source Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="source_account_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Payee Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="payee_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Amount:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Check Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="check_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

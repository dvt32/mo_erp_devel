#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;
use get_database_charset;
use lead_pad;
use insert_row;
use update_row;
use select_row;
use get_date_tomorrow;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI input parameters.
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @electronic_check_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@electronic_check_id_list, $1);
    }
  }
  # determine tomorrow's date.
  my ($year, $month, $day) = get_date_tomorrow(1);
  my $tomorrow = "$year-$month-$day";
  # begin database transaction so our updates are atomic.
  $dbh->begin_work();
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  # copy rows from electronic_check_tbl to ach_debit_txn_tbl.
  my (%electronic_check_by_id, @processed_electronic_check_id_list);
  for my $electronic_check_id (@electronic_check_id_list) {
    my $electronic_check = select_row($dbh, "electronic_check_tbl", $electronic_check_id);
    if ((!$electronic_check) || ($electronic_check->{"processed"}) || ($electronic_check->{"cancelled"}) || ($electronic_check->{"debit_date"}) gt $tomorrow) {
      next;
    }
    # insert new row in ach_debit_txn_tbl from electronic_check_tbl,
    @sql_column_list = ();
    push(@sql_column_list, ["cust_acct_id", $electronic_check->{"cust_acct_id"}, SQL_INTEGER]);
    push(@sql_column_list, ["amount", $electronic_check->{"amount"}]);
    push(@sql_column_list, ["customer_name", Encode::encode($db_charset, $electronic_check->{"bank_account_name"})]);
    push(@sql_column_list, ["customer_bank_id", $electronic_check->{"routing_number"}]);
    push(@sql_column_list, ["customer_bank_acct_num", $electronic_check->{"account_number"}]);
    push(@sql_column_list, ["customer_bank_acct_type", uc(substr($electronic_check->{"bank_account_type"}, 0, 1))]);
    push(@sql_column_list, ["company_bank_acct_num", "9962340795"]);
    push(@sql_column_list, ["company_bank_acct_type", "C"]);
    push(@sql_column_list, ["create_prenote", 0, SQL_BOOLEAN]);
    push(@sql_column_list, ["hold_transfer", 0, SQL_BOOLEAN]);
    push(@sql_column_list, ["electronic_check_id", $electronic_check_id, SQL_INTEGER]);
    if ($electronic_check->{"notes"}) {
      push(@sql_column_list, ["notes", Encode::encode($db_charset, "customer-entered notes: " . $electronic_check->{"notes"})]);
    }
    insert_row($dbh, "ach_debit_txn_tbl", \@sql_column_list);
    # mark copied row as processed.
    @sql_column_list = ();
    push(@sql_column_list, ["processed", 1, SQL_BOOLEAN]);
    update_row($dbh, "electronic_check_tbl", \@sql_column_list, $electronic_check_id);
    # save information for later display.
    $electronic_check->{"processed"} = 1;
    push(@processed_electronic_check_id_list, $electronic_check_id);
    $electronic_check_by_id{$electronic_check_id} = $electronic_check;
  }
  # commit the transaction.
  $dbh->commit();
  # fetch accounts.
  my %cust_acct_by_id;
  my $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <electronic-check-list>
---END_BLOCK---;
  for my $electronic_check_id (@processed_electronic_check_id_list) {
    my $electronic_check = $electronic_check_by_id{$electronic_check_id};
    my $cust_acct_id = $electronic_check->{"cust_acct_id"};
    my $account_name = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $bank_account_name = HTML::Entities::encode($electronic_check->{"bank_account_name"});
    my $bank_account_type = HTML::Entities::encode($electronic_check->{"bank_account_type"});
    my $routing_number = HTML::Entities::encode($electronic_check->{"routing_number"});
    my $account_number = HTML::Entities::encode($electronic_check->{"account_number"});
    my $amount = HTML::Entities::encode($electronic_check->{"amount"});
    my $debit_date = HTML::Entities::encode($electronic_check->{"debit_date"});
    my $notes = HTML::Entities::encode($electronic_check->{"notes"});
    $generated_xml .= <<"---END_BLOCK---;";
    <electronic-check>
      <id>$electronic_check_id</id>
      <account_name>$account_name</account_name>
      <bank_account_name>$bank_account_name</bank_account_name>
      <bank_account_type>$bank_account_type</bank_account_type>
      <routing_number>$routing_number</routing_number>
      <account_number>$account_number</account_number>
      <amount>$amount</amount>
      <debit_date>$debit_date</debit_date>
      <notes>$notes</notes>
    </electronic-check>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </electronic-check-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "enqueue_ach_debit_txns.xslt", $cgi);
}

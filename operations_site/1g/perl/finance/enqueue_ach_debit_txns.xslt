<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Enqueue ACH Debit Transactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  The following transactions were enqueued:
	</p>
	<xsl:apply-templates select="electronic-check-list"/>
	<p>
	  View <a href="/1g/finance/show_pending_ach_debit_txns.cgi">pending ACH debit transactions</a> or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="electronic-check-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="19" class="standard_table_title">
	  Queued ACH Debit Transactions
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td align="center">#</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Bank Acct Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Bank Acct Type</td>
	<td class="standard_table_spacer_column"></td>
	<td>Routing Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Acct Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Debit Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Notes</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(electronic-check) > 0">
	  <xsl:apply-templates select="electronic-check"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="17">No electronic checks were queued as ACH debit transactions.</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="electronic-check">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="right"><xsl:value-of select="position()"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="bank_account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="bank_account_type"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="routing_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="debit_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="notes"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

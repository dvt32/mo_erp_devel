<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Delete Payments (result)
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/make_deposit_input.cgi">Make Deposit</a>
	  <xsl:if test="role_list/role = 'role:finance/show_deposits'">
	    |
	    <a href="/1g/finance/show_deposits.cgi">Show Deposits</a>
	  </xsl:if>
        </p>
	<xsl:choose>
	  <xsl:when test="count(receive_payment_list/receive_payment) &gt; 0">
	    <xsl:apply-templates select="receive_payment_list"/>
	    <p>
	      <xsl:apply-templates select="deletion"/>
	    </p>
	  </xsl:when>
	  <xsl:otherwise>
	    <p>
	      No payment items were deleted.
	    </p>
	  </xsl:otherwise>
	</xsl:choose>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="deletion">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Deleted By:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="employee_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Total Amount:
	</td>
	<td class="map_table_value">
	  $<xsl:value-of select="total_amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  # of Items:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="count(../receive_payment_list/receive_payment)"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="receive_payment_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="19" class="standard_table_title">
	  Deleted Payments
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Account Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Form</td>
	<td class="standard_table_spacer_column"></td>
	<td>Ck #</td>
	<td class="standard_table_spacer_column"></td>
	<td>Name on Chk</td>
	<td class="standard_table_spacer_column"></td>
	<td>Chk Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Received Time</td>
	<td class="standard_table_spacer_column"></td>
	<td>Received By</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="receive_payment"/>
    </table>
  </xsl:template>

  <xsl:template match="receive_payment">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="payment_method"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="received_dt"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="employee_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

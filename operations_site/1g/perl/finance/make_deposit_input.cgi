#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use lead_pad;
use get_row_by_id_map;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/make_deposit");
  # load accounts.
  my (%cust_acct_by_id);
  my $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # load received payments with payment destination of "bank".
  $sql_query = "select * from recv_payment_tbl where deposit_id is null and (payment_destination = ? or payment_destination = ?) order by employee_id, recv_dt";
  @sql_param_list = ("bank", "manager");
  my (%recv_payment_by_id, @recv_payment_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%recv_payment_by_id, \@recv_payment_id_list);
  # load deposit bank accounts.
  $sql_query = "select * from deposit_bank_account_tbl";
  my (%deposit_bank_account_by_id, @deposit_bank_account_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%deposit_bank_account_by_id);
  # load banks.
  $sql_query = "select * from bank_tbl";
  my (%bank_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%bank_by_id);
  # load companies.
  $sql_query = "select * from company_tbl";
  my (%company_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%company_by_id);
  # populate information needed for display of page.
  # determine today's date.
  my ($year, $month, $day) = get_date_now(1);
  # sort deposit bank accounts.
  @deposit_bank_account_id_list = sort {
    my $deposit_bank_account_a = $deposit_bank_account_by_id{$a};
    my $deposit_bank_account_b = $deposit_bank_account_by_id{$b};
    my $company_a = $company_by_id{$deposit_bank_account_a->{"company_id"}};
    my $company_b = $company_by_id{$deposit_bank_account_b->{"company_id"}};
    my $sort_value = 0;
    $sort_value ||= $company_a->{"short_name"} cmp $company_b->{"short_name"};
    if (!$sort_value) {
      my $bank_a = $bank_by_id{$deposit_bank_account_a->{"bank_id"}};
      my $bank_b = $bank_by_id{$deposit_bank_account_b->{"bank_id"}};
      $sort_value ||= $bank_a->{"name"} cmp $bank_b->{"name"};
    }
    $sort_value;
  } keys(%deposit_bank_account_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>$year-$month-$day</today>
  <role_list>
---END_BLOCK---;
  for my $role (@$role_list) {
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role_list>
  <current_employee>
    <id>$employee->{"id"}</id>
  </current_employee>
  <employee_list>
---END_BLOCK---;
  for my $employee_id (@employee_id_list) {
    my $employee = $employee_by_id{$employee_id};
    my $first_name = HTML::Entities::encode($employee->{"first_name"});
    my $last_name = HTML::Entities::encode($employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee_list>
  <bank_account_list>
---END_BLOCK---;
  for my $deposit_bank_account_id (@deposit_bank_account_id_list) {
    my $deposit_bank_account = $deposit_bank_account_by_id{$deposit_bank_account_id};
    my $bank = $bank_by_id{$deposit_bank_account->{"bank_id"}};
    my $company = $company_by_id{$deposit_bank_account->{"company_id"}};
    my $bank_name = HTML::Entities::encode($bank->{"name"});
    my $bank_short_name = HTML::Entities::encode($bank->{"short_name"});
    my $routing_number = HTML::Entities::encode($bank->{"routing_number"});
    my $account_number = HTML::Entities::encode($deposit_bank_account->{"account_number"});
    my $company_short_name = HTML::Entities::encode($company->{"short_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <bank_account>
      <name>$bank_name</name>
      <short_name>$bank_short_name</short_name>
      <routing_number>$routing_number</routing_number>
      <account_number>$account_number</account_number>
      <entity>$company_short_name</entity>
    </bank_account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </bank_account_list>
  <receive_payment_list>
---END_BLOCK---;
  for my $recv_payment_id (@recv_payment_id_list) {
    my $recv_payment = $recv_payment_by_id{$recv_payment_id};
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    my $amount = $recv_payment->{"amount"};
    my $payment_method = HTML::Entities::encode($recv_payment->{"payment_method"});
    my $check_number = HTML::Entities::encode($recv_payment->{"check_number"});
    my $check_name = HTML::Entities::encode($recv_payment->{"check_name"});
    my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
    my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
    my $note = HTML::Entities::encode($recv_payment->{"note"});
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee = $employee_by_id{$employee_id};
    my $employee_name = HTML::Entities::encode($employee->{"first_name"} . " " . $employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <receive_payment>
      <id>$recv_payment_id</id>
      <account_name>$account_name</account_name>
      <amount>$amount</amount>
      <payment_method>$payment_method</payment_method>
      <check_number>$check_number</check_number>
      <check_name>$check_name</check_name>
      <check_date>$check_date</check_date>
      <received_dt>$received_dt</received_dt>
      <note>$note</note>
      <employee_name>$employee_name</employee_name>
    </receive_payment>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </receive_payment_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "make_deposit_input.xslt", $cgi);
}

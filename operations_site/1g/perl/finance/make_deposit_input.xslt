<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Make Deposit
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<script src="/1g/js/toggle_checkboxes.js">
	  &nbsp;
	</script>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  <xsl:if test="role_list/role = 'role:finance/show_deposits'">
	    |
	    <a href="/1g/finance/show_deposits.cgi">Show Deposits</a>
	  </xsl:if>
        </p>
	<xsl:apply-templates select="receive_payment_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="receive_payment_list">
    <form action="/1g/finance/make_deposit_result.cgi" method="post">
      <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	<tr>
	  <td colspan="21" class="standard_table_title">
	    Received Payments (Cash and Checks)
	  </td>
	</tr>
	<tr class="standard_table_column_heading_row">
	  <td class="standard_table_side_padding"></td>
	  <td align="center">
	    <input type="checkbox" name="select_all" onclick="toggle_checkboxes(this, 'id-');"/>
	  </td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Account Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Amount</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Form</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Ck #</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Name on Chk</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Chk Date</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Received Time</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Received By</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Note</td>
	  <td class="standard_table_side_padding"></td>
	</tr>
	<xsl:choose>
	  <xsl:when test="count(receive_payment) > 0">
	    <xsl:apply-templates select="receive_payment"/>
	    <xsl:if test="count(receive_payment[translate(check_date,'-','0') &gt; translate(/page/today,'-','0')]) > 0">
	      <tr>
		<td colspan="21" class="standard_table_footer">
		  *Checks dated in the future cannot be deposited. Wait until the check date.
		</td>
	      </tr>
	    </xsl:if>
	  </xsl:when>
	  <xsl:otherwise>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td colspan="19">No received payment items.</td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </xsl:otherwise>
	</xsl:choose>
      </table>
      <p>
	Deposited To:
	<select name="bank_name_routing_account">
	  <xsl:for-each select="/page/bank_account_list/bank_account">
	    <option>
	      <xsl:attribute name="value"><xsl:value-of select="short_name"/>-<xsl:value-of select="routing_number"/>-<xsl:value-of select="account_number"/></xsl:attribute>
	      <xsl:value-of select="entity"/>:
	      <xsl:value-of select="short_name"/>
	      <xsl:text> </xsl:text>
	      ***<xsl:value-of select="substring(account_number, string-length(account_number) - 3)"/>
	      <xsl:text> </xsl:text>
	    </option>
	  </xsl:for-each>
	</select>
      </p>
      <p>
	Deposited By:
	<select name="employee_id">
	  <xsl:for-each select="/page/employee_list/employee">
	    <option>
	      <xsl:if test="/page/current_employee/id = id">
		<xsl:attribute name="selected">selected</xsl:attribute>
	      </xsl:if>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	      <xsl:value-of select="last_name"/>
	      <xsl:text>, </xsl:text>
	      <xsl:value-of select="first_name"/>
	    </option>
	  </xsl:for-each>
	</select>
      </p>
      <p>
	Note: <input type="text" name="note" size="40"/>
      </p>
      <p>
	<input type="submit" name="submit_button" value="Make Deposit"/>
        <xsl:if test="/page/role_list/role = 'role:finance/delete_payments'">
	  <xsl:text> </xsl:text>
	  <input type="submit" name="submit_button" value="Delete Payments"/>
	</xsl:if>
      </p>
    </form>
  </xsl:template>

  <xsl:template match="receive_payment">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
	<xsl:if test="translate(check_date,'-','0') &lt;= translate(/page/today,'-','0')">
	  <input>
	    <xsl:attribute name="type">checkbox</xsl:attribute>
	    <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	  </input>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="payment_method"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="translate(check_date,'-','0') &gt; translate(/page/today,'-','0')">
	    <xsl:attribute name="class">check_date_in_future</xsl:attribute>
	    <xsl:value-of select="check_date"/>*
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="check_date"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="received_dt"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="employee_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use select_row;
use insert_row;
use update_row;
use delete_rows_with_query;
use get_date_now;
use get_timestamp_now;
use is_production_env;
use send_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $submit_button = $cgi->param("submit_button");
  if ($submit_button eq "Make Deposit") {
    make_deposit($instance_map, $cgi, $dbh, $employee);
  } elsif ($submit_button eq "Delete Payments") {
    delete_payments($instance_map, $cgi, $dbh, $employee);
  } else {
    die "show_page: make_deposit_bad_submit_button.html\n";
  }
}

sub make_deposit {
  my ($instance_map, $cgi, $dbh, $employee) = @_;
  my $role_list = authorize_role($instance_map, "role:finance/make_deposit");
  # get CGI input parameters.
  my $bank_name_routing_account = $cgi->param("bank_name_routing_account");
  my ($bank_name, $bank_routing_number, $bank_account_number) = split(/-/, $bank_name_routing_account);
  my $deposit_employee_id = $cgi->param("employee_id");
  my $deposit_employee;
  if ((!$deposit_employee_id) || (!($deposit_employee = select_row($dbh, "employee_tbl", $deposit_employee_id)))) {
    die "show_page: make_deposit_bad_employee.html\n";
  }
  my $deposit_note = $cgi->param("note");
  my @recv_payment_id_list = map {/^id-(\d+)$/; $1;} grep {/^id-\d+$/} $cgi->param();
  my @recv_payment_list;
  my $now_dt = get_timestamp_now();
  for my $recv_payment_id (@recv_payment_id_list) {
    my $recv_payment = select_row($dbh, "recv_payment_tbl", $recv_payment_id);
    if (($recv_payment) && ($recv_payment->{"payment_destination"} =~ /^(bank|manager)$/) && (!$recv_payment->{"deposit_id"}) &&
	($recv_payment->{"recv_dt"} lt $now_dt)) {
      push(@recv_payment_list, $recv_payment);
    }
  }
  # load accounts.
  my (%cust_acct_by_id);
  my $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # begin database transaction so our updates are atomic.
  $dbh->begin_work();
  my $deposit_id;
  if (@recv_payment_list) {
    my @sql_column_list;
    push(@sql_column_list, ["employee_id", $deposit_employee_id, SQL_INTEGER]);
    push(@sql_column_list, ["bank_account_number", $bank_account_number]);
    push(@sql_column_list, ["bank_routing_number", $bank_routing_number]);
    push(@sql_column_list, ["bank_name", $bank_name]);
    push(@sql_column_list, ["note", $deposit_note]);
    $deposit_id = insert_row($dbh, "deposit_tbl", \@sql_column_list, "deposit_seq");
    @sql_column_list = (["deposit_id", $deposit_id, SQL_INTEGER]);
    for my $recv_payment (@recv_payment_list) {
      my $recv_payment_id = $recv_payment->{"id"};
      update_row($dbh, "recv_payment_tbl", \@sql_column_list, $recv_payment_id);
    }
  }
  # commit the transaction.
  $dbh->commit();
  # send out email.
  my $total_amount = 0;
  my $num_deposit_items = scalar(@recv_payment_list);
  my $deposit_employee_name = $deposit_employee->{"first_name"} . " " . $deposit_employee->{"last_name"};
  my $email_message = "";
  for my $recv_payment (@recv_payment_list) {
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = $cust_acct->{"name"};
    my $amount = $recv_payment->{"amount"};
    $total_amount += $amount;
    my $payment_method = $recv_payment->{"payment_method"};
    my $check_number = $recv_payment->{"check_number"};
    my $check_name = $recv_payment->{"check_name"};
    my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
    my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
    my $note = $recv_payment->{"note"};
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee = $employee_by_id{$employee_id};
    my $employee_name = $employee->{"first_name"} . " " . $employee->{"last_name"};
    $amount = sprintf("%.2f", $amount);
    $email_message .= <<"---END_BLOCK---;";
$account_name \$$amount $payment_method $check_number ($check_name) $check_date
  Received by $employee_name at $received_dt, note: $note

---END_BLOCK---;
  }
  my $abbrev_bank_acct_num = substr($bank_account_number, length($bank_account_number) - 4);
  $total_amount = sprintf("%.2f", $total_amount);
  $email_message .= <<"---END_BLOCK---;";

$num_deposit_items items deposited to $bank_name ***$abbrev_bank_acct_num, total: \$$total_amount
---END_BLOCK---;
  if ($deposit_note) {
    $email_message .= <<"---END_BLOCK---;";
Additional operator note: $deposit_note
---END_BLOCK---;
  }
  if (is_production_env()) {
    send_email
      ("from" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
       "to" => "accounts_receivable\@microoffice.com",
       "subject" => "Deposit of \$$total_amount by $deposit_employee_name ($num_deposit_items items)",
       "message" => $email_message);
  }
  # populate information needed for display of page.
  $bank_name = HTML::Entities::encode($bank_name);
  $deposit_id ||= "";
  $deposit_employee_name = HTML::Entities::encode($deposit_employee_name);
  $deposit_note = HTML::Entities::encode($deposit_note);
  my ($today_year, $today_month, $today_day) = get_date_now(1);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <year>$today_year</year>
    <month>$today_month</month>
    <day>$today_day</day>
  </today>
  <role_list>
---END_BLOCK---;
  for my $role (@$role_list) {
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role_list>
  <receive_payment_list>
---END_BLOCK---;
  for my $recv_payment (@recv_payment_list) {
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    my $amount = $recv_payment->{"amount"};
    my $payment_method = HTML::Entities::encode($recv_payment->{"payment_method"});
    my $check_number = HTML::Entities::encode($recv_payment->{"check_number"});
    my $check_name = HTML::Entities::encode($recv_payment->{"check_name"});
    my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
    my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
    my $note = HTML::Entities::encode($recv_payment->{"note"});
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee = $employee_by_id{$employee_id};
    my $employee_name = HTML::Entities::encode($employee->{"first_name"} . " " . $employee->{"last_name"});
    $amount = sprintf("%.2f", $amount);
    $generated_xml .= <<"---END_BLOCK---;";
    <receive_payment>
      <account_name>$account_name</account_name>
      <amount>$amount</amount>
      <payment_method>$payment_method</payment_method>
      <check_number>$check_number</check_number>
      <check_name>$check_name</check_name>
      <check_date>$check_date</check_date>
      <received_dt>$received_dt</received_dt>
      <note>$note</note>
      <employee_name>$employee_name</employee_name>
    </receive_payment>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </receive_payment_list>
  <deposit>
    <id>$deposit_id</id>
    <employee_id>$deposit_employee_id</employee_id>
    <employee_name>$deposit_employee_name</employee_name>
    <bank_name>$bank_name</bank_name>
    <bank_routing_number>$bank_routing_number</bank_routing_number>
    <bank_account_number>$bank_account_number</bank_account_number>
    <total_amount>$total_amount</total_amount>
    <note>$deposit_note</note>
  </deposit>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "make_deposit_result.xslt", $cgi);
}

sub delete_payments {
  my ($instance_map, $cgi, $dbh, $employee) = @_;
  my $role_list = authorize_role($instance_map, "role:finance/delete_payments");
  # get CGI input parameters.
  my $deletion_note = $cgi->param("note");
  my @recv_payment_id_list = map {/^id-(\d+)$/; $1;} grep {/^id-\d+$/} $cgi->param();
  my @recv_payment_list;
  for my $recv_payment_id (@recv_payment_id_list) {
    my $recv_payment = select_row($dbh, "recv_payment_tbl", $recv_payment_id);
    if (($recv_payment) && ($recv_payment->{"payment_destination"} =~ /^(bank|manager)$/) && (!$recv_payment->{"deposit_id"})) {
      push(@recv_payment_list, $recv_payment);
    }
  }
  # load accounts.
  my (%cust_acct_by_id);
  my $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # begin database transaction so our updates are atomic.
  $dbh->begin_work();
  if (@recv_payment_list) {
    $sql_query = "delete from recv_payment_tbl where id = ? and deposit_id is null";
    for my $recv_payment (@recv_payment_list) {
      my $recv_payment_id = $recv_payment->{"id"};
      @sql_param_list = ([$recv_payment_id, SQL_INTEGER]);
      delete_rows_with_query($dbh, $sql_query, \@sql_param_list);
    }
  }
  # commit the transaction.
  $dbh->commit();
  # send out email.
  my $total_amount = 0;
  my $num_deletion_items = scalar(@recv_payment_list);
  my $email_message = "";
  for my $recv_payment (@recv_payment_list) {
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = $cust_acct->{"name"};
    my $amount = $recv_payment->{"amount"};
    $total_amount += $amount;
    my $payment_method = $recv_payment->{"payment_method"};
    my $check_number = $recv_payment->{"check_number"};
    my $check_name = $recv_payment->{"check_name"};
    my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
    my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
    my $note = $recv_payment->{"note"};
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee = $employee_by_id{$employee_id};
    my $employee_name = $employee->{"first_name"} . " " . $employee->{"last_name"};
    $amount = sprintf("%.2f", $amount);
    $email_message .= <<"---END_BLOCK---;";
$account_name \$$amount $payment_method $check_number ($check_name) $check_date
  Received by $employee_name at $received_dt, note: $note

---END_BLOCK---;
  }
  $total_amount = sprintf("%.2f", $total_amount);
  $email_message .= <<"---END_BLOCK---;";

$num_deletion_items items deleted, total: \$$total_amount
---END_BLOCK---;
  if ($deletion_note) {
    $email_message .= <<"---END_BLOCK---;";
Additional operator note: $deletion_note
---END_BLOCK---;
  }
  my $employee_name = $employee->{"first_name"} . " " . $employee->{"last_name"};
  if (is_production_env()) {
    send_email
      ("from" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
       "to" => "accounts_receivable\@microoffice.com",
       "subject" => "Deletion of \$$total_amount in payments by $employee_name ($num_deletion_items items)",
       "message" => $email_message);
  }
  # populate information needed for display of page.
  $employee_name = HTML::Entities::encode($employee_name);
  $deletion_note = HTML::Entities::encode($deletion_note);
  my ($today_year, $today_month, $today_day) = get_date_now(1);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role_list>
---END_BLOCK---;
  for my $role (@$role_list) {
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role_list>
  <receive_payment_list>
---END_BLOCK---;
  for my $recv_payment (@recv_payment_list) {
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    my $amount = $recv_payment->{"amount"};
    my $payment_method = HTML::Entities::encode($recv_payment->{"payment_method"});
    my $check_number = HTML::Entities::encode($recv_payment->{"check_number"});
    my $check_name = HTML::Entities::encode($recv_payment->{"check_name"});
    my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
    my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
    my $note = HTML::Entities::encode($recv_payment->{"note"});
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee = $employee_by_id{$employee_id};
    my $employee_name = HTML::Entities::encode($employee->{"first_name"} . " " . $employee->{"last_name"});
    $amount = sprintf("%.2f", $amount);
    $generated_xml .= <<"---END_BLOCK---;";
    <receive_payment>
      <account_name>$account_name</account_name>
      <amount>$amount</amount>
      <payment_method>$payment_method</payment_method>
      <check_number>$check_number</check_number>
      <check_name>$check_name</check_name>
      <check_date>$check_date</check_date>
      <received_dt>$received_dt</received_dt>
      <note>$note</note>
      <employee_name>$employee_name</employee_name>
    </receive_payment>
---END_BLOCK---;
  }
  my $employee_id = $employee->{"id"};
  $generated_xml .= <<"---END_BLOCK---;";
  </receive_payment_list>
  <deletion>
    <employee_id>$employee_id</employee_id>
    <employee_name>$employee_name</employee_name>
    <total_amount>$total_amount</total_amount>
    <note>$deletion_note</note>
  </deletion>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "make_deposit_delete_payments_result.xslt", $cgi);
}

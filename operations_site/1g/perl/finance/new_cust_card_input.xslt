<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: New Customer Credit/Debit Card
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/show_cust_cards.cgi">Show Credit/Debit Cards</a>
	  |
	  <a href="/1g/finance/new_cust_card_input.cgi">New Credit/Debit Card</a>
<!--	  |
	  <a href="/1g/finance/show_cust_bank_accts.cgi">Show Bank Accounts</a>
	  |
	  <a href="/1g/finance/new_cust_bank_acct_input.cgi">New Bank Account</a> -->
        </p>
	<xsl:apply-templates select="credit_card"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit_card">
    <table>
      <tr><td>
	<form action="/1g/finance/new_cust_card_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Customer Credit/Debit Card
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Account:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>account_id</xsl:text>
		    </xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/account-list/account">
		      <option>
			<xsl:if test="/page/account_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Person:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>person_id</xsl:text>
		    </xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/person-list/person">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="last_name"/><xsl:text>, </xsl:text><xsl:value-of select="first_name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Card Nickname:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">card_nickname</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Card Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">card_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Card Security Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">card_security_code</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Expiration Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>card_expiration_month</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/month-list/month">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			(<xsl:value-of select="number"/>) <xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">
		      <xsl:text>card_expiration_year</xsl:text>
		    </xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_name</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Company:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_company</xsl:attribute>
		  </input>
		</td>
	      </tr>
 	      <tr>
		<td style="padding: 4px; ">
		  Billing Address 1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_1</xsl:attribute>
		  </input>
		</td>
	      </tr>
 	      <tr>
		<td style="padding: 4px; ">
		  Billing Address 2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_2</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing City:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_city</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing State:
		</td>
		<td>
		  <select name="billing_state">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/state-list/state">
		      <option>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  or Province/Territory:
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_state_other</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Zip Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_zip_code</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Country:
		</td>
		<td>
		  <select name="billing_country">
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/country-list/country">
		      <option>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_phone_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Fax Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_fax_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Email Address:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_email_address</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Address Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_num</xsl:attribute>
		  </input>
		  (the numeric portion of the address)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

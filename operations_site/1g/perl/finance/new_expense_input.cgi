#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use get_role_list;
use sorted_tree_from_id_map;
use flatten_sorted_tree;
use get_datetime_now;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch all employees to generate drop down list.
  my (%employee_by_id, @employee_id_list);
  my $sql_query = "select * from employee_tbl where is_active order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  # get role list of current user.
  my $employee_id = $employee->{"id"};
  my $role_list = get_role_list($instance_map, $employee_id);
  # get all ledger accounts.
  my (%ledger_account_by_id);
  $sql_query = "select * from ledger_account_tbl where chart_ns = ?";
  get_row_by_id_map($dbh, $sql_query, ["oldglobal"], \%ledger_account_by_id);
  # populate information for display of page.
  my $ledger_account_tree = sorted_tree_from_id_map(\%ledger_account_by_id, "name");
  my $sorted_ledger_account_list = flatten_sorted_tree($ledger_account_tree);
  # determine current timestamp.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <employee-list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_first_name = HTML::Entities::encode($_employee->{"first_name"});
    my $_last_name = HTML::Entities::encode($_employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <first_name>$_first_name</first_name>
      <last_name>$_last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <current-date>
    <year>$year</year>
    <month>$month</month>
    <day>$day</day>
  </current-date>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <ledger_account_list>
---END_BLOCK---;
  for my $node (@$sorted_ledger_account_list) {
    my $ledger_account = $node->{"value"};
    my $depth = $node->{"depth"};
    my $ledger_account_id = $ledger_account->{"id"};
    my $ledger_account_label = "."x(($depth-1)*4) . HTML::Entities::encode($ledger_account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <ledger_account>
      <id>$ledger_account_id</id>
      <label>$ledger_account_label</label>
    </ledger_account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ledger_account_list>
  <expense/>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_expense_input.xslt", $cgi);
}


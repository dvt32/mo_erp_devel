<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: New Expense
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/show_expenses.cgi">Recent Expenses</a>
	  <xsl:if test="role-list/role = 'role:finance/view_all_expenses'">
	    |
	    <a href="/1g/finance/reconcile_expenses_input.cgi">Reconcile Expenses</a>
	  </xsl:if>
        </p>
	<xsl:apply-templates select="expense"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="expense">
    <table>
      <tr><td>
	<form action="/1g/finance/new_expense_result.cgi" method="post" enctype="multipart/form-data">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Expense
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <xsl:if test="/page/role-list/role = 'role:finance/view_all_expenses'">
		<tr>
		  <td style="padding: 4px; ">
		    Employee:
		  </td>
		  <td>
		    <select name="employee_id">
		      <xsl:for-each select="/page/employee-list/employee">
			<option>
			  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			  <xsl:value-of select="last_name"/>
			  <xsl:text>, </xsl:text>
			  <xsl:value-of select="first_name"/>
			</option>
		      </xsl:for-each>
		    </select>
		  </td>
		</tr>
	      </xsl:if>
	      <tr>
		<td style="padding: 4px; ">
		  Item Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">item_name</xsl:attribute>
		  </input>
		  (what did you buy?)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Vendor Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">vendor_name</xsl:attribute>
		  </input>
		  (where did you buy it?)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Purchase Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>month</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/month-list/month">
		      <option>
			<xsl:if test="/page/current-date/month = number">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select name="day">
		    <xsl:attribute name="name">
		      <xsl:text>day</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/day-list/day">
		      <option>
			<xsl:if test="/page/current-date/day = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">
		      <xsl:text>year</xsl:text>
		    </xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="/page/current-date/year"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Ledger Account:
		</td>
		<td>
		  <select name="ledger_account_id">
		    <xsl:for-each select="/page/ledger_account_list/ledger_account">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Location(s):
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">locations</xsl:attribute>
		  </input>
		  (buildings this purchase benefits: can be all or one or multiple)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Amount:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">amount</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Payer:
		</td>
		<td>
		  <select name="payer">
		    <option>company</option>
		    <option>personal</option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Payment Method:
		</td>
		<td>
		  <select name="payment_method">
		    <option>bill</option>
		    <option>cash</option>
		    <option>check</option>
		    <option>American Express 72001</option>
		    <option>BLS Debit Card 0491</option>
			<option>Capital One Visa 1863 WC</option>
			<option>Chase MC 6266 DR</option>
			<option>Chase MC 8297 DR</option>
			<option>Chase Visa 6322 WC</option>
			<option>CitiBusiness Visa 3522 DR</option>
			<option>Discover 9624 WC</option>
			<option>Wells Fargo - 4312 ML</option>
			<option>Wells Fargo - 2823 WC</option>
			<option>Wells Fargo - 4444 SB</option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Payment Identifier:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">payment_identifier</xsl:attribute>
		  </input>
		  (card number or check number)
		</td>
	</tr>
	<tr>
		<xsl:if test="role-list/role = 'role:finance/new_expense_book'">
		<td style="padding: 4px; ">
		  Book:
		</td>
		<td>
		  <select name="book">
		  	<option>None</option>
		  	<option>MOM</option> 
		  	<option>BLS</option> 
		  	<option>Icarus</option>
		  	<option>405 W Sup</option> 
		  	<option>18 S Mich</option>  
		  	<option>Stella</option>
		  </select>
		</td>
		</xsl:if>
	</tr>
	<tr>
		<xsl:if test="role-list/role = 'role:finance/new_expense_class'">
		<td style="padding: 4px; ">
		  Class:
		</td>
		<td>
		  <select name="class">
		  	<option>None</option>
		    <option>18 S Mich</option>
			<option>Basket</option>
			<option>0#MOM</option>
			<option>7#GCS</option>
			<option>10#CoSpc</option>
			<option>2#MOS2</option>
			<option>3#US902S</option>
			<option>4#MOS4</option>
			<option>405 W Sup</option>
			<option>Boston Link Space</option>
			<option>Icarus</option>
			<option>IL</option>
			<option>11#MPS</option>
			<option>13#CoRiverN</option>
			<option>NY</option>
			<option>NY1#MOS1</option>
			<option>NY2#MOS2</option>
			<option>NY3#US902S</option>
			<option>NY4#MOS4</option>
			<option>NY5#1601Bwy</option>
			<option>NY8#HVS</option>
		  </select>
		</td>
		</xsl:if>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Entered into QB by:
		</td>
		<td>
		  <select name="entered_by">
		  	<option>None</option>
		    <option>Aqeel</option>
		    <option>Raziya</option>
		    <option>Jerome</option>
		    <option>Jin</option>
		    <option>Joanna</option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Receipt:
		</td>
		<td>
		  <input type="file" name="receipt_file"/>
		  (upload a PDF file from scan, web page, or email)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

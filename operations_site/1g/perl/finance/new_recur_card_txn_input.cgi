#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_month_name;
use get_row_by_id_map;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/receive_payment/card");
  # load accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # load persons.
  my (%cust_pers_by_id);
  $sql_query = "select * from cust_pers_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment/card");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # populate information needed for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <account-list>
---END_BLOCK---;
  for my $_cust_acct_id (@cust_acct_id_list) {
    my $active_customer = $cust_acct_by_id{$_cust_acct_id};
    my $name = HTML::Entities::encode($active_customer->{"name"});
    my $primary_contact_cust_pers_id = $active_customer->{"primary_contact_cust_pers_id"};
    my $primary_contact = $cust_pers_by_id{$primary_contact_cust_pers_id};
    my $primary_contact_name = ($primary_contact->{"first_name"} || "") . " " . ($primary_contact->{"last_name"} || ""); 
    $primary_contact_name = HTML::Entities::encode($primary_contact_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$_cust_acct_id</id>
      <name>$name</name>
      <primary-contact>$primary_contact_name</primary-contact>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <employee-list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $employee = $employee_by_id{$_employee_id};
    my $first_name = HTML::Entities::encode($employee->{"first_name"});
    my $last_name = HTML::Entities::encode($employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <process_mode_list>
    <process_mode>automatic</process_mode>
    <process_mode>manual</process_mode>
  </process_mode_list>
  <frequency_list>
    <frequency>never</frequency>
    <frequency>monthly</frequency>
  </frequency_list>
  <recur_card_txn/>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_recur_card_txn_input.xslt", $cgi);
}

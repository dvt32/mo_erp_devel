#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch all payor companies to generate drop down list.
  my (%company_by_id, @company_id_list);
  my $sql_query = "select * from company_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%company_by_id, \@company_id_list);
  # fetch all corporate bank accounts to generate drop down list.
  my (%corp_bank_account_by_id, @corp_bank_account_id_list);
  $sql_query = "select * from corp_bank_account_vw where is_active order by short_name, nickname";
  get_row_by_id_map($dbh, $sql_query, undef, \%corp_bank_account_by_id, \@corp_bank_account_id_list);
  # fetch all vendor vaults to generate drop down list.
  my (%vend_vault_by_id, @vend_vault_id_list);
  $sql_query = "select * from vend_vault_vw where is_active order by name, routing_number, account_number";
  get_row_by_id_map($dbh, $sql_query, undef, \%vend_vault_by_id, \@vend_vault_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <company_list>
---END_BLOCK---;
  for my $company_id (@company_id_list) {
    my $company = $company_by_id{$company_id};
    my $company_long_name = HTML::Entities::encode($company->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <company>
      <id>$company_id</id>
      <name>$company_long_name</name>
    </company>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </company_list>
  <corp_bank_account_list>
---END_BLOCK---;
  for my $corp_bank_account_id (@corp_bank_account_id_list) {
    my $corp_bank_account = $corp_bank_account_by_id{$corp_bank_account_id};
    my $short_name = HTML::Entities::encode($corp_bank_account->{"short_name"});
    my $company_long_name = HTML::Entities::encode($corp_bank_account->{"company_long_name"});
    my $nickname = HTML::Entities::encode($corp_bank_account->{"nickname"});
    my $note = HTML::Entities::encode($corp_bank_account->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <corp_bank_account>
      <id>$corp_bank_account_id</id>
      <short_name>$short_name</short_name>
      <company_long_name>$company_long_name</company_long_name>
      <nickname>$nickname</nickname>
      <account_type>$corp_bank_account->{"account_type"}</account_type>
      <routing_number>$corp_bank_account->{"routing_number"}</routing_number>
      <account_number>$corp_bank_account->{"account_number"}</account_number>
      <note>$note</note>
    </corp_bank_account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </corp_bank_account_list>
  <vend_vault_list>
---END_BLOCK---;
  for my $vend_vault_id (@vend_vault_id_list) {
    my $vend_vault = $vend_vault_by_id{$vend_vault_id};
    my $name = HTML::Entities::encode($vend_vault->{"name"} || "");
    my $registered_name = HTML::Entities::encode($vend_vault->{"registered_name"} || "");
    my $note = HTML::Entities::encode($vend_vault->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <vend_vault>
      <id>$vend_vault_id</id>
      <name>$name</name>
      <account_type>$vend_vault->{"account_type"}</account_type>
      <owned_by>$vend_vault->{"owned_by"}</owned_by>
      <registered_name>$registered_name</registered_name>
      <routing_number>$vend_vault->{"routing_number"}</routing_number>
      <account_number>$vend_vault->{"account_number"}</account_number>
      <note>$note</note>
    </vend_vault>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </vend_vault_list>
  <vend_pmt/>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_vend_ach_pmt_input.xslt", $cgi);
}


<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: New Vendor ACH Payment
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
        </p>
	<xsl:apply-templates select="vend_pmt"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="vend_pmt">
    <table>
      <tr><td>
	<form action="/1g/finance/new_vend_ach_pmt_result.cgi" method="post" enctype="multipart/form-data">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Vendor Payment
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Payor Company:
		</td>
		<td>
		  <select name="company_id">
		    <xsl:for-each select="/page/company_list/company">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Corp Bank Account:
		</td>
		<td>
		  <select name="corp_bank_account_id">
		    <xsl:for-each select="/page/corp_bank_account_list/corp_bank_account">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="short_name"/>
			<xsl:text>: </xsl:text>
			<xsl:value-of select="nickname"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Vendor Bank Account:
		</td>
		<td>
		  <select name="vend_vault_id">
		    <xsl:for-each select="/page/vend_vault_list/vend_vault">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
			<xsl:text> - </xsl:text>
			<xsl:value-of select="owned_by"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="account_type"/>
			<xsl:text> - routing </xsl:text>
			<xsl:value-of select="routing_number"/>
			<xsl:text> - account </xsl:text>
			<xsl:value-of select="account_number"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Amount:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">amount</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Scheduled Date:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">scheduled_date</xsl:attribute>
		  </input>
		  (enter in YYYY-MM-DD format)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Payee Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">payee_name</xsl:attribute>
		  </input>
		  (leave this blank if payee name is already correct in vendor bank account)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="maxlength">80</xsl:attribute>
		  </input>
		  (what is the purchase for? enter invoice number, building and floor, and purpose/work done)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

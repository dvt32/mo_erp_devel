#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch all payor companies to generate drop down list.
  my (%company_by_id, @company_id_list);
  my $sql_query = "select * from company_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%company_by_id, \@company_id_list);
  # fetch all corporate bank accounts to generate drop down list.
  my (%corp_bank_account_by_id, @corp_bank_account_id_list);
  $sql_query = "select * from corp_bank_account_vw where is_active order by short_name, nickname";
  get_row_by_id_map($dbh, $sql_query, undef, \%corp_bank_account_by_id, \@corp_bank_account_id_list);
  # fetch all vendors to generate drop down list.
  my (%vendor_by_id, @vendor_id_list);
  $sql_query = "select * from vendor_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%vendor_by_id, \@vendor_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <company_list>
---END_BLOCK---;
  for my $company_id (@company_id_list) {
    my $company = $company_by_id{$company_id};
    my $company_long_name = HTML::Entities::encode($company->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <company>
      <id>$company_id</id>
      <name>$company_long_name</name>
    </company>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </company_list>
  <corp_bank_account_list>
---END_BLOCK---;
  for my $corp_bank_account_id (@corp_bank_account_id_list) {
    my $corp_bank_account = $corp_bank_account_by_id{$corp_bank_account_id};
    my $short_name = HTML::Entities::encode($corp_bank_account->{"short_name"});
    my $company_long_name = HTML::Entities::encode($corp_bank_account->{"company_long_name"});
    my $nickname = HTML::Entities::encode($corp_bank_account->{"nickname"});
    my $note = HTML::Entities::encode($corp_bank_account->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <corp_bank_account>
      <id>$corp_bank_account_id</id>
      <short_name>$short_name</short_name>
      <company_long_name>$company_long_name</company_long_name>
      <nickname>$nickname</nickname>
      <account_type>$corp_bank_account->{"account_type"}</account_type>
      <routing_number>$corp_bank_account->{"routing_number"}</routing_number>
      <account_number>$corp_bank_account->{"account_number"}</account_number>
      <note>$note</note>
    </corp_bank_account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </corp_bank_account_list>
  <vendor_list>
---END_BLOCK---;
  for my $vendor_id (@vendor_id_list) {
    my $vendor = $vendor_by_id{$vendor_id};
    my $name = HTML::Entities::encode($vendor->{"name"} || "");
    my $company_name = HTML::Entities::encode($vendor->{"company_name"} || "");
    my $email = HTML::Entities::encode($vendor->{"email"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <vendor>
      <id>$vendor_id</id>
      <name>$name</name>
      <company_name>$company_name</company_name>
      <email>$email</email>
    </vendor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </vendor_list>
  <vend_pmt/>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_vend_check_pmt_input.xslt", $cgi);
}


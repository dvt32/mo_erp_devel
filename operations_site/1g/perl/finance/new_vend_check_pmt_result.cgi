#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use decrypt_row;
use select_row;
use insert_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $company_id = $cgi->param("company_id");
  my $company;
  if (!($company = select_row($dbh, "company_tbl", $company_id))) {
    die "show_page: edit_vend_check_pmt_bad_company.html\n";
  }
  my $corp_bank_account_id = $cgi->param("corp_bank_account_id");
  my $corp_bank_account;
  if (!($corp_bank_account = select_row($dbh, "corp_bank_account_tbl", $corp_bank_account_id))) {
    die "show_page: edit_vend_check_pmt_bad_corp_bank_account.html\n";
  }
  if ($company_id != $corp_bank_account->{"company_id"}) {
    die "show_page: edit_vend_check_pmt_company_bank_account_mismatch.html\n";
  }
  my $vendor_id = $cgi->param("vendor_id");
  my $vendor;
  if (!($vendor = select_row($dbh, "vendor_tbl", $vendor_id))) {
    die "show_page: edit_vend_check_pmt_bad_vendor.html\n";
  }
  my $amount = Encode::decode($cgi_input_charset, $cgi->param("amount"));
  if ($amount !~ /^\d+(\.\d\d)?$/) {
    die "show_page: edit_vend_check_pmt_bad_amount.html\n";
  }
  my $check_date = Encode::decode($cgi_input_charset, $cgi->param("check_date"));
  if ($check_date !~ /^(\d{4}-\d\d-\d\d)$/) {
    die "show_page: edit_vend_check_pmt_bad_check_date.html\n";
  }
  my $payee_name = Encode::decode($cgi_input_charset, $cgi->param("payee_name")) || $vendor->{"name"};
  if (length($payee_name) > 32) {
    die "show_page: edit_vend_check_pmt_name_too_long.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  if (length($note) > 80) {
    die "show_page: edit_vend_check_pmt_note_too_long.html\n";
  }
  $dbh->begin_work();
  my $decrypt_corp_bank_account = decrypt_row("corp_bank_account_tbl", $corp_bank_account_id, $employee->{"id"});
  my ($vendor_email, $vendor_cc_email, $vendor_email_list);
  $vendor_email = $vendor->{"email"};
  $vendor_cc_email = $vendor->{"cc_email"};
  # email_array is a list of emails
  my @email_array = ();
  if (defined($vendor_email)) {
    push(@email_array, $vendor_email);
  }
  if (defined($vendor_cc_email) && $vendor_cc_email) {
    push(@email_array,split(/,/,$vendor_cc_email));
  }
  # remove duplicates (but preserve order)
  if (@email_array) {
    my $i = 0;
    # Map overwrites values for each subsequent key, we want to preserve the first appearance
    # reverse the array when mapping, then sort in reverse order
    my %hash = map { $_, $i++ } reverse(@email_array);
    @email_array = sort { $hash{$b} <=> $hash{$a} } keys %hash;
    $vendor_email_list = join(",",@email_array);
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["payor_company_id", $company_id]);
  push(@sql_column_list, ["source_routing_number", $corp_bank_account->{"routing_number"}]);
  push(@sql_column_list, ["source_account_number", $decrypt_corp_bank_account->{"account_number"}]);
  push(@sql_column_list, ["payee_name", Encode::encode($db_charset, $payee_name)]);
  push(@sql_column_list, ["payee_table_column", "vendor_tbl.id"]);
  push(@sql_column_list, ["payee_fk_id", $vendor->{"id"}, SQL_INTEGER]);
  push(@sql_column_list, ["amount", $amount]);
  push(@sql_column_list, ["check_date", $check_date]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  push(@sql_column_list, ["requester_id", $employee->{"id"}, SQL_INTEGER]);
  push(@sql_column_list, ["email", Encode::encode($db_charset, $vendor_email_list || "") || undef]);
  my $check_paid_id = insert_row($dbh, "check_paid_tbl", \@sql_column_list, "check_paid_seq");
  $dbh->commit();
  # populate information for display of page.
  my $payor_company_name = HTML::Entities::encode($company->{"name"});
  $payee_name = HTML::Entities::encode($payee_name || "");
  $note = HTML::Entities::encode($note || "");
  my $email = HTML::Entities::encode($vendor_email_list || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <check_paid>
    <id>$check_paid_id</id>
    <payor_company_id>$company_id</payor_company_id>
    <payor_company_name>$payor_company_name</payor_company_name>
    <source_routing_number>$corp_bank_account->{"routing_number"}</source_routing_number>
    <source_account_number>$corp_bank_account->{"account_number"}</source_account_number>
    <payee_name>$payee_name</payee_name>
    <amount>$amount</amount>
    <check_date>$check_date</check_date>
    <note>$note</note>
    <email>$email</email>
  </check_paid>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_vend_check_pmt_result.xslt", $cgi);
}

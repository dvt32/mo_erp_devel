#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use get_row_list;
use insert_row;
use enqueue_email;
use components_to_timestamp;
use strip_outside_whitespace;
use parse_dollars_cents;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/receive_payment/ach");
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my ($month, $day, $year, $hour, $minute) = map {$cgi->param($_)} (qw(month day year hour minute));
  if (($month < 1) || ($month > 12) || ($day < 1) || ($day > 31) || ($year < 2007) || ($year > 3000) ||
      ($hour < 0) || ($hour > 23) || ($minute < 0) || ($minute > 59)) {
    die "show_page: receive_ach_bad_time.html\n";
  }
  my $recv_dt = components_to_timestamp($year, $month, $day, $hour, $minute, 0);
  my $recv_employee_id = $cgi->param("employee_id");
  my $recv_employee;
  if ((!$recv_employee_id) || (!($recv_employee = select_row($dbh, "employee_tbl", $recv_employee_id)))) {
    die "show_page: receive_ach_bad_employee.html\n";
  }
  my $cust_acct_id = $cgi->param("cust_acct_id");
  my $cust_acct;
  if ((!$cust_acct_id) || (!($cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id)))) {
    die "show_page: receive_ach_bad_account.html\n";
  }
  my $process_month = $cgi->param("process_month");
  my $process_day = $cgi->param("process_day");
  my $process_year = $cgi->param("process_year");
  my $process_hour = $cgi->param("process_hour");
  my $process_minute = $cgi->param("process_minute");
  if ((!$process_month) || (!$process_day) || (!$process_year) ||
      ($process_month < 1) || ($process_month > 12) || ($process_day < 1) || ($process_day > 31) ||
      ($process_year < 2000) || ($process_year > 3000) || ($process_hour < 0) || ($process_hour > 23) ||
      ($process_minute < 0) || ($process_minute > 59)) {
    die "show_page: receive_ach_bad_process_txn_after_dt.html\n";
  }
  my $process_txn_after_dt = components_to_timestamp($process_year, $process_month, $process_day, $process_hour, $process_minute, 0);
  my $amount = $cgi->param("amount") || "";
  eval {
    my ($amount_dollars, $amount_cents) = parse_dollars_cents($amount);
    $amount = "$amount_dollars.$amount_cents";
  };
  if ($@) {
    die "show_page: receive_ach_bad_amount.html\n";
  }
  my $bank_account_name = Encode::decode($cgi_input_charset, $cgi->param("bank_account_name"));
  if (!$bank_account_name) {
    die "show_page: receive_ach_missing_bank_account_name.html\n";
  }
  my $bank_account_type = $cgi->param("bank_account_type");
  if ((!$bank_account_type) || ($bank_account_type !~ /^checking|savings$/)) {
    die "show_page: receive_ach_bad_bank_account_type.html\n";
  }
  my $bank_routing_number = $cgi->param("bank_routing_number");
  if ((!$bank_routing_number) || ($bank_routing_number !~ /^\d{9}$/)) {
    die "show_page: receive_ach_bad_bank_routing_number.html\n";
  }
  my $bank_account_number = $cgi->param("bank_account_number");
  if ((!$bank_account_number) || ($bank_account_number !~ /^\d{1,17}$/)) {
    die "show_page: receive_ach_bad_bank_account_number.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  my $client_hostname = $cgi->remote_host() || undef;
  my $client_ip_addr = $cgi->remote_addr();
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["employee_id", $recv_employee_id, SQL_INTEGER]);
  push(@sql_column_list, ["amount", $amount, SQL_DECIMAL]);
  push(@sql_column_list, ["recv_dt", $recv_dt]);
  push(@sql_column_list, ["payment_method", "ach"]);
  push(@sql_column_list, ["process_txn_after_dt", $process_txn_after_dt]);
  push(@sql_column_list, ["process_mode", "manual"]);
  push(@sql_column_list, ["process_status", "pending"]);
  push(@sql_column_list, ["client_hostname", $client_hostname]);
  push(@sql_column_list, ["client_ip_addr", $client_ip_addr]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  push(@sql_column_list, ["bank_account_name", Encode::encode($db_charset, $bank_account_name)]);
  push(@sql_column_list, ["bank_account_type", $bank_account_type]);
  push(@sql_column_list, ["bank_routing_number", $bank_routing_number]);
  push(@sql_column_list, ["bank_account_number", $bank_account_number]);
  insert_row($dbh, "recv_payment_tbl", \@sql_column_list);
    # insert new row in electronic_check_tbl.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["amount", $amount, SQL_DECIMAL]);
  push(@sql_column_list, ["debit_date", $process_txn_after_dt]);
  push(@sql_column_list, ["remote_host", $client_hostname]);
  push(@sql_column_list, ["remote_addr", $client_ip_addr]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $note)]);
  push(@sql_column_list, ["bank_account_name", Encode::encode($db_charset, $bank_account_name)]);
  push(@sql_column_list, ["bank_account_type", $bank_account_type]);
  push(@sql_column_list, ["routing_number", $bank_routing_number]);
  push(@sql_column_list, ["account_number", $bank_account_number]);
  insert_row($dbh, "electronic_check_tbl", \@sql_column_list);
  # get name to display instead of id
  my $account_name = $cust_acct->{"name"};
  my $obscured_routing_number = "*"x(length($bank_routing_number) - 3) . substr($bank_routing_number, length($bank_routing_number) - 3);
  my $obscured_account_number = "*"x(length($bank_account_number) - 3) . substr($bank_account_number, length($bank_account_number) - 3);
  # enqueue the receipt email.
  my $receipt_email_message = <<"---END_BLOCK---;";
$account_name (ID #$cust_acct_id) has paid \$$amount via ACH debit and provided account information to $recv_employee->{"first_name"} $recv_employee->{"last_name"}. The routing number is $obscured_routing_number. The account number is $obscured_account_number. The ACH debit must be manually processed on or after $process_txn_after_dt.
---END_BLOCK---;
  if ($note) {
    $receipt_email_message .= <<"---END_BLOCK---;";
Additional operator note: $note
---END_BLOCK---;
  }
  enqueue_email
    ("dbh" => $dbh,
     "account_id" => $cust_acct_id,
     "employee_id" => $recv_employee_id,
     "from_hdr" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
     "to_hdr" => "accounts_receivable\@microoffice.com",
     "subject_hdr" => "Payment of \$$amount from $account_name (ID #$cust_acct_id) by ACH debit",
     "message_body" => $receipt_email_message,
    );
  # populate information needed for display of page.
  $account_name = HTML::Entities::encode($account_name);
  my $first_name = HTML::Entities::encode($recv_employee->{"first_name"});
  my $last_name = HTML::Entities::encode($recv_employee->{"last_name"});
  $note = HTML::Entities::encode($note || "");
  $bank_account_name = HTML::Entities::encode($bank_account_name);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <receive-payment>
    <cust_acct_id>$cust_acct_id</cust_acct_id>
    <account_name>$account_name</account_name>
    <amount>$amount</amount>
    <employee>
      <id>$recv_employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
    <note>$note</note>
    <recv_dt>$recv_dt</recv_dt>
    <process_txn_after_dt>$process_txn_after_dt</process_txn_after_dt>
    <bank_account_name>$bank_account_name</bank_account_name>
    <bank_account_type>$bank_account_type</bank_account_type>
    <bank_routing_number>$obscured_routing_number</bank_routing_number>
    <bank_account_number>$obscured_account_number</bank_account_number>
  </receive-payment>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "receive_ach_result.xslt", $cgi);
}

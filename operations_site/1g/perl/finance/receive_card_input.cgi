#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use lead_pad;
use get_month_name;
use get_row_by_id_map;
use get_row_list;
use get_datetime_now;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/receive_payment/card");
  # load accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # load persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  $sql_query = "select * from cust_pers_tbl where is_active order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id, \@cust_pers_id_list);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment/card");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # determine current timestamp.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  # populate information needed for display of page.
  my $employee_email = HTML::Entities::encode($employee->{"email"});
  my ($cust_acct_id, $recv_payment);
  if (my $credit_card_id = $cgi->param("credit_card_id")) {
    my $credit_card;
    if (!($credit_card = select_row($dbh, "credit_card_tbl", $credit_card_id))) {
      die "show_page: receive_card_bad_credit_card_id.html\n";
    }
    $cust_acct_id = $credit_card->{"account_id"};
    $recv_payment =
      {
       "card_number" => $credit_card->{"card_number"},
       "card_expiration_month" => $credit_card->{"card_expiration_month"},
       "card_expiration_year" => $credit_card->{"card_expiration_year"},
       "card_security_code" => $credit_card->{"card_security_code"},
       "card_bill_name" => $credit_card->{"billing_name"},
       "card_bill_company" => $credit_card->{"billing_company"},
       "card_bill_address_1" => $credit_card->{"billing_address_1"},
       "card_bill_address_2" => $credit_card->{"billing_address_2"},
       "card_bill_city" => $credit_card->{"billing_city"},
       "card_bill_state" => $credit_card->{"billing_state"},
       "card_bill_zip_code" => $credit_card->{"billing_zip_code"},
       "card_bill_address_num" => $credit_card->{"billing_address_num"},
       "card_bill_country" => $credit_card->{"billing_country"},
       "card_bill_phone_number" => $credit_card->{"billing_phone_number"},
       "card_bill_fax_number" => $credit_card->{"billing_fax_number"},
       "card_bill_email_address" => $credit_card->{"billing_email_address"},
      };
  } elsif (my $credit_card_txn_id = $cgi->param("credit_card_txn_id")) {
    my $credit_card_txn;
    if (!($credit_card_txn = select_row($dbh, "credit_card_txn_tbl", $credit_card_txn_id))) {
      die "show_page: receive_card_bad_credit_card_txn_id.html\n";
    }
    $cust_acct_id = $credit_card_txn->{"cust_acct_id"};
    $recv_payment =
      {
       "card_number" => $credit_card_txn->{"card_number"},
       "card_expiration_month" => $credit_card_txn->{"card_expiration_month"},
       "card_expiration_year" => $credit_card_txn->{"card_expiration_year"},
       "card_security_code" => $credit_card_txn->{"card_security_code"},
       "card_bill_name" => $credit_card_txn->{"billing_name"},
       "card_bill_company" => $credit_card_txn->{"billing_company"},
       "card_bill_address_1" => $credit_card_txn->{"billing_address_1"},
       "card_bill_address_2" => $credit_card_txn->{"billing_address_2"},
       "card_bill_city" => $credit_card_txn->{"billing_city"},
       "card_bill_state" => $credit_card_txn->{"billing_state"},
       "card_bill_zip_code" => $credit_card_txn->{"billing_zip_code"},
       "card_bill_address_num" => $credit_card_txn->{"billing_address_num"},
       "card_bill_country" => $credit_card_txn->{"billing_country"},
       "card_bill_phone_number" => $credit_card_txn->{"billing_phone_number"},
       "card_bill_fax_number" => $credit_card_txn->{"billing_fax_number"},
       "card_bill_email_address" => $credit_card_txn->{"billing_email_address"},
      };
  } else {
    $cust_acct_id = undef;
    $recv_payment =
      {
       "card_number" => "",
       "card_expiration_month" => "",
       "card_expiration_year" => "",
       "card_security_code" => "",
       "card_bill_name" => "",
       "card_bill_company" => "",
       "card_bill_address_1" => "",
       "card_bill_address_2" => "",
       "card_bill_city" => "",
       "card_bill_state" => "",
       "card_bill_zip_code" => "",
       "card_bill_address_num" => "",
       "card_bill_country" => "",
       "card_bill_phone_number" => "",
       "card_bill_fax_number" => "",
       "card_bill_email_address" => "",
      };
  }
  $cust_acct_id ||= "";
  my $card_number = HTML::Entities::encode($recv_payment->{"card_number"});
  my $card_expiration_month = HTML::Entities::encode($recv_payment->{"card_expiration_month"});
  my $card_expiration_year = HTML::Entities::encode($recv_payment->{"card_expiration_year"});
  my $card_security_code = HTML::Entities::encode($recv_payment->{"card_security_code"} || "");
  my $card_bill_name = HTML::Entities::encode($recv_payment->{"card_bill_name"});
  my $card_bill_company = HTML::Entities::encode($recv_payment->{"card_bill_company"} || "");
  my $card_bill_address_num = HTML::Entities::encode($recv_payment->{"card_bill_address_num"} || "");
  my $card_bill_zip_code = HTML::Entities::encode($recv_payment->{"card_bill_zip_code"});
  my $card_bill_address_1 = HTML::Entities::encode($recv_payment->{"card_bill_address_1"});
  my $card_bill_address_2 = HTML::Entities::encode($recv_payment->{"card_bill_address_2"} || "");
  my $card_bill_city = HTML::Entities::encode($recv_payment->{"card_bill_city"});
  my $card_bill_state = HTML::Entities::encode($recv_payment->{"card_bill_state"});
  my $card_bill_state_other = "";
  if (!grep {$card_bill_state eq $_->{"name"}} @state_list) {
    $card_bill_state_other = $card_bill_state;
    $card_bill_state = "";
  }
  $card_bill_state = HTML::Entities::encode($card_bill_state);
  $card_bill_state_other = HTML::Entities::encode($card_bill_state_other);
  my $card_bill_country = HTML::Entities::encode($recv_payment->{"card_bill_country"});
  my $card_bill_phone_number = HTML::Entities::encode($recv_payment->{"card_bill_phone_number"} || "");
  my $card_bill_fax_number = HTML::Entities::encode($recv_payment->{"card_bill_fax_number"} || "");
  my $card_bill_email_address = HTML::Entities::encode($recv_payment->{"card_bill_email_address"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $active_customer = $cust_acct_by_id{$cust_acct_id};
    my $name = HTML::Entities::encode($active_customer->{"name"});
    my $primary_contact_cust_pers_id = $active_customer->{"primary_contact_cust_pers_id"};
    my $primary_contact = $cust_pers_by_id{$primary_contact_cust_pers_id};
    my $primary_contact_name = ($primary_contact->{"first_name"} || "") . " " . ($primary_contact->{"last_name"} || ""); 
    $primary_contact_name = HTML::Entities::encode($primary_contact_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <name>$name</name>
      <primary-contact>$primary_contact_name</primary-contact>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <current-employee>
    <id>$employee->{"id"}</id>
    <email>$employee_email</email>
  </current-employee>
  <employee-list>
---END_BLOCK---;
  for my $employee_id (@employee_id_list) {
    my $employee = $employee_by_id{$employee_id};
    my $first_name = HTML::Entities::encode($employee->{"first_name"});
    my $last_name = HTML::Entities::encode($employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <current-timestamp>
    <year>$year</year>
    <month>$month</month>
    <day>$day</day>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
  </current-timestamp>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <recv-payment>
    <cust_acct_id>$cust_acct_id</cust_acct_id>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <card_bill_name>$card_bill_name</card_bill_name>
    <card_bill_company>$card_bill_company</card_bill_company>
    <card_bill_address_num>$card_bill_address_num</card_bill_address_num>
    <card_bill_zip_code>$card_bill_zip_code</card_bill_zip_code>
    <card_bill_address_1>$card_bill_address_1</card_bill_address_1>
    <card_bill_address_2>$card_bill_address_2</card_bill_address_2>
    <card_bill_city>$card_bill_city</card_bill_city>
    <card_bill_state>$card_bill_state</card_bill_state>
    <card_bill_state_other>$card_bill_state_other</card_bill_state_other>
    <card_bill_country>$card_bill_country</card_bill_country>
    <card_bill_phone_number>$card_bill_phone_number</card_bill_phone_number>
    <card_bill_fax_number>$card_bill_fax_number</card_bill_fax_number>
    <card_bill_email_address>$card_bill_email_address</card_bill_email_address>
    <note></note>
  </recv-payment>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "receive_card_input.xslt", $cgi);
}

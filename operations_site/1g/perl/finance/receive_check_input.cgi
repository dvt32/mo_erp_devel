#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use lead_pad;
use get_month_name;
use get_row_by_id_map;
use get_row_list;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/receive_payment");
  # load accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # load persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  $sql_query = "select * from cust_pers_tbl where is_active order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id, \@cust_pers_id_list);
  # load enumeration of possible payment destinations for cash and check.
  my @enum_payment_destination_list;
  get_row_list($dbh, "select * from enum_payment_destination_tbl order by value", undef, \@enum_payment_destination_list);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # populate information needed for display of page.
  # determine current timestamp.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $active_customer = $cust_acct_by_id{$cust_acct_id};
    my $name = HTML::Entities::encode($active_customer->{"name"});
    my $primary_contact_cust_pers_id = $active_customer->{"primary_contact_cust_pers_id"};
    my $primary_contact = $cust_pers_by_id{$primary_contact_cust_pers_id};
    my $primary_contact_name = ($primary_contact->{"first_name"} || "") . " " . ($primary_contact->{"last_name"} || ""); 
    $primary_contact_name = HTML::Entities::encode($primary_contact_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <name>$name</name>
      <primary-contact>$primary_contact_name</primary-contact>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <current-employee>
    <id>$employee->{"id"}</id>
  </current-employee>
  <employee-list>
---END_BLOCK---;
  for my $employee_id (@employee_id_list) {
    my $employee = $employee_by_id{$employee_id};
    my $first_name = HTML::Entities::encode($employee->{"first_name"});
    my $last_name = HTML::Entities::encode($employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <enum-payment-destination-list>
---END_BLOCK---;
  for my $payment_destination (@enum_payment_destination_list) {
    my $_label = HTML::Entities::encode($payment_destination->{"label"});
    my $_value = HTML::Entities::encode($payment_destination->{"value"});
    $generated_xml .= <<"---END_BLOCK---;";
    <payment-destination>
      <label>$_label</label>
      <value>$_value</value>
    </payment-destination>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-payment-destination-list>
  <current-timestamp>
    <year>$year</year>
    <month>$month</month>
    <day>$day</day>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
  </current-timestamp>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "receive_check_input.xslt", $cgi);
}

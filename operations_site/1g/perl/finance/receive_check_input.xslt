<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Operations Center :: Receive Check
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  Receive One-Time Payment:
	  <a href="/1g/finance/receive_cash_input.cgi">Cash</a>
	  &middot;
	  <a href="/1g/finance/receive_check_input.cgi">Check</a>
	  &middot;
	  <a href="/1g/finance/receive_card_input.cgi">Credit/Debit Card</a>
	  &middot;
	  <a href="/1g/finance/receive_ach_input.cgi">ACH Debit</a>
        </p>
        <form action="/1g/finance/receive_check_result.cgi" method="post">
          <table cellspacing="0" cellpadding="10px" border="0" class="standard_table" style="white-space: nowrap;">
            <tr>
              <td class="standard_table_title" colspan="2">
                <xsl:text>Receive Check</xsl:text>
              </td>
            </tr>
	    <tr>
	      <td valign="top" colspan="2">
		<span class="required_field">Time Check was Received</span>:<br />
		<select>
		  <xsl:attribute name="name">
		    <xsl:text>month</xsl:text>
		  </xsl:attribute>
		  <xsl:for-each select="/page/month-list/month">
		    <option>
		      <xsl:if test="/page/current-timestamp/month = number">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		      <xsl:value-of select="name"/>
		    </option>
		  </xsl:for-each>
		</select>
		<select name="day">
		  <xsl:attribute name="name">
		    <xsl:text>day</xsl:text>
		  </xsl:attribute>
		  <xsl:for-each select="/page/day-list/day">
		    <option>
		      <xsl:if test="/page/current-timestamp/day = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">
		    <xsl:text>year</xsl:text>
		  </xsl:attribute>
		  <xsl:attribute name="size">4</xsl:attribute>
		  <xsl:attribute name="value"><xsl:value-of select="/page/current-timestamp/year"/></xsl:attribute>
		</input>
		<xsl:text> at </xsl:text>
		<select>
		  <xsl:attribute name="name">
		    <xsl:text>hour</xsl:text>
		  </xsl:attribute>
		  <xsl:for-each select="/page/hour-list/hour">
		    <option>
		      <xsl:if test="/page/current-timestamp/hour = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<xsl:text>:</xsl:text>
		<select name="minute">
		  <xsl:attribute name="name">
		    <xsl:text>minute</xsl:text>
		  </xsl:attribute>
		  <xsl:for-each select="/page/minute-list/minute">
		    <option>
		      <xsl:if test="/page/current-timestamp/minute = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
            <tr>
              <td valign="top" colspan="2">
		<span class="required_field">Account</span>:<br />
                <select>
                  <xsl:attribute name="name">
                    <xsl:text>cust_acct_id</xsl:text>
                  </xsl:attribute>
		  <option value="">[---]</option>
                  <xsl:for-each select="/page/account-list/account">
                    <option>
                      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
                      <xsl:value-of select="name"/><xsl:text>, </xsl:text><xsl:value-of select="primary-contact"/>
                    </option>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
            <tr>
              <td valign="top">
		<span class="required_field">Date Written on Check</span>:<br />
                <select>
                  <xsl:attribute name="name">
                    <xsl:text>check_month</xsl:text>
                  </xsl:attribute>
                  <xsl:for-each select="/page/month-list/month">
                    <option>
                      <xsl:if test="/page/current-timestamp/month = number">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
                      <xsl:value-of select="name"/>
                    </option>
                  </xsl:for-each>
                </select>
                <select>
                  <xsl:attribute name="name">
                    <xsl:text>check_day</xsl:text>
                  </xsl:attribute>
                  <xsl:for-each select="/page/day-list/day">
                    <option>
                      <xsl:if test="/page/current-timestamp/day = .">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="."/>
                    </option>
                  </xsl:for-each>
                </select>
                <input>
                  <xsl:attribute name="type">text</xsl:attribute>
                  <xsl:attribute name="name">
                    <xsl:text>check_year</xsl:text>
                  </xsl:attribute>
                  <xsl:attribute name="size">4</xsl:attribute>
                  <xsl:attribute name="value"><xsl:value-of select="/page/current-timestamp/year"/></xsl:attribute>
                </input>
              </td>
              <td style="text-align: right; ">
		Check #:<br />
                <input>
                  <xsl:attribute name="type">text</xsl:attribute>
                  <xsl:attribute name="name">check_number</xsl:attribute>
                  <xsl:attribute name="size">4</xsl:attribute>
                </input>
              </td>
            </tr>
            <tr>
              <td>Name on Check<br />
                <input>
                  <xsl:attribute name="type">text</xsl:attribute>
                  <xsl:attribute name="name">check_name</xsl:attribute>
                  <xsl:attribute name="size">25</xsl:attribute>
                </input>
              </td>
              <td style="text-align: right; ">
		<span class="required_field">Amount of Check</span>:<br />$
                <input>
                  <xsl:attribute name="type">text</xsl:attribute>
                  <xsl:attribute name="name">amount</xsl:attribute>
                  <xsl:attribute name="size">6</xsl:attribute>
                </input>
              </td>
            </tr>
            <tr>
              <td valign="top">
		<p>
		  <span class="required_field">Received by</span>:
		  <br />
		  <select name="employee_id">
		    <xsl:for-each select="/page/employee-list/employee">
		      <option>
			<xsl:if test="/page/current-employee/id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="last_name"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="first_name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</p>
		<p>
		  <span class="required_field">Payment Destination</span>:<br />
		  <select name="payment_destination">
		    <xsl:for-each select="/page/enum-payment-destination-list/payment-destination">
		      <option>
			<xsl:if test="value = 'bank'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</p>
              </td>
              <td style="white-space: nowrap; " valign="top">
		<b>Lockbox Address:</b>
		<br />
		Micro Office Management LLC
		<br />
		1375 Broadway, Suite 389
		<br />
		New York, NY 10018
              </td>
            </tr>
            <tr valign="top">
              <td colspan="2">
                Note:<br />
                <textarea>
                  <xsl:attribute name="name">note</xsl:attribute>
                  <xsl:attribute name="rows">4</xsl:attribute>
                  <xsl:attribute name="cols">40</xsl:attribute>
                  <!-- Need this blank note field; otherwise the surrounding textarea gets collapsed into a single tag. -->
		  <xsl:text>.</xsl:text>
                </textarea>
              </td>
            </tr>
            <tr valign="top">
              <td colspan="2">
		<span class="required_field">Required fields</span> are in red.
              </td>
            </tr>
            <tr>
              <td>
                <input type="submit" name="submitButton" value="Save"/>
              </td>
            </tr>
          </table>
        </form>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use get_row_list;
use insert_row;
use enqueue_email;
use components_to_timestamp;
use strip_outside_whitespace;
use parse_dollars_cents;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/receive_payment");
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $cust_acct_id = $cgi->param("cust_acct_id");
  my $cust_acct;
  if ((!$cust_acct_id) || (!($cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id)))) {
    die "show_page: receive_check_bad_account.html\n";
  }
  my $check_month = $cgi->param("check_month");
  my $check_day = $cgi->param("check_day");
  my $check_year = $cgi->param("check_year");
  if ((!$check_month) || (!$check_day) || (!$check_year) ||
      ($check_month < 1) || ($check_month > 12) || ($check_day < 1) || ($check_day > 31) ||
      ($check_year < 2000) || ($check_year > 3000)) {
    die "show_page: receive_check_bad_date_on_check.html\n";
  }
  my $check_date = int($check_year) . "-" . lead_pad(int($check_month), 2) . "-" . lead_pad(int($check_day), 2);
  my $check_number = $cgi->param("check_number");
  my $check_name = Encode::decode($cgi_input_charset, $cgi->param("check_name"));
  my $amount = $cgi->param("amount") || "";
  eval {
    my ($amount_dollars, $amount_cents) = parse_dollars_cents($amount);
    $amount = "$amount_dollars.$amount_cents";
  };
  if ($@) {
    die "show_page: receive_check_bad_amount.html\n";
  }
  my $recv_employee_id = $cgi->param("employee_id");
  my $recv_employee;
  if ((!$recv_employee_id) || (!($recv_employee = select_row($dbh, "employee_tbl", $recv_employee_id)))) {
    die "show_page: receive_check_bad_employee.html\n";
  }
  my ($month, $day, $year, $hour, $minute) = map {$cgi->param($_)} (qw(month day year hour minute));
  if (($month < 1) || ($month > 12) || ($day < 1) || ($day > 31) || ($year < 2007) || ($year > 3000) ||
      ($hour < 0) || ($hour > 23) || ($minute < 0) || ($minute > 59)) {
    die "show_page: receive_check_bad_time.html\n";
  }
  my $payment_destination = $cgi->param("payment_destination");
  # load enumeration of possible payment destinations for cash and check.
  my @enum_payment_destination_list;
  get_row_list($dbh, "select * from enum_payment_destination_tbl order by value", undef, \@enum_payment_destination_list);
  if ((!$payment_destination) || (!grep {$payment_destination eq $_->{"value"}} @enum_payment_destination_list)) {
    die "show_page: receive_check_bad_payment_destination.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  my $recv_dt = components_to_timestamp($year, $month, $day, $hour, $minute, 0);
  my $client_hostname = $cgi->remote_host() || undef;
  my $client_ip_addr = $cgi->remote_addr();
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["employee_id", $recv_employee_id, SQL_INTEGER]);
  push(@sql_column_list, ["amount", $amount, SQL_DECIMAL]);
  push(@sql_column_list, ["recv_dt", $recv_dt]);
  push(@sql_column_list, ["payment_method", "check"]);
  push(@sql_column_list, ["process_txn_after_dt", $check_date]);
  push(@sql_column_list, ["process_status", "complete"]);
  push(@sql_column_list, ["client_hostname", $client_hostname]);
  push(@sql_column_list, ["client_ip_addr", $client_ip_addr]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  push(@sql_column_list, ["payment_destination", $payment_destination]);
  push(@sql_column_list, ["check_name", Encode::encode($db_charset, $check_name)]);
  push(@sql_column_list, ["check_number", $check_number]);
  insert_row($dbh, "recv_payment_tbl", \@sql_column_list);
  # get name to display instead of id
  my $account_name = $cust_acct->{"name"};
  # enqueue the receipt email.
  $check_number ||= "";
  my $receipt_email_message = <<"---END_BLOCK---;";
$account_name (ID #$cust_acct_id) has paid \$$amount via check #$check_number and handed the check to $recv_employee->{"first_name"} $recv_employee->{"last_name"}. The check is dated $check_date. Payment destination: $payment_destination.
---END_BLOCK---;
  if ($note) {
    $receipt_email_message .= <<"---END_BLOCK---;";
Additional operator note: $note
---END_BLOCK---;
  }
  enqueue_email
    ("dbh" => $dbh,
     "account_id" => $cust_acct_id,
     "employee_id" => $recv_employee_id,
     "from_hdr" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
     "to_hdr" => "accounts_receivable\@microoffice.com",
     "subject_hdr" => "Payment of \$$amount from $account_name (ID #$cust_acct_id) by check",
     "message_body" => $receipt_email_message,
    );
  # populate information needed for display of page.
  $account_name = HTML::Entities::encode($account_name);
  my $first_name = HTML::Entities::encode($recv_employee->{"first_name"});
  my $last_name = HTML::Entities::encode($recv_employee->{"last_name"});
  $note = HTML::Entities::encode($note || "");
  $check_name = HTML::Entities::encode($check_name || "");
  $check_number = HTML::Entities::encode($check_number || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <receive-payment>
    <cust_acct_id>$cust_acct_id</cust_acct_id>
    <account_name>$account_name</account_name>
    <amount>$amount</amount>
    <employee>
      <id>$recv_employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </employee>
    <payment_destination>$payment_destination</payment_destination>
    <note>$note</note>
    <recv_dt>$recv_dt</recv_dt>
    <check_date>$check_date</check_date>
    <check_name>$check_name</check_name>
    <check_number>$check_number</check_number>
  </receive-payment>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "receive_check_result.xslt", $cgi);
}

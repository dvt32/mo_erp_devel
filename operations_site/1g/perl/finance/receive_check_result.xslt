<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Operations Center :: Receive Check (success)
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  Receive One-Time Payment:
	  <a href="/1g/finance/receive_cash_input.cgi">Cash</a>
	  &middot;
	  <a href="/1g/finance/receive_check_input.cgi">Check</a>
	  &middot;
	  <a href="/1g/finance/receive_card_input.cgi">Credit/Debit Card</a>
	  &middot;
	  <a href="/1g/finance/receive_ach_input.cgi">ACH Debit</a>
        </p>
        <xsl:apply-templates select="receive-payment"/>
        <p>
          <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="receive-payment">
    <table class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="2">
          Receive Check (success)
        </td>
      </tr>
      <tr><td colspan="2">You've successfully received the following check payment:</td></tr>
      <tr>
	<td><b>Account name: </b></td>
	<td> <xsl:value-of select="account_name"/> (ID #<xsl:value-of select="cust_acct_id"/>)</td>
      </tr>
      <tr>
	<td><b>Date written on check: </b></td>
	<td><xsl:value-of select="check_date"/></td>
      </tr>
      <tr>
	<td><b>Check number: </b></td>
	<td><xsl:value-of select="check_number"/></td>
      </tr>
      <tr>
	<td><b>Name on check: </b></td>
	<td><xsl:value-of select="check_name"/></td>
      </tr>
      <tr>
	<td><b>Amount of check: </b></td>
	<td>$<xsl:value-of select="amount"/></td>
      </tr>
      <tr>
	<td><b>Received by: </b></td>
	<td><xsl:value-of select="employee/first_name"/><xsl:text> </xsl:text><xsl:value-of select="employee/last_name"/></td>
      </tr>
      <tr>
	<td><b>Payment destination: </b></td>
	<td><xsl:value-of select="payment_destination"/></td>
      </tr>
      <xsl:if test="note != ''">
	<tr valign="top">
	  <td><b>Note: </b></td>
	  <td><xsl:value-of select="note"/></td>
	</tr>
      </xsl:if>
      <tr>
	<td><b>Time received: </b></td>
	<td><xsl:value-of select="recv_dt"/></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

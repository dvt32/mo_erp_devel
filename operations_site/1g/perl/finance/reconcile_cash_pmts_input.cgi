#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/reconcile_pmts");
  # fetch all unreconciled cash payments.
  my (%recv_payment_by_id, @recv_payment_id_list);
  my $sql_query = "select * from recv_payment_tbl where payment_method = ? and not(is_reconciled) order by recv_dt desc, cust_acct_id";
  get_row_by_id_map($dbh, $sql_query, ["cash"], \%recv_payment_by_id, \@recv_payment_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch employees.
  my %employee_by_id;
  $sql_query = "select * from employee_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <recv-payment-list>
---END_BLOCK---;
  for my $recv_payment_id (@recv_payment_id_list) {
    my $recv_payment = $recv_payment_by_id{$recv_payment_id};
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $account_name = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
      } else {
	$account_name = "(unknown account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee_name = "";
    if (($employee_id) && (my $employee = $employee_by_id{$employee_id})) {
      $employee_name = HTML::Entities::encode(qq{$employee->{"first_name"} $employee->{"last_name"}});
    }
    my $amount = $recv_payment->{"amount"};
    my $recv_dt = $recv_payment->{"recv_dt"};
    my $note = HTML::Entities::encode($recv_payment->{"note"} || "");
    my $payment_destination = HTML::Entities::encode($recv_payment->{"payment_destination"});
    $generated_xml .= <<"---END_BLOCK---;";
    <recv-payment>
      <id>$recv_payment_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <amount>$amount</amount>
      <recv_dt>$recv_dt</recv_dt>
      <note>$note</note>
      <payment_destination>$payment_destination</payment_destination>
    </recv-payment>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </recv-payment-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reconcile_cash_pmts_input.xslt", $cgi);
}

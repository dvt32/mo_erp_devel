#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;
use update_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/reconcile_pmts");
  # get CGI input parameters.
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @recv_payment_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@recv_payment_id_list, $1);
    }
  }
  # begin database transaction so our updates are atomic.
  $dbh->begin_work();
  my @sql_column_list = (["is_reconciled", 1, SQL_BOOLEAN]);
  my (%recv_payment_by_id, @processed_recv_payment_id_list);
  # update the is_reconciled flag on the selected check payments.
  for my $recv_payment_id (@recv_payment_id_list) {
    my $recv_payment = select_row($dbh, "recv_payment_tbl", $recv_payment_id);
    if ((!$recv_payment) || ($recv_payment->{"is_reconciled"})) {
      next;
    }
    if ($recv_payment->{"payment_method"} ne "check") {
      # if one of the payments was not a check, throw an exception and roll back all operations.
      $dbh->rollback();
      die "show_page: reconcile_check_pmts_not_a_check.html\n";
    }
    $recv_payment_by_id{$recv_payment_id} = $recv_payment;
    push(@processed_recv_payment_id_list, $recv_payment_id);
    update_row($dbh, "recv_payment_tbl", \@sql_column_list, $recv_payment_id);
  }
  # commit the transaction.
  $dbh->commit();
  # fetch accounts.
  my %cust_acct_by_id;
  my $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch employees.
  my %employee_by_id;
  $sql_query = "select * from employee_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <recv-payment-list>
---END_BLOCK---;
  for my $recv_payment_id (@processed_recv_payment_id_list) {
    my $recv_payment = $recv_payment_by_id{$recv_payment_id};
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $account_name = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
      } else {
	$account_name = "(unknown account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee_name = "";
    if (($employee_id) && (my $employee = $employee_by_id{$employee_id})) {
      $employee_name = HTML::Entities::encode(qq{$employee->{"first_name"} $employee->{"last_name"}});
    }
    my $amount = $recv_payment->{"amount"};
    my $recv_dt = $recv_payment->{"recv_dt"};
    my $check_date = $recv_payment->{"process_txn_after_dt"};
    my $note = HTML::Entities::encode($recv_payment->{"note"} || "");
    my $payment_destination = HTML::Entities::encode($recv_payment->{"payment_destination"});
    my $check_name = HTML::Entities::encode($recv_payment->{"check_name"} || "");
    my $check_number = HTML::Entities::encode($recv_payment->{"check_number"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <recv-payment>
      <id>$recv_payment_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <amount>$amount</amount>
      <recv_dt>$recv_dt</recv_dt>
      <check_date>$check_date</check_date>
      <note>$note</note>
      <payment_destination>$payment_destination</payment_destination>
      <check_name>$check_name</check_name>
      <check_number>$check_number</check_number>
    </recv-payment>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </recv-payment-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reconcile_check_pmts_result.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/view_all_expenses");
  # construct query.
  my $sql_query = "select * from expense_tbl where not(is_reconciled) order by purchase_date, person_id, item_name";
  my (%expense_by_id, @expense_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%expense_by_id, \@expense_id_list);
  # fetch all persons.
  my (%person_by_id);
  $sql_query = "select * from person_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  # get all ledger accounts.
  my (%ledger_account_by_id);
  $sql_query = "select * from ledger_account_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%ledger_account_by_id);
  # populate information needed for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <expense-list>
---END_BLOCK---;
  for my $expense_id (@expense_id_list) {
    my $expense = $expense_by_id{$expense_id};
    my $person_id = $expense->{"person_id"};
    my $person = $person_by_id{$person_id};
    my $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
    my $purchase_date = $expense->{"purchase_date"};
    my $item_name = HTML::Entities::encode($expense->{"item_name"});
    my $vendor_name = HTML::Entities::encode($expense->{"vendor_name"});
    my $ledger_account_name = HTML::Entities::encode($ledger_account_by_id{$expense->{"ledger_account_id"}}->{"name"});
    my $locations = HTML::Entities::encode($expense->{"locations"});
    my $amount = $expense->{"amount"};
    my $payer = HTML::Entities::encode($expense->{"payer"});
    my $book = HTML::Entities::encode($expense->{"book"});
    my $class = HTML::Entities::encode($expense->{"class"});
    my $entered_by = HTML::Entities::encode($expense->{"entered_by"});
    my $payment_method = HTML::Entities::encode($expense->{"payment_method"});
    my $is_reconciled = $expense->{"is_reconciled"} ? 1 : 0;
    my $is_reimbursed = $expense->{"is_reimbursed"} ? 1 : 0;
    my $note = HTML::Entities::encode($expense->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <expense>
      <id>$expense_id</id>
      <person_name>$person_name</person_name>
      <purchase_date>$purchase_date</purchase_date>
      <item_name>$item_name</item_name>
      <vendor_name>$vendor_name</vendor_name>
      <ledger_account_name>$ledger_account_name</ledger_account_name>
      <locations>$locations</locations>
      <amount>$amount</amount>
      <payer>$payer</payer>
      <book>$book</book>
      <class>$class</class>
      <entered_by>$entered_by</entered_by>
      <payment_method>$payment_method</payment_method>
      <is_reconciled>$is_reconciled</is_reconciled>
      <is_reimbursed>$is_reimbursed</is_reimbursed>
      <note>$note</note>
      </expense>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </expense-list>
  <book_list>
    <book>None</book>
    <book>MOM</book>
    <book>BLS</book>
    <book>Icarus</book>
    <book>405 W Sup</book>
    <book>18 S Mich</book>
    <book>Stella</book>
  </book_list>
  <class_list>
    <class>None</class>
    <class>18 S Mich</class>
    <class>Basket</class>
    <class>MOM</class>
    <class>GCS</class>
    <class>CoSpc</class>
    <class>MOS2</class>
    <class>US902S</class>
    <class>MOS4</class>
    <class>405 W Sup</class>
    <class>Boston Link Space</class>
    <class>Icarus</class>
    <class>MPS</class>
    <class>CoRiverN</class>
    <class>MOS1</class>
    <class>1601Bwy</class>
    <class>HVS</class>
  </class_list>
  <entered_by_list>
    <entered_by>None</entered_by>
    <entered_by>Aqeel</entered_by>
    <entered_by>Raziya</entered_by>
    <entered_by>Jerome</entered_by>
    <entered_by>Jin</entered_by>
    <entered_by>Joanna</entered_by>
  </entered_by_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reconcile_expenses_input.xslt", $cgi);
}

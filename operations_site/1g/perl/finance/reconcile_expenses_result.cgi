#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;
use update_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/view_all_expenses");
  # get CGI input parameters.
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @expense_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      push(@expense_id_list, $1);
    }
  }
    # save the book, class, and entered by data.
  my $save_new_info = $cgi->param("save_new_info") ? 1 : 0;
  my (%expense_by_id, %person_by_id, %ledger_account_by_id);
  if ($save_new_info) {
    for my $expense_id (@expense_id_list) {
      if (my $expense = select_row($dbh, "expense_tbl", $expense_id)) {
#  my @sql_column_list = (["book", $book]);
#  update_row($dbh, "expense_tbl", \@sql_column_list, $expense_id);
#  $expense_by_id{$expense->{"id"}} = $expense;
      }
    }
  }
  my $mark_as_processed = $cgi->param("mark_as_processed") ? 1 : 0;
  my (%expense_by_id, %person_by_id, %ledger_account_by_id);
  if ($mark_as_processed) {
    for my $expense_id (@expense_id_list) {
      if (my $expense = select_row($dbh, "expense_tbl", $expense_id)) {
	my @sql_column_list = (["is_reconciled", 1, SQL_BOOLEAN]);
	update_row($dbh, "expense_tbl", \@sql_column_list, $expense_id);
	$expense_by_id{$expense->{"id"}} = $expense;
      }
    }
    # fetch all persons.
    my $sql_query = "select * from person_vw";
    get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
    # get all ledger accounts.
    $sql_query = "select * from ledger_account_tbl";
    get_row_by_id_map($dbh, $sql_query, undef, \%ledger_account_by_id);
    @expense_id_list = sort {
      my $expense_a = $expense_by_id{$a};
      my $expense_b = $expense_by_id{$b};
      my $sort_result = 0;
      $sort_result ||= $expense_a->{"purchase_date"} cmp $expense_b->{"purchase_date"};
      $sort_result ||= $expense_a->{"person_id"} <=> $expense_b->{"person_id"};
      $sort_result ||= $expense_a->{"item_name"} cmp $expense_b->{"item_name"};
      return $sort_result;
    } keys(%expense_by_id);
  }
  # populate information needed for display of page.
  my $expense_id_csv = join(",", @expense_id_list);
  my $num_expense_ids = scalar(@expense_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <mark_as_processed>$mark_as_processed</mark_as_processed>
  <expense_id_csv>$expense_id_csv</expense_id_csv>
  <num_expense_ids>$num_expense_ids</num_expense_ids>
  <expense-list>
---END_BLOCK---;
  if ($mark_as_processed) {
    for my $expense_id (@expense_id_list) {
      my $expense = $expense_by_id{$expense_id};
      my $person_id = $expense->{"person_id"};
      my $person = $person_by_id{$person_id};
      my $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
      my $purchase_date = $expense->{"purchase_date"};
      my $item_name = HTML::Entities::encode($expense->{"item_name"});
      my $vendor_name = HTML::Entities::encode($expense->{"vendor_name"});
      my $ledger_account_name = HTML::Entities::encode($ledger_account_by_id{$expense->{"ledger_account_id"}}->{"name"});
      my $locations = HTML::Entities::encode($expense->{"locations"});
      my $amount = $expense->{"amount"};
      my $payer = HTML::Entities::encode($expense->{"payer"});
      my $book = HTML::Entities::encode($expense->{"book"});
      my $class = HTML::Entities::encode($expense->{"class"});
      my $entered_by = HTML::Entities::encode($expense->{"entered_by"});
      my $payment_method = HTML::Entities::encode($expense->{"payment_method"});
      my $is_reconciled = $expense->{"is_reconciled"} ? 1 : 0;
      my $is_reimbursed = $expense->{"is_reimbursed"} ? 1 : 0;
      my $note = HTML::Entities::encode($expense->{"note"} || "");
      $generated_xml .= <<"---END_BLOCK---;";
    <expense>
      <id>$expense_id</id>
      <person_name>$person_name</person_name>
      <purchase_date>$purchase_date</purchase_date>
      <item_name>$item_name</item_name>
      <vendor_name>$vendor_name</vendor_name>
      <ledger_account_name>$ledger_account_name</ledger_account_name>
      <locations>$locations</locations>
      <amount>$amount</amount>
      <payer>$payer</payer>
      <book>$book</book>
      <class>$class</class>
      <entered_by>$entered_by</entered_by>
      <payment_method>$payment_method</payment_method>
      <is_reconciled>$is_reconciled</is_reconciled>
      <is_reimbursed>$is_reimbursed</is_reimbursed>
      <note>$note</note>
    </expense>
---END_BLOCK---;
    }
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </expense-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reconcile_expenses_result.xslt", $cgi);
}

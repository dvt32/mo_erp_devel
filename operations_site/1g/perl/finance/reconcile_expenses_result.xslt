<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Reconcile Expenses
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/new_expense_input.cgi">New Expense</a>
	  |
	  <a href="/1g/finance/show_expenses.cgi">Recent Expenses</a>
        </p>
	<xsl:if test="mark_as_processed = 1">
	  <xsl:apply-templates select="expense-list"/>
	</xsl:if>
	<p>
	  <a>
	    <xsl:attribute name="href">/1g/finance/download_expenses.cgi?id_csv=<xsl:value-of select="expense_id_csv"/></xsl:attribute>
	    Download <xsl:value-of select="num_expense_ids"/> items as CSV spreadsheet and PDF receipts
	  </a>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="expense-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="21" class="standard_table_title">
	  Expenses Marked as Reconciled
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Person Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Item Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Vendor Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Ledger Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Locations</td>
	<td class="standard_table_spacer_column"></td>
	<td>Book</td>
	<td class="standard_table_spacer_column"></td>
	<td>Class</td>
	<td class="standard_table_spacer_column"></td>
	<td>Entered By</td>
	<td class="standard_table_spacer_column"></td>
	<td>Buy Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Pay Method</td>
	<td class="standard_table_spacer_column"></td>
	<td>Reim</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(expense) > 0">
	  <xsl:apply-templates select="expense"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="19">No expenses.</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="expense">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(person_name) > 20">
	    <xsl:value-of select="substring(person_name, 1, 20)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="person_name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(item_name) > 24">
	    <xsl:value-of select="substring(item_name, 1, 24)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="item_name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(vendor_name) > 24">
	    <xsl:value-of select="substring(vendor_name, 1, 24)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="vendor_name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="ledger_account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="locations"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="book"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="class"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="entered_by"/></td>
      <td><xsl:value-of select="purchase_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/finance/view_expense_receipt.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  $<xsl:value-of select="amount"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="payer"/><xsl:text> </xsl:text><xsl:value-of select="payment_method"/></td>
      <td class="standard_table_spacer_column"></td>
      <td align="center">
	<xsl:choose>
	  <xsl:when test="is_reimbursed = 1">
	    Yes
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:choose>
	      <xsl:when test="payer = 'company'">
		--
	      </xsl:when>
	      <xsl:otherwise>
		No
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(note) > 40">
	    <xsl:value-of select="substring(note, 1, 40)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="note"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

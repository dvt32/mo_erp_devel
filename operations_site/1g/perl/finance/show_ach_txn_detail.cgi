#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/show_elec_txn_detail");
  my $ach_debit_txn_id = $cgi->param("id");
  my $ach_debit_txn = select_row($dbh, "ach_debit_txn_tbl", $ach_debit_txn_id);
  die "show_ach_txn_detail_bad_txn.html\n" if (!$ach_debit_txn);
  my ($cust_acct_id, $cust_acct);
  if ($cust_acct_id = $ach_debit_txn->{"cust_acct_id"}) {
    $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  }
  # populate information needed for display of page.
  my $account_name = "";
  if (defined($cust_acct_id)) {
    if ($cust_acct) {
      $account_name = $cust_acct->{"name"};
    } else {
      $account_name = "(unknown account)";
    }
    $account_name = HTML::Entities::encode($account_name);
  }
  my $amount = $ach_debit_txn->{"amount"};
  my $customer_name = HTML::Entities::encode($ach_debit_txn->{"customer_name"});
  my $customer_bank_id = HTML::Entities::encode($ach_debit_txn->{"customer_bank_id"});
  my $customer_bank_acct_num = HTML::Entities::encode($ach_debit_txn->{"customer_bank_acct_num"});
  my $customer_bank_acct_type = HTML::Entities::encode($ach_debit_txn->{"customer_bank_acct_type"});
  my $company_bank_acct_num = HTML::Entities::encode($ach_debit_txn->{"company_bank_acct_num"});
  my $company_bank_acct_type = HTML::Entities::encode($ach_debit_txn->{"company_bank_acct_type"});
  my $start_date = HTML::Entities::encode($ach_debit_txn->{"start_date"} || "");
  my $prenote_date = HTML::Entities::encode($ach_debit_txn->{"prenote_date"} || "");
  my $last_collection_date = HTML::Entities::encode($ach_debit_txn->{"last_collection_date"} || "");
  my $create_prenote = $ach_debit_txn->{"create_prenote"} ? 1 : 0;
  my $hold_transfer = $ach_debit_txn->{"hold_transfer"} ? 1 : 0;
  my $queued_dt = HTML::Entities::encode($ach_debit_txn->{"queued_dt"});
  my $processed_dt = HTML::Entities::encode($ach_debit_txn->{"processed_dt"} || "");
  my $notes = HTML::Entities::encode($ach_debit_txn->{"notes"} || "");
  my $cancelled = $ach_debit_txn->{"cancelled"} ? 1 : 0;
  my $recv_payment_id = HTML::Entities::encode($ach_debit_txn->{"recv_payment_id"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ach-debit-txn>
    <id>$ach_debit_txn_id</id>
    <account_name>$account_name</account_name>
    <amount>$amount</amount>
    <customer_name>$customer_name</customer_name>
    <customer_bank_id>$customer_bank_id</customer_bank_id>
    <customer_bank_acct_num>$customer_bank_acct_num</customer_bank_acct_num>
    <customer_bank_acct_type>$customer_bank_acct_type</customer_bank_acct_type>
    <company_bank_acct_num>$company_bank_acct_num</company_bank_acct_num>
    <company_bank_acct_type>$company_bank_acct_type</company_bank_acct_type>
    <start_date>$start_date</start_date>
    <prenote_date>$prenote_date</prenote_date>
    <last_collection_date>$last_collection_date</last_collection_date>
    <create_prenote>$create_prenote</create_prenote>
    <hold_transfer>$hold_transfer</hold_transfer>
    <queued_dt>$queued_dt</queued_dt>
    <processed_dt>$processed_dt</processed_dt>
    <notes>$notes</notes>
    <cancelled>$cancelled</cancelled>
    <recv_payment_id>$recv_payment_id</recv_payment_id>
  </ach-debit-txn>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_ach_txn_detail.xslt", $cgi);
}

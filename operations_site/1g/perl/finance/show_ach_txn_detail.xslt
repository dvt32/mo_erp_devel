<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: ACH Transaction Detail
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="ach-debit-txn"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ach-debit-txn">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="2" class="standard_table_title">
	  ACH Transaction Detail
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Amount:
	</td>
	<td class="map_table_value">
	  $<xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Bank Account Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Bank Routing Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_bank_id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Bank Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_bank_acct_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Customer Bank Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="customer_bank_acct_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company Bank Account Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company_bank_acct_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company Bank Account Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company_bank_acct_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Start Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="start_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Prenote Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="prenote_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Last Collection Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="last_collection_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Create Prenote?
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="create_prenote = 1">
	      Yes
	    </xsl:when>
	    <xsl:when test="create_prenote = 0">
	      No
	    </xsl:when>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Hold Transfer?
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="hold_transfer = 1">
	      Yes
	    </xsl:when>
	    <xsl:when test="hold_transfer = 0">
	      No
	    </xsl:when>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Queued Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="substring(queued_dt, 1, 16)"/>
	</td>
      </tr>
      <xsl:if test="processed_dt != ''">
	<tr>
	  <td class="map_table_key">
	    Processed Time:
	  </td>
	  <td class="map_table_value">
	    <xsl:value-of select="substring(processed_dt, 1, 16)"/>
	  </td>
	</tr>
      </xsl:if>
      <tr>
	<td class="map_table_key">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Cancelled?
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="cancelled = 1">
	      Yes
	    </xsl:when>
	    <xsl:when test="cancelled = 0">
	      No
	    </xsl:when>
	  </xsl:choose>
	</td>
      </tr>
      <xsl:if test="recv_payment_id != ''">
	<tr>
	  <td class="map_table_key">
	    Received Payment:
	  </td>
	  <td class="map_table_value">
	    <a>
	      <xsl:attribute name="href">/1g/finance/show_recv_pmt_detail.cgi?id=<xsl:value-of select="recv_payment_id"/></xsl:attribute>
	      <xsl:text>Click for link to the associated payment record</xsl:text>
	    </a>
	  </td>
	</tr>
      </xsl:if>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/show_elec_txn_detail");
  my $credit_card_txn_id = $cgi->param("id");
  my $credit_card_txn = select_row($dbh, "credit_card_txn_tbl", $credit_card_txn_id);
  die "show_card_txn_detail_bad_txn.html\n" if (!$credit_card_txn);
  my ($cust_acct_id, $cust_acct);
  if ($cust_acct_id = $credit_card_txn->{"cust_acct_id"}) {
    $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  }
  # populate information needed for display of page.
  my $account_name = "";
  if (defined($cust_acct_id)) {
    if ($cust_acct) {
      $account_name = $cust_acct->{"name"};
    } else {
      $account_name = "(unknown account)";
    }
    $account_name = HTML::Entities::encode($account_name);
  }
  my $card_number = HTML::Entities::encode($credit_card_txn->{"card_number"});
  my $card_expiration_month = $credit_card_txn->{"card_expiration_month"};
  my $card_expiration_year = $credit_card_txn->{"card_expiration_year"};
  my $card_security_code = $credit_card_txn->{"card_security_code"} || "";
  my $billing_name = HTML::Entities::encode($credit_card_txn->{"billing_name"});
  my $billing_company = HTML::Entities::encode($credit_card_txn->{"billing_company"} || "");
  my $billing_address_num = HTML::Entities::encode($credit_card_txn->{"billing_address_num"} || "");
  my $billing_zip_code = HTML::Entities::encode($credit_card_txn->{"billing_zip_code"});
  my $billing_address_1 = HTML::Entities::encode($credit_card_txn->{"billing_address_1"});
  my $billing_address_2 = HTML::Entities::encode($credit_card_txn->{"billing_address_2"} || "");
  my $billing_city = HTML::Entities::encode($credit_card_txn->{"billing_city"});
  my $billing_state = HTML::Entities::encode($credit_card_txn->{"billing_state"});
  my $billing_country = HTML::Entities::encode($credit_card_txn->{"billing_country"});
  my $billing_phone_number = HTML::Entities::encode($credit_card_txn->{"billing_phone_number"} || "");
  my $billing_fax_number = HTML::Entities::encode($credit_card_txn->{"billing_fax_number"} || "");
  my $billing_email_address = HTML::Entities::encode($credit_card_txn->{"billing_email_address"} || "");
  my $total_charge = HTML::Entities::encode($credit_card_txn->{"total_charge"});
  my $queued_dt = HTML::Entities::encode($credit_card_txn->{"queued_dt"});
  my $processed_dt = HTML::Entities::encode($credit_card_txn->{"processed_dt"} || "");
  my $response_avs = HTML::Entities::encode($credit_card_txn->{"response_avs"} || "");
  my $response_order_num = HTML::Entities::encode($credit_card_txn->{"response_order_num"} || "");
  my $response_error = HTML::Entities::encode($credit_card_txn->{"response_error"} || "");
  my $response_approved = HTML::Entities::encode($credit_card_txn->{"response_approved"} || "");
  my $response_code = HTML::Entities::encode($credit_card_txn->{"response_code"} || "");
  my $response_message = HTML::Entities::encode($credit_card_txn->{"response_message"} || "");
  my $response_txn_dt = HTML::Entities::encode($credit_card_txn->{"response_txn_dt"} || "");
  my $response_reference_num = HTML::Entities::encode($credit_card_txn->{"response_reference_num"} || "");
  my $response_tdate = HTML::Entities::encode($credit_card_txn->{"response_tdate"} || "");
  my $notes = HTML::Entities::encode($credit_card_txn->{"notes"} || "");
  my $ip_address = HTML::Entities::encode($credit_card_txn->{"ip_address"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <credit-card-txn>
    <id>$credit_card_txn_id</id>
    <account_name>$account_name</account_name>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <billing_name>$billing_name</billing_name>
    <billing_company>$billing_company</billing_company>
    <billing_address_num>$billing_address_num</billing_address_num>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_country>$billing_country</billing_country>
    <billing_phone_number>$billing_phone_number</billing_phone_number>
    <billing_fax_number>$billing_fax_number</billing_fax_number>
    <billing_email_address>$billing_email_address</billing_email_address>
    <total_charge>$total_charge</total_charge>
    <queued_dt>$queued_dt</queued_dt>
    <processed_dt>$processed_dt</processed_dt>
    <response_avs>$response_avs</response_avs>
    <response_order_num>$response_order_num</response_order_num>
    <response_error>$response_error</response_error>
    <response_approved>$response_approved</response_approved>
    <response_code>$response_code</response_code>
    <response_message>$response_message</response_message>
    <response_txn_dt>$response_txn_dt</response_txn_dt>
    <response_reference_num>$response_reference_num</response_reference_num>
    <response_tdate>$response_tdate</response_tdate>
    <notes>$notes</notes>
    <ip_address>$ip_address</ip_address>
  </credit-card-txn>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_card_txn_detail.xslt", $cgi);
}

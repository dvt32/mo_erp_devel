#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/edit_cust_pmt_accts");
  # fetch credit cards.
  my $sql_query = "select * from credit_card_tbl";
  my %credit_card_by_id;
  get_row_by_id_map($dbh, $sql_query, undef, \%credit_card_by_id);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch persons.
  my %cust_pers_by_id;
  $sql_query = "select * from cust_pers_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id);
  # sort credit cards based on CGI parameters specifying sort order.
  my $sort_column = $cgi->param("sort_column") || "account";
  if ($sort_column !~ /^account|person|card_number|expiration_date$/) {
    die "show_page: show_cust_cards_bad_sort_column.html\n";
  }
  my @sort_column_list = ("account", "person", "card_number", "expiration_date");
  for(my $i = 0; $i < scalar(@sort_column_list); ) {
    if ($sort_column_list[$i] eq $sort_column) {
      splice(@sort_column_list, $i, 1);
    } else {
      $i++;
    }
  }
  unshift(@sort_column_list, $sort_column);
  my @credit_card_id_list = sort {
    my $card_a = $credit_card_by_id{$a};
    my $card_b = $credit_card_by_id{$b};
    my $sort_value = 0;
    for my $_sort_column (@sort_column_list) {
      if ($_sort_column eq "account") {
	my ($account_name_a, $account_name_b) = ("", "");
	my ($account_id_a, $account_a, $account_id_b, $account_b);
	if (($account_id_a = $card_a->{"account_id"}) && ($account_a = $cust_acct_by_id{$account_id_a})) {
	  $account_name_a = lc($account_a->{"name"});
	}
	if (($account_id_b = $card_b->{"account_id"}) && ($account_b = $cust_acct_by_id{$account_id_b})) {
	  $account_name_b = lc($account_b->{"name"});
	}
	$sort_value = $account_name_a cmp $account_name_b;
      } elsif ($_sort_column eq "person") {
	my ($person_name_a, $person_name_b) = ("", "");
	my ($person_id_a, $person_a, $person_id_b, $person_b);
	if (($person_id_a = $card_a->{"person_id"}) && ($person_a = $cust_pers_by_id{$person_id_a})) {
	  $person_name_a = lc($person_a->{"last_name"} . $person_a->{"first_name"});
	}
	if (($person_id_b = $card_b->{"person_id"}) && ($person_b = $cust_pers_by_id{$person_id_b})) {
	  $person_name_b = lc($person_b->{"last_name"} . $person_b->{"first_name"});
	}
	$sort_value = $person_name_a cmp $person_name_b;
      } elsif ($_sort_column eq "card_number") {
	$sort_value = $card_a->{"card_number"} cmp $card_b->{"card_number"};
      } elsif ($_sort_column eq "expiration_date") {
	$sort_value = $card_a->{"card_expiration_year"} <=> $card_b->{"card_expiration_year"};
	$sort_value ||= $card_a->{"card_expiration_month"} <=> $card_b->{"card_expiration_month"};
      } else {
	die "show_page: show_cust_cards_bad_sort_column.html\n";
      }
      last if ($sort_value != 0);
    }
    return $sort_value;
  } keys(%credit_card_by_id);
  # populate information needed for display of page.
  $sort_column = HTML::Entities::encode($sort_column);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sort_column>$sort_column</sort_column>
  <credit_card_list>
---END_BLOCK---;
  for my $credit_card_id (@credit_card_id_list) {
    my $credit_card = $credit_card_by_id{$credit_card_id};
    my $cust_acct_id = $credit_card->{"account_id"};
    my $account_name = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
      } else {
	$account_name = "(unknown account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $cust_pers_id = $credit_card->{"person_id"};
    my $person_name = "";
    if (defined($cust_pers_id)) {
      my $cust_pers = $cust_pers_by_id{$cust_pers_id};
      if ($cust_pers) {
	$person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
      } else {
	$person_name = "(unknown person)";
      }
      $person_name = HTML::Entities::encode($person_name);
    }
    my $card_number = $credit_card->{"card_number"};
    my $expiration_date = $credit_card->{"card_expiration_month"} . "/" . $credit_card->{"card_expiration_year"};
    my $billing_address_1 = HTML::Entities::encode($credit_card->{"billing_address_1"});
    my $billing_zip_code = HTML::Entities::encode($credit_card->{"billing_zip_code"});
    my $note = HTML::Entities::encode($credit_card->{"note"} || "");
    my $creation_dt = HTML::Entities::encode($credit_card->{"creation_dt"});
    $generated_xml .= <<"---END_BLOCK---;";
    <credit_card>
      <id>$credit_card_id</id>
      <account_name>$account_name</account_name>
      <person_name>$person_name</person_name>
      <card_number>$card_number</card_number>
      <expiration_date>$expiration_date</expiration_date>
      <billing_address_1>$billing_address_1</billing_address_1>
      <billing_zip_code>$billing_zip_code</billing_zip_code>
      <note>$note</note>
      <creation_dt>$creation_dt</creation_dt>
    </credit_card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </credit_card_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_cust_cards.xslt", $cgi);
}

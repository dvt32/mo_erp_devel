<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Customer Credit/Debit Cards
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/new_cust_card_input.cgi">New Credit/Debit Card</a>
<!--	  |
	  <a href="/1g/finance/show_cust_bank_accts.cgi">Show Bank Accounts</a>
	  |
	  <a href="/1g/finance/new_cust_bank_acct_input.cgi">New Bank Account</a> -->
        </p>
	<xsl:apply-templates select="credit_card_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit_card_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="19" class="standard_table_title">
	  Customer Credit/Debit Cards
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>
	  Charge
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Account Name
	  <xsl:choose>
	    <xsl:when test="/page/sort_column = 'account'">
	      [&uarr;]
	    </xsl:when>
	    <xsl:otherwise>
	      <a href="/1g/finance/show_cust_cards.cgi?sort_column=account" class="bare_link">&uarr;</a>
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Person Name
	  <xsl:choose>
	    <xsl:when test="/page/sort_column = 'person'">
	      [&uarr;]
	    </xsl:when>
	    <xsl:otherwise>
	      <a href="/1g/finance/show_cust_cards.cgi?sort_column=person" class="bare_link">&uarr;</a>
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Card Number
	  <xsl:choose>
	    <xsl:when test="/page/sort_column = 'card_number'">
	      [&uarr;]
	    </xsl:when>
	    <xsl:otherwise>
	      <a href="/1g/finance/show_cust_cards.cgi?sort_column=card_number" class="bare_link">&uarr;</a>
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Expire
	  <xsl:choose>
	    <xsl:when test="/page/sort_column = 'expiration_date'">
	      [&uarr;]
	    </xsl:when>
	    <xsl:otherwise>
	      <a href="/1g/finance/show_cust_cards.cgi?sort_column=expiration_date" class="bare_link">&uarr;</a>
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Address
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Zip
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Create Time
	</td>
	<td class="standard_table_spacer_column"></td>
	<td>
	  Note
	</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(credit_card) > 0">
	  <xsl:apply-templates select="credit_card"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="15">No customer credit/debit cards.</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="credit_card">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/finance/receive_card_input.cgi?credit_card_id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:text>Charge</xsl:text>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(account_name) > 24">
	    <xsl:value-of select="substring(account_name, 1, 24)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="account_name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="person_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/finance/edit_cust_card_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="card_number"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="expiration_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(billing_address_1) > 24">
	    <xsl:value-of select="substring(billing_address_1, 1, 24)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="billing_address_1"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="billing_zip_code"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="substring(creation_dt, 1, 16)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

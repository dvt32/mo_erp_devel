#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/show_deposits");
  # fetch most recent deposits.
  my (%deposit_by_id, @deposit_id_list);
  my $sql_query = "select * from deposit_tbl order by deposit_dt desc limit 150";
  get_row_by_id_map($dbh, $sql_query, undef, \%deposit_by_id, \@deposit_id_list);
  # fetch associated received payments.
  $sql_query = "select * from recv_payment_tbl where deposit_id = ?";
  my %recv_payment_map_by_deposit_id;
  for my $deposit_id (@deposit_id_list) {
    my @sql_param_list = ([$deposit_id, SQL_INTEGER]);
    my %recv_payment_by_id;
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%recv_payment_by_id);
    $recv_payment_map_by_deposit_id{$deposit_id} = \%recv_payment_by_id;
  }
  # fetch accounts.
  my %cust_acct_by_id;
  # $sql_query = "select * from cust_acct_tbl where is_active";
  $sql_query = "select cust_acct_tbl.*, floor_tbl.short_global_label from cust_acct_tbl left join floor_tbl on cust_acct_tbl.primary_floor_id =  floor_tbl.id  where cust_acct_tbl.is_active";  
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # load list of employees who can receive payment.
  my (%employee_by_id);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role_list>
---END_BLOCK---;
  for my $role (@$role_list) {
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role_list>
  <deposit_list>
---END_BLOCK---;
  for my $deposit_id (@deposit_id_list) {
    my $deposit = $deposit_by_id{$deposit_id};
    my $deposit_dt = $deposit->{"deposit_dt"};
    my $deposit_employee_id = $deposit->{"employee_id"};
    my $deposit_employee = $employee_by_id{$deposit_employee_id};
    my $deposit_employee_name = HTML::Entities::encode($deposit_employee->{"first_name"} . " " . $deposit_employee->{"last_name"});
    my $bank_name = HTML::Entities::encode($deposit->{"bank_name"});
    my $bank_routing_number = $deposit->{"bank_routing_number"};
    my $bank_account_number = $deposit->{"bank_account_number"};
    my $total_amount = 0;
    my $deposit_note = HTML::Entities::encode($deposit->{"note"});
    my $recv_payment_by_id = $recv_payment_map_by_deposit_id{$deposit_id};
    my @recv_payment_list = sort {
      my $sort_order = 0;
      $sort_order ||= $a->{"recv_dt"} cmp $b->{"recv_dt"};
    } values(%$recv_payment_by_id);
    for my $recv_payment (@recv_payment_list) {
      $total_amount += $recv_payment->{"amount"};
    }
    $generated_xml .= <<"---END_BLOCK---;";
    <deposit>
      <id>$deposit_id</id>
      <deposit_dt>$deposit_dt</deposit_dt>
      <employee_id>$deposit_employee_id</employee_id>
      <employee_name>$deposit_employee_name</employee_name>
      <bank_name>$bank_name</bank_name>
      <bank_routing_number>$bank_routing_number</bank_routing_number>
      <bank_account_number>$bank_account_number</bank_account_number>
      <total_amount>$total_amount</total_amount>
      <note>$deposit_note</note>
      <receive_payment_list>
---END_BLOCK---;
    for my $recv_payment (@recv_payment_list) {
      my $recv_payment_id = $recv_payment->{"id"};
      my $cust_acct_id = $recv_payment->{"cust_acct_id"};
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      my $account_name = HTML::Entities::encode($cust_acct->{"name"});
      my $account_floor = HTML::Entities::encode($cust_acct->{"short_global_label"});      
      my $amount = $recv_payment->{"amount"};
      my $payment_method = HTML::Entities::encode($recv_payment->{"payment_method"});
      my $check_number = HTML::Entities::encode($recv_payment->{"check_number"});
      my $check_name = HTML::Entities::encode($recv_payment->{"check_name"});
      my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
      my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
      my $note = HTML::Entities::encode($recv_payment->{"note"});
      my $employee_id = $recv_payment->{"employee_id"};
      my $employee = $employee_by_id{$employee_id};
      my $employee_name = HTML::Entities::encode($employee->{"first_name"} . " " . $employee->{"last_name"});
      $generated_xml .= <<"---END_BLOCK---;";
        <receive_payment>
          <id>$recv_payment_id</id>
          <account_name>$account_name</account_name>
          <account_floor>$account_floor</account_floor>          
          <amount>$amount</amount>
          <payment_method>$payment_method</payment_method>
          <check_number>$check_number</check_number>
          <check_name>$check_name</check_name>
          <check_date>$check_date</check_date>
          <received_dt>$received_dt</received_dt>
          <note>$note</note>
          <employee_name>$employee_name</employee_name>
        </receive_payment>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </receive_payment_list>
    </deposit>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </deposit_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_deposits.xslt", $cgi);
}

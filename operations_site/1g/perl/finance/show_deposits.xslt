<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Show Deposits
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/make_deposit_input.cgi">Make Deposit</a>
        </p>
	<xsl:apply-templates select="deposit_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="deposit_list">
    <xsl:apply-templates select="deposit"/>
  </xsl:template>

  <xsl:template match="deposit">
    <p>
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="21" class="standard_table_title">
	  Deposit to
	  <xsl:value-of select="bank_name"/>
	  <xsl:text> </xsl:text>
	  ***<xsl:value-of select="substring(bank_account_number, string-length(bank_account_number) - 3)"/>
	  at 
	  <xsl:value-of select="substring(deposit_dt, 1, 16)"/>
	  made by
	  <xsl:value-of select="employee_name"/>
	  <xsl:if test="/page/role_list/role = 'role:finance/undo_deposit'">
	    (<a>
	    <xsl:attribute name="href">/1g/finance/undo_deposit.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	    <xsl:attribute name="class">standard_table_title_link</xsl:attribute>
	    <xsl:text>undo</xsl:text>
	    </a>)
	  </xsl:if>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Account Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Floor</td>
	<td class="standard_table_spacer_column"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Form</td>
	<td class="standard_table_spacer_column"></td>
	<td>Ck #</td>
	<td class="standard_table_spacer_column"></td>
	<td>Name on Chk</td>
	<td class="standard_table_spacer_column"></td>
	<td>Chk Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Received Time</td>
	<td class="standard_table_spacer_column"></td>
	<td>Received By</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="receive_payment_list"/>
      <tr>
	<xsl:choose>
	  <xsl:when test="count(receive_payment_list/receive_payment) mod 2 = 1">
	    <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	  </xsl:otherwise>
	</xsl:choose>
	<td class="standard_table_side_padding"></td>
	<td class="standard_table_spacer_column"></td>
	<td class="standard_table_spacer_column"></td>
	<td style="text-align: right">
	  <xsl:value-of select="count(receive_payment_list/receive_payment)"/>
	  items, total:
	</td>
	<td class="standard_table_spacer_column"></td>
	<td class="standard_table_currency_cell">$<xsl:value-of select="format-number(total_amount, '###,###.00')"/></td>
	<td class="standard_table_spacer_column"></td>
	<td colspan="13">
	  Note:
	  <xsl:value-of select="note"/>
	</td>
	<td class="standard_table_side_padding"></td>
      </tr>
    </table>
    </p>
  </xsl:template>

  <xsl:template match="receive_payment_list">
    <xsl:apply-templates select="receive_payment"/>
  </xsl:template>

  <xsl:template match="receive_payment">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_floor"/></td>      
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="format-number(amount, '###,###.00')"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="payment_method"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="check_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="received_dt"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="employee_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

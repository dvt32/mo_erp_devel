#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_role_list;
use get_row_by_id_map;
use select_row;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $employee_id = $employee->{"id"};
  my $role_list = get_role_list($instance_map, $employee_id);
  if ((grep {$_ eq "role:finance/view_all_expenses"} @$role_list) && ($cgi->param("employee_id"))) {
    $employee_id = $cgi->param("employee_id");
  }
  if ($employee_id != $employee->{"id"}) {
    $employee = select_row($dbh, "employee_tbl", $employee_id);
  }
  my $num_rows_to_display = $cgi->param("num_rows") || 20;
  if ($num_rows_to_display !~ /^\d+$/) {
    die "show_page: show_expenses_bad_num_rows.html\n";
  } else {
    $num_rows_to_display = int($num_rows_to_display);
  }
  # construct query.
  my $sql_query = "select * from expense_tbl where account_id = ? and person_id = ? order by purchase_date desc, item_name limit $num_rows_to_display";
  my @sql_param_list = ([$account_id_of_corporate, SQL_INTEGER], [$employee_id, SQL_INTEGER]);
  my (%expense_by_id, @expense_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%expense_by_id, \@expense_id_list);
  # fetch all employees to generate drop down list.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  # get all ledger accounts.
  my (%ledger_account_by_id);
  $sql_query = "select * from ledger_account_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%ledger_account_by_id);
  # populate information needed for display of page.
  my $selected_employee = $employee_by_id{$employee_id};
  my $first_name = HTML::Entities::encode($selected_employee->{"first_name"});
  my $last_name = HTML::Entities::encode($selected_employee->{"last_name"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <selected-employee>
    <id>$employee_id</id>
    <first_name>$first_name</first_name>
    <last_name>$last_name</last_name>
  </selected-employee>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <employee-list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_first_name = HTML::Entities::encode($_employee->{"first_name"});
    my $_last_name = HTML::Entities::encode($_employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <first_name>$_first_name</first_name>
      <last_name>$_last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <num_rows>$num_rows_to_display</num_rows>
  <expense-list>
---END_BLOCK---;
  for my $expense_id (@expense_id_list) {
    my $expense = $expense_by_id{$expense_id};
    my $purchase_date = $expense->{"purchase_date"};
    my $item_name = HTML::Entities::encode($expense->{"item_name"});
    my $vendor_name = HTML::Entities::encode($expense->{"vendor_name"});
    my $ledger_account_name = HTML::Entities::encode($ledger_account_by_id{$expense->{"ledger_account_id"}}->{"name"});
    my $locations = HTML::Entities::encode($expense->{"locations"});
    my $amount = $expense->{"amount"};
    my $payer = HTML::Entities::encode($expense->{"payer"});
    my $payment_method = HTML::Entities::encode($expense->{"payment_method"});
    my $book = HTML::Entities::encode($expense->{"book"});
    my $class = HTML::Entities::encode($expense->{"class"});
    my $entered_by = HTML::Entities::encode($expense->{"entered_by"});
    my $is_reconciled = $expense->{"is_reconciled"} ? 1 : 0;
    my $is_reimbursed = $expense->{"is_reimbursed"} ? 1 : 0;
    my $note = HTML::Entities::encode($expense->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <expense>
      <id>$expense_id</id>
      <purchase_date>$purchase_date</purchase_date>
      <item_name>$item_name</item_name>
      <vendor_name>$vendor_name</vendor_name>
      <ledger_account_name>$ledger_account_name</ledger_account_name>
      <locations>$locations</locations>
      <amount>$amount</amount>
      <payer>$payer</payer>
      <payment_method>$payment_method</payment_method>
      <book>$book</book>
      <class>$class</class>
      <entered_by>$entered_by</entered_by>
      <is_reconciled>$is_reconciled</is_reconciled>
      <is_reimbursed>$is_reimbursed</is_reimbursed>
      <note>$note</note>
    </expense>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </expense-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_expenses.xslt", $cgi);
}

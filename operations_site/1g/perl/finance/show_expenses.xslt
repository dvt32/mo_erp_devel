<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Recent Expenses
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/finance/new_expense_input.cgi">New Expense</a>
	  <xsl:if test="role-list/role = 'role:finance/view_all_expenses'">
	    |
	    <a href="/1g/finance/reconcile_expenses_input.cgi">Reconcile Expenses</a>
	  </xsl:if>
        </p>
	<form action="/1g/finance/show_expenses.cgi" method="get">
	  <xsl:if test="role-list/role = 'role:finance/view_all_expenses'">
	    Employee:
	    <select name="employee_id">
	      <xsl:for-each select="/page/employee-list/employee">
		<option>
		  <xsl:if test="/page/selected-employee/id = id">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		  <xsl:value-of select="last_name"/>
		  <xsl:text>, </xsl:text>
		  <xsl:value-of select="first_name"/>
		</option>
	      </xsl:for-each>
	    </select>
	  </xsl:if>
	  Num Rows:
	  <select name="num_rows">
	    <option>
	      <xsl:if test="num_rows = 20">
		<xsl:attribute name="selected">selected</xsl:attribute>
	      </xsl:if>
	      <xsl:text>20</xsl:text>
	    </option>
	    <option>
	      <xsl:if test="num_rows = 40">
		<xsl:attribute name="selected">selected</xsl:attribute>
	      </xsl:if>
	      <xsl:text>40</xsl:text>
	    </option>
	    <option>
	      <xsl:if test="num_rows = 100">
		<xsl:attribute name="selected">selected</xsl:attribute>
	      </xsl:if>
	      <xsl:text>100</xsl:text>
	    </option>
	    <option>
	      <xsl:if test="num_rows = 500">
		<xsl:attribute name="selected">selected</xsl:attribute>
	      </xsl:if>
	      <xsl:text>500</xsl:text>
	    </option>
	  </select>
	  <input type="submit" value="Go" />
	</form>
	<xsl:apply-templates select="expense-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="expense-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="34" class="standard_table_title">
	  Recent Expenses
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>ID</td>
	<td class="standard_table_spacer_column"></td>
	<td>Item Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Vendor Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Buy Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Book</td>
	<td class="standard_table_spacer_column"></td>
	<td>Class</td>
	<td class="standard_table_spacer_column"></td>
	<td>Pay Method</td>
	<td class="standard_table_spacer_column"></td>
	<td>Amount</td>
	<td class="standard_table_spacer_column"></td>
	<td>Entered By</td>
	<td class="standard_table_spacer_column"></td>
	<td>Reimbursed</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(expense) > 0">
	  <xsl:apply-templates select="expense"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="15">No expenses.</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="expense">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td><xsl:value-of select="id"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="is_reconciled = 0 and is_reimbursed = 0">
	    <a>
	      <xsl:attribute name="class">bare_link</xsl:attribute>
	      <xsl:attribute name="href">
		<xsl:text>/1g/finance/edit_expense_input.cgi?id=</xsl:text>
		<xsl:value-of select="id"/>
	      </xsl:attribute>
	      <xsl:value-of select="item_name"/>
	    </a>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="item_name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="vendor_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="purchase_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="book"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="class"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="payer"/><xsl:text> </xsl:text><xsl:value-of select="payment_method"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/finance/view_expense_receipt.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  $<xsl:value-of select="amount"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="entered_by"/></td>
      <td class="standard_table_spacer_column"></td>
      <td align="center">
	<xsl:choose>
	  <xsl:when test="is_reimbursed = 1">
	    Yes
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:choose>
	      <xsl:when test="payer = 'company'">
		--
	      </xsl:when>
	      <xsl:otherwise>
		No
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

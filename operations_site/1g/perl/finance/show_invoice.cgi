#!/usr/bin/perl -w

use strict;
local($^W) = 1;

use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_quickbooks_handle;
use read_file;
use get_xml_parser;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that the invoice ID was specified.
  my $invoice_id = $cgi->param("id");
  if ((!defined($invoice_id)) || ($invoice_id !~ /^\d+$/)) {
    die "show_page: show_invoice_not_found.html\n";
  }
  my ($quickbooks_request_processor, $ticket) = get_quickbooks_handle($instance_map, "main");
  my $request_xml_string = read_file("show_invoice_request.xml");
  $request_xml_string =~ s{<cgi:ref_number/>}{$invoice_id}s;
  my $generated_xml = $quickbooks_request_processor->ProcessRequest($ticket, $request_xml_string);
  # done with QuickBooks connection, so close it.
  $quickbooks_request_processor->CloseConnection($ticket);
  my $xml_parser = get_xml_parser();
  # parse the response XML.
  my $xml_document = $xml_parser->parse_string($generated_xml);
  my $document_root = $xml_document->documentElement();
  # find the status code using XPath.
  my $status_code = int($document_root->find("/QBXML/QBXMLMsgsRs/InvoiceQueryRs/\@statusCode"));
  # status code of 500 means the invoice was not found.
  if ($status_code == 500) {
    die "show_page: show_invoice_not_found.html\n";
  }
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_invoice.xslt", $cgi);
}

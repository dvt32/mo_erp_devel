<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="QBXML">
    <xsl:apply-templates select="QBXMLMsgsRs/InvoiceQueryRs/InvoiceRet"/>
  </xsl:template>

  <xsl:template match="InvoiceRet">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Invoice
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<table cellspacing="0" cellpadding="4px" border="0">
	  <tr>
	    <td class="map_table_key">
	      Account:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="CustomerRef/FullName"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Date Issued:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="TxnDate"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Invoice Number:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="RefNumber"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Due Date:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="DueDate"/>
	    </td>
	  </tr>
	</table>
	<br/>
	<table cellspacing="0" cellpadding="0" border="0" class="quickbooks_invoice">
	  <tr class="quickbooks_invoice_column_heading_row">
	    <td class="quickbooks_invoice_column_header_side_padding"/>
	    <td class="quickbooks_invoice_column_header">Item</td>
	    <td class="quickbooks_invoice_column_header_spacer_left_column"/>
	    <td class="quickbooks_invoice_column_header_spacer_right_column"/>
	    <td class="quickbooks_invoice_column_header">Qty</td>
	    <td class="quickbooks_invoice_column_header_spacer_left_column"/>
	    <td class="quickbooks_invoice_column_header_spacer_right_column"/>
	    <td class="quickbooks_invoice_column_header">Description</td>
	    <td class="quickbooks_invoice_column_header_spacer_left_column"/>
	    <td class="quickbooks_invoice_column_header_spacer_right_column"/>
	    <td class="quickbooks_invoice_column_header">Rate</td>
	    <td class="quickbooks_invoice_column_header_spacer_left_column"/>
	    <td class="quickbooks_invoice_column_header_spacer_right_column"/>
	    <td class="quickbooks_invoice_column_header">Amount</td>
	    <td class="quickbooks_invoice_column_header_side_padding"/>
	  </tr>
	  <xsl:apply-templates select="InvoiceLineRet|InvoiceLineGroupRet"/>
	  <tr>
	    <td colspan="15" class="quickbooks_invoice_body_footer_separator"/>
	  </tr>
	  <xsl:apply-templates select="Subtotal"/>
	  <xsl:apply-templates select="AppliedAmount"/>
	  <xsl:apply-templates select="BalanceRemaining"/>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="InvoiceLineRet">
    <tr>
      <td class="quickbooks_invoice_body_side_padding"/>
      <td class="quickbooks_invoice_body_text"><xsl:value-of select="ItemRef/FullName"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_number"><xsl:value-of select="Quantity"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_text"><xsl:value-of select="Desc"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_number"><xsl:value-of select="Rate"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_amount"><xsl:value-of select="Amount"/></td>
      <td class="quickbooks_invoice_body_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="InvoiceLineGroupRet">
    <!--
    <tr>
      <td class="quickbooks_invoice_body_side_padding"/>
      <td class="quickbooks_invoice_body_text"><xsl:value-of select="ItemGroupRef/FullName"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_number"><xsl:value-of select="Quantity"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td></td>
      <td class="quickbooks_invoice_body_side_padding"/>
    </tr>
    <xsl:apply-templates select="InvoiceLineRet"/>
    <tr>
      <td class="quickbooks_invoice_body_side_padding"/>
      <td></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_text"><xsl:value-of select="Desc"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_amount"><xsl:value-of select="TotalAmount"/></td>
      <td class="quickbooks_invoice_body_side_padding"/>
    </tr>
    -->
    <tr>
      <td class="quickbooks_invoice_body_side_padding"/>
      <td class="quickbooks_invoice_body_text"><xsl:value-of select="ItemGroupRef/FullName"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_number"><xsl:value-of select="Quantity"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_text"><xsl:value-of select="Desc"/></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td></td>
      <td class="quickbooks_invoice_body_spacer_left_column"><br/></td>
      <td class="quickbooks_invoice_body_spacer_right_column"/>
      <td class="quickbooks_invoice_body_amount"><xsl:value-of select="TotalAmount"/></td>
      <td class="quickbooks_invoice_body_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="Subtotal">
    <tr>
      <td class="quickbooks_invoice_footer_side_padding"/>
      <td colspan="10" class="quickbooks_invoice_footer_label">Total</td>
      <td class="quickbooks_invoice_footer_spacer_left_column"/>
      <td class="quickbooks_invoice_footer_spacer_right_column"/>
      <td class="quickbooks_invoice_footer_amount"><xsl:value-of select="."/></td>
      <td class="quickbooks_invoice_footer_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="AppliedAmount">
    <tr>
      <td class="quickbooks_invoice_footer_side_padding"/>
      <td colspan="10" class="quickbooks_invoice_footer_label">Already Paid</td>
      <td class="quickbooks_invoice_footer_spacer_left_column"/>
      <td class="quickbooks_invoice_footer_spacer_right_column"/>
      <td class="quickbooks_invoice_footer_amount"><xsl:value-of select="."/></td>
      <td class="quickbooks_invoice_footer_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="BalanceRemaining">
    <tr>
      <td class="quickbooks_invoice_footer_side_padding"/>
      <td colspan="10" class="quickbooks_invoice_footer_label">Balance Due</td>
      <td class="quickbooks_invoice_footer_spacer_left_column"/>
      <td class="quickbooks_invoice_footer_spacer_right_column"/>
      <td class="quickbooks_invoice_footer_balance_amount"><xsl:value-of select="."/></td>
      <td class="quickbooks_invoice_footer_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl -w

use strict;
local($^W) = 1;

use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_quickbooks_handle;
use apply_xslt_output_xhtml;
use read_file;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my ($quickbooks_request_processor, $ticket) = get_quickbooks_handle($instance_map, "main");
  my $request_xml_string = read_file("show_open_invoices_request.xml");
  my $generated_xml = $quickbooks_request_processor->ProcessRequest($ticket, $request_xml_string);
  $quickbooks_request_processor->CloseConnection($ticket);
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_open_invoices.xslt", $cgi);
}

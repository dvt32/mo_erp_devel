<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="QBXML">
    <xsl:apply-templates select="QBXMLMsgsRs/GeneralDetailReportQueryRs/ReportRet"/>
  </xsl:template>

  <xsl:template match="ReportRet">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Open Invoices
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  When you click on an invoice, please wait up to 30 seconds
	  for the information to display.
	</p>
	<table cellspacing="0" cellpadding="0" border="0" class="quickbooks_detail">
	  <tr>
	    <td colspan="12" class="quickbooks_detail_title">
	      <xsl:value-of select="ReportTitle"/>
	      <br/>
	      <xsl:value-of select="ReportSubtitle"/>
	    </td>
	  </tr>
	  <tr class="quickbooks_detail_column_heading_row">
	    <td class="quickbooks_detail_column_header_side_padding"/>
	    <td class="quickbooks_detail_column_header_spacer_column"/>
	    <td class="quickbooks_detail_column_header">
	      <xsl:value-of select="ColDesc[@colID='2']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_detail_column_header_spacer_column"/>
	    <td class="quickbooks_detail_column_header">
	      <xsl:value-of select="ColDesc[@colID='4']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_detail_column_header_spacer_column"/>
	    <td class="quickbooks_detail_column_header">
	      <xsl:value-of select="ColDesc[@colID='7']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_detail_column_header_spacer_column"/>
	    <td class="quickbooks_detail_column_header">
	      <xsl:value-of select="ColDesc[@colID='9']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_detail_column_header_spacer_column"/>
	    <td class="quickbooks_detail_column_header">
	      <xsl:value-of select="ColDesc[@colID='10']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_detail_column_header_side_padding"/>
	  </tr>
	  <xsl:apply-templates select="ReportData"/>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ReportData">
    <xsl:apply-templates select="TextRow|DataRow|SubtotalRow|TotalRow"/>
  </xsl:template>

  <xsl:template match="TextRow">
    <tr>
      <td class="quickbooks_detail_body_side_padding"/>
      <td colspan="10" class="quickbooks_detail_body_section_heading">
	<xsl:value-of select="@value"/>
      </td>
      <td class="quickbooks_detail_body_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="DataRow">
    <tr>
      <td class="quickbooks_detail_body_side_padding"/>
      <td class="quickbooks_detail_body_spacer_column"/>
      <td class="quickbooks_detail_body_text">
	<xsl:value-of select="ColData[@colID='2']/@value"/>
      </td>
      <td class="quickbooks_detail_body_spacer_column"/>
      <td class="quickbooks_detail_body_number">
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/finance/show_invoice.cgi?id=</xsl:text>
	    <xsl:value-of select="ColData[@colID='4']/@value"/>
	  </xsl:attribute>
	  <xsl:value-of select="ColData[@colID='4']/@value"/>
	</a>
      </td>
      <td class="quickbooks_detail_body_spacer_column"/>
      <td class="quickbooks_detail_body_date">
	<xsl:value-of select="ColData[@colID='7']/@value"/>
      </td>
      <td class="quickbooks_detail_body_spacer_column"/>
      <td class="quickbooks_detail_body_number">
	<xsl:value-of select="ColData[@colID='9']/@value"/>
      </td>
      <td class="quickbooks_detail_body_spacer_column"/>
      <td class="quickbooks_detail_body_amount">
	<xsl:value-of select="ColData[@colID='10']/@value"/>
      </td>
      <td class="quickbooks_detail_body_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="SubtotalRow">
    <tr>
      <td class="quickbooks_detail_body_side_padding"/>
      <td colspan="8" class="quickbooks_detail_body_text">
	<xsl:value-of select="ColData[@colID='1']/@value"/>
      </td>
      <td class="quickbooks_detail_body_spacer_column"/>
      <td class="quickbooks_detail_body_subtotal_amount">
	<xsl:value-of select="ColData[@colID='10']/@value"/>
      </td>
      <td class="quickbooks_detail_body_side_padding"/>
    </tr>
    <tr>
      <td colspan="12" class="quickbooks_detail_body_blank">
	<hr width="10%"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="TotalRow">
    <tr>
      <td class="quickbooks_detail_footer_side_padding"/>
      <td colspan="8" class="quickbooks_detail_footer_label">
	<xsl:value-of select="ColData[@colID='1']/@value"/>
      </td>
      <td class="quickbooks_detail_footer_spacer_column"/>
      <td class="quickbooks_detail_footer_amount">
	<xsl:value-of select="ColData[@colID='10']/@value"/>
      </td>
      <td class="quickbooks_detail_footer_side_padding"/>
    </tr>
    <tr>
      <td colspan="12" class="quickbooks_detail_body_blank"/>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch pending ACH debit transactions.
  my (%ach_debit_txn_by_id, @ach_debit_txn_id_list);
  my $sql_query = "select * from ach_debit_txn_tbl where processed_dt is null and not(cancelled) order by queued_dt";
  get_row_by_id_map($dbh, $sql_query, undef, \%ach_debit_txn_by_id, \@ach_debit_txn_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch floors.
  my %floor_by_id;
  $sql_query = "select * from floor_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%floor_by_id);
  # populate variables.
  my $total_amount = 0;
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ach-debit-txn-list>
---END_BLOCK---;
  for my $ach_debit_txn_id (@ach_debit_txn_id_list) {
    my $ach_debit_txn = $ach_debit_txn_by_id{$ach_debit_txn_id};
    my $cust_acct_id = $ach_debit_txn->{"cust_acct_id"};
    my $account_name = "";
    my $floor_label = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
	my $floor_id = $cust_acct->{"primary_floor_id"};
	if ($floor_id) {
	  my $floor = $floor_by_id{$floor_id};
	  $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
	}
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $amount = HTML::Entities::encode($ach_debit_txn->{"amount"});
    $total_amount += $amount;
    my $customer_name = HTML::Entities::encode($ach_debit_txn->{"customer_name"});
    my $customer_bank_id = HTML::Entities::encode($ach_debit_txn->{"customer_bank_id"});
    my $customer_bank_acct_num = HTML::Entities::encode($ach_debit_txn->{"customer_bank_acct_num"});
    my $queued_dt = HTML::Entities::encode($ach_debit_txn->{"queued_dt"});
    my $notes = HTML::Entities::encode($ach_debit_txn->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <ach-debit-txn>
      <id>$ach_debit_txn_id</id>
      <account_name>$account_name</account_name>
      <floor_label>$floor_label</floor_label>
      <amount>$amount</amount>
      <customer_name>$customer_name</customer_name>
      <customer_bank_id>$customer_bank_id</customer_bank_id>
      <customer_bank_acct_num>$customer_bank_acct_num</customer_bank_acct_num>
      <queued_dt>$queued_dt</queued_dt>
      <notes>$notes</notes>
    </ach-debit-txn>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    <total_amount>$total_amount</total_amount>
  </ach-debit-txn-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_pending_ach_debit_txns.xslt", $cgi);
}

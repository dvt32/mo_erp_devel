<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Pending ACH Debit Transactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<script src="/1g/js/toggle_checkboxes.js">
	  &nbsp;
	</script>
      </head>
      <body>
	<xsl:apply-templates select="ach-debit-txn-list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ach-debit-txn-list">
    <form action="/1g/finance/download_pending_ach_debit_txns.cgi" method="post">
      <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	<tr>
	  <td colspan="23" class="standard_table_title">
	    Pending ACH Debit Transactions
	  </td>
	</tr>
	<tr class="standard_table_column_heading_row">
	  <td class="standard_table_side_padding"></td>
	  <td align="center">
	    <input type="checkbox" name="select_all" onclick="toggle_checkboxes(this, 'id-');"/>
	  </td>
	  <td class="standard_table_spacer_column"></td>
	  <td align="center">#</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Account Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Floor</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Amount</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Customer Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Routing Num</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Account Num</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Queued Timestamp</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Notes</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>X</td>
	  <td class="standard_table_side_padding"></td>
	</tr>
	<xsl:choose>
	  <xsl:when test="count(ach-debit-txn) > 0">
	    <xsl:apply-templates select="ach-debit-txn"/>
	    <xsl:apply-templates select="total_amount"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td colspan="21">No pending ACH debit transactions.</td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </xsl:otherwise>
	</xsl:choose>
      </table>
      <p>
	<input type="checkbox" name="mark_as_processed" checked="checked"/>Mark as processed
      </p>
      <input type="submit" name="submitButton" value="Download"/>
    </form>
  </xsl:template>

  <xsl:template match="ach-debit-txn">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
	<input>
	  <xsl:attribute name="type">checkbox</xsl:attribute>
	  <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	</input>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td align="right"><xsl:value-of select="position()"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="floor_label"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="customer_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="customer_bank_id"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="customer_bank_acct_num"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="queued_dt"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="notes"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/finance/cancel_ach_debit_txn.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:text>X</xsl:text>
	</a>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="total_amount">
    <tr>
      <xsl:choose>
	<xsl:when test="count(../ach-debit-txn) mod 2 = 1">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
      </td>
      <td class="standard_table_spacer_column"></td>
      <td align="right"></td>
      <td class="standard_table_spacer_column"></td>
      <td style="text-align: right">
	<xsl:value-of select="count(../ach-debit-txn)"/>
	items, total:
      </td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="format-number(., '###,###.00')"/></td>
      <td class="standard_table_spacer_column"></td>
      <td></td>
      <td class="standard_table_spacer_column"></td>
      <td></td>
      <td class="standard_table_spacer_column"></td>
      <td></td>
      <td class="standard_table_spacer_column"></td>
      <td></td>
      <td class="standard_table_spacer_column"></td>
      <td></td>
      <td class="standard_table_spacer_column"></td>
      <td>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;
use get_timestamp_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/show_electronic_txns");
  # fetch pending credit card transactions.
  my $sql_query = "select * from recv_payment_tbl where payment_method = ? and process_status = ? order by process_txn_after_dt, card_number";
  my @sql_param_list = ("card", "pending");
  my (%recv_payment_by_id, @recv_payment_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%recv_payment_by_id, \@recv_payment_id_list);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # fetch employees.
  my %employee_by_id;
  $sql_query = "select * from employee_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
  # determine current datetime.
  my $now_dt = get_timestamp_now();
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <recv_payment_list>
---END_BLOCK---;
  @recv_payment_id_list = sort {
    my $recv_payment_a = $recv_payment_by_id{$a};
    my $recv_payment_b = $recv_payment_by_id{$b};
    my $account_id_a = $recv_payment_a->{"cust_acct_id"};
    my $account_id_b = $recv_payment_b->{"cust_acct_id"};
    my $account_name_a = "";
    if (defined($account_id_a)) {
      if (my $account_a = $account_by_id{$account_id_a}) {
	$account_name_a = HTML::Entities::encode($account_a->{"name"});
      }
    }
    my $account_name_b = "";
    if (defined($account_id_b)) {
      if (my $account_b = $account_by_id{$account_id_b}) {
	$account_name_b = HTML::Entities::encode($account_b->{"name"});
      }
    }
    my $sort_order = 0;
    $sort_order = $recv_payment_a->{"process_txn_after_dt"} cmp $recv_payment_b->{"process_txn_after_dt"};
    $sort_order ||= $account_name_a cmp $account_name_b;
    $sort_order;
  } @recv_payment_id_list;
  for my $recv_payment_id (@recv_payment_id_list) {
    my $recv_payment = $recv_payment_by_id{$recv_payment_id};
    my $account_id = $recv_payment->{"cust_acct_id"};
    my $account_name = "";
    if (defined($account_id)) {
      if (my $account = $account_by_id{$account_id}) {
	$account_name = HTML::Entities::encode($account->{"name"});
      }
    }
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee_name = "";
    if (defined($employee_id)) {
      if (my $_employee = $employee_by_id{$employee_id}) {
	$employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
      }
    }
    my $card_number = HTML::Entities::encode($recv_payment->{"card_number"});
    substr($card_number, 1, -4) = '***************';
    my $card_expiration_month = HTML::Entities::encode($recv_payment->{"card_expiration_month"});
    my $card_expiration_year = HTML::Entities::encode($recv_payment->{"card_expiration_year"});
    my $card_bill_name = HTML::Entities::encode($recv_payment->{"card_bill_name"});
    my $card_bill_company = HTML::Entities::encode($recv_payment->{"card_bill_company"} || "");
    my $amount = HTML::Entities::encode($recv_payment->{"amount"});
    my $recv_dt = HTML::Entities::encode($recv_payment->{"recv_dt"});
    my $process_txn_after_dt = HTML::Entities::encode($recv_payment->{"process_txn_after_dt"});
    my $process_mode = HTML::Entities::encode($recv_payment->{"process_mode"});
    my $note = HTML::Entities::encode($recv_payment->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <recv_payment>
      <id>$recv_payment_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <card_bill_name>$card_bill_name</card_bill_name>
      <card_bill_company>$card_bill_company</card_bill_company>
      <amount>$amount</amount>
      <recv_dt>$recv_dt</recv_dt>
      <process_txn_after_dt>$process_txn_after_dt</process_txn_after_dt>
      <process_mode>$process_mode</process_mode>
      <note>$note</note>
    </recv_payment>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </recv_payment_list>
  <now_dt>$now_dt</now_dt>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_pending_card_txns.xslt", $cgi);
}

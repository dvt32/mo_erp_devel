#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/show_electronic_txns");
  # fetch last 100 processed ACH debit transactions.
  my (%ach_debit_txn_by_id, @ach_debit_txn_id_list);
  my $sql_query = "select * from ach_debit_txn_tbl where processed_dt is not null and not(cancelled) order by processed_dt desc, customer_name, customer_bank_id, customer_bank_acct_num limit 100";
  get_row_by_id_map($dbh, $sql_query, undef, \%ach_debit_txn_by_id, \@ach_debit_txn_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch floors.
  my %floor_by_id;
  $sql_query = "select * from floor_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%floor_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <ach-debit-txn-list>
---END_BLOCK---;
  for my $ach_debit_txn_id (@ach_debit_txn_id_list) {
    my $ach_debit_txn = $ach_debit_txn_by_id{$ach_debit_txn_id};
    my $cust_acct_id = $ach_debit_txn->{"cust_acct_id"};
    my $account_name = "";
    my $floor_label = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
	my $floor_id = $cust_acct->{"primary_floor_id"};
	if ($floor_id) {
	  my $floor = $floor_by_id{$floor_id};
	  $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
	}
      } else {
	$account_name = "(unknown account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $amount = $ach_debit_txn->{"amount"};
    my $customer_name = HTML::Entities::encode($ach_debit_txn->{"customer_name"});
    my $customer_bank_id = $ach_debit_txn->{"customer_bank_id"};
    my $customer_bank_acct_num = $ach_debit_txn->{"customer_bank_acct_num"};
    my $num_stars = length($customer_bank_acct_num) - 4;
    substr($customer_bank_acct_num, 1, $num_stars) = '*' x $num_stars;
    my $processed_dt = HTML::Entities::encode($ach_debit_txn->{"processed_dt"});
    my $notes = HTML::Entities::encode($ach_debit_txn->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <ach-debit-txn>
      <id>$ach_debit_txn_id</id>
      <account_name>$account_name</account_name>
      <floor_label>$floor_label</floor_label>
      <amount>$amount</amount>
      <customer_name>$customer_name</customer_name>
      <customer_bank_id>$customer_bank_id</customer_bank_id>
      <customer_bank_acct_num>$customer_bank_acct_num</customer_bank_acct_num>
      <processed_dt>$processed_dt</processed_dt>
      <notes>$notes</notes>
    </ach-debit-txn>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ach-debit-txn-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_processed_ach_txns.xslt", $cgi);
}

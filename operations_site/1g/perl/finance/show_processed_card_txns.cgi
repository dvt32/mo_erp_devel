#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/show_electronic_txns");
  # fetch last {n} processed credit card transactions. {n} = 100 by default.
  my $num_txns = $cgi->param("num_txns") || 100;
  if ($num_txns !~ /^\d+$/) {
    die "show_page: show_processed_card_txns_bad_num_txns.html\n";
  } else {
    $num_txns = int($num_txns);
  }
  my (%credit_card_txn_by_id, @credit_card_txn_id_list);
  my $sql_query = "select * from credit_card_txn_tbl where processed_dt is not null order by processed_dt desc, billing_name, card_number limit $num_txns";
  get_row_by_id_map($dbh, $sql_query, undef, \%credit_card_txn_by_id, \@credit_card_txn_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch floors.
  my %floor_by_id;
  $sql_query = "select * from floor_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%floor_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <num_txns>$num_txns</num_txns>
  <credit-card-txn-list>
---END_BLOCK---;
  for my $credit_card_txn_id (@credit_card_txn_id_list) {
    my $credit_card_txn = $credit_card_txn_by_id{$credit_card_txn_id};
    my $cust_acct_id = $credit_card_txn->{"cust_acct_id"};
    my $account_name = "";
    my $floor_label = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
	my $floor = $floor_by_id{$cust_acct->{"primary_floor_id"}};
	if ($floor) {
	  $floor_label = $floor->{"short_global_label"};
	}
      } else {
	$account_name = "(unknown account)";
      }
      $account_name = HTML::Entities::encode($account_name);
      $floor_label = HTML::Entities::encode($floor_label);
    }
    my $card_number = HTML::Entities::encode($credit_card_txn->{"card_number"});
    substr($card_number, 6, -4) = '******';
    my $card_expiration_month = $credit_card_txn->{"card_expiration_month"};
    my $card_expiration_year = $credit_card_txn->{"card_expiration_year"};
    my $billing_name = HTML::Entities::encode($credit_card_txn->{"billing_name"});
    my $total_charge = HTML::Entities::encode($credit_card_txn->{"total_charge"});
    my $processed_dt = HTML::Entities::encode($credit_card_txn->{"processed_dt"});
    my $response_approved = HTML::Entities::encode($credit_card_txn->{"response_approved"} || "");
    my $notes = HTML::Entities::encode($credit_card_txn->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <credit-card-txn>
      <id>$credit_card_txn_id</id>
      <account_name>$account_name</account_name>
      <floor_label>$floor_label</floor_label>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <billing_name>$billing_name</billing_name>
      <total_charge>$total_charge</total_charge>
      <processed_dt>$processed_dt</processed_dt>
      <response_approved>$response_approved</response_approved>
      <notes>$notes</notes>
    </credit-card-txn>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </credit-card-txn-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_processed_card_txns.xslt", $cgi);
}

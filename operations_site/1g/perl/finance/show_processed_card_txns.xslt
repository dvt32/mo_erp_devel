<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Last <xsl:value-of select="num_txns"/> Processed Card Transactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<script src="/1g/js/toggle_checkboxes.js">
	  &nbsp;
	</script>
      </head>
      <body>
	<form action="/1g/finance/show_processed_card_txns.cgi" method="get">
	  <p>
	    <a href="/1g/index.cgi">Main menu</a>
	    <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
	      |
	      Credit/Debit Card Transactions:
	      <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
	      &middot;
	      <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
	      &middot;
	      <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
	    </xsl:if>
	    |
	    Show:
	    <select name="num_txns">
	      <option>100</option>
	      <option>200</option>
	      <option>500</option>
	    </select>
	    Transactions
	    <input type="submit" name="submitButton" value="Go"/>
	  </p>
	<a href="/1g/finance/export_processed_card_txns.php">Export</a>
	</form>
	<xsl:apply-templates select="credit-card-txn-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="credit-card-txn-list">
    <form action="/1g/finance/act_on_processed_card_txns.cgi" method="post">
      <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	<tr>
	  <td colspan="21" class="standard_table_title">
	    Last <xsl:value-of select="../num_txns"/> Processed Card Transactions
	  </td>
	</tr>
	<tr class="standard_table_column_heading_row">
	  <td class="standard_table_side_padding"></td>
	  <td align="center">
	    <input type="checkbox" name="select_all" onclick="toggle_checkboxes(this, 'id-');"/>
	  </td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Account Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Floor</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Card Number</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Expires</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Amount</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Processed Time</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Response</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Charge</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Notes</td>
	  <td class="standard_table_side_padding"></td>
	</tr>
	<xsl:apply-templates select="credit-card-txn"/>
      </table>
      <p>
	<input type="submit" name="submitButton" value="Enter"/>
      </p>
    </form>
  </xsl:template>

  <xsl:template match="credit-card-txn">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
	<input>
	  <xsl:attribute name="type">checkbox</xsl:attribute>
	  <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	</input>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:if test="role-list/role = 'role:finance/show_electronic_txns/edit'">
	  <xsl:attribute name="href">
	    <xsl:text>/1g/finance/show_card_txn_detail.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	   </xsl:if>
	  <xsl:value-of select="account_name"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="floor_label"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="total_charge"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="substring(processed_dt, 1, 16)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a><xsl:if test="role-list/role = 'role:finance/show_electronic_txns/edit'">
	  <xsl:attribute name="href">
	    <xsl:text>/1g/finance/show_card_txn_detail.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  </xsl:if>
	  <span>
	    <xsl:if test="response_approved != 'APPROVED'">
	      <xsl:attribute name="class">response_approved_not_approved</xsl:attribute>
	    </xsl:if>
	    <xsl:choose>
	      <xsl:when test="response_approved = ''">
		<xsl:text>ERROR</xsl:text>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:value-of select="response_approved"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </span>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a><xsl:if test="role-list/role = 'role:finance/show_electronic_txns/edit'">
	  <xsl:attribute name="href">/1g/finance/receive_card_input.cgi?credit_card_txn_id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  </xsl:if>
	  <xsl:text>Charge</xsl:text>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="notes"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

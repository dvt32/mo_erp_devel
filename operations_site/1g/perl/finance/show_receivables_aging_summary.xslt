<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="QBXML">
    <xsl:apply-templates select="QBXMLMsgsRs/AgingReportQueryRs/ReportRet"/>
  </xsl:template>

  <xsl:template match="ReportRet">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Receivables Aging Summary
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<table cellspacing="0" cellpadding="0" border="0" class="quickbooks_summary">
	  <tr>
	    <td colspan="15" class="quickbooks_summary_title">
	      <xsl:value-of select="ReportTitle"/>
	      <br/>
	      <xsl:value-of select="ReportSubtitle"/>
	    </td>
	  </tr>
	  <tr class="quickbooks_summary_column_heading_row">
	    <td class="quickbooks_summary_column_header_side_padding"/>
	    <td class="quickbooks_summary_column_header_blank">
	      <xsl:value-of select="ColDesc[@colID='1']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_summary_column_header_spacer_column"/>
	    <td class="quickbooks_summary_column_header">
	      <xsl:value-of select="ColDesc[@colID='2']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_summary_column_header_spacer_column"/>
	    <td class="quickbooks_summary_column_header">
	      <xsl:value-of select="ColDesc[@colID='3']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_summary_column_header_spacer_column"/>
	    <td class="quickbooks_summary_column_header">
	      <xsl:value-of select="ColDesc[@colID='4']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_summary_column_header_spacer_column"/>
	    <td class="quickbooks_summary_column_header">
	      <xsl:value-of select="ColDesc[@colID='5']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_summary_column_header_spacer_column"/>
	    <td class="quickbooks_summary_column_header">
	      <xsl:value-of select="ColDesc[@colID='6']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_summary_column_header_spacer_column"/>
	    <td class="quickbooks_summary_column_header">
	      <xsl:value-of select="ColDesc[@colID='7']/ColTitle/@value"/>
	    </td>
	    <td class="quickbooks_summary_column_header_side_padding"/>
	  </tr>
	  <xsl:apply-templates select="ReportData"/>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ReportData">
    <xsl:apply-templates select="DataRow"/>
    <xsl:apply-templates select="TotalRow"/>
  </xsl:template>

  <xsl:template match="DataRow">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">quickbooks_summary_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">quickbooks_summary_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="quickbooks_summary_body_side_padding"/>
      <td class="quickbooks_summary_body_label">
	<xsl:choose>
	  <xsl:when test="/QBXML/QBXMLMsgsRs/CustomerQueryRs/CustomerRet/Name = RowData[@rowType='name']/@value">
	    <xsl:variable name="customer_name"><xsl:value-of select="RowData[@rowType='name']/@value"/></xsl:variable>
	    <a>
	      <xsl:attribute name="class">quickbooks_summary_body_label_link</xsl:attribute>
	      <xsl:attribute name="href">
		<xsl:text>/1g/finance/show_customer_invoices.cgi?list_id=</xsl:text>
		<xsl:value-of select="/QBXML/QBXMLMsgsRs/CustomerQueryRs/CustomerRet[Name = $customer_name]/ListID"/>
	      </xsl:attribute>
	      <xsl:value-of select="ColData[@colID='1']/@value"/>
	    </a>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="ColData[@colID='1']/@value"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="quickbooks_summary_body_spacer_column"/>
      <td class="quickbooks_summary_body_amount">
	<xsl:value-of select="ColData[@colID='2']/@value"/>
      </td>
      <td class="quickbooks_summary_body_spacer_column"/>
      <td class="quickbooks_summary_body_amount">
	<xsl:value-of select="ColData[@colID='3']/@value"/>
      </td>
      <td class="quickbooks_summary_body_spacer_column"/>
      <td class="quickbooks_summary_body_amount">
	<xsl:value-of select="ColData[@colID='4']/@value"/>
      </td>
      <td class="quickbooks_summary_body_spacer_column"/>
      <td class="quickbooks_summary_body_amount">
	<xsl:value-of select="ColData[@colID='5']/@value"/>
      </td>
      <td class="quickbooks_summary_body_spacer_column"/>
      <td class="quickbooks_summary_body_amount">
	<xsl:value-of select="ColData[@colID='6']/@value"/>
      </td>
      <td class="quickbooks_summary_body_spacer_column"/>
      <td class="quickbooks_summary_body_amount">
	<xsl:value-of select="ColData[@colID='7']/@value"/>
      </td>
      <td class="quickbooks_summary_body_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="TotalRow">
    <tr class="quickbooks_summary_footer_row">
      <td class="quickbooks_summary_footer_side_padding"/>
      <td class="quickbooks_summary_footer_label">
	<xsl:value-of select="ColData[@colID='1']/@value"/>
      </td>
      <td class="quickbooks_summary_footer_spacer_column"/>
      <td class="quickbooks_summary_footer_amount">
	<xsl:value-of select="ColData[@colID='2']/@value"/>
      </td>
      <td class="quickbooks_summary_footer_spacer_column"/>
      <td class="quickbooks_summary_footer_amount">
	<xsl:value-of select="ColData[@colID='3']/@value"/>
      </td>
      <td class="quickbooks_summary_footer_spacer_column"/>
      <td class="quickbooks_summary_footer_amount">
	<xsl:value-of select="ColData[@colID='4']/@value"/>
      </td>
      <td class="quickbooks_summary_footer_spacer_column"/>
      <td class="quickbooks_summary_footer_amount">
	<xsl:value-of select="ColData[@colID='5']/@value"/>
      </td>
      <td class="quickbooks_summary_footer_spacer_column"/>
      <td class="quickbooks_summary_footer_amount">
	<xsl:value-of select="ColData[@colID='6']/@value"/>
      </td>
      <td class="quickbooks_summary_footer_spacer_column"/>
      <td class="quickbooks_summary_footer_amount">
	<xsl:value-of select="ColData[@colID='7']/@value"/>
      </td>
      <td class="quickbooks_summary_footer_side_padding"/>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

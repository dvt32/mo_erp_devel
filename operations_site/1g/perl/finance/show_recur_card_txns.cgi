#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:finance/show_electronic_txns");
  # fetch recurring credit card transactions.
  my $sql_query = "select * from recur_card_txn_tbl";
  my %recur_card_txn_by_id;
  get_row_by_id_map($dbh, $sql_query, undef, \%recur_card_txn_by_id);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # fetch employees.
  my %employee_by_id;
  $sql_query = "select * from employee_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
  # sort recur card transactions by account name, followed by card number.
  my @recur_card_txn_id_list = sort {
    my $recur_card_txn_a = $recur_card_txn_by_id{$a};
    my $recur_card_txn_b = $recur_card_txn_by_id{$b};
    my $account_a = $account_by_id{$recur_card_txn_a->{"account_id"}};
    my $account_b = $account_by_id{$recur_card_txn_b->{"account_id"}};
    my $sort_result = lc($account_a->{"name"}) cmp lc($account_b->{"name"});
    $sort_result ||= $recur_card_txn_a->{"card_number"} cmp $recur_card_txn_b->{"card_number"};
    return $sort_result;
  } keys(%recur_card_txn_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <recur_card_txn_list>
---END_BLOCK---;
  for my $recur_card_txn_id (@recur_card_txn_id_list) {
    my $recur_card_txn = $recur_card_txn_by_id{$recur_card_txn_id};
    my $account_id = $recur_card_txn->{"account_id"};
    my $account_name = "";
    if (defined($account_id)) {
      if (my $account = $account_by_id{$account_id}) {
	$account_name = HTML::Entities::encode($account->{"name"});
      }
    }
    my $employee_id = $recur_card_txn->{"employee_id"};
    my $employee_name = "";
    if (defined($employee_id)) {
      if (my $_employee = $employee_by_id{$employee_id}) {
	$employee_name = HTML::Entities::encode($_employee->{"last_name"} . ", " . $_employee->{"first_name"});
      }
    }
    my $amount = HTML::Entities::encode($recur_card_txn->{"amount"});
    my $process_mode = HTML::Entities::encode($recur_card_txn->{"process_mode"});
    my $frequency = HTML::Entities::encode($recur_card_txn->{"frequency"});
    my $next_date = $recur_card_txn->{"next_date"};
    my $card_number = HTML::Entities::encode($recur_card_txn->{"card_number"});
    substr($card_number, 1, -4) = '***************';
    my $card_expiration_month = HTML::Entities::encode($recur_card_txn->{"card_expiration_month"});
    my $card_expiration_year = HTML::Entities::encode($recur_card_txn->{"card_expiration_year"});
    my $card_bill_name = HTML::Entities::encode($recur_card_txn->{"card_bill_name"});
    my $card_bill_company = HTML::Entities::encode($recur_card_txn->{"card_bill_company"} || "");
    my $note = HTML::Entities::encode($recur_card_txn->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <recur_card_txn>
      <id>$recur_card_txn_id</id>
      <account_name>$account_name</account_name>
      <employee_name>$employee_name</employee_name>
      <amount>$amount</amount>
      <process_mode>$process_mode</process_mode>
      <frequency>$frequency</frequency>
      <next_date>$next_date</next_date>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <card_bill_name>$card_bill_name</card_bill_name>
      <card_bill_company>$card_bill_company</card_bill_company>
      <note>$note</note>
    </recur_card_txn>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </recur_card_txn_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_recur_card_txns.xslt", $cgi);
}

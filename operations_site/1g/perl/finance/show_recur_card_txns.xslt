<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Recurring Credit Card Transactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<script src="/1g/js/toggle_checkboxes.js">
	  &nbsp;
	</script>
      </head>
      <body>
	<p>
          <a href="/1g/index.cgi">Main menu</a>
	  <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
	    |
	    Credit/Debit Card Transactions:
	    <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
	    &middot;
	    <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
	    &middot;
	    <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
	  </xsl:if>
	</p>
	<xsl:apply-templates select="recur_card_txn_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="recur_card_txn_list">
    <form action="/1g/finance/act_on_recur_card_txns.cgi" method="post">
      <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	<tr>
	  <td colspan="25">
	    <table cellspacing="0" cellpadding="0" border="0" width="100%">
	      <tr>
		<td class="standard_table_title" width="25%">&nbsp;</td>
		<td class="standard_table_title" style="text-align: center; white-space: nowrap;" width="50%">
		  Recurring Credit Card Transactions
		</td>
		<td class="standard_table_title" style="text-align: right" width="25%">
		  <a href="/1g/finance/new_recur_card_txn_input.cgi" class="bare_link" style="color: white">New</a>
		</td>
	      </tr>
	    </table>
	  </td>
	</tr>
	<tr class="standard_table_column_heading_row">
	  <td class="standard_table_side_padding"></td>
	  <td align="center">
	    <input type="checkbox" name="select_all" onclick="toggle_checkboxes(this, 'id-');"/>
	  </td>
	  <td class="standard_table_spacer_column"></td>
	  <td>ID</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Account Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Received By</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Amount</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Mode</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Frequency</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Next Date</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Card Number</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Expires</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Billing Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Note</td>
	  <td class="standard_table_side_padding"></td>
	</tr>
	<xsl:apply-templates select="recur_card_txn"/>
      </table>
      <p>
	<input type="checkbox" name="advance_next_date" value="1" checked="checked"/>
	Advance Next Date (only applies to "Enter")
      </p>
      <p>
	<input type="submit" name="submitButton" value="Enter"/>
	&nbsp;
	<input type="submit" name="submitButton" value="Delete"/>
      </p>
    </form>
  </xsl:template>

  <xsl:template match="recur_card_txn">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
	<input>
	  <xsl:attribute name="type">checkbox</xsl:attribute>
	  <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	</input>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
		
	  <xsl:attribute name="href">/1g/finance/edit_recur_card_txn_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	 
	  <xsl:value-of select="id"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="employee_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="process_mode"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="frequency"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="next_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="card_bill_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

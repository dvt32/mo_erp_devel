#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/show_payment_detail");
  my $recv_payment_id = $cgi->param("id");
  my $recv_payment;
  if ((!$recv_payment_id) || (!($recv_payment = select_row($dbh, "recv_payment_tbl", $recv_payment_id)))) {
    die "show_page: show_recv_pmt_detail_bad_id.html\n";
  }
  my $cust_acct_id = $recv_payment->{"cust_acct_id"};
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  my $employee_id = $recv_payment->{"employee_id"};
  my $linked_employee = $employee_id ? select_row($dbh, "employee_tbl", $employee_id) : undef;
  # populate information needed for display of page.
  my $account_name = $cust_acct ? $cust_acct->{"name"} : "(unknown account)";
  $account_name = HTML::Entities::encode($account_name);
  my $employee_name = $linked_employee ? qq{$linked_employee->{"first_name"} $linked_employee->{"last_name"}} : "";
  $employee_name = HTML::Entities::encode($employee_name);
  my $amount = $recv_payment->{"amount"};
  my $recv_dt = $recv_payment->{"recv_dt"};
  my $payment_method = HTML::Entities::encode($recv_payment->{"payment_method"});
  my $process_txn_after_dt = $recv_payment->{"process_txn_after_dt"};
  my $process_mode = HTML::Entities::encode($recv_payment->{"process_mode"});
  my $process_status = HTML::Entities::encode($recv_payment->{"process_status"});
  my $notify_email = HTML::Entities::encode($recv_payment->{"notify_email"} || "");
  my $client_hostname = HTML::Entities::encode($recv_payment->{"client_hostname"} || "");
  my $client_ip_addr = HTML::Entities::encode($recv_payment->{"client_ip_addr"} || "");
  my $invoice_number = HTML::Entities::encode($recv_payment->{"invoice_number"} || "");
  my $note = HTML::Entities::encode($recv_payment->{"note"} || "");
  my $is_reconciled = $recv_payment->{"is_reconciled"} ? 1 : 0;
  my $payment_destination = HTML::Entities::encode($recv_payment->{"payment_destination"} || "");
  my $check_name = HTML::Entities::encode($recv_payment->{"check_name"} || "");
  my $check_number = HTML::Entities::encode($recv_payment->{"check_number"} || "");
  my $bank_account_name = HTML::Entities::encode($recv_payment->{"bank_account_name"} || "");
  my $bank_account_type = HTML::Entities::encode($recv_payment->{"bank_account_type"} || "");
  my $bank_routing_number = HTML::Entities::encode($recv_payment->{"bank_routing_number"} || "");
  my $bank_account_number = HTML::Entities::encode($recv_payment->{"bank_account_number"} || "");
  my $card_number = HTML::Entities::encode($recv_payment->{"card_number"} || "");
  my $card_expiration_month = HTML::Entities::encode($recv_payment->{"card_expiration_month"} || "");
  my $card_expiration_year = HTML::Entities::encode($recv_payment->{"card_expiration_year"} || "");
  my $card_security_code = HTML::Entities::encode($recv_payment->{"card_security_code"} || "");
  my $card_bill_name = HTML::Entities::encode($recv_payment->{"card_bill_name"} || "");
  my $card_bill_company = HTML::Entities::encode($recv_payment->{"card_bill_company"} || "");
  my $card_bill_address_num = HTML::Entities::encode($recv_payment->{"card_bill_address_num"} || "");
  my $card_bill_zip_code = HTML::Entities::encode($recv_payment->{"card_bill_zip_code"} || "");
  my $card_bill_address_1 = HTML::Entities::encode($recv_payment->{"card_bill_address_1"} || "");
  my $card_bill_address_2 = HTML::Entities::encode($recv_payment->{"card_bill_address_2"} || "");
  my $card_bill_city = HTML::Entities::encode($recv_payment->{"card_bill_city"} || "");
  my $card_bill_state = HTML::Entities::encode($recv_payment->{"card_bill_state"} || "");
  my $card_bill_country = HTML::Entities::encode($recv_payment->{"card_bill_country"} || "");
  my $card_bill_phone_number = HTML::Entities::encode($recv_payment->{"card_bill_phone_number"} || "");
  my $card_bill_fax_number = HTML::Entities::encode($recv_payment->{"card_bill_fax_number"} || "");
  my $card_bill_email_address = HTML::Entities::encode($recv_payment->{"card_bill_email_address"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <recv-payment>
    <id>$recv_payment_id</id>
    <account_name>$account_name</account_name>
    <employee_name>$employee_name</employee_name>
    <amount>$amount</amount>
    <recv_dt>$recv_dt</recv_dt>
    <payment_method>$payment_method</payment_method>
    <process_txn_after_dt>$process_txn_after_dt</process_txn_after_dt>
    <process_mode>$process_mode</process_mode>
    <process_status>$process_status</process_status>
    <notify_email>$notify_email</notify_email>
    <client_hostname>$client_hostname</client_hostname>
    <client_ip_addr>$client_ip_addr</client_ip_addr>
    <invoice_number>$invoice_number</invoice_number>
    <note>$note</note>
    <is_reconciled>$is_reconciled</is_reconciled>
    <payment_destination>$payment_destination</payment_destination>
    <check_name>$check_name</check_name>
    <check_number>$check_number</check_number>
    <bank_account_name>$bank_account_name</bank_account_name>
    <bank_account_type>$bank_account_type</bank_account_type>
    <bank_routing_number>$bank_routing_number</bank_routing_number>
    <bank_account_number>$bank_account_number</bank_account_number>
    <card_number>$card_number</card_number>
    <card_expiration_month>$card_expiration_month</card_expiration_month>
    <card_expiration_year>$card_expiration_year</card_expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <card_bill_name>$card_bill_name</card_bill_name>
    <card_bill_company>$card_bill_company</card_bill_company>
    <card_bill_address_num>$card_bill_address_num</card_bill_address_num>
    <card_bill_zip_code>$card_bill_zip_code</card_bill_zip_code>
    <card_bill_address_1>$card_bill_address_1</card_bill_address_1>
    <card_bill_address_2>$card_bill_address_2</card_bill_address_2>
    <card_bill_city>$card_bill_city</card_bill_city>
    <card_bill_state>$card_bill_state</card_bill_state>
    <card_bill_country>$card_bill_country</card_bill_country>
    <card_bill_phone_number>$card_bill_phone_number</card_bill_phone_number>
    <card_bill_fax_number>$card_bill_fax_number</card_bill_fax_number>
    <card_bill_email_address>$card_bill_email_address</card_bill_email_address>
  </recv-payment>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_recv_pmt_detail.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Received Payment Detail
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="recv-payment"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="recv-payment">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="2" class="standard_table_title">
	  Received Payment Detail
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Received By:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="employee_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Amount:
	</td>
	<td class="map_table_value">
	  $<xsl:value-of select="amount"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Received Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="substring(recv_dt, 1, 16)"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Payment Method:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="payment_method"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Client Hostname:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="client_hostname"/> (<xsl:value-of select="client_ip_addr"/>)
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Invoice Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="invoice_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Is Reconciled?
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is_reconciled = 1">
	      Yes
	    </xsl:when>
	    <xsl:when test="is_reconciled = 0">
	      No
	    </xsl:when>
	  </xsl:choose>
	</td>
      </tr>
      <xsl:choose>
	<xsl:when test="payment_method = 'ach'">
	  <tr>
	    <td class="map_table_key">
	      Process After Time:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="substring(process_txn_after_dt, 1, 16)"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Process Mode:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="process_mode"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Process Status:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="process_status"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Bank Account Name:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="bank_account_name"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Bank Account Type:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="bank_account_type"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Bank Routing Number:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="bank_routing_number"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Bank Account Number:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="bank_account_number"/>
	    </td>
	  </tr>
	</xsl:when>
	<xsl:when test="payment_method = 'card'">
	  <tr>
	    <td class="map_table_key">
	      Process After Time:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="substring(process_txn_after_dt, 1, 16)"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Process Mode:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="process_mode"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Process Status:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="process_status"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Card Number:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_number"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Exp Date:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Security Code:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_security_code"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Name:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_name"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Company:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_company"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Address Num:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_address_num"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Zip Code:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_zip_code"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Address 1:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_address_1"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Address 2:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_address_2"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing City:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_city"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing State:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_state"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Country:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_country"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Phone Number:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_phone_number"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Fax Number:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_fax_number"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Billing Email Address:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="card_bill_email_address"/>
	    </td>
	  </tr>
	</xsl:when>
	<xsl:when test="payment_method = 'check'">
	  <tr>
	    <td class="map_table_key">
	      Check Date:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="substring(process_txn_after_dt, 1, 10)"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Check Name:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="check_name"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Check Number:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="check_number"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Payment Destination:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="payment_destination"/>
	    </td>
	  </tr>
	</xsl:when>
	<xsl:when test="payment_method = 'cash'">
	  <tr>
	    <td class="map_table_key">
	      Payment Destination:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="payment_destination"/>
	    </td>
	  </tr>
	</xsl:when>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

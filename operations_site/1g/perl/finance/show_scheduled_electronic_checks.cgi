#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;
use lead_pad;
use get_date_tomorrow;
use days_in_month;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch scheduled electronic checks.
  my (%electronic_check_by_id, @electronic_check_id_list);
  my $sql_query = "select * from electronic_check_tbl where not(processed) and not(cancelled) order by debit_date, bank_account_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%electronic_check_by_id, \@electronic_check_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch floors.
  my %floor_by_id;
  $sql_query = "select * from floor_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%floor_by_id);
  # determine tomorrow's date.
  my ($year, $month, $day) = get_date_tomorrow(1);
  my $tomorrow = "$year-$month-$day";
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <electronic-check-list>
---END_BLOCK---;
  for my $electronic_check_id (@electronic_check_id_list) {
    my $electronic_check = $electronic_check_by_id{$electronic_check_id};
    my $cust_acct_id = $electronic_check->{"cust_acct_id"};
    my $account_name = "";
    my $floor_label = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
	my $floor_id = $cust_acct->{"primary_floor_id"};
	if ($floor_id) {
	  my $floor = $floor_by_id{$floor_id};
	  $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
	}
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $bank_account_name = HTML::Entities::encode($electronic_check->{"bank_account_name"});
    my $bank_account_type = HTML::Entities::encode($electronic_check->{"bank_account_type"});
    my $bank_acct_holder_type = HTML::Entities::encode($electronic_check->{"bank_acct_holder_type"});
    my $routing_number = HTML::Entities::encode($electronic_check->{"routing_number"});
    my $account_number = HTML::Entities::encode($electronic_check->{"account_number"});
    my $amount = HTML::Entities::encode($electronic_check->{"amount"});
    my $debit_date = HTML::Entities::encode($electronic_check->{"debit_date"});
    my $invoice_number = HTML::Entities::encode($electronic_check->{"invoice_number"} || "");
    my $notes = HTML::Entities::encode($electronic_check->{"notes"});
    $generated_xml .= <<"---END_BLOCK---;";
    <electronic-check>
      <id>$electronic_check_id</id>
      <account_name>$account_name</account_name>
      <floor_label>$floor_label</floor_label>
      <bank_account_name>$bank_account_name</bank_account_name>
      <bank_account_type>$bank_account_type</bank_account_type>
      <bank_acct_holder_type>$bank_acct_holder_type</bank_acct_holder_type>
      <routing_number>$routing_number</routing_number>
      <account_number>$account_number</account_number>
      <amount>$amount</amount>
      <debit_date>$debit_date</debit_date>
      <invoice_number>$invoice_number</invoice_number>
      <notes>$notes</notes>
    </electronic-check>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </electronic-check-list>
  <tomorrow>$tomorrow</tomorrow>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_scheduled_electronic_checks.xslt", $cgi);
}

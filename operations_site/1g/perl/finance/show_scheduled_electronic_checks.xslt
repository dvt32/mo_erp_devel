<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Scheduled Electronic Checks
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<script src="/1g/js/toggle_checkboxes.js">
	  &nbsp;
	</script>
      </head>
      <body>
	<xsl:apply-templates select="electronic-check-list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="electronic-check-list">
    <form action="/1g/finance/enqueue_ach_debit_txns.cgi" method="post">
      <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	<tr>
	  <td colspan="27" class="standard_table_title">
	    Scheduled Electronic Checks
	  </td>
	</tr>
	<tr class="standard_table_column_heading_row">
	  <td class="standard_table_side_padding"></td>
	  <td align="center">
	    <input type="checkbox" name="select_all" onclick="toggle_checkboxes(this, 'id-');"/>
	  </td>
	  <td class="standard_table_spacer_column"></td>
	  <td align="center">#</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Account Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Floor</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Bnk Acc Name</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Bnk Acc Type</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Routing #</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Acct #</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Amount</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Debit Date</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Inv #</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Notes</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>X</td>
	  <td class="standard_table_side_padding"></td>
	</tr>
	<xsl:choose>
	  <xsl:when test="count(electronic-check) > 0">
	    <xsl:apply-templates select="electronic-check"/>
	    <xsl:if test="count(electronic-check[translate(debit_date,'-','0') &gt; translate(/page/tomorrow,'-','0')]) > 0">
	      <tr>
		<td colspan="25" class="standard_table_footer">
		  *Electronic checks scheduled in the future (i.e. after tomorrow) are not enqueued. Wait until the debit date.
		</td>
	      </tr>
	    </xsl:if>
	  </xsl:when>
	  <xsl:otherwise>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td colspan="25">No scheduled electronic checks.</td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </xsl:otherwise>
	</xsl:choose>
      </table>
      <p>
	<input type="submit" name="submitButton" value="Enqueue ACH Debit Transactions"/>
      </p>
    </form>
  </xsl:template>

  <xsl:template match="electronic-check">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
	<xsl:if test="translate(debit_date,'-','0') &lt;= translate(/page/tomorrow,'-','0')">
	  <input>
	    <xsl:attribute name="type">checkbox</xsl:attribute>
	    <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	  </input>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td align="right"><xsl:value-of select="position()"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="floor_label"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="bank_account_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="bank_account_type"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="routing_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell">$<xsl:value-of select="amount"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="translate(debit_date,'-','0') &gt; translate(/page/tomorrow,'-','0')">
	    <xsl:attribute name="class">debit_date_in_future</xsl:attribute>
	    <xsl:value-of select="debit_date"/>*
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="debit_date"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="invoice_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="notes"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/finance/cancel_electronic_check.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:text>X</xsl:text>
	</a>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

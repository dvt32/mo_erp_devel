#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use select_row;
use delete_row;
use update_with_statement;
use is_production_env;
use send_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:finance/undo_deposit");
  # get CGI input parameters.
  my $deposit_id = $cgi->param("id");
  my $deposit;
  if ((!$deposit_id) || (!($deposit = select_row($dbh, "deposit_tbl", $deposit_id)))) {
    die "show_page: undo_deposit_bad_id.html\n";
  }
  my $deposit_employee_id = $deposit->{"employee_id"};
  my $deposit_employee;
  if ((!$deposit_employee_id) || (!($deposit_employee = select_row($dbh, "employee_tbl", $deposit_employee_id)))) {
    die "show_page: undo_deposit_bad_employee.html\n";
  }
  # load received payment rows.
  my $sql_query = "select * from recv_payment_tbl where deposit_id = ? order by employee_id, recv_dt";
  my @sql_param_list = ([$deposit_id, SQL_INTEGER]);
  my (%recv_payment_by_id, @recv_payment_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%recv_payment_by_id, \@recv_payment_id_list);
  my @recv_payment_list = map { $recv_payment_by_id{$_} } @recv_payment_id_list;
  # begin database transaction so our updates are atomic.
  $dbh->begin_work();
  # delink received payment rows.
  my $sql_statement = "update recv_payment_tbl set deposit_id = NULL where deposit_id = ?";
  @sql_param_list = ([$deposit_id, SQL_INTEGER]);
  update_with_statement($dbh, $sql_statement, \@sql_param_list);
  # delete deposit.
  delete_row($dbh, "deposit_tbl", $deposit_id);
  # commit the transaction.
  $dbh->commit();
  # load accounts.
  my (%cust_acct_by_id);
  $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # load list of employees who can receive payment.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl where is_active and id in (select parent_id from ss_employee_x_role_tbl where element_value = ?) order by last_name, first_name";
  my @sql_param_list = ("role:finance/receive_payment");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_by_id, \@employee_id_list);
  # send out email to accounts receivable people.
  my $total_amount = 0;
  my $email_message = "";
  $email_message .= <<"---END_BLOCK---;";
The following amounts were originally marked as deposited, but now the transaction has been rolled back:

---END_BLOCK---;
  for my $recv_payment (@recv_payment_list) {
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = $cust_acct->{"name"};
    my $amount = $recv_payment->{"amount"};
    $total_amount += $amount;
    my $payment_method = $recv_payment->{"payment_method"};
    my $check_number = $recv_payment->{"check_number"};
    my $check_name = $recv_payment->{"check_name"};
    my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
    my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
    my $note = $recv_payment->{"note"};
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee = $employee_by_id{$employee_id};
    my $employee_name = $employee->{"first_name"} . " " . $employee->{"last_name"};
    $amount = sprintf("%.2f", $amount);
    $email_message .= <<"---END_BLOCK---;";
$account_name \$$amount $payment_method $check_number ($check_name) $check_date
  Received by $employee_name at $received_dt, note: $note

---END_BLOCK---;
  }
  my $num_deposit_items = scalar(@recv_payment_list);
  my $bank_name = $deposit->{"bank_name"};
  my $bank_routing_number = $deposit->{"bank_routing_number"};
  my $bank_account_number = $deposit->{"bank_account_number"};
  my $abbrev_bank_acct_num = substr($bank_account_number, length($bank_account_number) - 4);
  $total_amount = sprintf("%.2f", $total_amount);
  $email_message .= <<"---END_BLOCK---;";

Transaction rolled back: $num_deposit_items items deposited to $bank_name ***$abbrev_bank_acct_num, total: \$$total_amount
---END_BLOCK---;
  my $deposit_note = $deposit->{"note"};
  if ($deposit_note) {
    $email_message .= <<"---END_BLOCK---;";
Additional operator note: $deposit_note
---END_BLOCK---;
  }
  my $deposit_employee_name = $deposit_employee->{"first_name"} . " " . $deposit_employee->{"last_name"};
  if (is_production_env()) {
    send_email
      ("from" => "Micro Office Accounts Receivable <accounts_receivable\@microoffice.com>",
       "to" => "accounts_receivable\@microoffice.com",
       "subject" => "Undo deposit of \$$total_amount by $deposit_employee_name ($num_deposit_items items)",
       "message" => $email_message);
  }
  # populate information needed for display of page.
  $bank_name = HTML::Entities::encode($bank_name);
  my $deposit_dt = $deposit->{"deposit_dt"};
  my $deposit_employee_name = HTML::Entities::encode($deposit_employee->{"first_name"} . " " . $deposit_employee->{"last_name"});
  my $deposit_note = HTML::Entities::encode($deposit->{"note"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <receive_payment_list>
---END_BLOCK---;
  for my $recv_payment_id (@recv_payment_id_list) {
    my $recv_payment = $recv_payment_by_id{$recv_payment_id};
    my $cust_acct_id = $recv_payment->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    my $amount = $recv_payment->{"amount"};
    my $payment_method = HTML::Entities::encode($recv_payment->{"payment_method"});
    my $check_number = HTML::Entities::encode($recv_payment->{"check_number"});
    my $check_name = HTML::Entities::encode($recv_payment->{"check_name"});
    my $check_date = substr($recv_payment->{"process_txn_after_dt"}, 0, 10);
    my $received_dt = substr($recv_payment->{"recv_dt"}, 0, 16);
    my $note = HTML::Entities::encode($recv_payment->{"note"});
    my $employee_id = $recv_payment->{"employee_id"};
    my $employee = $employee_by_id{$employee_id};
    my $employee_name = HTML::Entities::encode($employee->{"first_name"} . " " . $employee->{"last_name"});
    $amount = sprintf("%.2f", $amount);
    $generated_xml .= <<"---END_BLOCK---;";
    <receive_payment>
      <account_name>$account_name</account_name>
      <amount>$amount</amount>
      <payment_method>$payment_method</payment_method>
      <check_number>$check_number</check_number>
      <check_name>$check_name</check_name>
      <check_date>$check_date</check_date>
      <received_dt>$received_dt</received_dt>
      <note>$note</note>
      <employee_name>$employee_name</employee_name>
    </receive_payment>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </receive_payment_list>
  <deposit>
    <id>$deposit_id</id>
    <deposit_dt>$deposit_dt</deposit_dt>
    <employee_id>$deposit_employee_id</employee_id>
    <employee_name>$deposit_employee_name</employee_name>
    <bank_name>$bank_name</bank_name>
    <bank_routing_number>$bank_routing_number</bank_routing_number>
    <bank_account_number>$bank_account_number</bank_account_number>
    <total_amount>$total_amount</total_amount>
    <note>$deposit_note</note>
  </deposit>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "undo_deposit.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;
use get_role_list;
use output_file;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $expense_id = $cgi->param("id");
  my $expense;
  if ((!$expense_id) || (!($expense = select_row($dbh, "expense_tbl", $expense_id)))) {
    die "show_page: view_expense_receipt_bad_id.html\n";
  }
  # get role list of current user.
  my $employee_id = $employee->{"id"};
  my $role_list = get_role_list($instance_map, $employee_id);
  # check that receipt is viewable by current user.
  if ((!grep {$_ eq "role:finance/view_all_expenses"} @$role_list) && ($expense->{"person_id"} != $employee_id)) {
    die "show_page: view_expense_receipt_not_owner.html\n";
  }
  my $file_id = $expense->{"receipt_file_id"};
  if (!$file_id) {
    die "show_page: view_expense_receipt_missing_receipt.html\n";
  }
  output_file($instance_map, $file_id, undef, undef, 1);
}

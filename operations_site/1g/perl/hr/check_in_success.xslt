<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Break-Tracking :: Check In 
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> | <a href="/1g/hr/check_out_input.cgi">Check Out</a> 
	</p>
	<xsl:apply-templates select="break-tracking"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="break-tracking">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="width: 400;">
      <tr>
	<td class="standard_table_title">
	  Employee Break-Tracking
	</td>
      </tr>
      <tr><td style="padding: 1em">
        <p>You've been successfully checked in at:  <xsl:value-of select="time"/>.</p>
	<p align="center">Don't forget to <a href="/1g/hr/check_out_input.cgi">check out</a> at the end of your break.</p>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

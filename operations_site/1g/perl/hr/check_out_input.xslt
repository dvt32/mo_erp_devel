<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Break-Tracking :: Check Out 
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> | <a href="/1g/hr/clock_in_input.cgi">Check Out</a>
	</p>
	<xsl:apply-templates select="time-tracking"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="time-tracking">
    <form action="/1g/hr/check_out_result.cgi" method="post" autocomplete="off">
      <table border="0" class="standard_table" style="width: 400px">
        <tr>
          <td class="standard_table_title">
	    Employee Break-Tracking
          </td>
        </tr>
        <tr><td style="padding: 10px 1em 4px 1em;">
	  Now click the button below to mark the current timestamp as your
          check out time:
        </td></tr>
        <tr><td>
	  <input type="submit" name="submitButton" value="Clock Out" style="margin: 0 40% 1em 40%"/>
        </td></tr>
      </table>
    </form>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

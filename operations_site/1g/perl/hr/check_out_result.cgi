#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use update_row;
use lead_pad;
use select_row_with_query;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $ip_addr = $cgi->remote_addr();
  # my $start_dt = $cgi->param("start_dt");
  # # error checking for the break time
  # if (!$start_dt) {
  #   $start_dt = 0; 
  # }
  # if ($start_dt >= 200) {
  #   die "show_page: check_out_long_break.html\n";
  # }
  # check for a clock-in from this employee without a clock-out
  my $employee_id = $employee->{"id"};
  my $sql_query = "select * from employee_break_tbl where employee_id = ? and end_dt is null";
  my @sql_param_list = ([$employee_id, SQL_INTEGER]);
  my $employee_break = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  # there aren't *any* unclosed clock-ins 
  if (!$employee_break) {
    die "show_page: check_out_no_check_ins.html\n";
  }
  my $start_dt = $employee_break->{"start_dt"};
  my $employee_break_id = $employee_break->{"id"};
  # if time in was different day, tell the employee he should manually clock out.
  $start_dt =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/;
  my ($in_year, $in_month, $in_day, $in_hour, $in_minute, $in_second) = ($1, $2, $3, $4, $5, $6);
  my ($now_year, $now_month, $now_day, $now_hour, $now_minute, $now_second) = get_datetime_now("all");
  if ("$in_year-$in_month-$in_day" ne "$now_year-$now_month-$now_day") {
    die "show_page: clock_out_go_manual.html\n";
  }
  my $timestamp_now = "$now_year-$now_month-$now_day $now_hour:$now_minute:$now_second";
  # issue the update command to the database.
  my (@sql_column_list);
  push(@sql_column_list, ["employee_id", $employee_id, SQL_INTEGER]);
  push(@sql_column_list, ["end_dt", $timestamp_now]);
  update_row($dbh, "employee_break_tbl", \@sql_column_list, $employee_break_id);
  # populate information needed for display of page.
  $start_dt = HTML::Entities::encode($start_dt);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <time-tracking>
    <end-dt>$timestamp_now</end-dt>
    <start-dt>$start_dt</start-dt>
  </time-tracking>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "check_out_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use insert_row;
use select_row_with_query;
use get_timestamp_now;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the timestamp.
  my $time_now = get_timestamp_now();
  my $ip_addr = $cgi->remote_addr();
  # check for unclosed previous clock-ins.
  my $employee_id = $employee->{"id"};
  my $sql_query = "select * from employee_time_tbl where employee_id = ? and time_out is null";
  my @sql_param_list = ([$employee_id, SQL_INTEGER]);
  if (my $employee_time = select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    my ($current_year, $current_month, $current_day) = get_date_now(1);
    my $current_date = "$current_year-$current_month-$current_day";
    if ($employee_time->{"time_in"} =~ /^$current_date/) {
      # if previous clock-in was today, display error that employee already clocked in today and so no need to clock in again.
      die "show_page: clock_in_already_clocked_in.html\n";
    } else {
      my $employee_time_id = $employee_time->{"id"};
      my $time_in = $employee_time->{"time_in"};
      my $generated_xml = "";
      $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <employee-time>
    <id>$employee_time_id</id>
    <time_in>$time_in</time_in>
  </employee-time>
</page>
---END_BLOCK---;
      # apply XSLT stylesheet and output XHTML page.
      apply_xslt_output_xhtml($generated_xml, "manual_clock_out_input.xslt", $cgi);
      return;
    }
  }
  # insert new row.
  my (@sql_column_list);
  push(@sql_column_list, ["employee_id", $employee_id, SQL_INTEGER]);
  push(@sql_column_list, ["time_in", $time_now]);
  push(@sql_column_list, ["clock_in_ip", $ip_addr]);
  insert_row($dbh, "employee_time_tbl", \@sql_column_list);
  # populate information needed for display of page.
  $time_now = HTML::Entities::encode($time_now);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <time-tracking>
    <time>$time_now</time>
  </time-tracking>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "clock_in_success.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Time-Tracking :: Clock Out 
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> | <a href="/1g/hr/clock_in_input.cgi">Clock In</a> | <a href="/1g/hr/time_report.cgi">Time Report</a>
	</p>
	<xsl:apply-templates select="time-tracking"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="time-tracking">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="width: 400px;">
      <tr>
	<td class="standard_table_title">
	  Employee Time-Tracking
	</td>
      </tr>
      <tr><td style="padding: 1em;">
      	<p>This page was loaded at <b><xsl:value-of select="time-out"/></b>. If that is not the current time, please refresh/reload this page to ensure your clock in/clock out actually gets saved to the database instead of being cached.
        </p>
        <p>
	  You've been successfully clocked out.
	  You are now in the system as having worked from:
	</p>
	<p style="text-align: center">
	  <xsl:value-of select="substring(time-in, 1, 19)"/>
	</p>
	<p style="text-align: center">
	  until
	</p>
	<p style="text-align: center">
	  <xsl:value-of select="time-out"/>
	</p>
	<p>
	  with a break of <xsl:value-of select="break"/> minutes.
	</p>
	<p>Please confirm these times for accuracy and speak to your supervisor if you notice anything incorrect.</p>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

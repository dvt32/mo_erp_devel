#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $employee_absence_id = $cgi->param("id");
  my $employee_absence;
  if ((!$employee_absence_id) || (!($employee_absence = select_row($dbh, "employee_absence_tbl", $employee_absence_id)))) {
    die "show_page: edit_vacation_not_found.html\n";
  }
  if ($employee_absence->{"employee_id"} != $employee->{"id"}) {
    die "show_page: edit_vacation_not_owner.html\n";
  }
  if ($employee_absence->{"day_type"} ne "vacation") {
    die "show_page: edit_vacation_wrong_day_type.html\n";
  }
  if ($employee_absence->{"status"} ne "pending") {
    die "show_page: edit_vacation_bad_status.html\n";
  }
  # populate information needed for display of page.
  my $now_year = (localtime(time))[5];
  $now_year += 1900;
  $employee_absence->{"absence_date"} =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  my $year = $1;
  my $month = int($2);
  my $day = int($3);
  my $note = HTML::Entities::encode($employee_absence->{"note"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <vacation>
    <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
      <month>
        <number>$i</number>
        <name>$month_name</name>
      </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </month-list>
    <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
      <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </day-list>
    <year-list>
---END_BLOCK---;
  for(my $i = $now_year - 1; $i <= $now_year + 1; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
      <year>$i</year>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </year-list>
    <id>$employee_absence_id</id>
    <year>$year</year>
    <month>$month</month>
    <day>$day</day>
    <note>$note</note>
  </vacation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_vacation_input.xslt", $cgi);
}

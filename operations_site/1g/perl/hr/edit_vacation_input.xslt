<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Vacation Day Request
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/hr/holiday_list.cgi">Holidays</a> |
	  <a href="/1g/hr/vacation_list.cgi">Vacation Days</a> |
	  <a href="/1g/hr/sick_list.cgi">Sick Days</a> |
	  <a href="/1g/hr/unpaid_list.cgi">Unpaid Days</a>
	</p>
	<xsl:apply-templates select="vacation"/>
	<p>
	  Do not enter Saturdays, Sundays, or <a href="/1g/hr/holiday_list.cgi">holidays</a>. If you need to request a stretch of
	  multiple vacation days, request them one by one.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="vacation">
    <table>
      <tr><td>
	<form action="/1g/hr/edit_vacation_result.cgi" method="post">
	  <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  </input>
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Vacation Day Request
	    </legend>
	    <table>
	      <tr>
		<td>
		  Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">month</xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/vacation/month-list/month">
		      <option>
			<xsl:if test="number = /page/vacation/month">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select>
		    <xsl:attribute name="name">day</xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/vacation/day-list/day">
		      <option>
			<xsl:if test=". = /page/vacation/day">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select>
		    <xsl:attribute name="name">year</xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/vacation/year-list/year">
		      <option>
			<xsl:if test=". = /page/vacation/year">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td>
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		  (enter a reason or note)
		</td>
	      </tr>
	      <tr>
		<td>
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

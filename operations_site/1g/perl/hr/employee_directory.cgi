#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use global_config;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load employees.
  my (%employee_by_id, @employee_id_list);
  my $sql_query = "select * from employee_tbl where in_directory and is_active order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <employee-list>
---END_BLOCK---;
  for my $employee_id (@employee_id_list) {
    my $employee = $employee_by_id{$employee_id};
    my $first_name = HTML::Entities::encode($employee->{"first_name"});
    my $last_name = HTML::Entities::encode($employee->{"last_name"});
    my $email = HTML::Entities::encode($employee->{"email"} || "");
    my $title = HTML::Entities::encode($employee->{"title"} || "");
    my $work_phone_number = HTML::Entities::encode($employee->{"work_phone_number"} || "");
    my $work_fax_number = HTML::Entities::encode($employee->{"work_fax_number"} || "");
    my $cell_phone_number = HTML::Entities::encode($employee->{"cell_phone_number"} || "");
    my $work_address_1 = HTML::Entities::encode($employee->{"work_address_1"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$employee_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
      <email>$email</email>
      <title>$title</title>
      <work_phone_number>$work_phone_number</work_phone_number>
      <work_fax_number>$work_fax_number</work_fax_number>
      <cell_phone_number>$cell_phone_number</cell_phone_number>
      <work_address_1>$work_address_1</work_address_1>
    </employee>
---END_BLOCK---;
  }
  my $b2f3_password = global_config::lookup("b2f3_password");
  my $b2f6_password = global_config::lookup("b2f6_password");
  my $b2f10_password = global_config::lookup("b2f10_password");
  my $b2f11_password = global_config::lookup("b2f11_password");
  my $b3f10_password = global_config::lookup("b3f10_password");
  my $b6f6_password = global_config::lookup("b6f6_password");
  my $b8f19_password = global_config::lookup("b8f19_password");
  my $b11f12_password = global_config::lookup("b11f12_password");
  my $b15f1_password = global_config::lookup("b15f1_password");
  my $b27f16_password = global_config::lookup("b27f16_password");
  my $b28f16_password = global_config::lookup("b28f16_password");
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <temp_email_list>
    <temp_email>
      <location>122 W 27th St, Fl 10</location>
      <email>b3f10\@microoffice.com</email>
      <password>$b3f10_password</password>
      <alias>122w27\@microoffice.com</alias>
      <phone>212-359-4209</phone>
    </temp_email>
    <temp_email>
      <location>535 Fifth Ave, Fl 16</location>
      <email>b27f16\@microoffice.com</email>
      <password>$b27f16_password</password>
      <alias>535fifth\@microoffice.com</alias>
      <phone>212-539-9523</phone>
    </temp_email>
    <temp_email>
      <location>902 Broadway, Fl 6</location>
      <email>b6f6\@microoffice.com</email>
      <password>$b6f6_password</password>
      <alias>902bwy\@microoffice.com</alias>
      <phone>212-624-9200</phone>
    </temp_email>
    <temp_email>
      <location>18 S Michigan Ave, Fl 12</location>
      <email>b14f12\@microoffice.com</email>
      <password>MOSf12b14!</password>
      <alias>18smichigan\@microoffice.com</alias>
      <phone>312-448-6170</phone>
    </temp_email>
    <temp_email>
      <location>68 Harrison Ave</location>
      <email>BostonAdmin\@coalitionspace.com</email>
      <password>$b8f19_password</password>
      <alias>n/a</alias>
      <phone>617-807-0940</phone>
    </temp_email>
    <temp_email>
      <location>95 Christopher Columbus Dr</location>
      <email>temp95cc\@coalitionspace.com</email>
      <password>$b28f16_password</password>
      <alias>n/a</alias>
      <phone>212-359-4207</phone>
    </temp_email>
  </temp_email_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "employee_directory.xslt", $cgi);
}

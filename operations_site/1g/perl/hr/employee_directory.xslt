<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Directory
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	</p>
	<xsl:apply-templates select="employee-list"/>
	<p>
	  &nbsp;
	</p>
	<xsl:apply-templates select="temp_email_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee-list">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table">
      <tr>
	<td colspan="7" class="standard_table_title">
	  Employee Directory
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td>
	  Name
	</td>
	<td>
	  Email
	</td>
	<td>
	  Title
	</td>
	<td>
	  Work Phone
	</td>
	<td>
	  Work Fax
	</td>
	<td>
	  Cell Phone
	</td>
	<td>
	  Work Address
	</td>
      </tr>
      <xsl:apply-templates select="employee"/>
    </table>
  </xsl:template>

  <xsl:template match="employee">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td>
	<a>
	  <xsl:attribute name="href">/3g_yii/web/employees/<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="last_name"/>
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="first_name"/>
	</a>
      </td>
      <td>
	<xsl:value-of select="email"/>
      </td>
      <td>
	<xsl:value-of select="title"/>
      </td>
      <td>
	<xsl:value-of select="work_phone_number"/>
      </td>
      <td>
	<xsl:value-of select="work_fax_number"/>
      </td>
      <td>
	<xsl:value-of select="cell_phone_number"/>
      </td>
      <td>
	<xsl:value-of select="work_address_1"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="temp_email_list">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table">
      <tr>
	<td colspan="5" class="standard_table_title">
	  Temp Email List
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td>
	  Location
	</td>
	<td>
	  Email
	</td>
	<td>
	  Password
	</td>
	<td>
	  Alias
	</td>
	<td>
	  Phone
	</td>
      </tr>
      <xsl:apply-templates select="temp_email"/>
    </table>
  </xsl:template>

  <xsl:template match="temp_email">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td>
	<xsl:value-of select="location"/>
      </td>
      <td>
	<xsl:value-of select="email"/>
      </td>
      <td>
	<xsl:value-of select="password"/>
      </td>
      <td>
	<xsl:value-of select="alias"/>
      </td>
      <td>
	<xsl:value-of select="phone"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

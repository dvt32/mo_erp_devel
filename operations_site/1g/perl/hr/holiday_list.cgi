#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_role_list;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = get_role_list($instance_map, $employee->{"id"});
  # get holidays in the next year.
  my (%holiday_by_id, @holiday_id_list);
  my $sql_query = "select id, name, holiday_date, extract(dow from holiday_date) as day_of_week, note from holiday_tbl where holiday_date >= current_date and holiday_date <= current_date + interval '1 year' order by holiday_date";
  get_row_by_id_map($dbh, $sql_query, undef, \%holiday_by_id, \@holiday_id_list);
  # populate information needed for display of page.
  my @day_of_week_list = (qw(Sunday Monday Tuesday Wednesday Thursday Friday Saturday));
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <holiday-list>
---END_BLOCK---;
  for my $holiday_id (@holiday_id_list) {
    my $holiday = $holiday_by_id{$holiday_id};
    my $name = HTML::Entities::encode($holiday->{"name"});
    my $holiday_date = $holiday->{"holiday_date"};
    my $day_of_week = $day_of_week_list[$holiday->{"day_of_week"}];
    my $note = HTML::Entities::encode($holiday->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
     <holiday>
       <id>$holiday_id</id>
       <name>$name</name>
       <holiday_date>$holiday_date</holiday_date>
       <day_of_week>$day_of_week</day_of_week>
       <note>$note</note>
     </holiday>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </holiday-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "holiday_list.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Holiday List
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/hr/holiday_list.cgi">Holidays</a> |
	  <a href="/1g/hr/vacation_list.cgi">Vacation Days</a> |
	  <a href="/1g/hr/sick_list.cgi">Sick Days</a> |
	  <a href="/1g/hr/unpaid_list.cgi">Unpaid Days</a>
	  <xsl:if test="role-list/role = 'role:hr/review_absences'">
	    |
	    <a href="/1g/hr/show_absences.cgi">Review Absences</a>
	  </xsl:if>
	</p>
	<p>The following holidays are observed by federal government agencies and by many businesses:</p>
	<table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	  <tr>
	    <td colspan="7" class="standard_table_title">
	      Names of Holidays
	    </td>
	  </tr>
	  <tr class="standard_table_column_heading_row">
	    <th class="standard_table_side_padding"></th>
	    <th>Holiday Name</th>
	    <th class="standard_table_spacer_column"></th>
	    <th>Federal</th>
	    <th class="standard_table_spacer_column"></th>
	    <th>Business</th>
	    <th class="standard_table_side_padding"></th>
	  </tr>
	  <tr class="standard_table_row_odd">
	    <td class="standard_table_side_padding"></td>
	    <td>Christmas</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_even">
	    <td class="standard_table_side_padding"></td>
	    <td>Columbus Day</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center"></td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_odd">
	    <td class="standard_table_side_padding"></td>
	    <td>Independence Day</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_even">
	    <td class="standard_table_side_padding"></td>
	    <td>Labor Day</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_odd">
	    <td class="standard_table_side_padding"></td>
	    <td>Martin Luther King Jr.'s Birthday</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center"></td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_even">
	    <td class="standard_table_side_padding"></td>
	    <td>Memorial Day</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_odd">
	    <td class="standard_table_side_padding"></td>
	    <td>New Year's Day</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_even">
	    <td class="standard_table_side_padding"></td>
	    <td>President's Day</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center"></td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_odd">
	    <td class="standard_table_side_padding"></td>
	    <td>Thanksgiving</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr class="standard_table_row_even">
	    <td class="standard_table_side_padding"></td>
	    <td>Veteran's Day</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center">x</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="text-align: center"></td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</table>
	<p>
	  Micro Office Solutions provides paid holidays to W-2 employees for those days listed under the "Business" column. Note that paid holidays do not count
	  towards overtime calculations.
	</p>
	<xsl:apply-templates select="holiday-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="holiday-list">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap; border-collapse: collapse">
      <tr>
        <td class="standard_table_title" colspan="9">
          Upcoming Paid Holidays
        </td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<th class="standard_table_side_padding"></th>
	<th>Holiday Name</th>
	<th class="standard_table_spacer_column"></th>
	<th>Date</th>
	<th class="standard_table_spacer_column"></th>
	<th>Day of Week</th>
	<th class="standard_table_spacer_column"></th>
	<th>Note</th>
	<th class="standard_table_side_padding"></th>
      </tr>
      <xsl:apply-templates select="holiday"/>
    </table>
  </xsl:template>

  <xsl:template match="holiday">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td><xsl:value-of select="name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="holiday_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td style="text-align: center"><xsl:value-of select="day_of_week"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

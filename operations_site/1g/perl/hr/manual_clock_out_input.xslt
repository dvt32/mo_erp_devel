<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Time-Tracking :: Manual Clock Out
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<xsl:call-template name="validation-js"/>  
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> | <a href="/1g/hr/clock_in_input.cgi">Clock In</a>
	</p>
	<xsl:apply-templates select="employee-time"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="validation-js">
    <script type="text/javascript">
      function validate_input() {
        var str = document.forms[0].elements[0].value;
        var int = parseInt(str).toString(); 
        if (0 != str.length) {
          if (str.length != int.length || int == "NaN") {
            alert("Please enter only a numerical value for your break time.")
            return false;
          }
        }
        return true;
      }
    </script>
  </xsl:template>

  <xsl:template match="employee-time">
    <form action="/1g/hr/manual_clock_out_result.cgi" method="post" autocomplete="off" onsubmit="return validate_input()">
      <input type="hidden" name="employee_time_id">
	<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
      </input>
      <table border="0" class="standard_table" style="width: 400px">
	<tr>
	  <td class="standard_table_title">
	    Manual Clock Out
	  </td>
	</tr>
	<tr><td style="padding: 1em">
	  It appears you forgot to clock out the last time you worked.
	  <br />
	  You last clocked in at <xsl:value-of select="substring(time_in, 1, 19)"/>.
	  <br />
	  Please enter the time you left work on <xsl:value-of select="substring(time_in, 1, 10)"/>.
	  <br />
	  Enter time in 24 hour format (17:30 = 5:30 PM).
	</td></tr>
	<tr><td>
	  <p style="text-align: center">
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">time_out_hour</xsl:attribute>
	      <xsl:attribute name="size">2</xsl:attribute>
	      <xsl:attribute name="maxlength">2</xsl:attribute>
	    </input>
	    :
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">time_out_minute</xsl:attribute>
	      <xsl:attribute name="size">2</xsl:attribute>
	      <xsl:attribute name="maxlength">2</xsl:attribute>
	    </input>
	  </p>
	  <p style="text-align: center">If you took a lunch, please note your break time in minutes:</p>
	  <p style="text-align: center">
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">break</xsl:attribute>
	      <xsl:attribute name="size">3</xsl:attribute>
	      <xsl:attribute name="maxlength">3</xsl:attribute>
	    </input>
	  </p>
	  <p style="text-align: center">Now click the button below to record the entry:</p>
	  <p style="text-align: center">
	    <input type="submit" name="submitButton" value="Clock Out"/>
	  </p>
	  <p style="text-align: center">
	    This record is tagged as manually entered by you for later review by a supervisor to determine accuracy.
	  </p>
	</td></tr>
      </table>
    </form>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

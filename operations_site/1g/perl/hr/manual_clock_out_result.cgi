#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the timestamp
  my $ip_addr = $cgi->remote_addr();
  my $employee_time_id = $cgi->param("employee_time_id");
  my $time_out_hour = $cgi->param("time_out_hour");
  my $time_out_minute = $cgi->param("time_out_minute");
  my $break = $cgi->param("break");
  # error checking input.
  if ((!$time_out_hour) || ($time_out_hour < 0) || ($time_out_hour > 23) ||
      (!$time_out_minute) || ($time_out_minute < 0) || ($time_out_minute > 59) ||
      (($time_out_hour == 0) && ($time_out_minute == 0))) {
    die "show_page: manual_clock_out_bad_time.html\n";
  }
  if(!$break) {
    $break = 0; 
  }
  if($break >= 200) {
    die "show_page: clock_out_long_lunch.html\n";
  }
  # check for a clock-in from this employee without a clock-out
  my $employee_id = $employee->{"id"};
  my $employee_time = select_row($dbh, "employee_time_tbl", $employee_time_id);
  if ((!$employee_time) || (defined($employee_time->{"time_out"})) || ($employee_time->{"employee_id"} != $employee_id)) {
    die "show_page: manual_clock_out_row_not_found.html\n";
  }
  my $time_in = $employee_time->{"time_in"};
  $time_in =~ /^(\d{4}-\d\d-\d\d)/;
  my $date = $1;
  my $time_out = $date . " " . lead_pad($time_out_hour, 2) . ":" . lead_pad($time_out_minute, 2) . ":00";
  # issue the update command to the database.
  my (@sql_column_list);
  push(@sql_column_list, ["time_out", $time_out]);
  push(@sql_column_list, ["break_min", $break, SQL_INTEGER]);
  push(@sql_column_list, ["clock_out_ip", $ip_addr]);
  push(@sql_column_list, ["is_manual_clock_out", 1, SQL_BOOLEAN]);
  update_row($dbh, "employee_time_tbl", \@sql_column_list, $employee_time_id);
  # populate information needed for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <employee-time>
    <time_in>$time_in</time_in>
    <time_out>$time_out</time_out>
    <break>$break</break>
  </employee-time>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "manual_clock_out_success.xslt", $cgi);
}

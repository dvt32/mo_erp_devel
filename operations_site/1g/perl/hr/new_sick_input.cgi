#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # populate information needed for display of page.
  my $now_year = (localtime(time))[5];
  $now_year += 1900;
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <form>
    <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = HTML::Entities::encode(get_month_name($i));
    $generated_xml .= <<"---END_BLOCK---;";
      <month>
        <number>$i</number>
        <name>$month_name</name>
      </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </month-list>
    <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
      <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </day-list>
    <year-list>
---END_BLOCK---;
  for(my $i = $now_year - 1; $i <= $now_year + 1; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
      <year>$i</year>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </year-list>
  </form>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_sick_input.xslt", $cgi);
}

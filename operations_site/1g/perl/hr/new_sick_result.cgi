#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use day_of_week;
use lead_pad;
use select_row_with_query;
use insert_row;
use day_of_week;
use get_date_now;
use send_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my ($month, $day, $year) = map {$cgi->param($_)} (qw(month day year));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # check that date is valid.
  if (($month < 1) || ($month > 12) || (length($month) > 2) || ($day < 1) || ($day > 31) || (length($day) > 2) ||
      ($year < 2008) || ($year > 3000) || (length($year) > 4)) {
    die "show_page: edit_sick_bad_date.html\n";
  }
  # check that date is in the future.
  my $sick_date = $year . "-" . lead_pad($month, 2) . "-" . lead_pad($day, 2);
  my ($now_year, $now_month, $now_day) = get_date_now(1);
  my $now_date = "$now_year-$now_month-$now_day";
  if ($sick_date gt $now_date) {
    die "show_page: edit_sick_date_in_future.html\n";
  }
  # check that date is not a Saturday or Sunday.
  my $day_of_week = day_of_week($month, $day, $year);
  if (($day_of_week == 0) || ($day_of_week == 6)) {
    die "show_page: edit_sick_bad_day_of_week.html\n";
  }
  # check that date is not a holiday.
  my $sql_query = "select * from holiday_tbl where holiday_date = ?";
  my @sql_param_list = ($sick_date);
  if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    die "show_page: edit_sick_already_holiday.html\n";
  }
  # begin transaction of inserting the new row.
  $dbh->begin_work();
  # wrap in an eval to catch exceptions.
  eval {
    # raise an exception if there is a database error (such as transaction error).
    local $dbh->{"RaiseError"} = 1;
    # check if the date has already been requested by the employee.
    $sql_query = "select * from employee_absence_tbl where employee_id = ? and absence_date = ?";
    @sql_param_list = ([$employee->{"id"}, SQL_INTEGER], $sick_date);
    if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
      die "duplicate_request\n";
    }
    # insert new row.
    my $db_charset = get_database_charset($instance_map, "main");
    my @sql_column_list;
    push(@sql_column_list, ["employee_id", $employee->{"id"}, SQL_INTEGER]);
    push(@sql_column_list, ["absence_date", $sick_date]);
    push(@sql_column_list, ["day_type", "sick"]);
    push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]) if ($note);
    insert_row($dbh, "employee_absence_tbl", \@sql_column_list);
    # commit the transaction.
    $dbh->commit();
  };
  my $transaction_error = $@;
  if ($transaction_error) {
    # remove trailing newline from exception message.
    chomp($transaction_error);
    # now rollback to undo the incomplete changes but do it in an eval{} as it may also fail.
    eval { $dbh->rollback(); };
  }
  # if error, display appropriate page.
  if (!$transaction_error) {
    # do nothing. continue with script.
  } elsif ($transaction_error eq "duplicate_request") {
    die "show_page: edit_sick_duplicate_request.html\n";
  } else {
    die $transaction_error;
  }
  # send notification via SMTP.
  my $message .= <<"---END_BLOCK---;";
Sick day $sick_date requested by $employee->{"first_name"} $employee->{"last_name"}.
---END_BLOCK---;
  send_email
    ("from" => "Micro Office Operations <webmaster\@microoffice.com>",
     "to" => "hr\@microoffice.com",
     "subject" => qq{Sick day $sick_date requested by $employee->{"first_name"} $employee->{"last_name"}},
     "message" => $message);
  # populate information needed for display of page.
  $note = HTML::Entities::encode($note || "");
  my $day_of_week = day_of_week($month, $day, $year);
  my @day_name_list = (qw(Sunday Monday Tuesday Wednesday Thursday Friday Saturday));
  my $day_of_week_name = $day_name_list[$day_of_week];
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sick>
    <sick_date>$sick_date</sick_date>
    <day_of_week>$day_of_week_name</day_of_week>
    <note>$note</note>
  </sick>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_sick_result.xslt", $cgi);
}

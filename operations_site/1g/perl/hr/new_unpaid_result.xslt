<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Request Unpaid Day
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/hr/holiday_list.cgi">Holidays</a> |
	  <a href="/1g/hr/vacation_list.cgi">Vacation Days</a> |
	  <a href="/1g/hr/sick_list.cgi">Sick Days</a> |
	  <a href="/1g/hr/unpaid_list.cgi">Unpaid Days</a>
	</p>
	<xsl:apply-templates select="unpaid"/>
	<p>If you have a stretch of multiple days, <a href="/1g/hr/new_unpaid_input.cgi">request another unpaid day</a>. Make
	sure that in addition to entering your unpaid dates here, you inform your manager that you have requested one or more
	unpaid days. Your manager will approve or deny the request(s) and respond to you.</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="unpaid">
    <table class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="2">
	  Requested Unpaid Date
        </td>
      </tr>
      <tr><td colspan="2">You requested the following unpaid day:</td></tr>
      <tr>
	<td><b>Date: </b></td>
	<td><xsl:value-of select="unpaid_date"/> (<xsl:value-of select="day_of_week"/>)</td>
      </tr>
      <tr>
	<td><b>Note: </b></td>
	<td><xsl:value-of select="note"/></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

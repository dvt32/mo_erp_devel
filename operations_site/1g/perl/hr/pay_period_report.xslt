<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Time-Tracking :: Pay Period Report
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/hr/clock_in_input.cgi">Clock In</a> |
	  <a href="/1g/hr/clock_out_input.cgi">Clock Out</a> |
	  <a href="/1g/hr/time_report.cgi">Time Report</a>
	  <xsl:if test="role-list/role = 'role:hr/view_all_time_sheets'">
	    |
	    <a href="/1g/hr/today_employee_times.cgi">Today's Employee Times</a>
	  </xsl:if>
	</p>
	<xsl:apply-templates select="employee_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee_list">
    <xsl:apply-templates select="employee"/>
  </xsl:template>

  <xsl:template match="employee">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap; border-collapse: collapse">
      <tr>
        <td class="standard_table_title" colspan="12">
          Time Report for
	  <xsl:value-of select="first_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="last_name"/>
        </td>
      </tr>
      <tr align="center" bgcolor="lightblue">
	<td></td>
	<td></td>
	<td></td>
	<td>Sun</td>
	<td>Mon</td>
	<td>Tue</td>
	<td>Wed</td>
	<td>Thu</td>
	<td>Fri</td>
	<td>Sat</td>
	<td></td>
	<td></td>
      </tr>
      <xsl:apply-templates select="time_report"/>
    </table>
    <br/>
  </xsl:template>

  <xsl:template match="week">
    <tr>
      <xsl:apply-templates select="day"/>
    </tr>
    <xsl:if test="position() &lt; last()">
      <tr>
	<td colspan="12"><hr/></td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template match="day">
    <xsl:if test="position() mod 7 = 1">
      <td align="center" valign="top">
	<br />
	week of
	<br />
	<xsl:value-of select="date"/>
	<br />
	to
	<br />
	<xsl:value-of select="following-sibling::day[6]/date"/>
      </td>
      <td align="center">
	|<br />|<br />|<br />|<br />|
      </td>
      <td align="right">
	date<br />in<br />out<br />break<br />net
      </td>
    </xsl:if>
    <td align="center">
      <xsl:choose>
	<xsl:when test="is_empty = 1">
	  <xsl:choose>
	    <xsl:when test="is_holiday = 1">
	      <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	      <br />
	      time<br />
	      off<br />
	      holi-<br />
	      day<br />
	    </xsl:when>
	    <xsl:when test="employee_absence_type = 'vacation'">
	      <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	      <br />
	      time<br />
	      off<br />
	      vaca-<br />
	      tion<br />
	    </xsl:when>
	    <xsl:when test="employee_absence_type = 'sick'">
	      <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	      <br />
	      time<br />
	      off<br />
	      sick<br />
	      day<br />
	    </xsl:when>
	    <xsl:when test="employee_absence_type = 'unpaid'">
	      <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	      <br />
	      time<br />
	      off<br />
	      un-<br />
	      paid<br />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	      <br />
	      <br />
	      <br />
	      <br />
	      <br />
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when>
	<xsl:when test="is_bad_end">
	  <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	  <br />
	  bad
	  <br />
	  end
	  <br />
	  time
	  <br />
	  <br />
	</xsl:when>
	<xsl:when test="is_incomplete_day">
	  <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	  <br />
	  <xsl:value-of select="substring(time_in, 12, 5)"/>
	  <br />
	  <br />
	  <br />
	  <br />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="substring(date, 6, 2)"/>/<xsl:value-of select="substring(date, 9, 2)"/>
	  <br />
	  <xsl:value-of select="substring(time_in, 12, 5)"/>
	  <br />
	  <xsl:value-of select="substring(time_out, 12, 5)"/>
	  <br />
	  <xsl:value-of select="break_min"/>
	  <br />
	  <xsl:value-of select="net_duration"/>
	  <xsl:if test="is_holiday = 1">
	    H
	  </xsl:if>
	</xsl:otherwise>
      </xsl:choose>
    </td>
    <xsl:if test="position() mod 7 = 0">
      <td align="center">
	|<br />|<br />|<br />|<br />|
      </td>
      <td valign="top">
	<xsl:choose>
	  <xsl:when test="sum(paid_absence_days) + sum(preceding-sibling::day[position() &lt; 7]/paid_absence_days) &gt; 0">
	    paid time off<br />
	    <p style="text-align: center; margin: 0"><xsl:value-of select="sum(paid_absence_days) + sum(preceding-sibling::day[position() &lt; 7]/paid_absence_days)"/> day(s)</p>
	  </xsl:when>
	  <xsl:otherwise>
	    <br />
	    <br />
	  </xsl:otherwise>
	</xsl:choose>
	<br />
	weekly total
	<br />
	<p style="text-align: center; margin: 0">
	  <xsl:value-of select="format-number((sum(net_duration) + sum(preceding-sibling::day[position() &lt; 7]/net_duration)) div 60, '#.##')"/>
	  hours
	</p>
      </td>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

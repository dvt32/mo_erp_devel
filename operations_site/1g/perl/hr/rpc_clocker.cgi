#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Encode ();
use URI::Escape;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use get_cgi_input_charset;
use get_database_charset;
use select_row_with_query;
use insert_row;
use update_row;
use get_timestamp_now;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $ssn = Encode::decode($cgi_input_charset, $cgi->param("ssn"));
  my $phone_pin = Encode::decode($cgi_input_charset, $cgi->param("phone_pin"));
  my $break = Encode::decode($cgi_input_charset, $cgi->param("break"));
  my $action = Encode::decode($cgi_input_charset, $cgi->param("action"));
  my $caller_id_number = Encode::decode($cgi_input_charset, $cgi->param("caller_id_number"));
  print $cgi->header("-type" => "text/plain");
  # missing or bad ssn. display error.
  if ((!$ssn) || ($ssn !~ /^\d+$/)) {
    print encode_response("error" => "bad_ssn_or_pin");
    return;
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my $sql_query = "select * from employee_tbl where substring(tax_id from '....\$') = ? and length(phone_pin) > 0 and is_active";
  my @sql_param_list = (Encode::encode($db_charset, $ssn));
  my $employee = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  # no row with matching ssn found, or employee does not have phone access, or user specified bad phone pin. display error.
  if ((!$employee) || (!$employee->{"phone_pin"}) || ($phone_pin ne $employee->{"phone_pin"})) {
    print encode_response("error" => "bad_ssn_or_pin");
    return;
  }
  my $employee_id = $employee->{"id"};
  # check whether employee clocked in today.
  my ($now_year, $now_month, $now_day, $now_hour, $now_minute, $now_second) = get_datetime_now();
  my $employee_time;
  if ($now_hour < 4) {
    # if it is before 4 AM, check today first.
    $sql_query = "select * from employee_time_tbl where employee_id = ? and date_trunc('day', time_in) = current_date";
    @sql_param_list = ([$employee_id, SQL_INTEGER]);
    $employee_time = select_row_with_query($dbh, $sql_query, \@sql_param_list);
    if (!$employee_time) {
      # if it is currently before 4 AM, next check previous day because employee work hours might have crossed midnight, but
      # only check entries that have no clock out value.
      $sql_query = "select * from employee_time_tbl where employee_id = ? and time_out is null and date_trunc('day', time_in) = current_date - interval '1 day'";
      @sql_param_list = ([$employee_id, SQL_INTEGER]);
      $employee_time = select_row_with_query($dbh, $sql_query, \@sql_param_list);
    }
  } else {
    $sql_query = "select * from employee_time_tbl where employee_id = ? and date_trunc('day', time_in) = current_date";
    @sql_param_list = ([$employee_id, SQL_INTEGER]);
    $employee_time = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  }
  if (!$action) {
    if ($employee_time) {
      if ($employee_time->{"time_out"}) {
	print encode_response("status" => "clocked_in_and_out");
      } else {
	print encode_response("status" => "clocked_in");
      }
      return;
    } else {
      print encode_response("status" => "not_clocked_in");
      return;
    }
  } elsif ($action eq "clock_in") {
    if ($employee_time) {
      print encode_response("response_code" => "error", "error" => "already_clocked_in");
      return;
    }
    my $time_now = get_timestamp_now();
    # insert new row.
    my (@sql_column_list);
    push(@sql_column_list, ["employee_id", $employee_id, SQL_INTEGER]);
    push(@sql_column_list, ["time_in", $time_now]);
    push(@sql_column_list, ["clock_in_caller_id_number", $caller_id_number]);
    insert_row($dbh, "employee_time_tbl", \@sql_column_list);
    print encode_response("response_code" => "success");
    return;
  } elsif ($action eq "clock_out") {
    if (!$employee_time) {
      print encode_response("error" => "not_clocked_in");
      return;
    } elsif ($employee_time->{"time_out"}) {
      print encode_response("error" => "already_clocked_in_and_out");
      return;
    }
    my $time_now = get_timestamp_now();
    # issue the update command to the database.
    my (@sql_column_list);
    push(@sql_column_list, ["employee_id", $employee_id, SQL_INTEGER]);
    push(@sql_column_list, ["time_out", $time_now]);
    push(@sql_column_list, ["break_min", $break, SQL_INTEGER]);
    push(@sql_column_list, ["clock_out_caller_id_number", $caller_id_number]);
    update_row($dbh, "employee_time_tbl", \@sql_column_list, $employee_time->{"id"});
    print encode_response("response_code" => "success");
    return;
  } else {
    print encode_response("error" => "bad_action");
    return;
  }
}

sub encode_response(%) {
  my %param_map = @_;
  return join("&", map {uri_escape($_) . "=" . uri_escape($param_map{$_})} keys(%param_map));
}

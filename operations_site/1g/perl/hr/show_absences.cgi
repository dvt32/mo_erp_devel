#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use authorize_role;
use get_row_by_id_map;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:hr/review_absences");
  my $sql_query = "select * from employee_absence_tbl where status = ? and absence_date > now() - interval '6 weeks' and absence_date < now() + '16 weeks'";
  my @sql_param_list = ("approved");
  my %approved_employee_absence_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%approved_employee_absence_by_id);
  $sql_query = "select * from employee_absence_tbl where status = ?";
  @sql_param_list = ("pending");
  my %pending_employee_absence_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%pending_employee_absence_by_id);
  # fetch all employees.
  my (%employee_by_id);
  $sql_query = "select * from employee_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id);
  # partition employee absences into past and future, and sort by date, then employee.
  my ($today_year, $today_month, $today_day) = get_date_now(1);
  my $today_date = "$today_year-$today_month-$today_day";
  my (@past_employee_absence_id_list, @future_employee_absence_id_list);
  for my $employee_absence_id (keys(%approved_employee_absence_by_id)) {
    my $employee_absence = $approved_employee_absence_by_id{$employee_absence_id};
    my $absence_date = $employee_absence->{"absence_date"};
    if ($absence_date lt $today_date) {
      push(@past_employee_absence_id_list, $employee_absence_id);
    } else {
      push(@future_employee_absence_id_list, $employee_absence_id);
    }
  }
  @past_employee_absence_id_list = sort {
    my $employee_absence_a = $approved_employee_absence_by_id{$a};
    my $employee_absence_b = $approved_employee_absence_by_id{$b};
    my $sort_value = $employee_absence_a->{"absence_date"} cmp $employee_absence_b->{"absence_date"};
    if (!$sort_value) {
      my $employee_a = $employee_by_id{$employee_absence_a->{"employee_id"}};
      my $employee_b = $employee_by_id{$employee_absence_b->{"employee_id"}};
      $sort_value = lc($employee_a->{"last_name"}) cmp lc($employee_b->{"last_name"});
      $sort_value ||= lc($employee_a->{"first_name"}) cmp lc($employee_b->{"first_name"});
    }
    return $sort_value;
  } @past_employee_absence_id_list;
  @future_employee_absence_id_list = sort {
    my $employee_absence_a = $approved_employee_absence_by_id{$a};
    my $employee_absence_b = $approved_employee_absence_by_id{$b};
    my $sort_value = $employee_absence_a->{"absence_date"} cmp $employee_absence_b->{"absence_date"};
    if (!$sort_value) {
      my $employee_a = $employee_by_id{$employee_absence_a->{"employee_id"}};
      my $employee_b = $employee_by_id{$employee_absence_b->{"employee_id"}};
      $sort_value = lc($employee_a->{"last_name"}) cmp lc($employee_b->{"last_name"});
      $sort_value ||= lc($employee_a->{"first_name"}) cmp lc($employee_b->{"first_name"});
    }
    return $sort_value;
  } @future_employee_absence_id_list;
  my @pending_employee_absence_id_list = sort {
    my $employee_absence_a = $pending_employee_absence_by_id{$a};
    my $employee_absence_b = $pending_employee_absence_by_id{$b};
    my $sort_value = $employee_absence_a->{"absence_date"} cmp $employee_absence_b->{"absence_date"};
    if (!$sort_value) {
      my $employee_a = $employee_by_id{$employee_absence_a->{"employee_id"}};
      my $employee_b = $employee_by_id{$employee_absence_b->{"employee_id"}};
      $sort_value = lc($employee_a->{"last_name"}) cmp lc($employee_b->{"last_name"});
      $sort_value ||= lc($employee_a->{"first_name"}) cmp lc($employee_b->{"first_name"});
    }
    return $sort_value;
  } keys(%pending_employee_absence_by_id);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <past_employee_absence_list>
---END_BLOCK---;
  for my $employee_absence_id (@past_employee_absence_id_list) {
    my $employee_absence = $approved_employee_absence_by_id{$employee_absence_id};
    my $employee = $employee_by_id{$employee_absence->{"employee_id"}};
    my $employee_name = HTML::Entities::encode($employee->{"last_name"} . ", " . $employee->{"first_name"});
    my $absence_date = $employee_absence->{"absence_date"};
    my $day_type = HTML::Entities::encode($employee_absence->{"day_type"});
    my $note = HTML::Entities::encode($employee_absence->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <employee_absence>
      <employee_name>$employee_name</employee_name>
      <absence_date>$absence_date</absence_date>
      <day_type>$day_type</day_type>
      <note>$note</note>
    </employee_absence>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </past_employee_absence_list>
  <future_employee_absence_list>
---END_BLOCK---;
  for my $employee_absence_id (@future_employee_absence_id_list) {
    my $employee_absence = $approved_employee_absence_by_id{$employee_absence_id};
    my $employee = $employee_by_id{$employee_absence->{"employee_id"}};
    my $employee_name = HTML::Entities::encode($employee->{"last_name"} . ", " . $employee->{"first_name"});
    my $absence_date = $employee_absence->{"absence_date"};
    my $day_type = HTML::Entities::encode($employee_absence->{"day_type"});
    my $note = HTML::Entities::encode($employee_absence->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <employee_absence>
      <employee_name>$employee_name</employee_name>
      <absence_date>$absence_date</absence_date>
      <day_type>$day_type</day_type>
      <note>$note</note>
    </employee_absence>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </future_employee_absence_list>
  <pending_employee_absence_list>
---END_BLOCK---;
  for my $employee_absence_id (@pending_employee_absence_id_list) {
    my $employee_absence = $pending_employee_absence_by_id{$employee_absence_id};
    my $employee = $employee_by_id{$employee_absence->{"employee_id"}};
    my $employee_name = HTML::Entities::encode($employee->{"last_name"} . ", " . $employee->{"first_name"});
    my $absence_date = $employee_absence->{"absence_date"};
    my $day_type = HTML::Entities::encode($employee_absence->{"day_type"});
    my $note = HTML::Entities::encode($employee_absence->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <employee_absence>
      <employee_name>$employee_name</employee_name>
      <absence_date>$absence_date</absence_date>
      <day_type>$day_type</day_type>
      <note>$note</note>
    </employee_absence>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </pending_employee_absence_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_absences.xslt", $cgi);
}

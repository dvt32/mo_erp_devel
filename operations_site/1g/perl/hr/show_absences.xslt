<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Time-Tracking :: Review Absences
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/hr/holiday_list.cgi">Holidays</a> |
	  <a href="/1g/hr/vacation_list.cgi">Vacation Days</a> |
	  <a href="/1g/hr/sick_list.cgi">Sick Days</a> |
	  <a href="/1g/hr/unpaid_list.cgi">Unpaid Days</a>
	  <xsl:if test="role-list/role = 'role:hr/review_absences'">
	    |
	    <a href="/1g/hr/show_absences.cgi">Review Absences</a>
	  </xsl:if>
	</p>
	<xsl:apply-templates select="past_employee_absence_list"/>
	<br/>
	<xsl:apply-templates select="future_employee_absence_list"/>
	<br/>
	<xsl:apply-templates select="pending_employee_absence_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="past_employee_absence_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="9">
          Past Employee Absences (past 6 weeks)
        </td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Employee</td>
	<td class="standard_table_spacer_column"></td>
	<td>Absence Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Type</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="employee_absence"/>
    </table>
  </xsl:template>

  <xsl:template match="future_employee_absence_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="9">
	  Future Employee Absences (upcoming 16 weeks)
        </td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Employee</td>
	<td class="standard_table_spacer_column"></td>
	<td>Absence Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Type</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="employee_absence"/>
    </table>
  </xsl:template>

  <xsl:template match="pending_employee_absence_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="9">
	  Pending Employee Absences (not yet approved)
        </td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Employee</td>
	<td class="standard_table_spacer_column"></td>
	<td>Absence Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Type</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="employee_absence"/>
    </table>
  </xsl:template>

  <xsl:template match="employee_absence">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td><xsl:value-of select="employee_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="absence_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="day_type"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use lead_pad;
use select_row;
use get_row_list;
use get_role_list;
use get_row_by_id_map;
use add_time;
use timestamp_to_minutes;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $employee_id = $employee->{"id"};
  my $role_list = get_role_list($instance_map, $employee_id);
  my $show_only_active_employees = (grep {$_ =~ m!^role:hr/view_active_time_sheets$!} @$role_list) ? 1 : 0;
  if ((grep {$_ =~ m!^role:hr/view_(all|active)_time_sheets$!} @$role_list) && ($cgi->param("employee_id"))) {
    $employee_id = $cgi->param("employee_id");
  }
  if ($employee_id != $employee->{"id"}) {
    $employee = select_row($dbh, "employee_tbl", $employee_id);
  }
  my $num_weeks_to_display = $cgi->param("num_weeks") || 5;
  if ($num_weeks_to_display !~ /^\d+$/) {
    die "show_page: time_report_bad_num_weeks.html\n";
  } else {
    $num_weeks_to_display = int($num_weeks_to_display);
  }
  # construct query.
  my ($now_year, $now_month, $now_day, $now_day_of_week) = (localtime(time))[5, 4, 3, 6];
  $now_year += 1900;
  $now_month++;
  my $num_days_backwards = ($num_weeks_to_display - 1) * 7 + $now_day_of_week;
  my $num_days_to_display = $num_weeks_to_display * 7;
  # go back number of weeks plus a sufficient number of extra days to make it Sunday.
  my ($start_year, $start_month, $start_day) = (add_time($now_year, $now_month, $now_day, 0, 0, 0, -$num_days_backwards, "day"))[0, 1, 2];
  $start_month = lead_pad($start_month, 2);
  $start_day = lead_pad($start_day, 2);
  my $start_timestamp = "$start_year-$start_month-$start_day 00:00:00";
  my ($end_year, $end_month, $end_day) = (add_time($now_year, $now_month, $now_day, 0, 0, 0, 7 - $now_day_of_week, "day"))[0, 1, 2];
  $end_month = lead_pad($end_month, 2);
  $end_day = lead_pad($end_day, 2);
  my $end_timestamp = "$end_year-$end_month-$end_day 00:00:00";
  my $sql_query = "select * from employee_time_tbl where employee_id = ? and time_in >= ? and time_in < ? order by time_in";
  my @sql_param_list = ([$employee_id, SQL_INTEGER], $start_timestamp, $end_timestamp);
  my @sparse_employee_time_list; # there can be holes in the employee time entries, so it is a sparse list.
  get_row_list($dbh, $sql_query, \@sql_param_list, \@sparse_employee_time_list);
  # if employee is a W-2 employee, fetch all holidays in the date range. Do not show holidays for consultants.
  my (%holiday_by_id, %holiday_by_date);
  if ($employee->{"type"} eq "employee") {
    $sql_query = "select * from holiday_tbl where holiday_date >= ? and holiday_date < ?";
    @sql_param_list = ($start_timestamp, $end_timestamp);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%holiday_by_id);
    for my $holiday_id (keys(%holiday_by_id)) {
      my $holiday = $holiday_by_id{$holiday_id};
      $holiday_by_date{$holiday->{"holiday_date"}} = $holiday;
    }
  }
  # fetch all employee absences (vacation day, sick day, unpaid day) in the date range that are approved.
  my %employee_absence_by_id;
  $sql_query = "select * from employee_absence_tbl where employee_id = ? and absence_date >= ? and absence_date < ? and status = ?";
  @sql_param_list = ($employee_id, $start_timestamp, $end_timestamp, "approved");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_absence_by_id);
  my %employee_absence_by_date;
  for my $employee_absence_id (keys(%employee_absence_by_id)) {
    my $employee_absence = $employee_absence_by_id{$employee_absence_id};
    $employee_absence_by_date{$employee_absence->{"absence_date"}} = $employee_absence;
  }
  # fetch all employees to generate drop down list.
  my (%employee_by_id, @employee_id_list);
  if ($show_only_active_employees) {
    $sql_query = "select * from employee_tbl where is_active order by last_name, first_name";
  } else {
    $sql_query = "select * from employee_tbl order by last_name, first_name";
  }
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  # populate information needed for display of page.
  my $sparse_index = 0;
  # first normalize sparse list. Multiple entries made on one day should be combined.
  while($sparse_index < scalar(@sparse_employee_time_list) - 1) {
    my $_employee_time_a = $sparse_employee_time_list[$sparse_index];
    my $_employee_time_b = $sparse_employee_time_list[$sparse_index + 1];
    $_employee_time_a->{"time_in"} =~ /^(\d{4}-\d\d-\d\d)/;
    my $_date_a = $1;
    $_employee_time_b->{"time_in"} =~ /^(\d{4}-\d\d-\d\d)/;
    my $_date_b = $1;
    if ($_date_a eq $_date_b) {
      if (defined($_employee_time_a->{"break_min"}) && defined($_employee_time_b->{"break_min"})) {
	my $_break_min_a = $_employee_time_a->{"break_min"};
	my $_break_min_b = $_employee_time_b->{"break_min"};
	my $_break_min_between = timestamp_to_minutes($_employee_time_b->{"time_in"}) - timestamp_to_minutes($_employee_time_a->{"time_out"});
	$sparse_employee_time_list[$sparse_index]->{"break_min"} = $_break_min_a + $_break_min_b + $_break_min_between;
	$sparse_employee_time_list[$sparse_index]->{"time_out"} = $sparse_employee_time_list[$sparse_index + 1]->{"time_out"};
	splice(@sparse_employee_time_list, $sparse_index + 1, 1);
      } else {
	$sparse_employee_time_list[$sparse_index]->{"break_min"} = undef;
	$sparse_employee_time_list[$sparse_index]->{"time_out"} = undef;
	splice(@sparse_employee_time_list, $sparse_index + 1, 1);
      }
    } else {
      $sparse_index++;
    }
  }
  $sparse_index = 0;
  # we need to print out full weeks of time sheet data, starting on a Sunday.
  my $num_days_in_month = days_in_month($start_month, $start_year);
  my ($_year, $_month, $_day) = ($start_year, $start_month, $start_day);
  my @day_map_list;
  for(my $i = 0; $i < $num_days_to_display; $i++) {
    my $_loop_date = $_year . "-" . lead_pad($_month, 2) . "-" . lead_pad($_day, 2);
    my %day_map = ();
    if (my $holiday = $holiday_by_date{$_loop_date}) {
      $day_map{"holiday"} = $holiday;
    }
    if (my $employee_absence = $employee_absence_by_date{$_loop_date}) {
      $day_map{"employee_absence"} = $employee_absence;
    }
    if ($sparse_index < scalar(@sparse_employee_time_list)) {
      my $employee_time = $sparse_employee_time_list[$sparse_index];
      $employee_time->{"time_in"} =~ /^(\d{4})-(\d\d)-(\d\d)/;
      if (($_year == $1) && ($_month == $2) && ($_day == $3)) {
	$day_map{"work"} = $employee_time;
	$sparse_index++;
      }
    }
    push(@day_map_list, \%day_map);
    if (++$_day > $num_days_in_month) {
      $_day = 1;
      if (++$_month > 12) {
	$_month = 1;
	$_year++;
      }
      $num_days_in_month = days_in_month($_month, $_year);
    }
  }
  my $selected_employee = $employee_by_id{$employee_id};
  my $first_name = HTML::Entities::encode($selected_employee->{"first_name"});
  my $last_name = HTML::Entities::encode($selected_employee->{"last_name"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <selected-employee>
    <id>$employee_id</id>
    <first_name>$first_name</first_name>
    <last_name>$last_name</last_name>
  </selected-employee>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <employee-list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_first_name = HTML::Entities::encode($_employee->{"first_name"});
    my $_last_name = HTML::Entities::encode($_employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <first_name>$_first_name</first_name>
      <last_name>$_last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <num_weeks>$num_weeks_to_display</num_weeks>
  <time-report>
---END_BLOCK---;
  $num_days_in_month = days_in_month($start_month, $start_year);
  ($_year, $_month, $_day) = ($start_year, $start_month, $start_day);
  my $_day_of_week = 0;
  for my $day_map (@day_map_list) {
    if ($_day_of_week == 0) {
      $generated_xml .= <<"---END_BLOCK---;";
    <week>
---END_BLOCK---;
    }
    my $employee_time = $day_map->{"work"};
    my $is_holiday = $day_map->{"holiday"} ? 1 : 0;
    if ($employee_time) {
      $employee_time->{"time_in"} =~ /^(\d{4}-\d\d-\d\d)/;
      my $work_date = $1;
      if ($employee_time->{"time_out"}) {
	$employee_time->{"time_out"} =~ /^(\d{4}-\d\d-\d\d)/;
	my $end_work_date = $1;
	if ($work_date eq $end_work_date) {
	  my $net_duration = timestamp_to_minutes($employee_time->{"time_out"}) - timestamp_to_minutes($employee_time->{"time_in"}) - $employee_time->{"break_min"};
	  my $paid_absence_days = $is_holiday;
	  $generated_xml .= <<"---END_BLOCK---;";
      <day>
        <date>$work_date</date>
        <time_in>$employee_time->{"time_in"}</time_in>
        <time_out>$employee_time->{"time_out"}</time_out>
        <break_min>$employee_time->{"break_min"}</break_min>
        <net_duration>$net_duration</net_duration>
        <is_holiday>$is_holiday</is_holiday>
        <paid_absence_days>$paid_absence_days</paid_absence_days>
      </day>
---END_BLOCK---;
	} elsif ($end_work_date gt $work_date) {
	  # assume next day (does not handle work session that spans more than two days!).
	  my $net_duration = timestamp_to_minutes($employee_time->{"time_out"}) - timestamp_to_minutes($employee_time->{"time_in"}) - $employee_time->{"break_min"} + 1440;
	  my $paid_absence_days = $is_holiday;
	  $generated_xml .= <<"---END_BLOCK---;";
      <day>
        <date>$work_date</date>
        <time_in>$employee_time->{"time_in"}</time_in>
        <time_out>$employee_time->{"time_out"}</time_out>
        <break_min>$employee_time->{"break_min"}</break_min>
        <net_duration>$net_duration</net_duration>
        <is_holiday>$is_holiday</is_holiday>
        <paid_absence_days>$paid_absence_days</paid_absence_days>
      </day>
---END_BLOCK---;
	} else {
	  $generated_xml .= <<"---END_BLOCK---;";
      <day>
        <date>$work_date</date>
        <is_bad_end>1</is_bad_end>
      </day>
---END_BLOCK---;
	}
      } else {
	$generated_xml .= <<"---END_BLOCK---;";
      <day>
        <date>$work_date</date>
        <time_in>$employee_time->{"time_in"}</time_in>
        <is_incomplete_day>1</is_incomplete_day>
      </day>
---END_BLOCK---;
      }
    } else {
      my $work_date = $_year . "-" . lead_pad($_month, 2) . "-" . lead_pad($_day, 2);
      my $employee_absence = $day_map->{"employee_absence"};
      my $employee_absence_type = $employee_absence ? $employee_absence->{"day_type"} : "";
      my $paid_absence_days = ($employee_absence && ($employee_absence_type ne "unpaid")) ? 1 : 0;
      $paid_absence_days ||= $is_holiday;
      $generated_xml .= <<"---END_BLOCK---;";
      <day>
        <date>$work_date</date>
        <is_empty>1</is_empty>
        <is_holiday>$is_holiday</is_holiday>
        <employee_absence_type>$employee_absence_type</employee_absence_type>
        <paid_absence_days>$paid_absence_days</paid_absence_days>
      </day>
---END_BLOCK---;
    }
    if (++$_day_of_week > 6) {
      $_day_of_week = 0;
      $generated_xml .= <<"---END_BLOCK---;";
    </week>
---END_BLOCK---;
    }
    if (++$_day > $num_days_in_month) {
      $_day = 1;
      if (++$_month > 12) {
	$_month = 1;
	$_year++;
      }
      $num_days_in_month = days_in_month($_month, $_year);
    }
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </time-report>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "time_report.xslt", $cgi);
}

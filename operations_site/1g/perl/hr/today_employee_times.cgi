#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use authorize_role;
use add_time;
use lead_pad;
use get_row_list;
use get_row_by_id_map;
use timestamp_to_minutes;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:hr/view_all_time_sheets");
  # construct query.
  my ($now_year, $now_month, $now_day, $now_day_of_week) = (localtime(time))[5, 4, 3, 6];
  $now_year += 1900;
  $now_month++;
  my ($start_year, $start_month, $start_day) = ($now_year, lead_pad($now_month, 2), lead_pad($now_day, 2));
  my $start_timestamp = "$start_year-$start_month-$start_day 00:00:00";
  my ($end_year, $end_month, $end_day) = (add_time($now_year, $now_month, $now_day, 0, 0, 0, 1, "day"))[0, 1, 2];
  $end_month = lead_pad($end_month, 2);
  $end_day = lead_pad($end_day, 2);
  my $end_timestamp = "$end_year-$end_month-$end_day 00:00:00";
  my $sql_query = "select * from employee_time_tbl where time_in >= ? and time_in < ? order by time_in";
  my @sql_param_list = ($start_timestamp, $end_timestamp);
  my @employee_time_list;
  get_row_list($dbh, $sql_query, \@sql_param_list, \@employee_time_list);
  my %employee_time_by_employee_id;
  for my $employee_time (@employee_time_list) {
    my $employee_id = $employee_time->{"employee_id"};
    $employee_time_by_employee_id{$employee_id} = $employee_time;
  }
  # fetch all employees.
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <year>$start_year</year>
    <month>$start_month</month>
    <day>$start_day</day>
  </today>
  <today-time-list>
---END_BLOCK---;
  for my $employee_id (@employee_id_list) {
    my $employee_time = $employee_time_by_employee_id{$employee_id};
    next if (!$employee_time);
    my $_employee = $employee_by_id{$employee_id};
    my $first_name = HTML::Entities::encode($_employee->{"first_name"});
    my $last_name = HTML::Entities::encode($_employee->{"last_name"});
    my $time_in = $employee_time->{"time_in"};
    my $time_out = $employee_time->{"time_out"} || "";
    my $break_min = defined($employee_time->{"break_min"}) ? $employee_time->{"break_min"} : "";
    my $net_duration = ($time_in && $time_out) ? timestamp_to_minutes($time_out) - timestamp_to_minutes($time_in) : "";
    $generated_xml .= <<"---END_BLOCK---;";
    <employee-time>
      <employee_id>$employee_id</employee_id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
      <time_in>$time_in</time_in>
      <time_out>$time_out</time_out>
      <break_min>$break_min</break_min>
      <net_duration>$net_duration</net_duration>
    </employee-time>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </today-time-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "today_employee_times.xslt", $cgi);
}

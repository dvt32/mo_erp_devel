<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Time-Tracking :: Today's Employee Times
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/hr/clock_in_input.cgi">Clock In</a> |
	  <a href="/1g/hr/clock_out_input.cgi">Clock Out</a> |
	  <a href="/1g/hr/time_report.cgi">Time Report</a>
	</p>
	<xsl:apply-templates select="today-time-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="today-time-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="11">
          Employee Times for
	  <xsl:value-of select="../today/year"/>
	  <xsl:text>-</xsl:text>
	  <xsl:value-of select="../today/month"/>
	  <xsl:text>-</xsl:text>
	  <xsl:value-of select="../today/day"/>
        </td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Employee</td>
	<td class="standard_table_spacer_column"></td>
	<td>Time In</td>
	<td class="standard_table_spacer_column"></td>
	<td>Time Out</td>
	<td class="standard_table_spacer_column"></td>
	<td>Break</td>
	<td class="standard_table_spacer_column"></td>
	<td>Net</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="employee-time"/>
    </table>
  </xsl:template>

  <xsl:template match="employee-time">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="last_name"/>, <xsl:value-of select="first_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="substring(time_in, 1, 19)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="substring(time_out, 1, 19)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="break_min"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="net_duration"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

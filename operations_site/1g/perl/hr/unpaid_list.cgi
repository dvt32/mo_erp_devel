#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_role_list;
use apply_xslt_output_xhtml;
use add_time;
use lead_pad;
use select_row;
use get_row_by_id_map;
use day_of_week;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $employee_id = $employee->{"id"};
  my $role_list = get_role_list($instance_map, $employee_id);
  if ((grep {$_ eq "role:hr/review_absences"} @$role_list) && ($cgi->param("employee_id"))) {
    $employee_id = $cgi->param("employee_id");
  }
  # construct query.
  my ($now_year, $now_month, $now_day, $now_day_of_week) = (localtime(time))[5, 4, 3, 6];
  $now_year += 1900;
  $now_month++;
  # go back two years.
  my ($start_year, $start_month, $start_day) = (add_time($now_year, $now_month, $now_day, 0, 0, 0, -730, "day"))[0, 1, 2];
  $start_month = lead_pad($start_month, 2);
  $start_day = lead_pad($start_day, 2);
  my $sql_query = "select * from employee_absence_tbl where employee_id = ? and absence_date >= ? and day_type = ? order by absence_date";
  my @sql_param_list = ([$employee_id, SQL_INTEGER], "$start_year-$start_month-$start_day", "unpaid");
  my (%employee_absence_by_id, @employee_absence_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%employee_absence_by_id, \@employee_absence_id_list);
  # fetch all employees to generate drop down list.
  my $employee = select_row($dbh, "employee_tbl", $employee_id);
  my (%employee_by_id, @employee_id_list);
  $sql_query = "select * from employee_tbl order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%employee_by_id, \@employee_id_list);
  # populate information needed for display of page.
  my $selected_employee = $employee_by_id{$employee_id};
  my $first_name = HTML::Entities::encode($selected_employee->{"first_name"});
  my $last_name = HTML::Entities::encode($selected_employee->{"last_name"});
  my @day_of_week_list = (qw(Sunday Monday Tuesday Wednesday Thursday Friday Saturday));
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <selected-employee>
    <id>$employee_id</id>
    <first_name>$first_name</first_name>
    <last_name>$last_name</last_name>
  </selected-employee>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    my $enc_role = HTML::Entities::encode($role);
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$enc_role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <employee-list>
---END_BLOCK---;
  for my $_employee_id (@employee_id_list) {
    my $_employee = $employee_by_id{$_employee_id};
    my $_first_name = HTML::Entities::encode($_employee->{"first_name"});
    my $_last_name = HTML::Entities::encode($_employee->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <employee>
      <id>$_employee_id</id>
      <first_name>$_first_name</first_name>
      <last_name>$_last_name</last_name>
    </employee>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee-list>
  <employee_absence_list>
---END_BLOCK---;
  for my $employee_absence_id (@employee_absence_id_list) {
    my $employee_absence = $employee_absence_by_id{$employee_absence_id};
    my $absence_date = $employee_absence->{"absence_date"};
    $absence_date =~ /^(\d{4})-(\d\d)-(\d\d)/;
    my ($_year, $_month, $_day) = ($1, $2, $3);
    my $day_of_week = HTML::Entities::encode($day_of_week_list[day_of_week($_month, $_day, $_year)]);
    my $status = HTML::Entities::encode($employee_absence->{"status"});
    my $note = HTML::Entities::encode($employee_absence->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <employee_absence>
      <id>$employee_absence_id</id>
      <absence_date>$absence_date</absence_date>
      <day_of_week>$day_of_week</day_of_week>
      <status>$status</status>
      <note>$note</note>
    </employee_absence>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </employee_absence_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "unpaid_list.xslt", $cgi);
}

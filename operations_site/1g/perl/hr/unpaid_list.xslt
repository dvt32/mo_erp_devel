<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Employee Time-Tracking :: Unpaid Days
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/hr/holiday_list.cgi">Holidays</a> |
	  <a href="/1g/hr/vacation_list.cgi">Vacation Days</a> |
	  <a href="/1g/hr/sick_list.cgi">Sick Days</a> |
	  <a href="/1g/hr/unpaid_list.cgi">Unpaid Days</a>
	  <xsl:if test="role-list/role = 'role:hr/review_absences'">
	    |
	    <a href="/1g/hr/show_absences.cgi">Review Absences</a>
	  </xsl:if>
	</p>
	<xsl:if test="role-list/role = 'role:hr/review_absences'">
	  <form action="/1g/hr/unpaid_list.cgi" method="post">
	    <select name="employee_id">
	      <xsl:for-each select="/page/employee-list/employee">
		<option>
		  <xsl:if test="/page/selected-employee/id = id">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		  <xsl:value-of select="last_name"/>
		  <xsl:text>, </xsl:text>
		  <xsl:value-of select="first_name"/>
		</option>
	      </xsl:for-each>
	    </select>
	    <input type="submit" value="Go" />
	  </form>
	</xsl:if>
	<xsl:apply-templates select="employee_absence_list"/>
	<p>
	  <a href="/1g/hr/new_unpaid_input.cgi">Request Unpaid Day</a>. Please request unpaid days at least 2 - 3 weeks in advance
	  so we can properly plan for your absence.</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="employee_absence_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
        <td class="standard_table_title" colspan="9">
          Unpaid Days for
	  <xsl:value-of select="../selected-employee/first_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="../selected-employee/last_name"/>
        </td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Day of Week</td>
	<td class="standard_table_spacer_column"></td>
	<td>Status</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="employee_absence"/>
      <xsl:if test="count(employee_absence) = 0">
	<tr>
	  <td colspan="9">
	    No unpaid days off in past 365 days.
	  </td>
	</tr>
      </xsl:if>
    </table>
  </xsl:template>

  <xsl:template match="employee_absence">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="status = 'pending'">
	    <a>
	      <xsl:attribute name="href">/1g/hr/edit_unpaid_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	      <xsl:attribute name="class">bare_link</xsl:attribute>
	      <xsl:value-of select="absence_date"/>
	    </a>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="absence_date"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="day_of_week"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="status"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

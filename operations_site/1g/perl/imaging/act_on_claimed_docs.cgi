#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_cgi;
use get_database_handle;
use get_employee;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_row_by_id_map;
use delete_row;
use delete_file;
use get_file;
use send_fax;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $submit_button = $cgi->param("submit_button");
  if ($submit_button eq "Delete Selected Documents") {
    delete_docs($instance_map);
  } elsif ($submit_button eq "Send Selected Documents as Fax") {
    send_docs_as_fax($instance_map);
  } else {
    die "show_page: act_on_claimed_docs_unknown_action.html\n";
  }
}

sub delete_docs($$$) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = (get_cgi($instance_map), get_database_handle($instance_map, "main"), get_employee($instance_map));
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @document_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      my $document_id = $1;
      my $document = select_row($dbh, "document_vw", $document_id);
      if ((!$document) || (!$document->{"account_id"}) || (!$document->{"person_id"}) ||
	  ($document->{"type"} !~ /^scan|fax$/)) {
	next;
      }
      push(@document_list, $document);
    }
  }
  my $sql_query = "select * from account_vw";
  my %account_by_id;
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  $sql_query = "select * from person_vw";
  my %person_by_id;
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  $dbh->begin_work();
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <document_list>
---END_BLOCK---;
  for my $document (@document_list) {
    my $document_id = $document->{"id"};
    my $tbl_name;
    if ($document->{"type"} eq "scan") {
      $tbl_name = "scan_tbl";
    } elsif ($document->{"type"} eq "fax") {
      $tbl_name = "fax_in_tbl";
    }
    delete_row($dbh, $tbl_name, $document_id);
    delete_file($instance_map, $document->{"file_id"});
    my $account = $document->{"account_id"} ? $account_by_id{$document->{"account_id"}} : undef;
    my $person = $document->{"person_id"} ? $person_by_id{$document->{"person_id"}} : undef;
    my $account_name = $account ? HTML::Entities::encode($account->{"name"}) : "";
    my $person_name = $person ? HTML::Entities::encode($person->{"first_name"} . " " . $person->{"last_name"}) : "";
    my $type = HTML::Entities::encode($document->{"type"});
    my $filename = HTML::Entities::encode($document->{"filename"});
    my $file_size = int($document->{"file_size"} / 1024) . "K";
    my $file_timestamp = $document->{"file_timestamp"};
    my $note = HTML::Entities::encode($document->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <document>
      <id>$document_id</id>
      <account_name>$account_name</account_name>
      <person_name>$person_name</person_name>
      <type>$type</type>
      <filename>$filename</filename>
      <file_size>$file_size</file_size>
      <file_timestamp>$file_timestamp</file_timestamp>
      <note>$note</note>
    </document>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </document_list>
</page>
---END_BLOCK---;
  $dbh->commit();
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "act_on_claimed_docs_delete_result.xslt", $cgi);
}

sub send_docs_as_fax($$$) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = (get_cgi($instance_map), get_database_handle($instance_map, "main"), get_employee($instance_map));
  # get CGI input parameters.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my @id_param_name_list = grep {/^id-\d+/} $cgi->param();
  my @file_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^id-(\d+)$/) {
      my $document_id = $1;
      my $document = select_row($dbh, "document_vw", $document_id);
      if ((!$document) || (!$document->{"account_id"}) || (!$document->{"person_id"})) {
	next;
      }
      push(@file_id_list, $document->{"file_id"});
    }
  }
  if (!@file_id_list) {
    die "show_page: send_docs_as_fax_no_documents.html\n";
  }
  my (@filename_list, @file_content_list);
  for my $file_id (@file_id_list) {
    my $file_data;
    get_file($instance_map, $file_id, \$file_data);
    push(@filename_list, "file_id_" . $file_id . ".pdf");
    push(@file_content_list, $file_data);
  }
  my $recipient_name = Encode::decode($cgi_input_charset, $cgi->param("recipient_name"));
  if ((!$recipient_name) || (length($recipient_name) > 32)) {
    die "show_page: send_docs_as_fax_bad_recipient_name.html\n";
  }
  my $fax_number = $cgi->param("fax_number");
  if ((!$fax_number) || ($fax_number !~ /^\d{10}$/) || ($fax_number =~ /^1/)) {
    die "show_page: send_docs_as_fax_bad_fax_number.html\n";
  }
  my $send_email_alert = $cgi->param("send_email_alert") || 0;
  my $job_id;
  my $db_charset = get_database_charset($instance_map, "main");
  eval {
    $job_id = send_fax
      (
       "dbh" => $dbh,
       "db_charset" => $db_charset,
       "sending_person" => $employee,
       "sending_account_type" => "corporate",
       "recipient_name" => $recipient_name,
       "fax_number" => $fax_number,
       "filename_list" => \@filename_list,
       "file_content_list" => \@file_content_list,
       "send_email_alert" => $send_email_alert,
      );
  };
  if ($@) {
    my $error_message = $@;
    chomp($error_message);
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <error_message>$error_message</error_message>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "send_fax_error.xslt", $cgi);
    return;
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <job_id>$job_id</job_id>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "send_docs_as_fax_result.xslt", $cgi);
}

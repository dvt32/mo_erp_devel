#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $document_id = $cgi->param("id");
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  if ((!$note) || ($note =~ /^\s*$/)) {
    die "show_page: claim_doc_bad_note.html\n";
  }
  my $document;
  if ((!$document_id) || (!($document = select_row($dbh, "document_vw", $document_id)))) {
    die "show_page: claim_doc_bad_id.html\n";
  }
  if (($document->{"account_id"}) || ($document->{"person_id"})) {
    die "show_page: claim_doc_already_claimed.html\n";
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["account_id", $account_id_of_corporate, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $employee->{"id"}, SQL_INTEGER]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  my $tbl_name;
  if ($document->{"type"} eq "scan") {
    $tbl_name = "scan_tbl";
  } elsif ($document->{"type"} eq "fax") {
    $tbl_name = "fax_in_tbl";
  } else {
    die "show_page: claim_doc_unknown_document_type.html\n";
  }
  update_row($dbh, $tbl_name, \@sql_column_list, $document_id);
  # generate XML.
  my $type = HTML::Entities::encode($document->{"type"});
  my $filename = HTML::Entities::encode($document->{"filename"});
  my $file_size = int($document->{"file_size"} / 1024) . "K";
  my $file_timestamp = $document->{"file_timestamp"};
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <document>
    <id>$document_id</id>
    <type>$type</type>
    <filename>$filename</filename>
    <file_size>$file_size</file_size>
    <file_timestamp>$file_timestamp</file_timestamp>
    <note>$note</note>
  </document>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "claim_doc_result.xslt", $cgi);
}

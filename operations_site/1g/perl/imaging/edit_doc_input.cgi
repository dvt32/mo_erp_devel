#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $document_id = $cgi->param("id");
  my $document;
  if ((!$document_id) || (!($document = select_row($dbh, "document_vw", $document_id)))) {
    die "show_page: edit_doc_bad_id.html\n";
  }
  my (%person_by_id, @person_id_list);
  my $sql_query = "select * from person_vw where is_active order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id, \@person_id_list);
  my (%account_by_id, @account_id_list);
  $sql_query = "select * from account_vw where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id, \@account_id_list);
  # generate XML.
  my $account_id = $document->{"account_id"};
  my $person_id = $document->{"person_id"};
  my $type = HTML::Entities::encode($document->{"type"});
  my $filename = HTML::Entities::encode($document->{"filename"});
  my $file_size = int($document->{"file_size"} / 1024) . "K";
  my $file_timestamp = $document->{"file_timestamp"};
  my $note = HTML::Entities::encode($document->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <document>
    <id>$document_id</id>
    <account_id>$account_id</account_id>
    <person_id>$person_id</person_id>
    <type>$type</type>
    <filename>$filename</filename>
    <file_size>$file_size</file_size>
    <file_timestamp>$file_timestamp</file_timestamp>
    <note>$note</note>
  </document>
  <person-list>
---END_BLOCK---;
  for $person_id (@person_id_list) {
    my $person = $person_by_id{$person_id};
    my $first_name = HTML::Entities::encode($person->{"first_name"});
    my $last_name = HTML::Entities::encode($person->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$person_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
  <account-list>
---END_BLOCK---;
  for my $account_id (@account_id_list) {
    my $account = $account_by_id{$account_id};
    my $name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$account_id</id>
      <name>$name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_doc_input.xslt", $cgi);
}

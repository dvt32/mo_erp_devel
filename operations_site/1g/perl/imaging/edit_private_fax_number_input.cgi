#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $fax_number_id = $cgi->param("id");
  my $fax_number;
  if ((!$fax_number_id) || (!($fax_number = select_row($dbh, "fax_number_tbl", $fax_number_id)))) {
    die "show_page: edit_private_fax_number_bad_id.html\n";
  }
  my $account_id = $fax_number->{"account_id"};
  my $person_id = $fax_number->{"person_id"};
  my (%person_by_id, @person_id_list);
  my $sql_query = "select distinct * from person_vw where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  if (($person_id) && (!exists($person_by_id{$person_id}))) {
    $person_by_id{$person_id} = select_row($dbh, "person_vw", $person_id);
  }
  @person_id_list = sort {
    my $person_a = $person_by_id{$a};
    my $person_b = $person_by_id{$b};
    lc($person_a->{"last_name"}.$person_a->{"first_name"}) cmp lc($person_b->{"last_name"}.$person_b->{"last_name"});
  } keys(%person_by_id);
  my (%account_by_id, @account_id_list);
  $sql_query = "select * from account_vw where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id, \@account_id_list);
  if (($account_id) && (!exists($account_by_id{$account_id}))) {
    $account_by_id{$account_id} = select_row($dbh, "account_vw", $account_id);
  }
  @account_id_list = sort {
    my $account_a = $account_by_id{$a};
    my $account_b = $account_by_id{$b};
    lc($account_a->{"name"}) cmp lc($account_b->{"name"});
  } keys(%account_by_id);
  # generate XML.
  $account_id ||= "";
  $person_id ||= "";
  $fax_number->{"fax_number"} =~ /^(\d{3})(\d{3})(\d{4})$/;
  my $fax_number_number = "$1-$2-$3";
  my $email_action = HTML::Entities::encode($fax_number->{"email_action"});
  my $cc_email = HTML::Entities::encode($fax_number->{"cc_email"} || "");
  my $note = HTML::Entities::encode($fax_number->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <fax_number>
    <id>$fax_number_id</id>
    <account_id>$account_id</account_id>
    <person_id>$person_id</person_id>
    <fax_number_number>$fax_number_number</fax_number_number>
    <email_action>$email_action</email_action>
    <cc_email>$cc_email</cc_email>
    <note>$note</note>
  </fax_number>
  <person-list>
---END_BLOCK---;
  for $person_id (@person_id_list) {
    my $person = $person_by_id{$person_id};
    my $first_name = HTML::Entities::encode($person->{"first_name"});
    my $last_name = HTML::Entities::encode($person->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$person_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
  <account-list>
---END_BLOCK---;
  for my $account_id (@account_id_list) {
    my $account = $account_by_id{$account_id};
    my $name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$account_id</id>
      <name>$name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <email_action_list>
    <email_action>none</email_action>
    <email_action>notify</email_action>
    <email_action>attach</email_action>
    <email_action>attach_delete</email_action>
  </email_action_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_private_fax_number_input.xslt", $cgi);
}

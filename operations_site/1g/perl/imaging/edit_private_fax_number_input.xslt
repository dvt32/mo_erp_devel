<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Private Fax Number
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/show_claimed_docs.cgi">Claimed Documents</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
	  |
	  <a href="/1g/imaging/show_private_fax_numbers.cgi">Private Fax Numbers</a>
        </p>
	<xsl:apply-templates select="fax_number"/>
	<p>
	  For email action, here are the possible values:
	  <ul>
	    <li>none = do not send email when incoming fax arrives</li>
	    <li>notify = send email when incoming fax arrives</li>
	    <li>attach = send email when incoming fax arrives and include fax as attachment</li>
	    <li>attach_delete = send email when incoming fax arrives and include fax as attachment and delete from server</li>
	  </ul>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="fax_number">
    <table>
      <tr><td>
	<form action="/1g/imaging/edit_private_fax_number_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Private Fax Number
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Account:
		</td>
		<td>
		  <select name="account_id">
		    <xsl:for-each select="/page/account-list/account">
		      <option>
			<xsl:if test="/page/fax_number/account_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Person:
		</td>
		<td>
		  <select name="person_id">
		    <xsl:for-each select="/page/person-list/person">
		      <option>
			<xsl:if test="/page/fax_number/person_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="last_name"/>, <xsl:value-of select="first_name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Fax Number:
		</td>
		<td>
		  <xsl:value-of select="fax_number_number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email Action:
		</td>
		<td>
		  <select name="email_action">
		    <xsl:for-each select="/page/email_action_list/email_action">
		      <option>
			<xsl:if test="/page/fax_number/email_action = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Cc Email:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">cc_email</xsl:attribute>
		    <xsl:attribute name="size">50</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="cc_email"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

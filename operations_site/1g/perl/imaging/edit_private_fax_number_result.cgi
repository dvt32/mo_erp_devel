#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;
use is_valid_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $fax_number_id = $cgi->param("id");
  my $fax_number;
  if ((!$fax_number_id) || (!($fax_number = select_row($dbh, "fax_number_tbl", $fax_number_id)))) {
    die "show_page: edit_private_fax_number_bad_id.html\n";
  }
  # check that account id and person id match up.
  my $account_id = $cgi->param("account_id");
  my $account;
  if ((!$account_id) || (!($account = select_row($dbh, "account_vw", $account_id)))) {
    die "show_page: edit_private_fax_number_bad_account_id.html\n";
  }
  my $person_id = $cgi->param("person_id");
  my $person;
  if ((!$person_id) || (!($person = select_row($dbh, "person_vw", $person_id)))) {
    die "show_page: edit_private_fax_number_bad_person_id.html\n";
  }
  if ($person->{"account_id"} != $account->{"id"}) {
    die "show_page: edit_private_fax_number_account_person_mismatch.html\n";
  }
  my $email_action = $cgi->param("email_action");
  if ($email_action !~ /^none|notify|attach|attach_delete$/) {
    die "show_page: edit_private_fax_number_bad_email_action.html\n";
  }
  my $cc_email = Encode::decode($cgi_input_charset, $cgi->param("cc_email"));
  $cc_email =~ s/\s+//g;
  if ($cc_email) {
    my @cc_email_list = split(/,/, $cc_email);
    for my $_cc_email (@cc_email_list) {
      if (!is_valid_email($_cc_email)) {
	die "show_page: edit_private_fax_number_bad_cc_email.html\n";
      }
    }
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # update row
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $person_id, SQL_INTEGER]);
  push(@sql_column_list, ["email_action", $email_action]);
  push(@sql_column_list, ["cc_email", Encode::encode($db_charset, $cc_email) || undef]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  update_row($dbh, "fax_number_tbl", \@sql_column_list, $fax_number_id);
  # generate XML.
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $person_name = HTML::Entities::encode($person->{"first_name"} . " " . $person->{"last_name"});
  $fax_number->{"fax_number"} =~ /^(\d{3})(\d{3})(\d{4})$/;
  my $fax_number_number = "$1-$2-$3";
  $email_action = HTML::Entities::encode($email_action);
  $cc_email = HTML::Entities::encode($cc_email);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <fax_number>
    <id>$fax_number_id</id>
    <account_name>$account_name</account_name>
    <person_name>$person_name</person_name>
    <fax_number_number>$fax_number_number</fax_number_number>
    <email_action>$email_action</email_action>
    <cc_email>$cc_email</cc_email>
    <note>$note</note>
  </fax_number>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_private_fax_number_result.xslt", $cgi);
}

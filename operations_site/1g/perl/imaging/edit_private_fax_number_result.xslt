<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Private Fax Number (success)
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/show_claimed_docs.cgi">Claimed Documents</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
	  |
	  <a href="/1g/imaging/show_private_fax_numbers.cgi">Private Fax Numbers</a>
        </p>
	<xsl:apply-templates select="fax_number"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="fax_number">
    <p>
      You successfully updated the following private fax number:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Person:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="person_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Fax Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="fax_number_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email Action:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email_action"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Cc Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cc_email"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

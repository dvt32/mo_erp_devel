#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $person_id = $cgi->param("person_id");
  if (!$person_id) {
    $person_id = $employee->{"id"};
  }
  my (%document_by_id, @document_id_list);
  my $sql_query = "select * from document_vw where person_id = ? order by file_timestamp";
  my @sql_param_list = ([$person_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%document_by_id, \@document_id_list);
  my $person = select_row($dbh, "person_vw", $person_id);
  my $account_id = $person->{"account_id"};
  my $account = select_row($dbh, "account_vw", $account_id);
  my (%person_by_id, @person_id_list);
  $sql_query = "select * from person_vw where is_active order by last_name, first_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id, \@person_id_list);
  my (%account_by_id);
  $sql_query = "select * from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # generate XML.
  my $email = HTML::Entities::encode($employee->{"email"} || "");
  my $first_name = HTML::Entities::encode($person->{"first_name"});
  my $last_name = HTML::Entities::encode($person->{"last_name"});
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <employee>
    <email>$email</email>
  </employee>
  <person>
    <id>$person_id</id>
    <first_name>$first_name</first_name>
    <last_name>$last_name</last_name>
  </person>
  <account>
    <id>$account_id</id>
    <name>$account_name</name>
  </account>
  <person-list>
---END_BLOCK---;
  for $person_id (@person_id_list) {
    $person = $person_by_id{$person_id};
    $first_name = HTML::Entities::encode($person->{"first_name"});
    $last_name = HTML::Entities::encode($person->{"last_name"});
    $account_id = $person->{"account_id"};
    $account = $account_by_id{$account_id};
    $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$person_id</id>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
      <account>
        <id>$account_id</id>
        <name>$account_name</name>
      </account>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
  <document_list>
---END_BLOCK---;
  for my $document_id (@document_id_list) {
    my $document = $document_by_id{$document_id};
    my $type = HTML::Entities::encode($document->{"type"});
    my $filename = HTML::Entities::encode($document->{"filename"});
    my $file_size = int($document->{"file_size"} / 1024) . "K";
    my $file_timestamp = $document->{"file_timestamp"};
    my $note = HTML::Entities::encode($document->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <document>
      <id>$document_id</id>
      <type>$type</type>
      <filename>$filename</filename>
      <file_size>$file_size</file_size>
      <file_timestamp>$file_timestamp</file_timestamp>
      <note>$note</note>
    </document>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </document_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_claimed_docs.xslt", $cgi);
}

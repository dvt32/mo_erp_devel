<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Show Claimed Documents
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<script src="/1g/js/toggle_checkboxes.js">
	  &nbsp;
	</script>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
        </p>
	<form method="post" action="/1g/imaging/show_claimed_docs.cgi">
	  Documents owned by:
	  <select name="person_id">
	    <xsl:for-each select="/page/person-list/person">
	      <option>
		<xsl:if test="/page/person/id = id">
		  <xsl:attribute name="selected">selected</xsl:attribute>
		</xsl:if>
		<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		<xsl:value-of select="last_name"/>, <xsl:value-of select="first_name"/>
		(<xsl:value-of select="account/name"/>)
	      </option>
	    </xsl:for-each>
	  </select>
	  <input type="submit" value="Go"/>
	</form>
	<xsl:apply-templates select="document_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="document_list">
    <form action="/1g/imaging/act_on_claimed_docs.cgi" method="post">
      <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	<tr>
	  <td colspan="13" class="standard_table_title">
	    Documents Claimed by
	    <xsl:value-of select="../person/first_name"/><xsl:text> </xsl:text><xsl:value-of select="../person/last_name"/>
	    (<xsl:value-of select="../account/name"/>)
	  </td>
	</tr>
	<tr class="standard_table_column_heading_row">
	  <td class="standard_table_side_padding"></td>
	  <td align="center">
	    <input type="checkbox" name="select_all" onclick="toggle_checkboxes(this, 'id-');"/>
	  </td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Timestamp</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Type</td>
	  <td class="standard_table_spacer_column"></td>
	  <td align="right">Size</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Filename</td>
	  <td class="standard_table_spacer_column"></td>
	  <td>Note</td>
	  <td class="standard_table_side_padding"></td>
	</tr>
	<xsl:choose>
	  <xsl:when test="count(document) > 0">
	    <xsl:apply-templates select="document"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <tr>
	      <td colspan="13" align="center">No documents for person.</td>
	    </tr>
	  </xsl:otherwise>
	</xsl:choose>
      </table>
      <p>
	<input type="submit" name="submit_button" value="Delete Selected Documents"/>
      </p>
      <p>
	If you just want to view a document, click on the filename of the desired document above.

	If you want to delete one or more documents, click the checkboxes next to the documents you would like to delete, then
	click on the "Delete Selected Documents" button. If you want to fax one or more documents, click the checkboxes next to
	the documents you would like to fax, then fill out the recipient name and fax number below and then click on the "Send
	Selected Documents as Fax" button below to send the document(s) as a fax.

	To perform other actions on a document, click on the note. Actions you may perform include reassigning the document to
	another person, changing the note, downloading the document, unclaiming the document, and deleting the document. If you
	accidentally claimed a document belonging to someone else, please do not delete it! Instead, unclaim it by clicking on
	the note, then clicking on "Unclaim" so that the rightful owner can claim it. Or, if you know who owns the document,
	you may also assign the document to the rightful owner.
      </p>
      <table>
	<tr>
	  <td>
	    Recipient Name:
	  </td>
	  <td>
	    <input type="text" maxlength="32" name="recipient_name"/>
	  </td>
	</tr>
	<tr>
	  <td>
	    Fax Number:
	  </td>
	  <td>
	    <input type="text" maxlength="10" name="fax_number"/>
	    (10 digits, no punctuation or spaces)
	  </td>
	</tr>
	<xsl:if test="/page/employee/email != ''">
	  <tr>
	    <td>
	      Email Alert:
	    </td>
	    <td>
	      <select name="send_email_alert">
		<option value="1">Yes</option>
		<option value="0" selected="selected">No</option>
	      </select>
	      (if "Yes", email <xsl:value-of select="/page/employee/email"/> with success/failure)
	    </td>
	  </tr>
	</xsl:if>
      </table>
      <p>
	<input type="submit" name="submit_button" value="Send Selected Documents as Fax"/>
      </p>
    </form>
  </xsl:template>

  <xsl:template match="document">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="center">
	<input>
	  <xsl:attribute name="type">checkbox</xsl:attribute>
	  <xsl:attribute name="name">id-<xsl:value-of select="id"/></xsl:attribute>
	</input>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="file_timestamp"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="type"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td align="right">
	<xsl:value-of select="file_size"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/imaging/view_doc.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="filename"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/imaging/edit_doc_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="note"/>
	</a>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

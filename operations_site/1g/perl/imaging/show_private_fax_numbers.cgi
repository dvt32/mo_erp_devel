#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $sql_query = "select * from fax_number_tbl order by fax_number";
  my (%fax_number_by_id, @fax_number_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%fax_number_by_id, \@fax_number_id_list);
  $sql_query = "select * from account_vw where id in (select account_id from fax_number_tbl)";
  my %account_by_id;
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  $sql_query = "select * from person_vw where id in (select person_id from fax_number_tbl)";
  my %person_by_id;
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <fax_number_list>
---END_BLOCK---;
  for my $fax_number_id (@fax_number_id_list) {
    my $fax_number_row = $fax_number_by_id{$fax_number_id};
    my $account_id = $fax_number_row->{"account_id"};
    my $account_name = $account_id ? HTML::Entities::encode($account_by_id{$account_id}->{"name"}) : "";
    my $person_id = $fax_number_row->{"person_id"};
    my $person_name = "";
    if ($person_id) {
      my $person = $person_by_id{$person_id};
      $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
    }
    $fax_number_row->{"fax_number"} =~ /^(\d{3})(\d{3})(\d{4})$/;
    my $fax_number = "$1-$2-$3";
    my $email_action = HTML::Entities::encode($fax_number_row->{"email_action"});
    my $cc_email = HTML::Entities::encode($fax_number_row->{"cc_email"} || "");
    my $note = HTML::Entities::encode($fax_number_row->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <fax_number>
      <id>$fax_number_id</id>
      <account_name>$account_name</account_name>
      <person_name>$person_name</person_name>
      <fax_number>$fax_number</fax_number>
      <email_action>$email_action</email_action>
      <cc_email>$cc_email</cc_email>
      <note>$note</note>
    </fax_number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </fax_number_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_private_fax_numbers.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Show Private Fax Numbers
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
        <p>
          <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
	  |
	  <a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
	  |
	  <a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
        </p>
	<xsl:apply-templates select="fax_number_list"/>
	<p>
	  Click the fax number to edit the account/person association or other properties of the fax number.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="fax_number_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
	  Private Fax Numbers
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Fax Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Person Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Email Action</td>
	<td class="standard_table_spacer_column"></td>
	<td>Cc Email</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(fax_number) > 0">
	  <xsl:apply-templates select="fax_number"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="13" align="center">No private fax numbers.</td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="fax_number">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/imaging/edit_private_fax_number_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="fax_number"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="account_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="person_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="email_action"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="cc_email"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="note"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

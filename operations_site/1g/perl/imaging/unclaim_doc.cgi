#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $document_id = $cgi->param("id");
  my $document;
  if ((!$document_id) || (!($document = select_row($dbh, "document_vw", $document_id)))) {
    die "show_page: unclaim_doc_bad_id.html\n";
  }
  my @sql_column_list;
  push(@sql_column_list, ["account_id", undef, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", undef, SQL_INTEGER]);
  push(@sql_column_list, ["note", undef]);
  my $tbl_name;
  if ($document->{"type"} eq "scan") {
    $tbl_name = "scan_tbl";
  } elsif ($document->{"type"} eq "fax") {
    $tbl_name = "fax_in_tbl";
  } else {
    die "show_page: unclaim_doc_unknown_document_type.html\n";
  }
  update_row($dbh, $tbl_name, \@sql_column_list, $document_id);
  # generate XML.
  my $type = HTML::Entities::encode($document->{"type"});
  my $filename = HTML::Entities::encode($document->{"filename"});
  my $file_size = int($document->{"file_size"} / 1024) . "K";
  my $file_timestamp = $document->{"file_timestamp"};
  my $note = HTML::Entities::encode($document->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <document>
    <id>$document_id</id>
    <type>$type</type>
    <filename>$filename</filename>
    <file_size>$file_size</file_size>
    <file_timestamp>$file_timestamp</file_timestamp>
    <note>$note</note>
  </document>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "unclaim_doc.xslt", $cgi);
}

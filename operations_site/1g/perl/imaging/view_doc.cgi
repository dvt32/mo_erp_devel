#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;
use output_file;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $document_id = $cgi->param("id");
  my $document;
  if ((!$document_id) || (!($document = select_row($dbh, "document_vw", $document_id)))) {
    die "show_page: view_doc_bad_id.html\n";
  }
  my $file_id = $document->{"file_id"};
  output_file($instance_map, $file_id);
}

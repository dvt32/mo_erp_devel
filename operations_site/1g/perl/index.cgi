#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_role_list;
use apply_xslt_output_xhtml;
use get_php_base_url;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $employee_name = HTML::Entities::encode($employee->{"first_name"} . " " . $employee->{"last_name"});
  my $role_list = get_role_list($instance_map, $employee->{"id"});
  my $php_base_url = get_php_base_url();
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <employee>
    <name>$employee_name</name>
  </employee>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <php-base-url>$php_base_url</php-base-url>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "index.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>Micro Office Solutions :: Operations Center</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css" />
	<link rel="icon" type="image/x-icon" href="/1g/favicon.ico" />
	<link rel="shortcut icon" type="image/x-icon" href="/1g/favicon.ico" />
      </head>
      <body>
	<p><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
	<p>Welcome to the operations center, <xsl:value-of select="employee/name"/>. If desired, go back to the operations <a href="/1g/../..">top level page</a>. Here are your menu options:</p>
	<ul>
	  <li>
	    Sales &amp; Marketing
	    <ul>
	      <li><a href="/1g/sales/show_prospect_list.cgi">Prospective Customers</a></li>
	      <li><a href="/1g/sales/batch_add_interactions_input.cgi">Batch Add Interactions</a></li>
<!--
	      <li><a href="/1g/sales/enroll_with_blank_form_input.cgi">Enroll New Customer with Blank Form</a></li>
	      <li><a href="/1g/sales/show_pending_enrollments.cgi">Show Pending Enrollments</a></li>
	      <li><a href="/1g/sales/renew_agreement.cgi">Renew Agreement with Existing Customer</a></li>
-->
	      <li><a href="/1g/sales/show_sales_order_list.cgi">Sales Orders</a></li>
	      <li><a href="/1g/sales/show_virtual_office_applications.cgi">Virtual Office Applications</a></li>
	      <li><a href="/1g/sales/sales_financial_report.cgi">Sales Financials</a></li>
<!--
              <xsl:if test="role-list/role = 'role:marketing/web'">
	      </xsl:if>
-->
<!--
	      <xsl:if test="role-list/role = 'role:marketing/lead_tracking'">
		<li>
		  Call Lead Tracking:
		  <a href="/1g/marketing/show_call_sources.cgi">Call Sources</a>
		  &middot;
		  <a href="/1g/marketing/find_cdr_input.cgi">Call Details</a>
		</li>
		</xsl:if>
		-->
	    </ul>
	  </li>
	  <li>
	    Licenses
	    <ul>
	      <li><a href="/1g/license/create_license_agreement.cgi">Create License Agreement</a></li>
	      <li><a href="/1g/license/list_license_agreements.cgi">Manage License Agreements</a></li>
	    </ul>
	  </li>
<!--
	  <li>
	    Legal
	    <ul>
	      <li><a href="/1g/legal/show_license_terms.cgi">License Terms</a></li>
	    </ul>
	  </li>
-->
	  <li>
	    Customers
	    <ul>
	      <li>
	      	<a href="/1g/tenant/show_active_aliases.cgi">Aliases</a>
 		&middot;
		<a href="/1g/tenant/search_input.cgi">Search</a>
		&middot;
		<a href="/1g/space/locate_tenant.cgi">Locate Tenant</a>
<!--		&middot;
		<a href="/1g/space/list_unplaced_tenants.cgi">List Unplaced Tenants</a>
-->
		&middot;
		<a href="/1g/tenant/show_active_accounts.cgi">All Active Accounts</a>
		&middot;
<!--		<a href="/1g/tenant/show_active_persons.cgi">All Active Persons</a> -->
		(<a href="/1g/tenant/download_active_persons.cgi">Download All Active Persons</a>)
		&middot;
	</li>
<!--		<a href="/1g/tenant/show_upcoming_guests.cgi">Upcoming Guests</a>
		&middot;
		<a>
		  <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/tenants/emailOptOut</xsl:attribute>
		  Email Opt-out
		</a>
	      </li>
	      <li>
		New:
		<a href="/1g/tenant/new_account_input.cgi">Account</a>
		&middot;
		<a href="/1g/tenant/new_person_input.cgi">Person</a>
		&middot;
		<a href="/1g/tenant/new_guest_input.cgi">Guest</a>
		(<a href="/1g/tenant/batch_add_guest_input.cgi">batch add</a>)
	      </li> -->
             <li>
               <a href="/1g/tenant/active_account_terms.cgi">Active account terms</a>
             </li>
	    </ul>
	  </li>
	  <li>
	    Physical Resources
	    <ul>
	      <li>
		Access Cards (<a href="/1g/tenant/proximity_card_request_change.html">Request Change</a>):
		<a href="/1g/tenant/show_proximity_cards.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/tenant/show_proximity_cards.cgi?floor_id=5">122W27F12</a>
		&middot;
		<a href="/1g/tenant/show_proximity_cards.cgi?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/1g/tenant/show_proximity_cards.cgi?floor_id=12">902BwyF7</a>
		&middot;
                <a href="/1g/tenant/show_proximity_cards.cgi?floor_id=18">18SMichF12</a>
	      </li>
	      <li>
		Mailboxes:
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=5">122W27F12</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=12">902BwyF7</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=18">18SMichF12</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=21">405WSupF03</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=27">405WSupF05</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=30">405WSupF07</a>
		&middot;
		<a href="/1g/tenant/show_mailboxes.cgi?floor_id=22">101ArchS1950</a>
	      </li>
	      <li>
		Rooms:
		<a href="/1g/meet/show_schedule.cgi?floor_id=10&amp;room_type=conference">902BwyF6</a>
		&middot;
		<a href="/1g/meet/show_schedule.cgi?floor_id=12&amp;room_type=conference">902BwyF7</a>
		&middot;
		122W27F10 (<a href="/1g/meet/show_schedule.cgi?floor_id=4&amp;room_type=conference">conference</a>,
		<a href="/1g/meet/show_schedule.cgi?floor_id=4&amp;room_type=therapy">therapy</a>)
		&middot;
		<a href="/1g/meet/show_schedule.cgi?floor_id=5&amp;room_type=conference">122W27F12</a>
	      </li>
	      <!--<li>
		Rooms (flask):
		<a href="/201605_flask/meet/reserve_room?floor_id=10&amp;room_type=conference">902BwyF6</a>
		&middot;
		<a href="/201605_flask/meet/reserve_room?floor_id=12&amp;room_type=conference">902BwyF7</a>
		&middot;
		122W27F10 (<a href="/201605_flask/meet/reserve_room?floor_id=4&amp;room_type=conference">conference</a>,
		<a href="/201605_flask/meet/reserve_room?floor_id=4&amp;room_type=therapy">therapy</a>)
		&middot;
		<a href="/201605_flask/meet/reserve_room?floor_id=5&amp;room_type=conference">122W27F12</a>
	      </li>-->
	      <li>
		Desk Maps:
		<a href="/1g/space/show_desks.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=12">902BwyF7</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=32">535FifthF16</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=33">68HarrisonF6</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=18">18SMichFl12</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=30">405WSupF7</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=43">1EWackerF8</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=34">95CCF16</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=44">95CCF16-QTS-area-3</a>
	      </li>
	      <li>
		Desk Maps (Inactive):
		<a href="/1g/space/show_desks.cgi?floor_id=5">122W27F12</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=21">405WSupF3</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=27">405WSupF5</a>
	      </li>
	      <!--<li>
		Day Passes:
		<a href="/201605_flask/daypass?floor_id=4">122W27</a>
		&middot;
		<a href="/201605_flask/daypass?floor_id=10">902Bwy</a>
		&middot;
		<a href="/201605_flask/daypass?floor_id=32">535Fifth</a>
		&middot;
		<a href="/201605_flask/daypass?floor_id=18">18SMich</a>
		&middot;
		<a href="/201605_flask/daypass?floor_id=30">405WSup</a>
		&middot;
		<a href="/201605_flask/daypass?floor_id=33">68Harrison</a>
	      </li>-->
        <li>
		Day Pass Access Codes:
		<a href="/201605_flask/accesscode">Generate Access Code(s)</a>
		&middot;
	      </li>
<!--
	      <xsl:if test="role-list/role = 'role:security/view_cameras'">
	      </xsl:if>
	      -->
	    </ul>
	  </li>
<!-- 	  <li>
	    Processes and Tasks
	    <ul>
	      <li>
		Packages: <a href="/1g/tenant/package_notification_input.cgi">Send Package Notification</a>
		&middot;
		<a href="/1g/tenant/delivered_notifications_result.cgi">Show Delivered Notifications</a>
	      </li>
          <li>
	    Tasks:
            <a>
               <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/tasks/add</xsl:attribute>
               Add Task
            </a>
         &middot;
            <a>
               <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/tasks</xsl:attribute>
               List Tasks
            </a>
            <iframe name="task_count" scrolling="no" frameborder="0" height="20px" width="20px" style="position:relative;top:3px;">
                <xsl:attribute name="src"><xsl:value-of select="php-base-url"/>/tasks/count</xsl:attribute>&nbsp;
            </iframe>

          </li>
	    </ul>
	  </li> -->
	  <li>
	    Communications and Imaging
	    <ul>
	      <li>
		Phone Numbers:
		<a href="/1g/telecom/show_phone_numbers.cgi">All</a>
		&middot;
		<a href="/1g/telecom/show_phone_numbers.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/telecom/show_phone_numbers.cgi?floor_id=5">122W27F12</a>
	      </li>
	      <li>
		Telephone System:
		<a href="/1g/telecom/show_calls.cgi">Active Calls</a>
		&middot;
		<a href="/1g/telecom/show_sofia_status.cgi">SIP Registrations</a>
		&middot;
		<a href="/1g/telecom/send_sms_input.cgi">SMS</a>
		&middot;
		<a href="/1g/telecom/show_ip_phones.cgi">IP Phones</a>
	      </li>
	      <li>
		Data Network:
		<a href="/1g/telecom/show_ip_addresses.cgi">IP Addresses</a>
		&middot;
		<a>
		  <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/network_monitor</xsl:attribute>
		  <xsl:text>Network Monitoring</xsl:text>
		</a>
		<xsl:if test="role-list/role = 'role:telecom/change_default_route'">
		  &middot;
		  122W27
		  (<a href="/1g/telecom/change_default_route.cgi?building_id=3&amp;route=primary">primary</a>
		  &middot;
		  <a href="/1g/telecom/change_default_route.cgi?building_id=3&amp;route=backup">backup</a>)
		</xsl:if>
	      </li>
	      <li>
		Scan/Fax:
		<a href="/1g/imaging/show_unclaimed_scans.cgi">Unclaimed Scans</a>
		&middot;
		<a href="/1g/imaging/show_unclaimed_faxes.cgi">Unclaimed Faxes</a>
		&middot;
		<a href="/1g/imaging/show_claimed_docs.cgi">Claimed Documents</a>
		&middot;
		<a href="/1g/imaging/send_fax_input.cgi">Send Fax</a>
		&middot;
		<a href="/1g/imaging/show_private_fax_numbers.cgi">Private Fax Numbers</a>
	      </li>
	      <li>
		Email:
		<a href="https://exchange.microoffice.com/owa">Outlook Web App (OWA)</a>, also available at mail.microoffice.com
	      </li>
	    </ul>
	  </li>
	  <li>
	    Human Resources
	    <ul>
	      <li>
		Time Management:
		<a href="/1g/hr/clock_in_input.cgi">Clock In</a>
		&middot;
		<a href="/1g/hr/clock_out_input.cgi">Clock Out</a>
		&middot;
		<a href="/1g/hr/time_report.cgi">Time Report</a>
		<xsl:if test="role-list/role = 'role:hr/view_all_time_sheets'">
		  &middot;
		  <a href="/1g/hr/today_employee_times.cgi">Today's Employee Times</a>
		  &middot;
		  <a href="/1g/hr/pay_period_report.cgi">Pay Period Report</a>
		</xsl:if>
	      </li>
	      <li>
		Days Off/Absences:
		<a href="/1g/hr/holiday_list.cgi">Holidays</a>
		&middot;
		<a href="/1g/hr/vacation_list.cgi">Vacation Days</a>
		&middot;
		<a href="/1g/hr/sick_list.cgi">Sick Days</a>
		&middot;
		<a href="/1g/hr/unpaid_list.cgi">Unpaid Days</a>
		<xsl:if test="role-list/role = 'role:hr/review_absences'">
		  &middot;
		  <a href="/1g/hr/show_absences.cgi">Review Absences</a>
		</xsl:if>
	      </li>
	      <li><a href="/1g/hr/employee_directory.cgi">Employee Directory</a></li>
	    </ul>
	  </li>
	  <li>
	    Finance
	    <ul>
              <xsl:if test="role-list/role = 'role:finance/receive_payment'">
		<li>
		  Receive One-Time Payment:
		  <a href="/1g/finance/receive_cash_input.cgi">Cash</a>
		  &middot;
		  <a href="/1g/finance/receive_check_input.cgi">Check</a>
		  <xsl:if test="role-list/role = 'role:finance/receive_payment/card'">
		    &middot;
		    <a href="/1g/finance/receive_card_input.cgi">Credit/Debit Card</a>
		  </xsl:if>
		  <xsl:if test="role-list/role = 'role:finance/receive_payment/ach'">
		    &middot;
		    <a href="/1g/finance/receive_ach_input.cgi">ACH Debit</a>
		  </xsl:if>
		</li>
	      </xsl:if>
	      <li>
		<a href="/1g/finance/wiring_instructions.html">Wiring Instructions</a>
	      </li>
              <xsl:if test="role-list/role = 'role:finance/make_deposit'">
		<li>
		  Deposit Items:
		  <a href="/1g/finance/make_deposit_input.cgi">Make Deposit</a>

		  <xsl:if test="role-list/role = 'role:finance/show_deposits'">
		    &middot;
		    <a href="/1g/finance/show_deposits.cgi">Show Deposits</a>
		  </xsl:if>
		</li>
	      </xsl:if>
	      <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
		<li>
		  Credit/Debit Card Transactions:
		  <a href="/1g/finance/show_processed_card_txns.cgi">Processed</a>
		  &middot;
		  <a href="/1g/finance/show_pending_card_txns.cgi">Pending</a>
		  &middot;
		  <a href="/1g/finance/show_recur_card_txns.cgi">Recurring</a>
		</li>
	      </xsl:if>
	      <li>
		NMI:
		<a href="/1g/../201605_flask/finance/show_pmt_vaults">Show NMI Vaults</a>
		&middot;
		<a href="/1g/../201605_flask/finance/show_pmt_nmi_txns">Show NMI Transactions</a>
		&middot;
		<a href="/1g/../201605_flask/finance/show_check_returns">Show Check Returns</a>
		&middot;
		<a href="/1g/../201605_flask/finance/query_pmt_nmi_txn">Query NMI Transactions</a>
	      </li>
<!--
	      <xsl:if test="role-list/role = 'role:finance/show_electronic_txns'">
		<li>
		  ACH Debit Transactions:
		  <a href="/1g/finance/show_processed_ach_txns.cgi">Processed</a>
		  &middot;
		  Pending
		  &middot;
		  Recurring
		  &middot;
		  Batches
		</li>
	      </xsl:if>
-->
              <xsl:if test="role-list/role = 'role:finance/reconcile_pmts'">
		<li>
		  Reconcile Payments:
		  <a href="/1g/finance/reconcile_cash_pmts_input.cgi">Cash</a>
		  &middot;
		  <a href="/1g/finance/reconcile_check_pmts_input.cgi">Check</a>
		  &middot;
		  <a href="/1g/finance/reconcile_ach_pmts_input.cgi">ACH Debit</a>
		</li>
	      </xsl:if>
              <xsl:if test="role-list/role = 'role:finance/edit_cust_pmt_accts'">
		<li>
		  Customer Payment Accounts:
		  <a href="/1g/finance/show_cust_cards.cgi">Credit/Debit Card</a>
		  &middot;
		  <a href="/1g/finance/show_cust_bank_accts.cgi">Bank Account</a>
		</li>
	      </xsl:if>
<!--
	      <li><a href="/1g/finance/show_receivables_aging_summary.cgi">Receivables Aging Summary</a> (can take up to 30 seconds to load)</li>
	      <li><a href="/1g/finance/show_open_invoices.cgi">Open Invoices</a> (can take up to 30 seconds to load)</li>
	      <li><a href="/1g/finance/show_quickbooks_queued_operations.cgi">QuickBooks Queued Operations</a></li>
-->
              <xsl:if test="role-list/role = 'role:finance/approve_electronic_txns'">
		<li>
		  <a href="/1g/finance/show_scheduled_electronic_checks.cgi">Scheduled Electronic Checks</a>
		  &middot;
		  <a href="/1g/finance/show_scheduled_ach_txns.cgi">Pending ACH Debits</a>
		  &middot;
		  <a href="/1g/finance/show_processed_ach_txns.cgi">Past Processed ACH Debits</a>
		</li>
	      </xsl:if>
	      <li>
		Vault Transactions:
		<a href="/201605_flask/finance/pending_sales">Pending Vault Sales</a>
		&middot;
		<a href="/201605_flask/finance/enqueue_recur">Enqueue Recurring Payments</a>
	      </li>
	      <li>
		Expenses:
		<a href="/1g/finance/new_expense_input.cgi">New Expense</a>
		&middot;
		<a href="/1g/finance/show_expenses.cgi">Recent Expenses</a>
		<xsl:if test="role-list/role = 'role:finance/view_all_expenses'">
		  &middot;
		  <a href="/1g/finance/reconcile_expenses_input.cgi">Reconcile Expenses</a>
		</xsl:if>
	      </li>
	      <li>
		Vendor Payment:
		<a href="/1g/finance/new_vend_ach_pmt_input.cgi">New Vendor ACH Payment</a>
		&middot;
		<a href="/1g/finance/new_vend_check_pmt_input.cgi">New Vendor Check Payment</a>
	      </li>
	      <!--<li>
		Financial Analysis:
		<a href="/201605_flask/finance/analysis?floor_id=4">122W27F10</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=5">122W27F12</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=12">902BwyF7</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=32">535FifthF16</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=33">68HarrisonF6</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=18">18SMichFl12</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=21">405WSupF3</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=27">405WSupF5</a>
		&middot;
		<a href="/201605_flask/finance/analysis?floor_id=30">405WSupF7</a>
	      </li>-->
	      <li>
		Manage Report Recipients:
		<a href="/201605_flask/finance/report?building_id=3">122W27</a>
		&middot;
		<a href="/201605_flask/finance/report?building_id=6">902Bwy</a>
		&middot;
		<a href="/201605_flask/finance/report?building_id=27">535Fifth</a>
		&middot;
		<a href="/201605_flask/finance/report?building_id=26">68Harrison</a>
		&middot;
		<a href="/201605_flask/finance/report?building_id=14">18SMich</a>
		&middot;
		<a href="/201605_flask/finance/report?building_id=17">405WSup</a>
		&middot;
		<a href="/201605_flask/finance/report?building_id=28">95CC</a>
		&middot;
		<a href="/201605_flask/finance/report?building_id=32">1EWacker</a>
	      </li>

<!--
	      <li><a href="/1g/finance/misc_billing_input.cgi">Bill Tenant for Miscellaneous Product/Service</a></li>
-->
	    </ul>
	  </li>
          <xsl:if test="role-list/role = 'role:corporate/company'">
	  <li>
	    Corporate
	    <ul>
		<li>
		  Companies:
		  <a href="/1g/corporate/show_companies.cgi">Show Companies</a>
		</li>
	    </ul>
	  </li>
          </xsl:if>
	  <li>
	    External Resources
	    <ul>
	      <li>
	        <a href="/1g/external/show_passwords.cgi">Passwords</a>
	      </li>
	    </ul>
	  </li>
	  <li>
	    Tools
	    <ul>
	      <li>
		<a href="/201605_flask/essensys/_update_accounts">Sync Essensys Account IDs</a>
	      </li>
	      <li>
		<a href="/201605_flask/essensys/_update_licenses">Sync Essensys Licenses</a>
	      </li>
	      <!--<li>
		<a href="/201605_flask/tools/disp_tbl">Query Table</a>
	      </li>-->
	    </ul>
	  </li>
	  <li>
	    Printed (Paper) Reports
	    <ul>
	      <li>
		Evening and Weekend Access by Company:
		<a href="/1g/report/evening_weekend_access_by_company.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/report/evening_weekend_access_by_company.cgi?floor_id=5">122W27F12</a>
	      </li>
	      <li>
		Evening and Weekend Access by Person:
		<a href="/1g/report/evening_weekend_access_by_person.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/report/evening_weekend_access_by_person.cgi?floor_id=5">122W27F12</a>
	      </li>
	      <li>
		Guest List:
		<a href="/1g/report/guest_list.cgi?building_id=11">1601Bwy</a>
	      </li>
	      <li>
		Delivery List:
		<a href="/1g/report/delivery_list.cgi?building_id=11">1601Bwy</a>
	      </li>
	      <li>
		Intercom Directory by Company:
		<a href="/1g/report/intercom_directory_by_company.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/report/intercom_directory_by_company.cgi?floor_id=5">122W27F12</a>
		&middot;
		<a href="/1g/report/intercom_directory_by_company.cgi?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/1g/report/intercom_directory_by_company.cgi?floor_id=12">902BwyF7</a>
	      </li>
	      <li>
		Intercom Directory by Person:
		<a href="/1g/report/intercom_directory_by_person.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/report/intercom_directory_by_person.cgi?floor_id=5">122W27F12</a>
		&middot;
		<a href="/1g/report/intercom_directory_by_person.cgi?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/1g/report/intercom_directory_by_person.cgi?floor_id=12">902BwyF7</a>
	      </li>
	      <li>
		Mailbox Mappings by Name:
		<a href="/1g/report/mailbox_mapping_by_name.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_name.cgi?floor_id=5">122W27F12</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_name.cgi?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_name.cgi?floor_id=12">902BwyF7</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_name.cgi?floor_id=18">18SMichF12</a>
	      </li>
	      <li>
		Mailbox Mappings by Number:
		<a href="/1g/report/mailbox_mapping_by_number.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_number.cgi?floor_id=5">122W27F12</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_number.cgi?floor_id=10">902BwyF6</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_number.cgi?floor_id=12">902BwyF7</a>
		&middot;
		<a href="/1g/report/mailbox_mapping_by_number.cgi?floor_id=18">18SMichF12</a>
	      </li>
	    </ul>
	  </li>
	  <li>
	    Mobile View
	    <ul>
	      <li>
		Phone Numbers:
		<a href="/1g/telecom/show_phone_numbers.cgi?view=mobile">All</a>
		&middot;
		<a href="/1g/telecom/show_phone_numbers.cgi?view=mobile&amp;floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/telecom/show_phone_numbers.cgi?view=mobile&amp;floor_id=5">122W27F12</a>
	      </li>
	    </ul>
	  </li>
	  <li>
	    Documentation and Help
	    <ul>
	      <li>
		System:
		<a href="/1g/doc/login_non_windows.cgi">Log in on Non-Windows (Mac or Chromebook, etc.)</a>
		&middot;
		<a href="/1g/doc/change_password.html">Change password</a>
	      </li>
	      <li>
		<a href="/1g/doc/exchange_server.cgi">Microsoft Exchange Server account (Outlook, iPhone/Android, web mail)</a>
	      </li>
	      <li>
		<a href="/1g/doc/applications.cgi">Applications</a>
		&middot;
		<a href="/1g/doc/map_network_drive.cgi">Map network drive for file server access</a>
		&middot;
		<a href="/1g/doc/vpn_access.cgi">VPN access</a>
		&middot;
		<a href="/1g/doc/remote_desktop_access.cgi">Remote desktop access</a>
		&middot;
		<a href="/1g/doc/msra.cgi">Microsoft Remote Assistance (msra)</a>
	      </li>
	      <li>
                <a href="/1g/doc/navicat.cgi">Navicat connection</a>
		&middot;
		<a href="/1g/doc/odbc.cgi">ODBC database access</a>
	      </li>
	      <li>
		<a href="/1g/doc/ip_address.html">Determine IP Address</a>
	      </li>
	      <li>
		<a href="/1g/doc/screenconnect.cgi">ScreenConnect usage</a>
	      </li>
	    </ul>
	  </li>
	</ul>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

function create_camera(url,fps) {
	var DisplayWidth = "320";
	var DisplayHeight = "240";

	var File = "/axis-cgi/mjpg/video.cgi?resolution=320x240&fps="+fps;

	var output = "";
	if ((navigator.appName == "Microsoft Internet Explorer") &&
		 (navigator.platform != "MacPPC") && (navigator.platform != "Mac68k"))
	{
		// If Internet Explorer under Windows then use ActiveX 
		output  = '<object class="player" width=';
		output += DisplayWidth;
		output += ' height=';
		output += DisplayHeight;
		output += ' classid="CLSID:DE625294-70E6-45ED-B895-CFFA13AEB044" ';
		output += 'codebase="';
		output += url;
		output += '/activex/AMC.cab#version=3,32,31,0">';
		output += '<param name="MediaURL" value="';
		output += url;
		output += File + '" />';
		output += '<param name="MediaType" value="mjpeg"/>';
		output += '<param name="ShowStatusBar" value="0"/>';
		output += '<param name="ShowToolbar" value="0"/>';
		output += '<param name="AutoStart" value="1"/>';
		output += '<param name="StretchToFit" value="1"/>';
		output += '<br /><b>Axis Media Control</b><br />';
		output += 'The AXIS Media Control, which enables you ';
		output += 'to view live image streams in Microsoft Internet';
		output += ' Explorer, could not be registered on your computer.';
		output += '<br /></object>';
	} else {
		// If not IE for Windows use the browser itself to display
		theDate = new Date();
		output  = '<a href="';
		output += url;
		output += '"><img class="grid" src="';
		output += url;
		output += File;
		output += '&dummy=' + theDate.getTime().toString(10);
		output += '" height="';
		output += DisplayHeight;
		output += '" width="';
		output += DisplayWidth;
		output += '" alt="Camera Image"/></a>';
	}
	
	document.write(output);
}

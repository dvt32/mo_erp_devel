function clear_option_list (select) {
	select.options.length = 0;
}

function add_to_option_list (select, text, value) {
	text = text.replace(/&amp;/, "\&");
	length = select.options.length;
	select.options[length] = new Option(text, value);
}

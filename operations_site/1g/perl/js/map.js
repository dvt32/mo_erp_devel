function mapover(that,div) {
  var myString = new String(that.coords);
  var myArray = myString.split(',');
	if (document.getElementById(div)) {
		document.getElementById(div).style.clip='rect('+myArray[1]+'px '+myArray[2]+'px '+myArray[3]+'px '+myArray[0]+'px)';
	} else {
		document.getElementById('orangepic').style.clip='rect('+myArray[1]+'px '+myArray[2]+'px '+myArray[3]+'px '+myArray[0]+'px)';
	}
  if (document.getElementById('seat' + that.alt)) document.getElementById('seat' + that.alt).style.display = 'block';
}

function mapout(that,div) {
  if (document.getElementById(div)) {
    document.getElementById(div).style.clip='rect(0px 0px 0px 0px)';
  } else {
    document.getElementById('orangepic').style.clip='rect(0px 0px 0px 0px)';
  }
  if (document.getElementById('altpic2')) {
    document.getElementById('altpic2').style.clip='rect(0px 0px 0px 0px)';
  }
  if (that.alt) {
    if (document.getElementById('seat' + that.alt)) document.getElementById('seat' + that.alt).style.display = 'none';
  }
}


$(document).ready(function() {
    update_deskmap_legend();
    //get_vo_desks('/201605_flask/space/get_vo_desks?floor_id=' + getUrlParameter('floor_id'));
});

function update_deskmap_legend() {
    /** Dynamic deskmap legend (1st of next, following, and following month) **/
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    // Get current date
    var now = new Date();

    for (var i = 1; i < 4; i++) {
	// Get array index of next month (0 to 11)
	var future_month = new Date(now.getFullYear(), now.getMonth() + i, 1);

	// Get name
	var month_name = months[future_month.getMonth()];

	// Update element
	document.getElementById("legend-" + i + "-mo").innerHTML = "Availability by " + month_name + " 1st";
    }
}

function get_vo_desks(url) {
    /** Async request for virtual office desks **/
    $.ajax({
	url: url,
	success: function(data) {
	    populate_table(data);
	}
    });
}

function populate_table(response) {
    /** Deprecated. Now populated via space/show_desks.cgi **/
    /** Transforms json response into table with active virtual accounts **/
    var essensys_url = "https://operate-us.essensys.tech/spacecontrol/mod/1/view/";
    var table = document.getElementById('virtual_office_table');
    table.innerHTML = '';

    // Generate header
    var th_row = document.createElement('tr');

    var headers = ['Client', 'Mailbox'];
    for (var i in headers) {
	var th = document.createElement('th');
	th.innerHTML = headers[i];
	th_row.appendChild(th);
    }
    table.appendChild(th_row);

    // Generate Body
    for (var i in response) {
	var tr = document.createElement('tr');
	var td_1 = document.createElement('td');
	var td_2 = document.createElement('td');
	var link = document.createElement('a');
	link.href = essensys_url + response[i]['accountname'];
	link.innerHTML = response[i]['outputvalue_accountname'];

	td_1.appendChild(link);
	// Only populate mailbox if it's not null
	td_2.innerHTML = response[i]['mailbox_number'] || '';

	tr.appendChild(td_1);
	tr.appendChild(td_2);
	table.appendChild(tr);
    }
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	sURLVariables = sPageURL.split('&'),
	sParameterName,
	i;

    for (i = 0; i < sURLVariables.length; i++) {
	sParameterName = sURLVariables[i].split('=');

	if (sParameterName[0] === sParam) {
	    return sParameterName[1] === undefined ? true : sParameterName[1];
	}
    }
}

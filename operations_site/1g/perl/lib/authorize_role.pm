use strict;
local($^W) = 1;

use get_employee;
use get_role_list;

# check if logged-in employee is member of $role.
# dies if no. if yes, returns a reference to a list of roles for the logged-in employee.
sub authorize_role($$) {
  my ($instance_map, $role) = @_;
  my $employee = get_employee($instance_map);
  my $role_list = get_role_list($instance_map, $employee->{"id"});
  if (!grep {$_ eq $role} @$role_list) {
    warn qq{Unauthorized access by $employee->{"first_name"} $employee->{"last_name"} (ID: $employee->{"id"})};
    die "show_page: ../auth/not_authorized.html\n";
  }
  return $role_list;
}

1;

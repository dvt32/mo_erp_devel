use strict;
local($^W) = 1;

use min;
use max;

# coordinate list as array: ([x1,y1],[x2,y2],[x3,y3],...)
# each coordinate c is array ref [x,y] with c->[0] = x, c->[1] = y
# returns array of two coordinates: ([x1,y1],[x2,y2])
# in return value, [x1,y1] is lower left corner and [x2,y2] is upper right corner
# assumes coordinate system where (0,0) is lower left and (1,1) is upper right quadrant.
sub bounding_box(@) {
  my @coordinate_list = @_;
  my $lower_left_x = min(map {$_->[0]} @coordinate_list);
  my $lower_left_y = min(map {$_->[1]} @coordinate_list);
  my $upper_right_x = max(map {$_->[0]} @coordinate_list);
  my $upper_right_y = max(map {$_->[1]} @coordinate_list);
  return ([$lower_left_x, $lower_left_y], [$upper_right_x, $upper_right_y]);
}

1;

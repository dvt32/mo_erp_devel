use strict;
local($^W) = 1;

# takes a sorted tree and flattens it into a list of nodes, walking depth first. also adds depth attribute to each node to
# make it easy to construct indented lists. depth is 1 for children of the root and increases from there.
# returns list of nodes in depth-first order.
sub flatten_sorted_tree {
  my ($tree) = @_;
  my @sorted_node_list;
  my @traversal_path = ($tree);
  my @child_index_list = (0);
  while(@traversal_path) {
    my $current_node = $traversal_path[-1];
    my $child_list = $current_node->{"child_list"};
    my $current_child_index = $child_index_list[-1];
    if ((!$child_list) || (scalar(@$child_list) <= $current_child_index)) {
      pop(@traversal_path);
      pop(@child_index_list);
      if (@child_index_list) {
	# go to successor node at same level as parent.
	$child_index_list[-1]++;
      }
      next;
    }
    my $current_child = $child_list->[$current_child_index];
    $current_child->{"depth"} = scalar(@traversal_path);
    push(@sorted_node_list, $current_child);
    my $grandchild_list = $current_child->{"child_list"};
    if (($grandchild_list) && (scalar(@$grandchild_list) > 0)) {
      push(@traversal_path, $current_child);
      push(@child_index_list, 0);
    } else {
      # go to successor node at same level as current node.
      $child_index_list[-1]++;
    }
  }
  return \@sorted_node_list;
}

1;

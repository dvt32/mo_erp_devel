use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();

use timestamp_to_minutes;
use lead_pad;
use display_time_to_minutes;
use minutes_to_display_time;
use day_of_week;
use get_previous_date;
use get_next_date;
use get_day_name;
use get_month_name;
use get_building_id_from_floor_id;

# query_date = yyyy-mm-dd
# time_slot_increment = factor of 60
sub generate_xml_for_room_schedule(%) {
  my %param_map = @_;
  my $dbh = $param_map{"database_handle"};
  my $floor_id = $param_map{"floor_id"};
  my $query_date = $param_map{"query_date"};
  my @room_list = @{$param_map{"room_list"}};
  my $time_slot_increment = $param_map{"time_slot_increment"};
  if ($query_date !~ /^\d{4}-\d\d-\d\d$/) {
    die "bad query_date";
  }
  my $sth;
  # set up reservation grid.
  my %reservation_by_id;
  # reservation_grid_by_room_id is a hash with the following structure:
  #   (room_id, reservation_grid)
  # reservation_grid is an anonymous hash with the following structure:
  #   (time in minutes, reservation_id)
  my %reservation_grid_by_room_id;
  # initialize reservation grid for each room.
  for my $room (@room_list) {
    my $room_id = $room->{"id"};
    my $reservation_grid = {};
    $reservation_grid_by_room_id{$room_id} = $reservation_grid;
  }
  # fetch reservations in all rooms on the tenant's floor. populate the reservation grid.
  # time slots are in 15 minute increments. time slot increment must divide evenly into 60.
  $sth = $dbh->prepare("select * from reservation_tbl where room_id in (select id from room_tbl where floor_id = ? and is_active) and (start_dt, end_dt) overlaps (date '$query_date', date '$query_date' + interval '1 day') order by start_dt");
  $sth->bind_param(1, $floor_id, SQL_INTEGER);
  $sth->execute();
  my ($earliest_start_time_minutes, $latest_end_time_minutes);
  while(my $row = $sth->fetchrow_hashref()) {
    my $room_id = $row->{"room_id"};
    my $reservation_id = $row->{"id"};
    $reservation_by_id{$reservation_id} = $row;
    my $start_time_minutes = timestamp_to_minutes($row->{"start_dt"});
    if (defined($earliest_start_time_minutes)) {
      if ($start_time_minutes < $earliest_start_time_minutes) {
	$earliest_start_time_minutes = $start_time_minutes;
      }
    } else {
      $earliest_start_time_minutes = $start_time_minutes;
    }
    my $end_time_minutes = timestamp_to_minutes($row->{"end_dt"});
    if (defined($latest_end_time_minutes)) {
      if ($end_time_minutes > $latest_end_time_minutes) {
	$latest_end_time_minutes = $end_time_minutes;
      }
    } else {
      $latest_end_time_minutes = $end_time_minutes;
    }
    # populate reservation grid.
    for(my $time_slot = $start_time_minutes; $time_slot < $end_time_minutes; $time_slot += $time_slot_increment) {
      $reservation_grid_by_room_id{$room_id}->{$time_slot} = $reservation_id;
    }
  }
  $sth->finish();
  # fetch accounts.
  my %account_by_id;
  my $building_id = get_building_id_from_floor_id($dbh, $floor_id);
  $sth = $dbh->prepare("select * from account_vw where id in (select account_id from reservation_tbl where room_id in (select id from room_tbl where is_active and floor_id in (select id from floor_tbl where building_id = ?)) and (start_dt, end_dt) overlaps (date '$query_date', date '$query_date' + interval '1 day'))");
  $sth->bind_param(1, $building_id, SQL_INTEGER);
  $sth->execute();
  while(my $row = $sth->fetchrow_hashref()) {
    my $account_id = $row->{"id"};
    $account_by_id{$account_id} = $row;
  }
  $sth->finish();
  # generate XML.
  my $default_start_time = "09:00";
  my $default_end_time = "17:00";
  my ($start_time_minutes, $end_time_minutes);
  $start_time_minutes = display_time_to_minutes($default_start_time);
  if (defined($earliest_start_time_minutes) && ($earliest_start_time_minutes < $start_time_minutes)) {
    $start_time_minutes = $earliest_start_time_minutes;
  }
  $end_time_minutes = display_time_to_minutes($default_end_time);
  if (defined($latest_end_time_minutes) && ($latest_end_time_minutes > $end_time_minutes)) {
    $end_time_minutes = $latest_end_time_minutes;
  }
  my ($today_year, $today_month_number, $today_day_number, $today_day_of_week) = (localtime(time()))[5, 4, 3, 6];
  $today_year += 1900;
  $today_month_number++;
  my ($schedule_year, $schedule_month_number, $schedule_day_number, $schedule_day_of_week);
  $query_date =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  ($schedule_year, $schedule_month_number, $schedule_day_number) = (int($1), int($2), int($3));
  $schedule_day_of_week = day_of_week($schedule_month_number, $schedule_day_number, $schedule_year);
  my ($previous_day_year, $previous_day_month, $previous_day_day) = get_previous_date($schedule_year, $schedule_month_number, $schedule_day_number);
  $previous_day_year = lead_pad($previous_day_year, 4);
  $previous_day_month = lead_pad($previous_day_month, 2);
  $previous_day_day = lead_pad($previous_day_day, 2);
  my $previous_day_date = "$previous_day_year-$previous_day_month-$previous_day_day";
  my ($next_day_year, $next_day_month, $next_day_day) = get_next_date($schedule_year, $schedule_month_number, $schedule_day_number);
  $next_day_year = lead_pad($next_day_year, 4);
  $next_day_month = lead_pad($next_day_month, 2);
  $next_day_day = lead_pad($next_day_day, 2);
  my $next_day_date = "$next_day_year-$next_day_month-$next_day_day";
  my $schedule_padded_month_number = lead_pad($schedule_month_number, 2);
  my $schedule_padded_day_number = lead_pad($schedule_day_number, 2);
  my $schedule_day_name = get_day_name($schedule_day_of_week);
  my $schedule_month_name = get_month_name($schedule_month_number);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<schedule>
  <date>
    <year>$schedule_year</year>
    <month-name>$schedule_month_name</month-name>
    <month-number>$schedule_month_number</month-number>
    <padded-month-number>$schedule_padded_month_number</padded-month-number>
    <day-number>$schedule_day_number</day-number>
    <padded-day-number>$schedule_padded_day_number</padded-day-number>
    <day-name>$schedule_day_name</day-name>
    <previous-date>$previous_day_date</previous-date>
    <next-date>$next_day_date</next-date>
  </date>
  <room-list>
---END_BLOCK---;
  for my $room (@room_list) {
    my $room_id = $room->{"id"};
    my $room_name = HTML::Entities::encode($room->{"short_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <room>
      <room-id>$room_id</room-id>
      <room-name>$room_name</room-name>
    </room>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </room-list>
  <reservation-list>
---END_BLOCK---;
  for(my $time_slot = $start_time_minutes; $time_slot < $end_time_minutes; $time_slot += $time_slot_increment) {
    my $display_time = minutes_to_display_time($time_slot, 1);
    $generated_xml .= <<"---END_BLOCK---;";
    <time-block>
      <start-time>
        <minutes-since-midnight>$time_slot</minutes-since-midnight>
        <display-time>$display_time</display-time>
      </start-time>
---END_BLOCK---;
    for(my $room_index = 0; $room_index < scalar(@room_list); $room_index++) {
      $generated_xml .= <<"---END_BLOCK---;";
      <room-reservation>
---END_BLOCK---;
      my $room = $room_list[$room_index];
      my $room_id = $room->{"id"};
      my $reservation_grid = $reservation_grid_by_room_id{$room_id};
      my $reservation_id = $reservation_grid->{$time_slot};
      if (defined($reservation_id)) {
	my $reservation = $reservation_by_id{$reservation_id};
	my $account_id = $reservation->{"account_id"};
	my $account_name = HTML::Entities::encode($account_by_id{$account_id}->{"name"});
	my $is_start = "false";
	my $is_end = "false";
	if (timestamp_to_minutes($reservation->{"start_dt"}) == $time_slot) {
	  $is_start = "true";
	}
	if (timestamp_to_minutes($reservation->{"end_dt"}) == $time_slot + $time_slot_increment) {
	  $is_end = "true";
	}
	$generated_xml .= <<"---END_BLOCK---;";
        <occupied-slot>
          <is-start>$is_start</is-start>
          <is-end>$is_end</is-end>
          <reservation-id>$reservation_id</reservation-id>
          <account-id>$account_id</account-id>
          <account-name>$account_name</account-name>
        </occupied-slot>
---END_BLOCK---;
      } else {
	$generated_xml .= <<"---END_BLOCK---;";
        <available-slot/>
---END_BLOCK---;
      }
      $generated_xml .= <<"---END_BLOCK---;";
      </room-reservation>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
    </time-block>
---END_BLOCK---;
    }
  $generated_xml .= <<"---END_BLOCK---;";
  </reservation-list>
</schedule>
---END_BLOCK---;
  return $generated_xml;
}

1;

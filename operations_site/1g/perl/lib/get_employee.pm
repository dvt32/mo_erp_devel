use strict;
local($^W) = 1;

use get_cgi;
use select_row_with_query;
use get_web_root_dir;
use get_database_handle;
use get_database_charset;

sub get_employee($) {
  my $instance_map = shift;
  my $web_root_dir = get_web_root_dir();
  if (!$instance_map->{"employee"}) {
    my $logon_name = $ENV{"REMOTE_USER"};

    # make logon_name lowercase to avoid case problems and strip the domain, if any.
    $logon_name = lc($logon_name);
    $logon_name =~ s/\@.*//;
    if (!$logon_name) {
      # i don't think we can make it here
      die "show_page: $web_root_dir/auth/missing_logon_name.html\n";
    }

    my $dbh = get_database_handle($instance_map, "main");
    my $db_charset = get_database_charset($instance_map, "main");
    my $sql_query = "select * from employee_tbl where windows_logon_name = ? and is_active";
    my @sql_param_list = (Encode::encode($db_charset, $logon_name));
    my $employee = select_row_with_query($dbh, $sql_query, \@sql_param_list);

    # no row with matching logon name found, or employee does not have login account. display error.
    if (!$employee) {
      # i don't think we can make it here
      die "show_page: $web_root_dir/auth/no_matching_logon_name.html\n";
    }
    $instance_map->{"employee"} = $employee;
  }
  return $instance_map->{"employee"};
}

1;

use strict;
local($^W) = 1;

use is_production_env;

sub get_php_base_url {
  # php base url for template
  my $php_base_url = "{{ php_base_url }}";
  if(is_production_env()){
    $php_base_url = "https://operations-php.microoffice.com";
  }
  return $php_base_url;
}

1;

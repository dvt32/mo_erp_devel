use strict;
local($^W) = 1;

use is_production_env;

sub get_public_id_cookie_name() {
  if (is_production_env()) {
    return "operations.microoffice.com:public_id";
  } else {
    return "operations-devel.microoffice.com:public_id";
  }
}

1;

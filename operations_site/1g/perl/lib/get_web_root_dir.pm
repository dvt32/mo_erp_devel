use strict;
use global_config;
local($^W) = 1;


my $web_root_dir = global_config::lookup("web_root_dir");

sub get_web_root_dir() {
  return $web_root_dir;
}

1;

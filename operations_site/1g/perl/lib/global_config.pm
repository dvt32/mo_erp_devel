package global_config;
use strict;
local($^W) = 1;

## camera/show_archvied_cameras.cgi
my %global_config_hash = (
## hr/employee_directory.cgi
            b2f3_password => 'MOSf3b2!',
            b2f6_password => 'MOSf6b2!',
            b2f10_password => 'MOSf10b2!',
            b2f11_password => 'MOSf11b2!',
            b3f10_password => 'MOSf10b3!',
	    b6f6_password => 'MOSf6b6!',
	    b8f19_password => 'blinkspace99!',
            b11f12_password => 'MOSf12b11!',
            b15f1_password => 'MOSf1b15!',
            b27f16_password => "dara\@atEz4",
            b28f16_password => "b28F16temp",
## lib/get_web_root_dir.pm
            web_root_dir => '{{ operations_basedir }}/1g',
## telecom/show_sofia_status.cgi
	    status_server => 'jojo.microoffice.com:8080',
	    status_db => 'freeswitch',
	    status_user => 'xxxusernamexxx',
	    status_pass => 'xxxpasswordxxx',
	    status_url => 'http://jojo.microoffice.com:8080/webapi/sofia?status%20profile%20internal',
## telecome/show_calls.cgi
	    calls_db => 'freeswitch',
            calls_user => 'xxxusernamexxx',
            calls_pass => 'xxxpasswordxxx',
	    calls_client => 'http://jojo.microoffice.com:8080/RPC2'
			  );

sub lookup {
    my $key = shift(@_);
    if (exists($global_config_hash{$key})) {
        return $global_config_hash{$key};
    } else {
        die "global config key not found: $key";
    }
}

1;

use strict;
local($^W) = 1;

use get_cgi;
use get_employee;
use get_web_root_dir;
use get_database_handle;

sub init_login_standard($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = (get_cgi($instance_map), get_database_handle($instance_map, "main"), get_employee($instance_map));
  if (!$employee) {
    # should not get here since Apache controls authentication, but display error just in case.
    my $web_root_dir = get_web_root_dir();
    die "show_page: $web_root_dir/auth/not_authenticated.html\n";
  }
  return ($cgi, $dbh, $employee);
}

1;

use strict;
local($^W) = 1;

sub max(@) {
  return (sort {$b <=> $a} @_)[0];
}

1;

use strict;
local($^W) = 1;

sub min(@) {
  return (sort {$a <=> $b} @_)[0];
}

1;

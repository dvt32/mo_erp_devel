use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use DBD::Pg qw(:pg_types);
use get_id;
use is_in_txn;
use insert_row;
use multi_insert_sth;

# owner_type: employee | customer | vendor.
# query_param: optional parameters of query in caller-specific format.
# returns ID of newly created result list.
sub new_result_list($$$$$;$) {
  my ($dbh, $owner_type, $owner_id, $table_name, $id_list, $query_param) = @_;
  if ((!$id_list) || (!@$id_list)) {
    die "new_result_list: ID list is empty or unspecified";
  }
  my $is_in_txn = is_in_txn($dbh);
  if (!$is_in_txn) {
    $dbh->begin_work();
  }
  my @sql_column_list;
  push(@sql_column_list, ["owner_type", $owner_type]);
  push(@sql_column_list, ["owner_id", $owner_id, SQL_INTEGER]);
  push(@sql_column_list, ["table_name", $table_name]);
  if ($query_param) {
    push(@sql_column_list, ["query_param", $query_param]);
  }
  my $result_list_id = insert_row($dbh, "result_list_tbl", \@sql_column_list, "result_list_seq");
  @sql_column_list = ();
  push(@sql_column_list, ["result_list_id", SQL_INTEGER]);
  push(@sql_column_list, ["index_number", SQL_INTEGER]);
  push(@sql_column_list, ["item_id", SQL_INTEGER]);
  my $sth = multi_insert_sth($dbh, "result_item_tbl", \@sql_column_list);
  for(my $i = 0; $i < scalar(@$id_list); $i++) {
    $sth->execute($result_list_id, $i, $id_list->[$i]);
  }
  $sth->finish();
  if (!$is_in_txn) {
    $dbh->commit();
  }
  return $result_list_id;
}

1;

use strict;
local($^W) = 1;

# path as string: "((x1,y1),(x2,y2),...)"
# returns list: ([x1,y1],[x2,y2],...)
# each coordinate is an array ref [x,y]
# returns empty list if there is an error parsing path.
sub path_to_coordinates($) {
  my $path = shift;
  my $coordinate_regex = q{\(-?\d+(?:\.\d+)?,-?\d+(?:\.\d+)?\)};
  my $coordinate_capture_regex = q{\((-?\d+(?:\.\d+)?),(-?\d+(?:\.\d+)?)\)};
  my @coordinate_list;
  if ($path =~ /^\(($coordinate_regex)((?:,$coordinate_regex)*)\)$/) {
    my $first_coordinate = $1;
    my $remaining_coordinates = $2;
    if ($first_coordinate =~ /^$coordinate_capture_regex$/) {
      push(@coordinate_list, [$1, $2]);
    } else {
      return;
    }
    if ($remaining_coordinates) {
      while ($remaining_coordinates =~ /(,$coordinate_regex)/g) {
	my $coordinate = $1;
	if ($coordinate =~ /^,$coordinate_capture_regex$/) {
	  push(@coordinate_list, [$1, $2]);
	} else {
	  return;
	}
      }
    }
  }
  return @coordinate_list;
}

1;

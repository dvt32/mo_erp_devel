use strict;
local($^W) = 1;

# tree is composed of a root node with child nodes. Each node has following structure:
#   value => row
#   child_list => ordered array of child nodes
# root node has only child_list attribute -- no value. All other nodes have value and child_list.
# pass in the row_by_id map and the sort key. Each row must have id and parent_id attributes, as well as the sort key.
# returns the root node of the tree.
sub sorted_tree_from_id_map {
  my ($row_by_id, $sort_key) = @_;
  my @sorted_id_list = sort {
    $row_by_id->{$a}->{$sort_key} cmp $row_by_id->{$b}->{$sort_key};
  } keys(%$row_by_id);
  my %node_by_id = map {
    $_ =>
      {
       "value" => $row_by_id->{$_},
       "child_list" => [],
      }
  } @sorted_id_list;
  my %sorted_tree = ("child_list" => []);
  for my $id (@sorted_id_list) {
    my $node = $node_by_id{$id};
    my $parent_id = $node->{"value"}->{"parent_id"};
    if (!$parent_id) {
      push(@{$sorted_tree{"child_list"}}, $node);
    } else {
      my $parent_node = $node_by_id{$parent_id};
      push(@{$parent_node->{"child_list"}}, $node);
    }
  }
  return \%sorted_tree;
}

1;

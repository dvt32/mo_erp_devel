use strict;
local($^W) = 1;

# transform a coordinate from coordinate system with (0,0) in lower left corner
# and (1,1) in upper right quadrant to coordinate system with (0,0) in upper
# left corner and (1,1) in lower right quadrant.
sub transform_coordinate($$$$) {
  my ($x, $y, $width, $height) = @_;
  return($x, $height - $y);
}


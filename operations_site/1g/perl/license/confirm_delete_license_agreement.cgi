#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use show_page;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get id from param list
  my $license_id = $cgi->param("id");
  my $licensee = $cgi->param("licensee");
  if ((!defined($license_id)) || ($license_id !~ /^\d+$/)) {
    $dbh->disconnect();
    show_page($instance_map, "download_license_not_found.html");
    return;
  }
  # fetch prospective customer.
  my $license_row = select_row($dbh, "license_agreement_tbl", $license_id);
  # finished with database access, so disconnect from database.
  $dbh->disconnect();
  # verify that prospective customer exists.
  if (!$license_row) {
    show_page($instance_map, "download_license_not_found.html");
    return;
  }
  # populate information needed for display of page.
  my $generated_xml = "";
  my $licensor_entity = HTML::Entities::encode($license_row->{"licensor_entity"});
  my $first_name = HTML::Entities::encode($license_row->{"first_name"});
  my $last_name = HTML::Entities::encode($license_row->{"last_name"});
  my $title = HTML::Entities::encode($license_row->{"title"});
  my $company = HTML::Entities::encode($license_row->{"company"});
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <id>$license_id</id>
  <license-details>
    <license-date>$license_row->{"license_date"}</license-date>
    <licensor-entity>$licensor_entity</licensor-entity>
    <prospective-customer-id>$license_row->{"prospective_customer_id"}</prospective-customer-id>
    <first-name>$first_name</first-name>
    <last-name>$last_name</last-name>
    <title>$title</title>
    <company>$company</company>
  </license-details>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "confirm_delete_license_agreement.xslt", $cgi);
}

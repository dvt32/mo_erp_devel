<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: License Management :: Delete License Agreement
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
				<xsl:apply-templates select="license-details"/>
				<p>
					<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
				</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="license-details">
		<p>
			<a href="/1g/index.cgi">Main menu</a> | <a>
            <xsl:attribute name="href">
              <xsl:text>/1g/license/manage_license_agreement.cgi?id=</xsl:text>
              <xsl:value-of select="/page/id"/>
            </xsl:attribute>
            <xsl:text>Manage This License Agreement</xsl:text>
          </a> | <a href="/1g/license/list_license_agreements.cgi">List All License Agreements</a>
		</p>
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="width: 400px;">
      <tr>
				<td class="standard_table_title">
					Confirm License Deletion	
				</td>
      </tr>
      <tr><td style="padding: 1em;">
				<p>Are you sure that you'd like to delete this license agreement for <xsl:value-of select="/page/license-details/first-name"/>&nbsp;<xsl:value-of select="/page/license-details/last-name"/> (ID:<xsl:value-of select="/page/id"/>)?</p>
        <p><a>
            <xsl:attribute name="href">
              <xsl:text>/1g/license/delete_license_agreement.cgi?id=</xsl:text>
              <xsl:value-of select="/page/id"/>
            </xsl:attribute>
            <xsl:text>Yes, Delete This License</xsl:text>
        </a></p>
				<p><a href="javascript:history.back()">No, Go Back</a></p>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

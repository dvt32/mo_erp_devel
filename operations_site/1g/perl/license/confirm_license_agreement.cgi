#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Lingua::EN::Nums2Words;
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use read_file;
use is_valid_email;
use days_in_month;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $licensor_entity = Encode::decode($cgi_input_charset, $cgi->param("licensor_entity"));
  my $licensor_signer = Encode::decode($cgi_input_charset, $cgi->param("licensor_signer"));
  my $prospective_customer_id = $cgi->param("prospective_customer_id");
  my ($mday, $mon, $year, $wday) = (localtime(time()))[3, 4, 5, 6];
  $year += 1900;
  my @mon_abbr = (qw( January February March April May June July August September October November December ));
  my @wday_abbr = (qw( Sunday Monday Tuesday Wednesday Thursday Friday Saturday ));
  my $numeric_month = $mon + 1;
  my $license_date = $year."-".$numeric_month."-".$mday; 
  my $formal_date = $wday_abbr[$wday].", ".$mon_abbr[$mon]." ".$mday.", ".$year; 
  my $first_name = Encode::decode($cgi_input_charset, $cgi->param("first_name"));
  my $last_name = Encode::decode($cgi_input_charset, $cgi->param("last_name"));
  my $company = Encode::decode($cgi_input_charset, $cgi->param("company"));
  my $signing_entity = Encode::decode($cgi_input_charset, $cgi->param("signing_entity"));
  # optional fields remain blank, with the ability to write in information later
  my $title = Encode::decode($cgi_input_charset, $cgi->param("title")) || "_______________________";
  my $company_signature;
  if ($signing_entity eq "person" ) {
    $title = "";
    $company_signature = "";
  } else {
    if (!$company) {
      die "show_page: create_license_missing_company.html\n";
    }
    $company_signature = "Company: ".$company;
    $title = "Title: ".$title;
  }
  my $business_type = Encode::decode($cgi_input_charset, $cgi->param("business_type")) || "_______________________";
  my $address1 = Encode::decode($cgi_input_charset, $cgi->param("address1"));
  my $address2 = Encode::decode($cgi_input_charset, $cgi->param("address2"));
  my $city = Encode::decode($cgi_input_charset, $cgi->param("city"));
  my $state = Encode::decode($cgi_input_charset, $cgi->param("state"));
  my $zip = Encode::decode($cgi_input_charset, $cgi->param("zip"));
  my $phone = Encode::decode($cgi_input_charset, $cgi->param("phone")) || "_______________________";
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email")) || "_______________________";
  my $desks = Encode::decode($cgi_input_charset, $cgi->param("desks"));
  my $offices = Encode::decode($cgi_input_charset, $cgi->param("offices"));
  my $phone_lines = Encode::decode($cgi_input_charset, $cgi->param("phone_lines"));
  my $persons = Encode::decode($cgi_input_charset, $cgi->param("persons"));
  my $months = Encode::decode($cgi_input_charset, $cgi->param("months"));
  my $start_month = Encode::decode($cgi_input_charset, $cgi->param("month"));
  my $start_day = Encode::decode($cgi_input_charset, $cgi->param("day"));
  my $start_year = Encode::decode($cgi_input_charset, $cgi->param("year"));
  my $license_fee = Encode::decode($cgi_input_charset, $cgi->param("license_fee"));
  my $security = Encode::decode($cgi_input_charset, $cgi->param("security"));
  my $spacenumber = Encode::decode($cgi_input_charset, $cgi->param("spacenumber"));
  my $phoneservice = Encode::decode($cgi_input_charset, $cgi->param("phoneservice"));
  my $confroomhrs = Encode::decode($cgi_input_charset, $cgi->param("confroomhrs"));
  my $sendemail = Encode::decode($cgi_input_charset, $cgi->param("sendemail"));
  # verify existence of a name, which is required.
  if (!$first_name || !$last_name) {
    die "show_page: create_license_missing_name.html\n";
  }
  # verify that given the existence of part of the address, all address fields are present
  if ($address1 || $address2 || $city || $state || $zip) {
    if(!$address1) {
      die "show_page: create_license_missing_address.html\n";
    }
    if(!$city) {
      die "show_page: create_license_missing_city.html\n";
    }
    if(!$state) {
      die "show_page: create_license_missing_state.html\n";
    }
    if(!$zip) {
      die "show_page: create_license_missing_zip.html\n";
    }
  }
  # verify existence of either phone or email address, one of which is required.
  if (($phone eq "____________________") && ($email eq "____________________")) {
    die "show_page: create_license_missing_phone_and_email.html\n";
  }
  # verify that email address is valid, if specified.
  if (($email) && !($email =~ m/^___/) && (!is_valid_email($email))) {
    die "show_page: create_license_bad_email_address.html\n";
  }
  # verify existence of desk space specification, which is required.
  if (!$desks && !$offices) {
    die "show_page: create_license_missing_space.html\n";
  }
  my $formal_desks = lc(num2word($desks))." (".$desks.")";
  my $formal_offices = lc(num2word($offices))." (".$offices.")";
  my $formal_phone_lines = lc(num2word($phone_lines))." (".$phone_lines.")";
  my $formal_persons = lc(num2word($persons))." (".$persons.")";
  # verify existence of months, which is required.
  if (!$months) {
    die "show_page: create_license_missing_months.html\n";
  }
  my $formal_months = lc(num2word($months))." (".$months.")";
  # verify that start month is valid.
  if (($start_month !~ /^(\d\d)$/) || (int($start_month) < 1) || (int($start_month) > 12)) {
    die "show_page: create_license_bad_start_month.html\n";
  }
  # verify that start year is valid.
  if (($start_year !~ /^(\d{4})$/) || ($start_year < $year) || ($start_year > ($year + 1))) {
    die "show_page: create_license_bad_start_year.html\n";
  }
  # verify that start day is valid.
  if (($start_day !~ /^(\d\d)$/) || (int($start_day) < 1) || (int($start_day) > days_in_month($start_month, $start_year))) {
    die "show_page: create_license_bad_start_day.html\n";
  }
  # verify existence of a fee specification, which is required.
  if (!$license_fee) {
    die "show_page: create_license_missing_fee.html\n";
  }
  # verify existence of a security specification, which is required.
  if (!$security) {
    die "show_page: create_license_missing_deposit.html\n";
  }
  my $start_date = $start_year."-".$start_month."-".$start_day;
  # for the following array calculations, months begin at 0, not 1.
  $start_month--;
  # write the start date out.
  my $formal_start_date = $mon_abbr[$start_month]." ".int($start_day).", ".$start_year;
  my $actual_months = $months;
  if( $start_day < 8 ) {
    # we'll count the current month in the number of "months" of the license if we're in the first week
    $actual_months--;
  } 
  # calculate end date from start date + number of months
  my $end_year = $start_year + (int(($start_month + $actual_months) / 12));
  my $end_month = (($start_month + $actual_months) % 12);
  my $end_day = days_in_month($end_month + 1, $end_year);
  # and write the end date out.
  my $formal_end_date = $mon_abbr[$end_month]." ".$end_day.", ".$end_year;
  # change month to express in a date, not an array
  $end_month++;
  my $end_date = $end_year."-".$end_month."-".$end_day;
  $licensor_entity = HTML::Entities::encode($licensor_entity);
  $licensor_signer = HTML::Entities::encode($licensor_signer);
  $company = HTML::Entities::encode($company);
  $first_name = HTML::Entities::encode($first_name);
  $last_name = HTML::Entities::encode($last_name);
  $title = HTML::Entities::encode($title);
  $signing_entity = HTML::Entities::encode($signing_entity);
  $address1 = HTML::Entities::encode($address1);
  $address2 = HTML::Entities::encode($address2);
  $city = HTML::Entities::encode($city);
  $state = HTML::Entities::encode($state);
  $zip = HTML::Entities::encode($zip);
  $phone = HTML::Entities::encode($phone);
  $email = HTML::Entities::encode($email);
  $business_type = HTML::Entities::encode($business_type);
  $company_signature = HTML::Entities::encode($company_signature);
  $spacenumber = HTML::Entities::encode($spacenumber);
  $phoneservice = HTML::Entities::encode($phoneservice);
  $confroomhrs = HTML::Entities::encode($confroomhrs);
  $sendemail = HTML::Entities::encode($sendemail);  
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <license-agreement-confirmation>
    <license-date>$license_date</license-date>
    <formal-date>$formal_date</formal-date>
    <licensor-entity>$licensor_entity</licensor-entity>
    <licensor-signer>$licensor_signer</licensor-signer>
    <prospective-customer-id>$prospective_customer_id</prospective-customer-id>
    <company>$company</company>
    <first-name>$first_name</first-name>
    <last-name>$last_name</last-name>
    <title>$title</title>
    <signing-entity>$signing_entity</signing-entity>
    <address1>$address1</address1>
    <address2>$address2</address2>
    <city>$city</city>
    <state>$state</state>
    <zip>$zip</zip>
    <phone>$phone</phone>
    <email>$email</email>
    <business-type>$business_type</business-type>
    <desks>$desks</desks>
    <formal-desks>$formal_desks</formal-desks>
    <offices>$offices</offices>
    <formal-offices>$formal_offices</formal-offices>
    <phone-lines>$phone_lines</phone-lines>
    <formal-phone-lines>$formal_phone_lines</formal-phone-lines>
    <persons>$persons</persons>
    <formal-persons>$formal_persons</formal-persons>
    <months>$months</months>
    <formal-months>$formal_months</formal-months>
    <start-date>$start_date</start-date>
    <formal-start-date>$formal_start_date</formal-start-date>
    <end-date>$end_date</end-date>
    <formal-end-date>$formal_end_date</formal-end-date>
    <license-fee>$license_fee</license-fee>
    <security>$security</security>
    <company-signature>$company_signature</company-signature>
    <spacenumber>$spacenumber</spacenumber>
    <phoneservice>$phoneservice</phoneservice>
    <confroomhrs>$confroomhrs</confroomhrs>
    <sendemail>$sendemail</sendemail>      
  </license-agreement-confirmation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "confirm_license_agreement.xslt", $cgi);
}

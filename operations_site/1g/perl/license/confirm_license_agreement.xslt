<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Confirm License Agreement Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<table>
	  <tr>
	    <td>
	      <xsl:apply-templates select="license-agreement-confirmation"/>
	    </td>
	  </tr>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="license-agreement-confirmation">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="width: 400px; white-space: nowrap">
      <tr>
        <td class="standard_table_title" colspan="2">
          Confirm License Agreement Details
        </td>
      </tr>
      <tr>
        <td style="padding: 1em;" colspan="2">
	  <p>Please confirm the details below for accuracy.</p> 
	  <p><a href="javascript:history.back()">Go back</a> to make any necessary changes.</p>
        </td>
      </tr>
      <tr>
	<td class="license_input">
	  Current Date:
	</td>
	<td>
	  <xsl:value-of select="formal-date"/>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  Licensor Entity:
	</td>
	<td>
	  <xsl:if test = "licensor-entity = 'mos1'" >
	    <b>Micro Office Solutions 1 LLC (36 W. 20th St., Fl. 3)</b>
          </xsl:if> 
	  <xsl:if test = "licensor-entity = 'mos2'" >
	    <b>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 3)</b>
          </xsl:if> 
	  <xsl:if test = "licensor-entity = 'mos2-6'" >
	    <b>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 6)</b>
          </xsl:if> 
	  <xsl:if test = "licensor-entity = 'mos2-10'" >
	    <b>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 10)</b>
          </xsl:if> 
	  <xsl:if test = "licensor-entity = 'mos2-11'" >
	    <b>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 11)</b>
          </xsl:if>           
	  <xsl:if test = "licensor-entity = 'mos4'" >
	    <b>Micro Office Solutions 4 LLC (122 W. 22nd St., Fl. 10 &amp; 12)</b>
          </xsl:if> 
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  Office/Space Number:
	</td>
	<td>
	  <b><xsl:value-of select="spacenumber"/></b>
	</td>
      </tr>      
      <tr>
	<td class="license_input">
	  Licensor Signer:
	</td>
	<td>
	  <xsl:if test = "licensor-signer = 'wchao'" >
	    <b>Wellie Chao</b>
          </xsl:if> 
	  <xsl:if test = "licensor-signer = 'drotbard'" >
	    <b>David Rotbard</b>
          </xsl:if> 
	  <xsl:if test = "licensor-signer = 'matt'" >
	    <b>Matthieu Rosanvallon</b>
          </xsl:if>
	  <xsl:if test = "licensor-signer = 'yon' " >
	    <b>Yon Lam</b>
	  </xsl:if>		           
	</td>
      </tr>
      <xsl:if test = "signing-entity = 'company' or string-length(company) != 0" >
      <tr>
	<td class="license_input">
	  Company:
	</td>
	<td>
	  <b><xsl:value-of select="company"/></b>
	</td>
      </tr>
      </xsl:if>
      <tr>
	<td class="license_input">
	  First Name:
	</td>
	<td>
	  <b><xsl:value-of select="first-name"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  Last Name:
	</td>
	<td>
	  <b><xsl:value-of select="last-name"/></b>
	</td>
      </tr>
      <xsl:if test = "not(contains(title,'_____')) and string-length(title) != 0">
	<tr>
	  <td class="license_input">
	    Title:
	  </td>
	  <td>
	    <b><xsl:value-of select="title"/></b>
	  </td>
	</tr>
      </xsl:if>
      <tr>
	<td class="license_input">
	  Signing Entity:
	</td>
	<td>
	  <xsl:if test = "signing-entity = 'person'" >
	    A person
          </xsl:if> 
	  <xsl:if test = "signing-entity = 'company'" >
	    A company
          </xsl:if> 
	</td>
      </tr>
      <xsl:if test = "string-length(address1) != 0">
	<tr>
	  <td class="license_input">
	    Address 1:
	  </td>
	  <td>
	    <b><xsl:value-of select="address1"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "string-length(address2) != 0">
	<tr>
	  <td class="license_input">
	    Address 2:
	  </td>
	  <td>
	    <b><xsl:value-of select="address2"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "string-length(city) != 0">
	<tr>
	  <td class="license_input">
	    City:
	  </td>
	  <td>
	    <b><xsl:value-of select="city"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "string-length(state) != 0">
	<tr>
	  <td class="license_input">
	    State:
	  </td>
	  <td>
	    <b><xsl:value-of select="state"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "string-length(zip) != 0">
	<tr>
	  <td class="license_input">
	    Zip Code:
	  </td>
	  <td>
	    <b><xsl:value-of select="zip"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "not(contains(phone,'_____'))">
	<tr>
	  <td class="license_input">
	    Phone Number:
	  </td>
	  <td>
	    <b><xsl:value-of select="phone"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "not(contains(email,'_____'))">
	<tr>
	  <td class="license_input">
	    Email Address:
	  </td>
	  <td>
	    <b><xsl:value-of select="email"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "not(contains(business-type,'_____'))">
	<tr>
	  <td class="license_input">
	    Type of Business:
	  </td>
	  <td>
	    <b><xsl:value-of select="business-type"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "desks != 0">
	<tr>
	  <td class="license_input">
	    No. of Desks:
	  </td>
	  <td>
	    <b><xsl:value-of select="formal-desks"/></b>
	  </td>
	</tr>
      </xsl:if>
      <xsl:if test = "offices != 0">
	<tr>
	  <td class="license_input">
	    No. of Offices:
	  </td>
	  <td>
	    <b><xsl:value-of select="formal-offices"/></b>
	  </td>
	</tr>
      </xsl:if>
      <tr>
	<td class="license_input">
	  No. of Phone Lines:
	</td>
	<td>
	  <b><xsl:value-of select="formal-phone-lines"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  No. of Persons with Internet Access:
	</td>
	<td>
	  <b><xsl:value-of select="formal-persons"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  No. of Months in Initial Term:
	</td>
	<td>
	  <b><xsl:value-of select="formal-months"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  Conference Rm Hours:
	</td>
	<td>
	  <b><xsl:value-of select="confroomhrs"/></b>
	</td>
      </tr>      
      <tr>
	<td class="license_input">
	  Start Date:
	</td>
	<td>
	  <b><xsl:value-of select="formal-start-date"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  End Date:
	</td>
	<td>
	  <b><xsl:value-of select="formal-end-date"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  Monthly License Fee:
	</td>
	<td>
	  $<b><xsl:value-of select="license-fee"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  Security Deposit:
	</td>
	<td>
	  $<b><xsl:value-of select="security"/></b>
	</td>
      </tr>
      <tr>
	<td class="license_input">
	  Phone Service:
	</td>
	<td>
	  <b><xsl:value-of select="phoneservice"/></b>
	</td>
      </tr>      
      <tr>
	<td style="padding: 1em;">
	  <form action="store_license_agreement.cgi" method="post">
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">licensor_entity</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="licensor-entity"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">licensor_signer</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="licensor-signer"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">spacenumber</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="spacenumber"/></xsl:attribute>
	    </input>	    
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">confroomhrs</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="confroomhrs"/></xsl:attribute>
	    </input>	
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">sendemail</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="sendemail"/></xsl:attribute>
	    </input>		    
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">phoneservice</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="phoneservice"/></xsl:attribute>
	    </input>		    
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">prospective_customer_id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="prospective-customer-id"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_date</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-date"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">license_date</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="license-date"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">company</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="company"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">first_name</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="first-name"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">last_name</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="last-name"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">signing_entity</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="signing-entity"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">title</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="title"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">address1</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="address1"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">address2</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="address2"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">city</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="city"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">state</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="state"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">zip</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="zip"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">phone</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="phone"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">email</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="email"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">business_type</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="business-type"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">desks</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="desks"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_desks</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-desks"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">offices</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="offices"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_offices</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-offices"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">phone_lines</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="phone-lines"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_phone_lines</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-phone-lines"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">persons</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="persons"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_persons</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-persons"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">months</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="months"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_months</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-months"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">start_date</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="start-date"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_start_date</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-start-date"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">end_date</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="end-date"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">formal_end_date</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="formal-end-date"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">license_fee</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="license-fee"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">security</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="security"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">company_signature</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="company-signature"/></xsl:attribute>
	    </input>
	    <p>I assert the accuracy of the above.</p>
	    <input type="submit" name="submitButton" value="Create According License Agreement"/>
	  </form>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

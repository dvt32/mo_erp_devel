#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_list;
use apply_xslt_output_xhtml;
use lead_pad;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch prospective customers.
  my @prospective_customer_list;
  my $sql_query = "select id, last_name, first_name, work_phone_number, email_address, company, business_description, amount_of_space, rental_start_date, duration_of_rental, desired_location, address_1, address_2, city, state, zip_code from prospective_customer_tbl where (sales_status = ? and (license_created is null or license_created = ?)) order by initial_contact_dt desc limit 150";
  my @sql_param_list = ("live", "f");
  get_row_list($dbh, $sql_query, \@sql_param_list, \@prospective_customer_list);
  # sort recent prospective customers by name.
  my @prospective_customer_by_name_list = sort {
    my $sort_order = 0;
    my $last_name_a = lc($a->{"last_name"});
    my $last_name_b = lc($b->{"last_name"});
    $sort_order = $last_name_a cmp $last_name_b;
    if ($sort_order == 0) {
      my $first_name_a = lc($a->{"first_name"});
      my $first_name_b = lc($b->{"first_name"});
      $sort_order = $first_name_a cmp $first_name_b;
    }
    return $sort_order;
  } @prospective_customer_list;
  my @month_name_list = (qw(January February March April May June July August September October November December));
  my ($year, $month, $day) = get_date_now();
  my $next_year = $year + 1;
  $month = lead_pad($month, 2);
  $day = lead_pad($day, 2);
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <licensor-entity-list>
    <licensor-entity>
      <name>Micro Office Solutions 1 LLC (36 W. 20th St., Fl. 3)</name>
      <value>mos1</value>
    </licensor-entity>
    <licensor-entity>
      <name>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 3)</name>
      <value>mos2-3</value>
    </licensor-entity>
    <licensor-entity>
      <name>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 6)</name>
      <value>mos2-6</value>
    </licensor-entity>
    <licensor-entity>
      <name>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 10)</name>
      <value>mos2-10</value>
    </licensor-entity>
    <licensor-entity>
      <name>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 11)</name>
      <value>mos2-11</value>
    </licensor-entity>    
    <licensor-entity>
      <name>Micro Office Solutions 4 LLC (122 W. 27th St., Fl. 10 and 12)</name>
      <value>mos4</value>
    </licensor-entity>
  </licensor-entity-list>
  <prospect-list>
    <prospect-entry>
      <prospective-customer-id>0</prospective-customer-id>
      <name>Not listed</name>
    </prospect-entry>
---END_BLOCK---;
  foreach my $prospective_customer_id (@prospective_customer_by_name_list) {
    # delete non-printable characters
    $prospective_customer_id->{"last_name"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"first_name"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"work_phone_number"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"email_address"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"company"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"business_description"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"desired_location"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"address_1"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"address_2"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"city"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"state"} =~ s/[\n\"]//gis;
    $prospective_customer_id->{"zip_code"} =~ s/[\n\"]//gis;
    # remaining safe encoding
    my $first_name = HTML::Entities::encode(ucfirst($prospective_customer_id->{"first_name"} || ""));
    my $last_name = HTML::Entities::encode(ucfirst($prospective_customer_id->{"last_name"} || ""));
    my $phone = HTML::Entities::encode($prospective_customer_id->{"work_phone_number"} || "");
    my $email = HTML::Entities::encode($prospective_customer_id->{"email_address"} || "");
    my $company = HTML::Entities::encode($prospective_customer_id->{"company"} || "");
    my $business_type = HTML::Entities::encode($prospective_customer_id->{"business_description"} || "");
    my $address1 = HTML::Entities::encode($prospective_customer_id->{"address_1"} || "");
    my $address2 = HTML::Entities::encode($prospective_customer_id->{"address_2"} || "");
    my $city = HTML::Entities::encode($prospective_customer_id->{"city"} || "");
    my $state = HTML::Entities::encode($prospective_customer_id->{"state"} || "");
    my $zip = HTML::Entities::encode($prospective_customer_id->{"zip_code"} || "");
    my $desired_location = HTML::Entities::encode($prospective_customer_id->{"desired_location"} || "");
    my $amount_of_space = HTML::Entities::encode($prospective_customer_id->{"amount_of_space"} || "");
    my $rental_start_date = HTML::Entities::encode($prospective_customer_id->{"rental_start_date"} || "");
    my $duration_of_rental = HTML::Entities::encode($prospective_customer_id->{"duration_of_rental"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <prospect-entry>
      <prospective-customer-id>$prospective_customer_id->{"id"}</prospective-customer-id>
      <name>$last_name, $first_name</name>
      <first-name>$first_name</first-name>
      <last-name>$last_name</last-name>
      <phone>$phone</phone>
      <email>$email</email>
      <company>$company</company>
      <business-type>$business_type</business-type>
      <address1>$address1</address1>
      <address2>$address2</address2>
      <city>$city</city>
      <state>$state</state>
      <zip>$zip</zip>
      <desired-location>$desired_location</desired-location>
      <amount-of-space>$amount_of_space</amount-of-space>
      <rental-start-date>$rental_start_date</rental-start-date>
      <duration-of-rental>$duration_of_rental</duration-of-rental>
    </prospect-entry>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </prospect-list>
  <current-date>
    <month>$month</month>
    <day>$day</day>
    <year>$year</year>
  </current-date>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $number = lead_pad($i, 2);
    my $name = $month_name_list[$i - 1];
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$number</number>
      <name>$name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    my $day = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$day</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <year-list>
    <year>$year</year>
    <year>$next_year</year>
  </year-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "create_license_agreement.xslt", $cgi);
}

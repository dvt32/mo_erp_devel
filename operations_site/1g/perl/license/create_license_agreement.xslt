<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Create New Customer License Agreement
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<xsl:call-template name="population-js"/>
	<xsl:call-template name="validation-js"/>
      </head>
      <body>
	<table>
	  <tr>
	    <td>
	      <xsl:call-template name="license-agreement-form"/>
	    </td>
	  </tr>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="population-js">
    <script type="text/javascript">
    function populate_fields() {	  
      form = document.forms['license_form']
      id = form.prospective_customer_id.value
      <xsl:for-each select="/page/prospect-list/prospect-entry">
	<xsl:if test="boolean(prospective-customer-id)">
	  if (id == <xsl:value-of select="prospective-customer-id"/>) {
	    form.company.value = "<xsl:value-of select="company"/>";
	    form.first_name.value = "<xsl:value-of select="first-name"/>";
	    form.last_name.value = "<xsl:value-of select="last-name"/>";
	    form.title.value = "<xsl:value-of select="title"/>";
	    form.address1.value = "<xsl:value-of select="address1"/>";
	    form.address2.value = "<xsl:value-of select="address2"/>";
	    form.city.value = "<xsl:value-of select="city"/>";
	    form.state.value = "<xsl:value-of select="state"/>";
	    form.zip.value = "<xsl:value-of select="zip"/>";
	    form.phone.value = "<xsl:value-of select="phone"/>";
	    form.email.value = "<xsl:value-of select="email"/>";
	    form.business_type.value = "<xsl:value-of select="business-type"/>";
	    <xsl:choose>
	      <xsl:when test="string-length(desired-location) = 0">
	    document.getElementById("desired_location").innerHTML = "";
	      </xsl:when>
	      <xsl:otherwise>
	    document.getElementById("desired_location").innerHTML = "previously stated:  <xsl:value-of select="desired-location"/>";
	      </xsl:otherwise>
	    </xsl:choose>
	    <xsl:choose>
	      <xsl:when test="string-length(amount-of-space) = 0">
	    document.getElementById("amount_of_space").innerHTML = "";
	      </xsl:when>
	      <xsl:otherwise>
	    document.getElementById("amount_of_space").innerHTML = "previously stated:  <xsl:value-of select="amount-of-space"/>";
	      </xsl:otherwise>
	    </xsl:choose>
	    <xsl:choose>
	      <xsl:when test="string-length(rental-start-date) = 0">
	    document.getElementById("rental_start_date").innerHTML = "";
	      </xsl:when>
	      <xsl:otherwise>
	    document.getElementById("rental_start_date").innerHTML = "previously stated:  <xsl:value-of select="rental-start-date"/>";
	      </xsl:otherwise>
	    </xsl:choose>
	    <xsl:choose>
	      <xsl:when test="string-length(duration-of-rental) = 0">
	    document.getElementById("duration_of_rental").innerHTML = "";
	      </xsl:when>
	      <xsl:otherwise>
	    document.getElementById("duration_of_rental").innerHTML = "previously stated:  <xsl:value-of select="duration-of-rental"/>";
	      </xsl:otherwise>
	    </xsl:choose>
	  }
	</xsl:if>
      </xsl:for-each>
    }
    </script>
  </xsl:template>

  <xsl:template name="validation-js">
    <script type="text/javascript">
    function validate_input() {
      form = document.forms['license_form']

      if (!form.desks.value) {
				alert("Please enter the number of desks you will need.  It is required for the license agreement.")
        form.desks.focus()
      } else if (!form.offices.value) {
				alert("Please enter the number of offices you will need.  It is required for the license agreement.")
        form.offices.focus()
      } else if (!form.phone_lines.value) {
				alert("Please enter the number of phone lines you will need.  It is required for the license agreement.")
        form.phone_lines.focus()
      } else if (!form.persons.value) {
				alert("Please enter the amount of people who you would like to have internet acccess.  It is required for the license agreement.")
        form.persons.focus()
      } else if (!form.months.value) {
				alert("Please enter the number of months in your first term.  It is required for the license agreement.")
        form.months.focus()
      } else if (!form.license_fee.value) {
				alert("Please enter the license fee you've agreed upon.  It is required for the license agreement.")
        form.license_fee.focus()
      } else if (!form.security.value) {
				alert("Please enter the security deposit you've agreed upon.  It is required for the license agreement.")
        form.security.focus()
      } else if (!form.confhours.value) {
				alert("Please enter the number of conference room hours you've agreed upon.  It is required for the license agreement.")
        form.confhours.focus() 
      } else {
			var str_desks = form.desks.value; 
			var int_desks = parseInt(str_desks).toString(); 
			var str_offices = form.offices.value; 
			var int_offices = parseInt(str_offices).toString(); 
			var str_phone_lines = form.phone_lines.value; 
			var int_phone_lines = parseInt(str_phone_lines).toString(); 
			var str_persons = form.persons.value; 
			var int_persons = parseInt(str_persons).toString(); 
			var str_months = form.months.value; 
			var int_months = parseInt(str_months).toString(); 
			var str_license_fee = form.license_fee.value; 
			var int_license_fee = parseInt(str_license_fee).toString(); 
			var str_security = form.security.value; 
			var int_security = parseInt(str_security).toString();
			var str_confhours = form.confhours.value;
			var int_confhours = parseInt(str_confhours).toString();
			var str_spacenumber = form.spacenumber.value;
			var int_spacenumber = parseInt(str_spacenumber).toString();			
			
			if (str_desks.length != int_desks.length || int_desks == "NaN") 
				alert("Please enter a numerical value for the amount of desks.");
			else if (str_offices.length != int_offices.length || int_offices == "NaN") 
				alert("Please enter a numerical value for the amount of offices.");
			else if (str_phone_lines.length != int_phone_lines.length || int_phone_lines == "NaN") 
				alert("Please enter a numerical value for the amount of phone lines.");
			else if (str_persons.length != int_persons.length || int_persons == "NaN") 
				alert("Please enter a numerical value for the amount of persons who will need internet access.");
			else if (str_months.length != int_months.length || int_months == "NaN") 
				alert("Please enter a numerical value for the amount of months.");
			else if (str_license_fee.length != int_license_fee.length || int_license_fee == "NaN") 
				alert("Please enter only a numerical value for the license fee.");
			else if (str_security.length != int_security.length || int_security == "NaN") 
				alert("Please enter only a numerical value for security deposit.");
			else if (str_confhours.length != int_confhours.length || int_confhours == "NaN") 
				alert("Please enter only a numerical value for conference room hours.");
			else if (str_spacenumber.length != int_spacenumber.length || int_spacenumber == "NaN") 
				alert("Please enter only a numerical value for desk/space number.");				
			else
				return true;
      }
      return false;
    }
    </script>
  </xsl:template>

  <xsl:template name="license-agreement-form">
    <form name="license_form" onsubmit="return validate_input()" action="confirm_license_agreement.cgi" method="post">
      <fieldset class="standard_fieldset">
	<legend>
	  Create New License Agreement 
	</legend>
	<table cellspacing="0" cellpadding="0" border="0">
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Licensor Entity:</span>
	    </td>
	    <td>
	      <select name="licensor_entity">
		<xsl:for-each select="/page/licensor-entity-list/licensor-entity">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
		    <xsl:value-of select="name"/>
		  </option>
		</xsl:for-each>
	      </select>
	      <span id="desired_location" style="font-size: 0.75em; color: gray; text-align: right; padding: 1em"/>
	    </td>
	  </tr>
		  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Office/Space Number:
	    </td>
	    <td>
	      #
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">spacenumber</xsl:attribute>
	      </input>
	    </td>
	  </tr>  
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Licensor Signer:</span>
	    </td>
	    <td>
	      <select name="licensor_signer">
		<option value="wchao">Wellie Chao</option>
		<option value="drotbard">David Rotbard</option>
		<option value="matt">Matthieu Rosanvallon</option>
		<option value="yon">Yon Lam</option>
	      </select>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Please select your name:</span>
	    </td>
	    <td>
	      <select name="prospective_customer_id" onchange="populate_fields()">
		<xsl:for-each select="/page/prospect-list/prospect-entry">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="prospective-customer-id"/></xsl:attribute>
		    <xsl:value-of select="name"/>
		  </option>
		</xsl:for-each>
	      </select>
	      <span style="font-size: .75em">&nbsp; &nbsp; &nbsp;<a href="create_license_agreement_complete_customer_list.cgi">(show older customers)</a></span>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Licensee Company:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">company</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Licensee First Name:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">first_name</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Licensee Last Name:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">last_name</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Signing Entity:</span>
	    </td>
	    <td>
	      <select name="signing_entity">
		<option>
		  <xsl:attribute name="value">person</xsl:attribute>A person
		</option>
		<option>
		  <xsl:attribute name="value">company</xsl:attribute>A company
		</option>
	      </select>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Title:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">title</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Address 1:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">address1</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Address 2:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">address2</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      City:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">city</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      State:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">state</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Zip Code:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">zip</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      *Phone Number:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">phone</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      *Email Address:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">email</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Type of Business:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">business_type</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">No. of Desks Needed:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">desks</xsl:attribute>
	      </input>
	      <span id="amount_of_space" style="font-size: 0.75em; color: gray; text-align: right; padding: 1em"/>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">No. of Offices Needed:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">offices</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">No. of Phone Lines:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">phone_lines</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">No. of Persons With Internet Access:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">persons</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">No. of Months:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">months</xsl:attribute>
	      </input>
	      <span id="duration_of_rental" style="font-size: 0.75em; color: gray; text-align: right; padding: 1em"/>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Conference Rm Hrs:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">confroomhrs</xsl:attribute>
	      </input>
	      <span id="con_hours" style="font-size: 0.75em; color: gray; text-align: right; padding: 1em"/>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Start Date:</span>
	    </td>
	    <td>
	      <select name="month">
		<xsl:for-each select="month-list/month">
		  <option>
		    <xsl:if test="number = ../../current-date/month">
		      <xsl:attribute name="selected">selected</xsl:attribute>
		    </xsl:if>
		    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		    <xsl:value-of select="name"/>
		  </option>
		</xsl:for-each>
	      </select>
	      <select name="day">
		<xsl:for-each select="day-list/day">
		  <option>
		    <xsl:if test=". = ../../current-date/day">
		      <xsl:attribute name="selected">selected</xsl:attribute>
		    </xsl:if>
		    <xsl:value-of select="."/>
		  </option>
		</xsl:for-each>
	      </select>
	      <select name="year">
		<xsl:for-each select="year-list/year">
		  <option>
		    <xsl:if test=". = ../../current-date/year">
		      <xsl:attribute name="selected">selected</xsl:attribute>
		    </xsl:if>
		    <xsl:value-of select="."/>
		  </option>
		</xsl:for-each>
	      </select>
	      <span id="rental_start_date" style="font-size: 0.75em; color: gray; text-align: right; padding: 1em"/>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Monthly License Fee:</span>
	    </td>
	    <td>
	      $
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">license_fee</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Security Deposit:</span>
	    </td>
	    <td>
	      $
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">security</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      <span class="required_field">Phone Service:</span>
	    </td>
	    <td>
	      <select name="phoneservice">
       <option>
         <xsl:attribute name="value">None</xsl:attribute>None
       </option>	      
       <option>
         <xsl:attribute name="value">Analog</xsl:attribute>Analog
       </option>
       <option>
         <xsl:attribute name="value">VOIP</xsl:attribute>VOIP
       </option>
	      </select>
	    </td>
	  </tr>	  
	  <tr style="white-space: nowrap">
	    <td style="padding: 4px; ">
	      Email service@microoffice.com:
	    </td>
	    <td>
     <input>
       <xsl:attribute name="type">checkbox</xsl:attribute>
       <xsl:attribute name="name">sendemail</xsl:attribute>
     </input>
	    </td>
	  </tr>		  
	  <tr>
	    <td style="padding: 10px; ">
	    </td>
	    <td style="padding: 10px 0 10px 0;">
	      <input type="submit" name="submitButton" value="Submit Fields"/>
	      <input type="reset" name="clearButton" value="Clear Fields"/>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	    </td>
	    <td>
              <span class="required_field">Required Fields are Red</span>
	      <p>*Please enter at least one out of the phone number and email fields.</p>
	      <p>Remaining optional fields may be completed on the license at a later date.</p>
	    </td>
	  </tr>
	</table>
      </fieldset>
    </form>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

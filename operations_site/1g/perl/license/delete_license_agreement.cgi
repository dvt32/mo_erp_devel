#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use show_page;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get id from param list
  my $license_id = $cgi->param("id");
  my $licensee = $cgi->param("licensee");
  if ((!defined($license_id)) || ($license_id !~ /^\d+$/)) {
    $dbh->disconnect();
    show_page($instance_map, "download_license_not_found.html");
    return;
  }
  # fetch prospective customer.
  my $return_value = delete_row($dbh, "license_agreement_tbl", $license_id);
  # finished with database access, so disconnect from database.
  $dbh->disconnect();
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <id>$license_id</id>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "license_agreement_deletion_successful.xslt", $cgi);
}

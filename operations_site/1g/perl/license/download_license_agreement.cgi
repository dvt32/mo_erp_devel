#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get id from param list
  my $license_id = $cgi->param("id");
  if ((!defined($license_id)) || ($license_id !~ /^\d+$/)) {
    die "show_page: download_license_not_found.html\n";
  }
  # fetch prospective customer.
  my $license_row = select_row($dbh, "license_agreement_tbl", $license_id);
  # verify that prospective customer exists.
  if (!$license_row) {
    die "show_page: download_license_not_found.html\n";
  }
  my $filename = $license_row->{"filename"};
  # print output
  print $cgi->header
    ("-type" => "application/rtf",
     "-content-disposition" => qq{attachment; filename="$filename"});
  print $license_row->{"license_agreement_rtf"};
}

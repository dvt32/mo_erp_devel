<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Confirm License Agreement Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="width: 400px; white-space: nowrap">
	  <tr>
	    <td class="standard_table_title" colspan="2">
	      Download License Agreement
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 1em;" colspan="2">
	      <a>
		<xsl:attribute name="href">
		  <xsl:text>download_license_agreement.cgi?id=</xsl:text>
		  <xsl:value-of select="id"/>
		</xsl:attribute>
		<xsl:text>Click to Download License Agreement</xsl:text>
	      </a>
	    </td>
	  </tr>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

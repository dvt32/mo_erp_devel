#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use show_page;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get id from param list
  my $license_id = $cgi->param("id");
  my $licensee = $cgi->param("licensee");
  if ((!defined($license_id)) || ($license_id !~ /^\d+$/)) {
    $dbh->disconnect();
    show_page($instance_map, "download_license_not_found.html");
    return;
  }
  # fetch prospective customer.
  my $license_row = select_row($dbh, "license_agreement_tbl", $license_id);
  # finished with database access, so disconnect from database.
  $dbh->disconnect();
  # verify that prospective customer exists.
  if (!$license_row) {
    show_page($instance_map, "download_license_not_found.html");
    return;
  } elsif(!($license_row->{"license_agreement_pdf"})) {
    show_page($instance_map, "download_license_not_found.html");
    return;
  }
  my $filename = $license_row->{"filename"};
  $filename =~ s/rtf$/pdf/; 
  # print output
  print $cgi->header
    ("-type" => "application/pdf",
     "-content-disposition" => qq{attachment; filename="$filename"});
  print $license_row->{"license_agreement_pdf"};
}

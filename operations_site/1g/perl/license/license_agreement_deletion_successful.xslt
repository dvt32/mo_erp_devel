<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: License Management :: Delete License Agreement
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
				<p>
					<a href="/1g/index.cgi">Main menu</a> | <a href="/1g/license/list_license_agreements.cgi">List All License Agreements</a>
				</p>
				<table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="width: 400px;">
					<tr>
						<td class="standard_table_title">
							License Deletion Successful
						</td>
					</tr>
					<tr><td style="padding: 1em;">
						<p>You've successfully deleted the license agreement for ID:<xsl:value-of select="/page/id"/>.</p>
					</td></tr>
				</table>
				<p>
					<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
				</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

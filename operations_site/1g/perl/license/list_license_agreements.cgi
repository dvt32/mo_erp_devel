#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main() {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $sth;
  # fetch prospective customers.
  my @license_list;
  $sth = $dbh->prepare("select id, license_date, last_name, first_name, company, business_type, start_date, end_date from license_agreement_tbl order by license_date desc limit 150");
  $sth->execute();
  while(my $row = $sth->fetchrow_hashref()) {
    push(@license_list, $row);
  }
  # finished with database access, so disconnect from database.
  $sth->finish();
  $dbh->disconnect();
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <license-list>
---END_BLOCK---;
  foreach my $license (@license_list) {
    # delete non-printable characters
    $license->{"last_name"} =~ s/[\n\"]//gis;
    $license->{"first_name"} =~ s/[\n\"]//gis;
    $license->{"company"} =~ s/[\n\"]//gis;
    $license->{"business_type"} =~ s/[\n\"]//gis;
    # remaining safe encoding
    my $id = $license->{"id"};
    my $license_date = HTML::Entities::encode($license->{"license_date"});
    my $prospective_customer_id = $license->{"prospective_customer_id"};
    my $first_name = HTML::Entities::encode(ucfirst(lc($license->{"first_name"})));
    my $last_name = HTML::Entities::encode(ucfirst(lc($license->{"last_name"})));
    my $company = HTML::Entities::encode($license->{"company"} || "");
    my $business_type = HTML::Entities::encode($license->{"business_type"} || "");
    my $start_date = $license->{"start_date"};
    my $end_date = $license->{"end_date"};
    $generated_xml .= <<"---END_BLOCK---;";
    <license>
      <id>$id</id>
      <license-date>$license_date</license-date>
      <name>$last_name, $first_name</name>
      <company>$company</company>
      <business-type>$business_type</business-type>
      <start-date>$start_date</start-date>
      <end-date>$end_date</end-date>
    </license>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </license-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "list_license_agreements.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Manage License Agreements 
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="license-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="license-list">
    <p>
      <a href="/1g/index.cgi">Main menu</a>
			|
			<a href="/1g/license/create_license_agreement.cgi">Create license agreement</a>
      |
      <a href="/1g/sales/show_prospect_list.cgi">Prospective customer list</a>
      |
      <a href="/1g/sales/show_recent_interactions.cgi">Recent interactions</a>
      |
      <a href="/1g/sales/new_prospect_input.cgi">New prospect</a>
      |
      <a href="/1g/sales/batch_add_interactions_input.cgi">Batch add interactions</a>
    </p>
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
				<td colspan="13" class="standard_table_title">
					Created and Stored License Agreements
					<xsl:choose>
						<xsl:when test="/page/type = 'prospect-list'">
							Last <xsl:value-of select="count(/page/prospective-customer-list/prospective-customer)"/> Prospective Customers
						</xsl:when>
						<xsl:when test="/page/type = 'filter-by-stage'">
							Stage <xsl:value-of select="/page/stage_number"/> Prospective Customers
						</xsl:when>
						<xsl:when test="/page/type = 'search-result-list'">
							Search Results: <xsl:value-of select="count(/page/prospective-customer-list/prospective-customer)"/> Prospective Customers
						</xsl:when>
					</xsl:choose>
				</td>
      </tr>
      <tr class="standard_table_column_heading_row">
				<td class="standard_table_side_padding"></td>
				<td style="white-space: nowrap;">Name</td>
				<td class="standard_table_spacer_column"></td>
				<td style="white-space: nowrap;">License Date / Start Date / End Date / Company / Company Description</td>
				<td class="standard_table_spacer_column"></td>
				<td style="white-space: nowrap;">Download</td>
				<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="license"/>
    </table>
  </xsl:template>

  <xsl:template match="license">
    <tr valign="top">
      <xsl:choose>
				<xsl:when test="position() mod 2 = 0">
					<xsl:attribute name="class">standard_table_row_even</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
				</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td style="white-space: nowrap;">
				<xsl:value-of select="position()"/>.
				<a>
					<xsl:attribute name="href">/1g/license/manage_license_agreement.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
					<xsl:value-of select="name"/>
				</a>
			</td>
			<td class="standard_table_spacer_column"></td>
			<td>
				<xsl:value-of select="license-date"/> //
				<xsl:value-of select="start-date"/> to <xsl:value-of select="end-date"/> //
				<xsl:if test="title != '' or company != ''">
					<xsl:if test="title != ''">
						<xsl:value-of select="title"/>,
					</xsl:if>
					<xsl:value-of select="company"/>
				</xsl:if>
				<xsl:if test="business-type != ''">
					<xsl:if test="title != '' or company != ''">
						<xsl:text> // </xsl:text>
					</xsl:if>
					<xsl:text>description: </xsl:text>
					<xsl:value-of select="business-type"/>
				</xsl:if>
			</td>
			<td class="standard_table_spacer_column"></td>
			<td style="white-space: nowrap; ">
				<a> 
					<xsl:attribute name="href">/1g/license/download_pdf.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
					<xsl:text>Signed PDF</xsl:text>
				</a>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>


  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use show_page;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $id = $cgi->param("id");
  if ((!defined($id)) || ($id !~ /^\d+$/)) {
    $dbh->disconnect();
    show_page($instance_map, "license_not_found.html");
    return;
  }
  my $sth;
  # fetch license and customer information.
  my $license;
  $sth = $dbh->prepare("select * from license_agreement_tbl where id = ?");
  $sth->bind_param(1, $id);
  $sth->execute();
  $license = $sth->fetchrow_hashref();
  my $prospective_customer_id = $license->{"prospective_customer_id"};
  $sth = $dbh->prepare("select * from prospective_customer_tbl where id = ?");
  $sth->bind_param(1, $prospective_customer_id);
  $sth->execute();
  my $prospective_customer = $sth->fetchrow_hashref();
  # finished with database access, so disconnect from database.
  $sth->finish();
  $dbh->disconnect();
  # delete non-printable characters
  $license->{"last_name"} =~ s/[\n\"]//gis;
  $license->{"first_name"} =~ s/[\n\"]//gis;
  $license->{"company"} =~ s/[\n\"]//gis;
  $license->{"business_type"} =~ s/[\n\"]//gis;
  # remaining safe encoding
  my $first_name = HTML::Entities::encode(ucfirst(lc($license->{"first_name"})));
  my $last_name = HTML::Entities::encode(ucfirst(lc($license->{"last_name"})));
  my $title = HTML::Entities::encode($license->{"title"} || "");
  my $company = HTML::Entities::encode($license->{"company"} || "");
  my $business_type = HTML::Entities::encode($license->{"business_type"} || "");
  my $address1 = HTML::Entities::encode($license->{"address1"} || "");
  my $address2 = HTML::Entities::encode($license->{"address2"} || "");
  my $city = HTML::Entities::encode($license->{"city"} || "");
  my $state = HTML::Entities::encode($license->{"state"} || "");
  my $zip = HTML::Entities::encode($license->{"zip"} || "");
  my $phone = HTML::Entities::encode($license->{"phone"} || "");
  my $email = HTML::Entities::encode($license->{"email"} || "");
  my $notes = HTML::Entities::encode($prospective_customer->{"notes"} || "");
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <id>$license->{"id"}</id>
  <license-details>
    <license-date>$license->{"license_date"}</license-date>
    <licensor-entity>$license->{"licensor_entity"}</licensor-entity>
    <prospective-customer-id>$prospective_customer_id</prospective-customer-id>
    <first-name>$first_name</first-name>
    <last-name>$last_name</last-name>
    <title>$title</title>
    <company>$company</company>
    <notes>$notes</notes>
    <business-type>$business_type</business-type>
    <address1>$address1</address1>
    <address2>$address2</address2>
    <city>$city</city>
    <state>$state</state>
    <zip>$zip</zip>
    <phone>$phone</phone>
    <email>$email</email>
    <business-type>$business_type</business-type>
    <desks>$license->{"desks"}</desks>
    <offices>$license->{"offices"}</offices>
    <phone-lines>$license->{"phone_lines"}</phone-lines>
    <persons>$license->{"persons"}</persons>
    <months>$license->{"months"}</months>
    <start-date>$license->{"start_date"}</start-date>
    <end-date>$license->{"end_date"}</end-date>
    <license-fee>$license->{"license_fee"}</license-fee>
    <security>$license->{"security"}</security>
	<phoneservice>$license->{"phoneservice"}</phoneservice>
  </license-details>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "manage_license_agreement.xslt", $cgi);
}

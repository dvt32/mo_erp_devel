<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Manage License Agreement Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<table>
	  <tr>
	    <td>
	      <xsl:apply-templates select="license-details"/>
	    </td>
	  </tr>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="license-details">
    <p>
      <a href="/1g/index.cgi">Main menu</a>
      |
      <a href="/1g/license/list_license_agreements.cgi">License agreement list</a>
      |
      <a href="/1g/license/create_license_agreement.cgi">Create license agreement</a>
      |
      <a href="/1g/sales/show_prospect_list.cgi">Prospective customer list</a>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="width: 400px; white-space: nowrap; float: left; ">
      <tr>
        <td class="standard_table_title" colspan="2">
          Current License Agreement Details
        </td>
      </tr>
      <tr>
				<td class="license_input">
					License Date:
				</td>
				<td>
					<xsl:value-of select="license-date"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					Licensor Entity:
				</td>
				<td>
					<xsl:if test = "licensor-entity = 'mos1'" >
						Micro Office Solutions 1 LLC (36 W. 20th St., Fl. 3)
					</xsl:if>
					<xsl:if test = "licensor-entity = 'mos2'" >
						Micro Office Solutions 2 LLC (1375 Broadway, Fl. 3)
					</xsl:if>
					<xsl:if test = "licensor-entity = 'mos4'" >
						Micro Office Solutions 4 LLC (122 W. 22nd St., Fl. 10 &amp; 12)
					</xsl:if>
				</td>
      </tr>
      <xsl:if test = "signing-entity = 'company' or string-length(company) != 0" >
      <tr>
				<td class="license_input">
					Company:
				</td>
				<td>
					<xsl:value-of select="company"/>
				</td>
      </tr>
      </xsl:if>
      <tr>
				<td class="license_input">
					First Name:
				</td>
				<td>
					<xsl:value-of select="first-name"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					Last Name:
				</td>
				<td>
					<xsl:value-of select="last-name"/>
				</td>
      </tr>
      <xsl:if test = "not(contains(title,'_____')) and string-length(title) != 0">
				<tr>
					<td class="license_input">
						Title:
					</td>
					<td>
						<xsl:value-of select="title"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "string-length(address1) != 0">
				<tr>
					<td class="license_input">
						Address 1:
					</td>
					<td>
						<xsl:value-of select="address1"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "string-length(address2) != 0">
				<tr>
					<td class="license_input">
						Address 2:
					</td>
					<td>
						<xsl:value-of select="address2"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "string-length(city) != 0">
				<tr>
					<td class="license_input">
						City:
					</td>
					<td>
						<xsl:value-of select="city"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "string-length(state) != 0">
				<tr>
					<td class="license_input">
						State:
					</td>
					<td>
						<xsl:value-of select="state"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "string-length(zip) != 0">
				<tr>
					<td class="license_input">
						Zip Code:
					</td>
					<td>
						<xsl:value-of select="zip"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "not(contains(phone,'_____'))">
				<tr>
					<td class="license_input">
						Phone Number:
					</td>
					<td>
						<xsl:value-of select="phone"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "not(contains(email,'_____'))">
				<tr>
					<td class="license_input">
						Email Address:
					</td>
					<td>
						<xsl:value-of select="email"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "not(contains(business-type,'_____'))">
				<tr>
					<td class="license_input">
						Type of Business:
					</td>
					<td>
						<xsl:value-of select="business-type"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "desks != 0">
				<tr>
					<td class="license_input">
						No. of Desks:
					</td>
					<td>
						<xsl:value-of select="desks"/>
					</td>
				</tr>
      </xsl:if>
      <xsl:if test = "offices != 0">
				<tr>
					<td class="license_input">
						No. of Offices:
					</td>
					<td>
						<xsl:value-of select="offices"/>
					</td>
				</tr>
      </xsl:if>
      <tr>
				<td class="license_input">
					No. of Phone Lines:
				</td>
				<td>
					<xsl:value-of select="phone-lines"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					No. of Persons with Internet Access:
				</td>
				<td>
					<xsl:value-of select="persons"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					No. of Months in Initial Term:
				</td>
				<td>
					<xsl:value-of select="months"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					Start Date:
				</td>
				<td>
					<xsl:value-of select="start-date"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					End Date:
				</td>
				<td>
					<xsl:value-of select="end-date"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					Monthly License Fee:
				</td>
				<td>
					$<xsl:value-of select="license-fee"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					Security Deposit:
				</td>
				<td>
					$<xsl:value-of select="security"/>
				</td>
      </tr>
      <tr>
				<td class="license_input">
					Phone Service:
				</td>
				<td>
					<xsl:value-of select="phoneservice"/>
				</td>
      </tr>
    </table>

		<table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="background-color: lightgray; height: 100%; margin-left: 1em;">
      <tr>
        <td class="standard_table_title" colspan="2">
           License Agreement Operations
        </td>
      </tr>
			<tr>
				<td style="padding: 1em; line-height: 2em; ">
					<a>
						<xsl:attribute name="href">
							<xsl:text>/1g/license/download_license_agreement.cgi?id=</xsl:text>
							<xsl:value-of select="/page/id"/>
						</xsl:attribute>
						<xsl:text>View This License</xsl:text>
					</a>
					<br /><a>
						<xsl:attribute name="href">
							<xsl:text>/1g/license/download_pdf.cgi?id=</xsl:text>
							<xsl:value-of select="/page/id"/>
						</xsl:attribute>
						<xsl:text>View Signed License [PDF]</xsl:text>
					</a>
					<hr />
					<a>
						<xsl:attribute name="href">
							<xsl:text>/1g/license/edit_license_agreement.cgi?id=</xsl:text>
							<xsl:value-of select="/page/id"/>
						</xsl:attribute>
						<xsl:text>Edit These License Details</xsl:text>
					</a>
					<br /><a>
						<xsl:attribute name="href">
							<xsl:text>/1g/license/upload_license_pdf.cgi?id=</xsl:text>
							<xsl:value-of select="/page/id"/>
						</xsl:attribute>
						<xsl:text>Upload PDF</xsl:text>
					</a>
					<br /><a>
						<xsl:attribute name="href">
							<xsl:text>/1g/license/upload_license_attachment.cgi?id=</xsl:text>
							<xsl:value-of select="/page/id"/>
						</xsl:attribute>
						<xsl:text>Upload Attachment</xsl:text>
					</a>
					<br /><a>
						<xsl:attribute name="href">
							<xsl:text>/1g/license/confirm_delete_license_agreement.cgi?id=</xsl:text>
							<xsl:value-of select="/page/id"/>
						</xsl:attribute>
						<xsl:text>Delete this License</xsl:text>
					</a>
				</td>
				<td>
				</td>
			</tr>
		</table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

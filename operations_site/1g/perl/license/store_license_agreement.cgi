#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Mail::Sendmail;
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_id;
use read_file;
use insert_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $licensor_entity = Encode::decode($cgi_input_charset, $cgi->param("licensor_entity"));
  my @mos2 = qw/mos2-3 mos2-6 mos2-10 mos2-11/;
  my %license_floor_numbers = ('mos2-3'=>'3', 'mos2-6'=>'6', 'mos2-10'=>'10', 'mos2-11'=>'11');
  my %license_floor_words = ('mos2-3'=>'third', 'mos2-6'=>'sixth', 'mos2-10'=>'tenth', 'mos2-11'=>'eleventh');
  my $floor_number = $license_floor_numbers{$licensor_entity};
  my $floor_word = $license_floor_words{$licensor_entity};
  $licensor_entity = 'mos2' if (grep {$licensor_entity eq $_} @mos2);
  my $licensor_signer = Encode::decode($cgi_input_charset, $cgi->param("licensor_signer"));
  my $template_data;
  if ($licensor_entity eq "mos1") {
    $template_data = read_file("../resource/36_w20th_st_license_agreement_template.rtf");
  } elsif ( $licensor_entity eq "mos2" ) {
    $template_data = read_file("../resource/1375_broadway_license_agreement_template.rtf");
  } elsif ($licensor_entity eq "mos4") {
    $template_data = read_file("../resource/122_w_27th_st_license_agreement_template.rtf");
  } else {
    $template_data = "Are you trying to create a license agreement for a new location?  Please talk to <a href=\"mailto:wchao\@microoffice.com\">Wellie Chao</a> if you are unsure what to do.";
  }
  if (!$template_data) {
    $template_data = "Couldn't read template file.";
  }
  if ($licensor_signer eq "wchao") {
    $licensor_signer = "Wellie Chao";
  } elsif ($licensor_signer eq "drotbard") {
    $licensor_signer = "David Rotbard";
  } elsif ($licensor_signer eq "matt") {
    $licensor_signer = "Matthieu Rosanvallon";
  } elsif ($licensor_signer eq "yon") {
    $licensor_signer = "Yon Lam";
  }
    my $prospective_customer_id = $cgi->param("prospective_customer_id");
  my $license_date = $cgi->param("license_date");
  my $formal_date = $cgi->param("formal_date");
  my $company = Encode::decode($cgi_input_charset, $cgi->param("company"));
  my $first_name = Encode::decode($cgi_input_charset, $cgi->param("first_name"));
  my $last_name = Encode::decode($cgi_input_charset, $cgi->param("last_name"));
  my $title = Encode::decode($cgi_input_charset, $cgi->param("title"));
  my $signing_entity = Encode::decode($cgi_input_charset, $cgi->param("signing_entity"));
  my $licensee;
  if ($signing_entity eq "person") {
    $licensee = $first_name." ".$last_name;
  } else {
    $licensee = $company;
  }
  my $address1 = Encode::decode($cgi_input_charset, $cgi->param("address1"));
  my $address2 = Encode::decode($cgi_input_charset, $cgi->param("address2"));
  my $city = Encode::decode($cgi_input_charset, $cgi->param("city"));
  my $state = Encode::decode($cgi_input_charset, $cgi->param("state"));
  my $zip = Encode::decode($cgi_input_charset, $cgi->param("zip"));
  my $phone = Encode::decode($cgi_input_charset, $cgi->param("phone"));
  my $email = Encode::decode($cgi_input_charset, $cgi->param("email"));
  my $business_type = Encode::decode($cgi_input_charset, $cgi->param("business_type"));
  my $desks = $cgi->param("desks");
  my $formal_desks = $cgi->param("formal_desks");
  my $offices = $cgi->param("offices");
  my $formal_offices = $cgi->param("formal_offices");
  my $formal_space;
  if ($desks > 0) {
    if ($offices > 0) {
      $formal_space = $formal_desks." desk(s) and ".$formal_offices." office(s)";
    } else {
      $formal_space = $formal_desks." desk(s)";
    }
  } elsif ($offices > 0) {
    $formal_space = $formal_offices." office(s)";
  } else {
    $dbh->disconnect();
    die "show_page: create_license_missing_space.html\n";
  }
  my $phone_lines = $cgi->param("phone_lines");
  my $formal_phone_lines = $cgi->param("formal_phone_lines");
  my $persons = $cgi->param("persons");
  my $formal_persons = $cgi->param("formal_persons");
  my $months = $cgi->param("months");
  my $formal_months = $cgi->param("formal_months");
  my $start_date = $cgi->param("start_date");
  my $formal_start_date = $cgi->param("formal_start_date");
  my $end_date = $cgi->param("end_date");
  my $formal_end_date = $cgi->param("formal_end_date");
  my $license_fee = $cgi->param("license_fee");
  my $security = $cgi->param("security");
  my $company_signature = Encode::decode($cgi_input_charset, $cgi->param("company_signature"));
  # add appropriate title to company signature
  if ($licensor_signer eq "Wellie Chao") {
    $licensor_signer .= '\par Title: Managing Member';
  } elsif ($licensor_signer eq "David Rotbard") {
    $licensor_signer .= '\par Title: Principal';
  } elsif ($licensor_signer eq "Matthieu Rosanvallon") {
    $licensor_signer .= '\par Title: Sales Manager';
  } elsif ($licensor_signer eq "Yon Lam") {
    $licensor_signer .= '\par Title: Sales Manager';
  }
  # piece together name and address
  my $name = $first_name." ".$last_name;
  my $address = "_____________________________";
  my $street_address = $address1;
  if ($city) {
    if ($address2) {
      $street_address = $address1." ".$address2;
    }
    $address = $street_address.", ".$city.", ".$state." ".$zip;
  }
  
  
  my $spacenumber = $cgi->param("spacenumber");
  my $confroomhrs = $cgi->param("confroomhrs");
  my $phoneservice = $cgi->param("phoneservice");
  
  $template_data =~ s{LicenseDate}{$formal_date}gs;
  $template_data =~ s{LicenseCompanyName}{$licensee}gs;
  $template_data =~ s{LicenseName}{$name}gs;
  $template_data =~ s{LicenseTitle}{$title}gs;
  $template_data =~ s{LicenseAddress}{$address}gs;
  $template_data =~ s{LicenseEmail}{$email}gs;
  $template_data =~ s{LicensePhoneNumber}{$phone}gs;
  $template_data =~ s{LicenseSpace}{$formal_space}gs;
  $template_data =~ s{LicenseFloorNumber}{$floor_number}gs;
  $template_data =~ s{LicenseFloorWord}{$floor_word}gs;
  $template_data =~ s{LicenseBusinessType}{$business_type}gs;
  $template_data =~ s{LicensePhoneLines}{$formal_phone_lines}gs;
  $template_data =~ s{LicensePersons}{$formal_persons}gs;
  $template_data =~ s{LicenseMonths}{$formal_months}gs;
  $template_data =~ s{LicenseStart}{$formal_start_date}gs;
  $template_data =~ s{LicenseEnd}{$formal_end_date}gs;
  $template_data =~ s{LicenseFee}{$license_fee}gs;
  $template_data =~ s{LicenseSecurity}{$security}gs;
  $template_data =~ s{LicenseCompanySignature}{$company_signature}gs;
  $template_data =~ s{LicensorSigner}{$licensor_signer}gs;
  $template_data =~ s{SpaceNumber}{$spacenumber}gs;
  $template_data =~ s{PhoneService}{$phoneservice}gs;
  $template_data =~ s{ConfRoomHrs}{$confroomhrs}gs;
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  my $row_id = get_id($dbh, "license_agreement_seq");
  push(@sql_column_list, ["id", $row_id, SQL_INTEGER]);
  push(@sql_column_list, ["licensor_entity", $licensor_entity]);
  push(@sql_column_list, ["prospective_customer_id", $prospective_customer_id, SQL_INTEGER]);
  push(@sql_column_list, ["license_date", $license_date]);
  push(@sql_column_list, ["company", Encode::encode($db_charset, $company)]);
  push(@sql_column_list, ["first_name", Encode::encode($db_charset, $first_name)]);
  push(@sql_column_list, ["last_name", Encode::encode($db_charset, $last_name)]);
  push(@sql_column_list, ["signing_entity", Encode::encode($db_charset, $signing_entity)]);
  push(@sql_column_list, ["title", Encode::encode($db_charset, $title)]);
  push(@sql_column_list, ["address1", Encode::encode($db_charset, $address1)]);
  push(@sql_column_list, ["address2", Encode::encode($db_charset, $address2)]);
  push(@sql_column_list, ["city", Encode::encode($db_charset, $city)]);
  push(@sql_column_list, ["state", Encode::encode($db_charset, $state)]);
  push(@sql_column_list, ["zip", Encode::encode($db_charset, $zip)]);
  push(@sql_column_list, ["phone", Encode::encode($db_charset, $phone)]);
  push(@sql_column_list, ["email", Encode::encode($db_charset, $email)]);
  push(@sql_column_list, ["business_type", Encode::encode($db_charset, $business_type)]);
  push(@sql_column_list, ["desks", $desks, SQL_INTEGER]);
  push(@sql_column_list, ["offices", $offices, SQL_INTEGER]);
  push(@sql_column_list, ["phone_lines", $phone_lines, SQL_INTEGER]);
  push(@sql_column_list, ["persons", $persons, SQL_INTEGER]);
  push(@sql_column_list, ["months", $months, SQL_INTEGER]);
  push(@sql_column_list, ["start_date", $start_date]);
  push(@sql_column_list, ["end_date", $end_date]);
  push(@sql_column_list, ["license_fee", $license_fee, SQL_INTEGER]);
  push(@sql_column_list, ["security", $security, SQL_INTEGER]);
  push(@sql_column_list, ["confroomhrs", $confroomhrs, SQL_INTEGER]);
  push(@sql_column_list, ["spacenumber", $spacenumber]);
  push(@sql_column_list, ["phoneservice", $phoneservice]);
  push(@sql_column_list, ["license_agreement_rtf", $template_data, "pg_bytea"]);
  $licensee =~ s/[\/ ;\[\]\<\>&\t]/_/g;
  $licensee =~ s/[.,]//g;
  my $filename = lc($licensee) . "_license_agreement.rtf"; 
  push(@sql_column_list, ["filename", Encode::encode($db_charset, $filename)]);
  my $web_posting_id = insert_row($dbh, "license_agreement_tbl", \@sql_column_list);
  my (@tmp_sql_column_list);
  push(@tmp_sql_column_list, ["license_created", 1, SQL_BOOLEAN]); 
  update_row($dbh, "prospective_customer_tbl", \@tmp_sql_column_list, $prospective_customer_id);
  my $html = <<HTML;
<html>
<body>
<p>An agreement has been created and the terms are below:</p>
<p>
License Date: $license_date<br> 
Formal Date: $formal_date<br> 
Licensor Entity: $licensor_entity<br> 
Licensor Signer: $licensor_signer<br> 
Prospective-customer-id: $prospective_customer_id<br> 
Company: $company<br> 
First Name: $first_name<br> 
Last Name: $last_name<br> 
Title: $title<br> 
Signing Entity: $signing_entity<br> 
Address1: $address1<br> 
Address2: $address2<br> 
City: $city<br> 
State: $state<br> 
Zip: $zip<br> 
Phone: $phone<br> 
Email: $email<br> 
Business Type: $business_type<br> 
Desks: $desks<br> 
Formal Desks: $formal_desks<br> 
Offices: $offices<br> 
Formal Offices: $formal_offices<br> 
Phone Lines: $phone_lines<br> 
Formal Phone Lines: $formal_phone_lines<br> 
Persons: $persons<br> 
Formal Persons: $formal_persons<br> 
Months: $months<br> 
Formal Months: $formal_months<br> 
Start Date: $start_date<br> 
Formal Start Date: $formal_start_date<br> 
End Date: $end_date<br> 
Formal End Date: $formal_end_date<br> 
License Fee: $license_fee<br> 
Security: $security<br> 
Company Signature: $company_signature<br> 
Floor Number: $floor_number<br>
Space Number: $spacenumber<br> 
Phone Service: $phoneservice<br> 
Conf Room Hrs: $confroomhrs<br>
</p>
</body>
</html>
HTML
	my %mail = (
         from => 'sales@microoffice.com',
         to => 'license_agreement@microoffice.com',
         subject => 'New Customer License Agreement',
         body => $html,
         'content-type' => 'text/html; charset="iso-8859-1"',);
         
  sendmail(%mail);  
  
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <id>$row_id</id>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "download_license_agreement.xslt", $cgi);
}

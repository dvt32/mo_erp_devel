#!/usr/bin/perl
use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use LWP;
use JSON;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use get_database_handle;
use get_row_list;
use insert_row;
use get_date_now;
use add_time;
use lead_pad;
use delete_rows_with_query;

my $bing_max_search_results = 48;
my $bing_max_search_tries = 3;
my $max_age = 60; # max age of records in days. older records are purged each day.
my $app_id = "4E3FB9D1DC9FD62C6513B487CDB7ECF1AB41DE91";

main();

sub main() {
  my $instance_map = {};
  my $dbh = get_database_handle($instance_map, "main");
  my $sql_query = "select * from monitored_keyword_tbl";
  my @monitored_keyword_list;
  get_row_list($dbh, $sql_query, undef, \@monitored_keyword_list);
					  
  # query bing on each monitored keyword.
  for my $monitored_keyword (@monitored_keyword_list) {
    my $keyword = $monitored_keyword->{"keyword"};
	my $search_language = "en-US";
	my $search_failed = 0;
	my $request_succeeded = 0;
	my $num_tries = 0;
	my $results;
	
	while(!$request_succeeded) {
	  my $browser = LWP::UserAgent->new;
	  my $url = "http://api.bing.net/json.aspx?AppId=$app_id&Version=2.2&Market=$search_language&Query=$keyword&Sources=web+spell&Web.Count=$bing_max_search_results";
	  my $response = $browser->get($url);
	  $results = from_json($response->content(), {utf8 => 1});
	  $num_tries++;
	  
	  if ($response->is_success()) {
	    $request_succeeded = 1;
	  } elsif ($num_tries >= $bing_max_search_tries) {
	    warn "response failure on keyword ($keyword): " . $response->status_line();
	    $search_failed = 1;
	    last;
	  } else {
	    # wait 30 seconds and try same HTTP request again.
	    sleep(30);
	  }
	}
	
	if (!$search_failed) {
	  my @sql_column_list;
      push(@sql_column_list, ["query", $keyword]);
      push(@sql_column_list, ["lr", $search_language]) if ($search_language);
      my $bing_query_id = insert_row($dbh, "bing_query_tbl", \@sql_column_list, "bing_query_seq");
      my $search_rank = 1;
	  
      for my $search_result(@{$results->{'SearchResponse'}->{'Web'}->{'Results'}}) {
        @sql_column_list = ();
	    push(@sql_column_list, ["bing_query_id", $bing_query_id, SQL_INTEGER]);
	    push(@sql_column_list, ["search_rank", $search_rank++, SQL_INTEGER]);
	    push(@sql_column_list, ["content", $search_result->{"Description"}]);
	    push(@sql_column_list, ["title", $search_result->{"Title"}]);
	    push(@sql_column_list, ["title_no_formatting", $search_result->{"Title"}]);
	    push(@sql_column_list, ["url", $search_result->{"Url"}]);
	    push(@sql_column_list, ["unescaped_url", $search_result->{"Url"}]);
	    push(@sql_column_list, ["visible_url", $search_result->{"DisplayUrl"}]);
	    push(@sql_column_list, ["cache_url", $search_result->{"CachedUrl"}]) if ($search_result->{"CachedUrl"});
	    insert_row($dbh, "bing_search_result_tbl", \@sql_column_list);
	  }
	}
  }
  
  # delete records older than max_age days.
  $sql_query = "delete from bing_search_result_tbl where bing_query_id in (select id from bing_query_tbl where creation_dt < ?)";
  my ($today_year, $today_month, $today_day) = get_date_now();
  my ($cutoff_year, $cutoff_month, $cutoff_day) = add_time($today_year, $today_month, $today_day, 0, 0, 0, "-$max_age", "day");
  my $cutoff_date = $cutoff_year . "-" . lead_pad($cutoff_month, 2) . "-" . lead_pad($cutoff_day, 2);
  my @sql_param_list = ($cutoff_date);
  delete_rows_with_query($dbh, $sql_query, \@sql_param_list);
  $sql_query = "delete from bing_query_tbl where creation_dt < ?";
  delete_rows_with_query($dbh, $sql_query, \@sql_param_list);
  $dbh->disconnect();
}



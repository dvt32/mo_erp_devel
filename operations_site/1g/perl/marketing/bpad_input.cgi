#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Storable qw(freeze nfreeze thaw);
use LWP::UserAgent;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_web_root_dir;
use read_file;
use get_cgi;
use get_database_handle;
use select_row;
use update_row;
use apply_xslt_output_xhtml;
use utc_datetime_to_epoch_time;
use timestamp_to_components;
use get_datetime_now;
use dump_to_file;
use global_config;

main_exception_wrapper(\&main);

sub main {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $web_posting_id = $cgi->param("id");
  if (!$web_posting_id) {
    die "show_page: bpad_missing_web_posting_id.html\n";
  }
  my $web_posting = select_row($dbh, "web_posting_tbl", $web_posting_id);
  if (!$web_posting) {
    die "show_page: bpad_web_posting_not_found.html\n";
  }
  if ($web_posting->{"processed_dt"}) {
    die "show_page: bpad_already_processed.html\n";
  }
  if ($web_posting->{"web_spider_start_dt"}) {
    my $web_spider_start_epoch_time = utc_datetime_to_epoch_time(timestamp_to_components($web_posting->{"web_spider_start_dt"}));
    my $now_epoch_time = utc_datetime_to_epoch_time(get_datetime_now());
    if (($now_epoch_time - $web_spider_start_epoch_time) < 60) {
      die "show_page: bpad_currently_being_processed.html\n";
    }
  }
  my (@sql_column_list);
  push(@sql_column_list, ["web_spider_start_dt", ["now()"]]);
  update_row($dbh, "web_posting_tbl", \@sql_column_list, $web_posting_id);
  my $internal_map = thaw($web_posting->{"internal_map"});
  if ($internal_map->{"verification_image_jpg"}) {
    # already retrieved verification image, so ask user to read and enter the letters.
    # generate XML.
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<page>
  <web_posting_id>$web_posting_id</web_posting_id>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "bpad_input.xslt", $cgi);
    return;
  }
  my $config_filename = $internal_map->{"config_filename"};
  if (!$config_filename) {
    die "you must specify the config filename.";
  }
  my $get_data_base_dir = global_config::lookup("data_base_dir");
  my $data_base_dir = get_web_root_dir() . $get_data_base_dir;
  if (!-e "$data_base_dir/$config_filename") {
    die "$config_filename does not exist.";
  }
  my $config_file_data = read_file("$data_base_dir/$config_filename");
  my $price;
  my $title;
  my $regionOther;
  my $posting_body_filename;
  my $posting_body;
  my $mapAddress;
  my $mapZip;
  if ($config_file_data =~ /^[ \t]*price[ \t]*=[ \t]*(.+)$/m) {
    $price = $1;
  } else {
    $price = "";
  }
  if ($config_file_data =~ /^[ \t]*title[ \t]*=[ \t]*(.+)$/m) {
    $title = $1;
  } else {
    die "must specify title in the config file";
  }
  if ($config_file_data =~ /^[ \t]*regionOther[ \t]*=[ \t]*(.+)$/m) {
    $regionOther = $1;
  } else {
    die "must specify regionOther in the config file";
  }
  if ($config_file_data =~ /^[ \t]*posting_body_filename[ \t]*=[ \t]*(.+)$/m) {
    $posting_body_filename = $1;
  } else {
    die "must specify posting_body_filename in the config file";
  }
  if ($config_file_data =~ /^[ \t]*mapAddress[ \t]*=[ \t]*(.+)$/m) {
    $mapAddress = $1;
  } else {
    $mapAddress = "";
  }
  if ($config_file_data =~ /^[ \t]*mapZip[ \t]*=[ \t]*(.+)$/m) {
    $mapZip = $1;
  }
  if (!-e "$data_base_dir/$posting_body_filename") {
    die "$posting_body_filename does not exist";
  }
  $posting_body = read_file("$data_base_dir/$posting_body_filename");
  my $user_agent = LWP::UserAgent->new();
  $user_agent->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)");
  push(@{$user_agent->requests_redirectable }, "POST");
  $user_agent->cookie_jar({});
  my $url = "";
  my %param_map = ();
  my $response;
  my $response_content;
  $url = "http://newyork.backpage.com/online/classifieds/index";
  $response = $user_agent->get($url);
  $response_content = $response->content();
  dump_to_file("/dev/shm/backpage1.html", $response_content);
  if ($response_content !~ m{<a href="http://newyork.backpage.com/CommercialForRent/">commercial</a>}) {
    die qq{no match for "commercial" link under rentals section.};
  }
  $url = "http://newyork.backpage.com/CommercialForRent/";
  $response = $user_agent->get($url);
  $response_content = $response->content();
  dump_to_file("/dev/shm/backpage2.html", $response_content);
  if ($response_content =~ m{<form name="formPost" action="http://posting.newyork.backpage.com/online/classifieds/PostAdPPI.html/nyc/newyork.backpage.com/" method="get">.*?<input type="submit" value="Post an Ad" class="button" id="postAdButton">.*?<input type="hidden" name="section" value="(\d+)">.*?<input type="hidden" name="category" value="(\d+)">.*?<input type="hidden" name="u" value="(\w+)">.*?<input type="hidden" name="serverName" value="(.*?)">.*?</form>}gis) {
    my $section = $1;
    my $category = $2;
    my $u = $3;
    my $serverName = $4;
    $url = "http://posting.newyork.backpage.com/online/classifieds/PostAdPPI.html/nyc/newyork.backpage.com/?section=$section&category=$category&u=$u&serverName=$serverName";
  } else {
    die qq{no match for "Post an Ad" button.};
  }
  $response = $user_agent->get($url);
  $response_content = $response->content();
  dump_to_file("/dev/shm/backpage3.html", $response_content);
  if ($response_content !~ m{<a href="(.+?)">Manhattan</a>}) {
    die qq{no match for "Manhattan" link on "Write Ad" page.};
  }
  $url = $1;
  $url =~ s/&amp;/&/g;
  $response = $user_agent->get($url);
  $response_content = $response->content();
  dump_to_file("/dev/shm/backpage4.html", $response_content);
  my $form_action;
  if ($response_content =~ m{<form name="f" method="post" action="(.+?)" enctype="multipart/form-data">}) {
    $form_action = $1;
  } else {
    die qq{no match for form on "Write Ad" page.};
  }
  my $u;
  if ($response_content =~ m{<input type="hidden" name="u" value="(.+?)">}) {
    $u = $1;
  } else {
    die qq{no match for u.};
  }
  my $serverName;
  if ($response_content =~ m{<input type="hidden" name="serverName" value="(.+?)">}) {
    $serverName = $1;
  } else {
    die qq{no match for serverName.};
  }
  my $lang;
  if ($response_content =~ m{<input type="hidden" name="lang" value="(.+?)">}) {
    $lang = $1;
  } else {
    die qq{no match for lang.};
  }
  my $section;
  if ($response_content =~ m{<input type="hidden" name="section" value="(.+?)">}) {
    $section = $1;
  } else {
    die qq{no match for section.};
  }
  my $category;
  if ($response_content =~ m{<input type="hidden" name="category" value="(.+?)">}) {
    $category = $1;
  } else {
    die qq{no match for category.};
  }
  my $disc;
  if ($response_content =~ m{<input type="hidden" name="disc" value="(.*?)">}) {
    $disc = $1;
  } else {
    die qq{no match for disc.};
  }
  my $firstName;
  if ($response_content =~ m{<input type="hidden" name="firstName" value="(.*?)">}) {
    $firstName = $1;
  } else {
    die qq{no match for firstName.};
  }
  my $lastName;
  if ($response_content =~ m{<input type="hidden" name="lastName" value="(.*?)">}) {
    $lastName = $1;
  } else {
    die qq{no match for lastName.};
  }
  my $address;
  if ($response_content =~ m{<input type="hidden" name="address" value="(.*?)">}) {
    $address = $1;
  } else {
    die qq{no match for address.};
  }
  my $customerCity;
  if ($response_content =~ m{<input type="hidden" name="customerCity" value="(.*?)">}) {
    $customerCity = $1;
  } else {
    die qq{no match for customerCity.};
  }
  my $state;
  if ($response_content =~ m{<input type="hidden" name="state" value="(.*?)">}) {
    $state = $1;
  } else {
    die qq{no match for state.};
  }
  my $zip;
  if ($response_content =~ m{<input type="hidden" name="zip" value="(.*?)">}) {
    $zip = $1;
  } else {
    die qq{no match for zip.};
  }
  my $region;
  if ($response_content =~ m{<input type="hidden" name="region" value="(.*?)">}) {
    $region = $1;
  } else {
    die qq{no match for region.};
  }
  my $nextPage;
  if ($response_content =~ m{<input type="hidden" name="nextPage" value="(.*?)">}) {
    $nextPage = $1;
  } else {
    die qq{no match for nextPage.};
  }
  my $largeImage1;
  if ($response_content =~ m{<input type="hidden" name="largeImage1" value="(.*?)">}) {
    $largeImage1 = $1;
  } else {
    die qq{no match for largeImage1.};
  }
  my $mediumImage1;
  if ($response_content =~ m{<input type="hidden" name="mediumImage1" value="(.*?)">}) {
    $mediumImage1 = $1;
  } else {
    die qq{no match for mediumImage1.};
  }
  my $smallImage1;
  if ($response_content =~ m{<input type="hidden" name="smallImage1" value="(.*?)">}) {
    $smallImage1 = $1;
  } else {
    die qq{no match for smallImage1.};
  }
  my $largeImage2;
  if ($response_content =~ m{<input type="hidden" name="largeImage2" value="(.*?)">}) {
    $largeImage2 = $1;
  } else {
    die qq{no match for largeImage2.};
  }
  my $mediumImage2;
  if ($response_content =~ m{<input type="hidden" name="mediumImage2" value="(.*?)">}) {
    $mediumImage2 = $1;
  } else {
    die qq{no match for mediumImage2.};
  }
  my $smallImage2;
  if ($response_content =~ m{<input type="hidden" name="smallImage2" value="(.*?)">}) {
    $smallImage2 = $1;
  } else {
    die qq{no match for smallImage2.};
  }
  my $largeImage3;
  if ($response_content =~ m{<input type="hidden" name="largeImage3" value="(.*?)">}) {
    $largeImage3 = $1;
  } else {
    die qq{no match for largeImage3.};
  }
  my $mediumImage3;
  if ($response_content =~ m{<input type="hidden" name="mediumImage3" value="(.*?)">}) {
    $mediumImage3 = $1;
  } else {
    die qq{no match for mediumImage3.};
  }
  my $smallImage3;
  if ($response_content =~ m{<input type="hidden" name="smallImage3" value="(.*?)">}) {
    $smallImage3 = $1;
  } else {
    die qq{no match for smallImage3.};
  }
  my $largeImage4;
  if ($response_content =~ m{<input type="hidden" name="largeImage4" value="(.*?)">}) {
    $largeImage4 = $1;
  } else {
    die qq{no match for largeImage4.};
  }
  my $mediumImage4;
  if ($response_content =~ m{<input type="hidden" name="mediumImage4" value="(.*?)">}) {
    $mediumImage4 = $1;
  } else {
    die qq{no match for mediumImage4.};
  }
  my $smallImage4;
  if ($response_content =~ m{<input type="hidden" name="smallImage4" value="(.*?)">}) {
    $smallImage4 = $1;
  } else {
    die qq{no match for smallImage4.};
  }
  my $superRegion;
  if ($response_content =~ m{<input type="hidden" name="superRegion" value="(.*?)">}) {
    $superRegion = $1;
  } else {
    die qq{no match for superRegion.};
  }
  $url = $form_action;
  %param_map =
    (
     "u" => $u,
     "serverName" => $serverName,
     "lang" => $lang,
     "section" => $section,
     "category" => $category,
     "disc" => $disc,
     "firstName" => $firstName,
     "lastName" => $lastName,
     "address" => $address,
     "customerCity" => $customerCity,
     "state" => $state,
     "zip" => $zip,
     "region" => $region,
     "nextPage" => $nextPage,
     "largeImage1" => $largeImage1,
     "mediumImage1" => $mediumImage1,
     "smallImage1" => $smallImage1,
     "largeImage2" => $largeImage2,
     "mediumImage2" => $mediumImage2,
     "smallImage2" => $smallImage2,
     "largeImage3" => $largeImage3,
     "mediumImage3" => $mediumImage3,
     "smallImage3" => $smallImage3,
     "largeImage4" => $largeImage4,
     "mediumImage4" => $mediumImage4,
     "smallImage4" => $smallImage4,
     "price" => $price,
     "bedrooms" => "",
     "superRegion" => $superRegion,
     "title" => $title,
     "regionOther" => $regionOther,
     "ad" => $posting_body,
     "adPlacedBy" => "Owner/Property Manager",
     "feesPaidBy" => "",
     "mapAddress" => $mapAddress,
     "mapZip" => $mapZip,
     "mapCrossStreetA" => "",
     "mapCrossStreetB" => "",
     "mapCrossStreetZip" => "",
     "email" => "sales\@microoffice.com",
     "emailConfirm" => "sales\@microoffice.com",
     "allowReplies" => "Anonymous",
     "showAdLinks" => "No",
     "repostCycle" => "1",
     "autoRepost" => "4",
     "sponsorWeeks" => "1",
     "promotionCode" => "",
     );
  $response = $user_agent->post($url, \%param_map);
  $response_content = $response->content();
  dump_to_file("/dev/shm/backpage5.html", $response_content);
  if ($response_content =~ m{Duplicate Ad Found}) {
    die qq{duplicate ad found.};
  }
  if ($response_content =~ m{<h3 class="error">Oops!</h3>}) {
    $response_content =~ m{<ul>.+?<li class="error">(.+?)</ul>}is;
    my $err_msg = $1;
    $err_msg =~ s/\s*$//;
    die qq{error before "Preview Ad" page: $err_msg};
  }
  if ($response_content =~ m{(<form name="f" method="post" action=".+?" onsubmit=["'].+?['"]>.+?<input type="submit"[^>]+?>)}is) {
    $response_content = $1;
  } else {
    die qq{no match for form on "Preview Ad" page.};
  }
  if ($response_content =~ m{<form name="f" method="post" action="(.+?)" onsubmit=["'].+?['"]>}) {
    $form_action = $1;
  } else {
    die qq{no match for form on "Preview Ad" page.};
  }
  my $adKey;
  if ($response_content =~ m{<input type="hidden" name="adKey" value="(.+?)">}) {
    $adKey = $1;
  } else {
    die qq{no match for adKey.};
  }
  if ($response_content =~ m{<input type="hidden" name="nextPage" value="(.+?)">}) {
    $nextPage = $1;
  } else {
    die qq{no match for nextPage.};
  }
  my ($recaptcha_noscript_url, $recaptcha_public_key);
  if ($response_content =~ m{<noscript>.*?<iframe .*?src="(http://www.google.com/recaptcha/api/noscript\?k=[^\"]+?)"}s) {
    $recaptcha_noscript_url = $1;
    $recaptcha_noscript_url =~ m{^http://www.google.com/recaptcha/api/noscript\?k=(.+)$};
    $recaptcha_public_key = $1;
  } else {
    die qq{no match for recaptcha noscript iframe.};
  }
  $response = $user_agent->get($recaptcha_noscript_url);
  $recaptcha_noscript_url = $response->request()->uri();
  $response_content = $response->content();
  my $recaptcha_challenge_field;
  if ($response_content =~ m{<input type="hidden" name="recaptcha_challenge_field" id="recaptcha_challenge_field" value="([^\"]+?)">}) {
    $recaptcha_challenge_field = $1;
  } else {
    die qq{no match for recaptcha challenge field on www.google.com/recaptcha/api/noscript page.};
  }
  my $recaptcha_image_url;
  if ($response_content =~ m{<center><img .*?src="([^\"]+?)"></center>}) {
    my $relative_url = $1;
    if ($relative_url !~ m{^/}) {
      $relative_url = "/" . $relative_url;
    }
    $recaptcha_noscript_url =~ m{^(http://.*)/};
    my $recaptcha_base_url = $1;
    $recaptcha_image_url = $recaptcha_base_url . $relative_url;
  } else {
    die qq{no match for recaptcha image on www.google.com/recaptcha/api/noscript page.};
  }
  $response = $user_agent->get($recaptcha_image_url);
  my $verification_image_jpg = $response->content();
  $url = $form_action;
  %param_map =
    (
     "adKey" => $adKey,
     "nextPage" => $nextPage,
     "u" => $u,
     "serverName" => $serverName,
     "lang" => $lang,
     "section" => $section,
     "category" => $category,
     "disc" => $disc,
     "price" => $price,
     "superRegion" => $superRegion,
     "title" => $title,
     "regionOther" => $regionOther,
     "adPlacedBy" => "Owner/Property Manager",
     "mapAddress" => $mapAddress,
     "mapZip" => $mapZip,
     "email" => "sales\@microoffice.com",
     "emailConfirm" => "sales\@microoffice.com",
     "allowReplies" => "Anonymous",
     "showAdLinks" => "No",
     "repostCycle" => "1",
     "autoRepost" => "4",
     "sponsorWeeks" => "1",
     "ad" => $posting_body,
     );
  my %internal_map =
    (
     "verification_image_jpg" => $verification_image_jpg,
     "recaptcha_noscript_url" => $recaptcha_noscript_url,
     "recaptcha_challenge_field" => $recaptcha_challenge_field,
    );
  my (@sql_column_list);
  push(@sql_column_list, ["user_agent", freeze($user_agent), "pg_bytea"]);
  push(@sql_column_list, ["url", $url]);
  push(@sql_column_list, ["param_map", freeze(\%param_map), "pg_bytea"]);
  push(@sql_column_list, ["internal_map", freeze(\%internal_map), "pg_bytea"]);
  update_row($dbh, "web_posting_tbl", \@sql_column_list, $web_posting_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<page>
  <web_posting_id>$web_posting_id</web_posting_id>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "bpad_input.xslt", $cgi);
}


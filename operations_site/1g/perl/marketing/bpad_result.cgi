#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Storable qw(freeze nfreeze thaw);
use LWP::UserAgent;
# need to explicitly 'use HTTP::Cookies' because deserialized LWP::UserAgent does not realize it needs to be loaded.
use HTTP::Cookies;
use HTML::Entities ();
use Encode;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use select_row;
use update_row;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use send_email;
use dump_to_file;

main_exception_wrapper(\&main);

sub main {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $web_posting_id = $cgi->param("id");
  if (!$web_posting_id) {
    die "show_page: bpad_missing_web_posting_id.html\n";
  }
  my $verification_word = Encode::decode($cgi_input_charset, $cgi->param("verification_word"));
  my $web_posting = select_row($dbh, "web_posting_tbl", $web_posting_id);
  if (!$web_posting) {
    die "show_page: bpad_web_posting_not_found.html\n";
  }
  if ($web_posting->{"processed_dt"}) {
    die "show_page: bpad_already_processed.html\n";
  }
  my $internal_map = thaw($web_posting->{"internal_map"});
  if (!$internal_map->{"verification_image_jpg"}) {
    # didn't post ad yet, so display error.
    die "show_page: bpad_missing_verification_image.html\n";
  }
  my $user_agent = thaw($web_posting->{"user_agent"});
  my $recaptcha_noscript_url = $internal_map->{"recaptcha_noscript_url"};
  my $recaptcha_challenge_field = $internal_map->{"recaptcha_challenge_field"};
  my %recaptcha_param_map =
    ("recaptcha_challenge_field" => $recaptcha_challenge_field,
     "recaptcha_response_field" => $verification_word,
     "submit" => "I\'m a human",);
  my $response = $user_agent->post($recaptcha_noscript_url, \%recaptcha_param_map);
  $recaptcha_noscript_url = $response->request()->uri();
  my $response_content = $response->content();
  if ($response_content =~ m{Your answer was correct. Please copy and paste the text in this text box into the box below.}i) {
    $response_content =~ m{<textarea.*?>(.+?)</textarea>}is;
    $recaptcha_challenge_field = $1;
    dump_to_file("/dev/shm/bpad_result_1.html", $response_content);
  } else {
    warn "reCAPTCHA.net: wrong word";
    dump_to_file("/dev/shm/bpad_result_2.html", $response_content);
    if ($response_content =~ m{<input type="hidden" name="recaptcha_challenge_field" id="recaptcha_challenge_field" value="([^\"]+?)">}i) {
      $internal_map->{"recaptcha_challenge_field"} = $1;
    } else {
      die qq{no match for recaptcha challenge field on www.google.com/recaptcha/api/noscript page.};
    }
    my $recaptcha_image_url;
    if ($response_content =~ m{<center><img .*?src="([^\"]+?)"></center>}) {
      my $relative_url = $1;
      if ($relative_url !~ m{^/}) {
	$relative_url = "/" . $relative_url;
      }
      $recaptcha_noscript_url =~ m{^(http://.*)/};
      my $recaptcha_base_url = $1;
      $recaptcha_image_url = $recaptcha_base_url . $relative_url;
    } else {
      die qq{no match for recaptcha image on www.google.com/recaptcha/api/noscript page.};
    }
    $response = $user_agent->get($recaptcha_image_url);
    my $verification_image_jpg = $response->content();
    $internal_map->{"recaptcha_noscript_url"} = $recaptcha_noscript_url;
    $internal_map->{"verification_image_jpg"} = $verification_image_jpg;
    my (@sql_column_list);
    push(@sql_column_list, ["user_agent", freeze($user_agent), "pg_bytea"]);
    push(@sql_column_list, ["internal_map", freeze($internal_map), "pg_bytea"]);
    update_row($dbh, "web_posting_tbl", \@sql_column_list, $web_posting_id);
    # generate XML.
    my $generated_xml = "";
    $verification_word = HTML::Entities::encode($verification_word);
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <web_posting_id>$web_posting_id</web_posting_id>
  <verification_word>$verification_word</verification_word>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "bpad_wrong_verification_word.xslt", $cgi);
    return;
  }
  my $param_map = thaw($web_posting->{"param_map"});
  $param_map->{"recaptcha_challenge_field"} = $recaptcha_challenge_field;
  $param_map->{"recaptcha_response_field"} = "manual_challenge";
  my $url = $web_posting->{"url"};
  $response = $user_agent->post($url, $param_map);
  $response_content = $response->content();
  dump_to_file("/dev/shm/bpad_result_3.html", $response_content);
  if ($response_content =~ m{You entered the wrong security code. Please try again.}i) {
    warn "backpage.com: wrong verification word";
    dump_to_file("/dev/shm/bpad_result_4.html", $response_content);
    my ($recaptcha_noscript_url, $recaptcha_public_key);
    if ($response_content =~ m{<noscript>.*?<iframe .*?src="(http://www.google.com/recaptcha/api/noscript\?k=[^\"]+?)"}s) {
      $recaptcha_noscript_url = $1;
      $recaptcha_noscript_url =~ m{^http://www.google.com/recaptcha/api/noscript\?k=(.+)$};
      $recaptcha_public_key = $1;
    } else {
      die qq{no match for recaptcha noscript iframe.};
    }
    $response = $user_agent->get($recaptcha_noscript_url);
    $recaptcha_noscript_url = $response->request()->uri();
    $response_content = $response->content();
    my $recaptcha_challenge_field;
    if ($response_content =~ m{<input type="hidden" name="recaptcha_challenge_field" id="recaptcha_challenge_field" value="([^\"]+?)">}) {
      $recaptcha_challenge_field = $1;
    } else {
      die qq{no match for recaptcha challenge field on www.google.com/recaptcha/api/noscript page.};
    }
    my $recaptcha_image_url;
    if ($response_content =~ m{<center><img .*?src="([^\"]+?)"></center>}) {
      my $relative_url = $1;
      if ($relative_url !~ m{^/}) {
	$relative_url = "/" . $relative_url;
      }
      $recaptcha_noscript_url =~ m{^(http://.*)/};
      my $recaptcha_base_url = $1;
      $recaptcha_image_url = $recaptcha_base_url . $relative_url;
    } else {
      die qq{no match for recaptcha image on www.google.com/recaptcha/api/noscript page.};
    }
    $response = $user_agent->get($recaptcha_image_url);
    my $verification_image_jpg = $response->content();
    $internal_map->{"verification_image_jpg"} = $verification_image_jpg;
    $internal_map->{"recaptcha_noscript_url"} = $recaptcha_noscript_url;
    $internal_map->{"recaptcha_challenge_field"} = $recaptcha_challenge_field;
    my (@sql_column_list);
    push(@sql_column_list, ["user_agent", freeze($user_agent), "pg_bytea"]);
    push(@sql_column_list, ["param_map", freeze($param_map), "pg_bytea"]);
    push(@sql_column_list, ["internal_map", freeze($internal_map), "pg_bytea"]);
    update_row($dbh, "web_posting_tbl", \@sql_column_list, $web_posting_id);
    # generate XML.
    my $generated_xml = "";
    $verification_word = HTML::Entities::encode($verification_word);
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <web_posting_id>$web_posting_id</web_posting_id>
  <verification_word>$verification_word</verification_word>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "bpad_wrong_verification_word.xslt", $cgi);
    return;
  }
  # if successful, send an email receipt.
  send_email
    (
     "from" => qq{"Micro Office web posting daemon" <web_posting_daemon\@microoffice.com>},
     "to" => qq{"Image Recognizers" <web_posting_people\@microoffice.com>},
     "subject" => "Successful posting; backpage.com [$web_posting_id]",
     "message" => "Your posting was successful. You entered: $verification_word.",
    );
  # update the database row to mark it as processed.
  my (@sql_column_list);
  push(@sql_column_list, ["processed_dt", ["now()"]]);
  # issue the update command to the database.
  update_row($dbh, "web_posting_tbl", \@sql_column_list, $web_posting_id);
  # generate XML.
  my $generated_xml = "";
  $verification_word = HTML::Entities::encode($verification_word);
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <web_posting_id>$web_posting_id</web_posting_id>
  <verification_word>$verification_word</verification_word>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "bpad_result.xslt", $cgi);
}


#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Storable qw(freeze nfreeze thaw);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_cgi;
use get_database_handle;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $web_posting_id = $cgi->param("id");
  if (!$web_posting_id) {
    die "missing web posting id";
  }
  my $web_posting = select_row($dbh, "web_posting_tbl", $web_posting_id);
  if (!$web_posting) {
    die "web posting not found";
  }
  my $internal_map = thaw($web_posting->{"internal_map"});
  if (!$internal_map->{"verification_image_jpg"}) {
    die "no verification image in web posting row";
  }
  # print out image data to stdout.
  print $cgi->header("image/jpeg");
  print $internal_map->{"verification_image_jpg"};
}


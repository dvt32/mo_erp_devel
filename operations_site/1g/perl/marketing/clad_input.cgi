#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Storable qw(freeze nfreeze thaw);
use LWP::UserAgent;
# need to explicitly 'use HTTP::Cookies' because deserialized LWP::UserAgent does not realize it needs to be loaded.
use HTTP::Cookies;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use get_web_root_dir;
use read_file;
use get_cgi;
use get_database_handle;
use select_row;
use update_row;
use apply_xslt_output_xhtml;
use utc_datetime_to_epoch_time;
use timestamp_to_components;
use get_datetime_now;
use dump_to_file;
use global_config;

main_exception_wrapper(\&main);

sub main {
  my $instance_map = shift;
  my ($cgi, $dbh) = (get_cgi($instance_map), get_database_handle($instance_map, "main"));
  my $web_posting_id = $cgi->param("id");
  if (!$web_posting_id) {
    die "show_page: clad_missing_web_posting_id.html\n";
  }
  my $web_posting = select_row($dbh, "web_posting_tbl", $web_posting_id);
  if (!$web_posting) {
    die "show_page: clad_web_posting_not_found.html\n";
  }
  if ($web_posting->{"processed_dt"}) {
    die "show_page: clad_already_processed.html\n";
  }
  if ($web_posting->{"web_spider_start_dt"}) {
    my $web_spider_start_epoch_time = utc_datetime_to_epoch_time(timestamp_to_components($web_posting->{"web_spider_start_dt"}));
    my $now_epoch_time = utc_datetime_to_epoch_time(get_datetime_now());
    if (($now_epoch_time - $web_spider_start_epoch_time) < 60) {
      die "show_page: clad_currently_being_processed.html\n";
    }
  }
  my (@sql_column_list);
  push(@sql_column_list, ["web_spider_start_dt", ["now()"]]);
  update_row($dbh, "web_posting_tbl", \@sql_column_list, $web_posting_id);
  my $internal_map = thaw($web_posting->{"internal_map"});
  if ($internal_map->{"verification_image_jpg"}) {
    # already retrieved verification image, so ask user to read and enter the letters.
    # generate XML.
    my $generated_xml = "";
    $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<page>
  <web_posting_id>$web_posting_id</web_posting_id>
</page>
---END_BLOCK---;
    # apply XSLT stylesheet and output XHTML page.
    apply_xslt_output_xhtml($generated_xml, "clad_input.xslt", $cgi);
    return;
  }
  my $config_filename = $internal_map->{"config_filename"};
  if (!$config_filename) {
    die "you must specify the config filename.";
  }
  my $data_base_dir = get_web_root_dir() . "/resource/marketing/craigslist";
  if (!-e "$data_base_dir/$config_filename") {
    die "$config_filename does not exist.";
  }
  my $config_file_data = read_file("$data_base_dir/$config_filename");
  my $subdomain;
  if ($config_file_data =~ /^[ \t]*subdomain[ \t]*=[ \t]*(.+)$/m) {
    $subdomain = $1;
  } else {
    die "must specify subdomain in the config file";
  }
  if ($subdomain ne "office_newyork") {
    die "unknown subdomain: $subdomain";
  }
  my $login_email = "sales\@microoffice.us";
  my $login_password = global_config::lookup("clad_input_login_password");
  my $ask;
  my $sqft;
  my $posting_title;
  my $geographic_area;
  my $posting_body_filename;
  my $posting_body;
  my $xstreet0;
  my $xstreet1;
  my $city;
  my $state;
  my $neighborhood;
  my $neighborhood_id;
  if ($config_file_data =~ /^[ \t]*login_email[ \t]*=[ \t]*(.+)$/m) {
    $login_email = $1;
  }
  if ($config_file_data =~ /^[ \t]*login_password[ \t]*=[ \t]*(.+)$/m) {
    $login_password = $1;
  }
  if ($config_file_data =~ /^[ \t]*ask[ \t]*=[ \t]*(.+)$/m) {
    $ask = $1;
  } else {
    $ask = "";
  }
  if ($config_file_data =~ /^[ \t]*sqft[ \t]*=[ \t]*(.+)$/m) {
    $sqft = $1;
  } else {
    $sqft = "";
  }
  if ($config_file_data =~ /^[ \t]*posting_title[ \t]*=[ \t]*(.+)$/m) {
    $posting_title = $1;
  } else {
    die "must specify posting_title in the config file";
  }
  if ($config_file_data =~ /^[ \t]*geographic_area[ \t]*=[ \t]*(.+)$/m) {
    $geographic_area = $1;
  }
  if ($config_file_data =~ /^[ \t]*posting_body_filename[ \t]*=[ \t]*(.+)$/m) {
    $posting_body_filename = $1;
  } else {
    die "must specify posting_body_filename in the config file";
  }
  if ($config_file_data =~ /^[ \t]*xstreet0[ \t]*=[ \t]*(.+)$/m) {
    $xstreet0 = $1;
  } else {
    $xstreet0 = "";
  }
  if ($config_file_data =~ /^[ \t]*xstreet1[ \t]*=[ \t]*(.+)$/m) {
    $xstreet1 = $1;
  } else {
    $xstreet1 = "";
  }
  if ($config_file_data =~ /^[ \t]*city[ \t]*=[ \t]*(.+)$/m) {
    $city = $1;
  }
  if ($config_file_data =~ /^[ \t]*state[ \t]*=[ \t]*(.+)$/m) {
    $state = $1;
  } else {
    die "must specify state in the config file";
  }
  if ($config_file_data =~ /^[ \t]*neighborhood[ \t]*=[ \t]*(.+)$/m) {
    $neighborhood = $1;
  }
  if ($config_file_data =~ /^[ \t]*neighborhood_id[ \t]*=[ \t]*(.+)$/m) {
    $neighborhood_id = $1;
  }
  if (!-e "$data_base_dir/$posting_body_filename") {
    die "$posting_body_filename does not exist";
  }
  $posting_body = read_file("$data_base_dir/$posting_body_filename");
  my $user_agent = thaw($web_posting->{"user_agent"});
  my $url = "";
  my %param_map = ();
  my $response;
  my $response_content;
  $url = $internal_map->{"confirm_email_post_url"};
  $response = $user_agent->get($url);
  $response_content = $response->content();
  dump_to_file("/dev/shm/clad_input_1.html", $response_content);
  my $base_url = "https://post.craigslist.org";
  my $form_action;
  my $single_form_content;
  if ($response_content =~ m{(<form action="[^\"]+?" method="post">.+?</form>)}s) {
    $single_form_content = $1;
    $single_form_content =~ m{<form action="([^\"]+?)" method="post">};
    $form_action = $1;
  } else {
    die qq{no match for form on CAPTCHA page.};
  }
  my ($recaptcha_noscript_url, $recaptcha_public_key);
  if ($response_content =~ m{<noscript><iframe .*?src="(https://www.google.com/recaptcha/api/noscript\?k=[^\"]+?)".*?></iframe>}) {
    $recaptcha_noscript_url = $1;
    $recaptcha_noscript_url =~ m{^https://www.google.com/recaptcha/api/noscript\?k=(.+)$};
    $recaptcha_public_key = $1;
  } else {
    die qq{no match for recaptcha noscript iframe.};
  }
  my @input_tag_list = ();
  while ($single_form_content =~ m{(<input .*?>)}g) {
    push(@input_tag_list, $1);
  }
  my @hidden_input_list = ();
  for my $input_tag (@input_tag_list) {
    $input_tag =~ s/<input\s+//;
    $input_tag =~ s/\s*>//;
    $input_tag =~ s/\s*=\s*/=/g;
    my ($input_name, $input_value);
    my $is_hidden = 0;
    while ($input_tag =~ /(\w+)="([^\"\']*?)"/g) {
      my ($attrib_name, $attrib_value) = ($1, $2);
      $attrib_name = lc($attrib_name);
      if ($attrib_name eq "name") {
	$input_name = $attrib_value;
      } elsif ($attrib_name eq "value") {
	$attrib_value =~ s/&amp;/&/g;
	$input_value = $attrib_value;
      } elsif (($attrib_name eq "type") && (lc($attrib_value) eq "hidden")) {
	$is_hidden = 1;
      }
    }
    if ($is_hidden) {
      push(@hidden_input_list, [$input_name, $input_value]);
    }
  }
  my $input_name_submit_button;
  if ($response_content =~ m{<button type="submit" name="(.+?)" value="Continue" tabindex="1">Continue</button>}) {
    $input_name_submit_button = $1;
  } else {
    die qq{no match for input_name_submit_button.};
  }
  $response = $user_agent->get($recaptcha_noscript_url);
  $recaptcha_noscript_url = $response->request()->uri();
  $response_content = $response->content();
  my $recaptcha_challenge_field;
  if ($response_content =~ m{<input type="hidden" name="recaptcha_challenge_field" id="recaptcha_challenge_field" value="([^\"]+?)">}) {
    $recaptcha_challenge_field = $1;
  } else {
    die qq{no match for recaptcha challenge field on api-secure.recaptcha.net noscript page.};
  }
  my $recaptcha_image_url;
  if ($response_content =~ m{<center><img .*?src="([^\"]+?)"></center>}) {
    my $relative_url = $1;
    if ($relative_url !~ m{^/}) {
      $relative_url = "/" . $relative_url;
    }
    $recaptcha_noscript_url =~ m{^(https://.*)/};
    my $recaptcha_base_url = $1;
    $recaptcha_image_url = $recaptcha_base_url . $relative_url;
  } else {
    die qq{no match for recaptcha image on api-secure.recaptcha.net noscript page.};
  }
  $response = $user_agent->get($recaptcha_image_url);
  my $verification_image_jpg = $response->content();
  if ($form_action =~ m{https://}) {
    $url = $form_action;
  } else {
    $url = $base_url . $form_action;
  }
  %param_map =
    (
     $input_name_submit_button => "Continue",
     );
  for(my $i = 0; $i < scalar(@hidden_input_list); $i++) {
    if (exists($param_map{$hidden_input_list[$i]->[0]})) {
      if (ref($param_map{$hidden_input_list[$i]->[0]}) eq "ARRAY") {
	push(@{$param_map{$hidden_input_list[$i]->[0]}}, $hidden_input_list[$i]->[1]);
      } else {
	$param_map{$hidden_input_list[$i]->[0]} = [$param_map{$hidden_input_list[$i]->[0]}, $hidden_input_list[$i]->[1]];
      }
    } else {
      $param_map{$hidden_input_list[$i]->[0]} = $hidden_input_list[$i]->[1];
    }
  }
  %$internal_map =
    (
     "verification_image_jpg" => $verification_image_jpg,
     "recaptcha_noscript_url" => $recaptcha_noscript_url,
     "recaptcha_challenge_field" => $recaptcha_challenge_field,
     "subdomain" => $subdomain,
    );
  my (@sql_column_list) = ();
  push(@sql_column_list, ["user_agent", freeze($user_agent), "pg_bytea"]);
  push(@sql_column_list, ["url", $url]);
  push(@sql_column_list, ["param_map", freeze(\%param_map), "pg_bytea"]);
  push(@sql_column_list, ["internal_map", freeze($internal_map), "pg_bytea"]);
  update_row($dbh, "web_posting_tbl", \@sql_column_list, $web_posting_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<page>
  <web_posting_id>$web_posting_id</web_posting_id>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "clad_input.xslt", $cgi);

}

sub craigslist_entity_escape($) {
  my $string = shift;
  $string =~ s/\&/&amp;/g;
  $string =~ s/\</&lt;/g;
  $string =~ s/\>/&gt;/g;
  $string =~ s/\"/&quot;/g;
  return $string;
}

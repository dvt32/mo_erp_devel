<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          Micro Office Solutions :: Operations Center :: Craigslist Posting Verification
        </title>
        <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  You entered the wrong verification word. Your entry was:
	  <xsl:value-of select="verification_word"/>.
	  Please enter the word from the image below.
	</p>
	<img>
	  <xsl:attribute name="src">/marketing/clad_image.cgi?id=<xsl:value-of select="web_posting_id"/></xsl:attribute>
	</img>
        <form action="/1g/marketing/clad_result.cgi" method="post">
	  <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="web_posting_id"/></xsl:attribute>
	  </input>
	  <input type="text" name="verification_word" size="15"/>
	  <input type="submit" name="submitButton" value="Go"/>
	</form>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

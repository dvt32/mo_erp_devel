#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/lead_tracking/edit_call_source");
  # check if marketing call source id is specified and try to load marketing call source.
  my ($marketing_call_source_id, $marketing_call_source) = ($cgi->param("id"), undef);
  if ((!defined($marketing_call_source_id)) || ($marketing_call_source_id !~ /^\d+$/) || (!($marketing_call_source = select_row($dbh, "marketing_call_source_tbl", $marketing_call_source_id)))) {
    die "show_page: edit_call_source_not_found.html\n";
  }
  # populate information needed for display of page.
  my $line_number = $marketing_call_source->{"line_number"};
  my $start_datetime = $marketing_call_source->{"start_datetime"};
  my $end_datetime = $marketing_call_source->{"end_datetime"};
  my $name = HTML::Entities::encode($marketing_call_source->{"name"});
  my $unit_cost = HTML::Entities::encode(defined($marketing_call_source->{"unit_cost"}) ? $marketing_call_source->{"unit_cost"} : "");
  my $unit_name = HTML::Entities::encode($marketing_call_source->{"unit_name"} || "");
  my $cost_per_month = HTML::Entities::encode(defined($marketing_call_source->{"cost_per_month"}) ? $marketing_call_source->{"cost_per_month"} : "");
  my $notes = HTML::Entities::encode($marketing_call_source->{"notes"} || "");
  my $active = $marketing_call_source->{"active"} ? 1 : 0;
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <marketing_call_source>
    <id>$marketing_call_source_id</id>
    <line_number>$line_number</line_number>
    <start_datetime>$start_datetime</start_datetime>
    <end_datetime>$end_datetime</end_datetime>
    <name>$name</name>
    <unit_cost>$unit_cost</unit_cost>
    <unit_name>$unit_name</unit_name>
    <cost_per_month>$cost_per_month</cost_per_month>
    <notes>$notes</notes>
    <active>$active</active>
  </marketing_call_source>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_call_source_input.xslt", $cgi);
}

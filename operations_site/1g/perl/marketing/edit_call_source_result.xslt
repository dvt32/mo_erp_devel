<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Marketing Call Source
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Marketing Call Source
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_call_sources.cgi">Call Sources</a>
	  |
	  <a href="/1g/marketing/find_cdr_input.cgi">Call Details</a>
	</p>
	<xsl:apply-templates select="marketing_call_source"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="marketing_call_source">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following marketing call source:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new marketing call source:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Line Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="line_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Start:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="substring(start_datetime, 1, 10)"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  End:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="substring(end_datetime, 1, 10)"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Unit Cost:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="unit_cost"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Unit Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="unit_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Cost per Month:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cost_per_month"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Is Active:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="active = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

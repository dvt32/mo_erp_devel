#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/lead_tracking/edit_call_source");
  my $marketing_call_id = $cgi->param("id");
  my $marketing_call;
  if ((!$marketing_call_id) || (!($marketing_call = select_row($dbh, "marketing_call_tbl", $marketing_call_id)))) {
    die "show_page: edit_cdr_not_found.html\n";
  }
  # populate information for display of page.
  my $call_datetime = $marketing_call->{"call_datetime"};
  my $line_number = $marketing_call->{"line_number"};
  my $callback_number = $marketing_call->{"callback_number"};
  my $minutes = $marketing_call->{"minutes"};
  my $note = HTML::Entities::encode($marketing_call->{"note"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <marketing_call>
    <id>$marketing_call_id</id>
    <call_datetime>$call_datetime</call_datetime>
    <line_number>$line_number</line_number>
    <callback_number>$callback_number</callback_number>
    <minutes>$minutes</minutes>
    <note>$note</note>
  </marketing_call>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_cdr_input.xslt", $cgi);
}

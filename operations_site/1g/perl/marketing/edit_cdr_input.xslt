<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Marketing Call Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_call_sources.cgi">Call Sources</a>
	  |
	  <a href="/1g/marketing/find_cdr_input.cgi">Call Details</a>
	</p>
	<xsl:apply-templates select="marketing_call"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="marketing_call">
    <table>
      <tr><td>
	<form action="/1g/marketing/edit_cdr_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Marketing Call Details
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Call Datetime:
		</td>
		<td>
		  <xsl:value-of select="substring(call_datetime, 1, 16)"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Line Number:
		</td>
		<td>
		  <xsl:value-of select="line_number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Callback Number:
		</td>
		<td>
		  <xsl:value-of select="callback_number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Minutes:
		</td>
		<td>
		  <xsl:value-of select="minutes"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/lead_tracking/edit_call_source");
  # check if marketing call id is specified and try to load marketing call.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my ($marketing_call_id, $marketing_call) = ($cgi->param("id"), undef);
  if ((!$marketing_call_id) || ($marketing_call_id !~ /^\d+$/) || (!($marketing_call = select_row($dbh, "marketing_call_tbl", $marketing_call_id)))) {
    die "show_page: edit_cdr_not_found.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || undef;
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "marketing_call_tbl", \@sql_column_list, $marketing_call_id);
  # populate information needed for display of page.
  my $call_datetime = $marketing_call->{"call_datetime"};
  my $line_number = $marketing_call->{"line_number"};
  my $callback_number = $marketing_call->{"callback_number"};
  my $minutes = $marketing_call->{"minutes"};
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <marketing_call>
    <call_datetime>$call_datetime</call_datetime>
    <line_number>$line_number</line_number>
    <callback_number>$callback_number</callback_number>
    <minutes>$minutes</minutes>
    <note>$note</note>
  </marketing_call>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_cdr_result.xslt", $cgi);
}

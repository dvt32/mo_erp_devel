<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Marketing Call Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_call_sources.cgi">Call Sources</a>
	  |
	  <a href="/1g/marketing/find_cdr_input.cgi">Call Details</a>
	</p>
	<xsl:apply-templates select="marketing_call"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="marketing_call">
    <p>
      You successfully updated the following marketing call details:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Call Datetime:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="call_datetime"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Line Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="line_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Callback Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="callback_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Minutes:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="minutes"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use get_row_list;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if inbound link id is specified and try to load inbound link.
  my ($inbound_link_id, $inbound_link) = ($cgi->param("id"), undef);
  if ((!defined($inbound_link_id)) || ($inbound_link_id !~ /^\d+$/) || (!($inbound_link = select_row($dbh, "inbound_link_tbl", $inbound_link_id)))) {
    die "show_page: edit_inbound_link_not_found.html\n";
  }
  # load parent web page.
  my $web_page_id = $inbound_link->{"web_page_id"};
  my $web_page = select_row($dbh, "web_page_tbl", $web_page_id);
  if (!$web_page) {
    die "show_page: edit_inbound_link_no_web_page.html\n";
  }
  my $owned_domain_name = select_row($dbh, "owned_domain_name_tbl", $web_page->{"owned_domain_name_id"});
  my $domain_name = $owned_domain_name->{"domain_name"};
  # load keywords.
  my $sql_query = "select * from ss_web_page_x_keyword_tbl where parent_id = ? order by element_value";
  my @sql_param_list = ([$web_page_id, SQL_INTEGER]);
  my (@keyword_row_list);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@keyword_row_list);
  my @keyword_list = map {$_->{"element_value"}} @keyword_row_list;
  # populate information needed for display of page.
  my $url_extra_param = HTML::Entities::encode($inbound_link->{"url_extra_param"} || "");
  my $referrer_url = HTML::Entities::encode($inbound_link->{"referrer_url"});
  my $link_text = HTML::Entities::encode($inbound_link->{"link_text"});
  my $note = HTML::Entities::encode($inbound_link->{"note"} || "");
  $domain_name = HTML::Entities::encode($domain_name);
  my $domain_prefix = HTML::Entities::encode($web_page->{"domain_prefix"} || "");
  my $uri_scheme = HTML::Entities::encode($web_page->{"uri_scheme"});
  my $uri_path = HTML::Entities::encode($web_page->{"uri_path"});
  my $web_page_note = HTML::Entities::encode($web_page->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <web_page>
    <domain_name>$domain_name</domain_name>
    <domain_prefix>$domain_prefix</domain_prefix>
    <uri_scheme>$uri_scheme</uri_scheme>
    <uri_path>$uri_path</uri_path>
    <note>$web_page_note</note>
    <keyword_list>
---END_BLOCK---;
  for my $keyword (@keyword_list) {
    my $encoded_keyword = HTML::Entities::encode($keyword);
    $generated_xml .= <<"---END_BLOCK---;";
      <keyword>$encoded_keyword</keyword>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </keyword_list>
  </web_page>
  <inbound_link>
    <id>$inbound_link_id</id>
    <web_page_id>$web_page_id</web_page_id>
    <url_extra_param>$url_extra_param</url_extra_param>
    <referrer_url>$referrer_url</referrer_url>
    <link_text>$link_text</link_text>
    <note>$note</note>
  </inbound_link>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_inbound_link_input.xslt", $cgi);
}

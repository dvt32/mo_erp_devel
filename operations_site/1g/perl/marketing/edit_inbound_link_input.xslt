<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Inbound Link
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  (<a href="/1g/marketing/show_rankings_keywords.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_owned.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_watched.cgi">Rankings</a>)
	</p>
	<xsl:apply-templates select="web_page"/>
	<xsl:apply-templates select="inbound_link"/>
	<p>
	  Example: Let us suppose that on the www.manhattancc.org web site, there is a link on the page
	  http://www.manhattancc.org/directory/office_space.asp?id=145. The link says "Micro Office - office space in New York"
	  and points to http://www.microoffice.com/manhattan_chamber.html?ref=biz_directory. This example assumes that you are
	  editing the web page whose URI scheme is http, domain prefix is www, domain name is microoffice.com, and URI path is
	  /manhattan_chamber.html.
	  <ul>
	    <li>http://www.manhattancc.org/directory/office_space.asp?id=145 is the referrer URL</li>
	    <li>"Micro Office - office space in New York" is the link text (no surrounding quotes)</li>
	    <li>?ref=biz_directory is the URL extra parameters field</li>
	  </ul>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="web_page">
    <xsl:text>The link target is </xsl:text>
    <xsl:value-of select="uri_scheme"/><xsl:text>://</xsl:text>
    <xsl:if test="domain_prefix != ''">
      <xsl:value-of select="domain_prefix"/><xsl:text>.</xsl:text>
    </xsl:if>
    <xsl:value-of select="domain_name"/>
    <xsl:value-of select="uri_path"/>.
    <p>
      Keywords are:
      <ul>
	<xsl:apply-templates select="keyword_list/keyword"/>
      </ul>
    </p>
  </xsl:template>

  <xsl:template match="keyword">
    <li><xsl:value-of select="."/></li>
  </xsl:template>

  <xsl:template match="inbound_link">
    <table>
      <tr><td>
	<form action="/1g/marketing/edit_inbound_link_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Inbound Link
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (for help, see example below)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Referrer URL:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">referrer_url</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="referrer_url"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Link Text:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">link_text</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="link_text"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URL Extra Parameters:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">url_extra_param</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="url_extra_param"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

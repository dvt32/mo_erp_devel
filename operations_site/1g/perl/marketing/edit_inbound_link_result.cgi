#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if inbound link id is specified and try to load inbound link.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my ($inbound_link_id, $inbound_link) = ($cgi->param("id"), undef);
  if ((!$inbound_link_id) || ($inbound_link_id !~ /^\d+$/) || (!($inbound_link = select_row($dbh, "inbound_link_tbl", $inbound_link_id)))) {
    die "show_page: edit_inbound_link_not_found.html\n";
  }
  my $referrer_url = $cgi->param("referrer_url");
  if ((!$referrer_url) || ($referrer_url !~ m{^http(?:s?)://})) {
    die "show_page: edit_inbound_link_bad_referrer_url.html\n";
  }
  my $link_text = Encode::decode($cgi_input_charset, $cgi->param("link_text"));
  if (!$link_text) {
    die "show_page: edit_inbound_link_bad_link_text.html\n";
  }
  my $url_extra_param = $cgi->param("url_extra_param");
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["url_extra_param", $url_extra_param]) if ($url_extra_param);
  push(@sql_column_list, ["referrer_url", $referrer_url]);
  push(@sql_column_list, ["link_text", Encode::encode($db_charset, $link_text)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]) if ($note);
  update_row($dbh, "inbound_link_tbl", \@sql_column_list, $inbound_link_id);
  # populate information needed for display of page.
  my $web_page_id = $inbound_link->{"web_page_id"};
  $url_extra_param = HTML::Entities::encode($url_extra_param);
  $referrer_url = HTML::Entities::encode($referrer_url);
  $link_text = HTML::Entities::encode($link_text);
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <inbound_link>
    <web_page_id>$web_page_id</web_page_id>
    <url_extra_param>$url_extra_param</url_extra_param>
    <referrer_url>$referrer_url</referrer_url>
    <link_text>$link_text</link_text>
    <note>$note</note>
  </inbound_link>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_inbound_link_result.xslt", $cgi);
}

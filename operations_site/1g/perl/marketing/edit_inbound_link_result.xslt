<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Inbound Link
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Inbound Link
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  (<a href="/1g/marketing/show_rankings_keywords.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_owned.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_watched.cgi">Rankings</a>)
	</p>
	<xsl:apply-templates select="inbound_link"/>
	<p>
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      If desired, enter a new inbound link now:
	    </xsl:when>
	    <xsl:otherwise>
	      If desired, enter another new inbound link now:
	    </xsl:otherwise>
	  </xsl:choose>
	</p>
	<xsl:call-template name="new_inbound_link"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="inbound_link">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following inbound link:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully entered the following new inbound link:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Referrer URL:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="referrer_url"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Link Text:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="link_text"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  URL Extra Parameters:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="url_extra_param"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="new_inbound_link">
    <table>
      <tr><td>
	<form action="/1g/marketing/new_inbound_link_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Inbound Link
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">web_page_id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="/page/inbound_link/web_page_id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (for help, see example below)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Referrer URL:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">referrer_url</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Link Text:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">link_text</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URL Extra Parameters:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">url_extra_param</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

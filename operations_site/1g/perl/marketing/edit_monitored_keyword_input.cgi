#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if monitored keyword id is specified and try to load monitored keyword.
  my ($monitored_keyword_id, $monitored_keyword) = ($cgi->param("id"), undef);
  if ((!defined($monitored_keyword_id)) || ($monitored_keyword_id !~ /^\d+$/) || (!($monitored_keyword = select_row($dbh, "monitored_keyword_tbl", $monitored_keyword_id)))) {
    die "show_page: edit_monitored_keyword_not_found.html\n";
  }
  # load search engines.
  my @search_engine_list;
  get_row_list($dbh, "select * from enum_search_engine_tbl where is_active order by label", undef, \@search_engine_list);
  # populate information needed for display of page.
  my $keyword = HTML::Entities::encode($monitored_keyword->{"keyword"});
  my $search_engine = HTML::Entities::encode($monitored_keyword->{"search_engine"});
  my $search_language = HTML::Entities::encode($monitored_keyword->{"search_language"} || "");
  my $note = HTML::Entities::encode($monitored_keyword->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <monitored_keyword>
    <id>$monitored_keyword_id</id>
    <keyword>$keyword</keyword>
    <search_engine>$search_engine</search_engine>
    <search_language>$search_language</search_language>
    <note>$note</note>
  </monitored_keyword>
  <search_engine_list>
---END_BLOCK---;
  for my $search_engine (@search_engine_list) {
    my $label = HTML::Entities::encode($search_engine->{"label"});
    my $value = HTML::Entities::encode($search_engine->{"value"});
    $generated_xml .= <<"---END_BLOCK---;";
    <search_engine>
      <label>$label</label>
      <value>$value</value>
    </search_engine>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </search_engine_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_monitored_keyword_input.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use strip_outside_whitespace;
use select_row_with_query;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if monitored keyword id is specified and try to load monitored keyword.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my ($monitored_keyword_id, $monitored_keyword) = ($cgi->param("id"), undef);
  if ((!$monitored_keyword_id) || ($monitored_keyword_id !~ /^\d+$/) || (!($monitored_keyword = select_row($dbh, "monitored_keyword_tbl", $monitored_keyword_id)))) {
    die "show_page: edit_monitored_keyword_not_found.html\n";
  }
  # check that keyword is not blank.
  my $keyword = lc(strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("keyword"))));
  $keyword =~ s/\s+/ /g; # collapse internal whitespace of multiple spaces to one space
  if ($keyword !~ /\w/) {
    die "show_page: edit_monitored_keyword_missing_keyword.html\n";
  }
  # check that search engine is not blank.
  my $search_engine = Encode::decode($cgi_input_charset, $cgi->param("search_engine"));
  if ($search_engine !~ /\w/) {
    die "show_page: edit_monitored_keyword_missing_search_engine.html\n";
  }
  my $search_language = Encode::decode($cgi_input_charset, $cgi->param("search_language"));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # check that another monitored keyword does not have the same (keyword, search engine) pair.
  my $sql_query = "select * from monitored_keyword_tbl where keyword = ? and search_engine = ? and id <> ?";
  my @sql_param_list = ($keyword, $search_engine, [$monitored_keyword_id, SQL_INTEGER]);
  if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    die "show_page: edit_monitored_keyword_duplicate_keyword.html\n";
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["keyword", Encode::encode($db_charset, $keyword)]);
  push(@sql_column_list, ["search_engine", Encode::encode($db_charset, $search_engine)]);
  push(@sql_column_list, ["search_language", Encode::encode($db_charset, $search_language) || undef]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  update_row($dbh, "monitored_keyword_tbl", \@sql_column_list, $monitored_keyword_id);
  # populate information needed for display of page.
  $keyword = HTML::Entities::encode($keyword);
  $search_engine = HTML::Entities::encode($search_engine);
  $search_language = HTML::Entities::encode($search_language);
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <monitored_keyword>
    <id>$monitored_keyword_id</id>
    <keyword>$keyword</keyword>
    <search_engine>$search_engine</search_engine>
    <search_language>$search_language</search_language>
    <note>$note</note>
  </monitored_keyword>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_monitored_keyword_result.xslt", $cgi);
}

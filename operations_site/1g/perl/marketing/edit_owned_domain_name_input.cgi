#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if owned domain name id is specified and try to load owned domain name.
  my ($owned_domain_name_id, $owned_domain_name) = ($cgi->param("id"), undef);
  if ((!defined($owned_domain_name_id)) || ($owned_domain_name_id !~ /^\d+$/) || (!($owned_domain_name = select_row($dbh, "owned_domain_name_tbl", $owned_domain_name_id)))) {
    die "show_page: edit_owned_domain_name_not_found.html\n";
  }
  # populate information needed for display of page.
  my $domain_name = HTML::Entities::encode($owned_domain_name->{"domain_name"});
  my $note = HTML::Entities::encode($owned_domain_name->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <owned_domain_name>
    <id>$owned_domain_name_id</id>
    <domain_name>$domain_name</domain_name>
    <note>$note</note>
  </owned_domain_name>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_owned_domain_name_input.xslt", $cgi);
}

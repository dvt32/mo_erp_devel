#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if watched domain name id is specified and try to load watched domain name.
  my ($watched_domain_name_id, $watched_domain_name) = ($cgi->param("id"), undef);
  if ((!defined($watched_domain_name_id)) || ($watched_domain_name_id !~ /^\d+$/) || (!($watched_domain_name = select_row($dbh, "watched_domain_name_tbl", $watched_domain_name_id)))) {
    die "show_page: edit_watched_domain_name_not_found.html\n";
  }
  # populate information needed for display of page.
  my $domain_name = HTML::Entities::encode($watched_domain_name->{"domain_name"});
  my $expire_date = HTML::Entities::encode($watched_domain_name->{"expire_date"} || "");
  my $owner_name = HTML::Entities::encode($watched_domain_name->{"owner_name"} || "");
  my $owner_phone_number = HTML::Entities::encode($watched_domain_name->{"owner_phone_number"} || "");
  my $owner_email_address = HTML::Entities::encode($watched_domain_name->{"owner_email_address"} || "");
  my $note = HTML::Entities::encode($watched_domain_name->{"note"} || "");
  my $show_rank = $watched_domain_name->{"show_rank"} ? 1 : 0;
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <watched_domain_name>
    <id>$watched_domain_name_id</id>
    <domain_name>$domain_name</domain_name>
    <expire_date>$expire_date</expire_date>
    <owner_name>$owner_name</owner_name>
    <owner_phone_number>$owner_phone_number</owner_phone_number>
    <owner_email_address>$owner_email_address</owner_email_address>
    <note>$note</note>
    <show_rank>$show_rank</show_rank>
  </watched_domain_name>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_watched_domain_name_input.xslt", $cgi);
}

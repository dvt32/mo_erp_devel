#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if watched domain name id is specified and try to load watched domain name.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my ($watched_domain_name_id, $watched_domain_name) = ($cgi->param("id"), undef);
  if ((!$watched_domain_name_id) || ($watched_domain_name_id !~ /^\d+$/) || (!($watched_domain_name = select_row($dbh, "watched_domain_name_tbl", $watched_domain_name_id)))) {
    die "show_page: edit_watched_domain_name_not_found.html\n";
  }
  # check that domain name is not blank.
  my $domain_name = lc(Encode::decode($cgi_input_charset, $cgi->param("domain_name")));
  if ($domain_name !~ /\w/) {
    die "show_page: edit_watched_domain_name_missing_domain_name.html\n";
  }
  # check format of expire date.
  my $expire_date = $cgi->param("expire_date");
  if (($expire_date) && ($expire_date !~ /^\d{4}-\d\d-\d\d$/)) {
    die "show_page: edit_watched_domain_name_bad_expire_date.html\n";
  }
  my $owner_name = Encode::decode($cgi_input_charset, $cgi->param("owner_name"));
  my $owner_phone_number = Encode::decode($cgi_input_charset, $cgi->param("owner_phone_number"));
  my $owner_email_address = Encode::decode($cgi_input_charset, $cgi->param("owner_email_address"));
  my $show_rank = $cgi->param("show_rank");
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["domain_name", Encode::encode($db_charset, $domain_name)]);
  push(@sql_column_list, ["expire_date", $expire_date || undef]);
  push(@sql_column_list, ["owner_name", Encode::encode($db_charset, $owner_name) || undef]);
  push(@sql_column_list, ["owner_phone_number", Encode::encode($db_charset, $owner_phone_number) || undef]);
  push(@sql_column_list, ["owner_email_address", Encode::encode($db_charset, $owner_email_address) || undef]);
  push(@sql_column_list, ["show_rank", $show_rank, SQL_BOOLEAN]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  update_row($dbh, "watched_domain_name_tbl", \@sql_column_list, $watched_domain_name_id);
  # populate information needed for display of page.
  $domain_name = HTML::Entities::encode($domain_name);
  $expire_date = HTML::Entities::encode($expire_date || "");
  $owner_name = HTML::Entities::encode($owner_name || "");
  $owner_phone_number = HTML::Entities::encode($owner_phone_number || "");
  $owner_email_address = HTML::Entities::encode($owner_email_address || "");
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <watched_domain_name>
    <id>$watched_domain_name_id</id>
    <domain_name>$domain_name</domain_name>
    <expire_date>$expire_date</expire_date>
    <owner_name>$owner_name</owner_name>
    <owner_phone_number>$owner_phone_number</owner_phone_number>
    <owner_email_address>$owner_email_address</owner_email_address>
    <note>$note</note>
    <show_rank>$show_rank</show_rank>
  </watched_domain_name>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_watched_domain_name_result.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use select_row;
use get_row_list;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # check if web page id is specified and try to load web page.
  my ($web_page_id, $web_page) = ($cgi->param("id"), undef);
  if ((!defined($web_page_id)) || ($web_page_id !~ /^\d+$/) || (!($web_page = select_row($dbh, "web_page_tbl", $web_page_id)))) {
    die "show_page: edit_web_page_not_found.html\n";
  }
  # load keywords.
  my $sql_query = "select * from ss_web_page_x_keyword_tbl where parent_id = ? order by element_value";
  my @sql_param_list = ([$web_page_id, SQL_INTEGER]);
  my (@keyword_row_list);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@keyword_row_list);
  my @keyword_list = map {$_->{"element_value"}} @keyword_row_list;
  # load inbound links.
  my $sql_query = "select * from inbound_link_tbl where web_page_id = ? order by referrer_url";
  my @sql_param_list = ([$web_page_id, SQL_INTEGER]);
  my (@inbound_link_list);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@inbound_link_list);
  # load owned domain names.
  $sql_query = "select * from owned_domain_name_tbl order by domain_name";
  my (%owned_domain_name_by_id, @owned_domain_name_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%owned_domain_name_by_id, \@owned_domain_name_id_list);
  # populate information needed for display of page.
  my $owned_domain_name_id = $web_page->{"owned_domain_name_id"};
  my $domain_name = HTML::Entities::encode($owned_domain_name_by_id{$owned_domain_name_id}->{"domain_name"});
  my $uri_scheme = HTML::Entities::encode($web_page->{"uri_scheme"});
  my $domain_prefix = HTML::Entities::encode($web_page->{"domain_prefix"} || "");
  my $uri_path = HTML::Entities::encode($web_page->{"uri_path"});
  my $note = HTML::Entities::encode($web_page->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <web_page>
    <id>$web_page_id</id>
    <owned_domain_name_id>$owned_domain_name_id</owned_domain_name_id>
    <domain_name>$domain_name</domain_name>
    <uri_scheme>$uri_scheme</uri_scheme>
    <domain_prefix>$domain_prefix</domain_prefix>
    <uri_path>$uri_path</uri_path>
    <note>$note</note>
    <keyword_list>
---END_BLOCK---;
  for my $keyword (@keyword_list) {
    my $encoded_keyword = HTML::Entities::encode($keyword);
    $generated_xml .= <<"---END_BLOCK---;";
      <keyword>$encoded_keyword</keyword>
---END_BLOCK---;
  }
  for(my $i = 0; $i < 5; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
      <keyword/>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </keyword_list>
    <inbound_link_list>
---END_BLOCK---;
  for my $inbound_link (@inbound_link_list) {
    my $inbound_link_id = $inbound_link->{"id"};
    my $url_extra_param = HTML::Entities::encode($inbound_link->{"url_extra_param"} || "");
    my $referrer_url = HTML::Entities::encode($inbound_link->{"referrer_url"});
    my $link_text = HTML::Entities::encode($inbound_link->{"link_text"});
    my $note = HTML::Entities::encode($inbound_link->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
      <inbound_link>
        <id>$inbound_link_id</id>
        <url_extra_param>$url_extra_param</url_extra_param>
        <referrer_url>$referrer_url</referrer_url>
        <link_text>$link_text</link_text>
        <note>$note</note>
      </inbound_link>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </inbound_link_list>
  </web_page>
  <uri_scheme_list>
    <uri_scheme>http</uri_scheme>
    <uri_scheme>https</uri_scheme>
  </uri_scheme_list>
  <owned_domain_name_list>
---END_BLOCK---;
  for my $owned_domain_name_id (@owned_domain_name_id_list) {
    my $owned_domain_name = $owned_domain_name_by_id{$owned_domain_name_id};
    my $domain_name = HTML::Entities::encode($owned_domain_name->{"domain_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <owned_domain_name>
      <id>$owned_domain_name_id</id>
      <domain_name>$domain_name</domain_name>
    </owned_domain_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </owned_domain_name_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_web_page_input.xslt", $cgi);
}

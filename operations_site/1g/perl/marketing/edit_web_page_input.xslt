<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Web Page
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  (<a href="/1g/marketing/show_rankings_keywords.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_owned.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_watched.cgi">Rankings</a>)
	</p>
	<xsl:apply-templates select="web_page"/>
	<p>
	  Example: The URL http://www.microoffice.com/office_space_nyc.html can be broken down as:
	  <ul>
	    <li>http is the URI scheme</li>
	    <li>www is the domain prefix (no trailing dot)</li>
	    <li>microoffice.com is the domain name</li>
	    <li>/office_space_nyc.html is the URI path (include the leading slash)</li>
	  </ul>
	</p>
	<xsl:apply-templates select="web_page/inbound_link_list"/>
	<xsl:call-template name="new_inbound_link"/>
	<p>
	  Example: Let us suppose that on the www.manhattancc.org web site, there is a link on the page
	  http://www.manhattancc.org/directory/office_space.asp?id=145. The link says "Micro Office - office space in New York"
	  and points to http://www.microoffice.com/manhattan_chamber.html?ref=biz_directory. This example assumes that you are
	  editing the web page whose URI scheme is http, domain prefix is www, domain name is microoffice.com, and URI path is
	  /manhattan_chamber.html.
	  <ul>
	    <li>http://www.manhattancc.org/directory/office_space.asp?id=145 is the referrer URL</li>
	    <li>"Micro Office - office space in New York" is the link text (no surrounding quotes)</li>
	    <li>?ref=biz_directory is the URL extra parameters field</li>
	  </ul>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="web_page">
    <table>
      <tr><td>
	<form action="/1g/marketing/edit_web_page_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Web Page
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">num_keywords</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="count(keyword_list/keyword)"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (for help, see example below)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URI Scheme:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>uri_scheme</xsl:text>
		    </xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/uri_scheme_list/uri_scheme">
		      <option>
			<xsl:if test="/page/web_page/uri_scheme = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Domain Prefix:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">domain_prefix</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="domain_prefix"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Domain Name:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>owned_domain_name_id</xsl:text>
		    </xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/owned_domain_name_list/owned_domain_name">
		      <option>
			<xsl:if test="/page/web_page/owned_domain_name_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="domain_name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URI Path:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">uri_path</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="uri_path"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; " colspan="2">
		  <hr/>
		</td>
	      </tr>
	      <xsl:apply-templates select="keyword_list/keyword"/>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (to enter more keywords, save first)
		  <br/>
		  (to delete keywords, clear entries)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="keyword">
    <tr>
      <td style="padding: 4px; ">
	Keyword <xsl:value-of select="position()"/>:
      </td>
      <td>
	<input>
	  <xsl:attribute name="type">text</xsl:attribute>
	  <xsl:attribute name="name">keyword_<xsl:value-of select="position()"/></xsl:attribute>
	  <xsl:attribute name="size">40</xsl:attribute>
	  <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
	</input>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="inbound_link_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td class="standard_table_title" colspan="11">
	  Inbound Links
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>ID</td>
	<td class="standard_table_spacer_column"></td>
	<td>Referrer URL</td>
	<td class="standard_table_spacer_column"></td>
	<td>Link Text</td>
	<td class="standard_table_spacer_column"></td>
	<td>Extra Params</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="inbound_link"/>
    </table>
  </xsl:template>

  <xsl:template match="inbound_link">
    <tr>
      <xsl:attribute name="valign">top</xsl:attribute>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="href">/1g/marketing/edit_inbound_link_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="id"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="referrer_url"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="link_text"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="url_extra_param"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="note"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template name="new_inbound_link">
    <table>
      <tr><td>
	<form action="/1g/marketing/new_inbound_link_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Inbound Link
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">web_page_id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="/page/web_page/id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (for help, see example below)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Referrer URL:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">referrer_url</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Link Text:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">link_text</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URL Extra Parameters:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">url_extra_param</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Web Page
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Web Page
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  (<a href="/1g/marketing/show_rankings_keywords.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_owned.cgi">Rankings</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (<a href="/1g/marketing/show_rankings_watched.cgi">Rankings</a>)
	</p>
	<xsl:apply-templates select="web_page"/>
	<p>
	  If desired, you can enter a new inbound link to the web page above now:
	</p>
	<xsl:call-template name="new_inbound_link"/>
	<p>
	  Example: Let us suppose that on the www.manhattancc.org web site, there is a link on the page
	  http://www.manhattancc.org/directory/office_space.asp?id=145. The link says "Micro Office - office space in New York"
	  and points to http://www.microoffice.com/manhattan_chamber.html?ref=biz_directory. This example assumes that you are
	  editing the web page whose URI scheme is http, domain prefix is www, domain name is microoffice.com, and URI path is
	  /manhattan_chamber.html.
	  <ul>
	    <li>http://www.manhattancc.org/directory/office_space.asp?id=145 is the referrer URL</li>
	    <li>"Micro Office - office space in New York" is the link text (no surrounding quotes)</li>
	    <li>?ref=biz_directory is the URL extra parameters field</li>
	  </ul>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="web_page">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following web page:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully entered the following new web page:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  URI Scheme:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="uri_scheme"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Domain Prefix:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="domain_prefix"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Domain Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="domain_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  URI Path:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="uri_path"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <hr/>
	</td>
      </tr>
      <xsl:apply-templates select="keyword_list/keyword"/>
    </table>
  </xsl:template>

  <xsl:template match="keyword">
    <tr>
      <td class="map_table_key">
	Keyword:
      </td>
      <td class="map_table_value">
	<xsl:value-of select="."/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="new_inbound_link">
    <table>
      <tr><td>
	<form action="/1g/marketing/new_inbound_link_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Inbound Link
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">web_page_id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="/page/web_page/id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (for help, see example below)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Referrer URL:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">referrer_url</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Link Text:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">link_text</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URL Extra Parameters:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">url_extra_param</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">80</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

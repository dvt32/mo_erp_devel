#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row_with_query;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/lead_tracking");
  my $sql_query = "select count(*) as row_count from marketing_call_tbl";
  my $num_call_rows = select_row_with_query($dbh, $sql_query, undef)->{"row_count"};
  $sql_query = "select max(call_datetime) as call_datetime from marketing_call_tbl";
  my $latest_call_datetime = select_row_with_query($dbh, $sql_query, undef)->{"call_datetime"};
  # populate information for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <num_call_rows>$num_call_rows</num_call_rows>
  <latest_call_datetime>$latest_call_datetime</latest_call_datetime>
  <quick_search_form/>
  <advanced_search_form/>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "find_cdr_input.xslt", $cgi);
}

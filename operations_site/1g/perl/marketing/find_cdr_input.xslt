<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Call Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_call_sources.cgi">Call Sources</a>
	</p>
	<p>
	  There are <xsl:value-of select="num_call_rows"/> calls in the marketing call database.
	  The latest call stored in the database took place <xsl:value-of select="substring(latest_call_datetime, 1, 16)"/>.
	</p>
	<xsl:apply-templates select="quick_search_form"/>
	<xsl:apply-templates select="advanced_search_form"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="quick_search_form">
    <table>
      <tr><td>
	<form action="/1g/marketing/find_cdr_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Quick Search
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  How Many Weeks:
		</td>
		<td>
                  <select name="num_weeks">
		    <option value="">[---]</option>
		    <option>4</option>
		    <option>8</option>
		    <option>13</option>
		    <option>26</option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Search"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="advanced_search_form">
    <table>
      <tr><td>
	<form action="/1g/marketing/find_cdr_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Advanced Search
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Start Date (yyyy-mm-dd):
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">start_datetime</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  End Date (yyyy-mm-dd):
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">end_datetime</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Search"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

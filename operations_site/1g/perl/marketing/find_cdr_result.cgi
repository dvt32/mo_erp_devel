#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;
use lead_pad;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/lead_tracking");
  my $start_datetime = $cgi->param("start_datetime");
  my $end_datetime = $cgi->param("end_datetime");
  my $num_weeks = $cgi->param("num_weeks");
  my $sql_query;
  my @sql_param_list;
  if (($start_datetime) || ($end_datetime)) {
    if (($start_datetime =~ /^\d{4}-\d\d-\d\d$/) && ($end_datetime =~ /^\d{4}-\d\d-\d\d$/)) {
      $sql_query = "select id, call_datetime, line_number from marketing_call_tbl where call_datetime >= ? and call_datetime <= ?";
      @sql_param_list = ("$start_datetime 00:00:00", "$end_datetime 23:59:59");
    } else {
      die "show_page: find_cdr_bad_start_end_date.html\n";
    }
  } elsif ($num_weeks) {
    $sql_query = "select id, call_datetime, line_number from marketing_call_tbl where call_datetime >= now() - interval '$num_weeks weeks'";
    @sql_param_list = ();
  } else {
    die "show_page: find_cdr_no_query.html\n";
  }
  my (%marketing_call_by_id);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%marketing_call_by_id);
  $sql_query = "select * from marketing_call_source_tbl";
  my (%marketing_call_source_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%marketing_call_source_by_id);
  my (%marketing_call_source_list_by_line_number);
  for my $marketing_call_source (values(%marketing_call_source_by_id)) {
    push(@{$marketing_call_source_list_by_line_number{$marketing_call_source->{"line_number"}}}, $marketing_call_source);
  }
  my (%num_call_map_by_marketing_call_source_id, %num_calls_by_marketing_call_source_id);
  my (%num_uncategorized_call_map, $num_uncategorized_calls);
  my %num_total_by_month;
  my ($first_date, $last_date);
  for my $marketing_call (values(%marketing_call_by_id)) {
    my $line_number = $marketing_call->{"line_number"};
    my $call_datetime = $marketing_call->{"call_datetime"};
    $call_datetime =~ /^(\d{4}-\d\d)(-\d\d)/;
    my $call_date = $1 . $2;
    my $year_month = $1;
    if ((!$first_date) || ($call_date lt $first_date)) {
      $first_date = $call_date;
    }
    if ((!$last_date) || ($call_date gt $last_date)) {
      $last_date = $call_date;
    }
    my $marketing_call_source_list = $marketing_call_source_list_by_line_number{$marketing_call->{"line_number"}};
    if ($marketing_call_source_list) {
      my $marketing_call_source;
      for my $_marketing_call_source (@$marketing_call_source_list) {
	if (($_marketing_call_source->{"start_datetime"} le $call_datetime) && ($_marketing_call_source->{"end_datetime"} ge $call_datetime)) {
	  $marketing_call_source = $_marketing_call_source;
	  last;
	}
      }
      if ($marketing_call_source) {
	$num_call_map_by_marketing_call_source_id{$marketing_call_source->{"id"}}->{$year_month}++;
	$num_calls_by_marketing_call_source_id{$marketing_call_source->{"id"}}++;
	$num_total_by_month{$year_month}++;
      } else {
	$num_uncategorized_call_map{$year_month}++;
	$num_uncategorized_calls++;
	$num_total_by_month{$year_month}++;
      }
    } else {
      $num_uncategorized_call_map{$year_month}++;
      $num_uncategorized_calls++;
      $num_total_by_month{$year_month}++;
    }
  }
  my @marketing_call_source_id_list = sort {
    my $marketing_call_source_a = $marketing_call_source_by_id{$a};
    my $marketing_call_source_b = $marketing_call_source_by_id{$b};
    return lc($marketing_call_source_a->{"name"}) cmp lc($marketing_call_source_b->{"name"});
  } keys(%num_call_map_by_marketing_call_source_id);
  $first_date =~ /^(\d{4})-(\d\d)/;
  my ($start_year, $start_month) = ($1, $2);
  my $start_month_index = $start_year * 12 + $start_month;
  $last_date =~ /^(\d{4})-(\d\d)/;
  my ($end_year, $end_month) = ($1, $2);
  my $end_month_index = $end_year * 12 + $end_month;
  # populate information for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <call_grid>
    <column_heading_list>
      <first_date>$first_date</first_date>
      <last_date>$last_date</last_date>
---END_BLOCK---;
  for(my $month_index = $start_month_index; $month_index <= $end_month_index; $month_index++) {
    my $_year = int($month_index/12);
    my $_month = $month_index % 12;
    if ($_month == 0) {
      $_year--;
      $_month = 12;
    }
    my $_abbr_month = substr(get_month_name($_month), 0, 3);
    $generated_xml .= <<"---END_BLOCK---;";
      <column_heading>
        <month>$_month</month>
        <month_abbreviation>$_abbr_month</month_abbreviation>
        <year>$_year</year>
      </column_heading>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </column_heading_list>
---END_BLOCK---;
  for my $marketing_call_source_id (@marketing_call_source_id_list) {
    my $marketing_call_source = $marketing_call_source_by_id{$marketing_call_source_id};
    my $name = HTML::Entities::encode($marketing_call_source->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <marketing_source_row>
      <marketing_call_source_id>$marketing_call_source_id</marketing_call_source_id>
      <name>$name</name>
---END_BLOCK---;
    for(my $month_index = $start_month_index; $month_index <= $end_month_index; $month_index++) {
      my $_year = int($month_index/12);
      my $_month = lead_pad($month_index % 12, 2);
      if ($_month == 0) {
	$_year--;
	$_month = 12;
      }
      my $num_calls = $num_call_map_by_marketing_call_source_id{$marketing_call_source_id}->{$_year . "-" . $_month};
      if ($num_calls) {
	$generated_xml .= <<"---END_BLOCK---;";
      <num_calls>$num_calls</num_calls>
---END_BLOCK---;
      } else {
	$generated_xml .= <<"---END_BLOCK---;";
      <num_calls/>
---END_BLOCK---;
      }
    }
    if (my $num_calls_total = $num_calls_by_marketing_call_source_id{$marketing_call_source_id}) {
      $generated_xml .= <<"---END_BLOCK---;";
      <num_calls_total>$num_calls_total</num_calls_total>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
      <num_calls_total/>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
    </marketing_source_row>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    <marketing_source_row>
      <marketing_call_source_id>uncategorized</marketing_call_source_id>
      <name>Uncategorized</name>
---END_BLOCK---;
  for(my $month_index = $start_month_index; $month_index <= $end_month_index; $month_index++) {
    my $_year = int($month_index/12);
    my $_month = lead_pad($month_index % 12, 2);
    if ($_month == 0) {
      $_year--;
      $_month = 12;
    }
    my $num_calls = $num_uncategorized_call_map{$_year . "-" . $_month};
    if ($num_calls) {
      $generated_xml .= <<"---END_BLOCK---;";
      <num_calls>$num_calls</num_calls>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
      <num_calls/>
---END_BLOCK---;
    }
  }
  if ($num_uncategorized_calls) {
    $generated_xml .= <<"---END_BLOCK---;";
      <num_calls_total>$num_uncategorized_calls</num_calls_total>
---END_BLOCK---;
  } else {
    $generated_xml .= <<"---END_BLOCK---;";
      <num_calls_total/>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </marketing_source_row>
---END_BLOCK---;
  $generated_xml .= <<"---END_BLOCK---;";
    <summary_row>
---END_BLOCK---;
  for(my $month_index = $start_month_index; $month_index <= $end_month_index; $month_index++) {
    my $_year = int($month_index/12);
    my $_month = lead_pad($month_index % 12, 2);
    if ($_month == 0) {
      $_year--;
      $_month = 12;
    }
    my $num_calls = $num_total_by_month{$_year . "-" . $_month};
    if ($num_calls) {
      $generated_xml .= <<"---END_BLOCK---;";
      <summary_num_calls>$num_calls</summary_num_calls>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
      <summary_num_calls/>
---END_BLOCK---;
    }
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </summary_row>
---END_BLOCK---;
  $generated_xml .= <<"---END_BLOCK---;";
  </call_grid>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "find_cdr_result.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Call Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_call_sources.cgi">Call Sources</a>
	  |
	  <a href="/1g/marketing/find_cdr_input.cgi">Call Details</a>
	</p>
	<xsl:apply-templates select="call_grid"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="call_grid">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td class="standard_table_title">
	  <xsl:attribute name="colspan"><xsl:value-of select="2 * count(column_heading_list/column_heading) + 5"/></xsl:attribute>
	  Marketing Call Details
	</td>
      </tr>
      <xsl:apply-templates select="column_heading_list"/>
      <xsl:apply-templates select="marketing_source_row"/>
      <xsl:apply-templates select="summary_row"/>
    </table>
  </xsl:template>

  <xsl:template match="column_heading_list">
    <tr>
      <td class="standard_table_side_padding"></td>
      <td>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	  <tr>
	    <td><xsl:value-of select="first_date"/> to <xsl:value-of select="last_date"/></td>
	    <td align="right">Year</td>
	  </tr>
	</table>
      </td>
      <xsl:for-each select="column_heading">
	<xsl:if test="position() = 1 or year != preceding-sibling::column_heading[1]/year">
	  <xsl:variable name="_year" select="year"/>
	  <td class="standard_table_spacer_column"></td>
	  <td>
	    <xsl:attribute name="colspan"><xsl:value-of select="2 * count(../column_heading[year = $_year]) - 1"/></xsl:attribute>
	    <xsl:attribute name="style">text-align: center; background-color: lightblue;</xsl:attribute>
	    <xsl:value-of select="year"/>
	  </td>
	</xsl:if>
      </xsl:for-each>
      <td class="standard_table_spacer_column"></td>
      <td></td>
      <td class="standard_table_side_padding"></td>
    </tr>
    <tr class="standard_table_column_heading_row">
      <td class="standard_table_side_padding"></td>
      <td>
	Marketing Source
      </td>
      <xsl:apply-templates select="column_heading"/>
      <td class="standard_table_spacer_column"></td>
      <td style="text-align: right; color: white; background-color: gray; font-weight: bold;">Total</td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="column_heading">
    <td class="standard_table_spacer_column"></td>
    <td>
      <xsl:value-of select="month_abbreviation"/>
    </td>
  </xsl:template>

  <xsl:template match="marketing_source_row">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="name"/>
      </td>
      <xsl:apply-templates select="num_calls"/>
      <xsl:apply-templates select="num_calls_total"/>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="num_calls">
    <td class="standard_table_spacer_column"></td>
    <td align="right">
      <xsl:if test=". != ''">
	<xsl:variable name="column_position" select="position()"/>
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/marketing/view_month_cdr.cgi?marketing_call_source_id=</xsl:text>
	    <xsl:value-of select="../marketing_call_source_id"/>
	    <xsl:text>&amp;year=</xsl:text>
	    <xsl:value-of select="/page/call_grid/column_heading_list/column_heading[$column_position]/year"/>
	    <xsl:text>&amp;month=</xsl:text>
	    <xsl:value-of select="/page/call_grid/column_heading_list/column_heading[$column_position]/month"/>
	  </xsl:attribute>
	  <xsl:value-of select="."/>
	</a>
      </xsl:if>
    </td>
  </xsl:template>

  <xsl:template match="num_calls_total">
    <td class="standard_table_spacer_column"></td>
    <td align="right" style="color: white; background-color: gray; font-weight: bold;">
      <xsl:value-of select="."/>
    </td>
  </xsl:template>

  <xsl:template match="summary_row">
    <tr class="standard_table_summary_row">
      <td class="standard_table_side_padding"></td>
      <td align="right">
	Total
      </td>
      <xsl:apply-templates select="summary_num_calls"/>
      <td class="standard_table_spacer_column"></td>
      <td></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="summary_num_calls">
    <td class="standard_table_spacer_column"></td>
    <td align="right">
      <xsl:value-of select="."/>
    </td>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

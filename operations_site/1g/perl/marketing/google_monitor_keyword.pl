#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use REST::Google::Search;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use get_database_handle;
use get_row_list;
use insert_row;
use get_date_now;
use add_time;
use lead_pad;
use delete_rows_with_query;

my $google_result_page_size = 8;
my $google_max_result_pages = 6;
my $google_max_search_tries = 3;
my $max_age = 60; # max age of records in days. older records are purged each day.

main();

sub main() {
  my $instance_map = {};
  my $dbh = get_database_handle($instance_map, "main");
  my $sql_query = "select * from monitored_keyword_tbl";
  my @monitored_keyword_list;
  get_row_list($dbh, $sql_query, undef, \@monitored_keyword_list);
  REST::Google::Search->http_referer('http://microoffice.com');
  # query Google on each monitored keyword.
  for my $monitored_keyword (@monitored_keyword_list) {
    my $keyword = $monitored_keyword->{"keyword"};
    my $search_language = $monitored_keyword->{"search_language"};
    my %google_param_map = ("q" => $keyword, "v" => "1.0", "rsz" => "large");
    $google_param_map{"lr"} = $search_language if ($search_language);
    my @search_result_list;
    my $search_failed = 0;
    my $_google_max_result_pages = $google_max_result_pages;
    for(my $i = 0; $i < $_google_max_result_pages; $i++) {
      $google_param_map{"start"} = $i * $google_result_page_size;
      my $google_response;
      my $num_tries = 0;
      my $request_succeeded = 0;
      while(!$request_succeeded) {
	$google_response = REST::Google::Search->new(%google_param_map);
	$num_tries++;
	if ($google_response->responseStatus() == 200) {
	  $request_succeeded = 1;
	} elsif ($num_tries >= $google_max_search_tries) {
	  warn "response status failure on keyword ($keyword): " . $google_response->responseDetails();
	  $search_failed = 1;
	  last;
	} else {
	  # wait 30 seconds and try same HTTP request again.
	  sleep(30);
	}
      }
      last if ($search_failed);
      my $response_data = $google_response->responseData();
      my $num_pages = scalar(@{$response_data->{"cursor"}->{"pages"}});
      if ($num_pages < $google_max_result_pages) {
	$_google_max_result_pages = $num_pages;
      }
      for my $search_result (@{$response_data->{"results"}}) {
	push(@search_result_list, $search_result);
      }
    }
    if ((!$search_failed) && (@search_result_list)) {
      my @sql_column_list;
      push(@sql_column_list, ["query", $keyword]);
      push(@sql_column_list, ["lr", $search_language]) if ($search_language);
      my $google_query_id = insert_row($dbh, "google_query_tbl", \@sql_column_list, "google_query_seq");
      my $search_rank = 1;
      for my $search_result (@search_result_list) {
	@sql_column_list = ();
	push(@sql_column_list, ["google_query_id", $google_query_id, SQL_INTEGER]);
	push(@sql_column_list, ["search_rank", $search_rank++, SQL_INTEGER]);
	push(@sql_column_list, ["content", $search_result->{"content"}]);
	push(@sql_column_list, ["title", $search_result->{"title"}]);
	push(@sql_column_list, ["title_no_formatting", $search_result->{"titleNoFormatting"}]);
	push(@sql_column_list, ["url", $search_result->{"url"}]);
	push(@sql_column_list, ["unescaped_url", $search_result->{"unescapedUrl"}]);
	push(@sql_column_list, ["visible_url", $search_result->{"visibleUrl"}]) if ($search_result->{"visibleUrl"});
	push(@sql_column_list, ["cache_url", $search_result->{"cacheUrl"}]) if ($search_result->{"cacheUrl"});
	insert_row($dbh, "google_search_result_tbl", \@sql_column_list);
      }
    }
  }
  # delete records older than max_age days.
  $sql_query = "delete from google_search_result_tbl where google_query_id in (select id from google_query_tbl where creation_dt < ?)";
  my ($today_year, $today_month, $today_day) = get_date_now();
  my ($cutoff_year, $cutoff_month, $cutoff_day) = add_time($today_year, $today_month, $today_day, 0, 0, 0, "-$max_age", "day");
  my $cutoff_date = $cutoff_year . "-" . lead_pad($cutoff_month, 2) . "-" . lead_pad($cutoff_day, 2);
  my @sql_param_list = ($cutoff_date);
  delete_rows_with_query($dbh, $sql_query, \@sql_param_list);
  $sql_query = "delete from google_query_tbl where creation_dt < ?";
  delete_rows_with_query($dbh, $sql_query, \@sql_param_list);
  $dbh->disconnect();
}

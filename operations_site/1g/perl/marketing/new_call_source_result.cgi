#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use insert_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/lead_tracking/edit_call_source");
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # check format of line number.
  my $line_number = $cgi->param("line_number");
  if ($line_number !~ /^\d{3}-\d{3}-\d{4}$/) {
    die "show_page: edit_call_source_bad_line_number.html\n";
  }
  # check format of start.
  my $start_datetime = $cgi->param("start_datetime");
  if ($start_datetime !~ /^\d{4}-\d\d-\d\d$/) {
    die "show_page: edit_call_source_bad_start_date.html\n";
  }
  $start_datetime .= " 00:00:00";
  # check format of end.
  my $end_datetime = $cgi->param("end_datetime");
  if ($end_datetime !~ /^\d{4}-\d\d-\d\d$/) {
    die "show_page: edit_call_source_bad_end_date.html\n";
  }
  $end_datetime .= " 23:59:59";
  # check that name of publication is non-blank.
  my $name = Encode::decode($cgi_input_charset, $cgi->param("name"));
  if (!$name) {
    die "show_page: edit_call_source_missing_name.html\n";
  }
  # check format of unit cost.
  my $unit_cost = $cgi->param("unit_cost");
  if ($unit_cost) {
    if ($unit_cost !~ /^\d+(\.\d\d)?$/) {
      die "show_page: edit_call_source_bad_unit_cost.html\n";
    }
  } elsif ($unit_cost eq "") {
    $unit_cost = undef;
  }
  my $unit_name = Encode::decode($cgi_input_charset, $cgi->param("unit_name")) || undef;
  # check format of cost per month.
  my $cost_per_month = $cgi->param("cost_per_month");
  if ($cost_per_month) {
    if ($cost_per_month !~ /^\d+(\.\d\d)?$/) {
      die "show_page: edit_call_source_bad_cost_per_month.html\n";
    }
  } elsif ($cost_per_month eq "") {
    $cost_per_month = undef;
  }
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes")) || undef;
  my $active = $cgi->param("active");
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["line_number", $line_number]);
  push(@sql_column_list, ["start_datetime", $start_datetime]);
  push(@sql_column_list, ["end_datetime", $end_datetime]);
  push(@sql_column_list, ["name", Encode::encode($db_charset, $name)]);
  push(@sql_column_list, ["unit_cost", $unit_cost, SQL_DECIMAL]);
  push(@sql_column_list, ["unit_name", Encode::encode($db_charset, $unit_name)]);
  push(@sql_column_list, ["cost_per_month", $cost_per_month, SQL_DECIMAL]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  push(@sql_column_list, ["active", $active, SQL_BOOLEAN]);
  insert_row($dbh, "marketing_call_source_tbl", \@sql_column_list);
  # populate information needed for display of page.
  $name = HTML::Entities::encode($name);
  $unit_cost = HTML::Entities::encode(defined($unit_cost) ? $unit_cost : "");
  $unit_name = HTML::Entities::encode($unit_name || "");
  $cost_per_month = HTML::Entities::encode(defined($cost_per_month) ? $cost_per_month : "");
  $notes = HTML::Entities::encode($notes || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <marketing_call_source>
    <line_number>$line_number</line_number>
    <start_datetime>$start_datetime</start_datetime>
    <end_datetime>$end_datetime</end_datetime>
    <name>$name</name>
    <unit_cost>$unit_cost</unit_cost>
    <unit_name>$unit_name</unit_name>
    <cost_per_month>$cost_per_month</cost_per_month>
    <notes>$notes</notes>
    <active>$active</active>
  </marketing_call_source>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_call_source_result.xslt", $cgi);
}

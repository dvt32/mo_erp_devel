#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use insert_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $web_page_id = $cgi->param("web_page_id");
  my $web_page;
  if (($web_page_id !~ /^\d+$/) || (!($web_page = select_row($dbh, "web_page_tbl", $web_page_id)))) {
    die "show_page: edit_inbound_link_bad_web_page.html\n";
  }
  my $referrer_url = $cgi->param("referrer_url");
  if ((!$referrer_url) || ($referrer_url !~ m{^http(?:s?)://})) {
    die "show_page: edit_inbound_link_bad_referrer_url.html\n";
  }
  my $link_text = Encode::decode($cgi_input_charset, $cgi->param("link_text"));
  if (!$link_text) {
    die "show_page: edit_inbound_link_bad_link_text.html\n";
  }
  my $url_extra_param = $cgi->param("url_extra_param");
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["web_page_id", $web_page_id, SQL_INTEGER]);
  push(@sql_column_list, ["url_extra_param", $url_extra_param]) if ($url_extra_param);
  push(@sql_column_list, ["referrer_url", $referrer_url]);
  push(@sql_column_list, ["link_text", Encode::encode($db_charset, $link_text)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]) if ($note);
  insert_row($dbh, "inbound_link_tbl", \@sql_column_list);
  # populate information needed for display of page.
  $url_extra_param = HTML::Entities::encode($url_extra_param);
  $referrer_url = HTML::Entities::encode($referrer_url);
  $link_text = HTML::Entities::encode($link_text);
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <inbound_link>
    <web_page_id>$web_page_id</web_page_id>
    <url_extra_param>$url_extra_param</url_extra_param>
    <referrer_url>$referrer_url</referrer_url>
    <link_text>$link_text</link_text>
    <note>$note</note>
  </inbound_link>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_inbound_link_result.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_list;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # load search engines.
  my @search_engine_list;
  get_row_list($dbh, "select * from enum_search_engine_tbl where is_active order by label", undef, \@search_engine_list);
  # populate information for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <monitored_keyword/>
  <search_engine_list>
---END_BLOCK---;
  for my $search_engine (@search_engine_list) {
    my $label = HTML::Entities::encode($search_engine->{"label"});
    my $value = HTML::Entities::encode($search_engine->{"value"});
    $generated_xml .= <<"---END_BLOCK---;";
    <search_engine>
      <label>$label</label>
      <value>$value</value>
    </search_engine>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </search_engine_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_monitored_keyword_input.xslt", $cgi);
}

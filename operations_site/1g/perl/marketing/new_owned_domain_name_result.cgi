#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use insert_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # check that domain name is not blank.
  my $domain_name = lc(Encode::decode($cgi_input_charset, $cgi->param("domain_name")));
  if ($domain_name !~ /\w/) {
    die "show_page: edit_owned_domain_name_missing_domain_name.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["domain_name", Encode::encode($db_charset, $domain_name)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  insert_row($dbh, "owned_domain_name_tbl", \@sql_column_list);
  # populate information needed for display of page.
  $domain_name = HTML::Entities::encode($domain_name);
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <owned_domain_name>
    <domain_name>$domain_name</domain_name>
    <note>$note</note>
  </owned_domain_name>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_owned_domain_name_result.xslt", $cgi);
}

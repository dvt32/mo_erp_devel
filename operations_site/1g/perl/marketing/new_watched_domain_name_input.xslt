<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: New Watched Domain Name
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  Rankings: <a href="/1g/marketing/show_bing_rankings_keywords.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_keywords.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_google_rankings_keywords.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_owned.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_owned.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_owned.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_watched.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_watched.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_watched.cgi">Yahoo</a>)
	</p>
	<xsl:apply-templates select="watched_domain_name"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="watched_domain_name">
    <table>
      <tr><td>
	<form action="/1g/marketing/new_watched_domain_name_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Watched Domain Name
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Domain Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">domain_name</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Expire Date (yyyy-mm-dd):
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">expire_date</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Owner Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">owner_name</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Owner Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">owner_phone_number</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Owner Email Address:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">owner_email_address</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Show Rank:
		</td>
		<td>
                  <select name="show_rank">
		    <option>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>Yes</xsl:text>
		    </option>
		    <option>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>No</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

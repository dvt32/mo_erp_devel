#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # load owned domain names.
  my $sql_query = "select * from owned_domain_name_tbl order by domain_name";
  my (%owned_domain_name_by_id, @owned_domain_name_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%owned_domain_name_by_id, \@owned_domain_name_id_list);
  # populate information for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <web_page>
    <keyword_list>
      <keyword/>
      <keyword/>
      <keyword/>
      <keyword/>
      <keyword/>
    </keyword_list>
  </web_page>
  <uri_scheme_list>
    <uri_scheme>http</uri_scheme>
    <uri_scheme>https</uri_scheme>
  </uri_scheme_list>
  <owned_domain_name_list>
---END_BLOCK---;
  for my $owned_domain_name_id (@owned_domain_name_id_list) {
    my $owned_domain_name = $owned_domain_name_by_id{$owned_domain_name_id};
    my $domain_name = HTML::Entities::encode($owned_domain_name->{"domain_name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <owned_domain_name>
      <id>$owned_domain_name_id</id>
      <domain_name>$domain_name</domain_name>
    </owned_domain_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </owned_domain_name_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_web_page_input.xslt", $cgi);
}

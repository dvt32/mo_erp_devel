<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: New Web Page
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  Rankings: <a href="/1g/marketing/show_bing_rankings_keywords.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_keywords.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_google_rankings_keywords.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_owned.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_owned.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_owned.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_watched.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_watched.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_watched.cgi">Yahoo</a>)
	</p>
	<xsl:apply-templates select="web_page"/>
	<p>
	  Example: The URL http://www.microoffice.com/office_space_nyc.html can be broken down as:
	  <ul>
	    <li>http is the URI scheme</li>
	    <li>www is the domain prefix (no trailing dot)</li>
	    <li>microoffice.com is the domain name</li>
	    <li>/office_space_nyc.html is the URI path (include the leading slash)</li>
	  </ul>
	</p>
	<p>
	  After you have saved the new web page, you can enter inbound links.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="web_page">
    <table>
      <tr><td>
	<form action="/1g/marketing/new_web_page_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      New Web Page
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">num_keywords</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="count(keyword_list/keyword)"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (for help, see example below)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URI Scheme:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>uri_scheme</xsl:text>
		    </xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/uri_scheme_list/uri_scheme">
		      <option>
			<xsl:if test=". = 'http'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Domain Prefix:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">domain_prefix</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Domain Name:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>owned_domain_name_id</xsl:text>
		    </xsl:attribute>
		    <option value="">[---]</option>
		    <xsl:for-each select="/page/owned_domain_name_list/owned_domain_name">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="domain_name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  URI Path:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">uri_path</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; " colspan="2">
		  <hr/>
		</td>
	      </tr>
	      <xsl:apply-templates select="keyword_list/keyword"/>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (to enter more keywords, save first)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="keyword">
    <tr>
      <td style="padding: 4px; ">
	Keyword <xsl:value-of select="position()"/>:
      </td>
      <td>
	<input>
	  <xsl:attribute name="type">text</xsl:attribute>
	  <xsl:attribute name="name">keyword_<xsl:value-of select="position()"/></xsl:attribute>
	  <xsl:attribute name="size">40</xsl:attribute>
	</input>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

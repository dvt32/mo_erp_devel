#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use strip_outside_whitespace;
use select_row;
use select_row_with_query;
use insert_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $uri_scheme = $cgi->param("uri_scheme");
  if ($uri_scheme !~ /^http|https$/) {
    die "show_page: edit_web_page_bad_uri_scheme.html\n";
  }
  my $domain_prefix = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("domain_prefix")));
  if (($domain_prefix !~ /^[a-zA-Z0-9_\-\.]*$/) || ($domain_prefix =~ /\.$/)) {
    die "show_page: edit_web_page_bad_domain_prefix.html\n";
  }
  my $owned_domain_name_id = $cgi->param("owned_domain_name_id");
  my $owned_domain_name;
  if (($owned_domain_name_id !~ /^\d+$/) ||
      (!($owned_domain_name = select_row($dbh, "owned_domain_name_tbl", $owned_domain_name_id)))) {
    die "show_page: edit_web_page_bad_owned_domain_name.html\n";
  }
  my $uri_path = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("uri_path")));
  if ($uri_path !~ m{^/}) {
    die "show_page: edit_web_page_bad_uri_path.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  my $num_keywords = $cgi->param("num_keywords");
  my @keyword_list = map {strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("keyword_$_")))} (1..$num_keywords);
  for(my $i = 0; $i < scalar(@keyword_list);) {
    $keyword_list[$i] =~ s/\s+/ /g; # collapse internal whitespace of multiple spaces to one space
    if ($keyword_list[$i]) {
      $i++;
    } else {
      splice(@keyword_list, $i, 1);
    }
  }
  # check that another web page does not have the same (URI scheme, domain prefix, domain name, and URI path) tuple.
  my $sql_query = "select * from web_page_tbl where uri_scheme = ? and domain_prefix = ? and owned_domain_name_id = ? and uri_path = ?";
  my @sql_param_list = ($uri_scheme, $domain_prefix, [$owned_domain_name_id, SQL_INTEGER], $uri_path);
  if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    die "show_page: edit_web_page_duplicate_row.html\n";
  }
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["owned_domain_name_id", $owned_domain_name_id, SQL_INTEGER]);
  push(@sql_column_list, ["domain_prefix", Encode::encode($db_charset, $domain_prefix)]) if ($domain_prefix);
  push(@sql_column_list, ["uri_scheme", $uri_scheme]);
  push(@sql_column_list, ["uri_path", Encode::encode($db_charset, $uri_path)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]) if ($note);
  my $web_page_id = insert_row($dbh, "web_page_tbl", \@sql_column_list, "web_page_seq");
  # insert rows in child table ss_web_page_x_keyword_tbl.
  for my $keyword (@keyword_list) {
    @sql_column_list = ();
    push(@sql_column_list, ["parent_id", $web_page_id, SQL_INTEGER]);
    push(@sql_column_list, ["element_value", Encode::encode($db_charset, $keyword)]);
    insert_row($dbh, "ss_web_page_x_keyword_tbl", \@sql_column_list);
  }
  # populate information needed for display of page.
  $uri_scheme = HTML::Entities::encode($uri_scheme);
  $domain_prefix = HTML::Entities::encode($domain_prefix || "");
  my $domain_name = HTML::Entities::encode($owned_domain_name->{"domain_name"});
  $uri_path = HTML::Entities::encode($uri_path);
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <web_page>
    <id>$web_page_id</id>
    <uri_scheme>$uri_scheme</uri_scheme>
    <domain_prefix>$domain_prefix</domain_prefix>
    <domain_name>$domain_name</domain_name>
    <uri_path>$uri_path</uri_path>
    <note>$note</note>
    <keyword_list>
---END_BLOCK---;
  for my $keyword (@keyword_list) {
    my $encoded_keyword = HTML::Entities::encode($keyword);
    $generated_xml .= <<"---END_BLOCK---;";
      <keyword>$encoded_keyword</keyword>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </keyword_list>
  </web_page>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_web_page_result.xslt", $cgi);
}

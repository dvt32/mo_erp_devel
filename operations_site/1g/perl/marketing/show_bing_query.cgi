#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $bing_query_id = $cgi->param("id");
  my ($bing_query_id, $bing_query) = ($cgi->param("id"), undef);
  if ((!$bing_query_id) || ($bing_query_id !~ /^\d+$/) || (!($bing_query = select_row($dbh, "bing_query_tbl", $bing_query_id)))) {
    die "show_page: show_bing_query_not_found.html\n";
  }
  my $sql_query = "select * from bing_search_result_tbl where bing_query_id = ? order by search_rank";
  my @sql_param_list = ([$bing_query_id, SQL_INTEGER]);
  my (%bing_search_result_by_id, @bing_search_result_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%bing_search_result_by_id, \@bing_search_result_id_list);
  # populate information needed for display of page.
  my $query = HTML::Entities::encode($bing_query->{"query"});
  my $note = HTML::Entities::encode($bing_query->{"note"} || "");
  my $creation_dt = HTML::Entities::encode($bing_query->{"creation_dt"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <bing_query>
    <id>$bing_query_id</id>
    <query>$query</query>
    <note>$note</note>
    <creation_dt>$creation_dt</creation_dt>
  </bing_query>
  <bing_search_result_list>
---END_BLOCK---;
  for my $bing_search_result_id (@bing_search_result_id_list) {
    my $bing_search_result = $bing_search_result_by_id{$bing_search_result_id};
    my $search_rank = $bing_search_result->{"search_rank"};
    my $content = HTML::Entities::encode($bing_search_result->{"content"} || "");
    my $title = HTML::Entities::encode($bing_search_result->{"title"} || "");
    my $title_no_formatting = HTML::Entities::encode($bing_search_result->{"title_no_formatting"} || "");
    my $url = HTML::Entities::encode($bing_search_result->{"url"});
    my $unescaped_url = HTML::Entities::encode($bing_search_result->{"unescaped_url"});
    my $visible_url = HTML::Entities::encode($bing_search_result->{"visible_url"} || "");
    my $cache_url = HTML::Entities::encode($bing_search_result->{"cache_url"} || "");
    my $note = HTML::Entities::encode($bing_search_result->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <bing_search_result>
      <id>$bing_search_result_id</id>
      <search_rank>$search_rank</search_rank>
      <content>$content</content>
      <title>$title</title>
      <title_no_formatting>$title_no_formatting</title_no_formatting>
      <url>$url</url>
      <unescaped_url>$unescaped_url</unescaped_url>
      <visible_url>$visible_url</visible_url>
      <cache_url>$cache_url</cache_url>
      <note>$note</note>
    </bing_search_result>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </bing_search_result_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_bing_query.xslt", $cgi);
}

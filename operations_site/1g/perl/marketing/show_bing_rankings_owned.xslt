<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Rankings of Owned Domain Names
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_keywords.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_keywords.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_keywords.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  Rankings: <a href="/1g/marketing/show_bing_rankings_owned.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_owned.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_owned.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_watched.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_watched.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_watched.cgi">Yahoo</a>)
	</p>
	<xsl:apply-templates select="owned_domain_name_list"/>
	<form action="/1g/marketing/show_bing_rankings_owned.cgi">
	  Query Date:
	  <select>
	    <xsl:attribute name="name">
	      <xsl:text>query_month</xsl:text>
	    </xsl:attribute>
	    <xsl:for-each select="/page/month_list/month">
	      <option>
		<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		<xsl:value-of select="name"/>
	      </option>
	    </xsl:for-each>
	  </select>
	  <select name="day">
	    <xsl:attribute name="name">
	      <xsl:text>query_day</xsl:text>
	    </xsl:attribute>
	    <xsl:for-each select="/page/day_list/day">
	      <option>
		<xsl:value-of select="."/>
	      </option>
	    </xsl:for-each>
	  </select>
	  <input>
	    <xsl:attribute name="type">text</xsl:attribute>
	    <xsl:attribute name="name">
	      <xsl:text>query_year</xsl:text>
	    </xsl:attribute>
	    <xsl:attribute name="size">4</xsl:attribute>
	  </input>
	  <input type="submit" name="submitButton" value="Go"/>
	  (or click a date below)
	</form>
	<xsl:apply-templates select="monitored_date_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="monitored_date_list">
    Recent Query Dates:
    <xsl:apply-templates select="monitored_date"/>
  </xsl:template>

  <xsl:template match="monitored_date">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/marketing/show_bing_rankings_owned.cgi?query_date=<xsl:value-of select="."/></xsl:attribute>
      <xsl:value-of select="."/>
    </a>
    <xsl:if test="position() &lt; last()">
      <xsl:text> &middot; </xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="owned_domain_name_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="5" class="standard_table_title">
	  Rankings of Owned Domain Names on <xsl:value-of select="/page/query_date"/>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Owned Domain</td>
	<td class="standard_table_spacer_column"></td>
	<td>Keywords</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="owned_domain_name"/>
    </table>
  </xsl:template>

  <xsl:template match="owned_domain_name">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="domain_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:apply-templates select="bing_search_result_match_list"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="bing_search_result_match_list">
    <xsl:apply-templates select="bing_search_result_match"/>
  </xsl:template>

  <xsl:template match="bing_search_result_match">
    <xsl:value-of select="query"/><xsl:text> </xsl:text><xsl:value-of select="search_rank"/>
    <br/>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

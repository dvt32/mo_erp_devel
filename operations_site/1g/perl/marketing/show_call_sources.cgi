#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:marketing/lead_tracking");
  my $sql_query = "select * from marketing_call_source_tbl order by line_number, active, start_datetime";
  my (%marketing_call_source_by_id, @marketing_call_source_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%marketing_call_source_by_id, \@marketing_call_source_id_list);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <role-list>
---END_BLOCK---;
  for my $role (@$role_list) {
    $generated_xml .= <<"---END_BLOCK---;";
     <role>$role</role>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </role-list>
  <marketing_call_source_list>
---END_BLOCK---;
  for my $marketing_call_source_id (@marketing_call_source_id_list) {
    my $marketing_call_source = $marketing_call_source_by_id{$marketing_call_source_id};
    my $line_number = $marketing_call_source->{"line_number"};
    my $start_datetime = $marketing_call_source->{"start_datetime"};
    my $end_datetime = $marketing_call_source->{"end_datetime"};
    my $name = HTML::Entities::encode($marketing_call_source->{"name"});
    my $unit_cost = HTML::Entities::encode(defined($marketing_call_source->{"unit_cost"}) ? $marketing_call_source->{"unit_cost"} : "");
    my $unit_name = HTML::Entities::encode($marketing_call_source->{"unit_name"} || "");
    my $cost_per_month = HTML::Entities::encode(defined($marketing_call_source->{"cost_per_month"}) ? $marketing_call_source->{"cost_per_month"} : "");
    my $notes = HTML::Entities::encode($marketing_call_source->{"notes"} || "");
    my $active = $marketing_call_source->{"active"} ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
    <marketing_call_source>
      <id>$marketing_call_source_id</id>
      <line_number>$line_number</line_number>
      <start_datetime>$start_datetime</start_datetime>
      <end_datetime>$end_datetime</end_datetime>
      <name>$name</name>
      <unit_cost>$unit_cost</unit_cost>
      <unit_name>$unit_name</unit_name>
      <cost_per_month>$cost_per_month</cost_per_month>
      <notes>$notes</notes>
      <active>$active</active>
    </marketing_call_source>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </marketing_call_source_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_call_sources.xslt", $cgi);
}

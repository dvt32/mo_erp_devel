<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Call Sources
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/find_cdr_input.cgi">Call Details</a>
	</p>
	bolded green * = active
	<xsl:apply-templates select="marketing_call_source_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="marketing_call_source_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="19">
	  <table cellspacing="0" cellpadding="0" border="0" width="100%">
	    <tr>
	      <td class="standard_table_title" width="20%">&nbsp;</td>
	      <td class="standard_table_title" style="text-align: center" width="60%">Marketing Call Sources</td>
	      <td class="standard_table_title" style="text-align: right" width="20%">
		<xsl:choose>
		  <xsl:when test="/page/role-list/role = 'role:marketing/lead_tracking/edit_call_source'">
		    <a href="/1g/marketing/new_call_source_input.cgi" class="bare_link" style="color: white">New</a>
		  </xsl:when>
		  <xsl:otherwise>
		    &nbsp;
		  </xsl:otherwise>
		</xsl:choose>
	      </td>
	    </tr>
	  </table>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td></td>
	<td class="standard_table_spacer_column"></td>
	<td>Line Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Start</td>
	<td class="standard_table_spacer_column"></td>
	<td>End</td>
	<td class="standard_table_spacer_column"></td>
	<td>Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Unit Cost</td>
	<td class="standard_table_spacer_column"></td>
	<td>Unit</td>
	<td class="standard_table_spacer_column"></td>
	<td>Cost/mo.</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(marketing_call_source) > 0">
	  <xsl:apply-templates select="marketing_call_source"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="17">No marketing call sources.</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="marketing_call_source">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="valign">top</xsl:attribute>
      <xsl:choose>
	<xsl:when test="active = 1">
	  <xsl:attribute name="style">font-weight: bold; color: green</xsl:attribute>
	</xsl:when>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:if test="active = 1">
	  *
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td style="white-space: nowrap">
	<xsl:choose>
	  <xsl:when test="active = 1">
	    <xsl:choose>
	      <xsl:when test="/page/role-list/role = 'role:marketing/lead_tracking/edit_call_source'">
		<a>
		  <xsl:attribute name="class">bare_link</xsl:attribute>
		  <xsl:attribute name="style">color: green</xsl:attribute>
		  <xsl:attribute name="href">/1g/marketing/edit_call_source_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
		  <xsl:value-of select="line_number"/>
		</a>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:value-of select="line_number"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:choose>
	      <xsl:when test="/page/role-list/role = 'role:marketing/lead_tracking/edit_call_source'">
		<a>
		  <xsl:attribute name="class">bare_link</xsl:attribute>
		  <xsl:attribute name="href">/1g/marketing/edit_call_source_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
		  <xsl:value-of select="line_number"/>
		</a>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:value-of select="line_number"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td style="white-space: nowrap"><xsl:value-of select="substring(start_datetime, 1, 10)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td style="white-space: nowrap"><xsl:value-of select="substring(end_datetime, 1, 10)"/></td>
      <td class="standard_table_spacer_column"></td>
      <td style="white-space: nowrap"><xsl:value-of select="name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell" style="white-space: nowrap">
	<xsl:if test="unit_cost != ''">
	  $<xsl:value-of select="unit_cost"/>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td style="white-space: nowrap"><xsl:value-of select="unit_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td class="standard_table_currency_cell" style="white-space: nowrap">
	<xsl:if test="cost_per_month != ''">
	  $<xsl:value-of select="cost_per_month"/>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="notes"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

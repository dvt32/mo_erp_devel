#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $google_query_id = $cgi->param("id");
  my ($google_query_id, $google_query) = ($cgi->param("id"), undef);
  if ((!$google_query_id) || ($google_query_id !~ /^\d+$/) || (!($google_query = select_row($dbh, "google_query_tbl", $google_query_id)))) {
    die "show_page: show_google_query_not_found.html\n";
  }
  my $sql_query = "select * from google_search_result_tbl where google_query_id = ? order by search_rank";
  my @sql_param_list = ([$google_query_id, SQL_INTEGER]);
  my (%google_search_result_by_id, @google_search_result_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%google_search_result_by_id, \@google_search_result_id_list);
  # populate information needed for display of page.
  my $query = HTML::Entities::encode($google_query->{"query"});
  my $note = HTML::Entities::encode($google_query->{"note"} || "");
  my $creation_dt = HTML::Entities::encode($google_query->{"creation_dt"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <google_query>
    <id>$google_query_id</id>
    <query>$query</query>
    <note>$note</note>
    <creation_dt>$creation_dt</creation_dt>
  </google_query>
  <google_search_result_list>
---END_BLOCK---;
  for my $google_search_result_id (@google_search_result_id_list) {
    my $google_search_result = $google_search_result_by_id{$google_search_result_id};
    my $search_rank = $google_search_result->{"search_rank"};
    my $content = HTML::Entities::encode($google_search_result->{"content"} || "");
    my $title = HTML::Entities::encode($google_search_result->{"title"} || "");
    my $title_no_formatting = HTML::Entities::encode($google_search_result->{"title_no_formatting"} || "");
    my $url = HTML::Entities::encode($google_search_result->{"url"});
    my $unescaped_url = HTML::Entities::encode($google_search_result->{"unescaped_url"});
    my $visible_url = HTML::Entities::encode($google_search_result->{"visible_url"} || "");
    my $cache_url = HTML::Entities::encode($google_search_result->{"cache_url"} || "");
    my $note = HTML::Entities::encode($google_search_result->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <google_search_result>
      <id>$google_search_result_id</id>
      <search_rank>$search_rank</search_rank>
      <content>$content</content>
      <title>$title</title>
      <title_no_formatting>$title_no_formatting</title_no_formatting>
      <url>$url</url>
      <unescaped_url>$unescaped_url</unescaped_url>
      <visible_url>$visible_url</visible_url>
      <cache_url>$cache_url</cache_url>
      <note>$note</note>
    </google_search_result>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </google_search_result_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_google_query.xslt", $cgi);
}

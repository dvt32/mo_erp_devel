#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $sql_query = "select * from monitored_keyword_tbl order by keyword";
  my (%monitored_keyword_by_id, @monitored_keyword_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%monitored_keyword_by_id, \@monitored_keyword_id_list);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <monitored_keyword_list>
---END_BLOCK---;
  for my $monitored_keyword_id (@monitored_keyword_id_list) {
    my $monitored_keyword = $monitored_keyword_by_id{$monitored_keyword_id};
    my $keyword = HTML::Entities::encode($monitored_keyword->{"keyword"});
    my $search_language = HTML::Entities::encode($monitored_keyword->{"search_language"} || "");
    my $search_engine = HTML::Entities::encode($monitored_keyword->{"search_engine"});
    my $note = HTML::Entities::encode($monitored_keyword->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <monitored_keyword>
      <id>$monitored_keyword_id</id>
      <keyword>$keyword</keyword>
      <search_language>$search_language</search_language>
      <search_engine>$search_engine</search_engine>
      <note>$note</note>
    </monitored_keyword>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </monitored_keyword_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_monitored_keywords.xslt", $cgi);
}

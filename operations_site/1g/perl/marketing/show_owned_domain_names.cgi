#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $sql_query = "select * from owned_domain_name_tbl order by domain_name";
  my (%owned_domain_name_by_id, @owned_domain_name_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%owned_domain_name_by_id, \@owned_domain_name_id_list);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <owned_domain_name_list>
---END_BLOCK---;
  for my $owned_domain_name_id (@owned_domain_name_id_list) {
    my $owned_domain_name = $owned_domain_name_by_id{$owned_domain_name_id};
    my $domain_name = HTML::Entities::encode($owned_domain_name->{"domain_name"});
    my $note = HTML::Entities::encode($owned_domain_name->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <owned_domain_name>
      <id>$owned_domain_name_id</id>
      <domain_name>$domain_name</domain_name>
      <note>$note</note>
    </owned_domain_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </owned_domain_name_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_owned_domain_names.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use lead_pad;
use get_row_list;
use get_row_by_id_map;
use get_month_name;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # query date could be specified in (query_month, query_day, query_year) or query_date or omitted altogether.
  my ($query_month, $query_day, $query_year) = map {$cgi->param($_)} (qw(query_month query_day query_year));
  my $query_date = $cgi->param("query_date");
  if ($query_date) {
    if ($query_date !~ /^(\d{4})-(\d\d)-(\d\d)$/) {
      die "show_page: show_rankings_keywords_bad_query_date.html\n";
    } else {
      ($query_year, $query_month, $query_day) = ($1, $2, $3);
    }
  }
  if ($query_month || $query_day || $query_year) {
    if (($query_month < 1) || ($query_month > 12) ||
	($query_day < 1) || ($query_day > 31) ||
	($query_year < 2008) || ($query_year > 3000)) {
      die "show_page: show_rankings_keywords_bad_query_date.html\n";
    }
    $query_date = $query_year . "-" . lead_pad(int($query_month), 2) . "-" . lead_pad(int($query_day), 2);
  }
  # get 5 most recent dates on which keywords were monitored.
  my $sql_query = "select distinct cast(creation_dt as date) as creation_date from google_query_tbl order by creation_date desc limit 5";
  my @monitored_date_row_list;
  get_row_list($dbh, $sql_query, undef, \@monitored_date_row_list);
  my @monitored_date_list = map {$_->{"creation_date"}} @monitored_date_row_list;
  # get the queries and search results from the specified query date, or the most recent date on which keywords were monitored
  # (if query date is not specified).
  my (%google_query_by_id, @google_query_id_list);
  my (%google_search_result_by_id, %google_search_result_id_list_by_google_query_id);
  my (%owned_domain_name_by_id, %watched_domain_name_by_id);
  if (@monitored_date_list) {
    $query_date = $monitored_date_list[0] if (!$query_date);
    $sql_query = "select * from google_query_tbl where cast(creation_dt as date) = ? order by query";
    my @sql_param_list = ($query_date);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%google_query_by_id, \@google_query_id_list);
    if (!@google_query_id_list) {
      die "show_page: show_rankings_keywords_no_queries_on_date.html\n";
    }
    $sql_query = "select * from google_search_result_tbl where google_query_id in (select id from google_query_tbl where cast(creation_dt as date) = ?)";
    @sql_param_list = ($query_date);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%google_search_result_by_id);
    for my $google_search_result (values(%google_search_result_by_id)) {
      push(@{$google_search_result_id_list_by_google_query_id{$google_search_result->{"google_query_id"}}}, $google_search_result->{"id"});
    }
    # get the owned domain names.
    $sql_query = "select * from owned_domain_name_tbl";
    get_row_by_id_map($dbh, $sql_query, undef, \%owned_domain_name_by_id);
    # get the watched domain names.
    $sql_query = "select * from watched_domain_name_tbl where show_rank";
    get_row_by_id_map($dbh, $sql_query, undef, \%watched_domain_name_by_id);
  }
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <month_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month_list>
  <day_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day_list>
  <monitored_date_list>
---END_BLOCK---;
  for my $monitored_date (@monitored_date_list) {
    $generated_xml .= <<"---END_BLOCK---;";
    <monitored_date>$monitored_date</monitored_date>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </monitored_date_list>
  <query_date>$query_date</query_date>
  <google_query_list>
---END_BLOCK---;
  for my $google_query_id (@google_query_id_list) {
    my $google_query = $google_query_by_id{$google_query_id};
    my $query = HTML::Entities::encode($google_query->{"query"});
    my $note = HTML::Entities::encode($google_query->{"note"} || "");
    # scan through the google search results to see if any URLs match domain names owned or watched by us.
    my $google_search_result_id_list = $google_search_result_id_list_by_google_query_id{$google_query_id};
    my (@owned_match_list, @watched_match_list);
    if ($google_search_result_id_list) {
      for my $google_search_result_id (@$google_search_result_id_list) {
	my $google_search_result = $google_search_result_by_id{$google_search_result_id};
	$google_search_result->{"url"} =~ m{://([^/]+?)/};
	my $full_server_name = lc($1);
	my $domain_name;
	if ($full_server_name =~ m|\.([^\.]+\.\w+)$|) {
	  $domain_name = lc($1);
	} else {
	  $domain_name = lc($full_server_name);
	}
	if (grep {$domain_name eq lc($_->{"domain_name"})} values(%owned_domain_name_by_id)) {
	  push(@owned_match_list, [$domain_name, $google_search_result->{"search_rank"}]);
	}
	if (grep {$domain_name eq lc($_->{"domain_name"})} values(%watched_domain_name_by_id)) {
	  push(@watched_match_list, [$domain_name, $google_search_result->{"search_rank"}]);
	}
      }
    }
    @owned_match_list = sort {$a->[1] <=> $b->[1]} @owned_match_list;
    @watched_match_list = sort {$a->[1] <=> $b->[1]} @watched_match_list;
    $generated_xml .= <<"---END_BLOCK---;";
    <google_query>
      <id>$google_query_id</id>
      <query>$query</query>
      <note>$note</note>
      <owned_match_list>
---END_BLOCK---;
    for my $owned_match (@owned_match_list) {
      my $server = HTML::Entities::encode($owned_match->[0]);
      my $search_rank = $owned_match->[1];
      $generated_xml .= <<"---END_BLOCK---;";
        <owned_match>
          <server>$server</server>
          <search_rank>$search_rank</search_rank>
        </owned_match>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </owned_match_list>
      <watched_match_list>
---END_BLOCK---;
    for my $watched_match (@watched_match_list) {
      my $server = HTML::Entities::encode($watched_match->[0]);
      my $search_rank = $watched_match->[1];
      $generated_xml .= <<"---END_BLOCK---;";
        <watched_match>
          <server>$server</server>
          <search_rank>$search_rank</search_rank>
        </watched_match>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </watched_match_list>
    </google_query>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </google_query_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_rankings_keywords.xslt", $cgi);
}

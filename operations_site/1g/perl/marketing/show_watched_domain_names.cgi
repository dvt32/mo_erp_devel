#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $sort_by = $cgi->param("sort_by") || "";
  my $order_by_clause = "order by ";
  if ($sort_by eq "domain_name") {
    $order_by_clause .= "domain_name";
  } else {
    $order_by_clause .= "expire_date, domain_name";
  }
  my $sql_query = "select * from watched_domain_name_tbl $order_by_clause";
  my (%watched_domain_name_by_id, @watched_domain_name_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%watched_domain_name_by_id, \@watched_domain_name_id_list);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sort_by_list>
    <sort_by>
      <label>Expire Date</label>
      <value>expire_date</value>
    </sort_by>
    <sort_by>
      <label>Domain Name</label>
      <value>domain_name</value>
    </sort_by>
  </sort_by_list>
  <sort_by>$sort_by</sort_by>
  <watched_domain_name_list>
---END_BLOCK---;
  for my $watched_domain_name_id (@watched_domain_name_id_list) {
    my $watched_domain_name = $watched_domain_name_by_id{$watched_domain_name_id};
    my $domain_name = HTML::Entities::encode($watched_domain_name->{"domain_name"});
    my $expire_date = HTML::Entities::encode($watched_domain_name->{"expire_date"} || "");
    my $owner_name = HTML::Entities::encode($watched_domain_name->{"owner_name"} || "");
    my $owner_phone_number = HTML::Entities::encode($watched_domain_name->{"owner_phone_number"} || "");
    my $owner_email_address = HTML::Entities::encode($watched_domain_name->{"owner_email_address"} || "");
    my $note = HTML::Entities::encode($watched_domain_name->{"note"} || "");
    my $show_rank = $watched_domain_name->{"show_rank"} ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
    <watched_domain_name>
      <id>$watched_domain_name_id</id>
      <domain_name>$domain_name</domain_name>
      <expire_date>$expire_date</expire_date>
      <owner_name>$owner_name</owner_name>
      <owner_phone_number>$owner_phone_number</owner_phone_number>
      <owner_email_address>$owner_email_address</owner_email_address>
      <note>$note</note>
      <show_rank>$show_rank</show_rank>
    </watched_domain_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </watched_domain_name_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_watched_domain_names.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Watched Domain Names
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  Rankings: <a href="/1g/marketing/show_bing_rankings_keywords.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_keywords.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_google_rankings_keywords.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_owned.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_owned.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_owned.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_watched.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_watched.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_watched.cgi">Yahoo</a>)
	</p>
	<form action="/1g/marketing/show_watched_domain_names.cgi">
	  Sort by:
	  <select name="sort_by">
	    <xsl:for-each select="/page/sort_by_list/sort_by">
	      <option>
		<xsl:if test="/page/sort_by = value">
		  <xsl:attribute name="selected">selected</xsl:attribute>
		</xsl:if>
		<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
		<xsl:value-of select="label"/>
	      </option>
	    </xsl:for-each>
	  </select>
	  <input type="submit" name="submitButton" value="Go"/>
	</form>
	<xsl:apply-templates select="watched_domain_name_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="watched_domain_name_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="15">
	  <table cellspacing="0" cellpadding="0" border="0" width="100%">
	    <tr>
	      <td class="standard_table_title" width="20%">&nbsp;</td>
	      <td class="standard_table_title" style="text-align: center" width="60%">Watched Domain Names</td>
	      <td class="standard_table_title" style="text-align: right" width="20%">
		<a href="/1g/marketing/new_watched_domain_name_input.cgi" class="bare_link" style="color: white">New</a>
	      </td>
	    </tr>
	  </table>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Domain Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Expire Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Owner Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Owner Phone</td>
	<td class="standard_table_spacer_column"></td>
	<td>Owner Email</td>
	<td class="standard_table_spacer_column"></td>
	<td>Rank</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="watched_domain_name"/>
    </table>
  </xsl:template>

  <xsl:template match="watched_domain_name">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="href">/1g/marketing/edit_watched_domain_name_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="domain_name"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="expire_date"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="owner_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="owner_phone_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="owner_email_address"/></td>
      <td class="standard_table_spacer_column"></td>
      <td style="text-align: center;">
	<xsl:choose>
	  <xsl:when test="show_rank = 1">
	    Yes
	  </xsl:when>
	  <xsl:otherwise>
	    No
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

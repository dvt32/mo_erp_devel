#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  my $sql_query = "select * from web_page_tbl";
  my (%web_page_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%web_page_by_id);
  $sql_query = "select * from owned_domain_name_tbl";
  my (%owned_domain_name_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%owned_domain_name_by_id);
  $sql_query = "select * from ss_web_page_x_keyword_tbl";
  my (%web_page_x_keyword_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%web_page_x_keyword_by_id);
  my %keyword_list_by_web_page_id;
  for my $web_page_x_keyword (values(%web_page_x_keyword_by_id)) {
    my $web_page_id = $web_page_x_keyword->{"parent_id"};
    my $keyword = $web_page_x_keyword->{"element_value"};
    push(@{$keyword_list_by_web_page_id{$web_page_id}}, $keyword);
  }
  for my $keyword_list (values(%keyword_list_by_web_page_id)) {
    @$keyword_list = sort(@$keyword_list);
  }
  $sql_query = "select * from inbound_link_tbl";
  my (%inbound_link_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%inbound_link_by_id);
  my (%inbound_link_list_by_web_page_id);
  for my $inbound_link (values(%inbound_link_by_id)) {
    my $web_page_id = $inbound_link->{"web_page_id"};
    push(@{$inbound_link_list_by_web_page_id{$web_page_id}}, $inbound_link);
  }
  for my $inbound_link_list (values(%inbound_link_list_by_web_page_id)) {
    @$inbound_link_list = sort {
      $a->{"referrer_url"} cmp $b->{"referrer_url"};
    } @$inbound_link_list;
  }
  my @web_page_id_list = sort {
    my $web_page_a = $web_page_by_id{$a};
    my $web_page_b = $web_page_by_id{$b};
    my $owned_domain_name_id_a = $web_page_a->{"owned_domain_name_id"};
    my $owned_domain_name_id_b = $web_page_b->{"owned_domain_name_id"};
    my $owned_domain_name_a = $owned_domain_name_by_id{$owned_domain_name_id_a};
    my $owned_domain_name_b = $owned_domain_name_by_id{$owned_domain_name_id_b};
    my $sort_result = $owned_domain_name_a->{"domain_name"} cmp $owned_domain_name_b->{"domain_name"};
    $sort_result ||= $web_page_a->{"domain_prefix"} cmp $web_page_b->{"domain_prefix"};
    $sort_result ||= $web_page_a->{"uri_path"} cmp $web_page_b->{"uri_path"};
    return $sort_result;
  } keys(%web_page_by_id);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <web_page_list>
---END_BLOCK---;
  for my $web_page_id (@web_page_id_list) {
    my $web_page = $web_page_by_id{$web_page_id};
    my $owned_domain_name_id = $web_page->{"owned_domain_name_id"};
    my $owned_domain_name = $owned_domain_name_by_id{$owned_domain_name_id};
    my $domain_name = HTML::Entities::encode($owned_domain_name->{"domain_name"});
    my $uri_scheme = HTML::Entities::encode($web_page->{"uri_scheme"});
    my $domain_prefix = HTML::Entities::encode($web_page->{"domain_prefix"} || "");
    my $uri_path = HTML::Entities::encode($web_page->{"uri_path"});
    my $note = HTML::Entities::encode($web_page->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <web_page>
      <id>$web_page_id</id>
      <owned_domain_name_id>$owned_domain_name_id</owned_domain_name_id>
      <domain_name>$domain_name</domain_name>
      <uri_scheme>$uri_scheme</uri_scheme>
      <domain_prefix>$domain_prefix</domain_prefix>
      <uri_path>$uri_path</uri_path>
      <note>$note</note>
      <keyword_list>
---END_BLOCK---;
    if ($keyword_list_by_web_page_id{$web_page_id}) {
      for my $keyword (@{$keyword_list_by_web_page_id{$web_page_id}}) {
	my $encoded_keyword = HTML::Entities::encode($keyword);
	$generated_xml .= <<"---END_BLOCK---;";
        <keyword>$encoded_keyword</keyword>
---END_BLOCK---;
      }
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </keyword_list>
      <inbound_link_list>
---END_BLOCK---;
    if ($inbound_link_list_by_web_page_id{$web_page_id}) {
      for my $inbound_link (@{$inbound_link_list_by_web_page_id{$web_page_id}}) {
	my $inbound_link_id = $inbound_link->{"id"};
	my $referrer_url = HTML::Entities::encode($inbound_link->{"referrer_url"});
	my $link_text = HTML::Entities::encode($inbound_link->{"link_text"});
	$generated_xml .= <<"---END_BLOCK---;";
        <inbound_link>
          <id>$inbound_link_id</id>
          <referrer_url>$referrer_url</referrer_url>
          <link_text>$link_text</link_text>
        </inbound_link>
---END_BLOCK---;
      }
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </inbound_link_list>
    </web_page>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </web_page_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_web_pages.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Web Pages
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  Rankings: <a href="/1g/marketing/show_bing_rankings_keywords.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_keywords.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_google_rankings_keywords.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_owned.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_owned.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_owned.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_watched.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_watched.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_watched.cgi">Yahoo</a>)
	</p>
	<xsl:apply-templates select="web_page_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="web_page_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="9">
	  <table cellspacing="0" cellpadding="0" border="0" width="100%">
	    <tr>
	      <td class="standard_table_title" width="10%">&nbsp;</td>
	      <td class="standard_table_title" style="text-align: center; white-space: nowrap;" width="60%">
		&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;
		Web Pages
		&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;
	      </td>
	      <td class="standard_table_title" style="text-align: right" width="10%">
		<a href="/1g/marketing/new_web_page_input.cgi" class="bare_link" style="color: white">New</a>
	      </td>
	    </tr>
	  </table>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>ID</td>
	<td class="standard_table_spacer_column"></td>
	<td>Domain Name / URL / Note</td>
	<td class="standard_table_spacer_column"></td>
	<td>Keywords</td>
	<td class="standard_table_spacer_column"></td>
	<td>Inbound Links</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="web_page"/>
    </table>
  </xsl:template>

  <xsl:template match="web_page">
    <tr>
      <xsl:attribute name="valign">top</xsl:attribute>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="href">/1g/marketing/edit_web_page_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="id"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	Domain Name:
	<xsl:value-of select="domain_name"/>
	<br/>
	URL:
	<xsl:value-of select="uri_scheme"/>
	<xsl:text>://</xsl:text>
	<xsl:if test="domain_prefix != ''">
	  <xsl:value-of select="domain_prefix"/>
	  <xsl:text>.</xsl:text>
	</xsl:if>
	<xsl:value-of select="domain_name"/>
	<xsl:value-of select="uri_path"/>
	<br/>
	Note:
	<xsl:value-of select="note"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:apply-templates select="keyword_list/keyword"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:apply-templates select="inbound_link_list/inbound_link"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="keyword">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <br/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="inbound_link">
    <xsl:value-of select="link_text"/>
    <xsl:text> (from </xsl:text><xsl:value-of select="referrer_url"/><xsl:text>)</xsl:text>
    <xsl:if test="position() != last()">
      <br/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

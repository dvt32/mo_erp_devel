<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Yahoo Search Results
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_monitored_keywords.cgi">Monitored Keywords</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_keywords.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_keywords.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_keywords.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_web_pages.cgi">Web Pages</a>
	  |
	  <a href="/1g/marketing/show_owned_domain_names.cgi">Owned Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_owned.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_owned.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_owned.cgi">Yahoo</a>)
	  |
	  <a href="/1g/marketing/show_watched_domain_names.cgi">Watched Domain Names</a>
	  (Rankings: <a href="/1g/marketing/show_bing_rankings_watched.cgi">Bing</a>
	  &middot;
	  <a href="/1g/marketing/show_rankings_watched.cgi">Google</a>
	  &middot;
	  <a href="/1g/marketing/show_yahoo_rankings_watched.cgi">Yahoo</a>)
	</p>
	<xsl:apply-templates select="yahoo_search_result_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="yahoo_search_result_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="7" class="standard_table_title">
	  yahoo Search Results
	  [keyword = <xsl:value-of select="/page/yahoo_query/query"/>]
	  [date = <xsl:value-of select="substring(/page/yahoo_query/creation_dt, 1, 10)"/>]
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Rank</td>
	<td class="standard_table_spacer_column"></td>
	<td>Domain</td>
	<td class="standard_table_spacer_column"></td>
	<td>URL / Title / Content</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="yahoo_search_result"/>
    </table>
  </xsl:template>

  <xsl:template match="yahoo_search_result">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td style="text-align: right;">
	<xsl:value-of select="search_rank"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="visible_url"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	URL: <xsl:value-of select="unescaped_url"/><br/>
	Title: <xsl:value-of select="title_no_formatting"/><br/>
	Content: <xsl:value-of select="content"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

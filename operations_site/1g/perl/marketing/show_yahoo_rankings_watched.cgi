#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use lead_pad;
use get_row_list;
use get_row_by_id_map;
use get_month_name;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/web");
  # query date could be specified in (query_month, query_day, query_year) or query_date or omitted altogether.
  my ($query_month, $query_day, $query_year) = map {$cgi->param($_)} (qw(query_month query_day query_year));
  my $query_date = $cgi->param("query_date");
  if ($query_date) {
    if ($query_date !~ /^(\d{4})-(\d\d)-(\d\d)$/) {
      die "show_page: show_rankings_watched_bad_query_date.html\n";
    } else {
      ($query_year, $query_month, $query_day) = ($1, $2, $3);
    }
  }
  if ($query_month || $query_day || $query_year) {
    if (($query_month < 1) || ($query_month > 12) ||
	($query_day < 1) || ($query_day > 31) ||
	($query_year < 2008) || ($query_year > 3000)) {
      die "show_page: show_rankings_watched_bad_query_date.html\n";
    }
    $query_date = $query_year . "-" . lead_pad(int($query_month), 2) . "-" . lead_pad(int($query_day), 2);
  }
  # get 5 most recent dates on which keywords were monitored.
  my $sql_query = "select distinct cast(creation_dt as date) as creation_date from yahoo_query_tbl order by creation_date desc limit 5";
  my @monitored_date_row_list;
  get_row_list($dbh, $sql_query, undef, \@monitored_date_row_list);
  my @monitored_date_list = map {$_->{"creation_date"}} @monitored_date_row_list;
  # get watched domain names.
  $sql_query = "select * from watched_domain_name_tbl order by domain_name";
  my (%watched_domain_name_by_id, @watched_domain_name_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%watched_domain_name_by_id, \@watched_domain_name_id_list);
  # get the queries and search results from the specified query date, or the most recent date on which keywords were monitored
  # (if query date is not specified).
  my (%yahoo_query_by_id, @yahoo_query_id_list);
  my (%yahoo_search_result_by_id);
  if (@monitored_date_list) {
    $query_date = $monitored_date_list[0] if (!$query_date);
    $sql_query = "select * from yahoo_query_tbl where cast(creation_dt as date) = ? order by query";
    my @sql_param_list = ($query_date);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%yahoo_query_by_id, \@yahoo_query_id_list);
    if (!@yahoo_query_id_list) {
      die "show_page: show_rankings_watched_no_queries_on_date.html\n";
    }
    $sql_query = "select * from yahoo_search_result_tbl where yahoo_query_id in (select id from yahoo_query_tbl where cast(creation_dt as date) = ?)";
    @sql_param_list = ($query_date);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%yahoo_search_result_by_id);
  }
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <month_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month_list>
  <day_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day_list>
  <monitored_date_list>
---END_BLOCK---;
  for my $monitored_date (@monitored_date_list) {
    $generated_xml .= <<"---END_BLOCK---;";
    <monitored_date>$monitored_date</monitored_date>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </monitored_date_list>
  <query_date>$query_date</query_date>
  <watched_domain_name_list>
---END_BLOCK---;
  for my $watched_domain_name_id (@watched_domain_name_id_list) {
    my $watched_domain_name = $watched_domain_name_by_id{$watched_domain_name_id};
    my $domain_name = HTML::Entities::encode($watched_domain_name->{"domain_name"});
    # scan through the yahoo search results to see if any URLs match current domain name.
    my (@yahoo_search_result_match_list);
    for my $yahoo_search_result (values(%yahoo_search_result_by_id)) {
      $yahoo_search_result->{"url"} =~ m{://([^/]+?)/};
      my $full_server_name = lc($1);
      my $_domain_name;
      if ($full_server_name =~ m|\.([^\.]+\.\w+)$|) {
	$_domain_name = lc($1);
      } else {
	$_domain_name = lc($full_server_name);
      }
      if ($_domain_name eq lc($domain_name)) {
	my $yahoo_query = $yahoo_query_by_id{$yahoo_search_result->{"yahoo_query_id"}};
	push(@yahoo_search_result_match_list, [$yahoo_query->{"query"}, $yahoo_search_result->{"search_rank"}]);
      }
    }
    @yahoo_search_result_match_list = sort {
      my $sort_value = $a->[0] cmp $b->[0];
      $sort_value ||= $a->[1] <=> $b->[1];
      $sort_value;
    } @yahoo_search_result_match_list;
    $generated_xml .= <<"---END_BLOCK---;";
    <watched_domain_name>
      <id>$watched_domain_name_id</id>
      <domain_name>$domain_name</domain_name>
      <yahoo_search_result_match_list>
---END_BLOCK---;
    for my $yahoo_search_result_match (@yahoo_search_result_match_list) {
      my $query = HTML::Entities::encode($yahoo_search_result_match->[0]);
      my $search_rank = $yahoo_search_result_match->[1];
      $generated_xml .= <<"---END_BLOCK---;";
        <yahoo_search_result_match>
          <query>$query</query>
          <search_rank>$search_rank</search_rank>
        </yahoo_search_result_match>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </yahoo_search_result_match_list>
    </watched_domain_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </watched_domain_name_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_yahoo_rankings_watched.xslt", $cgi);
}

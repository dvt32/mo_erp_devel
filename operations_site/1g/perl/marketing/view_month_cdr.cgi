#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use lead_pad;
use days_in_month;
use get_row_by_id_map;
use get_month_name;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  authorize_role($instance_map, "role:marketing/lead_tracking");
  my $year = $cgi->param("year");
  if ($year !~ /^\d{4}$/) {
    die "show_page: view_month_cdr_bad_year.html\n";
  }
  my $month = $cgi->param("month");
  if ((!$month) || ($month < 1) || ($month > 12)) {
    die "show_page: view_month_cdr_bad_month.html\n";
  }
  my $marketing_call_source_id = $cgi->param("marketing_call_source_id");
  my $marketing_call_source;
  if ($marketing_call_source_id =~ /^\d+$/) {
    if (!($marketing_call_source = select_row($dbh, "marketing_call_source_tbl", $marketing_call_source_id))) {
      die "show_page: view_month_cdr_bad_marketing_call_source.html\n";
    }
  } elsif ($marketing_call_source_id eq "uncategorized") {
    # do nothing.
  } else {
    die "show_page: view_month_cdr_bad_marketing_call_source.html\n";
  }
  my $start_datetime = $year . "-" . lead_pad($month, 2) . "-01 00:00:00";
  my $num_days_in_month = days_in_month($month, $year);
  my $end_datetime = $year . "-" . lead_pad($month, 2) . "-$num_days_in_month 23:59:59";
  my $sql_query = "select * from marketing_call_tbl where call_datetime >= ? and call_datetime <= ?";
  my @sql_param_list = ($start_datetime, $end_datetime);
  my (%marketing_call_by_id);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%marketing_call_by_id);
  $sql_query = "select * from marketing_call_source_tbl";
  my (%marketing_call_source_by_id);
  get_row_by_id_map($dbh, $sql_query, undef, \%marketing_call_source_by_id);
  my (%marketing_call_source_list_by_line_number);
  for my $marketing_call_source (values(%marketing_call_source_by_id)) {
    push(@{$marketing_call_source_list_by_line_number{$marketing_call_source->{"line_number"}}}, $marketing_call_source);
  }
  my (@marketing_call_id_list);
  for my $marketing_call (values(%marketing_call_by_id)) {
    my $line_number = $marketing_call->{"line_number"};
    my $call_datetime = $marketing_call->{"call_datetime"};
    if ($marketing_call_source) {
      if (($line_number eq $marketing_call_source->{"line_number"}) && ($marketing_call_source->{"start_datetime"} le $call_datetime) && ($marketing_call_source->{"end_datetime"} ge $call_datetime)) {
	push(@marketing_call_id_list, $marketing_call->{"id"});
      }
    } else {
      # see if this marketing call is uncategorized.
      my $marketing_call_source_list = $marketing_call_source_list_by_line_number{$line_number};
      if ($marketing_call_source_list) {
	my $is_uncategorized = 1;
	for my $_marketing_call_source (@$marketing_call_source_list) {
	  if (($_marketing_call_source->{"start_datetime"} le $call_datetime) && ($_marketing_call_source->{"end_datetime"} ge $call_datetime)) {
	    $is_uncategorized = 0;
	    last;
	  }
	}
	if ($is_uncategorized) {
	  push(@marketing_call_id_list, $marketing_call->{"id"});
	}
      } else {
	push(@marketing_call_id_list, $marketing_call->{"id"});
      }
    }
  }
  my @marketing_call_id_list = sort {
    my $marketing_call_a = $marketing_call_by_id{$a};
    my $marketing_call_b = $marketing_call_by_id{$b};
    return $marketing_call_a->{"call_datetime"} cmp $marketing_call_b->{"call_datetime"};
  } @marketing_call_id_list;
  # populate information for display of page.
  my $month_name = get_month_name($month);
  my $marketing_call_source_name = $marketing_call_source ? HTML::Entities::encode($marketing_call_source->{"name"}) : "Uncategorized";
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <marketing_call_list>
    <year>$year</year>
    <month_name>$month_name</month_name>
    <marketing_call_source>$marketing_call_source_name</marketing_call_source>
---END_BLOCK---;
  for my $marketing_call_id (@marketing_call_id_list) {
    my $marketing_call = $marketing_call_by_id{$marketing_call_id};
    my $call_datetime = $marketing_call->{"call_datetime"};
    my $line_number = $marketing_call->{"line_number"};
    my $callback_number = HTML::Entities::encode($marketing_call->{"callback_number"});
    my $caller_id_name = HTML::Entities::encode($marketing_call->{"caller_id_name"} || "");
    my $minutes = $marketing_call->{"minutes"};
    my $note = HTML::Entities::encode($marketing_call->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <marketing_call>
      <id>$marketing_call_id</id>
      <call_datetime>$call_datetime</call_datetime>
      <line_number>$line_number</line_number>
      <callback_number>$callback_number</callback_number>
      <caller_id_name>$caller_id_name</caller_id_name>
      <minutes>$minutes</minutes>
      <note>$note</note>
    </marketing_call>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </marketing_call_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "view_month_cdr.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Call Details
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  <a href="/1g/marketing/show_call_sources.cgi">Call Sources</a>
	  |
	  <a href="/1g/marketing/find_cdr_input.cgi">Call Details</a>
	</p>
	<xsl:apply-templates select="marketing_call_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="marketing_call_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td class="standard_table_title" colspan="13">
	  Marketing Call Details
	  (<xsl:value-of select="marketing_call_source"/><xsl:text>, </xsl:text>
	  <xsl:value-of select="month_name"/><xsl:text> </xsl:text><xsl:value-of select="year"/>)
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Call Datetime</td>
	<td class="standard_table_spacer_column"></td>
	<td>Line Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Callback Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Caller ID Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Min</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="marketing_call"/>
    </table>
  </xsl:template>

  <xsl:template match="marketing_call">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="href">/1g/marketing/edit_cdr_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="substring(call_datetime, 1, 16)"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="line_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="callback_number"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="caller_id_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td align="right"><xsl:value-of select="minutes"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

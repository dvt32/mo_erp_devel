#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use select_row_with_query;
use military_time_to_am_pm;
use day_of_week;
use get_day_name;
use get_month_name;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load reservation.
  my $reservation_id = $cgi->param("id");
  if ((!defined($reservation_id)) || ($reservation_id !~ /^\d+$/)) {
    die "show_page: delete_reservation_not_found.html\n";
  }
  my $reservation = select_row($dbh, "reservation_tbl", $reservation_id);
  if (!$reservation) {
    die "show_page: delete_reservation_not_found.html\n";
  }
  # delete reservation.
  delete_row($dbh, "reservation_tbl", $reservation_id);
  # load account.
  my $account = select_row($dbh, "account_vw", $reservation->{"account_id"});
  # load room.
  my $room = select_row_with_query($dbh, "select * from room_tbl where id = ? and is_active", [[$reservation->{"room_id"}, SQL_INTEGER]]);
  # determine reservation information needed for display of page.
  my $generated_xml = "";
  $reservation->{"start_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my ($year, $padded_month, $padded_day, $padded_hour, $padded_minute) = ($1, $2, $3, $4, $5);
  my $month = int($padded_month);
  my $day = int($padded_day);
  my $hour = int($padded_hour);
  my $minute = int($padded_minute);
  my $day_of_week = day_of_week($month, $day, $year);
  my $day_name = get_day_name($day_of_week);
  my $month_name = get_month_name($month);
  my $start_time = military_time_to_am_pm(qq{$padded_hour:$padded_minute});
  $reservation->{"end_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my $end_time = military_time_to_am_pm(qq{$4:$5});
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $room_name = HTML::Entities::encode($room->{"short_name"});
  my $note = HTML::Entities::encode($reservation->{"note"} || "");
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <room-reservation>
    <account-name>$account_name</account-name>
    <room-name>$room_name</room-name>
    <day-name>$day_name</day-name>
    <day-number>$day</day-number>
    <month-name>$month_name</month-name>
    <year>$year</year>
    <start-time>$start_time</start-time>
    <end-time>$end_time</end-time>
    <note>$note</note>
  </room-reservation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_reservation_success.xslt", $cgi);
}

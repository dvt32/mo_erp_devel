#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use select_row_with_query;
use get_row_list;
use military_time_to_am_pm;
use day_of_week;
use get_day_name;
use get_month_name;
use minutes_to_display_time;
use display_time_to_minutes;
use generate_xml_for_room_schedule;
use get_building_id_from_floor_id;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if reservation id is specified.
  my $reservation_id = $cgi->param("id");
  if ((!defined($reservation_id)) || ($reservation_id !~ /^\d+$/)) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  # load reservation.
  my $reservation = select_row($dbh, "reservation_tbl", $reservation_id);
  if (!$reservation) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  # parse the start date.
  $reservation->{"start_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my ($year, $padded_month, $padded_day, $padded_hour, $padded_minute) = ($1, $2, $3, $4, $5);
  my $query_date = "$year-$padded_month-$padded_day";
  # load room attached to the reservation.
  my $room_id = $reservation->{"room_id"};
  my $room = select_row_with_query($dbh, "select * from room_tbl where id = ? and is_active", [[$room_id, SQL_INTEGER]]);
  my $floor_id = $room->{"floor_id"};
  # fetch all rooms on the same floor.
  my @room_list;
  my $sql_query = "select * from room_tbl where floor_id = ? and is_active order by short_name";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@room_list);
  # time slots are in 15 minute increments. time slot increment must divide evenly into 60.
  my $time_slot_increment = 15;
  my $room_schedule_xml = generate_xml_for_room_schedule
    ("database_handle" => $dbh,
     "floor_id" => $floor_id,
     "query_date" => $query_date,
     "room_list" => \@room_list,
     "time_slot_increment" => $time_slot_increment);
  # fetch the linked account.
  my $account_id = $reservation->{"account_id"};
  my $account = select_row($dbh, "account_vw", $account_id);
  # fetch all accounts in the building.
  my @account_list;
  my $building_id = get_building_id_from_floor_id($dbh, $floor_id);
  $sql_query = "select * from account_vw where (id = ?) or (is_active and (primary_floor_id in (select id from floor_tbl where building_id = ?))) order by name";
  @sql_param_list = ([$account_id_of_corporate, SQL_INTEGER], [$building_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@account_list);
  # populate information needed for display of page.
  my $month = int($padded_month);
  my $day = int($padded_day);
  my $hour = int($padded_hour);
  my $minute = int($padded_minute);
  my $day_of_week = day_of_week($month, $day, $year);
  my $day_name = get_day_name($day_of_week);
  my $month_name = get_month_name($month);
  my $start_time = military_time_to_am_pm(qq{$padded_hour:$padded_minute});
  $reservation->{"end_dt"} =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my $end_time = military_time_to_am_pm(qq{$4:$5});
  my $existing_start_time_in_minutes = display_time_to_minutes($start_time);
  my $existing_duration_in_minutes = display_time_to_minutes($end_time) - $existing_start_time_in_minutes;
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $room_name = HTML::Entities::encode($room->{"short_name"});
  my $note = HTML::Entities::encode($reservation->{"note"} || "");
  # determine today's date.
  my ($today_year, $today_month_number, $today_day_number, $today_day_of_week) = (localtime(time()))[5, 4, 3, 6];
  $today_year += 1900;
  $today_month_number++;
  my $today_day_name = get_day_name($today_day_of_week);
  my $today_month_name = get_month_name($today_month_number);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <day-name>$today_day_name</day-name>
    <month-name>$today_month_name</month-name>
    <day-number>$today_day_number</day-number>
    <year>$today_year</year>
  </today>
  <existing-room-reservation>
    <reservation-id>$reservation_id</reservation-id>
    <account-id>$account_id</account-id>
    <account-name>$account_name</account-name>
    <day-name>$day_name</day-name>
    <day-number>$day</day-number>
    <month-name>$month_name</month-name>
    <year>$year</year>
    <start-time>$start_time</start-time>
    <end-time>$end_time</end-time>
    <room-id>$room_id</room-id>
    <room-name>$room_name</room-name>
    <note>$note</note>
  </existing-room-reservation>
  <action-list>
    <reserve-room>
      <date>
        <day-name>$day_name</day-name>
        <day-number>$day</day-number>
        <month-name>$month_name</month-name>
        <year>$year</year>
      </date>
      <account-list>
---END_BLOCK---;
  for $account (@account_list) {
    $account_id = $account->{"id"};
    $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
        <account>
          <id>$account_id</id>
          <name>$account_name</name>
        </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </account-list>
      <room-select>
---END_BLOCK---;
  for $room (@room_list) {
    $room_id = $room->{"id"};
    $room_name = HTML::Entities::encode($room->{"short_name"});
    $generated_xml .= <<"---END_BLOCK---;";
        <room>
          <id>$room_id</id>
          <name>$room_name</name>
        </room>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </room-select>
      <time-select>
---END_BLOCK---;
  for(my $time_slot = 8*60; $time_slot < 24*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    if ($time_slot == $existing_start_time_in_minutes) {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value" selected="selected">$option_label</option>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value">$option_label</option>
---END_BLOCK---;
    }
  }
  for(my $time_slot = 0; $time_slot < 8*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    if ($time_slot == $existing_start_time_in_minutes) {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value" selected="selected">$option_label</option>
---END_BLOCK---;
    } else {
      $generated_xml .= <<"---END_BLOCK---;";
        <option value="$option_value">$option_label</option>
---END_BLOCK---;
    }
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </time-select>
      <duration-select>
---END_BLOCK---;
  my @duration_list =
    ([15, "15 minutes"],
     [30, "30 minutes"],
     [45, "45 minutes"],
     [60, "1 hour"],
     [90, "1.5 hours"],
     [120, "2 hours"],
     [150, "2.5 hours"],
     [180, "3 hours"],
     [210, "3.5 hours"],
     [240, "4 hours"],
     );
  for my $duration_entry (@duration_list) {
    my $duration_in_minutes = $duration_entry->[0];
    my $duration_label = $duration_entry->[1];
    my $selected_attribute = "";
    if ($duration_in_minutes == $existing_duration_in_minutes) {
      $selected_attribute = qq{ selected="selected"};
    }
    $generated_xml .= <<"---END_BLOCK---;";
        <option value="$duration_in_minutes"$selected_attribute>$duration_label</option>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </duration-select>
      <note>$note</note>
    </reserve-room>
  </action-list>
---END_BLOCK---;
  $generated_xml .= $room_schedule_xml;
  $generated_xml .= <<"---END_BLOCK---;";
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_reservation_input.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use update_row;
use select_row;
use select_row_with_query;
use military_time_to_am_pm;
use day_of_week;
use get_day_name;
use get_month_name;
use minutes_to_display_time;
use get_building_id_from_floor_id;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that the start time is valid.
  my $start_time = $cgi->param("time");
  if ((!defined($start_time)) || ($start_time eq "") || ($start_time !~ /^\d+$/) || ($start_time < 0) || ($start_time > 24 * 60)) {
    die "show_page: edit_reservation_bad_start_time.html\n";
  }
  # verify that the duration is valid.
  my $duration = $cgi->param("duration");
  if ((!$duration) || ($duration !~ /^\d+$/) || ($duration < 0) || ($duration > 4 * 60)) {
    die "show_page: edit_reservation_bad_duration.html\n";
  }
  # verify that the start time plus the duration do not exceed midnight (i.e. cross over into a new day).
  if (($start_time + $duration) > 24 * 60) {
    die "show_page: edit_reservation_day_boundary_error.html\n";
  }
  # load reservation.
  my $reservation_id = $cgi->param("reservation_id");
  if ((!defined($reservation_id)) || ($reservation_id !~ /^\d+$/)) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  my $reservation = select_row($dbh, "reservation_tbl", $reservation_id);
  if (!$reservation) {
    die "show_page: edit_reservation_not_found.html\n";
  }
  # verify account is valid.
  my $account_id = $cgi->param("account_id");
  my $account;
  if ((!defined($account_id)) || ($account_id !~ /^\d+$/) || (!($account = select_row($dbh, "account_vw", $account_id)))) {
    die "show_page: edit_reservation_bad_account.html\n";
  }
  # verify room is valid.
  my $room_id = $cgi->param("room_id");
  if ((!defined($room_id)) || ($room_id !~ /^\d+$/)) {
    die "show_page: edit_reservation_bad_room.html\n";
  }
  # load room.
  my $room = select_row_with_query($dbh, "select * from room_tbl where id = ? and is_active", [[$room_id, SQL_INTEGER]]);
  # verify room exists.
  if (!$room) {
    die "show_page: edit_reservation_bad_room.html\n";
  }
  # verify account is in same building as room.
  my $account_floor_id = $account->{"primary_floor_id"};
  my $account_building_id = get_building_id_from_floor_id($dbh, $account_floor_id);
  my $room_floor_id = $room->{"floor_id"};
  my $room_building_id = get_building_id_from_floor_id($dbh, $room_floor_id);
  if (($account_id != $account_id_of_corporate) && ($account_building_id != $room_building_id)) {
    die "show_page: edit_reservation_building_mismatch.html\n";
  }
  # calculate start and end timestamps.
  $reservation->{"start_dt"} =~ /^(\d{4}-\d\d-\d\d)/;
  my $reservation_date = $1;
  my $start_dt = $reservation_date . " " . minutes_to_display_time($start_time) . ":00";
  my $end_dt = $reservation_date . " " . minutes_to_display_time($start_time + $duration) . ":00";
  # get the note.
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of inserting the reservation row.
  $dbh->begin_work();
  # wrap in an eval to catch exceptions.
  eval {
    # raise an exception if there is a database error (such as transaction error).
    local $dbh->{"RaiseError"} = 1;
    my $sql_query = "select * from reservation_tbl where id <> ? and room_id = ? and (start_dt, end_dt) overlaps (timestamp ?, timestamp ?)";
    my @sql_param_list = ([$reservation_id, SQL_INTEGER], [$room_id, SQL_INTEGER], $start_dt, $end_dt);
    # there is an overlapping reservation in the same room. roll back the transaction and display an error.
    if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
      die "overlap\n";
    }
    # no problems, so update the reservation row.
    # issue the update command to the database.
    my $db_charset = get_database_charset($instance_map, "main");
    my (@sql_column_list);
    push(@sql_column_list, ["room_id", $room_id, SQL_INTEGER]);
    push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
    push(@sql_column_list, ["start_dt", $start_dt]);
    push(@sql_column_list, ["end_dt", $end_dt]);
    push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
    update_row($dbh, "reservation_tbl", \@sql_column_list, $reservation_id);
    # commit the transaction.
    $dbh->commit();
  };
  my $transaction_err = $@;
  if ($transaction_err) {
    # remove trailing newline from exception message.
    chomp($transaction_err);
    # now rollback to undo the incomplete changes but do it in an eval{} as it may also fail.
    eval { $dbh->rollback(); };
  }
  # if error, display appropriate page.
  if (!$transaction_err) {
    # do nothing. continue with script.
  } elsif ($transaction_err eq "overlap") {
    die "show_page: edit_reservation_overlap_error.html\n";
  } else {
    die $transaction_err;
  }
  # determine reservation information needed for display of page.
  my $generated_xml = "";
  $start_dt =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my ($year, $padded_month, $padded_day, $padded_hour, $padded_minute) = ($1, $2, $3, $4, $5);
  my $month = int($padded_month);
  my $day = int($padded_day);
  my $hour = int($padded_hour);
  my $minute = int($padded_minute);
  my $day_of_week = day_of_week($month, $day, $year);
  my $day_name = get_day_name($day_of_week);
  my $month_name = get_month_name($month);
  my $start_time = military_time_to_am_pm(qq{$padded_hour:$padded_minute});
  $end_dt =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):\d\d/;
  my $end_time = military_time_to_am_pm(qq{$4:$5});
  my $room_name = HTML::Entities::encode($room->{"short_name"});
  my $account_name = HTML::Entities::encode($account->{"name"});
  $note = HTML::Entities::encode($note);
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <room-reservation>
    <account-name>$account_name</account-name>
    <room-name>$room_name</room-name>
    <day-name>$day_name</day-name>
    <day-number>$day</day-number>
    <month-name>$month_name</month-name>
    <year>$year</year>
    <start-time>$start_time</start-time>
    <end-time>$end_time</end-time>
    <note>$note</note>
  </room-reservation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_reservation_result_success.xslt", $cgi);
}

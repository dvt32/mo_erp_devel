#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use select_row_with_query;
use insert_with_statement;
use minutes_to_display_time;
use day_of_week;
use get_day_name;
use get_month_name;
use get_building_id_from_floor_id;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify the format of the date parameter and parse the date.
  my $reservation_date = $cgi->param("date");
  if ((!$reservation_date) || ($reservation_date !~ /^(\d{4})-(\d\d)-(\d\d)$/)) {
    die "show_page: reserve_room_bad_date.html\n";
  }
  # verify that the start time is valid.
  my $start_time = $cgi->param("time");
  if ((!defined($start_time)) || ($start_time eq "") ||
      ($start_time !~ /^\d+$/) ||
      ($start_time < 0) || ($start_time > 24 * 60)) {
    die "show_page: reserve_room_bad_start_time.html\n";
  }
  # verify that the duration is valid.
  my $duration = $cgi->param("duration");
  if ((!$duration) || ($duration !~ /^\d+$/) || ($duration < 0) || ($duration > 4 * 60)) {
    die "show_page: reserve_room_bad_duration.html\n";
  }
  # verify that the start time plus the duration do not exceed midnight (i.e. cross over into a new day).
  if (($start_time + $duration) > 24 * 60) {
    die "show_page: reserve_room_day_boundary_error.html\n";
  }
  # verify that the room was specified.
  my $room_id = $cgi->param("room_id");
  if ((!defined($room_id)) || ($room_id !~ /^\d+$/)) {
    die "show_page: reserve_room_bad_room.html\n";
  }
  # verify that the account was specified and exists.
  my $account_id = $cgi->param("account_id");
  my $account;
  if ((!defined($account_id)) || ($account_id !~ /^\d+$/) || (!($account = select_row($dbh, "account_vw", $account_id)))) {
    die "show_page: reserve_room_bad_account.html\n";
  }
  # verify that the specified room is in the same building as the account.
  my $room = select_row($dbh, "room_tbl", $room_id);
  if ($room) {
    if ($account_id != $account_id_of_corporate) {
      my $room_building_id = get_building_id_from_floor_id($dbh, $room->{"floor_id"});
      my $account_building_id = get_building_id_from_floor_id($dbh, $account->{"primary_floor_id"});
      if ($room_building_id != $account_building_id) {
	die "show_page: reserve_room_bad_room.html\n";
      }
    }
  } else { 
    # room did not exist.
    die "show_page: reserve_room_bad_room.html\n";
  }
  my $start_dt = $reservation_date . " " . minutes_to_display_time($start_time) . ":00";
  my $end_dt = $reservation_date . " " . minutes_to_display_time($start_time + $duration) . ":00";
  # get the note.
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of inserting the reservation row.
  $dbh->begin_work();
  my $sql_query = "select * from reservation_tbl where room_id = ? and (start_dt, end_dt) overlaps (timestamp ?, timestamp ?)";
  my @sql_param_list = ([$room_id, SQL_INTEGER], $start_dt, $end_dt);
  if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    # there is an overlapping reservation in the same room. roll back the transaction and display an error.
    $dbh->rollback();
    die "show_page: reserve_room_overlap_error.html\n";
  }
  # no problems, so insert the new reservation row.
  my $sql_statement = "insert into reservation_tbl (room_id, account_id, start_dt, end_dt, note) values (?, ?, timestamp ?, timestamp ?, ?)";
  my $db_charset = get_database_charset($instance_map, "main");
  @sql_param_list = ([$room_id, SQL_INTEGER], [$account_id, SQL_INTEGER], $start_dt, $end_dt, Encode::encode($db_charset, $note));
  insert_with_statement($dbh, $sql_statement, \@sql_param_list);
  # commit the transaction.
  $dbh->commit();
  # determine reservation information needed for display of page.
  my $account_name = HTML::Entities::encode($account->{"name"});
  $reservation_date =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  my ($year, $month, $day) = ($1, $2, $3);
  my $day_name = get_day_name(day_of_week($month, $day, $year));
  my $month_name = get_month_name($month);
  my $day_number = $day;
  my $start_time_display = minutes_to_display_time($start_time, 1);
  my $end_time_display = minutes_to_display_time($start_time + $duration, 1);
  my $room_name = HTML::Entities::encode($room->{"short_name"});
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <room-reservation>
    <account-name>$account_name</account-name>
    <day-name>$day_name</day-name>
    <month-name>$month_name</month-name>
    <day-number>$day_number</day-number>
    <year>$year</year>
    <start-time>$start_time_display</start-time>
    <end-time>$end_time_display</end-time>
    <room-name>$room_name</room-name>
    <note>$note</note>
  </room-reservation>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "reserve_room_success.xslt", $cgi);
}

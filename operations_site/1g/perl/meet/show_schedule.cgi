#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_list;
use lead_pad;
use get_month_name;
use get_day_name;
use day_of_week;
use minutes_to_display_time;
use generate_xml_for_room_schedule;
use generate_xml_for_calendar_month;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $room_type = $cgi->param("room_type");
  if ((!$room_type) || ($room_type !~ /^(conference|therapy)$/)) {
    die "show_page: show_schedule_bad_room_type.html\n";
  }
  # verify that floor id was specified and is valid.
  my $floor_id = $cgi->param("floor_id");
  my $floor;
  if ((defined($floor_id)) && ($floor_id =~ /^\d+$/)) {
    $floor = select_row($dbh, "floor_tbl", $floor_id);
  }
  if (!$floor) {
    die "show_page: show_schedule_bad_floor.html\n";
  }
  my $building_id = $floor->{"building_id"};
  # determine today's date.
  my ($today_year, $today_month_number, $today_day_number, $today_day_of_week) = (localtime(time()))[5, 4, 3, 6];
  $today_year += 1900;
  $today_month_number++;
  my $today_day_name = get_day_name($today_day_of_week);
  my $today_month_name = get_month_name($today_month_number);
  # check if date was specified as a parameter.
  my ($schedule_year, $schedule_month_number, $schedule_day_number, $schedule_day_of_week);
  my ($schedule_padded_month_number, $schedule_padded_day_number, $schedule_day_name, $schedule_month_name);
  my $query_date = $cgi->param("date");
  if ($query_date) {
    if ($query_date =~ /^(\d{4})-(\d\d)-(\d\d)$/) {
      ($schedule_year, $schedule_month_number, $schedule_day_number) = (int($1), int($2), int($3));
      $schedule_day_of_week = day_of_week($schedule_month_number, $schedule_day_number, $schedule_year);
      $schedule_padded_month_number = lead_pad($schedule_month_number, 2);
      $schedule_padded_day_number = lead_pad($schedule_day_number, 2);
      $schedule_day_name = get_day_name($schedule_day_of_week);
      $schedule_month_name = get_month_name($schedule_month_number);
    } else {
      die "show_page: show_schedule_bad_date.html\n";
    }
  } else {
    ($schedule_year, $schedule_month_number, $schedule_day_number, $schedule_day_of_week) =
      ($today_year, $today_month_number, $today_day_number, $today_day_of_week);
    $schedule_padded_month_number = lead_pad($schedule_month_number, 2);
    $schedule_padded_day_number = lead_pad($schedule_day_number, 2);
    $schedule_day_name = get_day_name($schedule_day_of_week);
    $schedule_month_name = get_month_name($schedule_month_number);
    $query_date = "$schedule_year-$schedule_padded_month_number-$schedule_padded_day_number";
  }
  # fetch accounts in the building.
  my @account_list;
  my $sql_query = "select distinct * from account_vw where (id = ?) or (is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)) order by name";
  my @sql_param_list = ([$account_id_of_corporate, SQL_INTEGER], [$building_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@account_list);
  # fetch rooms on the floor.
  my (@room_list);
  $sql_query = "select * from room_tbl where is_active and floor_id = ? and room_type = ? order by short_name";
  @sql_param_list = ([$floor_id, SQL_INTEGER], $room_type);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@room_list);
  # time slots are in 15 minute increments. time slot increment must divide evenly into 60.
  my $time_slot_increment = 15;
  my $room_schedule_xml = generate_xml_for_room_schedule
    ("database_handle" => $dbh,
     "floor_id" => $floor_id,
     "query_date" => $query_date,
     "room_list" => \@room_list,
     "time_slot_increment" => $time_slot_increment);
  $room_type = HTML::Entities::encode($room_type);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <today>
    <day-name>$today_day_name</day-name>
    <month-name>$today_month_name</month-name>
    <day-number>$today_day_number</day-number>
    <year>$today_year</year>
  </today>
  <room_type>$room_type</room_type>
  <floor_id>$floor_id</floor_id>
---END_BLOCK---;
  $generated_xml .= $room_schedule_xml;
  $generated_xml .= <<"---END_BLOCK---;";
  <action-list>
    <reserve-room>
      <date>
        <year>$schedule_year</year>
        <month-name>$schedule_month_name</month-name>
        <month-number>$schedule_month_number</month-number>
        <padded-month-number>$schedule_padded_month_number</padded-month-number>
        <day-number>$schedule_day_number</day-number>
        <padded-day-number>$schedule_padded_day_number</padded-day-number>
        <day-name>$schedule_day_name</day-name>
      </date>
      <account-select>
        <account-list>
---END_BLOCK---;
  for my $account (@account_list) {
    my $account_id = $account->{"id"};
    my $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
          <account>
            <id>$account_id</id>
            <name>$account_name</name>
          </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
        </account-list>
      </account-select>
      <room-select>
        <room-list>
---END_BLOCK---;
  for my $room (@room_list) {
    my $room_id = $room->{"id"};
    my $room_name = HTML::Entities::encode($room->{"short_name"});
    $generated_xml .= <<"---END_BLOCK---;";
          <room>
            <room-id>$room_id</room-id>
            <room-name>$room_name</room-name>
          </room>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
        </room-list>
      </room-select>
      <time-select>
---END_BLOCK---;
  for(my $time_slot = 8*60; $time_slot < 24*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    # 9 AM should be selected by default.
    my $is_selected = ($time_slot == 9*60) ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
        <time-option>
          <label>$option_label</label>
          <value>$option_value</value>
          <is_selected>$is_selected</is_selected>
        </time-option>
---END_BLOCK---;
  }
  for(my $time_slot = 0; $time_slot < 8*60; $time_slot += $time_slot_increment) {
    my $option_value = $time_slot;
    my $option_label = minutes_to_display_time($time_slot, 1);
    # 9 AM should be selected by default.
    my $is_selected = ($time_slot == 9*60) ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
        <time-option>
          <label>$option_label</label>
          <value>$option_value</value>
          <is_selected>$is_selected</is_selected>
        </time-option>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </time-select>
      <duration-select>
        <duration-option>
          <label>15 minutes</label>
          <value>15</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>30 minutes</label>
          <value>30</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>45 minutes</label>
          <value>45</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>1 hour</label>
          <value>60</value>
          <is_selected>1</is_selected>
        </duration-option>
        <duration-option>
          <label>1.5 hours</label>
          <value>90</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>2 hours</label>
          <value>120</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>2.5 hours</label>
          <value>150</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>3 hours</label>
          <value>180</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>3.5 hours</label>
          <value>210</value>
          <is_selected>0</is_selected>
        </duration-option>
        <duration-option>
          <label>4 hours</label>
          <value>240</value>
          <is_selected>0</is_selected>
        </duration-option>
      </duration-select>
    </reserve-room>
    <switch-day>
      <calendar-month-list>
---END_BLOCK---;
  # generate calendar for the current month and two following months.
  for(my $i = 0; $i < 3; $i++) {
    my $year = $today_year;
    my $month_number = $today_month_number + $i;
    if ($month_number > 12) {
      $month_number -= 12;
      $year++;
    }
    $generated_xml .= generate_xml_for_calendar_month($year, $month_number);
  }
  $generated_xml .= <<"---END_BLOCK---;";
      </calendar-month-list>
    </switch-day>
  </action-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_schedule.xslt", $cgi);
}

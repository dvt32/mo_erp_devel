<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Room Schedules
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="schedule"/>
	<br/>
	<xsl:apply-templates select="action-list"/>
	<p>
	  Today is 
	  <xsl:value-of select="today/day-name"/>,
	  <xsl:value-of select="today/month-name"/><xsl:text> </xsl:text><xsl:value-of select="today/day-number"/>,
	  <xsl:value-of select="today/year"/>. Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="schedule">
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
	<td class="schedule_date_header">
	  <xsl:attribute name="colspan"><xsl:value-of select="count(room-list/room) * 2 + 1"/></xsl:attribute>
          <xsl:apply-templates select="date"/>
	</td>
      </tr>
      <xsl:apply-templates select="room-list"/>
      <xsl:apply-templates select="reservation-list"/>
    </table>
  </xsl:template>

  <xsl:template match="date">
    <a>
      <xsl:attribute name="class">adjacent_chevron_link</xsl:attribute>
      <xsl:attribute name="href">
	<xsl:text>/1g/meet/show_schedule.cgi?floor_id=</xsl:text>
	<xsl:value-of select="/page/floor_id"/>
	<xsl:text>&amp;room_type=</xsl:text>
	<xsl:value-of select="/page/room_type"/>
	<xsl:text>&amp;date=</xsl:text>
	<xsl:value-of select="previous-date"/>
      </xsl:attribute>
      &lt;&lt;
    </a>
    <xsl:value-of select="day-name"/>,
    <xsl:value-of select="month-name"/><xsl:text> </xsl:text><xsl:value-of select="day-number"/>, <xsl:value-of select="year"/>
    (<xsl:value-of select="year"/>-<xsl:value-of select="padded-month-number"/>-<xsl:value-of select="padded-day-number"/>)
    <a>
      <xsl:attribute name="class">adjacent_chevron_link</xsl:attribute>
      <xsl:attribute name="href">
	<xsl:text>/1g/meet/show_schedule.cgi?floor_id=</xsl:text>
	<xsl:value-of select="/page/floor_id"/>
	<xsl:text>&amp;room_type=</xsl:text>
	<xsl:value-of select="/page/room_type"/>
	<xsl:text>&amp;date=</xsl:text>
	<xsl:value-of select="next-date"/>
      </xsl:attribute>
      &gt;&gt;
    </a>
  </xsl:template>

  <xsl:template match="room-list">
    <tr>
      <td class="schedule_upper_left_corner">
	Time
      </td>
      <xsl:for-each select="room">
	<td class="schedule_header_spacer_column"></td>
	<td class="schedule_room_name_header">
	  <xsl:apply-templates select="room-name"/>
	</td>
      </xsl:for-each>
    </tr>
  </xsl:template>

  <xsl:template match="room-name">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="reservation-list">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="time-block">
    <tr>
      <td class="schedule_time">
	<xsl:apply-templates select="start-time"/>
      </td>
      <xsl:for-each select="room-reservation">
	<td class="schedule_spacer_column"></td>
	<xsl:apply-templates/>
      </xsl:for-each>
    </tr>
  </xsl:template>

  <xsl:template match="start-time">
    <xsl:value-of select="display-time"/>
  </xsl:template>

  <xsl:template match="available-slot">
    <td class="schedule_reservation_available_slot">
      ---
    </td>
  </xsl:template>

  <xsl:template match="occupied-slot">
    <td>
      <xsl:attribute name="class">
	<xsl:if test="(is-start = 'true') and (is-end = 'true')">schedule_reservation_occupied_slot_start_and_end</xsl:if>
	<xsl:if test="(is-start = 'false') and (is-end = 'false')">schedule_reservation_occupied_slot_middle</xsl:if>
	<xsl:if test="(is-start = 'true') and (is-end = 'false')">schedule_reservation_occupied_slot_start</xsl:if>
	<xsl:if test="(is-start = 'false') and (is-end = 'true')">schedule_reservation_occupied_slot_end</xsl:if>
      </xsl:attribute>
      <a>
	<xsl:attribute name="href">
	  <xsl:text>/1g/meet/edit_reservation_input.cgi?id=</xsl:text>
	  <xsl:value-of select="reservation-id"/>
	</xsl:attribute>
	<xsl:attribute name="class">
	  <xsl:text>edit_reservation_link</xsl:text>
	</xsl:attribute>
	<xsl:value-of select="account-name"/>
      </a>
    </td>
  </xsl:template>

  <xsl:template match="action-list">
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
	<td class="schedule_action_header">
	  Reserve a Room
	</td>
	<xsl:if test="/page/floor_id=2">
	  <td rowspan="4">
	    <img src="/1g/img/conference_room_diagram.png" alt="conference room diagram" style="padding: 4px"/>
	  </td>
	</xsl:if>
      </tr>
      <tr>
	<td class="schedule_action_body">
	  <xsl:apply-templates select="reserve-room"/>
	  <p>The "Note" field is only visible to the tenant who owns the reservation.</p>
	</td>
      </tr>
    </table>
    <br/>
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
	<td class="schedule_action_header">
	  View Another Day
	</td>
      </tr>
      <tr>
	<td class="schedule_action_body">
	  <xsl:apply-templates select="switch-day"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="reserve-room">
    <form action="/1g/meet/reserve_room.cgi" method="post">
      <fieldset class="standard_fieldset">
	<legend>
	  <xsl:value-of select="date/day-name"/>,
	  <xsl:value-of select="date/month-name"/><xsl:text> </xsl:text>
	  <xsl:value-of select="date/day-number"/>,
	  <xsl:value-of select="date/year"/>
	  (see "View Another Day" below to switch days)
	</legend>
	<input>
	  <xsl:attribute name="type">hidden</xsl:attribute>
	  <xsl:attribute name="name">date</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="date/year"/>
	    <xsl:text>-</xsl:text>
	    <xsl:value-of select="date/padded-month-number"/>
	    <xsl:text>-</xsl:text>
	    <xsl:value-of select="date/padded-day-number"/>
	  </xsl:attribute>
	</input>
	<table cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <td style="padding: 4px; ">
	      Account:
	    </td>
	    <td>
	      <xsl:apply-templates select="account-select"/>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      Room:
	    </td>
	    <td>
	      <xsl:apply-templates select="room-select"/>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      Start:
	    </td>
	    <td>
	      <xsl:apply-templates select="time-select"/>
	      Duration:
	      <xsl:apply-templates select="duration-select"/>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      Note:
	    </td>
	    <td>
	      <input type="text" name="note"/>
	      <input type="submit" name="submitButton" value="Save"/>
	    </td>
	  </tr>
	</table>
      </fieldset>
    </form>
  </xsl:template>

  <xsl:template match="account-select">
    <select name="account_id">
      <xsl:for-each select="account-list/account">
	<option>
	  <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="name"/>
	</option>
      </xsl:for-each>
    </select>
  </xsl:template>

  <xsl:template match="room-select">
    <select name="room_id">
      <xsl:for-each select="room-list/room">
	<option>
	  <xsl:attribute name="value"><xsl:value-of select="room-id"/></xsl:attribute>
	  <xsl:value-of select="room-name"/>
	</option>
      </xsl:for-each>
    </select>
  </xsl:template>

  <xsl:template match="time-select">
    <select name="time">
      <xsl:apply-templates select="time-option"/>
    </select>
  </xsl:template>

  <xsl:template match="time-option">
    <option>
      <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
      <xsl:if test="is_selected = 1">
	<xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
      <xsl:value-of select="label"/>
    </option>
  </xsl:template>

  <xsl:template match="duration-select">
    <select name="duration">
      <xsl:apply-templates select="duration-option"/>
    </select>
  </xsl:template>

  <xsl:template match="duration-option">
    <option>
      <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
      <xsl:if test="is_selected = 1">
	<xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
      <xsl:value-of select="label"/>
    </option>
  </xsl:template>

  <xsl:template match="switch-day">
    <br/>
    <table cellspacing="0" cellpadding="0" border="0">
      <tr valign="top">
	<xsl:apply-templates/>
      </tr>
    </table>
    <br/>
  </xsl:template>

  <xsl:template match="calendar-month-list">
    <td class="calendar_month_spacer_column">
      <xsl:text> </xsl:text>
    </td>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="calendar-month">
    <td>
      <table cellspacing="0" cellpadding="0" border="0">
	<tr>
	  <td colspan="7" class="calendar_month_name_header">
	    <xsl:value-of select="month-name"/>
	    <xsl:text> </xsl:text>
	    <xsl:value-of select="year"/>
	  </td>
	</tr>
	<tr>
	  <td class="calendar_day_of_week_header">S</td>
	  <td class="calendar_day_of_week_header">M</td>
	  <td class="calendar_day_of_week_header">T</td>
	  <td class="calendar_day_of_week_header">W</td>
	  <td class="calendar_day_of_week_header">T</td>
	  <td class="calendar_day_of_week_header">F</td>
	  <td class="calendar_day_of_week_header">S</td>
	</tr>
	<xsl:apply-templates select="week-list"/>
      </table>
    </td>
    <td class="calendar_month_spacer_column">
      <xsl:text> </xsl:text>
    </td>
  </xsl:template>

  <xsl:template match="week-list">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="week">
    <tr>
      <xsl:apply-templates/>
    </tr>
  </xsl:template>

  <xsl:template match="day-blank-cell">
    <td>
      <xsl:text> </xsl:text>
    </td>
  </xsl:template>

  <xsl:template match="day">
    <td class="calendar_day_cell">
      <a>
	<xsl:attribute name="class">calendar_day_link</xsl:attribute>
	<xsl:attribute name="href">
	  <xsl:text>/1g/meet/show_schedule.cgi?floor_id=</xsl:text>
	  <xsl:value-of select="/page/floor_id"/>
	  <xsl:text>&amp;room_type=</xsl:text>
	  <xsl:value-of select="/page/room_type"/>
	  <xsl:text>&amp;date=</xsl:text>
	  <xsl:value-of select="../../../year"/>
	  <xsl:text>-</xsl:text>
	  <xsl:value-of select="../../../padded-month-number"/>
	  <xsl:text>-</xsl:text>
	  <xsl:value-of select="padded-day-number"/>
	</xsl:attribute>
	<xsl:value-of select="day-number"/>
      </a>
    </td>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

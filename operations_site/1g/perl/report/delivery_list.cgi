#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use lead_pad;
use read_file;
use select_row;
use get_date_now;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $template_data = read_file("../resource/delivery_list_template.xml");
  my $building_id = $cgi->param("building_id");
  if (!$building_id) {
    die "show_page: delivery_list_missing_building_id.html\n";
  }
  # fetch information on the building.
  my $building = select_row($dbh, "building_tbl", $building_id);
  # get all floors in the building.
  my $sql_query = "select * from floor_tbl where building_id = ?";
  my @sql_param_list = ([$building_id, SQL_INTEGER]);
  my %floor_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%floor_by_id);
  # select all accounts linked to floors in the building.
  $sql_query = "select * from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  my %account_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # select all aliases linked to accounts.
  $sql_query = "select * from alias_tbl where account_id in (select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?))";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  my %alias_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%alias_by_id);
  # select all persons linked to accounts.
  $sql_query = "select * from person_vw where is_active and account_id in (select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?))";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  my %person_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%person_by_id);
  # merge account names, aliases, and person names into one big list and sort it.
  my @name_list;
  for my $id (keys(%account_by_id)) {
    my $account = $account_by_id{$id};
    my $floor = $floor_by_id{$account->{"primary_floor_id"}};
    push(@name_list, { "name" => $account->{"name"},
		       "type" => "account",
		       "floor" => $floor->{"short_local_label"}, });
  }
  for my $id (keys(%alias_by_id)) {
    my $alias = $alias_by_id{$id};
    my $account = $account_by_id{$alias->{"account_id"}};
    my $floor = $floor_by_id{$account->{"primary_floor_id"}};
    push(@name_list, { "name" => $alias->{"name"},
		       "type" => "alias",
		       "floor" => $floor->{"short_local_label"}, });
  }
  for my $id (keys(%person_by_id)) {
    my $person = $person_by_id{$id};
    my $account = $account_by_id{$person->{"account_id"}};
    my $floor = $floor_by_id{$account->{"primary_floor_id"}};
    my $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
    if ($person->{"middle_name"}) {
      $person_name .= " " . $person->{"middle_name"};
    }
    push(@name_list, { "name" => $person_name,
		       "type" => "person",
		       "floor" => $floor->{"short_local_label"}, });
  }
  @name_list = sort {
    lc($a->{"name"}) cmp lc($b->{"name"})
  } @name_list;
  # parse and fill in template.
  my ($today_year, $today_month_number, $today_day_number) = get_date_now(1);
  my $date = "$today_year-$today_month_number-$today_day_number";
  $template_data =~ s{YYYY-MM-DD}{$date}gs;
  my $list_designation = $building->{"address_1"};
  my $filename_suffix = lc($building->{"address_1"});
  $filename_suffix =~ s/\s+/_/;
  $template_data =~ s{LIST_DESIGNATION}{$list_designation}gs;
  if (1) {
    my $name = HTML::Entities::encode_numeric($name_list[0]->{"name"});
    my $floor = HTML::Entities::encode_numeric($name_list[0]->{"floor"});
    $template_data =~ s{Name1}{$name}gs;
    $template_data =~ s{FloorNum1}{$floor}gs;
  }
  $template_data =~ m{&lt;even&gt;(.+?)&lt;/even&gt;}s;
  my $template_for_even_row = $1;
  $template_data =~ m{&lt;odd&gt;(.+?)&lt;/odd&gt;}s;
  my $template_for_odd_row = $1;
  my $additional_rows = "";
  for(my $i = 1; $i < @name_list; $i++) {
    my $template_for_row = "";
    if (($i + 1) % 2 == 0) {
      # even row
      $template_for_row = $template_for_even_row;
      my $name = HTML::Entities::encode_numeric($name_list[$i]->{"name"});
      my $floor = HTML::Entities::encode_numeric($name_list[$i]->{"floor"});
      $template_for_row =~ s{Name2}{$name}gs;
      $template_for_row =~ s{FloorNum2}{$floor}gs;
    } else {
      # odd row
      $template_for_row = $template_for_odd_row;
      my $name = HTML::Entities::encode_numeric($name_list[$i]->{"name"});
      my $floor = HTML::Entities::encode_numeric($name_list[$i]->{"floor"});
      $template_for_row =~ s{Name3}{$name}gs;
      $template_for_row =~ s{FloorNum3}{$floor}gs;
    }
    $additional_rows .= $template_for_row;
  }
  $template_data =~ s{&lt;even&gt;.+?&lt;/even&gt;&lt;odd&gt;.+?&lt;/odd&gt;}{$additional_rows}es;
  # print out data to stdout as a Word XML file.
  print $cgi->header
    ("-type" => "application/msword",
     "-content-disposition" => "attachment; filename=\"delivery_list_$filename_suffix.xml\"");
  print $template_data;
}

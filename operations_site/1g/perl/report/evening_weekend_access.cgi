#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use lead_pad;
use read_file;
use select_row;
use get_row_by_id_map;
use get_row_list;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  my $template_data = read_file("../resource/evening_weekend_access_template.xml");
  my $building_id = $cgi->param("building_id");
  if (!$building_id) {
    die "show_page: evening_weekend_access_missing_building_id.html\n";
  }
  # fetch information on the building.
  my $building = select_row($dbh, "building_tbl", $building_id);
  # get all floors in the building.
  my ($sql_query, @sql_param_list);
  $sql_query = "select * from floor_tbl where building_id = ?";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  my %floor_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%floor_by_id);
  # select all accounts at the building.
  my %account_by_id;
  $sql_query = "select * from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # select all aliases linked to the accounts.
  my %alias_by_id;
  $sql_query = "select * from alias_tbl where account_id in (select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?))";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%alias_by_id);
  # select all persons linked to the accounts that are marked "register_with_building".
  my %person_by_id;
  $sql_query = "select * from person_vw where is_active and register_with_building and account_id in (select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?))";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%person_by_id);
  # process persons to attach them to accounts.
  my %person_id_list_by_account_id;
  for my $id (keys(%person_by_id)) {
    my $person = $person_by_id{$id};
    push(@{$person_id_list_by_account_id{$person->{"account_id"}}}, $id);
  }
  # select all phone numbers attached to the accounts.
  {
    my @account_list;
    $sql_query = "select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
    get_row_list($dbh, $sql_query, \@sql_param_list, \@account_list);
    my $placeholders = join(", ", map {"?"} @account_list);
    $sql_query = "select * from phone_number_tbl where visibility <> ? and purpose = ? and account_id in ($placeholders)";
    @sql_param_list = ("hidden", "voice");
    push(@sql_param_list, map {[$_->{"id"}, SQL_INTEGER]} @account_list);
  }
  my %phone_number_by_id;
  get_row_by_id_map($dbh_phone, $sql_query, \@sql_param_list, \%phone_number_by_id);
  # process phone numbers to attach them to persons.
  my %phone_number_id_list_by_person_id;
  for my $id (keys(%phone_number_by_id)) {
    my $phone_number = $phone_number_by_id{$id};
    if (my $account_id = $phone_number->{"account_id"}) {
      if (my $person_id = $phone_number->{"person_id"}) {
	push(@{$phone_number_id_list_by_person_id{$person_id}}, $id);
      }
    }
  }
  # merge account names, aliases, and person names into one big list and sort it.
  my @phone_number_type_list = (qw(work home cell other));
  my @name_list;
  for my $id (keys(%account_by_id)) {
    # if account has no registered persons, skip it.
    next if (!$person_id_list_by_account_id{$id});
    my $account = $account_by_id{$id};
    my $floor = $floor_by_id{$account->{"primary_floor_id"}};
    my @person_name_list;
    for my $person_id (@{$person_id_list_by_account_id{$id}}) {
      my $person = $person_by_id{$person_id};
      if ($person) {
	my $person_name = "";
	$person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
	if ($person->{"middle_name"}) {
	  $person_name .= " " . $person->{"middle_name"};
	}
	push(@person_name_list, $person_name);
      }
    }
    @person_name_list = sort {lc($a) cmp lc($b)} (@person_name_list);
    push(@name_list, { "name" => $account->{"name"},
		       "person_names" => join("; ", @person_name_list),
		       "type" => "account",
		       "floor" => $floor->{"short_local_label"}, });
  }
  for my $id (keys(%alias_by_id)) {
    my $alias = $alias_by_id{$id};
    my $account_id = $alias->{"account_id"};
    # if account has no registered persons, skip it.
    next if (!$person_id_list_by_account_id{$account_id});
    my $account = $account_by_id{$account_id};
    my $floor = $floor_by_id{$account->{"primary_floor_id"}};
    my @person_name_list;
    for my $person_id (@{$person_id_list_by_account_id{$account_id}}) {
      my $person = $person_by_id{$person_id};
      if ($person) {
	my $person_name = "";
	$person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
	if ($person->{"middle_name"}) {
	  $person_name .= " " . $person->{"middle_name"};
	}
	push(@person_name_list, $person_name);
      }
    }
    @person_name_list = sort {lc($a) cmp lc($b)} (@person_name_list);
    push(@name_list, { "name" => $alias->{"name"},
		       "person_names" => join("; ", @person_name_list),
		       "type" => "alias",
		       "floor" => $floor->{"short_local_label"}, });
  }
  for my $id (keys(%person_by_id)) {
    my $person = $person_by_id{$id};
    my $account = $account_by_id{$person->{"account_id"}};
    my $account_name = $account->{"name"};
    my $floor = $floor_by_id{$account->{"primary_floor_id"}};
    my $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
    if ($person->{"middle_name"}) {
      $person_name .= " " . $person->{"middle_name"};
    }
    my %phone_number_unique_set;
    if (my $phone_number_id_list = $phone_number_id_list_by_person_id{$person->{"id"}}) {
      for my $phone_number_id (@$phone_number_id_list) {
	my $phone_number = $phone_number_by_id{$phone_number_id};
	$phone_number_unique_set{$phone_number->{"phone_number"}} = 1;
      }
    }
    for my $phone_number_type (@phone_number_type_list) {
      my $phone_number = $person->{$phone_number_type . "_phone_number"};
      if (($phone_number) && ($phone_number !~ /^\s*$/)) {
	$phone_number_unique_set{$phone_number} = 1;
      }
    }
    my $phone_number_formatted = join("; ", sort(keys(%phone_number_unique_set)));
    push(@name_list, { "name" => $person_name,
		       "account_name" => $account_name,
		       "phone_number" => $phone_number_formatted,
		       "type" => "person",
		       "floor" => $floor->{"short_local_label"}, });
  }
  @name_list = sort {
    lc($a->{"name"}) cmp lc($b->{"name"})
  } @name_list;
  # parse and fill in template.
  my ($today_year, $today_month_number, $today_day_number) = get_date_now(1);
  my $date = "$today_year-$today_month_number-$today_day_number";
  $template_data =~ s{YYYY-MM-DD}{$date}gs;
  my $list_designation = $building->{"address_1"};
  my $filename_suffix = lc($building->{"address_1"});
  $filename_suffix =~ s/\s+/_/;
  $template_data =~ s{LIST_DESIGNATION}{$list_designation}gs;
  my $row_text = "";
  if ($name_list[0]->{"type"} eq "person") {
    $row_text .= HTML::Entities::encode_numeric($name_list[0]->{"name"});
    $row_text .= " (" . HTML::Entities::encode_numeric($name_list[0]->{"account_name"}) . ")";
    $row_text .= " (" . HTML::Entities::encode_numeric($name_list[0]->{"floor"}) . ")";
    if ($name_list[0]->{"phone_number"}) {
      $row_text .= " " . HTML::Entities::encode_numeric($name_list[0]->{"phone_number"});
    }
  } else {
    $row_text .= HTML::Entities::encode_numeric($name_list[0]->{"name"});
    $row_text .= " (" . HTML::Entities::encode_numeric($name_list[0]->{"floor"}) . ")";
    $row_text .= ": see " . HTML::Entities::encode_numeric($name_list[0]->{"person_names"});
  }
  $template_data =~ s{ListRow1}{$row_text}gs;
  $template_data =~ m{&lt;even&gt;(.+?)&lt;/even&gt;}s;
  my $template_for_even_row = $1;
  $template_data =~ m{&lt;odd&gt;(.+?)&lt;/odd&gt;}s;
  my $template_for_odd_row = $1;
  my $additional_rows = "";
  for(my $i = 1; $i < @name_list; $i++) {
    my $template_for_row = "";
    $row_text = "";
    if (($i + 1) % 2 == 0) {
      # even row
      $template_for_row = $template_for_even_row;
      if ($name_list[$i]->{"type"} eq "person") {
	$row_text .= HTML::Entities::encode_numeric($name_list[$i]->{"name"});
	$row_text .= " (" . HTML::Entities::encode_numeric($name_list[$i]->{"account_name"}) . ")";
	$row_text .= " (" . HTML::Entities::encode_numeric($name_list[$i]->{"floor"}) . ")";
	if ($name_list[$i]->{"phone_number"}) {
	  $row_text .= " " . HTML::Entities::encode_numeric($name_list[$i]->{"phone_number"});
	}
      } else {
	$row_text .= HTML::Entities::encode_numeric($name_list[$i]->{"name"});
	$row_text .= " (" . HTML::Entities::encode_numeric($name_list[$i]->{"floor"}) . ")";
	$row_text .= ": see " . HTML::Entities::encode_numeric($name_list[$i]->{"person_names"});
      }
      $template_for_row =~ s{ListRow2}{$row_text}gs;
    } else {
      # odd row
      $template_for_row = $template_for_odd_row;
      if ($name_list[$i]->{"type"} eq "person") {
	$row_text .= HTML::Entities::encode_numeric($name_list[$i]->{"name"});
	$row_text .= " (" . HTML::Entities::encode_numeric($name_list[$i]->{"account_name"}) . ")";
	$row_text .= " (" . HTML::Entities::encode_numeric($name_list[$i]->{"floor"}) . ")";
	if ($name_list[$i]->{"phone_number"}) {
	  $row_text .= " " . HTML::Entities::encode_numeric($name_list[$i]->{"phone_number"});
	}
      } else {
	$row_text .= HTML::Entities::encode_numeric($name_list[$i]->{"name"});
	$row_text .= " (" . HTML::Entities::encode_numeric($name_list[$i]->{"floor"}) . ")";
	$row_text .= ": see " . HTML::Entities::encode_numeric($name_list[$i]->{"person_names"});
      }
      $template_for_row =~ s{ListRow3}{$row_text}gs;
    }
    $additional_rows .= $template_for_row;
  }
  $template_data =~ s{&lt;even&gt;.+?&lt;/even&gt;&lt;odd&gt;.+?&lt;/odd&gt;}{$additional_rows}es;
  # print out data to stdout as a Word XML file.
  print $cgi->header
    ("-type" => "application/msword",
     "-content-disposition" => "attachment; filename=\"evening_weekend_access_$filename_suffix.xml\"");
  print $template_data;
}

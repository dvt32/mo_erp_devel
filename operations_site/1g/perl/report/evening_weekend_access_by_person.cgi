#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use lead_pad;
use read_file;
use select_row;
use get_row_by_id_map;
use get_row_list;
use get_date_now;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  my $template_data = read_file("../resource/evening_weekend_access_by_person_template.xml");
  my $floor_id = $cgi->param("floor_id");
  my $building_id = $cgi->param("building_id");
  if ((!$floor_id) && (!$building_id)) {
    die "show_page: evening_weekend_access_by_person_missing_floor_id_and_building_id.html\n";
  }
  # fetch information on the floor, if floor_id was specified.
  my $floor;
  if ($floor_id) {
    $floor = select_row($dbh, "floor_tbl", $floor_id);
    $building_id = $floor->{"building_id"};
  }
  # fetch information on the building.
  my $building = select_row($dbh, "building_tbl", $building_id);
  # select all accounts at the building or on the floor.
  my ($sql_query, @sql_param_list);
  my (%account_by_id);
  if ($floor_id) {
    $sql_query = "select * from account_vw where is_active and primary_floor_id = ?";
    @sql_param_list = ([$floor_id, SQL_INTEGER]);
  } elsif ($building_id) {
    $sql_query = "select * from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
  }
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # get the Micro Office account.
  $sql_query = "select * from account_vw where id = ?";
  @sql_param_list = ([$account_id_of_corporate, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # select all people at the building or on the floor who should be registered.
  if ($floor_id) {
    $sql_query = "select * from cust_pers_tbl where is_active and register_with_building and cust_acct_id in (select id from account_vw where is_active and primary_floor_id = ?)";
    @sql_param_list = ([$floor_id, SQL_INTEGER]);
  } elsif ($building_id) {
    $sql_query = "select * from cust_pers_tbl where is_active and register_with_building and cust_acct_id in (select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?))";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
  }
  my (%person_by_id);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%person_by_id);
  # add the employees.
  $sql_query = "select * from employee_tbl where is_active and in_directory";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  for my $person (values(%person_by_id)) {
    if ($person->{"cust_acct_id"}) {
      $person->{"account_id"} = $person->{"cust_acct_id"};
      delete $person->{"cust_acct_id"};
    } else {
      # person is an employee.
      $person->{"account_id"} = $account_id_of_corporate;
    }
  }
  # select all voice phone numbers attached to accounts at the building or on the floor.
  if ($floor_id) {
    my @account_list;
    $sql_query = "select id from account_vw where is_active and primary_floor_id = ?";
    @sql_param_list = ([$floor_id, SQL_INTEGER]);
    get_row_list($dbh, $sql_query, \@sql_param_list, \@account_list);
    my $placeholders = join(", ", map {"?"} @account_list);
    $sql_query = "select * from phone_number_tbl where visibility <> ? and purpose = ? and account_id in ($placeholders)";
    @sql_param_list = ("hidden", "voice");
    push(@sql_param_list, map {[$_->{"id"}, SQL_INTEGER]} @account_list);
  } elsif ($building_id) {
    my @account_list;
    $sql_query = "select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
    get_row_list($dbh, $sql_query, \@sql_param_list, \@account_list);
    my $placeholders = join(", ", map {"?"} @account_list);
    $sql_query = "select * from phone_number_tbl where visibility <> ? and purpose = ? and account_id in ($placeholders)";
    @sql_param_list = ("hidden", "voice");
    push(@sql_param_list, map {[$_->{"id"}, SQL_INTEGER]} @account_list);
  }
  my (%phone_number_by_id, %phone_number_id_list_by_person_id);
  get_row_by_id_map($dbh_phone, $sql_query, \@sql_param_list, \%phone_number_by_id);
  for my $phone_number (values(%phone_number_by_id)) {
    my $person_id = $phone_number->{"person_id"};
    if (defined($person_id) && exists($person_by_id{$person_id})) {
      $phone_number_by_id{$phone_number->{"id"}} = $phone_number;
      push(@{$phone_number_id_list_by_person_id{$phone_number->{"person_id"}}}, $phone_number->{"id"});
    }
  }
  # sort the person id values by last name, first name, then middle name
  my @person_id_list = sort {
    lc($person_by_id{$a}->{"last_name"}) cmp lc($person_by_id{$b}->{"last_name"}) || lc($person_by_id{$a}->{"first_name"}) cmp lc($person_by_id{$b}->{"first_name"}) || lc($person_by_id{$a}->{"middle_name"}) cmp lc($person_by_id{$b}->{"middle_name"});
  } keys(%person_by_id);
  # sort the phone number id lists by phone number.
  for my $person_id (keys(%phone_number_id_list_by_person_id)) {
    @{$phone_number_id_list_by_person_id{$person_id}} = sort {
      $phone_number_by_id{$a}->{"phone_number"} cmp $phone_number_by_id{$b}->{"phone_number"};
    } @{$phone_number_id_list_by_person_id{$person_id}};
  }
  my @list_entry_list;
  my $list_entry;
  my @phone_number_type_list = (qw(work home cell other));
  for my $person_id (@person_id_list) {
    my $person = $person_by_id{$person_id};
    my $account_id = $person->{"account_id"};
    my $account = $account_by_id{$account_id};
    my $account_name = $account->{"name"};
    my $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
    if ($person->{"middle_name"}) {
      $person_name .= " " . $person->{"middle_name"};
    }
    my @phone_number_list;
    my $phone_number_id_list = $phone_number_id_list_by_person_id{$person_id};
    if (defined($phone_number_id_list)) {
      push(@phone_number_list, map {$phone_number_by_id{$_}->{"phone_number"}} @$phone_number_id_list);
    }
    for my $phone_number_type (@phone_number_type_list) {
      my $phone_number = $person->{$phone_number_type . "_phone_number"};
      if (($phone_number) && ($phone_number !~ /^\s*$/) &&
	  (!grep {$_ eq $phone_number} @phone_number_list)) {
	push(@phone_number_list, $phone_number);
      }
    }
    $list_entry = {};
    $list_entry->{"person_name"} = $person_name;
    $list_entry->{"company_name"} = $account_name;
    $list_entry->{"phone_number"} = join("; ", @phone_number_list);
    push(@list_entry_list, $list_entry);
  }
  my ($today_year, $today_month_number, $today_day_number) = get_date_now(1);
  my $date = "$today_year-$today_month_number-$today_day_number";
  $template_data =~ s{YYYY-MM-DD}{$date}gs;
  my $list_designation = $building->{"address_1"} . ", ";
  my $filename_suffix = "";
  if ($floor) {
    $list_designation .= $floor->{"label"};
    $filename_suffix = lc($floor->{"label"});
    $filename_suffix =~ s/\s+/_/;
  } else {
    $list_designation .= "all floors";
    $filename_suffix = "all_floors";
  }
  $template_data =~ s{LIST_DESIGNATION}{$list_designation}gs;
  $template_data =~ s{PersonName1}{HTML::Entities::encode_numeric($list_entry_list[0]->{"person_name"})}ges;
  $template_data =~ s{CompanyName1}{HTML::Entities::encode_numeric($list_entry_list[0]->{"company_name"})}ges;
  $template_data =~ s{PhoneNumber1}{HTML::Entities::encode_numeric($list_entry_list[0]->{"phone_number"})}ges;
  $template_data =~ m{&lt;even&gt;(.+?)&lt;/even&gt;}s;
  my $template_for_even_row = $1;
  $template_data =~ m{&lt;odd&gt;(.+?)&lt;/odd&gt;}s;
  my $template_for_odd_row = $1;
  my $additional_rows = "";
  for(my $i = 1; $i < @list_entry_list; $i++) {
    my $template_for_row = "";
    if (($i + 1) % 2 == 0) {
      # even row
      $template_for_row = $template_for_even_row;
      $template_for_row =~ s{PersonName2}{HTML::Entities::encode_numeric($list_entry_list[$i]->{"person_name"})}ges;
      $template_for_row =~ s{CompanyName2}{HTML::Entities::encode_numeric($list_entry_list[$i]->{"company_name"})}ges;
      $template_for_row =~ s{PhoneNumber2}{HTML::Entities::encode_numeric($list_entry_list[$i]->{"phone_number"})}ges;
    } else {
      # odd row
      $template_for_row = $template_for_odd_row;
      $template_for_row =~ s{PersonName3}{HTML::Entities::encode_numeric($list_entry_list[$i]->{"person_name"})}ges;
      $template_for_row =~ s{CompanyName3}{HTML::Entities::encode_numeric($list_entry_list[$i]->{"company_name"})}ges;
      $template_for_row =~ s{PhoneNumber3}{HTML::Entities::encode_numeric($list_entry_list[$i]->{"phone_number"})}ges;
    }
    $additional_rows .= $template_for_row;
  }
  $template_data =~ s{&lt;even&gt;.+?&lt;/even&gt;&lt;odd&gt;.+?&lt;/odd&gt;}{$additional_rows}es;
  # print out data to stdout as a Word XML file.
  print $cgi->header
    ("-type" => "application/msword",
     "-content-disposition" => "attachment; filename=\"evening_weekend_access_by_person_$filename_suffix.xml\"");
  print $template_data;
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use lead_pad;
use read_file;
use select_row;
use get_date_now;
use day_of_week;
use get_row_by_id_map;
use add_time;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $template_data = read_file("../resource/guest_list_template.xml");
  my $building_id = $cgi->param("building_id");
  if (!$building_id) {
    die "show_page: guest_list_missing_building_id.html\n";
  }
  my $report_date = $cgi->param("report_date");
  my ($report_date_year, $report_date_month, $report_date_day);
  if ($report_date) {
    if ($report_date !~ /^(\d{4})-(\d\d)-(\d\d)$/) {
      die "show_page: guest_list_bad_report_date.html\n";
    } else {
      ($report_date_year, $report_date_month, $report_date_day) = ($1, $2, $3);
    }
  } else {
    ($report_date_year, $report_date_month, $report_date_day) = get_date_now(1);
    $report_date = "$report_date_year-$report_date_month-$report_date_day";
  }
  my $day_of_week = day_of_week($report_date_month, $report_date_day, $report_date_year);
  # fetch information on the building.
  my $building = select_row($dbh, "building_tbl", $building_id);
  # get all floors in the building.
  my $sql_query = "select * from floor_tbl where building_id = ?";
  my @sql_param_list = ([$building_id, SQL_INTEGER]);
  my %floor_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%floor_by_id);
  # select all accounts linked to floors in the building.
  $sql_query = "select * from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
  @sql_param_list = ([$building_id, SQL_INTEGER]);
  my %account_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # select all guests linked to the accounts in the building where date is specified date.
  my $report_end_date;
  if ($day_of_week == 5) {
    # if Friday, include Saturday and Sunday
    my @report_end_date_component_list = add_time($report_date_year, $report_date_month, $report_date_day, 0, 0, 0, 2, "day");
    my ($y, $m, $d) = @report_end_date_component_list[0, 1, 2];
    $m = lead_pad($m, 2);
    $d = lead_pad($d, 2);
    $report_end_date = "$y-$m-$d";
  } else {
    $report_end_date = $report_date;
  }
  $sql_query = "select * from guest_person_tbl where account_id in (select id from account_vw where primary_floor_id in (select id from floor_tbl where building_id = ?)) and (start_date, end_date) overlaps (date ?, date ?) order by last_name, first_name";
  @sql_param_list = ([$building_id, SQL_INTEGER], $report_date, $report_end_date);
  my (%guest_person_by_id, @guest_person_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%guest_person_by_id, \@guest_person_id_list);
  if (!@guest_person_id_list) {
    die "show_page: guest_list_no_guests.html\n";
  }
  # parse and fill in template.
  if ($report_date eq $report_end_date) {
    $template_data =~ s{YYYY-MM-DD}{$report_date}gs;
  } else {
    $template_data =~ s{YYYY-MM-DD}{$report_date to $report_end_date}gs;
  }
  my $list_designation = $building->{"address_1"};
  my $filename_suffix = lc($building->{"address_1"}) . "_" . $report_date;
  $filename_suffix =~ s/\s+/_/;
  $template_data =~ s{LIST_DESIGNATION}{$list_designation}gs;
  if (1) {
    my $guest_person = $guest_person_by_id{$guest_person_id_list[0]};
    $guest_person->{"account_id"};
    my $name = $guest_person->{"last_name"} . ", " . $guest_person->{"first_name"};
    if ($guest_person->{"salutation"}) {
      $name .= " (" . $guest_person->{"salutation"} . ")";
    }
    $name = HTML::Entities::encode_numeric($name);
    my $host = HTML::Entities::encode_numeric($guest_person->{"host_contact"} || "");
    my $account = $account_by_id{$guest_person->{"account_id"}};
    my $account_name = HTML::Entities::encode_numeric($account->{"name"});
    my $floor = $floor_by_id{$account->{"primary_floor_id"}};
    my $floor_label = HTML::Entities::encode_numeric($floor->{"label"});
    $template_data =~ s{Name1}{$name}gs;
    $template_data =~ s{Host1}{$host}gs;
    $template_data =~ s{Account1}{$account_name}gs;
    $template_data =~ s{Floor1}{$floor_label}gs;
  }
  $template_data =~ m{&lt;even&gt;(.+?)&lt;/even&gt;}s;
  my $template_for_even_row = $1;
  $template_data =~ m{&lt;odd&gt;(.+?)&lt;/odd&gt;}s;
  my $template_for_odd_row = $1;
  my $additional_rows = "";
  for(my $i = 1; $i < @guest_person_id_list; $i++) {
    my $template_for_row = "";
    if (($i + 1) % 2 == 0) {
      # even row
      $template_for_row = $template_for_even_row;
      my $guest_person = $guest_person_by_id{$guest_person_id_list[$i]};
      $guest_person->{"account_id"};
      my $name = $guest_person->{"last_name"} . ", " . $guest_person->{"first_name"};
      if ($guest_person->{"salutation"}) {
	$name .= " (" . $guest_person->{"salutation"} . ")";
      }
      $name = HTML::Entities::encode_numeric($name);
      my $host = HTML::Entities::encode_numeric($guest_person->{"host_contact"} || "");
      my $account = $account_by_id{$guest_person->{"account_id"}};
      my $account_name = HTML::Entities::encode_numeric($account->{"name"});
      my $floor = $floor_by_id{$account->{"primary_floor_id"}};
      my $floor_label = HTML::Entities::encode_numeric($floor->{"label"});
      $template_for_row =~ s{Name2}{$name}gs;
      $template_for_row =~ s{Host2}{$host}gs;
      $template_for_row =~ s{Account2}{$account_name}gs;
      $template_for_row =~ s{Floor2}{$floor_label}gs;
    } else {
      # odd row
      $template_for_row = $template_for_odd_row;
      my $guest_person = $guest_person_by_id{$guest_person_id_list[$i]};
      $guest_person->{"account_id"};
      my $name = $guest_person->{"last_name"} . ", " . $guest_person->{"first_name"};
      if ($guest_person->{"salutation"}) {
	$name .= " (" . $guest_person->{"salutation"} . ")";
      }
      $name = HTML::Entities::encode_numeric($name);
      my $host = HTML::Entities::encode_numeric($guest_person->{"host_contact"} || "");
      my $account = $account_by_id{$guest_person->{"account_id"}};
      my $account_name = HTML::Entities::encode_numeric($account->{"name"});
      my $floor = $floor_by_id{$account->{"primary_floor_id"}};
      my $floor_label = HTML::Entities::encode_numeric($floor->{"label"});
      $template_for_row =~ s{Name3}{$name}gs;
      $template_for_row =~ s{Host3}{$host}gs;
      $template_for_row =~ s{Account3}{$account_name}gs;
      $template_for_row =~ s{Floor3}{$floor_label}gs;
    }
    $additional_rows .= $template_for_row;
  }
  $template_data =~ s{&lt;even&gt;.+?&lt;/even&gt;&lt;odd&gt;.+?&lt;/odd&gt;}{$additional_rows}es;
  # print out data to stdout as a Word XML file.
  print $cgi->header
    ("-type" => "application/msword",
     "-content-disposition" => "attachment; filename=\"guest_list_$filename_suffix.xml\"");
  print $template_data;
}

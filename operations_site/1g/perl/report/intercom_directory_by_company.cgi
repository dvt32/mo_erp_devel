#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use lead_pad;
use read_file;
use select_row;
use get_row_by_id_map;
use get_row_list;
use get_date_now;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  my $template_data = read_file("../resource/intercom_directory_by_company_template.xml");
  my $floor_id = $cgi->param("floor_id");
  my $building_id = $cgi->param("building_id");
  if ((!$floor_id) && (!$building_id)) {
    die "show_page: intercom_directory_by_company_missing_floor_id_and_building_id.html\n";
  }
  # fetch information on the floor, if floor_id was specified.
  my $floor;
  if ($floor_id) {
    $floor = select_row($dbh, "floor_tbl", $floor_id);
    $building_id = $floor->{"building_id"};
  }
  # fetch information on the building.
  my $building = select_row($dbh, "building_tbl", $building_id);
  # select all accounts at the building or on the floor.
  my ($sql_query, @sql_param_list);
  if ($floor_id) {
    $sql_query = "select * from account_vw where is_active and primary_floor_id = ?";
    @sql_param_list = ([$floor_id, SQL_INTEGER]);
  } elsif ($building_id) {
    $sql_query = "select * from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
  }
  my (%account_by_id, @directory_account_and_alias_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # get the Micro Office account.
  $sql_query = "select * from account_vw where id = ?";
  @sql_param_list = ([$account_id_of_corporate, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  for my $account (values(%account_by_id)) {
    my $name = $account->{"name"};
    push(@directory_account_and_alias_list, {"type" => "account", "name" => $name, "row" => $account});
  }
  # select all aliases attached to the accounts of interest.
  if ($floor_id) {
    $sql_query = "select * from alias_tbl where account_id in (select id from account_vw where is_active and primary_floor_id = ?)";
    @sql_param_list = ([$floor_id, SQL_INTEGER]);
  } elsif ($building_id) {
    $sql_query = "select * from alias_tbl where account_id in (select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?))";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
  }
  my %alias_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%alias_by_id);
  for my $alias (values(%alias_by_id)) {
    my $name = $alias->{"name"};
    push(@directory_account_and_alias_list, {"type" => "alias", "name" => $name, "row" => $alias});
  }
  # sort the account and alias list by name
  @directory_account_and_alias_list = sort {lc($a->{"name"}) cmp lc($b->{"name"})} @directory_account_and_alias_list;
  my %num_non_alias_entries_by_account_id;
  # select all people at the building or on the floor.
  if ($floor_id) {
    $sql_query = "select * from cust_pers_tbl where is_active and cust_acct_id in (select id from account_vw where is_active and primary_floor_id = ?)";
    @sql_param_list = ([$floor_id, SQL_INTEGER]);
  } elsif ($building_id) {
    $sql_query = "select * from cust_pers_tbl where is_active and cust_acct_id in (select id from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?))";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
  }
  my (%person_by_id, %person_list_by_account_id);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%person_by_id);
  # add the employees.
  $sql_query = "select * from employee_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  for my $person (values(%person_by_id)) {
    if ($person->{"cust_acct_id"}) {
      $person->{"account_id"} = $person->{"cust_acct_id"};
      delete $person->{"cust_acct_id"};
    } else {
      # person is an employee.
      $person->{"account_id"} = $account_id_of_corporate;
      $person->{"visibility"} = "visible";
    }
    push(@{$person_list_by_account_id{$person->{"account_id"}}}, $person);
    if (has_external_phone_number($person)) {
      $num_non_alias_entries_by_account_id{$person->{"account_id"}}++;
    }
  }
  # select all voice phone numbers on the floor or in the building.
  if ($floor_id) {
    $sql_query = "select * from phone_number_tbl where account_id is not null and visibility <> ? and purpose = ? and floor_id = ?";
    @sql_param_list = ("hidden", "voice", [$floor_id, SQL_INTEGER]);
  } elsif ($building_id) {
    my @floor_list;
    $sql_query = "select id from floor_tbl where building_id = ?";
    @sql_param_list = ([$building_id, SQL_INTEGER]);
    get_row_list($dbh, $sql_query, \@sql_param_list, \@floor_list);
    my $placeholders = join(", ", map {"?"} @floor_list);
    $sql_query = "select * from phone_number_tbl where account_id is not null and visibility <> ? and purpose = ? and floor_id in ($placeholders)";
    @sql_param_list = ("hidden", "voice");
    push(@sql_param_list, map {[$_->{"id"}, SQL_INTEGER]} @floor_list);
  }
  my (%phone_number_by_id, %phone_number_id_list_by_account_id, %phone_number_id_list_by_person_id, %no_person_phone_number_id_list_by_account_id);
  get_row_by_id_map($dbh_phone, $sql_query, \@sql_param_list, \%phone_number_by_id);
  for my $phone_number (values(%phone_number_by_id)) {
    push(@{$phone_number_id_list_by_account_id{$phone_number->{"account_id"}}}, $phone_number->{"id"});
    $num_non_alias_entries_by_account_id{$phone_number->{"account_id"}}++;
    if (defined($phone_number->{"person_id"})) {
      push(@{$phone_number_id_list_by_person_id{$phone_number->{"person_id"}}}, $phone_number->{"id"});
    } else {
      push(@{$no_person_phone_number_id_list_by_account_id{$phone_number->{"account_id"}}}, $phone_number->{"id"});
    }
  }
  # within each account, sort the persons by name
  for my $account_id (keys(%person_list_by_account_id)) {
    @{$person_list_by_account_id{$account_id}} = sort {lc($a->{"last_name"}) cmp lc($b->{"last_name"}) || lc($a->{"first_name"}) cmp lc($b->{"first_name"}) || lc($a->{"middle_name"}) cmp lc($b->{"middle_name"})} @{$person_list_by_account_id{$account_id}};
  }
  # sort the phone number id lists in by_person_id and no_person_xxx_by_account_id.
  for my $phone_number_id_list (values(%phone_number_id_list_by_person_id)) {
    @{$phone_number_id_list} = sort {$phone_number_by_id{$a}->{"extension"} cmp $phone_number_by_id{$b}->{"extension"}} @{$phone_number_id_list};
  }
  for my $phone_number_id_list (values(%no_person_phone_number_id_list_by_account_id)) {
    @{$phone_number_id_list} = sort {$phone_number_by_id{$a}->{"extension"} cmp $phone_number_by_id{$b}->{"extension"}} @{$phone_number_id_list};
  }
  # generate directory entries.
  my @directory_entry_list;
  my $directory_entry;
  for my $potential_dir_entry (@directory_account_and_alias_list) {
    my $type = $potential_dir_entry->{"type"};
    if ($type eq "account") {
      my $account = $potential_dir_entry->{"row"};
      my $account_id = $account->{"id"};
      my $account_name = $account->{"name"};
      my $phone_number_without_person_list = $no_person_phone_number_id_list_by_account_id{$account_id};
      if (defined($phone_number_without_person_list)) {
	for my $phone_number_id (@$phone_number_without_person_list) {
	  my $phone_number = $phone_number_by_id{$phone_number_id};
	  $directory_entry = {};
	  $directory_entry->{"name"} = $account_name;
	  $directory_entry->{"intercom"} = $phone_number->{"extension"};
	  $directory_entry->{"external_phone_numbers"} = "";
	  if ($phone_number->{"visibility"} eq "emphasized") {
	    $directory_entry->{"is_emphasized"} = 1;
	  }
	  push(@directory_entry_list, $directory_entry);
	}
      }
      if (!$person_list_by_account_id{$account_id}) {
	next;
      }
      my @person_list = @{$person_list_by_account_id{$account_id}};
      for(my $i = 0; $i < @person_list; $i++) {
	my $person = $person_list[$i];
	my $person_id = $person->{"id"};
	my $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
	if ($person->{"middle_name"}) {
	  $person_name .= " " . $person->{"middle_name"};
	}
	my $phone_number_id_list = $phone_number_id_list_by_person_id{$person_id};
	if ((defined($phone_number_id_list)) && (scalar(@$phone_number_id_list) > 0)) {
	  my $_phone_number_id = $phone_number_id_list->[0];
	  my $_phone_number = $phone_number_by_id{$_phone_number_id};
	  $directory_entry = {};
	  $directory_entry->{"name"} = $account_name . " (" . $person_name . ")";
	  $directory_entry->{"intercom"} = $_phone_number->{"extension"};
	  if (has_external_phone_number($person)) {
	    $directory_entry->{"external_phone_numbers"} = render_external_phone_number($person);
	  } else {
	    $directory_entry->{"external_phone_numbers"} = "";
	  }
	  if (($person->{"visibility"} eq "emphasized") || ($_phone_number->{"visibility"} eq "emphasized")) {
	    $directory_entry->{"is_emphasized"} = 1;
	  }
	  push(@directory_entry_list, $directory_entry);
	  for(my $j = 1; $j < @$phone_number_id_list; $j++) {
	    $_phone_number_id = $phone_number_id_list->[$j];
	    $_phone_number = $phone_number_by_id{$_phone_number_id};
	    $directory_entry = {};
	    $directory_entry->{"name"} = $account_name . " (" . $person_name . ")";
	    $directory_entry->{"intercom"} = $_phone_number->{"extension"};
	    $directory_entry->{"external_phone_numbers"} = "";
	    if ($_phone_number->{"visibility"} eq "emphasized") {
	      $directory_entry->{"is_emphasized"} = 1;
	    }
	    push(@directory_entry_list, $directory_entry);
	  }
	} else {
	  if (has_external_phone_number($person)) {
	    $directory_entry = {};
	    $directory_entry->{"name"} = $account_name . " (" . $person_name . ")";
	    $directory_entry->{"intercom"} = "";
	    $directory_entry->{"external_phone_numbers"} = render_external_phone_number($person);
	    if ($person->{"visibility"} eq "emphasized") {
	      $directory_entry->{"is_emphasized"} = 1;
	    }
	    push(@directory_entry_list, $directory_entry);
	  }
	}
      }
    } elsif ($type eq "alias") {
      my $alias = $potential_dir_entry->{"row"};
      my $account = $account_by_id{$alias->{"account_id"}};
      my $account_name = $account->{"name"};
      # only show an alias if it is different from the account name.
      if ($alias->{"name"} ne $account_name) {
	# if there are no directory entries for the account besides aliases, omit the aliases.
	if ($num_non_alias_entries_by_account_id{$account->{"id"}}) {
	  $directory_entry = {};
	  $directory_entry->{"name"} = HTML::Entities::encode_numeric($alias->{"name"}) . ':</w:t><w:t xml:space="preserve">  </w:t></w:r><w:r><w:rPr><w:i/><w:u w:val="single"/></w:rPr><w:t>see</w:t></w:r><w:r><w:t xml:space="preserve">  </w:t><w:t>' . HTML::Entities::encode_numeric($account_name);
	  $directory_entry->{"suppress_HTML::Entities::encode_name"} = 1;
	  $directory_entry->{"intercom"} = "";
	  $directory_entry->{"external_phone_numbers"} = "";
	  push(@directory_entry_list, $directory_entry);
	}
      }
    } else {
      die "bad type: $type";
    }
  }
  my ($today_year, $today_month_number, $today_day_number) = get_date_now(1);
  my $date = "$today_year-$today_month_number-$today_day_number";
  $template_data =~ s{YYYY-MM-DD}{$date}gs;
  my $receptionist_extensions = "";
  if ($building_id == 2) {
    $receptionist_extensions = "1270, 1273, 1271, or 5412";
  } elsif ($building_id == 3) {
    $receptionist_extensions = "4201 or 4202";
  }
  $template_data =~ s{RECEPTIONIST_EXTENSIONS}{$receptionist_extensions}gs;
  my $list_designation = $building->{"address_1"} . ", ";
  my $filename_suffix = "";
  if ($floor) {
    $list_designation .= $floor->{"label"};
    $filename_suffix = lc($floor->{"label"});
    $filename_suffix =~ s/\s+/_/;
  } else {
    $list_designation .= "all floors";
    $filename_suffix = "all_floors";
  }
  $template_data =~ s{LIST_DESIGNATION}{$list_designation}gs;
  $template_data =~ m{<w:body>(.+?)</w:body>}s;
  my $document_body_template = $1;
  my (@row_template_list);
  for(my $i = 1; $document_body_template =~ m{(<w:p(?: .+?)?>.+?\(r$i\).+?\(/r$i\).+?</w:p>)}g; $i++) {
    push(@row_template_list, $1);
  }
  # row templates:
  # 1 = normal odd row
  # 2 = normal even row
  # 3 = emphasized odd row
  # 4 = emphasized even row
  my $num_row_templates = scalar(@row_template_list);
  my $row_data = "";
  for(my $i = 0; $i < @directory_entry_list; $i++) {
    my $template_number;
    if (($i + 1) % 2 == 0) {
      # even row
      if ($directory_entry_list[$i]->{"is_emphasized"}) {
	$template_number = 4;
      } else {
	$template_number = 2;
      }
    } else {
      # odd row
      if ($directory_entry_list[$i]->{"is_emphasized"}) {
	$template_number = 3;
      } else {
	$template_number = 1;
      }
    }
    my $template_for_row = $row_template_list[$template_number - 1];
    my $_name = $directory_entry_list[$i]->{"name"};
    if (!$directory_entry_list[$i]->{"suppress_HTML::Entities::encode_name"}) {
      $_name = HTML::Entities::encode_numeric($_name);
    }
    $template_for_row =~ s{\(NAME\)}{$_name}gs;
    $template_for_row =~ s{\(INT\)}{HTML::Entities::encode_numeric($directory_entry_list[$i]->{"intercom"})}ges;
    $template_for_row =~ s{\(PHN\)}{HTML::Entities::encode_numeric($directory_entry_list[$i]->{"external_phone_numbers"})}ges;
    $template_for_row =~ s{\(r$template_number\)}{}gs;
    $template_for_row =~ s{\(/r$template_number\)}{}gs;
    $row_data .= $template_for_row;
  }
  $template_data =~ s{<w:p(?: .+?)?>.+?\(r1\).+?\(/r$num_row_templates\).+?</w:p>}{$row_data}es;
  # print out data to stdout as a Word XML file.
  print $cgi->header
    ("-type" => "application/msword",
     "-content-disposition" => "attachment; filename=\"intercom_directory_by_company_$filename_suffix.xml\"");
  print $template_data;
}

sub has_external_phone_number($) {
  my $person = shift;
  my $has_number = 0;
  for(my $i = 1; $i <= 3; $i++) {
    if (my $phone_number_type = $person->{"directory_$i"}) {
      if (my $phone_number = $person->{$phone_number_type . "_phone_number"}) {
	if ($phone_number !~ /^\s*$/) {
	  $has_number = 1;
	}
      }
    }
  }
  return $has_number;
}

sub render_external_phone_number($) {
  my $person = shift;
  my @phone_number_list;
  for(my $i = 1; $i <= 3; $i++) {
    if (my $phone_number_type = $person->{"directory_$i"}) {
      if (my $phone_number = $person->{$phone_number_type . "_phone_number"}) {
	if (($phone_number !~ /^\s*$/) && (!grep {$_ eq $phone_number} @phone_number_list)) {
	  push(@phone_number_list, $phone_number);
	}
      }
    }
  }
  return join(" / ", @phone_number_list);
}


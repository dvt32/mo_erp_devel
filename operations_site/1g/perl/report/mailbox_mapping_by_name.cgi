#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use lead_pad;
use read_file;
use select_row;
use get_row_by_id_map;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $template_data = read_file("../resource/mailbox_mapping_by_name_template.xml");
  my $floor_id = $cgi->param("floor_id");
  if (!$floor_id) {
    die "show_page: mailbox_mapping_by_name_missing_floor_id.html\n";
  }
  # fetch information on the floor and the building.
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  my $building_id = $floor->{"building_id"};
  my $building = select_row($dbh, "building_tbl", $building_id);
  # select all mailboxes on the floor that are attached to accounts.
  my $sql_query = "select * from mailbox_tbl where floor_id = ? and account_id is not null";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  my (%mailbox_by_id, %mailbox_id_by_account_id);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%mailbox_by_id);
  for my $mailbox (values(%mailbox_by_id)) {
    $mailbox_id_by_account_id{$mailbox->{"account_id"}} = $mailbox->{"id"};
  }
  # select all active accounts with mailbox on the floor.
  $sql_query = "select * from account_vw where is_active and id in (select account_id from mailbox_tbl where floor_id = ?)";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  my (%account_by_id, @account_and_alias_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  for my $account (values(%account_by_id)) {
    if (!exists($mailbox_id_by_account_id{$account->{"id"}})) {
      next;
    }
    my $name = $account->{"name"};
    push(@account_and_alias_list, {"type" => "account", "name" => $name, "row" => $account});
  }
  # select all aliases attached to selected accounts.
  $sql_query = "select * from alias_tbl where account_id in (select id from account_vw where is_active and id in (select account_id from mailbox_tbl where floor_id = ?))";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  my %alias_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%alias_by_id);
  for my $alias (values(%alias_by_id)) {
    if (!exists($mailbox_id_by_account_id{$alias->{"account_id"}})) {
      next;
    }
    my $name = $alias->{"name"};
    push(@account_and_alias_list, {"type" => "alias", "name" => $name, "row" => $alias});
  }
  # sort the account and alias list by name
  @account_and_alias_list = sort {lc($a->{"name"}) cmp lc($b->{"name"})} @account_and_alias_list;
  my @mailbox_entry_list;
  my $mailbox_entry;
  # generate the mailbox entries.
  for my $potential_entry (@account_and_alias_list) {
    my $type = $potential_entry->{"type"};
    if ($type eq "account") {
      my $account = $potential_entry->{"row"};
      my $account_id = $account->{"id"};
      my $account_name = $account->{"name"};
      my $mailbox_id = $mailbox_id_by_account_id{$account_id};
      # if there is no mailbox for the account, skip over it.
      if (!defined($mailbox_id)) {
	next;
      }
      my $mailbox = $mailbox_by_id{$mailbox_id};
      my $mailbox_number = $mailbox->{"mailbox_number"};
      $mailbox_entry = {};
      $mailbox_entry->{"name"} = $account_name;
      $mailbox_entry->{"mailbox_number"} = $mailbox_number;
      push(@mailbox_entry_list, $mailbox_entry);
    } elsif ($type eq "alias") {
      my $alias = $potential_entry->{"row"};
      my $account = $account_by_id{$alias->{"account_id"}};
      # only include an alias if the linked account is active.
      if (defined($account)) {
	my $account_name = $account->{"name"};
	# only show an alias if it is different from the account name.
	if ($alias->{"name"} ne $account_name) {
	  my $name = HTML::Entities::encode_numeric($alias->{"name"}) . ':</w:t><w:t xml:space="preserve">  </w:t></w:r><w:r><w:rPr><w:i/><w:u w:val="single"/></w:rPr><w:t>see</w:t></w:r><w:r><w:t xml:space="preserve">  </w:t><w:t>' . HTML::Entities::encode_numeric($account_name);
	  $mailbox_entry = {};
	  $mailbox_entry->{"suppress_HTML::Entities::encode_name"} = 1;
	  $mailbox_entry->{"name"} = $name;
	  $mailbox_entry->{"mailbox_number"} = "";
	  push(@mailbox_entry_list, $mailbox_entry);
	}
      }
    } else {
      die "bad type: $type";
    }
  }
  my ($today_year, $today_month_number, $today_day_number) = get_date_now(1);
  my $date = "$today_year-$today_month_number-$today_day_number";
  $template_data =~ s{YYYY-MM-DD}{$date}gs;
  my $list_designation = $building->{"address_1"} . ", " . $floor->{"label"};
  my $filename_suffix = lc($floor->{"label"});
  $filename_suffix =~ s/\s+/_/;
  $template_data =~ s{LIST_DESIGNATION}{$list_designation}gs;
  if (1) {
    my $name = $mailbox_entry_list[0]->{"name"};
    $name = $mailbox_entry_list[0]->{"suppress_HTML::Entities::encode_name"} ? $name : HTML::Entities::encode_numeric($name);
    $template_data =~ s{ByNameName1}{$name}gs;
  }
  $template_data =~ s{\!1}{$mailbox_entry_list[0]->{"mailbox_number"}}ges;
  $template_data =~ m{&lt;even&gt;(.+?)&lt;/even&gt;}s;
  my $template_for_even_row = $1;
  $template_data =~ m{&lt;odd&gt;(.+?)&lt;/odd&gt;}s;
  my $template_for_odd_row = $1;
  my $additional_rows = "";
  for(my $i = 1; $i < @mailbox_entry_list; $i++) {
    my $template_for_row = "";
    if (($i + 1) % 2 == 0) {
      # even row
      $template_for_row = $template_for_even_row;
      my $name = $mailbox_entry_list[$i]->{"name"};
      $name = $mailbox_entry_list[$i]->{"suppress_HTML::Entities::encode_name"} ? $name : HTML::Entities::encode_numeric($name);
      $template_for_row =~ s{ByNameName2}{$name}gs;
      $template_for_row =~ s{\!2}{$mailbox_entry_list[$i]->{"mailbox_number"}}ges;
    } else {
      # odd row
      $template_for_row = $template_for_odd_row;
      my $name = $mailbox_entry_list[$i]->{"name"};
      $name = $mailbox_entry_list[$i]->{"suppress_HTML::Entities::encode_name"} ? $name : HTML::Entities::encode_numeric($name);
      $template_for_row =~ s{ByNameName3}{$name}gs;
      $template_for_row =~ s{\!3}{$mailbox_entry_list[$i]->{"mailbox_number"}}ges;
    }
    $additional_rows .= $template_for_row;
  }
  $template_data =~ s{&lt;even&gt;.+?&lt;/even&gt;&lt;odd&gt;.+?&lt;/odd&gt;}{$additional_rows}es;
  # print out data to stdout as a Word XML file.
  print $cgi->header
    ("-type" => "application/msword",
     "-content-disposition" => "attachment; filename=\"mailbox_mapping_by_name_$filename_suffix.xml\"");
  print $template_data;
}

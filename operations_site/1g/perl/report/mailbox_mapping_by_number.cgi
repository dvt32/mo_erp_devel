#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use lead_pad;
use read_file;
use select_row;
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $template_data = read_file("../resource/mailbox_mapping_by_number_template.xml");
  my $floor_id = $cgi->param("floor_id");
  if (!$floor_id) {
    die "show_page: mailbox_mapping_by_number_missing_floor_id.html\n";
  }
  # fetch information on the floor and the building.
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  my $building_id = $floor->{"building_id"};
  my $building = select_row($dbh, "building_tbl", $building_id);
  # select all mailboxes on the floor that are attached to accounts.
  my $sql_query = "select * from mailbox_tbl where floor_id = ? and account_id is not null order by mailbox_number";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  my (%mailbox_by_id, @mailbox_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%mailbox_by_id, \@mailbox_id_list);
  # select all active accounts attached to mailboxes.
  $sql_query = "select * from account_vw where is_active and id in (select account_id from mailbox_tbl where floor_id = ?)";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  my %account_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  my @mailbox_entry_list;
  my $mailbox_entry;
  for my $mailbox_id (@mailbox_id_list) {
    my $mailbox = $mailbox_by_id{$mailbox_id};
    my $account_id = $mailbox->{"account_id"};
    my $account = $account_by_id{$account_id};
    # only include mailboxes for active accounts.
    if (defined($account)) {
      my $account_name = $account->{"name"};
      $mailbox_entry = {};
      $mailbox_entry->{"mailbox_number"} = $mailbox->{"mailbox_number"};
      $mailbox_entry->{"name"} = $account_name;
      push(@mailbox_entry_list, $mailbox_entry);
    }
  }
  my ($today_year, $today_month_number, $today_day_number) = get_date_now(1);
  my $date = "$today_year-$today_month_number-$today_day_number";
  $template_data =~ s{YYYY-MM-DD}{$date}gs;
  my $list_designation = $building->{"address_1"} . ", " . $floor->{"label"};
  my $filename_suffix = lc($floor->{"label"});
  $filename_suffix =~ s/\s+/_/;
  $template_data =~ s{LIST_DESIGNATION}{$list_designation}gs;
  $template_data =~ s{ByNumberName1}{HTML::Entities::encode_numeric($mailbox_entry_list[0]->{"name"})}ges;
  $template_data =~ s{\!1}{$mailbox_entry_list[0]->{"mailbox_number"}}ges;
  $template_data =~ m{&lt;even&gt;(.+?)&lt;/even&gt;}s;
  my $template_for_even_row = $1;
  $template_data =~ m{&lt;odd&gt;(.+?)&lt;/odd&gt;}s;
  my $template_for_odd_row = $1;
  my $additional_rows = "";
  for(my $i = 1; $i < @mailbox_entry_list; $i++) {
    my $template_for_row = "";
    if (($i + 1) % 2 == 0) {
      # even row
      $template_for_row = $template_for_even_row;
      $template_for_row =~ s{ByNumberName2}{HTML::Entities::encode_numeric($mailbox_entry_list[$i]->{"name"})}ges;
      $template_for_row =~ s{\!2}{$mailbox_entry_list[$i]->{"mailbox_number"}}ges;
    } else {
      # odd row
      $template_for_row = $template_for_odd_row;
      $template_for_row =~ s{ByNumberName3}{HTML::Entities::encode_numeric($mailbox_entry_list[$i]->{"name"})}ges;
      $template_for_row =~ s{\!3}{$mailbox_entry_list[$i]->{"mailbox_number"}}ges;
    }
    $additional_rows .= $template_for_row;
  }
  $template_data =~ s{&lt;even&gt;.+?&lt;/even&gt;&lt;odd&gt;.+?&lt;/odd&gt;}{$additional_rows}es;
  # print out data to stdout as a Word XML file.
  print $cgi->header
    ("-type" => "application/msword",
     "-content-disposition" => "attachment; filename=\"mailbox_mapping_by_number_$filename_suffix.xml\"");
  print $template_data;
}

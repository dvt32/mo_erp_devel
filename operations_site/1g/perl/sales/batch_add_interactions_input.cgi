#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use get_row_list;
use lead_pad;
use get_month_name;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # select salesperson as the employee currently logged in
  my $username = $employee->{"first_name"};
  # load prospective customers.
  my (%prospective_customer_by_id, @prospective_customer_id_list);
  my $sql_query = "select distinct * from prospective_customer_tbl where (initial_contact_dt > now() - interval ? or follow_up_date >= current_date - interval ?) and sales_status = ? order by last_name, first_name, initial_contact_dt";
  my @sql_param_list = ("60 days", "30 days", "live");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%prospective_customer_by_id, \@prospective_customer_id_list);
  # determine number of interactions.
  my %num_interactions_by_prospective_customer_id;
  $sql_query = "select prospective_customer_id, count(id) as num_interactions from sales_interaction_tbl where prospective_customer_id in (select id from prospective_customer_tbl where initial_contact_dt > now() - interval ? or follow_up_date >= current_date - interval ?) group by prospective_customer_id";
  @sql_param_list = ("60 days", "30 days");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%num_interactions_by_prospective_customer_id);
  for my $prospective_customer_id (keys(%num_interactions_by_prospective_customer_id)) {
    my $row = $num_interactions_by_prospective_customer_id{$prospective_customer_id};
    $num_interactions_by_prospective_customer_id{$prospective_customer_id} = $row->{"num_interactions"};
  }
  # load enumeration of possible sales stages.
  my @enum_sales_stage_list;
  $sql_query = "select * from enum_sales_stage_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@enum_sales_stage_list);
  # load enumeration of possible sales tags.
  my @enum_sales_tag_list;
  $sql_query = "select * from enum_sales_tag_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@enum_sales_tag_list);
  # populate information needed for display of page.
  # determine current timestamp.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer-list>
---END_BLOCK---;
  for my $prospective_customer_id (@prospective_customer_id_list) {
    my $prospective_customer = $prospective_customer_by_id{$prospective_customer_id};
    my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"});
    my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
    my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"});
    my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
    my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
    my $num_interactions = $num_interactions_by_prospective_customer_id{$prospective_customer_id} || 0;
    my $follow_up_date = $prospective_customer->{"follow_up_date"} || "";
    my $reminder_email_address = HTML::Entities::encode($prospective_customer->{"reminder_email_address"} || "");
    my $sales_status = HTML::Entities::encode($prospective_customer->{"sales_status"} || "");
    my $sales_stage = HTML::Entities::encode($prospective_customer->{"sales_stage"} || "");
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
    my $marketing_source = HTML::Entities::encode($prospective_customer->{"marketing_source"} || "");
    my $referrer_url = HTML::Entities::encode($prospective_customer->{"referrer_url"} || "");
    my $initial_page_url = HTML::Entities::encode($prospective_customer->{"initial_page_url"} || "");
    # generate derived values.
    my $follow_up_month = "";
    my $follow_up_day = "";
    my $follow_up_year = "";
    if (($follow_up_date) && ($follow_up_date =~ /^(\d{4})-(\d\d)-(\d\d)$/)) {
      $follow_up_year = $1;
      $follow_up_month = int($2);
      $follow_up_day = int($3);
    }
    $generated_xml .= <<"---END_BLOCK---;";
    <prospective-customer>
      <id>$prospective_customer_id</id>
      <first_name>$first_name</first_name>
      <middle_name>$middle_name</middle_name>
      <last_name>$last_name</last_name>
      <title>$title</title>
      <company>$company</company>
      <initial_contact_dt>$initial_contact_dt</initial_contact_dt>
      <num_interactions>$num_interactions</num_interactions>
    </prospective-customer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </prospective-customer-list>
  <quick-edit-form>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
  </quick-edit-form>
  <interaction-type-list>
    <interaction-type>call</interaction-type>
    <interaction-type>email</interaction-type>
    <interaction-type>meeting</interaction-type>
  </interaction-type-list>
  <user>$username</user>
  <salesperson-list>
    <salesperson>
      <name>David</name>
      <value>drotbard</value>
    </salesperson>
    <salesperson>
      <name>Wellie</name>
      <value>wchao</value>
    </salesperson>
    <salesperson>
      <name>Matthieu</name>
      <value>mrosanvallon</value>
    </salesperson>
    <salesperson>
      <name>Danielle</name>
      <value>dnoket</value>
    </salesperson>
  </salesperson-list>
  <enum-sales-status-list>
    <sales-status>live</sales-status>
    <sales-status>customer</sales-status>
    <sales-status>dead</sales-status>
  </enum-sales-status-list>
  <enum-sales-stage-list>
---END_BLOCK---;
  for my $sales_stage (@enum_sales_stage_list) {
    my $name = HTML::Entities::encode($sales_stage->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-stage>$name</sales-stage>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-stage-list>
  <enum-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@enum_sales_tag_list) {
    my $name = HTML::Entities::encode($sales_tag->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$name</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-tag-list>
  <current-timestamp>
    <month>$month</month>
    <day>$day</day>
    <year>$year</year>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
  </current-timestamp>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <second-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <second>$padded_number</second>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </second-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "batch_add_interactions_input.xslt", $cgi);
}

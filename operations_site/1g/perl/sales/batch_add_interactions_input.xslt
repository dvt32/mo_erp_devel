<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Batch Add Interactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	</p>
	<xsl:if test="count(saved-interaction-list/saved-interaction) > 0">
	  <xsl:apply-templates select="saved-interaction-list"/>
	</xsl:if>
	<form action="/1g/sales/batch_add_interactions_result.cgi" method="post">
	  <p>
	    <input type="submit" name="submitButton" value="Save All"/>
	  </p>
	  <xsl:apply-templates select="quick-edit-form/quick-edit"/>
	  <p>
	    <input type="submit" name="submitButton" value="Save All"/>
	  </p>
	</form>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="saved-interaction-list">
    <p>You saved new interactions attached to the following prospective customers:</p>
    <ul>
      <xsl:apply-templates select="saved-interaction"/>
    </ul>
    <p>You can enter more new interactions below or click on a navigation link above.</p>
  </xsl:template>

  <xsl:template match="saved-interaction">
    <li>
      <xsl:text>Interaction #</xsl:text>
      <xsl:value-of select="index_position"/>
      <xsl:text>: </xsl:text>
      <xsl:variable name="prospective_customer_id" select="prospective_customer_id"/>
      <xsl:variable name="prospective_customer" select="/page/saved-prospective-customer-list/saved-prospective-customer[id = $prospective_customer_id]"/>
      <xsl:value-of select="$prospective_customer/last_name"/>
      <xsl:text>, </xsl:text>
      <xsl:value-of select="$prospective_customer/first_name"/>
      <xsl:if test="$prospective_customer/middle_name != ''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$prospective_customer/middle_name"/>
      </xsl:if>
      <xsl:if test="$prospective_customer/title != '' or $prospective_customer/company != ''">
	<xsl:text> (</xsl:text>
	<xsl:if test="$prospective_customer/title != ''">
	  <xsl:value-of select="$prospective_customer/title"/>
	  <xsl:if test="$prospective_customer/company != ''">
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	</xsl:if>
	<xsl:if test="$prospective_customer/company != ''">
	  <xsl:value-of select="$prospective_customer/company"/>
	</xsl:if>
	<xsl:text>)</xsl:text>
      </xsl:if>
      <xsl:text> // id: </xsl:text>
      <xsl:value-of select="$prospective_customer/id"/>
      <xsl:text> // timestamp: </xsl:text>
      <xsl:value-of select="$prospective_customer/initial_contact_dt"/>
    </li>
  </xsl:template>

  <xsl:template match="quick-edit">
    <xsl:if test="position() > 1">
      <br/>
    </xsl:if>
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap;">
      <tr>
	<td class="standard_table_title">
	  <xsl:text>Interaction #</xsl:text>
	  <xsl:value-of select="position()"/>
	</td>
      </tr>
      <tr><td>
	<table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	  <tr>
	    <td style="padding: 4px; text-align: right; ">
	      Prospect:
	    </td>
	    <td colspan="2">
	      <select>
		<xsl:attribute name="name">
		  <xsl:value-of select="position()"/>
		  <xsl:text>_prospective_customer_id</xsl:text>
		</xsl:attribute>
		<option value="">[---]</option>
		<xsl:for-each select="/page/prospective-customer-list/prospective-customer">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		    <xsl:value-of select="last_name"/>
		    <xsl:text>, </xsl:text>
		    <xsl:value-of select="first_name"/>
		    <xsl:if test="middle_name != ''">
		      <xsl:text> </xsl:text>
		      <xsl:value-of select="middle_name"/>
		    </xsl:if>
		    <xsl:if test="title != '' or company != ''">
		      <xsl:text> (</xsl:text>
		      <xsl:if test="title != ''">
			<xsl:value-of select="title"/>
			<xsl:if test="company != ''">
			  <xsl:text>, </xsl:text>
			</xsl:if>
		      </xsl:if>
		      <xsl:if test="company != ''">
			<xsl:value-of select="company"/>
		      </xsl:if>
		      <xsl:text>)</xsl:text>
		    </xsl:if>
		    <xsl:text> // id: </xsl:text>
		    <xsl:value-of select="id"/>
		    <xsl:text> // timestamp: </xsl:text>
		    <xsl:value-of select="initial_contact_dt"/>
		    <xsl:if test="num_interactions > 0">
		      <xsl:text> // interactions: </xsl:text>
		      <xsl:value-of select="num_interactions"/>
		    </xsl:if>
		  </option>
		</xsl:for-each>
	      </select>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; text-align: right; ">
	      Follow-up Date:
	    </td>
	    <td>
	      <select>
				  <xsl:attribute name="name"><xsl:value-of select="position()"/>_follow_up_month</xsl:attribute>
					<option value="">[---]</option>
					<xsl:for-each select="/page/month-list/month">
						<option>
							<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
							<xsl:value-of select="name"/>
						</option>
					</xsl:for-each>
	      </select>
	      <select>
				  <xsl:attribute name="name"><xsl:value-of select="position()"/>_follow_up_day</xsl:attribute>
					<option value="">[---]</option>
					<xsl:for-each select="/page/day-list/day">
						<option>
							<xsl:value-of select="."/>
						</option>
					</xsl:for-each>
	      </select>
	      <input>
					<xsl:attribute name="type">text</xsl:attribute>
					<xsl:attribute name="name"><xsl:value-of select="position()"/>_follow_up_year</xsl:attribute>
					<xsl:attribute name="size">2</xsl:attribute>
					<xsl:attribute name="value">
						<xsl:value-of select="/page/current-timestamp/year"/>
					</xsl:attribute>
	      </input>
	    </td>
	    <td rowspan="5">
	      <table style="margin: 1em; border-left: thin solid gray; " cols="4">
		<tr>
		  <td style="padding: 4px; text-align: right; " valign="top">Interaction Time:</td>
		  <td colspan="3">
		    <select>
		      <xsl:attribute name="name">
						<xsl:value-of select="position()"/>
						<xsl:text>_month</xsl:text>
		      </xsl:attribute>
		      <xsl:for-each select="/page/month-list/month">
			<option>
			  <xsl:if test="/page/current-timestamp/month = number">
			    <xsl:attribute name="selected">selected</xsl:attribute>
			  </xsl:if>
			  <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			  <xsl:value-of select="name"/>
			</option>
		      </xsl:for-each>
		    </select>
		    <select name="day">
		      <xsl:attribute name="name">
			<xsl:value-of select="position()"/>
			<xsl:text>_day</xsl:text>
		      </xsl:attribute>
		      <xsl:for-each select="/page/day-list/day">
			<option>
			  <xsl:if test="/page/current-timestamp/day = .">
			    <xsl:attribute name="selected">selected</xsl:attribute>
			  </xsl:if>
			  <xsl:value-of select="."/>
			</option>
		      </xsl:for-each>
		    </select>
		    <input>
		      <xsl:attribute name="type">text</xsl:attribute>
		      <xsl:attribute name="name">
			<xsl:value-of select="position()"/>
			<xsl:text>_year</xsl:text>
		      </xsl:attribute>
		      <xsl:attribute name="size">2</xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="/page/current-timestamp/year"/></xsl:attribute>
		    </input>
		    <xsl:text> at </xsl:text>
		    <select>
		      <xsl:attribute name="name">
			<xsl:value-of select="position()"/>
			<xsl:text>_hour</xsl:text>
		      </xsl:attribute>
		      <xsl:for-each select="/page/hour-list/hour">
			<option>
			  <xsl:if test="/page/current-timestamp/hour = .">
			    <xsl:attribute name="selected">selected</xsl:attribute>
			  </xsl:if>
			  <xsl:value-of select="."/>
			</option>
		      </xsl:for-each>
		    </select>
		    <xsl:text>:</xsl:text>
		    <select name="minute">
		      <xsl:attribute name="name">
			<xsl:value-of select="position()"/>
			<xsl:text>_minute</xsl:text>
		      </xsl:attribute>
		      <xsl:for-each select="/page/minute-list/minute">
			<option>
			  <xsl:if test="/page/current-timestamp/minute = .">
			    <xsl:attribute name="selected">selected</xsl:attribute>
			  </xsl:if>
			  <xsl:value-of select="."/>
			</option>
		      </xsl:for-each>
		    </select>
		    <xsl:text>:</xsl:text>
		    <select name="second">
		      <xsl:attribute name="name">
			<xsl:value-of select="position()"/>
			<xsl:text>_second</xsl:text>
		      </xsl:attribute>
		      <xsl:for-each select="/page/second-list/second">
			<option>
			  <xsl:if test="/page/current-timestamp/second = .">
			    <xsl:attribute name="selected">selected</xsl:attribute>
			  </xsl:if>
			  <xsl:value-of select="."/>
			</option>
		      </xsl:for-each>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; text-align: right; " valign="top">
		    Interaction Type:
		  </td>
		  <td valign="top">
				<xsl:variable name="position"><xsl:value-of select="position()"/></xsl:variable>  
		    <xsl:for-each select="/page/interaction-type-list/interaction-type">
		      <xsl:element name="input">
						<xsl:attribute name="type">radio</xsl:attribute>
						<xsl:attribute name="name"><xsl:value-of select="$position"/>_interaction_type</xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
						<xsl:if test="'call' = .">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="."/>
						<br />
		      </xsl:element>
		    </xsl:for-each>
		  </td>
		  <td style="padding: 4px; text-align: right; " valign="top">
		    Salesperson:
		  </td>
		  <td>
				<xsl:variable name="position"><xsl:value-of select="position()"/></xsl:variable>  
		    <xsl:for-each select="/page/salesperson-list/salesperson">
          <xsl:element name="input">
						<xsl:attribute name="type">radio</xsl:attribute>
						<xsl:attribute name="name"><xsl:value-of select="$position"/><xsl:text>_salesperson</xsl:text></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
						<xsl:if test="/page/user = name">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="name"/>
						<br />
		      </xsl:element>
		    </xsl:for-each>
		  </td>
		</tr>
		<tr valign="top">
		  <td style="padding: 4px; text-align: right; ">
		    Notes:
		  </td>
		  <td colspan="3">
		    <textarea>
		      <xsl:attribute name="name">
			<xsl:value-of select="position()"/>
			<xsl:text>_notes</xsl:text>
		      </xsl:attribute>
		      <xsl:attribute name="rows">4</xsl:attribute>
		      <xsl:attribute name="cols">40</xsl:attribute>
		      <!-- Need this blank notes field; otherwise the surrounding textarea gets collapsed into a single tag. -->
		      <xsl:choose>
			<xsl:when test="1">.</xsl:when>
			<xsl:otherwise>
			  <xsl:value-of select="notes"/>
			</xsl:otherwise>
		      </xsl:choose>
		    </textarea>
		  </td>
		</tr>
	      </table>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; text-align: right; ">
	      Reminder Email Address:
	    </td>
	    <td>
	      <input>
					<xsl:attribute name="type">text</xsl:attribute>
					<xsl:attribute name="name"><xsl:value-of select="position()"/>_reminder_email_address</xsl:attribute>
					<xsl:attribute name="size">30</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr valign="top">
	    <td colspan="2">
	      <table><tr>
					<td style="padding: 4px; text-align: right; white-space: nowrap; " valign="top">
						Sales Status:
					</td>
					<td style="white-space: nowrap; " valign="top">
						<xsl:variable name="position"><xsl:value-of select="position()"/></xsl:variable>  
						<xsl:for-each select="/page/enum-sales-status-list/sales-status">
							<xsl:element name="input">
								<xsl:attribute name="type">radio</xsl:attribute>
								<xsl:attribute name="name"><xsl:value-of select="$position"/>_sales_status</xsl:attribute>
								<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
								<xsl:value-of select="."/>
								<br />
							</xsl:element>
						</xsl:for-each>
					</td>
					<td style="padding: 4px; text-align: right; white-space: nowrap; " valign="top">
						Sales Stage:
					</td>
					<td style="white-space: nowrap; ">
						<xsl:variable name="position"><xsl:value-of select="position()"/></xsl:variable>  
						<xsl:for-each select="/page/enum-sales-stage-list/sales-stage">
							<xsl:element name="input">
								<xsl:attribute name="type">radio</xsl:attribute>
								<xsl:attribute name="name"><xsl:value-of select="$position"/>_sales_stage</xsl:attribute>
								<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
								<xsl:value-of select="."/>
								<br />
							</xsl:element>
						</xsl:for-each>
					</td>
	      </tr></table>
	    </td>
	  </tr>
	  <tr valign="top">
	    <td colspan="3" style="text-align: right; ">
	      <input type="submit" name="submitButton" value="Save"/>
	    </td>
	  </tr>
	</table>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

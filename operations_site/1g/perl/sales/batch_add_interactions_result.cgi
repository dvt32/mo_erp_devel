#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use is_valid_email;
use lead_pad;
use insert_row;
use update_row;
use select_row;
use get_row_by_id_map;
use get_row_list;
use get_month_name;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my @interaction_list;
  my @customer_edits_list;
  my $i = 1;
  while($cgi->param($i . "_prospective_customer_id")) {
    my %interaction = ();
    for my $field_name (qw(prospective_customer_id month day year hour minute second interaction_type salesperson notes)) {
      $interaction{$field_name} = Encode::decode($cgi_input_charset, $cgi->param($i . "_" . $field_name));
    }
    $interaction{"index_position"} = $i;
    push(@interaction_list, \%interaction);
    my $edits_to_save = 0;
    my %customer_edits = ();
    for my $field_name (qw(prospective_customer_id follow_up_month follow_up_day follow_up_year reminder_email_address sales_status sales_stage sales_tag_list)) {
      if($cgi->param($i . "_" . $field_name)) {
        $customer_edits{$field_name} = Encode::decode($cgi_input_charset, $cgi->param($i . "_" . $field_name));
        $edits_to_save++;
      }
    }
    if ($edits_to_save) {
      $customer_edits{"index_position"} = $i;
      push(@customer_edits_list, \%customer_edits);
    }
    $i++;
  }
  # check for errors.
  my @error_list;
  for my $interaction (@interaction_list) {
    my $index_position = $interaction->{"index_position"};
    # verify that prospective customer id was specified.
    my $prospective_customer_id = $interaction->{"prospective_customer_id"};
    if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/)) {
      push(@error_list, "missing prospect selection in interaction #" . $index_position . ".");
    }
    # verify that interaction timestamp is valid.
    my $month = $interaction->{"month"};
    my $day = $interaction->{"day"};
    my $year = $interaction->{"year"};
    my $hour = $interaction->{"hour"};
    my $minute = $interaction->{"minute"};
    my $second = $interaction->{"second"};
    if (($month < 1) || ($month > 12)) {
      push(@error_list, "bad month in interaction #" . $index_position . ".");
    }
    $month = lead_pad(int($month), 2);
    if (($day < 1) || ($day > 31)) {
      push(@error_list, "bad day in interaction #" . $index_position . ".");
    }
    $day = lead_pad(int($day), 2);
    if (($year < 2000) || ($year > 9999)) {
      push(@error_list, "bad year in interaction #" . $index_position . ".");
    }
    $year = int($year);
    if (($hour < 0) || ($hour > 23)) {
      push(@error_list, "bad hour in interaction #" . $index_position . ".");
    }
    $hour = lead_pad(int($hour), 2);
    if (($minute < 0) || ($minute > 59)) {
      push(@error_list, "bad minute in interaction #" . $index_position . ".");
    }
    $minute = lead_pad(int($minute), 2);
    if (($second < 0) || ($second > 59)) {
      push(@error_list, "bad second in interaction #" . $index_position . ".");
    }
    $second = lead_pad(int($second), 2);
    my $interaction_dt = "$year-$month-$day $hour:$minute:$second";
    $interaction->{"interaction_dt"} = $interaction_dt;
    # verify that interaction type is valid.
    my $interaction_type = $interaction->{"interaction_type"};
    if ($interaction_type !~ /^(call|email|meeting)$/) {
      push(@error_list, "bad interaction type in interaction #" . $index_position . ".");
    }
    # verify that notes were specified.
    my $notes = $interaction->{"notes"};
  }
  for my $customer_edits (@customer_edits_list) {
    my $index_position = $customer_edits->{"index_position"};
    # verify that prospective customer id was specified.
    my $prospective_customer_id = $customer_edits->{"prospective_customer_id"};
    if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/)) {
      push(@error_list, "missing prospect selection in interaction #" . $index_position . ".");
    }
    if ($customer_edits->{"follow_up_month"} || $customer_edits->{"follow_up_day"}) {
      my $follow_up_month = $customer_edits->{"follow_up_month"};
      my $follow_up_day = $customer_edits->{"follow_up_day"};
      my $follow_up_year = $customer_edits->{"follow_up_year"};
      if (!$follow_up_month || !$follow_up_day || !$follow_up_year) {
        push(@error_list, "bad follow-up date in interaction #" . $index_position . ".");
      }
      my $follow_up_date = $follow_up_year . "-" . $follow_up_month . "-" . $follow_up_day;
      $customer_edits->{"follow_up_date"} = $follow_up_date;
    }
    if ($customer_edits->{"reminder_email_address"}) {
      if (!is_valid_email($customer_edits->{"reminder_email_address"})) {
        push(@error_list, "bad email in interaction #" . $index_position . ".");
      }
    }
  }
  if (@error_list) {
    print_error_list($cgi, @error_list);
    return;
  }
  # load prospective customers (for display of form).
  my (%prospective_customer_by_id, @prospective_customer_id_list);
  my $sql_query = "select distinct * from prospective_customer_tbl where initial_contact_dt > now() - interval ? or follow_up_date >= current_date - interval ? order by last_name, first_name, initial_contact_dt";
  my @sql_param_list = ("60 days", "30 days");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%prospective_customer_by_id, \@prospective_customer_id_list);
  # load enumeration of possible sales stages.
  my @enum_sales_stage_list;
  $sql_query = "select * from enum_sales_stage_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@enum_sales_stage_list);
  # load enumeration of possible sales tags.
  my @enum_sales_tag_list;
  $sql_query = "select * from enum_sales_tag_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@enum_sales_tag_list);
  # determine number of interactions (for display of form).
  my %num_interactions_by_prospective_customer_id;
  $sql_query = "select prospective_customer_id, count(id) as num_interactions from sales_interaction_tbl where prospective_customer_id in (select id from prospective_customer_tbl where initial_contact_dt > now() - interval ? or follow_up_date >= current_date - interval ?) group by prospective_customer_id";
  @sql_param_list = ("60 days", "30 days");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%num_interactions_by_prospective_customer_id);
  for my $prospective_customer_id (keys(%num_interactions_by_prospective_customer_id)) {
    my $row = $num_interactions_by_prospective_customer_id{$prospective_customer_id};
    $num_interactions_by_prospective_customer_id{$prospective_customer_id} = $row->{"num_interactions"};
  }
  # begin transaction of inserting the new rows.
  $dbh->begin_work();
  # verify that the prospective customers exist (this also loads them for later display).
  my %saved_prospective_customer_by_id;
  for my $interaction (@interaction_list) {
    my $prospective_customer_id = $interaction->{"prospective_customer_id"};
    my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
    if (!$prospective_customer) {
      my $index_position = $interaction->{"index_position"};
      push(@error_list, "could not find linked prospective customer for interaction #" . $index_position . ". It could have been deleted.");
    }
    $saved_prospective_customer_by_id{$prospective_customer_id} = $prospective_customer;
  }
  if (@error_list) {
    print_error_list($cgi, @error_list);
    return;
  }
  # insert new rows.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  for my $interaction (@interaction_list) {
    @sql_column_list = ();
    my $prospective_customer_id = $interaction->{"prospective_customer_id"};
    my $interaction_dt = $interaction->{"interaction_dt"};
    my $interaction_type = $interaction->{"interaction_type"};
    my $salesperson = $interaction->{"salesperson"};
    my $notes = $interaction->{"notes"};
    push(@sql_column_list, ["prospective_customer_id", $prospective_customer_id]);
    push(@sql_column_list, ["interaction_dt", $interaction_dt]);
    push(@sql_column_list, ["interaction_type", $interaction_type]);
    push(@sql_column_list, ["salesperson", Encode::encode($db_charset, $salesperson)]);
    push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
    insert_row($dbh, "sales_interaction_tbl", \@sql_column_list);
  }
  # commit the transaction.
  $dbh->commit();
  # process customer status edits
  for my $customer_edits (@customer_edits_list) {
    my $prospective_customer_id = $customer_edits->{"prospective_customer_id"};
    my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
    if (!$prospective_customer) {
      my $index_position = $customer_edits->{"index_position"};
      push(@error_list, "could not find linked prospective customer for interaction #" . $index_position . ". It could have been deleted.");
    }
    $saved_prospective_customer_by_id{$prospective_customer_id} = $prospective_customer;
  }
  if (@error_list) {
    print_error_list($cgi, @error_list);
    return;
  }
  # update customers in database
  $dbh->begin_work();
  for my $customer_edits (@customer_edits_list) {
    my $prospective_customer_id = $customer_edits->{"prospective_customer_id"};
    my $follow_up_date = $customer_edits->{"follow_up_date"};
    my $reminder_email_address = $customer_edits->{"reminder_email_address"};
    my $sales_status = $customer_edits->{"sales_status"};
    my $sales_stage = $customer_edits->{"sales_stage"};
    my (@sql_column_list);
    if ($follow_up_date) { 
      push(@sql_column_list, ["follow_up_date", $follow_up_date]);
    }
    if ($reminder_email_address) {
      push(@sql_column_list, ["reminder_email_address", $reminder_email_address]);
    }
    if ($sales_status) {
      push(@sql_column_list, ["sales_status", $sales_status]);
    }
    if ($sales_stage) {
      push(@sql_column_list, ["sales_stage", $sales_stage]);
    }
    if (@sql_column_list) {
      update_row($dbh, "prospective_customer_tbl", \@sql_column_list, $prospective_customer_id);
    }
    # fetch updated prospective customer.
    my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
    $saved_prospective_customer_by_id{$prospective_customer_id} = $prospective_customer;
  }
  # populate information needed for display of page.
  # determine current timestamp.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <saved-prospective-customer-list>
---END_BLOCK---;
  for my $prospective_customer_id (keys(%saved_prospective_customer_by_id)) {
    my $prospective_customer = $saved_prospective_customer_by_id{$prospective_customer_id};
    my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"});
    my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
    my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"});
    my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
    my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
    my $follow_up_month = $prospective_customer->{"follow_up_month"} || "";
    my $follow_up_day = $prospective_customer->{"follow_up_day"} || "";
    my $follow_up_year = $prospective_customer->{"follow_up_year"} || "";
    my $reminder_email_address = HTML::Entities::encode($prospective_customer->{"reminder_email_address"} || "");
    my $sales_status = HTML::Entities::encode($prospective_customer->{"sales_status"});
    my $sales_stage = HTML::Entities::encode($prospective_customer->{"sales_stage"});
    $generated_xml .= <<"---END_BLOCK---;";
    <saved-prospective-customer>
      <id>$prospective_customer_id</id>
      <first_name>$first_name</first_name>
      <middle_name>$middle_name</middle_name>
      <last_name>$last_name</last_name>
      <title>$title</title>
      <company>$company</company>
      <initial_contact_dt>$initial_contact_dt</initial_contact_dt>
      <follow_up_month>$follow_up_month</follow_up_month>
      <follow_up_day>$follow_up_day</follow_up_day>
      <follow_up_year>$follow_up_year</follow_up_year>
      <reminder_email_address>$reminder_email_address</reminder_email_address>
      <sales_status>$sales_status</sales_status>
      <sales_stage>$sales_stage</sales_stage>
    </saved-prospective-customer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </saved-prospective-customer-list>
  <saved-interaction-list>
---END_BLOCK---;
  for my $interaction (@interaction_list) {
    my $prospective_customer_id = $interaction->{"prospective_customer_id"};
    my $index_position = $interaction->{"index_position"};
    $generated_xml .= <<"---END_BLOCK---;";
    <saved-interaction>
      <prospective_customer_id>$prospective_customer_id</prospective_customer_id>
      <index_position>$index_position</index_position>
    </saved-interaction>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </saved-interaction-list>
  <prospective-customer-list>
---END_BLOCK---;
  for my $prospective_customer_id (@prospective_customer_id_list) {
    my $prospective_customer = $prospective_customer_by_id{$prospective_customer_id};
    my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"});
    my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
    my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"});
    my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
    my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
    my $num_interactions = $num_interactions_by_prospective_customer_id{$prospective_customer_id} || 0;
    $generated_xml .= <<"---END_BLOCK---;";
    <prospective-customer>
      <id>$prospective_customer_id</id>
      <first_name>$first_name</first_name>
      <middle_name>$middle_name</middle_name>
      <last_name>$last_name</last_name>
      <title>$title</title>
      <company>$company</company>
      <initial_contact_dt>$initial_contact_dt</initial_contact_dt>
      <num_interactions>$num_interactions</num_interactions>
    </prospective-customer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </prospective-customer-list>
  <quick-edit-form>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
    <quick-edit/>
  </quick-edit-form>
  <interaction-type-list>
    <interaction-type>call</interaction-type>
    <interaction-type>email</interaction-type>
    <interaction-type>meeting</interaction-type>
  </interaction-type-list>
  <salesperson-list>
    <salesperson>
      <name>David</name>
      <value>drotbard</value>
    </salesperson>
    <salesperson>
      <name>Wellie</name>
      <value>wchao</value>
    </salesperson>
    <salesperson>
      <name>Matthieu</name>
      <value>mrosanvallon</value>
    </salesperson>
    <salesperson>
      <name>Danielle</name>
      <value>dnoket</value>
    </salesperson>
  </salesperson-list>
  <enum-sales-status-list>
    <sales-status>live</sales-status>
    <sales-status>customer</sales-status>
    <sales-status>dead</sales-status>
  </enum-sales-status-list>
  <enum-sales-stage-list>
---END_BLOCK---;
  for my $sales_stage (@enum_sales_stage_list) {
    my $name = HTML::Entities::encode($sales_stage->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-stage>$name</sales-stage>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-stage-list>
  <enum-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@enum_sales_tag_list) {
    my $name = HTML::Entities::encode($sales_tag->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$name</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-tag-list>
  <current-timestamp>
    <month>$month</month>
    <day>$day</day>
    <year>$year</year>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
  </current-timestamp>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <second-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <second>$padded_number</second>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </second-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "batch_add_interactions_input.xslt", $cgi);
}

sub print_error_list($@) {
  my $cgi = shift;
  my @error_list = @_;
  my $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<page>
  <error-list>
---END_BLOCK---;
  for my $error (@error_list) {
    $generated_xml .= <<"---END_BLOCK---;";
    <error>$error</error>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </error-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "batch_add_interactions_result_error.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row_with_query;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load virtual office application.
  my $virtual_office_application_id = $cgi->param("id");
  if ((!$virtual_office_application_id) || ($virtual_office_application_id !~ /^\d+$/)) {
    die "show_page: cancel_virtual_office_application_not_found.html\n";
  }
  my $sql_query = "select * from virtual_office_application_tbl where id = ? and not(processed) and not(cancelled)";
  my @sql_param_list = ([$virtual_office_application_id, SQL_INTEGER]);
  my $virtual_office_application = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if (!$virtual_office_application) {
    die "show_page: cancel_virtual_office_application_not_found.html\n";
  }
  my @sql_column_list;
  push(@sql_column_list, ["cancelled", 1, SQL_BOOLEAN]);
  update_row($dbh, "virtual_office_application_tbl", \@sql_column_list, $virtual_office_application_id);
  # populate information needed for display of page.
  my $request_datetime = $virtual_office_application->{"request_datetime"};
  $salutation = HTML::Entities::encode($salutation);
  $first_name = HTML::Entities::encode($first_name);
  $middle_name = HTML::Entities::encode($middle_name);
  $last_name = HTML::Entities::encode($last_name);
  $suffix = HTML::Entities::encode($suffix);
  $title = HTML::Entities::encode($title);
  $company = HTML::Entities::encode($company);
  $business_description = HTML::Entities::encode($business_description);
  $mailing_address_1 = HTML::Entities::encode($mailing_address_1);
  $mailing_address_2 = HTML::Entities::encode($mailing_address_2);
  $mailing_city = HTML::Entities::encode($mailing_city);
  $mailing_state = HTML::Entities::encode($mailing_state);
  $mailing_zip_code = HTML::Entities::encode($mailing_zip_code);
  $mailing_country = HTML::Entities::encode($mailing_country);
  $work_phone_number = HTML::Entities::encode($work_phone_number);
  $home_phone_number = HTML::Entities::encode($home_phone_number);
  $cell_phone_number = HTML::Entities::encode($cell_phone_number);
  $fax_number = HTML::Entities::encode($fax_number);
  $email_address = HTML::Entities::encode($email_address);
  $web_site = HTML::Entities::encode($web_site);
  $name_on_card = HTML::Entities::encode($name_on_card);
  $company_on_card = HTML::Entities::encode($company_on_card);
  $credit_card_type = HTML::Entities::encode($credit_card_type);
  $billing_address_1 = HTML::Entities::encode($billing_address_1);
  $billing_address_2 = HTML::Entities::encode($billing_address_2);
  $billing_city = HTML::Entities::encode($billing_city);
  $billing_state = HTML::Entities::encode($billing_state);
  $billing_zip_code = HTML::Entities::encode($billing_zip_code);
  $billing_country = HTML::Entities::encode($billing_country);
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  my $mailbox_number = HTML::Entities::encode($mailbox->{"mailbox_number"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <virtual-office-application>
    <id>$virtual_office_application_id</id>
    <request_datetime>$request_datetime</request_datetime>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <mailing_address_1>$mailing_address_1</mailing_address_1>
    <mailing_address_2>$mailing_address_2</mailing_address_2>
    <mailing_city>$mailing_city</mailing_city>
    <mailing_state>$mailing_state</mailing_state>
    <mailing_zip_code>$mailing_zip_code</mailing_zip_code>
    <mailing_country>$mailing_country</mailing_country>
    <work_phone_number>$work_phone_number</work_phone_number>
    <home_phone_number>$home_phone_number</home_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <fax_number>$fax_number</fax_number>
    <email_address>$email_address</email_address>
    <web_site>$web_site</web_site>
    <name_on_card>$name_on_card</name_on_card>
    <company_on_card>$company_on_card</company_on_card>
    <credit_card_type>$credit_card_type</credit_card_type>
    <credit_card_number>$credit_card_number</credit_card_number>
    <expiration_month>$expiration_month</expiration_month>
    <expiration_year>$expiration_year</expiration_year>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
  </virtual-office-application>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "cancel_virtual_office_application.xslt", $cgi);
}

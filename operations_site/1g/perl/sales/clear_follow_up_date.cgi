#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  # verify that prospective customer id was specified and exists.
  my $prospective_customer_id = $cgi->param("id");
  if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/)) {
    die "show_page: clear_follow_up_date_not_found.html\n";
  }
  # begin transaction of deleting the row.
  $dbh->begin_work();
  # verify that the prospective customer exists.
  my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  if (!$prospective_customer) {
    $dbh->rollback();
    die "show_page: clear_follow_up_date_not_found.html\n";
  }
  # issue the update command to the database.
  my (@sql_column_list);
  push(@sql_column_list, ["follow_up_date", undef]);
  update_row($dbh, "prospective_customer_tbl", \@sql_column_list, $prospective_customer_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"} || "");
  my $suffix = HTML::Entities::encode($prospective_customer->{"suffix"} || "");
  my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
  my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
  my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
  my @space_needs_component_list;
  for my $tag_name (qw(amount_of_space rental_start_date duration_of_rental desired_location)) {
    if ($prospective_customer->{$tag_name}) {
      push(@space_needs_component_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
    }
  }
  my $follow_up_date = $prospective_customer->{"follow_up_date"};
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <space_needs>
---END_BLOCK---;
  for my $space_needs_component (@space_needs_component_list) {
    my $tag_name = $space_needs_component->[0];
    my $tag_value = $space_needs_component->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </space_needs>
    <follow_up_date>$follow_up_date</follow_up_date>
  </prospective-customer>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "clear_follow_up_date.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Clear Follow-Up Date
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_follow_ups.cgi">Follow-ups</a>
	  |
	  <a href="/1g/sales/show_recent_interactions.cgi">Recent interactions</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	  |
	  <a>
	    <xsl:attribute name="href">/1g/sales/show_prospect_detail.cgi?id=<xsl:value-of select="/page/prospective-customer/id"/></xsl:attribute>
	    <xsl:text>Prospect detail</xsl:text>
	  </a>
	</p>
	<xsl:apply-templates select="prospective-customer"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="prospective-customer">
    <p>
      You successfully cleared the follow-up date of the prospective customer shown below:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="first_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="middle_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="last_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="suffix"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Company:
	</td>
	<td class="map_table_value">
	  <xsl:if test="title != '' or company != ''">
	    <xsl:if test="title != ''">
	      <xsl:value-of select="title"/>,
	    </xsl:if>
	    <xsl:value-of select="company"/>
	  </xsl:if>
	  <xsl:if test="business_description != ''">
	    <xsl:if test="title != '' or company != ''">
	      <xsl:text> // </xsl:text>
	    </xsl:if>
	    <xsl:text>description: </xsl:text>
	    <xsl:value-of select="business_description"/>
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Space Needs:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="space_needs"/>
	</td>
      </tr>
    </table>
    <p>
      <xsl:text>The follow-up date was previously </xsl:text>
      <xsl:choose>
	<xsl:when test="string(follow_up_date) = ''">
	  <xsl:text>(blank)</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="follow_up_date"/>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:text>. Now it is blank.</xsl:text>
    </p>
  </xsl:template>

  <xsl:template match="space_needs">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="amount_of_space | rental_start_date | duration_of_rental | desired_location">
    <xsl:choose>
      <xsl:when test="name(.) = 'amount_of_space'">
	<xsl:text>size: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'rental_start_date'">
	<xsl:text>start: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'duration_of_rental'">
	<xsl:text>duration: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'desired_location'">
	<xsl:text>location: </xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:value-of select="."/>
    <xsl:choose>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

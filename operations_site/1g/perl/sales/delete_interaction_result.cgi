#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use delete_row;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  # verify that interaction id was specified and exists.
  my $interaction_id = $cgi->param("id");
  if ((!defined($interaction_id)) || ($interaction_id !~ /^\d+$/)) {
    die "show_page: delete_interaction_not_found.html\n";
  }
  # begin transaction of deleting the row.
  $dbh->begin_work();
  # verify that the interaction exists.
  my $interaction = select_row($dbh, "sales_interaction_tbl", $interaction_id);
  if (!$interaction) {
    $dbh->rollback();
    die "show_page: delete_interaction_not_found.html\n";
  }
  # issue the delete command to the database.
  delete_row($dbh, "sales_interaction_tbl", $interaction_id);
  # commit the transaction.
  $dbh->commit();
  # load the prospective customer.
  my $prospective_customer_id = $interaction->{"prospective_customer_id"};
  my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  # populate information needed for display of page.
  my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"} || "");
  my $suffix = HTML::Entities::encode($prospective_customer->{"suffix"} || "");
  my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
  my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
  my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
  my $amount_of_space = HTML::Entities::encode($prospective_customer->{"amount_of_space"} || "");
  my $rental_start_date = HTML::Entities::encode($prospective_customer->{"rental_start_date"} || "");
  my $duration_of_rental = HTML::Entities::encode($prospective_customer->{"duration_of_rental"} || "");
  my $desired_location = HTML::Entities::encode($prospective_customer->{"desired_location"} || "");
  my $interaction_dt = HTML::Entities::encode($interaction->{"interaction_dt"});
  my $interaction_type = HTML::Entities::encode($interaction->{"interaction_type"});
  my $salesperson = HTML::Entities::encode($interaction->{"salesperson"});
  my $notes = HTML::Entities::encode($interaction->{"notes"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <interaction>
    <interaction_dt>$interaction_dt</interaction_dt>
    <interaction_type>$interaction_type</interaction_type>
    <salesperson>$salesperson</salesperson>
    <notes>$notes</notes>
  </interaction>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <amount_of_space>$amount_of_space</amount_of_space>
    <rental_start_date>$rental_start_date</rental_start_date>
    <duration_of_rental>$duration_of_rental</duration_of_rental>
    <desired_location>$desired_location</desired_location>
  </prospective-customer>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_interaction_result_success.xslt", $cgi);
}

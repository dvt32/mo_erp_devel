<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Delete Interaction
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	  |
	  <a>
	    <xsl:attribute name="href">/1g/sales/show_prospect_detail.cgi?id=<xsl:value-of select="/page/prospective-customer/id"/></xsl:attribute>
	    <xsl:text>Prospect detail</xsl:text>
	  </a>
	</p>
	<xsl:apply-templates select="interaction"/>
	<xsl:apply-templates select="prospective-customer"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="interaction">
    <p>
      You successfully deleted the following interaction:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Interaction Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="substring(interaction_dt, 1, 16)"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Interaction Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="interaction_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Salesperson:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salesperson"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="prospective-customer">
    <p>
      The interaction above was linked to the following prospective customer:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="first_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="middle_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="last_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="suffix"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Company:
	</td>
	<td class="map_table_value">
	  <xsl:if test="title != '' or company != ''">
	    <xsl:if test="title != ''">
	      <xsl:value-of select="title"/>,
	    </xsl:if>
	    <xsl:value-of select="company"/>
	  </xsl:if>
	  <xsl:if test="business_description != ''">
	    <xsl:text> // </xsl:text>
	    <xsl:value-of select="business_description"/>
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Space Needs:
	</td>
	<td class="map_table_value">
	  <xsl:if test="amount_of_space != ''">
	    size: <xsl:value-of select="amount_of_space"/>
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	  <xsl:if test="rental_start_date != ''">
	    start: <xsl:value-of select="rental_start_date"/>
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	  <xsl:if test="duration_of_rental != ''">
	    duration: <xsl:value-of select="duration_of_rental"/>
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	  <xsl:if test="desired_location != ''">
	    location: <xsl:value-of select="desired_location"/>
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	</td>
      </tr>
    </table>
    <p>
      (The prospective customer was not deleted; only the interaction was deleted).
    </p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

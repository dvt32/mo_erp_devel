#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use generate_vcard;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get id from param list
  my $prospective_customer_id = $cgi->param("id");
  if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/)) {
    die "show_page: download_vcard_not_found.html\n";
  }
  # fetch prospective customer.
  my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  # verify that prospective customer exists.
  if (!$prospective_customer) {
    die "show_page: download_vcard_not_found.html\n";
  }
  # generate output
  my $first_name = ucfirst($prospective_customer->{"first_name"});
  my $middle_name = ucfirst($prospective_customer->{"middle_name"});
  my $last_name = ucfirst($prospective_customer->{"last_name"});
  my $vcard_filename = $first_name . " " . $last_name . ".vcf";
  my $vcard_state = $prospective_customer->{"state"};
  my $vcard_content = generate_vcard
    ("salutation" => $prospective_customer->{"salutation"},
     "first_name" => $first_name,
     "middle_name" => $middle_name,
     "last_name" => $last_name,
     "suffix" => $prospective_customer->{"suffix"},
     "title" => $prospective_customer->{"title"},
     "organization" => $prospective_customer->{"company"},
     "tel_work_voice" => $prospective_customer->{"work_phone_number"},
     "tel_home_voice" => $prospective_customer->{"home_phone_number"},
     "tel_cell_voice" => $prospective_customer->{"cell_phone_number"},
     "work_address_1" => $prospective_customer->{"address_1"},
     "work_address_2" => $prospective_customer->{"address_2"},
     "work_city" => $prospective_customer->{"city"},
     "work_state" => $vcard_state,
     "work_zip_code" => $prospective_customer->{"zip_code"},
     "work_country" => $prospective_customer->{"country"},
     "email_address" => $prospective_customer->{"email_address"},);
  # print output
  print $cgi->header
    ("-type" => "text/x-vcard",
     "-content-disposition" => qq{attachment; filename="$vcard_filename"});
  print $vcard_content;
}

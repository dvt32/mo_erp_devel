#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_month_name;
use lead_pad;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if interaction id is specified.
  my $interaction_id = $cgi->param("id");
  if ((!defined($interaction_id)) || ($interaction_id !~ /^\d+$/)) {
    die "show_page: edit_interaction_not_found.html\n";
  }
  # load interaction and check that it exists.
  my $interaction = select_row($dbh, "sales_interaction_tbl", $interaction_id);
  if (!$interaction) {
    die "show_page: edit_interaction_not_found.html\n";
  }
  # load prospective customer.
  my $prospective_customer_id = $interaction->{"prospective_customer_id"};
  my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  if (!$prospective_customer) {
    die "show_page: edit_interaction_prospective_customer_not_found.html\n";
  }
  # populate information needed for display of page.
  my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"} || "");
  my $suffix = HTML::Entities::encode($prospective_customer->{"suffix"} || "");
  my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
  my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
  my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
  my @space_needs_component_list;
  for my $tag_name (qw(amount_of_space rental_start_date duration_of_rental desired_location)) {
    if ($prospective_customer->{$tag_name}) {
      push(@space_needs_component_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
    }
  }
  $interaction->{"interaction_dt"} =~ m/^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/;
  my $year = int($1);
  my $month = int($2);
  my $day = int($3);
  my $hour = $4;
  my $minute = $5;
  my $second = $6;
  my $interaction_type = HTML::Entities::encode($interaction->{"interaction_type"});
  my $salesperson = HTML::Entities::encode($interaction->{"salesperson"});
  my $notes = HTML::Entities::encode($interaction->{"notes"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <space_needs>
---END_BLOCK---;
  for my $space_needs_component (@space_needs_component_list) {
    my $tag_name = $space_needs_component->[0];
    my $tag_value = $space_needs_component->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </space_needs>
  </prospective-customer>
  <interaction>
    <id>$interaction_id</id>
    <month>$month</month>
    <day>$day</day>
    <year>$year</year>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
    <interaction_type>$interaction_type</interaction_type>
    <salesperson>$salesperson</salesperson>
    <notes>$notes</notes>
  </interaction>
  <interaction-type-list>
    <interaction-type>call</interaction-type>
    <interaction-type>email</interaction-type>
    <interaction-type>meeting</interaction-type>
  </interaction-type-list>
  <salesperson-list>
    <salesperson>drotbard</salesperson>
    <salesperson>jcho</salesperson>
    <salesperson>vrodriguez</salesperson>
    <salesperson>wchao</salesperson>
    <salesperson>ysoto</salesperson>
  </salesperson-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <second-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <second>$padded_number</second>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </second-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_interaction_input.xslt", $cgi);
}

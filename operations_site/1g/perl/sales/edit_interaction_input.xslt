<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Interaction
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	  |
	  <a>
	    <xsl:attribute name="href">/1g/sales/show_prospect_detail.cgi?id=<xsl:value-of select="/page/prospective-customer/id"/></xsl:attribute>
	    <xsl:text>Prospect detail</xsl:text>
	  </a>
	</p>
	<xsl:apply-templates select="prospective-customer"/>
	<br />
	<xsl:apply-templates select="interaction"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="prospective-customer">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table">
      <tr>
	<td colspan="2" class="standard_table_title">
          Prospective Customer Detail
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="first_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="middle_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="last_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="suffix"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company:
	</td>
	<td class="map_table_value">
	  <xsl:if test="title != '' or company != ''">
	    <xsl:if test="title != ''">
	      <xsl:value-of select="title"/>,
	    </xsl:if>
	    <xsl:value-of select="company"/>
	  </xsl:if>
	  <xsl:if test="business_description != ''">
	    <xsl:if test="title != '' or company != ''">
	      <xsl:text> // </xsl:text>
	    </xsl:if>
	    <xsl:text>description: </xsl:text>
	    <xsl:value-of select="business_description"/>
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Space Needs:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="space_needs"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="space_needs">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="amount_of_space | rental_start_date | duration_of_rental | desired_location">
    <xsl:choose>
      <xsl:when test="name(.) = 'amount_of_space'">
	<xsl:text>size: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'rental_start_date'">
	<xsl:text>start: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'duration_of_rental'">
	<xsl:text>duration: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'desired_location'">
	<xsl:text>location: </xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:value-of select="."/>
    <xsl:choose>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="interaction">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap;">
      <tr>
	<td class="standard_table_title">
          Edit Interaction
	</td>
      </tr>
      <tr><td>
	<form action="/1g/sales/edit_interaction_result.cgi" method="post">
	  <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  </input>
	  <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	    <tr>
	      <td style="padding: 4px; ">
		Interaction Time:
	      </td>
	      <td>
		<select name="month">
		  <xsl:for-each select="/page/month-list/month">
		    <option>
		      <xsl:if test="/page/interaction/month = number">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		      <xsl:value-of select="name"/>
		    </option>
		  </xsl:for-each>
		</select>
		<select name="day">
		  <xsl:for-each select="/page/day-list/day">
		    <option>
		      <xsl:if test="/page/interaction/day = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">year</xsl:attribute>
		  <xsl:attribute name="size">2</xsl:attribute>
		  <xsl:attribute name="value"><xsl:value-of select="year"/></xsl:attribute>
		</input>
		<xsl:text> at </xsl:text>
		<select name="hour">
		  <xsl:for-each select="/page/hour-list/hour">
		    <option>
		      <xsl:if test="/page/interaction/hour = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<xsl:text>:</xsl:text>
		<select name="minute">
		  <xsl:for-each select="/page/minute-list/minute">
		    <option>
		      <xsl:if test="/page/interaction/minute = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<xsl:text>:</xsl:text>
		<select name="second">
		  <xsl:for-each select="/page/second-list/second">
		    <option>
		      <xsl:if test="/page/interaction/second = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Interaction Type:
	      </td>
	      <td>
		<select name="interaction_type">
		  <xsl:for-each select="/page/interaction-type-list/interaction-type">
		    <option>
		      <xsl:if test="/page/interaction/interaction_type = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Salesperson:
	      </td>
	      <td>
		<select name="salesperson">
		  <xsl:for-each select="/page/salesperson-list/salesperson">
		    <option>
		      <xsl:if test="/page/interaction/salesperson = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Notes:
	      </td>
	      <td>
		<textarea>
		  <xsl:attribute name="name">notes</xsl:attribute>
		  <xsl:attribute name="rows">8</xsl:attribute>
		  <xsl:attribute name="cols">80</xsl:attribute>
                  <!-- Need this blank notes field; otherwise the surrounding textarea gets collapsed into a single tag. -->
		  <xsl:choose>
		    <xsl:when test="notes = ''">.</xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="notes"/>
		    </xsl:otherwise>
		  </xsl:choose>
		</textarea>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
	      </td>
	      <td>
		<input type="submit" name="submitButton" value="Save"/>
	      </td>
	    </tr>
	  </table>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

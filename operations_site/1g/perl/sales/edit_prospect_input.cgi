#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_list;
use timestamp_to_components;
use get_month_name;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if prospect id is specified and try to load prospect.
  my ($prospective_customer_id, $prospective_customer) = ($cgi->param("id"), undef);
  if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/) || (!($prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id)))) {
    die "show_page: edit_prospect_not_found.html\n";
  }
  # load enumeration of possible sales stages.
  my @enum_sales_stage_list;
  get_row_list($dbh, "select * from enum_sales_stage_tbl order by name", undef, \@enum_sales_stage_list);
  # load linked sales tags.
  my @selected_sales_tag_list;
  my $sql_query = "select * from ss_prospective_customer_x_sales_tag_tbl where parent_id = ? order by element_value";
  my @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  # load enumeration of possible sales tags.
  my @enum_sales_tag_list;
  get_row_list($dbh, "select * from enum_sales_tag_tbl order by name", undef, \@enum_sales_tag_list);
  # merge the linked sales tags with the enumeration of possible sales tags.
  for my $sales_tag (@selected_sales_tag_list) {
    if (!grep {$sales_tag->{"element_value"} eq $_->{"name"}} @enum_sales_tag_list) {
      my $row = {"name" => $sales_tag->{"element_value"}};
      push(@enum_sales_tag_list, $row);
    }
  }
  # re-sort the enumeration of possible sales tags.
  @enum_sales_tag_list = sort {$a->{"name"} cmp $b->{"name"}} @enum_sales_tag_list;
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # load floors.
  my @floor_list;
  get_row_list($dbh, "select * from floor_tbl order by short_global_label", undef, \@floor_list);
  # populate information needed for display of page.
  my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"} || "");
  my $suffix = HTML::Entities::encode($prospective_customer->{"suffix"} || "");
  my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
  my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
  my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
  my $work_phone_number = HTML::Entities::encode($prospective_customer->{"work_phone_number"} || "");
  my $home_phone_number = HTML::Entities::encode($prospective_customer->{"home_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($prospective_customer->{"cell_phone_number"} || "");
  my $other_phone_number = HTML::Entities::encode($prospective_customer->{"other_phone_number"} || "");
  my $fax_number = HTML::Entities::encode($prospective_customer->{"fax_number"} || "");
  my $email_address = HTML::Entities::encode($prospective_customer->{"email_address"} || "");
  my $web_site = HTML::Entities::encode($prospective_customer->{"web_site"} || "");
  my $address_1 = HTML::Entities::encode($prospective_customer->{"address_1"} || "");
  my $address_2 = HTML::Entities::encode($prospective_customer->{"address_2"} || "");
  my $city = HTML::Entities::encode($prospective_customer->{"city"} || "");
  my $state = HTML::Entities::encode($prospective_customer->{"state"} || "");
  my $zip_code = HTML::Entities::encode($prospective_customer->{"zip_code"} || "");
  my $country = HTML::Entities::encode($prospective_customer->{"country"} || "");
  my $amount_of_space = HTML::Entities::encode($prospective_customer->{"amount_of_space"} || "");
  my $rental_start_date = HTML::Entities::encode($prospective_customer->{"rental_start_date"} || "");
  my $duration_of_rental = HTML::Entities::encode($prospective_customer->{"duration_of_rental"} || "");
  my $desired_location = HTML::Entities::encode($prospective_customer->{"desired_location"} || "");
  my $notes = HTML::Entities::encode($prospective_customer->{"notes"} || "");
  my $follow_up_date = $prospective_customer->{"follow_up_date"} || "";
  my $reminder_email_address = HTML::Entities::encode($prospective_customer->{"reminder_email_address"} || "");
  my $sales_status = HTML::Entities::encode($prospective_customer->{"sales_status"} || "");
  my $sales_stage = HTML::Entities::encode($prospective_customer->{"sales_stage"} || "");
  my ($initial_contact_year, $initial_contact_month, $initial_contact_day, $initial_contact_hour, $initial_contact_minute, $initial_contact_second) = timestamp_to_components($prospective_customer->{"initial_contact_dt"});
  $initial_contact_hour = lead_pad($initial_contact_hour, 2);
  $initial_contact_minute = lead_pad($initial_contact_minute, 2);
  $initial_contact_second = lead_pad($initial_contact_second, 2);
  my $initial_contact_method = HTML::Entities::encode($prospective_customer->{"initial_contact_method"} || "");
  my $marketing_source = HTML::Entities::encode($prospective_customer->{"marketing_source"} || "");
  my $broker_name = HTML::Entities::encode($prospective_customer->{"broker_name"} || "");
  my $referrer_url = HTML::Entities::encode($prospective_customer->{"referrer_url"} || "");
  my $initial_page_url = HTML::Entities::encode($prospective_customer->{"initial_page_url"} || "");
  my $user_agent = HTML::Entities::encode($prospective_customer->{"user_agent"} || "");
  my $remote_host = HTML::Entities::encode($prospective_customer->{"remote_host"} || "");
  my $remote_addr = HTML::Entities::encode($prospective_customer->{"remote_addr"} || "");
  # generate derived values.
  my $state_other = "";
  if (!grep {$_->{"name"} eq $state} @state_list) {
    $state_other = $state;
    $state = "";
  }
  my $rental_start_month = "";
  my $rental_start_day = "";
  my $rental_start_year = "";
  if (($rental_start_date) && ($rental_start_date =~ /^(\d{4})-(\d\d)-(\d\d)$/)) {
    $rental_start_year = $1;
    $rental_start_month = int($2);
    $rental_start_day = int($3);
  }
  my $follow_up_month = "";
  my $follow_up_day = "";
  my $follow_up_year = "";
  if (($follow_up_date) && ($follow_up_date =~ /^(\d{4})-(\d\d)-(\d\d)$/)) {
    $follow_up_year = $1;
    $follow_up_month = int($2);
    $follow_up_day = int($3);
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <work_phone_number>$work_phone_number</work_phone_number>
    <home_phone_number>$home_phone_number</home_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <other_phone_number>$other_phone_number</other_phone_number>
    <fax_number>$fax_number</fax_number>
    <email_address>$email_address</email_address>
    <web_site>$web_site</web_site>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <state_other>$state_other</state_other>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <amount_of_space>$amount_of_space</amount_of_space>
    <rental_start_month>$rental_start_month</rental_start_month>
    <rental_start_day>$rental_start_day</rental_start_day>
    <rental_start_year>$rental_start_year</rental_start_year>
    <duration_of_rental>$duration_of_rental</duration_of_rental>
    <desired_location>$desired_location</desired_location>
    <notes>$notes</notes>
    <follow_up_month>$follow_up_month</follow_up_month>
    <follow_up_day>$follow_up_day</follow_up_day>
    <follow_up_year>$follow_up_year</follow_up_year>
    <reminder_email_address>$reminder_email_address</reminder_email_address>
    <sales_status>$sales_status</sales_status>
    <sales_stage>$sales_stage</sales_stage>
    <initial_contact_month>$initial_contact_month</initial_contact_month>
    <initial_contact_day>$initial_contact_day</initial_contact_day>
    <initial_contact_year>$initial_contact_year</initial_contact_year>
    <initial_contact_hour>$initial_contact_hour</initial_contact_hour>
    <initial_contact_minute>$initial_contact_minute</initial_contact_minute>
    <initial_contact_second>$initial_contact_second</initial_contact_second>
    <initial_contact_method>$initial_contact_method</initial_contact_method>
    <marketing_source>$marketing_source</marketing_source>
    <broker_name>$broker_name</broker_name>
    <referrer_url>$referrer_url</referrer_url>
    <initial_page_url>$initial_page_url</initial_page_url>
    <user_agent>$user_agent</user_agent>
    <remote_host>$remote_host</remote_host>
    <remote_addr>$remote_addr</remote_addr>
  </prospective-customer>
  <selected-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@selected_sales_tag_list) {
    my $name = HTML::Entities::encode($sales_tag->{"element_value"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$name</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </selected-sales-tag-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <second-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <second>$padded_number</second>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </second-list>
  <enum-sales-stage-list>
---END_BLOCK---;
  for my $sales_stage (@enum_sales_stage_list) {
    my $name = HTML::Entities::encode($sales_stage->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-stage>$name</sales-stage>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-stage-list>
  <enum-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@enum_sales_tag_list) {
    my $name = HTML::Entities::encode($sales_tag->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$name</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-tag-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <floor-list>
---END_BLOCK---;
  for my $floor (@floor_list) {
    my $label = HTML::Entities::encode($floor->{"short_global_label"});
    $generated_xml .= <<"---END_BLOCK---;";
    <floor>$label</floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </floor-list>
  <contact-method-list>
    <contact-method>call</contact-method>
    <contact-method>email</contact-method>
    <contact-method>meeting</contact-method>
    <contact-method>web</contact-method>
  </contact-method-list>
  <sales-status-list>
    <sales-status>live</sales-status>
    <sales-status>customer</sales-status>
    <sales-status>dead</sales-status>
  </sales-status-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_prospect_input.xslt", $cgi);
}

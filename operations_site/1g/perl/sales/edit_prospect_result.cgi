#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use is_valid_email;
use lead_pad;
use update_row;
use select_row;
use insert_row;
use get_row_list;
use delete_rows_with_query;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $salutation = Encode::decode($cgi_input_charset, $cgi->param("salutation"));
  my $first_name = Encode::decode($cgi_input_charset, $cgi->param("first_name"));
  my $middle_name = Encode::decode($cgi_input_charset, $cgi->param("middle_name"));
  my $last_name = Encode::decode($cgi_input_charset, $cgi->param("last_name"));
  my $suffix = Encode::decode($cgi_input_charset, $cgi->param("suffix"));
  my $title = Encode::decode($cgi_input_charset, $cgi->param("title"));
  my $company = Encode::decode($cgi_input_charset, $cgi->param("company"));
  my $business_description = Encode::decode($cgi_input_charset, $cgi->param("business_description"));
  my $work_phone_number = Encode::decode($cgi_input_charset, $cgi->param("work_phone_number"));
  my $home_phone_number = Encode::decode($cgi_input_charset, $cgi->param("home_phone_number"));
  my $cell_phone_number = Encode::decode($cgi_input_charset, $cgi->param("cell_phone_number"));
  my $other_phone_number = Encode::decode($cgi_input_charset, $cgi->param("other_phone_number"));
  my $fax_number = Encode::decode($cgi_input_charset, $cgi->param("fax_number"));
  my $email_address = Encode::decode($cgi_input_charset, $cgi->param("email_address"));
  my $web_site = Encode::decode($cgi_input_charset, $cgi->param("web_site"));
  my $address_1 = Encode::decode($cgi_input_charset, $cgi->param("address_1"));
  my $address_2 = Encode::decode($cgi_input_charset, $cgi->param("address_2"));
  my $city = Encode::decode($cgi_input_charset, $cgi->param("city"));
  my $state = Encode::decode($cgi_input_charset, $cgi->param("state"));
  my $state_other = Encode::decode($cgi_input_charset, $cgi->param("state_other"));
  my $zip_code = Encode::decode($cgi_input_charset, $cgi->param("zip_code"));
  my $country = Encode::decode($cgi_input_charset, $cgi->param("country"));
  my $amount_of_space = Encode::decode($cgi_input_charset, $cgi->param("amount_of_space"));
  my $rental_start_month = Encode::decode($cgi_input_charset, $cgi->param("rental_start_month"));
  my $rental_start_day = Encode::decode($cgi_input_charset, $cgi->param("rental_start_day"));
  my $rental_start_year = Encode::decode($cgi_input_charset, $cgi->param("rental_start_year"));
  my $duration_of_rental = Encode::decode($cgi_input_charset, $cgi->param("duration_of_rental"));
  my $desired_location = Encode::decode($cgi_input_charset, $cgi->param("desired_location"));
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes"));
  my $initial_contact_month = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_month"));
  my $initial_contact_day = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_day"));
  my $initial_contact_year = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_year"));
  my $initial_contact_hour = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_hour"));
  my $initial_contact_minute = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_minute"));
  my $initial_contact_second = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_second"));
  my $initial_contact_method = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_method"));
  my $marketing_source = Encode::decode($cgi_input_charset, $cgi->param("marketing_source"));
  my $broker_name = Encode::decode($cgi_input_charset, $cgi->param("broker_name"));
  my $follow_up_month = Encode::decode($cgi_input_charset, $cgi->param("follow_up_month"));
  my $follow_up_day = Encode::decode($cgi_input_charset, $cgi->param("follow_up_day"));
  my $follow_up_year = Encode::decode($cgi_input_charset, $cgi->param("follow_up_year"));
  my $reminder_email_address = Encode::decode($cgi_input_charset, $cgi->param("reminder_email_address"));
  my $sales_status = Encode::decode($cgi_input_charset, $cgi->param("sales_status"));
  my $sales_stage = Encode::decode($cgi_input_charset, $cgi->param("sales_stage"));
  my @selected_sales_tag_list = map {Encode::decode($cgi_input_charset, $_)} $cgi->param("sales_tag_list");
  @selected_sales_tag_list = sort(@selected_sales_tag_list);
  # verify that prospective customer id was specified and exists.
  my $prospective_customer_id = $cgi->param("id");
  if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/)) {
    die "show_page: edit_prospect_not_found.html\n";
  }
  # verify existence of first and last name, which are required.
  if ((!$first_name) || (!$last_name)) {
    die "show_page: edit_prospect_missing_name.html\n";
  }
  # verify existence of either work phone or email address, one of which is required.
  if ((!$work_phone_number) && (!$email_address)) {
    die "show_page: edit_prospect_missing_phone_and_email.html\n";
  }
  # verify that email address is valid, if specified.
  if (($email_address) && (!is_valid_email($email_address))) {
    die "show_page: edit_prospect_bad_email_address.html\n";
  }
  # verify valid state, if specified.
  if ($state && $state_other) {
    die "show_page: edit_prospect_bad_state.html\n";
  } elsif (!$state) {
    $state = $state_other;
  }
  # verify that rental start date is valid.
  my $rental_start_date = undef;
  if (($rental_start_month) || ($rental_start_day) || ($rental_start_year)) {
    if ((!$rental_start_month) || (!$rental_start_day) || (!$rental_start_year)) {
      die "show_page: edit_prospect_bad_rental_start_date.html\n";
    }
    if (($rental_start_month < 1) || ($rental_start_month > 12)) {
      die "show_page: edit_prospect_bad_rental_start_date.html\n";
    }
    $rental_start_month = lead_pad(int($rental_start_month), 2);
    if (($rental_start_day < 1) || ($rental_start_day > 31)) {
      die "show_page: edit_prospect_bad_rental_start_date.html\n";
    }
    $rental_start_day = lead_pad(int($rental_start_day), 2);
    if (($rental_start_year < 2000) || ($rental_start_year > 9999)) {
      die "show_page: edit_prospect_bad_rental_start_date.html\n";
    }
    $rental_start_year = int($rental_start_year);
    # assemble date information into single variable.
    $rental_start_date = "$rental_start_year-$rental_start_month-$rental_start_day";
  }
  # verify that initial contact timestamp is valid.
  if (($initial_contact_month < 1) || ($initial_contact_month > 12)) {
    die "show_page: edit_prospect_bad_initial_contact_dt.html\n";
  }
  $initial_contact_month = lead_pad(int($initial_contact_month), 2);
  if (($initial_contact_day < 1) || ($initial_contact_day > 31)) {
    die "show_page: edit_prospect_bad_initial_contact_dt.html\n";
  }
  $initial_contact_day = lead_pad(int($initial_contact_day), 2);
  if (($initial_contact_year < 2000) || ($initial_contact_year > 9999)) {
    die "show_page: edit_prospect_bad_initial_contact_dt.html\n";
  }
  $initial_contact_year = int($initial_contact_year);
  if (($initial_contact_hour < 0) || ($initial_contact_hour > 23)) {
    die "show_page: edit_prospect_bad_initial_contact_dt.html\n";
  }
  $initial_contact_hour = lead_pad(int($initial_contact_hour), 2);
  if (($initial_contact_minute < 0) || ($initial_contact_minute > 59)) {
    die "show_page: edit_prospect_bad_initial_contact_dt.html\n";
  }
  $initial_contact_minute = lead_pad(int($initial_contact_minute), 2);
  if (($initial_contact_second < 0) || ($initial_contact_second > 59)) {
    die "show_page: edit_prospect_bad_initial_contact_dt.html\n";
  }
  $initial_contact_second = lead_pad(int($initial_contact_second), 2);
  my $initial_contact_dt = "$initial_contact_year-$initial_contact_month-$initial_contact_day $initial_contact_hour:$initial_contact_minute:$initial_contact_second";
  # verify that follow up date is valid.
  my $follow_up_date = undef;
  if (($follow_up_month) || ($follow_up_day) || ($follow_up_year)) {
    if ((!$follow_up_month) || (!$follow_up_day) || (!$follow_up_year)) {
      die "show_page: edit_prospect_bad_follow_up_date.html\n";
    }
    if (($follow_up_month < 1) || ($follow_up_month > 12)) {
      die "show_page: edit_prospect_bad_follow_up_date.html\n";
    }
    $follow_up_month = lead_pad(int($follow_up_month), 2);
    if (($follow_up_day < 1) || ($follow_up_day > 31)) {
      die "show_page: edit_prospect_bad_follow_up_date.html\n";
    }
    $follow_up_day = lead_pad(int($follow_up_day), 2);
    if (($follow_up_year < 2000) || ($follow_up_year > 9999)) {
      die "show_page: edit_prospect_bad_follow_up_date.html\n";
    }
    $follow_up_year = int($follow_up_year);
    # assemble date information into single variable.
    $follow_up_date = "$follow_up_year-$follow_up_month-$follow_up_day";
  }
  # verify that reminder email address is valid.
  if (($reminder_email_address) && (!is_valid_email($reminder_email_address))) {
    die "show_page: edit_prospect_bad_reminder_email_address.html\n";
  }
  # verify that sales status is valid.
  if (($sales_status) && ($sales_status !~ /^live|customer|dead$/)) {
    die "show_page: edit_prospect_bad_sales_status.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the prospective customer exists.
  my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  if (!$prospective_customer) {
    $dbh->rollback();
    die "show_page: edit_prospect_not_found.html\n";
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["salutation", Encode::encode($db_charset, $salutation)]);
  push(@sql_column_list, ["first_name", Encode::encode($db_charset, $first_name)]);
  push(@sql_column_list, ["middle_name", Encode::encode($db_charset, $middle_name)]);
  push(@sql_column_list, ["last_name", Encode::encode($db_charset, $last_name)]);
  push(@sql_column_list, ["suffix", Encode::encode($db_charset, $suffix)]);
  push(@sql_column_list, ["title", Encode::encode($db_charset, $title)]);
  push(@sql_column_list, ["company", Encode::encode($db_charset, $company)]);
  push(@sql_column_list, ["business_description", Encode::encode($db_charset, $business_description)]);
  push(@sql_column_list, ["work_phone_number", Encode::encode($db_charset, $work_phone_number)]);
  push(@sql_column_list, ["home_phone_number", Encode::encode($db_charset, $home_phone_number)]);
  push(@sql_column_list, ["cell_phone_number", Encode::encode($db_charset, $cell_phone_number)]);
  push(@sql_column_list, ["other_phone_number", Encode::encode($db_charset, $other_phone_number)]);
  push(@sql_column_list, ["fax_number", Encode::encode($db_charset, $fax_number)]);
  push(@sql_column_list, ["email_address", Encode::encode($db_charset, $email_address)]);
  push(@sql_column_list, ["web_site", Encode::encode($db_charset, $web_site)]);
  push(@sql_column_list, ["address_1", Encode::encode($db_charset, $address_1)]);
  push(@sql_column_list, ["address_2", Encode::encode($db_charset, $address_2)]);
  push(@sql_column_list, ["city", Encode::encode($db_charset, $city)]);
  push(@sql_column_list, ["state", Encode::encode($db_charset, $state)]);
  push(@sql_column_list, ["zip_code", Encode::encode($db_charset, $zip_code)]);
  push(@sql_column_list, ["country", Encode::encode($db_charset, $country)]);
  push(@sql_column_list, ["amount_of_space", Encode::encode($db_charset, $amount_of_space)]);
  push(@sql_column_list, ["rental_start_date", Encode::encode($db_charset, $rental_start_date)]);
  push(@sql_column_list, ["duration_of_rental", Encode::encode($db_charset, $duration_of_rental)]);
  push(@sql_column_list, ["desired_location", Encode::encode($db_charset, $desired_location)]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  push(@sql_column_list, ["initial_contact_dt", Encode::encode($db_charset, $initial_contact_dt)]);
  push(@sql_column_list, ["initial_contact_method", Encode::encode($db_charset, $initial_contact_method)]);
  push(@sql_column_list, ["marketing_source", Encode::encode($db_charset, $marketing_source)]);
  push(@sql_column_list, ["broker_name", $broker_name ? Encode::encode($db_charset, $broker_name) : undef]);
  push(@sql_column_list, ["follow_up_date", Encode::encode($db_charset, $follow_up_date)]);
  push(@sql_column_list, ["reminder_email_address", Encode::encode($db_charset, $reminder_email_address)]);
  push(@sql_column_list, ["sales_status", Encode::encode($db_charset, $sales_status)]);
  push(@sql_column_list, ["sales_stage", Encode::encode($db_charset, $sales_stage)]);
  update_row($dbh, "prospective_customer_tbl", \@sql_column_list, $prospective_customer_id);
  # retrieve linked sales tags.
  my @linked_sales_tag_list;
  my $sql_query = "select * from ss_prospective_customer_x_sales_tag_tbl where parent_id = ? order by element_value";
  my @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@linked_sales_tag_list);
  # compare linked sales tags to selected sales tags.
  my $matching_sales_tags = 0;
  if (scalar(@linked_sales_tag_list) == scalar(@selected_sales_tag_list)) {
    $matching_sales_tags = 1;
    for(my $i = 0; $i < scalar(@linked_sales_tag_list); $i++) {
      if ($linked_sales_tag_list[$i]->{"element_value"} ne $selected_sales_tag_list[$i]) {
	$matching_sales_tags = 0;
      }
    }
  }
  # if no exact match, delete existing linked tags and insert new ones.
  if (!$matching_sales_tags) {
    if (scalar(@linked_sales_tag_list) > 0) {
      $sql_query = "delete from ss_prospective_customer_x_sales_tag_tbl where parent_id = ?";
      @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
      delete_rows_with_query($dbh, $sql_query, \@sql_param_list);
    }
    for my $sales_tag (@selected_sales_tag_list) {
      @sql_column_list = ();
      push(@sql_column_list, ["parent_id", $prospective_customer_id, SQL_INTEGER]);
      push(@sql_column_list, ["element_value", $sales_tag]);
      insert_row($dbh, "ss_prospective_customer_x_sales_tag_tbl", \@sql_column_list);
    }
  }
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $salutation = HTML::Entities::encode($salutation || "");
  $first_name = HTML::Entities::encode($first_name);
  $middle_name = HTML::Entities::encode($middle_name || "");
  $last_name = HTML::Entities::encode($last_name);
  $suffix = HTML::Entities::encode($suffix || "");
  $title = HTML::Entities::encode($title || "");
  $company = HTML::Entities::encode($company || "");
  $business_description = HTML::Entities::encode($business_description || "");
  $work_phone_number = HTML::Entities::encode($work_phone_number || "");
  $home_phone_number = HTML::Entities::encode($home_phone_number || "");
  $cell_phone_number = HTML::Entities::encode($cell_phone_number || "");
  $other_phone_number = HTML::Entities::encode($other_phone_number || "");
  $fax_number = HTML::Entities::encode($fax_number || "");
  $email_address = HTML::Entities::encode($email_address || "");
  $web_site = HTML::Entities::encode($web_site || "");
  $address_1 = HTML::Entities::encode($address_1 || "");
  $address_2 = HTML::Entities::encode($address_2 || "");
  $city = HTML::Entities::encode($city || "");
  $state = HTML::Entities::encode($state || "");
  $zip_code = HTML::Entities::encode($zip_code || "");
  $country = HTML::Entities::encode($country || "");
  $amount_of_space = HTML::Entities::encode($amount_of_space || "");
  $rental_start_date = HTML::Entities::encode($rental_start_date || "");
  $duration_of_rental = HTML::Entities::encode($duration_of_rental || "");
  $desired_location = HTML::Entities::encode($desired_location || "");
  $notes = HTML::Entities::encode($notes || "");
  $initial_contact_dt = HTML::Entities::encode($initial_contact_dt);
  $initial_contact_method = HTML::Entities::encode($initial_contact_method);
  $marketing_source = HTML::Entities::encode($marketing_source || "");
  $broker_name = HTML::Entities::encode($broker_name || "");
  $follow_up_date = HTML::Entities::encode($follow_up_date || "");
  $reminder_email_address = HTML::Entities::encode($reminder_email_address || "");
  $sales_status = HTML::Entities::encode($sales_status || "");
  $sales_stage = HTML::Entities::encode($sales_stage || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <work_phone_number>$work_phone_number</work_phone_number>
    <home_phone_number>$home_phone_number</home_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <other_phone_number>$other_phone_number</other_phone_number>
    <fax_number>$fax_number</fax_number>
    <email_address>$email_address</email_address>
    <web_site>$web_site</web_site>
    <address_1>$address_1</address_1>
    <address_2>$address_2</address_2>
    <city>$city</city>
    <state>$state</state>
    <zip_code>$zip_code</zip_code>
    <country>$country</country>
    <amount_of_space>$amount_of_space</amount_of_space>
    <rental_start_date>$rental_start_date</rental_start_date>
    <duration_of_rental>$duration_of_rental</duration_of_rental>
    <desired_location>$desired_location</desired_location>
    <notes>$notes</notes>
    <initial_contact_dt>$initial_contact_dt</initial_contact_dt>
    <initial_contact_method>$initial_contact_method</initial_contact_method>
    <marketing_source>$marketing_source</marketing_source>
    <broker_name>$broker_name</broker_name>
    <follow_up_date>$follow_up_date</follow_up_date>
    <reminder_email_address>$reminder_email_address</reminder_email_address>
    <sales_status>$sales_status</sales_status>
    <sales_stage>$sales_stage</sales_stage>
  </prospective-customer>
  <sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@selected_sales_tag_list) {
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$sales_tag</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </sales-tag-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_prospect_result_success.xslt", $cgi);
}

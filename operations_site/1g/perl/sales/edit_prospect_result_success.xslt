<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Prospective Customer
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Prospective Customer
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	  |
	  <a>
	    <xsl:attribute name="href">/1g/sales/show_prospect_detail.cgi?id=<xsl:value-of select="/page/prospective-customer/id"/></xsl:attribute>
	    <xsl:text>Prospect detail</xsl:text>
	  </a>
	</p>
	<xsl:apply-templates select="prospective-customer"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="prospective-customer">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following prospective customer:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new prospective customer:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Salutation:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  First Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="first_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Middle Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="middle_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Last Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="last_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Suffix:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="suffix"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Title:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="title"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Company:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Business Description:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="business_description"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Work Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="work_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Home Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="home_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Cell Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cell_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Other Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="other_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Fax Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="fax_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Email Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Web Site:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="web_site"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  State/Province/Territory:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Zip Code/Postal Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Amount of Space:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="amount_of_space"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Rental Start Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="rental_start_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Duration of Rental:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="duration_of_rental"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Desired Location:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="desired_location"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key" style="white-space: nowrap">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="notes"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key" style="white-space: nowrap">
	  Initial Contact Timestamp:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="initial_contact_dt"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key" style="white-space: nowrap">
	  Initial Contact Method:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="initial_contact_method"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Marketing Source:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="marketing_source"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Broker:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="broker_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Follow-up Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="follow_up_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Reminder Email Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="reminder_email_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Sales Status:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="sales_status"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Sales Stage:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="sales_stage"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Sales Tags:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="/page/sales-tag-list"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="notes">
    <xsl:call-template name="newline_to_br">
      <xsl:with-param name="text_content" select="./text()"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="newline_to_br">
    <xsl:param name="text_content"/>
    <xsl:choose>
      <xsl:when test="contains($text_content, '&#10;')">
	<xsl:value-of select="substring-before($text_content, '&#10;')"/>
	<br/>
	<xsl:call-template name="newline_to_br">
	  <xsl:with-param name="text_content" select="substring-after($text_content, '&#10;')"/>
	</xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="$text_content"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="sales-tag-list">
    <xsl:apply-templates select="sales-tag"/>
  </xsl:template>

  <xsl:template match="sales-tag">
    <xsl:text>[</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>]</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

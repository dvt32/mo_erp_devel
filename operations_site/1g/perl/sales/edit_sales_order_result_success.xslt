<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Sales Order
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Sales Order
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	  |
	  <a>
	    <xsl:attribute name="href">/1g/sales/show_prospect_detail.cgi?id=<xsl:value-of select="/page/prospective-customer/id"/></xsl:attribute>
	    <xsl:text>Prospect detail</xsl:text>
	  </a>
	  |
	  <a href="/1g/sales/show_sales_order_list.cgi">Sales Orders</a>
	</p>
	<xsl:apply-templates select="sales_order"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sales_order">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following sales order:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new sales order:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  ID:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Licensee Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="licensee_name"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key" style="white-space: nowrap">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

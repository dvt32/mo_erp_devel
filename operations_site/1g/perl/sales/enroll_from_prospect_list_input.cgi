#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load recent prospective customers.
  my @prospective_customer_list;
  get_row_list($dbh, "select * from prospective_customer_tbl order by initial_contact_dt desc limit 50", undef, \@prospective_customer_list);
  # sort recent prospective customers by name.
  my @prospective_customer_by_name_list = sort {
    my $sort_order = 0;
    my $last_name_a = lc($a->{"last_name"});
    my $last_name_b = lc($b->{"last_name"});
    $sort_order = $last_name_a cmp $last_name_b;
    if ($sort_order == 0) {
      my $first_name_a = lc($a->{"first_name"});
      my $first_name_b = lc($b->{"first_name"});
      $sort_order = $first_name_a cmp $first_name_b;
    }
    return $sort_order;
  } @prospective_customer_list;
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer-selection>
    <prospective-customer-by-datetime>
---END_BLOCK---;
  for my $prospective_customer (@prospective_customer_list) {
    my $id = $prospective_customer->{"id"};
    my $name = HTML::Entities::encode($prospective_customer->{"last_name"} . ", " . $prospective_customer->{"first_name"});
    my $company_name = HTML::Entities::encode($prospective_customer->{"company"} || "");
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
    $generated_xml .= <<"---END_BLOCK---;";
      <prospective-customer>
        <id>$id</id>
        <name>$name</name>
        <company-name>$company_name</company-name>
        <initial-contact-dt>$initial_contact_dt</initial-contact-dt>
      </prospective-customer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </prospective-customer-by-datetime>
    <prospective-customer-by-name>
---END_BLOCK---;
  for my $prospective_customer (@prospective_customer_by_name_list) {
    my $id = $prospective_customer->{"id"};
    my $name = HTML::Entities::encode($prospective_customer->{"last_name"} . ", " . $prospective_customer->{"first_name"});
    my $company_name = HTML::Entities::encode($prospective_customer->{"company"} || "");
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
    $generated_xml .= <<"---END_BLOCK---;";
      <prospective-customer>
        <id>$id</id>
        <name>$name</name>
        <company-name>$company_name</company-name>
        <initial-contact-dt>$initial_contact_dt</initial-contact-dt>
      </prospective-customer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </prospective-customer-by-name>
  </prospective-customer-selection>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "enroll_from_prospect_list_input.xslt", $cgi);
}

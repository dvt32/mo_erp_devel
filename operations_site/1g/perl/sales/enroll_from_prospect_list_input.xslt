<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Enroll New Customer from Prospect List
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="prospective-customer-selection"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="prospective-customer-selection">
    <table>
      <tr>
	<td>
	  <xsl:apply-templates select="prospective-customer-by-datetime"/>
	  <xsl:apply-templates select="prospective-customer-by-name"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="prospective-customer-by-datetime">
    <form action="/1g/sales/enroll_from_prospect_list_select.cgi" method="post">
      <fieldset class="standard_fieldset">
	<legend>
	  Sorted by Initial Contact Datetime (50 most recent)
	</legend>
	<table cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <td style="padding: 4px; ">
	      Prospective Customer:
	    </td>
	    <td>
	      <select name="prospective_customer_id">
		<xsl:for-each select="prospective-customer">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		    <xsl:value-of select="initial-contact-dt"/>
		    <xsl:text> </xsl:text>
		    <xsl:value-of select="name"/>
		    <xsl:if test="company-name != ''">
		      <xsl:text> </xsl:text>
		      (<xsl:value-of select="company-name"/>)
		    </xsl:if>
		  </option>
		</xsl:for-each>
	      </select>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	    </td>
	    <td>
	      <input type="submit" name="submitButton" value="Select"/>
	    </td>
	  </tr>
	</table>
      </fieldset>
    </form>
  </xsl:template>

  <xsl:template match="prospective-customer-by-name">
    <form action="/1g/sales/enroll_new_customer_select.cgi" method="post">
      <fieldset class="standard_fieldset">
	<legend>
	  Sorted by Contact Name (50 most recent)
	</legend>
	<table cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <td style="padding: 4px; ">
	      Prospective Customer:
	    </td>
	    <td>
	      <select name="prospective_customer_id">
		<xsl:for-each select="prospective-customer">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		    <xsl:value-of select="name"/>
		    <xsl:if test="company-name != ''">
		      <xsl:text> </xsl:text>
		      (<xsl:value-of select="company-name"/>)
		    </xsl:if>
		    <xsl:text> </xsl:text>
		    // <xsl:value-of select="initial-contact-dt"/>
		  </option>
		</xsl:for-each>
	      </select>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	    </td>
	    <td>
	      <input type="submit" name="submitButton" value="Select"/>
	    </td>
	  </tr>
	</table>
      </fieldset>
    </form>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

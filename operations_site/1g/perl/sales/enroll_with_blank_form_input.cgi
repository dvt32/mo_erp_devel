#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # finished with database access, so disconnect from database.
  $dbh->disconnect();
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <licensor-entity-list>
    <licensor-entity>
      <name>Micro Office Solutions 1 LLC (36 W. 20th St., Fl. 3)</name>
      <value>mos1</value>
    </licensor-entity>
    <licensor-entity>
      <name>Micro Office Solutions 2 LLC (1375 Broadway, Fl. 3)</name>
      <value>mos2</value>
    </licensor-entity>
  </licensor-entity-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "enroll_with_blank_form_input.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Enroll New Customer with Blank Form
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<table>
	  <tr>
	    <td>
	      <xsl:call-template name="blank-enrollment-form"/>
	    </td>
	  </tr>
	</table>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="blank-enrollment-form">
    <form action="/1g/sales/enroll_new_customer_result.cgi" method="post">
      <fieldset class="standard_fieldset">
	<legend>
	  Enroll New Customer with Blank Form
	</legend>
	<table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	  <tr>
	    <td style="padding: 4px; ">
	      <span class="required_field">Licensor Entity:</span>
	    </td>
	    <td>
	      <select name="licensor_entity">
		<xsl:for-each select="/page/licensor-entity-list/licensor-entity">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
		    <xsl:value-of select="name"/>
		  </option>
		</xsl:for-each>
	      </select>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      <span class="required_field">First Name:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">first_name</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      <span class="required_field">Last Name:</span>
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">last_name</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      Company:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">company</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	      Email Address:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">email</xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td style="padding: 4px; ">
	    </td>
	    <td>
	      <input type="submit" name="submitButton" value="Save"/>
	      <xsl:text> </xsl:text>
              <span class="required_field">Required Fields are Red</span>
	    </td>
	  </tr>
	</table>
      </fieldset>
    </form>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use generate_xml_for_prospect_list;
use get_row_by_id_map;
use get_set_by_id_map;
use select_row_with_query;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI params.
  my $stage_number = $cgi->param("stage_number") || "";
  if ((!$stage_number) || ($stage_number !~ /^\d+$/)) {
    die "show_page: filter_by_stage_not_found.html\n";
  }
  my $sales_stage = "";
  # fetch sales stages.
  if (my $sales_stage_row = select_row_with_query($dbh, "select * from enum_sales_stage_tbl where name like ?", [$stage_number . "-%"])) {
    $sales_stage = $sales_stage_row->{"name"};
  } else {
    die "show_page: filter_by_stage_not_found.html\n";
  }
  # fetch prospective customers.
  my (%prospective_customer_by_id, @prospective_customer_id_list);
  my $sql_query = "select * from prospective_customer_tbl where sales_status = ? and sales_stage = ? order by initial_contact_dt desc limit 100";
  my @sql_param_list = ("live", $sales_stage);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%prospective_customer_by_id, \@prospective_customer_id_list);
  # fetch linked interactions.
  my %sales_interaction_by_id;
  $sql_query = "select * from sales_interaction_tbl where prospective_customer_id in (select id from prospective_customer_tbl where sales_status = ? and sales_stage = ? order by initial_contact_dt desc limit 100)";
  @sql_param_list = ("live", $sales_stage);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%sales_interaction_by_id);
  # associate linked interactions with prospective customers.
  my %sales_interaction_id_list_by_prospective_customer_id;
  for my $sales_interaction_id (keys(%sales_interaction_by_id)) {
    my $sales_interaction = $sales_interaction_by_id{$sales_interaction_id};
    my $prospective_customer_id = $sales_interaction->{"prospective_customer_id"};
    push(@{$sales_interaction_id_list_by_prospective_customer_id{$prospective_customer_id}}, $sales_interaction_id);
  }
  # fetch linked sales tags.
  my %sales_tag_list_by_prospective_customer_id;
  $sql_query = "select * from ss_prospective_customer_x_sales_tag_tbl where parent_id in (select id from prospective_customer_tbl where sales_status = ? and sales_stage = ? order by initial_contact_dt desc limit 100)";
  @sql_param_list = ("live", $sales_stage);
  get_set_by_id_map($dbh, $sql_query, \@sql_param_list, \%sales_tag_list_by_prospective_customer_id);
  # generate XML.
  my $generated_xml = generate_xml_for_prospect_list
    (
     "type" => "filter-by-stage",
     "stage_number" => $stage_number,
     "prospective_customer_id_list" => \@prospective_customer_id_list,
     "prospective_customer_by_id" => \%prospective_customer_by_id,
     "sales_interaction_by_id" => \%sales_interaction_by_id,
     "sales_interaction_id_list_by_prospective_customer_id" => \%sales_interaction_id_list_by_prospective_customer_id,
     "sales_tag_list_by_prospective_customer_id" => \%sales_tag_list_by_prospective_customer_id,
     );
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_prospect_list.xslt", $cgi);
}

use strict;
local($^W) = 1;

use HTML::Entities ();

sub generate_xml_for_prospect_list {
  my %param_map = @_;
  my $type = HTML::Entities::encode($param_map{"type"});
  my $stage_number = HTML::Entities::encode($param_map{"stage_number"} || "");
  my $prospective_customer_id_list = $param_map{"prospective_customer_id_list"};
  my $prospective_customer_by_id = $param_map{"prospective_customer_by_id"};
  my $sales_interaction_by_id = $param_map{"sales_interaction_by_id"};
  my $sales_interaction_id_list_by_prospective_customer_id = $param_map{"sales_interaction_id_list_by_prospective_customer_id"};
  my $sales_tag_list_by_prospective_customer_id = $param_map{"sales_tag_list_by_prospective_customer_id"};
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <type>$type</type>
  <stage_number>$stage_number</stage_number>
  <prospective-customer-list>
---END_BLOCK---;
  for my $prospective_customer_id (@$prospective_customer_id_list) {
    my $prospective_customer = $prospective_customer_by_id->{$prospective_customer_id};
    my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
    my $first_name = HTML::Entities::encode(ucfirst($prospective_customer->{"first_name"} || ""));
    my $last_name = HTML::Entities::encode(ucfirst($prospective_customer->{"last_name"} || ""));
    my @contact_point_list;
    for my $tag_name (qw(work_phone_number home_phone_number cell_phone_number other_phone_number fax_number email_address)) {
      if ($prospective_customer->{$tag_name}) {
	push(@contact_point_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
      }
    }
    my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
    my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
    my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
    my @space_needs_component_list;
    for my $tag_name (qw(amount_of_space rental_start_date duration_of_rental desired_location)) {
      if (my $tag_value = $prospective_customer->{$tag_name}) {
	if ($tag_value eq "1375 Broadway, Floor 3") {
	  $tag_value = "1375BwyF3";
	} elsif ($tag_value eq "36 W. 20th St., Floor 3") {
	  $tag_value = "36W20F3";
	}
	push(@space_needs_component_list, [$tag_name, HTML::Entities::encode($tag_value)]);
      }
    }
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
    my $marketing_source = HTML::Entities::encode($prospective_customer->{"marketing_source"} || "");
    my $follow_up_date = $prospective_customer->{"follow_up_date"} || "";
    my $notes = HTML::Entities::encode($prospective_customer->{"notes"} || "");
    my $sales_status = HTML::Entities::encode($prospective_customer->{"sales_status"} || "");
    my $sales_stage = HTML::Entities::encode($prospective_customer->{"sales_stage"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <prospective-customer>
      <id>$prospective_customer_id</id>
      <salutation>$salutation</salutation>
      <first_name>$first_name</first_name>
      <last_name>$last_name</last_name>
      <contact-points>
---END_BLOCK---;
    for my $contact_point (@contact_point_list) {
      my $tag_name = $contact_point->[0];
      my $tag_value = $contact_point->[1];
      $generated_xml .= <<"---END_BLOCK---;";
        <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </contact-points>
      <title>$title</title>
      <company>$company</company>
      <business_description>$business_description</business_description>
      <space_needs>
---END_BLOCK---;
    for my $space_needs_component (@space_needs_component_list) {
      my $tag_name = $space_needs_component->[0];
      my $tag_value = $space_needs_component->[1];
      $generated_xml .= <<"---END_BLOCK---;";
        <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </space_needs>
      <initial_contact_dt>$initial_contact_dt</initial_contact_dt>
      <marketing_source>$marketing_source</marketing_source>
      <follow_up_date>$follow_up_date</follow_up_date>
      <notes>$notes</notes>
      <status-stage-tags>
---END_BLOCK---;
    if ($sales_status) {
      $generated_xml .= <<"---END_BLOCK---;";
        <sales_status>$sales_status</sales_status>
---END_BLOCK---;
    }
    if ($sales_stage) {
      $generated_xml .= <<"---END_BLOCK---;";
        <sales_stage>$sales_stage</sales_stage>
---END_BLOCK---;
    }
    if (my $sales_tag_list = $sales_tag_list_by_prospective_customer_id->{$prospective_customer_id}) {
      $generated_xml .= <<"---END_BLOCK---;";
        <sales-tag-list>
---END_BLOCK---;
      for my $sales_tag (@$sales_tag_list) {
	$generated_xml .= <<"---END_BLOCK---;";
          <sales-tag>$sales_tag</sales-tag>
---END_BLOCK---;
      }
      $generated_xml .= <<"---END_BLOCK---;";
        </sales-tag-list>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </status-stage-tags>
      <sales-interaction-list>
---END_BLOCK---;
    if (my $sales_interaction_id_list = $sales_interaction_id_list_by_prospective_customer_id->{$prospective_customer_id}) {
      for my $sales_interaction_id (@$sales_interaction_id_list) {
	my $sales_interaction = $sales_interaction_by_id->{$sales_interaction_id};
	my $interaction_dt = HTML::Entities::encode($sales_interaction->{"interaction_dt"});
	my $salesperson = HTML::Entities::encode($sales_interaction->{"salesperson"} || "");
	my $interaction_type = HTML::Entities::encode($sales_interaction->{"interaction_type"} || "");
	my $notes = HTML::Entities::encode($sales_interaction->{"notes"} || "");
	$generated_xml .= <<"---END_BLOCK---;";
        <sales-interaction>
          <interaction_dt>$interaction_dt</interaction_dt>
          <salesperson>$salesperson</salesperson>
          <interaction_type>$interaction_type</interaction_type>
          <notes>$notes</notes>
        </sales-interaction>
---END_BLOCK---;
      }
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </sales-interaction-list>
    </prospective-customer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </prospective-customer-list>
</page>
---END_BLOCK---;
  return $generated_xml;
}

1;

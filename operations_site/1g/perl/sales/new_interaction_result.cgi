#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use insert_row;
use select_row;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $prospective_customer_id = $cgi->param("prospective_customer_id");
  my $month = $cgi->param("month") || "";
  my $day = $cgi->param("day") || "";
  my $year = $cgi->param("year") || "";
  my $hour = $cgi->param("hour") || "";
  my $minute = $cgi->param("minute") || "";
  my $second = $cgi->param("second") || "";
  my $interaction_type = $cgi->param("interaction_type") || "";
  my $salesperson = Encode::decode($cgi_input_charset, $cgi->param("salesperson")) || "";
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes")) || "";
  # verify that interaction timestamp is valid.
  if (($month < 1) || ($month > 12)) {
    die "show_page: edit_interaction_bad_month.html\n";
  }
  $month = lead_pad(int($month), 2);
  if (($day < 1) || ($day > 31)) {
    die "show_page: edit_interaction_bad_day.html\n";
  }
  $day = lead_pad(int($day), 2);
  if (($year < 2000) || ($year > 9999)) {
    die "show_page: edit_interaction_bad_year.html\n";
  }
  $year = int($year);
  if (($hour < 0) || ($hour > 23)) {
    die "show_page: edit_interaction_bad_hour.html\n";
  }
  $hour = lead_pad(int($hour), 2);
  if (($minute < 0) || ($minute > 59)) {
    die "show_page: edit_interaction_bad_minute.html\n";
  }
  $minute = lead_pad(int($minute), 2);
  if (($second < 0) || ($second > 59)) {
    die "show_page: edit_interaction_bad_second.html\n";
  }
  $second = lead_pad(int($second), 2);
  my $interaction_dt = "$year-$month-$day $hour:$minute:$second";
  # verify that interaction type is valid.
  if ($interaction_type !~ /^(call|email|meeting)$/) {
    die "show_page: edit_interaction_bad_interaction_type.html\n";
  }
  # begin transaction of inserting the new row.
  $dbh->begin_work();
  # verify that prospective customer record exists.
  my $prospective_customer;
  if ($prospective_customer_id) {
    $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  }
  if (!$prospective_customer) {
    $dbh->rollback();
    die "show_page: edit_interaction_prospective_customer_not_found.html\n";
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["prospective_customer_id", $prospective_customer_id, SQL_INTEGER]);
  push(@sql_column_list, ["interaction_dt", $interaction_dt]);
  push(@sql_column_list, ["interaction_type", $interaction_type]);
  push(@sql_column_list, ["salesperson", Encode::encode($db_charset, $salesperson)]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  insert_row($dbh, "sales_interaction_tbl", \@sql_column_list);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"} || "");
  my $suffix = HTML::Entities::encode($prospective_customer->{"suffix"} || "");
  my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
  my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
  my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
  my @space_needs_component_list;
  for my $tag_name (qw(amount_of_space rental_start_date duration_of_rental desired_location)) {
    if ($prospective_customer->{$tag_name}) {
      push(@space_needs_component_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
    }
  }
  $interaction_type = HTML::Entities::encode($interaction_type);
  $salesperson = HTML::Entities::encode($salesperson);
  $notes = HTML::Entities::encode($notes);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <space_needs>
---END_BLOCK---;
  for my $space_needs_component (@space_needs_component_list) {
    my $tag_name = $space_needs_component->[0];
    my $tag_value = $space_needs_component->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </space_needs>
  </prospective-customer>
  <interaction>
    <interaction_dt>$interaction_dt</interaction_dt>
    <interaction_type>$interaction_type</interaction_type>
    <salesperson>$salesperson</salesperson>
    <notes>$notes</notes>
  </interaction>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_interaction_result_success.xslt", $cgi);
}

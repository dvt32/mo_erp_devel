#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_month_name;
use lead_pad;
use get_row_list;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load enumeration of possible sales stages.
  my @enum_sales_stage_list;
  get_row_list($dbh, "select * from enum_sales_stage_tbl order by name", undef, \@enum_sales_stage_list);
  # load enumeration of possible sales tags.
  my @enum_sales_tag_list;
  get_row_list($dbh, "select * from enum_sales_tag_tbl order by name", undef, \@enum_sales_tag_list);
  # load states.
  my @state_list;
  get_row_list($dbh, "select * from state_tbl order by name", undef, \@state_list);
  # load countries.
  my @country_list;
  get_row_list($dbh, "select * from country_tbl order by name", undef, \@country_list);
  # load floors.
  my @floor_list;
  get_row_list($dbh, "select * from floor_tbl order by short_global_label", undef, \@floor_list);
  # populate information for display of page.
  # determine today's date.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer>
    <initial_contact_month>$month</initial_contact_month>
    <initial_contact_day>$day</initial_contact_day>
    <initial_contact_year>$year</initial_contact_year>
    <initial_contact_hour>$hour</initial_contact_hour>
    <initial_contact_minute>$minute</initial_contact_minute>
    <initial_contact_second>$second</initial_contact_second>
    <notes></notes>
  </prospective-customer>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = HTML::Entities::encode(get_month_name($i));
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <second-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <second>$padded_number</second>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </second-list>
  <enum-sales-stage-list>
---END_BLOCK---;
  for my $sales_stage (@enum_sales_stage_list) {
    my $name = HTML::Entities::encode($sales_stage->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-stage>$name</sales-stage>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-stage-list>
  <enum-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@enum_sales_tag_list) {
    my $name = HTML::Entities::encode($sales_tag->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$name</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-tag-list>
  <state-list>
---END_BLOCK---;
  for my $state (@state_list) {
    my $name = HTML::Entities::encode($state->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>$name</state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country (@country_list) {
    my $name = HTML::Entities::encode($country->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>$name</country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <floor-list>
---END_BLOCK---;
  for my $floor (@floor_list) {
    my $label = HTML::Entities::encode($floor->{"short_global_label"});
    $generated_xml .= <<"---END_BLOCK---;";
    <floor>$label</floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </floor-list>
  <contact-method-list>
    <contact-method>call</contact-method>
    <contact-method>email</contact-method>
    <contact-method>meeting</contact-method>
    <contact-method>web</contact-method>
  </contact-method-list>
  <sales-status-list>
    <sales-status>live</sales-status>
    <sales-status>customer</sales-status>
    <sales-status>dead</sales-status>
  </sales-status-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_prospect_input.xslt", $cgi);
}

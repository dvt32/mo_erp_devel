<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: New Prospective Customer
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	</p>
	<xsl:apply-templates select="prospective-customer"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="prospective-customer">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap;">
      <tr>
	<td class="standard_table_title">
	  New Prospective Customer
	</td>
      </tr>
      <tr><td>
	<form action="/1g/sales/new_prospect_result.cgi" method="post" autocomplete="off">
	  <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	    <tr>
	      <td style="padding: 4px; ">
		<xsl:text> </xsl:text>
	      </td>
	      <td><span class="required_field">Required Fields are Red</span></td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Salutation:
	      </td>
	      <td>
		<select name="salutation">
		  <option value="">[---]</option>
		  <option>Mr.</option>
		  <option>Ms.</option>
		  <option>Mrs.</option>
		  <option>Miss</option>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		<span class="required_field">First Name:</span>
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">first_name</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Middle Name:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">middle_name</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		<span class="required_field">Last Name:</span>
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">last_name</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Suffix:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">suffix</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Title:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">title</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Company:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">company</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Business Description:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">business_description</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		<span class="required_field">Work Phone:</span>
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">work_phone_number</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Home Phone:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">home_phone_number</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Cell Phone:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">cell_phone_number</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Other Phone:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">other_phone_number</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Fax Number:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">fax_number</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		<span class="required_field">Email Address:</span>
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">email_address</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Web Site:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">web_site</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Address 1:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">address_1</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Address 2:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">address_2</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		City:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">city</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		State:
	      </td>
	      <td>
		<select name="state">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/state-list/state">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		or Province/Territory:
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">state_other</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Zip Code/Postal Code:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">zip_code</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Country:
	      </td>
	      <td>
		<select name="country">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/country-list/country">
		    <option>
		      <xsl:if test=". = 'United States'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Amount of Space:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">amount_of_space</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Rental Start Date:
	      </td>
	      <td>
		<select name="rental_start_month">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/month-list/month">
		    <option>
		      <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		      <xsl:value-of select="name"/>
		    </option>
		  </xsl:for-each>
		</select>
		<select name="rental_start_day">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/day-list/day">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">rental_start_year</xsl:attribute>
		  <xsl:attribute name="size">2</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Duration of Rental:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">duration_of_rental</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Desired Floor:
	      </td>
	      <td>
		<select name="desired_location">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/floor-list/floor">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Notes:
	      </td>
	      <td>
		<textarea>
		  <xsl:attribute name="name">notes</xsl:attribute>
		  <xsl:attribute name="rows">8</xsl:attribute>
		  <xsl:attribute name="cols">80</xsl:attribute>
		  <xsl:choose>
		    <xsl:when test="notes = ''">.</xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="notes"/>
		    </xsl:otherwise>
		  </xsl:choose>
		</textarea>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Initial Contact Timestamp:
	      </td>
	      <td>
		<select name="initial_contact_month">
		  <xsl:for-each select="/page/month-list/month">
		    <option>
		      <xsl:if test="/page/prospective-customer/initial_contact_month = number">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		      <xsl:value-of select="name"/>
		    </option>
		  </xsl:for-each>
		</select>
		<select name="initial_contact_day">
		  <xsl:for-each select="/page/day-list/day">
		    <option>
		      <xsl:if test="/page/prospective-customer/initial_contact_day = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">initial_contact_year</xsl:attribute>
		  <xsl:attribute name="size">2</xsl:attribute>
		  <xsl:attribute name="value"><xsl:value-of select="initial_contact_year"/></xsl:attribute>
		</input>
		<xsl:text> at </xsl:text>
		<select name="initial_contact_hour">
		  <xsl:for-each select="/page/hour-list/hour">
		    <option>
		      <xsl:if test="/page/prospective-customer/initial_contact_hour = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<xsl:text>:</xsl:text>
		<select name="initial_contact_minute">
		  <xsl:for-each select="/page/minute-list/minute">
		    <option>
		      <xsl:if test="/page/prospective-customer/initial_contact_minute = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<xsl:text>:</xsl:text>
		<select name="initial_contact_second">
		  <xsl:for-each select="/page/second-list/second">
		    <option>
		      <xsl:if test="/page/prospective-customer/initial_contact_second = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Initial Contact Method:
	      </td>
	      <td>
		<select name="initial_contact_method">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/contact-method-list/contact-method">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Marketing Source:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">marketing_source</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Follow-up Date:
	      </td>
	      <td>
		<select name="follow_up_month">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/month-list/month">
		    <option>
		      <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		      <xsl:value-of select="name"/>
		    </option>
		  </xsl:for-each>
		</select>
		<select name="follow_up_day">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/day-list/day">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">follow_up_year</xsl:attribute>
		  <xsl:attribute name="size">2</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Reminder Email Address:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">reminder_email_address</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Sales Status:
	      </td>
	      <td>
		<select name="sales_status">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/sales-status-list/sales-status">
		    <option>
		      <xsl:if test=". = 'live'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Sales Stage:
	      </td>
	      <td>
		<select name="sales_stage">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/enum-sales-stage-list/sales-stage">
		    <option>
		      <xsl:if test=". = '1-awaiting response from us'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Sales Tags:
	      </td>
	      <td>
		<select name="sales_tag_list" multiple="multiple" size="8">
		  <xsl:for-each select="/page/enum-sales-tag-list/sales-tag">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; text-align: right; ">
		<input type="submit" name="submitButton" value="Save"/>
	      </td>
	      <td><span class="required_field">Required Fields are Red</span></td>
	    </tr>
	  </table>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

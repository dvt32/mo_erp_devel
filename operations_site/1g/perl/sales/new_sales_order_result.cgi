#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use insert_row;
use get_role_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # determine authorization.
  my $role_list = get_role_list($instance_map, $employee->{"id"});
  if (!grep {$_ eq "role:sales/manage_sales_order"} @$role_list) {
    die "show_page: ../auth/not_authorized.html\n";
  }
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $licensee_name = Encode::decode($cgi_input_charset, $cgi->param("licensee_name"));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # verify non-blank licensee name, which is required.
  if (!$licensee_name) {
    die "show_page: edit_sales_order_missing_licensee_name.html\n";
  }
  # begin transaction of inserting the row.
  $dbh->begin_work();
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["licensee_name", Encode::encode($db_charset, $licensee_name)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  my $sales_order_id = insert_row($dbh, "sales_order_tbl", \@sql_column_list, "sales_order_seq");
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $licensee_name = HTML::Entities::encode($licensee_name || "");
  $note = HTML::Entities::encode($note || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <sales_order>
    <id>$sales_order_id</id>
    <licensee_name>$licensee_name</licensee_name>
    <note>$note</note>
  </sales_order>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_sales_order_result_success.xslt", $cgi);
}

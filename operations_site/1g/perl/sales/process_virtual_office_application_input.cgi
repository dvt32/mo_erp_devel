#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use POSIX;
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_database_handle;
use lead_pad;
use get_month_name;
use get_state_map;
use days_in_month;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # load virtual office application.
  my ($virtual_office_application_id, $virtual_office_application);
  if ((!($virtual_office_application_id = $cgi->param("id"))) ||
      ($virtual_office_application_id !~ /^\d+$/) ||
      (!($virtual_office_application = select_row($dbh, "virtual_office_application_tbl", $virtual_office_application_id)))) {
    die "show_page: process_virtual_office_application_not_found.html\n";
  }
  if ($virtual_office_application->{"processed"}) {
    die "show_page: process_virtual_office_application_already_processed.html\n";
  }
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active", undef, \%floor_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl where is_active", undef, \%building_by_id);
  # get available mailboxes
  my (%mailbox_by_id, @mailbox_id_list);
  my $sql_query = "select * from mailbox_tbl where account_id is null order by floor_id, mailbox_number";
  get_row_by_id_map($dbh, $sql_query, undef, \%mailbox_by_id, \@mailbox_id_list);
  # get available phone numbers
  my (%phone_number_by_id, @phone_number_id_list);
  $sql_query = "select * from phone_number_tbl where account_id is null order by floor_id, phone_number";
  get_row_by_id_map($dbh_phone, $sql_query, undef, \%phone_number_by_id, \@phone_number_id_list);
  # get states.
  my $state_map = get_state_map();
  # get countries.
  my (%country_by_id, @country_id_list);
  $sql_query = "select * from country_tbl order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%country_by_id, \@country_id_list);
  # sort floor list.
  my @floor_id_sorted_list = sort {
    my ($floor_a, $floor_b) = ($floor_by_id{$a}, $floor_by_id{$b});
    my ($building_id_a, $building_id_b) = ($floor_a->{"building_id"}, $floor_b->{"building_id"});
    my ($building_a, $building_b) = ($building_by_id{$building_id_a}, $building_by_id{$building_id_b});
    my $building_name_a = $building_a->{"address_1"} . $building_a->{"city"} . $building_a->{"state"};
    my $building_name_b = $building_b->{"address_1"} . $building_b->{"city"} . $building_b->{"state"};
    ($building_name_a . $floor_a->{"short_local_label"}) cmp ($building_name_b . $floor_b->{"short_local_label"});
  } keys(%floor_by_id);
  # populate information needed for display of page.
  my $request_datetime = $virtual_office_application->{"request_datetime"};
  my $remote_host = HTML::Entities::encode($virtual_office_application->{"remote_host"} || "");
  my $remote_addr = HTML::Entities::encode($virtual_office_application->{"remote_addr"} || "");
  my $salutation = $virtual_office_application->{"salutation"};
  my $first_name = HTML::Entities::encode($virtual_office_application->{"first_name"});
  my $middle_name = HTML::Entities::encode($virtual_office_application->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($virtual_office_application->{"last_name"});
  my $suffix = HTML::Entities::encode($virtual_office_application->{"suffix"} || "");
  my $company = HTML::Entities::encode($virtual_office_application->{"company"} || "");
  my $title = HTML::Entities::encode($virtual_office_application->{"title"} || "");
  my $business_description = HTML::Entities::encode($virtual_office_application->{"business_description"} || "");
  my $mailing_address_1 = HTML::Entities::encode($virtual_office_application->{"mailing_address_1"});
  my $mailing_address_2 = HTML::Entities::encode($virtual_office_application->{"mailing_address_2"} || "");
  my $mailing_city = HTML::Entities::encode($virtual_office_application->{"mailing_city"});
  my $mailing_state = HTML::Entities::encode($virtual_office_application->{"mailing_state"} || "");
  my $mailing_province = "";
  if (!exists($state_map->{$mailing_state})) {
    $mailing_province = $mailing_state;
    $mailing_state = "";
  }
  my $mailing_zip_code = HTML::Entities::encode($virtual_office_application->{"mailing_zip_code"});
  my $mailing_country = HTML::Entities::encode($virtual_office_application->{"mailing_country"});
  my $work_phone_number = HTML::Entities::encode($virtual_office_application->{"work_phone_number"});
  my $evening_phone_number = HTML::Entities::encode($virtual_office_application->{"evening_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($virtual_office_application->{"cell_phone_number"} || "");
  my $fax_number = HTML::Entities::encode($virtual_office_application->{"fax_number"} || "");
  my $email_address = HTML::Entities::encode(lc($virtual_office_application->{"email_address"}));
  my $web_site = HTML::Entities::encode($virtual_office_application->{"web_site"} || "");
  my $name_on_card = HTML::Entities::encode($virtual_office_application->{"name_on_card"});
  my $company_on_card = HTML::Entities::encode($virtual_office_application->{"company_on_card"} || "");
  my $credit_card_type = HTML::Entities::encode($virtual_office_application->{"credit_card_type"});
  my $credit_card_number = $virtual_office_application->{"credit_card_number"};
  my $expiration_month = $virtual_office_application->{"expiration_month"};
  my $expiration_year = $virtual_office_application->{"expiration_year"};
  my $card_security_code = $virtual_office_application->{"card_security_code"};
  my $billing_address_1 = HTML::Entities::encode($virtual_office_application->{"billing_address_1"});
  my $billing_address_2 = HTML::Entities::encode($virtual_office_application->{"billing_address_2"} || "");
  my $billing_city = HTML::Entities::encode($virtual_office_application->{"billing_city"});
  my $billing_state = HTML::Entities::encode($virtual_office_application->{"billing_state"} || "");
  my $billing_province = "";
  if (!exists($state_map->{$billing_state})) {
    $billing_province = $billing_state;
    $billing_state = "";
  }
  my $billing_zip_code = HTML::Entities::encode($virtual_office_application->{"billing_zip_code"});
  my $billing_country = HTML::Entities::encode($virtual_office_application->{"billing_country"});
  my $service_plan = HTML::Entities::encode($virtual_office_application->{"service_plan"});
  my $desired_address = HTML::Entities::encode($virtual_office_application->{"desired_address"});
  my $marketing_source = HTML::Entities::encode($virtual_office_application->{"marketing_source"});
  my $notes = HTML::Entities::encode($virtual_office_application->{"notes"} || "");
  my $billing_address_number = "";
  if ($billing_address_1 =~ /^\s*(\d+)/) {
    $billing_address_number = $1;
  }
  my $prorate_date = POSIX::strftime("%Y-%m-%d", localtime());
  my $setup_fee = "99.00";
  my $monthly_fee = "";
  if ($service_plan eq "basic") {
    $monthly_fee = "153.44";
  } else {
    $monthly_fee = "99.00";
  }
  my $total_charge = "";
  print $total_charge;
  $total_charge = sprintf("%.2f", $setup_fee + $monthly_fee);
  my $deferred_credit = "";
  my ($month, $day, $year) = (localtime())[4, 3, 5];
  if ($day > 1) {
    $month++;
    $year += 1900;
    my $days_in_month = days_in_month($month, $year);
    $deferred_credit = sprintf("%.2f", int(($monthly_fee * ($day - 1) / $days_in_month) * 100 + 0.5)/100);
  } else {
    $deferred_credit = "0.00";
  }
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <virtual-office-application>
    <id>$virtual_office_application_id</id>
    <request_datetime>$request_datetime</request_datetime>
    <remote_host>$remote_host</remote_host>
    <remote_addr>$remote_addr</remote_addr>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <company>$company</company>
    <title>$title</title>
    <business_description>$business_description</business_description>
    <mailing_address_1>$mailing_address_1</mailing_address_1>
    <mailing_address_2>$mailing_address_2</mailing_address_2>
    <mailing_city>$mailing_city</mailing_city>
    <mailing_state>$mailing_state</mailing_state>
    <mailing_province>$mailing_province</mailing_province>
    <mailing_zip_code>$mailing_zip_code</mailing_zip_code>
    <mailing_country>$mailing_country</mailing_country>
    <work_phone_number>$work_phone_number</work_phone_number>
    <evening_phone_number>$evening_phone_number</evening_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <fax_number>$fax_number</fax_number>
    <email_address>$email_address</email_address>
    <web_site>$web_site</web_site>
    <name_on_card>$name_on_card</name_on_card>
    <company_on_card>$company_on_card</company_on_card>
    <credit_card_type>$credit_card_type</credit_card_type>
    <credit_card_number>$credit_card_number</credit_card_number>
    <expiration_month>$expiration_month</expiration_month>
    <expiration_year>$expiration_year</expiration_year>
    <card_security_code>$card_security_code</card_security_code>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_province>$billing_province</billing_province>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
    <service_plan>$service_plan</service_plan>
    <desired_address>$desired_address</desired_address>
    <marketing_source>$marketing_source</marketing_source>
    <notes>$notes</notes>
    <nickname>$first_name</nickname>
    <billing_address_number>$billing_address_number</billing_address_number>
    <prorate_date>$prorate_date</prorate_date>
    <setup_fee>$setup_fee</setup_fee>
    <monthly_fee>$monthly_fee</monthly_fee>
    <total_charge>$total_charge</total_charge>
    <deferred_credit>$deferred_credit</deferred_credit>
  </virtual-office-application>
  <state-list>
---END_BLOCK---;
  for my $state (sort(keys(%$state_map))) {
    my $name = HTML::Entities::encode($state);
    my $abbreviation = HTML::Entities::encode($state_map->{$state});
    $generated_xml .= <<"---END_BLOCK---;";
    <state>
      <name>$name</name>
      <abbreviation>$abbreviation</abbreviation>
    </state>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </state-list>
  <country-list>
---END_BLOCK---;
  for my $country_id (@country_id_list) {
    my $country = $country_by_id{$country_id};
    my $name = HTML::Entities::encode($country->{"name"});
    my $iso_2 = HTML::Entities::encode($country->{"iso_2"});
    $generated_xml .= <<"---END_BLOCK---;";
    <country>
      <name>$name</name>
      <abbreviation>$iso_2</abbreviation>
    </country>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </country-list>
  <floor-list>
---END_BLOCK---;
  for my $floor_id (@floor_id_sorted_list) {
    my $floor = $floor_by_id{$floor_id};
    my $building_id = $floor->{"building_id"};
    my $building = $building_by_id{$building_id};
    my $name = HTML::Entities::encode($building->{"address_1"} . ", " . $floor->{"label"} . " (" . $building->{"city"} . ", " . $building->{"state"} . ")");
    $generated_xml .= <<"---END_BLOCK---;";
    <floor>
      <id>$floor_id</id>
      <name>$name</name>
    </floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </floor-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $label = "(" . lead_pad($i, 2) . ") " . get_month_name($i);
    my $value = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <label>$label</label>
      <value>$value</value>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <mailbox-list>
---END_BLOCK---;
  for my $mailbox_id (@mailbox_id_list) {
    my $mailbox = $mailbox_by_id{$mailbox_id};
    my $floor = $floor_by_id{$mailbox->{"floor_id"}};
    my $floor_label = $floor->{"short_global_label"};
    my $label = $mailbox->{"mailbox_number"} . " ($floor_label)";
    my $value = $mailbox_id;
    $generated_xml .= <<"---END_BLOCK---;";
    <mailbox>
      <label>$label</label>
      <value>$value</value>
    </mailbox>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </mailbox-list>
  <phone-number-list>
---END_BLOCK---;
  for my $phone_number_id (@phone_number_id_list) {
    my $phone_number = $phone_number_by_id{$phone_number_id};
    my $label = $phone_number->{"phone_number"};
    my $value = $phone_number_id;
    $generated_xml .= <<"---END_BLOCK---;";
    <phone-number>
      <label>$label</label>
      <value>$value</value>
    </phone-number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </phone-number-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "process_virtual_office_application_input.xslt", $cgi);
}

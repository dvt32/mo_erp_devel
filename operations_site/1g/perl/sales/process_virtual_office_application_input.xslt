<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Process Virtual Office Application
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="virtual-office-application"/>
	<p>
	  <a>
	    <xsl:attribute name="href">/1g/sales/cancel_virtual_office_application.cgi?id=<xsl:value-of select="virtual-office-application/id"/></xsl:attribute>
	    <xsl:text>Cancel Virtual Office Application</xsl:text>
	  </a>
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="virtual-office-application">
    <table>
      <tr><td>
	<form action="/1g/sales/process_virtual_office_application_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Process Virtual Office Application
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">deferred_credit</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="deferred_credit"/></xsl:attribute>
	    </input>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">prorate_date</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="prorate_date"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Order Time:
		</td>
		<td>
		  <xsl:value-of select="request_datetime"/>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Name and Description</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Salutation:
		</td>
		<td>
                  <select name="salutation">
		    <option value="">[-----]</option>
		    <option>
		      <xsl:if test="salutation = 'Mr.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Mr.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Ms.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Ms.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Mrs.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Mrs.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Miss'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Miss</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  First Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">first_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="first_name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Middle Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">middle_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="middle_name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Last Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">last_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="last_name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Suffix:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">suffix</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="suffix"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Nickname:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">nickname</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="nickname"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Title:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">title</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="title"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Company:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">company</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="company"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Business Description:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">business_description</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="business_description"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">account_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="company"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  External Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">external_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="company"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Physical or Mailing Address</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Mailing Address 1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">mailing_address_1</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="mailing_address_1"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Mailing Address 2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">mailing_address_2</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="mailing_address_2"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Mailing City:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">mailing_city</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="mailing_city"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Mailing State:
		</td>
		<td>
                  <select name="mailing_state">
		    <option value="">[-----]</option>
		    <xsl:for-each select="/page/state-list/state">
		      <option>
			<xsl:if test="/page/virtual-office-application/mailing_state = name">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  or Province/Territory:
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">mailing_province</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="mailing_province"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Mailing Zip Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">mailing_zip_code</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="mailing_zip_code"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Mailing Country:
		</td>
		<td>
                  <select name="mailing_country">
		    <option value="">[-----]</option>
		    <xsl:for-each select="/page/country-list/country">
		      <option>
			<xsl:if test="/page/virtual-office-application/mailing_country = name">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Phone, Email, and Web</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Work Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">work_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="work_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Home Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">home_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="home_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Cell Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">cell_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="cell_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Fax Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">fax_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="fax_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email Address:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email_address</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="email_address"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Web Site:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">web_site</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="web_site"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Payment Method Details</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Name on Card:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">name_on_card</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="name_on_card"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Company on Card:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">company_on_card</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="company_on_card"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Credit Card Type:
		</td>
		<td>
                  <select name="credit_card_type">
		    <option>
		      <xsl:if test="credit_card_type = 'American Express'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>American Express</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="credit_card_type = 'Discover'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Discover</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="credit_card_type = 'MasterCard'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>MasterCard</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="credit_card_type = 'Visa'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Visa</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Credit Card Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">credit_card_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="credit_card_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Expiration Date:
		</td>
		<td>
                  <select name="expiration_month">
		    <xsl:for-each select="/page/month-list/month">
		      <option>
			<xsl:if test="/page/virtual-office-application/expiration_month = value">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">expiration_year</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="expiration_year"/></xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		  </input>
		  (4 digit year)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Card Security Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">card_security_code</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="card_security_code"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Billing Address</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Address Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_num</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="billing_address_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Address 1:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_1</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="billing_address_1"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Address 2:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_address_2</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="billing_address_2"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing City:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_city</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="billing_city"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing State:
		</td>
		<td>
                  <select name="billing_state">
		    <option value="">[-----]</option>
		    <xsl:for-each select="/page/state-list/state">
		      <option>
			<xsl:if test="/page/virtual-office-application/billing_state = name">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  or Province/Territory:
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_province</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="billing_province"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Zip Code:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">billing_zip_code</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="billing_zip_code"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Billing Country:
		</td>
		<td>
                  <select name="billing_country">
		    <option value="">[-----]</option>
		    <xsl:for-each select="/page/country-list/country">
		      <option>
			<xsl:if test="/page/virtual-office-application/billing_country = name">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Service Details</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Service Plan:
		</td>
		<td>
		  <xsl:value-of select="service_plan"/>
		  <xsl:if test="desired_address != ''">
		    (requested address: <xsl:value-of select="desired_address"/>)
		  </xsl:if>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Building and Floor:
		</td>
		<td>
                  <select name="floor_id">
		    <xsl:for-each select="/page/floor-list/floor">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Assign Mailbox:
		</td>
		<td>
                  <select name="mailbox_id">
		    <option value="">[-----]</option>
		    <xsl:for-each select="/page/mailbox-list/mailbox">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Assign Phone Number:
		</td>
		<td>
                  <select name="phone_number_id">
		    <option value="">[-----]</option>
		    <xsl:for-each select="/page/phone-number-list/phone-number">
		      <option>
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Bizcenter Password:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">login_password</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Total Charge:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">total_charge</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="total_charge"/></xsl:attribute>
		  </input>
		  ($<xsl:value-of select="total_charge"/> assuming $<xsl:value-of select="setup_fee"/> setup fee +
		  $<xsl:value-of select="monthly_fee"/> monthly fee + tax)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Deferred Credit:
		</td>
		<td>
		  $<xsl:value-of select="deferred_credit"/> assuming proration for days before <xsl:value-of select="prorate_date"/>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Client IP Address</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Remote Host:
		</td>
		<td>
		  <xsl:value-of select="remote_host"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Remote IP Address:
		</td>
		<td>
		  <xsl:value-of select="remote_addr"/>
		</td>
	      </tr>
	      <tr>
		<td class="form_section_separator" colspan="2">Miscellaneous</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Marketing Source:
		</td>
		<td>
		  <xsl:value-of select="marketing_source"/>
		</td>
	      </tr>
	      <tr valign="top">
		<td style="padding: 4px; ">
		  Notes from Customer:
		</td>
		<td>
		  <xsl:value-of select="notes"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		  (will charge the customer's card)
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

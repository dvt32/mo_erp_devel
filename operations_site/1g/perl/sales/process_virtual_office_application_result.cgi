#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_database_handle;
use insert_row;
use charge_card;
use update_row;
use update_with_statement;
use select_row;
use select_row_with_query;
use ctime_to_timestamp;
use send_email;
use strip_outside_whitespace;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # load virtual office application.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $virtual_office_application_id = $cgi->param("id");
  if ((!$virtual_office_application_id) || ($virtual_office_application_id !~ /^\d+$/)) {
    die "show_page: process_virtual_office_application_not_found.html\n";
  }
  my $sql_query = "select * from virtual_office_application_tbl where id = ? and not(processed) and not(cancelled)";
  my @sql_param_list = ([$virtual_office_application_id, SQL_INTEGER]);
  my $virtual_office_application = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if (!$virtual_office_application) {
    die "show_page: process_virtual_office_application_not_found.html\n";
  }
  # find the service_id associated with the virtual office service.
  my $service = select_row_with_query($dbh, "select * from service_tbl where name = ?", ["virtual"]);
  my $service_id = $service->{"id"};
  # get the form fields and validate them.
  my $salutation = Encode::decode($cgi_input_charset, $cgi->param("salutation"));
  my $first_name = Encode::decode($cgi_input_charset, $cgi->param("first_name"));
  my $middle_name = Encode::decode($cgi_input_charset, $cgi->param("middle_name"));
  my $last_name = Encode::decode($cgi_input_charset, $cgi->param("last_name"));
  my $suffix = Encode::decode($cgi_input_charset, $cgi->param("suffix"));
  my $nickname = Encode::decode($cgi_input_charset, $cgi->param("nickname"));
  my $title = Encode::decode($cgi_input_charset, $cgi->param("title"));
  my $company = Encode::decode($cgi_input_charset, $cgi->param("company"));
  my $business_description = Encode::decode($cgi_input_charset, $cgi->param("business_description"));
  my $account_name = Encode::decode($cgi_input_charset, $cgi->param("account_name")) ||
    die "show_page: process_virtual_office_application_missing_account_name.html\n";
  my $external_name = Encode::decode($cgi_input_charset, $cgi->param("external_name")) ||
    die "show_page: process_virtual_office_application_missing_external_name.html\n";
  my $mailing_address_1 = Encode::decode($cgi_input_charset, $cgi->param("mailing_address_1"));
  my $mailing_address_2 = Encode::decode($cgi_input_charset, $cgi->param("mailing_address_2"));
  my $mailing_city = Encode::decode($cgi_input_charset, $cgi->param("mailing_city"));
  my $mailing_state = Encode::decode($cgi_input_charset, $cgi->param("mailing_state") || $cgi->param("mailing_province"));
  my $mailing_zip_code = Encode::decode($cgi_input_charset, $cgi->param("mailing_zip_code"));
  my $mailing_country = Encode::decode($cgi_input_charset, $cgi->param("mailing_country"));
  my $work_phone_number = Encode::decode($cgi_input_charset, $cgi->param("day_phone_number"));
  my $home_phone_number = Encode::decode($cgi_input_charset, $cgi->param("evening_phone_number"));
  my $cell_phone_number = Encode::decode($cgi_input_charset, $cgi->param("cell_phone_number"));
  my $fax_number = Encode::decode($cgi_input_charset, $cgi->param("fax_number"));
  my $email_address = Encode::decode($cgi_input_charset, lc(strip_outside_whitespace($cgi->param("email_address"))));
  my $web_site = Encode::decode($cgi_input_charset, $cgi->param("web_site"));
  my $name_on_card = Encode::decode($cgi_input_charset, $cgi->param("name_on_card"));
  my $company_on_card = Encode::decode($cgi_input_charset, $cgi->param("company_on_card"));
  my $credit_card_type = Encode::decode($cgi_input_charset, $cgi->param("credit_card_type"));
  my $credit_card_number = Encode::decode($cgi_input_charset, $cgi->param("credit_card_number"));
  my $expiration_month = Encode::decode($cgi_input_charset, $cgi->param("expiration_month"));
  my $expiration_year = Encode::decode($cgi_input_charset, $cgi->param("expiration_year"));
  my $card_security_code = Encode::decode($cgi_input_charset, $cgi->param("card_security_code"));
  my $billing_address_num = Encode::decode($cgi_input_charset, $cgi->param("billing_address_num"));
  my $billing_address_1 = Encode::decode($cgi_input_charset, $cgi->param("billing_address_1"));
  my $billing_address_2 = Encode::decode($cgi_input_charset, $cgi->param("billing_address_2"));
  my $billing_city = Encode::decode($cgi_input_charset, $cgi->param("billing_city"));
  my $billing_state = Encode::decode($cgi_input_charset, $cgi->param("billing_state") || $cgi->param("billing_province"));
  my $billing_zip_code = Encode::decode($cgi_input_charset, $cgi->param("billing_zip_code"));
  my $billing_country = Encode::decode($cgi_input_charset, $cgi->param("billing_country"));
  my $floor_id = $cgi->param("floor_id") || "";
  my $floor;
  if (($floor_id !~ /^\d+$/) || (!($floor = select_row($dbh, "floor_tbl", $floor_id)))) {
    die "show_page: process_virtual_office_application_bad_floor.html\n";
  }
  my $mailbox_id = $cgi->param("mailbox_id") || "";
  my $mailbox;
  if (($mailbox_id !~ /^\d+$/) || (!($mailbox = select_row($dbh, "mailbox_tbl", $mailbox_id)))) {
    die "show_page: process_virtual_office_application_bad_mailbox.html\n";
  }
  if ($mailbox->{"account_id"}) {
    die "show_page: process_virtual_office_application_mailbox_already_assigned.html\n";
  }
  my $phone_number_id = $cgi->param("phone_number_id") || "";
  my $phone_number;
  if ($phone_number_id) {
    if ($phone_number_id !~ /^\d+$/) {
      die "show_page: process_virtual_office_application_bad_phone_number.html\n";
    }
    $phone_number = select_row($dbh_phone, "phone_number_tbl", $phone_number_id);
    if ($phone_number->{"account_id"}) {
      die "show_page: process_virtual_office_application_phone_number_already_assigned.html\n";
    }
  }
  my $login_password = Encode::decode($cgi_input_charset, strip_outside_whitespace($cgi->param("login_password")));
  my $total_charge = Encode::decode($cgi_input_charset, $cgi->param("total_charge"));
  my $deferred_credit = Encode::decode($cgi_input_charset, $cgi->param("deferred_credit"));
  my $prorate_date = Encode::decode($cgi_input_charset, $cgi->param("prorate_date"));

  # copy credit card info to recur_card_txn_tbl
  my ($month, $day, $year) = (localtime())[4, 3, 5];
  $month++; # 1st one to move 0-11 to 1-12
  $month++; # 2nd time to ensure it moves to the next month
  $year += 1900;
  # check if month is 13 move to Jan of next year
  if ($month > 12) {
      $month = 1;
      $year++;
  }
  $day = 1;
  my $next_date = sprintf("%d-%02d-%02d", $year, $month, $day);
 
  # begin transaction of inserting new rows.
  $dbh->begin_work();
  # check that account with same name has not already been created.
  $sql_query = "select * from cust_acct_tbl where name = ?";
  @sql_param_list = ($account_name);
  if (select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    $dbh->rollback();
    die "show_page: process_virtual_office_application_duplicate_account.html\n";
  }
  # perform credit card charge
  my %order_map =
    (
     "client_ip_address" => $virtual_office_application->{"remote_addr"},
     "total_charge" => $total_charge,
     "card_number" => $credit_card_number,
     "card_expiration_month" => $expiration_month,
     "card_expiration_year" => $expiration_year,
     "card_security_code" => $card_security_code,
     "billing_name" => $name_on_card,
     "billing_company" => $company_on_card,
     "billing_address_num" => $billing_address_num,
     "billing_zip_code" => $billing_zip_code,
     "billing_address_1" => $billing_address_1,
     "billing_address_2" => $billing_address_2,
     "billing_city" => $billing_city,
     "billing_state" => $billing_state,
     "billing_country" => $billing_country,
     "billing_phone_number" => $work_phone_number || $home_phone_number || $cell_phone_number,
     "billing_fax_number" => $fax_number,
     "billing_email_address" => $email_address,
     );
  my %response_map = charge_card($instance_map, %order_map);
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_column_list;
  if ($response_map{"r_approved"} ne "APPROVED") {
    $dbh->rollback();
    # create record of failed credit card transaction.
    @sql_column_list = ();
    push(@sql_column_list, ["card_number", $credit_card_number]);
    push(@sql_column_list, ["card_expiration_month", $expiration_month]);
    push(@sql_column_list, ["card_expiration_year", $expiration_year]);
    push(@sql_column_list, ["card_security_code", $card_security_code]);
    push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $name_on_card)]);
    push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $company_on_card)]);
    push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $billing_address_num)]);
    push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
    push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $billing_address_1)]);
    push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $billing_address_2)]);
    push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $billing_city)]);
    push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $billing_state)]);
    push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $billing_country)]);
    push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $work_phone_number || $home_phone_number || $cell_phone_number)]);
    push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $fax_number)]);
    push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $email_address)]);
    push(@sql_column_list, ["total_charge", $total_charge]);
    push(@sql_column_list, ["processed_dt", ["now()"]]);
    push(@sql_column_list, ["response_avs", $response_map{"r_avs"}]);
    push(@sql_column_list, ["response_order_num", $response_map{"r_ordernum"}]);
    push(@sql_column_list, ["response_error", $response_map{"r_error"}]);
    push(@sql_column_list, ["response_approved", $response_map{"r_approved"}]);
    push(@sql_column_list, ["response_code", $response_map{"r_code"}]);
    push(@sql_column_list, ["response_message", $response_map{"r_message"}]);
    push(@sql_column_list, ["response_txn_dt", ctime_to_timestamp($response_map{"r_time"})]);
    push(@sql_column_list, ["response_reference_num", $response_map{"ref"}]);
    push(@sql_column_list, ["response_tdate", $response_map{"r_tdate"}]);
    insert_row($dbh, "credit_card_txn_tbl", \@sql_column_list);
    if ($response_map{"r_error"} =~ /declined/i) {
      die "show_page: process_virtual_office_application_charge_declined.html\n";
    } elsif ($response_map{"r_error"} =~ /invalid credit card number/i) {
      die "show_page: process_virtual_office_application_bad_card_number.html\n";
    } elsif ($response_map{"r_error"} =~ /credit card is expired/i) {
      die "show_page: process_virtual_office_application_card_expired.html\n";
    } else {
      die "show_page: process_virtual_office_application_unknown_charge_error.html\n";
    }
  }
  # create new account.
  push(@sql_column_list, ["primary_floor_id", $floor_id, SQL_INTEGER]);
  push(@sql_column_list, ["name", Encode::encode($db_charset, $account_name)]);
  push(@sql_column_list, ["external_name", Encode::encode($db_charset, $external_name)]);
  push(@sql_column_list, ["description", Encode::encode($db_charset, $business_description)]);
  push(@sql_column_list, ["web_site", Encode::encode($db_charset, $web_site)]) if ($web_site);
  push(@sql_column_list, ["type", "customer"]);
  push(@sql_column_list, ["visible", 1, SQL_BOOLEAN]);
  push(@sql_column_list, ["service_id", $service_id, SQL_INTEGER]);
  push(@sql_column_list, ["preferred_payment_method", "credit card"]);
  push(@sql_column_list, ["room_minutes", 600, SQL_INTEGER]);
  my $cust_acct_id = insert_row($dbh, "cust_acct_tbl", \@sql_column_list, "account_seq");
  # create new person.
  @sql_column_list = ();
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["salutation", Encode::encode($db_charset, $salutation)]) if ($salutation);
  push(@sql_column_list, ["first_name", Encode::encode($db_charset, $first_name)]);
  push(@sql_column_list, ["middle_name", Encode::encode($db_charset, $middle_name)]) if ($middle_name);
  push(@sql_column_list, ["last_name", Encode::encode($db_charset, $last_name)]);
  push(@sql_column_list, ["suffix", Encode::encode($db_charset, $suffix)]) if ($suffix);
  push(@sql_column_list, ["nickname", Encode::encode($db_charset, $nickname)]) if ($nickname);
  push(@sql_column_list, ["email", Encode::encode($db_charset, $email_address)]) if ($email_address);
  push(@sql_column_list, ["password", Encode::encode($db_charset, $login_password)]) if ($login_password);
  if ($work_phone_number) {
    push(@sql_column_list, ["work_phone_number", Encode::encode($db_charset, $work_phone_number)]);
    push(@sql_column_list, ["directory_1", "work"]);
  }
  if ($home_phone_number) {
    push(@sql_column_list, ["home_phone_number", Encode::encode($db_charset, $home_phone_number)]);
    push(@sql_column_list, ["directory_2", "home"]);
  }
  if ($cell_phone_number) {
    push(@sql_column_list, ["cell_phone_number", Encode::encode($db_charset, $cell_phone_number)]);
    push(@sql_column_list, ["directory_3", "cell"]);
  }
  push(@sql_column_list, ["register_with_building", 0, SQL_BOOLEAN]);
  push(@sql_column_list, ["title", Encode::encode($db_charset, $title)]) if ($title);
  push(@sql_column_list, ["company", Encode::encode($db_charset, $company)]) if ($company);
  push(@sql_column_list, ["work_fax_number", Encode::encode($db_charset, $fax_number)]) if ($fax_number);
  push(@sql_column_list, ["work_address_1", Encode::encode($db_charset, $mailing_address_1)]);
  push(@sql_column_list, ["work_address_2", Encode::encode($db_charset, $mailing_address_2)]) if ($mailing_address_2);
  push(@sql_column_list, ["work_city", Encode::encode($db_charset, $mailing_city)]);
  push(@sql_column_list, ["work_state", Encode::encode($db_charset, $mailing_state)]);
  push(@sql_column_list, ["work_zip_code", Encode::encode($db_charset, $mailing_zip_code)]);
  push(@sql_column_list, ["work_country", Encode::encode($db_charset, $mailing_country)]);
  push(@sql_column_list, ["home_address_1", Encode::encode($db_charset, $billing_address_1)]);
  push(@sql_column_list, ["home_address_2", Encode::encode($db_charset, $billing_address_2)]) if ($billing_address_2);
  push(@sql_column_list, ["home_city", Encode::encode($db_charset, $billing_city)]);
  push(@sql_column_list, ["home_state", Encode::encode($db_charset, $billing_state)]);
  push(@sql_column_list, ["home_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
  push(@sql_column_list, ["home_country", Encode::encode($db_charset, $billing_country)]);
  my $cust_pers_id = insert_row($dbh, "cust_pers_tbl", \@sql_column_list, "person_seq");
  # update primary_contact_cust_pers_id in cust_acct_tbl
  @sql_param_list = ();
  push(@sql_param_list, [$cust_pers_id, SQL_INTEGER]);
  push(@sql_param_list, [$cust_acct_id, SQL_INTEGER]);
  update_with_statement($dbh, "update cust_acct_tbl set primary_contact_cust_pers_id = ? where id = ?", \@sql_param_list);
  # create record of successful credit card transaction.
  @sql_column_list = ();
  push(@sql_column_list, ["cust_acct_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["card_number", $credit_card_number]);
  push(@sql_column_list, ["card_expiration_month", $expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $expiration_year]);
  push(@sql_column_list, ["card_security_code", $card_security_code]);
  push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $name_on_card)]);
  push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $company_on_card)]);
  push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $billing_address_num)]);
  push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
  push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $billing_address_1)]);
  push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $billing_address_2)]);
  push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $billing_city)]);
  push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $billing_state)]);
  push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $billing_country)]);
  push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $work_phone_number || $home_phone_number || $cell_phone_number)]);
  push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $fax_number)]);
  push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $email_address)]);
  push(@sql_column_list, ["total_charge", $total_charge]);
  push(@sql_column_list, ["processed_dt", ["now()"]]);
  push(@sql_column_list, ["response_avs", $response_map{"r_avs"}]);
  push(@sql_column_list, ["response_order_num", $response_map{"r_ordernum"}]);
  push(@sql_column_list, ["response_error", $response_map{"r_error"}]);
  push(@sql_column_list, ["response_approved", $response_map{"r_approved"}]);
  push(@sql_column_list, ["response_code", $response_map{"r_code"}]);
  push(@sql_column_list, ["response_message", $response_map{"r_message"}]);
  push(@sql_column_list, ["response_txn_dt", ctime_to_timestamp($response_map{"r_time"})]);
  push(@sql_column_list, ["response_reference_num", $response_map{"ref"}]);
  push(@sql_column_list, ["response_tdate", $response_map{"r_tdate"}]);
  insert_row($dbh, "credit_card_txn_tbl", \@sql_column_list);
  
  # create record of recurring credit card transaction.
  @sql_column_list = ();
  push(@sql_column_list, ["account_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["employee_id", $employee->{"id"}, SQL_INTEGER]);
  push(@sql_column_list, ["amount", $total_charge]);
  push(@sql_column_list, ["frequency", "monthly"]);
  push(@sql_column_list, ["next_date", $next_date]);
  push(@sql_column_list, ["card_number", $credit_card_number]);
  push(@sql_column_list, ["card_expiration_month", $expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $expiration_year]);
  push(@sql_column_list, ["card_security_code", $card_security_code]);
  push(@sql_column_list, ["card_bill_name", Encode::encode($db_charset, $name_on_card)]);
  push(@sql_column_list, ["card_bill_company", Encode::encode($db_charset, $company_on_card)]);
  push(@sql_column_list, ["card_bill_address_num", Encode::encode($db_charset, $billing_address_num)]);
  push(@sql_column_list, ["card_bill_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
  push(@sql_column_list, ["card_bill_address_1", Encode::encode($db_charset, $billing_address_1)]);
  push(@sql_column_list, ["card_bill_address_2", Encode::encode($db_charset, $billing_address_2)]);
  push(@sql_column_list, ["card_bill_city", Encode::encode($db_charset, $billing_city)]);
  push(@sql_column_list, ["card_bill_state", Encode::encode($db_charset, $billing_state)]);
  push(@sql_column_list, ["card_bill_country", Encode::encode($db_charset, $billing_country)]);
  push(@sql_column_list, ["card_bill_phone_number", Encode::encode($db_charset, $work_phone_number)]);
  push(@sql_column_list, ["card_bill_fax_number", Encode::encode($db_charset, $fax_number)]);
  push(@sql_column_list, ["card_bill_email_address", Encode::encode($db_charset, $email_address)]);
  insert_row($dbh, "recur_card_txn_tbl", \@sql_column_list);
  
  if ($login_password) {
    # send email with new password.
    my $message .= <<"---END_BLOCK---;";
Your password for the Micro Office Solutions online business center (https://bizcenter.microoffice.com) is $login_password.

Use your email address ($email_address) and your password to log in.

We advise you to change the password to something that is both secure (hard to guess for other people) and easy for you to remember.
---END_BLOCK---;
    send_email
      ("from" => "Micro Office Solutions <service\@microoffice.com>",
       "to" => $email_address,
       "subject" => "Your new login for Micro Office online business center.",
       "message" => $message);
  }
  # create new credit card.
  @sql_column_list = ();
  push(@sql_column_list, ["account_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $cust_pers_id, SQL_INTEGER]);
  push(@sql_column_list, ["card_number", $credit_card_number]);
  push(@sql_column_list, ["card_security_code", $card_security_code]) if ($card_security_code);
  push(@sql_column_list, ["billing_name", Encode::encode($db_charset, $name_on_card)]);
  push(@sql_column_list, ["billing_company", Encode::encode($db_charset, $company_on_card)]);
  push(@sql_column_list, ["billing_address_num", Encode::encode($db_charset, $billing_address_num)]);
  push(@sql_column_list, ["billing_zip_code", Encode::encode($db_charset, $billing_zip_code)]);
  push(@sql_column_list, ["billing_address_1", Encode::encode($db_charset, $billing_address_1)]);
  push(@sql_column_list, ["billing_address_2", Encode::encode($db_charset, $billing_address_2)]);
  push(@sql_column_list, ["billing_city", Encode::encode($db_charset, $billing_city)]);
  push(@sql_column_list, ["billing_state", Encode::encode($db_charset, $billing_state)]);
  push(@sql_column_list, ["billing_country", Encode::encode($db_charset, $billing_country)]);
  push(@sql_column_list, ["billing_phone_number", Encode::encode($db_charset, $work_phone_number || $home_phone_number || $cell_phone_number)]);
  push(@sql_column_list, ["billing_fax_number", Encode::encode($db_charset, $fax_number)]);
  push(@sql_column_list, ["billing_email_address", Encode::encode($db_charset, $email_address)]);
  push(@sql_column_list, ["card_expiration_month", $expiration_month]);
  push(@sql_column_list, ["card_expiration_year", $expiration_year]);
  push(@sql_column_list, ["is_primary", 1, SQL_BOOLEAN]);
  insert_row($dbh, "credit_card_tbl", \@sql_column_list);
  # assign mailbox. first check that mailbox is still available.
  $mailbox = select_row($dbh, "mailbox_tbl", $mailbox_id);
  if ($mailbox->{"account_id"}) {
    $dbh->rollback();
    die "show_page: process_virtual_office_application_mailbox_already_assigned.html\n";
  }
  @sql_column_list = (["account_id", $cust_acct_id, SQL_INTEGER]);
  update_row($dbh, "mailbox_tbl", \@sql_column_list, $mailbox_id);
  # assign phone number. check that phone number is still available.
  if ($phone_number_id) {
    my $phone_number = select_row($dbh_phone, "phone_number_tbl", $phone_number_id);
    if ($phone_number->{"account_id"}) {
      $dbh->rollback();
      die "show_page: process_virtual_office_application_phone_number_already_assigned.html\n";
    }
    @sql_column_list = ();
    push(@sql_column_list, ["floor_id", $floor_id, SQL_INTEGER]);
    push(@sql_column_list, ["account_id", $cust_acct_id, SQL_INTEGER]);
    push(@sql_column_list, ["person_id", $cust_pers_id, SQL_INTEGER]);
    update_row($dbh_phone, "phone_number_tbl", \@sql_column_list, $phone_number_id);
  }
  # --- queue QuickBooks operations [start]
  # create customer in QuickBooks.
  # create invoice in QuickBooks with setup fee and prorated first month.
  # receive payment in QuickBooks.
  # create memorized invoice for recurring billing.
  # --- queue QuickBooks operations [end]
  # mark virtual office application as processed.
  @sql_column_list = (["processed", 1, SQL_BOOLEAN]);
  update_row($dbh, "virtual_office_application_tbl", \@sql_column_list, $virtual_office_application_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $request_datetime = $virtual_office_application->{"request_datetime"};
  $salutation = HTML::Entities::encode($salutation);
  $first_name = HTML::Entities::encode($first_name);
  $middle_name = HTML::Entities::encode($middle_name);
  $last_name = HTML::Entities::encode($last_name);
  $suffix = HTML::Entities::encode($suffix);
  $title = HTML::Entities::encode($title);
  $company = HTML::Entities::encode($company);
  $business_description = HTML::Entities::encode($business_description);
  $mailing_address_1 = HTML::Entities::encode($mailing_address_1);
  $mailing_address_2 = HTML::Entities::encode($mailing_address_2);
  $mailing_city = HTML::Entities::encode($mailing_city);
  $mailing_state = HTML::Entities::encode($mailing_state);
  $mailing_zip_code = HTML::Entities::encode($mailing_zip_code);
  $mailing_country = HTML::Entities::encode($mailing_country);
  $work_phone_number = HTML::Entities::encode($work_phone_number);
  $home_phone_number = HTML::Entities::encode($home_phone_number);
  $cell_phone_number = HTML::Entities::encode($cell_phone_number);
  $fax_number = HTML::Entities::encode($fax_number);
  $email_address = HTML::Entities::encode($email_address);
  $web_site = HTML::Entities::encode($web_site);
  $name_on_card = HTML::Entities::encode($name_on_card);
  $company_on_card = HTML::Entities::encode($company_on_card);
  $credit_card_type = HTML::Entities::encode($credit_card_type);
  $billing_address_1 = HTML::Entities::encode($billing_address_1);
  $billing_address_2 = HTML::Entities::encode($billing_address_2);
  $billing_city = HTML::Entities::encode($billing_city);
  $billing_state = HTML::Entities::encode($billing_state);
  $billing_zip_code = HTML::Entities::encode($billing_zip_code);
  $billing_country = HTML::Entities::encode($billing_country);
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  my $mailbox_number = HTML::Entities::encode($mailbox->{"mailbox_number"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <virtual-office-application>
    <id>$virtual_office_application_id</id>
    <request_datetime>$request_datetime</request_datetime>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <mailing_address_1>$mailing_address_1</mailing_address_1>
    <mailing_address_2>$mailing_address_2</mailing_address_2>
    <mailing_city>$mailing_city</mailing_city>
    <mailing_state>$mailing_state</mailing_state>
    <mailing_zip_code>$mailing_zip_code</mailing_zip_code>
    <mailing_country>$mailing_country</mailing_country>
    <work_phone_number>$work_phone_number</work_phone_number>
    <home_phone_number>$home_phone_number</home_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <fax_number>$fax_number</fax_number>
    <email_address>$email_address</email_address>
    <web_site>$web_site</web_site>
    <name_on_card>$name_on_card</name_on_card>
    <company_on_card>$company_on_card</company_on_card>
    <credit_card_type>$credit_card_type</credit_card_type>
    <credit_card_number>$credit_card_number</credit_card_number>
    <expiration_month>$expiration_month</expiration_month>
    <expiration_year>$expiration_year</expiration_year>
    <billing_address_1>$billing_address_1</billing_address_1>
    <billing_address_2>$billing_address_2</billing_address_2>
    <billing_city>$billing_city</billing_city>
    <billing_state>$billing_state</billing_state>
    <billing_zip_code>$billing_zip_code</billing_zip_code>
    <billing_country>$billing_country</billing_country>
  </virtual-office-application>
  <virtual-office-txn>
    <floor_label>$floor_label</floor_label>
    <mailbox_number>$mailbox_number</mailbox_number>
    <phone_number>$phone_number->{"phone_number"}</phone_number>
    <cust_acct_id>$cust_acct_id</cust_acct_id>
    <email_address>$email_address</email_address>
    <login_password>$login_password</login_password>
    <total_charge>$total_charge</total_charge>
    <deferred_credit>$deferred_credit</deferred_credit>
    <prorate_date>$prorate_date</prorate_date>
  </virtual-office-txn>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "process_virtual_office_application_result.xslt", $cgi);
}

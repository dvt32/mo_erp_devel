<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Process Virtual Office Application
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  The following virtual office application was successfully processed:
	</p>
	<p>
	  <xsl:apply-templates select="virtual-office-application"/>
	</p>
	<p>
	  <xsl:apply-templates select="virtual-office-txn"/>
	</p>
	<p>
	  Please create the customer in QuickBooks and enter the initial invoice and memorize the recurring invoice. Then, receive payment for the Total Charge. Also create a credit memo for the Deferred Credit. If the status report above says that "Bizcenter Login Sent" is "Yes", then the password has been emailed to the virtual office customer already. However, you still need to send a welcome email.
	</p>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="virtual-office-application">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Order Time:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="request_datetime"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Salutation:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  First Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="first_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Middle Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="middle_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Last Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="last_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Suffix:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="suffix"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Title:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="title"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Business Description:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="business_description"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Mailing Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mailing_address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Mailing Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mailing_address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Mailing City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mailing_city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Mailing State/Province:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mailing_state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Mailing Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mailing_zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Mailing Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mailing_country"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Work Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="work_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Home Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="home_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Cell Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cell_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Fax Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="fax_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Web Site:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="web_site"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Name on Card:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="name_on_card"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company on Card:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company_on_card"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Credit Card Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="credit_card_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Credit Card Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="credit_card_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Expiration Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="expiration_month"/>/<xsl:value-of select="expiration_year"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Address 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_address_2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing City:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_city"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing State/Province:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_state"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Zip Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_zip_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Billing Country:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="billing_country"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="virtual-office-txn">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Building and Floor:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="floor_label"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Assigned Mailbox:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mailbox_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Assigned Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account ID:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cust_acct_id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Bizcenter Login Sent:
	</td>
	<td class="map_table_value">
	    <xsl:choose>
	      <xsl:when test="login_password = ''">
		No
	      </xsl:when>
	      <xsl:otherwise>
		Yes (to <xsl:value-of select="email_address"/>)
	      </xsl:otherwise>
	    </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Total Charge:
	</td>
	<td class="map_table_value">
	  $<xsl:value-of select="total_charge"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Deferred Credit:
	</td>
	<td class="map_table_value">
	  $<xsl:value-of select="deferred_credit"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Prorate Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="prorate_date"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

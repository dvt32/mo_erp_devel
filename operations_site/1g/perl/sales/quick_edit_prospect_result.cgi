#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use lead_pad;
use get_month_name;
use is_valid_email;
use select_row;
use get_row_list;
use update_row;
use insert_row;
use delete_rows_with_query;
use select_row_with_query;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # get id from param list.
  my $prospective_customer_id = $cgi->param("id");
  if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/)) {
    die "show_page: quick_edit_prospect_not_found.html\n";
  }
  # get form fields.
  my $follow_up_month = $cgi->param("follow_up_month");
  my $follow_up_day = $cgi->param("follow_up_day");
  my $follow_up_year = $cgi->param("follow_up_year");
  my $reminder_email_address = $cgi->param("reminder_email_address");
  my $sales_status = $cgi->param("sales_status");
  my $sales_stage = $cgi->param("sales_stage");
  # from possible new interaction as well
  my $interaction_type = $cgi->param("interaction_type") || "";
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes")) || "";
  my $new_interaction = undef;
  if ($interaction_type || $notes) {
    $new_interaction = 1;
    my $month = $cgi->param("month") || "";
    my $day = $cgi->param("day") || "";
    my $year = $cgi->param("year") || "";
    my $hour = $cgi->param("hour") || "";
    my $minute = $cgi->param("minute") || "";
    my $second = $cgi->param("second") || "";
    my $salesperson = Encode::decode($cgi_input_charset, $cgi->param("salesperson")) || "";
    # verify that interaction timestamp is valid.
    if (($month < 1) || ($month > 12)) {
      die "show_page: edit_interaction_bad_month.html\n";
    }
    $month = lead_pad(int($month), 2);
    if (($day < 1) || ($day > 31)) {
      die "show_page: edit_interaction_bad_day.html\n";
    }
    $day = lead_pad(int($day), 2);
    if ((!$year) || ($year < 2000) || ($year > 9999)) {
      die "show_page: edit_interaction_bad_year.html\n";
    }
    $year = int($year);
    if (($hour < 0) || ($hour > 23)) {
      die "show_page: edit_interaction_bad_hour.html\n";
    }
    $hour = lead_pad(int($hour), 2);
    if (($minute < 0) || ($minute > 59)) {
      die "show_page: edit_interaction_bad_minute.html\n";
    }
    $minute = lead_pad(int($minute), 2);
    if (($second < 0) || ($second > 59)) {
      die "show_page: edit_interaction_bad_second.html\n";
    }
    $second = lead_pad(int($second), 2);
    my $interaction_dt = "$year-$month-$day $hour:$minute:$second";
    # verify that interaction type is valid.
    if ($interaction_type !~ /^(call|email|meeting)$/) {
      die "show_page: edit_interaction_bad_interaction_type.html\n";
    }
    # begin transaction of inserting the new row.
    $dbh->begin_work();
    # verify that prospective customer record exists.
    my $prospective_customer;
    if ($prospective_customer_id) {
      $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
    }
    if (!$prospective_customer) {
      $dbh->rollback();
      die "show_page: edit_interaction_prospective_customer_not_found.html\n";
    }
    my $db_charset = get_database_charset($instance_map, "main");
    my (@sql_column_list);
    push(@sql_column_list, ["prospective_customer_id", $prospective_customer_id, SQL_INTEGER]);
    push(@sql_column_list, ["interaction_dt", $interaction_dt]);
    push(@sql_column_list, ["interaction_type", $interaction_type]);
    push(@sql_column_list, ["salesperson", Encode::encode($db_charset, $salesperson)]);
    push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
    insert_row($dbh, "sales_interaction_tbl", \@sql_column_list);
    # commit the transaction.
    $dbh->commit(); 
  }
  my @selected_sales_tag_list = $cgi->param("sales_tag_list");
  @selected_sales_tag_list = sort(@selected_sales_tag_list);
  # verify that follow up date is valid.
  my $follow_up_date = undef;
  if (($follow_up_month) || ($follow_up_day)) {
    if ((!$follow_up_month) || (!$follow_up_day) || (!$follow_up_year)) {
      die "show_page: quick_edit_prospect_bad_follow_up_date.html\n";
    }
    if (($follow_up_month < 1) || ($follow_up_month > 12)) {
      die "show_page: quick_edit_prospect_bad_follow_up_date.html\n";
    }
    $follow_up_month = lead_pad(int($follow_up_month), 2);
    if (($follow_up_day < 1) || ($follow_up_day > 31)) {
      die "show_page: quick_edit_prospect_bad_follow_up_date.html\n";
    }
    $follow_up_day = lead_pad(int($follow_up_day), 2);
    if (($follow_up_year < 2000) || ($follow_up_year > 9999)) {
      die "show_page: quick_edit_prospect_bad_follow_up_date.html\n";
    }
    $follow_up_year = int($follow_up_year);
    # assemble date information into single variable.
    $follow_up_date = "$follow_up_year-$follow_up_month-$follow_up_day";
  }
  # verify that reminder email address is valid.
  if (($reminder_email_address) && (!is_valid_email($reminder_email_address))) {
    die "show_page: quick_edit_prospect_bad_reminder_email_address.html\n";
  }
  # verify that sales status is valid.
  if (($sales_status) && ($sales_status !~ /^live|customer|dead$/)) {
    die "show_page: quick_edit_prospect_bad_sales_status.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the prospective customer exists.
  my $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  if (!$prospective_customer) {
    $dbh->rollback();
    die "show_page: quick_edit_prospect_not_found.html\n";
  }
  # issue the update command to the database.
  my (@sql_column_list);
  push(@sql_column_list, ["follow_up_date", $follow_up_date]);
  push(@sql_column_list, ["reminder_email_address", $reminder_email_address]);
  push(@sql_column_list, ["sales_status", $sales_status]);
  push(@sql_column_list, ["sales_stage", $sales_stage]);
  update_row($dbh, "prospective_customer_tbl", \@sql_column_list, $prospective_customer_id);
  # retrieve linked sales tags.
  my @linked_sales_tag_list;
  my $sql_query = "select * from ss_prospective_customer_x_sales_tag_tbl where parent_id = ? order by element_value";
  my @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@linked_sales_tag_list);
  # compare linked sales tags to selected sales tags.
  my $matching_sales_tags = 0;
  if (scalar(@linked_sales_tag_list) == scalar(@selected_sales_tag_list)) {
    $matching_sales_tags = 1;
    for(my $i = 0; $i < scalar(@linked_sales_tag_list); $i++) {
      if ($linked_sales_tag_list[$i]->{"element_value"} ne $selected_sales_tag_list[$i]) {
	$matching_sales_tags = 0;
      }
    }
  }
  # if no exact match, delete existing linked tags and insert new ones.
  if (!$matching_sales_tags) {
    if (scalar(@linked_sales_tag_list) > 0) {
      $sql_query = "delete from ss_prospective_customer_x_sales_tag_tbl where parent_id = ?";
      @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
      delete_rows_with_query($dbh, $sql_query, \@sql_param_list);
    }
    for my $sales_tag (@selected_sales_tag_list) {
      @sql_column_list = ();
      push(@sql_column_list, ["parent_id", $prospective_customer_id, SQL_INTEGER]);
      push(@sql_column_list, ["element_value", $sales_tag]);
      insert_row($dbh, "ss_prospective_customer_x_sales_tag_tbl", \@sql_column_list);
    }
  }
  # commit the transaction.
  $dbh->commit();
  # fetch prospective customer.
  $prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id);
  # verify that prospective customer exists.
  if (!$prospective_customer) {
    die "show_page: quick_edit_prospect_not_found.html\n";
  }
  # fetch linked interactions.
  my @interaction_list;
  $sql_query = "select * from sales_interaction_tbl where prospective_customer_id = ? order by interaction_dt desc limit 10";
  @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@interaction_list);
  # count the number of linked interactions.
  my $interaction_count = 0;
  $sql_query = "select count(*) as num_interactions from sales_interaction_tbl where prospective_customer_id = ?";
  @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  if (my $row = select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    $interaction_count = $row->{"num_interactions"};
  }
  # load enumeration of possible sales stages.
  my @enum_sales_stage_list;
  get_row_list($dbh, "select * from enum_sales_stage_tbl order by name", undef, \@enum_sales_stage_list);
  # fetch linked sales tags.
  @linked_sales_tag_list = ();
  $sql_query = "select * from ss_prospective_customer_x_sales_tag_tbl where parent_id = ? order by element_value";
  @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@linked_sales_tag_list);
  # load enumeration of possible sales tags.
  my @enum_sales_tag_list;
  get_row_list($dbh, "select * from enum_sales_tag_tbl order by name", undef, \@enum_sales_tag_list);
  # merge the linked sales tags with the enumeration of possible sales tags.
  for my $sales_tag (@linked_sales_tag_list) {
    if (!grep {$sales_tag->{"element_value"} eq $_->{"name"}} @enum_sales_tag_list) {
      my $row = {"name" => $sales_tag->{"element_value"}};
      push(@enum_sales_tag_list, $row);
    }
  }
  # re-sort the enumeration of possible sales tags.
  @enum_sales_tag_list = sort {$a->{"name"} cmp $b->{"name"}} @enum_sales_tag_list;
  # populate data needed for display.
  my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"} || "");
  my $suffix = HTML::Entities::encode($prospective_customer->{"suffix"} || "");
  my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
  my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
  my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
  my @phone_number_list = ();
  if ($prospective_customer->{"work_phone_number"}) {
    push(@phone_number_list, ["work", HTML::Entities::encode($prospective_customer->{"work_phone_number"})]);
  }
  if ($prospective_customer->{"home_phone_number"}) {
    push(@phone_number_list, ["home", HTML::Entities::encode($prospective_customer->{"home_phone_number"})]);
  }
  if ($prospective_customer->{"cell_phone_number"}) {
    push(@phone_number_list, ["cell", HTML::Entities::encode($prospective_customer->{"cell_phone_number"})]);
  }
  if ($prospective_customer->{"other_phone_number"}) {
    push(@phone_number_list, ["other", HTML::Entities::encode($prospective_customer->{"other_phone_number"})]);
  }
  if ($prospective_customer->{"fax_number"}) {
    push(@phone_number_list, ["fax", HTML::Entities::encode($prospective_customer->{"fax_number"})]);
  }
  my $email_address = HTML::Entities::encode($prospective_customer->{"email_address"} || "");
  my $web_site = HTML::Entities::encode($prospective_customer->{"web_site"} || "");
  my @address_component_list;
  for my $tag_name (qw(address_1 address_2 city state zip_code country)) {
    if ($prospective_customer->{$tag_name}) {
      push(@address_component_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
    }
  }
  my @space_needs_component_list;
  for my $tag_name (qw(amount_of_space rental_start_date duration_of_rental desired_location)) {
    if ($prospective_customer->{$tag_name}) {
      push(@space_needs_component_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
    }
  }
  my $notes = HTML::Entities::encode($prospective_customer->{"notes"} || "");
  $follow_up_date = $prospective_customer->{"follow_up_date"} || "";
  $reminder_email_address = HTML::Entities::encode($prospective_customer->{"reminder_email_address"} || "");
  $sales_status = HTML::Entities::encode($prospective_customer->{"sales_status"} || "");
  $sales_stage = HTML::Entities::encode($prospective_customer->{"sales_stage"} || "");
  my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
  my $marketing_source = HTML::Entities::encode($prospective_customer->{"marketing_source"} || "");
  my $referrer_url = HTML::Entities::encode($prospective_customer->{"referrer_url"} || "");
  my $initial_page_url = HTML::Entities::encode($prospective_customer->{"initial_page_url"} || "");
  # generate derived values.
  $follow_up_month = "";
  $follow_up_day = "";
  $follow_up_year = "";
  if (($follow_up_date) && ($follow_up_date =~ /^(\d{4})-(\d\d)-(\d\d)$/)) {
    $follow_up_year = $1;
    $follow_up_month = int($2);
    $follow_up_day = int($3);
  }
  # determine today's date.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  # determine validity of email.
  my $is_valid_email = $prospective_customer->{"email_address"} ? (is_valid_email($prospective_customer->{"email_address"}) ? 1 : 0) : 0;
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <phone_numbers>
---END_BLOCK---;
  for my $phone_number (@phone_number_list) {
    my $phone_number_name = $phone_number->[0];
    my $phone_number_number = $phone_number->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <phone_number>
        <name>$phone_number_name</name>
        <number>$phone_number_number</number>
      </phone_number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </phone_numbers>
    <email_address>$email_address</email_address>
    <is_valid_email>$is_valid_email</is_valid_email>
    <web_site>$web_site</web_site>
    <address>
---END_BLOCK---;
  for my $address_component (@address_component_list) {
    my $tag_name = $address_component->[0];
    my $tag_value = $address_component->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </address>
    <space_needs>
---END_BLOCK---;
  for my $space_needs_component (@space_needs_component_list) {
    my $tag_name = $space_needs_component->[0];
    my $tag_value = $space_needs_component->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </space_needs>
    <notes>$notes</notes>
    <follow_up_date>$follow_up_date</follow_up_date>
    <reminder_email_address>$reminder_email_address</reminder_email_address>
    <sales_status>$sales_status</sales_status>
    <sales_stage>$sales_stage</sales_stage>
    <initial_contact_dt>$initial_contact_dt</initial_contact_dt>
    <marketing_source>$marketing_source</marketing_source>
    <referrer_url>$referrer_url</referrer_url>
    <initial_page_url>$initial_page_url</initial_page_url>
  </prospective-customer>
  <linked-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@linked_sales_tag_list) {
    my $id = $sales_tag->{"id"};
    my $name = $sales_tag->{"sales_tag"};
    $generated_xml .= <<"---END_BLOCK---;";
    <linked-sales-tag>
      <id>$id</id>
      <name>$name</name>
    </linked-sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </linked-sales-tag-list>
  <interaction-list>
    <interaction-count>$interaction_count</interaction-count>
---END_BLOCK---;
  for my $interaction (@interaction_list) {
    my $id = $interaction->{"id"};
    my $interaction_dt = $interaction->{"interaction_dt"};
    my $interaction_type = $interaction->{"interaction_type"} || "";
    my $salesperson = HTML::Entities::encode($interaction->{"salesperson"} || "");
    my $notes = HTML::Entities::encode($interaction->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <interaction>
      <id>$id</id>
      <interaction_dt>$interaction_dt</interaction_dt>
      <interaction_type>$interaction_type</interaction_type>
      <salesperson>$salesperson</salesperson>
      <notes>$notes</notes>
    </interaction>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </interaction-list>
  <quick-edit-form>
    <follow_up_month>$follow_up_month</follow_up_month>
    <follow_up_day>$follow_up_day</follow_up_day>
    <follow_up_year>$follow_up_year</follow_up_year>
  </quick-edit-form>
  <enum-sales-status-list>
    <sales-status>live</sales-status>
    <sales-status>customer</sales-status>
    <sales-status>dead</sales-status>
  </enum-sales-status-list>
  <enum-sales-stage-list>
---END_BLOCK---;
  for my $sales_stage (@enum_sales_stage_list) {
    my $name = HTML::Entities::encode($sales_stage->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-stage>$name</sales-stage>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-stage-list>
  <enum-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@enum_sales_tag_list) {
    my $name = HTML::Entities::encode($sales_tag->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$name</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-tag-list>
  <new-interaction-form>
    <month>$month</month>
    <day>$day</day>
    <year>$year</year>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
    <notes></notes>
  </new-interaction-form>
  <interaction-type-list>
    <interaction-type>call</interaction-type>
    <interaction-type>email</interaction-type>
    <interaction-type>meeting</interaction-type>
  </interaction-type-list>
  <salesperson-list>
    <salesperson>
      <name>David</name>
      <value>drotbard</value>
    </salesperson>
    <salesperson>
      <name>Matthieu</name>
      <value>mrosanvallon</value>
    </salesperson>
    <salesperson>
      <name>Yon</name>
      <value>ylam</value>
    </salesperson>
    <salesperson>
      <name>Zoila</name>
      <value>zprimo</value>
    </salesperson>
    <salesperson>
      <name>Jin</name>
      <value>jcho</value>
    </salesperson>
    <salesperson>
      <name>Wellie</name>
      <value>wchao</value>
    </salesperson>
  </salesperson-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <second-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <second>$padded_number</second>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </second-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_prospect_detail.xslt", $cgi);
}

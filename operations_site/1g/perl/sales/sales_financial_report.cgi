#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use get_row_by_id_map;
use init_login_standard;
use apply_xslt_output_xhtml;
use main_exception_wrapper;
use get_date_now;
use timestamp_to_components;
use local_datetime_to_epoch_time;
use get_row_list;
use currency_format;

main_exception_wrapper(\&main); 

sub main() {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  
  # read floor rows from db.
  my @floors;
  my $sql_query = "select * from floor_tbl order by short_global_label ASC";
  get_row_list($dbh,$sql_query,undef,\@floors);
  
  my $tot_current_revenue = 0;
	my $tot_potential_revenue = 0;
	my $tot_internet_telephone_revenue = 0;
	my $tot_revenue_lost = 0;
  my $tot_new_business1 = 0;
	my $tot_new_business2 = 0;
	my $tot_new_business3 = 0;
	my $tot_new_business4 = 0;
	  
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <floor-list>
---END_BLOCK---;
  
  for my $floor (@floors){
  	my $floor_name = $floor->{"short_global_label"};
  	my $floor_id = $floor->{"id"};
  	my $current_revenue = 0;
	  my $potential_revenue = 0;
	  my $internet_telephone_revenue = 0;
	  my $revenue_lost = 0;
	  my $new_business1 = 0;
	  my $new_business2 = 0;
	  my $new_business3 = 0;
	  my $new_business4 = 0;
	  my $seconds_in_day = 60 * 60 * 24;
	  my ($now_year, $now_month, $now_day) = get_date_now(1);
  	my $now_epoch_time = local_datetime_to_epoch_time($now_year,$now_month,$now_day,0,0,0);
	  
	  # read desk rows from db.
    my (%desk_by_id, @desk_id_list);
    my $sql_query = "select * from desk_tbl where floor_id = ? order by assigned_number";
    my @sql_param_list = ([$floor_id, SQL_INTEGER]);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%desk_by_id, \@desk_id_list);
  
	  for my $desk_id (@desk_id_list) {
	    my $desk = $desk_by_id{$desk_id};
	  	my $current_price = $desk->{"current_price"};
	  	my $list_price = $desk->{"list_price"};
	  	my $is_active = $desk->{"is_active"};
	  	
	  	# calculate current revenue if desk space is occupied and calculate lost revenue if current price < list price or desk space is unoccupied
	  	if ($is_active) {
	  		my $account_id = $desk->{"account_id"};
	  		if ($account_id) {
	  			$current_revenue += ($current_price > 0)?$current_price:$list_price;
	  			#if($current_price < $list_price && $current_price > 0) {
	  				#$revenue_lost += $list_price - $current_price;
	  			#}
	  		} #else {
	  			#$revenue_lost += $list_price;
	  		#}
	  	}
	  	# calculate potential revenue by adding list prices
	    $potential_revenue += $list_price;
	    # calculate internet/telephone revenue by adding internet_telephone_revenue
	    $internet_telephone_revenue += $desk->{"internet_telephone_revenue"};
	    
	    if ($desk->{"start_date"}  =~ m/(\d{4})-(\d\d)-(\d\d)/){
	    	my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($desk->{"start_date"}." 00:00:00");
	      my $start_date_epoch_time = local_datetime_to_epoch_time($year, $month, $day, $hour, $minute, $second);
	      # calculate New business in the past 30 days
	      if(($now_epoch_time-$start_date_epoch_time)<=($seconds_in_day*30)){
	    	  $new_business1 += ($current_price > 0)?$current_price:$list_price;
	      }
	      # calculate New business in the past 60 days
	      if(($now_epoch_time-$start_date_epoch_time)<=($seconds_in_day*60)){
	    	  $new_business2 += ($current_price > 0)?$current_price:$list_price;
	      }
	      # calculate New Business Since the 1st of current month
	      my $first_of_month = ($now_day==1)?$now_day:$now_day-($now_day-1);
	      my $first_of_current_month_epoch_time = local_datetime_to_epoch_time($now_year,$now_month,$first_of_month,0,0,0);
	      if($start_date_epoch_time >= $first_of_current_month_epoch_time){
	    	  $new_business3 += ($current_price > 0)?$current_price:$list_price;
	      }
	      # calculate New Business Since the 1st of last month
	      my $last_month = ($now_month==1)?12:$now_month-1;
	      my $last_month_year = ($now_month==1)?$$now_year-1:$now_year;
	      my $first_of_last_month_epoch_time = local_datetime_to_epoch_time($last_month_year,$last_month,$first_of_month,0,0,0);
	      if($start_date_epoch_time >= $first_of_last_month_epoch_time){
	    	  $new_business4 += ($current_price > 0)?$current_price:$list_price;
	      }
	    }
	  }
	  
	  # accumulate totals
	  $tot_current_revenue += $current_revenue;
	  $tot_potential_revenue += $potential_revenue;
	  $tot_internet_telephone_revenue += $internet_telephone_revenue;
	  $tot_new_business1 += $new_business1;
	  $tot_new_business2 += $new_business2;
	  $tot_new_business3 += $new_business3;
	  $tot_new_business4 += $new_business4;
	  
	  # format currency
	  $revenue_lost = ($potential_revenue>$current_revenue)?$potential_revenue-$current_revenue:0;
	  $current_revenue = currency_format($current_revenue);
	  $potential_revenue = currency_format($potential_revenue);
	  $revenue_lost = currency_format($revenue_lost);
	  $internet_telephone_revenue = currency_format($internet_telephone_revenue);
	  $new_business1 = currency_format($new_business1);
	  $new_business2 = currency_format($new_business2);
	  $new_business3 = currency_format($new_business3);
	  $new_business4 = currency_format($new_business4);
	  
	  $generated_xml .= <<"---END_BLOCK---;";
	  <floor>
	    <floor-name>$floor_name</floor-name>
      <current-revenue>$current_revenue</current-revenue>
      <potential-revenue>$potential_revenue</potential-revenue>
      <revenue-lost>$revenue_lost</revenue-lost>
      <internet-telephone-revenue>$internet_telephone_revenue</internet-telephone-revenue>
      <new-business1>$new_business1</new-business1>
      <new-business2>$new_business2</new-business2>
      <new-business3>$new_business3</new-business3>
      <new-business4>$new_business4</new-business4>
    </floor>
---END_BLOCK---;

  }
  
  # format currency
  $tot_revenue_lost = ($tot_potential_revenue>$tot_current_revenue)?$tot_potential_revenue-$tot_current_revenue:0;
  $tot_current_revenue = currency_format($tot_current_revenue);
	$tot_potential_revenue = currency_format($tot_potential_revenue);
	$tot_revenue_lost = currency_format($tot_revenue_lost);
	$tot_internet_telephone_revenue = currency_format($tot_internet_telephone_revenue);
	$tot_new_business1 = currency_format($tot_new_business1);
	$tot_new_business2 = currency_format($tot_new_business2);
	$tot_new_business3 = currency_format($tot_new_business3);
	$tot_new_business4 = currency_format($tot_new_business4);
	  
  $generated_xml .= <<"---END_BLOCK---;";
  </floor-list>
  <tot-current-revenue>$tot_current_revenue</tot-current-revenue>
	<tot-potential-revenue>$tot_potential_revenue</tot-potential-revenue>
	<tot-revenue-lost>$tot_revenue_lost</tot-revenue-lost>
	<tot-internet-telephone-revenue>$tot_internet_telephone_revenue</tot-internet-telephone-revenue>
	<tot-new-business1>$tot_new_business1</tot-new-business1>
	<tot-new-business2>$tot_new_business2</tot-new-business2>
	<tot-new-business3>$tot_new_business3</tot-new-business3>
	<tot-new-business4>$tot_new_business4</tot-new-business4>
</page>
---END_BLOCK---;

  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "sales_financial_report.xslt", $cgi);
}

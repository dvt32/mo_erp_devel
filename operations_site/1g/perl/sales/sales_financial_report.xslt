<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:template match="page">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<title>Micro Office Solutions :: Operations Center :: Sales Financials Report</title>
			<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
		</head>
		<body>
			<p>
				<a href="/1g/index.cgi">Main Menu</a>
			</p>
			<xsl:apply-templates select="floor-list"/>
			<p>
				<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
			</p>
		</body>
	</html>
</xsl:template>

<xsl:template match="floor-list">
	<table cellspacing="0" cellpadding="0" border="0" class="standard_table">
		<tr>
			<td colspan="19" class="standard_table_title">Sales Financials</td>
		</tr>
		<tr class="standard_table_column_heading_row">
			<td class="standard_table_side_padding"></td>
			<td>Floor</td>
			<td class="standard_table_spacer_column"></td>
			<td>Total Current Revenue</td>
			<td class="standard_table_spacer_column"></td>
			<td>Potential Revenue</td>
			<td class="standard_table_side_padding"></td>
			<td>Lost Revenue</td>
			<td class="standard_table_side_padding"></td>
			<td>Internet/Telephone Revenue</td>
			<td class="standard_table_side_padding"></td>
			<td>New Business-Past 30 Days</td>
			<td class="standard_table_side_padding"></td>
			<td>New Business-Past 60 Days</td>
			<td class="standard_table_side_padding"></td>
			<td>New Business since the 1st of this Month</td>
			<td class="standard_table_side_padding"></td>
			<td>New Business since the 1st of last Month</td>
			<td class="standard_table_side_padding"></td>
		</tr>
		<xsl:apply-templates select="floor"/>
		<tr>
			<td colspan="19">&nbsp;</td>
		</tr>
		<tr>
			<td class="standard_table_side_padding"></td>
		  <td align="right"><b>Totals:</b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-current-revenue"/></b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-potential-revenue"/></b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-revenue-lost"/></b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-internet-telephone-revenue"/></b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-new-business1"/></b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-new-business2"/></b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-new-business3"/></b></td>
		  <td class="standard_table_side_padding"></td>
		  <td align="right"><b><xsl:value-of select="../tot-new-business4"/></b></td>
		  <td class="standard_table_side_padding"></td>
		</tr>
	</table>
</xsl:template>

<xsl:template match="floor">
	<tr valign="top">
		<xsl:choose>
			<xsl:when test="position() mod 2 = 0">
				<xsl:attribute name="class">standard_table_row_even</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<td class="standard_table_side_padding"></td>
		<td align="right">
			<xsl:value-of select="floor-name"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="current-revenue"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="potential-revenue"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="revenue-lost"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
		  <xsl:value-of select="internet-telephone-revenue"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="new-business1"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="new-business2"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="new-business3"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="new-business4"/>
		</td>
		<td class="standard_table_spacer_column"></td>
	</tr>
</xsl:template>

<xsl:template match="@*|node()" priority="-1">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()"/>
	</xsl:copy>
</xsl:template>
</xsl:stylesheet>

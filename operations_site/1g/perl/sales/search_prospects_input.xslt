<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Search Prospects
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	  |
	  <a href="/1g/sales/show_follow_ups.cgi">Follow-ups</a>
	  |
	  <a href="/1g/sales/show_recent_interactions.cgi">Recent interactions</a>
	  |
	  Stage
	  <a href="/1g/sales/filter_by_stage.cgi?stage_number=1">1</a>
	  <xsl:text> </xsl:text>
	  <a href="/1g/sales/filter_by_stage.cgi?stage_number=2">2</a>
	  <xsl:text> </xsl:text>
	  <a href="/1g/sales/filter_by_stage.cgi?stage_number=3">3</a>
	  <xsl:text> </xsl:text>
	  <a href="/1g/sales/filter_by_stage.cgi?stage_number=4">4</a>
	  <xsl:text> </xsl:text>
	  <a href="/1g/sales/filter_by_stage.cgi?stage_number=5">5</a>
	  <xsl:text> </xsl:text>
	  <a href="/1g/sales/filter_by_stage.cgi?stage_number=6">6</a>
	  <xsl:text> </xsl:text>
	  <a href="/1g/sales/filter_by_stage.cgi?stage_number=7">7</a>
	  |
	  Search
	  <a href="/1g/sales/search_prospects_input.cgi">prospects</a>
	  or
	  <a href="/1g/sales/search_interactions_input.cgi">interactions</a>
	  |
	  <a href="/1g/sales/new_prospect_input.cgi">New prospect</a>
	  |
	  <a href="/1g/sales/batch_add_interactions_input.cgi">Batch add interactions</a>
	</p>
	<xsl:apply-templates select="search-form"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="search-form">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap;">
      <tr>
	<td class="standard_table_title">
	  Search Prospects
	</td>
      </tr>
      <tr><td>
	<form action="/1g/sales/search_prospects_result.cgi" method="post" autocomplete="off">
	  <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	    <tr>
	      <td style="padding: 4px; ">
		First Name:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">first_name</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Last Name:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">last_name</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Title:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">title</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Company:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">company</xsl:attribute>
		  <xsl:attribute name="size">40</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		City:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">city</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		State:
	      </td>
	      <td>
		<select name="state">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/state-list/state">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		or Province/Territory:
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">state_other</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Country:
	      </td>
	      <td>
		<select name="country">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/country-list/country">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Desired Floor:
	      </td>
	      <td>
		<select name="desired_location">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/floor-list/floor">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Rental Start Date:
	      </td>
	      <td>
		<table cellspacing="0" cellpadding="1px" border="0">
		  <tr>
		    <td>&gt;= </td>
		    <td>
		      <select name="begin_rental_start_month">
			<option value="">[---]</option>
			<xsl:for-each select="/page/month-list/month">
			  <option>
			    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			    <xsl:value-of select="name"/>
			  </option>
			</xsl:for-each>
		      </select>
		      <select name="begin_rental_start_day">
			<option value="">[---]</option>
			<xsl:for-each select="/page/day-list/day">
			  <option>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <input>
			<xsl:attribute name="type">text</xsl:attribute>
			<xsl:attribute name="name">begin_rental_start_year</xsl:attribute>
			<xsl:attribute name="size">2</xsl:attribute>
		      </input>
		    </td>
		  </tr>
		  <tr>
		    <td>&lt;= </td>
		    <td>
		      <select name="end_rental_start_month">
			<option value="">[---]</option>
			<xsl:for-each select="/page/month-list/month">
			  <option>
			    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			    <xsl:value-of select="name"/>
			  </option>
			</xsl:for-each>
		      </select>
		      <select name="end_rental_start_day">
			<option value="">[---]</option>
			<xsl:for-each select="/page/day-list/day">
			  <option>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <input>
			<xsl:attribute name="type">text</xsl:attribute>
			<xsl:attribute name="name">end_rental_start_year</xsl:attribute>
			<xsl:attribute name="size">2</xsl:attribute>
		      </input>
		    </td>
		  </tr>
		</table>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Follow-up Date:
	      </td>
	      <td>
		<table cellspacing="0" cellpadding="1px" border="0">
		  <tr>
		    <td>&gt;= </td>
		    <td>
		      <select name="begin_follow_up_month">
			<option value="">[---]</option>
			<xsl:for-each select="/page/month-list/month">
			  <option>
			    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			    <xsl:value-of select="name"/>
			  </option>
			</xsl:for-each>
		      </select>
		      <select name="begin_follow_up_day">
			<option value="">[---]</option>
			<xsl:for-each select="/page/day-list/day">
			  <option>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <input>
			<xsl:attribute name="type">text</xsl:attribute>
			<xsl:attribute name="name">begin_follow_up_year</xsl:attribute>
			<xsl:attribute name="size">2</xsl:attribute>
		      </input>
		    </td>
		  </tr>
		  <tr>
		    <td>&lt;= </td>
		    <td>
		      <select name="end_follow_up_month">
			<option value="">[---]</option>
			<xsl:for-each select="/page/month-list/month">
			  <option>
			    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			    <xsl:value-of select="name"/>
			  </option>
			</xsl:for-each>
		      </select>
		      <select name="end_follow_up_day">
			<option value="">[---]</option>
			<xsl:for-each select="/page/day-list/day">
			  <option>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <input>
			<xsl:attribute name="type">text</xsl:attribute>
			<xsl:attribute name="name">end_follow_up_year</xsl:attribute>
			<xsl:attribute name="size">2</xsl:attribute>
		      </input>
		    </td>
		  </tr>
		</table>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Initial Contact Timestamp:
	      </td>
	      <td>
		<table cellspacing="0" cellpadding="1px" border="0">
		  <tr>
		    <td>&gt;= </td>
		    <td>
		      <select name="begin_initial_contact_month">
			<option value="">[---]</option>
			<xsl:for-each select="/page/month-list/month">
			  <option>
			    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			    <xsl:value-of select="name"/>
			  </option>
			</xsl:for-each>
		      </select>
		      <select name="begin_initial_contact_day">
			<option value="">[---]</option>
			<xsl:for-each select="/page/day-list/day">
			  <option>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <input>
			<xsl:attribute name="type">text</xsl:attribute>
			<xsl:attribute name="name">begin_initial_contact_year</xsl:attribute>
			<xsl:attribute name="size">2</xsl:attribute>
		      </input>
		    </td>
		  </tr>
		  <tr>
		    <td>&lt;= </td>
		    <td>
		      <select name="end_initial_contact_month">
			<option value="">[---]</option>
			<xsl:for-each select="/page/month-list/month">
			  <option>
			    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			    <xsl:value-of select="name"/>
			  </option>
			</xsl:for-each>
		      </select>
		      <select name="end_initial_contact_day">
			<option value="">[---]</option>
			<xsl:for-each select="/page/day-list/day">
			  <option>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <input>
			<xsl:attribute name="type">text</xsl:attribute>
			<xsl:attribute name="name">end_initial_contact_year</xsl:attribute>
			<xsl:attribute name="size">2</xsl:attribute>
		      </input>
		    </td>
		  </tr>
		</table>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Initial Contact Method:
	      </td>
	      <td>
		<select name="initial_contact_method">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/contact-method-list/contact-method">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Sales Status:
	      </td>
	      <td>
		<select name="sales_status">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/sales-status-list/sales-status">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
		Sales Stage:
	      </td>
	      <td>
		<select name="sales_stage">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/enum-sales-stage-list/sales-stage">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Sales Tag:
	      </td>
	      <td>
		<select name="sales_tag">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/enum-sales-tag-list/sales-tag">
		    <option>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Limit:
	      </td>
	      <td>
		<select name="limit">
		  <option>100</option>
		  <option>250</option>
		  <option>500</option>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
	      </td>
	      <td>
		<input type="submit" name="submitButton" value="Search"/>
	      </td>
	    </tr>
	  </table>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

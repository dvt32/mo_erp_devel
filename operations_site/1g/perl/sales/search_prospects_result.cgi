#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use lead_pad;
use generate_xml_for_prospect_list;
use get_row_by_id_map;
use get_set_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI parameters.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $first_name = Encode::decode($cgi_input_charset, $cgi->param("first_name")) || "";
  my $last_name = Encode::decode($cgi_input_charset, $cgi->param("last_name")) || "";
  my $title = Encode::decode($cgi_input_charset, $cgi->param("title")) || "";
  my $company = Encode::decode($cgi_input_charset, $cgi->param("company")) || "";
  my $city = Encode::decode($cgi_input_charset, $cgi->param("city")) || "";
  my $state = Encode::decode($cgi_input_charset, $cgi->param("state")) || "";
  my $state_other = Encode::decode($cgi_input_charset, $cgi->param("state_other")) || "";
  my $country = Encode::decode($cgi_input_charset, $cgi->param("country")) || "";
  my $desired_location = Encode::decode($cgi_input_charset, $cgi->param("desired_location")) || "";
  my $begin_rental_start_month = Encode::decode($cgi_input_charset, $cgi->param("begin_rental_start_month")) || "";
  my $begin_rental_start_day = Encode::decode($cgi_input_charset, $cgi->param("begin_rental_start_day")) || "";
  my $begin_rental_start_year = Encode::decode($cgi_input_charset, $cgi->param("begin_rental_start_year")) || "";
  my $end_rental_start_month = Encode::decode($cgi_input_charset, $cgi->param("end_rental_start_month")) || "";
  my $end_rental_start_day = Encode::decode($cgi_input_charset, $cgi->param("end_rental_start_day")) || "";
  my $end_rental_start_year = Encode::decode($cgi_input_charset, $cgi->param("end_rental_start_year")) || "";
  my $begin_follow_up_month = Encode::decode($cgi_input_charset, $cgi->param("begin_follow_up_month")) || "";
  my $begin_follow_up_day = Encode::decode($cgi_input_charset, $cgi->param("begin_follow_up_day")) || "";
  my $begin_follow_up_year = Encode::decode($cgi_input_charset, $cgi->param("begin_follow_up_year")) || "";
  my $end_follow_up_month = Encode::decode($cgi_input_charset, $cgi->param("end_follow_up_month")) || "";
  my $end_follow_up_day = Encode::decode($cgi_input_charset, $cgi->param("end_follow_up_day")) || "";
  my $end_follow_up_year = Encode::decode($cgi_input_charset, $cgi->param("end_follow_up_year")) || "";
  my $begin_initial_contact_month = Encode::decode($cgi_input_charset, $cgi->param("begin_initial_contact_month")) || "";
  my $begin_initial_contact_day = Encode::decode($cgi_input_charset, $cgi->param("begin_initial_contact_day")) || "";
  my $begin_initial_contact_year = Encode::decode($cgi_input_charset, $cgi->param("begin_initial_contact_year")) || "";
  my $end_initial_contact_month = Encode::decode($cgi_input_charset, $cgi->param("end_initial_contact_month")) || "";
  my $end_initial_contact_day = Encode::decode($cgi_input_charset, $cgi->param("end_initial_contact_day")) || "";
  my $end_initial_contact_year = Encode::decode($cgi_input_charset, $cgi->param("end_initial_contact_year")) || "";
  my $initial_contact_method = Encode::decode($cgi_input_charset, $cgi->param("initial_contact_method")) || "";
  my $sales_status = Encode::decode($cgi_input_charset, $cgi->param("sales_status")) || "";
  my $sales_stage = Encode::decode($cgi_input_charset, $cgi->param("sales_stage")) || "";
  my $sales_tag = Encode::decode($cgi_input_charset, $cgi->param("sales_tag")) || "";
  my $limit = $cgi->param("limit") || "";
  # if province, copy value to state variable.
  if ($state_other) {
    $state = $state_other;
  }
  # verify begin rental start date is valid.
  my $begin_rental_start_date = undef;
  if (($begin_rental_start_month) || ($begin_rental_start_day) || ($begin_rental_start_year)) {
    if ((!$begin_rental_start_month) || (!$begin_rental_start_day) || (!$begin_rental_start_year)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    if (($begin_rental_start_month < 1) || ($begin_rental_start_month > 12)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    $begin_rental_start_month = lead_pad(int($begin_rental_start_month), 2);
    if (($begin_rental_start_day < 1) || ($begin_rental_start_day > 31)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    $begin_rental_start_day = lead_pad(int($begin_rental_start_day), 2);
    if (($begin_rental_start_year < 2000) || ($begin_rental_start_year > 9999)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    $begin_rental_start_year = int($begin_rental_start_year);
    # assemble date information into single variable.
    $begin_rental_start_date = "$begin_rental_start_year-$begin_rental_start_month-$begin_rental_start_day";
  }
  # verify end rental start date is valid.
  my $end_rental_start_date = undef;
  if (($end_rental_start_month) || ($end_rental_start_day) || ($end_rental_start_year)) {
    if ((!$end_rental_start_month) || (!$end_rental_start_day) || (!$end_rental_start_year)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    if (($end_rental_start_month < 1) || ($end_rental_start_month > 12)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    $end_rental_start_month = lead_pad(int($end_rental_start_month), 2);
    if (($end_rental_start_day < 1) || ($end_rental_start_day > 31)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    $end_rental_start_day = lead_pad(int($end_rental_start_day), 2);
    if (($end_rental_start_year < 2000) || ($end_rental_start_year > 9999)) {
      die "show_page: search_prospects_bad_rental_start_date.html\n";
    }
    $end_rental_start_year = int($end_rental_start_year);
    # assemble date information into single variable.
    $end_rental_start_date = "$end_rental_start_year-$end_rental_start_month-$end_rental_start_day";
  }
  # verify begin follow-up date is valid.
  my $begin_follow_up_date = undef;
  if (($begin_follow_up_month) || ($begin_follow_up_day) || ($begin_follow_up_year)) {
    if ((!$begin_follow_up_month) || (!$begin_follow_up_day) || (!$begin_follow_up_year)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    if (($begin_follow_up_month < 1) || ($begin_follow_up_month > 12)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    $begin_follow_up_month = lead_pad(int($begin_follow_up_month), 2);
    if (($begin_follow_up_day < 1) || ($begin_follow_up_day > 31)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    $begin_follow_up_day = lead_pad(int($begin_follow_up_day), 2);
    if (($begin_follow_up_year < 2000) || ($begin_follow_up_year > 9999)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    $begin_follow_up_year = int($begin_follow_up_year);
    # assemble date information into single variable.
    $begin_follow_up_date = "$begin_follow_up_year-$begin_follow_up_month-$begin_follow_up_day";
  }
  # verify end follow-up date is valid.
  my $end_follow_up_date = undef;
  if (($end_follow_up_month) || ($end_follow_up_day) || ($end_follow_up_year)) {
    if ((!$end_follow_up_month) || (!$end_follow_up_day) || (!$end_follow_up_year)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    if (($end_follow_up_month < 1) || ($end_follow_up_month > 12)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    $end_follow_up_month = lead_pad(int($end_follow_up_month), 2);
    if (($end_follow_up_day < 1) || ($end_follow_up_day > 31)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    $end_follow_up_day = lead_pad(int($end_follow_up_day), 2);
    if (($end_follow_up_year < 2000) || ($end_follow_up_year > 9999)) {
      die "show_page: search_prospects_bad_follow_up_date.html\n";
    }
    $end_follow_up_year = int($end_follow_up_year);
    # assemble date information into single variable.
    $end_follow_up_date = "$end_follow_up_year-$end_follow_up_month-$end_follow_up_day";
  }
  # verify begin initial contact date is valid.
  my $begin_initial_contact_date = undef;
  if (($begin_initial_contact_month) || ($begin_initial_contact_day) || ($begin_initial_contact_year)) {
    if ((!$begin_initial_contact_month) || (!$begin_initial_contact_day) || (!$begin_initial_contact_year)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    if (($begin_initial_contact_month < 1) || ($begin_initial_contact_month > 12)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    $begin_initial_contact_month = lead_pad(int($begin_initial_contact_month), 2);
    if (($begin_initial_contact_day < 1) || ($begin_initial_contact_day > 31)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    $begin_initial_contact_day = lead_pad(int($begin_initial_contact_day), 2);
    if (($begin_initial_contact_year < 2000) || ($begin_initial_contact_year > 9999)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    $begin_initial_contact_year = int($begin_initial_contact_year);
    # assemble date information into single variable.
    $begin_initial_contact_date = "$begin_initial_contact_year-$begin_initial_contact_month-$begin_initial_contact_day";
  }
  # verify end initial contact date is valid.
  my $end_initial_contact_date = undef;
  if (($end_initial_contact_month) || ($end_initial_contact_day) || ($end_initial_contact_year)) {
    if ((!$end_initial_contact_month) || (!$end_initial_contact_day) || (!$end_initial_contact_year)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    if (($end_initial_contact_month < 1) || ($end_initial_contact_month > 12)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    $end_initial_contact_month = lead_pad(int($end_initial_contact_month), 2);
    if (($end_initial_contact_day < 1) || ($end_initial_contact_day > 31)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    $end_initial_contact_day = lead_pad(int($end_initial_contact_day), 2);
    if (($end_initial_contact_year < 2000) || ($end_initial_contact_year > 9999)) {
      die "show_page: search_prospects_bad_initial_contact_date.html\n";
    }
    $end_initial_contact_year = int($end_initial_contact_year);
    # assemble date information into single variable.
    $end_initial_contact_date = "$end_initial_contact_year-$end_initial_contact_month-$end_initial_contact_day";
  }
  # generate query.
  my $db_charset = get_database_charset($instance_map, "main");
  my @sql_query_list;
  my @sql_param_list;
  if ($first_name) {
    push(@sql_query_list, "first_name ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $first_name));
  }
  if ($last_name) {
    push(@sql_query_list, "last_name ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $last_name));
  }
  if ($title) {
    push(@sql_query_list, "title ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $title));
  }
  if ($company) {
    push(@sql_query_list, "company ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $company));
  }
  if ($city) {
    push(@sql_query_list, "city ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $city));
  }
  if ($state) {
    push(@sql_query_list, "state ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $state));
  }
  if ($country) {
    push(@sql_query_list, "country like ?");
    push(@sql_param_list, Encode::encode($db_charset, $country));
  }
  if ($desired_location) {
    push(@sql_query_list, "desired_location = ?");
    push(@sql_param_list, Encode::encode($db_charset, $desired_location));
  }
  if ($begin_rental_start_date) {
    push(@sql_query_list, "rental_start_date >= ?");
    push(@sql_param_list, $begin_rental_start_date);
  }
  if ($end_rental_start_date) {
    push(@sql_query_list, "rental_start_date <= ?");
    push(@sql_param_list, $end_rental_start_date);
  }
  if ($begin_follow_up_date) {
    push(@sql_query_list, "follow_up_date >= ?");
    push(@sql_param_list, $begin_follow_up_date);
  }
  if ($end_follow_up_date) {
    push(@sql_query_list, "follow_up_date <= ?");
    push(@sql_param_list, $end_follow_up_date);
  }
  if ($begin_initial_contact_date) {
    push(@sql_query_list, "initial_contact_dt >= ?");
    push(@sql_param_list, $begin_initial_contact_date);
  }
  if ($end_initial_contact_date) {
    push(@sql_query_list, "initial_contact_dt <= ?");
    push(@sql_param_list, $end_initial_contact_date);
  }
  if ($initial_contact_method) {
    push(@sql_query_list, "initial_contact_method = ?");
    push(@sql_param_list, $initial_contact_method);
  }
  if ($sales_status) {
    push(@sql_query_list, "sales_status = ?");
    push(@sql_param_list, $sales_status);
  }
  if ($sales_stage) {
    push(@sql_query_list, "sales_stage = ?");
    push(@sql_param_list, $sales_stage);
  }
  if ($sales_tag) {
    push(@sql_query_list, "id in (select parent_id from ss_prospective_customer_x_sales_tag_tbl where element_value = ?)");
    push(@sql_param_list, $sales_tag);
  }
  # construct query.
  my $sql_where_criteria = join(" and ", @sql_query_list);
  my $sql_where_clause = scalar(@sql_query_list) > 0 ? " where $sql_where_criteria" : "";
  # fetch prospective customers.
  my (%prospective_customer_by_id, @prospective_customer_id_list);
  my $sql_query = "select * from prospective_customer_tbl$sql_where_clause order by initial_contact_dt desc limit $limit";
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%prospective_customer_by_id, \@prospective_customer_id_list);
  # fetch linked interactions.
  my (%sales_interaction_by_id, @sales_interaction_id_list);
  $sql_query = "select * from sales_interaction_tbl where prospective_customer_id in (select id from prospective_customer_tbl$sql_where_clause order by initial_contact_dt desc limit $limit) order by interaction_dt desc";
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%sales_interaction_by_id, \@sales_interaction_id_list);
  # associate linked interactions with prospective customers.
  my %sales_interaction_id_list_by_prospective_customer_id;
  for my $sales_interaction_id (@sales_interaction_id_list) {
    my $sales_interaction = $sales_interaction_by_id{$sales_interaction_id};
    my $prospective_customer_id = $sales_interaction->{"prospective_customer_id"};
    push(@{$sales_interaction_id_list_by_prospective_customer_id{$prospective_customer_id}}, $sales_interaction_id);
  }
  # fetch linked sales tags.
  my %sales_tag_list_by_prospective_customer_id;
  $sql_query = "select * from ss_prospective_customer_x_sales_tag_tbl where parent_id in (select id from prospective_customer_tbl$sql_where_clause order by initial_contact_dt desc limit $limit)";
  get_set_by_id_map($dbh, $sql_query, \@sql_param_list, \%sales_tag_list_by_prospective_customer_id);
  # generate XML.
  my $generated_xml = generate_xml_for_prospect_list
    (
     "type" => "search-result-list",
     "prospective_customer_id_list" => \@prospective_customer_id_list,
     "prospective_customer_by_id" => \%prospective_customer_by_id,
     "sales_interaction_by_id" => \%sales_interaction_by_id,
     "sales_interaction_id_list_by_prospective_customer_id" => \%sales_interaction_id_list_by_prospective_customer_id,
     "sales_tag_list_by_prospective_customer_id" => \%sales_tag_list_by_prospective_customer_id,
     );
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_prospect_list.xslt", $cgi);
}

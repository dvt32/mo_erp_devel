<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Upcoming Follow-Ups
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="prospective-customer-list">
    <p>
      <a href="/1g/index.cgi">Main menu</a>
      |
      <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
      |
      <a href="/1g/sales/show_recent_interactions.cgi">Recent interactions</a>
      |
      <a href="/1g/sales/search_prospects_input.cgi">Search prospects</a>
      |
      <a href="/1g/sales/new_prospect_input.cgi">New prospect</a>
      |
      <a href="/1g/sales/batch_add_interactions_input.cgi">Batch add interactions</a>
    </p>
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
          Upcoming Follow-Ups
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td style="white-space: nowrap;">Name / Init Time / FU Date</td>
	<td class="standard_table_spacer_column"></td>
	<td style="white-space: nowrap;">Contact Point(s) and Marketing Source / Prospect Details and Space Needs / Notes</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="prospective-customer"/>
    </table>
  </xsl:template>

  <xsl:template match="prospective-customer">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td style="white-space: nowrap;">
	<xsl:value-of select="position()"/>.
	<a>
	  <xsl:attribute name="href">/1g/sales/show_prospect_detail.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="last-name"/>
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="first-name"/>
	</a>
	<br />
	<xsl:value-of select="initial-contact-dt"/>
	<xsl:if test="follow-up-date != ''">
	  <br />
	  <span class="follow_up_date">follow up: <xsl:value-of select="follow-up-date"/></span>
	  <xsl:text> (</xsl:text>
	  <a>
	    <xsl:attribute name="href">/1g/sales/clear_follow_up_date.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	    <xsl:text>X</xsl:text>
	  </a>
	  <xsl:text>)</xsl:text>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:apply-templates select="contact-points"/>
	<xsl:if test="marketing-source != ''">
	  (marketing source: <xsl:value-of select="marketing-source"/>)
	</xsl:if>
	<br />
	<xsl:if test="title != '' or company != ''">
	  <xsl:if test="title != ''">
	    <xsl:value-of select="title"/>,
	  </xsl:if>
	  <xsl:value-of select="company"/>
	</xsl:if>
	<xsl:if test="business_description != ''">
	  <xsl:if test="title != '' or company != ''">
	    <xsl:text> // </xsl:text>
	  </xsl:if>
	  <xsl:text>description: </xsl:text>
	  <xsl:value-of select="business_description"/>
	</xsl:if>
	<xsl:if test="count(space_needs/*) > 0">
	  <xsl:if test="title != '' or company != '' or business_description != ''">
	    <xsl:text> // </xsl:text>
	  </xsl:if>
	  <xsl:apply-templates select="space_needs"/>
	</xsl:if>
	<xsl:apply-templates select="status-stage-tags"/>
	<xsl:if test="notes != ''">
	  <br />
	  Notes:
	  <xsl:value-of select="notes"/>
	</xsl:if>
	<xsl:apply-templates select="sales-interaction-list"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="contact-points">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="work_phone_number | home_phone_number | cell_phone_number | other_phone_number | fax_number | email_address">
    <xsl:choose>
      <xsl:when test="name(.) = 'work_phone_number'">
	<xsl:text>work: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'home_phone_number'">
	<xsl:text>home: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'cell_phone_number'">
	<xsl:text>cell: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'other_phone_number'">
	<xsl:text>other: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'fax_number'">
	<xsl:text>fax: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'email_address'">
	<xsl:text>email: </xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:value-of select="."/>
    <xsl:choose>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="space_needs">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="amount_of_space | rental_start_date | duration_of_rental | desired_location">
    <xsl:choose>
      <xsl:when test="name(.) = 'amount_of_space'">
	<xsl:text>size: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'rental_start_date'">
	<xsl:text>start: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'duration_of_rental'">
	<xsl:text>duration: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'desired_location'">
	<xsl:text>location: </xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:value-of select="."/>
    <xsl:choose>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="status-stage-tags">
    <xsl:if test="sales_status != '' or sales_stage != '' or count(sales-tag-list/sales-tag) > 0">
      <br/>
      <xsl:apply-templates select="sales_status|sales_stage|sales-tag-list"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="sales_status | sales_stage | sales-tag-list">
    <xsl:choose>
      <xsl:when test="name(.) = 'sales_status'">
	<xsl:text>status: </xsl:text>
	<xsl:choose>
	  <xsl:when test=". = 'live'">
	    <span class="sales_status_live"><xsl:value-of select="."/></span>
	  </xsl:when>
	  <xsl:when test=". = 'dead'">
	    <span class="sales_status_dead"><xsl:value-of select="."/></span>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="."/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:when>
      <xsl:when test="name(.) = 'sales_stage'">
	<xsl:text>stage: </xsl:text>
	<span class="sales_stage">
	  <xsl:value-of select="."/>
	</span>
      </xsl:when>
      <xsl:when test="name(.) = 'sales-tag-list'">
	<xsl:if test="count(sales-tag) > 0">
	  <xsl:text>sales tags: </xsl:text>
	  <xsl:apply-templates select="sales-tag"/>
	</xsl:if>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="position() != last()">
      <xsl:text> // </xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="sales-interaction-list">
    <xsl:apply-templates select="sales-interaction"/>
  </xsl:template>

  <xsl:template match="sales-interaction">
    <hr/>
    Date and Time: <xsl:value-of select="interaction_dt"/>,
    Salesperson: <xsl:value-of select="salesperson"/>,
    Type: <xsl:value-of select="interaction_type"/>
    <br/>
    Notes: <xsl:value-of select="notes"/>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

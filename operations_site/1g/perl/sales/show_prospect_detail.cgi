#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use lead_pad;
use get_month_name;
use select_row;
use select_row_with_query;
use get_row_list;
use is_valid_email;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # select salesperson as the employee currently logged in
  my $username = $employee->{"first_name"};
  # get id from param list and try to load prospective customer.
  my ($prospective_customer_id, $prospective_customer) = ($cgi->param("id"), undef);
  if ((!defined($prospective_customer_id)) || ($prospective_customer_id !~ /^\d+$/) || (!($prospective_customer = select_row($dbh, "prospective_customer_tbl", $prospective_customer_id)))) {
    die "show_page: show_prospect_detail_not_found.html\n";
  }
  # fetch linked interactions.
  my @interaction_list;
  my $sql_query = "select * from sales_interaction_tbl where prospective_customer_id = ? order by interaction_dt desc limit 10";
  my @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@interaction_list);
  # count the number of linked interactions.
  my $interaction_count = 0;
  $sql_query = "select count(*) as num_interactions from sales_interaction_tbl where prospective_customer_id = ?";
  @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  if (my $row = select_row_with_query($dbh, $sql_query, \@sql_param_list)) {
    $interaction_count = $row->{"num_interactions"};
  }
  # load enumeration of possible sales stages.
  my @enum_sales_stage_list;
  get_row_list($dbh, "select * from enum_sales_stage_tbl order by name", undef, \@enum_sales_stage_list);
  # fetch linked sales tags.
  my @linked_sales_tag_list;
  $sql_query = "select * from ss_prospective_customer_x_sales_tag_tbl where parent_id = ? order by element_value";
  @sql_param_list = ([$prospective_customer_id, SQL_INTEGER]);
  get_row_list($dbh, $sql_query, \@sql_param_list, \@linked_sales_tag_list);
  # load enumeration of possible sales tags.
  my @enum_sales_tag_list;
  get_row_list($dbh, "select * from enum_sales_tag_tbl order by name", undef, \@enum_sales_tag_list);
  # merge the linked sales tags with the enumeration of possible sales tags.
  for my $sales_tag (@linked_sales_tag_list) {
    if (!grep {$sales_tag->{"element_value"} eq $_->{"name"}} @enum_sales_tag_list) {
      my $row = {"name" => $sales_tag->{"element_value"}};
      push(@enum_sales_tag_list, $row);
    }
  }
  # re-sort the enumeration of possible sales tags.
  @enum_sales_tag_list = sort {$a->{"name"} cmp $b->{"name"}} @enum_sales_tag_list;
  # populate data needed for display.
  my $salutation = HTML::Entities::encode($prospective_customer->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($prospective_customer->{"first_name"} || "");
  my $middle_name = HTML::Entities::encode($prospective_customer->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($prospective_customer->{"last_name"} || "");
  my $suffix = HTML::Entities::encode($prospective_customer->{"suffix"} || "");
  my $title = HTML::Entities::encode($prospective_customer->{"title"} || "");
  my $company = HTML::Entities::encode($prospective_customer->{"company"} || "");
  my $business_description = HTML::Entities::encode($prospective_customer->{"business_description"} || "");
  my @phone_number_list = ();
  if ($prospective_customer->{"work_phone_number"}) {
    push(@phone_number_list, ["work", HTML::Entities::encode($prospective_customer->{"work_phone_number"})]);
  }
  if ($prospective_customer->{"home_phone_number"}) {
    push(@phone_number_list, ["home", HTML::Entities::encode($prospective_customer->{"home_phone_number"})]);
  }
  if ($prospective_customer->{"cell_phone_number"}) {
    push(@phone_number_list, ["cell", HTML::Entities::encode($prospective_customer->{"cell_phone_number"})]);
  }
  if ($prospective_customer->{"other_phone_number"}) {
    push(@phone_number_list, ["other", HTML::Entities::encode($prospective_customer->{"other_phone_number"})]);
  }
  if ($prospective_customer->{"fax_number"}) {
    push(@phone_number_list, ["fax", HTML::Entities::encode($prospective_customer->{"fax_number"})]);
  }
  my $email_address = HTML::Entities::encode($prospective_customer->{"email_address"} || "");
  my $web_site = HTML::Entities::encode($prospective_customer->{"web_site"} || "");
  my @address_component_list;
  for my $tag_name (qw(address_1 address_2 city state zip_code country)) {
    if ($prospective_customer->{$tag_name}) {
      push(@address_component_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
    }
  }
  my @space_needs_component_list;
  for my $tag_name (qw(amount_of_space rental_start_date duration_of_rental desired_location)) {
    if ($prospective_customer->{$tag_name}) {
      push(@space_needs_component_list, [$tag_name, HTML::Entities::encode($prospective_customer->{$tag_name})]);
    }
  }
  my $notes = HTML::Entities::encode($prospective_customer->{"notes"} || "");
  my $follow_up_date = $prospective_customer->{"follow_up_date"} || "";
  my $reminder_email_address = HTML::Entities::encode($prospective_customer->{"reminder_email_address"} || "sales_reminder\@microoffice.com");
  my $sales_status = HTML::Entities::encode($prospective_customer->{"sales_status"} || "");
  my $sales_stage = HTML::Entities::encode($prospective_customer->{"sales_stage"} || "");
  my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"};
  my $marketing_source = HTML::Entities::encode($prospective_customer->{"marketing_source"} || "");
  my $broker_name = HTML::Entities::encode($prospective_customer->{"broker_name"} || "");
  my $referrer_url = HTML::Entities::encode($prospective_customer->{"referrer_url"} || "");
  my $initial_page_url = HTML::Entities::encode($prospective_customer->{"initial_page_url"} || "");
  # generate derived values.
  my $follow_up_month = "";
  my $follow_up_day = "";
  my $follow_up_year = "";
  if (($follow_up_date) && ($follow_up_date =~ /^(\d{4})-(\d\d)-(\d\d)$/)) {
    $follow_up_year = $1;
    $follow_up_month = int($2);
    $follow_up_day = int($3);
  }
  # determine today's date.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  # determine validity of email.
  my $is_valid_email = $prospective_customer->{"email_address"} ? (is_valid_email($prospective_customer->{"email_address"}) ? 1 : 0) : 0;
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <prospective-customer>
    <id>$prospective_customer_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <title>$title</title>
    <company>$company</company>
    <business_description>$business_description</business_description>
    <phone_numbers>
---END_BLOCK---;
  for my $phone_number (@phone_number_list) {
    my $phone_number_name = $phone_number->[0];
    my $phone_number_number = $phone_number->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <phone_number>
        <name>$phone_number_name</name>
        <number>$phone_number_number</number>
      </phone_number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </phone_numbers>
    <email_address>$email_address</email_address>
    <is_valid_email>$is_valid_email</is_valid_email>
    <web_site>$web_site</web_site>
    <address>
---END_BLOCK---;
  for my $address_component (@address_component_list) {
    my $tag_name = $address_component->[0];
    my $tag_value = $address_component->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </address>
    <space_needs>
---END_BLOCK---;
  for my $space_needs_component (@space_needs_component_list) {
    my $tag_name = $space_needs_component->[0];
    my $tag_value = $space_needs_component->[1];
    $generated_xml .= <<"---END_BLOCK---;";
      <$tag_name>$tag_value</$tag_name>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </space_needs>
    <notes>$notes</notes>
    <follow_up_date>$follow_up_date</follow_up_date>
    <reminder_email_address>$reminder_email_address</reminder_email_address>
    <sales_status>$sales_status</sales_status>
    <sales_stage>$sales_stage</sales_stage>
    <initial_contact_dt>$initial_contact_dt</initial_contact_dt>
    <marketing_source>$marketing_source</marketing_source>
    <broker_name>$broker_name</broker_name>
    <referrer_url>$referrer_url</referrer_url>
    <initial_page_url>$initial_page_url</initial_page_url>
  </prospective-customer>
  <linked-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@linked_sales_tag_list) {
    my $name = $sales_tag->{"element_value"};
    $generated_xml .= <<"---END_BLOCK---;";
    <linked-sales-tag>
      <name>$name</name>
    </linked-sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </linked-sales-tag-list>
  <interaction-list>
    <interaction-count>$interaction_count</interaction-count>
---END_BLOCK---;
  for my $interaction (@interaction_list) {
    my $id = $interaction->{"id"};
    my $interaction_dt = $interaction->{"interaction_dt"};
    my $interaction_type = $interaction->{"interaction_type"} || "";
    my $salesperson = HTML::Entities::encode($interaction->{"salesperson"} || "");
    my $notes = HTML::Entities::encode($interaction->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <interaction>
      <id>$id</id>
      <interaction_dt>$interaction_dt</interaction_dt>
      <interaction_type>$interaction_type</interaction_type>
      <salesperson>$salesperson</salesperson>
      <notes>$notes</notes>
    </interaction>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </interaction-list>
  <quick-edit-form>
    <follow_up_month>$follow_up_month</follow_up_month>
    <follow_up_day>$follow_up_day</follow_up_day>
    <follow_up_year>$follow_up_year</follow_up_year>
  </quick-edit-form>
  <current-timestamp>
    <month>$month</month>
    <day>$day</day>
    <year>$year</year>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
  </current-timestamp>
  <enum-sales-status-list>
    <sales-status>live</sales-status>
    <sales-status>customer</sales-status>
    <sales-status>dead</sales-status>
  </enum-sales-status-list>
  <enum-sales-stage-list>
---END_BLOCK---;
  for my $sales_stage (@enum_sales_stage_list) {
    my $name = HTML::Entities::encode($sales_stage->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-stage>$name</sales-stage>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-stage-list>
  <enum-sales-tag-list>
---END_BLOCK---;
  for my $sales_tag (@enum_sales_tag_list) {
    my $name = HTML::Entities::encode($sales_tag->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>$name</sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-sales-tag-list>
  <interaction-type-list>
    <interaction-type>call</interaction-type>
    <interaction-type>email</interaction-type>
    <interaction-type>meeting</interaction-type>
  </interaction-type-list>
  <user>$username</user>
  <salesperson-list>
    <salesperson>
      <name>David</name>
      <value>drotbard</value>
    </salesperson>
    <salesperson>
      <name>Matthieu</name>
      <value>mrosanvallon</value>
    </salesperson>
    <salesperson>
      <name>Yon</name>
      <value>ylam</value>
    </salesperson>
    <salesperson>
      <name>Zoila</name>
      <value>zprimo</value>
    </salesperson>
    <salesperson>
      <name>Jin</name>
      <value>jcho</value>
    </salesperson>
    <salesperson>
      <name>Wellie</name>
      <value>wchao</value>
    </salesperson>
  </salesperson-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <second-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <second>$padded_number</second>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </second-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_prospect_detail.xslt", $cgi);
}

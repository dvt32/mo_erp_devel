<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Prospective Customer Detail
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
	  |
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>/1g/sales/edit_prospect_input.cgi?id=</xsl:text>
	      <xsl:value-of select="/page/prospective-customer/id"/>
	    </xsl:attribute>
	    <xsl:text>Edit prospect</xsl:text>
	  </a>
	  |
	  <a>
	    <xsl:attribute name="href">
	      <xsl:text>/1g/sales/enroll_prospect_input.cgi?id=</xsl:text>
	      <xsl:value-of select="/page/prospective-customer/id"/>
	    </xsl:attribute>
	    <xsl:text>Enroll prospect as new customer</xsl:text>
	  </a>
	</p>
	<xsl:apply-templates select="prospective-customer"/>
	<br/>
	<xsl:apply-templates select="interaction-list"/>
	<br/>
	<xsl:apply-templates select="quick-edit-form"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="prospective-customer">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table">
      <tr>
	<td colspan="2" class="standard_table_title">
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table" style="border: 0" width="100%">
	    <tr>
	      <td class="standard_table_title" style="padding: 0; text-align: left; white-space: nowrap;" width="33%">
		<a>
		  <xsl:attribute name="href">
		    <xsl:text>/1g/sales/download_vcard.cgi?id=</xsl:text>
		    <xsl:value-of select="/page/prospective-customer/id"/>
		  </xsl:attribute>
		  <xsl:attribute name="class">standard_table_title_link</xsl:attribute>
		  <xsl:text>Download vCard</xsl:text>
		</a>
	      </td>
	      <td class="standard_table_title" style="padding: 0; white-space: nowrap;" width="34%">
		Prospective Customer Detail
	      </td>
	      <td class="standard_table_title" style="padding: 0; text-align: right; white-space: nowrap;" width="33%">
		<xsl:text> </xsl:text>
	      </td>
	    </tr>
	  </table>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Name &amp; Co.:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="first_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="middle_name"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="last_name"/>
	  <xsl:if test="suffix != ''">
	    <xsl:text> </xsl:text>
	    <xsl:value-of select="suffix"/>
	  </xsl:if>
	  <xsl:text>, </xsl:text>
	  <xsl:if test="title != '' or company != ''">
	    <xsl:if test="title != ''">
	      <xsl:value-of select="title"/>,
	    </xsl:if>
	    <xsl:value-of select="company"/>
	  </xsl:if>
	  <xsl:if test="business_description != ''">
	    <xsl:if test="title != '' or company != ''">
	      <xsl:text> // </xsl:text>
	    </xsl:if>
	    <xsl:text>description: </xsl:text>
	    <xsl:value-of select="business_description"/>
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap;">
	  Phone &amp; Email:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="phone_numbers"/>
	  <xsl:if test="email_address != ''">
	    email:
	    <xsl:choose>
	      <xsl:when test="is_valid_email = 1">
		<a>
		  <xsl:attribute name="href">
		    <xsl:text>mailto:</xsl:text>
		    <xsl:value-of select="email_address"/>
		  </xsl:attribute>
		  <xsl:value-of select="email_address"/>
		</a>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:value-of select="email_address"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:if>
	  <xsl:if test="web_site != ''">
	    <xsl:if test="email_address != ''">
	      <xsl:text>, </xsl:text>
	    </xsl:if>
	    web: <xsl:value-of select="web_site"/>
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Address:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Space Needs:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="space_needs"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:apply-templates select="notes"/>
	</td>
      </tr>
      <tr>
	<td colspan="2" class="standard_table_title">
          Sales Detail
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Follow-up Date:
	</td>
	<td class="map_table_value">
	  <xsl:if test="follow_up_date != ''">
	    <xsl:value-of select="follow_up_date"/>
	    <xsl:choose>
	      <xsl:when test="reminder_email_address = ''">
		(no email reminder)
	      </xsl:when>
	      <xsl:otherwise>
		(send reminder to <xsl:value-of select="reminder_email_address"/>)
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Status &amp; Stage:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="sales_status"/>
	  <xsl:text>, </xsl:text>
	  <xsl:text>stage: </xsl:text>
	  <xsl:value-of select="sales_stage"/>
	  <xsl:text>, </xsl:text>
	  <xsl:text>tags: </xsl:text>
	  <xsl:apply-templates select="/page/linked-sales-tag-list" mode="output"/>
	</td>
      </tr>
      <tr>
	<td colspan="2" class="standard_table_title">
          Marketing Detail
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap;">
	  Marketing Source:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="marketing_source"/>
	  <xsl:if test="broker_name != ''">
	    <xsl:text>, broker: </xsl:text>
	    <xsl:value-of select="broker_name"/>
	  </xsl:if>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Initial Contact:
	</td>
	<td class="map_table_value">
	  timestamp: <xsl:value-of select="initial_contact_dt"/>,
	  page URL: <xsl:value-of select="initial_page_url"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Referrer URL:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="referrer_url"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="notes">
    <xsl:call-template name="newline_to_br">
      <xsl:with-param name="text_content" select="./text()"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="newline_to_br">
    <xsl:param name="text_content"/>
    <xsl:choose>
      <xsl:when test="contains($text_content, '&#10;')">
	<xsl:value-of select="substring-before($text_content, '&#10;')"/>
	<br/>
	<xsl:call-template name="newline_to_br">
	  <xsl:with-param name="text_content" select="substring-after($text_content, '&#10;')"/>
	</xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="$text_content"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="phone_numbers">
    <xsl:apply-templates select="phone_number"/>
  </xsl:template>

  <xsl:template match="phone_number">
    <xsl:value-of select="name"/>
    <xsl:text>: </xsl:text>
    <xsl:value-of select="number"/>
    <xsl:text>, </xsl:text>
  </xsl:template>

  <xsl:template match="address">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="address_1 | address_2 | city | state | zip_code | country">
    <xsl:value-of select="."/>
    <xsl:choose>
      <xsl:when test="name(following-sibling::*) = 'zip_code'">
	<xsl:text> </xsl:text>
      </xsl:when>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="space_needs">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="amount_of_space | rental_start_date | duration_of_rental | desired_location">
    <xsl:choose>
      <xsl:when test="name(.) = 'amount_of_space'">
	<xsl:text>size: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'rental_start_date'">
	<xsl:text>start: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'duration_of_rental'">
	<xsl:text>duration: </xsl:text>
      </xsl:when>
      <xsl:when test="name(.) = 'desired_location'">
	<xsl:text>location: </xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:value-of select="."/>
    <xsl:choose>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="linked-sales-tag-list" mode="output">
    <xsl:apply-templates select="linked-sales-tag" mode="output"/>
  </xsl:template>

  <xsl:template match="linked-sales-tag" mode="output">
    <xsl:text>[</xsl:text>
    <xsl:value-of select="name"/>
    <xsl:text>]</xsl:text>
    <xsl:choose>
      <xsl:when test="position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="interaction-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="7">
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table" width="100%">
	    <tr>
	      <td class="standard_table_title" style="text-align: left; white-space: nowrap;" width="33%">
		<xsl:text> </xsl:text>
	      </td>
	      <td class="standard_table_title" style="white-space: nowrap;" width="34%">
		<xsl:text>Interactions (</xsl:text>
		<xsl:if test="interaction-count > count(interaction)">
		  <xsl:value-of select="count(interaction)"/>
		  <xsl:text> most recent out of </xsl:text>
		</xsl:if>
		<xsl:value-of select="interaction-count"/>
		<xsl:text> total)</xsl:text>
	      </td>
	      <td class="standard_table_title" style="text-align: right; white-space: nowrap;" width="33%">
		<xsl:if test="interaction-count > count(interaction)">
		  <a style="color: white; font-weight: normal;">
		    <xsl:attribute name="href">
		      <xsl:text>/1g/sales/show_all_interactions.cgi?prospective_customer_id=</xsl:text>
		      <xsl:value-of select="/page/prospective-customer/id"/>
		    </xsl:attribute>
		    <xsl:text>Show all interactions</xsl:text>
		  </a>
		</xsl:if>
	      </td>
	    </tr>
	  </table>
	</td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(interaction) > 0">
	  <tr class="standard_table_column_heading_row">
	    <td class="standard_table_side_padding"></td>
	    <td>#</td>
	    <td class="standard_table_spacer_column"></td>
	    <td style="white-space: nowrap;">Time / Type &amp; Person</td>
	    <td class="standard_table_spacer_column"></td>
	    <td>Notes</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <xsl:apply-templates select="interaction"/>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="7" align="center">
	      No interactions.
	    </td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="interaction">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="position()"/>.
      </td>
      <td class="standard_table_spacer_column"></td>
      <td style="white-space: nowrap;">
	<xsl:value-of select="interaction_dt"/>
	<br/>
	<xsl:value-of select="interaction_type"/>,
	<xsl:value-of select="salesperson"/>
	<br/>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/sales/edit_interaction_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:text>Edit</xsl:text>
	</a>
	|
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/sales/delete_interaction_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:text>Delete</xsl:text>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:apply-templates select="notes"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="quick-edit-form">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap;">
      <tr>
	<td class="standard_table_title">
          Quick Edit
	</td>
      </tr>
      <tr><td>
	<form action="/1g/sales/quick_edit_prospect_result.cgi" method="post">
	  <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="/page/prospective-customer/id"/></xsl:attribute>
	  </input>
	  <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	    <tr>
	      <td style="padding: 4px; text-align: right; ">
		Follow-up Date:
	      </td>
	      <td>
		<select name="follow_up_month">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/month-list/month">
		    <option>
		      <xsl:if test="/page/quick-edit-form/follow_up_month = number">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		      <xsl:value-of select="name"/>
		    </option>
		  </xsl:for-each>
		</select>
		<select name="follow_up_day">
		  <option value="">[---]</option>
		  <xsl:for-each select="/page/day-list/day">
		    <option>
		      <xsl:if test="/page/quick-edit-form/follow_up_day = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">follow_up_year</xsl:attribute>
		  <xsl:attribute name="size">2</xsl:attribute>
		  <xsl:attribute name="value">
		    <xsl:if test="string-length(/page/quick-edit-form/follow_up_year)!=0"><xsl:value-of select="/page/quick-edit-form/follow_up_year"/></xsl:if>
		    <xsl:if test="string-length(/page/quick-edit-form/follow_up_year)=0"><xsl:value-of select="/page/current-timestamp/year"/></xsl:if>
		  </xsl:attribute>
		</input>
	      </td>
	      <td rowspan="5" style="margin-left: 4px; ">
		<table style="margin: 1em; border-left: thin solid gray; white-space: nowrap; ">
		  <tr>
		    <td style="padding: 4px; text-align: right; " valign="top">
		      Interaction Time:
		    </td>
		    <td colspan="3">
		      <select name="month">
			<xsl:for-each select="/page/month-list/month">
			  <option>
			    <xsl:if test="/page/current-timestamp/month = number">
			      <xsl:attribute name="selected">selected</xsl:attribute>
			    </xsl:if>
			    <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			    <xsl:value-of select="name"/>
			  </option>
			</xsl:for-each>
		      </select>
		      <select name="day">
			<xsl:for-each select="/page/day-list/day">
			  <option>
			    <xsl:if test="/page/current-timestamp/day = .">
			      <xsl:attribute name="selected">selected</xsl:attribute>
			    </xsl:if>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <input>
			<xsl:attribute name="type">text</xsl:attribute>
			<xsl:attribute name="name">year</xsl:attribute>
			<xsl:attribute name="size">2</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="/page/current-timestamp/year"/></xsl:attribute>
		      </input>
		      <xsl:text> at </xsl:text>
		      <select name="hour">
			<xsl:for-each select="/page/hour-list/hour">
			  <option>
			    <xsl:if test="/page/current-timestamp/hour = .">
			      <xsl:attribute name="selected">selected</xsl:attribute>
			    </xsl:if>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <xsl:text>:</xsl:text>
		      <select name="minute">
			<xsl:for-each select="/page/minute-list/minute">
			  <option>
			    <xsl:if test="/page/current-timestamp/minute = .">
			      <xsl:attribute name="selected">selected</xsl:attribute>
			    </xsl:if>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		      <xsl:text>:</xsl:text>
		      <select name="second">
			<xsl:for-each select="/page/second-list/second">
			  <option>
			    <xsl:if test="/page/current-timestamp/second = .">
			      <xsl:attribute name="selected">selected</xsl:attribute>
			    </xsl:if>
			    <xsl:value-of select="."/>
			  </option>
			</xsl:for-each>
		      </select>
		    </td>
		  </tr>
		  <tr>
		    <td style="padding: 4px; text-align: right; " valign="top">
		      Interaction Type:
		    </td>
		    <td valign="top">
		      <xsl:for-each select="/page/interaction-type-list/interaction-type">
			<xsl:element name="input">
			  <xsl:attribute name="type">radio</xsl:attribute>
			  <xsl:attribute name="name">interaction_type</xsl:attribute>
			  <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
				<xsl:if test="'call' = .">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
			  <xsl:value-of select="."/>
			  <br />
			</xsl:element>
		      </xsl:for-each>
		    </td>
		    <td style="padding: 4px; text-align: right; " valign="top">
		      Salesperson:
		    </td>
		    <td>
		      <xsl:for-each select="/page/salesperson-list/salesperson">
			<xsl:element name="input">
			  <xsl:attribute name="type">radio</xsl:attribute>
			  <xsl:attribute name="name">salesperson</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
				<xsl:if test="/page/user = name">
					<xsl:attribute name="checked">checked</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="name"/>
			  <br />
			</xsl:element>
		      </xsl:for-each>
		    </td>
		  </tr>
		  <tr valign="top">
		    <td style="padding: 4px; text-align: right ">
		      Notes:
		    </td>
		    <td colspan="3">
		      <textarea>
			<xsl:attribute name="name">notes</xsl:attribute>
			<xsl:attribute name="rows">8</xsl:attribute>
			<xsl:attribute name="cols">40</xsl:attribute>
			<!-- Need this blank notes field; otherwise the surrounding textarea gets collapsed into a single tag. -->
			<xsl:text>.</xsl:text>
		      </textarea>
		    </td>
		  </tr>
		</table>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; text-align: right; ">
		Reminder Email Address:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">reminder_email_address</xsl:attribute>
		  <xsl:attribute name="value"><xsl:value-of select="/page/prospective-customer/reminder_email_address"/></xsl:attribute>
		  <xsl:attribute name="size">30</xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td colspan="2">
	      <table><tr>
		<td style="padding: 4px; text-align: right; white-space: nowrap; " valign="top">
		  Sales Status:
		</td>
		<td style="white-space: nowrap; " valign="top">
		  <xsl:for-each select="/page/enum-sales-status-list/sales-status">
		    <xsl:element name="input">
		      <xsl:attribute name="type">radio</xsl:attribute>
		      <xsl:attribute name="name">sales_status</xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
		      <xsl:if test="/page/prospective-customer/sales_status = .">
			  <xsl:attribute name="checked">true</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		      <br />
		    </xsl:element>
		  </xsl:for-each>
		</td>
		<td style="padding: 4px; text-align: right; white-space: nowrap; " valign="top">
		  Sales Stage:
		</td>
		<td style="white-space: nowrap; ">
		  <xsl:for-each select="/page/enum-sales-stage-list/sales-stage">
		    <xsl:element name="input">
		      <xsl:attribute name="type">radio</xsl:attribute>
		      <xsl:attribute name="name">sales_stage</xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
		      <xsl:if test="/page/prospective-customer/sales_stage = .">
			<xsl:attribute name="checked">true</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		      <br />
		    </xsl:element>
		  </xsl:for-each>
		</td>
	      </tr></table></td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; text-align: right; ">
		Sales Tags:
	      </td>
	      <td>
		<select name="sales_tag_list" multiple="multiple" size="8">
		  <xsl:for-each select="/page/enum-sales-tag-list/sales-tag">
		    <option>
		      <xsl:if test="/page/linked-sales-tag-list/linked-sales-tag/name = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td colspan="3" style="text-align: right; ">
		<input type="submit" name="submitButton" value="Save"/>
	      </td>
	    </tr>
	  </table>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch interactions from the last fourteen days forward.
  my (%interaction_by_id, @interaction_id_list);
  my $sql_query = "select * from sales_interaction_tbl where interaction_dt >= current_date - interval ? order by interaction_dt desc, salesperson, interaction_type";
  my @sql_param_list = ("14 days");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%interaction_by_id, \@interaction_id_list);
  # fetch linked prospective customers.
  my %prospective_customer_by_id;
  $sql_query = "select distinct prospective_customer_tbl.* from sales_interaction_tbl, prospective_customer_tbl where sales_interaction_tbl.interaction_dt >= current_date - interval ? and sales_interaction_tbl.prospective_customer_id = prospective_customer_tbl.id";
  @sql_param_list = ("14 days");
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%prospective_customer_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <interaction-list>
---END_BLOCK---;
  for my $interaction_id (@interaction_id_list) {
    my $interaction = $interaction_by_id{$interaction_id};
    my $prospective_customer_id = $interaction->{"prospective_customer_id"};
    my $interaction_dt = $interaction->{"interaction_dt"};
    my $salesperson = HTML::Entities::encode($interaction->{"salesperson"} || "");
    my $interaction_type = HTML::Entities::encode($interaction->{"interaction_type"} || "");
    my $notes = HTML::Entities::encode($interaction->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <interaction>
      <id>$interaction_id</id>
      <prospective_customer_id>$prospective_customer_id</prospective_customer_id>
      <interaction_dt>$interaction_dt</interaction_dt>
      <salesperson>$salesperson</salesperson>
      <interaction_type>$interaction_type</interaction_type>
      <notes>$notes</notes>
---END_BLOCK---;
    my $prospective_customer = $prospective_customer_by_id{$prospective_customer_id};
    my $first_name = HTML::Entities::encode(ucfirst($prospective_customer->{"first_name"} || ""));
    my $last_name = HTML::Entities::encode(ucfirst($prospective_customer->{"last_name"} || ""));
    my $follow_up_date = $prospective_customer->{"follow_up_date"} || "";
    $generated_xml .= <<"---END_BLOCK---;";
      <prospective-customer>
        <id>$prospective_customer_id</id>
        <first_name>$first_name</first_name>
        <last_name>$last_name</last_name>
        <follow_up_date>$follow_up_date</follow_up_date>
      </prospective-customer>
    </interaction>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </interaction-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_recent_interactions.xslt", $cgi);
}

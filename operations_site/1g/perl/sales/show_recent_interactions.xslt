<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Recent Interactions
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="interaction-list">
    <p>
      <a href="/1g/index.cgi">Main menu</a>
      |
      <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
      |
      <a href="/1g/sales/show_follow_ups.cgi">Follow-ups</a>
      |
      <a href="/1g/sales/search_prospects_input.cgi">Search prospects</a>
      |
      <a href="/1g/sales/new_prospect_input.cgi">New prospect</a>
      |
      <a href="/1g/sales/batch_add_interactions_input.cgi">Batch add interactions</a>
    </p>
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
          Recent Interactions of the Past 14 Days
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td style="white-space: nowrap;">Name / Follow-Up Date</td>
	<td class="standard_table_spacer_column"></td>
	<td style="white-space: nowrap;">Interaction Time, Interaction Type, Salesperson / Notes</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="interaction"/>
    </table>
  </xsl:template>

  <xsl:template match="interaction">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td style="white-space: nowrap;">
	<a>
	  <xsl:attribute name="href">/1g/sales/show_prospect_detail.cgi?id=<xsl:value-of select="prospective-customer/id"/></xsl:attribute>
	  <xsl:value-of select="prospective-customer/last_name"/>
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="prospective-customer/first_name"/>
	</a>
	<xsl:if test="prospective-customer/follow_up_date != ''">
	  <br />
	  <span class="follow_up_date">follow up: <xsl:value-of select="prospective-customer/follow_up_date"/></span>
	  <xsl:text> (</xsl:text>
	  <a>
	    <xsl:attribute name="href">/1g/sales/clear_follow_up_date.cgi?id=<xsl:value-of select="prospective-customer/id"/></xsl:attribute>
	    <xsl:text>X</xsl:text>
	  </a>
	  <xsl:text>)</xsl:text>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="interaction_dt"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="interaction_type"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="salesperson"/>
	<xsl:if test="notes != ''">
	  <br />
	  <xsl:apply-templates select="notes"/>
	</xsl:if>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="notes">
    <xsl:call-template name="newline_to_br">
      <xsl:with-param name="text_content" select="./text()"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="newline_to_br">
    <xsl:param name="text_content"/>
    <xsl:choose>
      <xsl:when test="contains($text_content, '&#10;')">
	<xsl:value-of select="substring-before($text_content, '&#10;')"/>
	<br/>
	<xsl:call-template name="newline_to_br">
	  <xsl:with-param name="text_content" select="substring-after($text_content, '&#10;')"/>
	</xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="$text_content"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

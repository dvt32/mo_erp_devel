#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use get_role_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # determine authorization.
  my $role_list = get_role_list($instance_map, $employee->{"id"});
  if (!grep {$_ eq "role:sales/manage_sales_order"} @$role_list) {
    die "show_page: ../auth/not_authorized.html\n";
  }
  # fetch sales orders.
  my (%sales_order_by_id, @sales_order_id_list);
  my $sql_query = "select * from sales_order_tbl order by creation_dt desc limit 100";
  get_row_by_id_map($dbh, $sql_query, undef, \%sales_order_by_id, \@sales_order_id_list);
  # finished with database access, so disconnect from database.
  $dbh->disconnect();
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sales_order_list>
---END_BLOCK---;
  for my $sales_order_id (@sales_order_id_list) {
    my $sales_order = $sales_order_by_id{$sales_order_id};
    my $licensee_name = HTML::Entities::encode($sales_order->{"licensee_name"} || "");
    my $note = HTML::Entities::encode($sales_order->{"note"} || "");
    my $creation_dt = HTML::Entities::encode($sales_order->{"creation_dt"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <sales_order>
      <id>$sales_order_id</id>
      <licensee_name>$licensee_name</licensee_name>
      <note>$note</note>
      <creation_dt>$creation_dt</creation_dt>
    </sales_order>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </sales_order_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_sales_order_list.xslt", $cgi);
}

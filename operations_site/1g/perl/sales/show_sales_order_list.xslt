<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  <xsl:text>Micro Office Solutions :: Operations Center :: Show Sales Orders</xsl:text>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="sales_order_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sales_order_list">
    <p>
      <a href="/1g/index.cgi">Main menu</a>
      |
      <a href="/1g/sales/show_prospect_list.cgi">Prospect list</a>
      |
      <a href="/1g/sales/show_follow_ups.cgi">Follow-ups</a>
      |
      <a href="/1g/sales/show_recent_interactions.cgi">Recent interactions</a>
      |
      Stage
      <a href="/1g/sales/filter_by_stage.cgi?stage_number=1">1</a>
      <xsl:text> </xsl:text>
      <a href="/1g/sales/filter_by_stage.cgi?stage_number=2">2</a>
      <xsl:text> </xsl:text>
      <a href="/1g/sales/filter_by_stage.cgi?stage_number=3">3</a>
      <xsl:text> </xsl:text>
      <a href="/1g/sales/filter_by_stage.cgi?stage_number=4">4</a>
      <xsl:text> </xsl:text>
      <a href="/1g/sales/filter_by_stage.cgi?stage_number=5">5</a>
      <xsl:text> </xsl:text>
      <a href="/1g/sales/filter_by_stage.cgi?stage_number=6">6</a>
      <xsl:text> </xsl:text>
      <a href="/1g/sales/filter_by_stage.cgi?stage_number=7">7</a>
      |
      Search
      <a href="/1g/sales/search_prospects_input.cgi">prospects</a>
      or
      <a href="/1g/sales/search_interactions_input.cgi">interactions</a>
      |
      <a href="/1g/sales/new_prospect_input.cgi">New prospect</a>
      |
      <a href="/1g/sales/batch_add_interactions_input.cgi">Batch add interactions</a>
    </p>
    <h1>
      New Sales Order
    </h1>
    <form action="/1g/sales/new_sales_order_result.cgi" method="POST">
      <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	<tr>
	  <td style="padding: 4px; ">
	    <xsl:text> </xsl:text>
	  </td>
	  <td><span class="required_field">Required Fields are Red</span></td>
	</tr>
	<tr>
	  <td style="padding: 4px; ">
	    <span class="required_field">Licensee Name:</span>
	  </td>
	  <td>
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">licensee_name</xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td style="padding: 4px; ">
	    Note:
	  </td>
	  <td>
	    <input>
	      <xsl:attribute name="type">text</xsl:attribute>
	      <xsl:attribute name="name">note</xsl:attribute>
	      <xsl:attribute name="size">60</xsl:attribute>
	    </input>
	  </td>
	</tr>
	<tr>
	  <td style="padding: 4px; text-align: right; ">
	    <input type="submit" name="submitButton" value="Create"/>
	  </td>
	  <td><span class="required_field">Required Fields are Red</span></td>
	</tr>
      </table>
    </form>
    <h1>
      Upload Sales Order Form
    </h1>
    <p>
      After entering the sales order above and reading the sales order ID below, make sure you
      <a href="http://ops2015.microoffice.com/3g/salesorderform/file_input.php">upload the sales order form</a>.
      That helps test the upload feature.
    </p>
    <h1>
      Previous Sales Orders
    </h1>
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="9" class="standard_table_title">
	  Last <xsl:value-of select="count(/page/sales_order_list/sales_order)"/> Sales Orders
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td style="white-space: nowrap;">ID</td>
	<td class="standard_table_spacer_column"></td>
	<td style="white-space: nowrap;">Timestamp</td>
	<td class="standard_table_spacer_column"></td>
	<td style="white-space: nowrap;">Name</td>
	<td class="standard_table_spacer_column"></td>
	<td style="white-space: nowrap;">Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="sales_order"/>
    </table>
  </xsl:template>

  <xsl:template match="sales_order">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="id"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="creation_dt"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="licensee_name"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="note"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

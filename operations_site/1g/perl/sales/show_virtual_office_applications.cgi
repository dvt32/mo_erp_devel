#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load unprocessed virtual office applications.
  my @virtual_office_application_list;
  get_row_list($dbh, "select * from virtual_office_application_tbl where not(processed) and not(cancelled) order by request_datetime", undef, \@virtual_office_application_list);
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <virtual-office-application-list>
---END_BLOCK---;
  for my $virtual_office_application (@virtual_office_application_list) {
    my $id = $virtual_office_application->{"id"};
    my $request_datetime = $virtual_office_application->{"request_datetime"};
    my $salutation = $virtual_office_application->{"salutation"} || "";
    my $first_name = HTML::Entities::encode($virtual_office_application->{"first_name"});
    my $middle_name = HTML::Entities::encode($virtual_office_application->{"middle_name"} || "");
    my $last_name = HTML::Entities::encode($virtual_office_application->{"last_name"});
    my $company = HTML::Entities::encode($virtual_office_application->{"company"} || "");
    my $title = HTML::Entities::encode($virtual_office_application->{"title"} || "");
    my $business_description = HTML::Entities::encode($virtual_office_application->{"business_description"} || "");
    my $day_phone_number = HTML::Entities::encode($virtual_office_application->{"day_phone_number"} || "");
    my $evening_phone_number = HTML::Entities::encode($virtual_office_application->{"evening_phone_number"} || "");
    my $cell_phone_number = HTML::Entities::encode($virtual_office_application->{"cell_phone_number"} || "");
    my $fax_number = HTML::Entities::encode($virtual_office_application->{"fax_number"} || "");
    my $email_address = HTML::Entities::encode($virtual_office_application->{"email_address"});
    my $web_site = HTML::Entities::encode($virtual_office_application->{"web_site"} || "");
    my $name_on_card = HTML::Entities::encode($virtual_office_application->{"name_on_card"});
    my $company_on_card = HTML::Entities::encode($virtual_office_application->{"company_on_card"} || "");
    my $credit_card_type = HTML::Entities::encode($virtual_office_application->{"credit_card_type"});
    my $credit_card_number = HTML::Entities::encode($virtual_office_application->{"credit_card_number"});
    my $service_plan = HTML::Entities::encode($virtual_office_application->{"service_plan"});
    my $marketing_source = HTML::Entities::encode($virtual_office_application->{"marketing_source"} || "");
    my $notes = HTML::Entities::encode($virtual_office_application->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <virtual-office-application>
      <id>$id</id>
      <request-datetime>$request_datetime</request-datetime>
      <salutation>$salutation</salutation>
      <first-name>$first_name</first-name>
      <middle-name>$middle_name</middle-name>
      <last-name>$last_name</last-name>
      <company>$company</company>
      <title>$title</title>
      <business-description>$business_description</business-description>
      <day-phone-number>$day_phone_number</day-phone-number>
      <evening-phone-number>$evening_phone_number</evening-phone-number>
      <cell-phone-number>$cell_phone_number</cell-phone-number>
      <fax-number>$fax_number</fax-number>
      <email-address>$email_address</email-address>
      <web-site>$web_site</web-site>
      <name-on-card>$name_on_card</name-on-card>
      <company-on-card>$company_on_card</company-on-card>
      <credit-card-type>$credit_card_type</credit-card-type>
      <credit-card-number>$credit_card_number</credit-card-number>
      <service-plan>$service_plan</service-plan>
      <marketing-source>$marketing_source</marketing-source>
      <notes>$notes</notes>
    </virtual-office-application>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </virtual-office-application-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_virtual_office_applications.xslt", $cgi);
}

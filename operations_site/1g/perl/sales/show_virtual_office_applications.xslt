<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Virtual Office Applications
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="virtual-office-application-list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="virtual-office-application-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="15" class="standard_table_title">
          Virtual Office Applications
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>#</td>
	<td class="standard_table_spacer_column"></td>
	<td>Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Company</td>
	<td class="standard_table_spacer_column"></td>
	<td>Title</td>
	<td class="standard_table_spacer_column"></td>
	<td>Service Plan</td>
	<td class="standard_table_spacer_column"></td>
	<td>Marketing Source</td>
	<td class="standard_table_side_padding"></td>
	<td>Order Time</td>
	<td class="standard_table_spacer_column"></td>
      </tr>
      <xsl:apply-templates select="virtual-office-application"/>
      <xsl:if test="count(virtual-office-application) &lt; 1">
	<tr>
	  <td colspan="15" class="standard_table_footer">No Unprocessed Virtual Office Applications</td>
	</tr>
      </xsl:if>
    </table>
  </xsl:template>

  <xsl:template match="virtual-office-application">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="right"><xsl:value-of select="position()"/>.</td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/sales/process_virtual_office_application_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="last-name"/>
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="first-name"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="company"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="title"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="service-plan"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="marketing-source"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="request-datetime"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>&nbsp;</td>
      <td class="standard_table_spacer_column"></td>
      <td>&nbsp;</td>
      <td class="standard_table_spacer_column"></td>
      <td colspan="9"><xsl:value-of select="business-description"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use init_login_standard;
use apply_xslt_output_xhtml;
use get_month_name;
use get_row_by_id_map;
use select_row_with_query;
use main_exception_wrapper;
use timestamp_to_components;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $desk_id = $cgi->param("desk_id");
  # error checking for the desk_id
  if (!$desk_id || $desk_id !~ /^\d{1,4}$/ ) {
    die "show_page: edit_desk_bad_id.html";
  }
  # check for current info on this desk_id
  my $sql_query = "select * from desk_tbl where id = ?";
  my @sql_param_list = ([$desk_id, SQL_INTEGER]);
  my $desk_info = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  # there isn't any entry for that desk_id
  if (!$desk_info) {
    die "show_page: edit_desk_problem.html\n";
  }
  # get area
  $sql_query = "select * from desk_area_vw where id = ?";
  @sql_param_list = ([$desk_id, SQL_INTEGER]);
  my $desk_area = select_row_with_query($dbh, $sql_query, \@sql_param_list);

  my $floor_id        = $desk_info->{"floor_id"};
  my $assigned_number = $desk_info->{"assigned_number"};
  my $account_id      = $desk_info->{"account_id"};
  my $cust_pers_id    = $desk_info->{"cust_pers_id"};
  my $num_people      = $desk_info->{"num_people"};
  my $is_enclosed     = $desk_info->{"is_enclosed"};
  my $list_price      = $desk_info->{"list_price"};
  my $current_price   = $desk_info->{"current_price"};
  my $internet_telephone_revenue = $desk_info->{"internet_telephone_revenue"};
  my $start_date      = ($desk_info->{"start_date"})?format_date($desk_info->{"start_date"}):'';
  my $end_date        = ($desk_info->{"end_date"})?format_date($desk_info->{"end_date"}):'';
  my $increase_date   = ($desk_info->{"increase_date"})?format_date($desk_info->{"increase_date"}):'';
  my $waiting_account = $desk_info->{"waiting_account"};
  my $note            = HTML::Entities::encode($desk_info->{"note"});

  # check for linked desks
  my @linked_desks;
  my $sql_query = "select id, assigned_number from desk_tbl where account_id = ? and id != ?";
  my @sql_param_list = ([$account_id, SQL_INTEGER], [$desk_id, SQL_INTEGER]);
  get_row_list($dbh,$sql_query,\@sql_param_list,\@linked_desks);

  # load accounts.
  my (%customer_by_id, @customer_id_list);
  my $sql_query = "select * from account_vw where primary_floor_id IN (SELECT id FROM floor_tbl WHERE building_id IN (SELECT building_id FROM floor_tbl WHERE id = ?)) OR primary_floor_id = 16 and is_active = 1 order by name";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%customer_by_id, \@customer_id_list);

  # load persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  my $sql_query = "select cp.id, first_name, last_name, cust_acct_id from cust_pers_tbl cp
    left join account_vw a on cp.cust_acct_id = a.id
    where primary_floor_id = ? and cp.is_active
    order by cp.cust_acct_id, first_name;";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_pers_by_id, \@cust_pers_id_list);

  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <floor-id>$floor_id</floor-id>
  <form>
    <desk-info>
      <desk-id>$desk_id</desk-id>
      <assigned-number>$assigned_number</assigned-number>
      <num-people>$num_people</num-people>
      <is-enclosed>$is_enclosed</is-enclosed>
      <list-price>$list_price</list-price>
      <current-price>$current_price</current-price>
      <internet-telephone-revenue>$internet_telephone_revenue</internet-telephone-revenue>
      <start-date>$start_date</start-date>
      <end-date>$end_date</end-date>
      <increase-date>$increase_date</increase-date>
      <desk_area>$desk_area->{"desk_area"}</desk_area>
---END_BLOCK---;
  if ($note) {
    $generated_xml .= <<"---END_BLOCK---;";
      <note>$note</note>
---END_BLOCK---;
  }
  if ($account_id) {
    $generated_xml .= <<"---END_BLOCK---;";
      <account-id>$account_id</account-id>
---END_BLOCK---;
  }
  if ($cust_pers_id) {
    $generated_xml .= <<"---END_BLOCK---;";
      <cust-pers-id>$cust_pers_id</cust-pers-id>
---END_BLOCK---;
  }
  if ($waiting_account) {
    $generated_xml .= <<"---END_BLOCK---;";
      <waiting-account>$waiting_account</waiting-account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </desk-info>
    <customer-list>
---END_BLOCK---;
  for my $id (@customer_id_list) {
    my $customer = $customer_by_id{$id};
    my $name = HTML::Entities::encode($customer->{"name"});
    my $is_active = $customer->{"is_active"} ? 1 : 0;
    $generated_xml .= <<"---END_BLOCK---;";
      <customer>
        <id>$id</id>
        <name>$name</name>
        <is_active>$is_active</is_active>
      </customer>
---END_BLOCK---;
  }

  $generated_xml .= <<"---END_BLOCK---;";
  	</customer-list>
    <person-list>
---END_BLOCK---;
  for my $id (@cust_pers_id_list) {
    my $person = $cust_pers_by_id{$id};
    my $pers_cust_acct_id = $person->{"cust_acct_id"};
    my $name = HTML::Entities::encode($person->{"first_name"})." ".HTML::Entities::encode($person->{"last_name"});
    $generated_xml .= <<"---END_BLOCK---;";
      <person>
        <id>$id</id>
        <name>$name</name>
        <cust_acct_id>$pers_cust_acct_id</cust_acct_id>
      </person>
---END_BLOCK---;
  }

  $generated_xml .= <<"---END_BLOCK---;";
  	</person-list>
	<linked-desks>
---END_BLOCK---;

  for my $desk (@linked_desks){
	my $id = $desk->{'id'};
	my $assigned_number = $desk->{'assigned_number'};
	$generated_xml .= <<"---END_BLOCK---;";
      <desk>
        <id>$id</id>
		<assigned-number>$assigned_number</assigned-number>
      </desk>
---END_BLOCK---;
  }

$generated_xml .= <<"---END_BLOCK---;";
    </linked-desks>
  </form>
</page>
---END_BLOCK---;

  # apply XSLT stylesheet and output XHTML page.
  if (lc($cgi->param("mode")) eq "view") {
	apply_xslt_output_xhtml($generated_xml, "view_desk_info.xslt", $cgi);
  } else {
	apply_xslt_output_xhtml($generated_xml, "edit_desk_info.xslt", $cgi);
  }

}
sub format_date($){
	my $date = shift;
	my @date_parts;
  push(@date_parts, split(/-/, $date));
  return "$date_parts[1]/$date_parts[2]/$date_parts[0]";
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Desk Information
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	<link type="text/css" href="/1g/css/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="/1g/js/jquery-latest.pack.js"><xsl:comment></xsl:comment></script>
	<script type="text/javascript" src="/1g/js/jquery-ui-1.8.custom.min.js"><xsl:comment></xsl:comment></script>
	<script type="text/javascript">
		$(document).ready(
			function(){
			  $('input[name=start_date]').datepicker({changeMonth: true,changeYear: true});
			  $('input[name=end_date]').datepicker({changeMonth: true,changeYear: true});
			  $('input[name=increase_date]').datepicker({changeMonth: true,changeYear: true});
			}
		);
	</script>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a>
	    <xsl:attribute name="href">/1g/space/show_desks.cgi?floor_id=<xsl:value-of select="floor-id"/></xsl:attribute>
	    Back to Desk Map</a> |
	    <a href="/1g/tenant/show_active_accounts.cgi">Show Active Accounts</a> |
	    <a>
	      <xsl:attribute name="href">/201605_flask/space/edit_desk_info/<xsl:value-of select="form/desk-info/desk-id"/></xsl:attribute>
	      View New Edit Desk Page
	    </a>
	</p>
	<xsl:apply-templates select="form"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="form">
    <form action="/1g/space/update_desk_info.cgi" method="post">
      <input type="hidden" name="desk_id">
	<xsl:attribute name="value"><xsl:value-of select="desk-info/desk-id"/></xsl:attribute>
      </input>
      <fieldset class="compact_fieldset">
	<legend>
	  Edit Information for Desk Number <xsl:value-of select="desk-info/assigned-number"/>
	</legend>
	<table>
	  <xsl:if test="count(linked-desks/desk) &gt; 0">
	  <tr>
	    <td colspan="2"><b>This account is also linked to space(s): </b>
		<xsl:for-each select="linked-desks/desk">
		<a>
		  <xsl:attribute name="href">/1g/space/edit_desk_info.cgi?desk_id=<xsl:value-of select="id"/>&#38;mode=view</xsl:attribute>
		  #<xsl:value-of select="assigned-number"/>
		</a>
		  <xsl:if test="position()!=last()">
		    <xsl:text>, </xsl:text>
		  </xsl:if>
		</xsl:for-each>
		</td>
	  </tr>
	  </xsl:if>
	  <tr>
	    <td>
	      Licensee Account:
	    </td>
	    <td>
	      <select name="account_id">
		<option value=""> ---[Unoccupied]--- </option>
		<xsl:for-each select="customer-list/customer">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		    <xsl:if test="id = /page/form/desk-info/account-id">
		      <xsl:attribute name="selected">selected</xsl:attribute>
		    </xsl:if>
		    <xsl:value-of select="name"/> (ID #<xsl:value-of select="id"/>)
		    <xsl:if test="is_active = 0">
		      (inactive)
		    </xsl:if>
		  </option>
		</xsl:for-each>
	      </select>
	    </td>
	  </tr>
      <tr>
	    <td>
	      Person:
	    </td>
	    <td>
	      <select name="cust_pers_id">
		<option value=""> ---[Not Specified]--- </option>
		<xsl:for-each select="person-list/person">
		  <option>
		    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		    <xsl:if test="id = /page/form/desk-info/cust-pers-id">
		      <xsl:attribute name="selected">selected</xsl:attribute>
		    </xsl:if>
		    <xsl:value-of select="name"/> (ACCT ID #<xsl:value-of select="cust_acct_id"/>)
		  </option>
		</xsl:for-each>
	      </select>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      Number of People:
	    </td>
	    <td>
	      <input type="text" name="num_people">
		<xsl:attribute name="value"><xsl:value-of select="desk-info/num-people"/></xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      Enclosed?:
	    </td>
	    <td>
	      <select name="is_enclosed">
		<option>
		  <xsl:attribute name="value">0</xsl:attribute>
		  <xsl:if test="/page/form/desk-info/is-enclosed = 0">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  false
		</option>
		<option>
		  <xsl:attribute name="value">1</xsl:attribute>
		  <xsl:if test="/page/form/desk-info/is-enclosed = 1">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  true
		</option>
	      </select>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      List Price:
	    </td>
	    <td>
	      $ <input type="text" name="list_price">
	      <xsl:attribute name="value"><xsl:value-of select="desk-info/list-price"/></xsl:attribute>
	    </input>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      Current Price:
	    </td>
	    <td>
	      $ <input type="text" name="current_price">
	      <xsl:attribute name="value"><xsl:value-of select="desk-info/current-price"/></xsl:attribute>
	    </input>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      Internet/Telephone Revenue:
	    </td>
	    <td>
	      $ <input type="text" name="internet_telephone_revenue">
	      <xsl:attribute name="value"><xsl:value-of select="desk-info/internet-telephone-revenue"/></xsl:attribute>
	    </input>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      Start Date:
	    </td>
	    <td>
	      <input type="text" name="start_date">
	        <xsl:attribute name="value"><xsl:value-of select="desk-info/start-date" /></xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      End Date:
	    </td>
	    <td>
	      <input type="text" name="end_date">
	        <xsl:attribute name="value"><xsl:value-of select="desk-info/end-date" /></xsl:attribute>
	      </input>
	    </td>
	  </tr>
	   <tr>
	    <td>
	      Increase Date:
	    </td>
	    <td>
	      <input type="text" name="increase_date">
	        <xsl:attribute name="value"><xsl:value-of select="desk-info/increase-date" /></xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <xsl:if test="desk-info/account-id">
	    <tr>
	      <td>
		Waiting Tenant:
	      </td>
	      <td>
		<select name="waiting_account">
		  <option value=""> ---[No one waiting for this spot]--- </option>
		  <xsl:for-each select="customer-list/customer">
		    <option>
		      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		      <xsl:if test="id = /page/form/desk-info/waiting-account">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="name"/> (ID #<xsl:value-of select="id"/>)
		      <xsl:if test="is_active = 0">
			(inactive)
		      </xsl:if>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	  </xsl:if>
	  <tr>
	    <td>
	      Note:
	    </td>
	    <td>
	      <input>
		<xsl:attribute name="type">text</xsl:attribute>
		<xsl:attribute name="name">note</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="desk-info/note"/></xsl:attribute>
	      </input>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      Desk Area (sf):
	    </td>
	    <td>
	      <xsl:value-of select="desk-info/desk_area"/>
	    </td>
	  </tr>
	  <tr>
	    <td>
	    </td>
	    <td>
	      <input type="submit" name="submitButton" value="Save"/>
	    </td>
	  </tr>
	</table>
      </fieldset>
    </form>
  </xsl:template>
  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

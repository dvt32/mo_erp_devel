#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use GD;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use transform_coordinate;
use main_exception_wrapper;
use init_login_standard;
use select_row;
use get_row_by_id_map;
use path_to_coordinates;
use bounding_box;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check floor_id parameter for validity.
  my $floor_id = $cgi->param("floor_id");
  my $hide_occupied = $cgi->param("hide_occupied") || 0;
  if ((!$floor_id) || ($floor_id !~ /^\d+$/)) {
    die "show_page: show_desks_bad_floor.html\n";
  }
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  if (!$floor) {
    die "show_page: show_desks_bad_floor.html\n";
  }
  # read desk rows from db.
  my %desk_by_id;
  my $sql_query = "select * from desk_area_vw where floor_id = ?";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%desk_by_id);
  # load accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # set palette type to true color.
  GD::Image->trueColor(1);
  # load background image of floor plan.
  my $gd_image = GD::Image->new("../resource/floor_id_$floor_id.png");
  my $image_width = $gd_image->width();
  my $image_height = $gd_image->height();
  my $image_aspect_ratio = $image_width/$image_height;
  my @floor_bounding_polygon_coordinate_list = path_to_coordinates($floor->{"bounding_polygon"});
  if (!@floor_bounding_polygon_coordinate_list) {
    die "show_page: show_desks_bad_floor_bounding_polygon.html\n";
  }
  my @floor_bounding_box = bounding_box(@floor_bounding_polygon_coordinate_list);
  # assumes lower left is (0,0).
  my ($floor_plan_width, $floor_plan_height) = ($floor_bounding_box[1]->[0], $floor_bounding_box[1]->[1]);
  my $model_aspect_ratio = $floor_plan_width/$floor_plan_height;
  if (($model_aspect_ratio/$image_aspect_ratio > 1.01) || ($model_aspect_ratio/$image_aspect_ratio < 0.99)) {
    die "show_page: show_desks_aspect_ratio_mismatch.html\n";
  }
  # determine scaling factor (raster image size divided by model drawing size).
  my $scaling_factor = $image_width/$floor_plan_width;
  my $red = $gd_image->colorAllocate(255,0,0);
  my $green = $gd_image->colorAllocate(0,255,0);
  my $blue = $gd_image->colorAllocate(0,0,255);
  my $yellow = $gd_image->colorAllocate(255,255,0);
  my $black = $gd_image->colorAllocate(0,0,0);
  my $orange_red = $gd_image->colorAllocate(255, 69, 0);

  my $desk_map_office_color_unoccupied_now = $gd_image->colorAllocate({{ desk_map_office_unoccupied_now_color }});
  my $desk_map_office_color_unoccupied_1_mo = $gd_image->colorAllocate({{ desk_map_office_unoccupied_1_mo_color }});
  my $desk_map_office_color_unoccupied_2_mo = $gd_image->colorAllocate({{ desk_map_office_unoccupied_2_mo_color }});
  my $desk_map_office_color_unoccupied_3_mo = $gd_image->colorAllocate({{ desk_map_office_unoccupied_3_mo_color }});
  my $desk_map_office_color_occupied_month_to_month = $gd_image->colorAllocate({{ desk_map_office_occupied_mtm_color }});
  my $desk_map_office_color_occupied = $gd_image->colorAllocate({{ desk_map_office_occupied_color }});

  my $contrast_color_unoccupied_now = $gd_image->colorAllocate({{ desk_map_office_numbering_color_unoccupied_now }});
  my $contrast_color_unoccupied_1_mo = $gd_image->colorAllocate({{ desk_map_office_numbering_color_unoccupied_1_mo }});
  my $contrast_color_unoccupied_2_mo = $gd_image->colorAllocate({{ desk_map_office_numbering_color_unoccupied_2_mo }});
  my $contrast_color_unoccupied_3_mo = $gd_image->colorAllocate({{ desk_map_office_numbering_color_unoccupied_3_mo }});
  my $contrast_color_occupied_month_to_month = $gd_image->colorAllocate({{ desk_map_office_numbering_color_occupied_mtm }});
  my $contrast_color_occupied = $gd_image->colorAllocate({{ desk_map_office_numbering_color_occupied }});
  my $numbering_color = $red;

  for my $desk_id (keys(%desk_by_id)) {
    my $desk = $desk_by_id{$desk_id};
    if (!exists($desk->{"is_active"}) || $desk->("is_active")) {
      my @vertex_list = path_to_coordinates($desk->{"bounding_polygon"});
      my @bounding_box = bounding_box(@vertex_list);
      my $gd_poly = GD::Polygon->new();
      for my $vertex (@vertex_list) {
	my ($scaled_x, $scaled_y) = (int($vertex->[0]*$scaling_factor), int($vertex->[1]*$scaling_factor));
	$gd_poly->addPt(transform_coordinate($scaled_x, $scaled_y, $image_width, $image_height));
      }

      if (!$hide_occupied) {
	my $unoccupied_now = "SELECT 1 FROM desk_price_tbl WHERE desk_id = ? AND current_date BETWEEN start_date AND end_date AND is_active;";
	my $unoccupied_1_month = "SELECT 1 FROM desk_price_tbl WHERE desk_id = ? AND date_trunc('month', current_date + interval '1 month') BETWEEN start_date AND end_date AND is_active;";
	my $unoccupied_2_month = "SELECT 1 FROM desk_price_tbl WHERE desk_id = ? AND date_trunc('month', current_date + interval '2 month') BETWEEN start_date AND end_date AND is_active;";
	my $unoccupied_3_month = "SELECT 1 FROM desk_price_tbl WHERE desk_id = ? AND date_trunc('month', current_date + interval '3 month') BETWEEN start_date AND end_date AND is_active;";
	my $occupied_month_to_month = "SELECT 1 FROM desk_price_tbl WHERE desk_id = ? AND current_date BETWEEN start_date AND end_date AND is_monthly AND is_active;";

	if (!is_desk_occupied_by_query($dbh, $unoccupied_now, $desk_id)) {
	  # Desk is currently unoccupied
	  $gd_image->setStyle($desk_map_office_color_unoccupied_now);
	  $numbering_color = $contrast_color_unoccupied_now;
	} elsif (!is_desk_occupied_by_query($dbh, $unoccupied_1_month, $desk_id)) {
	  # Desk will be unoccupied by 1st of next month
	  $gd_image->setStyle($desk_map_office_color_unoccupied_1_mo);
	  $numbering_color = $contrast_color_unoccupied_1_mo;
	} elsif (!is_desk_occupied_by_query($dbh, $unoccupied_2_month, $desk_id)) {
	  # Desk will be unoccupied by 1st of 2nd month from now
	  $gd_image->setStyle($desk_map_office_color_unoccupied_2_mo);
	  $numbering_color = $contrast_color_unoccupied_2_mo;
	} elsif (!is_desk_occupied_by_query($dbh, $unoccupied_3_month, $desk_id)) {
	  # Desk will be unoccupied by 1st of 3rd month from now
	  $gd_image->setStyle($desk_map_office_color_unoccupied_3_mo);
	  $numbering_color = $contrast_color_unoccupied_3_mo;
	} elsif (is_desk_occupied_by_query($dbh, $occupied_month_to_month, $desk_id)) {
	  # Desk is occupied but is month to month
	  # if statement is reversed here (check for existence rather than lack of)
	  $gd_image->setStyle($desk_map_office_color_occupied_month_to_month);
	  $numbering_color = $contrast_color_occupied_month_to_month;
	} else {
	  # Desk is occupied for 3 months
	  $gd_image->setStyle($desk_map_office_color_occupied);
	  $numbering_color = $contrast_color_occupied;
	}
	# Fill in the polygon
	$gd_image->filledPolygon($gd_poly, gdStyled);
      }
      # outline the polygon with black.
      $gd_image->openPolygon($gd_poly, $black);
      # determine which vertex is closest to upper left.
      my @vertex_list_sorted_by_distance = sort {
	($a->[0]**2 + ($floor_plan_height - $a->[1])**2) <=> ($b->[0]**2 + ($floor_plan_height - $b->[1])**2);
      } @vertex_list;
      my $vertex_nearest_upper_left = $vertex_list_sorted_by_distance[0];
      my $bounding_box_scaled_width = int(($bounding_box[1]->[0] - $bounding_box[0]->[0]) * $scaling_factor);
      my $bounding_box_scaled_height = int(($bounding_box[1]->[1] - $bounding_box[0]->[1]) * $scaling_factor);
      my ($text_x, $text_y) = (int($vertex_nearest_upper_left->[0]*$scaling_factor), int($vertex_nearest_upper_left->[1]*$scaling_factor));
      # print desk assigned number in appropriate space on drawing.
      if ($bounding_box_scaled_width > 50) {
	$text_x += int($bounding_box_scaled_width/2 - 16);
	$text_y -= int($bounding_box_scaled_height/4 - 8);
	($text_x, $text_y) = transform_coordinate($text_x, $text_y, $image_width, $image_height);
	$gd_image->string(gdGiantFont, $text_x, $text_y, $desk->{"assigned_number"}, $numbering_color);
      } elsif ($bounding_box_scaled_width < 25) {
	$text_x += 2;
	$text_y -= 2;
	($text_x, $text_y) = transform_coordinate($text_x, $text_y, $image_width, $image_height);
	$gd_image->string(gdSmallFont, $text_x, $text_y, $desk->{"assigned_number"}, $numbering_color);
      } else {
	$text_x += 4;
	$text_y -= 4;
	($text_x, $text_y) = transform_coordinate($text_x, $text_y, $image_width, $image_height);
	$gd_image->string(gdGiantFont, $text_x, $text_y, $desk->{"assigned_number"}, $numbering_color);
      }
    }
  }
  # print out image data to stdout.
  print $cgi->header("image/png");
  print $gd_image->png();
}

sub is_desk_occupied_by_query($$) {
  # returns 1 if the desk if occupied on specified query or 0 if not occupied
  my ($dbh, $sql_query, $desk_id) = @_;
  my @sql_param_list = ([$desk_id, SQL_INTEGER]);
  my $desk_price_row = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if ($desk_price_row) {
    return 1;
  }
  return 0;
}

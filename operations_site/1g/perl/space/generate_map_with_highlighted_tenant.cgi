#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use GD;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use transform_coordinate;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use select_row;
use path_to_coordinates;
use bounding_box;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check account_id parameter for validity.
  my $account_id = $cgi->param("account_id");
  if ((!$account_id) || ($account_id !~ /^\d+$/)) {
    die "show_page: show_desks_bad_account.html\n";
  }
  my $account = select_row($dbh, "account_vw", $account_id);
  if (!$account) {
    die "show_page: show_desks_bad_account.html\n";
  }
  # get primary floor of account.
  my $floor_id = $account->{"primary_floor_id"};
  if (!$floor_id) {
    die "show_page: show_desks_no_primary_floor.html\n";
  }
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  if (!$floor) {
    die "show_page: show_desks_bad_floor.html\n";
  }
  # get desks for full floor map.
  my %desk_by_id;
  my $sql_query = "select * from desk_vw where floor_id = ?";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%desk_by_id);
  # set palette type to true color.
  GD::Image->trueColor(1);
  # load background image of floor plan.
  my $gd_image = GD::Image->new("../resource/floor_id_$floor_id.png");
  my $image_width = $gd_image->width();
  my $image_height = $gd_image->height();
  my $image_aspect_ratio = $image_width/$image_height;
  my @floor_bounding_polygon_coordinate_list = path_to_coordinates($floor->{"bounding_polygon"});
  if (!@floor_bounding_polygon_coordinate_list) {
    die "show_page: show_desks_bad_floor_bounding_polygon.html\n";
  }
  my @floor_bounding_box = bounding_box(@floor_bounding_polygon_coordinate_list);
  # assumes lower left is (0,0).
  my ($floor_plan_width, $floor_plan_height) = ($floor_bounding_box[1]->[0], $floor_bounding_box[1]->[1]);
  my $model_aspect_ratio = $floor_plan_width/$floor_plan_height;
  if (($model_aspect_ratio/$image_aspect_ratio > 1.01) || ($model_aspect_ratio/$image_aspect_ratio < 0.99)) {
    die "show_page: show_desks_aspect_ratio_mismatch.html\n";
  }
  # determine scaling factor (raster image size divided by model drawing size).
  my $scaling_factor = $image_width/$floor_plan_width;
  my $red = $gd_image->colorAllocate(255,0,0);
  my $green = $gd_image->colorAllocate(0,255,0);
  my $blue = $gd_image->colorAllocate(0,0,255);
  my $yellow = $gd_image->colorAllocate(255,255,0);
  my $black = $gd_image->colorAllocate(0,0,0);
  $gd_image->setStyle($yellow,gdTransparent);
  for my $desk_id (keys(%desk_by_id)) {
    my $desk = $desk_by_id{$desk_id};
    if ($desk->{"is_active"}) {
      my @vertex_list = path_to_coordinates($desk->{"bounding_polygon"});
      my @bounding_box = bounding_box(@vertex_list);
      my $gd_poly = GD::Polygon->new();
      for my $vertex (@vertex_list) {
	my ($scaled_x, $scaled_y) = (int($vertex->[0]*$scaling_factor), int($vertex->[1]*$scaling_factor));
	$gd_poly->addPt(transform_coordinate($scaled_x, $scaled_y, $image_width, $image_height));
      }
      # fill the polygon with dotted yellow, if occupied.
      if (defined($desk->{"account_id"})) {
	if ($desk->{"account_id"} == $account_id) {
	  $gd_image->setStyle($red,gdTransparent);
	  $gd_image->filledPolygon($gd_poly, gdStyled);
	  $gd_image->setStyle($yellow,gdTransparent);
	} else {
	  $gd_image->filledPolygon($gd_poly, gdStyled);
	}
      }
      # outline the polygon with black.
      $gd_image->openPolygon($gd_poly, $black);
      # determine which vertex is closest to upper left.
      my @vertex_list_sorted_by_distance = sort {
	($a->[0]**2 + ($floor_plan_height - $a->[1])**2) <=> ($b->[0]**2 + ($floor_plan_height - $b->[1])**2);
      } @vertex_list;
      my $vertex_nearest_upper_left = $vertex_list_sorted_by_distance[0];
      my $bounding_box_scaled_width = int(($bounding_box[1]->[0] - $bounding_box[0]->[0]) * $scaling_factor);
      my $bounding_box_scaled_height = int(($bounding_box[1]->[1] - $bounding_box[0]->[1]) * $scaling_factor);
      my ($text_x, $text_y) = (int($vertex_nearest_upper_left->[0]*$scaling_factor), int($vertex_nearest_upper_left->[1]*$scaling_factor));
      # print desk assigned number in appropriate space on drawing.
      if ($bounding_box_scaled_width > 50) {
	$text_x += int($bounding_box_scaled_width/2 - 16);
	$text_y -= int($bounding_box_scaled_height/4 - 8);
	($text_x, $text_y) = transform_coordinate($text_x, $text_y, $image_width, $image_height);
	$gd_image->string(gdGiantFont, $text_x, $text_y, $desk->{"assigned_number"}, $red);
      } elsif ($bounding_box_scaled_width < 25) {
	$text_x += 2;
	$text_y -= 2;
	($text_x, $text_y) = transform_coordinate($text_x, $text_y, $image_width, $image_height);
	$gd_image->string(gdSmallFont, $text_x, $text_y, $desk->{"assigned_number"}, $red);
      } else {
	$text_x += 4;
	$text_y -= 4;
	($text_x, $text_y) = transform_coordinate($text_x, $text_y, $image_width, $image_height);
	$gd_image->string(gdGiantFont, $text_x, $text_y, $desk->{"assigned_number"}, $red);
      }
    }
  }
  # print out image data to stdout.
  print $cgi->header("image/png");
  print $gd_image->png();
}

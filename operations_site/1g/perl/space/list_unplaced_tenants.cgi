#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load active accounts.
  my (%account_by_id, @account_id_list);
  my $sql_query = "select id, name from cust_acct_tbl where is_active and service_id = 1 order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id, \@account_id_list);
  # load occupied desks.
  my (%desk_by_id, @desk_id_list);
  my $sql_query = "select distinct account_id, id from desk_tbl where account_id is not null;";
  get_row_by_id_map($dbh, $sql_query, undef, \%desk_by_id, \@desk_id_list);
	my %desks_by_account_id;
	for my $desk_id (@desk_id_list) {
		my $account_id = $desk_by_id{$desk_id}->{"account_id"};
		$desks_by_account_id{$account_id} = $desk_id;
	}
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
---END_BLOCK---;
  for my $account_id (@account_id_list) {
		if (!exists($desks_by_account_id{$account_id})) {
			my $account_name = HTML::Entities::encode($account_by_id{$account_id}->{"name"});
			$generated_xml .= <<"---END_BLOCK---;";
	<account>
		<id>$account_id</id>	
		<name>$account_name</name>
	</account>
---END_BLOCK---;
		}
	}
  $generated_xml .= <<"---END_BLOCK---;";
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "list_unplaced_tenants.xslt", $cgi);
}

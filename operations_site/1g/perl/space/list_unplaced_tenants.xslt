<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: Edit Desk Information
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
				<p>
					<a href="/1g/index.cgi">Main menu</a>
		| Desk Maps:
		<a href="/1g/space/show_desks.cgi?floor_id=1">36W20F3</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=2">1375BwyF3</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=3">1375BwyF11</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=4">122W27F10</a>
		&middot;
		<a href="/1g/space/show_desks.cgi?floor_id=5">122W27F12</a>
				</p>
				<ul>
				<xsl:apply-templates select="account"/>
				</ul>
				<p>
					<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
				</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account">
		<li>
			<a>
				<xsl:attribute name="href">/1g/tenant/edit_account_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
				<xsl:value-of select="name"/>
			</a>
		</li>
	</xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

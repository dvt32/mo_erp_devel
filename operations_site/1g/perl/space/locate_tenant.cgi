#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # load active accounts.
  my (%active_account_by_id, @active_account_id_list);
  my $sql_query = "select * from account_vw where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%active_account_by_id, \@active_account_id_list);
  # load active persons.
  my (%active_person_by_id, %active_persons_by_account_id);
  my $sql_query = "select * from person_vw where is_active order by last_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%active_person_by_id);
  for my $person (values(%active_person_by_id)) {
    push(@{$active_persons_by_account_id{$person->{"account_id"}}}, $person);
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <active-account-list>
---END_BLOCK---;
  for my $id (@active_account_id_list) {
    my $active_account = $active_account_by_id{$id};
    my $name = HTML::Entities::encode($active_account->{"name"});
    my $primary_contact_person_id = $active_account->{"primary_contact_person_id"};
    my $primary_contact = $active_person_by_id{$primary_contact_person_id};
    my $primary_contact_name = ($primary_contact->{"first_name"} || "") . " " . ($primary_contact->{"last_name"} || ""); 
    $primary_contact_name = HTML::Entities::encode($primary_contact_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <active-account>
      <id>$id</id>
      <name>$name</name>
      <contact-name>$primary_contact_name</contact-name>
    </active-account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </active-account-list>
	<accounts-sorted-by-primary-contact>
---END_BLOCK---;
	# also present the list in order by primary contact, if desired.
	my @sorted_account_id_list = sort { 
		lc($active_person_by_id{$active_account_by_id{$a}->{"primary_contact_person_id"}}->{"last_name"}) cmp 
		lc($active_person_by_id{$active_account_by_id{$b}->{"primary_contact_person_id"}}->{"last_name"})
	} @active_account_id_list;
  for my $id (@sorted_account_id_list) {
    my $active_account = $active_account_by_id{$id};
    my $name = HTML::Entities::encode($active_account->{"name"});
    my $primary_contact_person_id = $active_account->{"primary_contact_person_id"};
    my $primary_contact = $active_person_by_id{$primary_contact_person_id};
    my $primary_contact_name = ($primary_contact->{"first_name"} || "") . " " . ($primary_contact->{"last_name"} || ""); 
    $primary_contact_name = HTML::Entities::encode($primary_contact_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <active-account>
      <id>$id</id>
      <name>$name</name>
      <contact-name>$primary_contact_name</contact-name>
    </active-account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
	</accounts-sorted-by-primary-contact>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "locate_tenant.xslt", $cgi);
}

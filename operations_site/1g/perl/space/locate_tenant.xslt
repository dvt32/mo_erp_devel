<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: Locate Tenant On Desk Map 
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
				<script type="text/javascript" src="/1g/js/edit_option_list.js"> /* this is necessary for the declaration of JS with our Perl HTML processor */ var dummy_variable; </script>
				<script type="text/javascript">
					function order_by_contact() {
						clear_option_list(document.forms['locate_tenant'].account_id); 
						<xsl:for-each select="/page/accounts-sorted-by-primary-contact/active-account">
						add_to_option_list(document.forms['locate_tenant'].account_id, "<xsl:value-of select="contact-name"/>, <xsl:value-of select="name"/>",  '<xsl:value-of select="id"/>');
						</xsl:for-each>
					}
				</script>
      </head>
      <body>
				<p>
					<a href="/1g/index.cgi">Main menu</a> | <a href="/1g/tenant/show_active_accounts.cgi">Account list</a> | <a href="/1g/space/show_desks.html">Full Desk Maps</a>
				</p>
				<form action="/1g/space/locate_tenant_input.cgi" method="post" name="locate_tenant">
					<table cellspacing="0" cellpadding="10px" border="0" class="standard_table" style="white-space: nowrap;">
						<tr>
							<td class="standard_table_title" colspan="2">
								<xsl:text>Select Account to Locate</xsl:text>
							</td>
						</tr>
						<tr>
							<td valign="top" colspan="2">Account:<br />
								<select>
									<xsl:attribute name="name">
										<xsl:text>account_id</xsl:text>
									</xsl:attribute>
									<xsl:for-each select="/page/active-account-list/active-account">
										<option>
											<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
											<xsl:value-of select="name"/><xsl:text>, </xsl:text><xsl:value-of select="contact-name"/>
										</option>
									</xsl:for-each>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<input type="submit" name="submitButton" value="Submit"/>
							</td>
							<td>
								<p style="text-align: right;">[<a href="javascript:order_by_contact()">Order list by primary contact</a>]</p>
							</td>
						</tr>
					</table>
				</form>
				<p>
					<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
				</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use GD;
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use transform_coordinate;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use select_row;
use path_to_coordinates;
use bounding_box;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
	# check account_id parameter for validity.
	my $account_id = $cgi->param("account_id");
  if ((!$account_id) || ($account_id !~ /^\d+$/)) {
    die "show_page: show_desks_bad_account.html\n";
  }
	my $account = select_row($dbh, "account_vw", $account_id);
	if (!$account) {
		die "show_page: show_desks_bad_account.html\n";
	}
	my $account_name = $account->{"name"};
	if ($account->{"type"} eq "customer") {
		my $customer = select_row($dbh, "cust_acct_tbl", $account_id);
		if (!$customer) {
			die "show_page: show_desks_bad_account.html\n";
		}
		if ($customer->{"service_id"} == 4) {
			die "show_page: show_desks_virtual.html\n";
		}
	}
	# get primary floor of account.
	my $floor_id = $account->{"primary_floor_id"};
	if (!$floor_id) {
		die "show_page: show_desks_no_primary_floor.html\n";
	}
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  if (!$floor) {
    die "show_page: show_desks_bad_floor.html\n";
  }
	my $floor_name = $floor->{"short_global_label"};
	# get desks for full floor map.
	my %desk_by_id;
	my $sql_query = "select * from desk_tbl where floor_id = ?";
	my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%desk_by_id);
  # load background image of floor plan.
  my $gd_image = GD::Image->new("../resource/floor_id_$floor_id.png");
  my $image_width = $gd_image->width();
  my $image_height = $gd_image->height();
  my $image_aspect_ratio = $image_width/$image_height;
  my @floor_bounding_polygon_coordinate_list = path_to_coordinates($floor->{"bounding_polygon"});
  if (!@floor_bounding_polygon_coordinate_list) {
    die "show_page: show_desks_bad_floor_bounding_polygon.html\n";
  }
  my @floor_bounding_box = bounding_box(@floor_bounding_polygon_coordinate_list);
  # assumes lower left is (0,0).
  my ($floor_plan_width, $floor_plan_height) = ($floor_bounding_box[1]->[0], $floor_bounding_box[1]->[1]);
  my $model_aspect_ratio = $floor_plan_width/$floor_plan_height;
  if (($model_aspect_ratio/$image_aspect_ratio > 1.01) || ($model_aspect_ratio/$image_aspect_ratio < 0.99)) {
    die "show_page: show_desks_aspect_ratio_mismatch.html\n";
  }
  # determine scaling factor (raster image size divided by model drawing size).
  my $scaling_factor = $image_width/$floor_plan_width;
  $account_name = HTML::Entities::encode($account_name);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
	<account-id>$account_id</account-id>
	<account-name>$account_name</account-name>
	<floor-id>$floor_id</floor-id>
	<floor-name>$floor_name</floor-name>
---END_BLOCK---;
  for my $desk_id (keys(%desk_by_id)) {
    my $desk = $desk_by_id{$desk_id};
		if ($desk->{"is_active"}) {
			my @vertex_list = path_to_coordinates($desk->{"bounding_polygon"});
			my @bounding_box = bounding_box(@vertex_list);
			my $polygon_origin_x;
			my $polygon_origin_y;
			$generated_xml .= <<"---END_BLOCK---;";
		<poly>
			<desk-id>$desk_id</desk-id>
			<coords>
---END_BLOCK---;
			for my $vertex (@vertex_list) {
				my ($scaled_x, $scaled_y) = (int($vertex->[0]*$scaling_factor), int($vertex->[1]*$scaling_factor));
				($scaled_x, $scaled_y) = transform_coordinate($scaled_x, $scaled_y, $image_width, $image_height);
			$generated_xml .= <<"---END_BLOCK---;";
				$scaled_x, $scaled_y,
---END_BLOCK---;
				if ($vertex == @vertex_list[0]) {
					$polygon_origin_x = $scaled_x;
					$polygon_origin_y = $scaled_y;
				}
			}
			$generated_xml .= <<"---END_BLOCK---;";
				$polygon_origin_x,$polygon_origin_y
			</coords>
		</poly>
---END_BLOCK---;
		}
	}
  $generated_xml .= <<"---END_BLOCK---;";
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_tenant.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use GD;
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use read_file;
use select_row;
use select_row_with_query;
use get_row_by_id_map;
use init_login_standard;
use apply_xslt_output_xhtml;
use transform_coordinate;
use main_exception_wrapper;
use path_to_coordinates;
use bounding_box;
use get_date_now;
use timestamp_to_components;
use local_datetime_to_epoch_time;
use currency_format;
use get_php_base_url;

main_exception_wrapper(\&main);

sub main() {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check floor_id parameter for validity.
  my $floor_id = $cgi->param("floor_id");
  my $hide_occupied = $cgi->param("hide_occupied") || 0;
  if ((!$floor_id) || ($floor_id !~ /^\d+$/)) {
    die "show_page: show_desks_bad_floor.html\n";
  }
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  if (!$floor) {
    die "show_page: show_desks_bad_floor.html\n";
  }
  my $floor_name = $floor->{"short_global_label"};
  # read virtual desk rows from db.
  my (%vo_desk_by_id, @vo_desk_id_list);
  my $sql_query = "select * from essensys_virtual_license_vw where building_id in (select building_id from floor_tbl where id = ?) order by account_name";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%vo_desk_by_id, \@vo_desk_id_list);
  # read desk rows from db.
  my (%desk_by_id, @desk_id_list);
  my $sql_query = "select * from desk_vw where floor_id = ? order by assigned_number";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%desk_by_id, \@desk_id_list);
  # get occupied and total desk area
  my $sql_query = "select sum(desk_area) from desk_area_vw where id in (select id from desk_vw where is_active and floor_id = ?";
  my $total_desk_sf = (select_row_with_query($dbh, "select sum(desk_area) from desk_area_vw where id in (select id from desk_vw where is_active and floor_id = ?)", [$floor_id]))->{"sum"};
  my $occupied_desk_sf = (select_row_with_query($dbh, "select sum(desk_area) from desk_area_vw where id in (select id from desk_vw where is_active and floor_id = ? and account_id is not null)", [$floor_id]))->{"sum"};
  # read desk area.
  my (%desk_area_by_id);
  my $sql_query = "select * from desk_area_vw where floor_id = ?";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%desk_area_by_id);
  # read account rows from db.
  my %account_by_id;
  my $sql_query = "select id,name from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # load background image of floor plan.
  my $gd_image = GD::Image->new("{{ operations_basedir }}/1g/resource/floor_id_$floor_id.png");
  my $image_width = $gd_image->width();
  my $image_height = $gd_image->height();
  my $image_aspect_ratio = $image_width/$image_height;
  my @floor_bounding_polygon_coordinate_list = path_to_coordinates($floor->{"bounding_polygon"});
  if (!@floor_bounding_polygon_coordinate_list) {
    die "show_page: show_desks_bad_floor_bounding_polygon.html\n";
  }
  my @floor_bounding_box = bounding_box(@floor_bounding_polygon_coordinate_list);
  # assumes lower left is (0,0).
  my ($floor_plan_width, $floor_plan_height) = ($floor_bounding_box[1]->[0], $floor_bounding_box[1]->[1]);
  my $model_aspect_ratio = $floor_plan_width/$floor_plan_height;
  if (($model_aspect_ratio/$image_aspect_ratio > 1.01) || ($model_aspect_ratio/$image_aspect_ratio < 0.99)) {
    die "show_page: show_desks_aspect_ratio_mismatch.html\n";
  }
  # determine scaling factor (raster image size divided by model drawing size).
  my $scaling_factor = $image_width/$floor_plan_width;
  # php base url for template
  my $php_base_url = get_php_base_url();
  my $essensys_base_url = "{{ essensys_base_url }}";
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <floor-id>$floor_id</floor-id>
  <floor-name>$floor_name</floor-name>
  <hide_occupied>$hide_occupied</hide_occupied>
---END_BLOCK---;
  for my $desk_id (keys(%desk_by_id)) {
    my $desk = $desk_by_id{$desk_id};
    if ($desk->{"is_active"}) {
      my @vertex_list = path_to_coordinates($desk->{"bounding_polygon"});
      my @bounding_box = bounding_box(@vertex_list);
      my $polygon_origin_x;
      my $polygon_origin_y;
      $generated_xml .= <<"---END_BLOCK---;";
    <poly>
      <desk-id>$desk_id</desk-id>
      <coords>
---END_BLOCK---;
      for my $vertex (@vertex_list) {
        my ($scaled_x, $scaled_y) = (int($vertex->[0]*$scaling_factor), int($vertex->[1]*$scaling_factor));
        ($scaled_x, $scaled_y) = transform_coordinate($scaled_x, $scaled_y, $image_width, $image_height);
	$generated_xml .= <<"---END_BLOCK---;";
        $scaled_x, $scaled_y,
---END_BLOCK---;
        if ($vertex == @vertex_list[0]) {
          $polygon_origin_x = $scaled_x;
          $polygon_origin_y = $scaled_y;
        }
      }
      $generated_xml .= <<"---END_BLOCK---;";
        $polygon_origin_x,$polygon_origin_y
      </coords>
    </poly>
---END_BLOCK---;
    }
  }
  for my $vo_desk_id (@vo_desk_id_list) {
    my $vo_desk = $vo_desk_by_id{$vo_desk_id};
    my ($account_id, $account_name);
    $account_id = $vo_desk->{"account_id"};
    $account_name = $vo_desk->{"account_name"};
    $account_name =~ s/&(?!amp;)/&amp;/gi;
    $generated_xml .= <<"---END_BLOCK---;";
    <vo-desk-info>
      <account-id>$account_id</account-id>
      <essensys-base-url>$essensys_base_url</essensys-base-url>
      <account-name>$account_name</account-name>
    </vo-desk-info>
---END_BLOCK---;
  }
  for my $desk_id (@desk_id_list) {
    my $desk = $desk_by_id{$desk_id};
    my $desk_area = $desk_area_by_id{$desk_id};
    if ($desk->{"is_active"}) {
      my ($number, $account_id, $name);
      $number = $desk->{"assigned_number"};
      $account_id = $desk->{"account_id"};
      if ($account_id) {
        $name = HTML::Entities::encode($account_by_id{$account_id}->{"name"});
      } else {
        $name = "Currently unoccupied";
      }
      my $desk_area_int = int($desk_area->{"desk_area"} + 0.5);
      $generated_xml .= <<"---END_BLOCK---;";
    <desk-info>
      <id>$desk_id</id>
      <php-base-url>$php_base_url</php-base-url>
      <number>$number</number>
      <name>$name</name>
      <desk_area>$desk_area_int</desk_area>
    </desk-info>
---END_BLOCK---;
    }
  }

  $generated_xml .= <<"---END_BLOCK---;";
	<financial-analysis>
---END_BLOCK---;
  my $sql_query = "SELECT SUM(current_price) AS total_current_rev, SUM(list_price) AS potential_rev, SUM(current_price) / SUM(list_price) * 100 AS pct_rev , SUM(list_price) - SUM(current_price) AS lost_rev, SUM(internet_revenue) AS internet_rev, SUM(phone_revenue) AS telephone_rev FROM desk_vw WHERE floor_id = ?";
  my $financial_analysis = select_row_with_query($dbh, $sql_query, [$floor_id]);
  my $current_revenue = $financial_analysis->{"total_current_rev"};
  my $potential_revenue = $financial_analysis->{"potential_rev"};
  my $revenue_lost = $financial_analysis->{"lost_rev"};
  my $pct_revenue = $financial_analysis->{"pct_rev"};
  my $internet_revenue = $financial_analysis->{"internet_rev"};
  my $telephone_revenue = $financial_analysis->{"telephone_rev"};
  # New business in the past 30/60 days
  my $new_business1 = (select_row_with_query($dbh, "SELECT SUM(price) FROM desk_price_tbl WHERE is_active AND start_date BETWEEN current_date - interval '30 days' AND current_date AND desk_id IN (SELECT id FROM desk_tbl WHERE floor_id = ?)", [$floor_id]))->{"sum"};
  my $new_business2 = (select_row_with_query($dbh, "SELECT SUM(price) FROM desk_price_tbl WHERE is_active AND start_date BETWEEN current_date - interval '60 days' AND current_date AND desk_id IN (SELECT id FROM desk_tbl WHERE floor_id = ?)", [$floor_id]))->{"sum"};
  # New business since the first of the month
  my $new_business_query = "SELECT SUM(price) FROM desk_price_tbl WHERE is_active AND start_date BETWEEN date_trunc('month', current_date)::DATE AND current_date AND desk_id IN (SELECT id FROM desk_tbl WHERE floor_id = ?)";
  my $new_business3 = (select_row_with_query($dbh, $new_business_query, [$floor_id]))->{"sum"};
  # New business since the first of the previous month
  my $new_business_query = "SELECT SUM(price) FROM desk_price_tbl WHERE is_active AND start_date BETWEEN date_trunc('month', current_date - interval '1 month')::DATE AND current_date AND desk_id IN (SELECT id FROM desk_tbl WHERE floor_id = ?)";
  my $new_business4 = (select_row_with_query($dbh, $new_business_query, [$floor_id]))->{"sum"};

  # Area calculations
  my $unoccupied_desk_sf = int($total_desk_sf - $occupied_desk_sf);
  $total_desk_sf = int($total_desk_sf);
  $occupied_desk_sf = int($occupied_desk_sf);

  # Format data
  my $pct_occupied_sf = sprintf("%.2f%%", $occupied_desk_sf * 100 / $total_desk_sf);
  $pct_revenue = sprintf("%.2f%%", $pct_revenue);
  $current_revenue = currency_format($current_revenue);
  $potential_revenue = currency_format($potential_revenue);
  $revenue_lost = currency_format($revenue_lost);
  $internet_revenue = currency_format($internet_revenue);
  $telephone_revenue = currency_format($telephone_revenue);
  $new_business1 = currency_format($new_business1);
  $new_business2 = currency_format($new_business2);
  $new_business3 = currency_format($new_business3);
  $new_business4 = currency_format($new_business4);

  $generated_xml .= <<"---END_BLOCK---;";
    <current-revenue>$current_revenue</current-revenue>
    <potential-revenue>$potential_revenue</potential-revenue>
    <pct-revenue>$pct_revenue</pct-revenue>
    <revenue-lost>$revenue_lost</revenue-lost>
    <internet-revenue>$internet_revenue</internet-revenue>
    <telephone-revenue>$telephone_revenue</telephone-revenue>
    <new-business1>$new_business1</new-business1>
    <new-business2>$new_business2</new-business2>
    <new-business3>$new_business3</new-business3>
    <new-business4>$new_business4</new-business4>
    <occupied-sf>$occupied_desk_sf</occupied-sf>
    <unoccupied-sf>$unoccupied_desk_sf</unoccupied-sf>
    <total-sf>$total_desk_sf</total-sf>
    <pct-occupied>$pct_occupied_sf</pct-occupied>
  </financial-analysis>

---END_BLOCK---;
  my @desk_map_color_array = ('{{ desk_map_office_occupied_color }}',
			      '{{ desk_map_office_occupied_mtm_color }}',
			      '{{ desk_map_office_unoccupied_now_color }}',
			      '{{ desk_map_office_unoccupied_1_mo_color }}',
			      '{{ desk_map_office_unoccupied_2_mo_color }}',
			      '{{ desk_map_office_unoccupied_3_mo_color }}');

  # format each color into a 6 digits
  foreach my $color (@desk_map_color_array) {
    $color =~ s/\s+//g;
    my @split_color = split(',',$color);
    # convert from base 10 to hex, # is for HTML
    $color = sprintf("#%2.2X%2.2X%2.2X",@split_color[0],@split_color[1],@split_color[2])
  }

  $generated_xml .= <<"---END_BLOCK---;";
  <desk-map-legend>
    <desk-map-color-occupied>$desk_map_color_array[0]</desk-map-color-occupied>
    <desk-map-color-occupied-mtm>$desk_map_color_array[1]</desk-map-color-occupied-mtm>
    <desk-map-color-unoccupied-now>$desk_map_color_array[2]</desk-map-color-unoccupied-now>
    <desk-map-color-unoccupied-1-mo>$desk_map_color_array[3]</desk-map-color-unoccupied-1-mo>
    <desk-map-color-unoccupied-2-mo>$desk_map_color_array[4]</desk-map-color-unoccupied-2-mo>
    <desk-map-color-unoccupied-3-mo>$desk_map_color_array[5]</desk-map-color-unoccupied-3-mo>
  </desk-map-legend>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_desks.xslt", $cgi);
}

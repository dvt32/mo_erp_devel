<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: <xsl:value-of select="floor-name"/> Desk Map
	</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><xsl:comment></xsl:comment></script>
	<script type="text/javascript" src="/1g/js/show_desks.js"><xsl:comment></xsl:comment></script>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a href="/1g/tenant/show_active_accounts.cgi">Show Active Accounts</a> |
	  <xsl:choose>
	    <xsl:when test="hide_occupied=1">
	      <a><xsl:attribute name="href">/1g/space/show_desks.cgi?floor_id=<xsl:value-of select="floor-id"/></xsl:attribute>Toggle Hide Occupied</a> |
	    </xsl:when>
	    <xsl:otherwise>
	      <a><xsl:attribute name="href">/1g/space/show_desks.cgi?floor_id=<xsl:value-of select="floor-id"/>&amp;hide_occupied=1</xsl:attribute>Toggle Hide Occupied</a> |
	    </xsl:otherwise>
	  </xsl:choose>
	  <a>
	    <xsl:attribute name="href">
	      javascript: window.open('generate_map.cgi?floor_id=<xsl:value-of select="floor-id"/>&amp;hide_occupied=<xsl:value-of select="hide_occupied"/>').print()
	    </xsl:attribute>
	    Print Desk Map</a> |
	  <b><xsl:value-of select="floor-name"/></b>
	</p>
	<p>
	  Desk Maps:
	  <a href="/1g/space/show_desks.cgi?floor_id=4">122W27F10</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=10">902BwyF6</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=12">902BwyF7</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=32">535FifthF16</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=33">68HarrisonF6</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=18">18SMichFl12</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=43">1EWackerF8</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=34">95CCF16</a>
	  &middot;
	  <a href="/1g/space/show_desks.cgi?floor_id=44">95CCF16-QTS-area-3</a>
	</p>
	<img>
	  <xsl:attribute name="src">generate_map.cgi?floor_id=<xsl:value-of select="floor-id"/>&amp;hide_occupied=<xsl:value-of select="hide_occupied"/></xsl:attribute>
	  <xsl:attribute name="usemap">#desk_map</xsl:attribute>
	  <xsl:attribute name="border">0</xsl:attribute>
	</img>
	<map name="desk_map">
	  <xsl:apply-templates select="poly"/>
	</map>
	<div>
	  <div style="float:left;width:35%">
	    <b>Desk Info:</b>
	    <ul>
	      <xsl:apply-templates select="desk-info"/>
	    </ul>
	  </div>
	  <div style="float:left;width:35%">
	    <b>Virtual Desks</b>
	    (<a href="/201605_flask/essensys/_update_licenses">Resync with Operate</a>)
	    <ul>
	      <xsl:apply-templates select="vo-desk-info"/>
	    </ul>
	  </div>
	  <div style="float:left;width:30%">
	    <xsl:apply-templates select="desk-map-legend"/>
	    <xsl:apply-templates select="financial-analysis"/>
	  </div>
	  <div style="clear:left;" />
	</div>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="poly">
    <area>
      <xsl:attribute name="shape">poly</xsl:attribute>
      <xsl:attribute name="coords">
	<xsl:value-of select="coords"/>
      </xsl:attribute>
      <xsl:attribute name="href">/201605_flask/space/edit_desk_info/<xsl:value-of select="desk-id"/></xsl:attribute>
    </area>
  </xsl:template>

  <xsl:template match="vo-desk-info">
    <li>
      <a>
	<xsl:attribute name="target">_blank</xsl:attribute>
	<xsl:attribute name="href">
	  <xsl:value-of select="essensys-base-url"/><xsl:value-of select="account-id"/>
	</xsl:attribute>
	<xsl:value-of select="account-name"/>
      </a>
    </li>
  </xsl:template>

  <xsl:template match="desk-info">
    <li>
        <a>
        <xsl:attribute name="href">/201605_flask/space/edit_desk_info/<xsl:value-of select="id"/></xsl:attribute>
        Desk #<xsl:value-of select="number"/>
        </a>
        :
    <xsl:choose>
      <xsl:when test="name != 'Currently unoccupied'">
	<a>
	  <xsl:attribute name="href">/201605_flask/space/edit_desk_info/<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:value-of select="name"/>
	</a>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="name"/>
      </xsl:otherwise>
    </xsl:choose>
    (sf: <xsl:value-of select="desk_area"/>)
    </li>
  </xsl:template>
  <xsl:template match="desk-map-legend">
    <table>
      <tr>
	<td colspan="2" align="center"><b>Desk Map Legend</b></td>
      </tr>
      <tr>
	<td width="250" align="right">Currently Available</td>
	<td width="10%">
	  <xsl:attribute name="bgcolor"><xsl:value-of select="desk-map-color-unoccupied-now"/></xsl:attribute>
	</td>
      </tr>
      <tr>
	<td id="legend-1-mo" align="right">Availability within next months</td>
	<td>
	  <xsl:attribute name="bgcolor"><xsl:value-of select="desk-map-color-unoccupied-1-mo"/></xsl:attribute>
	</td>
      </tr>
      <tr>
	<td id="legend-2-mo" align="right">Availability within next months</td>
	<td>
	  <xsl:attribute name="bgcolor"><xsl:value-of select="desk-map-color-unoccupied-2-mo"/></xsl:attribute>
	</td>
      </tr>
      <tr>
	<td id="legend-3-mo" align="right">Availability within 3 months</td>
	<td>
	  <xsl:attribute name="bgcolor"><xsl:value-of select="desk-map-color-unoccupied-3-mo"/></xsl:attribute>
	</td>
      </tr>
      <tr>
	<td align="right">Occupied (Month to Month)</td>
	<td>
	  <xsl:attribute name="bgcolor"><xsl:value-of select="desk-map-color-occupied-mtm"/></xsl:attribute>
	</td>
      </tr>
      <tr>
	<td align="right">Occupied-Active Tenant</td>
	<td>
	  <xsl:attribute name="bgcolor"><xsl:value-of select="desk-map-color-occupied"/></xsl:attribute>
	</td>
      </tr>
    </table>
  </xsl:template>
  <xsl:template match="financial-analysis">
  	<table>
  	  <tr>
  	    <td colspan="2" align="center"><b>Financial Analysis for <xsl:value-of select="../floor-name"/></b></td>
  	  </tr>
  	  <tr>
  	    <td width="250" align="right">Total Current Revenue:</td>
  	    <td align="right"><xsl:value-of select="current-revenue"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">Potential Revenue:</td>
  	    <td align="right"><xsl:value-of select="potential-revenue"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">Percent Revenue:</td>
  	    <td align="right"><xsl:value-of select="pct-revenue"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">Revenue Lost:</td>
  	    <td align="right"><xsl:value-of select="revenue-lost"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">Internet Revenue:</td>
  	    <td align="right"><xsl:value-of select="internet-revenue"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">Telephone Revenue:</td>
  	    <td align="right"><xsl:value-of select="telephone-revenue"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">New business in the past 30 days:</td>
  	    <td align="right"><xsl:value-of select="new-business1"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">New business in the past 60 days:</td>
  	    <td align="right"><xsl:value-of select="new-business2"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">New business since the 1st of this Month:</td>
  	    <td align="right"><xsl:value-of select="new-business3"/></td>
  	  </tr>
  	  <tr>
  	    <td align="right">New business since the 1st of last Month:</td>
  	    <td align="right"><xsl:value-of select="new-business4"/></td>
  	  </tr>
	  <tr>
  	    <td align="right">Total Desk Area (sf.):</td>
  	    <td align="right"><xsl:value-of select="total-sf"/></td>
  	  </tr>
	  <tr>
  	    <td align="right">Occupied Area (sf.):</td>
  	    <td align="right"><xsl:value-of select="occupied-sf"/></td>
  	  </tr>
	  <tr>
  	    <td align="right">Percent Occupied:</td>
  	    <td align="right"><xsl:value-of select="pct-occupied"/></td>
  	  </tr>
	  <tr>
  	    <td align="right">Unoccupied Area (sf.):</td>
  	    <td align="right"><xsl:value-of select="unoccupied-sf"/></td>
  	  </tr>

  	</table>
  </xsl:template>
  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

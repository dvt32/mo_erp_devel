<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: <xsl:value-of select="floor-name"/> Desk Map 
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
				<p>
					<a href="/1g/index.cgi">Main menu</a> |
					<a href="/1g/tenant/show_active_accounts.cgi">Show Active Accounts</a> |
					<b><xsl:value-of select="floor-name"/></b> &gt; 
					<xsl:value-of select="account-name"/>
				</p>
				<img>
					<xsl:attribute name="src">generate_map_with_highlighted_tenant.cgi?account_id=<xsl:value-of select="account-id"/></xsl:attribute>
					<xsl:attribute name="usemap">#desk_map</xsl:attribute>
					<xsl:attribute name="border">0</xsl:attribute>
				</img>
				<map name="desk_map">
				<xsl:apply-templates select="poly"/>
				</map>
				<p>
					<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
				</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="poly">
		<area>
			<xsl:attribute name="shape">poly</xsl:attribute>
			<xsl:attribute name="coords">
				<xsl:value-of select="coords"/>
			</xsl:attribute>
			<xsl:attribute name="href">edit_desk_info.cgi?desk_id=<xsl:value-of select="desk-id"/></xsl:attribute>
		</area>
	</xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>


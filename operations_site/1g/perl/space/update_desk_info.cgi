#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use lead_pad;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_row_by_id_map;
use select_row_with_query;
use main_exception_wrapper;
use update_row;
use insert_row;
use Date::Calc qw(check_date);
use get_date_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $desk_id = Encode::decode($cgi_input_charset, $cgi->param("desk_id"));
  my $account_id = Encode::decode($cgi_input_charset, $cgi->param("account_id"));
  my $cust_pers_id = Encode::decode($cgi_input_charset, $cgi->param("cust_pers_id")|| undef);
  my $num_people = Encode::decode($cgi_input_charset, $cgi->param("num_people"));
  my $is_enclosed = Encode::decode($cgi_input_charset, $cgi->param("is_enclosed"));
  my $list_price = Encode::decode($cgi_input_charset, $cgi->param("list_price"));
  my $current_price = Encode::decode($cgi_input_charset, $cgi->param("current_price"));
  my $internet_telephone_revenue = Encode::decode($cgi_input_charset, $cgi->param("internet_telephone_revenue"));
  my $waiting_account = Encode::decode($cgi_input_charset, $cgi->param("waiting_account"));
	my $start_date = Encode::decode($cgi_input_charset, $cgi->param("start_date") || undef);
	my $note = Encode::decode($cgi_input_charset, $cgi->param("note") || "");
	my $end_date = Encode::decode($cgi_input_charset, $cgi->param("end_date") || undef);
	my $increase_date = Encode::decode($cgi_input_charset, $cgi->param("increase_date") || undef);
	
	# error checking for the desk_id
  if (!$desk_id || $desk_id !~ /\d{1,4}/ ) {
		die "show_page: edit_desk_bad_id.html";
  }
  
  # check for current info on this desk_id 
  my $sql_query = "select * from desk_tbl where id = ?";
  my @sql_param_list = ([$desk_id, SQL_INTEGER]);
  my $desk_info = select_row_with_query($dbh, $sql_query, \@sql_param_list);
 	my $floor_id = $desk_info->{"floor_id"};
  my $assigned_number = $desk_info->{"assigned_number"};
  
  # there isn't any entry for that desk_id
  if (!$desk_info) {
    die "show_page: edit_desk_problem.html\n";
  }
  
  # verify the leasing account and waiting accoutn field are active accounts
  my (%account_by_id, $current_account, $waiting_account_name);
  my $sql_query = "select * from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  
  if($account_id) {
  	die "show_page: update_desk_inactive_leasing_account.html\n" if (!$account_by_id{$account_id}->{"is_active"});
  }
  if($waiting_account) {
  	die "show_page: update_desk_inactive_waiting_account.html\n" if (!$account_by_id{$waiting_account}->{"is_active"});
  } else {
  	undef $waiting_account;
  }
  
  # verify that the cust_pers is a member of cust_acct
  if($cust_pers_id){
    my (%cust_pers_by_id, $pers, $pers2);
    my $sql_query = "select cust_acct_id from cust_pers_tbl where id = ?";
    my @sql_param_list = ([$cust_pers_id, SQL_INTEGER]);
    my $cust_pers_by_id = select_row_with_query($dbh, $sql_query, \@sql_param_list);
    if($cust_pers_by_id->{"cust_acct_id"}!=$account_id){
        die "show_page: update_desk_acct_mismatch.html\n";
    }
  }
  
	# validate the start date
	if ($account_id) {
		die "show_page: update_desk_bad_current_price.html\n" if ($current_price !~ /\d+(\.\d{2})?/ or $current_price <= 0);
		$start_date = validate_date($start_date, 'update_desk_start_date_bad_date.html');
		
		if($end_date){
			# validate the end date
			$end_date = validate_date($end_date, 'update_desk_end_date_bad_date.html');
			
		  my @now = get_date_now(1);
		
		  # make sure the end date is greater than now
		  #die "show_page: update_desk_end_date_bad_date.html\n" if (compare_dates($end_date,"$now[0]-$now[1]-$now[2]")<1);
	
		  # make sure the end date is greater than the start date
		  die "show_page: update_desk_end_date_bad_date.html\n" if (compare_dates($end_date,$start_date)<1);
		} 
		
		if($increase_date){
			# validate the increase date
			$increase_date = validate_date($increase_date, 'update_desk_end_date_bad_date.html');
		}
		
		if ($list_price !~ /\d+(\.\d{2})?/) {
			$list_price = 0;
		}
		if ($current_price !~ /\d+(\.\d{2})?/) {
			$current_price = 0;
		}
		if($internet_telephone_revenue !~ /\d+(\.\d{2})?/) {
			$internet_telephone_revenue = 0;
		}
	} else {
		$account_id = undef;
		$current_price = 0;
		$internet_telephone_revenue = 0;
		$start_date = undef;
		$end_date = undef;
		$increase_date = undef;
		$note = undef;
        $cust_pers_id = undef;
	}
	
	if ($num_people !~ /\d+/ ) {
		$num_people = 0;
	}
  
  # issue the update command to the database.
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id]);
  push(@sql_column_list, ["cust_pers_id", $cust_pers_id]);
  push(@sql_column_list, ["num_people", $num_people]);
  push(@sql_column_list, ["is_enclosed", $is_enclosed]);
  push(@sql_column_list, ["list_price", $list_price]);
  push(@sql_column_list, ["current_price", $current_price]);
  push(@sql_column_list, ["internet_telephone_revenue", $internet_telephone_revenue]);
  push(@sql_column_list, ["start_date", $start_date]);
  push(@sql_column_list, ["end_date", $end_date]);
  push(@sql_column_list, ["increase_date", $increase_date]);
  push(@sql_column_list, ["waiting_account", $waiting_account]);
  push(@sql_column_list, ["note", $note]);
  update_row($dbh, "desk_tbl", \@sql_column_list, $desk_id);
	# get current account name, if account is currently associated with this desk
	if ($account_id && $account_id != 0) {
		$current_account = HTML::Entities::encode($account_by_id{$account_id}->{"name"});
	}
	if ($account_id && $account_id == 0) {
		$current_account = "(currently unoccupied)";
	}
	if ($waiting_account && $waiting_account != 0) {
		$waiting_account_name = HTML::Entities::encode($account_by_id{$waiting_account}->{"name"});
	}
	if ($waiting_account && $waiting_account == 0) {
		$waiting_account_name = "(no one waiting for this space)";
	}
  
    # Save new tenants to the desk assignment history table
    #$dbh->begin_work();
    if($account_id){
    my $sql_query = "select * from desk_assignment_history_tbl where desk_id = ? order by start_date desc limit 1;";
    my @sql_param_list = ([$desk_id, SQL_INTEGER]);
    my $last_desk_history_record = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  
    my (@sql_column_list);
    push(@sql_column_list, ["desk_id", $desk_id, SQL_INTEGER]);
    push(@sql_column_list, ["cust_acct_id", $account_id, SQL_INTEGER]);
    push(@sql_column_list, ["cust_pers_id", $cust_pers_id, SQL_INTEGER]);
    push(@sql_column_list, ["start_date", $start_date]);
    push(@sql_column_list, ["end_date", $end_date]);
    push(@sql_column_list, ["current_price", $current_price]);
    push(@sql_column_list, ["note", $note]);
    
    if($last_desk_history_record){
        if($last_desk_history_record->{"cust_acct_id"}==$account_id && $last_desk_history_record->{"start_date"} eq $start_date){
            # "this record exists, lets update it";
            my $sql_statement = "update desk_assignment_history_tbl 
                set cust_acct_id = ?, cust_pers_id = ?, end_date = ?, note = ?, current_price = ?
                where desk_id = ? and start_date = ?";
            my $sth = $dbh->prepare($sql_statement);
            my $response = $sth->execute($account_id, $cust_pers_id, $end_date, $note, $current_price, $desk_id, $start_date);
        }else{
            # "looks like we've changed tenants, lets insert a new record.";            
            insert_row($dbh, "desk_assignment_history_tbl", \@sql_column_list);
        }
    }else{
        # "no record, lets insert";
        insert_row($dbh, "desk_assignment_history_tbl", \@sql_column_list);
    }
   }
  # commit the transaction.
  #$dbh->commit();
 
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
	<floor-id>$floor_id</floor-id>
	<desk-info>
		<desk-id>$desk_id</desk-id>
		<assigned-number>$assigned_number</assigned-number>
		<num-people>$num_people</num-people>
		<is-enclosed>$is_enclosed</is-enclosed>
		<list-price>$list_price</list-price>
		<current-price>$current_price</current-price>
		<internet-telephone-revenue>$internet_telephone_revenue</internet-telephone-revenue>
---END_BLOCK---;
	if ($account_id) {
    $generated_xml .= <<"---END_BLOCK---;";
		<account-id>$account_id</account-id>
		<current-account>$current_account</current-account>
---END_BLOCK---;
		if ($waiting_account_name) {
			$generated_xml .= <<"---END_BLOCK---;";
		<waiting-account>$waiting_account_name</waiting-account>
---END_BLOCK---;
		}	
	}
	if ($start_date) {
    $generated_xml .= <<"---END_BLOCK---;";
		<start-date>$start_date</start-date>
---END_BLOCK---;
	}
	if ($end_date) {
    $generated_xml .= <<"---END_BLOCK---;";
		<end-date>$end_date</end-date>
---END_BLOCK---;
	}
	if ($increase_date) {
    $generated_xml .= <<"---END_BLOCK---;";
		<increase-date>$increase_date</increase-date>
---END_BLOCK---;
	}
	if ($note) {
    $generated_xml .= <<"---END_BLOCK---;";
		<note>$note</note>
---END_BLOCK---;
	}
	$generated_xml .= <<"---END_BLOCK---;";
	</desk-info>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "update_desk_info_success.xslt", $cgi);
}

sub validate_date($$){
	my ($date,$error_pg) = @_;
	if ($date =~ m!^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.]((?:19|20)\d\d)$!){
		if (check_date($3,$1,$2)){
			$date = "$3-$1-$2";
		} else {
			die "show_page: $error_pg\n";
		}
	} else {
		die "show_page: $error_pg\n";
	}
	return $date;
}

sub compare_dates($$){
	# (greater than "1", less than "-1", or equal to "0")
	my ($dt1, $dt2) = @_;
	
	my @date1;
	my @date2;
	my $limit = 0;
	
	push(@date1, split(/-/, $dt1));
	push(@date2, split(/-/, $dt2));
	
	# compare up to the lesser number of elements
	# (like if one datetime only has a date and no time, don't try to compare time)
	if(@date1 == @date2) { $limit = @date1 }
	elsif (@date1 > @date2) { $limit = @date2 }
	elsif (@date1 < @date2) { $limit = @date1 }
	
	for (my $i = 0; $i < $limit; $i++) {
		if ($date1[$i] > $date2[$i]) { return 1; last; }# date1 greater than date2
		if ($date1[$i] < $date2[$i]) { return -1; last; }# date1 less than date2
	}
	return 0;# dates are equal
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: Edit Desk Information
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
				<p>
					<a href="/1g/index.cgi">Main menu</a> |
					<a>
						<xsl:attribute name="href">/1g/space/show_desks.cgi?floor_id=<xsl:value-of select="floor-id"/></xsl:attribute>
						Back to Desk Map</a> |
					<a href="/1g/tenant/show_active_accounts.cgi">Show Active Accounts</a>
				</p>
				<xsl:apply-templates select="desk-info"/>
				<p>
					<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
				</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="desk-info">
		<p>Updated information for desk number <xsl:value-of select="assigned-number"/></p>
		
		<xsl:if test="current-account">
			<p>Current leasing account: <xsl:value-of select="current-account"/></p>
		</xsl:if>

		<p>Number of people: <xsl:value-of select="num-people"/></p>
		<p>Enclosed?: 
		<xsl:if test="is-enclosed = 0">No</xsl:if>
		<xsl:if test="is-enclosed = 1">Yes</xsl:if></p>
		<p>List price: <xsl:value-of select="list-price"/></p>
		<p>Current price: <xsl:value-of select="current-price"/></p>
		<p>Internet/Telephone Revenue: <xsl:value-of select="internet-telephone-revenue"/></p>
		
		<xsl:if test="start-date">
		<p>Start date: <xsl:value-of select="start-date"/></p>
		</xsl:if>
		
		<xsl:if test="end-date">
		<p>End date: <xsl:value-of select="end-date"/></p>
		</xsl:if>
		
		<xsl:if test="increase-date">
		<p>Increase date: <xsl:value-of select="increase-date"/></p>
		</xsl:if>
		
		<xsl:if test="waiting-account">
			<p>Waiting account: <xsl:value-of select="waiting-account"/></p>
		</xsl:if>

		<xsl:if test="note">
			<p>Notes: <xsl:value-of select="note"/></p>
		</xsl:if>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>


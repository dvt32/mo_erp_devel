<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: View Desk Information
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a>
	    <xsl:attribute name="href">/1g/space/show_desks.cgi?floor_id=<xsl:value-of select="floor-id"/></xsl:attribute>
	    Back to Desk Map</a> |
	    <a href="/1g/tenant/show_active_accounts.cgi">Show Active Accounts</a>
	</p>
	<xsl:apply-templates select="form"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="form">
	Information for Desk Number <xsl:value-of select="desk-info/assigned-number"/>
	<table cellspacing="0" cellpadding="4px" border="0">
	  <xsl:if test="count(linked-desks/desk) &gt; 0">
	  <tr>
	    <td colspan="2"><b>This account is also linked to space(s): </b>
		<xsl:for-each select="linked-desks/desk">
		<a>
		  <xsl:attribute name="href">/1g/space/edit_desk_info.cgi?desk_id=<xsl:value-of select="id"/>&#38;mode=view</xsl:attribute>
		  #<xsl:value-of select="assigned-number"/>
		</a>
		  <xsl:if test="position()!=last()">
		    <xsl:text>, </xsl:text>
		  </xsl:if>
		</xsl:for-each>
		</td>
	  </tr>
	  </xsl:if>
	  <tr>
	    <td class="map_table_key">
	      Leasing Account:
	    </td>
	    <td class="map_table_value">
		<xsl:choose>
		  <xsl:when test="/page/form/desk-info/account-id">
		    <xsl:for-each select="customer-list/customer">
		      <xsl:if test="id = /page/form/desk-info/account-id">
		        <xsl:value-of select="name"/> (ID #<xsl:value-of select="id"/>)
		      </xsl:if>
		    </xsl:for-each>
		  </xsl:when>
		  <xsl:otherwise>
		    [Unoccupied]
		  </xsl:otherwise>
		</xsl:choose>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Number of People:
	    </td>
	    <td class="map_table_value"><xsl:value-of select="desk-info/num-people"/></td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Enclosed?:
	    </td>
	    <td class="map_table_value">
		  <xsl:if test="/page/form/desk-info/is-enclosed = 0">
		    no
		  </xsl:if>
		  <xsl:if test="/page/form/desk-info/is-enclosed = 1">
		    yes
		  </xsl:if>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key"> 
	      List Price:
	    </td>
	    <td class="map_table_value">
	      $ <xsl:value-of select="desk-info/list-price"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Current Price:
	    </td>
	    <td class="map_table_value">
	      $ <xsl:value-of select="desk-info/current-price"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Internet/Telephone Revenue:
	    </td>
	    <td class="map_table_value">
	      $ <xsl:value-of select="desk-info/internet-telephone-revenue"/>
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      Start Date:
	    </td>
	    <td class="map_table_value">
	      <xsl:value-of select="desk-info/start-date" />
	    </td>
	  </tr>
	  <tr>
	    <td class="map_table_key">
	      End Date:
	    </td>
	    <td class="map_table_value">
	        <xsl:value-of select="desk-info/end-date" />
	    </td>
	  </tr>
	   <tr>
	    <td class="map_table_key">
	      Increase Date:
	    </td>
	    <td class="map_table_value">
	        <xsl:value-of select="desk-info/increase-date" />
	    </td>
	  </tr>
	  <xsl:if test="desk-info/account-id">
	    <tr>
	      <td class="map_table_key">
		Waiting Tenant:
	      </td>
	      <td class="map_table_value">
		  <xsl:choose>
		  <xsl:when test="/page/form/desk-info/waiting-account">
		    <xsl:for-each select="customer-list/customer">
		      <xsl:if test="id = /page/form/desk-info/waiting-account">
		        <xsl:value-of select="name"/> (ID #<xsl:value-of select="id"/>)
		      </xsl:if>
		    </xsl:for-each>
		  </xsl:when>
		  <xsl:otherwise>
		    [No one waiting for this spot]
		  </xsl:otherwise>
		</xsl:choose>
	      </td>
	    </tr>
	  </xsl:if>
	  <tr>
	    <td class="map_table_key">
	      Note:
	    </td>
	    <td class="map_table_value">
		  <xsl:value-of select="desk-info/note"/>
	    </td>
	  </tr>
	</table>
  </xsl:template>
  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>


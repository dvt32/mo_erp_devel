<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Change Log
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>The log of changes is shown below in reverse chronological order (most recent changes first).</p>
	<xsl:apply-templates select="change_log"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="change_log">
    <ul>
      <li>
	2009-07-16: show inactive tenants in edit desk page. show inactive tenants as orange red on map in show desks page.
      </li>
      <li>
	2009-06-09: added upcoming guests link under Customers group.
      </li>
      <li>
	2009-06-09: added guest list report.
      </li>
      <li>
	2009-02-16: added batch add guest function.
      </li>
      <li>
	2009-02-15: added new/edit guest function.
      </li>
      <li>
	2009-01-27: added delete expense function.
      </li>
      <li>
	2009-01-18: added show/edit web pages and show/edit inbound links functions to Sales/Marketing category.
      </li>
      <li>
	2009-01-09: edit phone number can now change type, device type, device name, port, and floor.
      </li>
      <li>
	2009-01-08: added rankings by keywords, of owned domains, and of watched domains under Sales/Marketing category.
      </li>
      <li>
	2008-12-29: added monitor keywords function under Sales/Marketing category.
      </li>
      <li>
	2008-12-15: consolidated credit/debit card transactions under Finance category. Added recurring credit/debit card transaction management.
      </li>
      <li>
	2008-11-27: added HR :: Review Absences.
      </li>
      <li>
	2008-11-27: added owned domain names.
      </li>
      <li>
	2008-11-25: added watched domain names.
      </li>
      <li>
	2008-11-23: added marketing call source, marketing call details, edit marketing call details pages.
      </li>
      <li>
	2008-10-31: added account search, person search, account detail, person detail pages.
      </li>
      <li>
	2008-08-25: added vacation, sick, unpaid day management under Human Resources group.
      </li>
      <li>
	2008-07-22: added batch delete function to Claimed Documents page under Communication and Imaging group.
      </li>
      <li>
	2008-07-18: added archived footage under Physical Resources :: Cameras. Also consolidated live footage into single page.
      </li>
      <li>
	2008-07-06: added expense tracking under Finance section.
      </li>
      <li>
	2008-06-21: added private fax number feature.
      </li>
      <li>
	2008-05-26: consolidated Scan and Fax functions (unclaimed scans, unclaimed faxes, and now working with claimed documents).
      </li>
      <li>
	2008-05-25: added Scan functions (unclaimed scans, claimed scans, send scan as fax, claim scan, unclaim scan, delete scan).
      </li>
    </ul>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

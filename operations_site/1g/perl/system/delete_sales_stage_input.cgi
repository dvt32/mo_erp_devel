#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if sales stage id is specified.
  my $sales_stage_id = $cgi->param("id");
  if ((!defined($sales_stage_id)) || ($sales_stage_id !~ /^\d+$/)) {
    die "show_page: delete_sales_stage_not_found.html\n";
  }
  # load sales stage and check that it exists.
  my $sales_stage = select_row($dbh, "enum_sales_stage_tbl", $sales_stage_id);
  if (!$sales_stage) {
    die "show_page: delete_sales_stage_not_found.html\n";
  }
  # populate information needed for display of page.
  my $name = HTML::Entities::encode($sales_stage->{"name"});
  my $notes = HTML::Entities::encode($sales_stage->{"notes"} || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sales-stage>
    <id>$sales_stage_id</id>
    <name>$name</name>
    <notes>$notes</notes>
  </sales-stage>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_sales_stage_input.xslt", $cgi);
}

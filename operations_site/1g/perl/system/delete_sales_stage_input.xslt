<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Delete Sales Stage
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a href="/1g/system/show_sales_stages.cgi">Show sales stages</a>
	</p>
	<xsl:apply-templates select="sales-stage"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sales-stage">
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td colspan="2" class="standard_table_title">
          Delete Sales Stage
	</td>
      </tr>
      <tr>
	<td class="map_table_key" style="white-space: nowrap">
	  Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="name"/>
	</td>
      </tr>
      <tr valign="top">
	<td class="map_table_key">
	  Notes:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="notes"/>
	</td>
      </tr>
    </table>
    <p>Are you sure you want to delete the sales stage?</p>
    <form action="/1g/system/delete_sales_stage_result.cgi" method="post">
      <input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">id</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
      </input>
      <input type="submit" name="submitButton" value="Delete"/>
    </form>
    <p>(If you do not want to delete the sales stage, click one of the navigation links at the top of the page)</p>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

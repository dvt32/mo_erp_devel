#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use delete_row;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  # verify that sales stage id was specified and exists.
  my $sales_stage_id = $cgi->param("id");
  if ((!defined($sales_stage_id)) || ($sales_stage_id !~ /^\d+$/)) {
    die "show_page: delete_sales_stage_not_found.html\n";
  }
  # begin transaction of deleting the row.
  $dbh->begin_work();
  # verify that the sales stage exists.
  my $sales_stage = select_row($dbh, "enum_sales_stage_tbl", $sales_stage_id);
  if (!$sales_stage) {
    $dbh->rollback();
    die "show_page: delete_sales_stage_not_found.html\n";
  }
  # issue the delete command to the database.
  delete_row($dbh, "enum_sales_stage_tbl", $sales_stage_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $name = HTML::Entities::encode($sales_stage->{"name"} || "");
  my $notes = HTML::Entities::encode($sales_stage->{"notes"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sales-stage>
    <id>$sales_stage_id</id>
    <name>$name</name>
    <notes>$notes</notes>
  </sales-stage>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_sales_stage_result_success.xslt", $cgi);
}

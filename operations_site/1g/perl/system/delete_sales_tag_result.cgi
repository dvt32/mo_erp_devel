#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use delete_row;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  # verify that sales tag id was specified and exists.
  my $sales_tag_id = $cgi->param("id");
  if ((!defined($sales_tag_id)) || ($sales_tag_id !~ /^\d+$/)) {
    die "show_page: delete_sales_tag_not_found.html\n";
  }
  # begin transaction of deleting the row.
  $dbh->begin_work();
  # verify that the sales tag exists.
  my $sales_tag = select_row($dbh, "enum_sales_tag_tbl", $sales_tag_id);
  if (!$sales_tag) {
    $dbh->rollback();
    die "show_page: delete_sales_tag_not_found.html\n";
  }
  # issue the delete command to the database.
  delete_row($dbh, "enum_sales_tag_tbl", $sales_tag_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $name = HTML::Entities::encode($sales_tag->{"name"} || "");
  my $notes = HTML::Entities::encode($sales_tag->{"notes"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sales-tag>
    <id>$sales_tag_id</id>
    <name>$name</name>
    <notes>$notes</notes>
  </sales-tag>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_sales_tag_result_success.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Sales Stage
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a>
	  |
	  <a>
	    <xsl:attribute name="href">/1g/system/delete_sales_stage_input.cgi?id=<xsl:value-of select="/page/sales-stage/id"/></xsl:attribute>
	    <xsl:text>Delete sales stage</xsl:text>
	  </a>
	</p>
	<xsl:apply-templates select="sales-stage"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sales-stage">
    <table cellspacing="0" cellpadding="4px" border="0" class="standard_table" style="white-space: nowrap;">
      <tr>
	<td class="standard_table_title">
          Edit Sales Stage
	</td>
      </tr>
      <tr><td>
	<form action="/1g/system/edit_sales_stage_result.cgi" method="post">
	  <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	  </input>
	  <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	    <tr>
	      <td style="padding: 4px; ">
		Stage Name:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">name</xsl:attribute>
		  <xsl:attribute name="value"><xsl:value-of select="name"/></xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr valign="top">
	      <td style="padding: 4px; ">
		Notes:
	      </td>
	      <td>
		<input>
		  <xsl:attribute name="type">text</xsl:attribute>
		  <xsl:attribute name="name">notes</xsl:attribute>
		  <xsl:attribute name="size">80</xsl:attribute>
		  <xsl:attribute name="value"><xsl:value-of select="notes"/></xsl:attribute>
		</input>
	      </td>
	    </tr>
	    <tr>
	      <td style="padding: 4px; ">
	      </td>
	      <td>
		<input type="submit" name="submitButton" value="Save"/>
	      </td>
	    </tr>
	  </table>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

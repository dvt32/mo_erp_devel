#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use update_row;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $name = Encode::decode($cgi_input_charset, $cgi->param("name"));
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes"));
  # verify that sales tag id was specified and exists.
  my $sales_tag_id = $cgi->param("id");
  if ((!defined($sales_tag_id)) || ($sales_tag_id !~ /^\d+$/)) {
    die "show_page: edit_sales_tag_not_found.html\n";
  }
  # verify existence of name.
  if (!$name) {
    die "show_page: edit_sales_tag_missing_name.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the sales tag exists.
  my $sales_tag = select_row($dbh, "enum_sales_tag_tbl", $sales_tag_id);
  if (!$sales_tag) {
    $dbh->rollback();
    die "show_page: edit_prospect_not_found.html\n";
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["name", Encode::encode($db_charset, $name)]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  update_row($dbh, "enum_sales_tag_tbl", \@sql_column_list, $sales_tag_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $name = HTML::Entities::encode($name);
  $notes = HTML::Entities::encode($notes || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <sales-tag>
    <id>$sales_tag_id</id>
    <name>$name</name>
    <notes>$notes</notes>
  </sales-tag>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_sales_tag_result_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use insert_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $name = Encode::decode($cgi_input_charset, $cgi->param("name"));
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes"));
  # verify existence of name.
  if (!$name) {
    die "show_page: edit_sales_stage_missing_name.html\n";
  }
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["name", Encode::encode($db_charset, $name)]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  insert_row($dbh, "enum_sales_stage_tbl", \@sql_column_list);
  # populate information needed for display of page.
  $name = HTML::Entities::encode($name);
  $notes = HTML::Entities::encode($notes || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <sales-stage>
    <name>$name</name>
    <notes>$notes</notes>
  </sales-stage>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_sales_stage_result_success.xslt", $cgi);
}

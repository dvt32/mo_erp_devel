#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch sales stages.
  my (%stage_by_id, @stage_id_list);
  my $sql_query = "select * from enum_sales_stage_tbl order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%stage_by_id, \@stage_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sales-stage-list>
---END_BLOCK---;
  for my $stage_id (@stage_id_list) {
    my $stage = $stage_by_id{$stage_id};
    my $name = HTML::Entities::encode($stage->{"name"});
    my $notes = HTML::Entities::encode($stage->{"notes"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-stage>
      <id>$stage_id</id>
      <name>$name</name>
      <notes>$notes</notes>
    </sales-stage>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </sales-stage-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_sales_stages.xslt", $cgi);
}

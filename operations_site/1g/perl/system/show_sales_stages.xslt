<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Sales Stages
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="sales-stage-list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sales-stage-list">
    <p>
      <a href="/1g/index.cgi">Main menu</a>
      |
      <a href="/1g/system/new_sales_stage_input.cgi">New Sales Stage</a>
    </p>
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="11" class="standard_table_title">
          Sales Stages
	</td>
      </tr>
      <xsl:choose>
	<xsl:when test="count(sales-stage) > 0">
	  <tr class="standard_table_column_heading_row">
	    <td class="standard_table_side_padding"></td>
	    <td>Stage Name</td>
	    <td class="standard_table_spacer_column"></td>
	    <td>Notes</td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</xsl:when>
	<xsl:otherwise>
	  <tr>
	    <td colspan="5" align="center">
	      No stages.
	    </td>
	  </tr>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="sales-stage"/>
    </table>
  </xsl:template>

  <xsl:template match="sales-stage">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">
	    <xsl:text>/1g/system/edit_sales_stage_input.cgi?id=</xsl:text>
	    <xsl:value-of select="id"/>
	  </xsl:attribute>
	  <xsl:value-of select="name"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="notes"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

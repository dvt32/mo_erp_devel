#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch sales tags.
  my (%tag_by_id, @tag_id_list);
  my $sql_query = "select * from enum_sales_tag_tbl order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%tag_by_id, \@tag_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sales-tag-list>
---END_BLOCK---;
  for my $tag_id (@tag_id_list) {
    my $tag = $tag_by_id{$tag_id};
    my $name = HTML::Entities::encode($tag->{"name"});
    my $notes = HTML::Entities::encode($tag->{"notes"});
    $generated_xml .= <<"---END_BLOCK---;";
    <sales-tag>
      <id>$tag_id</id>
      <name>$name</name>
      <notes>$notes</notes>
    </sales-tag>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </sales-tag-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_sales_tags.xslt", $cgi);
}

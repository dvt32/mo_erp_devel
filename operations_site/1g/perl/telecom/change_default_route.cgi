#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use LWP::UserAgent;
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use authorize_role;
use select_row;
use apply_xslt_output_xhtml;
use global_config; 

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = authorize_role($instance_map, "role:telecom/change_default_route");
  my $building_id = $cgi->param("building_id");
  my $building = select_row($dbh, "building_tbl", $building_id);
  if (!$building) {
    die "show_page: change_default_route_bad_building.html\n";
  }
  my $building_label = HTML::Entities::encode($building->{"short_global_label"});
  my $route = $cgi->param("route");
  my $user_agent = LWP::UserAgent->new();
  my $server;
  if ($building_id == 1) {
    if ($route eq "primary") {
      $server = global_config::lookup("telecom_server1");
    } elsif ($route eq "backup") {
      $server = global_config::lookup("telecom_server2");
    } else {
      die "show_page: change_default_route_bad_route.html\n";
    }
  } elsif ($building_id == 3) {
    if ($route eq "primary") {
      $server = global_config::lookup("telecom_server3");
    } elsif ($route eq "backup") {
      $server = global_config::lookup("telecom_server4");
    } else {
      die "show_page: change_default_route_bad_route.html\n";
    }
  } else {
    die "show_page: change_default_route_bad_building.html\n";
  }
  my $url = "http://$server/route/$route.cgi";
  my $response = $user_agent->get($url);
  my $response_content = $response->content();
  if ($response_content !~ /^OK$/) {
    die "show_page: change_default_route_failed_change.html\n";
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <status>
    <building_id>$building_id</building_id>
    <building_label>$building_label</building_label>
    <route>$route</route>
  </status>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "change_default_route.xslt", $cgi);
}

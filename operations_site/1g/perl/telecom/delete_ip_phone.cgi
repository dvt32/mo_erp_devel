#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that ip_phone id was specified.
  my $ip_phone_id = $cgi->param("id");
  if ((!defined($ip_phone_id)) || ($ip_phone_id !~ /^\d+$/)) {
    die "show_page: delete_ip_phone_not_found.html\n";
  }
  # connect to phone database.
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # begin transaction of deleting the row.
  $dbh_phone->begin_work();
  # verify that the ip_phone exists.
  my $ip_phone = select_row($dbh_phone, "ip_phone_tbl", $ip_phone_id);
  if (!$ip_phone) {
    $dbh_phone->rollback();
    die "show_page: delete_ip_phone_not_found.html\n";
  }
  delete_row($dbh_phone, "ip_phone_tbl", $ip_phone_id);
  # commit the transaction.
  $dbh_phone->commit();
  # populate information needed for display of page.
  my $manufacturer = HTML::Entities::encode($ip_phone->{"manufacturer"});
  my $model = HTML::Entities::encode($ip_phone->{"model"});
  my $mac_address = HTML::Entities::encode($ip_phone->{"mac_address"});
  my $ownership = HTML::Entities::encode($ip_phone->{"ownership"});
  my $note = HTML::Entities::encode($ip_phone->{"note"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ip_phone>
    <manufacturer>$manufacturer</manufacturer>
    <model>$model</model>
    <mac_address>$mac_address</mac_address>
    <ownership>$ownership</ownership>
    <note>$note</note>
  </ip_phone>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_ip_phone_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that ip address id is valid and try to load ip address.
  my ($ip_address_id, $ip_address) = ($cgi->param("id"), undef);
  if ((!defined($ip_address_id)) || ($ip_address_id !~ /^\d+$/) || (!($ip_address = select_row($dbh, "ip_address_tbl", $ip_address_id)))) {
    die "show_page: edit_ip_address_not_found.html\n";
  }
  my $building_id = $ip_address->{"building_id"};
  # fetch accounts.
  my %account_by_id;
  my $sql_query = "select * from account_vw where is_active and primary_floor_id in (select id from floor_tbl where building_id = ?)";
  my @sql_param_list = ([$building_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # if the account attached to the ip address is not active, load it too.
  if ((defined(my $account_id = $ip_address->{"account_id"})) && (!exists($account_by_id{$ip_address->{"account_id"}}))) {
    $account_by_id{$account_id} = select_row($dbh, "account_vw", $account_id);
  }
  # if the corporate account is not active, load it too.
  if (!$account_by_id{$account_id_of_corporate}) {
    $account_by_id{$account_id_of_corporate} = select_row($dbh, "account_vw", $account_id_of_corporate);
  }
  my @account_id_list = sort {
    lc($account_by_id{$a}->{"name"}) cmp lc($account_by_id{$b}->{"name"});
  } keys(%account_by_id);
  # get building.
  my $building = select_row($dbh, "building_tbl", $building_id);
  # populate information needed for display of page.
  my $building_label = HTML::Entities::encode($building->{"short_global_label"});
  my $account_id = $ip_address->{"account_id"} || "";
  my $address = HTML::Entities::encode($ip_address->{"ip_address"});
  my $hostname = HTML::Entities::encode($ip_address->{"hostname"} || "");
  my $note = HTML::Entities::encode($ip_address->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ip-address>
    <id>$ip_address_id</id>
    <building-id>$building_id</building-id>
    <building-label>$building_label</building-label>
    <account-id>$account_id</account-id>
    <address>$address</address>
    <hostname>$hostname</hostname>
    <note>$note</note>
  </ip-address>
  <account-list>
    <account>
      <id></id>
      <name>(unassigned)</name>
    </account>
---END_BLOCK---;
  for $account_id (@account_id_list) {
    my $account = $account_by_id{$account_id};
    my $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$account_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_ip_address_input.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

my $account_id_of_corporate = 1;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that ip address id was specified.
  my $ip_address_id = $cgi->param("id");
  if ((!defined($ip_address_id)) || ($ip_address_id !~ /^\d+$/)) {
    die "show_page: edit_ip_address_not_found.html\n";
  }
  # get the form fields.
  my $account_id = $cgi->param("account_id");
  if (defined($account_id) && ($account_id =~ /^\s*$/)) {
    $account_id = undef;
  }
  my $hostname = $cgi->param("hostname") || "";
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the ip address exists.
  my $ip_address = select_row($dbh, "ip_address_tbl", $ip_address_id);
  if (!$ip_address) {
    $dbh->rollback();
    die "show_page: edit_ip_address_not_found.html\n";
  }
  # load account.
  my $account;
  my $account_name = "";
  if ((defined($account_id)) && ($account_id != $account_id_of_corporate)) {
    if (($account_id =~ /^\d+$/) && ($account = select_row($dbh, "account_vw", $account_id))) {
      $account_name = $account->{"name"};
    } else {
      $dbh->rollback();
      die "show_page: edit_ip_address_bad_account.html\n";
    }
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["hostname", $hostname]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "ip_address_tbl", \@sql_column_list, $ip_address_id);
  # commit the transaction.
  $dbh->commit();
  # load building.
  my $building = select_row($dbh, "building_tbl", $ip_address->{"building_id"});
  # populate information needed for display of page.
  my $address = HTML::Entities::encode($ip_address->{"ip_address"});
  my $building_label = HTML::Entities::encode($building->{"short_global_label"});
  $account_name = HTML::Entities::encode($account_name);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <ip-address>
    <address>$address</address>
    <building-label>$building_label</building-label>
    <account-name>$account_name</account-name>
    <hostname>$hostname</hostname>
    <note>$note</note>
  </ip-address>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_ip_address_result_success.xslt", $cgi);
}

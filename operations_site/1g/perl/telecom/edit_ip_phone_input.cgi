#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # verify that ip phone id is valid and try to load ip phone.
  my ($ip_phone_id, $ip_phone) = ($cgi->param("id"), undef);
  if ((!defined($ip_phone_id)) || ($ip_phone_id !~ /^\d+$/) || (!($ip_phone = select_row($dbh_phone, "ip_phone_tbl", $ip_phone_id)))) {
    die "show_page: edit_ip_phone_not_found.html\n";
  }
  # populate information needed for display of page.
  my $manufacturer = HTML::Entities::encode($ip_phone->{"manufacturer"});
  my $model = HTML::Entities::encode($ip_phone->{"model"});
  my $mac_address = HTML::Entities::encode($ip_phone->{"mac_address"});
  my $ownership = HTML::Entities::encode($ip_phone->{"ownership"});
  my $note = HTML::Entities::encode($ip_phone->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <manufacturer_list>
    <manufacturer>cisco</manufacturer>
    <manufacturer>grandstream</manufacturer>
    <manufacturer>linksys</manufacturer>
    <manufacturer>polycom</manufacturer>
    <manufacturer>snom</manufacturer>
  </manufacturer_list>
  <ownership_list>
    <ownership>carrier</ownership>
    <ownership>customer</ownership>
  </ownership_list>
  <ip_phone>
    <id>$ip_phone_id</id>
    <manufacturer>$manufacturer</manufacturer>
    <model>$model</model>
    <mac_address>$mac_address</mac_address>
    <ownership>$ownership</ownership>
    <note>$note</note>
  </ip_phone>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_ip_phone_input.xslt", $cgi);
}

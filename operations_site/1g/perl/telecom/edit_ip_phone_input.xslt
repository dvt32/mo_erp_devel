<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit IP Phone
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">main menu</a> |
	  <a href="/1g/telecom/show_ip_phones.cgi">show IP phones</a> |
	  <a href="/1g/telecom/new_ip_phone_input.cgi">new IP phone</a>
	</p>
	<xsl:apply-templates select="ip_phone"/>
	<p>
	  <a>
	    <xsl:attribute name="href">/1g/telecom/delete_ip_phone.cgi?id=<xsl:value-of select="ip_phone/id"/></xsl:attribute>
	    <xsl:text>delete this IP phone</xsl:text>
	  </a>
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ip_phone">
    <table>
      <tr><td>
	<form action="/1g/telecom/edit_ip_phone_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit IP Phone
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Manufacturer:
		</td>
		<td>
                  <select name="manufacturer">
		    <option value="">[---select---]</option>
		    <xsl:for-each select="/page/manufacturer_list/manufacturer">
		      <option>
			<xsl:if test="/page/ip_phone/manufacturer = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
 	      <tr>
		<td style="padding: 4px; ">
		  Model:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">model</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="model"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
 	      <tr>
		<td style="padding: 4px; ">
		  MAC Address:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">mac_address</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="mac_address"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
 	      <tr>
		<td style="padding: 4px; ">
		  Ownership:
		</td>
		<td>
                  <select name="ownership">
		    <option value="">[---select---]</option>
		    <xsl:for-each select="/page/ownership_list/ownership">
		      <option>
			<xsl:if test="/page/ip_phone/ownership = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

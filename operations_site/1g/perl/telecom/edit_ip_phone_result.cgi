#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;
use strip_outside_whitespace;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get form fields from CGI input.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $manufacturer = Encode::decode($cgi_input_charset, $cgi->param("manufacturer"));
  my $model = Encode::decode($cgi_input_charset, lc($cgi->param("model")));
  my $mac_address = Encode::decode($cgi_input_charset, lc(strip_outside_whitespace($cgi->param("mac_address"))));
  if ($mac_address !~ m{^[0-9a-f]{12}$}i) {
    die "show_page: edit_ip_phone_bad_mac_address.html\n";
  }
  my $ownership = Encode::decode($cgi_input_charset, $cgi->param("ownership"));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note"));
  # connect to phone database.
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # begin transaction of updating the row.
  $dbh_phone->begin_work();
  # verify that ip phone id is valid and try to load ip phone.
  my ($ip_phone_id, $ip_phone) = ($cgi->param("id"), undef);
  if ((!defined($ip_phone_id)) || ($ip_phone_id !~ /^\d+$/) || (!($ip_phone = select_row($dbh_phone, "ip_phone_tbl", $ip_phone_id)))) {

    $dbh_phone->rollback();
    die "show_page: edit_ip_phone_not_found.html\n";
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["manufacturer", Encode::encode($db_charset, $manufacturer) || ""]);
  push(@sql_column_list, ["model", Encode::encode($db_charset, $model) || ""]);
  push(@sql_column_list, ["mac_address", Encode::encode($db_charset, $mac_address)]);
  push(@sql_column_list, ["ownership", Encode::encode($db_charset, $ownership) || ""]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || ""]);
  update_row($dbh_phone, "ip_phone_tbl", \@sql_column_list, $ip_phone_id);
  # commit the transaction.
  $dbh_phone->commit();
  # populate information needed for display of page.
  $manufacturer = HTML::Entities::encode($manufacturer);
  $model = HTML::Entities::encode($model);
  $mac_address = HTML::Entities::encode($mac_address);
  $ownership = HTML::Entities::encode($ownership);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <ip_phone>
    <id>$ip_phone_id</id>
    <manufacturer>$manufacturer</manufacturer>
    <model>$model</model>
    <mac_address>$mac_address</mac_address>
    <ownership>$ownership</ownership>
    <note>$note</note>
  </ip_phone>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_ip_phone_result.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit IP Phone
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New IP Phone
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">main menu</a> |
	  <a href="/1g/telecom/show_ip_phones.cgi">show IP phones</a> |
	  <a href="/1g/telecom/new_ip_phone_input.cgi">new IP phone</a>
	</p>
	<xsl:apply-templates select="ip_phone"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ip_phone">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following IP phone:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new IP phone:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Manufacturer:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="manufacturer"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Model:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="model"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  MAC Address:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="mac_address"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  ownership:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="ownership"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

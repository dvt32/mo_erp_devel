#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # verify that phone number id is valid and try to load it.
  my ($phone_number_id, $phone_number) = ($cgi->param("id"), undef);
  if ((!defined($phone_number_id)) || ($phone_number_id !~ /^\d+$/) || (!($phone_number = select_row($dbh_phone, "phone_number_tbl", $phone_number_id)))) {
    die "show_page: edit_phone_number_not_found.html\n";
  }
  # fetch IAD devices.
  my (%iad_by_id, @iad_id_list);
  my $sql_query = "select * from iad_tbl order by device_name";
  get_row_by_id_map($dbh_phone, $sql_query, undef, \%iad_by_id, \@iad_id_list);
  # fetch IP phone devices.
  my (%ip_phone_by_id, @ip_phone_id_list);
  $sql_query = "select * from ip_phone_tbl order by mac_address";
  get_row_by_id_map($dbh_phone, $sql_query, undef, \%ip_phone_by_id, \@ip_phone_id_list);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # if the account attached to the phone number is not active, load it too.
  if ((defined(my $account_id = $phone_number->{"account_id"})) && (!exists($account_by_id{$phone_number->{"account_id"}}))) {
    $account_by_id{$account_id} = select_row($dbh, "account_vw", $account_id);
  }
  my @account_id_list = sort {
    lc($account_by_id{$a}->{"name"}) cmp lc($account_by_id{$b}->{"name"});
  } keys(%account_by_id);
  # fetch persons.
  my %person_by_id;
  $sql_query = "select * from person_vw where is_active and account_id in (select id from account_vw where is_active)";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  # if the person attached to the phone number is not active, load it too.
  if ((defined(my $person_id = $phone_number->{"person_id"})) && (!exists($person_by_id{$phone_number->{"person_id"}}))) {
    $person_by_id{$person_id} = select_row($dbh, "person_vw", $person_id);
  }
  my @person_id_list = sort {
    my ($person_a, $person_b) = ($person_by_id{$a}, $person_by_id{$b});
    my ($last_name_a, $last_name_b) = ($person_a->{"last_name"}, $person_b->{"last_name"});
    my ($first_name_a, $first_name_b) = ($person_a->{"first_name"}, $person_b->{"first_name"});
    my ($middle_name_a, $middle_name_b) = ($person_a->{"middle_name"}, $person_b->{"middle_name"});
    my $sort_order = 0;
    $sort_order ||= lc($last_name_a) cmp lc($last_name_b);
    $sort_order ||= lc($first_name_a) cmp lc($first_name_b);
    $sort_order ||= lc($middle_name_a) cmp lc($middle_name_b);
    return $sort_order;
  } keys(%person_by_id);
  # get floor and all floors.
  my $phone_number_floor_id = $phone_number->{"floor_id"} || "";
  my (%floor_by_id, @floor_id_list);
  $sql_query = "select * from floor_tbl where is_active order by short_global_label";
  get_row_by_id_map($dbh, $sql_query, undef, \%floor_by_id, \@floor_id_list);
  # populate information needed for display of page.
  my $account_id = $phone_number->{"account_id"} || "";
  my $person_id = $phone_number->{"person_id"} || "";
  my $number = HTML::Entities::encode($phone_number->{"phone_number"});
  my $purpose = HTML::Entities::encode($phone_number->{"purpose"});
  my $visibility = HTML::Entities::encode($phone_number->{"visibility"});
  my $domain = HTML::Entities::encode($phone_number->{"domain"});
  my $extension = HTML::Entities::encode($phone_number->{"extension"} || "");
  my $sip_profile = $phone_number->{"sip_profile"} || "";
  my $device_type = HTML::Entities::encode($phone_number->{"device_type"});
  my $iad_id = $phone_number->{"iad_id"} || "";
  my $device_port = $phone_number->{"device_port"} || "";
  my $ip_phone_id = $phone_number->{"ip_phone_id"} || "";
  my $is_custom_inbound_dialplan = $phone_number->{"is_custom_inbound_dialplan"} ? 1 : 0;
  my $is_custom_outbound_dialplan = $phone_number->{"is_custom_outbound_dialplan"} ? 1 : 0;
  my $do_call_forward = $phone_number->{"do_call_forward"} ? 1 : 0;
  my $call_forward_delay = $phone_number->{"call_forward_delay"};
  my $call_forward_dst_num = HTML::Entities::encode($phone_number->{"call_forward_dst_num"});
  my $do_simring = $phone_number->{"do_simring"} ? 1 : 0;
  my $simring_dst_num_list = HTML::Entities::encode($phone_number->{"simring_dst_num_list"});
  my $block_outbound_caller_id = $phone_number->{"block_outbound_caller_id"} ? 1 : 0;
  my $is_voicemail_on = $phone_number->{"is_voicemail_on"} ? 1 : 0;
  my $voicemail_box_num = HTML::Entities::encode($phone_number->{"voicemail_box_num"} || "");
  my $voicemail_pin = HTML::Entities::encode($phone_number->{"voicemail_pin"} || "");
  my $voicemail_delay = $phone_number->{"voicemail_delay"};
  my $voicemail_as_email = HTML::Entities::encode($phone_number->{"voicemail_as_email"});
  my $hold_music = HTML::Entities::encode($phone_number->{"hold_music"});
  my $email_addr_for_vm = HTML::Entities::encode($phone_number->{"email_addr_for_vm"} || "");
  my $fax_as_email = HTML::Entities::encode($phone_number->{"fax_as_email"});
  my $email_addr_for_fax = HTML::Entities::encode($phone_number->{"email_addr_for_fax"} || "");
  my $sip_register_user_id = HTML::Entities::encode($phone_number->{"sip_register_user_id"} || "");
  my $sip_register_password = HTML::Entities::encode($phone_number->{"sip_register_password"} || "");
  my $is_active = $phone_number->{"is_active"} ? 1 : 0;
  my $note = HTML::Entities::encode($phone_number->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <purpose_list>
    <purpose>voice</purpose>
    <purpose>fax</purpose>
    <purpose>efax</purpose>
  </purpose_list>
  <device_type_list>
    <device_type>none</device_type>
    <device_type>iad</device_type>
    <device_type>ip_phone</device_type>
  </device_type_list>
  <phone_number>
    <id>$phone_number_id</id>
    <account_id>$account_id</account_id>
    <person_id>$person_id</person_id>
    <floor_id>$phone_number_floor_id</floor_id>
    <phone_number>$number</phone_number>
    <purpose>$purpose</purpose>
    <visibility>$visibility</visibility>
    <domain>$domain</domain>
    <extension>$extension</extension>
    <sip_profile>$sip_profile</sip_profile>
    <device_type>$device_type</device_type>
    <iad_id>$iad_id</iad_id>
    <device_port>$device_port</device_port>
    <ip_phone_id>$ip_phone_id</ip_phone_id>
    <is_custom_inbound_dialplan>$is_custom_inbound_dialplan</is_custom_inbound_dialplan>
    <is_custom_outbound_dialplan>$is_custom_outbound_dialplan</is_custom_outbound_dialplan>
    <do_call_forward>$do_call_forward</do_call_forward>
    <call_forward_delay>$call_forward_delay</call_forward_delay>
    <call_forward_dst_num>$call_forward_dst_num</call_forward_dst_num>
    <do_simring>$do_simring</do_simring>
    <simring_dst_num_list>$simring_dst_num_list</simring_dst_num_list>
    <block_outbound_caller_id>$block_outbound_caller_id</block_outbound_caller_id>
    <is_voicemail_on>$is_voicemail_on</is_voicemail_on>
    <voicemail_box_num>$voicemail_box_num</voicemail_box_num>
    <voicemail_pin>$voicemail_pin</voicemail_pin>
    <voicemail_delay>$voicemail_delay</voicemail_delay>
    <voicemail_as_email>$voicemail_as_email</voicemail_as_email>
    <hold_music>$hold_music</hold_music>
    <email_addr_for_vm>$email_addr_for_vm</email_addr_for_vm>
    <fax_as_email>$fax_as_email</fax_as_email>
    <email_addr_for_fax>$email_addr_for_fax</email_addr_for_fax>
    <sip_register_user_id>$sip_register_user_id</sip_register_user_id>
    <sip_register_password>$sip_register_password</sip_register_password>
    <is_active>$is_active</is_active>
    <note>$note</note>
  </phone_number>
  <iad_list>
---END_BLOCK---;
  for my $_iad_id (@iad_id_list) {
    my $iad = $iad_by_id{$_iad_id};
    my $floor_id = $iad->{"floor_id"};
    my $floor_label = HTML::Entities::encode($floor_by_id{$floor_id}->{"short_global_label"});
    my $device_name = HTML::Entities::encode($iad->{"device_name"});
    my $manufacturer = HTML::Entities::encode($iad->{"manufacturer"});
    my $model = HTML::Entities::encode($iad->{"model"});
    my $mac_address = HTML::Entities::encode($iad->{"mac_address"});
    $generated_xml .= <<"---END_BLOCK---;";
    <iad>
      <id>$_iad_id</id>
      <floor_label>$floor_label</floor_label>
      <device_name>$device_name</device_name>
      <manufacturer>$manufacturer</manufacturer>
      <model>$model</model>
      <mac_address>$mac_address</mac_address>
    </iad>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </iad_list>
  <device_port_list>
---END_BLOCK---;
  for(my $_device_port = 1; $_device_port <= 24; $_device_port++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <device_port>$_device_port</device_port>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </device_port_list>
  <ip_phone_list>
---END_BLOCK---;
  for my $_ip_phone_id (@ip_phone_id_list) {
    my $ip_phone = $ip_phone_by_id{$_ip_phone_id};
    my $manufacturer = HTML::Entities::encode($ip_phone->{"manufacturer"});
    my $model = HTML::Entities::encode($ip_phone->{"model"});
    my $mac_address = HTML::Entities::encode($ip_phone->{"mac_address"});
    $generated_xml .= <<"---END_BLOCK---;";
    <ip_phone>
      <id>$_ip_phone_id</id>
      <manufacturer>$manufacturer</manufacturer>
      <model>$model</model>
      <mac_address>$mac_address</mac_address>
    </ip_phone>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ip_phone_list>
  <account_list>
---END_BLOCK---;
  for $account_id (@account_id_list) {
    my $account = $account_by_id{$account_id};
    my $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$account_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account_list>
  <person_list>
---END_BLOCK---;
  for $person_id (@person_id_list) {
    my $person = $person_by_id{$person_id};
    my $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
    if ($person->{"middle_name"}) {
      $person_name .= " " . $person->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$person_id</id>
      <name>$person_name</name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person_list>
  <floor_list>
---END_BLOCK---;
  for my $_floor_id (@floor_id_list) {
    my $floor = $floor_by_id{$_floor_id};
    my $floor_label = HTML::Entities::encode($floor->{"short_global_label"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <floor>
      <id>$_floor_id</id>
      <label>$floor_label</label>
    </floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </floor_list>
  <visibility_list>
    <visibility>hidden</visibility>
    <visibility>visible</visibility>
    <visibility>emphasized</visibility>
  </visibility_list>
  <voicemail_as_email_list>
    <option>none</option>
    <option>notify</option>
    <option>attach</option>
  </voicemail_as_email_list>
  <fax_as_email_list>
    <option>none</option>
    <option>notify</option>
    <option>attach</option>
  </fax_as_email_list>
  <hold_music_list>
    <option>default</option>
    <option>silence</option>
  </hold_music_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_phone_number_input.xslt", $cgi);
}

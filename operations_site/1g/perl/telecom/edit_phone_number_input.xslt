<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Phone Number
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  Phone Numbers:
	  <a href="/1g/telecom/show_phone_numbers.cgi">All</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=1">36W20F3</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=2">1375BwyF3</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=9">1375BwyF6</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=3">1375BwyF11</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=4">122W27F10</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=5">122W27F12</a>
	</p>
	<xsl:apply-templates select="phone_number"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="phone_number">
    <table>
      <tr><td>
	<form action="/1g/telecom/edit_phone_number_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Phone Number
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Number:
		</td>
		<td>
		  <xsl:value-of select="phone_number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Purpose:
		</td>
		<td>
                  <select name="purpose">
		    <xsl:for-each select="/page/purpose_list/purpose">
		      <option>
			<xsl:if test="/page/phone_number/purpose = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Visibility:
		</td>
		<td>
                  <select name="visibility">
		    <xsl:for-each select="/page/visibility_list/visibility">
		      <option>
			<xsl:if test="/page/phone_number/visibility = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  (whether visible or not in directory)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Device Type:
		</td>
		<td>
                  <select name="device_type">
		    <xsl:for-each select="/page/device_type_list/device_type">
		      <option>
			<xsl:if test="/page/phone_number/device_type = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  IAD:
		</td>
		<td>
                  <select name="iad_id" style="font-family: monospace">
		    <option value="">(none)</option>
		    <xsl:for-each select="/page/iad_list/iad">
		      <option>
			<xsl:if test="/page/phone_number/iad_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="mac_address"/>
			(<xsl:value-of select="floor_label"/>,
			<xsl:value-of select="manufacturer"/><xsl:text> </xsl:text><xsl:value-of select="model"/>)
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Device Port:
		</td>
		<td>
                  <select name="device_port">
		    <option value="">(none)</option>
		    <xsl:for-each select="/page/device_port_list/device_port">
		      <option>
			<xsl:if test="/page/phone_number/device_port = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  IP Phone:
		</td>
		<td>
                  <select name="ip_phone_id" style="font-family: monospace">
		    <option value="">(none)</option>
		    <xsl:for-each select="/page/ip_phone_list/ip_phone">
		      <option>
			<xsl:if test="/page/phone_number/ip_phone_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="mac_address"/> - 
			<xsl:value-of select="manufacturer"/><xsl:text> </xsl:text><xsl:value-of select="model"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  SIP Register User ID:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sip_register_user_id</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sip_register_user_id"/></xsl:attribute>
		  </input>
		  (blank to lock out SIP registration)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  SIP Register Password:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sip_register_password</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sip_register_password"/></xsl:attribute>
		  </input>
		  (blank to lock out SIP registration)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Domain:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">domain</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="domain"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Extension:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">extension</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="extension"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
		<tr>
		<td style="padding: 4px; ">
		  Sip Profile:
		</td>
		<td>
                 <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">sip_profile</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="sip_profile"/></xsl:attribute>
		  </input>
		</td>
	       </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Custom Inbound Dialplan:
		</td>
		<td>
                  <select name="is_custom_inbound_dialplan">
		    <option value="1">
		      <xsl:if test="/page/phone_number/is_custom_inbound_dialplan = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      Yes
		    </option>
		    <option value="0">
		      <xsl:if test="/page/phone_number/is_custom_inbound_dialplan = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      No
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Custom Outbound Dialplan:
		</td>
		<td>
                  <select name="is_custom_outbound_dialplan">
		    <option value="1">
		      <xsl:if test="/page/phone_number/is_custom_outbound_dialplan = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      Yes
		    </option>
		    <option value="0">
		      <xsl:if test="/page/phone_number/is_custom_outbound_dialplan = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      No
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Call Forwarding Enabled:
		</td>
		<td>
                  <select name="do_call_forward">
		    <option value="1">
		      <xsl:if test="/page/phone_number/do_call_forward = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      Yes
		    </option>
		    <option value="0">
		      <xsl:if test="/page/phone_number/do_call_forward = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      No
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Call Forward Delay:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">call_forward_delay</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="call_forward_delay"/></xsl:attribute>
		  </input>
		  (0 or 5 - 45)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Forward To Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">call_forward_dst_num</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="call_forward_dst_num"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Simring Enabled:
		</td>
		<td>
                  <select name="do_simring">
		    <option value="1">
		      <xsl:if test="/page/phone_number/do_simring = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      Yes
		    </option>
		    <option value="0">
		      <xsl:if test="/page/phone_number/do_simring = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      No
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Simring Targets:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">simring_dst_num_list</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="simring_dst_num_list"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		  (comma-separated)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Block Outbound Caller ID:
		</td>
		<td>
                  <select name="block_outbound_caller_id">
		    <option value="1">
		      <xsl:if test="/page/phone_number/block_outbound_caller_id = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      Yes
		    </option>
		    <option value="0">
		      <xsl:if test="/page/phone_number/block_outbound_caller_id = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      No
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Voicemail Enabled:
		</td>
		<td>
                  <select name="is_voicemail_on">
		    <option value="1">
		      <xsl:if test="/page/phone_number/is_voicemail_on = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      Yes
		    </option>
		    <option value="0">
		      <xsl:if test="/page/phone_number/is_voicemail_on = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      No
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Voicemail Box Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">voicemail_box_num</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="voicemail_box_num"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Voicemail PIN:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">voicemail_pin</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="voicemail_pin"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Voicemail Delay:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">voicemail_delay</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="voicemail_delay"/></xsl:attribute>
		  </input>
		  (0 or 5 - 45)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Voicemail as Email:
		</td>
		<td>
                  <select name="voicemail_as_email">
		    <xsl:for-each select="/page/voicemail_as_email_list/option">
		      <option>
			<xsl:if test="/page/phone_number/voicemail_as_email = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email for Voicemail:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email_addr_for_vm</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="email_addr_for_vm"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Hold Music:
		</td>
		<td>
                  <select name="hold_music">
		    <xsl:for-each select="/page/hold_music_list/option">
		      <option>
			<xsl:if test="/page/phone_number/hold_music = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Fax as Email:
		</td>
		<td>
                  <select name="fax_as_email">
		    <xsl:for-each select="/page/fax_as_email_list/option">
		      <option>
			<xsl:if test="/page/phone_number/fax_as_email = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email for Fax:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email_addr_for_fax</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="email_addr_for_fax"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Number is Active:
		</td>
		<td>
                  <select name="is_active">
		    <option value="1">
		      <xsl:if test="/page/phone_number/is_active = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      Yes
		    </option>
		    <option value="0">
		      <xsl:if test="/page/phone_number/is_active = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      No
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Building and Floor:
		</td>
		<td>
                  <select name="floor_id">
		    <option value="">(none)</option>
		    <xsl:for-each select="/page/floor_list/floor">
		      <option>
			<xsl:if test="/page/phone_number/floor_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account:
		</td>
		<td>
                  <select name="account_id">
		    <option value="">(unassigned)</option>
		    <xsl:for-each select="/page/account_list/account">
		      <option>
			<xsl:if test="/page/phone_number/account_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Person:
		</td>
		<td>
                  <select name="person_id">
		    <option value="">(unassigned)</option>
		    <xsl:for-each select="/page/person_list/person">
		      <option>
			<xsl:if test="/page/phone_number/person_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (the note is visible to the tenant!)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

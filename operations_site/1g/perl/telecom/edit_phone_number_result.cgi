#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;
use is_valid_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $dbh_phone = get_database_handle($instance_map, "phone");
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that phone number id was specified.
  my $phone_number_id = $cgi->param("id");
  if ((!defined($phone_number_id)) || ($phone_number_id !~ /^\d+$/)) {
    die "show_page: edit_phone_number_not_found.html\n";
  }
  # get the form fields.
  my $purpose = $cgi->param("purpose");
  if ($purpose !~ /^(voice|fax|efax)$/) {
    die "show_page: edit_phone_number_bad_purpose.html\n";
  }
  my $visibility = $cgi->param("visibility");
  if ($visibility !~ /^(hidden|visible|emphasized)$/) {
    die "show_page: edit_phone_number_bad_visibility.html\n";
  }
  my $device_type = $cgi->param("device_type");
  if ($device_type !~ /^(none|iad|ip_phone)$/) {
    die "show_page: edit_phone_number_bad_device_type.html\n";
  }
  my $iad_id = $cgi->param("iad_id");
  if (defined($iad_id) && ($iad_id =~ /^\s*$/)) {
    $iad_id = undef;
  }
  my $iad;
  if ($iad_id) {
    if (($iad_id !~ /^\d+$/) || !($iad = select_row($dbh_phone, "iad_tbl", $iad_id))) {
      die "show_page: edit_phone_number_bad_iad.html\n";
    }
  }
  my $device_port = $cgi->param("device_port");
  if (defined($device_port) && ($device_port =~ /^\s*$/)) {
    $device_port = undef;
  }
  if (($device_port) && (($device_port !~ /^\d+$/) || ($device_port < 1) || ($device_port > 24))) {
    die "show_page: edit_phone_number_bad_device_port.html\n";
  }
  my $ip_phone_id = $cgi->param("ip_phone_id");
  if (defined($ip_phone_id) && ($ip_phone_id =~ /^\s*$/)) {
    $ip_phone_id = undef;
  }
  my $ip_phone;
  if ($ip_phone_id) {
    if (($ip_phone_id !~ /^\d+$/) || !($ip_phone = select_row($dbh_phone, "ip_phone_tbl", $ip_phone_id))) {
      die "show_page: edit_phone_number_bad_ip_phone.html\n";
    }
  }
  my $sip_register_user_id = Encode::decode($cgi_input_charset, $cgi->param("sip_register_user_id"));
  $sip_register_user_id =~ s/\s+//g;
  my $sip_register_password = Encode::decode($cgi_input_charset, $cgi->param("sip_register_password"));
  $sip_register_password =~ s/\s+//g;
  if ($device_type eq "iad") {
    if ((!defined($iad_id)) || (!$device_port)) {
      die "show_page: edit_phone_number_missing_iad.html\n";
    } elsif (defined($ip_phone)) {
      die "show_page: edit_phone_number_extraneous_ip_phone.html\n";
    }
  } elsif ($device_type eq "ip_phone") {
    if ((defined($iad_id)) || ($device_port)) {
      die "show_page: edit_phone_number_extraneous_iad.html\n";
    } elsif (!defined($ip_phone)) {
      die "show_page: edit_phone_number_missing_ip_phone.html\n";
    } elsif ((!$sip_register_user_id) || (!$sip_register_password)) {
      die "show_page: edit_phone_number_bad_sip_registration.html\n";
    }
  } elsif ($device_type eq "none") {
    if ((defined($iad_id)) || ($device_port)) {
      die "show_page: edit_phone_number_extraneous_iad.html\n";
    } elsif (defined($ip_phone_id)) {
      die "show_page: edit_phone_number_extraneous_ip_phone.html\n";
    }
  }
  my $domain = Encode::decode($cgi_input_charset, $cgi->param("domain"));
  $domain =~ s/\s+//g;
  if (!$domain) {
    die "show_page: edit_phone_number_bad_domain.html\n";
  }
  my $extension = $cgi->param("extension");
  if (!$extension) {
    die "show_page: edit_phone_number_bad_extension.html\n";
  }
  my $sip_profile = $cgi->param("sip_profile");
  my $is_custom_inbound_dialplan = $cgi->param("is_custom_inbound_dialplan") ? 1 : 0;
  my $is_custom_outbound_dialplan = $cgi->param("is_custom_outbound_dialplan") ? 1 : 0;
  my $do_call_forward = $cgi->param("do_call_forward") ? 1 : 0;
  my $call_forward_delay = $cgi->param("call_forward_delay");
  if (($call_forward_delay =~ /^\d+$/) && (($call_forward_delay == 0) || (($call_forward_delay >= 5) && ($call_forward_delay <= 45)))) {
  } else {
    die "show_page: edit_phone_number_bad_call_forward_delay.html\n";
  }
  my $call_forward_dst_num = $cgi->param("call_forward_dst_num");
  $call_forward_dst_num =~ s/\s+//g;
  if (($call_forward_dst_num) && ($call_forward_dst_num !~ /^\d{10}$/)) {
    die "show_page: edit_phone_number_bad_call_forward_dst_num.html\n";
  }
  my $do_simring = $cgi->param("do_simring") ? 1 : 0;
  my $simring_dst_num_list = $cgi->param("simring_dst_num_list");
  $simring_dst_num_list =~ s/\s+//g;
  if ($simring_dst_num_list) {
    my @simring_dst_num_list = split(/,/, $simring_dst_num_list);
    for my $simring_dst_num (@simring_dst_num_list) {
      if ($simring_dst_num !~ /^\d{10}$/) {
	die "show_page: edit_phone_number_bad_simring_dst_num_list.html\n";
      }
    }
  }
  my $block_outbound_caller_id = $cgi->param("block_outbound_caller_id") ? 1 : 0;
  my $is_voicemail_on = $cgi->param("is_voicemail_on") ? 1 : 0;
  my $voicemail_box_num = $cgi->param("voicemail_box_num");
  $voicemail_box_num =~ s/\s+//g;
  if ($voicemail_box_num !~ /^\d*$/) {
    die "show_page: edit_phone_number_bad_voicemail_box_num.html\n";
  }
  my $voicemail_pin = $cgi->param("voicemail_pin");
  $voicemail_pin =~ s/\s+//g;
  if ($voicemail_pin !~ /^\d*$/) {
    die "show_page: edit_phone_number_bad_voicemail_pin.html\n";
  }
  my $voicemail_delay = $cgi->param("voicemail_delay");
  $voicemail_delay =~ s/\s+//g;
  if (($voicemail_delay =~ /^\d+$/) && (($voicemail_delay == 0) || (($voicemail_delay >= 5) && ($voicemail_delay <= 45)))) {
  } else {
    die "show_page: edit_phone_number_bad_voicemail_delay.html\n";
  }
  my $voicemail_as_email = $cgi->param("voicemail_as_email");
  if ($voicemail_as_email !~ /^(none|notify|attach)$/) {
    die "show_page: edit_phone_number_bad_voicemail_as_email.html\n";
  }
  my $email_addr_for_vm = Encode::decode($cgi_input_charset, $cgi->param("email_addr_for_vm"));
  $email_addr_for_vm =~ s/\s+//g;
  if (($email_addr_for_vm) && (!is_valid_email($email_addr_for_vm))) {
    die "show_page: edit_phone_number_bad_email_addr_for_vm.html\n";
  }
  my $hold_music = $cgi->param("hold_music");
  if ($hold_music !~ /^(default|silence)$/) {
    die "show_page: edit_phone_number_bad_hold_music.html\n";
  }
  my $fax_as_email = $cgi->param("fax_as_email");
  if ($fax_as_email !~ /^(none|notify|attach)$/) {
    die "show_page: edit_phone_number_bad_fax_as_email.html\n";
  }
  my $email_addr_for_fax = Encode::decode($cgi_input_charset, $cgi->param("email_addr_for_fax"));
  $email_addr_for_fax =~ s/\s+//g;
  if (($email_addr_for_fax) && (!is_valid_email($email_addr_for_fax))) {
    die "show_page: edit_phone_number_bad_email_addr_for_fax.html\n";
  }
  my $is_active = $cgi->param("is_active") ? 1 : 0;
  my $floor_id = $cgi->param("floor_id");
  if (defined($floor_id) && ($floor_id =~ /^\s*$/)) {
    $floor_id = undef;
  } elsif ($floor_id !~ /^\d+$/) {
    die "show_page: edit_phone_number_bad_floor.html\n";
  }
  my $account_id = $cgi->param("account_id");
  if (defined($account_id) && ($account_id =~ /^\s*$/)) {
    $account_id = undef;
  } elsif ($account_id !~ /^\d+$/) {
    die "show_page: edit_phone_number_bad_account.html\n";
  }
  my $person_id = $cgi->param("person_id");
  if (defined($person_id) && ($person_id =~ /^\s*$/)) {
    $person_id = undef;
  } elsif ($person_id !~ /^\d+$/) {
    die "show_page: edit_phone_number_bad_person.html\n";
  }
  if ((defined($person_id)) && (!defined($account_id))) {
    die "show_page: edit_phone_number_person_no_account.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the phone number exists.
  my $phone_number = select_row($dbh_phone, "phone_number_tbl", $phone_number_id);
  if (!$phone_number) {
    $dbh->rollback();
    die "show_page: edit_phone_number_not_found.html\n";
  }
  # if iad specified, verify existence.
  my $iad;
  my $iad_label = "";
  if (defined($iad_id)) {
    $iad = select_row($dbh_phone, "iad_tbl", $iad_id);
    if ($iad) {
      my $floor = select_row($dbh, "floor_tbl", $iad->{"floor_id"});
      my $floor_label = $floor->{"short_global_label"};
      $iad_label = $iad->{"device_name"} . " (" . $floor_label . ", " . $iad->{"manufacturer"} . " " . $iad->{"model"} . ", " . $iad->{"mac_address"} . ")";
    } else {
      $dbh->rollback();
      die "show_page: edit_phone_number_bad_iad.html\n";
    }
  }
  # if IP phone specified, verify existence.
  my $ip_phone;
  my $ip_phone_label = "";
  if (defined($ip_phone_id)) {
    $ip_phone = select_row($dbh_phone, "ip_phone_tbl", $ip_phone_id);
    if ($ip_phone) {
      $ip_phone_label = $ip_phone->{"mac_address"} . " - " . $ip_phone->{"manufacturer"} . " " . $ip_phone->{"model"};
    } else {
      $dbh->rollback();
      die "show_page: edit_phone_number_bad_ip_phone.html\n";
    }
  }
  # if floor specified, verify existence.
  my $floor;
  my $floor_label = "";
  if (defined($floor_id)) {
    $floor = select_row($dbh, "floor_tbl", $floor_id);
    if ($floor) {
      $floor_label = $floor->{"short_global_label"};
    } else {
      $dbh->rollback();
      die "show_page: edit_phone_number_bad_floor.html\n";
    }
  }
  # if account specified, verify existence.
  my $account;
  my $account_name = "";
  if (defined($account_id)) {
    if (($account_id =~ /^\d+$/) && ($account = select_row($dbh, "account_vw", $account_id))) {
      $account_name = $account->{"name"};
    } else {
      $dbh->rollback();
      die "show_page: edit_phone_number_bad_account.html\n";
    }
  }
  # if person specified, verify existence and then load person.
  my $person;
  my $person_name = "";
  if (defined($person_id)) {
    if (($person_id =~ /^\d+$/) && ($person = select_row($dbh, "person_vw", $person_id))) {
      $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
      if ($person->{"middle_name"}) {
	$person_name .= " " . $person->{"middle_name"};
      }
    } else {
      $dbh->rollback();
      die "show_page: edit_phone_number_bad_person.html\n";
    }
  }
  # verify that the person is linked to the account.
  if (defined($account_id) && defined($person_id)) {
    if ($account->{"id"} != $person->{"account_id"}) {
      $dbh->rollback();
      die "show_page: edit_phone_number_account_person_mismatch.html\n";
    }
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["purpose", $purpose]);
  push(@sql_column_list, ["visibility", $visibility]);
  push(@sql_column_list, ["device_type", $device_type]);
  push(@sql_column_list, ["iad_id", $iad_id, SQL_INTEGER]);
  push(@sql_column_list, ["device_port", $device_port, SQL_INTEGER]);
  push(@sql_column_list, ["ip_phone_id", $ip_phone_id, SQL_INTEGER]);
  push(@sql_column_list, ["sip_register_user_id", Encode::encode($db_charset, $sip_register_user_id)]);
  push(@sql_column_list, ["sip_register_password", Encode::encode($db_charset, $sip_register_password)]);
  push(@sql_column_list, ["domain", $domain]);
  push(@sql_column_list, ["extension", $extension]);
  push(@sql_column_list, ["sip_profile", $sip_profile]);
  push(@sql_column_list, ["is_custom_inbound_dialplan", $is_custom_inbound_dialplan, SQL_BOOLEAN]);
  push(@sql_column_list, ["is_custom_outbound_dialplan", $is_custom_outbound_dialplan, SQL_BOOLEAN]);
  push(@sql_column_list, ["do_call_forward", $do_call_forward, SQL_BOOLEAN]);
  push(@sql_column_list, ["call_forward_delay", $call_forward_delay]);
  push(@sql_column_list, ["call_forward_dst_num", $call_forward_dst_num]);
  push(@sql_column_list, ["do_simring", $do_simring, SQL_BOOLEAN]);
  push(@sql_column_list, ["simring_dst_num_list", $simring_dst_num_list]);
  push(@sql_column_list, ["block_outbound_caller_id", $block_outbound_caller_id, SQL_BOOLEAN]);
  push(@sql_column_list, ["is_voicemail_on", $is_voicemail_on, SQL_BOOLEAN]);
  push(@sql_column_list, ["voicemail_box_num", $voicemail_box_num]);
  push(@sql_column_list, ["voicemail_pin", $voicemail_pin]);
  push(@sql_column_list, ["voicemail_delay", $voicemail_delay]);
  push(@sql_column_list, ["voicemail_as_email", $voicemail_as_email]);
  push(@sql_column_list, ["email_addr_for_vm", Encode::encode($db_charset, $email_addr_for_vm)]);
  push(@sql_column_list, ["hold_music", $hold_music]);
  push(@sql_column_list, ["fax_as_email", $fax_as_email]);
  push(@sql_column_list, ["email_addr_for_fax", Encode::encode($db_charset, $email_addr_for_fax)]);
  push(@sql_column_list, ["is_active", $is_active, SQL_BOOLEAN]);
  push(@sql_column_list, ["floor_id", $floor_id, SQL_INTEGER]);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $person_id, SQL_INTEGER]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh_phone, "phone_number_tbl", \@sql_column_list, $phone_number_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $number = HTML::Entities::encode($phone_number->{"phone_number"});
  $purpose = HTML::Entities::encode($purpose);
  $visibility = HTML::Entities::encode($visibility);
  $device_type = HTML::Entities::encode($device_type);
  $iad_label = HTML::Entities::encode($iad_label);
  $device_port = "" if (!defined($device_port));
  $ip_phone_label = HTML::Entities::encode($ip_phone_label);
  $sip_register_user_id = HTML::Entities::encode($sip_register_user_id);
  $sip_register_password = HTML::Entities::encode($sip_register_password);
  $domain = HTML::Entities::encode($domain);
  $extension = HTML::Entities::encode($extension);
  $sip_profile = HTML::Entities::encode($sip_profile);
  $call_forward_dst_num = HTML::Entities::encode($call_forward_dst_num);
  $simring_dst_num_list = HTML::Entities::encode($simring_dst_num_list);
  $voicemail_box_num = HTML::Entities::encode($voicemail_box_num);
  $voicemail_pin = HTML::Entities::encode($voicemail_pin);
  $voicemail_delay = HTML::Entities::encode($voicemail_delay);
  $voicemail_as_email = HTML::Entities::encode($voicemail_as_email);
  $email_addr_for_vm = HTML::Entities::encode($email_addr_for_vm);
  $hold_music = HTML::Entities::encode($hold_music);
  $fax_as_email = HTML::Entities::encode($fax_as_email);
  $email_addr_for_fax = HTML::Entities::encode($email_addr_for_fax);
  $floor_label = HTML::Entities::encode($floor_label);
  $account_name = HTML::Entities::encode($account_name);
  $person_name = HTML::Entities::encode($person_name);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <phone_number>
    <number>$number</number>
    <purpose>$purpose</purpose>
    <visibility>$visibility</visibility>
    <device_type>$device_type</device_type>
    <iad_label>$iad_label</iad_label>
    <device_port>$device_port</device_port>
    <ip_phone_label>$ip_phone_label</ip_phone_label>
    <sip_register_user_id>$sip_register_user_id</sip_register_user_id>
    <sip_register_password>$sip_register_password</sip_register_password>
    <domain>$domain</domain>
    <extension>$extension</extension>
    <sip_profile>$sip_profile</sip_profile>
    <is_custom_inbound_dialplan>$is_custom_inbound_dialplan</is_custom_inbound_dialplan>
    <is_custom_outbound_dialplan>$is_custom_outbound_dialplan</is_custom_outbound_dialplan>
    <do_call_forward>$do_call_forward</do_call_forward>
    <call_forward_delay>$call_forward_delay</call_forward_delay>
    <call_forward_dst_num>$call_forward_dst_num</call_forward_dst_num>
    <do_simring>$do_simring</do_simring>
    <simring_dst_num_list>$simring_dst_num_list</simring_dst_num_list>
    <block_outbound_caller_id>$block_outbound_caller_id</block_outbound_caller_id>
    <is_voicemail_on>$is_voicemail_on</is_voicemail_on>
    <voicemail_box_num>$voicemail_box_num</voicemail_box_num>
    <voicemail_pin>$voicemail_pin</voicemail_pin>
    <voicemail_delay>$voicemail_delay</voicemail_delay>
    <voicemail_as_email>$voicemail_as_email</voicemail_as_email>
    <email_addr_for_vm>$email_addr_for_vm</email_addr_for_vm>
    <hold_music>$hold_music</hold_music>
    <fax_as_email>$fax_as_email</fax_as_email>
    <email_addr_for_fax>$email_addr_for_fax</email_addr_for_fax>
    <is_active>$is_active</is_active>
    <floor_label>$floor_label</floor_label>
    <account_name>$account_name</account_name>
    <person_name>$person_name</person_name>
    <note>$note</note>
  </phone_number>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_phone_number_result.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Phone Number
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a>
	  |
	  Phone Numbers:
	  <a href="/1g/telecom/show_phone_numbers.cgi">All</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=1">36W20F3</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=2">1375BwyF3</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=9">1375BwyF6</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=3">1375BwyF11</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=4">122W27F10</a>
	  &middot;
	  <a href="/1g/telecom/show_phone_numbers.cgi?floor_id=5">122W27F12</a>
	</p>
	<xsl:apply-templates select="phone_number"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="phone_number">
    <p>
      You successfully updated the following phone number:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Purpose:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="purpose"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Visibility:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="visibility"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Device Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="device_type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  IAD:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="iad_label"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Device Port:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="device_port"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  IP Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="ip_phone_label"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  SIP Register User ID:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="sip_register_user_id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  SIP Register Password:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="sip_register_password"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Domain:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="domain"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Extension:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="extension"/>
	</td>
      </tr>
	<tr>
	<td class="map_table_key">
	  Sip profile:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="sip_profile"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Custom Inbound Dialplan:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is_custom_inbound_dialplan = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Custom Outbound Dialplan:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is_custom_outbound_dialplan = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Call Forwarding Enabled:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="do_call_forward = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Call Forward Delay:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="call_forward_delay"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Forward To Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="call_forward_dst_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Simring Enabled:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="do_simring = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Simring Targets:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="simring_dst_num_list"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Block Outbound Caller ID:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="block_outbound_caller_id = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Voicemail Enabled:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is_voicemail_on = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Voicemail Box Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="voicemail_box_num"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Voicemail PIN:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="voicemail_pin"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Voicemail Delay:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="voicemail_delay"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Voicemail as Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="voicemail_as_email"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email for Voicemail:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email_addr_for_vm"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Hold Music:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="hold_music"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Fax as Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="fax_as_email"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email for Fax:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email_addr_for_fax"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Number is Active:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is_active = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Building and Floor:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="floor_label"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Person:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="person_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

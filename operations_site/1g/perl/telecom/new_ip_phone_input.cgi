#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <manufacturer_list>
    <manufacturer>cisco</manufacturer>
    <manufacturer>grandstream</manufacturer>
    <manufacturer>linksys</manufacturer>
    <manufacturer>polycom</manufacturer>
    <manufacturer>snom</manufacturer>
  </manufacturer_list>
  <ownership_list>
    <ownership>carrier</ownership>
    <ownership>customer</ownership>
  </ownership_list>
  <ip_phone/>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_ip_phone_input.xslt", $cgi);
}

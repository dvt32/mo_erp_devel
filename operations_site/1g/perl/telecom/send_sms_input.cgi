#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # populate information needed for display of page.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <form>
    <from_email>service\@microoffice.com</from_email>
  </form>
  <carrier_list>
    <carrier>Alltell</carrier>
    <carrier>AT&amp;T</carrier>
    <carrier>MetroPCS</carrier>
    <carrier>Nextel</carrier>
    <carrier>Powertel</carrier>
    <carrier>Sprint</carrier>
    <carrier>SunCom</carrier>
    <carrier>T-Mobile</carrier>
    <carrier>US Cellular</carrier>
    <carrier>Verizon</carrier>
    <carrier>Virgin Mobile</carrier>
  </carrier_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "send_sms_input.xslt", $cgi);
}

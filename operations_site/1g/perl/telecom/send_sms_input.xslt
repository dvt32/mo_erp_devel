<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Send SMS
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a> |
	  <a href="/1g/telecom/show_calls.cgi">Active Calls</a> |
	  <a href="/1g/telecom/show_sofia_status.cgi">SIP Registrations</a>
	</p>
	<xsl:apply-templates select="form"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="form">
    <table>
      <tr><td>
	<form action="/1g/telecom/send_sms_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Send SMS
	    </legend>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  From Email:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">from_email</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="from_email"/></xsl:attribute>
		    <xsl:attribute name="size">24</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Message:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">message</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Phone Number:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">phone_number</xsl:attribute>
		    <xsl:attribute name="size">10</xsl:attribute>
		  </input>
		  (10 digit xxxyyyzzzz)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Carrier:
		</td>
		<td>
                  <select name="carrier">
		    <xsl:for-each select="/page/carrier_list/carrier">
		      <option><xsl:value-of select="."/></option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Send"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

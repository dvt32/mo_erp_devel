#!/usr/bin/perl

use strict;
local($^W) = 1;

use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use is_valid_email;
use send_email;
use strip_outside_whitespace;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $from_email = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("from_email")));
  if (!is_valid_email($from_email)) {
    die "show_page: send_sms_bad_email.html\n";
  }
  my $message = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("message")));
  if (!$message) {
    die "show_page: send_sms_missing_message.html\n";
  }
  my $phone_number = strip_outside_whitespace($cgi->param("phone_number"));
  if ($phone_number !~ /^\d{10}$/) {
    die "show_page: send_sms_bad_phone_number.html\n";
  }
  my $carrier = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("carrier")));
  my $to_email = "";
  if ($carrier eq "Alltell") {
    $to_email = "message.alltel.com";
  } elsif ($carrier eq "AT&T") {
    $to_email = "txt.att.net";
  } elsif ($carrier eq "MetroPCS") {
    $to_email = "mymetropcs.com";
  } elsif ($carrier eq "Nextel") {
    $to_email = "messaging.nextel.com";
  } elsif ($carrier eq "Powertel") {
    $to_email = "ptel.net";
  } elsif ($carrier eq "Sprint") {
    $to_email = "messaging.sprintpcs.com";
  } elsif ($carrier eq "SunCom") {
    $to_email = "tms.suncom.com";
  } elsif ($carrier eq "T-Mobile") {
    $to_email = "tmomail.net";
  } elsif ($carrier eq "US Cellular") {
    $to_email = "email.uscc.net";
  } elsif ($carrier eq "Verizon") {
    $to_email = "vtext.com";
  } elsif ($carrier eq "Virgin Mobile") {
    $to_email = "vmobl.com";
  } else {
    die "show_page: send_sms_bad_carrier.html\n";
  }
  $to_email = $phone_number . "\@" . $to_email;
  send_email
    ("from" => $from_email,
     "to" => $to_email,
     "message" => $message);
  # populate information needed for display of page.
  $from_email = HTML::Entities::encode($from_email);
  $message = HTML::Entities::encode($message);
  $carrier = HTML::Entities::encode($carrier);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <sms>
    <from_email>$from_email</from_email>
    <message>$message</message>
    <phone_number>$phone_number</phone_number>
    <carrier>$carrier</carrier>
  </sms>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "send_sms_result.xslt", $cgi);
}

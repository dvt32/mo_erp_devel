<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Send SMS
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a> |
	  <a href="/1g/telecom/show_calls.cgi">Active Calls</a> |
	  <a href="/1g/telecom/show_sofia_status.cgi">SIP Registrations</a> |
	  <a href="/1g/telecom/send_sms_input.cgi">SMS</a>
	</p>
	<xsl:apply-templates select="sms"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sms">
    <p>
      You sent the following SMS:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  From Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="from_email"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Message:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="message"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Phone Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Carrier:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="carrier"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

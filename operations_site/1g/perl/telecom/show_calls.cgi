#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use RPC::XML::Client;
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $client = RPC::XML::Client->new("http://jojo.microoffice.com:8080/RPC2");
  my $request = RPC::XML::request->new("freeswitch.api", "show", "calls as xml");
  $client->credentials("freeswitch", "mo_operator", "e8hYVFqCl");
  my $response = $client->send_request($request);
  my $response_content = $response->value();
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
---END_BLOCK---;
  $generated_xml .= $response_content;
  $generated_xml .= <<"---END_BLOCK---;";
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_calls.xslt", $cgi);
}

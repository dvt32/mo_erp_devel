<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Active Calls
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main Menu</a> | <a href="/1g/telecom/show_sofia_status.cgi">SIP Registrations</a> |
	  <a href="/1g/telecom/send_sms_input.cgi">SMS</a>
	</p>
	<xsl:apply-templates/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="result">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="15" class="standard_table_title">
	  Active Calls (<xsl:value-of select="@row_count"/> total)
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Call Created</td>
	<td class="standard_table_spacer_column"></td>
	<td>Caller CID Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Caller CID #</td>
	<td class="standard_table_spacer_column"></td>
	<td>Caller Dest #</td>
	<td class="standard_table_spacer_column"></td>
	<td>Callee CID Name</td>
	<td class="standard_table_spacer_column"></td>
	<td>Callee CID #</td>
	<td class="standard_table_spacer_column"></td>
	<td>Callee Dest #</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="row"/>
    </table>
  </xsl:template>

  <xsl:template match="row">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<xsl:value-of select="call_created"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="caller_cid_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="caller_cid_num"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="caller_dest_num"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="callee_cid_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="callee_cid_num"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(callee_dest_num) > 30">
	    <xsl:value-of select="substring(callee_dest_num, 1, 30)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="callee_dest_num"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

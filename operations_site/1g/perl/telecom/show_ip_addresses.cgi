#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch ip addresses.
  my (%ip_address_by_id, @ip_address_id_list);
  get_row_by_id_map($dbh, "select * from ip_address_tbl order by ip_address", undef, \%ip_address_by_id, \@ip_address_id_list);
  # fetch accounts.
  my %account_by_id;
  my $sql_query = "select * from account_vw where id in (select account_id from ip_address_tbl)";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl where is_active", undef, \%building_by_id);
  # fetch ISPs.
  my %isp_by_id;
  get_row_by_id_map($dbh, "select * from isp_tbl", undef, \%isp_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <ip-address-list>
---END_BLOCK---;
  for my $ip_address_id (@ip_address_id_list) {
    my $ip_address = $ip_address_by_id{$ip_address_id};
    my $building_id = $ip_address->{"building_id"};
    my $building = $building_by_id{$building_id};
    my $building_label = HTML::Entities::encode($building->{"short_global_label"});
    my $isp_id = $ip_address->{"isp_id"};
    my $isp = $isp_by_id{$isp_id};
    my $isp_name = HTML::Entities::encode($isp->{"short_name"});
    my $account_id = $ip_address->{"account_id"};
    my $account_name = "";
    if (defined($account_id)) {
      my $account = $account_by_id{$account_id};
      if ($account) {
	$account_name = $account->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $address = $ip_address->{"ip_address"};
    my $hostname = HTML::Entities::encode($ip_address->{"hostname"} || "");
    my $note = HTML::Entities::encode($ip_address->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <ip-address>
      <id>$ip_address_id</id>
      <building-label>$building_label</building-label>
      <isp_name>$isp_name</isp_name>
      <account-name>$account_name</account-name>
      <address>$address</address>
      <hostname>$hostname</hostname>
      <note>$note</note>
    </ip-address>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ip-address-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_ip_addresses.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: IP Addresses
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
<pre>
Other network details.

902 Broadway:
IP addresses: 198.176.46.34 - 46
subnet mask: 255.255.255.240
gateway: 198.176.46.33
primary DNS: 198.176.46.33
secondary DNS: 8.8.8.8

122 W. 27th St.:
IP addresses: 162.208.109.2 - 254
subnet mask: 255.255.255.0
gateway: 162.208.109.1
primary DNS: 162.208.109.1
secondary DNS: 8.8.8.8

1375 Broadway:
IP addresses: 162.208.108.2 - 254
subnet mask: 255.255.255.0
gateway: 162.208.108.1
primary DNS: 162.208.108.1
secondary DNS: 8.8.8.8

1601 Broadway:
IP addresses: 50.74.183.154 - 158
subnet mask: 255.255.255.248
gateway: 50.74.183.153
primary DNS: 24.29.99.35
secondary DNS: 24.29.99.36

318 W 118th St:
IP addresses: 108.21.89.34
subnet mask: 255.255.255.252
gateway: 108.21.89.33
primary DNS: 68.237.161.12
secondary DNS: 71.250.0.12
</pre>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ip-address-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="15" class="standard_table_title">
          IP Addresses
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>IP Address</td>
	<td class="standard_table_spacer_column"></td>
	<td>Building</td>
	<td class="standard_table_spacer_column"></td>
	<td>ISP</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Person</td>
	<td class="standard_table_spacer_column"></td>
	<td>Hostname</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="ip-address"/>
    </table>
  </xsl:template>

  <xsl:template match="ip-address">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/telecom/edit_ip_address_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="address"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="building-label"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="isp_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(account-name) > 32">
	    <xsl:value-of select="substring(account-name, 1, 32)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="account-name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(person-name) > 24">
	    <xsl:value-of select="substring(person-name, 1, 24)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="person-name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(hostname) > 48">
	    <xsl:value-of select="substring(hostname, 1, 48)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="hostname"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(note) > 48">
	    <xsl:value-of select="substring(note, 1, 48)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="note"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

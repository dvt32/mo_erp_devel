#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI parameters for filter and sort.
  my $filter_manufacturer = $cgi->param("manufacturer") || "";
  my @sort_order_list;
  for(my $i = 1; $i <= 3; $i++) {
    if ($cgi->param("sort_$i")) {
      push(@sort_order_list, $cgi->param("sort_$i"));
    }
  }
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # fetch ip_phone_tbl rows.
  my %ip_phone_by_id;
  my (@sql_where_clause_list, @sql_param_list);
  if ($filter_manufacturer) {
    push(@sql_where_clause_list, "manufacturer = ?");
    push(@sql_param_list, $filter_manufacturer);
  }
  my $sql_where_clause = scalar(@sql_where_clause_list) ? " where " . join(" and ", @sql_where_clause_list) : "";
  my $sql_query = "select * from ip_phone_tbl$sql_where_clause";
  get_row_by_id_map($dbh_phone, $sql_query, \@sql_param_list, \%ip_phone_by_id);
  # sort the phone number entries.
  if (!@sort_order_list) {
    @sort_order_list = ("manufacturer", "model", "mac_address");
  }
  my $sort_func = sub {
    my $sort_result = 0;
    for(my $i = 0; $i < @sort_order_list; $i++) {
      my $sort_column = $sort_order_list[$i];
      my $ip_phone_a = $ip_phone_by_id{$a};
      my $ip_phone_b = $ip_phone_by_id{$b};
      if ($sort_column eq "manufacturer") {
	$sort_result = $ip_phone_a->{$sort_column} cmp $ip_phone_b->{$sort_column};
      } elsif ($sort_column eq "model") {
	$sort_result = $ip_phone_a->{$sort_column} cmp $ip_phone_b->{$sort_column};
      } elsif ($sort_column eq "mac_address") {
	$sort_result = $ip_phone_a->{$sort_column} cmp $ip_phone_b->{$sort_column};
      }
      if ($sort_result != 0) {
	last;
      }
    }
    return $sort_result;
  };
  my @ip_phone_id_list = sort {
    $sort_func->($a, $b)
  } keys(%ip_phone_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <current_manufacturer>$filter_manufacturer</current_manufacturer>
  <manufacturer_list>
    <manufacturer>cisco</manufacturer>
    <manufacturer>grandstream</manufacturer>
    <manufacturer>linksys</manufacturer>
    <manufacturer>polycom</manufacturer>
    <manufacturer>snom</manufacturer>
  </manufacturer_list>
  <current_sort_list>
---END_BLOCK---;
  for(my $i = 0; $i < @sort_order_list; $i++) {
    my $sort_column = $sort_order_list[$i];
    $generated_xml .= <<"---END_BLOCK---;";
    <sort_column>$sort_column</sort_column>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </current_sort_list>
  <sort_column_list>
    <sort_column>$sort_order_list[0]</sort_column>
    <sort_column>$sort_order_list[1]</sort_column>
    <sort_column>$sort_order_list[2]</sort_column>
  </sort_column_list>
  <ip_phone_list>
---END_BLOCK---;
  for my $ip_phone_id (@ip_phone_id_list) {
    my $ip_phone = $ip_phone_by_id{$ip_phone_id};
    my $manufacturer = HTML::Entities::encode($ip_phone->{"manufacturer"});
    my $model = HTML::Entities::encode($ip_phone->{"model"});
    my $mac_address = HTML::Entities::encode($ip_phone->{"mac_address"});
    my $ownership = HTML::Entities::encode($ip_phone->{"ownership"});
    my $note = HTML::Entities::encode($ip_phone->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <ip_phone>
      <id>$ip_phone_id</id>
      <manufacturer>$manufacturer</manufacturer>
      <model>$model</model>
      <mac_address>$mac_address</mac_address>
      <ownership>$ownership</ownership>
      <note>$note</note>
    </ip_phone>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ip_phone_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_ip_phones.xslt", $cgi);
}

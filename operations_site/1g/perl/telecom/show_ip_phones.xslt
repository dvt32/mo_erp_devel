<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: IP Phones
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">main menu</a> |
	  <a href="/1g/telecom/new_ip_phone_input.cgi">new IP phone</a>
	</p>
	<xsl:apply-templates select="ip_phone_list"/>
	<form action="/1g/telecom/show_ip_phones.cgi" method="get">
	  <table>
	    <tr>
	      <td colspan="2" style="background-color: orange;">Filter</td>
	    </tr>
	    <tr>
	      <td>Manufacturer:</td>
	      <td>
		<select name="manufacturer">
		  <option value="">(all)</option>
		  <xsl:for-each select="/page/manufacturer_list/manufacturer">
		    <option>
		      <xsl:if test="/page/current_manufacturer = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td colspan="2" style="background-color: orange;">Sort</td>
	    </tr>
	    <tr>
	      <td>Sort 1:</td>
	      <td>
		<select name="sort_1">
		  <option value="">(none)</option>
		  <xsl:for-each select="/page/sort_column_list/sort_column">
		    <option>
		      <xsl:if test="/page/current_sort_list/sort_column[1] = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td>Sort 2:</td>
	      <td>
		<select name="sort_2">
		  <option value="">(none)</option>
		  <xsl:for-each select="/page/sort_column_list/sort_column">
		    <option>
		      <xsl:if test="/page/current_sort_list/sort_column[2] = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td>Sort 3:</td>
	      <td>
		<select name="sort_3">
		  <option value="">(none)</option>
		  <xsl:for-each select="/page/sort_column_list/sort_column">
		    <option>
		      <xsl:if test="/page/current_sort_list/sort_column[3] = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	  </table>
	  <input type="submit" name="submitButton" value="Apply Filter and Sort"/>
	</form>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ip_phone_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="11" class="standard_table_title">
	  IP Phones
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>MAC Address</td>
	<td class="standard_table_spacer_column"></td>
	<td>Manufacturer</td>
	<td class="standard_table_spacer_column"></td>
	<td>Model</td>
	<td class="standard_table_spacer_column"></td>
	<td>Ownership</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_spacer_column"></td>
      </tr>
      <xsl:apply-templates select="ip_phone"/>
    </table>
  </xsl:template>

  <xsl:template match="ip_phone">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/telecom/edit_ip_phone_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:attribute name="style">font-family: monospace;</xsl:attribute>
	  <xsl:value-of select="mac_address"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="manufacturer"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="model"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="ownership"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(note) > 48">
	    <xsl:value-of select="substring(note, 1, 48)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="note"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get CGI parameters for filter and sort.
  my $filter_device_type = $cgi->param("device_type") || "";
  my $filter_floor_id = $cgi->param("floor_id") || "";
  my @sort_order_list;
  for(my $i = 1; $i <= 3; $i++) {
    if ($cgi->param("sort_$i")) {
      push(@sort_order_list, $cgi->param("sort_$i"));
    }
  }
  my $view = $cgi->param("view") || ""; # mobile or desktop (desktop is default if blank)
  my $dbh_phone = get_database_handle($instance_map, "phone");
  # fetch phone numbers.
  my (%phone_number_by_id, @phone_number_id_list);
  my (@sql_where_clause_list, @sql_param_list);
  if ($filter_device_type) {
    push(@sql_where_clause_list, "device_type = ?");
    push(@sql_param_list, $filter_device_type);
  }
  if ($filter_floor_id) {
    if ($filter_floor_id == -1) {
      push(@sql_where_clause_list, "floor_id is null");
    } else {
      push(@sql_where_clause_list, "floor_id = ?");
      push(@sql_param_list, [$filter_floor_id, SQL_INTEGER]);
    }
  }
  my $sql_where_clause = scalar(@sql_where_clause_list) ? " where " . join(" and ", @sql_where_clause_list) : "";
  get_row_by_id_map($dbh_phone, "select * from phone_number_tbl$sql_where_clause", \@sql_param_list, \%phone_number_by_id);
  # fetch iad_tbl rows.
  my %iad_by_id;
  my $sql_query = "select * from iad_tbl";
  get_row_by_id_map($dbh_phone, $sql_query, undef, \%iad_by_id);
  # fetch ip_phone_tbl rows.
  my %ip_phone_by_id;
  my $sql_query = "select * from ip_phone_tbl";
  get_row_by_id_map($dbh_phone, $sql_query, undef, \%ip_phone_by_id);
  # fetch accounts.
  my %account_by_id;
  my $sql_query = "select * from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # fetch cust_acct rows (for customer account attributes).
  my %cust_acct_by_id;
  my $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch persons.
  my %person_by_id;
  $sql_query = "select * from person_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%person_by_id);
  # fetch services.
  my %service_by_id;
  $sql_query = "select * from service_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%service_by_id);
  # fetch floors.
  my (%floor_by_id, @floor_id_list);
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active order by short_global_label", undef, \%floor_by_id, \@floor_id_list);
  # sort the phone number entries.
  if (!@sort_order_list) {
    @sort_order_list = ("phone_number");
  }
  my $sort_func = sub {
    my $sort_result = 0;
    for(my $i = 0; $i < @sort_order_list; $i++) {
      my $sort_column = $sort_order_list[$i];
      my $phone_number_a = $phone_number_by_id{$a};
      my $phone_number_b = $phone_number_by_id{$b};
      if ($sort_column eq "phone_number") {
	$sort_result = $phone_number_a->{"phone_number"} cmp $phone_number_b->{"phone_number"};
      } elsif ($sort_column eq "device") {
	my ($device_type_a, $device_type_b) = ($phone_number_a->{"device_type"}, $phone_number_b->{"device_type"});
	my ($device_name_a, $device_name_b) = ("", "");
	my $device_port_a = defined($phone_number_a->{"device_port"}) ? $phone_number_a->{"device_port"} : -1;
	my $device_port_b = defined($phone_number_b->{"device_port"}) ? $phone_number_b->{"device_port"} : -1;
	if ($device_type_a eq "iad") {
	  if ($phone_number_a->{"iad_id"}) {
	    my $iad = $iad_by_id{$phone_number_a->{"iad_id"}};
	    if ($iad) {
	      $device_name_a = $iad->{"device_name"};
	    }
	  }
	} elsif ($device_type_a eq "ip_phone") {
	  if ($phone_number_a->{"ip_phone_id"}) {
	    my $ip_phone = $ip_phone_by_id{$phone_number_a->{"ip_phone_id"}};
	    if ($ip_phone) {
	      $device_name_a = $ip_phone->{"manufacturer"} . " " . $ip_phone->{"model"} . " - " . $ip_phone->{"mac_address"};
	    }
	  }
	}
	if ($device_type_b eq "iad") {
	  if ($phone_number_b->{"iad_id"}) {
	    my $iad = $iad_by_id{$phone_number_b->{"iad_id"}};
	    if ($iad) {
	      $device_name_b = $iad->{"device_name"};
	    }
	  }
	} elsif ($device_type_b eq "ip_phone") {
	  if ($phone_number_b->{"ip_phone_id"}) {
	    my $ip_phone = $ip_phone_by_id{$phone_number_b->{"ip_phone_id"}};
	    if ($ip_phone) {
	      $device_name_b = $ip_phone->{"manufacturer"} . " " . $ip_phone->{"model"} . " - " . $ip_phone->{"mac_address"};
	    }
	  }
	}
	$sort_result = $device_name_a cmp $device_name_b;
	$sort_result ||= $device_port_a <=> $device_port_b;
      } elsif ($sort_column eq "floor") {
	my $floor_a = $floor_by_id{$phone_number_a->{"floor_id"}};
	my $floor_b = $floor_by_id{$phone_number_b->{"floor_id"}};
	$sort_result = $floor_a->{"short_global_label"} cmp $floor_b->{"short_global_label"};
      } elsif ($sort_column eq "account") {
	my $account_a = $phone_number_a->{"account_id"} ? $account_by_id{$phone_number_a->{"account_id"}} : undef;
	my $account_name_a = $account_a ? $account_a->{"name"} : "";
	my $account_b = $phone_number_b->{"account_id"} ? $account_by_id{$phone_number_b->{"account_id"}} : undef;
	my $account_name_b = $account_b ? $account_b->{"name"} : "";
	$sort_result = $account_name_a cmp $account_name_b;
      } elsif ($sort_column eq "person") {
	my $person_a = $phone_number_a->{"person_id"} ? $person_by_id{$phone_number_a->{"person_id"}} : undef;
	my $person_name_a = "";
	if ($person_a) {
	  $person_name_a = $person_a->{"last_name"} . ", " . $person_a->{"first_name"};
	  if ($person_a->{"middle_name"}) {
	    $person_name_a .= " " . $person_a->{"middle_name"};
	  }
	}
	my $person_b = $phone_number_b->{"person_id"} ? $person_by_id{$phone_number_b->{"person_id"}} : undef;
	my $person_name_b = "";
	if ($person_b) {
	  $person_name_b = $person_b->{"last_name"} . ", " . $person_b->{"first_name"};
	  if ($person_b->{"middle_name"}) {
	    $person_name_b .= " " . $person_b->{"middle_name"};
	  }
	}
	$sort_result = $person_name_a cmp $person_name_b;
      }
      if ($sort_result != 0) {
	last;
      }
    }
    return $sort_result;
  };
  @phone_number_id_list = sort {
    $sort_func->($a, $b)
  } keys(%phone_number_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <current-device-type>$filter_device_type</current-device-type>
  <current-floor-id>$filter_floor_id</current-floor-id>
  <device-type-list>
    <device-type>iad</device-type>
    <device-type>ip_phone</device-type>
  </device-type-list>
  <current-sort-list>
---END_BLOCK---;
  for(my $i = 0; $i < @sort_order_list; $i++) {
    my $sort_column = $sort_order_list[$i];
    $generated_xml .= <<"---END_BLOCK---;";
    <sort-column>$sort_column</sort-column>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </current-sort-list>
  <sort-column-list>
    <sort-column>phone_number</sort-column>
    <sort-column>device</sort-column>
    <sort-column>floor</sort-column>
    <sort-column>account</sort-column>
    <sort-column>person</sort-column>
  </sort-column-list>
  <phone-number-list>
---END_BLOCK---;
  for my $phone_number_id (@phone_number_id_list) {
    my $phone_number = $phone_number_by_id{$phone_number_id};
    my $floor_id = $phone_number->{"floor_id"};
    my $floor_label = "";
    if ($floor_id) {
      my $floor = $floor_by_id{$floor_id};
      if ($floor) {
	$floor_label = HTML::Entities::encode($floor->{"short_global_label"});
      }
    }
    my $account_id = $phone_number->{"account_id"};
    my $account_name = "";
    my $service_name = "";
    if (defined($account_id)) {
      my $account = $account_by_id{$account_id};
      if ($account) {
	$account_name = $account->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
      if (my $cust_acct = $cust_acct_by_id{$account_id}) {
	my $service = $service_by_id{$cust_acct->{"service_id"}};
	$service_name = HTML::Entities::encode($service->{"name"});
      }
    }
    my $person_id = $phone_number->{"person_id"};
    my $person_name = "";
    if (defined($person_id)) {
      my $person = $person_by_id{$person_id};
      if ($person) {
	$person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
	if ($person->{"middle_name"}) {
	  $person_name .= " " . $person->{"middle_name"};
	}
      } else {
	$person_name = "(inactive person)";
      }
      $person_name = HTML::Entities::encode($person_name);
    }
    my $number = HTML::Entities::encode($phone_number->{"phone_number"});
    my $device_type = HTML::Entities::encode($phone_number->{"device_type"});
    my $device_name = "";
    if ($device_type eq "iad") {
      if ($phone_number->{"iad_id"}) {
	my $iad = $iad_by_id{$phone_number->{"iad_id"}};
	if ($iad) {
	  $device_name = HTML::Entities::encode($iad->{"device_name"} . " port " . $phone_number->{"device_port"});
	}
      }
    } elsif ($device_type eq "ip_phone") {
      if ($phone_number->{"ip_phone_id"}) {
	my $ip_phone = $ip_phone_by_id{$phone_number->{"ip_phone_id"}};
	if ($ip_phone) {
	  $device_name = HTML::Entities::encode($ip_phone->{"manufacturer"} . " " . $ip_phone->{"model"} . " - " . $ip_phone->{"mac_address"});
	}
      }
    }
    my $note = HTML::Entities::encode($phone_number->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <phone-number>
      <id>$phone_number_id</id>
      <floor-label>$floor_label</floor-label>
      <account-name>$account_name</account-name>
      <person-name>$person_name</person-name>
      <service_name>$service_name</service_name>
      <number>$number</number>
      <device_type>$device_type</device_type>
      <device_name>$device_name</device_name>
      <note>$note</note>
    </phone-number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </phone-number-list>
  <floor-list>
    <floor>
      <id>-1</id>
      <name>(unlinked)</name>
    </floor>
---END_BLOCK---;
  for my $floor_id (@floor_id_list) {
    my $floor = $floor_by_id{$floor_id};
    my $floor_name = HTML::Entities::encode($floor->{"short_global_label"});
    $generated_xml .= <<"---END_BLOCK---;";
    <floor>
      <id>$floor_id</id>
      <name>$floor_name</name>
    </floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </floor-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  my $xslt_file = ($view eq "mobile") ? "show_phone_numbers_mobile_view.xslt" : "show_phone_numbers.xslt";
  apply_xslt_output_xhtml($generated_xml, $xslt_file, $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Phone Numbers
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="phone-number-list"/>
	<form action="/1g/telecom/show_phone_numbers.cgi" method="get">
	  <table>
	    <tr>
	      <td colspan="2" style="background-color: orange;">Filter</td>
	    </tr>
	    <tr>
	      <td>Device Type:</td>
	      <td>
		<select name="device_type">
		  <option value="">(all)</option>
		  <xsl:for-each select="/page/device-type-list/device-type">
		    <option>
		      <xsl:if test="/page/current-device-type = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td>Floor:</td>
	      <td>
		<select name="floor_id">
		  <option value="">(all)</option>
		  <xsl:for-each select="/page/floor-list/floor">
		    <option>
		      <xsl:if test="/page/current-floor-id = id">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
		      <xsl:value-of select="name"/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td colspan="2" style="background-color: orange;">Sort</td>
	    </tr>
	    <tr>
	      <td>Sort 1:</td>
	      <td>
		<select name="sort_1">
		  <option value="">(none)</option>
		  <xsl:for-each select="/page/sort-column-list/sort-column">
		    <option>
		      <xsl:if test="/page/current-sort-list/sort-column[1] = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td>Sort 2:</td>
	      <td>
		<select name="sort_2">
		  <option value="">(none)</option>
		  <xsl:for-each select="/page/sort-column-list/sort-column">
		    <option>
		      <xsl:if test="/page/current-sort-list/sort-column[2] = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td>Sort 3:</td>
	      <td>
		<select name="sort_3">
		  <option value="">(none)</option>
		  <xsl:for-each select="/page/sort-column-list/sort-column">
		    <option>
		      <xsl:if test="/page/current-sort-list/sort-column[3] = .">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:value-of select="."/>
		    </option>
		  </xsl:for-each>
		</select>
	      </td>
	    </tr>
	  </table>
	  <input type="submit" name="submitButton" value="Apply Filter and Sort"/>
	</form>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="phone-number-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
          Phone Numbers
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Phone Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Device</td>
	<td class="standard_table_spacer_column"></td>
	<td>Building &amp; Floor</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Person</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="phone-number"/>
    </table>
  </xsl:template>

  <xsl:template match="phone-number">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/telecom/edit_phone_number_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="number"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="device_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="floor-label"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(account-name) > 32">
	    <xsl:value-of select="substring(account-name, 1, 32)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="account-name"/>
	  </xsl:otherwise>
	</xsl:choose>
	<xsl:if test="service_name = 'virtual'">
	  <xsl:text> </xsl:text><span style="font-weight: bold">[V]</span>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(person-name) > 24">
	    <xsl:value-of select="substring(person-name, 1, 24)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="person-name"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:choose>
	  <xsl:when test="string-length(note) > 48">
	    <xsl:value-of select="substring(note, 1, 48)"/>...
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="note"/>
	  </xsl:otherwise>
	</xsl:choose>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

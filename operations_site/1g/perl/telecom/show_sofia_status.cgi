#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use LWP::UserAgent;
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $user_agent = LWP::UserAgent->new();
  $user_agent->credentials("jojo.microoffice.com:8080", "freeswitch", "mo_operator", "e8hYVFqCl");
  $user_agent->cookie_jar({});
  my $url = "http://jojo.microoffice.com:8080/webapi/sofia?status%20profile%20internal";
  my $response = $user_agent->get($url);
  my $response_content = $response->content();
  my $mime_type = "text/plain";
  print $cgi->header("-type" => $mime_type);
  print $response_content;
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that account id was specified.
  my $cust_acct_id = $cgi->param("id");
  if ((!defined($cust_acct_id)) || ($cust_acct_id !~ /^\d+$/)) {
    die "show_page: activate_account_not_found.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the account exists.
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  if (!$cust_acct) {
    $dbh->rollback();
    die "show_page: activate_account_not_found.html\n";
  }
  my @sql_column_list;
  push(@sql_column_list, ["is_active", 1, SQL_BOOLEAN]);
  update_row($dbh, "cust_acct_tbl", \@sql_column_list, $cust_acct_id);
  # commit the transaction.
  $dbh->commit();
  # load rows needed for display of results.
  my $cust_pers_id = $cust_acct->{"primary_contact_cust_pers_id"};
  my $cust_pers;
  if (defined($cust_pers_id)) {
    $cust_pers = select_row($dbh, "cust_pers_tbl", $cust_pers_id);
  }
  my $floor_id = $cust_acct->{"primary_floor_id"};
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  my $service_id = $cust_acct->{"service_id"};
  my $service = select_row($dbh, "service_tbl", $service_id);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $external_name = HTML::Entities::encode($cust_acct->{"external_name"});
  my $person_name;
  if ($cust_pers) {
    $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
  }
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  my $service_name = HTML::Entities::encode($service->{"name"});
  my $description = HTML::Entities::encode($cust_acct->{"description"} || "");
  my $web_site = HTML::Entities::encode($cust_acct->{"web_site"} || "");
  my $note = HTML::Entities::encode($cust_acct->{"note"} || "");
  my $tax_id = HTML::Entities::encode($cust_acct->{"tax_id"} || "");
  my $preferred_payment_method = HTML::Entities::encode($cust_acct->{"preferred_payment_method"} || "");
  my $is_visible = $cust_acct->{"visible"} ? 1 : 0;
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account>
    <account-name>$account_name</account-name>
    <external-name>$external_name</external-name>
    <person-name>$person_name</person-name>
    <floor-label>$floor_label</floor-label>
    <service-name>$service_name</service-name>
    <description>$description</description>
    <web-site>$web_site</web-site>
    <note>$note</note>
    <tax-id>$tax_id</tax-id>
    <preferred-payment-method>$preferred_payment_method</preferred-payment-method>
    <is-visible>$is_visible</is-visible>
  </account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "activate_account_success.xslt", $cgi);
}

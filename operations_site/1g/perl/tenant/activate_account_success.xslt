<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Activate Account
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="account"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account">
    <p>
      You successfully activated the following account:
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Account Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  External Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="external-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Primary Contact Person Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="person-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Building and Floor:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="floor-label"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Service:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="service-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Description:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="description"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Web Site:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="web-site"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Tax ID:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="tax-id"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Preferred Payment Method:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="preferred-payment-method"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Is Visible?:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is-visible = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use get_row_by_id_map;
use init_login_standard;
use apply_xslt_output_xhtml;
use main_exception_wrapper;
use timestamp_to_components;
use local_datetime_to_epoch_time;
use get_row_list;

main_exception_wrapper(\&main); 

sub main() {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);

  my @data;
  my $sql_query = "select account_vw.name, sum(desk_tbl.current_price) as current_price, sum(desk_tbl.list_price) as list_price, concatenate(desk_tbl.note) as note, min(desk_tbl.start_date) as start_date, max(desk_tbl.end_date) as end_date, min(desk_tbl.increase_date) as increase_date, sum(desk_tbl.internet_telephone_revenue) as internet_telephone_revenue from account_vw, desk_tbl where account_vw.id = desk_tbl.account_id and desk_tbl.is_active = true group by account_vw.name order by account_vw.name ASC";
  get_row_list($dbh,$sql_query,undef,\@data);
    
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <active-account-list>
---END_BLOCK---;
  
  for my $data (@data){
  	my $account_name = HTML::Entities::encode($data->{"name"});
	  my $current_price = currency_format($data->{"current_price"});
	  my $list_price = currency_format($data->{"list_price"});
	  my $internet_telephone_price = currency_format($data->{"internet_telephone_revenue"});
	  my $note = HTML::Entities::encode($data->{"note"});
	  my $start_date = '';
	  my $end_date = '';
	  my $increase_date = '';
	  if($data->{"start_date"}){
  		my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($data->{"start_date"} . ' 00:00:00');
  		$start_date = "$month/$day/$year";
	  }
	  if($data->{"end_date"}){
  		my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($data->{"end_date"}. ' 00:00:00');
  		$end_date = "$month/$day/$year";
	  }
	  if($data->{"increase_date"}){
  		my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($data->{"increase_date"} . ' 00:00:00');
  		$increase_date = "$month/$day/$year";
	  }

	  $generated_xml .= <<"---END_BLOCK---;";
	  	<active-account>
	  		<account-name>$account_name</account-name>
	  		<current-price>$current_price</current-price>
	  		<list-price>$list_price</list-price>
	  		<start-date>$start_date</start-date>
	  		<internet-telephone-price>$internet_telephone_price</internet-telephone-price>
	  		<end-date>$end_date</end-date>
	  		<increase-date>$increase_date</increase-date>
	  		<notes>$note</notes>
	  	</active-account>
---END_BLOCK---;
  }
  
  $generated_xml .= <<"---END_BLOCK---;";
  </active-account-list>
</page>
---END_BLOCK---;

  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "active_account_terms.xslt", $cgi);
}

sub currency_format($) {
	my $amount = sprintf "%.2f", shift @_;
  # add one comma each time through the do-nothing loop
  1 while $amount =~ s/^(-?\d+)(\d\d\d)/$1,$2/;
  # put the dollar sign in the right place
  $amount =~ s/^(-?)/$1\$/;
	return $amount;
}

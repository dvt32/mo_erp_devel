<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:template match="page">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<title>Micro Office Solutions :: Operations Center :: Active Account Terms</title>
			<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
		</head>
		<body>
			<p>
				<a href="/1g/index.cgi">Main Menu</a>
			</p>
			<xsl:apply-templates select="active-account-list"/>
			<p>
				<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
			</p>
		</body>
	</html>
</xsl:template>

<xsl:template match="active-account-list">
	<table cellspacing="0" cellpadding="0" border="0" class="standard_table">
		<tr>
			<td colspan="17" class="standard_table_title">Active Account Terms</td>
		</tr>
		<tr class="standard_table_column_heading_row">
			<td class="standard_table_side_padding"></td>
			<td>Account Name</td>
			<td class="standard_table_side_padding"></td>
			<td>Current Price</td>
			<td class="standard_table_spacer_column"></td>
			<td>List Price</td>
			<td class="standard_table_spacer_column"></td>
			<td>Internet/Telephone Price</td>
			<td class="standard_table_spacer_column"></td>
			<td>Start Date</td>
			<td class="standard_table_side_padding"></td>
			<td>End Date</td>
			<td class="standard_table_side_padding"></td>
			<td>Increase Date</td>
			<td class="standard_table_side_padding"></td>
			<td>Notes</td>
			<td class="standard_table_side_padding"></td>
		</tr>
		<xsl:apply-templates select="active-account"/>
	</table>
</xsl:template>

<xsl:template match="active-account">
	<tr valign="top">
		<xsl:choose>
			<xsl:when test="position() mod 2 = 0">
				<xsl:attribute name="class">standard_table_row_even</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<td class="standard_table_side_padding"></td>
		<td>
			<xsl:value-of select="account-name"/>
		</td>
		<td class="standard_table_side_padding"></td>
		<td align="right">
			<xsl:value-of select="current-price"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="list-price"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="internet-telephone-price"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="start-date"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="end-date"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td align="right">
			<xsl:value-of select="increase-date"/>
		</td>
		<td class="standard_table_spacer_column"></td>
		<td>
			<xsl:value-of select="notes"/>
		</td>
		<td class="standard_table_spacer_column"></td>
	</tr>
</xsl:template>

<xsl:template match="@*|node()" priority="-1">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()"/>
	</xsl:copy>
</xsl:template>
</xsl:stylesheet>

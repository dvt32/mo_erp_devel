<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Batch Add Guests
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="form"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="form">
    <table>
      <tr><td>
	<form action="/1g/tenant/batch_add_guest_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Batch Add New Guests
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">num_guests</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="count(guest_entry)"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td colspan="2" style="padding: 4px; background-color: orange; font-weight: bold; text-align: center;">
		  Host
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account:
		</td>
		<td>
                  <select name="cust_acct_id">
		    <xsl:for-each select="/page/account_list/account">
		      <option>
			<xsl:if test="/page/form/account_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Host Contact:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">host_contact</xsl:attribute>
		    <xsl:attribute name="size">32</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="host_contact"/></xsl:attribute>
		  </input>
		  (name and phone)
		</td>
	      </tr>
	      <tr>
		<td colspan="2" style="padding: 4px; background-color: orange; font-weight: bold; text-align: center;">
		  Date and Note
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Start Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>start_date_month</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/month_list/month">
		      <option>
			<xsl:if test="/page/current_date/month = number">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>start_date_day</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/day_list/day">
		      <option>
			<xsl:if test="/page/current_date/day = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">
		      <xsl:text>start_date_year</xsl:text>
		    </xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="/page/current_date/year"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  End Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>end_date_month</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/month_list/month">
		      <option>
			<xsl:if test="/page/current_date/month = number">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>end_date_day</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/day_list/day">
		      <option>
			<xsl:if test="/page/current_date/day = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">
		      <xsl:text>end_date_year</xsl:text>
		    </xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="/page/current_date/year"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td colspan="2" style="padding: 4px; background-color: orange; font-weight: bold; text-align: center;">
		  Guests
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  Salutatio
		  <input type="text" name="heading_first_name" value="First Name" readonly="1"/>
		  <input type="text" name="heading_last_name" value="Last Name" readonly="1"/>
		</td>
	      </tr>
	      <xsl:for-each select="guest_entry">
		<tr>
		  <td style="padding: 4px; ">
		    Guest <xsl:value-of select="position()"/>:
		  </td>
		  <td>
		    <select>
		      <xsl:attribute name="name">salutation_<xsl:value-of select="position()"/></xsl:attribute>
		      <option value="">[---]</option>
		      <option>Mr.</option>
		      <option>Ms.</option>
		      <option>Mrs.</option>
		      <option>Miss</option>
		    </select>
		    <input>
		      <xsl:attribute name="type">text</xsl:attribute>
		      <xsl:attribute name="name">first_name_<xsl:value-of select="position()"/></xsl:attribute>
		    </input>
		    <input>
		      <xsl:attribute name="type">text</xsl:attribute>
		      <xsl:attribute name="name">last_name_<xsl:value-of select="position()"/></xsl:attribute>
		    </input>
		  </td>
		</tr>
	      </xsl:for-each>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

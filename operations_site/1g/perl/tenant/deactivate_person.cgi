#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that person id was specified.
  my $cust_pers_id = $cgi->param("id");
  if ((!defined($cust_pers_id)) || ($cust_pers_id !~ /^\d+$/)) {
    die "show_page: deactivate_person_not_found.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the person exists.
  my $cust_pers = select_row($dbh, "cust_pers_tbl", $cust_pers_id);
  if (!$cust_pers) {
    $dbh->rollback();
    die "show_page: deactivate_person_not_found.html\n";
  }
  my @sql_column_list = (["is_active", 0, SQL_BOOLEAN]);
  update_row($dbh, "cust_pers_tbl", \@sql_column_list, $cust_pers_id);
  # commit the transaction.
  $dbh->commit();
  # load rows needed for display of results.
  my $cust_acct_id = $cust_pers->{"cust_acct_id"};
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  # populate information needed for display of page.
  my $salutation = HTML::Entities::encode($cust_pers->{"salutation"});
  my $first_name = HTML::Entities::encode($cust_pers->{"first_name"});
  my $middle_name = HTML::Entities::encode($cust_pers->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($cust_pers->{"last_name"});
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $nickname = HTML::Entities::encode($cust_pers->{"nickname"} || "");
  my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
  my $work_phone_number = HTML::Entities::encode($cust_pers->{"work_phone_number"} || "");
  my $home_phone_number = HTML::Entities::encode($cust_pers->{"home_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($cust_pers->{"cell_phone_number"} || "");
  my $other_phone_number = HTML::Entities::encode($cust_pers->{"other_phone_number"} || "");
  my $directory_1 = HTML::Entities::encode($cust_pers->{"directory_1"} || "");
  my $directory_2 = HTML::Entities::encode($cust_pers->{"directory_2"} || "");
  my $directory_3 = HTML::Entities::encode($cust_pers->{"directory_3"} || "");
  my $is_registered = $cust_pers->{"register_with_building"} ? 1 : 0;
  my $note = HTML::Entities::encode($cust_pers->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <person>
    <salutation>$salutation</salutation>
    <first-name>$first_name</first-name>
    <middle-name>$middle_name</middle-name>
    <last-name>$last_name</last-name>
    <account-name>$account_name</account-name>
    <nickname>$nickname</nickname>
    <email>$email</email>
    <work-phone-number>$work_phone_number</work-phone-number>
    <home-phone-number>$home_phone_number</home-phone-number>
    <cell-phone-number>$cell_phone_number</cell-phone-number>
    <other-phone-number>$other_phone_number</other-phone-number>
    <directory-1>$directory_1</directory-1>
    <directory-2>$directory_2</directory-2>
    <directory-3>$directory_3</directory-3>
    <is-registered>$is_registered</is-registered>
    <note>$note</note>
  </person>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "deactivate_person_success.xslt", $cgi);
}

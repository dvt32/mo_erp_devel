#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that alias id was specified.
  my $alias_id = $cgi->param("id");
  if ((!defined($alias_id)) || ($alias_id !~ /^\d+$/)) {
    die "show_page: delete_alias_not_found.html\n";
  }
  # begin transaction of deleting the row.
  $dbh->begin_work();
  # verify that the alias exists.
  my $alias = select_row($dbh, "alias_tbl", $alias_id);
  if (!$alias) {
    $dbh->rollback();
    die "show_page: delete_alias_not_found.html\n";
  }
  delete_row($dbh, "alias_tbl", $alias_id);
  # commit the transaction.
  $dbh->commit();
  # load rows needed for display of results.
  my $account_id = $alias->{"account_id"};
  my $account = select_row($dbh, "cust_acct_tbl", $account_id);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $name = HTML::Entities::encode($alias->{"name"});
  my $note = HTML::Entities::encode($alias->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <alias>
    <account-name>$account_name</account-name>
    <name>$name</name>
    <note>$note</note>
  </alias>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_alias_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use delete_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that guest person id was specified.
  my $guest_person_id = $cgi->param("id");
  if ((!defined($guest_person_id)) || ($guest_person_id !~ /^\d+$/)) {
    die "show_page: delete_guest_not_found.html\n";
  }
  # begin transaction of deleting the row.
  $dbh->begin_work();
  # verify that the guest person exists.
  my $guest_person = select_row($dbh, "guest_person_tbl", $guest_person_id);
  if (!$guest_person) {
    $dbh->rollback();
    die "show_page: delete_guest_not_found.html\n";
  }
  delete_row($dbh, "guest_person_tbl", $guest_person_id);
  # commit the transaction.
  $dbh->commit();
  # load rows needed for display of results.
  my $account_id = $guest_person->{"account_id"};
  my $account = select_row($dbh, "cust_acct_tbl", $account_id);
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($account->{"name"});
  my $host_contact = HTML::Entities::encode($guest_person->{"host_contact"} || "");
  my $salutation = HTML::Entities::encode($guest_person->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($guest_person->{"first_name"});
  my $middle_name = HTML::Entities::encode($guest_person->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($guest_person->{"last_name"});
  my $suffix = HTML::Entities::encode($guest_person->{"suffix"} || "");
  my $nickname = HTML::Entities::encode($guest_person->{"nickname"} || "");
  my $title = HTML::Entities::encode($guest_person->{"title"} || "");
  my $company = HTML::Entities::encode($guest_person->{"company"} || "");
  my $work_phone_number = HTML::Entities::encode($guest_person->{"work_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($guest_person->{"cell_phone_number"} || "");
  my $email = HTML::Entities::encode($guest_person->{"email"} || "");
  my $start_date = HTML::Entities::encode($guest_person->{"start_date"} || "");
  my $end_date = HTML::Entities::encode($guest_person->{"end_date"} || "");
  my $note = HTML::Entities::encode($guest_person->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <guest_person>
    <account_name>$account_name</account_name>
    <host_contact>$host_contact</host_contact>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <nickname>$nickname</nickname>
    <title>$title</title>
    <company>$company</company>
    <work_phone_number>$work_phone_number</work_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <email>$email</email>
    <start_date>$start_date</start_date>
    <end_date>$end_date</end_date>
    <note>$note</note>
  </guest_person>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "delete_guest.xslt", $cgi);
}

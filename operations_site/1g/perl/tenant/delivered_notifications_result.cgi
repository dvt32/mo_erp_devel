#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Lingua::EN::Nums2Words;
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use get_row_list;
use get_datetime_now;
use military_time_to_am_pm;
use timestamp_to_components;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  
  my $sql_query = "SELECT * FROM mail_package_tbl where now() - interval '30 days' < date  ORDER BY date DESC";
  my @notifications;
  get_row_list($dbh, $sql_query, undef, \@notifications);
 
  my $xml = "";
  $xml .= <<__;
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
	<notification_list>
__

	# populate information needed for display of page.
  for my $notification (@notifications) {
  	my $timestamp = HTML::Entities::encode($notification->{"date"});
	my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($timestamp);
	my $date = "$month/$day/$year";
	my $time = military_time_to_am_pm("$hour:$minute");
	my $sender = HTML::Entities::encode($notification->{"sender"} || "");
  	my $recipient_email=HTML::Entities::encode($notification->{"recipient_email"} || "");
  	$recipient_email =~ s/,/, /g;
  	my $carrier = HTML::Entities::encode($notification->{"carrier"} || "");
  	my $package_type = HTML::Entities::encode($notification->{"package_type"} || "");
  	$xml .= <<__;
  	<notification>
	  	<date>$date</date>
	  	<time>$time</time>
	  	<sender>$sender</sender>
	  	<recipient_email>$recipient_email</recipient_email>
	  	<carrier>$carrier</carrier>
	  	<package_type>$package_type</package_type>
	  </notification>
__

  }
  
  $xml .= <<__;
  </notification_list>
</page>
__

  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($xml, "delivered_notifications_result.xslt", $cgi);
}

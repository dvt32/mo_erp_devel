<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Delivered Notifications
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="notification_list"/>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="notification_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
	  Delivered Notifications
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Date</td>
	<td class="standard_table_spacer_column"></td>
	<td>Time</td>
	<td class="standard_table_spacer_column"></td>
	<td>Sender</td>
	<td class="standard_table_spacer_column"></td>
	<td>Recipient Email</td>
	<td class="standard_table_side_padding"></td>
	<td>Carrier</td>
	<td class="standard_table_side_padding"></td>
	<td>Package Type</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="notification"/>
    </table>
  </xsl:template>

  <xsl:template match="notification">
    <tr valign="top">
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="right">
	<xsl:value-of select="date"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="time"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="sender"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="recipient_email"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="carrier"/>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:value-of select="package_type"/>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

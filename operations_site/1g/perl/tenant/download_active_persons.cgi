#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  my $sql_query = "select cust_pers_tbl.* from cust_pers_tbl, cust_acct_tbl where cust_pers_tbl.is_active and cust_pers_tbl.cust_acct_id = cust_acct_tbl.id and cust_acct_tbl.is_active order by cust_pers_tbl.last_name, cust_pers_tbl.first_name, cust_pers_tbl.middle_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id, \@cust_pers_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active", undef, \%floor_by_id);
  # print out data to stdout as a CSV file.
  print $cgi->header
    ("-type" => "application/vnd.ms-excel",
     "-content-disposition" => "attachment; filename=\"active_person_list.csv\"");
  my @column_headings = ("Person ID", "Salutation", "First Name", "Last Name", "Account Name", "Email", "Work Phone", "Home Phone", "Cell Phone", "Other Phone", "Floor");
  my $csv_line;
  $csv_line = join(",", map {if (/,/) {qq{"$_"}} else {$_}} @column_headings);
  print $csv_line;
  print "\n";
  for my $cust_pers_id (@cust_pers_id_list) {
    my $cust_pers = $cust_pers_by_id{$cust_pers_id};
    my $cust_acct_id = $cust_pers->{"cust_acct_id"};
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = $cust_acct->{"name"};
    my $floor = $floor_by_id{$cust_acct->{"primary_floor_id"}};
    my $floor_label = $floor->{"short_global_label"};
    my @column_values = ();
    push(@column_values, $cust_pers->{"id"});
    push(@column_values, $cust_pers->{"salutation"});
    push(@column_values, $cust_pers->{"first_name"});
    push(@column_values, $cust_pers->{"last_name"});
    push(@column_values, $account_name || "");
    push(@column_values, $cust_pers->{"email"});
    push(@column_values, $cust_pers->{"work_phone_number"});
    push(@column_values, $cust_pers->{"home_phone_number"});
    push(@column_values, $cust_pers->{"cell_phone_number"});
    push(@column_values, $cust_pers->{"other_phone_number"});
    push(@column_values, $floor_label);
    $csv_line = join(",", map {if (($_) && (/,/)) {qq{"$_"}} else {defined($_) ? $_ : ""}} @column_values);
    print $csv_line;
    print "\n";    
  }
}

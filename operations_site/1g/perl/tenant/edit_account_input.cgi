#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if account id is specified and try to load account.
  my ($cust_acct_id, $cust_acct) = ($cgi->param("id"), undef);
  if ((!defined($cust_acct_id)) || ($cust_acct_id !~ /^\d+$/) || (!($cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id)))) {
    die "show_page: edit_account_not_found.html\n";
  }
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active", undef, \%floor_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl where is_active", undef, \%building_by_id);
  # fetch services.
  my (%service_by_id, @service_id_list);
  get_row_by_id_map($dbh, "select * from service_tbl where is_active order by name", undef, \%service_by_id, \@service_id_list);
  # fetch payment methods.
  my (%payment_method_by_id, @payment_method_id_list);
  get_row_by_id_map($dbh, "select * from payment_method_tbl where is_active order by name", undef, \%payment_method_by_id, \@payment_method_id_list);
  # fetch persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  my $sql_query = "select * from cust_pers_tbl where is_active and cust_acct_id = ? order by last_name, first_name, middle_name";
  my @sql_param_list = ([$cust_acct_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_pers_by_id, \@cust_pers_id_list);
  # populate information needed for display of page.
  my @floor_id_list = sort {
    my ($floor_a, $floor_b) = ($floor_by_id{$a}, $floor_by_id{$b});
    my ($building_id_a, $building_id_b) = ($floor_a->{"building_id"}, $floor_b->{"building_id"});
    my ($building_a, $building_b) = ($building_by_id{$building_id_a}, $building_by_id{$building_id_b});
    my $sort_value = 0;
    $sort_value ||= $building_a->{"address_1"} cmp $building_b->{"address_1"};
    $sort_value ||= $floor_a->{"short_local_label"} cmp $floor_b->{"short_local_label"};
    return $sort_value;
  } keys(%floor_by_id);
  my $cust_acct_id = $cust_acct->{"id"};
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  my $external_name = HTML::Entities::encode($cust_acct->{"external_name"} || "");
  my $cust_pers_id = $cust_acct->{"primary_contact_cust_pers_id"} || "";
  my $floor_id = $cust_acct->{"primary_floor_id"};
  my $service_id = $cust_acct->{"service_id"};
  my $description = HTML::Entities::encode($cust_acct->{"description"} || "");
  my $web_site = HTML::Entities::encode($cust_acct->{"web_site"} || "");
  my $note = HTML::Entities::encode($cust_acct->{"note"} || "");
  my $tax_id = HTML::Entities::encode($cust_acct->{"tax_id"} || "");
  my $preferred_payment_method = HTML::Entities::encode($cust_acct->{"preferred_payment_method"});
  my $is_visible = $cust_acct->{"visible"} ? 1 : 0;
  my $is_active = $cust_acct->{"is_active"} ? 1 : 0;
  my $room_minutes = $cust_acct->{"room_minutes"};
  my $pkg_notify_email = $cust_acct->{"pkg_notify_email"};
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account>
    <id>$cust_acct_id</id>
    <name>$account_name</name>
    <external-name>$external_name</external-name>
    <cust_pers_id>$cust_pers_id</cust_pers_id>
    <primary_floor_id>$floor_id</primary_floor_id>
    <service-id>$service_id</service-id>
    <description>$description</description>
    <web-site>$web_site</web-site>
    <note>$note</note>
    <tax-id>$tax_id</tax-id>
    <preferred-payment-method>$preferred_payment_method</preferred-payment-method>
    <is-active>$is_active</is-active>
    <is-visible>$is_visible</is-visible>
    <room_minutes>$room_minutes</room_minutes>
    <pkg_notify_email>$pkg_notify_email</pkg_notify_email>
  </account>
  <floor-list>
---END_BLOCK---;
  for $floor_id (@floor_id_list) {
    my $floor = $floor_by_id{$floor_id};
    my $building_id = $floor->{"building_id"};
    my $building = $building_by_id{$building_id};
    my $floor_label = HTML::Entities::encode($building->{"address_1"} . ", " . $floor->{"label"});
    $generated_xml .= <<"---END_BLOCK---;";
    <floor>
      <id>$floor_id</id>
      <label>$floor_label</label>
    </floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </floor-list>
  <service-list>
---END_BLOCK---;
  for $service_id (@service_id_list) {
    my $service = $service_by_id{$service_id};
    my $service_name = HTML::Entities::encode($service->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <service>
      <id>$service_id</id>
      <name>$service_name</name>
    </service>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </service-list>
  <payment-method-list>
---END_BLOCK---;
  for my $payment_method_id (@payment_method_id_list) {
    my $payment_method = $payment_method_by_id{$payment_method_id};
    my $payment_method_name = HTML::Entities::encode($payment_method->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <payment-method>$payment_method_name</payment-method>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </payment-method-list>
  <person-list>
    <person>
      <id></id>
      <name>(unassigned)</name>
    </person>
---END_BLOCK---;
  for $cust_pers_id (@cust_pers_id_list) {
    my $cust_pers = $cust_pers_by_id{$cust_pers_id};
    my $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$cust_pers_id</id>
      <name>$person_name</name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_account_input.xslt", $cgi);
}

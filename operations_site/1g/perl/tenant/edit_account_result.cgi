#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;
use is_valid_email;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that account id was specified.
  my $cust_acct_id = $cgi->param("id");
  if ((!defined($cust_acct_id)) || ($cust_acct_id !~ /^\d+$/)) {
    die "show_page: edit_account_not_found.html\n";
  }
  # verify that account name was specified.
  my $account_name = Encode::decode($cgi_input_charset, $cgi->param("name"));
  if ((!$account_name) || ($account_name =~ /^\s*$/)) {
    die "show_page: edit_account_missing_name.html\n";
  }
  # verify that external name was specified.
  my $external_name = Encode::decode($cgi_input_charset, $cgi->param("external_name"));
  if ((!$external_name) || ($external_name =~ /^\s*$/)) {
    die "show_page: edit_account_missing_external_name.html\n";
  }
  # verify that description was specified.
  my $description = Encode::decode($cgi_input_charset, $cgi->param("description"));
  if ((!$description) || ($description =~ /^\s*$/)) {
    die "show_page: edit_account_missing_description.html\n";
  }
  # verify that preferred payment method was specified and is valid.
  my $preferred_payment_method = $cgi->param("preferred_payment_method");
  if ((!$preferred_payment_method) || ($preferred_payment_method =~ /^\s*$/)) {
    die "show_page: edit_account_missing_preferred_payment_method.html\n";
  } elsif ($preferred_payment_method !~ /^(ach|cash|check|credit card)$/) {
    die "show_page: edit_account_bad_preferred_payment_method.html\n";
  }
  # verify that room_minutes is either a non-negative number or blank (null).
  my $room_minutes = $cgi->param("room_minutes");
  if (!defined($room_minutes) || ($room_minutes eq "")) {
    $room_minutes = 0;
  } elsif ($room_minutes !~ /^\d+$/) {
    die "show_page: edit_account_bad_room_minutes.html\n";
  }
  # verify that package notify emails has zero or more valid email addresses.
  my $pkg_notify_email = Encode::decode($cgi_input_charset, $cgi->param("pkg_notify_email"));
  $pkg_notify_email =~ s/\s+//g;
  $pkg_notify_email =~ s/,+$//;
  my @pkg_notify_email_list = split(/,/, $pkg_notify_email);
  if (grep {!is_valid_email($_)} @pkg_notify_email_list) {
    die "show_page: edit_account_bad_pkg_notify_email.html\n";
  }
  # get the other form fields.
  my $web_site = Encode::decode($cgi_input_charset, $cgi->param("web_site")) || "";
  my $tax_id = $cgi->param("tax_id") || "";
  my $is_active = $cgi->param("is_active") ? 1 : 0;
  my $is_visible = $cgi->param("is_visible") ? 1 : 0;
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the account exists.
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  if (!$cust_acct) {
    $dbh->rollback();
    die "show_page: edit_account_not_found.html\n";
  }
  # if the primary contact person id was specified, verify that it is
  # a number and that the person exists and is linked to this account.
  my $cust_pers_id = $cgi->param("cust_pers_id");
  my $cust_pers;
  if (defined($cust_pers_id)) {
    if ($cust_pers_id =~ /^\s*$/) {
      $cust_pers_id = undef;
    } else {
      if (($cust_pers_id =~ /^\d+$/) && ($cust_pers = select_row($dbh, "cust_pers_tbl", $cust_pers_id))) {
	if ($cust_pers->{"cust_acct_id"} != $cust_acct_id) {
	  $dbh->rollback();
	  die "show_page: edit_account_account_person_mismatch.html\n";
	}
      } else {
	$dbh->rollback();
	die "show_page: edit_account_bad_person.html\n";
      }
    }
  }
  # verify that the floor id was specified and is valid.
  my $floor_id = $cgi->param("primary_floor_id");
  my $floor;
  if (defined($floor_id) && ($floor_id =~ /^\d+$/)) {
    $floor = select_row($dbh, "floor_tbl", $floor_id);
    if (!$floor) {
      $dbh->rollback();
      die "show_page: edit_account_bad_floor.html\n";
    }
  } else {
    $dbh->rollback();
    die "show_page: edit_account_missing_floor.html\n";
  }
  # verify that the service id was specified and that the specified
  # service is valid.
  my $service_id = $cgi->param("service_id");
  my $service;
  if (defined($service_id) && ($service_id =~ /^\d+$/)) {
    $service = select_row($dbh, "service_tbl", $service_id);
    if (!$service) {
      $dbh->rollback();
      die "show_page: edit_account_bad_service.html\n";
    }
  } else {
    $dbh->rollback();
    die "show_page: edit_account_missing_service.html\n";
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["name", Encode::encode($db_charset, $account_name)]);
  push(@sql_column_list, ["external_name", Encode::encode($db_charset, $external_name)]);
  push(@sql_column_list, ["primary_contact_cust_pers_id", $cust_pers_id, SQL_INTEGER]);
  push(@sql_column_list, ["primary_floor_id", $floor_id, SQL_INTEGER]);
  push(@sql_column_list, ["service_id", $service_id, SQL_INTEGER]);
  push(@sql_column_list, ["description", Encode::encode($db_charset, $description)]);
  push(@sql_column_list, ["web_site", Encode::encode($db_charset, $web_site)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  push(@sql_column_list, ["tax_id", $tax_id]);
  push(@sql_column_list, ["preferred_payment_method", $preferred_payment_method]);
  push(@sql_column_list, ["is_active", $is_active, SQL_BOOLEAN]);
  push(@sql_column_list, ["visible", $is_visible, SQL_BOOLEAN]);
  push(@sql_column_list, ["room_minutes", $room_minutes, SQL_INTEGER]);
  push(@sql_column_list, ["pkg_notify_email", $pkg_notify_email]);
  update_row($dbh, "cust_acct_tbl", \@sql_column_list, $cust_acct_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $account_name = HTML::Entities::encode($account_name);
  $external_name = HTML::Entities::encode($external_name);
  my $person_name = "";
  if ($cust_pers) {
    $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
  }
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  my $service_name = HTML::Entities::encode($service->{"name"});
  $description = HTML::Entities::encode($description);
  $web_site = HTML::Entities::encode($web_site);
  $note = HTML::Entities::encode($note);
  $tax_id = HTML::Entities::encode($tax_id);
  $preferred_payment_method = HTML::Entities::encode($preferred_payment_method);
  $pkg_notify_email = HTML::Entities::encode($pkg_notify_email);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <account>
    <account-name>$account_name</account-name>
    <external-name>$external_name</external-name>
    <person-name>$person_name</person-name>
    <floor-label>$floor_label</floor-label>
    <service-name>$service_name</service-name>
    <description>$description</description>
    <web-site>$web_site</web-site>
    <note>$note</note>
    <tax-id>$tax_id</tax-id>
    <preferred-payment-method>$preferred_payment_method</preferred-payment-method>
    <is-active>$is_active</is-active>
    <is-visible>$is_visible</is-visible>
    <room_minutes>$room_minutes</room_minutes>
    <pkg_notify_email>$pkg_notify_email</pkg_notify_email>
  </account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_account_result_success.xslt", $cgi);
}

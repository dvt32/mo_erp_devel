#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if alias id is specified.
  my $alias_id = $cgi->param("id");
  if ((!defined($alias_id)) || ($alias_id !~ /^\d+$/)) {
    die "show_page: edit_alias_not_found.html\n";
  }
  # load alias and check that it exists.
  my $alias = select_row($dbh, "alias_tbl", $alias_id);
  if (!$alias) {
    die "show_page: edit_alias_not_found.html\n";
  }
  # fetch accounts.
  my %account_by_id;
  my $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # add currently linked account if it was not in the list retrieved above.
  if ((defined(my $account_id = $alias->{"account_id"})) && (!exists($account_by_id{$alias->{"account_id"}}))) {
    my $row = select_row($dbh, "cust_acct_tbl", $account_id);
    $account_by_id{$account_id} = $row;
  }
  my @account_id_by_name_list = sort {
    lc($account_by_id{$a}->{"name"}) cmp lc($account_by_id{$b}->{"name"});
  } keys(%account_by_id);
  # populate information needed for display of page.
  my $account_id = $alias->{"account_id"};
  my $name = HTML::Entities::encode($alias->{"name"});
  my $note = HTML::Entities::encode($alias->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <alias>
    <id>$alias_id</id>
    <account-id>$account_id</account-id>
    <name>$name</name>
    <note>$note</note>
  </alias>
  <account-list>
---END_BLOCK---;
  for $account_id (@account_id_by_name_list) {
    my $account = $account_by_id{$account_id};
    my $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$account_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_alias_input.xslt", $cgi);
}

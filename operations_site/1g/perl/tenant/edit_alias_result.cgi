#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $alias_id = $cgi->param("id");
  my $account_id = $cgi->param("account_id");
  my $name = Encode::decode($cgi_input_charset, $cgi->param("name"));
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # verify that alias id was specified.
  if ((!defined($alias_id)) || ($alias_id !~ /^\d+$/)) {
    die "show_page: edit_alias_not_found.html\n";
  }
  # verify that the account id was specified and is a number.
  if ((!defined($account_id)) || ($account_id !~ /^\d+$/)) {
    die "show_page: edit_alias_bad_account.html\n";
  }
  # verify that name is not blank.
  if ((!$name) || ($name =~ /^\s+$/)) {
    die "show_page: edit_alias_missing_name.html\n";
  }
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the alias exists.
  my $alias = select_row($dbh, "alias_tbl", $alias_id);
  if (!$alias) {
    $dbh->rollback();
    die "show_page: edit_alias_not_found.html\n";
  }
  # verify that account exists.
  my $account = select_row($dbh, "cust_acct_tbl", $account_id);
  if (!$account) {
    $dbh->rollback();
    die "show_page: edit_alias_bad_account.html\n";
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["name", Encode::encode($db_charset, $name)]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "alias_tbl", \@sql_column_list, $alias_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $name = HTML::Entities::encode($name);
  my $account_name = HTML::Entities::encode($account->{"name"});
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <alias>
    <name>$name</name>
    <account-name>$account_name</account-name>
    <note>$note</note>
  </alias>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_alias_result_success.xslt", $cgi);
}

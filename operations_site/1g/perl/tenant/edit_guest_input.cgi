#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use get_month_name;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if guest person id is specified.
  my $guest_person_id = $cgi->param("id");
  if ((!defined($guest_person_id)) || ($guest_person_id !~ /^\d+$/)) {
    die "show_page: edit_guest_not_found.html\n";
  }
  # load guest person and check that it exists.
  my $guest_person = select_row($dbh, "guest_person_tbl", $guest_person_id);
  if (!$guest_person) {
    die "show_page: edit_guest_not_found.html\n";
  }
  # fetch accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # populate information needed for display of page.
  my $account_id = $guest_person->{"account_id"} || "";
  my $host_contact = HTML::Entities::encode($guest_person->{"host_contact"} || "");
  my $salutation = HTML::Entities::encode($guest_person->{"salutation"} || "");
  my $first_name = HTML::Entities::encode($guest_person->{"first_name"});
  my $middle_name = HTML::Entities::encode($guest_person->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($guest_person->{"last_name"});
  my $suffix = HTML::Entities::encode($guest_person->{"suffix"} || "");
  my $nickname = HTML::Entities::encode($guest_person->{"nickname"} || "");
  my $title = HTML::Entities::encode($guest_person->{"title"} || "");
  my $company = HTML::Entities::encode($guest_person->{"company"} || "");
  my $work_phone_number = HTML::Entities::encode($guest_person->{"work_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($guest_person->{"cell_phone_number"} || "");
  my $email = HTML::Entities::encode($guest_person->{"email"} || "");
  $guest_person->{"start_date"} =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  my $start_date_year = $1;
  my $start_date_month = int($2);
  my $start_date_day = int($3);
  $guest_person->{"end_date"} =~ /^(\d{4})-(\d\d)-(\d\d)$/;
  my $end_date_year = $1;
  my $end_date_month = int($2);
  my $end_date_day = int($3);
  my $note = HTML::Entities::encode($guest_person->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <guest_person>
    <id>$guest_person_id</id>
    <account_id>$account_id</account_id>
    <host_contact>$host_contact</host_contact>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <nickname>$nickname</nickname>
    <title>$title</title>
    <company>$company</company>
    <work_phone_number>$work_phone_number</work_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <email>$email</email>
    <start_date_year>$start_date_year</start_date_year>
    <start_date_month>$start_date_month</start_date_month>
    <start_date_day>$start_date_day</start_date_day>
    <end_date_year>$end_date_year</end_date_year>
    <end_date_month>$end_date_month</end_date_month>
    <end_date_day>$end_date_day</end_date_day>
    <note>$note</note>
  </guest_person>
  <month_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month_list>
  <day_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day_list>
  <account_list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_guest_input.xslt", $cgi);
}

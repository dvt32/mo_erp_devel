<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Guest
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="guest_person"/>
	<p>
	  <a>
	    <xsl:attribute name="href">/1g/tenant/delete_guest.cgi?id=<xsl:value-of select="guest_person/id"/></xsl:attribute>
	    <xsl:text>Delete this guest</xsl:text>
	  </a>
	  or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="guest_person">
    <table>
      <tr><td>
	<form action="/1g/tenant/edit_guest_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Guest
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td colspan="2" style="padding: 4px; background-color: orange; font-weight: bold; text-align: center;">
		  Host
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account:
		</td>
		<td>
                  <select name="cust_acct_id">
		    <xsl:for-each select="/page/account_list/account">
		      <option>
			<xsl:if test="/page/guest_person/account_id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Host Contact:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">host_contact</xsl:attribute>
		    <xsl:attribute name="size">32</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="host_contact"/></xsl:attribute>
		  </input>
		  (name and phone)
		</td>
	      </tr>
	      <tr>
		<td colspan="2" style="padding: 4px; background-color: orange; font-weight: bold; text-align: center;">
		  Guest
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Start Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>start_date_month</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/month_list/month">
		      <option>
			<xsl:if test="/page/guest_person/start_date_month = number">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>start_date_day</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/day_list/day">
		      <option>
			<xsl:if test="/page/guest_person/start_date_day = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">
		      <xsl:text>start_date_year</xsl:text>
		    </xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="/page/guest_person/start_date_year"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  End Date:
		</td>
		<td>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>end_date_month</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/month_list/month">
		      <option>
			<xsl:if test="/page/guest_person/end_date_month = number">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select>
		    <xsl:attribute name="name">
		      <xsl:text>end_date_day</xsl:text>
		    </xsl:attribute>
		    <xsl:for-each select="/page/day_list/day">
		      <option>
			<xsl:if test="/page/guest_person/end_date_day = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">
		      <xsl:text>end_date_year</xsl:text>
		    </xsl:attribute>
		    <xsl:attribute name="size">4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="/page/guest_person/end_date_year"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Salutation:
		</td>
		<td>
                  <select name="salutation">
		    <option value="">[---]</option>
		    <option>
		      <xsl:if test="salutation = 'Mr.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Mr.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Ms.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Ms.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Mrs.'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Mrs.</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="salutation = 'Miss'">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:text>Miss</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  First Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">first_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="first_name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Middle Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">middle_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="middle_name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Last Name:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">last_name</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="last_name"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Suffix:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">suffix</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="suffix"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Nickname:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">nickname</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="nickname"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Title:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">title</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="title"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Company:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">company</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="company"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Work Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">work_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="work_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Cell Phone:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">cell_phone_number</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="cell_phone_number"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Email:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">email</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="email"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (the note is visible to the tenant!)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Guest
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Guest
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="guest_person"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="guest_person">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following guest:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new guest:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Host Contact:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="host_contact"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Start Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="start_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  End Date:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="end_date"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Salutation:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  First Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="first_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Middle Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="middle_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Last Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="last_name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Suffix:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="suffix"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Nickname:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="nickname"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Title:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="title"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Company:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="company"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Work Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="work_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Cell Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cell_phone_number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

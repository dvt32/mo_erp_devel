#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use timestamp_to_components;
use get_month_name;
use lead_pad;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if mailbox id is specified and try to load mailbox.
  my ($mailbox_id, $mailbox) = ($cgi->param("id"), undef);
  if ((!defined($mailbox_id)) || ($mailbox_id !~ /^\d+$/) || (!($mailbox = select_row($dbh, "mailbox_tbl", $mailbox_id)))) {
    die "show_page: edit_mailbox_not_found.html\n";
  }
  # fetch accounts.
  my (%account_by_id, @account_id_list);
  my $sql_query = "select * from account_vw where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id, \@account_id_list);
  if ((defined(my $account_id = $mailbox->{"account_id"})) && (!exists($account_by_id{$mailbox->{"account_id"}}))) {
    $account_by_id{$account_id} = select_row($dbh, "account_vw", $account_id);
  }
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active", undef, \%floor_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl where is_active", undef, \%building_by_id);
  my $account_id = $mailbox->{"account_id"} || "";
  my $floor_id = $mailbox->{"floor_id"};
  my $floor = $floor_by_id{$floor_id};
  my $building_id = $floor->{"building_id"};
  my $building = $building_by_id{$building_id};
  my $floor_label = HTML::Entities::encode($building->{"address_1"} . ", " . $floor->{"label"});
  my $number = $mailbox->{"mailbox_number"};
  my $note = HTML::Entities::encode($mailbox->{"note"} || "");
  my ($lc_year, $lc_month, $lc_day, $lc_hour, $lc_minute, $lc_second) = timestamp_to_components($mailbox->{"last_checked_dt"});
  ($lc_hour, $lc_minute, $lc_second) = (lead_pad($lc_hour, 2), lead_pad($lc_minute, 2), lead_pad($lc_second, 2));
  my $bool_mail_present = $mailbox->{"bool_mail_present"} ? 1 : 0;
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <mailbox>
    <id>$mailbox_id</id>
    <account-id>$account_id</account-id>
    <floor-id>$floor_id</floor-id>
    <floor_label>$floor_label</floor_label>
    <number>$number</number>
    <note>$note</note>
    <last_checked_dt>
      <year>$lc_year</year>
      <month>$lc_month</month>
      <day>$lc_day</day>
      <hour>$lc_hour</hour>
      <minute>$lc_minute</minute>
      <second>$lc_second</second>
    </last_checked_dt>
    <bool_mail_present>$bool_mail_present</bool_mail_present>
  </mailbox>
  <account-list>
    <account>
      <id></id>
      <name>(unassigned)</name>
    </account>
---END_BLOCK---;
  for $account_id (@account_id_list) {
    my $account = $account_by_id{$account_id};
    my $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$account_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_mailbox_input.xslt", $cgi);
}

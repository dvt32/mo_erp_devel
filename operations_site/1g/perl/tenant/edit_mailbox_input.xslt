<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Mailbox
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="mailbox"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="mailbox">
    <table>
      <tr><td>
	<form action="/1g/tenant/edit_mailbox_result.cgi" method="post" autocomplete="off">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Mailbox
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0">
	      <tr>
		<td style="padding: 4px; ">
		  Number:
		</td>
		<td>
		  <xsl:value-of select="number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Building and Floor:
		</td>
		<td>
		  <xsl:value-of select="floor_label"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account:
		</td>
		<td>
                  <select name="account_id">
		    <xsl:for-each select="/page/account-list/account">
		      <option>
			<xsl:if test="/page/mailbox/account-id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Last Checked:
		</td>
		<td>
		  <select name="month">
		    <xsl:for-each select="/page/month-list/month">
		      <option>
			<xsl:if test="/page/mailbox/last_checked_dt/month = number">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <select name="day">
		    <xsl:for-each select="/page/day-list/day">
		      <option>
			<xsl:if test="/page/mailbox/last_checked_dt/day = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <input type="text" name="year">
		    <xsl:attribute name="size">4</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="/page/mailbox/last_checked_dt/year"/></xsl:attribute>
		  </input>
		  <xsl:text> at </xsl:text>
		  <select name="hour">
		    <xsl:for-each select="/page/hour-list/hour">
		      <option>
			<xsl:if test="/page/mailbox/last_checked_dt/hour = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		  <xsl:text>:</xsl:text>
		  <select name="minute">
		    <xsl:for-each select="/page/minute-list/minute">
		      <option>
			<xsl:if test="/page/mailbox/last_checked_dt/minute = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Has Mail:
		</td>
		<td>
                  <select name="bool_mail_present">
		    <option>
		      <xsl:if test="bool_mail_present = 1">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">1</xsl:attribute>
		      <xsl:text>Yes</xsl:text>
		    </option>
		    <option>
		      <xsl:if test="bool_mail_present = 0">
			<xsl:attribute name="selected">selected</xsl:attribute>
		      </xsl:if>
		      <xsl:attribute name="value">0</xsl:attribute>
		      <xsl:text>No</xsl:text>
		    </option>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

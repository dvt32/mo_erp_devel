#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;
use components_to_timestamp;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that mailbox id was specified.
  my $mailbox_id = $cgi->param("id");
  if ((!defined($mailbox_id)) || ($mailbox_id !~ /^\d+$/)) {
    die "show_page: edit_mailbox_not_found.html\n";
  }
  # if the account was specified, verify that is a number.
  my $account_id = $cgi->param("account_id");
  if (defined($account_id) && ($account_id =~ /^\s*$/)) {
    $account_id = undef;
  }
  if (defined($account_id) && ($account_id !~ /^\d+$/)) {
    die "show_page: edit_mailbox_bad_account.html\n";
  }
  my ($month, $day, $year, $hour, $minute) = map {$cgi->param($_)} (qw(month day year hour minute));
  if (($month < 1) || ($month > 12) || ($day < 1) || ($day > 31) || ($year < 2007) || ($year > 3000) ||
      ($hour < 0) || ($hour > 23) || ($minute < 0) || ($minute > 59)) {
    die "show_page: edit_mailbox_bad_time.html\n";
  }
  # get remaining form fields.
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  my $last_checked_dt = components_to_timestamp($year, $month, $day, $hour, $minute, 0);
  my $bool_mail_present = $cgi->param("bool_mail_present") || 0;
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the mailbox exists.
  my $mailbox = select_row($dbh, "mailbox_tbl", $mailbox_id);
  if (!$mailbox) {
    $dbh->rollback();
    die "show_page: edit_mailbox_not_found.html\n";
  }
  # if account was specified, verify that it exists.
  my $account;
  if (defined($account_id)) {
    $account = select_row($dbh, "account_vw", $account_id);
    if (!$account) {
      $dbh->rollback();
      die "show_page: edit_mailbox_bad_account.html\n";
    }
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  push(@sql_column_list, ["last_checked_dt", $last_checked_dt]);
  push(@sql_column_list, ["bool_mail_present", $bool_mail_present, SQL_BOOLEAN]);
  update_row($dbh, "mailbox_tbl", \@sql_column_list, $mailbox_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $number = $mailbox->{"mailbox_number"};
  my $floor_id = $mailbox->{"floor_id"};
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  my $account_name = "";
  if ($account) {
    $account_name = HTML::Entities::encode($account->{"name"});
  }
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <mailbox>
    <number>$number</number>
    <floor-label>$floor_label</floor-label>
    <account-name>$account_name</account-name>
    <note>$note</note>
    <last_checked_dt>$last_checked_dt</last_checked_dt>
    <bool_mail_present>$bool_mail_present</bool_mail_present>
  </mailbox>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_mailbox_result_success.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if person id is specified.
  my $cust_pers_id = $cgi->param("id");
  if ((!defined($cust_pers_id)) || ($cust_pers_id !~ /^\d+$/)) {
    die "show_page: edit_person_not_found.html\n";
  }
  # load person and check that it exists.
  my $cust_pers = select_row($dbh, "cust_pers_tbl", $cust_pers_id);
  if (!$cust_pers) {
    die "show_page: edit_person_not_found.html\n";
  }
  # fetch accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # populate information needed for display of page.
  my $cust_acct_id = $cust_pers->{"cust_acct_id"} || "";
  my $salutation = $cust_pers->{"salutation"};
  my $first_name = HTML::Entities::encode($cust_pers->{"first_name"});
  my $middle_name = HTML::Entities::encode($cust_pers->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($cust_pers->{"last_name"});
  my $nickname = HTML::Entities::encode($cust_pers->{"nickname"} || "");
  my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
  my $work_phone_number = HTML::Entities::encode($cust_pers->{"work_phone_number"} || "");
  my $work_fax_number = HTML::Entities::encode($cust_pers->{"work_fax_number"} || "");
  my $home_phone_number = HTML::Entities::encode($cust_pers->{"home_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($cust_pers->{"cell_phone_number"} || "");
  my $other_phone_number = HTML::Entities::encode($cust_pers->{"other_phone_number"} || "");
  my $note = HTML::Entities::encode($cust_pers->{"note"} || "");
  my $is_active = $cust_pers->{"is_active"} ? 1 : 0;
  my $is_registered = $cust_pers->{"register_with_building"} ? 1 : 0;
  my $directory_1 = HTML::Entities::encode($cust_pers->{"directory_1"} || "");
  my $directory_2 = HTML::Entities::encode($cust_pers->{"directory_2"} || "");
  my $directory_3 = HTML::Entities::encode($cust_pers->{"directory_3"} || "");
  my $password = HTML::Entities::encode($cust_pers->{"password"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <person>
    <id>$cust_pers_id</id>
    <cust_acct_id>$cust_acct_id</cust_acct_id>
    <salutation>$salutation</salutation>
    <first-name>$first_name</first-name>
    <middle-name>$middle_name</middle-name>
    <last-name>$last_name</last-name>
    <nickname>$nickname</nickname>
    <email>$email</email>
    <work-phone-number>$work_phone_number</work-phone-number>
    <work-fax-number>$work_fax_number</work-fax-number>
    <home-phone-number>$home_phone_number</home-phone-number>
    <cell-phone-number>$cell_phone_number</cell-phone-number>
    <other-phone-number>$other_phone_number</other-phone-number>
    <note>$note</note>
    <is-active>$is_active</is-active>
    <is-registered>$is_registered</is-registered>
    <directory-1>$directory_1</directory-1>
    <directory-2>$directory_2</directory-2>
    <directory-3>$directory_3</directory-3>
    <password>$password</password>
  </person>
  <account-list>
---END_BLOCK---;
  for $cust_acct_id (@cust_acct_id_list) {
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_person_input.xslt", $cgi);
}

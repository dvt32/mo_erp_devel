<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Person
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Person
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="person"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="person">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following person:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new person:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Salutation:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="salutation"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  First Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="first-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Middle Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="middle-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Last Name:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="last-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Nickname:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="nickname"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Email:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="email"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Password:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="password"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Work Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="work-phone-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Work Fax:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="work-fax-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Home Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="home-phone-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Cell Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="cell-phone-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Other Phone:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="other-phone-number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Directory 1:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="directory-1"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Directory 2:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="directory-2"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Directory 3:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="directory-3"/>
	</td>
      </tr>
      <xsl:if test="../operation = 'edit'">
	<tr>
	  <td class="map_table_key">
	    Is Active?:
	  </td>
	  <td class="map_table_value">
	    <xsl:choose>
	      <xsl:when test="is-active = 1">
		Yes
	      </xsl:when>
	      <xsl:otherwise>
		No
	      </xsl:otherwise>
	    </xsl:choose>
	  </td>
	</tr>
      </xsl:if>
      <tr>
	<td class="map_table_key">
	  Is Registered?:
	</td>
	<td class="map_table_value">
	  <xsl:choose>
	    <xsl:when test="is-registered = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if proximity card id is specified and try to load proximity card.
  my ($proximity_card_id, $proximity_card) = ($cgi->param("id"), undef);
  if ((!defined($proximity_card_id)) || ($proximity_card_id !~ /^\d+$/) || (!($proximity_card = select_row($dbh, "proximity_card_tbl", $proximity_card_id)))) {
    die "show_page: edit_proximity_card_not_found.html\n";
  }
  # fetch accounts.
  my %account_by_id;
  my $sql_query = "select * from account_vw where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  if ((defined(my $account_id = $proximity_card->{"account_id"})) && (!exists($account_by_id{$proximity_card->{"account_id"}}))) {
    $account_by_id{$account_id} = select_row($dbh, "account_vw", $account_id);
  }
  # fetch persons
  my %person_by_id;
  my $floor_id = $proximity_card->{"floor_id"};
  $sql_query = "select * from person_vw where is_active and account_id in (select id from account_vw where primary_floor_id = ?)";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%person_by_id);
  if ((defined(my $person_id = $proximity_card->{"person_id"})) && (!exists($person_by_id{$proximity_card->{"person_id"}}))) {
    $person_by_id{$person_id} = select_row($dbh, "person_vw", $person_id);
  }
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active", undef, \%floor_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl where is_active", undef, \%building_by_id);
  # fetch access levels.
  my (%access_level_by_id, @access_level_id_list);
  get_row_by_id_map($dbh, "select * from access_level_tbl order by name", undef, \%access_level_by_id, \@access_level_id_list);
  # populate information needed for display of page.
  my @account_id_list = sort {
    my ($account_a, $account_b) = ($account_by_id{$a}, $account_by_id{$b});
    $account_a->{"name"} cmp $account_b->{"name"};
  } keys(%account_by_id);
  my @person_id_list = sort {
    my ($person_a, $person_b) = ($person_by_id{$a}, $person_by_id{$b});
    my ($last_name_a, $last_name_b) = ($person_a->{"last_name"}, $person_b->{"last_name"});
    my ($first_name_a, $first_name_b) = ($person_a->{"first_name"}, $person_b->{"first_name"});
    my ($middle_name_a, $middle_name_b) = ($person_a->{"middle_name"}, $person_b->{"middle_name"});
    my $sort_value = 0;
    $sort_value ||= lc($last_name_a) cmp lc($last_name_b);
    $sort_value ||= lc($first_name_a) cmp lc($first_name_b);
    $sort_value ||= lc($middle_name_a) cmp lc($middle_name_b);
    return $sort_value;
  } keys(%person_by_id);
  my @floor_id_list = sort {
    my ($floor_a, $floor_b) = ($floor_by_id{$a}, $floor_by_id{$b});
    my ($building_id_a, $building_id_b) = ($floor_a->{"building_id"}, $floor_b->{"building_id"});
    my ($building_a, $building_b) = ($building_by_id{$building_id_a}, $building_by_id{$building_id_b});
    my $sort_value = 0;
    $sort_value ||= $building_a->{"address_1"} cmp $building_b->{"address_1"};
    $sort_value ||= $floor_a->{"short_local_label"} cmp $floor_b->{"short_local_label"};
    return $sort_value;
  } keys(%floor_by_id);
  my $account_id = $proximity_card->{"account_id"} || "";
  my $person_id = $proximity_card->{"person_id"} || "";
  my $access_level_id = $proximity_card->{"access_level_id"} || "";
  my $site_code = HTML::Entities::encode(defined($proximity_card->{"site_code"}) ? $proximity_card->{"site_code"} : "");
  my $number = HTML::Entities::encode($proximity_card->{"number"});
  my $technology = HTML::Entities::encode($proximity_card->{"technology"});
  my $type = HTML::Entities::encode($proximity_card->{"type"});
  my $status = HTML::Entities::encode($proximity_card->{"status"});
  my $note = HTML::Entities::encode($proximity_card->{"note"} || "");
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <proximity-card>
    <id>$proximity_card_id</id>
    <site_code>$site_code</site_code>
    <number>$number</number>
    <technology>$technology</technology>
    <type>$type</type>
    <status>$status</status>
    <floor-id>$floor_id</floor-id>
    <access-level-id>$access_level_id</access-level-id>
    <account-id>$account_id</account-id>
    <person-id>$person_id</person-id>
    <note>$note</note>
  </proximity-card>
  <status-list>
    <status>active</status>
    <status>inactive</status>
    <status>lost</status>
  </status-list>
  <account-list>
    <account>
      <id></id>
      <name>(unassigned)</name>
    </account>
---END_BLOCK---;
  for $account_id (@account_id_list) {
    my $account = $account_by_id{$account_id};
    my $account_name = HTML::Entities::encode($account->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$account_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <person-list>
    <person>
      <id></id>
      <name>(unassigned)</name>
    </person>
---END_BLOCK---;
  for $person_id (@person_id_list) {
    my $person = $person_by_id{$person_id};
    my $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
    if ($person->{"middle_name"}) {
      $person_name .= " " . $person->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$person_id</id>
      <name>$person_name</name>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
  <floor-list>
---END_BLOCK---;
  for $floor_id (@floor_id_list) {
    my $floor = $floor_by_id{$floor_id};
    my $building_id = $floor->{"building_id"};
    my $building = $building_by_id{$building_id};
    my $floor_label = HTML::Entities::encode($building->{"address_1"} . ", " . $floor->{"label"});
    $generated_xml .= <<"---END_BLOCK---;";
    <floor>
      <id>$floor_id</id>
      <label>$floor_label</label>
    </floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </floor-list>
  <access-level-list>
    <access-level>
      <id></id>
      <name>(unassigned)</name>
    </access-level>
---END_BLOCK---;
  for $access_level_id (@access_level_id_list) {
    my $access_level = $access_level_by_id{$access_level_id};
    my $access_level_name = HTML::Entities::encode($access_level->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <access-level>
      <id>$access_level_id</id>
      <name>$access_level_name</name>
    </access-level>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </access-level-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_proximity_card_input.xslt", $cgi);
}

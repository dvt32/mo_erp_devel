<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Edit Proximity Card
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="proximity-card"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="proximity-card">
    <table>
      <tr><td>
	<form action="/1g/tenant/edit_proximity_card_result.cgi" method="post">
	  <fieldset class="standard_fieldset">
	    <legend>
	      Edit Proximity Card
	    </legend>
	    <input>
	      <xsl:attribute name="type">hidden</xsl:attribute>
	      <xsl:attribute name="name">id</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
	    </input>
	    <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
	      <tr>
		<td style="padding: 4px; ">
		  Site Code:
		</td>
		<td>
		  <xsl:value-of select="site_code"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Number:
		</td>
		<td>
		  <xsl:value-of select="number"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Technology:
		</td>
		<td>
		  <xsl:value-of select="technology"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Type:
		</td>
		<td>
		  <xsl:value-of select="type"/>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Status:
		</td>
		<td>
                  <select name="status">
		    <xsl:for-each select="/page/status-list/status">
		      <option>
			<xsl:if test="/page/proximity-card/status = .">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Building and Floor:
		</td>
		<td>
                  <select name="floor_id">
		    <xsl:for-each select="/page/floor-list/floor">
		      <option>
			<xsl:if test="/page/proximity-card/floor-id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="label"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Account:
		</td>
		<td>
                  <select name="account_id">
		    <xsl:for-each select="/page/account-list/account">
		      <option>
			<xsl:if test="/page/proximity-card/account-id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Person:
		</td>
		<td>
                  <select name="person_id">
		    <xsl:for-each select="/page/person-list/person">
		      <option>
			<xsl:if test="/page/proximity-card/person-id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Access Level:
		</td>
		<td>
                  <select name="access_level_id">
		    <xsl:for-each select="/page/access-level-list/access-level">
		      <option>
			<xsl:if test="/page/proximity-card/access-level-id = id">
			  <xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
			<xsl:value-of select="name"/>
		      </option>
		    </xsl:for-each>
		  </select>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		  Note:
		</td>
		<td>
		  <input>
		    <xsl:attribute name="type">text</xsl:attribute>
		    <xsl:attribute name="name">note</xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="note"/></xsl:attribute>
		    <xsl:attribute name="size">40</xsl:attribute>
		  </input>
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  (the note is visible to the tenant!)
		</td>
	      </tr>
	      <tr>
		<td style="padding: 4px; ">
		</td>
		<td>
		  <input type="submit" name="submitButton" value="Save"/>
		</td>
	      </tr>
	    </table>
	  </fieldset>
	</form>
      </td></tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use update_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that proximity card id was specified.
  my $proximity_card_id = $cgi->param("id");
  if ((!defined($proximity_card_id)) || ($proximity_card_id !~ /^\d+$/)) {
    die "show_page: edit_proximity_card_not_found.html\n";
  }
  # verify that the status is valid.
  my $status = $cgi->param("status");
  if (!defined($status) || ($status !~ /^(active|inactive|lost)$/)) {
    die "show_page: edit_proximity_card_bad_status.html\n";
  }
  # verify that floor id was specified.
  my $floor_id = $cgi->param("floor_id");
  if ((!defined($floor_id)) || ($floor_id !~ /^\d+$/)) {
    die "show_page: edit_proximity_card_bad_floor.html\n";
  }
  # get the form fields.
  my $account_id = $cgi->param("account_id");
  my $person_id = $cgi->param("person_id");
  my $access_level_id = $cgi->param("access_level_id");
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of updating the row.
  $dbh->begin_work();
  # verify that the proximity card exists.
  my $proximity_card = select_row($dbh, "proximity_card_tbl", $proximity_card_id);
  if (!$proximity_card) {
    $dbh->rollback();
    die "show_page: edit_proximity_card_not_found.html\n";
  }
  # verify floor exists.
  my $floor = select_row($dbh, "floor_tbl", $floor_id);
  if (!$floor) {
    $dbh->rollback();
    die "show_page: edit_proximity_card_bad_floor.html\n";
  }
  # if the account was specified, verify that it exists.
  my $account;
  if (defined($account_id)) {
    if ($account_id eq "") {
      $account_id = undef;
    } else {
      $account = select_row($dbh, "account_vw", $account_id);
      if (!$account) {
	$dbh->rollback();
	die "show_page: edit_proximity_card_missing_account.html\n";
      }
    }
  }
  # if the person was specified, verify that it exists.
  my $person;
  if (defined($person_id)) {
    if ($person_id eq "") {
      $person_id = undef;
    } else {
      $person = select_row($dbh, "person_vw", $person_id);
      if (!$person) {
	$dbh->rollback();
	die "show_page: edit_proximity_card_missing_person.html\n";
      }
    }
  }
  # verify that the person is linked to the account.
  if (defined($account_id) && defined($person_id)) {
    if ($account->{"id"} != $person->{"account_id"}) {
      $dbh->rollback();
      die "show_page: edit_proximity_card_account_person_mismatch.html\n";
    }
  }
  # if the access level was specified, verify that it exists.
  my $access_level;
  if (defined($access_level_id)) {
    if ($access_level_id eq "") {
      $access_level_id = undef;
    } else {
      $access_level = select_row($dbh, "access_level_tbl", $access_level_id);
      if (!$access_level) {
	$dbh->rollback();
	die "show_page: edit_proximity_card_missing_access_level.html\n";
      }
    }
  }
  # issue the update command to the database.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["status", $status]);
  push(@sql_column_list, ["floor_id", $floor_id, SQL_INTEGER]);
  push(@sql_column_list, ["account_id", $account_id, SQL_INTEGER]);
  push(@sql_column_list, ["person_id", $person_id, SQL_INTEGER]);
  push(@sql_column_list, ["access_level_id", $access_level_id, SQL_INTEGER]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  update_row($dbh, "proximity_card_tbl", \@sql_column_list, $proximity_card_id);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $status = HTML::Entities::encode($status);
  my $site_code = HTML::Entities::encode($proximity_card->{"site_code"} || "");
  my $number = $proximity_card->{"number"};
  my $technology = HTML::Entities::encode($proximity_card->{"technology"});
  my $type = HTML::Entities::encode($proximity_card->{"type"});
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  my $account_name = $account ? HTML::Entities::encode($account->{"name"}) : "";
  my $person_name = "";
  if ($person) {
    $person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
    if ($person->{"middle_name"}) {
      $person_name .= " " . $person->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
  }
  my $access_level_name = $access_level ? HTML::Entities::encode($access_level->{"name"}) : "";
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>edit</operation>
  <proximity-card>
    <site_code>$site_code</site_code>
    <number>$number</number>
    <technology>$technology</technology>
    <type>$type</type>
    <status>$status</status>
    <floor-label>$floor_label</floor-label>
    <account-name>$account_name</account-name>
    <person-name>$person_name</person-name>
    <access-level-name>$access_level_name</access-level-name>
    <note>$note</note>
  </proximity-card>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_proximity_card_result_success.xslt", $cgi);
}

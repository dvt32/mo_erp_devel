<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center ::
	  <xsl:choose>
	    <xsl:when test="operation = 'edit'">
	      Edit Proximity Card
	    </xsl:when>
	    <xsl:when test="operation = 'new'">
	      New Proximity Card
	    </xsl:when>
	    <xsl:otherwise>
	      Error -- no operation specified.
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="proximity-card"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="proximity-card">
    <p>
      <xsl:choose>
	<xsl:when test="../operation = 'edit'">
	  You successfully updated the following proximity card:
	</xsl:when>
	<xsl:when test="../operation = 'new'">
	  You successfully created the following new proximity card:
	</xsl:when>
	<xsl:otherwise>
	  Error -- no operation specified.
	</xsl:otherwise>
      </xsl:choose>
    </p>
    <table cellspacing="0" cellpadding="4px" border="0">
      <tr>
	<td class="map_table_key">
	  Site Code:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="site_code"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Number:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="number"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Technology:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="technology"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Type:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="type"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Status:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="status"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Building and Floor:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="floor-label"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Account:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="account-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Person:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="person-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Access Level:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="access-level-name"/>
	</td>
      </tr>
      <tr>
	<td class="map_table_key">
	  Note:
	</td>
	<td class="map_table_value">
	  <xsl:value-of select="note"/>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

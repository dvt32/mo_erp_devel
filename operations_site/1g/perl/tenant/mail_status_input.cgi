#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use select_row;
use get_month_name;
use lead_pad;
use get_datetime_now;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if floor id is specified and try to load floor.
  my $floor_id = $cgi->param("floor_id");
  my ($floor_id, $floor) = ($cgi->param("floor_id"), undef);
  if ((!defined($floor_id)) || ($floor_id !~ /^\d+$/) || (!($floor = select_row($dbh, "floor_tbl", $floor_id)))) {
    die "show_page: mail_status_floor_not_found.html\n";
  }
  # fetch mailboxes.
  my (%mailbox_by_id, @mailbox_id_list);
  my $sql_query = "select * from mailbox_tbl where floor_id = ? and account_id is not null order by mailbox_number";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%mailbox_by_id, \@mailbox_id_list);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw where id in (select account_id from mailbox_tbl where floor_id = ?)";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # populate information needed for display of page.
  # determine current timestamp.
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now("time");
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <floor_id>$floor_id</floor_id>
  <floor-label>$floor_label</floor-label>
  <current-timestamp>
    <year>$year</year>
    <month>$month</month>
    <day>$day</day>
    <hour>$hour</hour>
    <minute>$minute</minute>
    <second>$second</second>
  </current-timestamp>
  <month-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month-list>
  <day-list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day-list>
  <hour-list>
---END_BLOCK---;
  for(my $i = 0; $i < 24; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <hour>$padded_number</hour>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </hour-list>
  <minute-list>
---END_BLOCK---;
  for(my $i = 0; $i < 60; $i++) {
    my $padded_number = lead_pad($i, 2);
    $generated_xml .= <<"---END_BLOCK---;";
    <minute>$padded_number</minute>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </minute-list>
  <mailbox-list>
---END_BLOCK---;
  for my $mailbox_id (@mailbox_id_list) {
    my $mailbox = $mailbox_by_id{$mailbox_id};
    my $account_id = $mailbox->{"account_id"};
    my $account_name = "";
    if (defined($account_id)) {
      my $account = $account_by_id{$account_id};
      if ($account) {
	$account_name = $account->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $number = HTML::Entities::encode($mailbox->{"mailbox_number"});
    my $note = HTML::Entities::encode($mailbox->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <mailbox>
      <id>$mailbox_id</id>
      <account-name>$account_name</account-name>
      <number>$number</number>
      <note>$note</note>
    </mailbox>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </mailbox-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "mail_status_input.xslt", $cgi);
}

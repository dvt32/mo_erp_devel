<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Update Mail Status
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a>
	    <xsl:attribute name="href">/1g/tenant/show_mailboxes.cgi?floor_id=<xsl:value-of select="floor_id"/></xsl:attribute>
	    <xsl:text>Show Mailboxes</xsl:text>
	  </a>
	</p>
	<form action="/1g/tenant/mail_status_result.cgi" method="post">
	  <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">floor_id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="floor_id"/></xsl:attribute>
	  </input>
	  <p>
	    Time of Mail Check:<br/>
	    <select name="month">
	      <xsl:for-each select="/page/month-list/month">
		<option>
		  <xsl:if test="/page/current-timestamp/month = number">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:attribute name="value"><xsl:value-of select="number"/></xsl:attribute>
		  <xsl:value-of select="name"/>
		</option>
	      </xsl:for-each>
	    </select>
	    <select name="day">
	      <xsl:for-each select="/page/day-list/day">
		<option>
		  <xsl:if test="/page/current-timestamp/day = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	    <input type="text" name="year">
	      <xsl:attribute name="size">4</xsl:attribute>
	      <xsl:attribute name="value"><xsl:value-of select="/page/current-timestamp/year"/></xsl:attribute>
	    </input>
	    <xsl:text> at </xsl:text>
	    <select name="hour">
	      <xsl:for-each select="/page/hour-list/hour">
		<option>
		  <xsl:if test="/page/current-timestamp/hour = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	    <xsl:text>:</xsl:text>
	    <select name="minute">
	      <xsl:for-each select="/page/minute-list/minute">
		<option>
		  <xsl:if test="/page/current-timestamp/minute = .">
		    <xsl:attribute name="selected">selected</xsl:attribute>
		  </xsl:if>
		  <xsl:value-of select="."/>
		</option>
	      </xsl:for-each>
	    </select>
	  </p>
	  <xsl:apply-templates select="mailbox-list"/>
	  <p>
	    <input type="submit" name="submitButton" value="Save"/>
	  </p>
	</form>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="mailbox-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="9" class="standard_table_title">
          Mailboxes at
	  <xsl:value-of select="../floor-label"/>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_spacer_column"></td>
	<td>Has Mail</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="mailbox"/>
    </table>
  </xsl:template>

  <xsl:template match="mailbox">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/tenant/edit_mailbox_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="number"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account-name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<select>
	  <xsl:attribute name="name">bool_mail_present-<xsl:value-of select="id"/></xsl:attribute>
	  <option>
	    <xsl:attribute name="value"></xsl:attribute>
	    <xsl:text>[not checked]</xsl:text>
	  </option>
	  <option>
	    <xsl:attribute name="value">1</xsl:attribute>
	    <xsl:text>Yes</xsl:text>
	  </option>
	  <option>
	    <xsl:attribute name="value">0</xsl:attribute>
	    <xsl:text>No</xsl:text>
	  </option>
	</select>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

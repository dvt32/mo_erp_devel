#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use components_to_timestamp;
use select_row;
use update_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # verify that the floor id was specified and is valid.
  my $floor_id = $cgi->param("floor_id");
  my $floor;
  if ((!defined($floor_id)) || ($floor_id !~ /^\d+$/) || (!($floor = select_row($dbh, "floor_tbl", $floor_id)))) {
    die "show_page: mail_status_bad_floor.html\n";
  }
  # fetch time of mail check.
  my ($month, $day, $year, $hour, $minute) = map {$cgi->param($_)} (qw(month day year hour minute));
  if (($month < 1) || ($month > 12) || ($day < 1) || ($day > 31) || ($year < 2007) || ($year > 3000) ||
      ($hour < 0) || ($hour > 23) || ($minute < 0) || ($minute > 59)) {
    die "show_page: mail_status_bad_time.html\n";
  }
  my $last_checked_dt = components_to_timestamp($year, $month, $day, $hour, $minute, 0);
  # go through input fields and find bool_mail_present select boxes.
  my @id_param_name_list = grep {/^bool_mail_present-\d+/} $cgi->param();
  my @mailbox_id_list;
  for my $id_param_name (@id_param_name_list) {
    if ($id_param_name =~ /^bool_mail_present-(\d+)$/) {
      my $mailbox_id = $1;
      my $bool_mail_present = $cgi->param($id_param_name);
      if ($bool_mail_present =~ /^0|1$/) {
	push(@mailbox_id_list, $mailbox_id);
      }
    }
  }
  # begin transaction of updating the rows.
  $dbh->begin_work();
  # issue the update commands to the database.
  my (@sql_column_list);
  for my $mailbox_id (@mailbox_id_list) {
    my $bool_mail_present = $cgi->param("bool_mail_present-$mailbox_id");
    @sql_column_list = ();
    push(@sql_column_list, ["last_checked_dt", $last_checked_dt]);
    push(@sql_column_list, ["bool_mail_present", $bool_mail_present, SQL_BOOLEAN]);
    update_row($dbh, "mailbox_tbl", \@sql_column_list, $mailbox_id);
  }
  # commit the transaction.
  $dbh->commit();
  # fetch mailboxes.
  my (%mailbox_by_id);
  my $sql_query = "select * from mailbox_tbl where floor_id = ?";
  my @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%mailbox_by_id);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw where id in (select account_id from mailbox_tbl where floor_id = ?)";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # sort mailboxes by number.
  @mailbox_id_list = sort {
    my $mailbox_a = $mailbox_by_id{$a};
    my $mailbox_b = $mailbox_by_id{$b};
    return $mailbox_a->{"mailbox_number"} cmp $mailbox_b->{"mailbox_number"};
  } @mailbox_id_list;
  # populate information needed for display of page.
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <floor_id>$floor_id</floor_id>
  <floor-label>$floor_label</floor-label>
  <mailbox-list>
---END_BLOCK---;
  for my $mailbox_id (@mailbox_id_list) {
    my $mailbox = $mailbox_by_id{$mailbox_id};
    my $account_id = $mailbox->{"account_id"};
    my $account_name = "";
    if (defined($account_id)) {
      my $account = $account_by_id{$account_id};
      if ($account) {
	$account_name = $account->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $number = HTML::Entities::encode($mailbox->{"mailbox_number"});
    my $note = HTML::Entities::encode($mailbox->{"note"} || "");
    my $last_checked_dt = $mailbox->{"last_checked_dt"};
    my $bool_mail_present = $mailbox->{"bool_mail_present"};
    $generated_xml .= <<"---END_BLOCK---;";
    <mailbox>
      <id>$mailbox_id</id>
      <account-name>$account_name</account-name>
      <number>$number</number>
      <note>$note</note>
      <last_checked_dt>$last_checked_dt</last_checked_dt>
      <bool_mail_present>$bool_mail_present</bool_mail_present>
    </mailbox>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </mailbox-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "mail_status_result.xslt", $cgi);
}

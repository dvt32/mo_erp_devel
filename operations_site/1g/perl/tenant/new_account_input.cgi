#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch floors.
  my (%floor_by_id, @floor_id_list);
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active order by short_global_label", undef, \%floor_by_id, \@floor_id_list);
  # fetch services.
  my (%service_by_id, @service_id_list);
  get_row_by_id_map($dbh, "select * from service_tbl where is_active order by name", undef, \%service_by_id, \@service_id_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <form>
    <floor_list>
---END_BLOCK---;
  for my $floor_id (@floor_id_list) {
    my $floor = $floor_by_id{$floor_id};
    my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
    $generated_xml .= <<"---END_BLOCK---;";
      <floor>
        <id>$floor_id</id>
        <label>$floor_label</label>
      </floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </floor_list>
    <service-list>
---END_BLOCK---;
  for my $service_id (@service_id_list) {
    my $service = $service_by_id{$service_id};
    my $service_name = HTML::Entities::encode($service->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
      <service>
        <id>$service_id</id>
        <name>$service_name</name>
      </service>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </service-list>
    <payment-method-list>
      <payment-method>ach</payment-method>
      <payment-method>check</payment-method>
      <payment-method>credit card</payment-method>
    </payment-method-list>
  </form>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_account_input.xslt", $cgi);
}

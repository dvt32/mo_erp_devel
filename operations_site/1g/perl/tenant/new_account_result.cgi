#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use insert_row;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  # verify that account name was specified.
  my $account_name = Encode::decode($cgi_input_charset, $cgi->param("name"));
  if ((!$account_name) || ($account_name =~ /^\s*$/)) {
    die "show_page: edit_account_missing_name.html\n";
  }
  # verify that external name was specified.
  my $external_name = Encode::decode($cgi_input_charset, $cgi->param("external_name"));
  if ((!$external_name) || ($external_name =~ /^\s*$/)) {
    die "show_page: edit_account_missing_external_name.html\n";
  }
  # verify that description was specified.
  my $description = Encode::decode($cgi_input_charset, $cgi->param("description"));
  if ((!$description) || ($description =~ /^\s*$/)) {
    die "show_page: edit_account_missing_description.html\n";
  }
  # verify that preferred payment method was specified and is valid.
  my $preferred_payment_method = $cgi->param("preferred_payment_method");
  if ((!$preferred_payment_method) || ($preferred_payment_method =~ /^\s*$/)) {
    die "show_page: edit_account_missing_preferred_payment_method.html\n";
  } elsif ($preferred_payment_method !~ /^(ach|check|credit card)$/) {
    die "show_page: edit_account_bad_preferred_payment_method.html\n";
  }
  # verify that room_minutes is either a non-negative number or blank (null).
  my $room_minutes = $cgi->param("room_minutes");
  if (!defined($room_minutes) || ($room_minutes eq "")) {
    $room_minutes = 0;
  } elsif ($room_minutes !~ /^\d+$/) {
    die "show_page: edit_account_bad_room_minutes.html\n";
  }
  # get the form fields.
  my $web_site = Encode::decode($cgi_input_charset, $cgi->param("web_site")) || "";
  my $tax_id = $cgi->param("tax_id") || "";
  my $is_visible = $cgi->param("is_visible") ? 1 : 0;
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # begin transaction of inserting the new row.
  $dbh->begin_work();
  # verify that floor id was specified and is a valid floor.
  my $floor_id = $cgi->param("primary_floor_id");
  my $floor;
  if (defined($floor_id) && ($floor_id =~ /^\d+$/)) {
    $floor = select_row($dbh, "floor_tbl", $floor_id);
    if (!$floor) {
      $dbh->rollback();
      die "show_page: edit_account_bad_floor.html\n";
    }
  } else {
    $dbh->rollback();
    die "show_page: edit_account_missing_floor.html\n";
  }
  my $building_id = $floor->{"building_id"};
  my $building = select_row($dbh, "building_tbl", $building_id);
  # verify that the service id was specified and that the specified
  # service is valid.
  my $service_id = $cgi->param("service_id");
  my $service;
  if (defined($service_id) && ($service_id =~ /^\d+$/)) {
    $service = select_row($dbh, "service_tbl", $service_id);
    if (!$service) {
      $dbh->rollback();
      die "show_page: edit_account_bad_service.html\n";
    }
  } else {
    $dbh->rollback();
    die "show_page: edit_account_missing_service.html\n";
  }
  # insert new row.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["name", Encode::encode($db_charset, $account_name)]);
  push(@sql_column_list, ["external_name", Encode::encode($db_charset, $external_name)]);
  push(@sql_column_list, ["primary_floor_id", $floor_id, SQL_INTEGER]);
  push(@sql_column_list, ["service_id", $service_id, SQL_INTEGER]);
  push(@sql_column_list, ["description", Encode::encode($db_charset, $description)]);
  push(@sql_column_list, ["web_site", Encode::encode($db_charset, $web_site)]);
  push(@sql_column_list, ["type", "customer"]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note)]);
  push(@sql_column_list, ["tax_id", $tax_id]);
  push(@sql_column_list, ["preferred_payment_method", $preferred_payment_method]);
  push(@sql_column_list, ["visible", $is_visible, SQL_BOOLEAN]);
  push(@sql_column_list, ["room_minutes", $room_minutes, SQL_INTEGER]);
  insert_row($dbh, "cust_acct_tbl", \@sql_column_list);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  $account_name = HTML::Entities::encode($account_name);
  $external_name = HTML::Entities::encode($external_name);
  my $floor_label = HTML::Entities::encode($building->{"label"} . ", " . $floor->{"label"});
  my $service_name = HTML::Entities::encode($service->{"name"});
  $description = HTML::Entities::encode($description);
  $web_site = HTML::Entities::encode($web_site);
  $note = HTML::Entities::encode($note);
  $tax_id = HTML::Entities::encode($tax_id);
  $preferred_payment_method = HTML::Entities::encode($preferred_payment_method);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <account>
    <account-name>$account_name</account-name>
    <external-name>$external_name</external-name>
    <floor_label>$floor_label</floor_label>
    <service-name>$service_name</service-name>
    <description>$description</description>
    <web-site>$web_site</web-site>
    <note>$note</note>
    <tax-id>$tax_id</tax-id>
    <preferred-payment-method>$preferred_payment_method</preferred-payment-method>
    <is-visible>$is_visible</is-visible>
    <room_minutes>$room_minutes</room_minutes>
  </account>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_account_result_success.xslt", $cgi);
}

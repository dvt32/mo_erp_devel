#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use select_row;
use get_date_now;
use get_month_name;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $account_id = $cgi->param("account_id") || "";
  # fetch accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # if account id specified, fetch primary contact to populate host contact with suggested contact.
  my $host_contact;
  if (($account_id) && (my $cust_acct = $cust_acct_by_id{$account_id})) {
    my $cust_pers_id = $cust_acct->{"primary_contact_cust_pers_id"};
    my $cust_pers = select_row($dbh, "cust_pers_tbl", $cust_pers_id);
    if ($cust_pers) {
      $host_contact = $cust_pers->{"first_name"} . " " . $cust_pers->{"last_name"};
      if ($cust_pers->{"work_phone_number"}) {
	$host_contact .= " " . $cust_pers->{"work_phone_number"};
      } elsif ($cust_pers->{"cell_phone_number"}) {
	$host_contact .= " " . $cust_pers->{"cell_phone_number"};
      }
    }
  }
  # populate information for display of page.
  my ($current_year, $current_month, $current_day) = get_date_now();
  $host_contact = HTML::Entities::encode($host_contact || "");
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <current_date>
    <year>$current_year</year>
    <month>$current_month</month>
    <day>$current_day</day>
  </current_date>
  <month_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 12; $i++) {
    my $month_name = get_month_name($i);
    $generated_xml .= <<"---END_BLOCK---;";
    <month>
      <number>$i</number>
      <name>$month_name</name>
    </month>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </month_list>
  <day_list>
---END_BLOCK---;
  for(my $i = 1; $i <= 31; $i++) {
    $generated_xml .= <<"---END_BLOCK---;";
    <day>$i</day>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </day_list>
  <account_list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <name>$account_name</name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account_list>
  <form>
    <account_id>$account_id</account_id>
    <host_contact>$host_contact</host_contact>
  </form>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_guest_input.xslt", $cgi);
}

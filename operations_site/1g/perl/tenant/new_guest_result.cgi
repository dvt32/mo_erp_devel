#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use select_row;
use lead_pad;
use days_since_1_bc;
use insert_row;

my $max_duration = 14; # max number of days a guest can be active.

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $cust_acct_id = $cgi->param("cust_acct_id");
  my $host_contact = Encode::decode($cgi_input_charset, $cgi->param("host_contact"));
  my $salutation = Encode::decode($cgi_input_charset, $cgi->param("salutation")) || "";
  my $first_name = Encode::decode($cgi_input_charset, $cgi->param("first_name")) || "";
  my $middle_name = Encode::decode($cgi_input_charset, $cgi->param("middle_name")) || "";
  my $last_name = Encode::decode($cgi_input_charset, $cgi->param("last_name")) || "";
  my $suffix = Encode::decode($cgi_input_charset, $cgi->param("suffix"));
  my $nickname = Encode::decode($cgi_input_charset, $cgi->param("nickname")) || "";
  my $title = Encode::decode($cgi_input_charset, $cgi->param("title"));
  my $company = Encode::decode($cgi_input_charset, $cgi->param("company"));
  my $work_phone_number = Encode::decode($cgi_input_charset, $cgi->param("work_phone_number")) || "";
  my $cell_phone_number = Encode::decode($cgi_input_charset, $cgi->param("cell_phone_number")) || "";
  my $email = lc(Encode::decode($cgi_input_charset, $cgi->param("email")) || "");
  my $start_date_month = $cgi->param("start_date_month");
  my $start_date_day = $cgi->param("start_date_day");
  my $start_date_year = $cgi->param("start_date_year");
  if ((!$start_date_month) || (!$start_date_day) || (!$start_date_year) ||
      ($start_date_month < 1) || ($start_date_month > 12) || ($start_date_day < 1) || ($start_date_day > 31) ||
      ($start_date_year < 2000) || ($start_date_year > 3000)) {
    die "show_page: edit_guest_bad_start_date.html\n";
  }
  my $start_date_date = int($start_date_year) . "-" . lead_pad(int($start_date_month), 2) . "-" . lead_pad(int($start_date_day), 2);
  my $end_date_month = $cgi->param("end_date_month");
  my $end_date_day = $cgi->param("end_date_day");
  my $end_date_year = $cgi->param("end_date_year");
  if ((!$end_date_month) || (!$end_date_day) || (!$end_date_year) ||
      ($end_date_month < 1) || ($end_date_month > 12) || ($end_date_day < 1) || ($end_date_day > 31) ||
      ($end_date_year < 2000) || ($end_date_year > 3000)) {
    die "show_page: edit_guest_bad_end_date.html\n";
  }
  my $end_date_date = int($end_date_year) . "-" . lead_pad(int($end_date_month), 2) . "-" . lead_pad(int($end_date_day), 2);
  # verify that end date comes after start date and end date is not more than max_duration days after start date.
  if (($start_date_date gt $end_date_date) || (days_since_1_bc($end_date_month, $end_date_day, $end_date_year) - days_since_1_bc($start_date_month, $start_date_day, $start_date_year) > $max_duration)) {
    die "show_page: edit_guest_bad_date_relation.html\n";
  }
  my $note = Encode::decode($cgi_input_charset, $cgi->param("note")) || "";
  # verify that account id was specified.
  if ((!defined($cust_acct_id)) || ($cust_acct_id !~ /^\d+$/)) {
    die "show_page: edit_guest_bad_account.html\n";
  }
  # verify that name is not blank.
  if ((!$first_name) || ($first_name =~ /^\s+$/) || (!$last_name) || ($last_name =~ /^\s+$/)) {
    die "show_page: edit_guest_missing_name.html\n";
  }
  # begin transaction of inserting the new row.
  $dbh->begin_work();
  # verify that the account exists and is active.
  my $cust_acct = select_row($dbh, "cust_acct_tbl", $cust_acct_id);
  if ((!$cust_acct) || (!$cust_acct->{"is_active"})) {
    $dbh->rollback();
    die "show_page: edit_guest_bad_account.html\n";
  }
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $cust_acct_id, SQL_INTEGER]);
  push(@sql_column_list, ["host_contact", Encode::encode($db_charset, $host_contact) || undef]);
  push(@sql_column_list, ["salutation", Encode::encode($db_charset, $salutation) || undef]);
  push(@sql_column_list, ["first_name", Encode::encode($db_charset, $first_name)]);
  push(@sql_column_list, ["middle_name", Encode::encode($db_charset, $middle_name) || undef]);
  push(@sql_column_list, ["last_name", Encode::encode($db_charset, $last_name)]);
  push(@sql_column_list, ["suffix", Encode::encode($db_charset, $suffix) || undef]);
  push(@sql_column_list, ["nickname", Encode::encode($db_charset, $nickname) || undef]);
  push(@sql_column_list, ["title", Encode::encode($db_charset, $title) || undef]);
  push(@sql_column_list, ["company", Encode::encode($db_charset, $company) || undef]);
  push(@sql_column_list, ["work_phone_number", Encode::encode($db_charset, $work_phone_number) || undef]);
  push(@sql_column_list, ["cell_phone_number", Encode::encode($db_charset, $cell_phone_number) || undef]);
  push(@sql_column_list, ["email", Encode::encode($db_charset, $email) || undef]);
  push(@sql_column_list, ["start_date", $start_date_date]);
  push(@sql_column_list, ["end_date", $end_date_date]);
  push(@sql_column_list, ["note", Encode::encode($db_charset, $note) || undef]);
  insert_row($dbh, "guest_person_tbl", \@sql_column_list);
  # commit the transaction.
  $dbh->commit();
  # populate information needed for display of page.
  my $account_name = HTML::Entities::encode($cust_acct->{"name"});
  $host_contact = HTML::Entities::encode($host_contact);
  $salutation = HTML::Entities::encode($salutation);
  $first_name = HTML::Entities::encode($first_name);
  $middle_name = HTML::Entities::encode($middle_name);
  $last_name = HTML::Entities::encode($last_name);
  $suffix = HTML::Entities::encode($suffix);
  $nickname = HTML::Entities::encode($nickname);
  $title = HTML::Entities::encode($title);
  $company = HTML::Entities::encode($company);
  $work_phone_number = HTML::Entities::encode($work_phone_number);
  $cell_phone_number = HTML::Entities::encode($cell_phone_number);
  $email = HTML::Entities::encode($email);
  $note = HTML::Entities::encode($note);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <operation>new</operation>
  <guest_person>
    <account_name>$account_name</account_name>
    <host_contact>$host_contact</host_contact>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <suffix>$suffix</suffix>
    <nickname>$nickname</nickname>
    <title>$title</title>
    <company>$company</company>
    <work_phone_number>$work_phone_number</work_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <email>$email</email>
    <start_date>$start_date_date</start_date>
    <end_date>$end_date_date</end_date>
    <note>$note</note>
  </guest_person>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "edit_guest_result.xslt", $cgi);
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $account_id = $cgi->param("account_id") || "";
  # fetch accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # populate information for display of page.
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <form>
    <account_id>$account_id</account_id>
    <account-list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
      <account>
        <id>$cust_acct_id</id>
        <name>$account_name</name>
      </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </account-list>
  </form>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "new_person_input.xslt", $cgi);
}

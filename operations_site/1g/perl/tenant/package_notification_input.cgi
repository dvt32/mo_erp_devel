#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row_with_query;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  
  # fetch company names
  my $sql_query = "SELECT id, name, pkg_notify_email FROM cust_acct_tbl where is_active ORDER BY name";
  my @companies;
  get_row_list($dbh, $sql_query, undef, \@companies);
  my %account_by_id = map {$_->{"id"} => $_} @companies;
  
  # fetch emails
  my $sql_query = "SELECT cust_acct_tbl.id, cust_pers_tbl.email FROM cust_acct_tbl, cust_pers_tbl WHERE cust_acct_tbl.primary_contact_cust_pers_id = cust_pers_tbl.id";
  my @contacts;
  get_row_list($dbh, $sql_query, undef, \@contacts);
   
  # generate XML.
  my $xml = "";
  $xml .= <<__;
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <package_form />
  <company_list>
__

  for my $company (@companies) {
    my $company_id = HTML::Entities::encode($company->{"id"});
    my $company_name = HTML::Entities::encode($company->{"name"});
    my $pkg_notify_email = HTML::Entities::encode($company->{"pkg_notify_email"});
    $xml .= <<__;
    <company>
      <id>$company_id</id>
      <name>$company_name</name>
      <pkg_notify_email>$pkg_notify_email</pkg_notify_email>
    </company>
__

  }
  $xml .= <<__;
  </company_list>
  <contact_list>
__
  for my $contact (@contacts) {
    my $company_id = HTML::Entities::encode($contact->{"id"});
    my $contact_email;
    my $account = $account_by_id{$company_id};
    if (($account) && ($account->{"pkg_notify_email"})) {
      $contact_email = HTML::Entities::encode($account->{"pkg_notify_email"});
    } else {
      $contact_email = HTML::Entities::encode($contact->{"email"} || "");
    }
    $xml .= <<__;
    <contact>
      <company_id>$company_id</company_id>
      <email>$contact_email</email>
    </contact>
__
  }
  $xml .= <<__;
  </contact_list>
</page>
__

  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($xml, "package_notification_input.xslt", $cgi);
}

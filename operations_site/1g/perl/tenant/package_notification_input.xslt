<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:template match="page">
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	  <title>Micro Office Solutions :: Operations Center :: Package Notification</title>
	  <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
	  <script type="text/javascript" src="/1g/js/jquery-latest.pack.js">
	    <xsl:comment></xsl:comment>
	  </script>
	  <script type="text/javascript">
		  $(document).ready(
		  	function() {
				$('#sender_manager').change(
					function() {
						if ($(this).val() == 'Other') {
							$('#hidden').html('<input type="text" id="sender_other"  name="sender_other" style="margin-left:8px; width:188;" />');
						} else {
							$('#hidden').html('');
						}
					});
					
				$('#submitButton').click(
					function() {
						if ($('#sender_manager').val() == 'Other') {
							if ($('#sender_other').val() == '') {
								alert('Please enter a Sender');
								return false;
							}
							
						} 
						if ($('#sender_manager').val() == '') {
							alert('Please select a sender');
							return false;
						}
					});
			});
		</script>
	  <xsl:call-template name="contacts-js"/>
    </head>
    <body>
	  <xsl:apply-templates select="package_form"/>
	  <p>Return to the <a href="/1g/index.cgi">main menu</a>.</p>
	  <p><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
    </body>
  </html>
</xsl:template>
<xsl:template name="contacts-js">
  <script type="text/javascript">
    function populate_email() {	  
      form = document.forms['package_notification'];
      selected_id = form.recipient_id.selectedIndex;
	  contact_id = form.recipient_id.options[selected_id].value;
	  
	  if (contact_id == "") {
	    form.recipient_email.value = "";
	  }
	  
      <xsl:for-each select="/page/contact_list/contact">
	  if (contact_id == <xsl:value-of select="company_id"/>) {
	    form.recipient_email.value = "<xsl:value-of select="email"/>";
	  }
      </xsl:for-each>
    }
  </script>
</xsl:template>
<xsl:template match="package_form">
  <table>
    <tr>
	  <td>
	    <form action="/1g/tenant/package_notification_result.cgi" method="post" name="package_notification">
	      <fieldset class="standard_fieldset">
	        <legend>Package Notification</legend>
	        <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
		      <tr>
				<td style="padding: 4px;">Package Recipient:</td>
				<td>
				  <select id="recipient_id" name="recipient_id" onchange="populate_email()" style="width:270;">
		            <option value="">[---]</option>
		            <xsl:for-each select="/page/company_list/company">
			          <option>
			            <xsl:attribute name="value"><xsl:value-of select="id" /></xsl:attribute>
			            <xsl:value-of select="name" />
			          </option>
			        </xsl:for-each>
			      </select>
				</td>
		      </tr>
		      <tr>
				<td style="padding: 4px;">Recipient Email:</td>
				<td><input type="text" id="recipient_email"  name="recipient_email" style="width:270;"/></td>
		      </tr>
			  <tr>
				<td style="padding: 4px;">Carrier:</td>
				<td>
				  <select name="carrier">
		            <option value="Other">[---]</option>
			        <option value="US Post">US Post</option>
			        <option value="Fedex">Fedex</option>
			        <option value="UPS">UPS</option>
			        <option value="DHL">DHL</option>
			        <option value="Hand Delivered">Hand Delivered</option>
			        <option value="Other">Other</option>
			      </select>
				</td>
			  </tr>
		      <tr>
				<td style="padding: 4px;">Type of Package:</td>
				<td>
				    <select name="package_type">
		              <option value="Other">[---]</option>
			          <option value="Letter">Letter</option>
			          <option value="box">Box</option>
			          <option value="Other">Other</option>
			        </select>
			    </td>
			  </tr>
			  <tr>
				<td style="padding: 4px;">Email Message:</td>
				<td>
				  <textarea name="email_message" style="width:270; height:150px">&nbsp;</textarea>
			    </td>
			  </tr>
			  <tr>
				<td style="padding: 4px;">Signed for by:</td>
				<td>
				  <select id="sender_manager" name="sender_manager">
		            <option value="">[---]</option>
			        <option value="Allyson">Allyson</option>
			        <option value="Amiee">Amiee</option>
			        <option value="Emma">Emma</option>
			        <option value="David">David</option>
			        <option value="Gabriela">Gabriela</option>
			        <option value="Matthieu">Matthieu</option>
				<option value="Pilar">Pilar</option>
			        <option value="Rahim">Rahim</option>
			        <option value="Ryan">Ryan</option>
				<option value="Sharleen">Sharleen</option>
			        <option value="Wellie">Wellie</option>
			        <option value="Kyle">Kyle</option>
			        <option value="Zoila">Zoila</option>     
                                <option value="Amiee">Amiee</option>
			        <option value="Other">Other</option>
			      </select>
			      <span id="hidden"></span>
				</td>
			  </tr>
			  <tr>
				<td style="padding: 4px;"></td>
			    <td><input type="submit" id="submitButton" name="submitButton" value="Send Notification"/></td>
		      </tr>
	        </table>
	      </fieldset>
	    </form>
	  </td>
    </tr>
  </table>
</xsl:template>
<xsl:template match="@*|node()" priority="-1">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Lingua::EN::Nums2Words;
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use is_valid_email;
use select_row;
use insert_row;
use get_datetime_now;
use military_time_to_am_pm;
use Mail::Sendmail;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  
  # get the form fields.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $customer_id = Encode::decode($cgi_input_charset, $cgi->param("recipient_id"));
  my $recipient_email = Encode::decode($cgi_input_charset, $cgi->param("recipient_email"));
  my $carrier = Encode::decode($cgi_input_charset, $cgi->param("carrier"));
  my $package_type = Encode::decode($cgi_input_charset, $cgi->param("package_type"));
  my $email_message = Encode::decode($cgi_input_charset, $cgi->param("email_message"));
  my $sender_manager = Encode::decode($cgi_input_charset, $cgi->param("sender_manager" || ""));
  my $sender_other = Encode::decode($cgi_input_charset, $cgi->param("sender_other") || "");
  
  # verify a customer was selected
  if ($customer_id !~ /^\d+$/) {
    die "show_page: package_notification_invalid_customer_id.html\n";
  }

  my $customer = select_row($dbh, "cust_acct_tbl", $customer_id); 
  if (!defined($customer->{"id"})) {
    die "show_page: package_notification_invalid_customer_id.html\n";
  }
  
  # verify the email address is valid, if specified.
  $recipient_email =~ s/\s+//g;
  $recipient_email =~ s/,+$//;
  my @recipient_email_list = split(/,/, $recipient_email);
  if (grep {!is_valid_email($_)} @recipient_email_list) {
    die "show_page: package_notification_invalid_email_address.html\n";
  }
  
  # verify the notification sender
  if ($sender_manager eq "" && $sender_other eq "" ) {
  	die "show_page: package_notification_invalid_notification_sender.html\n";
  }

  my $sender = (lc($sender_manager) eq "other")?$sender_other:$sender_manager;
  my $customer_name = $customer->{"name"};
  my ($year, $month, $day, $hour, $minute, $second) = get_datetime_now('all');
  my $time = military_time_to_am_pm("$hour:$minute");
  my $html = <<HTML;
<html>
<body>
<p>Dear <b>$customer_name</b>,</p>

<p>
Today at <b>$time</b> we have received a package for your company. 
<br />
The package was delivered by <b>$carrier</b> in the form of a <b>$package_type</b> and it was signed for by <b>$sender</b>. 
<br />
Please make sure the package is retrieved before the end of the day at 5pm. After the end of the business day Micro Office will hold your package for you, however we are not responsible for the long term safe keeping of this item. Should you have any questions, please contact the office manager at your location.  <br />
<b>$email_message</b>
</p>

<p>We appreciate your business and have a pleasant day.</p>

<p>Micro Office Management</p>

<p>
_________<br />

Emma  212-624-9200 <br />
<a href=\"mailto:elarson\@microoffice.com\">elarson\@microoffice.com</a><br />
Ryan  646-723-1270<br />
<a href=\"mailto:ryoung\@microoffice.com\">ryoung\@microoffice.com</a><br />
Zoila  212-359-4202 <br />
<a href=\"mailto:zoila\@microoffice.com\">zoila\@microoffice.com</a><br />
Allyson  646-723-1273 <br />
<a href=\"mailto:amunoz\@microoffice.com\">amunoz\@microoffice.com</a><br />
Gabriela  646-237-6979 <br />
<a href=\"mailto:glargaespada\@microoffice.com\">glargaespada\@microoffice.com</a><br />
________
</p>
</body>
</html>
HTML

  my @sql_column_list;
  push(@sql_column_list, ["customer_id", $customer_id]);
	push(@sql_column_list, ["recipient_email", $recipient_email]);
	push(@sql_column_list, ["carrier", $carrier]);
	push(@sql_column_list, ["package_type", $package_type]);
	push(@sql_column_list, ["sender", $sender]);
	push(@sql_column_list, ["email_message", $html]);
	insert_row($dbh, "mail_package_tbl", \@sql_column_list);
	
	my %mail = (
         from => 'service@microoffice.com',
         to => $recipient_email,
         subject => 'Package Notification',
         body => $html,
         'content-type' => 'text/html; charset="iso-8859-1"',);
         
  sendmail(%mail);
    
  my $xml = "";
  $xml .= <<__;
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <notification>
    <recipient_email>$recipient_email</recipient_email>
  </notification>
</page>
__

  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($xml, "package_notification_result.xslt", $cgi);
}

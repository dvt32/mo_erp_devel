<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:template match="page">
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	  <title>Micro Office Solutions :: Operations Center :: Package Notification Email Sent</title>
	  <link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
    </head>
    <body>
	  <p>The package notification email was successfully sent to <xsl:value-of select="notification/recipient_email"/>.</p>
	  <p>Return to the <a href="/1g/index.cgi">main menu</a>.</p>
	  <p>New <a href="/1g/tenant/package_notification_input.cgi">Package Notification</a>.</p>
	  <p><img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/></p>
    </body>
  </html>
</xsl:template>
<xsl:template match="@*|node()" priority="-1">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>
</xsl:stylesheet>

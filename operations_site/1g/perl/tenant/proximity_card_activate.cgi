#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use insert_row;
use select_row;
use enqueue_email;
use components_to_timestamp;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get input data.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $account_id = $cgi->param("account_id");
  my $person_id = $cgi->param("person_id");
  my $access_level = $cgi->param("access_level");
  my $new_number = $cgi->param("new_number");
  my $operator = Encode::decode($cgi_input_charset, $cgi->param("operator"));
  my $cause = Encode::decode($cgi_input_charset, $cgi->param("cause"));
  my $notes = Encode::decode($cgi_input_charset, $cgi->param("notes"));
  # check that ids and numbers are digits
  if( !defined($account_id) || ($account_id !~ /^\d+$/) || 
      !defined($person_id) || ($person_id !~ /^\d+$/) || 
      !defined($access_level) || ($access_level !~ /^\d+$/) || 
      !defined($new_number) || ($new_number !~ /^\d+$/) || 
      !defined($operator) || 
      !defined($cause)) {
    die "show_page: missing_card_information.html\n";
  }
  # create the new card activation request.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_column_list);
  push(@sql_column_list, ["account_id", $account_id]);
  push(@sql_column_list, ["person_id", $person_id]);
  push(@sql_column_list, ["access_level_id", $access_level]);
  push(@sql_column_list, ["number", $new_number]);
  push(@sql_column_list, ["action_requested", "activate"]);
  push(@sql_column_list, ["operator", Encode::encode($db_charset, $operator)]);
  push(@sql_column_list, ["cause", Encode::encode($db_charset, $cause)]);
  push(@sql_column_list, ["notes", Encode::encode($db_charset, $notes)]);
  insert_row($dbh, "proximity_card_edit_tbl", \@sql_column_list);
  # get names to display instead of ids
  my $account_name = (select_row($dbh, "account_vw", $account_id))->{"name"};
  my $access_level_name;
  if($access_level != 0)
  {
    $access_level_name = (select_row($dbh, "access_level_tbl", $access_level))->{"name"};
  }
  else
  {
    $access_level_name = "Unknown Access Level";
  }
  my $person_name;
  if($person_id != 0)
  {
    my $person_row = select_row($dbh, "person_vw", $person_id);
    $person_name = ($person_row->{"first_name"} || "") . " " . ($person_row->{"last_name"} || "");
  }
  else
  {
    $person_name = "Unknown Person";
  }
  # enqueue the receipt email.
  my $receipt_email_message = <<"---END_BLOCK---;";
Card number $new_number needs to be activated to $access_level_name (access level ID #$access_level) due to a $cause. 
It will be assigned to $person_name (person ID #$person_id) from $account_name (account ID #$account_id). 
Reported by $operator.  
---END_BLOCK---;
  if($notes) {
    $receipt_email_message .= <<"---END_BLOCK---;";
Additional operator notes: $notes.
---END_BLOCK---;
  }
  enqueue_email
    ("dbh" => $dbh,
     "account_id" => $account_id,
     "from_hdr" => "Micro Office Solutions <service\@microoffice.us>",
     "to_hdr" => "access_card_req\@microoffice.us",
     "subject_hdr" => "Request for proximity card deactivation",
     "message_body" => $receipt_email_message,
    );
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-name>$account_name</account-name>
  <person-name>$person_name</person-name>
  <access-level-name>$access_level_name</access-level-name>
  <new-number>$new_number</new-number>
  <operator>$operator</operator>
  <cause>$cause</cause>
  <notes>$notes</notes>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "proximity_card_request_success.xslt", $cgi);
}

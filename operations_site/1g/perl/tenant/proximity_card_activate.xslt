<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: Activate Proximity Card 
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
				<script type="text/javascript" src="/1g/js/dynamic_option_list.js"> /* this is necessary for the declaration of JS with our Perl HTML processor */ var dummy_variable; </script>
				<script type="text/javascript">
					var populate = new DynamicOptionList("account_id","person_id", "access_level");
					<xsl:for-each select="/page/active-customer-list/active-customer">
						populate.forValue("<xsl:value-of select="id"/>").addOptionsTextValue( <xsl:for-each select="active-person-list/active-person"> "<xsl:value-of select="name"/>", "<xsl:value-of select="id"/>", </xsl:for-each> "(Unknown Person)", "0" ); 
						<xsl:variable name="account">
							<xsl:value-of select="id"/>
						</xsl:variable> 
						<xsl:variable name="floor">
							<xsl:value-of select="floor-id"/>
						</xsl:variable> 
						<xsl:for-each select="active-person-list/active-person">
							populate.forValue("<xsl:value-of select="$account"/>").forValue("<xsl:value-of select="id"/>").addOptionsTextValue( <xsl:for-each select="/page/access-level-list/access-level"><xsl:if test="floor-id=$floor"> "<xsl:value-of select="name"/>", "<xsl:value-of select="id"/>", </xsl:if></xsl:for-each> "(Unknown Preferred Floor)", "0" );
						</xsl:for-each>
					</xsl:for-each>
				</script>
      </head>
      <body onLoad="initDynamicOptionLists();">
				<p>
					<a href="/1g/index.cgi">Main menu</a> | <a href="/1g/tenant/show_active_accounts.cgi">Account list</a> | <a href="/1g/tenant/show_proximity_cards.cgi">Proximity Card List</a>
				</p>
				<form action="/1g/tenant/proximity_card_activate.cgi" method="post">
					<table cellspacing="0" cellpadding="10px" border="0" class="standard_table" style="white-space: nowrap;">
						<tr>
							<td class="standard_table_title" colspan="2">
								<xsl:text>Enter Information to Request Activation of Proximity Card</xsl:text>
							</td>
						</tr>
						<tr>
							<td valign="top" colspan="2">Account Associated with Card:<br />
								<select>
									<xsl:attribute name="name">
										<xsl:text>account_id</xsl:text>
									</xsl:attribute>
									<xsl:for-each select="/page/active-customer-list/active-customer">
										<option>
											<xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
											<xsl:value-of select="name"/><xsl:text>, </xsl:text><xsl:value-of select="primary-contact"/>
										</option>
									</xsl:for-each>
								</select>
							</td>
						</tr>
						<tr>
							<td>Name Associated with Card<br />
								<select>
									<xsl:attribute name="name">
										<xsl:text>person_id</xsl:text>
									</xsl:attribute>
									<script type="text/javascript">populate.printOptions("person_id")</script>
								</select>
							</td>
							<td>Access Level Requested<br />
								<select>
									<xsl:attribute name="name">
										<xsl:text>access_level</xsl:text>
									</xsl:attribute>
									<script type="text/javascript">populate.printOptions("access_level")</script>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">New Proximity Card Number<br />
								<input>
									<xsl:attribute name="type">text</xsl:attribute>
									<xsl:attribute name="name">new_number</xsl:attribute>
									<xsl:attribute name="size">6</xsl:attribute>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top">
								Operator Recording Request:<br />
								<xsl:for-each select="/page/salesperson-list/salesperson">
									<xsl:element name="input">
										<xsl:attribute name="type">radio</xsl:attribute>
										<xsl:attribute name="name">operator</xsl:attribute>
										<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
										<xsl:if test="/page/user = name">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="name"/>
										<br />
									</xsl:element>
								</xsl:for-each>
							</td>
							<td style="white-space: nowrap; " valign="top">
								Reason for Activation:<br />
								<xsl:for-each select="/page/enum-cause-list/cause">
									<xsl:element name="input">
										<xsl:attribute name="type">radio</xsl:attribute>
										<xsl:attribute name="name">cause</xsl:attribute>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<br />
								</xsl:for-each>
							</td>
						</tr>
						<tr valign="top">
							<td colspan="2">
								Notes:<br />
								<textarea>
									<xsl:attribute name="name">notes</xsl:attribute>
									<xsl:attribute name="rows">4</xsl:attribute>
									<xsl:attribute name="cols">60</xsl:attribute>
									<!-- Need this blank notes field; otherwise the surrounding textarea gets collapsed into a single tag. -->
		  <xsl:choose>
		    <xsl:when test="notes = ''">.</xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="notes"/>
		    </xsl:otherwise>
		  </xsl:choose>
								</textarea>
							</td>
						</tr>
						<tr>
							<td>
								<input type="submit" name="submitButton" value="Save"/>
							</td>
						</tr>
					</table>
				</form>
				<p>
					<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
				</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

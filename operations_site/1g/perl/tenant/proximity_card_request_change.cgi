#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use lead_pad;
use get_month_name;
use get_row_by_id_map;
use get_row_list;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # determine type of proximity card change is being requested
  my $proximity_card_change_type = $cgi->param("id");
  if (!$proximity_card_change_type) {
    die "show_page: proximity_card_request_change.html\n";
  }
  # select salesperson as the employee currently logged in
  my $username = $employee->{"first_name"};
  # load active customers.
  my (%active_customer_by_id, @active_customer_id_list);
  my $sql_query = "select * from account_vw where is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%active_customer_by_id, \@active_customer_id_list);
  # load active persons.
  my (%active_person_by_id, %active_persons_by_account_id);
  my $sql_query = "select * from person_vw where is_active order by last_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%active_person_by_id);
  for my $person (values(%active_person_by_id)) {
    push(@{$active_persons_by_account_id{$person->{"account_id"}}}, $person);
  }
  # load enumeration of possible check actions.
  my @enum_proximity_card_change_cause_list;
  $sql_query = "select * from enum_proximity_card_change_cause_tbl order by name";
  get_row_list($dbh, $sql_query, undef, \@enum_proximity_card_change_cause_list);
  # load access levels.
  my @access_level_list;
  $sql_query = "select * from access_level_tbl";
  get_row_list($dbh, $sql_query, undef, \@access_level_list);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <active-customer-list>
---END_BLOCK---;
  for my $id (@active_customer_id_list) {
    my $active_customer = $active_customer_by_id{$id};
    my $name = HTML::Entities::encode($active_customer->{"name"});
    my $floor_id = $active_customer->{"primary_floor_id"};
    my $primary_contact_person_id = $active_customer->{"primary_contact_person_id"};
    my $primary_contact = $active_person_by_id{$primary_contact_person_id};
    my $primary_contact_name = ($primary_contact->{"first_name"} || "") . " " . ($primary_contact->{"last_name"} || ""); 
    $primary_contact_name = HTML::Entities::encode($primary_contact_name);
    $generated_xml .= <<"---END_BLOCK---;";
    <active-customer>
      <id>$id</id>
      <name>$name</name>
      <floor-id>$floor_id</floor-id>
      <primary-contact>$primary_contact_name</primary-contact>
      <active-person-list>
---END_BLOCK---;
    for my $active_person (@{$active_persons_by_account_id{$id}}) {
      my $name = $active_person->{"first_name"} . " " . $active_person->{"last_name"};
      $name = HTML::Entities::encode($name);
      $generated_xml .= <<"---END_BLOCK---;";
        <active-person>
          <id>$active_person->{"id"}</id>
          <name>$name</name>
        </active-person>
---END_BLOCK---;
    }
    $generated_xml .= <<"---END_BLOCK---;";
      </active-person-list>
    </active-customer>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </active-customer-list>
  <user>$username</user>
  <salesperson-list>
    <salesperson>
      <name>David</name>
      <value>drotbard</value>
    </salesperson>
    <salesperson>
      <name>Jin</name>
      <value>jcho</value>
    </salesperson>
    <salesperson>
      <name>Yon</name>
      <value>ylam</value>
    </salesperson>
    <salesperson>
      <name>Wellie</name>
      <value>wchao</value>
    </salesperson>
    <salesperson>
      <name>Zoila</name>
      <value>zprimo</value>
    </salesperson>
  </salesperson-list>
  <action-list>
    <action>activate</action>
    <action>deactivate</action>
  </action-list>
  <access-level-list>
---END_BLOCK---;
  for my $access_level (@access_level_list) {
    my $id = $access_level->{"id"};
    my $floor_id = HTML::Entities::encode($access_level->{"floor_id"});
    my $name = HTML::Entities::encode($access_level->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <access-level>
      <id>$id</id>
      <name>$name</name>
      <floor-id>$floor_id</floor-id>
    </access-level>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </access-level-list>
  <enum-cause-list>
---END_BLOCK---;
  for my $proximity_card_change_cause (@enum_proximity_card_change_cause_list) {
    my $name = HTML::Entities::encode($proximity_card_change_cause->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <cause>$name</cause>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </enum-cause-list>
</page>
---END_BLOCK---;
  my $xslt_file = "";
  if ($proximity_card_change_type == 1) {
    $xslt_file = "proximity_card_deactivate_and_activate.xslt";
  } elsif ($proximity_card_change_type == 2) {
    $xslt_file = "proximity_card_activate.xslt";
  } elsif ($proximity_card_change_type == 3) {
    $xslt_file = "proximity_card_deactivate.xslt";
  }
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, $xslt_file, $cgi);
}

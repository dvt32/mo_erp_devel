<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
				<title>
					Micro Office Solutions :: Operations Center :: Successful Proximity Card Change Request
				</title>
				<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
				<xsl:choose>
					<xsl:when test="new-number"> 
						<xsl:choose>
							<xsl:when test="old-number"> 
								<p>You have successfully requested an activation and deactivation of proximity cards.</p>
							</xsl:when>
							<xsl:otherwise><p>You have successfully requested activation of a proximity card.</p></xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="old-number"> 
								<p>You have successfully requested deactivation of a proximity card.</p>
							</xsl:when>
							<xsl:otherwise><p>There is a problem with the change you just requested.</p></xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<table cellspacing="0" cellpadding="4px" border="0">
					<tr>
						<td class="map_table_key">
							Account:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="account-name"/>
						</td>
					</tr>
					<tr>
						<td class="map_table_key">
							Person:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="person-name"/>
						</td>
					</tr>
					<xsl:if test="new-number">
					<tr>
						<td class="map_table_key">
							Access Level Requested:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="access-level-name"/>
						</td>
					</tr>
					</xsl:if>
					<xsl:if test="old-number">
					<tr>
						<td class="map_table_key">
							Old Proximity Card Number:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="old-number"/>
						</td>
					</tr>
					</xsl:if>
					<xsl:if test="new-number">
					<tr>
						<td class="map_table_key">
							New Proximity Card Number:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="new-number"/>
						</td>
					</tr>
					</xsl:if>
					<tr>
						<td class="map_table_key">
							Cause of change:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="cause"/>
						</td>
					</tr>
					<tr>
						<td class="map_table_key">
							Operator Recording:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="operator"/>
						</td>
					</tr>
					<xsl:if test="notes">
					<tr>
						<td class="map_table_key">
							Any notes:
						</td>
						<td class="map_table_value">
							<xsl:value-of select="notes"/>
						</td>
					</tr>
					</xsl:if>
				</table>
			<p>
				Return to the <a href="/1g/index.cgi">main menu</a>.
			</p>
			<p>
				Return to <a href="/1g/tenant/proximity_card_request_change.html">card change request</a>.
			</p>            
			<p>
				<img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
			</p>
      </body>
    </html>
  </xsl:template>


  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use strip_outside_whitespace;
use get_row_list;
use new_result_list;
use get_row_by_id_map;
use serialize_hash;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get form input.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $account_name = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("account_name")));
  my $person_name = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("person_name")));
  my $alias = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("alias")));
  my $building_floor_id = $cgi->param("building_floor_id");
  my $record_age = $cgi->param("record_age");
  my $is_active = $cgi->param("is_active");
  # generate SQL query.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_where_clause_list, @sql_param_list, @sql_table_list);
  push(@sql_table_list, "cust_acct_tbl");
  if ($account_name) {
    my $_account_name = $account_name;
    $_account_name =~ s/\*/\%/g;
    push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
    push(@sql_where_clause_list, "cust_acct_tbl.name ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $_account_name));
  }
  if ($person_name) {
    my $_person_name = $person_name;
    $_person_name =~ s/\*/\%/g;
    my ($last_name, $first_name) = split(/\s*,\s*/, $_person_name);
    push(@sql_table_list, "cust_pers_tbl") if (!grep {$_ eq "cust_pers_tbl"} @sql_table_list);
    if ($first_name) {
      push(@sql_where_clause_list, "cust_pers_tbl.last_name ilike ? and cust_pers_tbl.first_name ilike ?");
      push(@sql_param_list, Encode::encode($db_charset, $last_name));
      push(@sql_param_list, Encode::encode($db_charset, $first_name));
    } else {
      push(@sql_where_clause_list, "cust_pers_tbl.last_name ilike ? or cust_pers_tbl.first_name ilike ?");
      push(@sql_param_list, Encode::encode($db_charset, $last_name));
      push(@sql_param_list, Encode::encode($db_charset, $last_name));
    }
    push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
    push(@sql_where_clause_list, "cust_pers_tbl.cust_acct_id = cust_acct_tbl.id");
  }
  if ($alias) {
    my $_alias = $alias;
    $_alias =~ s/\*/\%/g;
    push(@sql_table_list, "alias_tbl") if (!grep {$_ eq "alias_tbl"} @sql_table_list);
    push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
    push(@sql_where_clause_list, "alias_tbl.name ilike ? and alias_tbl.account_id = cust_acct_tbl.id");
    push(@sql_param_list, Encode::encode($db_charset, $_alias));
  }
  if ($building_floor_id) {
    if ($building_floor_id =~ /^building_id=(\d+)$/) {
      my $building_id = $1;
      push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
      push(@sql_table_list, "floor_tbl") if (!grep {$_ eq "floor_tbl"} @sql_table_list);
      push(@sql_where_clause_list, "cust_acct_tbl.primary_floor_id = floor_tbl.id and floor_tbl.building_id = ?");
      push(@sql_param_list, [$building_id, SQL_INTEGER]);
    } elsif ($building_floor_id =~ /^floor_id=(\d+)$/) {
      my $floor_id = $1;
      push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
      push(@sql_where_clause_list, "cust_acct_tbl.primary_floor_id = ?");
      push(@sql_param_list, [$floor_id, SQL_INTEGER]);
    } else {
      die "unknown building/floor specifier: $building_floor_id";
    }
  }
  if ($record_age) {
    if ($record_age =~ /^\d+$/) {
      push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
      push(@sql_where_clause_list, "cust_acct_tbl.creation_dt >= now() - interval ?");
      push(@sql_param_list, $record_age . " days");
    } else {
      die "bad record age: $record_age";
    }
  }
  if ($is_active ne "") {
    push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
    if ($is_active) {
      push(@sql_where_clause_list, "cust_acct_tbl.is_active");
    } else {
      push(@sql_where_clause_list, "not(cust_acct_tbl.is_active)");
    }
  }
  if (!@sql_where_clause_list) {
    die "show_page: search_account_blank_query.html\n";
  }
  my $sql_query = "select cust_acct_tbl.id from " . join(", ", @sql_table_list) . " where " . join(" and ", map {"(" . $_ . ")"} @sql_where_clause_list) . " order by cust_acct_tbl.name";
  my @sql_row_list;
  get_row_list($dbh, $sql_query, \@sql_param_list, \@sql_row_list);
  my @cust_acct_id_list = map {$_->{"id"}} @sql_row_list;
  my $result_list_id;
  if (!@cust_acct_id_list) {
    die "show_page: search_account_no_matches.html\n";
  }
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl", undef, \%floor_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl", undef, \%building_by_id);
  my $building_name = "";
  my $floor_name = "";
  if ($building_floor_id) {
    if ($building_floor_id =~ /^building_id=(\d+)$/) {
      $building_name = $building_by_id{$1}->{"address_1"};
    } elsif ($building_floor_id =~ /^floor_id=(\d+)$/) {
      $floor_name = $floor_by_id{$1}->{"label"};
    }
  }
  # generate new result list.
  my %result_list_query_param_map = ("account_name" => $account_name, "person_name" => $person_name, "alias" => $alias, "building_name" => $building_name, "floor_name" => $floor_name, "record_age" => $record_age, "is_active" => $is_active);
  my $result_list_query_param = serialize_hash(\%result_list_query_param_map);
  $result_list_id = new_result_list($dbh, "employee", $employee->{"id"}, "cust_acct_tbl", \@cust_acct_id_list, $result_list_query_param);
  # get accounts.
  $sql_query = "select * from cust_acct_tbl where id in (select item_id from result_item_tbl where result_list_id = ? order by index_number)";
  @sql_param_list = ([$result_list_id, SQL_INTEGER]);
  my %cust_acct_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_acct_by_id);
  # get persons.
  $sql_query = "select * from cust_pers_tbl where id in (select primary_contact_cust_pers_id from cust_acct_tbl where id in (select item_id from result_item_tbl where result_list_id = ?))";
  @sql_param_list = ([$result_list_id, SQL_INTEGER]);
  my %cust_pers_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_pers_by_id);
  # fetch services.
  my %service_by_id;
  get_row_by_id_map($dbh, "select * from service_tbl", undef, \%service_by_id);
  # generate XML.
  $account_name = HTML::Entities::encode($account_name);
  $person_name = HTML::Entities::encode($person_name);
  $alias = HTML::Entities::encode($alias);
  $building_name = HTML::Entities::encode($building_name);
  $floor_name = HTML::Entities::encode($floor_name);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <result_list_id>$result_list_id</result_list_id>
  <query>
    <account_name>$account_name</account_name>
    <person_name>$person_name</person_name>
    <alias>$alias</alias>
    <building_name>$building_name</building_name>
    <floor_name>$floor_name</floor_name>
    <record_age>$record_age</record_age>
    <is_active>$is_active</is_active>
  </query>
  <account_list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    my $cust_pers_id = $cust_acct->{"primary_contact_cust_pers_id"};
    my $person_name = "";
    if (defined($cust_pers_id)) {
      my $cust_pers = $cust_pers_by_id{$cust_pers_id};
      if ($cust_pers) {
	$person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
	if ($cust_pers->{"middle_name"}) {
	  $person_name .= " " . $cust_pers->{"middle_name"};
	}
      } else {
	$person_name = "(inactive person)";
      }
      $person_name = HTML::Entities::encode($person_name);
    }
    my $floor = $floor_by_id{$cust_acct->{"primary_floor_id"}};
    my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
    my $service_name = "";
    if (my $service_id = $cust_acct->{"service_id"}) {
      $service_name = HTML::Entities::encode($service_by_id{$service_id}->{"name"} || "");
    }
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <account_name>$account_name</account_name>
      <person_name>$person_name</person_name>
      <floor_label>$floor_label</floor_label>
      <service_name>$service_name</service_name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "search_account_result.xslt", $cgi);
}

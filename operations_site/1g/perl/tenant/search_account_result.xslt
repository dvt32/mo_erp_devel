<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Customer :: Search Accounts (result)
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <xsl:apply-templates select="query"/>
	</p>
	<xsl:apply-templates select="account_list"/>
	<p>
	  Create <a href="/1g/tenant/new_account_input.cgi">new account</a> or return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="query">
    Your query:
    <xsl:if test="account_name != ''">
      [account_name = <xsl:value-of select="account_name"/>]
    </xsl:if>
    <xsl:if test="person_name != ''">
      [person_name = <xsl:value-of select="person_name"/>]
    </xsl:if>
    <xsl:if test="alias != ''">
      [alias = <xsl:value-of select="alias"/>]
    </xsl:if>
    <xsl:if test="building_name != ''">
      [building = <xsl:value-of select="building_name"/>]
    </xsl:if>
    <xsl:if test="floor_name != ''">
      [floor = <xsl:value-of select="floor_name"/>]
    </xsl:if>
    <xsl:if test="record_age != ''">
      [record_age = <xsl:value-of select="record_age"/>]
    </xsl:if>
    <xsl:if test="is_active != ''">
      <xsl:text>[is_active = </xsl:text>
      <xsl:choose>
	<xsl:when test="is_active = 1">
	  <xsl:text>yes</xsl:text>
	</xsl:when>
	<xsl:when test="is_active = 0">
	  <xsl:text>no</xsl:text>
	</xsl:when>
      </xsl:choose>
      <xsl:text>]</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="account_list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="9" class="standard_table_title">
          Accounts in search result list
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Primary Contact</td>
	<td class="standard_table_spacer_column"></td>
	<td>Building and Floor</td>
	<td class="standard_table_spacer_column"></td>
	<td>Service</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="account"/>
    </table>
  </xsl:template>

  <xsl:template match="account">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/tenant/show_account_detail.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="account_name"/>
	</a>
	(#<xsl:value-of select="id"/>)
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="person_name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="floor_label"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="service_name"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

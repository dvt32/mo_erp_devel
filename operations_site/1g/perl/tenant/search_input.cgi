#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch building and floor rows.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl", undef, \%building_by_id);
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl", undef, \%floor_by_id);
  # generate merged list of building and floor entries, sorted first by building, then by floor.
  my %floor_id_list_by_building_id;
  for my $floor (values(%floor_by_id)) {
    push(@{$floor_id_list_by_building_id{$floor->{"building_id"}}}, $floor->{"id"});
  }
  my @building_floor_list;
  for my $building (sort {$a->{"address_1"} cmp $b->{"address_1"}} values(%building_by_id)) {
    my $floor_id_list = $floor_id_list_by_building_id{$building->{"id"}};
    # building must have corresponding floor rows, or else we skip it.
    if (defined($floor_id_list)) {
      if (scalar(@$floor_id_list) > 1) {
	push(@building_floor_list, {"type" => "building", "id" => $building->{"id"}});
	for my $floor_id (sort {$floor_by_id{$a}->{"short_local_label"} cmp $floor_by_id{$b}->{"short_local_label"}} @$floor_id_list) {
	  push(@building_floor_list, {"type" => "floor", "id" => $floor_id});
	}
      } else {
	# if building has only one active floor, then handle it specially. Do not display floors in select box.
	push(@building_floor_list, {"type" => "building", "id" => $building->{"id"}, "only_floor_id" => $floor_id_list->[0]});
      }
    }
  }
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account_form/>
  <person_form/>
  <common>
    <building_floor_list>
---END_BLOCK---;
  for my $building_floor (@building_floor_list) {
    my ($label, $value);
    my $type = $building_floor->{"type"};
    if ($type eq "building") {
      my $building = $building_by_id{$building_floor->{"id"}};
      if (my $floor_id = $building_floor->{"only_floor_id"}) {
	my $floor = $floor_by_id{$floor_id};
	$label = HTML::Entities::encode($building->{"address_1"} . " (only " . $floor->{"label"} . ")");
      } else {
	$label = HTML::Entities::encode($building->{"address_1"} . " (all floors)");
      }
      $value = HTML::Entities::encode("building_id=" . $building->{"id"});
    } elsif ($type eq "floor") {
      my $floor = $floor_by_id{$building_floor->{"id"}};
      $label = HTML::Entities::encode($floor->{"label"});
      $value = HTML::Entities::encode("floor_id=" . $floor->{"id"});
    } else {
      die "unknown type: " . $building_floor->{"type"};
    }
    $generated_xml .= <<"---END_BLOCK---;";
      <building_floor>
        <type>$type</type>
        <label>$label</label>
        <value>$value</value>
      </building_floor>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
    </building_floor_list>
    <record_age_list>
      <record_age>
        <label>7 days</label>
        <value>7</value>
      </record_age>
      <record_age>
        <label>14 days</label>
        <value>14</value>
      </record_age>
      <record_age>
        <label>30 days</label>
        <value>30</value>
      </record_age>
      <record_age>
        <label>90 days</label>
        <value>90</value>
      </record_age>
    </record_age_list>
  </common>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "search_input.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Customer :: Search
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  Use * as wildcard in the account name, person name, alias, or email fields. For example:
	</p>
	<ul>
	  <li>*smith* finds all names that have "smith" in the middle</li>
	  <li>smith* finds all names that start with smith</li>
	  <li>*smith finds all names that end with smith</li>
	</ul>
	<p>
	  For person name, enter the name either as a single word (searches both first and last name) or in the format Last
	  Name, First Name. For example, "Smith, John" (do not type quotes) would find only people named John Smith. "bar*,
	  greg*" would find people with a last name starting with bar and first name starting with greg (for instance, Gregory
	  Barton).
	</p>
	<xsl:apply-templates select="account_form"/>
	<xsl:apply-templates select="person_form"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account_form">
    <table>
      <tr>
	<td>
	  <form action="/1g/tenant/search_account_result.cgi" method="post">
	    <fieldset class="standard_fieldset">
	      <legend>
		Search Accounts
	      </legend>
	      <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
		<tr>
		  <td style="padding: 4px; ">
		    Account Name:
		  </td>
		  <td>
		    <input type="text" name="account_name"/>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Person Name:
		  </td>
		  <td>
		    <input type="text" name="person_name"/>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Alias:
		  </td>
		  <td>
		    <input type="text" name="alias"/>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Building/Floor:
		  </td>
		  <td>
		    <select name="building_floor_id">
		      <option value="">[---]</option>
		      <xsl:for-each select="/page/common/building_floor_list/building_floor">
			<option>
			  <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			  <xsl:choose>
			    <xsl:when test="type = 'floor'">
			      --<xsl:value-of select="label"/>
			    </xsl:when>
			    <xsl:when test="type = 'building'">
			      <xsl:value-of select="label"/>
			    </xsl:when>
			    <xsl:otherwise>
			      unknown type
			    </xsl:otherwise>
			  </xsl:choose>
			</option>
		      </xsl:for-each>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Record Age:
		  </td>
		  <td>
		    <select name="record_age">
		      <option value="">[---]</option>
		      <xsl:for-each select="/page/common/record_age_list/record_age">
			<option>
			  <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			  <xsl:value-of select="label"/>
			</option>
		      </xsl:for-each>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Is Active:
		  </td>
		  <td>
		    <select name="is_active">
		      <option value="">[---]</option>
		      <option value="1">Yes</option>
		      <option value="0">No</option>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		  </td>
		  <td>
		    <input type="submit" name="submitButton" value="Search"/>
		  </td>
		</tr>
	      </table>
	    </fieldset>
	  </form>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="person_form">
    <table>
      <tr>
	<td>
	  <form action="/1g/tenant/search_person_result.cgi" method="post">
	    <fieldset class="standard_fieldset">
	      <legend>
		Search People
	      </legend>
	      <table cellspacing="0" cellpadding="0" border="0" style="white-space: nowrap">
		<tr>
		  <td style="padding: 4px; ">
		    Account Name:
		  </td>
		  <td>
		    <input type="text" name="account_name"/>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Person Name:
		  </td>
		  <td>
		    <input type="text" name="person_name"/>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Email:
		  </td>
		  <td>
		    <input type="text" name="email"/>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Building/Floor:
		  </td>
		  <td>
		    <select name="building_floor_id">
		      <option value="">[---]</option>
		      <xsl:for-each select="/page/common/building_floor_list/building_floor">
			<option>
			  <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			  <xsl:choose>
			    <xsl:when test="type = 'floor'">
			      --<xsl:value-of select="label"/>
			    </xsl:when>
			    <xsl:when test="type = 'building'">
			      <xsl:value-of select="label"/>
			    </xsl:when>
			    <xsl:otherwise>
			      unknown type
			    </xsl:otherwise>
			  </xsl:choose>
			</option>
		      </xsl:for-each>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Record Age:
		  </td>
		  <td>
		    <select name="record_age">
		      <option value="">[---]</option>
		      <xsl:for-each select="/page/common/record_age_list/record_age">
			<option>
			  <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			  <xsl:value-of select="label"/>
			</option>
		      </xsl:for-each>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		    Is Active:
		  </td>
		  <td>
		    <select name="is_active">
		      <option value="">[---]</option>
		      <option value="1">Yes</option>
		      <option value="0">No</option>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td style="padding: 4px; ">
		  </td>
		  <td>
		    <input type="submit" name="submitButton" value="Search"/>
		  </td>
		</tr>
	      </table>
	    </fieldset>
	  </form>
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

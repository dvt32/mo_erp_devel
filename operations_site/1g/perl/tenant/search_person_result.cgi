#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use Encode ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_cgi_input_charset;
use get_database_charset;
use strip_outside_whitespace;
use get_row_list;
use new_result_list;
use get_row_by_id_map;
use serialize_hash;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # get form input.
  my $cgi_input_charset = get_cgi_input_charset($instance_map);
  my $account_name = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("account_name")));
  my $person_name = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("person_name")));
  my $email = strip_outside_whitespace(Encode::decode($cgi_input_charset, $cgi->param("email")));
  my $building_floor_id = $cgi->param("building_floor_id");
  my $record_age = $cgi->param("record_age");
  my $is_active = $cgi->param("is_active");
  # generate SQL query.
  my $db_charset = get_database_charset($instance_map, "main");
  my (@sql_where_clause_list, @sql_param_list, @sql_table_list);
  push(@sql_table_list, "cust_pers_tbl");
  if ($account_name) {
    my $_account_name = $account_name;
    $_account_name =~ s/\*/\%/g;
    push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
    push(@sql_where_clause_list, "cust_acct_tbl.name ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $_account_name));
    push(@sql_where_clause_list, "cust_acct_tbl.id = cust_pers_tbl.cust_acct_id");
  }
  if ($person_name) {
    my $_person_name = $person_name;
    $_person_name =~ s/\*/\%/g;
    my ($last_name, $first_name) = split(/\s*,\s*/, $_person_name);
    push(@sql_table_list, "cust_pers_tbl") if (!grep {$_ eq "cust_pers_tbl"} @sql_table_list);
    if ($first_name) {
      push(@sql_where_clause_list, "cust_pers_tbl.last_name ilike ? and cust_pers_tbl.first_name ilike ?");
      push(@sql_param_list, Encode::encode($db_charset, $last_name));
      push(@sql_param_list, Encode::encode($db_charset, $first_name));
    } else {
      push(@sql_where_clause_list, "cust_pers_tbl.last_name ilike ? or cust_pers_tbl.first_name ilike ?");
      push(@sql_param_list, Encode::encode($db_charset, $last_name));
      push(@sql_param_list, Encode::encode($db_charset, $last_name));
    }
  }
  if ($email) {
    my $_email = $email;
    $_email =~ s/\*/\%/g;
    push(@sql_table_list, "cust_pers_tbl") if (!grep {$_ eq "cust_pers_tbl"} @sql_table_list);
    push(@sql_where_clause_list, "cust_pers_tbl.email ilike ?");
    push(@sql_param_list, Encode::encode($db_charset, $_email));
  }
  if ($building_floor_id) {
    if ($building_floor_id =~ /^building_id=(\d+)$/) {
      my $building_id = $1;
      push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
      push(@sql_table_list, "floor_tbl") if (!grep {$_ eq "floor_tbl"} @sql_table_list);
      push(@sql_where_clause_list, "cust_acct_tbl.primary_floor_id = floor_tbl.id and floor_tbl.building_id = ? and cust_acct_tbl.id = cust_pers_tbl.cust_acct_id");
      push(@sql_param_list, [$building_id, SQL_INTEGER]);
    } elsif ($building_floor_id =~ /^floor_id=(\d+)$/) {
      my $floor_id = $1;
      push(@sql_table_list, "cust_acct_tbl") if (!grep {$_ eq "cust_acct_tbl"} @sql_table_list);
      push(@sql_where_clause_list, "cust_acct_tbl.primary_floor_id = ? and cust_acct_tbl.id = cust_pers_tbl.cust_acct_id");
      push(@sql_param_list, [$floor_id, SQL_INTEGER]);
    } else {
      die "unknown building/floor specifier: $building_floor_id";
    }
  }
  if ($record_age) {
    if ($record_age =~ /^\d+$/) {
      push(@sql_table_list, "cust_pers_tbl") if (!grep {$_ eq "cust_pers_tbl"} @sql_table_list);
      push(@sql_where_clause_list, "cust_pers_tbl.creation_dt >= now() - interval ?");
      push(@sql_param_list, $record_age . " days");
    } else {
      die "bad record age: $record_age";
    }
  }
  if ($is_active ne "") {
    push(@sql_table_list, "cust_pers_tbl") if (!grep {$_ eq "cust_pers_tbl"} @sql_table_list);
    if ($is_active) {
      push(@sql_where_clause_list, "cust_pers_tbl.is_active");
    } else {
      push(@sql_where_clause_list, "not(cust_pers_tbl.is_active)");
    }
  }
  if (!@sql_where_clause_list) {
    die "show_page: search_person_blank_query.html\n";
  }
  my $sql_query = "select cust_pers_tbl.id from " . join(", ", @sql_table_list) . " where " . join(" and ", map {"(" . $_ . ")"} @sql_where_clause_list) . " order by cust_pers_tbl.last_name, cust_pers_tbl.first_name";
  my @sql_row_list;
  get_row_list($dbh, $sql_query, \@sql_param_list, \@sql_row_list);
  my @cust_pers_id_list = map {$_->{"id"}} @sql_row_list;
  my $result_list_id;
  if (!@cust_pers_id_list) {
    die "show_page: search_person_no_matches.html\n";
  }
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl", undef, \%floor_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl", undef, \%building_by_id);
  my $building_name = "";
  my $floor_name = "";
  if ($building_floor_id) {
    if ($building_floor_id =~ /^building_id=(\d+)$/) {
      $building_name = $building_by_id{$1}->{"address_1"};
    } elsif ($building_floor_id =~ /^floor_id=(\d+)$/) {
      $floor_name = $floor_by_id{$1}->{"label"};
    }
  }
  # generate new result list.
  my %result_list_query_param_map = ("account_name" => $account_name, "person_name" => $person_name, "email" => $email, "building_name" => $building_name, "floor_name" => $floor_name, "record_age" => $record_age, "is_active" => $is_active);
  my $result_list_query_param = serialize_hash(\%result_list_query_param_map);
  $result_list_id = new_result_list($dbh, "employee", $employee->{"id"}, "cust_pers_tbl", \@cust_pers_id_list, $result_list_query_param);
  # get persons.
  $sql_query = "select * from cust_pers_tbl where id in (select item_id from result_item_tbl where result_list_id = ? order by index_number)";
  @sql_param_list = ([$result_list_id, SQL_INTEGER]);
  my %cust_pers_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_pers_by_id);
  # get accounts.
  $sql_query = "select * from cust_acct_tbl where id in (select cust_acct_id from cust_pers_tbl where id in (select item_id from result_item_tbl where result_list_id = ?))";
  @sql_param_list = ([$result_list_id, SQL_INTEGER]);
  my %cust_acct_by_id;
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%cust_acct_by_id);
  # fetch services.
  my %service_by_id;
  get_row_by_id_map($dbh, "select * from service_tbl", undef, \%service_by_id);
  # generate XML.
  $account_name = HTML::Entities::encode($account_name);
  $person_name = HTML::Entities::encode($person_name);
  $email = HTML::Entities::encode($email);
  $building_name = HTML::Entities::encode($building_name);
  $floor_name = HTML::Entities::encode($floor_name);
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <result_list_id>$result_list_id</result_list_id>
  <query>
    <account_name>$account_name</account_name>
    <person_name>$person_name</person_name>
    <email>$email</email>
    <building_name>$building_name</building_name>
    <floor_name>$floor_name</floor_name>
    <record_age>$record_age</record_age>
    <is_active>$is_active</is_active>
  </query>
  <person_list>
---END_BLOCK---;
  for my $cust_pers_id (@cust_pers_id_list) {
    my $cust_pers = $cust_pers_by_id{$cust_pers_id};
    my $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
    my $cust_acct_id = $cust_pers->{"cust_acct_id"};
    my $account_name = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$cust_pers_id</id>
      <person_name>$person_name</person_name>
      <account_name>$account_name</account_name>
      <email>$email</email>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "search_person_result.xslt", $cgi);
}

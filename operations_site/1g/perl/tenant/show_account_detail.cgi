#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use strip_outside_whitespace;
use get_role_list;
use get_php_base_url;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = get_role_list($instance_map, $employee->{"id"});
  my $account_id = $cgi->param("id");
  my $account;
  if ((!$account_id) || (!($account = select_row($dbh, "cust_acct_tbl", $account_id)))) {
    die "account not found: $account_id";
  }
  # fetch linked persons.
  my $sql_query = "select * from cust_pers_tbl where cust_acct_id = ? order by last_name, first_name";
  my @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%person_by_id, @person_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%person_by_id, \@person_id_list);
  # fetch primary contact person.
  my $primary_contact_person = select_row($dbh, "cust_pers_tbl", $account->{"primary_contact_cust_pers_id"});
  # fetch service.
  my $service = select_row($dbh, "service_tbl", $account->{"service_id"});
  if (!$service) {
    die "service not found: " . $account->{"service_id"};
  }
  # fetch linked access cards.
  $sql_query = "select * from proximity_card_tbl where account_id = ? order by number";
  @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%proximity_card_by_id, @proximity_card_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%proximity_card_by_id, \@proximity_card_id_list);
  # fetch linked phone numbers.
  my $dbh_phone = get_database_handle($instance_map, "phone");
  $sql_query = "select * from phone_number_tbl where account_id = ? order by phone_number";
  @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%phone_number_by_id, @phone_number_id_list);
  get_row_by_id_map($dbh_phone, $sql_query, \@sql_param_list, \%phone_number_by_id, \@phone_number_id_list);
  # fetch linked IP addresses.
  $sql_query = "select * from ip_address_tbl where account_id = ? order by ip_address";
  @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%ip_address_by_id, @ip_address_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%ip_address_by_id, \@ip_address_id_list);
  # fetch linked mailboxes.
  $sql_query = "select * from mailbox_tbl where account_id = ? order by mailbox_number";
  @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%mailbox_by_id, @mailbox_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%mailbox_by_id, \@mailbox_id_list);
  # fetch linked aliases.
  $sql_query = "select * from alias_tbl where account_id = ? order by name";
  @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%alias_by_id, @alias_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%alias_by_id, \@alias_id_list);
  # fetch linked guests.
  $sql_query = "select * from guest_person_tbl where account_id = ? and end_date >= current_date order by last_name, first_name";
  @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%guest_person_by_id, @guest_person_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%guest_person_by_id, \@guest_person_id_list);
  # declare customer payment storage.
  my (%credit_card_by_id, @credit_card_id_list);
  my (%bank_account_by_id, @bank_account_id_list);
  # check if employee has access to customer payment accounts.
  if (grep {$_ eq "role:finance/edit_cust_pmt_accts"} @$role_list) {
    # fetch linked credit cards.
    $sql_query = "select * from credit_card_tbl where account_id = ? order by card_number";
    @sql_param_list = ([$account_id, SQL_INTEGER]);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%credit_card_by_id, \@credit_card_id_list);
    # fetch linked bank accounts.
    $sql_query = "select * from bank_account_tbl where account_id = ? order by bank_routing_number, bank_account_number";
    @sql_param_list = ([$account_id, SQL_INTEGER]);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%bank_account_by_id, \@bank_account_id_list);
  }
  # declare electronic transaction storage.
  my (%credit_card_txn_by_id, @credit_card_txn_id_list);
  my (%ach_debit_txn_by_id, @ach_debit_txn_id_list);
  # check if employee has access to electronic transactions.
  if (grep {$_ eq "role:finance/show_electronic_txns"} @$role_list) {
    # fetch linked credit card transactions.
    $sql_query = "select * from credit_card_txn_tbl where cust_acct_id = ? and processed_dt is not null order by processed_dt desc limit 10";
    @sql_param_list = ([$account_id, SQL_INTEGER]);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%credit_card_txn_by_id, \@credit_card_txn_id_list);
    # fetch linked ACH transactions.
    $sql_query = "select * from ach_debit_txn_tbl where cust_acct_id = ? and processed_dt is not null order by processed_dt desc limit 10";
    @sql_param_list = ([$account_id, SQL_INTEGER]);
    get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%ach_debit_txn_by_id, \@ach_debit_txn_id_list);
  }
  # fetch primary floor name.
  my $floor = select_row($dbh, "floor_tbl", $account->{"primary_floor_id"});
  if (!$floor) {
    die "floor not found: " . $account->{"primary_floor_id"};
  }
  # php base url for template
  my $php_base_url = get_php_base_url();
  # populate information needed for display of page.
  my $name = HTML::Entities::encode(strip_outside_whitespace($account->{"name"}));
  my $external_name = HTML::Entities::encode(strip_outside_whitespace($account->{"external_name"}));
  my $primary_contact_person_name = $primary_contact_person ? HTML::Entities::encode($primary_contact_person->{"last_name"} . ", " . $primary_contact_person->{"first_name"}) : "";
  my $is_active = $account->{"is_active"} ? 1 : 0;
  my $visible = $account->{"visible"} ? 1 : 0;
  my $description = HTML::Entities::encode($account->{"description"} || "");
  my $web_site = HTML::Entities::encode($account->{"web_site"} || "");
  my $note = HTML::Entities::encode($account->{"note"} || "");
  my $tax_id = HTML::Entities::encode($account->{"tax_id"} || "");
  my $service_name = HTML::Entities::encode($service->{"name"});
  my $preferred_payment_method = HTML::Entities::encode($account->{"preferred_payment_method"});
  my $room_minutes = HTML::Entities::encode($account->{"room_minutes"});
  my $primary_floor_name = HTML::Entities::encode($floor->{"short_global_label"});
  my $creation_dt = HTML::Entities::encode($account->{"creation_dt"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account>
    <id>$account_id</id>
    <name>$name</name>
    <external_name>$external_name</external_name>
    <primary_contact_person_name>$primary_contact_person_name</primary_contact_person_name>
    <is_active>$is_active</is_active>
    <description>$description</description>
    <web_site>$web_site</web_site>
    <visible>$visible</visible>
    <note>$note</note>
    <tax_id>$tax_id</tax_id>
    <service_name>$service_name</service_name>
    <preferred_payment_method>$preferred_payment_method</preferred_payment_method>
    <room_minutes>$room_minutes</room_minutes>
    <primary_floor_name>$primary_floor_name</primary_floor_name>
    <creation_dt>$creation_dt</creation_dt>
    <php-base-url>$php_base_url</php-base-url>
  </account>
  <person_list>
---END_BLOCK---;
  for my $person_id (@person_id_list) {
    my $person = $person_by_id{$person_id};
    my $salutation = HTML::Entities::encode($person->{"salutation"});
    my $first_name = HTML::Entities::encode($person->{"first_name"});
    my $middle_name = HTML::Entities::encode($person->{"middle_name"} || "");
    my $last_name = HTML::Entities::encode($person->{"last_name"});
    my $nickname = HTML::Entities::encode($person->{"nickname"} || "");
    my $email = HTML::Entities::encode($person->{"email"} || "");
    my $is_active = $person->{"is_active"} ? 1 : 0;
    my $register_with_building = $person->{"register_with_building"} ? 1 : 0;
    my $note = HTML::Entities::encode($person->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$person_id</id>
      <salutation>$salutation</salutation>
      <first_name>$first_name</first_name>
      <middle_name>$middle_name</middle_name>
      <last_name>$last_name</last_name>
      <nickname>$nickname</nickname>
      <email>$email</email>
      <is_active>$is_active</is_active>
      <register_with_building>$register_with_building</register_with_building>
      <note>$note</note>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person_list>
  <guest_person_list>
---END_BLOCK---;
  for my $guest_person_id (@guest_person_id_list) {
    my $guest_person = $guest_person_by_id{$guest_person_id};
    my $host_contact = HTML::Entities::encode($guest_person->{"host_contact"} || "");
    my $salutation = HTML::Entities::encode($guest_person->{"salutation"} || "");
    my $first_name = HTML::Entities::encode($guest_person->{"first_name"});
    my $middle_name = HTML::Entities::encode($guest_person->{"middle_name"} || "");
    my $last_name = HTML::Entities::encode($guest_person->{"last_name"});
    my $suffix = HTML::Entities::encode($guest_person->{"suffix"} || "");
    my $nickname = HTML::Entities::encode($guest_person->{"nickname"} || "");
    my $title = HTML::Entities::encode($guest_person->{"title"} || "");
    my $company = HTML::Entities::encode($guest_person->{"company"} || "");
    my $work_phone_number = HTML::Entities::encode($guest_person->{"work_phone_number"} || "");
    my $cell_phone_number = HTML::Entities::encode($guest_person->{"cell_phone_number"} || "");
    my $email = HTML::Entities::encode($guest_person->{"email"} || "");
    my $start_date = HTML::Entities::encode($guest_person->{"start_date"});
    my $end_date = HTML::Entities::encode($guest_person->{"end_date"});
    my $note = HTML::Entities::encode($guest_person->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <guest_person>
      <id>$guest_person_id</id>
      <host_contact>$host_contact</host_contact>
      <salutation>$salutation</salutation>
      <first_name>$first_name</first_name>
      <middle_name>$middle_name</middle_name>
      <last_name>$last_name</last_name>
      <suffix>$suffix</suffix>
      <nickname>$nickname</nickname>
      <title>$title</title>
      <company>$company</company>
      <work_phone_number>$work_phone_number</work_phone_number>
      <cell_phone_number>$cell_phone_number</cell_phone_number>
      <email>$email</email>
      <start_date>$start_date</start_date>
      <end_date>$end_date</end_date>
      <note>$note</note>
    </guest_person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </guest_person_list>
  <phone_number_list>
---END_BLOCK---;
  for my $phone_number_id (@phone_number_id_list) {
    my $phone_number = $phone_number_by_id{$phone_number_id};
    my $phone_number_number = HTML::Entities::encode($phone_number->{"phone_number"});
    my $port = HTML::Entities::encode($phone_number->{"port"} || "");
    my $device_name = HTML::Entities::encode($phone_number->{"device_name"} || "");
    my $person_id = $phone_number->{"person_id"};
    my $person_name = "";
    if ($person_id) {
      my $person = $person_by_id{$person_id};
      $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
    }
    my $note = HTML::Entities::encode($phone_number->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <phone_number>
      <id>$phone_number_id</id>
      <phone_number>$phone_number_number</phone_number>
      <port>$port</port>
      <device_name>$device_name</device_name>
      <person_name>$person_name</person_name>
      <note>$note</note>
    </phone_number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </phone_number_list>
  <ip_address_list>
---END_BLOCK---;
  for my $ip_address_id (@ip_address_id_list) {
    my $ip_address = $ip_address_by_id{$ip_address_id};
    my $ip_address_address = HTML::Entities::encode($ip_address->{"ip_address"});
    my $note = HTML::Entities::encode($ip_address->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <ip_address>
      <id>$ip_address_id</id>
      <ip_address>$ip_address_address</ip_address>
      <note>$note</note>
    </ip_address>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ip_address_list>
  <mailbox_list>
---END_BLOCK---;
  for my $mailbox_id (@mailbox_id_list) {
    my $mailbox = $mailbox_by_id{$mailbox_id};
    my $mailbox_number = HTML::Entities::encode($mailbox->{"mailbox_number"});
    my $note = HTML::Entities::encode($mailbox->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <mailbox>
      <id>$mailbox_id</id>
      <mailbox_number>$mailbox_number</mailbox_number>
      <note>$note</note>
    </mailbox>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </mailbox_list>
  <alias_list>
---END_BLOCK---;
  for my $alias_id (@alias_id_list) {
    my $alias = $alias_by_id{$alias_id};
    my $name = HTML::Entities::encode($alias->{"name"});
    my $note = HTML::Entities::encode($alias->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <alias>
      <id>$alias_id</id>
      <name>$name</name>
      <note>$note</note>
    </alias>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </alias_list>
  <proximity_card_list>
---END_BLOCK---;
  for my $proximity_card_id (@proximity_card_id_list) {
    my $proximity_card = $proximity_card_by_id{$proximity_card_id};
    my $number = HTML::Entities::encode($proximity_card->{"number"});
    my $type = HTML::Entities::encode($proximity_card->{"type"});
    my $status = HTML::Entities::encode($proximity_card->{"status"});
    my $technology = HTML::Entities::encode($proximity_card->{"technology"});
    my $person_id = $proximity_card->{"person_id"};
    my $person_name = "";
    if ($person_id) {
      my $person = $person_by_id{$person_id};
      $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
    }
    my $note = HTML::Entities::encode($proximity_card->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <proximity_card>
      <id>$proximity_card_id</id>
      <number>$number</number>
      <type>$type</type>
      <status>$status</status>
      <technology>$technology</technology>
      <person_name>$person_name</person_name>
      <note>$note</note>
    </proximity_card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </proximity_card_list>
  <credit_card_list>
---END_BLOCK---;
  for my $credit_card_id (@credit_card_id_list) {
    my $credit_card = $credit_card_by_id{$credit_card_id};
    my $card_number = HTML::Entities::encode($credit_card->{"card_number"});
    my $card_expiration_month = HTML::Entities::encode($credit_card->{"card_expiration_month"});
    my $card_expiration_year = HTML::Entities::encode($credit_card->{"card_expiration_year"});
    my $billing_name = HTML::Entities::encode($credit_card->{"billing_name"});
    my $billing_zip_code = HTML::Entities::encode($credit_card->{"billing_zip_code"});
    my $person_id = $credit_card->{"person_id"};
    my $person_name = "";
    if ($person_id) {
      my $person = $person_by_id{$person_id};
      $person_name = HTML::Entities::encode($person->{"last_name"} . ", " . $person->{"first_name"});
    }
    my $note = HTML::Entities::encode($credit_card->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <credit_card>
      <id>$credit_card_id</id>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <billing_name>$billing_name</billing_name>
      <billing_zip_code>$billing_zip_code</billing_zip_code>
      <person_id>$person_id</person_id>
      <person_name>$person_name</person_name>
      <note>$note</note>
    </credit_card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </credit_card_list>
  <bank_account_list>
---END_BLOCK---;
  for my $bank_account_id (@bank_account_id_list) {
    my $bank_account = $bank_account_by_id{$bank_account_id};
    my $bank_routing_number = HTML::Entities::encode($bank_account->{"bank_routing_number"});
    my $bank_account_number = HTML::Entities::encode($bank_account->{"bank_account_number"});
    my $notes = HTML::Entities::encode($bank_account->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <bank_account>
      <id>$bank_account_id</id>
      <bank_routing_number>$bank_routing_number</bank_routing_number>
      <bank_account_number>$bank_account_number</bank_account_number>
      <notes>$notes</notes>
    </bank_account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </bank_account_list>
  <credit_card_txn_list>
---END_BLOCK---;
  for my $credit_card_txn_id (@credit_card_txn_id_list) {
    my $credit_card_txn = $credit_card_txn_by_id{$credit_card_txn_id};
    my $card_number = HTML::Entities::encode($credit_card_txn->{"card_number"});
    my $card_expiration_month = HTML::Entities::encode($credit_card_txn->{"card_expiration_month"});
    my $card_expiration_year = HTML::Entities::encode($credit_card_txn->{"card_expiration_year"});
    my $total_charge = HTML::Entities::encode($credit_card_txn->{"total_charge"});
    my $processed_dt = HTML::Entities::encode($credit_card_txn->{"processed_dt"});
    my $response_error = HTML::Entities::encode($credit_card_txn->{"response_error"});
    my $response_approved = HTML::Entities::encode($credit_card_txn->{"response_approved"});
    my $notes = HTML::Entities::encode($credit_card_txn->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <credit_card_txn>
      <id>$credit_card_txn_id</id>
      <card_number>$card_number</card_number>
      <card_expiration_month>$card_expiration_month</card_expiration_month>
      <card_expiration_year>$card_expiration_year</card_expiration_year>
      <total_charge>$total_charge</total_charge>
      <processed_dt>$processed_dt</processed_dt>
      <response_error>$response_error</response_error>
      <response_approved>$response_approved</response_approved>
      <notes>$notes</notes>
    </credit_card_txn>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </credit_card_txn_list>
  <ach_debit_txn_list>
---END_BLOCK---;
  for my $ach_debit_txn_id (@ach_debit_txn_id_list) {
    my $ach_debit_txn = $ach_debit_txn_by_id{$ach_debit_txn_id};
    my $amount = HTML::Entities::encode($ach_debit_txn->{"amount"});
    my $customer_bank_id = HTML::Entities::encode($ach_debit_txn->{"customer_bank_id"});
    my $customer_bank_acct_num = HTML::Entities::encode($ach_debit_txn->{"customer_bank_acct_num"});
    my $processed_dt = HTML::Entities::encode($ach_debit_txn->{"processed_dt"});
    my $notes = HTML::Entities::encode($ach_debit_txn->{"notes"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <ach_debit_txn>
      <id>$ach_debit_txn_id</id>
      <amount>$amount</amount>
      <customer_bank_id>$customer_bank_id</customer_bank_id>
      <customer_bank_acct_num>$customer_bank_acct_num</customer_bank_acct_num>
      <processed_dt>$processed_dt</processed_dt>
      <notes>$notes</notes>
    </ach_debit_txn>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </ach_debit_txn_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_account_detail.xslt", $cgi);
}

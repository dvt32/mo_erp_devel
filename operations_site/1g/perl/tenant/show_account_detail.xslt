<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Customer :: Account Detail
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <xsl:apply-templates select="account"/>
	</p>
	<table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	  <tr class="standard_table_column_heading_row">
	    <td class="standard_table_side_padding"></td>
	    <td>
	      Persons
	    </td>
	    <td class="standard_table_spacer_column"></td>
	    <td align="right">
	      <a>
		<xsl:attribute name="href">/1g/tenant/new_person_input.cgi?account_id=<xsl:value-of select="account/id"/></xsl:attribute>
		<xsl:attribute name="class">bare_link</xsl:attribute>
		New
	      </a>
	    </td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td colspan="3">
	      <xsl:apply-templates select="person_list"/>
	    </td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</table>
	<xsl:if test="count(guest_person_list/guest_person) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Guests
	      </td>
	      <td class="standard_table_spacer_column"></td>
	      <td align="right">
		<a>
		  <xsl:attribute name="href">/1g/tenant/new_guest_input.cgi?account_id=<xsl:value-of select="account/id"/></xsl:attribute>
		  <xsl:attribute name="class">bare_link</xsl:attribute>
		  New
		</a>
		/
		<a>
		  <xsl:attribute name="href">/1g/tenant/batch_add_guest_input.cgi?account_id=<xsl:value-of select="account/id"/></xsl:attribute>
		  <xsl:attribute name="class">bare_link</xsl:attribute>
		  Batch Add
		</a>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td colspan="3">
		<xsl:apply-templates select="guest_person_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(phone_number_list/phone_number) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Phone Numbers
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="phone_number_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(proximity_card_list/proximity_card) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Access Cards
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="proximity_card_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(alias_list/alias) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Aliases
	      </td>
	      <td class="standard_table_spacer_column"></td>
	      <td align="right">
		<a>
		  <xsl:attribute name="href">/1g/tenant/new_alias_input.cgi?account_id=<xsl:value-of select="account/id"/></xsl:attribute>
		  <xsl:attribute name="class">bare_link</xsl:attribute>
		  New
		</a>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td colspan="3">
		<xsl:apply-templates select="alias_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(mailbox_list/mailbox) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Mailboxes
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="mailbox_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(ip_address_list/ip_address) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		IP Addresses
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="ip_address_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(credit_card_list/credit_card) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Credit Cards
	      </td>
	      <td class="standard_table_spacer_column"></td>
	      <td align="right">
		<a>
		  <xsl:attribute name="href">/1g/finance/new_cust_card_input.cgi?account_id=<xsl:value-of select="account/id"/></xsl:attribute>
		  <xsl:attribute name="class">bare_link</xsl:attribute>
		  New
		</a>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td colspan="3">
		<xsl:apply-templates select="credit_card_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(bank_account_list/bank_account) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Bank Accounts
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="bank_account_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(credit_card_txn_list/credit_card_txn) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Credit Card Transactions (10 most recent)
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="credit_card_txn_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(ach_debit_txn_list/ach_debit_txn) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		ACH Debit Transactions (10 most recent)
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="ach_debit_txn_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<br/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td class="standard_table_title">&nbsp;</td>
	<td class="standard_table_title">
          Account Detail for <xsl:value-of select="name"/> (Account ID: <xsl:value-of select="id"/>)
	</td>
	<td class="standard_table_title" style="text-align: right;">
	  <a>
	    <xsl:attribute name="class">bare_link</xsl:attribute>
	    <xsl:attribute name="style">color: white;</xsl:attribute>
	    <xsl:attribute name="href">/1g/tenant/edit_account_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	    <xsl:text>Edit</xsl:text>
	  </a>
	</td>
      </tr>
      <tr>
	<td class="standard_table_side_padding"></td>
	<td>
	  <xsl:if test="name != external_name">
	    External Name: <xsl:value-of select="external_name"/> /
	  </xsl:if>
	  Service: <xsl:value-of select="service_name"/> /
	  Floor and Building: <xsl:value-of select="primary_floor_name"/> /
	  Pmt Method: <xsl:value-of select="preferred_payment_method"/> /
	  Room Mins: <xsl:value-of select="room_minutes"/> /
	  Tax ID: <xsl:value-of select="tax_id"/>
	  <br/>
	  Primary Contact: <xsl:value-of select="primary_contact_person_name"/>
	  /
	  Note: <xsl:value-of select="note"/>
	  <br/>
	  Description: <xsl:value-of select="description"/>
	  /
	  Web Site: <xsl:value-of select="web_site"/>
	  <br/>
	  Active:
	  <xsl:choose>
	    <xsl:when test="is_active = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	  /
	  Visible:
	  <xsl:choose>
	    <xsl:when test="visible = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	  /
	  Created: <xsl:value-of select="substring(creation_dt, 1, 19)"/>
	</td>
	<td class="standard_table_side_padding"></td>
    </tr>
      <tr>
      <td class="standard_table_side_padding"></td>
      <td>
      <a>
	    <xsl:attribute name="class">bare_link</xsl:attribute>
	    <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/accounts/deskHistory/<xsl:value-of select="id"/></xsl:attribute>
	    <xsl:text>View desk assignment history</xsl:text>
	  </a>
      </td>
    </tr>
    </table>
  </xsl:template>

  <xsl:template match="person_list">
    <xsl:apply-templates select="person"/>
  </xsl:template>

  <xsl:template match="person">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/tenant/show_person_detail.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="last_name"/><xsl:text>, </xsl:text><xsl:value-of select="first_name"/>
      <xsl:if test="middle_name != ''"><xsl:text> </xsl:text><xsl:value-of select="middle_name"/></xsl:if>
    </a>
    <xsl:if test="nickname != ''">
      /
      nickname: <xsl:value-of select="nickname"/>
    </xsl:if>
    <xsl:if test="email != ''">
      /
      email: <a><xsl:attribute name="href">mailto:<xsl:value-of select="email"/></xsl:attribute><xsl:value-of select="email"/></a>
    </xsl:if>
    /
    active:
    <xsl:choose>
      <xsl:when test="is_active = 1">
	yes
      </xsl:when>
      <xsl:otherwise>
	no
      </xsl:otherwise>
    </xsl:choose>
    /
    24 hour access:
    <xsl:choose>
      <xsl:when test="register_with_building = 1">
	yes
      </xsl:when>
      <xsl:otherwise>
	no
      </xsl:otherwise>
    </xsl:choose>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="guest_person_list">
    <xsl:apply-templates select="guest_person"/>
  </xsl:template>

  <xsl:template match="guest_person">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/tenant/edit_guest_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="last_name"/><xsl:text>, </xsl:text><xsl:value-of select="first_name"/>
      <xsl:if test="middle_name != ''"><xsl:text> </xsl:text><xsl:value-of select="middle_name"/></xsl:if>
    </a>
    /
    start date: <xsl:value-of select="start_date"/>
    /
    end date: <xsl:value-of select="end_date"/>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="mailbox_list">
    <xsl:apply-templates select="mailbox"/>
  </xsl:template>

  <xsl:template match="mailbox">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/tenant/edit_mailbox_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="mailbox_number"/>
    </a>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="alias_list">
    <xsl:apply-templates select="alias"/>
  </xsl:template>

  <xsl:template match="alias">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/tenant/edit_alias_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="name"/>
    </a>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="phone_number_list">
    <xsl:apply-templates select="phone_number"/>
  </xsl:template>

  <xsl:template match="phone_number">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/telecom/edit_phone_number_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="phone_number"/>
    </a>
    /
    device: <xsl:value-of select="device_name"/>
    <xsl:if test="port != ''">
      /
      port: <xsl:value-of select="port"/>
    </xsl:if>
    <xsl:if test="person_name != ''">
      /
      person: <xsl:value-of select="person_name"/>
    </xsl:if>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="ip_address_list">
    <xsl:apply-templates select="ip_address"/>
  </xsl:template>

  <xsl:template match="ip_address">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/telecom/edit_ip_address_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="ip_address"/>
    </a>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="proximity_card_list">
    <xsl:apply-templates select="proximity_card"/>
  </xsl:template>

  <xsl:template match="proximity_card">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/tenant/edit_proximity_card_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="number"/>
    </a>
    /
    type: <xsl:value-of select="type"/>
    /
    status: <xsl:value-of select="status"/>
    /
    technology: <xsl:value-of select="technology"/>
    <xsl:if test="person_name != ''">
      /
      person: <xsl:value-of select="person_name"/>
    </xsl:if>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="credit_card_list">
    <xsl:apply-templates select="credit_card"/>
  </xsl:template>

  <xsl:template match="credit_card">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/finance/edit_cust_card_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="card_number"/>
    </a>
    /
    exp: <xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/>
    /
    name on card: <xsl:value-of select="billing_name"/>
    /
    zip code: <xsl:value-of select="billing_zip_code"/>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="bank_account_list">
    <xsl:apply-templates select="bank_account"/>
  </xsl:template>

  <xsl:template match="bank_account">
    routing: <xsl:value-of select="bank_routing_number"/>
    /
    account: <xsl:value-of select="bank_account_number"/>
    /
    note: <xsl:value-of select="notes"/>
    <br/>
  </xsl:template>

  <xsl:template match="credit_card_txn_list">
    <xsl:apply-templates select="credit_card_txn"/>
  </xsl:template>

  <xsl:template match="credit_card_txn">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/finance/show_card_txn_detail.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="substring(processed_dt, 1, 19)"/>
    </a>
    /
    card: <xsl:value-of select="card_number"/>
    /
    exp: <xsl:value-of select="card_expiration_month"/>/<xsl:value-of select="card_expiration_year"/>
    /
    amount: $<xsl:value-of select="total_charge"/>
    /
    result: <xsl:value-of select="response_error"/> <xsl:value-of select="response_approved"/>
    /
    note: <xsl:value-of select="notes"/>
    <br/>
  </xsl:template>

  <xsl:template match="ach_debit_txn_list">
    <xsl:apply-templates select="ach_debit_txn"/>
  </xsl:template>

  <xsl:template match="ach_debit_txn">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/finance/show_ach_txn_detail.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="substring(processed_dt, 1, 19)"/>
    </a>
    /
    routing: <xsl:value-of select="customer_bank_id"/>
    /
    account: <xsl:value-of select="customer_bank_acct_num"/>
    /
    amount: $<xsl:value-of select="amount"/>
    /
    note: <xsl:value-of select="notes"/>
    <br/>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;
use get_php_base_url;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch floor id from params
  my $floor_id = (defined($cgi->param("floor_id")))?$cgi->param("floor_id"):0;
  # should we order by floor?
  my $order_by = ($cgi->param("order_by"))?$cgi->param("order_by"):0;
  # fetch accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query;
  if(!$floor_id){
    if($order_by){
        if($order_by eq "account"){
            $sql_query = "select * from cust_acct_tbl where is_active order by name";            
        }elsif($order_by eq "contact"){
            $sql_query = "select cust_acct_tbl.* 
                from cust_acct_tbl 
                    left join cust_pers_tbl 
                    on cust_pers_tbl.id = cust_acct_tbl.primary_contact_cust_pers_id
                where cust_acct_tbl.is_active 
                order by last_name, first_name";            
        }elsif($order_by eq "floor"){
            $sql_query = "select * from cust_acct_tbl where is_active order by primary_floor_id, name";            
        }elsif($order_by eq "service"){
            $sql_query = "select * from cust_acct_tbl where is_active order by service_id, name";            
        }else{
            $sql_query = "select * from cust_acct_tbl where is_active order by name";
        }
                
    }else{
        $sql_query = "select * from cust_acct_tbl where is_active order by name";
    }
  }else{
    $sql_query = sprintf("select * from cust_acct_tbl where is_active and primary_floor_id = %d order by name",$floor_id);
  }
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # fetch persons.
  my %cust_pers_by_id;
  get_row_by_id_map($dbh, "select * from cust_pers_tbl where is_active", undef, \%cust_pers_by_id);
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active", undef, \%floor_by_id);
  # fetch services.
  my %service_by_id;
  get_row_by_id_map($dbh, "select * from service_tbl", undef, \%service_by_id);
  # generate XML.
  my $generated_xml = "";
  # php base url for template
  my $php_base_url = get_php_base_url();
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    my $cust_pers_id = $cust_acct->{"primary_contact_cust_pers_id"};
    my $person_name = "";
    if (defined($cust_pers_id)) {
      my $cust_pers = $cust_pers_by_id{$cust_pers_id};
      if ($cust_pers) {
	$person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
	if ($cust_pers->{"middle_name"}) {
	  $person_name .= " " . $cust_pers->{"middle_name"};
	}
      } else {
	$person_name = "(inactive person)";
      }
      $person_name = HTML::Entities::encode($person_name);
    }
    my $floor = $floor_by_id{$cust_acct->{"primary_floor_id"}};
    my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
    my $floor_id = $cust_acct->{"primary_floor_id"};
    my $service_name = "";
    if (my $service_id = $cust_acct->{"service_id"}) {
      $service_name = HTML::Entities::encode($service_by_id{$service_id}->{"name"} || "");
    }
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <account-name>$account_name</account-name>
      <person-name>$person_name</person-name>
      <floor-label>$floor_label</floor-label>
      <floor-id>$floor_id</floor-id>
      <service-name>$service_name</service-name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
  <php-base-url>$php_base_url</php-base-url>
  <current-floor-id>$floor_id</current-floor-id>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_active_accounts.xslt", $cgi);
}

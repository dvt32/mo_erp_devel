<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Accounts
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="account-list"/>
	<p>
	  Create a <a href="/1g/tenant/new_account_input.cgi">new account</a>,
	  show <a href="/1g/tenant/show_inactive_accounts.cgi">inactive accounts</a>,
      <xsl:if test="current-floor-id > 0">
        email <a>
               <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/tenants/emailFloor/<xsl:value-of select="current-floor-id"/></xsl:attribute>
               <xsl:value-of select="account-name"/>
            tenants,</a>
      </xsl:if> or
	  return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="9" class="standard_table_title">
          Active Accounts
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td><a href="/1g/tenant/show_active_accounts.cgi?order_by=account" class="bare_link">Account</a></td>
	<td class="standard_table_spacer_column"></td>
	<td><a href="/1g/tenant/show_active_accounts.cgi?order_by=contact" class="bare_link">Primary Contact</a></td>
	<td class="standard_table_spacer_column"></td>
	<td><a href="/1g/tenant/show_active_accounts.cgi?order_by=floor" class="bare_link">Building and Floor</a></td>
	<td class="standard_table_spacer_column"></td>
	<td><a href="/1g/tenant/show_active_accounts.cgi?order_by=service" class="bare_link">Service</a></td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="account"/>
    </table>
  </xsl:template>

  <xsl:template match="account">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/tenant/edit_account_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="account-name"/>
	</a>
	(#<xsl:value-of select="id"/>)
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="person-name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
        <a>
            <xsl:attribute name="href">/1g/tenant/show_active_accounts.cgi?floor_id=<xsl:value-of select="floor-id"/></xsl:attribute>
            <xsl:attribute name="class">bare_link</xsl:attribute>
            <xsl:value-of select="floor-label"/>
        </a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="service-name"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

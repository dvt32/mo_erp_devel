#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch aliases.
  my (%alias_by_id, @alias_id_list);
  my $sql_query = "select alias_tbl.* from alias_tbl, cust_acct_tbl where alias_tbl.account_id = cust_acct_tbl.id and cust_acct_tbl.is_active order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%alias_by_id, \@alias_id_list);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <alias-list>
---END_BLOCK---;
  for my $alias_id (@alias_id_list) {
    my $alias = $alias_by_id{$alias_id};
    my $account_id = $alias->{"account_id"};
    my $account_name = "";
    if (defined($account_id)) {
      my $account = $account_by_id{$account_id};
      if ($account) {
	$account_name = $account->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $name = HTML::Entities::encode($alias->{"name"});
    my $note = HTML::Entities::encode($alias->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <alias>
      <id>$alias_id</id>
      <account-name>$account_name</account-name>
      <name>$name</name>
      <note>$note</note>
    </alias>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </alias-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_active_aliases.xslt", $cgi);
}

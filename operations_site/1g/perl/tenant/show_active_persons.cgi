#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  my $sql_query = "select cust_pers_tbl.* from cust_pers_tbl, cust_acct_tbl where cust_pers_tbl.is_active and cust_pers_tbl.cust_acct_id = cust_acct_tbl.id and cust_acct_tbl.is_active order by cust_pers_tbl.last_name, cust_pers_tbl.first_name, cust_pers_tbl.middle_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id, \@cust_pers_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl where is_active";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <person-list>
---END_BLOCK---;
  for my $cust_pers_id (@cust_pers_id_list) {
    my $cust_pers = $cust_pers_by_id{$cust_pers_id};
    my $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
    my $cust_acct_id = $cust_pers->{"cust_acct_id"};
    my $account_name = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = $cust_acct->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$cust_pers_id</id>
      <person-name>$person_name</person-name>
      <account-name>$account_name</account-name>
      <email>$email</email>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_active_persons.xslt", $cgi);
}

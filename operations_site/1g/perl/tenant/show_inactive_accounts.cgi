#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch accounts.
  my (%cust_acct_by_id, @cust_acct_id_list);
  my $sql_query = "select * from cust_acct_tbl where not(is_active) order by name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id, \@cust_acct_id_list);
  # fetch persons.
  my %cust_pers_by_id;
  get_row_by_id_map($dbh, "select * from cust_pers_tbl", undef, \%cust_pers_by_id);
  # fetch floors.
  my %floor_by_id;
  get_row_by_id_map($dbh, "select * from floor_tbl where is_active", undef, \%floor_by_id);
  # fetch buildings.
  my %building_by_id;
  get_row_by_id_map($dbh, "select * from building_tbl where is_active", undef, \%building_by_id);
  # fetch services.
  my %service_by_id;
  get_row_by_id_map($dbh, "select * from service_tbl", undef, \%service_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <account-list>
---END_BLOCK---;
  for my $cust_acct_id (@cust_acct_id_list) {
    my $cust_acct = $cust_acct_by_id{$cust_acct_id};
    my $account_name = HTML::Entities::encode($cust_acct->{"name"});
    my $cust_pers_id = $cust_acct->{"primary_contact_cust_pers_id"};
    my $person_name = "";
    if (defined($cust_pers_id)) {
      my $cust_pers = $cust_pers_by_id{$cust_pers_id};
      $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
      if ($cust_pers->{"middle_name"}) {
	$person_name .= " " . $cust_pers->{"middle_name"};
      }
      $person_name = HTML::Entities::encode($person_name);
    }
    my $floor_id = $cust_acct->{"primary_floor_id"};
    my $floor = $floor_by_id{$floor_id};
    my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
    my $service_name = HTML::Entities::encode($service_by_id{$cust_acct->{"service_id"}}->{"name"});
    $generated_xml .= <<"---END_BLOCK---;";
    <account>
      <id>$cust_acct_id</id>
      <account-name>$account_name</account-name>
      <person-name>$person_name</person-name>
      <floor-label>$floor_label</floor-label>
      <service-name>$service_name</service-name>
    </account>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </account-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_inactive_accounts.xslt", $cgi);
}

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Inactive Accounts
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="account-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="13" class="standard_table_title">
          Inactive Accounts
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td align="center">#</td>
	<td class="standard_table_spacer_column"></td>
	<td></td>
	<td class="standard_table_spacer_column"></td>
	<td>Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Primary Contact</td>
	<td class="standard_table_spacer_column"></td>
	<td>Building and Floor</td>
	<td class="standard_table_spacer_column"></td>
	<td>Service</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="account"/>
      <tr>
	<td colspan="13" class="standard_table_footer">
	  Legend: A = activate account.
	</td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="account">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td align="right"><xsl:value-of select="position()"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/tenant/activate_account.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:text>A</xsl:text>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account-name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="person-name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="floor-label"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="service-name"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

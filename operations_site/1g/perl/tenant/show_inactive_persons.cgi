#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # fetch persons.
  my (%cust_pers_by_id, @cust_pers_id_list);
  my $sql_query = "select * from cust_pers_tbl where not(is_active) order by last_name, first_name, middle_name";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_pers_by_id, \@cust_pers_id_list);
  # fetch accounts.
  my %cust_acct_by_id;
  $sql_query = "select * from cust_acct_tbl";
  get_row_by_id_map($dbh, $sql_query, undef, \%cust_acct_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <person-list>
---END_BLOCK---;
  for my $cust_pers_id (@cust_pers_id_list) {
    my $cust_pers = $cust_pers_by_id{$cust_pers_id};
    my $person_name = $cust_pers->{"last_name"} . ", " . $cust_pers->{"first_name"};
    if ($cust_pers->{"middle_name"}) {
      $person_name .= " " . $cust_pers->{"middle_name"};
    }
    $person_name = HTML::Entities::encode($person_name);
    my $cust_acct_id = $cust_pers->{"cust_acct_id"};
    my $account_name = "";
    if (defined($cust_acct_id)) {
      my $cust_acct = $cust_acct_by_id{$cust_acct_id};
      if ($cust_acct) {
	$account_name = HTML::Entities::encode($cust_acct->{"name"});
      }
    }
    my $email = HTML::Entities::encode($cust_pers->{"email"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <person>
      <id>$cust_pers_id</id>
      <person-name>$person_name</person-name>
      <account-name>$account_name</account-name>
      <email>$email</email>
    </person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </person-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_inactive_persons.xslt", $cgi);
}

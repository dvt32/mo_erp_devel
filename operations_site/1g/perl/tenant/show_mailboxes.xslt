<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Mailboxes
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <a href="/1g/index.cgi">Main menu</a> |
	  <a>
	    <xsl:attribute name="href">/1g/tenant/mail_status_input.cgi?floor_id=<xsl:value-of select="floor_id"/></xsl:attribute>
	    <xsl:text>Update Mail Status</xsl:text>
	  </a>
	</p>
	<xsl:apply-templates select="mailbox-list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="mailbox-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="11" class="standard_table_title">
          Mailboxes at
	  <xsl:value-of select="../floor-label"/>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Num</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Note</td>
	<td class="standard_table_spacer_column"></td>
	<td>Last Checked</td>
	<td class="standard_table_spacer_column"></td>
	<td>Has Mail</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="mailbox"/>
    </table>
  </xsl:template>

  <xsl:template match="mailbox">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/tenant/edit_mailbox_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="number"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account-name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="note"/></td>
      <td class="standard_table_spacer_column"></td>
      <td>
	<xsl:if test="account-name != ''">
	  <xsl:value-of select="substring(last_checked_dt, 1, 16)"/>
	</xsl:if>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td style="text-align: center;">
	<xsl:if test="account-name != ''">
	  <xsl:choose>
	    <xsl:when test="bool_mail_present = 1">
	      Yes
	    </xsl:when>
	    <xsl:otherwise>
	      No
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:if>
      </td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

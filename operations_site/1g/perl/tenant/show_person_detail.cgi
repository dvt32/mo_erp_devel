#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_database_handle;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;
use strip_outside_whitespace;
use get_role_list;
use get_php_base_url;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $role_list = get_role_list($instance_map, $employee->{"id"});
  my $person_id = $cgi->param("id");
  my $person;
  if ((!$person_id) || (!($person = select_row($dbh, "cust_pers_tbl", $person_id)))) {
    die "person not found: $person_id";
  }
  # fetch linked account.
  my $account_id = $person->{"cust_acct_id"};
  my $account = select_row($dbh, "cust_acct_tbl", $account_id);
  if (!$account) {
    die "account not found: $account_id";
  }
  # fetch primary contact person.
  my $primary_contact_person = select_row($dbh, "cust_pers_tbl", $account->{"primary_contact_cust_pers_id"});
  if (!$primary_contact_person) {
    die "primary contact person not found: " . $account->{"primary_contact_cust_pers_id"};
  }
  # fetch service.
  my $service = select_row($dbh, "service_tbl", $account->{"service_id"});
  if (!$service) {
    die "service not found: " . $account->{"service_id"};
  }
  # fetch primary floor name.
  my $floor = select_row($dbh, "floor_tbl", $account->{"primary_floor_id"});
  if (!$floor) {
    die "floor not found: " . $account->{"primary_floor_id"};
  }
  # fetch linked access cards.
  my $sql_query = "select * from proximity_card_tbl where person_id = ? order by number";
  my @sql_param_list = ([$person_id, SQL_INTEGER]);
  my (%proximity_card_by_id, @proximity_card_id_list);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%proximity_card_by_id, \@proximity_card_id_list);
  # fetch linked phone numbers.
  my $dbh_phone = get_database_handle($instance_map, "phone");
  $sql_query = "select * from phone_number_tbl where person_id = ? order by phone_number";
  @sql_param_list = ([$account_id, SQL_INTEGER]);
  my (%phone_number_by_id, @phone_number_id_list);
  get_row_by_id_map($dbh_phone, $sql_query, \@sql_param_list, \%phone_number_by_id, \@phone_number_id_list);
  # php base url for template
  my $php_base_url = get_php_base_url();
  # populate information needed for display of page.
  my $salutation = HTML::Entities::encode($person->{"salutation"});
  my $first_name = HTML::Entities::encode($person->{"first_name"});
  my $middle_name = HTML::Entities::encode($person->{"middle_name"} || "");
  my $last_name = HTML::Entities::encode($person->{"last_name"});
  my $nickname = HTML::Entities::encode($person->{"nickname"} || "");
  my $email = HTML::Entities::encode($person->{"email"} || "");
  my $work_phone_number = HTML::Entities::encode($person->{"work_phone_number"} || "");
  my $home_phone_number = HTML::Entities::encode($person->{"home_phone_number"} || "");
  my $cell_phone_number = HTML::Entities::encode($person->{"cell_phone_number"} || "");
  my $other_phone_number = HTML::Entities::encode($person->{"other_phone_number"} || "");
  my $is_active = $person->{"is_active"} ? 1 : 0;
  my $register_with_building = $person->{"register_with_building"} ? 1 : 0;
  my $note = HTML::Entities::encode($person->{"note"} || "");
  my $account_name = HTML::Entities::encode(strip_outside_whitespace($account->{"name"}));
  my $account_external_name = HTML::Entities::encode(strip_outside_whitespace($account->{"external_name"}));
  my $primary_contact_person_name = HTML::Entities::encode($primary_contact_person->{"last_name"} . ", " . $primary_contact_person->{"first_name"});
  my $account_is_active = $account->{"is_active"} ? 1 : 0;
  my $visible = $account->{"visible"} ? 1 : 0;
  my $description = HTML::Entities::encode($account->{"description"} || "");
  my $web_site = HTML::Entities::encode($account->{"web_site"} || "");
  my $account_note = HTML::Entities::encode($account->{"note"} || "");
  my $tax_id = HTML::Entities::encode($account->{"tax_id"} || "");
  my $service_name = HTML::Entities::encode($service->{"name"});
  my $preferred_payment_method = HTML::Entities::encode($account->{"preferred_payment_method"});
  my $room_minutes = HTML::Entities::encode($account->{"room_minutes"});
  my $primary_floor_name = HTML::Entities::encode($floor->{"short_global_label"});
  my $creation_dt = HTML::Entities::encode($account->{"creation_dt"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <person>
    <id>$person_id</id>
    <salutation>$salutation</salutation>
    <first_name>$first_name</first_name>
    <middle_name>$middle_name</middle_name>
    <last_name>$last_name</last_name>
    <nickname>$nickname</nickname>
    <email>$email</email>
    <work_phone_number>$work_phone_number</work_phone_number>
    <home_phone_number>$home_phone_number</home_phone_number>
    <cell_phone_number>$cell_phone_number</cell_phone_number>
    <other_phone_number>$other_phone_number</other_phone_number>
    <is_active>$is_active</is_active>
    <register_with_building>$register_with_building</register_with_building>
    <note>$note</note>
    <php-base-url>$php_base_url</php-base-url>
  </person>
  <account>
    <id>$account_id</id>
    <name>$account_name</name>
    <external_name>$account_external_name</external_name>
    <primary_contact_person_name>$primary_contact_person_name</primary_contact_person_name>
    <is_active>$account_is_active</is_active>
    <description>$description</description>
    <web_site>$web_site</web_site>
    <visible>$visible</visible>
    <note>$account_note</note>
    <tax_id>$tax_id</tax_id>
    <service_name>$service_name</service_name>
    <preferred_payment_method>$preferred_payment_method</preferred_payment_method>
    <room_minutes>$room_minutes</room_minutes>
    <primary_floor_name>$primary_floor_name</primary_floor_name>
    <creation_dt>$creation_dt</creation_dt>
  </account>
  <phone_number_list>
---END_BLOCK---;
  for my $phone_number_id (@phone_number_id_list) {
    my $phone_number = $phone_number_by_id{$phone_number_id};
    my $phone_number_number = HTML::Entities::encode($phone_number->{"phone_number"});
    my $port = HTML::Entities::encode($phone_number->{"port"} || "");
    my $device_name = HTML::Entities::encode($phone_number->{"device_name"} || "");
    my $note = HTML::Entities::encode($phone_number->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <phone_number>
      <id>$phone_number_id</id>
      <phone_number>$phone_number_number</phone_number>
      <port>$port</port>
      <device_name>$device_name</device_name>
      <note>$note</note>
    </phone_number>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </phone_number_list>
  <proximity_card_list>
---END_BLOCK---;
  for my $proximity_card_id (@proximity_card_id_list) {
    my $proximity_card = $proximity_card_by_id{$proximity_card_id};
    my $number = HTML::Entities::encode($proximity_card->{"number"});
    my $type = HTML::Entities::encode($proximity_card->{"type"});
    my $status = HTML::Entities::encode($proximity_card->{"status"});
    my $technology = HTML::Entities::encode($proximity_card->{"technology"});
    my $note = HTML::Entities::encode($proximity_card->{"note"} || "");
    $generated_xml .= <<"---END_BLOCK---;";
    <proximity_card>
      <id>$proximity_card_id</id>
      <number>$number</number>
      <type>$type</type>
      <status>$status</status>
      <technology>$technology</technology>
      <note>$note</note>
    </proximity_card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </proximity_card_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_person_detail.xslt", $cgi);
}

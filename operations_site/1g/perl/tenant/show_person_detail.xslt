<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Customer :: Person Detail
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<p>
	  <xsl:apply-templates select="person"/>
	</p>
	<table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	  <tr class="standard_table_column_heading_row">
	    <td class="standard_table_side_padding"></td>
	    <td>
	      Account
	    </td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	  <tr>
	    <td class="standard_table_side_padding"></td>
	    <td>
	      <xsl:apply-templates select="account"/>
	    </td>
	    <td class="standard_table_side_padding"></td>
	  </tr>
	</table>
	<xsl:if test="count(phone_number_list/phone_number) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Phone Numbers
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="phone_number_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<xsl:if test="count(proximity_card_list/proximity_card) &gt; 0">
	  <br/>
	  <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
	    <tr class="standard_table_column_heading_row">
	      <td class="standard_table_side_padding"></td>
	      <td>
		Access Cards
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	    <tr>
	      <td class="standard_table_side_padding"></td>
	      <td>
		<xsl:apply-templates select="proximity_card_list"/>
	      </td>
	      <td class="standard_table_side_padding"></td>
	    </tr>
	  </table>
	</xsl:if>
	<br/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="person">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td class="standard_table_title">&nbsp;</td>
	<td class="standard_table_title">
          Person Detail for
	  <xsl:value-of select="last_name"/><xsl:text>, </xsl:text>
	  <xsl:value-of select="first_name"/><xsl:text> </xsl:text>
	  <xsl:value-of select="middle_name"/>
	  (Person ID: <xsl:value-of select="id"/>)
	</td>
	<td class="standard_table_title" style="text-align: right;">
	  <a>
	    <xsl:attribute name="class">bare_link</xsl:attribute>
	    <xsl:attribute name="style">color: white;</xsl:attribute>
	    <xsl:attribute name="href">/1g/tenant/edit_person_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	    <xsl:text>Edit</xsl:text>
	  </a>
	</td>
      </tr>
      <tr>
	<td class="standard_table_side_padding"></td>
	<td>
	  <xsl:if test="nickname != ''">
	    nickname: <xsl:value-of select="nickname"/>
	    /
	  </xsl:if>
	  <xsl:if test="email != ''">
	    email: <xsl:value-of select="email"/>
	    /
	  </xsl:if>
	  <xsl:if test="work_phone_number != ''">
	    work phone: <xsl:value-of select="work_phone_number"/>
	    /
	  </xsl:if>
	  <xsl:if test="home_phone_number != ''">
	    home phone: <xsl:value-of select="home_phone_number"/>
	    /
	  </xsl:if>
	  <xsl:if test="cell_phone_number != ''">
	    cell phone: <xsl:value-of select="cell_phone_number"/>
	    /
	  </xsl:if>
	  <xsl:if test="other_phone_number != ''">
	    other phone: <xsl:value-of select="other_phone_number"/>
	    /
	  </xsl:if>
	  active:
	  <xsl:choose>
	    <xsl:when test="is_active = 1">
	      yes
	    </xsl:when>
	    <xsl:otherwise>
	      no
	    </xsl:otherwise>
	  </xsl:choose>
	  /
	  24 hour access:
	  <xsl:choose>
	    <xsl:when test="register_with_building = 1">
	      yes
	    </xsl:when>
	    <xsl:otherwise>
	      no
	    </xsl:otherwise>
	  </xsl:choose>
	  /
	  note: <xsl:value-of select="note"/>
    <br />
    <a>
        <xsl:attribute name="class">bare_link</xsl:attribute>
        <xsl:attribute name="href"><xsl:value-of select="php-base-url"/>/people/deskHistory/<xsl:value-of select="id"/></xsl:attribute>
        <xsl:text>View desk assignment history</xsl:text>
    </a>
	</td>
	<td class="standard_table_side_padding"></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="account">
    name:
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/tenant/show_account_detail.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="name"/> (Account ID: <xsl:value-of select="id"/>)
    </a>
    <br/>
    <xsl:if test="name != external_name">
      external name: <xsl:value-of select="external_name"/> /
    </xsl:if>
    service: <xsl:value-of select="service_name"/> /
    floor and building: <xsl:value-of select="primary_floor_name"/> /
    pmt method: <xsl:value-of select="preferred_payment_method"/> /
    room mins: <xsl:value-of select="room_minutes"/> /
    tax id: <xsl:value-of select="tax_id"/>
    <br/>
    primary contact: <xsl:value-of select="primary_contact_person_name"/>
    /
    note: <xsl:value-of select="note"/>
    <br/>
    description: <xsl:value-of select="description"/>
    /
    web site: <xsl:value-of select="web_site"/>
    <br/>
    active:
    <xsl:choose>
      <xsl:when test="is_active = 1">
	yes
      </xsl:when>
      <xsl:otherwise>
	no
      </xsl:otherwise>
    </xsl:choose>
    /
    visible:
    <xsl:choose>
      <xsl:when test="visible = 1">
	yes
      </xsl:when>
      <xsl:otherwise>
	no
      </xsl:otherwise>
    </xsl:choose>
    /
    created: <xsl:value-of select="substring(creation_dt, 1, 19)"/>
  </xsl:template>

  <xsl:template match="phone_number_list">
    <xsl:apply-templates select="phone_number"/>
  </xsl:template>

  <xsl:template match="phone_number">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/telecom/edit_phone_number_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="phone_number"/>
    </a>
    /
    device: <xsl:value-of select="device_name"/>
    <xsl:if test="port != ''">
      /
      port: <xsl:value-of select="port"/>
    </xsl:if>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="proximity_card_list">
    <xsl:apply-templates select="proximity_card"/>
  </xsl:template>

  <xsl:template match="proximity_card">
    <a>
      <xsl:attribute name="class">bare_link</xsl:attribute>
      <xsl:attribute name="href">/1g/tenant/edit_proximity_card_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
      <xsl:value-of select="number"/>
    </a>
    /
    type: <xsl:value-of select="type"/>
    /
    status: <xsl:value-of select="status"/>
    /
    technology: <xsl:value-of select="technology"/>
    /
    note: <xsl:value-of select="note"/>
    <br/>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use apply_xslt_output_xhtml;
use select_row;
use get_row_by_id_map;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  # check if floor id is specified and try to load floor.
  my ($floor_id, $floor) = ($cgi->param("floor_id"), undef);
  if ((!defined($floor_id)) || ($floor_id !~ /^\d+$/) || (!($floor = select_row($dbh, "floor_tbl", $floor_id)))) {
    die "show_page: show_proximity_cards_floor_not_found.html\n";
  }
  # prepare for queries.
  my ($sql_query, @sql_param_list);
  # fetch proximity cards.
  my (%proximity_card_by_id, @proximity_card_id_list);
  $sql_query = "select * from proximity_card_tbl where floor_id = ? order by number";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%proximity_card_by_id, \@proximity_card_id_list);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw where is_active and id in (select account_id from proximity_card_tbl where floor_id = ?)";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%account_by_id);
  # fetch persons.
  my %person_by_id;
  $sql_query = "select * from person_vw where is_active and id in (select person_id from proximity_card_tbl where floor_id = ?)";
  @sql_param_list = ([$floor_id, SQL_INTEGER]);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%person_by_id);
  # fetch access levels.
  my %access_level_by_id;
  get_row_by_id_map($dbh, "select * from access_level_tbl", undef, \%access_level_by_id);
  # populate information needed for display of page.
  my $floor_label = HTML::Entities::encode($floor->{"short_global_label"});
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <floor-label>$floor_label</floor-label>
  <proximity-card-list>
---END_BLOCK---;
  for my $proximity_card_id (@proximity_card_id_list) {
    my $proximity_card = $proximity_card_by_id{$proximity_card_id};
    my $account_id = $proximity_card->{"account_id"};
    my $account_name = "";
    if (defined($account_id)) {
      my $account = $account_by_id{$account_id};
      if ($account) {
	$account_name = $account->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $person_id = $proximity_card->{"person_id"};
    my $person_name = "";
    if (defined($person_id)) {
      my $person = $person_by_id{$person_id};
      if ($person) {
	$person_name = $person->{"last_name"} . ", " . $person->{"first_name"};
	if ($person->{"middle_name"}) {
	  $person_name .= " " . $person->{"middle_name"};
	}
      } else {
	$person_name = "(inactive person)";
      }
      $person_name = HTML::Entities::encode($person_name);
    }
    my $number = HTML::Entities::encode($proximity_card->{"number"});
    my $status = HTML::Entities::encode($proximity_card->{"status"});
    my $access_level_id = $proximity_card->{"access_level_id"};
    my $access_level_name = "";
    if (defined($access_level_id)) {
      my $access_level = $access_level_by_id{$access_level_id};
      $access_level_name = HTML::Entities::encode($access_level->{"name"});
    }
    $generated_xml .= <<"---END_BLOCK---;";
    <proximity-card>
      <id>$proximity_card_id</id>
      <account-name>$account_name</account-name>
      <person-name>$person_name</person-name>
      <number>$number</number>
      <status>$status</status>
      <access-level-name>$access_level_name</access-level-name>
    </proximity-card>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </proximity-card-list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_proximity_cards.xslt", $cgi);
}

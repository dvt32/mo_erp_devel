<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="page">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  Micro Office Solutions :: Operations Center :: Access Cards
	</title>
	<link rel="Stylesheet" type="text/css" href="/1g/css/operations.css"/>
      </head>
      <body>
	<xsl:apply-templates select="proximity-card-list"/>
	<p>
	  Return to the <a href="/1g/index.cgi">main menu</a>.
	</p>
	<p>
	  <img src="/1g/img/micro_office_logo_256x68.png" alt="Micro Office logo"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="proximity-card-list">
    <table cellspacing="0" cellpadding="0" border="0" class="standard_table">
      <tr>
	<td colspan="11" class="standard_table_title">
          Access Cards at
	  <xsl:value-of select="../floor-label"/>
	</td>
      </tr>
      <tr class="standard_table_column_heading_row">
	<td class="standard_table_side_padding"></td>
	<td>Number</td>
	<td class="standard_table_spacer_column"></td>
	<td>Status</td>
	<td class="standard_table_spacer_column"></td>
	<td>Access Level</td>
	<td class="standard_table_spacer_column"></td>
	<td>Account</td>
	<td class="standard_table_spacer_column"></td>
	<td>Person</td>
	<td class="standard_table_side_padding"></td>
      </tr>
      <xsl:apply-templates select="proximity-card"/>
    </table>
  </xsl:template>

  <xsl:template match="proximity-card">
    <tr>
      <xsl:choose>
	<xsl:when test="position() mod 2 = 0">
	  <xsl:attribute name="class">standard_table_row_even</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="class">standard_table_row_odd</xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      <td class="standard_table_side_padding"></td>
      <td>
	<a>
	  <xsl:attribute name="href">/1g/tenant/edit_proximity_card_input.cgi?id=<xsl:value-of select="id"/></xsl:attribute>
	  <xsl:attribute name="class">bare_link</xsl:attribute>
	  <xsl:value-of select="number"/>
	</a>
      </td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="status"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="access-level-name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="account-name"/></td>
      <td class="standard_table_spacer_column"></td>
      <td><xsl:value-of select="person-name"/></td>
      <td class="standard_table_side_padding"></td>
    </tr>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use HTML::Entities ();
use lib "{{ operations_commondir }}";
use lib "{{ perl_commondir }}";
use main_exception_wrapper;
use init_login_standard;
use get_row_by_id_map;
use apply_xslt_output_xhtml;

main_exception_wrapper(\&main);

sub main($) {
  my $instance_map = shift;
  my ($cgi, $dbh, $employee) = init_login_standard($instance_map);
  my $sql_query = "select * from guest_person_tbl where end_date >= current_date order by last_name, first_name";
  my (%guest_person_by_id, @guest_person_id_list);
  get_row_by_id_map($dbh, $sql_query, undef, \%guest_person_by_id, \@guest_person_id_list);
  # fetch accounts.
  my %account_by_id;
  $sql_query = "select * from account_vw";
  get_row_by_id_map($dbh, $sql_query, undef, \%account_by_id);
  # generate XML.
  my $generated_xml = "";
  $generated_xml .= <<"---END_BLOCK---;";
<?xml version="1.0"?>
<!DOCTYPE page PUBLIC "-//MOM//DTD XML Data 1.0//EN" "http://www.microoffice.com/xmldata1/DTD/xmldata1.dtd">
<page>
  <guest_person_list>
---END_BLOCK---;
  for my $guest_person_id (@guest_person_id_list) {
    my $guest_person = $guest_person_by_id{$guest_person_id};
    my $guest_person_name = HTML::Entities::encode($guest_person->{"last_name"} . ", " . $guest_person->{"first_name"});
    my $account_id = $guest_person->{"account_id"};
    my $account_name = "";
    if (defined($account_id)) {
      my $account = $account_by_id{$account_id};
      if ($account) {
	$account_name = $account->{"name"};
      } else {
	$account_name = "(inactive account)";
      }
      $account_name = HTML::Entities::encode($account_name);
    }
    my $host_contact = HTML::Entities::encode($guest_person->{"host_contact"} || "");
    my $start_date = $guest_person->{"start_date"};
    my $end_date = $guest_person->{"end_date"};
    $generated_xml .= <<"---END_BLOCK---;";
    <guest_person>
      <id>$guest_person_id</id>
      <guest_person_name>$guest_person_name</guest_person_name>
      <account_name>$account_name</account_name>
      <account_id>$account_id</account_id>
      <host_contact>$host_contact</host_contact>
      <start_date>$start_date</start_date>
      <end_date>$end_date</end_date>
    </guest_person>
---END_BLOCK---;
  }
  $generated_xml .= <<"---END_BLOCK---;";
  </guest_person_list>
</page>
---END_BLOCK---;
  # apply XSLT stylesheet and output XHTML page.
  apply_xslt_output_xhtml($generated_xml, "show_upcoming_guests.xslt", $cgi);
}

include $(repo_root)/common/apache.mk
include $(repo_root)/common/func.mk
include $(repo_root)/common/postgresql.mk
include $(repo_root)/common/python.mk

.PHONY: websetup
.PHONY: install

# libcurl4-openssl-dev, libssl-dev, python-dev needed by pycurl
deb_packages := libcurl4-openssl-dev libssl-dev python-dev
target_base := $(operations_basedir)/$(operations_flask)
# jinja2 needed despite jinja2_pkg dependency because we need jinja2 in pip
pip_packages := Flask psycopg2 jinja2 Flask-WTF WTForms-Components pycurl python-dateutil pytz requests

websetup: apache_wsgi_pkgs python_dev_pkgs jinja2_pkg postgresql_repo postgresql_client_dev_pkgs
# set up Python virtualenv with packages from PyPI
	@mkdir -p $(target_base)
	@$(call install_packages,$(deb_packages))
	@echo ..create Python virtualenv for operations_flask and install Python packages with pip
	@$(call install_virtualenv,$(target_base)/venv,$(pip_packages))

install: jinja2_pkg
	$(call check_system_properties,static_base_url essensys_client_id essensys_client_secret essensys_user essensys_password pmt_nmi_txn_num_days_in_past pmt_nmi_txn_num_days_in_past_failed outbound_auth_mail_server outbound_mail_user outbound_mail_pw)
	@echo ..create deployment directories for operations_flask
	@for i in bin lib share templates; do mkdir -p $(target_base)/$$i; done
	$(eval _file_list:=)
	@$(call copy_enqueue,$(CURDIR)/flask/application.wsgi,$(target_base)/application.wsgi,www-data.www-data,0755)
	@$(call copy_enqueue,$(CURDIR)/flask/route.py,$(target_base)/route.py,www-data.www-data,0755)
	@$(foreach file,$(wildcard $(CURDIR)/bin/*.py),$(call copy_enqueue,$(file),$(target_base)/bin/$(notdir $(file)),www-data.www-data,0755);)
	@$(foreach file,$(wildcard $(CURDIR)/lib/*.py),$(call copy_enqueue,$(file),$(target_base)/lib/$(notdir $(file)),www-data.www-data,0755);)
	@$(foreach file,$(wildcard $(repo_root)/operations_site/1g/perl/auth/*.html),$(call copy_enqueue,$(file),$(target_base)/templates/$(notdir $(file)),www-data.www-data,0755);)
# use copy_file (not copy_enqueue) for email templates because Jinja invoked at run-time
	@$(foreach file,$(wildcard $(repo_root)/payment/nmi/share/*),$(call copy_file,$(file),$(target_base)/share/$(notdir $(file)),www-data.www-data,0755);)
	@$(foreach file,$(wildcard $(repo_root)/payment/nmi/lib/*.py),$(call copy_enqueue,$(file),$(target_base)/lib/$(notdir $(file)),www-data.www-data,0755);)
	@$(foreach file,$(wildcard $(CURDIR)/templates/*.html),$(call copy_file,$(file),$(target_base)/templates/$(notdir $(file)),www-data.www-data,0755);)
	@$(call copy_file,$(repo_root)/static_site/forms/html/_formhelpers.html,$(target_base)/templates/_formhelpers.html,www-data.www-data,0755)
	@echo ..substitute properties in queued files: $(_file_list)
	@$(call subst_batch,$(system_properties))

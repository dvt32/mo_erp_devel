import flask
import re
from flask import g
from flask import request, render_template, url_for, redirect
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField, RadioField, HiddenField, SelectField
from wtforms import validators, ValidationError
import psycopg2.extras
import database
import dateutil.parser

class table_form(FlaskForm):
  table_name = SelectField('Table Name')
  columns    = StringField('Columns')
  filter_    = StringField('Filter')
  filter_op  = SelectField('Filter Operators', choices=[('=','='), ('<','<'), ('>','>'), ('<=', '<='), ('>=', '>='), ('!=', '!=')])
  filter_val = StringField('Filter Operand')
  sort_by    = StringField('Sort By' )
  asc_desc   = RadioField('ASC_DESC', choices=[('asc', 'ascending'), ('desc', 'descending')], default='asc')
  limit      = IntegerField('Limit', [validators.optional()])
  rows_per_page = IntegerField('Rows Per Page', default='10')
  submit     = SubmitField('Submit')

  def validate_table_name(form, field):
    # Validate the table_name input
    # There should be only 1 matching row
    field.data = field.data.lower()
    if not unique_table_exists(field.data):
      raise ValidationError('No table with name ' + field.data + ' found.')

  def validate_columns(form,field):
    if field.data:
      tbl_name = form.table_name.data
      if not unique_table_exists(tbl_name):
        raise ValidationError('Unable to validate columns, invalid table name')
      else:
        input_names = field.data.split(',')
        for name in input_names:
          name = name.strip()
          if not column_exists(tbl_name, name):
            raise ValidationError('Table ' + tbl_name + ' does not contain column ' + name + '. Valid choices are: ' + ", ".join(get_col_names(tbl_name)))

  def validate_filter_(form,field):
    if field.data:
      field.data = field.data.lower()
      tbl_name = form.table_name.data
      if not unique_table_exists(tbl_name):
        raise ValidationError('Unable to validate filter, invalid table name')
      else:
        col = field.data
        if not column_exists(tbl_name, col):
          raise ValidationError('Column ' + col + ' does not exist.')
        if not form.filter_val.data:
          raise ValidationError('The second field is required if limit has an input')

  def validate_filter_op(form, field):
    if field.data:
      if field.data not in ('=', '>', '<', '<=', '>=', '!='):
        raise ValidationError('Invalid choice for filter operator')

  def validate_filter_val(form, field):
    if field.data:
      if (';' or '--') in field.data:
        raise ValidationError('Invalid input! Does not accept ; or -- as part of string.')
      else:
        tbl_name = form.table_name.data
        if not form.filter_.data:
          raise ValidationError('Unable to validate filter because first field is empty!')

        if not unique_table_exists(tbl_name):
          raise ValidationError('Unable to validate filter because table name is invalid')

        if not column_exists(tbl_name, form.filter_.data):
          raise ValidationError('Unable to validate filter because filter column does not exist')

        type_name = get_column_type(tbl_name, form.filter_.data)

        if (type_name is 'integer' or type_name is 'numeric'):
          try:
            # int(field.data) does not include decimal value case
            float(field.data)
          except ValueError:
            raise ValidationError('%s requires an integer input' % form.filter_.data)
        if (type_name == 'character varying'):
          # can't really validate a string
          pass
        if (type_name == 'boolean'):
          if field.data not in ('t', 'f', 'true', 'false', '1', '0'):
            raise ValidationError('%s requires a boolean input' % (form.filter_.data))
        if ('timestamp' in type_name):
          try:
            dateutil.parser.parse(field.data)
          except ValueError:
            raise ValidationError('%s requires a valid date-time input' % form.filter_.data)
        if (type_name is 'polygon'):
          # needs some regex here, should be of form ((int,int),(int,int),(int,int),(int,int))
          coordinates = field.data.replace(" ","")[2:-2].split("),(") # get int,int pairs
          if len(coordinates) is not 4:
            raise ValidationError('%s requires a valid polygon coordinate input' % form.filter_.data)
          for each in coordinates: # pairs
            for xy in each: # each numeric value
              try:
                float(xy)
              except ValueError:
                raise ValidationError('All coordinates must be numbers, %s is not a valid value' % xy)

  def validate_sort_by(form,field):
    field.data = field.data.strip()
    if field.data: #check if not empty
      if form.columns.data:
        # You can only sort by columns you've chosen
        column_names = form.columns.data.split(',')
        col_names = []
        for item in column_names:
          col_names.append(item.strip())
        if field.data not in col_names:
          raise ValidationError(field.data + ' was not found in your chosen columns ')
      else:
        tbl_name = form.table_name.data
        # check if table_name is valid before checking if column exists
        if not unique_table_exists(tbl_name):
          raise ValidationError('Unable to validate sort_by, invalid table name')
        else:
          if not column_exists(tbl_name, field.data):
            # Table does not have this column
            raise ValidationError('Table ' + tbl_name + ' does not contain column ' + field.data + '. Valid choices are ' + ', '.join(get_col_names(tbl_name)))

  def validate_limit(form, field):
    # Reject negative limit if input (field optional)
    if (field.data is not None):
      if (field.data < 0):
        raise ValidationError('Limit cannot be negative!')

# Function checks if there exists exactly 1 table with parameter name
def unique_table_exists(name):
  name = name.lower()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SELECT * FROM information_schema.table_privileges WHERE table_name = %s AND grantee=(SELECT CURRENT_USER) AND privilege_type='SELECT'", (name,))
  return curdict.rowcount == 1

# Returns a list of columns with input table name (assumes table exists)
def get_col_names(tableName):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  query = 'SELECT * FROM ' + tableName + ' LIMIT 0'
  curdict.execute(query)
  col_names = [desc[0].lower() for desc in curdict.description]
  return col_names

# checks if a column exists within a table (assumes table exists)
def column_exists(tableName, colName):
  colName = colName.lower()
  col_names = get_col_names(tableName)
  return colName in col_names

def get_column_type(tableName, colName):
  # 1 - String, 2 - Binary, 3 - Number, 4 - Datetime, 5 - RowID
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("""select *
               from information_schema.columns
               where table_schema NOT IN ('information_schema', 'pg_catalog')
               and column_name = %s
               and table_name = %s
               order by table_schema, table_name""", (colName, tableName,))
  row = curdict.fetchone()
  return row['data_type']

def get_form():
  form = table_form()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SELECT table_name FROM information_schema.table_privileges WHERE grantee=(SELECT CURRENT_USER) AND privilege_type='SELECT' ORDER BY table_name;")
  form.table_name.choices = [(x['table_name'], x['table_name']) for x in curdict]
  return form

def show_form():
  # Display the form
  return render_template('displayTableInput.html', form=get_form())

def store_form():
  form = get_form()
  if not form.validate_on_submit():
    errors = []
    # form.errors is a dict containing each field and a list of associated errors
    for field, error_list in form.errors.items():
      errors.append(field + ':')
      for error in error_list:
        errors[-1] += (' ' + error + ',')
      errors[-1] = errors[-1][:-1] #remove the last comma
      errors[-1] = errors[-1].strip()
    return render_template('displayTableInput.html', form=get_form(), errors=errors)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  filter_string = None
  if form.filter_.data and form.filter_val.data: # Will only make a filter string if both are not empty
    filter_string = form.filter_.data + form.filter_op.data + "'" +  form.filter_val.data + "'"

  # Store the form in the database, piece by piece
  curdict.execute('INSERT INTO disp_tbl (tbl_name, columns, filter, order_by, asc_desc, lim, rows_per_page, page) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)', (form.table_name.data, form.columns.data, (filter_string), form.sort_by.data, form.asc_desc.data, form.limit.data, form.rows_per_page.data, 1,))

  # Get the id of the most recent row (the one we just insert)
  curdict.execute('SELECT id FROM disp_tbl ORDER BY id DESC')
  request_id = curdict.fetchone()['id']

  # Check if user has permission to view table
  try:
    curdict.execute(generate_query(request_id))
  except psycopg2.Error as e:
    return render_template('displayTableInput.html', form=get_form(), errors=["Permission to view table denied."])

  # Commit upon success
  conn.commit()
  return request_id

def generate_query(request_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * from disp_tbl WHERE id = %s', (request_id,))
  fields = curdict.fetchone()
  if fields['columns']:
    query = 'SELECT ' + fields['columns']
  else:
    query = 'SELECT *'

  query += ' FROM ' + fields['tbl_name']

  if fields['filter']:
    query += ' WHERE ' + fields['filter']

  if fields['order_by']:
    query += ' ORDER BY ' +  fields['order_by'] + ' ' + fields['asc_desc']

  if fields['lim']:
    query += ' LIMIT ' + str(fields['lim'])

  return query

def show_table(request_id=None):
  if not request_id:
    request_id = store_form()
    
  # store_form returns an integer upon success
  # returns render_template(form) on failure
  # If it's not an integer, show the form.
  if not isinstance(request_id,int):
    return request_id

  if id_exists(request_id):
    conn2 = database.get_connection()
    disp_tbl_curdict = conn2.cursor(cursor_factory=psycopg2.extras.DictCursor)
    disp_tbl_curdict.execute('SELECT * FROM disp_tbl WHERE id=%s',(request_id,))
    disp_tbl = disp_tbl_curdict.fetchone()
    conn1 = database.get_connection()
    query_curdict = conn1.cursor(cursor_factory=psycopg2.extras.DictCursor)
    query_curdict.execute(generate_query(request_id))
    return render_template('displayTable.html', disp_tbl=disp_tbl, query=query_curdict)
  else:
    return 'Invalid request_id'

def id_exists(request_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT 1 FROM disp_tbl WHERE id=%s', (request_id,))
  return curdict.rowcount==1

def get_page(request_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT page FROM disp_tbl WHERE id = %s', (request_id,))

  return curdict.fetchone()['page']

def get_table_name(request_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT tbl_name FROM disp_tbl WHERE id = %s', (request_id,))

  return curdict.fetchone()['tbl_name']

def sort_table(request_id, column):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT order_by, asc_desc FROM disp_tbl WHERE id =%s', (request_id,))
  row = curdict.fetchone()

  # handles 3 cases
  #     1&2. order_by is same, swap asc to desc or vice versa
  #     3.   sort by new value, default to asc
  if (row['order_by'] == column):
    # Note that 'asc_desc' is varchar(4) so it's either "asc " or "desc" never "asc"
    # unless we use .strip()
    if row['asc_desc'] == 'asc ':
      curdict.execute("UPDATE disp_tbl SET asc_desc = 'desc' WHERE id=%s", (request_id,))
    else:
      curdict.execute("UPDATE disp_tbl SET asc_desc = 'asc ' WHERE id=%s", (request_id,))
  else:
    # sort by different column, set asc/desc to asc and set order_by to specified column
    curdict.execute("UPDATE disp_tbl SET order_by = %s, asc_desc='asc ' WHERE id=%s", (column,request_id,))

  # Change page to 1 since user updated the query
  curdict.execute('UPDATE disp_tbl SET page = 1 WHERE id=%s', (request_id,))
  conn.commit()
  return show_table(request_id)

def next_page(request_id, num_pages):
  #skips forward num_pages pages (can be negative)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # Get total rows of query
  curdict.execute(generate_query(request_id))
  rowcount = curdict.rowcount

  # Get user designated rows_per_page
  curdict.execute('SELECT rows_per_page FROM disp_tbl WHERE id=%s', (request_id,))
  rows_per_page = curdict.fetchone()['rows_per_page']

  # Calculate value of last page
  last_page = int(rowcount/rows_per_page) + 1

  # Get the user designated page
  curdict.execute('SELECT page FROM disp_tbl WHERE id=%s', (request_id,))
  page = curdict.fetchone()['page']
  if page+num_pages > 0 and page+num_pages <= last_page:
    curdict.execute('UPDATE disp_tbl SET page=%s WHERE id=%s', (page+num_pages,request_id))
    conn.commit()
  return show_table(request_id)

def first_page(request_id):
  # set page = 1 and regenerate table
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('UPDATE disp_tbl SET page=1 WHERE id=%s',(request_id,))
  conn.commit()
  return show_table(request_id)

def last_page(request_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute(generate_query(request_id))
  rowcount = curdict.rowcount
  curdict.execute('SELECT rows_per_page FROM disp_tbl WHERE id=%s',(request_id,))
  rows_per_page = curdict.fetchone()['rows_per_page']
  last_page = int(rowcount / rows_per_page) + 1 #casting to int drops the decimals, adding one 'rounds up'
  curdict.execute('UPDATE disp_tbl set page=%s WHERE id=%s', (last_page, request_id,))
  conn.commit()
  return show_table(request_id)

def skip_to_page(request_id, page_num):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('UPDATE disp_tbl SET page=%s WHERE id=%s', (page_num, request_id,))
  conn.commit()
  return show_table(request_id)

def set_rows_per_page(request_id, rpp):
  if rpp > 0:
    # set rows_per_page and regenerate table (with page=1)
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('UPDATE disp_tbl SET page = 1, rows_per_page=%s WHERE id=%s', (rpp,request_id,))
    conn.commit()
  return show_table(request_id)

def set_limit(request_id, limit):
  if limit > 0:
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('UPDATE disp_tbl SET lim=%s WHERE id=%s',(limit, request_id,))
    conn.commit()

  return show_table(request_id)

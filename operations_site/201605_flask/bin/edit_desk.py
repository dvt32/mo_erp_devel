import flask
from flask import render_template, abort, request, redirect, url_for, jsonify
from flask_wtf import FlaskForm, csrf
from ajax_helper import validate_csrf_token
from wtforms import StringField, IntegerField, SubmitField, DateField, SelectField, BooleanField, TextAreaField
from wtforms.fields.html5 import DecimalField
from wtforms import validators, ValidationError
import psycopg2.extras
import database
import re
from datetime import datetime
from collections import OrderedDict
from decimal import Decimal
import auth

# We don't want pre-validation because waiting_account.choices is filled with accounts associated with the desk only
# The remaining accounts are fetched via query_accounts (AJAX)
class CustomSelectField(SelectField):
  def pre_validate(self, form):
    """pre_validation is disabled"""

# This class represents a single desk from table desk_tbl
class desk_tbl_form(FlaskForm):
  person  = SelectField('Person')
  num_people = IntegerField('Number of People')
  is_enclosed = SelectField('Enclosed?', choices=[('True', 'True'), ('False','False')])
  list_price = DecimalField('List Price ($)', places=2)
  waiting_account = CustomSelectField('Waiting Tenant')
  internet_list_price = DecimalField('Internet List Price ($)', places=2)
  phone_list_price = DecimalField('Phone List Price ($)', places=2)
  is_reviewed_by_manager = SelectField('Reviewed by Manager?', choices=[('y','YES'), ('n','NO')])
  note = TextAreaField('Note')
  submit = SubmitField('Save Changes')

  def validate_waiting_account(form, field):
    if field.data == '0':
      return True
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT 1 FROM account_vw WHERE id = %s', (field.data,))
    if curdict.rowcount!=1:
      raise ValidationError('Error validating waiting account!')

class interaction_form(FlaskForm):
  note = TextAreaField('Note')
  submit = SubmitField('Submit Interaction')

# Returns true if argument is in proper money format
def validate_money(data):
  amount = str(data)
  match = re.match(r'^\d+(\.\d{2})?', amount)
  return match != None

def trim(s):
  # Trims a string that's too long
  return (s[:32] + '...') if len(s) > 35 else s

def is_authorized():
  return auth.authorize_role('role:space/edit_desk')

def show_page(desk_id):
  '''Shows view_desk if not authorized or view is set to true; Show the full edit desk page otherwise'''
  if request.args.get('view')=='true' or not is_authorized():
    return view_desk(desk_id)
  return show_form(desk_id)

def view_desk(desk_id):
  ''' Shows a desk page without allowing edits; Allows user to submit interactions'''
  form = interaction_form()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('''SELECT * FROM desk_vw WHERE id = %s''', (desk_id,))
  desk = curdict.fetchone()
  curdict.execute('''SELECT building_id FROM floor_tbl WHERE id = %s''', (desk['floor_id'],))
  building_id = curdict.fetchone()['building_id']
  curdict.execute('''SELECT desk_price_vw.*, cust_acct_tbl.name AS account_name FROM desk_price_vw LEFT JOIN cust_acct_tbl ON desk_price_vw.account_id = cust_acct_tbl.id WHERE desk_id = %s''', (desk_id,))
  desk_price_vw = curdict.fetchone()
  curdict.execute('''SELECT price, internet_revenue, internet_lines, 1 AS internet_discount, phone_revenue, phone_lines, 1 AS phone_discount, portal_lines, CASE WHEN is_monthly THEN 'Monthly' ELSE 'Lease' END AS type, start_date, end_date, name || ' (ID #' || account_vw.id || ')' as account, note FROM desk_price_tbl LEFT JOIN account_vw ON desk_price_tbl.account_id = account_vw.id WHERE desk_id = %s AND desk_price_tbl.is_active ORDER BY start_date''', (desk_id,))
  desk_prices = curdict.fetchall()
  for row in desk_prices:
    if row['internet_lines']:
      row['internet_discount'] = get_internet_discount(building_id, row['internet_revenue'] / row['internet_lines'], curdict)
    if row['phone_lines']:
      row['phone_discount'] = get_phone_discount(building_id, row['phone_revenue'] / row['phone_lines'], curdict)

  curdict.execute('''SELECT desk_area FROM desk_area_vw WHERE id = %s''', (desk_id,))
  desk_area = curdict.fetchone()['desk_area']
  curdict.execute('''SELECT first_name || ' ' || last_name AS name, date, interactions.note FROM desk_sale_interaction_tbl interactions LEFT JOIN employee_tbl ON interactions.employee_id = employee_tbl.id WHERE desk_id = %s ORDER BY date DESC''', (desk_id,))
  interactions = curdict.fetchall()
  property_dict = {'desk_prices': desk_prices, 'desk_id': desk_id, 'desk_area_sf':desk_area, 'desk':desk, 'desk_price_vw': desk_price_vw, 'form':form, 'interactions':interactions, 'floor_id':desk['floor_id'] }
  curdict.execute('''SELECT first_name, last_name FROM cust_pers_tbl WHERE id = %s''', (desk['cust_pers_id'],))
  if curdict.rowcount == 1:
    property_dict['person'] = curdict.fetchone()
  if desk['account_id']:
    curdict.execute('SELECT id, assigned_number FROM desk_vw WHERE account_id = %s and id != %s and is_active and floor_id = %s ORDER BY assigned_number', (desk['account_id'], desk['id'], desk['floor_id'],))
    property_dict['linked_desk_list'] = [{'id':x['id'], 'assigned_number':x['assigned_number']} for x in curdict]

  property_dict['internet_list_price'] = get_list_price(building_id, 'internet', curdict)
  property_dict['phone_list_price'] = get_list_price(building_id, 'phone', curdict)
  return render_template('view_desk.html', **property_dict)

def submit_interaction(desk_id):
  form = interaction_form()
  if not form.validate_on_submit():
    print(form.errors)
    redirect(url_for('show_edit_desk', desk_id=desk_id))

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  employee_id = auth.get_employee()['id']
  curdict.execute('''INSERT INTO desk_sale_interaction_tbl (desk_id, employee_id, date, note) VALUES (%s, %s, %s, %s)''', (desk_id, employee_id, datetime.now(), form.note.data,))
  conn.commit()
  return redirect(url_for('show_edit_desk', desk_id=desk_id))

def get_form(desk_id):
  ''' Returns flaskform object with populated options.
  This does set any data attributes for any of the fields '''
  form = desk_tbl_form()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # make sure row exists
  curdict.execute('SELECT floor_id FROM desk_tbl WHERE id=%s and is_active', (desk_id,))
  if curdict.rowcount!=1:
    abort(400,'Error! Could not desk with id %s. It may not exist or be inactive.' % desk_id)

  floor_id = curdict.fetchone()['floor_id']

  # populate account and waiting account select fields with existing values
  # Accounts are fetched via Ajax call to query_accounts(terms)
  curdict.execute('SELECT id, name, is_active FROM account_vw WHERE id IN (SELECT account_id FROM desk_price_tbl WHERE desk_id = %s) OR id IN (SELECT waiting_account FROM desk_tbl WHERE id = %s)', (desk_id, desk_id,))

  # Populate waiting_account field (which is also used as a template for desk_price rows in edit_desk.html)
  form.waiting_account.choices = []
  for row in curdict:
    if row['is_active']:
      form.waiting_account.choices.append((str(row['id']), trim(row['name']) + ' (ID #' + str(row['id']) + ')'))
    else:
      form.waiting_account.choices.append((str(row['id']), trim(row['name']) + ' (ID #' + str(row['id']) + ') (inactive)'))
  form.waiting_account.choices.insert(0,('0', '---[No Account]---'))

  # Populate person select field (includes current desk_tbl.cust_pers_id so that values are not overriden when desk_vw.account_id is null)
  form.person.choices = [('', '---[Not Specified]---')]
  curdict.execute('''SELECT id, cust_acct_id, first_name, last_name FROM cust_pers_tbl WHERE cust_acct_id IN (SELECT account_id FROM desk_vw WHERE id = %s) OR id IN (SELECT cust_pers_id FROM desk_tbl WHERE id = %s)''', (desk_id,desk_id,))
  form.person.choices.extend([(str(row['id']), row['first_name'] + ' ' + row['last_name']) for row in curdict])
  return form

# Generate form of associated desk
def show_form(desk_id):
  '''Shows the edit desk management page. This is reserved for full access users'''
  form = get_form(desk_id)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # get building and floor labels (of associated desk)
  curdict.execute('SELECT floor_id FROM desk_tbl WHERE id=%s', (desk_id,))
  floor_id = curdict.fetchone()['floor_id']

  curdict.execute('SELECT building_id, building_label, label FROM floor_vw WHERE id=%s', (floor_id,))
  row = curdict.fetchone()

  # set building and floor labels
  building_id    = row['building_id']
  building_label = row['building_label']
  floor_label    = row['label']

  # make sure row exists
  curdict.execute('SELECT assigned_number, cust_pers_id, num_people, is_enclosed, list_price, waiting_account, reviewed_by_manager, note FROM desk_tbl WHERE id=%s', (desk_id,))
  row = curdict.fetchone()

  assigned_number                  = row['assigned_number']
  form.person.data                 = str(row['cust_pers_id'])
  form.num_people.data             = row['num_people']
  form.is_enclosed.data            = 'True' if row['is_enclosed'] else 'False'
  form.list_price.data             = row['list_price']
  form.internet_list_price.data    = get_list_price(building_id, 'internet', curdict)
  form.phone_list_price.data       = get_list_price(building_id, 'phone', curdict)
  form.waiting_account.data        = str(row['waiting_account'])
  form.is_reviewed_by_manager.data = 'y' if row['reviewed_by_manager'] else 'n'
  form.note.data                   = row['note']

  property_dict = {}
  # internet and phone discount are added so we can modify them
  curdict.execute('SELECT *, 0 AS internet_discount, 0 AS phone_discount FROM desk_price_tbl WHERE desk_id = %s AND is_active ORDER BY start_date, end_date', (desk_id,))
  desk_prices = curdict.fetchall()
  for row in desk_prices:
    if row['internet_lines']:
      row['internet_discount'] = get_internet_discount(building_id, row['internet_revenue'] / row['internet_lines'], curdict)
    if row['phone_lines']:
      row['phone_discount'] = get_phone_discount(building_id, row['phone_revenue'] / row['phone_lines'], curdict)

  curdict.execute('SELECT desk_area FROM desk_area_vw WHERE id=%s', (desk_id,))
  desk_area = curdict.fetchone()['desk_area'] if curdict.rowcount==1 else -1
  curdict.execute('SELECT * FROM desk_sale_interaction_tbl WHERE desk_id=%s ORDER BY date DESC', (desk_id,))
  desk_sale_interactions = curdict.fetchall()
  # Populate employee list with all active employees and inactive employees associated with prior interactions
  curdict.execute('SELECT id, first_name, last_name FROM employee_tbl WHERE (is_active and in_directory) OR id IN (SELECT employee_id FROM desk_sale_interaction_tbl WHERE desk_id = %s)', (desk_id,))
  employee_list = curdict.fetchall()

  property_dict.update({'floor_id':floor_id, 'desk_assigned_number': assigned_number, 'desk_area_sf':desk_area, 'desk_prices':desk_prices, 'desk_id':desk_id, 'static_base_url':'{{ static_base_url }}', 'desk_sale_interactions':desk_sale_interactions, 'employee_list':employee_list, 'employee':auth.get_employee() })

  curdict.execute('SELECT desk_price_vw.*, cust_acct_tbl.name AS account_name FROM desk_price_vw LEFT JOIN cust_acct_tbl ON desk_price_vw.account_id = cust_acct_tbl.id WHERE desk_id = %s', (desk_id,))
  if curdict.rowcount == 1:
    property_dict['desk_price_vw'] = curdict.fetchone()
    account_id = property_dict['desk_price_vw']['account_id']
    if account_id:
      # Linked desks are any desks with the same account ID in the same floor.
      curdict.execute('SELECT id, assigned_number FROM desk_vw WHERE account_id = %s and id!= %s and is_active and floor_id = %s ORDER BY assigned_number', (account_id, desk_id, floor_id,))
      property_dict['linked_desk_list'] = [{'id':x['id'], 'assigned_number':x['assigned_number']} for x in curdict]


  return render_template('edit_desk.html', form=form, **property_dict)

def delete_desk_price():
  ''' Delete by ajax call '''
  if not is_authorized():
    return jsonify(fail='Not authorized')
  if not validate_csrf_token():
    return jsonify(fail='invalid token')
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  desk_id = request.args.get('desk_id')
  desk_price_id = request.args.get('desk_price_id')
  curdict.execute('UPDATE desk_price_tbl SET is_active = FALSE WHERE id = %s AND desk_id = %s', (desk_price_id, desk_id,))
  conn.commit()
  return jsonify(success=1, desk_price_id=desk_price_id)

def delete_desk_sale_interaction():
  ''' Delete by ajax call '''
  if not is_authorized():
    return jsonify(fail='Not authorized')
  if not validate_csrf_token():
    return jsonify(fail='invalid token')
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  desk_id = request.args.get('desk_id')
  desk_sale_interaction_id = request.args.get('desk_sale_interaction_id')
  curdict.execute('DELETE FROM desk_sale_interaction_tbl WHERE id=%s AND desk_id=%s', (desk_sale_interaction_id, desk_id,))
  conn.commit()
  return jsonify(success=1, desk_sale_interaction_id=desk_sale_interaction_id)

def query_accounts():
  '''Get accounts for the dropdown menu; Solution to having 2000+ rows per select field'''
  if not validate_csrf_token():
    return jsonify(fail='invalid token')
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  terms = request.args.get('term','').lower()
  curdict.execute('''SELECT id, name, is_active FROM account_vw WHERE id::text LIKE '%%' || %s || '%%' OR name ILIKE '%%' || %s || '%%' ORDER BY name''', (terms, terms,))
  # include no option as an option
  arr = [ { 'id': 0, 'text': '---[No Account]---' } ]
  for row in curdict:
    row_dict = {
      'id': row['id'],
      'text': '{} (ID: {})'.format(row['name'], row['id'])
    }
    if not row['is_active']:
      row_dict['text'] += ' (inactive)'
    arr.append(row_dict)
  return jsonify(success=1, items=arr)

def submit_desk_form(desk_id):
  # This function handles a form that corresponds to 3 tables: desk_tbl, desk_price_tbl, and desk_sale_interaction_tbl.
  # The desk_tbl form is a standard WTForm and is taken care of
  # The other two tables are generated 'on the fly' and so each field must be retrieved from request.form and validated individually
  if not is_authorized():
      return render_template('not_authorized.html')
  form = get_form(desk_id)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT building_id FROM floor_tbl WHERE id IN (SELECT floor_id FROM desk_tbl WHERE id = %s)', (desk_id,))
  building_id = curdict.fetchone()['building_id']

  # Javascript should handle all verificatiion and so an invalid form should never be submit
  if not form.validate_on_submit():
    for error in form.errors:
      print(error)
      print(form.errors[error])
    return 'Error validating form, please report this to the administrator'

  # Account for nothing selected (empty strings)
  if not form.person.data:
    form.person.data = None
  if form.waiting_account.data == '0':
    form.waiting_account.data = None


  sql_set_list = ['cust_pers_id=%s', 'num_people=%s', 'is_enclosed=%s', 'list_price=%s', 'waiting_account=%s', 'note=%s', 'reviewed_by_manager=%s']
  sql_param_list = [form.person.data, form.num_people.data, form.is_enclosed.data, form.list_price.data, form.waiting_account.data, form.note.data, form.is_reviewed_by_manager.data, desk_id]
  query = 'UPDATE desk_tbl SET ' + ', '.join(sql_set_list)  + ' WHERE id=%s'
  curdict.execute(query, tuple(sql_param_list))

  update_discount(building_id, 'internet', form.internet_list_price.data, curdict)
  update_discount(building_id, 'phone', form.phone_list_price.data, curdict)

  # list of new interactions (each is dictionary)
  new_interaction_dict_list = []
  new_price_dict_list = []

  # existing interactions dict: id: { dict containing values }
  existing_interaction_dict = {}
  existing_price_dict = {}

  # regex patterns used to extract ids for each type of field set
  interaction_pattern = r'interaction_note_[0-9]+$'
  new_interaction_pattern = r'new_interaction_note_[0-9]+$'
  price_pattern = r'price_rent_[0-9]+$'
  new_price_pattern = r'new_price_rent_[0-9]+$'

  # iterate through all the fields in the form and search for each price or interaction set of fields
  # e.g. interaction ID 445 would have 3 fields with names interaction_note_445, interaction_date_445, interaction_employee_445
  for field in request.form:
    existing_interaction_result = re.match(interaction_pattern, field)
    if existing_interaction_result:
      interaction_id = existing_interaction_result.group(0)[17:]
      field_dict = {'note': 'interaction_note_' + interaction_id,
                    'employee': 'interaction_employee_' + interaction_id,
                    'date': 'interaction_date_' + interaction_id }

      existing_interaction_dict[interaction_id] = generate_sales_interaction_dict(field_dict, request.form)
      continue

    new_interaction_result = re.match(new_interaction_pattern, field)

    if new_interaction_result:
      new_interaction_id = new_interaction_result.group(0)[21:]

      field_dict = {'note': 'new_interaction_note_' + new_interaction_id,
                    'employee': 'new_interaction_employee_' + new_interaction_id,
                    'date': 'new_interaction_date_' + new_interaction_id }

      new_interaction_dict_list.append(generate_sales_interaction_dict(field_dict, request.form))
      continue

    existing_price_result = re.match(price_pattern, field)
    if existing_price_result:
      price_id = existing_price_result.group(0)[11:]

      field_dict = {
        'desk_id': desk_id,
        'price':'price_rent_' + price_id,
        'start_date':'price_start_' + price_id,
        'internet_revenue': 'price_internet_' + price_id,
        'internet_lines': 'price_lines_internet_' + price_id,
        'phone_revenue': 'price_phone_' + price_id,
        'phone_lines': 'price_lines_phone_' + price_id,
        'portal_lines': 'price_lines_portal_' + price_id,
        'end_date' : 'price_end_' + price_id,
        'account_id': 'price_account_' + price_id,
        'is_monthly': 'price_monthly_' + price_id,
        'note': 'price_note_' + price_id }

      existing_price_dict[price_id] = generate_price_dict(field_dict, request.form)
      continue

    new_price_result = re.match(new_price_pattern, field)
    if new_price_result:
      new_price_id = new_price_result.group(0)[15:]
      field_dict = {
        'desk_id' :desk_id,
        'price':'new_price_rent_' + new_price_id,
        'start_date':'new_price_start_' + new_price_id,
        'internet_revenue':'new_price_internet_' + new_price_id,
        'internet_lines': 'new_price_lines_internet_' + new_price_id,
        'phone_revenue':'new_price_phone_' + new_price_id,
        'phone_lines': 'new_price_lines_phone_' + new_price_id,
        'portal_lines': 'new_price_lines_portal_' + new_price_id,
        'end_date': 'new_price_end_' + new_price_id,
        'account_id': 'new_price_account_' + new_price_id,
        'is_monthly': 'new_price_monthly_' + new_price_id,
        'note': 'new_price_note_' + new_price_id }

      new_price_dict_list.append(generate_price_dict(field_dict, request.form))

  # Update or Insert new rows as needed
  for interaction_id in existing_interaction_dict:
    curdict.execute('UPDATE desk_sale_interaction_tbl SET note=%s, employee_id=%s, date=%s WHERE id=%s', (existing_interaction_dict[interaction_id]['note'], existing_interaction_dict[interaction_id]['employee'], existing_interaction_dict[interaction_id]['date'], interaction_id,))

  for interaction in new_interaction_dict_list:
    curdict.execute('INSERT INTO desk_sale_interaction_tbl (desk_id, employee_id, date, note) VALUES (%s, %s, %s, %s)', (desk_id, interaction['employee'], interaction['date'], interaction['note'],))

  # Create a query with the fields and '%s' the appropriate number of times
  fields = ['desk_id', 'price', 'portal_lines', 'internet_revenue', 'internet_lines', 'phone_revenue', 'phone_lines', 'start_date', 'end_date', 'account_id', 'is_monthly', 'note']
  query = 'INSERT INTO desk_price_tbl ({}) VALUES ({})'.format(', '.join(fields), ','.join(['%s'] * len(fields)))

  for price_id in existing_price_dict:
    desk_price = existing_price_dict[price_id]
    # Check if the row has been modified
    if not is_desk_price_row_modified(price_id, desk_price, curdict):
      continue

    if desk_price['start_date'] >= desk_price['end_date']:
      return 'Start dates must always be prior to end dates'

    # We don't actually delete anything from desk_price_tbl, just flag it as inactive
    curdict.execute('UPDATE desk_price_tbl SET is_active = FALSE WHERE id = %s', (price_id,))
    values = tuple(desk_price[field] for field in fields)
    curdict.execute(query, values)

  for new_price in new_price_dict_list:
    if new_price['start_date'] >= new_price['end_date']:
      return 'Start dates must always be prior to end dates'
    values = tuple(new_price[field] for field in fields)
    curdict.execute(query, values)

  # Validate that there are no overlaps for ANY desk_price rows (asserts two accounts cannot have the same time range)
  curdict.execute('SELECT id, start_date, end_date FROM desk_price_tbl WHERE desk_id = %s AND is_active', (desk_id,))
  validation_cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  for desk_price in curdict:
    validation_cursor.execute('SELECT id, start_date, end_date FROM desk_price_tbl WHERE is_active AND desk_id = %s AND id != %s AND (start_date, end_date) OVERLAPS (%s, %s)', (desk_id, desk_price['id'], desk_price['start_date'], desk_price['end_date'],))
    if validation_cursor.rowcount != 0:
      overlap_row = validation_cursor.fetchone()
      return 'Overlap between desk_price_tbl row ID %s (start %s, end %s) and row ID %s (start %s, end %s)' % (desk_price['id'], desk_price['start_date'], desk_price['end_date'], overlap_row['id'], overlap_row['start_date'], overlap_row['end_date'])

  conn.commit()
  return redirect(url_for('show_edit_desk', desk_id=desk_id))

def generate_price_dict(field_dict, form):
  # generate a dict containing price, start date, end date for a specific input
  # field dict has 6 required keys: start, end, price, internet, phone, monthly
  # inputs are named in one of the following two conventions
  # price_##, price_start_###, price_end_### OR new_price_##, new_price_start_###, new_price_end_###

  price = request.form[field_dict['price']]
  internet = request.form[field_dict['internet_revenue']]
  phone = request.form[field_dict['phone_revenue']]

  internet_lines = request.form[field_dict['internet_lines']]
  phone_lines = request.form[field_dict['phone_lines']]
  portal_lines = request.form[field_dict['portal_lines']]

  try:
    "{:.2f}".format(float(price))
    "{:.2f}".format(float(internet))
    "{:.2f}".format(float(phone))
  except ValueError:
    abort(400, 'Invalid input for a price. It must be a number with exactly two decimal places')

  if float(price) >= (10 ** 6) or float(internet) >= (10 ** 6) or float(phone) >= (10 ** 6):
    abort(400, 'Encountered a price (rent, internet, or phone) that was too large. Please note that the database only supports values up to $1,000,000.00 per month.')

  try:
    int(phone_lines)
    int(internet_lines)
    int(portal_lines)
  except ValueError:
    abort(400, 'Invalid input for a # of lines input. It must be an integer')

  account = request.form[field_dict['account_id']]

  # Start is required
  try:
    start = datetime.strptime(request.form[field_dict['start_date']], '%Y-%m-%d')
    end = datetime.strptime(request.form[field_dict['end_date']], '%Y-%m-%d')
  except ValueError:
    abort(400, 'Unable to parse start %s or end %s' % (request.form[field_dict['start_date']], request.form[field_dict['end_date']]))

  if request.form[field_dict['is_monthly']] not in ['Monthly', 'License']:
    abort(400, 'Found an unexpected value for a contract type. Expected License or Monthly but got %s' % monthly)

  monthly = request.form[field_dict['is_monthly']] == 'Monthly'
  note = request.form[field_dict['note']]

  return  { 'desk_id': field_dict['desk_id'], 'price':price, 'internet_revenue':internet, 'internet_lines':internet_lines,
            'phone_revenue':phone, 'phone_lines':phone_lines, 'portal_lines':portal_lines, 'start_date':start, 'end_date':end,
            'account_id':account, 'is_monthly':monthly, 'note':note }

def generate_sales_interaction_dict(field_dict, form):
  # generate dictionary with format interaction_date_##, interaction_note_##, interaction_employee_##
  try:
    date = datetime.strptime(request.form[field_dict['date']], '%Y-%m-%d')
  except ValueError:
    abort(400, 'Unable to parse date %s' % request.form[field_dict['date']])
  note = request.form[field_dict['note']]
  empl_id = request.form[field_dict['employee']]

  return {'note':note, 'employee':empl_id, 'date':date}

def is_desk_price_row_modified(desk_price_row_id, submitted_info, curdict):
  # Checks to see if any of the columns have been modified
  # There's a bit of type-conflict going on so each item must be treated separately

  curdict.execute('''
  SELECT
    price,
    internet_revenue,
    internet_lines,
    phone_revenue,
    phone_lines,
    portal_lines,
    start_date,
    end_date,
    account_id,
    is_monthly,
    note
  FROM desk_price_tbl
  WHERE id = %s''', (desk_price_row_id,))

  db_price_row = curdict.fetchone()

  for key in ['price', 'internet_revenue', 'phone_revenue', 'internet_lines', 'phone_lines', 'portal_lines']:
    if db_price_row[key] != Decimal(submitted_info[key]):
      return True

  for key in ['start_date', 'end_date']:
    if db_price_row[key] != submitted_info[key].date():
      return True

  if db_price_row['account_id'] != int(submitted_info['account_id']):
    return True

  if db_price_row['is_monthly'] != submitted_info['is_monthly']:
    return True

  if db_price_row['note'] != submitted_info['note']:
    return True

  return False


def get_internet_discount(building_id, amount, curdict):
  ''' Gets internet discount '''
  return get_discount(building_id, 'internet', amount, curdict)

def get_phone_discount(building_id, amount, curdict):
  ''' Gets phone discount '''
  return get_discount(building_id, 'phone', amount, curdict)

def get_list_price(building_id, service_name, curdict):
  curdict.execute('SELECT amount FROM revenue_list_price_tbl WHERE building_id = %s AND revenue_type_id IN (SELECT id FROM revenue_type_tbl WHERE description = %s)', (building_id, service_name,))
  if curdict.rowcount == 0:
    return 0

  return curdict.fetchone()['amount']

def get_discount(building_id, service_name, amount, curdict):
  ''' Calculates discount for service based on building_id '''

  if not amount:
    # Check for invalid amount (None or 0)
    return "N/A"

  list_price = get_list_price(building_id, service_name, curdict)

  if not list_price:
    return "N/A"

  # Discount = (list - amount) / list
  return '{}%'.format(int((list_price - amount) * 100 / list_price ))

def update_discount(building_id, service_name, amount, curdict):
  curdict.execute('SELECT id FROM revenue_type_tbl WHERE description = %s', (service_name,))
  if curdict.rowcount == 0:
    abort(400, 'Failed to find revenue {}'.format(service_name))

  service_id = curdict.fetchone()['id']

  curdict.execute('SELECT 1 FROM revenue_list_price_tbl WHERE building_id = %s AND revenue_type_id = %s', (building_id, service_id,))
  if curdict.rowcount == 1:
    curdict.execute('UPDATE revenue_list_price_tbl SET amount = %s WHERE building_id = %s AND revenue_type_id = %s', (amount, building_id, service_id,))
  else:
    curdict.execute('INSERT INTO revenue_list_price_tbl (amount, building_id, revenue_type_id) VALUES (%s, %s, %s)', (amount, building_id, service_id,))

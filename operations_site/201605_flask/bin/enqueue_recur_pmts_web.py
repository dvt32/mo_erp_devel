from flask import render_template
from flask_wtf import FlaskForm
from wtforms import SubmitField, HiddenField, SelectField, StringField
from wtforms.fields.html5 import DateField,  IntegerField
from wtforms import validators
from wtforms import ValidationError
import psycopg2
import psycopg2.extras
import database
import re
from datetime import datetime, date
import pytz

# Column Headers and Associated Label... Lists are used to preserve order
recur_pmt_headers = ['id', 'date_local', 'pmt_method', 'floor_label', 'cust_acct_id', 'name', 'email', 'note']
recur_pmt_labels = ['Recur ID','Date (local)','Meth','Floor','CstID','Name','Email', 'Note']

class DateRangeForm(FlaskForm):
  """Form used to select range of dates between which to generate enqueue payments
  """
  id_list = HiddenField()
  start = DateField('Start Date UTC', validators=[validators.InputRequired()], default=date.today())
  start_time = StringField('Start Time', validators=[validators.Optional()], render_kw={'placeholder':'e.g. 15:23:00'})
  end = DateField('End Date UTC', validators=[validators.InputRequired()], default=date.today())
  end_time = StringField('End Time', validators=[validators.Optional()])
  allow_duplicates = SelectField('Duplicate Handling', choices = [('all', 'Enqueue'), ('manual','Display Only'), ('none', 'Hide')], default='manual')
  recur_id_field = IntegerField('Recur ID', validators=[validators.Optional(), validators.NumberRange(min=0, message='ID must be greater than 0')], render_kw={'placeholder':'optional'})
  submit = SubmitField()

  def validate_start_time(form, field):
    field.data = format_string_to_time(field.data)

  def validate_end_time(form, field):
    field.data = format_string_to_time(field.data)

  def validate_end(form, field):
    if not form.start.data:
      # Don't try to validate if start is null/empty/invalid
      return
    if form.start.data > field.data:
      raise ValidationError('Start date must be after End date')
    elif field.data == form.start.data:
      if form.start_time.data and form.end_time.data:
        start = format_string_to_time(form.start_time.data)
        end = format_string_to_time(form.end_time.data)
        start_time = datetime.strptime(start, '%H:%M:%S')
        end_time = datetime.strptime(end, '%H:%M:%S')
        if start_time >= end_time:
          raise ValidationError('End date must be later than Start date')
      else:
        raise ValidationError('End date must be later than Start date')

def format_string_to_time(s):
  """Gets integers from string and formats into a 6 digit, '0' padded string

  Args:
      s (string): String containing integers

  Returns:
     String in format 'hh:mm:ss'
  """
  ints = "".join(re.findall("\d+", s))
  if len(ints) > 6:
    raise ValidationError('Error parsing %s, too many digits' % s)
  elif len(ints)%2 != 0:
    # assume user omitted digit in hours
    ints = '0' + ints
  filler = '0' * (6 - len(ints))
  ints = ints + filler
  t = iter(ints)
  time = ':'.join(a+b for a,b, in zip(t,t))
  return time

def show_form():
  form = DateRangeForm()
  return render_template('enqueue_recur_pmts_form.html', form=form)

def show_confirm_form():
  form = DateRangeForm()
  if not form.validate_on_submit():
    return render_template('enqueue_recur_pmts_form.html', form=form)

  start = combine_datetime(form.start.data, form.start_time.data)
  end = combine_datetime(form.end.data, form.end_time.data)

  allow_duplicates = form.allow_duplicates.data
  property_dict = enqueue_recur_pmts(start, end, allow_duplicates, dry_run=True, recur_id=form.recur_id_field.data)
  if 'errors' in property_dict:
    return render_template('enqueue_recur_pmts_form.html', form=form, errors=property_dict['errors'])

  # get list of ids as comma separated values for id_list
  form.id_list.data = [x['id'] for x in property_dict['pmt_recur_list_dict']]
  property_dict['form']=form # used for validation and passing of start/end ts
  return render_template('enqueue_recur_pmts_result.html', **property_dict)

def submit_enqueue():
  form = DateRangeForm()
  if not form.validate_on_submit():
    return render_template('enqueue_recur_pmts_form.html', form=form)

  if not form.id_list.data:
   return render_template('enqueue_recur_pmts_form.html', form=form, errors=["Error validating data. Please try again (Was the table changed?)"])

  # ensure that start/end are datetime objects and not date objects
  start = combine_datetime(form.start.data, form.start_time.data)
  end = combine_datetime(form.end.data, form.end_time.data)

  # enqueue the payments and get the result
  allow_duplicates = form.allow_duplicates.data
  property_dict = enqueue_recur_pmts(start, end, allow_duplicates, dry_run=False, id_list=form.id_list.data, recur_id=form.recur_id_field.data)
  if 'errors' in property_dict:
    return render_template('enqueue_recur_pmts_form.html', form=form, errors=property_dict['errors'])

  return render_template('enqueue_recur_pmts_result.html', **property_dict)

def combine_datetime(date, time):
  """Combines the date of the first parameter and the time of the second

  Args:
      date (date): Date value to keep
      time (datetime/None): datetime object to extract time from, can be None

  Returns:
     Combines date and time of parameters. Converts date into datetime object
         if time is None.
  """

  if not time:
    time = datetime.min.time()
  else:
    time = datetime.strptime(time, '%H:%M:%S').time()
  return datetime.combine(date, time)


def enqueue_recur_pmts(start_ts, end_ts, allow_duplicates, dry_run, recur_id=None, id_list=None):
  """Enqueues payments between two timestamps returning property_dict

  Enqueues payments with value 0.01 for each payment. Allows duplicates to be
  skipped, force added, or displayed depending on mode. dry_run enables a no-commit mode.
  Key 'errors' exists in property_dict if an error was encountered
  Everything is done in UTC to avoid any time issues.

  Payments are defined as being 'ready' at midnight (12:00am or 0000) of that day.

  e.g. 7/31 9:01 pm @ LA = 8/1 12:01 am @ NY
  4:01 AM UTC is unambiguously 8/1 in NY and 7/31 in LA

  Args:
      start_ts (datetime): Start Timestamp without timezone
      end_ts (datetime): End Timestamp without timezone
      allow_duplicates (string): Duplicate handling mode (all, manual, none)
      dry_run (boolean): if True, no commits will occur. If false, payments will be enqueued
      recur_id (int): if specified, will only list transactions by pmt_recur_tbl.id
      id_list (string): List of id from form HiddenField in format "['1', '2', '3']"

  Returns:
      property_dict (dict): contains redundant information and enqueue results
          into a single dictionary for Jinja rendering.
  """
  property_dict = {}


  if not dry_run and not id_list:
    raise Exception('Function cannot run without id_list in this context.')

  if id_list:
    # format id_list (remove brackets, spaces, and quotes)
    id_list = re.sub("[\[\]\' ]",'',id_list)
    if not id_list:
      # catch empty id_list
      return { 'errors' : ['No transactions to submit.'] }
    # convert each item to int and put in a list
    id_list = [int(x) for x in id_list.split(',')]

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # Add timestamp to datetime since form is naive timestamp
  utc_tz = pytz.timezone('UTC')
  start_ts = utc_tz.localize(start_ts)
  end_ts = utc_tz.localize(end_ts)

  # load pmt_vault_txn_vw rows to handle duplicates (if allow_duplications option is not 'all').
  # keys in dictionary are in format 'YYYY-MM-DD:xx' where xx is the cust_acct_id.
  dup_pmt_vault_txn_by_date_cust_acct_id = {}
  if allow_duplicates == 'all':
    sql_query = None
  elif allow_duplicates == 'manual':
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE scheduled_date BETWEEN %s AND %s AND pmt_recur_id IS NULL', (start_ts.date(), end_ts.date()))
    for pmt_vault_txn in curdict:
      dup_pmt_vault_txn_by_date_cust_acct_id[str(pmt_vault_txn['scheduled_date']) + ':' + str(pmt_vault_txn['cust_acct_id'])] = pmt_vault_txn
  elif allow_duplicates == 'none':
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE scheduled_date BETWEEN %s AND %s', (start_ts.date(), end_ts.date()))
    for pmt_vault_txn in curdict:
      dup_pmt_vault_txn_by_date_cust_acct_id[str(pmt_vault_txn['scheduled_date']) + ':' + str(pmt_vault_txn['cust_acct_id'])] = pmt_vault_txn

  # create temporary tables needed for pmt_recur_vw query.
  curdict.execute('CREATE TEMPORARY TABLE used_timezone_tbl AS SELECT DISTINCT building_timezone FROM pmt_recur_vw')
  curdict.execute('CREATE TEMPORARY TABLE date_range_tbl AS SELECT DISTINCT * FROM (SELECT generate_series(%s::timestamp with time zone at time zone building_timezone, %s::timestamp with time zone at time zone building_timezone, %s)::date AS date_local, building_timezone FROM used_timezone_tbl UNION SELECT (%s::timestamp with time zone at time zone building_timezone)::date, building_timezone FROM used_timezone_tbl) AS t1', (start_ts, end_ts, '1 day', end_ts))
  # fetch pmt_recur_vw rows for dates in the range of dates
  sql_where_list = []
  sql_param_list = []
  # add parameters from ON clause
  sql_param_list.append('day')
  sql_where_list.append('(pmt_recur_vw.start_date IS NULL OR pmt_recur_vw.start_date <= date_range_tbl.date_local)')
  sql_where_list.append('(pmt_recur_vw.end_date IS NULL OR pmt_recur_vw.end_date >= date_range_tbl.date_local)')
  sql_where_list.append('pmt_recur_vw.is_active')
  if recur_id:
    sql_param_list.append(recur_id)
    sql_where_list.append('pmt_recur_vw.id=%s')
  sql_query = 'SELECT pmt_recur_vw.*, date_range_tbl.date_local FROM pmt_recur_vw JOIN date_range_tbl ON (pmt_recur_vw.building_timezone = date_range_tbl.building_timezone AND pmt_recur_vw.day_of_month = date_part(%s, date_range_tbl.date_local)) WHERE ' + ' AND '.join(sql_where_list) + ' ORDER BY pmt_recur_vw.id'
  sql_param_tuple = tuple(sql_param_list)
  curdict.execute(sql_query, sql_param_tuple)

  num_added_rows = 0
  # dict used to show list of inserted rows via jinja
  pmt_recur_list_dict = []
  # keep track of duplicate rows so we can report and allow operator to fix problems.
  dup_pmt_vault_txn_list = []

  # keep track of held pmt_vault_txn rows so that we can report the rows that we refrained from inserting (duplicate detection) AND
  # were on hold. Those rows require operator intervention to avoid missing a charge.
  held_pmt_vault_txn_list = []

  insert_curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  for pmt_recur in curdict:
    dup_pmt_vault_txn = dup_pmt_vault_txn_by_date_cust_acct_id.get(str(pmt_recur['date_local']) + ':' + str(pmt_recur['cust_acct_id']))
    if dup_pmt_vault_txn:
      dup_pmt_vault_txn_list.append(dup_pmt_vault_txn)
      if dup_pmt_vault_txn['hold']:
        held_pmt_vault_txn_list.append(dup_pmt_vault_txn)
      continue
    pmt_recur_list_dict.append(dict(pmt_recur))
    if not dry_run:
      insert_curdict.execute('INSERT INTO pmt_vault_txn_tbl (pmt_vault_id, amount, scheduled_date, processor, email, pmt_recur_id) VALUES (%s, %s, %s, %s, %s, %s)', (pmt_recur['pmt_vault_id'], '0.01', pmt_recur['date_local'], pmt_recur['processor'], pmt_recur['email'], pmt_recur['id']))
    num_added_rows += 1

  if not dry_run:
    for item in pmt_recur_list_dict:
      if item['id'] in id_list:
        id_list.remove(item['id'])
      else:
        errors = ['Could not validate a transaction with id %s, please try again.' % item['id']]
        property_dict = {'errors':errors}
        return property_dict
    if id_list:
      # if there are any ids left, just display them as a warning message but continue anyways.
      curdict.execute('SELECT * FROM pmt_recur_vw WHERE id IN %s', (tuple(id_list),))
      missing_txn_dict = curdict.fetchall()
      property_dict['missing_txn_dict'] = missing_txn_dict

    conn.commit()

  property_dict.update({'start_ts':start_ts, 'end_ts':end_ts, 'pmt_recur_list_dict':pmt_recur_list_dict, 'recur_pmt_headers':recur_pmt_headers, 'recur_pmt_labels':recur_pmt_labels, 'num_added_rows':num_added_rows, 'dup_pmt_vault_txn_list':dup_pmt_vault_txn_list, 'dry_run':dry_run, 'held_pmt_vault_txn_list':held_pmt_vault_txn_list })

  curdict.close()
  insert_curdict.close()
  conn.close()
  return property_dict

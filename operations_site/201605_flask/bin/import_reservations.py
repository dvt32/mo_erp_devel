#!/var/www/sites/{{ operations_fqdn }}/{{ operations_flask }}/venv/bin/python3

import sys
import argparse
import psycopg2
import psycopg2.extras
import pytz

def main():
  ''' Original reservation_tbl stored naive timestamps as timestamps with timezone
  so all dates are stored as UTC. The following code will process the data and
  convert them to the appropriate UTC times '''

  argparser = argparse.ArgumentParser(description='Import existing reservations from reservation_tbl to utc_reservation_tbl.')
  argparser.add_argument('--commit', help='Commit all changes. Run without this to see number of new rows', action='store_true')
  args = argparser.parse_args()
  conn = psycopg2.connect(database='{{ main_database_name }}', user='{{ main_database_operations_username }}', password='{{ main_database_operations_password }}', host='{{ main_database_hostname }}', sslmode='{{ main_database_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  insert_curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SET TIME ZONE 'UTC'")
  insert_curdict.execute("SET TIME ZONE 'UTC'")

  # create dictionary containing all distinct timezones
  curdict.execute('SELECT room.id, building.timezone FROM room_tbl room, building_tbl building WHERE building.id in (SELECT building_id FROM floor_tbl WHERE id in (SELECT floor_id FROM room_tbl WHERE id = room.id))')
  tz = {}
  for row in curdict:
    tz[row['id']] = pytz.timezone(row['timezone'])

  curdict.execute('SELECT id, room_id, account_id, start_dt, end_dt, note FROM reservation_tbl WHERE id NOT IN (select id from utc_reservation_tbl)')
  total_rows = curdict.rowcount
  print('Processing %s rows...' % total_rows)

  if args.commit:
    i=1
    for row in curdict:
      # remove the tzinfo and then localize to the appropriate timezone (which is fetched from building_tbl)
      row['start_dt' ] = tz[row['room_id']].localize(row['start_dt'].replace(tzinfo=None))
      row['end_dt' ] = tz[row['room_id']].localize(row['end_dt'].replace(tzinfo=None))
      insert_curdict.execute('INSERT INTO utc_reservation_tbl (id, room_id, account_id, start_dt, end_dt, note) VALUES (%s, %s, %s, %s, %s, %s)', (row))

    conn.commit()
    print('Successfully imported all rows')
  else:
    print('Did not commit, please run with --commit')

if __name__ == "__main__":
  main()

import database
import psycopg2
import psycopg2.extras
from flask import render_template, jsonify, request
from flask_wtf import csrf
from ajax_helper import validate_csrf_token
from wtforms import ValidationError

''' This file meant to replace the current system of CMs
  browsing emails to determine if clients have purchased daypasses.'''

def show_page():
  ''' Shows page with table of active and inactive daypasses

  Args:
    floor_id: floor_tbl.id (GET param)
  '''
  try:
    floor_id = int(request.args.get('floor_id'))
  except ValueError:
    return 'An invalid floor id was specified.'
  except TypeError:
    return 'You must specify a floor id to view this page.'

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.execute('SELECT short_global_label FROM floor_tbl WHERE id = %s and has_daypass_service', (floor_id,))
  if curdict.rowcount != 1:
    return 'The floor you have specified does not exist or does not offer daypasses.'
  floor_label = curdict.fetchone()['short_global_label']
  curdict.execute('SELECT * FROM daypass_vw WHERE floor_id = %s AND NOT is_redeemed order by date_of_service, id', (floor_id,))
  curdict2 = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict2.execute('SELECT * FROM daypass_vw WHERE floor_id = %s AND is_redeemed order by date_of_service, id', (floor_id,))
  property_dict = {'active_passes':curdict, 'inactive_passes':curdict2, 'static_base_url':'{{ static_base_url }}', 'csrf_token': csrf.generate_csrf(), 'floor_label':floor_label }
  return render_template('manage_daypass.html', **property_dict)

def update_pass():
  if not validate_csrf_token():
    return jsonify(result='fail')
  pass_id = request.args.get('pass_id')
  status = request.args.get('status')
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('UPDATE daypass_tbl SET is_redeemed = %s WHERE id = %s', (status, pass_id,))
  conn.commit()
  return jsonify(result='success')

def update_note():
  if not validate_csrf_token():
    return jsonify(result='fail')
  pass_id = request.args.get('pass_id')
  note = request.args.get('note')
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('UPDATE daypass_tbl SET note = %s WHERE id = %s', (note, pass_id,))
  conn.commit()
  return jsonify(result='success')

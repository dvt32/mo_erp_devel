import database
import psycopg2
import psycopg2.extras
from flask import render_template, jsonify, request
from flask_wtf import csrf
from ajax_helper import validate_csrf_token
import auth
import re

''' Manage who receives emails
    e.g. desk_sale_interactions emails '''

def show_page():
  ''' Shows page with table of active and inactive daypasses

  Args:
    location: building_tbl.id (GET param)
  '''
  if not is_authorized():
    return jsonify(error='You are not authorized to view this page.')
  try:
    building_id = int(request.args.get('building_id'))
  except ValueError:
    return 'An invalid location was specified.'
  except TypeError:
    return 'You must specify a location to view this page.'

  property_dict = {}
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.execute('SELECT short_global_label FROM building_tbl WHERE id = %s', (building_id,))
  if curdict.rowcount == 0:
    return 'Unable to find building with id {}'.format(building_id)
  property_dict['building_label'] = curdict.fetchone()['short_global_label']

  curdict.execute('SELECT id, name, description FROM report_type_tbl ORDER BY id ASC')
  report_types = { str(row['id']): {'description' : row['description'], 'name':row['name'] } for row in curdict }
  property_dict['report_types'] = report_types

  for report_id in report_types:
    curdict.execute('SELECT employee_id as id, first_name, last_name, email FROM report_email_tbl LEFT JOIN employee_tbl ON employee_tbl.id = employee_id WHERE report_type_id = %s AND report_email_tbl.building_id = %s ORDER BY first_name, last_name', (report_id, building_id,))
    property_dict['report_types'][report_id]['employees'] = curdict.fetchall()

  # Get all active employees
  curdict.execute('SELECT first_name, last_name, id, email FROM employee_tbl WHERE is_active')
  property_dict['employees'] = curdict.fetchall()
  property_dict['csrf_token'] = csrf.generate_csrf()
  return render_template('manage_report.html', **property_dict)

def delete_recipient():
  if not is_authorized():
    return jsonify(error='You are not authorized to do this action')
  if not validate_csrf_token():
    return jsonify(error='Invalid token. Please reload the page and try again.')
  try:
    building_id = int(request.args.get('building_id'))
    report_type_id = int(request.args.get('report_type_id'))
    employee_id = int(request.args.get('employee_id'))
  except (ValueError, TypeError):
    print('Failed to update; invalid ajax request')
    return jsonify(error='Malformed request')

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('DELETE FROM report_email_tbl WHERE employee_id = %s AND building_id = %s AND report_type_id = %s', (employee_id, building_id, report_type_id,))
  conn.commit()

  return jsonify(result='ok')

def add_recipient():
  if not is_authorized():
    return jsonify(error='You are not authorized to do this action')
  if not validate_csrf_token():
    return jsonify(error='Invalid token. Please reload the page and try again.')
  try:
    building_id = int(request.args.get('building_id'))
    report_type_id = int(request.args.get('report_type_id'))
    employee_id = int(request.args.get('employee_id'))
  except (ValueError, TypeError):
    return jsonify(error='Malformed request')

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT email FROM employee_tbl WHERE id = %s', (employee_id,))
  if curdict.rowcount == 0:
    return jsonify(error='Failed to add recipient (does not exist)')

  email = curdict.fetchone()['email']
  if not re.match(r'^.*@((microoffice)|(coalitionspace))\.com', email):
    return jsonify(error='{} is not a valid email.'.format(email))

  curdict.execute('SELECT 1 FROM report_email_tbl WHERE employee_id = %s AND building_id = %s AND report_type_id = %s', (employee_id, building_id, report_type_id,))
  if curdict.rowcount == 0:
    curdict.execute('INSERT INTO report_email_tbl (employee_id, building_id, report_type_id) VALUES (%s, %s, %s)', (employee_id, building_id, report_type_id,))
    conn.commit()

  return jsonify(result='ok')

def is_authorized():
  return auth.authorize_role('role:report/manage_recipients')

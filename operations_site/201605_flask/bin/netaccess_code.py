import flask
import re
from flask import g
from flask import request, render_template, url_for, redirect
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField, HiddenField, SelectField
from wtforms.fields.html5 import EmailField, DateField
from wtforms_components import If
from wtforms import validators, ValidationError
import psycopg2.extras
import database
import dateutil.parser
import random
import string
import datetime
import pytz
from email.mime.text import MIMEText
import email.utils
import email.header
import smtplib

class CodeForm(FlaskForm):
  first_name = StringField('First Name', validators=[validators.Optional()], render_kw={'placeholder':'First Name'})
  last_name = StringField('Last Name', validators=[validators.Optional()], render_kw={'placeholder':'Last Name'})
  company = StringField('Company', validators=[validators.Optional()], render_kw={'placeholder': 'Company'})
  email = EmailField('Guest Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder':'Guest\'s Email'})
  phone = StringField('Phone Number', validators=[validators.Optional()], render_kw={'placeholder': 'Phone Number'})
  building = SelectField('Building', validators=[validators.InputRequired()])
  duration = IntegerField('Duration', validators=[If(lambda form, field: form.rel_end.data == '', validators.InputRequired()), If(lambda form, field: form.rel_end.data != '', validators.Optional())], render_kw={'placeholder': 'Duration (in minutes)'})
  rel_end = SelectField('For one:', choices=[('', ''), ('day', 'day'), ('week', 'week'), ('month', 'month')], validators=[If(lambda form, field: not form.duration.data, validators.InputRequired()), If(lambda form, field: form.duration.data, validators.Optional())])
  not_before = DateField('Code Not Valid Before', validators=[validators.Optional()])
  code_expire = IntegerField('Code is valid for:', validators=[validators.Optional(), validators.NumberRange(min=1, max=366)], render_kw={'placeholder': '# of days code is valid for'})
  how_many_codes = SelectField('How Many Codes', validators=[validators.InputRequired()], coerce=int)
  submitForm = SubmitField('Generate Code')
  
  def validate_duration(form, field):
    if form.rel_end.data and field.data != None:
      raise ValidationError('You can only enter in duration OR relative end, not both.')
    return True
  
  def validate_rel_end(form, field):
    if form.duration.data and field.data != None:
      raise ValidationError('You can only enter in duration OR relative end, not both.')
    return True

def get_code_form():
  form = CodeForm()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT building_tbl.*, state_tbl.abbreviation FROM building_tbl JOIN state_tbl ON building_tbl.state = state_tbl.name WHERE building_tbl.is_active = %s AND building_tbl.type = %s', (True, 'officesuite'))
  form.building.choices = [(str(building['id']), building['address_1'] + ', ' + building['city'] + ', ' + building['abbreviation'] +
  ' ' + building['zip_code']) for building in curdict]
  form.how_many_codes.choices = [(x, str(x)) for x in range(1, 11)]
  return form

def access_code():
  if request.method == 'GET':
    form = get_code_form()
    return render_template('code_form.html', form=form)
  else:
    form = get_code_form()
    if not form.validate_on_submit():
      return render_template('code_form.html', form=form)
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM external_daypass_user_tbl WHERE email = %s', (form.email.data,))
    existing_daypass_user = curdict.fetchone()
    if existing_daypass_user:
      curdict.execute('UPDATE external_daypass_user_tbl SET (first_name, last_name, company, building_id) = (%s, %s, %s, %s) WHERE id = %s RETURNING *', (form.first_name.data, form.last_name.data, form.company.data, form.building.data, existing_daypass_user['id']))
    else:
      curdict.execute('INSERT INTO external_daypass_user_tbl (first_name, last_name, company, email, building_id) VALUES (%s, %s, %s, %s, %s) RETURNING *', (form.first_name.data, form.last_name.data, form.company.data, form.email.data, form.building.data))
    account = curdict.fetchone()
    curdict.execute('SELECT * FROM building_tbl WHERE id = %s', (account['building_id'],))
    building = curdict.fetchone()
    curdict.execute('SELECT * FROM building_link_tbl WHERE building_id = %s', (account['building_id'],))
    building_link = curdict.fetchone()
    if form.not_before.data:
      not_before = datetime.datetime.combine(form.not_before.data, datetime.datetime.now(pytz.timezone('UTC')).timetz())
    else:
      not_before = datetime.datetime.now(pytz.timezone('UTC'))
    expire = not_before.astimezone(pytz.timezone(building['timezone']))
    if not form.code_expire.data or form.code_expire.data == 1:
      expire = expire.replace(hour=23,minute=59,second=59)
      expire = expire.astimezone(pytz.timezone('UTC'))
    else:
      expire += datetime.timedelta(days=form.code_expire.data)
      expire = expire.astimezone(pytz.timezone('UTC'))
    codes = []
    for i in range(form.how_many_codes.data):
      code = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
      codes.append(code)
      curdict.execute("INSERT INTO netauth_device_transient_tbl (external_daypass_user_id, building_id) VALUES (%s, %s) RETURNING *", (account['id'], form.building.data))
      dummy_dev = curdict.fetchone()
      if form.duration.data:
        curdict.execute("INSERT INTO netauth_access_code_tbl (netauth_device_id, activation_code, duration, code_expire_ts, not_before_ts) VALUES (%s, %s, interval '%s minutes', %s, %s)", (dummy_dev['id'], code, form.duration.data, expire, not_before))
      else:
        curdict.execute("INSERT INTO netauth_access_code_tbl (netauth_device_id, activation_code, relative_end, code_expire_ts, not_before_ts) VALUES (%s, %s, %s, %s, %s)", (dummy_dev['id'], code, form.rel_end.data, expire, not_before))
    conn.commit()
    # convert to local timezone for display purposes
    not_before = not_before.astimezone(pytz.timezone(building['timezone']))
    expire = expire.astimezone(pytz.timezone(building['timezone']))
    mail_body = 'This email is being sent to grant you temporary network access at Micro Office. To use it, on the DEVICE(S) YOU WISH TO GRANT ACCESS TO, first connect to the %s network and enter the password %s. Then visit {{ netauth_base_url }}/temporary and enter one of the following codes. They are each good ' % (building_link['ssid'], building_link['password'])
    if form.duration.data:
      hours = form.duration.data // 60
      mins = form.duration.data % 60
      mail_body += 'for %s hours and %s minutes.' % (hours, mins)
    elif form.rel_end.data:
      mail_body += 'for one %s.' % (form.rel_end.data)
    for code in codes:
      mail_body += '\n%s' % (code,)
    mail_body += '\nThey are valid after %s and will expire on %s.' % (not_before.strftime("%b %d, %Y at %-I:%M %p, %Z"), expire.strftime("%b %d, %Y at %-I:%M %p, %Z"))
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = 'service@microoffice.com'
    mail_msg['To'] = form.email.data
    mail_subject = 'Temporary Access on Micro Office Network for %s %s' % (form.first_name.data, form.last_name.data)
    mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_auth_mail_server }}')
    mailer.starttls()
    mailer.login('{{ outbound_mail_user }}','{{ outbound_mail_pw }}')
    mailer.send_message(mail_msg)
    mailer.quit()
    return render_template('code_thanks.html', codes=codes, duration=form.duration.data, rel_end=form.rel_end.data, not_before=not_before, expire=expire)
    
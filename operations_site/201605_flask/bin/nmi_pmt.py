import psycopg2
import psycopg2.extras
from flask import request, render_template
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, StringField
from wtforms import validators, ValidationError
from wtforms.fields.html5 import DateField, DecimalField
import database

class QueryPmtNmiTxnForm(FlaskForm):
  floor_id = SelectField('Floor', choices=[], render_kw={'placeholder':'Floor'})
  start_date = DateField('Start Date', validators=[validators.Optional()])
  end_date = DateField('End Date', validators=[validators.Optional()])
  min_amount = DecimalField('Min Amount', places=2, validators=[validators.Optional()])
  max_amount = DecimalField('Max Amount', places=2, validators=[validators.Optional()])
  pmt_method = SelectField('Payment Method', choices=[('', '(all)'), ('ach', 'ach'), ('card', 'card')])
  status = SelectField('Status', choices=[('sale', 'sale (all)'), ('settled', 'settled'), ('failed', 'failed')])
  txn_id = StringField('Transaction ID')
  limit = SelectField('Limit', choices=[('100', '100'), ('250', '250'), ('500', '500')])

  def validate_end_date(form, field):
    if form.start_date.data and form.start_date.data > field.data:
      raise ValidationError('start_date must be earlier than end_date; you entered start_date %s and end_date %s' % (str(form.start_date.data), str(field.data)))

def show_pmt_vaults():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_vw ORDER BY id')
  pmt_vault_list = []
  for pmt_vault in curdict:
    pmt_vault_list.append(pmt_vault)
  conn.close()
  return render_template('show_pmt_vaults.html', pmt_vault_list=pmt_vault_list)

num_days_in_past = {{ pmt_nmi_txn_num_days_in_past }}
num_days_in_past_failed = {{ pmt_nmi_txn_num_days_in_past_failed }}

def show_pmt_nmi_txns():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE request_posted_ts > now() - interval \'%s days\' ORDER BY request_posted_ts DESC', (num_days_in_past, ))
  processed_pmt_nmi_txn_list = [pmt_nmi_txn for pmt_nmi_txn in curdict]
  curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE request_posted_ts IS NULL ORDER BY scheduled_ts_utc')
  pending_pmt_nmi_txn_list = [pmt_nmi_txn for pmt_nmi_txn in curdict]
  curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE (response != %s OR id IN (SELECT pmt_nmi_txn_id FROM pmt_nmi_txn_action_tbl WHERE action_type IN (%s, %s))) AND request_posted_ts > now() - interval \'%s days\' ORDER BY request_posted_ts DESC', ('1', 'check_return', 'check_late_return', num_days_in_past_failed))
  failed_pmt_nmi_txn_list = [pmt_nmi_txn for pmt_nmi_txn in curdict]
  # fetch linked pmt_nmi_txn_action_tbl rows.
  curdict.execute('SELECT * FROM pmt_nmi_txn_action_tbl WHERE pmt_nmi_txn_id IN (SELECT id FROM pmt_nmi_txn_vw WHERE request_posted_ts IS NULL OR (request_posted_ts > now() - interval \'%s days\')) ORDER BY action_ts', (num_days_in_past_failed, ))
  pmt_nmi_txn_action_list_by_pmt_nmi_txn_id = {}
  for pmt_nmi_txn_action in curdict:
    if not pmt_nmi_txn_action['pmt_nmi_txn_id'] in pmt_nmi_txn_action_list_by_pmt_nmi_txn_id:
      pmt_nmi_txn_action_list_by_pmt_nmi_txn_id[pmt_nmi_txn_action['pmt_nmi_txn_id']] = []
    pmt_nmi_txn_action_list_by_pmt_nmi_txn_id[pmt_nmi_txn_action['pmt_nmi_txn_id']].append(pmt_nmi_txn_action)
  conn.close()
  return render_template('show_pmt_nmi_txns.html', num_days_in_past=num_days_in_past, processed_pmt_nmi_txn_list=processed_pmt_nmi_txn_list, pending_pmt_nmi_txn_list=pending_pmt_nmi_txn_list, num_days_in_past_failed=num_days_in_past_failed, failed_pmt_nmi_txn_list=failed_pmt_nmi_txn_list, pmt_nmi_txn_action_list_by_pmt_nmi_txn_id=pmt_nmi_txn_action_list_by_pmt_nmi_txn_id)

def show_check_returns():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id IN (SELECT pmt_nmi_txn_id FROM pmt_nmi_txn_action_tbl WHERE action_type IN (%s, %s) AND action_ts > now() - interval \'%s days\')', ('check_return', 'check_late_return', num_days_in_past_failed))
  returned_pmt_nmi_txn_list = [pmt_nmi_txn for pmt_nmi_txn in curdict]
  # fetch linked pmt_nmi_txn_action_tbl rows.
  curdict.execute('SELECT * FROM pmt_nmi_txn_action_tbl WHERE pmt_nmi_txn_id IN (SELECT pmt_nmi_txn_id FROM pmt_nmi_txn_action_tbl WHERE action_type IN (%s, %s) AND action_ts > now() - interval \'%s days\') ORDER BY action_ts', ('check_return', 'check_late_return', num_days_in_past_failed))
  pmt_nmi_txn_action_list_by_pmt_nmi_txn_id = {}
  for pmt_nmi_txn_action in curdict:
    if not pmt_nmi_txn_action['pmt_nmi_txn_id'] in pmt_nmi_txn_action_list_by_pmt_nmi_txn_id:
      pmt_nmi_txn_action_list_by_pmt_nmi_txn_id[pmt_nmi_txn_action['pmt_nmi_txn_id']] = []
    pmt_nmi_txn_action_list_by_pmt_nmi_txn_id[pmt_nmi_txn_action['pmt_nmi_txn_id']].append(pmt_nmi_txn_action)
  conn.close()
  returned_pmt_nmi_txn_list.sort(key=lambda pmt_nmi_txn: pmt_nmi_txn_action_list_by_pmt_nmi_txn_id[pmt_nmi_txn['id']][-1]['action_ts'], reverse=True)
  return render_template('show_check_returns.html', num_days_in_past_failed=num_days_in_past_failed, returned_pmt_nmi_txn_list=returned_pmt_nmi_txn_list, pmt_nmi_txn_action_list_by_pmt_nmi_txn_id=pmt_nmi_txn_action_list_by_pmt_nmi_txn_id)

def get_query_pmt_nmi_txn_form():
  form = QueryPmtNmiTxnForm()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT floor_tbl.* FROM floor_tbl, building_tbl WHERE floor_tbl.is_active AND building_tbl.is_active AND building_tbl.type = %s AND building_tbl.id = floor_tbl.building_id ORDER BY floor_tbl.short_global_label', ('officesuite', ))
  choice_list = [('', '(all)')]
  choice_list.extend([(str(floor['id']), floor['short_global_label']) for floor in curdict])
  form.floor_id.choices = choice_list
  return form

def query_pmt_nmi_txn_input():
  form = get_query_pmt_nmi_txn_form()
  return render_template('query_pmt_nmi_txn_input.html', form=form)

def query_pmt_nmi_txn_result():
  form = get_query_pmt_nmi_txn_form()
  if not form.validate_on_submit():
    return render_template('query_pmt_nmi_txn_input.html', form=form)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  sql_table_list = ['pmt_nmi_txn_vw']
  sql_where_list = []
  sql_param_list = []
  criteria_list = []
  sql_where_list.append('request_posted_ts IS NOT NULL')
  if form.floor_id.data:
    sql_where_list.append('floor_id = %s')
    sql_param_list.append(form.floor_id.data)
    curdict.execute('SELECT * FROM floor_vw WHERE id = %s', (form.floor_id.data, ))
    row = curdict.fetchone()
    criteria_list.append('floor ' + row['short_global_label'])
  if form.start_date.data:
    sql_where_list.append('request_posted_ts >= %s')
    sql_param_list.append(form.start_date.data)
    criteria_list.append('start date ' + str(form.start_date.data))
  if form.end_date.data:
    sql_where_list.append('request_posted_ts <= %s')
    sql_param_list.append(form.end_date.data)
    criteria_list.append('end date ' + str(form.end_date.data))
  if form.min_amount.data:
    sql_where_list.append('amount >= %s::money')
    sql_param_list.append(form.min_amount.data)
    criteria_list.append('min amount ' + str(form.min_amount.data))
  if form.max_amount.data:
    sql_where_list.append('amount <= %s::money')
    sql_param_list.append(form.max_amount.data)
    criteria_list.append('max amount ' + str(form.max_amount.data))
  if form.pmt_method.data:
    sql_where_list.append('pmt_method = %s')
    sql_param_list.append(form.pmt_method.data)
    criteria_list.append('payment method ' + str(form.pmt_method.data))
  if form.status.data:
    criteria_list.append('status ' + str(form.status.data))
    if form.status.data == 'settled':
      sql_where_list.append('action_list::text ~~ %s')
      sql_param_list.append('%|settle|%')
    elif form.status.data == 'failed':
      sql_where_list.append('(response != %s OR action_list::text ~~ %s OR action_list::text ~~ %s)')
      sql_param_list.append('1')
      sql_param_list.append('%|check_return|%')
      sql_param_list.append('%|check_late_return|%')
  if form.txn_id.data:
    sql_where_list.append('transactionid = %s')
    sql_param_list.append(form.txn_id.data)
    criteria_list.append('transaction id ' + str(form.txn_id.data))
  sql_query = 'SELECT pmt_nmi_txn_vw.* FROM ' + ', '.join(sql_table_list)
  if len(sql_where_list) > 0:
    sql_query += ' WHERE ' + ' AND '.join(sql_where_list)
  sql_limit = 100
  if form.limit.data and int(form.limit.data) <= 500:
    sql_limit = int(form.limit.data)
    criteria_list.append('limit ' + str(form.limit.data))
  sql_query += ' ORDER BY request_posted_ts DESC LIMIT %s'
  curdict.execute(sql_query, tuple(sql_param_list + [sql_limit]))
  pmt_nmi_txn_list = [pmt_nmi_txn for pmt_nmi_txn in curdict]
  # fetch linked pmt_nmi_txn_action_tbl rows.
  sql_query = 'SELECT pmt_nmi_txn_vw.id FROM ' + ', '.join(sql_table_list)
  if len(sql_where_list) > 0:
    sql_query += ' WHERE ' + ' AND '.join(sql_where_list)
  sql_query += ' ORDER BY request_posted_ts DESC LIMIT %s'
  sql_query = 'SELECT * FROM pmt_nmi_txn_action_tbl WHERE pmt_nmi_txn_id IN (' + sql_query + ') ORDER BY action_ts'
  curdict.execute(sql_query, tuple(sql_param_list + [sql_limit]))
  pmt_nmi_txn_action_list_by_pmt_nmi_txn_id = {}
  for pmt_nmi_txn_action in curdict:
    if not pmt_nmi_txn_action['pmt_nmi_txn_id'] in pmt_nmi_txn_action_list_by_pmt_nmi_txn_id:
      pmt_nmi_txn_action_list_by_pmt_nmi_txn_id[pmt_nmi_txn_action['pmt_nmi_txn_id']] = []
    pmt_nmi_txn_action_list_by_pmt_nmi_txn_id[pmt_nmi_txn_action['pmt_nmi_txn_id']].append(pmt_nmi_txn_action)
  conn.close()
  return render_template('query_pmt_nmi_txn_result.html', pmt_nmi_txn_list=pmt_nmi_txn_list, pmt_nmi_txn_action_list_by_pmt_nmi_txn_id=pmt_nmi_txn_action_list_by_pmt_nmi_txn_id, criteria_list=criteria_list)

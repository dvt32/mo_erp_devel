from flask import render_template
from flask_wtf import FlaskForm
from wtforms import SubmitField, HiddenField
from wtforms import ValidationError
import psycopg2
import psycopg2.extras
import database
import re
import chargefacade
import decimal
import datetime

ach_delay_hours = {{ pay_ach_delay_hours }}

# Which columns to display to the user
COLUMNS_TO_DISPLAY = ['id', 'cust_acct_id','name', 'pmt_method', 'amount', 'scheduled_date', 'floor_label', 'note']
COLUMN_NAMES = ['Txn ID', 'Acct ID', 'Name', 'Meth', 'Amount', 'Sched. date', 'Floor Label', 'Note']

class DummyForm(FlaskForm):
  """Dummy form used to take advantage of csrf validation in wtforms
  """
  submit = SubmitField()

class ConfirmationForm(FlaskForm):
  """Confirmation form used for csrf validation and to store/validate a list of transaction ids
  """
  id_list = HiddenField()
  submit = SubmitField('Confirm')

  def validate_id_list(form,field):
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    # Remove brackets and single quotes
    field.data = field.data[1:-1].replace("'","")

    # If field.data is null, no transactions have been chosen
    if not field.data:
      raise ValidationError('At least one transaction must be selected')

    # split by comma and cast to integer, save as list
    # This will raise a ValueError if a non-integer is found
    field.data = [int(x) for x in field.data.split(',')]

    # This removes any duplicates, not that order is not preserved
    field.data = list(set(field.data))

    for id in field.data:
      curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE id = %s AND ((pmt_method = \'ach\' AND scheduled_ts_utc <= now() + interval \'%s hours\') OR (pmt_method = \'card\' AND scheduled_ts_utc <= now())) AND hold = false AND request_posted_ts IS NULL AND response IS NULL ORDER BY created_ts', (id, ach_delay_hours,))
      if curdict.rowcount != 1:
        raise ValidationError('Valid transaction with id %s not found.' % id)


def show_form(errors=[]):
  """Outputs a template listing pending vault transactions

  Arguments:
    errors (tuple) - list of errors
  """
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE ((pmt_method = \'ach\' AND scheduled_ts_utc <= now() + interval \'%s hours\') OR (pmt_method = \'card\' AND scheduled_ts_utc <= now())) AND hold = false AND request_posted_ts IS NULL AND response IS NULL ORDER BY id', (ach_delay_hours,))
  return render_template('pending_sales.html', form=DummyForm(), static_base_url = '{{ static_base_url }}', columns=COLUMNS_TO_DISPLAY, column_names=COLUMN_NAMES, pmt_vault_list=curdict, errors=errors)

def confirm_payment_selection(PostArgs):
  """Generates a table with selected (valid) payment vault transactions

  Arguments:
    PostArgs (Immutable Dict List) - list of post arguments, includes csrf token, submit, and select boxes

  Returns:
    Template containing selected transactions or shows base form if an error was encountered.
  """
  form = DummyForm()

  # Validate CSRF token
  if not form.validate_on_submit():
    error = ('invalid CSRF token',)
    return show_form(error)

  id_list = []
  pattern =  r'^select_[0-9]+$'

  for arg in PostArgs:
    result = re.match(pattern,arg)
    if result:
      id_list.append(result.group(0)[7:])

  if len(id_list)==0:
    error = ('At least one transaction must be selected',)
    return show_form(error)

  form = ConfirmationForm()
  form.id_list.data = id_list
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE id in %s AND((pmt_method = \'ach\' AND scheduled_ts_utc <= now() + interval \'%s hours\') OR (pmt_method = \'card\' AND scheduled_ts_utc <= now())) AND hold = false AND request_posted_ts IS NULL AND response IS NULL ORDER BY created_ts', (tuple(id_list), ach_delay_hours,))

  if curdict.rowcount>0:
    return render_template('confirm_sales.html', form=form, static_base_url='{{ static_base_url }}', columns=COLUMNS_TO_DISPLAY, column_names=COLUMN_NAMES, pmt_vault_list=curdict)

  # Not enough valid rows found
  return show_form(('Error generating table (timeout or invalid transaction choice)',))


def process_payments():
  """  Processes user submitted pending payments.

  Returns:
    Webpage displaying results of payment(s)
  """
  form = ConfirmationForm()

  # Validate CSRF token and parse/validate id_list field
  if not form.validate_on_submit():
    return show_form(tuple(form.errors.values()))

  id_list = form.id_list.data

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  num_success = 0
  num_failure = 0
  subtotal = 0
  failed_subtotal = 0
  nmi_facade = chargefacade.NmiFacade(conn)
  # If subject lines need Jinja variables at run time, use:
  # .replace('[[', '{''{').replace(']]', '}''}')
  # That eliminates conflict between Jinja during build and Jinja during runtime.
  ach_succ_subj_tmpl  = 'Processed ACH/bank payment to Micro Office'
  ach_fail_subj_tmpl  = 'ACTION REQUIRED - failed ACH/bank payment to Micro Office'
  card_succ_subj_tmpl = 'Successful card payment to Micro Office'
  card_fail_subj_tmpl = 'ACTION REQUIRED - failed card payment to Micro Office'

  # template path
  if not {{is_production_deployment}}:
    nmi_facade.set_demo_mode(True) # Also set transactions to go through demo gateway
    templ_path = '{{operations_basedir}}/share/' # needed because operations site is not currently deployable on devenv
  else:
    templ_path = '/var/www/sites/{{ operations_fqdn }}/{{ operations_flask }}/share/'

  with open(templ_path + 'ach_success_tmpl.txt', 'r') as tmpl_file:
    ach_succ_text_tmpl = tmpl_file.read()
  with open(templ_path + 'ach_success_tmpl.html', 'r') as tmpl_file:
    ach_succ_html_tmpl = tmpl_file.read()
  with open(templ_path + 'ach_failure_tmpl.txt', 'r') as tmpl_file:
    ach_fail_text_tmpl = tmpl_file.read()
  with open(templ_path + 'ach_failure_tmpl.html', 'r') as tmpl_file:
    ach_fail_html_tmpl = tmpl_file.read()
  with open(templ_path + 'card_success_tmpl.txt', 'r') as tmpl_file:
    card_succ_text_tmpl = tmpl_file.read()
  with open(templ_path + 'card_success_tmpl.html', 'r') as tmpl_file:
    card_succ_html_tmpl = tmpl_file.read()
  with open(templ_path + 'card_failure_tmpl.txt', 'r') as tmpl_file:
    card_fail_body_tmpl = tmpl_file.read()
  with open(templ_path + 'card_failure_tmpl.html', 'r') as tmpl_file:
    card_fail_html_tmpl = tmpl_file.read()
  nmi_facade.set_ach_succ_tmpl(ach_succ_subj_tmpl, ach_succ_text_tmpl, ach_succ_html_tmpl)
  nmi_facade.set_ach_fail_tmpl(ach_fail_subj_tmpl, ach_fail_text_tmpl, ach_fail_html_tmpl)
  nmi_facade.set_card_succ_tmpl(card_succ_subj_tmpl, card_succ_text_tmpl, card_succ_html_tmpl)
  nmi_facade.set_card_fail_tmpl(card_fail_subj_tmpl, card_fail_body_tmpl, card_fail_html_tmpl)

  pmt_vault_txn_list = []

  for id in id_list:
    curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_vault_txn_vw WHERE id=%s AND ((pmt_method = \'ach\' AND scheduled_ts_utc <= now() + interval \'%s hours\') OR (pmt_method = \'card\' AND scheduled_ts_utc <= now())) AND hold = false AND request_posted_ts IS NULL AND response IS NULL ORDER BY created_ts', (id, ach_delay_hours,))

    pmt_vault_txn = curdict.fetchone()
    validated_id = pmt_vault_txn['id']

    try:
      result_pmt_vault_txn = nmi_facade.process_pmt_vault_txn_as_client(validated_id) # untestable on dev env, use process_pmt_vault_txn_by_row
      curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
      email_id = curdict.fetchone()['next_id']
      more_dict = { 'email_id' : email_id }
      nmi_facade.set_more_tmpl_dict(more_dict)
      nmi_facade.send_mail_vault_txn(result_pmt_vault_txn)
    except psycopg2.IntegrityError as e:
      print('Failed on pmt_vault_txn_vw ID: ' + str(id))
      print('ID List: ' + str(id_list))
      raise

    pmt_vault_txn_list.append(result_pmt_vault_txn)
    if result_pmt_vault_txn['response']=='1':
      num_success += 1
      subtotal += pmt_vault_txn['amount_dec']
    else:
      num_failure += 1
      failed_subtotal += pmt_vault_txn['amount_dec']
  return render_template('confirm_sales_result.html', pmt_vault_txn_list=pmt_vault_txn_list, num_success=num_success, num_failed=num_failure, subtotal=subtotal, failed_subtotal=failed_subtotal)

from flask import render_template, request, redirect, url_for, abort, jsonify
from datetime import timedelta, datetime, date, time
from flask_wtf import FlaskForm, csrf
from ajax_helper import validate_csrf_token
from wtforms import StringField, SelectField, HiddenField, SubmitField, validators, ValidationError
import dateutil
import database
import psycopg2
import psycopg2.extras
import pytz

account_id_of_corporate = 1
time_interval = 15 ## minutes between
time_start = 9     ## start time (in mililtary time, integer)
time_end =  17     ## end time (in military time, integer)
ROOM_TYPES = ['conference', 'therapy']
static_base_url = '{{ static_base_url }}'

class ReservationForm(FlaskForm):
  user_input = HiddenField('Contains start_ts, end_ts, room_id')
  reservation_id = HiddenField('optional reservation id, start/end will be updated')
  account = SelectField('Account')
  new_note = StringField('Note')
  edit_note = StringField('Note')
  submit = SubmitField('Save')

def set_cursor_to_utc(curdict):
  # used to make sure all time zones are inserted / retrieved in UTC (regardless of database default)
  curdict.execute("SET TIME ZONE 'UTC'")

def utc_to_local(utc_dt, local_tz):
  ### utc_dt is a datetime object with timezone
  ### local_tz is a pytz object i.e. pytz.timezone('America/New_York')
  return utc_dt.replace(tzinfo=pytz.utc).astimezone(tz=local_tz)

def local_to_utc(local_dt, local_tz):
  ### local_dt is the naive datetime
  ### local_tz is the pytz object i.e. pytz.timezone('America/New_York')
  d_tz = local_tz.normalize(local_tz.localize(local_dt))
  return d_tz.astimezone(pytz.utc)

def get_reservation_form():
  form = ReservationForm()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


  floor_id = request.args.get('floor_id', None, type=int)
  Date = request.args.get('date', None)
  room_type = request.args.get('room_type',None)

  if floor_id:
    curdict.execute('SELECT * FROM floor_vw WHERE id=%s', (floor_id,))
    if curdict.rowcount==0:
      abort(400, 'You omitted the floor or entered a bad floor.')
  else:
    abort(400, 'You omitted the floor or entered a bad floor.')

  if room_type is None or room_type not in ROOM_TYPES:
    abort(400, 'You omitted the room type or entered a bad room type.')

  try:
    form.time_start_date = datetime.strptime(Date, '%Y-%m-%d').date()
  except ValueError:
    abort(400, 'You entered a bad date. The format should be YYYY-MM-DD.')

  curdict.execute('SELECT * FROM building_tbl WHERE id IN (SELECT building_id FROM floor_tbl WHERE id=%s)', (floor_id,))
  building = dict(curdict.fetchone())
  building_id = building['id']
  form.building_timezone = building['timezone']

  curdict.execute('SELECT DISTINCT * FROM account_vw WHERE (id=%s) OR (is_active AND primary_floor_id in (SELECT id FROM floor_tbl WHERE building_id=%s)) ORDER BY name', (account_id_of_corporate, building_id,))

  form.account.choices = [(str(x['id']), x['name']) for x in curdict]
  form.account.choices.insert(0, ('','---')) # add an empty option as default value

  form.floor_id = floor_id
  form.room_type = room_type
  return form


def get_room_list(floor_id, room_type):
  # Returns a list of dictionaries, each item being a dictionary
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM room_tbl WHERE floor_id = %s AND room_type = %s AND is_active', (floor_id, room_type,))
  room_list = [dict(row) for row in curdict]
  return room_list

def show_page():
  ''' Generate the reserve_room page
  '''
  # redirect for default date... Explicitly include it in URL to prevent any timezone date ambiguity
  if not request.args.get('date', None):
    return redirect(url_for('show_reserve_room', floor_id=request.args.get('floor_id'), room_type=request.args.get('room_type'), date=datetime.today().strftime('%Y-%m-%d')))
  form = get_reservation_form()
  rooms = get_room_list(form.floor_id, form.room_type)
  tz = pytz.timezone(form.building_timezone)

  times = []  ## list of dicts containing utc timestamp and local time (eg. 08:00 am, 08:30 am, ...)
  local_date = form.time_start_date
  local_start_time = datetime.combine(local_date, time.min)
  local_end_time = datetime.combine(local_date, time.max)

  utc_start_time = local_to_utc(local_start_time, tz)
  utc_end_time = local_to_utc(local_end_time, tz)

  t_iter = utc_start_time
  t_delta = timedelta(minutes=time_interval)

  while t_iter < utc_end_time:
    local_time = utc_to_local(t_iter,tz).strftime('%I:%M %p')
    local_end_time = utc_to_local(t_iter+t_delta, tz).strftime('%I:%M %p')
    utc_time = t_iter.strftime('%Y-%m-%d %H:%M')
    utc_end = (t_iter+t_delta).strftime('%Y-%m-%d %H:%M')
    times.append({'utc': utc_time, 'utc_end':utc_end, 'local': local_time, 'local_end': local_end_time })
    t_iter = t_iter + t_delta

  timezone = utc_to_local(utc_start_time,tz).strftime('%Z')
  property_dict = { 'times':times, 'form':form, 'rooms':rooms, 'static_base_url':static_base_url, 'timezone':timezone }
  return render_template('reserve_room.html', **property_dict)

def submit_page():
  ''' Creates a new reservation '''
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  set_cursor_to_utc(curdict)
  form = get_reservation_form()
  date = form.time_start_date.strftime('%Y-%m-%d')

  if not form.validate_on_submit():
    # only validates the CSRF token here
    abort(400,'Something went wrong while submitting your reservation request. (Invalid token, usually fixed by reloading the previous page.)')

  # javascript function get_selected() populates the hidden field with data as | separated values
  start, end, room_id = form.user_input.data.split(' | ')

  try:
    start_utc = pytz.utc.localize(datetime.strptime(start, '%Y-%m-%d %H:%M'))
    end_utc = pytz.utc.localize(datetime.strptime(end, '%Y-%m-%d %H:%M'))
  except ValueError:
    abort(400,'Something went wrong while submitting your reservation request. (Invalid time input)')


  ## form validation
  # validate chosen time range
  curdict.execute('SELECT * FROM utc_reservation_tbl WHERE room_id=%s AND (start_dt, end_dt) OVERLAPS (%s, %s)', (room_id, start_utc, end_utc,))
  if curdict.rowcount!=0:
    # redirect to page without any database changes, this will cause the page to update with any conflicts.
    abort(400,'Something went wrong while submitting your reservation request. (Overlaps another reservation)')

  # validate account ID
  if not form.account.data:
    abort(400,'Something went wrong while submitting your reservation request. (Did not receive account data)')

  curdict.execute('SELECT * FROM account_vw WHERE id=%s', (form.account.data,))
  if curdict.rowcount!=1:
    abort(400,'Something went wrong while submitting your reservation request. (Invalid Account data)')

  # validate room ID
  curdict.execute('SELECT * FROM room_tbl WHERE id=%s AND is_active', (room_id,))
  if curdict.rowcount!=1:
    abort(400,'Something went wrong while submitting your reservation request. (Invalid room ID)')
  curdict.execute('INSERT INTO utc_reservation_tbl (room_id, account_id, start_dt, end_dt, note) VALUES (%s, %s, %s, %s, %s)', (room_id, form.account.data, start_utc, end_utc, form.new_note.data))
  conn.commit()
  date = form.time_start_date.strftime('%Y-%m-%d')
  return redirect(url_for('show_reserve_room', date=date, floor_id=form.floor_id, room_type=form.room_type))

def update_reservation():
  ''' Updates a specific reservation ID and check if there are any overlaps
  '''
  reservation_id = request.args.get('reservation_id', 0, type=int)
  start = request.args.get('start_ts',0)
  end = request.args.get('end_ts',0)
  room_id = request.args.get('room_id', 0, type=int)
  if not validate_csrf_token():
    return jsonify(error='Invalid token. Please reload the page and try again.')

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  set_cursor_to_utc(curdict)

  start_utc = pytz.utc.localize(datetime.strptime(start, '%Y-%m-%d %H:%M'))
  end_utc = pytz.utc.localize(datetime.strptime(end, '%Y-%m-%d %H:%M'))

  curdict.execute('SELECT * FROM utc_reservation_tbl WHERE room_id=%s AND (start_dt, end_dt) OVERLAPS (%s, %s)', (room_id, start_utc, end_utc,))
  if curdict.rowcount>1:
    return jsonify(error='The selected timeframe overlaps another reservation.')
  else:
    for row in curdict:
      if row['id'] != reservation_id:
        return jsonify(error='The selected timeframe overlaps another reservation.')

  curdict.execute('UPDATE utc_reservation_tbl SET start_dt=%s, end_dt=%s, room_id=%s WHERE id=%s', (start_utc, end_utc, room_id, reservation_id))
  conn.commit()
  return jsonify(result='ok')

def update_reservation_note():
  ''' updates the note
  '''

  reservation_id = request.args.get('reservation_id', 0, type=int)
  note = request.args.get('note')

  if not validate_csrf_token():
    return jsonify(error='Invalid token. Please reload the page and try again.')

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  set_cursor_to_utc(curdict)
  curdict.execute('UPDATE utc_reservation_tbl SET note=%s WHERE id=%s', (note, reservation_id,))
  conn.commit()
  return jsonify(result='ok')


def get_reservation():
  ''' Gets all reservation on a given date, must be run from a browser context

  GET Args:
    floor_id:  Floor id
    room_type: Type of room. Must be in global variable 'ROOM_TYPE'
    date:      Date in format YYYY-MM-DD

  Returns:     JSON Dictionary
    Key:   reservation_id
    Value: JSON Dictionary
      Keys:   start_ts:     timestamp YYYY-MM-DD HH:MM
              end_ts:       timestamp YYYY-MM-DD HH:MM
              room_id:      room ID
              account_name: account name (string)
  '''

  # Get Arguments
  floor_id = request.args.get('floor_id', 0, type=int)
  room_type = request.args.get('room_type')
  date = request.args.get('date')

  if not validate_csrf_token():
    return jsonify(error='Invalid token. Please reload the page and try again.')

  # Validate Arguments
  try:
    date = datetime.strptime(date, '%Y-%m-%d')
  except ValueError:
    return jsonify(error='You entered a bad date. The format should be YYYY-MM-DD.')

  if room_type not in ROOM_TYPES:
    return jsonify(error= 'Bad room type received. Must be in %s' % ROOM_TYPES)

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


  # initialize timezone object
  curdict.execute('SELECT timezone FROM building_tbl WHERE id in (SELECT building_id FROM floor_tbl WHERE id=%s)', (floor_id,))
  timezone = curdict.fetchone()['timezone']
  tz = pytz.timezone(timezone)

  start_time = local_to_utc(date, tz)

  # initialize dict with keys from times
  reservations = {}
  set_cursor_to_utc(curdict)
  curdict.execute('''SELECT * FROM utc_reservation_vw WHERE room_id IN (SELECT id FROM room_tbl WHERE floor_id=%s AND room_type=%s AND is_active) AND (start_dt, end_dt) OVERLAPS (%s, %s + INTERVAL '1 day')''', (floor_id, room_type, start_time, start_time))

  for row in curdict:
    start = row['start_dt'].strftime('%Y-%m-%d %H:%M')
    end =   row['end_dt'].strftime('%Y-%m-%d %H:%M')
    reservation = {'start_ts':start, 'end_ts':end, 'room_id':row['room_id'], 'account_name':row['account_name'], 'note':row['note']}
    reservations[row['id']] = reservation

  return jsonify(reservations)

def delete_reservation():
  ''' Deletes a reservation by ID
  '''
  reservation_id = request.args.get('reservation_id')
  if not validate_csrf_token():
    return jsonify(error='Invalid token. Please reload the page and try again.')

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('DELETE FROM utc_reservation_tbl WHERE id=%s', (reservation_id,))
  conn.commit()

  # Doesn't really need to return anything, get_reservations() will update accordingly
  return jsonify(result='ok')

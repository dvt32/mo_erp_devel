import sys
sys.path.append('/var/www/sites/{{ operations_fqdn }}/{{ operations_flask }}/bin')
sys.path.append('/var/www/sites/{{ operations_fqdn }}/{{ operations_flask }}/lib')
from flask import Flask, g, request
import nmi_pmt
import display_table
import edit_desk
import pending_vault_sales_web
import enqueue_recur_pmts_web
import reserve_room
import manage_daypass
import financial_analysis
import netaccess_code
import manage_report
import essensys_helper

app = Flask(__name__)
app.secret_key = '{{ operations_flask_secret_key }}'
app.debug = {{ operations_flask_app_debug }}

@app.context_processor
def define_static_url():
    return dict(static_base_url='{{ static_base_url }}')

@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@app.route('/finance/show_pmt_vaults')
def show_pmt_vaults():
  return nmi_pmt.show_pmt_vaults()

@app.route('/finance/show_pmt_nmi_txns')
def show_pmt_nmi_txns():
  return nmi_pmt.show_pmt_nmi_txns()

@app.route('/finance/show_check_returns')
def show_check_returns():
  return nmi_pmt.show_check_returns()

@app.route('/finance/query_pmt_nmi_txn', methods=['GET', 'POST'])
def query_pmt_nmi_txn():
  if request.method == 'GET':
    return nmi_pmt.query_pmt_nmi_txn_input()
  else:
    return nmi_pmt.query_pmt_nmi_txn_result()

@app.route('/tools/disp_tbl/')
def show_form():
  return display_table.show_form()

@app.route('/tools/disp_tbl/result', methods=['GET','POST'])
def show_table():
  if request.method != 'POST':
    return redirect(url_for('show_form'))
  else:
    return display_table.show_table()

@app.route('/tools/disp_tbl/<int:request_id>/', methods=['GET'])
def show_table2(request_id):
  return display_table.show_table(request_id)

@app.route('/tools/disp_tbl/<int:request_id>/sort/<argument>/', methods=['GET'])
def sort_table(request_id, argument):
  return display_table.sort_table(request_id, argument)

@app.route('/tools/disp_tbl/<int:request_id>/next/', methods=['GET'])
def next_page(request_id):
  return display_table.next_page(request_id, 1)

@app.route('/tools/disp_tbl/<int:request_id>/prev/', methods=['GET'])
def prev_page(request_id):
  return display_table.next_page(request_id, -1)

@app.route('/tools/disp_tbl/<int:request_id>/skip/<int:page_num>', methods=['GET'])
def skip_page(request_id, page_num):
  if page_num <= 0:
    return display_table.show_table(request_id)
  return display_table.skip_to_page(request_id, page_num)

@app.route('/tools/disp_tbl/<int:request_id>/first/', methods=['GET'])
def first_page(request_id):
  return display_table.first_page(request_id)

@app.route('/tools/disp_tbl/<int:request_id>/last/', methods=['GET'])
def last_page(request_id):
  return display_table.last_page(request_id)

@app.route('/tools/disp_tbl/<int:request_id>/rows/<int:rpp>', methods=['GET'])
def rows_per_page(request_id, rpp):
  return display_table.set_rows_per_page(request_id,rpp)

@app.route('/tools/disp_tbl/<int:request_id>/limit/<int:limit>', methods=['GET'])
def change_limit(request_id, limit):
  return display_table.set_limit(request_id, limit)

@app.route('/tools/edit_tbl/<table>/<int:id>', methods=['GET'])
def edit_table(table, id):
  if table == 'desk_tbl':
    return edit_desk.show_form(id)
  else:
    return 'No form built for this table.'

@app.route('/tools/edit_table/desk_tbl/result', methods=['POST'])
def submit_desk_tbl():
  return 'Under construction'

@app.route('/finance/pending_sales/')
def pending_sales():
  return pending_vault_sales_web.show_form()

@app.route('/finance/pending_sales/submit', methods=['POST'])
def submit_pending_sales():
  return pending_vault_sales_web.confirm_payment_selection(request.form)

@app.route('/finance/confirm_sales/submit', methods=['POST'])
def confirm_submission():
  return pending_vault_sales_web.process_payments()

@app.route('/finance/enqueue_recur')
def show_enqueue():
  return enqueue_recur_pmts_web.show_form()

@app.route('/finance/enqueue_recur/confirm', methods=['POST'])
def confirm_enqueue():
  return enqueue_recur_pmts_web.show_confirm_form()

@app.route('/finance/enqueue_recur/result', methods=['POST'])
def submit_enqueue():
  return enqueue_recur_pmts_web.submit_enqueue()

## financial analysis/
@app.route('/finance/analysis', methods=['GET'])
def show_financial_analysis():
  return financial_analysis.show_page()

@app.route('/finance/report', methods=['GET'])
def show_report_recipients():
    return manage_report.show_page()

@app.route('/finance/report/delete_recipient', methods=['GET'])
def delete_report_recipient():
    return manage_report.delete_recipient()

@app.route('/finance/report/add_recipient', methods=['GET'])
def add_report_recipient():
    return manage_report.add_recipient()

# room reservation tools
@app.route('/meet/_update_reservation')
def update_reservation():
  return reserve_room.update_reservation()

@app.route('/meet/_update_reservation_note')
def update_reservation_note():
  return reserve_room.update_reservation_note()

@app.route('/meet/_get_reservation')
def get_reservations():
  return reserve_room.get_reservation()

@app.route('/meet/_delete_reservation')
def delete_reservation():
  return reserve_room.delete_reservation()

@app.errorhandler(400)
def custom400(e):
  return '%s<br/>Click <a href="#" onclick="history.go(-1)">here</a> to go back or you can return to the <a href="http://operations.microoffice.com/1g/">main menu</a>.' % (e.description)

@app.route('/meet/reserve_room', methods=['GET'])
def show_reserve_room():
  return reserve_room.show_page()

@app.route('/meet/reserve_room/submit', methods=['POST'])
def submit_reserve_room():
  return reserve_room.submit_page()

# edit desk tools
@app.route('/space/edit_desk_info/<int:desk_id>', methods=['GET'])
def show_edit_desk(desk_id):
  return edit_desk.show_page(desk_id)

@app.route('/space/edit_desk_info/<int:desk_id>/submit_sale_interaction', methods=['GET', 'POST'])
def submit_desk_sale_interaction(desk_id):
  return edit_desk.submit_interaction(desk_id)

@app.route('/space/edit_desk_info/<int:desk_id>/submit_desk', methods=['GET', 'POST'])
def submit_edit_desk(desk_id):
  return edit_desk.submit_desk_form(desk_id)

@app.route('/space/edit_desk_info/delete_desk_price', methods=['GET', 'POST'])
def delete_desk_price():
  return edit_desk.delete_desk_price()

@app.route('/space/edit_desk_info/delete_desk_sale_interaction', methods=['GET', 'POST'])
def delete_desk_sale_interaction():
  return edit_desk.delete_desk_sale_interaction()

@app.route('/space/query_accounts', methods=['GET', 'POST'])
def query_desk_accounts():
  return edit_desk.query_accounts()

# manage daypass tools
@app.route('/daypass')
def manage_daypass_by_floor():
  return manage_daypass.show_page()

@app.route('/daypass/_update_status')
def update_daypass():
  return manage_daypass.update_pass()

@app.route('/daypass/_update_note')
def update_daypass_note():
  return manage_daypass.update_note()

@app.route('/accesscode', methods=['GET', 'POST'])
def access_code():
  return netaccess_code.access_code()

# Essensys tools
@app.route('/essensys/_update_accounts')
def update_essensys_accounts():
  # Updates cust_acct_tbl.essensys_acct_id
  return essensys_helper.update_accounts()

@app.route('/essensys/_update_licenses')
def update_essensys_licenses():
  # Updates essensys_license_tbl
  return essensys_helper.update_licenses()

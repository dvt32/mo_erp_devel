from flask_wtf import csrf
from flask import request

# Helps take care of some standard ajax request functions

def validate_csrf_token():
  ''' function to be run in AJAX context
  gets CSRF-Token in request header and returns true or false

  Returns T/F:
    True - Valid token
    False - Invalid token
  '''
  # Should not occur (token was not passed as a header)

  if 'X-Csrftoken' not in request.headers.keys():
    return False

  csrf_token = request.headers['X-Csrftoken']
  try:
    csrf.validate_csrf(csrf_token)
  except ValidationError:
    return False
  return True

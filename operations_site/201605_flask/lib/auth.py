import database
import psycopg2
import psycopg2.extras
from flask import abort, g, request

def get_role_list():
  role_list = getattr(g, '_role_list', None)
  if role_list is None:
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    employee_id = get_employee()['id']
    curdict.execute('SELECT element_value FROM ss_employee_x_role_tbl WHERE parent_id = %s', (employee_id,))
    role_list = g._role_list = [ row['element_value'] for row in curdict ]
  return role_list

def get_employee():
  # Get lower case employee
  employee = getattr(g, '_employee', None)
  if employee is None:
    logon_name = request.environ.get('REMOTE_USER').lower()
    # Remove domain
    if '@' in logon_name:
      logon_name = logon_name.split('@')[0]
    if not logon_name:
      abort(400, 'error getting logon name')
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM employee_tbl WHERE windows_logon_name = %s and is_active', (logon_name,))
    if curdict.rowcount == 0:
      abort(400, 'error getting logon name')
    employee = g._employee = curdict.fetchone()
  return employee

# Returns true if employee has role
def authorize_role(role):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  employee_id = get_employee()['id']
  curdict.execute('SELECT 1 FROM ss_employee_x_role_tbl WHERE parent_id = %s AND element_value = %s', (employee_id, role,))
  return curdict.rowcount == 1

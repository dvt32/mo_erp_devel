import requests
import database
import psycopg2
import psycopg2.extras
from flask import jsonify
from collections import OrderedDict

ESSENSYS_VIRTUAL_OFFICE_ID = 23

class TokenException(Exception):
  """ Occurs when failing to get a token. Typically occurs if essensys_user or essensys_password is incorrect in system properties """
  pass

class EssensysFacade:
  base_auth_url = 'https://operate-api.essensys.tech/oauth2'
  base_api_url = 'https://operate-api.essensys.tech/api/1.0'

  def __init__(self):
    self.header = {
      'Authorization': 'Bearer {}'.format(self.get_token())
    }

  def get_token(self):
    URL = self.base_auth_url + '/token'

    data = {
      'grant_type':'password',
      'scope':'hubapi',
      'client_id': '{{ essensys_client_id }}',
      'client_secret': '{{ essensys_client_secret }}',
      'username':'{{ essensys_user }}',
      'password':'{{ essensys_password }}'
    }

    # Make the POST request with passed in URL and DATA
    # Get the object as JSON
    # Access the 'access_token'
    request = requests.post(url=URL, data=data)

    # Check if request failed
    if request.status_code == 400:
      raise TokenException()
    return request.json()['access_token']

  def make_request(self, url, where=None, fields=None, data=None):
    # Make a GET request with URL and optional DATA

    url = self.base_api_url + url

    url += '?'
    if where:
      url += 'where={}'.format(where)
    if fields:
      url += '&fields=[{}]'.format(','.join(fields))

    return requests.get(url=url, data=data, headers=self.header).json()['response']

  def get_locations(self):
    # Get list of locations ONLINE ALIASES
    URL = '/Location'# + '?where=&fields=[onlinealias,locationid]'

    return jsonify(self.make_request(URL)['locations'])

  def get_accounts(self):
    URL = '/Account'
    where = "accounttype_id=-1" # customers only
    fields = ['accountsid', 'accountid', 'clientname']
    return self.make_request(URL, where=where, fields=fields)['accounts']

  def get_licenses(self, floor_id=None, location_id=None):
    ''' Expects either floor_id or location_

    Return value has keys 'clienttype', 'accountname', 'licencestatusid', 'licencestatus'
    ideally we want to do a where locationid=%s and licencestatusid = 2 (active)
    '''
    if location_id is None:
      if floor_id is None:
        return { 'error': 'floor or location id must be specified' }

      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT essensys_location_id FROM building_tbl WHERE id IN (SELECT building_id FROM floor_tbl WHERE id = %s) AND essensys_location_id is not null', (floor_id,))
      if curdict.rowcount != 1:
        return { 'error' : 'Invalid floor id or no associated essensys location id' }

      location_id = curdict.fetchone()['essensys_location_id']

    URL = '/Licence'
    where = 'locationname={}'.format(location_id)
    fields = ['clienttype', 'accountname', 'licencestatusid', 'licencestatus']

    return self.make_request(URL, where, fields)['licences']

  def get_licenses_for_update(self):
    URL = '/Licence'
    fields = ['items', 'accountname', 'enddate', 'startdate', 'licenceid', 'locationname', 'licencestatusid']
    return self.make_request(URL, fields=fields)['licences']

  def get_client_types(self):
    URL  ='/ClientType' + '?fields=[clienttypename,clienttypeid]'
    return self.make_request(URL)['clienttypes']

def update_accounts():
  ''' Updates MicroOffice cust_acct_tbl to contain the essensys account ids '''
  e = EssensysFacade()
  accts = e.get_accounts()
  rows = ({ 'id': acct['accountsid'], 'essensys_id': acct['accountid'] } for acct in accts if has_valid_account_id(acct))
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.executemany('''UPDATE cust_acct_tbl SET essensys_acct_id = %(essensys_id)s WHERE id = %(id)s AND essensys_acct_id is null''', rows)
  conn.commit()
  return 'Complete.'

def has_valid_account_id(account):
  # Check if it doesn't have an MO account id
  if 'accountsid' not in account:
    return False

  # check if it's none
  if not account['accountsid']:
    return False

  # if it's not an integer
  return account['accountsid'].isdigit()

def get_vo_licenses(floor_id):
  ''' expects a floor_id returns active virtual office licenses with mailbox number

  Returns a JSON containing the keys as returned by get_licenses and 'mailbox_number'
  '''
  e = EssensysFacade()
  licenses = e.get_licenses(floor_id=floor_id)
  licenses = [x for x in licenses if validate_license(x)]
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # Get mailbox information from mailbox_tbl
  for license in licenses:
    curdict.execute('''
    SELECT
      mailbox_number
    FROM mailbox_tbl
    LEFT JOIN cust_acct_tbl ON
      mailbox_tbl.account_id = cust_acct_tbl.id
    WHERE
      essensys_acct_id = %s
    ''', (license['accountname'],))

    if curdict.rowcount == 1:
      license['mailbox_number'] = curdict.fetchone()['mailbox_number']

  return jsonify(licenses)

def validate_license(license):
  ''' Validates a virtual office license '''
  # Check if the license is active
  if not int(license['licencestatusid']) == 2:
    return False

  # Check if virtual
  if 'virtual' not in license['outputvalue_clienttype'].lower():
    return False

  return True

def update_licenses():
  ''' Fetches ALL essensys licenses and inserts them into essensys_license_tbl '''

  # Could be improved by adding a WHERE modified_ts > last_update OR created_ts > last_update
  e = EssensysFacade()
  licenses = e.get_licenses_for_update()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # Fields is a map of Essensys Operate's API keys to our local essensys_license_tbl columns
  # The order must be preserved

  fields = OrderedDict([
    ('id','licenceid'),
    ('account_id', 'accountname'),
    ('account_name', 'outputvalue_accountname'),
    ('status', 'outputvalue_licencestatusid'),
    ('location_id', 'locationname'),
    ('items', 'items'),
    ('start_date', 'startdate'),
    ('end_date', 'enddate')
   ])

  rows = ({ microoffice_key: license.get(operate_key) for microoffice_key, operate_key in fields.items() } for license in licenses)

  # This looks something like "%(id)s, %(account_id)s, %(account_name)s ..."
  field_keys = ','.join("%({})s".format(x) for x in fields)

  # This looks like id = excluded.id, account_id = excluded.account_id
  # This is a way of doing an UPDATE if the row already exists.
  update_clause = ', '.join('{0} = excluded.{0}'.format(x) for x in fields)

  # Combine all the data into (01, 001, 'company name' ...), (02, 002, 'company name 2'), ...
  data_text = ', '.join(curdict.mogrify('({})'.format(field_keys), row).decode('utf-8') for row in rows)
  query = 'INSERT INTO essensys_license_tbl VALUES {} ON CONFLICT ON CONSTRAINT essensys_license_tbl_pkey DO UPDATE SET {}'.format(data_text, update_clause)
  curdict.execute(query, rows)

  # Delete rows that were deleted from Essensys
  license_ids = [int(license['licenceid']) for license in licenses]

  curdict.execute('DELETE FROM essensys_license_tbl WHERE id <> ALL(%s)', (license_ids,))
  conn.commit()

  return 'Complete.'

import psycopg2
import psycopg2.extras
import database
from flask import render_template, request
from datetime import datetime

''' This file is meant to show a cross-sectional analysis of a floor and display useful information about its occupancy'''

def show_page():
  ''' Shows the page for financial analysis. Expects a valid floor_id and optional date as GET argument'''
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  floor_id = request.args.get('floor_id', 0)
  date = request.args.get('date',datetime.now().strftime('%Y-%m-%d'))

  # validate floor_id
  curdict.execute('SELECT short_global_label FROM floor_tbl WHERE id = %s and is_active', (floor_id,))
  if curdict.rowcount is 1:
    floor_label = curdict.fetchone()['short_global_label']
  else:
    return 'Invalid floor_id specified. Click <a href="operations.microoffice.com/1g">here</a> to return to the main menu'

  # validate datetime
  try:
    datetime.strptime(date, '%Y-%m-%d')
  except ValueError:
    return 'Invalid date specified. Click <a href="operations.microoffice.com/1g">here</a> to return to the main menu'

  property_dict = get_financial_analysis_dict(floor_id, date)
  property_dict['floor_label'] = floor_label
  property_dict['accounts'] = get_financial_analysis_account_dict(floor_id, date)
  property_dict['date'] = date
  property_dict['static_base_url'] = '{{ static_base_url }}'
  return render_template('financial_analysis.html', **property_dict)


def get_financial_analysis_dict(floor_id, date):
  ''' get financial information for a floor on a certain date. Dictionary returned has the following information:

  Returns
    total_sf: total square footage
    lost_rent: sum of list prices for desks with no contracts
  '''

  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # Get total_sf
  curdict.execute('SELECT SUM(desk_area) AS total_sf FROM desk_area_vw WHERE floor_id = %s', (floor_id, ))
  total_sf = curdict.fetchone()['total_sf']

  # sum list_price for desks without an active contract on the given date
  curdict.execute('SELECT SUM(list_price) AS lost_rent FROM desk_tbl WHERE floor_id = %s AND id NOT IN (SELECT desk_id FROM desk_price_tbl WHERE id IN (SELECT id FROM desk_tbl WHERE floor_id = %s) AND %s BETWEEN start_date AND end_date)', (floor_id, floor_id, date,))
  lost_rent = curdict.fetchone()['lost_rent']

  analysis_dict = {
    'total_sf': total_sf,
    'lost_rent': lost_rent
  }

  return analysis_dict

def get_financial_analysis_account_dict(floor_id, date):
  ''' Get information about all contracts on a floor.

  Returns
    accounts (dict): keys are cust_acct_tbl.id and values are name, total_rent_revenue, total_internet_revenue, total_phone_revenue, and total_occupied_sf
  '''
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  # get total values by account for active contracts on a certain floor and date
  curdict.execute('''SELECT desk_price_tbl.account_id AS account_id, name, SUM(price) as total_rent_revenue, SUM(internet_revenue) as total_internet_revenue, SUM(phone_revenue) AS total_phone_revenue, SUM(desk_area) as total_occupied_sf FROM desk_price_tbl LEFT JOIN account_vw ON account_vw.id = desk_price_tbl.account_id LEFT JOIN desk_area_vw ON desk_price_tbl.desk_id = desk_area_vw.id WHERE %s BETWEEN start_date AND end_date AND desk_id IN (SELECT id FROM desk_tbl WHERE floor_id = %s) GROUP BY desk_price_tbl.account_id, account_vw.name''', (date, floor_id,))

  # create dictionary with key as account_id and value as a dict containing useful information
  accounts = {
    row['account_id']: {
      'name': row['name'],
      'total_rent_revenue': row['total_rent_revenue'],
      'total_internet_revenue': row['total_internet_revenue'],
      'total_phone_revenue': row['total_phone_revenue'],
      'total_occupied_sf': row['total_occupied_sf'] }
    for row in curdict }

  for account in accounts:
    # add linked desks to the dictionary in the form key (desk_id) value (assigned number)
    curdict.execute('SELECT assigned_number, desk_id FROM desk_price_tbl LEFT JOIN desk_tbl ON desk_tbl.id = desk_price_tbl.desk_id WHERE desk_price_tbl.account_id = %s AND %s BETWEEN desk_price_tbl.start_date AND desk_price_tbl.end_date AND desk_id IN (SELECT id FROM desk_tbl WHERE floor_id = %s)', (account, date, floor_id,))
    accounts[account]['linked_desks'] = { row['desk_id']: row['assigned_number'] for row in curdict }

  return accounts

### Makefile to install code into the /var/www hierarchy.

include $(repo_root)/common/func.mk

INSTALLDIR := $(operations_basedir)/2g
RSYNCFLAGS := --inplace -PHqza --exclude Makefile
# after we're done, chown the files to this
OWNER := www-data:www-data
# and chmod to this (except cgi files)
MODE := 644

all:
	@:

$(INSTALLDIR)/composer.phar:
	@cd $(INSTALLDIR); curl -s https://getcomposer.org/installer | php

$(INSTALLDIR)/composer.lock:	$(INSTALLDIR)/composer.phar $(INSTALLDIR)/composer.json
	@cd $(INSTALLDIR); php composer.phar update; php composer.phar install

updatedeps:	$(INSTALLDIR)/composer.lock

install:	makedirectory copyfiles makelinks permowner substitute updatedeps
	@echo "installed to $(INSTALLDIR)"

clean:
ifneq ($(INSTALLDIR),)
ifneq ($(INSTALLDIR),/)
	@echo "will delete $(INSTALLDIR) in 5 seconds; press <control-c> to interrupt"
	@sleep 5
	rm -fr $(INSTALLDIR)
	rm Makefile
endif
endif

copyfiles:
	@rsync $(RSYNCFLAGS) * .htaccess $(INSTALLDIR)/
	@rsync $(RSYNCFLAGS) $(INSTALLDIR)/webroot/js/libs/libero/* $(INSTALLDIR)/webroot/js/

makelinks:
	@ln -sf $(INSTALLDIR)/webroot $(INSTALLDIR)/webroot/app
	@ln -sf $(INSTALLDIR)/Plugin/DebugKit/webroot $(INSTALLDIR)/webroot/debug_kit

permowner:
	@chown -R $(OWNER) $(INSTALLDIR)

# do not include Javascript files in general because they use {% %}, which gives Jinja problems
template_file_regex := .*\.\(py\|pl\|pm\|cgi\|php\|xslt\|html\|ctp\|conf\|env\)$$

substitute:	$(INSTALLDIR)
	$(eval _file_list:=$(shell find $(INSTALLDIR)/app -type f -regex '$(template_file_regex)' -print))
	$(eval _file_list+=$(INSTALLDIR)/webroot/js/nav.js)
	@$(call subst_batch,$(system_properties))

# common php-related defines
include ../../../common/php.mk

# eof

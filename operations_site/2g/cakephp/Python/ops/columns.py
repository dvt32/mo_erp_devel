__author__ = 'johnjjung'
__version__ = '1.0'

# connects to postgres db, adds created and modified if it doesn't exist

from sqlalchemy import *

# connects to postgresql db
db = create_engine('postgres://root:R23herbYT@162.208.108.52:5432/mo_operations_develop')
results = db.execute(''' select * from information_schema.tables where table_catalog = 'mo_operations_develop' and table_schema = 'public' ''')

for table in results:
	print table[2]
	table_name = table[2]
	columns = db.execute("SELECT * FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '"+table_name+"'")

	meshed = []
	for column in columns:
		meshed.append(column[3])

	# print meshed
	# print "created" in meshed
	if not table_name.endswith("_vw"):
		# Created Column
		if 'created' not in meshed:
			created_query = "ALTER TABLE \""+table_name+"\" ADD COLUMN \"created\" Timestamp With Time Zone;"
			# print created_query
			db.execute(created_query)
		else:
			print "Created Exists"
		# Modified Column
		if 'modified' not in meshed:
			modified_query = "ALTER TABLE \""+table_name+"\" ADD COLUMN \"modified\" Timestamp With Time Zone;"
			# print modified_query
			db.execute(modified_query)
		else:
			print "Modified Exists"
		# Modifier ID COlumn
		if 'modifier_id' not in meshed:
			modifier_query = "ALTER TABLE \""+table_name+"\" ADD COLUMN \"modifier_id\" INTEGER;"
			# print modified_query
			db.execute(modifier_query)
		else:
			print "Modifier Exists"


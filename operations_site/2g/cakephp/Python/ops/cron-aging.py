__author__ = 'johnjung'

from sqlalchemy import *
import urllib2
import pprint
import json
import datetime

cronkey = 'uBjs0O6uc9ULETBCqAVENzlTcuTASyuMHm9k2Tt5KxexzXXz6jPr6EasU9tX'
base_url = 'http://ops.dev/'
headers = { 'Content-Type': 'application/json' }

def daysOverdue(duedate):
	date_format = "%m/%d/%Y"
	now = datetime.date.today()
	diff = now - duedate
	# print diff.days
	return diff.days

def updateBilling():

	# run update refresh function to make sure statuses are up to date and days overdue are up to date
	response = urllib2.urlopen(base_url+'customerAccounts/updateAllBillingInformation/'+cronkey+'.json/')
	html = response.read()
	html = json.loads(html)
	# pprint.pprint(json.dumps(html['return']))

	return html['return']

def status_update():
	print "send request to check on CRON. If status is not updated every 24 hours, CRON status is down"

def email_OM():

	now = datetime.date.today()
	# print now.day
	# send on 20th of every month

	if now.day == 18 or now.day == 12:
		req_url = base_url+'/employees/cronEmailBillingReviewPrompt/'+cronkey+'.json'
		try:
			response = urllib2.urlopen(req_url)
			html = response.read()
			html = json.loads(html)
		except urllib2.URLError, e:
			print req_url
			print e.code
			print e.msg
			print e.headers
			print e.fp.read()
			html = '{ "return" : false }'
			pass
		if html['return'] != 1:
			print "Failed: OM Emails not sent"
		else :
			print "OM Emails Sent Successfully"

def aging_1(ca_id):

	req_url = base_url+'/billingAmounts/cronEmailAging1/'+ca_id+'/'+cronkey+'.json'
	# print req_url
	try:
		response = urllib2.urlopen(req_url)
		html = response.read()
		html = json.loads(html)
	except urllib2.URLError, e:
		print req_url
		print e.code
		print e.msg
		print e.headers
		print e.fp.read()
		html = '{ "return" : false }'
		pass

	if html['return'] != 1:
		print "Failed: Aging 2 Email for "+ca_id+" not sent\n\n"+str(html)

def aging_2(ca_id):
	# build link
	req_url = base_url+'billingAmounts/cronEmailAging2/'+ca_id+'/'+cronkey+'.json'
	# print req_url
	try:
		response = urllib2.urlopen(req_url)
		html = response.read()
		# print html
	except urllib2.URLError, e:
		print req_url
		print e.code
		print e.msg
		print e.headers
		print e.fp.read()
		html = '{ "return" : false }'
		pass


	html = json.loads(html)

	if "return" in html:
		if html['return'] != 1:
			print "Failed: Aging 2 Email for "+ca_id+" not sent\n Check if PrimaryCustomer Exists!\n"
	else:
		print "Failed: Aging 2 Email for "+ca_id+" not sent\n\n"

def aging_3(ca_id):
	# build link
	req_url = base_url+'billingAmounts/cronEmailAging3/'+ca_id+'/'+cronkey+'.json'
	# print req_url
	try:
		response = urllib2.urlopen(req_url)
		html = response.read()
		# print html
	except urllib2.URLError, e:
		print req_url
		print e.code
		print e.msg
		print e.headers
		print e.fp.read()
		html = '{ "return" : false }'
		pass


	html = json.loads(html)

	if "return" in html:
		if html['return'] != 1:
			print "Failed: Aging 3 Email for "+ca_id+" not sent\n Check if PrimaryCustomer Exists!\n"
	else:
		print "Failed: Aging 3 Email for "+ca_id+" not sent\n\n"


def aging_4(ca_id):
	# build link
	req_url = base_url+'billingAmounts/cronEmailAging4/'+ca_id+'/'+cronkey+'.json'
	# print req_url
	try:
		response = urllib2.urlopen(req_url)
		html = response.read()
		# print html
	except urllib2.URLError, e:
		print req_url
		print e.code
		print e.msg
		print e.headers
		print e.fp.read()
		html = '{ "return" : false }'
		pass


	html = json.loads(html)

	if "return" in html:
		if html['return'] != 1:
			print "Failed: Aging 4 Email for "+ca_id+" not sent\n Check if PrimaryCustomer Exists!\n"
	else:
		print "Failed: Aging 4 Email for "+ca_id+" not sent\n\n"


def aging_5(ca_id):
	# build link
	req_url = base_url+'billingAmounts/cronEmailAging5/'+ca_id+'/'+cronkey+'.json'
	# print req_url
	try:
		response = urllib2.urlopen(req_url)
		html = response.read()
		# print html
	except urllib2.URLError, e:
		print req_url
		print e.code
		print e.msg
		print e.headers
		print e.fp.read()
		html = '{ "return" : false }'
		pass


	html = json.loads(html)

	if "return" in html:
		if html['return'] != 1:
			print "Failed: Aging 5 Email for "+ca_id+" not sent\n Check if PrimaryCustomer Exists!\n"
	else:
		print "Failed: Aging 5 Email for "+ca_id+" not sent\n\n"

def overView():
	# connects to postgres db
	db = create_engine('postgresql://johnjjung:prosciutto@127.0.0.1:5432/mo_operations_dev_s6')

	# list of customer accounts that are aging
	ca = 'SELECT * from cust_acct_tbl WHERE aging_count > 0 AND is_active = true'
	ca = db.execute(ca)

	if ca.rowcount != 0:
		for account in ca:
			# list of billing amounts for each customer accounts
			# print account[0]
			bas = 'SELECT id, stage, due_date from billing_amount_tbl WHERE cust_acct_id ='+str(account[0])
			bas = db.execute(bas)

			if bas.rowcount != 0:
				for ba in bas:

					# pprint.pprint(ba)
					# validation

					if ba[2] == None:
						print "error: due date not defined for customer account "+str(ba[0])
					elif ba[1] == None:
						print "error: stage not defined for customer account "+str(ba[0])
					else:
						# pprint.pprint(ba[2])
						# pprint.pprint(daysOverdue(ba[2]))
						if ba[1] != 'aging_hold' or ba[1] != 'aging_closed' or ba[1] != 'receiving' or ba[1] != 'invoicing':
							# if 5 days overdue execute php script
							if daysOverdue(ba[2]) == 5:
								aging_1(str(ba[0]))
							# if 30 days overdue execute php script
							elif daysOverdue(ba[2]) == 30:
								aging_2(str(ba[0]))
							# if 40 days overdue execute php script
							elif daysOverdue(ba[2]) == 40:
								aging_3(str(ba[0]))
							# if 45 days overdue execute php script
							elif daysOverdue(ba[2]) == 45:
								aging_4(str(ba[0]))
							# if 75 days overdue execute php script
							elif daysOverdue(ba[2]) == 70:
								aging_5(str(ba[0]))

def promises():
	# connects to postgres db
	db = create_engine('postgresql://johnjjung:prosciutto@127.0.0.1:5432/mo_operations_dev_s6')

	# list of customer accounts that are aging
	promises = 'SELECT * from promise_tbl WHERE status=\'approved\''
	promises = db.execute(promises)

	for promise in promises:
		# only run on the expiration date
		if daysOverdue(promise['expiration_date']) == 0:

			req_url = base_url+'/promises/promiseExpirationDate/'+str(promise[0])+'/'+cronkey+'.json'
			# print req_url
			try:
				response = urllib2.urlopen(req_url)
				html = response.read()
				# pprint.pprint(html)
			except urllib2.URLError, e:
				print req_url
				print e.code
				print e.msg
				print e.headers
				print e.fp.read()
				html = '{ "return" : false }'
				pass

			html = json.loads(html)

			# pprint.pprint(promises)

			if "return" in html:
				if html['return'] != 1:
					print "Failed: Promises Email for promise id "+str(promise[0])+" not sent\n "
			else:
				print "Failed: Promises Email for promise id "+str(promise[0])+" not sent\n\n"


promises()
email_OM()
overView()
__author__ = 'johnjjung'

from sqlalchemy import *
import urllib2
import simplejson
import json
import time

# connects to mysql db
db = create_engine('postgresql://mo_operator:cGmf24S9I@kleene.microoffice.com:5432/microoffice')

results = db.execute('SELECT * FROM public.proximity_card_tbl')
for row in results:
	# generate an address string
	print row['status']+" "+row['technology']+" "+row['card_number']+" "+row['number']
	
	# send request to google maps api v3
	gurl = 'http://maps.googleapis.com/maps/api/geocode/json?address='
	astring = row['address_1']+" "+row['zip_code']
	gurl2= '&sensor=false'
	
	link_built = gurl+urllib2.quote(astring)+gurl2
	print link_built
	
	req = urllib2.Request(link_built, None, {'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36'})
	opener = urllib2.build_opener()
	f = opener.open(req)
	
	# parses the json
	json = simplejson.load(f)
	
	# # doesn't exist
	if (json['status'] == 'ZERO_RESULTS'):
		query = 'UPDATE public.building_tbl SET latitude = '+'null'+' WHERE building_tbl.id = '+row['id']+';'
		results = db.execute(query)
	else:
		# print each of the json array
		print json['results'][0]['geometry']['location']['lat']
		print json['results'][0]['geometry']['location']['lng']
	
		lat =  str(json['results'][0]['geometry']['location']['lat'])
		lng =  str(json['results'][0]['geometry']['location']['lng'])
	
		# INSERT INTO _google_maps (lng, lat, _locations_id) VALUES (-73.0453137, 40.8179265, (SELECT _locations.id FROM _locations WHERE zip = 00501))
		query = 'UPDATE public.building_tbl SET latitude = '+lat+' WHERE building_tbl.id = '+str(row['id'])+';'
		db.execute(query)
		query = 'UPDATE public.building_tbl SET longitude = '+lng+' WHERE building_tbl.id = '+str(row['id'])+';'
		db.execute(query)
	#
		time.sleep(0.1)
	

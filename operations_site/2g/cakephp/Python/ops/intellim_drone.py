__author__ = 'johnjjung'

from sqlalchemy import *
import urllib2
import simplejson
import json
import time
import re


# connects to postgres db
db = create_engine('postgresql://johnjung:prosciutto@127.0.0.1:5432/mo_operations_dev')

# ### SYNCHRONIZES INTELLIM SYSTEM WITH DB


# send request to lemur
start_num = 0;

lemur_url = 'http://doors-int.microoffice.com:18779/infinias/ia/People?Username=jjung&Password=pithstudio&Start='+str(start_num)+'&Limit=50'

print lemur_url

req = urllib2.Request(lemur_url, None, {'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36'})
opener = urllib2.build_opener()
f = opener.open(req)

# parses the json
json = simplejson.load(f)

numberOfRecordsNotInDB = 0

if (json['success'] == True):

	while start_num < (json['data']['Count'] + 100):

		# send requests
		lemur_url = 'http://doors-int.microoffice.com:18779/infinias/ia/People?Username=jjung&Password=pithstudio&Start='+str(start_num)+'&Limit=100'
		req = urllib2.Request(lemur_url, None, {'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36'})
		opener = urllib2.build_opener()
		f = opener.open(req)

		if (json['success'] == True):
			for person in json['data']['Items']:
				# values to update
				card_num = person['CardNumber']
				card_num = card_num[2:]

				# Check to see if they exist in the DB
				check = 'SELECT COUNT(*) FROM public.proximity_card_tbl WHERE intellim_id = '+str(person['Id'])
				check = db.execute(check)
				for count in check:
					if (count['count'] == 0):
						# Updates db
						query = 'UPDATE public.proximity_card_tbl SET intellim_id = \''+str(person['Id'])+\
						        '\', is_credential_holder = \''+str(person['IsCredentialHolder'])+\
								'\', in_group = \''+str(person['InGroup'])+\
								'\', card_number = \''+str(person['CardNumber'])+\
								'\', is_intellim = \'True'+\
						        '\' WHERE number = \''+str(card_num)+'\';'
						#counts number of records missing in intellim DB
						numberOfRecordsNotInDB = numberOfRecordsNotInDB + 1
						# print query

					# else:
						# print str(count['count'])+" of intellim_id's are in the database!"
		# print start_num
		start_num = start_num + 100
		time.sleep(0.1)
		# print numberOfRecordsNotInDB



# ### SYNCHRONIZES INTELLIM SYSTEM WITH DB

### SYNCHRONIZES DB WITH INTELLIM SYSTEM

# Updates db
# results = db.execute('SELECT * FROM public.proximity_card_tbl')
# for row in results:
# 	intellimID = row['intellim_id']
# 	# generate an address string
# 	if (isinstance(intellimID, int)):
# 		# send requests
# 		lemur_url = 'http://doors-int.microoffice.com:18779/infinias/ia/People/Details?Username=jjung&Password=pithstudio&Id='+str(intellimID)
# 		print lemur_url
# 		req = urllib2.Request(lemur_url, None, {'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36'})
# 		opener = urllib2.build_opener()
# 		f = opener.open(req)
# 		json_result = f.read()
# 		if (re.search('failed', json_result) == None):
# 			json_result = re.sub("new Date\([0-9]{12,14}\)",'null', json_result)
# 			json_result = simplejson.loads(json_result)
# 			print json_result
# 			print json_result['data']['RoleInfo']
# 			time.sleep(0.1)




### SYNCHRONIZES DB WITH INTELLIM SYSTEM


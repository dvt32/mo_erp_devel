__author__ = 'johnjjung'

from sqlalchemy import *
import urllib2
import simplejson
from pprint import pprint
import json
import time
import logging

# use to print prettified json array
# start_num = 0
# while start_num < 1300:
# 	# print start_num
# 	# send requests
# 	lemur_url = 'http://doors-int.microoffice.com:18779/infinias/ia/People?Username=jjung&Password=pithstudio&Start='+str(start_num)+'&Limit=100'
# 	req = urllib2.Request(lemur_url, None, {'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36'})
# 	opener = urllib2.build_opener()
# 	f = opener.open(req)
# 	# parses the json
# 	json = simplejson.load(f)
# 	print(simplejson.dumps(json['data']['Items']))
# 	start_num = start_num + 100

# load and parse json array
jsonfile = open('2013-12-18-intellim.json')
data = simplejson.load(jsonfile)

# connects to postgres db
db = create_engine('postgresql://johnjung:prosciutto@127.0.0.1:5432/dev_microoffice')

listofcardsindb = 'SELECT * FROM public.proximity_card_tbl'
listofcardsindb = db.execute(listofcardsindb)
for card in listofcardsindb:
	# print card['number']
	checking_number = card['number']
	exists = 0
	# print checking_number
	for card in data:
		# check to look for numbers in db that are not in intellim
		# print str(card['CardNumber'][2:])+" "+str(checking_number)
		if (str(checking_number) == str(card['CardNumber'][2:])):
			# set exists flag as true
			exists = 1
	# if card number does not exist in intellim db print
	if (exists == 0):
		# print checking_number
		findCard = 'SELECT * FROM public.proximity_card_tbl WHERE number = \''+str(checking_number)+'\';'
		findCard = db.execute(findCard)
		for card in findCard:
			# print card
			print str(card['id'])+"\t"+\
			      str(card['card_number'])+\
			      "\t"+str(card['status'])+\
			      "\t"+str(card['account_id'])+\
			      "\t"+str(card['person_id'])+\
			      "\t"+str(card['note'])+\
			      "\t"+str(card['floor_id'])+\
			      "\t"+str(card['technology'])

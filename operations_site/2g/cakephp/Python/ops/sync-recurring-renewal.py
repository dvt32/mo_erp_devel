__author__ = 'johnjung'

from sqlalchemy import *
import urllib2
import simplejson
from pprint import pprint
import json
import time
import logging
from mmap import mmap,ACCESS_READ
from xlrd import open_workbook
import pprint

# connects to postgres db
db = create_engine('postgresql://johnjung:prosciutto@127.0.0.1:5432/dev_microoffice')

wbname = raw_input("What is the raw, quickbooks export filename (It better be an xlsx file...)? ")

if wbname == '' or wbname[-5:] != '.xlsx':
	print "File not found or it's not an xlsx"
else :

	print "\n LINKING ALL QB NAMES WITH THE DB ...\n"
	# open workbook
	wb = open_workbook(wbname)
	for s in wb.sheets():
		if s.name == 'Sheet1':
			for row in range(s.nrows):
				values = []
				for col in range(s.ncols):
					values.append(s.cell(row, col).value)

				# if string starts with Monthly
				if values[1][:7] != 'Monthly' and values[2] == 'Invoice' and values[5] != 'Never':
					ca = 'SELECT id, name, qb_name FROM cust_acct_tbl WHERE \"name\" ILIKE \'' + values[1] + "\'"
					ca = db.execute(ca)
					# print ca.rowcount

					if ca.rowcount != 0:
						for caid in ca:
							# print caid[0]
							q = 'UPDATE cust_acct_tbl SET pay_terms=\''+values[5]+'\', qb_name=\''+values[1]+'\' WHERE id='+str(caid[0])+';'
							print "\n************************************ SQL ****************************************\n"
							print q
							print "\n************************************ SQL ****************************************\n"
							db.execute(q)

							# create new billing amounts if it doesn't exist
							ex = 'SELECT id FROM billing_amount_tbl WHERE cust_acct_id ='+str(caid[0])
							ex = db.execute(ex)
							if ex.rowcount == 0:

								ba = 'INSERT INTO billing_amount_tbl (recurring_amount, grand_total, created, modified, modifier_id, cust_acct_id, balance) VALUES ('+\
								     str(values[4])+', '+str(values[4])+\
								     ', NOW(), NOW(), 5905, '+str(caid[0])+\
								     ', '+str(values[4])+');'
								print "\n************************************ SQL ****************************************\n"
								print ba
								print "\n************************************ SQL ****************************************\n"

								db.execute(ba)
					else:
						print "\n OKAY, there's some that aren't matching up. I need your help! \n"

						# can't find in db and needs human links
						print values
						print "OKAY, LET'S SEARCH FOR ANYTHING CLOSE TO THE NAME \n"
						bk = str.split(str(values[1]))
						for b in bk:
							print "SEARCHING SOMETHING CLOSE TO: "+str(b)
							# grab something close and print it for human verification
							check_q = 'SELECT id, name, is_active, external_name, description, qb_name FROM cust_acct_tbl WHERE "name" ILIKE \'%%'+str(b)+'%%\';'
							print check_q
							check_q = db.execute(check_q)

							if check_q.rowcount != 0:
								for qy in check_q:
									print qy
							else :
								print "Can't find anything close! =( Try looking manually"


						# input customer account to update with info searched
						caid = raw_input("\n\nWhat is the customer account ID for "+str(values[1])+"?")

						if caid == "none" or caid == None or caid == '':
							print "Hmm, Investigate... and rerun this ... \n"
						else :

							# print caid[0]
							q = 'UPDATE cust_acct_tbl SET pay_terms=\''+values[5]+'\', qb_name=\''+values[1]+'\' WHERE id='+str(caid)+';'
							print "\n************************************ SQL ****************************************\n"
							print q
							print "\n************************************ SQL ****************************************\n"
							db.execute(q)

							# create new billing amounts if it doesn't exist
							ex = 'SELECT id FROM billing_amount_tbl WHERE cust_acct_id ='+str(caid)
							ex = db.execute(ex)
							if ex.rowcount == 0:

								ba = 'INSERT INTO billing_amount_tbl (recurring_amount, grand_total, created, modified, modifier_id, cust_acct_id, balance) VALUES ('+\
								     str(values[4])+', '+str(values[4])+\
								     ', NOW(), NOW(), 5905, '+str(caid)+\
								     ', '+str(values[4])+');'
								print "\n************************************ SQL ****************************************\n"
								print ba
								print "\n************************************ SQL ****************************************\n"
								db.execute(ba)

							print "Account Updated!"


	# now display all the active customers who are not linked to quickbooks
	q = 'SELECT id, name, qb_name, service_id FROM cust_acct_tbl WHERE qb_name IS NULL AND is_active = 1'
	q = db.execute(q)

	if q.rowcount == 0:
		print "Goodness, it's perfect!"
	else :
		print "Sweet Dancing Jesus, There are Customer Accounts that are active without quickbook names associated!\n\n"
		print "\n1 = regular, 2 = starter, 3 = timeshare, 4 = virtual, 5 = therapy\n"
		for acct in q:
			print acct


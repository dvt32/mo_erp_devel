
--
-- Data for Name: company_tbl; Type: TABLE DATA; Schema: public; Owner: pithadmin
--

INSERT INTO company_tbl VALUES (2, 'Micro Office Solutions 1 L.L.C.', '2004-10-17 18:37:17-04', 143555071, '2004-10-17 18:37:17-04', 'MOS1');
INSERT INTO company_tbl VALUES (3, 'Micro Office Solutions 2 L.L.C.', '2004-10-17 18:41:34-04', 1179999743, '2004-10-17 18:41:52-04', 'MOS2');
INSERT INTO company_tbl VALUES (1, 'Micro Office Management L.L.C.', '2004-10-17 18:36:47-04', 882819839, '2004-10-17 18:36:47-04', 'MOM');
INSERT INTO company_tbl VALUES (5, 'Micro Office Solutions 4 L.L.C.', '2006-11-02 13:13:42-05', 341529855, '2006-11-02 13:15:50-05', 'MOS4');
INSERT INTO company_tbl VALUES (4, 'Union Square 902 Suites LLC', '2004-10-17 18:41:53-04', -2087278336, '2004-10-17 18:42:04-04', 'US902S');
INSERT INTO company_tbl VALUES (6, '1601 Broadway Suites L.L.C.', '2013-11-18 19:54:53-05', 1936783261, '2013-11-18 19:54:53-05', '1601Bwy');
INSERT INTO company_tbl VALUES (7, '18 South Michigan Property Investors L.L.C.', '2013-11-18 19:55:15-05', -494836676, '2013-11-18 19:55:15-05', '18SMich');
INSERT INTO company_tbl VALUES (8, 'Harlem Venture Space 2 L.L.C.', '2013-11-18 19:55:39-05', -310091328, '2013-11-18 19:55:39-05', 'HVS');


--
-- PostgreSQL database dump complete
--


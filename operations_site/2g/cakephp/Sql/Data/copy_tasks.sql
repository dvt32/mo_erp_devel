COPY task_tbl (id, description, assigned_employee_id, requestedby_employee_id, requestedby_cust_pers_id, requestedby_cust_acct_id, creation_date, requested_completion_date, completion_date, status, urgency, note, ms_access_creation_dt, ms_access_random_num, creation_dt, floor_id, area, jumble)
FROM 'task_tbl.sql'
WITH DELIMITER AS '~';

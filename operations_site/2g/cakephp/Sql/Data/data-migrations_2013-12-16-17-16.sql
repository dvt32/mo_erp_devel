/* --------------------------------------- */
/* IMPORTANT! apply struct sql diffs first */
/* --------------------------------------- */

/* re-index enum_sales_stage_tbl.id to correspond to name */
UPDATE enum_sales_stage_tbl SET id=id+10;
UPDATE enum_sales_stage_tbl SET id=1 WHERE name LIKE '1%';
UPDATE enum_sales_stage_tbl SET id=2 WHERE name LIKE '2%';
UPDATE enum_sales_stage_tbl SET id=3 WHERE name LIKE '3%';
UPDATE enum_sales_stage_tbl SET id=4 WHERE name LIKE '4%';
UPDATE enum_sales_stage_tbl SET id=5 WHERE name LIKE '5%';
UPDATE enum_sales_stage_tbl SET id=6 WHERE name LIKE '6%';
UPDATE enum_sales_stage_tbl SET id=7 WHERE name LIKE '7%';

/* prospective_customer_tbl.desired_location to building_id */
UPDATE prospective_customer_tbl
	SET building_id=3 
	WHERE desired_location='122W27F12'
		OR desired_location='122 W. 27th St.'
		OR desired_location='122 W. 27th St., Floor 12'
		OR desired_location='NY-122W27F10'
		OR desired_location='NY-122W27F12';
UPDATE prospective_customer_tbl
	SET building_id=2 
	WHERE desired_location='1375 Broadway'
		OR desired_location='1375 Broadway, Floor 3'
		OR desired_location='1375BwyF3'
		OR desired_location='1375Bwy F3'
		OR desired_location='NY-1375BwyF03'
		OR desired_location='NY-1375BwyF06'
		OR desired_location='NY-1375BwyF11';
UPDATE prospective_customer_tbl
	SET building_id=11
	WHERE desired_location='1601Bwy'
		OR desired_location='NY-1601BwyF12';
UPDATE prospective_customer_tbl
	SET building_id=1 
		WHERE desired_location='36W20 F3'
		OR desired_location='36 W. 20th St.'
		OR desired_location='36 W. 20th St., Floor 3'
		OR desired_location='NY-36W20F3';
UPDATE prospective_customer_tbl
	SET building_id=6 
	WHERE desired_location='902Bwy'
		OR desired_location='NY-902BwyF07';

/* prospective_customer_tbl.sales_status to sales_status_id */
UPDATE prospective_customer_tbl
	SET sales_stage_id=1
	WHERE sales_stage='1-awaiting response from us';
UPDATE prospective_customer_tbl
	SET sales_stage_id=2
	WHERE sales_stage='2-left message or sent email';
UPDATE prospective_customer_tbl
	SET sales_stage_id=3
	WHERE sales_stage='3-established conversation';
UPDATE prospective_customer_tbl
	SET sales_stage_id=4
	WHERE sales_stage='4-scheduled meeting';
UPDATE prospective_customer_tbl
	SET sales_stage_id=5
	WHERE sales_stage='5-meeting completed';
UPDATE prospective_customer_tbl
	SET sales_stage_id=6
	WHERE sales_stage='6-sent agreement';
UPDATE prospective_customer_tbl
	SET sales_stage_id=7
	WHERE sales_stage='7-received agreement';

/* sales_interaction_tbl.salesperson to sales_person_id */
UPDATE sales_interaction_tbl
	SET sales_person_id=43
	WHERE salesperson='David'
		OR salesperson='drotbard';
UPDATE sales_interaction_tbl
	SET sales_person_id=55
	WHERE salesperson='jcho'
		OR salesperson='Jin';
UPDATE sales_interaction_tbl
	SET sales_person_id=115
	WHERE salesperson='mrosanvallon';
UPDATE sales_interaction_tbl
	SET sales_person_id=109
	WHERE salesperson='vrodriguez';
UPDATE sales_interaction_tbl
	SET sales_person_id=41
	WHERE salesperson='wchao';
UPDATE sales_interaction_tbl
	SET sales_person_id=3864
	WHERE salesperson='ylam';
UPDATE sales_interaction_tbl
	SET sales_person_id=69
	WHERE salesperson='ysoto'
		OR salesperson='Yvonne';
UPDATE sales_interaction_tbl
	SET sales_person_id=2762
	WHERE salesperson='zprimo';


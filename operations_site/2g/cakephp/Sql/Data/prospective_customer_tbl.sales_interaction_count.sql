UPDATE "prospective_customer_tbl" AS "prospect" 
SET "prospect"."sales_interaction_count"=(
  SELECT COUNT(*)
  FROM "sales_interaction_tbl" AS "interaction"
  WHERE "interaction"."prospective_customer_id"="prospect"."id"
);

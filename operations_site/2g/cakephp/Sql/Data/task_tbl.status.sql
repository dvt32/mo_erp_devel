UPDATE task_tbl SET status='Awaiting Response' WHERE status='Requested';
UPDATE task_tbl SET status='Awaiting Response' WHERE status='Open';
UPDATE task_tbl SET status='Left Message or Sent Email' WHERE status='In Progress';
-- UPDATE task_tbl SET status='Repair Scheduled' WHERE status='In Progress';
UPDATE task_tbl SET status='Confirmed Complete' WHERE status='Confirmed Completed';
-- UPDATE task_tbl SET status='Cancelled' WHERE status='Cancelled';

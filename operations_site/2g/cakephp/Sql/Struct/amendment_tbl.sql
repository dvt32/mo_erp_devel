CREATE TABLE "public"."amendment_tbl" ( 
	"id" INTEGER DEFAULT nextval('amendment_tbl_id_seq'::regclass) NOT NULL UNIQUE, 
	"license_amendment_id" INTEGER, 
	"amendment" TEXT COLLATE "pg_catalog"."default", 
	"modifier_id" INTEGER, 
	"modified" TIMESTAMP WITH TIME ZONE, 
	"created" TIMESTAMP WITH TIME ZONE, 
	"ds_envelope_id" CHARACTER VARYING( 255 ) COLLATE "pg_catalog"."default",
	PRIMARY KEY ( "id" )
);
CREATE INDEX "index_id4" ON "public"."amendment_tbl" USING btree( "id" ASC NULLS LAST );

/*
Navicat PGSQL Data Transfer

Source Server         : dev_microoffice
Source Server Version : 90109
Source Host           : 192.168.1.103:5432
Source Database       : dev_microoffice
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90109
File Encoding         : 65001

Date: 2014-02-24 20:19:11
*/


-- ----------------------------
-- Sequence for "public"."billing_memo_template_seq"
-- ----------------------------
CREATE SEQUENCE "public"."billing_memo_template_seq" START 17;


-- ----------------------------
-- Table structure for "public"."billing_memo_template_tbl"
-- ----------------------------
DROP TABLE "public"."billing_memo_template_tbl";
CREATE TABLE "public"."billing_memo_template_tbl" (
"id" int4 DEFAULT nextval('billing_memo_template_seq'::regclass) NOT NULL,
"name" varchar(120) NOT NULL,
"is_recurring" bool DEFAULT true NOT NULL,
"body" text
)
WITH (OIDS=FALSE)

;


-- ----------------------------
-- Records of billing_memo_template_tbl
-- ----------------------------
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('1', 'Telephone Line', 't', 'ADD telephone line
number = ___-___-____');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('2', 'Internet Line', 't', 'ADD internet line
ip = __.__.__.__');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('3', 'Desk / Person', 't', 'ADD desk/person
desk-id = ___');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('4', 'Private E-Fax Line', 't', 'ADD private e-fax line:
number = ___-___-____');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('5', 'Physical Phone (one-time fee)', 'f', 'ADD physical phone installation fee');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('6', 'Voicemail Transcription', 't', 'ADD voicemail transcription service
number = ___-___-____');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('7', '212 Area Code Phone Number (one-time fee)', 'f', 'ADD 212 area code phone number fee');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('8', 'IP Address', 't', 'ADD ip address: 
ip = __.__.__.__');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('9', 'BGP-Backed IP Address', 't', 'ADD bgp-backed ip address: 
ip = __.__.__.__');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('10', 'Telephone Rental', 't', 'ADD telephone rental
number = ___-___-____');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('11', 'Access Key Card', 't', 'ADD access key card:
type=______
number=_____
date-issued=__/__/____');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('12', 'Filing Cabinet Key (one-time fee)', 'f', 'ADD cut filing cabinet key fee');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('13', 'Mail Forwarding', 't', 'ADD mail forwarding service');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('14', 'Premium Conference Room Hours (one-time fee)', 'f', 'ADD premium conference room hours');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('15', 'Bounced Payment Processing Fee (one-time fee)', 'f', 'ADD bounced payment processing fee');
INSERT INTO "public"."billing_memo_template_tbl" VALUES ('16', 'Overages in Conference Room Hours (one-time fee)', 'f', 'ADD conference room hours overage fee
hours=__
minutes=__');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table "public"."billing_memo_template_tbl"
-- ----------------------------
ALTER TABLE "public"."billing_memo_template_tbl" ADD PRIMARY KEY ("id");

CREATE TABLE "public"."enum_amendment_template_tbl" ( 
	"id" INTEGER DEFAULT nextval('enum_amendment_tembplate_tbl_id_seq'::regclass) NOT NULL UNIQUE, 
	"name" CHARACTER VARYING COLLATE "pg_catalog"."default", 
	"amendment" TEXT COLLATE "pg_catalog"."default", 
	"created" TIMESTAMP WITH TIME ZONE, 
	"modified" TIMESTAMP WITH TIME ZONE, 
	"modifier_id" INTEGER, 
	"is_active" BOOLEAN,
	PRIMARY KEY ( "id" )
);
CREATE INDEX "index_id3" ON "public"."enum_amendment_template_tbl" USING btree( "id" ASC NULLS LAST );

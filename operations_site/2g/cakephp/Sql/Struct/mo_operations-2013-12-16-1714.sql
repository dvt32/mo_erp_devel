--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: concat2(text, text); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION concat2(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN $1 IS NULL OR $1 = '' THEN $2
            WHEN $2 IS NULL OR $2 = '' THEN $1
            ELSE $1 || ' / ' || $2
            END;
$_$;


ALTER FUNCTION public.concat2(text, text) OWNER TO wchao;

--
-- Name: generate_public_id(integer); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION generate_public_id(length integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
  DECLARE
    public_id varchar;
  BEGIN
    public_id := timeofday() || ' ';
    FOR i IN 1..length LOOP
      public_id := public_id || random_alphanum_char();
    END LOOP;
    RETURN public_id;
  END;
$$;


ALTER FUNCTION public.generate_public_id(length integer) OWNER TO wchao;

--
-- Name: generate_random(integer); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION generate_random(length integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
	result text := '';
	i integer := 0;
BEGIN
	if length < 0 then raise exception 'Given length cannot be less than 0';
	end if;
	for i in 1..length loop
	result := result || chars[1+random()*(array_length(chars, 1)-1)];
end loop;
return result;
END;
$$;


ALTER FUNCTION public.generate_random(length integer) OWNER TO wchao;

--
-- Name: msaccessbool(boolean, integer); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION msaccessbool(boolean, integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
BEGIN
  IF $1 ISNULL THEN
    RETURN NULL;
  END IF;
  IF $1 IS TRUE THEN
    IF $2 <> 0 THEN
      RETURN TRUE;
    END IF;
  ELSE
    IF $2 = 0 THEN
      RETURN TRUE;
    END IF;
  END IF;
  RETURN FALSE;
END;
$_$;


ALTER FUNCTION public.msaccessbool(boolean, integer) OWNER TO wchao;

--
-- Name: msaccessrandomnum(); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION msaccessrandomnum() RETURNS bigint
    LANGUAGE sql STRICT
    AS $$
  select round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision))::bigint
$$;


ALTER FUNCTION public.msaccessrandomnum() OWNER TO wchao;

--
-- Name: plpgsql_call_handler(); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler
    LANGUAGE c
    AS '$libdir/plpgsql', 'plpgsql_call_handler';


ALTER FUNCTION public.plpgsql_call_handler() OWNER TO wchao;

--
-- Name: plpgsql_validator(oid); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION plpgsql_validator(oid) RETURNS void
    LANGUAGE c
    AS '$libdir/plpgsql', 'plpgsql_validator';


ALTER FUNCTION public.plpgsql_validator(oid) OWNER TO wchao;

--
-- Name: random_alphanum_char(); Type: FUNCTION; Schema: public; Owner: wchao
--

CREATE FUNCTION random_alphanum_char() RETURNS character
    LANGUAGE plpgsql
    AS $$
  DECLARE
    universe_size CONSTANT integer := 62;
    random_num integer;
    random_char char;
  BEGIN
    random_num := round(random() * (universe_size - 1));
    IF random_num < 10 THEN
      random_char := chr(ascii('0') + random_num);
    ELSIF random_num >= 10 AND random_num < 36 THEN
      random_char := chr(ascii('A') + (random_num - 10));
    ELSIF random_num >= 36 AND random_num < 62 THEN
      random_char := chr(ascii('a') + (random_num - 36));
    ELSE
      RAISE EXCEPTION 'random number out of bounds (%)', random_num;
    END IF;
    RETURN random_char;
  END;
$$;


ALTER FUNCTION public.random_alphanum_char() OWNER TO wchao;

--
-- Name: concatenate(text); Type: AGGREGATE; Schema: public; Owner: wchao
--

CREATE AGGREGATE concatenate(text) (
    SFUNC = concat2,
    STYPE = text,
    INITCOND = ''
);


ALTER AGGREGATE public.concatenate(text) OWNER TO wchao;

--
-- Name: =; Type: OPERATOR; Schema: public; Owner: wchao
--

CREATE OPERATOR = (
    PROCEDURE = msaccessbool,
    LEFTARG = boolean,
    RIGHTARG = integer,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = eqsel,
    JOIN = eqjoinsel
);


ALTER OPERATOR public.= (boolean, integer) OWNER TO wchao;

--
-- Name: access_level_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE access_level_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.access_level_seq OWNER TO wchao;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: access_level_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE access_level_tbl (
    id integer DEFAULT nextval(('access_level_seq'::text)::regclass) NOT NULL,
    name character varying(32) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    note text,
    floor_id integer NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.access_level_tbl OWNER TO pithadmin;

--
-- Name: account_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE account_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_seq OWNER TO wchao;

--
-- Name: cust_acct_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE cust_acct_tbl (
    id integer DEFAULT nextval(('account_seq'::text)::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    primary_contact_cust_pers_id integer,
    is_active boolean DEFAULT true NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    external_name character varying(64),
    description character varying(255),
    web_site character varying(255),
    visible boolean NOT NULL,
    type character varying(32) NOT NULL,
    note text,
    tax_id character varying(32),
    service_id integer,
    preferred_payment_method character varying(24),
    room_minutes integer DEFAULT 0 NOT NULL,
    primary_floor_id integer NOT NULL,
    pkg_notify_email text DEFAULT ''::text NOT NULL,
    logo_url character varying(255),
    status character varying(20),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.cust_acct_tbl OWNER TO pithadmin;

--
-- Name: account_vw; Type: VIEW; Schema: public; Owner: wchao
--

CREATE VIEW account_vw AS
    SELECT cust_acct_tbl.id, 'customer'::character varying(16) AS type, cust_acct_tbl.primary_floor_id, cust_acct_tbl.name, cust_acct_tbl.primary_contact_cust_pers_id AS primary_contact_person_id, cust_acct_tbl.is_active FROM cust_acct_tbl UNION ALL SELECT 1 AS id, 'corporate'::character varying(16) AS type, 2 AS primary_floor_id, 'Micro Office Solutions'::character varying AS name, 41 AS primary_contact_person_id, true AS is_active;


ALTER TABLE public.account_vw OWNER TO wchao;

--
-- Name: acct_reconcile_check_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE acct_reconcile_check_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.acct_reconcile_check_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: acct_reconcile_check_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE acct_reconcile_check_tbl (
    id integer DEFAULT nextval('acct_reconcile_check_seq'::regclass) NOT NULL,
    bank_number character(3) NOT NULL,
    account_number character varying(10) NOT NULL,
    is_void boolean NOT NULL,
    check_number character varying(10) NOT NULL,
    check_amount numeric(10,2) NOT NULL,
    issue_date_of_check date NOT NULL,
    additional_data character(15),
    processed_dt timestamp(0) with time zone,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.acct_reconcile_check_tbl OWNER TO pithadmin;

--
-- Name: ach_debit_txn_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ach_debit_txn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ach_debit_txn_seq OWNER TO wchao;

--
-- Name: ach_debit_txn_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ach_debit_txn_tbl (
    id integer DEFAULT nextval('ach_debit_txn_seq'::regclass) NOT NULL,
    cust_acct_id integer,
    amount numeric(8,2) NOT NULL,
    customer_name character varying(22) NOT NULL,
    customer_bank_id character(9) NOT NULL,
    customer_bank_acct_num character varying(17) NOT NULL,
    customer_bank_acct_type character(1) NOT NULL,
    company_bank_acct_num character varying(17) NOT NULL,
    company_bank_acct_type character(1) NOT NULL,
    start_date date,
    prenote_date date,
    last_collection_date date,
    create_prenote boolean NOT NULL,
    hold_transfer boolean NOT NULL,
    queued_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    processed_dt timestamp(0) with time zone,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    cancelled boolean DEFAULT false NOT NULL,
    electronic_check_id integer,
    recv_payment_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ach_debit_txn_tbl OWNER TO pithadmin;

--
-- Name: alias_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE alias_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alias_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: alias_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE alias_tbl (
    id integer DEFAULT nextval(('alias_seq'::text)::regclass) NOT NULL,
    account_id integer NOT NULL,
    name character varying(128) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.alias_tbl OWNER TO pithadmin;

--
-- Name: amendment_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE amendment_tbl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.amendment_tbl_id_seq OWNER TO pithadmin;

SET default_with_oids = false;

--
-- Name: amendment_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE amendment_tbl (
    agreement text,
    created timestamp with time zone,
    ds_envelope_id character varying(255),
    id integer DEFAULT nextval('amendment_tbl_id_seq'::regclass) NOT NULL,
    license_agreement_id integer,
    modified timestamp with time zone,
    modifier_id integer,
    type character varying(45)
);


ALTER TABLE public.amendment_tbl OWNER TO pithadmin;

--
-- Name: bank_account_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE bank_account_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_account_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: bank_account_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE bank_account_tbl (
    id integer DEFAULT nextval(('bank_account_seq'::text)::regclass) NOT NULL,
    account_id integer NOT NULL,
    depository_name character varying(48) NOT NULL,
    address_1 character varying(128) NOT NULL,
    address_2 character varying(128),
    city character varying(48) NOT NULL,
    state character varying(64) NOT NULL,
    zip_code character varying(16) NOT NULL,
    bank_account_type character varying(16) NOT NULL,
    bank_routing_number character(9) NOT NULL,
    bank_account_number character varying(17) NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    bank_account_customer_name character varying(128) NOT NULL,
    bank_account_ownership_type character varying(16) NOT NULL,
    bank_account_nickname character varying(32),
    is_primary boolean DEFAULT false NOT NULL,
    country character varying(64) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    is_active boolean
);


ALTER TABLE public.bank_account_tbl OWNER TO pithadmin;

--
-- Name: bing_query_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE bing_query_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bing_query_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: bing_query_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE bing_query_tbl (
    id integer DEFAULT nextval('bing_query_seq'::regclass) NOT NULL,
    query text NOT NULL,
    lr character varying(128),
    note text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.bing_query_tbl OWNER TO pithadmin;

--
-- Name: bing_search_result_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE bing_search_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bing_search_result_seq OWNER TO wchao;

--
-- Name: bing_search_result_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE bing_search_result_tbl (
    id integer DEFAULT nextval('bing_search_result_seq'::regclass) NOT NULL,
    bing_query_id integer NOT NULL,
    search_rank integer NOT NULL,
    content text,
    title text,
    title_no_formatting text,
    url text NOT NULL,
    unescaped_url text NOT NULL,
    visible_url text,
    cache_url text,
    note text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.bing_search_result_tbl OWNER TO pithadmin;

--
-- Name: broker_referral_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE broker_referral_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.broker_referral_seq OWNER TO wchao;

--
-- Name: broker_referral_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE broker_referral_tbl (
    id integer DEFAULT nextval('broker_referral_seq'::regclass) NOT NULL,
    broker_salutation character varying(16),
    broker_first_name character varying(32) NOT NULL,
    broker_middle_name character varying(48),
    broker_last_name character varying(48) NOT NULL,
    broker_suffix character varying(32),
    broker_title character varying(80),
    broker_company character varying(128),
    broker_address_1 character varying(255),
    broker_address_2 character varying(255),
    broker_city character varying(64),
    broker_state character varying(64),
    broker_zip_code character varying(32),
    broker_country character varying(96),
    broker_work_phone_number character varying(48),
    broker_cell_phone_number character varying(48),
    broker_work_fax_number character varying(48),
    broker_email_address character varying(255),
    type_of_business character varying(255),
    intended_use character varying(255),
    amount_of_space character varying(255),
    rental_start_date date,
    duration_of_rental character varying(64),
    desired_location character varying(32),
    client_salutation character varying(16),
    client_first_name character varying(32),
    client_middle_name character varying(48),
    client_last_name character varying(48),
    client_suffix character varying(32),
    client_title character varying(80),
    client_company character varying(128),
    client_address_1 character varying(255),
    client_address_2 character varying(255),
    client_city character varying(64),
    client_state character varying(64),
    client_zip_code character varying(32),
    client_country character varying(96),
    client_work_phone_number character varying(48),
    client_cell_phone_number character varying(48),
    client_work_fax_number character varying(48),
    client_email_address character varying(255),
    marketing_source character varying(255),
    note text,
    referrer_url text,
    initial_page_url character varying(255),
    remote_host character varying(255),
    remote_addr character varying(15),
    user_agent character varying(255),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    domain_name character varying(80),
    x_forwarded_for text DEFAULT ''::text NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.broker_referral_tbl OWNER TO pithadmin;

--
-- Name: building_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE building_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.building_seq OWNER TO wchao;

--
-- Name: building_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE building_tbl (
    id integer DEFAULT nextval('building_seq'::regclass) NOT NULL,
    address_1 character varying(128) NOT NULL,
    address_2 character varying(128),
    city character varying(64) NOT NULL,
    state character varying(32) NOT NULL,
    zip_code character varying(16) NOT NULL,
    country character varying(64) NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    short_global_label character varying(255) NOT NULL,
    longitude double precision,
    latitude double precision,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    type character varying(60),
    server1 character varying(60),
    server2 character varying(60),
    licensor_entity character varying(10)
);


ALTER TABLE public.building_tbl OWNER TO pithadmin;

--
-- Name: camera_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE camera_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.camera_seq OWNER TO wchao;

--
-- Name: camera_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE camera_tbl (
    id integer DEFAULT nextval('camera_seq'::regclass) NOT NULL,
    mac_address character varying(12) NOT NULL,
    model character varying(255),
    version character varying(8),
    lan_ip_addr character varying(15),
    lan_port integer,
    username_admin character varying(32),
    password_admin character varying(32),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    floor_id integer NOT NULL,
    description character varying(255),
    last_recorded_date date,
    do_date_monitoring boolean DEFAULT true NOT NULL,
    storage_server character varying(255),
    storage_base_path character varying(255),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.camera_tbl OWNER TO pithadmin;

--
-- Name: company_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE company_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: company_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE company_tbl (
    id integer DEFAULT nextval(('company_seq'::text)::regclass) NOT NULL,
    name character varying(64) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    short_name character varying(64) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.company_tbl OWNER TO pithadmin;

--
-- Name: country_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE country_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: country_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE country_tbl (
    id integer DEFAULT nextval('country_seq'::regclass) NOT NULL,
    name character varying(80) NOT NULL,
    iso_2 character(2) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    iso_3 character(3),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.country_tbl OWNER TO pithadmin;

--
-- Name: COLUMN country_tbl.iso_2; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN country_tbl.iso_2 IS 'ISO 3166-1 alpha-2 code';


--
-- Name: COLUMN country_tbl.iso_3; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN country_tbl.iso_3 IS 'ISO 3166-1 alpha-3 code';


--
-- Name: credit_card_payment_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE credit_card_payment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_card_payment_seq OWNER TO wchao;

--
-- Name: credit_card_payment_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE credit_card_payment_tbl (
    id integer DEFAULT nextval('credit_card_payment_seq'::regclass) NOT NULL,
    cust_acct_id integer,
    card_number character varying(24) NOT NULL,
    card_expiration_month character(2) NOT NULL,
    card_expiration_year character(4) NOT NULL,
    card_security_code character varying(4),
    billing_name character varying(64) NOT NULL,
    billing_company character varying(64),
    billing_address_num character varying(16),
    billing_zip_code character varying(16) NOT NULL,
    billing_address_1 character varying(128) NOT NULL,
    billing_address_2 character varying(128),
    billing_city character varying(48) NOT NULL,
    billing_state character varying(64) NOT NULL,
    billing_country character varying(64) NOT NULL,
    billing_phone_number character varying(32),
    billing_fax_number character varying(32),
    billing_email_address character varying(64),
    subtotal numeric(7,2) NOT NULL,
    charge_date date NOT NULL,
    invoice_number character varying(128),
    note text,
    processed boolean DEFAULT false NOT NULL,
    cancelled boolean DEFAULT false NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    convenience_fee numeric(7,2) NOT NULL,
    remote_addr character varying(15),
    remote_host character varying(255),
    transaction_json text,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.credit_card_payment_tbl OWNER TO pithadmin;

--
-- Name: credit_card_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE credit_card_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_card_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: credit_card_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE credit_card_tbl (
    id integer DEFAULT nextval(('credit_card_seq'::text)::regclass) NOT NULL,
    account_id integer NOT NULL,
    card_number character varying(16) NOT NULL,
    card_security_code character varying(4),
    billing_name character varying(64) NOT NULL,
    billing_address_1 character varying(128) NOT NULL,
    billing_address_2 character varying(128),
    billing_city character varying(48) NOT NULL,
    billing_state character varying(48) NOT NULL,
    billing_zip_code character varying(16) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    billing_company character varying(64),
    billing_country character varying(64) NOT NULL,
    card_expiration_month character(2) NOT NULL,
    card_expiration_year character(4) NOT NULL,
    billing_phone_number character varying(32),
    billing_fax_number character varying(32),
    billing_email_address character varying(64),
    is_primary boolean DEFAULT false NOT NULL,
    card_nickname character varying(32),
    billing_address_num character varying(16),
    person_id integer NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    is_active boolean
);


ALTER TABLE public.credit_card_tbl OWNER TO pithadmin;

--
-- Name: credit_card_txn_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE credit_card_txn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_card_txn_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: credit_card_txn_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE credit_card_txn_tbl (
    id integer DEFAULT nextval('credit_card_txn_seq'::regclass) NOT NULL,
    cust_acct_id integer,
    card_number character varying(16) NOT NULL,
    card_expiration_month character(2) NOT NULL,
    card_expiration_year character(4) NOT NULL,
    card_security_code character varying(4),
    billing_name character varying(64) NOT NULL,
    billing_company character varying(64),
    billing_address_num character varying(16),
    billing_zip_code character varying(16) NOT NULL,
    billing_address_1 character varying(128) NOT NULL,
    billing_address_2 character varying(128),
    billing_city character varying(48) NOT NULL,
    billing_state character varying(64) NOT NULL,
    billing_country character varying(64) NOT NULL,
    billing_phone_number character varying(32),
    billing_fax_number character varying(32),
    billing_email_address character varying(64),
    total_charge numeric(7,2) NOT NULL,
    queued_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    processed_dt timestamp(0) with time zone,
    response_avs character(4),
    response_order_num character varying(32),
    response_error character varying(128),
    response_approved character varying(8),
    response_code character varying(48),
    response_message character varying(128),
    response_txn_dt timestamp(0) with time zone,
    response_reference_num character varying(16),
    response_tdate character varying(32),
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ip_address character varying(15),
    credit_card_payment_id integer,
    recv_payment_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.credit_card_txn_tbl OWNER TO pithadmin;

SET default_with_oids = true;

--
-- Name: cust_pers_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE cust_pers_tbl (
    id integer DEFAULT nextval(('person_seq'::text)::regclass) NOT NULL,
    cust_acct_id integer,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(32),
    last_name character varying(48) NOT NULL,
    nickname character varying(32),
    email character varying(128),
    work_phone_number character varying(32),
    home_phone_number character varying(32),
    cell_phone_number character varying(32),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    note text,
    register_with_building boolean DEFAULT true NOT NULL,
    other_phone_number character varying(32),
    directory_1 character varying(8),
    directory_2 character varying(8),
    directory_3 character varying(8),
    title character varying(80),
    work_fax_number character varying(32),
    work_address_1 character varying(128),
    work_address_2 character varying(128),
    work_city character varying(64),
    work_state character varying(32),
    work_zip_code character varying(16),
    work_country character varying(64),
    home_address_1 character varying(128),
    home_address_2 character varying(128),
    home_city character varying(64),
    home_state character varying(32),
    home_zip_code character varying(16),
    home_country character varying(64),
    birthday date,
    company character varying(128),
    visibility character varying(12) DEFAULT 'visible'::character varying NOT NULL,
    suffix character varying(32),
    password character varying(32),
    email2 character varying(128),
    email3 character varying(128),
    inbound_fax_alert_cutoff_dt timestamp(0) without time zone,
    enable_cc_pay boolean DEFAULT true,
    receive_email boolean DEFAULT true NOT NULL,
    fax_sender_name character varying(255),
    user_id integer,
    profile_picture_url character varying(255),
    hash character(60),
    salt character(29),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.cust_pers_tbl OWNER TO pithadmin;

--
-- Name: COLUMN cust_pers_tbl.receive_email; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN cust_pers_tbl.receive_email IS 'Used so clients can opt out from receiving email notifications.';


--
-- Name: deposit_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE deposit_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.deposit_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: deposit_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE deposit_tbl (
    id integer DEFAULT nextval('deposit_seq'::regclass) NOT NULL,
    employee_id integer NOT NULL,
    deposit_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    bank_account_number character varying(24) NOT NULL,
    bank_routing_number character varying(9) NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    bank_name character varying(64) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.deposit_tbl OWNER TO pithadmin;

--
-- Name: desk_assignment_history_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE desk_assignment_history_tbl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.desk_assignment_history_tbl_id_seq OWNER TO pithadmin;

--
-- Name: desk_assignment_history_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE desk_assignment_history_tbl (
    desk_id integer NOT NULL,
    cust_acct_id integer,
    cust_pers_id integer,
    start_date date DEFAULT ('now'::text)::date NOT NULL,
    end_date date,
    current_price numeric(8,2),
    note text,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    id integer DEFAULT nextval('desk_assignment_history_tbl_id_seq'::regclass) NOT NULL
);


ALTER TABLE public.desk_assignment_history_tbl OWNER TO pithadmin;

--
-- Name: desk_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE desk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.desk_seq OWNER TO wchao;

--
-- Name: desk_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE desk_tbl (
    id integer DEFAULT nextval('desk_seq'::regclass) NOT NULL,
    floor_id integer NOT NULL,
    assigned_number integer NOT NULL,
    account_id integer,
    num_people integer NOT NULL,
    is_enclosed boolean DEFAULT false NOT NULL,
    list_price numeric(8,2) DEFAULT 495 NOT NULL,
    when_available date,
    bounding_polygon path NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    waiting_account integer,
    current_price numeric(8,2) DEFAULT 0 NOT NULL,
    start_date date,
    end_date date,
    internet_telephone_revenue numeric(8,2) DEFAULT 0 NOT NULL,
    increase_date date,
    cust_pers_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.desk_tbl OWNER TO pithadmin;

--
-- Name: document_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE document_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.document_seq OWNER TO wchao;

--
-- Name: fax_in_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE fax_in_tbl (
    id integer DEFAULT nextval('document_seq'::regclass) NOT NULL,
    account_id integer,
    person_id integer,
    file_id integer NOT NULL,
    file_size integer NOT NULL,
    file_timestamp timestamp(0) without time zone DEFAULT now() NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    filename character varying(255) NOT NULL,
    fax_number character varying(10) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.fax_in_tbl OWNER TO pithadmin;

--
-- Name: scan_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE scan_tbl (
    id integer DEFAULT nextval('document_seq'::regclass) NOT NULL,
    account_id integer,
    person_id integer,
    file_id integer NOT NULL,
    scanner_name character varying(255),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    file_size integer NOT NULL,
    file_timestamp timestamp(0) without time zone NOT NULL,
    filename character varying(255) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.scan_tbl OWNER TO pithadmin;

--
-- Name: document_vw; Type: VIEW; Schema: public; Owner: wchao
--

CREATE VIEW document_vw AS
    SELECT scan_tbl.id, 'scan'::character varying(16) AS type, scan_tbl.account_id, scan_tbl.person_id, scan_tbl.file_id, scan_tbl.filename, scan_tbl.file_size, scan_tbl.file_timestamp, scan_tbl.note FROM scan_tbl UNION ALL SELECT fax_in_tbl.id, 'fax'::character varying(16) AS type, fax_in_tbl.account_id, fax_in_tbl.person_id, fax_in_tbl.file_id, fax_in_tbl.filename, fax_in_tbl.file_size, fax_in_tbl.file_timestamp, fax_in_tbl.note FROM fax_in_tbl;


ALTER TABLE public.document_vw OWNER TO wchao;

--
-- Name: electronic_check_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE electronic_check_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.electronic_check_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: electronic_check_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE electronic_check_tbl (
    id integer DEFAULT nextval(('electronic_check_seq'::text)::regclass) NOT NULL,
    cust_acct_id integer NOT NULL,
    bank_account_name character varying(22) NOT NULL,
    bank_account_type character varying(8) NOT NULL,
    routing_number character varying(9) NOT NULL,
    account_number character varying(17) NOT NULL,
    amount numeric(8,2) NOT NULL,
    debit_date date NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    invoice_number character varying(128),
    processed boolean DEFAULT false NOT NULL,
    cancelled boolean DEFAULT false NOT NULL,
    remote_addr character varying(15),
    remote_host character varying(255),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.electronic_check_tbl OWNER TO pithadmin;

--
-- Name: email_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE email_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: email_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE email_tbl (
    id integer DEFAULT nextval('email_seq'::regclass) NOT NULL,
    account_id integer,
    from_hdr character varying(255) NOT NULL,
    to_hdr character varying(255) NOT NULL,
    date_hdr timestamp(0) with time zone,
    subject_hdr character varying(255) NOT NULL,
    message_body text NOT NULL,
    notes text,
    sent_dt timestamp(0) with time zone,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    process_id integer,
    investor_id integer,
    employee_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.email_tbl OWNER TO pithadmin;

--
-- Name: employee_absence_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE employee_absence_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employee_absence_seq OWNER TO wchao;

--
-- Name: employee_absence_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE employee_absence_tbl (
    id integer DEFAULT nextval('employee_absence_seq'::regclass) NOT NULL,
    employee_id integer NOT NULL,
    absence_date date NOT NULL,
    start_time time(0) with time zone,
    end_time time(0) with time zone,
    day_type character varying(8) NOT NULL,
    status character varying(16) DEFAULT 'pending'::character varying NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.employee_absence_tbl OWNER TO pithadmin;

--
-- Name: COLUMN employee_absence_tbl.day_type; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN employee_absence_tbl.day_type IS 'vacation | sick | unpaid';


--
-- Name: COLUMN employee_absence_tbl.status; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN employee_absence_tbl.status IS 'pending | approved | denied';


--
-- Name: employee_bank_account_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE employee_bank_account_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employee_bank_account_seq OWNER TO wchao;

--
-- Name: employee_bank_account_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE employee_bank_account_tbl (
    id integer DEFAULT nextval('employee_bank_account_seq'::regclass) NOT NULL,
    employee_id integer NOT NULL,
    depository_name character varying(48) NOT NULL,
    address_1 character varying(128),
    address_2 character varying(128),
    city character varying(64),
    state character varying(32),
    zip_code character varying(16),
    country character varying(64),
    account_type character varying(16) NOT NULL,
    routing_number character(9) NOT NULL,
    account_number character varying(17) NOT NULL,
    owner_name character varying(128) NOT NULL,
    owner_type character varying(16) NOT NULL,
    account_nickname character varying(32),
    is_active boolean DEFAULT true NOT NULL,
    is_primary boolean DEFAULT false NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.employee_bank_account_tbl OWNER TO pithadmin;

--
-- Name: person_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE person_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_seq OWNER TO wchao;

--
-- Name: employee_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE employee_tbl (
    id integer DEFAULT nextval('person_seq'::regclass) NOT NULL,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(32),
    last_name character varying(48) NOT NULL,
    nickname character varying(32),
    email character varying(64),
    work_phone_number character varying(32),
    work_fax_number character varying(32),
    home_phone_number character varying(32),
    cell_phone_number character varying(32),
    other_phone_number character varying(32),
    title character varying(80),
    company character varying(128),
    work_address_1 character varying(128),
    work_address_2 character varying(128),
    work_city character varying(64),
    work_state character varying(32),
    work_zip_code character varying(16),
    work_country character varying(64),
    home_address_1 character varying(128),
    home_address_2 character varying(128),
    home_city character varying(64),
    home_state character varying(32),
    home_zip_code character varying(16),
    home_country character varying(64),
    birthday date,
    tax_id character varying(16),
    is_active boolean DEFAULT true NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    email2 character varying(64),
    email3 character varying(64),
    type character varying(16) DEFAULT 'employee'::character varying NOT NULL,
    in_directory boolean DEFAULT true NOT NULL,
    password character varying(32),
    suffix character varying(32),
    allowed_ip_addr character varying(15),
    phone_pin character varying(16) DEFAULT ''::character varying NOT NULL,
    vacation_days integer DEFAULT 5 NOT NULL,
    has_assigned_task boolean DEFAULT false NOT NULL,
    hash character(60),
    salt character(29),
    reload_roles boolean DEFAULT false NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    windows_logon_name character varying(20) DEFAULT ''::character varying NOT NULL,
    windows_domain_name character varying(15) DEFAULT ''::character varying NOT NULL,
    windows_upn_suffix character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.employee_tbl OWNER TO pithadmin;

--
-- Name: employee_public_vw; Type: VIEW; Schema: public; Owner: pithadmin
--

CREATE VIEW employee_public_vw AS
    SELECT employee_tbl.id, employee_tbl.salutation, employee_tbl.first_name, employee_tbl.middle_name, employee_tbl.last_name, employee_tbl.nickname, employee_tbl.email, employee_tbl.work_phone_number, employee_tbl.work_fax_number, ''::character varying(32) AS home_phone_number, ''::character varying(32) AS cell_phone_number, ''::character varying(32) AS other_phone_number, employee_tbl.title, employee_tbl.company, employee_tbl.work_address_1, employee_tbl.work_address_2, employee_tbl.work_city, employee_tbl.work_state, employee_tbl.work_zip_code, employee_tbl.work_country, ''::character varying(128) AS home_address_1, ''::character varying(128) AS home_address_2, ''::character varying(64) AS home_city, ''::character varying(32) AS home_state, ''::character varying(16) AS home_zip_code, ''::character varying(64) AS home_country, NULL::date AS birthday, ''::character varying(16) AS tax_id, employee_tbl.is_active, employee_tbl.note, employee_tbl.ms_access_creation_dt, employee_tbl.ms_access_random_num, employee_tbl.creation_dt, ''::character varying(64) AS email2, ''::character varying(64) AS email3, employee_tbl.type, employee_tbl.in_directory, ''::character varying(32) AS password, employee_tbl.suffix, employee_tbl.allowed_ip_addr, ''::character varying(16) AS phone_pin, employee_tbl.vacation_days, employee_tbl.has_assigned_task FROM employee_tbl;


ALTER TABLE public.employee_public_vw OWNER TO pithadmin;

--
-- Name: employee_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE employee_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employee_seq OWNER TO wchao;

--
-- Name: employee_time_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE employee_time_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employee_time_seq OWNER TO wchao;

--
-- Name: employee_time_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE employee_time_tbl (
    id integer DEFAULT nextval('employee_time_seq'::regclass) NOT NULL,
    employee_id integer NOT NULL,
    time_in timestamp(0) with time zone NOT NULL,
    time_out timestamp(0) with time zone,
    break_min integer,
    clock_in_ip character varying(15) DEFAULT ''::character varying NOT NULL,
    clock_out_ip character varying(15) DEFAULT ''::character varying NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    is_manual_clock_out boolean DEFAULT false NOT NULL,
    clock_in_caller_id_number character varying(16) DEFAULT ''::character varying NOT NULL,
    clock_out_caller_id_number character varying(16) DEFAULT ''::character varying NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.employee_time_tbl OWNER TO pithadmin;

--
-- Name: enum_agreement_template_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE enum_agreement_template_tbl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_agreement_template_tbl_id_seq OWNER TO pithadmin;

--
-- Name: enum_agreement_template_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_agreement_template_tbl (
    building_id integer,
    created time with time zone,
    id integer DEFAULT nextval('enum_agreement_template_tbl_id_seq'::regclass) NOT NULL,
    modified timestamp with time zone,
    modifier_id integer,
    name character varying,
    template text,
    is_active boolean
);


ALTER TABLE public.enum_agreement_template_tbl OWNER TO pithadmin;

--
-- Name: enum_amendment_tembplate_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE enum_amendment_tembplate_tbl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_amendment_tembplate_tbl_id_seq OWNER TO pithadmin;

--
-- Name: enum_amendment_template_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_amendment_template_tbl (
    id integer DEFAULT nextval('enum_amendment_tembplate_tbl_id_seq'::regclass) NOT NULL,
    name character varying,
    amendment text,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    is_active boolean
);


ALTER TABLE public.enum_amendment_template_tbl OWNER TO pithadmin;

--
-- Name: enum_check_action_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE enum_check_action_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_check_action_seq OWNER TO wchao;

--
-- Name: enum_check_action_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_check_action_tbl (
    id integer DEFAULT nextval('enum_check_action_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.enum_check_action_tbl OWNER TO pithadmin;

--
-- Name: eum_intellim_group_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE eum_intellim_group_tbl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eum_intellim_group_tbl_id_seq OWNER TO pithadmin;

--
-- Name: enum_intellim_group_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_intellim_group_tbl (
    id integer DEFAULT nextval('eum_intellim_group_tbl_id_seq'::regclass) NOT NULL,
    name character varying(60),
    intellim_id integer NOT NULL,
    zone_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.enum_intellim_group_tbl OWNER TO pithadmin;

--
-- Name: enum_payment_destination_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE enum_payment_destination_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_payment_destination_seq OWNER TO wchao;

--
-- Name: enum_payment_destination_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_payment_destination_tbl (
    id integer DEFAULT nextval('enum_payment_destination_seq'::regclass) NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.enum_payment_destination_tbl OWNER TO pithadmin;

--
-- Name: enum_proximity_card_change_cause_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE enum_proximity_card_change_cause_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_proximity_card_change_cause_seq OWNER TO wchao;

--
-- Name: enum_proximity_card_change_cause_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_proximity_card_change_cause_tbl (
    id integer DEFAULT nextval('enum_proximity_card_change_cause_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.enum_proximity_card_change_cause_tbl OWNER TO pithadmin;

--
-- Name: enum_sales_stage_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE enum_sales_stage_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_sales_stage_seq OWNER TO wchao;

--
-- Name: enum_sales_stage_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_sales_stage_tbl (
    id integer DEFAULT nextval('enum_sales_stage_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.enum_sales_stage_tbl OWNER TO pithadmin;

--
-- Name: enum_sales_tag_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE enum_sales_tag_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_sales_tag_seq OWNER TO wchao;

--
-- Name: enum_sales_tag_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_sales_tag_tbl (
    id integer DEFAULT nextval('enum_sales_tag_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.enum_sales_tag_tbl OWNER TO pithadmin;

--
-- Name: enum_search_engine_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE enum_search_engine_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enum_search_engine_seq OWNER TO wchao;

--
-- Name: enum_search_engine_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE enum_search_engine_tbl (
    id integer DEFAULT nextval('enum_search_engine_seq'::regclass) NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.enum_search_engine_tbl OWNER TO pithadmin;

--
-- Name: event_seq; Type: SEQUENCE; Schema: public; Owner: czhang
--

CREATE SEQUENCE event_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.event_seq OWNER TO czhang;

--
-- Name: event_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE event_tbl (
    created time with time zone,
    description text,
    event_page_url character varying(255),
    id integer DEFAULT nextval('event_seq'::regclass) NOT NULL,
    images_url character varying(255),
    modified time with time zone,
    name character varying(255),
    modifier_id integer
);


ALTER TABLE public.event_tbl OWNER TO pithadmin;

--
-- Name: expense_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE expense_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expense_seq OWNER TO wchao;

--
-- Name: expense_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE expense_tbl (
    id integer DEFAULT nextval('expense_seq'::regclass) NOT NULL,
    account_id integer NOT NULL,
    person_id integer NOT NULL,
    purchase_date date NOT NULL,
    item_name character varying(255) NOT NULL,
    vendor_name character varying(255) NOT NULL,
    ledger_account_id integer NOT NULL,
    locations character varying(255) NOT NULL,
    amount numeric(7,2) NOT NULL,
    payer character varying(16) NOT NULL,
    payment_method character varying(32) NOT NULL,
    payment_identifier character varying(32),
    receipt_file_id integer,
    is_reconciled boolean DEFAULT false NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    is_reimbursed boolean DEFAULT false NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.expense_tbl OWNER TO pithadmin;

--
-- Name: fax_in_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE fax_in_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fax_in_seq OWNER TO wchao;

--
-- Name: fax_machine_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE fax_machine_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fax_machine_seq OWNER TO wchao;

--
-- Name: fax_machine_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE fax_machine_tbl (
    id integer DEFAULT nextval('fax_machine_seq'::regclass) NOT NULL,
    account_id integer NOT NULL,
    person_id integer NOT NULL,
    mac_address character varying(12) NOT NULL,
    email_action character varying(16) DEFAULT 'none'::character varying NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.fax_machine_tbl OWNER TO pithadmin;

--
-- Name: COLUMN fax_machine_tbl.email_action; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN fax_machine_tbl.email_action IS 'none | notify';


--
-- Name: fax_number_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE fax_number_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fax_number_seq OWNER TO wchao;

--
-- Name: fax_number_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE fax_number_tbl (
    id integer DEFAULT nextval('fax_number_seq'::regclass) NOT NULL,
    account_id integer,
    person_id integer,
    fax_number character varying(10) NOT NULL,
    email_action character varying(16) DEFAULT 'none'::character varying NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    cc_email character varying(255),
    in_device_action character varying(16) DEFAULT 'none'::character varying NOT NULL,
    in_device_target character varying(255) DEFAULT ''::character varying NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.fax_number_tbl OWNER TO pithadmin;

--
-- Name: COLUMN fax_number_tbl.email_action; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN fax_number_tbl.email_action IS 'none | notify | attach | attach_delete. none stores fax online. notify sends email notification and stores fax online. attach includes fax as attachment and stores online. attach_delete attaches and then deletes the fax from online storage.
';


--
-- Name: COLUMN fax_number_tbl.in_device_action; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN fax_number_tbl.in_device_action IS 'none | print | print_delete';


--
-- Name: fax_out_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE fax_out_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fax_out_seq OWNER TO wchao;

--
-- Name: fax_out_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE fax_out_tbl (
    id integer DEFAULT nextval('fax_out_seq'::regclass) NOT NULL,
    account_id integer NOT NULL,
    person_id integer NOT NULL,
    job_id integer NOT NULL,
    comm_id character varying(16),
    recipient_name character varying(32) NOT NULL,
    fax_number character varying(10) NOT NULL,
    short_status character varying(16),
    long_status text,
    send_time timestamp(0) without time zone,
    complete_time timestamp(0) without time zone,
    xmit_time time(0) without time zone,
    page_count integer,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    send_email_alert boolean DEFAULT false NOT NULL,
    file_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.fax_out_tbl OWNER TO pithadmin;

--
-- Name: file_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE file_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.file_seq OWNER TO wchao;

--
-- Name: file_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE file_tbl (
    id integer DEFAULT nextval('file_seq'::regclass) NOT NULL,
    filename character varying(255) NOT NULL,
    mime_type character varying(128) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    namespace character varying(255) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.file_tbl OWNER TO pithadmin;

--
-- Name: floor_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE floor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.floor_seq OWNER TO wchao;

--
-- Name: floor_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE floor_tbl (
    id integer DEFAULT nextval('floor_seq'::regclass) NOT NULL,
    building_id integer NOT NULL,
    label character varying(32) NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    bounding_polygon path NOT NULL,
    short_global_label character varying(64) NOT NULL,
    short_local_label character varying(16),
    office_manager_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    map_height integer,
    map_width integer
);


ALTER TABLE public.floor_tbl OWNER TO pithadmin;

--
-- Name: COLUMN floor_tbl.office_manager_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN floor_tbl.office_manager_id IS 'office_manager_id is a link to employee_tbl';


--
-- Name: friend_referral_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE friend_referral_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.friend_referral_seq OWNER TO wchao;

--
-- Name: friend_referral_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE friend_referral_tbl (
    id integer DEFAULT nextval('friend_referral_seq'::regclass) NOT NULL,
    tenant_salutation character varying(16),
    tenant_first_name character varying(32) NOT NULL,
    tenant_middle_name character varying(48),
    tenant_last_name character varying(48) NOT NULL,
    tenant_suffix character varying(32),
    tenant_title character varying(80),
    tenant_company character varying(128),
    tenant_address_1 character varying(255),
    tenant_address_2 character varying(255),
    tenant_city character varying(64),
    tenant_state character varying(64),
    tenant_zip_code character varying(32),
    tenant_country character varying(96),
    tenant_work_phone_number character varying(48),
    tenant_cell_phone_number character varying(48),
    tenant_work_fax_number character varying(48),
    tenant_email_address character varying(255),
    type_of_business character varying(255),
    intended_use character varying(255),
    amount_of_space character varying(255),
    rental_start_date date,
    duration_of_rental character varying(64),
    desired_location character varying(32),
    friend_salutation character varying(16),
    friend_first_name character varying(32),
    friend_middle_name character varying(48),
    friend_last_name character varying(48),
    friend_suffix character varying(32),
    friend_title character varying(80),
    friend_company character varying(128),
    friend_address_1 character varying(255),
    friend_address_2 character varying(255),
    friend_city character varying(64),
    friend_state character varying(64),
    friend_zip_code character varying(32),
    friend_country character varying(96),
    friend_work_phone_number character varying(48),
    friend_cell_phone_number character varying(48),
    friend_work_fax_number character varying(48),
    friend_email_address character varying(255),
    note text,
    referrer_url text,
    initial_page_url character varying(255),
    remote_host character varying(255),
    remote_addr character varying(15),
    user_agent character varying(255),
    domain_name character varying(80),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.friend_referral_tbl OWNER TO pithadmin;

--
-- Name: google_query_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE google_query_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.google_query_seq OWNER TO wchao;

--
-- Name: google_query_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE google_query_tbl (
    id integer DEFAULT nextval('google_query_seq'::regclass) NOT NULL,
    query text NOT NULL,
    lr character varying(16),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.google_query_tbl OWNER TO pithadmin;

--
-- Name: google_search_result_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE google_search_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.google_search_result_seq OWNER TO wchao;

--
-- Name: google_search_result_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE google_search_result_tbl (
    id integer DEFAULT nextval('google_search_result_seq'::regclass) NOT NULL,
    google_query_id integer NOT NULL,
    search_rank integer NOT NULL,
    content text,
    title text,
    title_no_formatting text,
    url text NOT NULL,
    unescaped_url text NOT NULL,
    visible_url text,
    cache_url text,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.google_search_result_tbl OWNER TO pithadmin;

--
-- Name: guest_person_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE guest_person_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guest_person_seq OWNER TO wchao;

--
-- Name: guest_person_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE guest_person_tbl (
    id integer DEFAULT nextval('guest_person_seq'::regclass) NOT NULL,
    account_id integer NOT NULL,
    host_contact character varying(255),
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(32),
    last_name character varying(48) NOT NULL,
    suffix character varying(32),
    nickname character varying(32),
    title character varying(80),
    company character varying(128),
    work_phone_number character varying(32),
    cell_phone_number character varying(32),
    email character varying(128),
    start_date date NOT NULL,
    end_date date NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.guest_person_tbl OWNER TO pithadmin;

--
-- Name: holiday_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE holiday_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.holiday_seq OWNER TO wchao;

--
-- Name: holiday_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE holiday_tbl (
    id integer DEFAULT nextval('holiday_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    holiday_date date NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.holiday_tbl OWNER TO pithadmin;

--
-- Name: inbound_link_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE inbound_link_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inbound_link_seq OWNER TO wchao;

--
-- Name: inbound_link_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE inbound_link_tbl (
    id integer DEFAULT nextval('inbound_link_seq'::regclass) NOT NULL,
    web_page_id integer NOT NULL,
    url_extra_param text,
    referrer_url text NOT NULL,
    link_text text NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.inbound_link_tbl OWNER TO pithadmin;

--
-- Name: incoming_debt_investment_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE incoming_debt_investment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.incoming_debt_investment_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: incoming_debt_investment_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE incoming_debt_investment_tbl (
    id integer DEFAULT nextval(('incoming_debt_investment_seq'::text)::regclass) NOT NULL,
    company_id integer NOT NULL,
    investor_id integer NOT NULL,
    amount numeric(12,2) NOT NULL,
    cash_received boolean NOT NULL,
    documentation_received boolean NOT NULL,
    debt_payment_style character varying(16) DEFAULT 'mortgage'::character varying NOT NULL,
    debt_start_date date NOT NULL,
    debt_pik_period integer NOT NULL,
    debt_duration integer NOT NULL,
    debt_payment_frequency character varying(32) DEFAULT 'quarterly'::character varying NOT NULL,
    debt_payment_per_period numeric(12,2) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    rate numeric(4,2),
    date_of_first_pmt date NOT NULL,
    date_of_last_pmt date NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.incoming_debt_investment_tbl OWNER TO pithadmin;

--
-- Name: incoming_equity_investment_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE incoming_equity_investment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.incoming_equity_investment_seq OWNER TO wchao;

--
-- Name: incoming_equity_investment_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE incoming_equity_investment_tbl (
    id integer DEFAULT nextval(('incoming_equity_investment_seq'::text)::regclass) NOT NULL,
    company_id integer NOT NULL,
    investor_id integer NOT NULL,
    amount numeric(12,2) NOT NULL,
    cash_received boolean NOT NULL,
    documentation_received boolean NOT NULL,
    equity_round_closing_date date NOT NULL,
    equity_round_number integer NOT NULL,
    pre_money_valuation numeric(12,2) NOT NULL,
    voting_units double precision NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.incoming_equity_investment_tbl OWNER TO pithadmin;

--
-- Name: investor_bank_account_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE investor_bank_account_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.investor_bank_account_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: investor_bank_account_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE investor_bank_account_tbl (
    id integer DEFAULT nextval('investor_bank_account_seq'::regclass) NOT NULL,
    investor_id integer NOT NULL,
    depository_name character varying(48) NOT NULL,
    address_1 character varying(128),
    address_2 character varying(128),
    city character varying(64),
    state character varying(32),
    zip_code character varying(16),
    country character varying(64),
    account_type character varying(16) NOT NULL,
    routing_number character(9) NOT NULL,
    account_number character varying(17) NOT NULL,
    owner_name character varying(128) NOT NULL,
    owner_type character varying(16) NOT NULL,
    account_nickname character varying(32),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.investor_bank_account_tbl OWNER TO pithadmin;

--
-- Name: investor_deal_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE investor_deal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.investor_deal_seq OWNER TO wchao;

--
-- Name: investor_deal_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE investor_deal_tbl (
    id integer DEFAULT nextval('investor_deal_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    terms text NOT NULL,
    offer_date date NOT NULL,
    deadline_date date,
    is_active boolean DEFAULT true NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    max_amount integer,
    nda_required boolean DEFAULT true NOT NULL,
    short_name character varying(255) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.investor_deal_tbl OWNER TO pithadmin;

--
-- Name: investor_participation_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE investor_participation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.investor_participation_seq OWNER TO wchao;

--
-- Name: investor_participation_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE investor_participation_tbl (
    id integer DEFAULT nextval('investor_participation_seq'::regclass) NOT NULL,
    investor_id integer NOT NULL,
    investor_deal_id integer NOT NULL,
    nda_signed_dt timestamp(0) with time zone,
    nda_ip_address character varying(15),
    amount integer DEFAULT 0 NOT NULL,
    submission_dt timestamp(0) with time zone,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    download_packet_dt timestamp(0) with time zone,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.investor_participation_tbl OWNER TO pithadmin;

--
-- Name: investor_payment_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE investor_payment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.investor_payment_seq OWNER TO wchao;

--
-- Name: investor_payment_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE investor_payment_tbl (
    id integer DEFAULT nextval('investor_payment_seq'::regclass) NOT NULL,
    investor_id integer NOT NULL,
    amount numeric(12,2) NOT NULL,
    payment_purpose character varying(16) DEFAULT 'debt'::character varying NOT NULL,
    payment_class character varying(8) NOT NULL,
    payment_method character varying(16) DEFAULT 'check'::character varying NOT NULL,
    payment_status character varying(16) DEFAULT 'issued'::character varying NOT NULL,
    txn_date date DEFAULT ('now'::text)::date NOT NULL,
    send_name character varying(80) NOT NULL,
    send_account_type character varying(8) NOT NULL,
    send_routing_number character varying(9) NOT NULL,
    send_account_number character varying(24) NOT NULL,
    reference_number character varying(24),
    note text,
    recv_name character varying(80),
    address_1 character varying(128),
    address_2 character varying(128),
    city character varying(64),
    state character varying(32),
    zip_code character varying(16),
    country character varying(64),
    recv_account_name character varying(22),
    recv_account_type character varying(8),
    recv_routing_number character varying(9),
    recv_account_number character varying(24),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.investor_payment_tbl OWNER TO pithadmin;

--
-- Name: investor_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE investor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.investor_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: investor_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE investor_tbl (
    id integer DEFAULT nextval(('investor_seq'::text)::regclass) NOT NULL,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(32),
    last_name character varying(64) NOT NULL,
    investor_name character varying(80) NOT NULL,
    tax_id_number character varying(32),
    address_1 character varying(128),
    address_2 character varying(128),
    city character varying(64),
    state character varying(32),
    zip_code character varying(16),
    country character varying(80),
    work_phone_number character varying(32),
    work_fax_number character varying(32),
    home_phone_number character varying(32),
    cell_phone_number character varying(32),
    email character varying(64),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    referrer character varying(64),
    note text,
    email2 character varying(64),
    email3 character varying(64),
    is_active boolean DEFAULT true NOT NULL,
    password character varying(32),
    primary_bank_account_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.investor_tbl OWNER TO pithadmin;

--
-- Name: ip_address_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ip_address_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ip_address_seq OWNER TO wchao;

--
-- Name: ip_address_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ip_address_tbl (
    id integer DEFAULT nextval(('ip_address_seq'::text)::regclass) NOT NULL,
    account_id integer,
    ip_address character varying(15) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    hostname character varying(64),
    isp_id integer NOT NULL,
    building_id integer NOT NULL,
    rir_owner_id integer NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ip_address_tbl OWNER TO pithadmin;

--
-- Name: isp_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE isp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.isp_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: isp_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE isp_tbl (
    id integer DEFAULT nextval('isp_seq'::regclass) NOT NULL,
    name character varying(64) NOT NULL,
    short_name character varying(32) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.isp_tbl OWNER TO pithadmin;

--
-- Name: ledger_account_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ledger_account_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ledger_account_seq OWNER TO wchao;

--
-- Name: ledger_account_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ledger_account_tbl (
    id integer DEFAULT nextval('ledger_account_seq'::regclass) NOT NULL,
    parent_id integer,
    name character varying(255) NOT NULL,
    type character varying(64) NOT NULL,
    description text,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ledger_account_tbl OWNER TO pithadmin;

--
-- Name: license_agreement_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE license_agreement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.license_agreement_seq OWNER TO wchao;

--
-- Name: license_agreement_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE license_agreement_tbl (
    id integer DEFAULT nextval('license_agreement_seq'::regclass) NOT NULL,
    license_date date NOT NULL,
    licensor_entity character varying(4) NOT NULL,
    prospective_customer_id integer,
    signing_entity character varying(10) NOT NULL,
    company character varying(80) NOT NULL,
    first_name character varying(32) NOT NULL,
    last_name character varying(32) NOT NULL,
    title character varying(80),
    address1 character varying(128),
    address2 character varying(128),
    city character varying(64),
    state character varying(64),
    zip character varying(16),
    phone character varying(128),
    email character varying(80),
    business_type character varying(250),
    desks integer NOT NULL,
    offices integer NOT NULL,
    phone_lines integer NOT NULL,
    persons integer NOT NULL,
    months integer NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    license_fee integer NOT NULL,
    security integer NOT NULL,
    license_agreement_rtf bytea NOT NULL,
    license_agreement_pdf bytea,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    filename character varying(100),
    spacenumber character varying(5),
    phoneservice character varying(10),
    confroomhrs integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    tax_number character varying(11),
    agreement_rtf_url character varying(255),
    agreement_pdf_url character varying(255),
    desk_id integer,
    account_id integer,
    person_id integer,
    floor_id integer,
    "ds_envelopeID" character varying(255),
    ds_json_envelope_assets text,
    ds_json_envelope_documents text,
    ds_json_audit_events text,
    ds_json_envelope_recipients text,
    ds_json_envelope text,
    license_agreement_html text
);


ALTER TABLE public.license_agreement_tbl OWNER TO pithadmin;

--
-- Name: log_event_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE log_event_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_event_seq OWNER TO wchao;

--
-- Name: log_event_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE log_event_tbl (
    id integer DEFAULT nextval('log_event_seq'::regclass) NOT NULL,
    event_level character varying(255) DEFAULT 'information'::character varying NOT NULL,
    event_timestamp timestamp(0) with time zone DEFAULT now() NOT NULL,
    event_source character varying(255) DEFAULT ''::character varying NOT NULL,
    event_code integer NOT NULL,
    event_user character varying(255) DEFAULT ''::character varying NOT NULL,
    event_computer character varying(255),
    description text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.log_event_tbl OWNER TO pithadmin;

--
-- Name: mail_package_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE mail_package_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mail_package_seq OWNER TO wchao;

--
-- Name: mail_package_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE mail_package_tbl (
    id integer DEFAULT nextval('mail_package_seq'::regclass) NOT NULL,
    date timestamp(0) with time zone DEFAULT now() NOT NULL,
    customer_id integer NOT NULL,
    recipient_email text DEFAULT ''::character varying NOT NULL,
    email_message text DEFAULT ''::text NOT NULL,
    carrier character varying(100) DEFAULT ''::character varying NOT NULL,
    package_type character varying(100) DEFAULT ''::character varying NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    sender character varying(80) DEFAULT ''::character varying NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.mail_package_tbl OWNER TO pithadmin;

--
-- Name: mailbox_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE mailbox_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mailbox_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: mailbox_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE mailbox_tbl (
    id integer DEFAULT nextval(('mailbox_seq'::text)::regclass) NOT NULL,
    account_id integer,
    mailbox_number character varying(8) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    note text,
    floor_id integer NOT NULL,
    last_checked_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    bool_mail_present boolean DEFAULT false NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.mailbox_tbl OWNER TO pithadmin;

--
-- Name: marketing_call_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE marketing_call_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marketing_call_seq OWNER TO wchao;

--
-- Name: marketing_call_source_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE marketing_call_source_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marketing_call_source_seq OWNER TO wchao;

--
-- Name: marketing_call_source_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE marketing_call_source_tbl (
    id integer DEFAULT nextval(('marketing_call_source_seq'::text)::regclass) NOT NULL,
    line_number character varying(12) NOT NULL,
    start_datetime timestamp(0) with time zone NOT NULL,
    end_datetime timestamp(0) with time zone NOT NULL,
    name character varying(255) NOT NULL,
    unit_cost numeric(8,2),
    unit_name character varying(32),
    cost_per_month numeric(8,2),
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    active boolean DEFAULT true NOT NULL,
    tracking_provider character varying(32),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.marketing_call_source_tbl OWNER TO pithadmin;

--
-- Name: marketing_call_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE marketing_call_tbl (
    id integer DEFAULT nextval(('marketing_call_seq'::text)::regclass) NOT NULL,
    call_datetime timestamp(0) with time zone NOT NULL,
    line_number character varying(12) NOT NULL,
    callback_number character varying(12),
    minutes numeric(4,1),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    note text,
    is_junk boolean DEFAULT false NOT NULL,
    caller_id_name character varying(24) DEFAULT ''::character varying NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.marketing_call_tbl OWNER TO pithadmin;

--
-- Name: misc_billing_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE misc_billing_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.misc_billing_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: misc_billing_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE misc_billing_tbl (
    id integer DEFAULT nextval('misc_billing_seq'::regclass) NOT NULL,
    account_id integer NOT NULL,
    person_id integer,
    item character varying(255) NOT NULL,
    quantity integer NOT NULL,
    cost_per_item numeric(7,2) NOT NULL,
    total_amount numeric(7,2) NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    operator character varying(16),
    reconciled boolean DEFAULT false,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.misc_billing_tbl OWNER TO pithadmin;

--
-- Name: monitored_keyword_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE monitored_keyword_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.monitored_keyword_seq OWNER TO wchao;

--
-- Name: monitored_keyword_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE monitored_keyword_tbl (
    id integer DEFAULT nextval('monitored_keyword_seq'::regclass) NOT NULL,
    keyword text NOT NULL,
    search_language character varying(16),
    search_engine character varying(255) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    group_name character varying(255),
    priority integer DEFAULT 1 NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.monitored_keyword_tbl OWNER TO pithadmin;

--
-- Name: moving_checklist_seq; Type: SEQUENCE; Schema: public; Owner: czhang
--

CREATE SEQUENCE moving_checklist_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.moving_checklist_seq OWNER TO czhang;

--
-- Name: moving_checklist_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE moving_checklist_tbl (
    account_id integer,
    deposit_address_1 character varying(128),
    deposit_address_2 character varying(128),
    deposit_city character varying(64),
    deposit_state character varying(32),
    deposit_zipcode character varying(16),
    id integer DEFAULT nextval('moving_checklist_seq'::regclass) NOT NULL,
    made_payable_to character varying(64),
    office_manager_notified boolean,
    proximity_card_status character varying(8),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.moving_checklist_tbl OWNER TO pithadmin;

--
-- Name: network_switch_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE network_switch_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.network_switch_seq OWNER TO wchao;

--
-- Name: network_switch_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE network_switch_tbl (
    id integer DEFAULT nextval('network_switch_seq'::regclass) NOT NULL,
    mac_address character varying(12) NOT NULL,
    model character varying(255),
    version character varying(8),
    ip_address character varying(15),
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    username character varying(32),
    password character varying(32),
    port integer,
    floor_id integer NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.network_switch_tbl OWNER TO pithadmin;

--
-- Name: notification_seq; Type: SEQUENCE; Schema: public; Owner: czhang
--

CREATE SEQUENCE notification_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.notification_seq OWNER TO czhang;

--
-- Name: notification_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE notification_tbl (
    created timestamp with time zone,
    description text,
    id integer DEFAULT nextval('notification_seq'::regclass) NOT NULL,
    modified timestamp with time zone,
    name character varying(255),
    room_id integer,
    expires timestamp with time zone,
    employee_id integer NOT NULL,
    floors integer[],
    is_emailed boolean DEFAULT false NOT NULL,
    modifier_id integer,
    is_active boolean,
    floor_id integer,
    building_id integer
);


ALTER TABLE public.notification_tbl OWNER TO pithadmin;

--
-- Name: owned_domain_name_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE owned_domain_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.owned_domain_name_seq OWNER TO wchao;

--
-- Name: owned_domain_name_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE owned_domain_name_tbl (
    id integer DEFAULT nextval('owned_domain_name_seq'::regclass) NOT NULL,
    domain_name character varying(255) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.owned_domain_name_tbl OWNER TO pithadmin;

--
-- Name: payment_method_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE payment_method_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_method_seq OWNER TO wchao;

--
-- Name: payment_method_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE payment_method_tbl (
    id integer DEFAULT nextval('payment_method_seq'::regclass) NOT NULL,
    name character varying(16) NOT NULL,
    note text,
    is_active boolean DEFAULT true NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.payment_method_tbl OWNER TO pithadmin;

--
-- Name: person_vw; Type: VIEW; Schema: public; Owner: pithadmin
--

CREATE VIEW person_vw AS
    SELECT cust_pers_tbl.id, cust_pers_tbl.cust_acct_id AS account_id, cust_pers_tbl.salutation, cust_pers_tbl.first_name, cust_pers_tbl.middle_name, cust_pers_tbl.last_name, cust_pers_tbl.suffix, cust_pers_tbl.nickname, cust_pers_tbl.title, cust_pers_tbl.company, cust_pers_tbl.email, cust_pers_tbl.password, cust_pers_tbl.work_phone_number, cust_pers_tbl.home_phone_number, cust_pers_tbl.cell_phone_number, cust_pers_tbl.other_phone_number, cust_pers_tbl.work_fax_number, cust_pers_tbl.work_address_1, cust_pers_tbl.work_address_2, cust_pers_tbl.work_city, cust_pers_tbl.work_state, cust_pers_tbl.work_zip_code, cust_pers_tbl.work_country, cust_pers_tbl.home_address_1, cust_pers_tbl.home_address_2, cust_pers_tbl.home_city, cust_pers_tbl.home_state, cust_pers_tbl.home_zip_code, cust_pers_tbl.home_country, cust_pers_tbl.note, cust_pers_tbl.is_active FROM cust_pers_tbl UNION ALL SELECT employee_tbl.id, 1 AS account_id, employee_tbl.salutation, employee_tbl.first_name, employee_tbl.middle_name, employee_tbl.last_name, employee_tbl.suffix, employee_tbl.nickname, employee_tbl.title, employee_tbl.company, employee_tbl.email, employee_tbl.password, employee_tbl.work_phone_number, employee_tbl.home_phone_number, employee_tbl.cell_phone_number, employee_tbl.other_phone_number, employee_tbl.work_fax_number, employee_tbl.work_address_1, employee_tbl.work_address_2, employee_tbl.work_city, employee_tbl.work_state, employee_tbl.work_zip_code, employee_tbl.work_country, employee_tbl.home_address_1, employee_tbl.home_address_2, employee_tbl.home_city, employee_tbl.home_state, employee_tbl.home_zip_code, employee_tbl.home_country, employee_tbl.note, employee_tbl.is_active FROM employee_tbl;


ALTER TABLE public.person_vw OWNER TO pithadmin;

--
-- Name: phone_carrier_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE phone_carrier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phone_carrier_seq OWNER TO wchao;

--
-- Name: phone_carrier_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE phone_carrier_tbl (
    id integer DEFAULT nextval('phone_carrier_seq'::regclass) NOT NULL,
    name character varying(64) NOT NULL,
    short_name character varying(32) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    url text,
    username character varying(64),
    password character varying(128),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.phone_carrier_tbl OWNER TO pithadmin;

--
-- Name: port_seq; Type: SEQUENCE; Schema: public; Owner: czhang
--

CREATE SEQUENCE port_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.port_seq OWNER TO czhang;

--
-- Name: port_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE port_tbl (
    id integer DEFAULT nextval('port_seq'::regclass) NOT NULL,
    description text,
    requestedby_employee_id integer,
    requestedby_cust_acct_id integer,
    creation_date date DEFAULT ('now'::text)::date NOT NULL,
    confirmed_date date,
    status character varying(30),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    port_phone_number numeric,
    notes character varying(30),
    modified_dt timestamp without time zone,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.port_tbl OWNER TO pithadmin;

--
-- Name: prospective_customer_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE prospective_customer_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prospective_customer_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: prospective_customer_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE prospective_customer_tbl (
    id integer DEFAULT nextval(('prospective_customer_seq'::text)::regclass) NOT NULL,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    last_name character varying(32),
    other_phone_number character varying(128),
    work_phone_number character varying(128),
    home_phone_number character varying(128),
    cell_phone_number character varying(128),
    email_address character varying(80),
    company character varying(80),
    business_description text,
    address_1 character varying(128),
    address_2 character varying(128),
    city character varying(64),
    state character varying(64),
    zip_code character varying(16),
    amount_of_space character varying(255),
    rental_start_date date,
    duration_of_rental character varying(64),
    desired_location character varying(32),
    initial_contact_dt timestamp(0) with time zone,
    initial_contact_method character varying(32),
    marketing_source character varying(255),
    follow_up_date date,
    notes text,
    sales_status character varying(16),
    referrer_url text,
    remote_host text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created_by character varying(32) DEFAULT "current_user"() NOT NULL,
    user_agent text,
    initial_page_url text,
    country character varying(64),
    middle_name character varying(32),
    remote_addr character varying(15),
    title character varying(80),
    web_site character varying(255),
    fax_number character varying(32),
    suffix character varying(32),
    reminder_email_address character varying(80),
    sales_stage character varying(64),
    license_created boolean DEFAULT false NOT NULL,
    broker_name character varying(255),
    domain_name character varying(80),
    x_forwarded_for text DEFAULT ''::text NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    building_id integer,
    sales_stage_id integer
);


ALTER TABLE public.prospective_customer_tbl OWNER TO pithadmin;

--
-- Name: COLUMN prospective_customer_tbl.modifier_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN prospective_customer_tbl.modifier_id IS 'fk to employee_tbl';


--
-- Name: COLUMN prospective_customer_tbl.building_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN prospective_customer_tbl.building_id IS 'fk to building_tbl';


--
-- Name: prospective_investment_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE prospective_investment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prospective_investment_seq OWNER TO wchao;

--
-- Name: prospective_investment_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE prospective_investment_tbl (
    id integer DEFAULT nextval(('prospective_investment_seq'::text)::regclass) NOT NULL,
    company_id integer NOT NULL,
    investor_id integer NOT NULL,
    type character varying(16) NOT NULL,
    low_amount numeric(12,2),
    high_amount numeric(12,2),
    likely_amount numeric(12,2) NOT NULL,
    probability integer NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.prospective_investment_tbl OWNER TO pithadmin;

SET default_with_oids = false;

--
-- Name: prospects_sales_tags_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE prospects_sales_tags_tbl (
    id integer DEFAULT nextval(('prospects_sales_tags_seq'::text)::regclass) NOT NULL,
    prospect_id integer NOT NULL,
    sales_tag_id integer NOT NULL,
    creation_dt timestamp with time zone DEFAULT now(),
    ms_access_creation_dt timestamp with time zone DEFAULT now(),
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    modifier_id integer
);


ALTER TABLE public.prospects_sales_tags_tbl OWNER TO pithadmin;

--
-- Name: prospects_sales_tags_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE prospects_sales_tags_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prospects_sales_tags_seq OWNER TO pithadmin;

--
-- Name: prospects_sales_tags_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pithadmin
--

ALTER SEQUENCE prospects_sales_tags_seq OWNED BY prospects_sales_tags_tbl.id;


--
-- Name: proximity_card_edit_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE proximity_card_edit_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proximity_card_edit_seq OWNER TO wchao;

--
-- Name: proximity_card_edit_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE proximity_card_edit_tbl (
    id integer DEFAULT nextval('proximity_card_edit_seq'::regclass) NOT NULL,
    number integer NOT NULL,
    account_id integer NOT NULL,
    person_id integer,
    action_requested character varying(64) NOT NULL,
    cause character varying(64) NOT NULL,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    operator character varying(16),
    access_level_id integer,
    completed boolean DEFAULT false,
    rejected boolean DEFAULT false,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.proximity_card_edit_tbl OWNER TO pithadmin;

--
-- Name: proximity_card_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE proximity_card_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proximity_card_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: proximity_card_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE proximity_card_tbl (
    id integer DEFAULT nextval(('proximity_card_seq'::text)::regclass) NOT NULL,
    number integer NOT NULL,
    type character varying(16) NOT NULL,
    status character varying(16) NOT NULL,
    account_id integer,
    person_id integer,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    access_level_id integer,
    floor_id integer NOT NULL,
    technology character varying(16) NOT NULL,
    site_code integer,
    intellim_id integer,
    card_number character varying(20),
    intellim_employee_id character varying(20),
    user_role character varying(20),
    in_group boolean,
    group_intellim_id character varying(60),
    is_credential_holder boolean,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    is_intellim boolean
);


ALTER TABLE public.proximity_card_tbl OWNER TO pithadmin;

--
-- Name: receivable_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE receivable_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.receivable_seq OWNER TO wchao;

--
-- Name: receivable_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE receivable_tbl (
    id integer DEFAULT nextval(('receivable_seq'::text)::regclass) NOT NULL,
    account_id integer NOT NULL,
    period_start date NOT NULL,
    period_end date NOT NULL,
    amount numeric(8,2) NOT NULL,
    due_date date NOT NULL,
    paid boolean NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    company_id integer NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.receivable_tbl OWNER TO pithadmin;

--
-- Name: recur_card_txn_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE recur_card_txn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recur_card_txn_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: recur_card_txn_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE recur_card_txn_tbl (
    id integer DEFAULT nextval('recur_card_txn_seq'::regclass) NOT NULL,
    account_id integer NOT NULL,
    employee_id integer,
    amount numeric(10,2) NOT NULL,
    process_mode character varying(12) DEFAULT 'manual'::character varying NOT NULL,
    frequency character varying(16) NOT NULL,
    next_date date NOT NULL,
    card_number character varying(24) NOT NULL,
    card_expiration_month character(2) NOT NULL,
    card_expiration_year character(4) NOT NULL,
    card_security_code character varying(4),
    card_bill_name character varying(64) NOT NULL,
    card_bill_company character varying(64),
    card_bill_address_num character varying(16),
    card_bill_zip_code character varying(16) NOT NULL,
    card_bill_address_1 character varying(128) NOT NULL,
    card_bill_address_2 character varying(128),
    card_bill_city character varying(48) NOT NULL,
    card_bill_state character varying(64) NOT NULL,
    card_bill_country character varying(64) NOT NULL,
    card_bill_phone_number character varying(32),
    card_bill_fax_number character varying(32),
    card_bill_email_address character varying(64),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.recur_card_txn_tbl OWNER TO pithadmin;

--
-- Name: recv_payment_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE recv_payment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recv_payment_seq OWNER TO wchao;

--
-- Name: recv_payment_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE recv_payment_tbl (
    id integer DEFAULT nextval('recv_payment_seq'::regclass) NOT NULL,
    cust_acct_id integer NOT NULL,
    employee_id integer,
    amount numeric(10,2) NOT NULL,
    recv_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    payment_method character varying(16) NOT NULL,
    process_txn_after_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    process_mode character varying(12) DEFAULT 'automatic'::character varying NOT NULL,
    process_status character varying(12) DEFAULT 'pending'::character varying NOT NULL,
    notify_email character varying(80),
    client_hostname character varying(128),
    client_ip_addr character varying(15),
    invoice_number character varying(128),
    note text,
    is_reconciled boolean DEFAULT false NOT NULL,
    payment_destination character varying(16),
    check_name character varying(80),
    check_number character varying(24),
    bank_account_name character varying(22),
    bank_account_type character varying(8),
    bank_routing_number character varying(9),
    bank_account_number character varying(24),
    card_number character varying(24),
    card_expiration_month character(2),
    card_expiration_year character(4),
    card_security_code character varying(4),
    card_bill_name character varying(64),
    card_bill_company character varying(64),
    card_bill_address_num character varying(16),
    card_bill_zip_code character varying(16),
    card_bill_address_1 character varying(128),
    card_bill_address_2 character varying(128),
    card_bill_city character varying(48),
    card_bill_state character varying(64),
    card_bill_country character varying(64),
    card_bill_phone_number character varying(32),
    card_bill_fax_number character varying(32),
    card_bill_email_address character varying(64),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    deposit_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.recv_payment_tbl OWNER TO pithadmin;

--
-- Name: reservation_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE reservation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservation_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: reservation_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE reservation_tbl (
    id integer DEFAULT nextval(('reservation_seq'::text)::regclass) NOT NULL,
    room_id integer NOT NULL,
    account_id integer NOT NULL,
    start_dt timestamp(0) with time zone NOT NULL,
    end_dt timestamp(0) with time zone NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.reservation_tbl OWNER TO pithadmin;

SET default_with_oids = false;

--
-- Name: result_item_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE result_item_tbl (
    result_list_id integer NOT NULL,
    index_number integer NOT NULL,
    item_id integer NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.result_item_tbl OWNER TO pithadmin;

--
-- Name: result_list_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE result_list_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.result_list_seq OWNER TO wchao;

--
-- Name: result_list_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE result_list_tbl (
    id integer DEFAULT nextval('result_list_seq'::regclass) NOT NULL,
    owner_type character varying(16) NOT NULL,
    owner_id integer NOT NULL,
    table_name character varying(255) NOT NULL,
    query_param text,
    note text,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.result_list_tbl OWNER TO pithadmin;

--
-- Name: room_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE room_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.room_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: room_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE room_tbl (
    id integer DEFAULT nextval(('room_seq'::text)::regclass) NOT NULL,
    name character varying(64) NOT NULL,
    short_name character varying(32) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    floor_id integer NOT NULL,
    is_shared_floor boolean DEFAULT true NOT NULL,
    room_type character varying(16) DEFAULT 'conference'::character varying NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.room_tbl OWNER TO pithadmin;

--
-- Name: COLUMN room_tbl.room_type; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN room_tbl.room_type IS 'conference | therapy';


--
-- Name: row_update_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE row_update_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.row_update_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: row_update_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE row_update_tbl (
    id integer DEFAULT nextval('row_update_seq'::regclass) NOT NULL,
    table_name character varying(255) NOT NULL,
    old_value_summary text NOT NULL,
    new_value_summary text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.row_update_tbl OWNER TO pithadmin;

--
-- Name: sales_interaction_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE sales_interaction_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sales_interaction_seq OWNER TO wchao;

--
-- Name: sales_interaction_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE sales_interaction_tbl (
    id integer DEFAULT nextval('sales_interaction_seq'::regclass) NOT NULL,
    prospective_customer_id integer NOT NULL,
    interaction_dt timestamp(0) with time zone NOT NULL,
    salesperson character varying(32),
    interaction_type character varying(32),
    notes text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    sales_person_id integer
);


ALTER TABLE public.sales_interaction_tbl OWNER TO pithadmin;

--
-- Name: COLUMN sales_interaction_tbl.sales_person_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN sales_interaction_tbl.sales_person_id IS 'fk to employee_tbl';


--
-- Name: scanner_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE scanner_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scanner_seq OWNER TO wchao;

--
-- Name: scanner_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE scanner_tbl (
    id integer DEFAULT nextval('scanner_seq'::regclass) NOT NULL,
    mac_address character varying(12) NOT NULL,
    model character varying(255),
    version character varying(8),
    floor_id integer,
    ip_address character varying(15),
    username character varying(32),
    password character varying(32),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.scanner_tbl OWNER TO pithadmin;

--
-- Name: service_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE service_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: service_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE service_tbl (
    id integer DEFAULT nextval(('service_seq'::text)::regclass) NOT NULL,
    name character varying(32) NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.service_tbl OWNER TO pithadmin;

--
-- Name: session_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE session_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.session_seq OWNER TO wchao;

--
-- Name: session_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE session_tbl (
    id integer DEFAULT nextval(('session_seq'::text)::regclass) NOT NULL,
    public_id character varying(255) DEFAULT generate_public_id(192) NOT NULL,
    payload text NOT NULL,
    update_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    remote_addr character varying(15),
    remote_host character varying(255),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.session_tbl OWNER TO pithadmin;

--
-- Name: ss_country_x_prefix_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ss_country_x_prefix_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ss_country_x_prefix_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: ss_country_x_prefix_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ss_country_x_prefix_tbl (
    id integer DEFAULT nextval('ss_country_x_prefix_seq'::regclass) NOT NULL,
    parent_id integer NOT NULL,
    element_value character varying(255) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ss_country_x_prefix_tbl OWNER TO pithadmin;

--
-- Name: ss_employee_x_role_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ss_employee_x_role_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ss_employee_x_role_seq OWNER TO wchao;

--
-- Name: ss_employee_x_role_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ss_employee_x_role_tbl (
    id integer DEFAULT nextval('ss_employee_x_role_seq'::regclass) NOT NULL,
    parent_id integer NOT NULL,
    element_value character varying(255) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ss_employee_x_role_tbl OWNER TO pithadmin;

--
-- Name: ss_investor_x_role_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ss_investor_x_role_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ss_investor_x_role_seq OWNER TO wchao;

--
-- Name: ss_investor_x_role_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ss_investor_x_role_tbl (
    id integer DEFAULT nextval('ss_investor_x_role_seq'::regclass) NOT NULL,
    parent_id integer NOT NULL,
    element_value character varying(255) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ss_investor_x_role_tbl OWNER TO pithadmin;

--
-- Name: ss_prospective_customer_x_sales_tag_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ss_prospective_customer_x_sales_tag_tbl (
    parent_id integer NOT NULL,
    element_value character varying(255) NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer,
    id integer NOT NULL
);


ALTER TABLE public.ss_prospective_customer_x_sales_tag_tbl OWNER TO pithadmin;

--
-- Name: ss_prospective_customer_x_sales_tag_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE ss_prospective_customer_x_sales_tag_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ss_prospective_customer_x_sales_tag_seq OWNER TO pithadmin;

--
-- Name: ss_prospective_customer_x_sales_tag_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pithadmin
--

ALTER SEQUENCE ss_prospective_customer_x_sales_tag_seq OWNED BY ss_prospective_customer_x_sales_tag_tbl.id;


--
-- Name: ss_vend_pers_x_role_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ss_vend_pers_x_role_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ss_vend_pers_x_role_seq OWNER TO wchao;

--
-- Name: ss_vend_pers_x_role_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ss_vend_pers_x_role_tbl (
    id integer DEFAULT nextval('ss_vend_pers_x_role_seq'::regclass) NOT NULL,
    parent_id integer NOT NULL,
    element_value character varying(255) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ss_vend_pers_x_role_tbl OWNER TO pithadmin;

--
-- Name: ss_web_page_x_keyword_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE ss_web_page_x_keyword_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ss_web_page_x_keyword_seq OWNER TO wchao;

--
-- Name: ss_web_page_x_keyword_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE ss_web_page_x_keyword_tbl (
    id integer DEFAULT nextval('ss_web_page_x_keyword_seq'::regclass) NOT NULL,
    parent_id integer NOT NULL,
    element_value character varying(255) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.ss_web_page_x_keyword_tbl OWNER TO pithadmin;

--
-- Name: state_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE state_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.state_seq OWNER TO wchao;

--
-- Name: state_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE state_tbl (
    id integer DEFAULT nextval('state_seq'::regclass) NOT NULL,
    name character varying(80) NOT NULL,
    abbreviation character varying(4) NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.state_tbl OWNER TO pithadmin;

--
-- Name: subnet_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE subnet_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subnet_seq OWNER TO wchao;

--
-- Name: subnet_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE subnet_tbl (
    id integer DEFAULT nextval('subnet_seq'::regclass) NOT NULL,
    network character varying(15) NOT NULL,
    netmask character varying(15) NOT NULL,
    gateway character varying(15),
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    start_ip_address character varying(15),
    end_ip_address character varying(15),
    floor_id integer,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.subnet_tbl OWNER TO pithadmin;

--
-- Name: switch_octet_count_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE switch_octet_count_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.switch_octet_count_seq OWNER TO wchao;

--
-- Name: switch_octet_count_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE switch_octet_count_tbl (
    id integer DEFAULT nextval('switch_octet_count_seq'::regclass) NOT NULL,
    network_switch_id integer NOT NULL,
    sample_time timestamp with time zone NOT NULL,
    in_1 bigint,
    in_2 bigint,
    in_3 bigint,
    in_4 bigint,
    in_5 bigint,
    in_6 bigint,
    in_7 bigint,
    in_8 bigint,
    in_9 bigint,
    in_10 bigint,
    in_11 bigint,
    in_12 bigint,
    in_13 bigint,
    in_14 bigint,
    in_15 bigint,
    in_16 bigint,
    in_17 bigint,
    in_18 bigint,
    in_19 bigint,
    in_20 bigint,
    in_21 bigint,
    in_22 bigint,
    in_23 bigint,
    in_24 bigint,
    in_25 bigint,
    in_26 bigint,
    in_27 bigint,
    in_28 bigint,
    in_29 bigint,
    in_30 bigint,
    in_31 bigint,
    in_32 bigint,
    in_33 bigint,
    in_34 bigint,
    in_35 bigint,
    in_36 bigint,
    in_37 bigint,
    in_38 bigint,
    in_39 bigint,
    in_40 bigint,
    in_41 bigint,
    in_42 bigint,
    in_43 bigint,
    in_44 bigint,
    in_45 bigint,
    in_46 bigint,
    in_47 bigint,
    in_48 bigint,
    in_49 bigint,
    in_50 bigint,
    in_51 bigint,
    in_52 bigint,
    out_1 bigint,
    out_2 bigint,
    out_3 bigint,
    out_4 bigint,
    out_5 bigint,
    out_6 bigint,
    out_7 bigint,
    out_8 bigint,
    out_9 bigint,
    out_10 bigint,
    out_11 bigint,
    out_12 bigint,
    out_13 bigint,
    out_14 bigint,
    out_15 bigint,
    out_16 bigint,
    out_17 bigint,
    out_18 bigint,
    out_19 bigint,
    out_20 bigint,
    out_21 bigint,
    out_22 bigint,
    out_23 bigint,
    out_24 bigint,
    out_25 bigint,
    out_26 bigint,
    out_27 bigint,
    out_28 bigint,
    out_29 bigint,
    out_30 bigint,
    out_31 bigint,
    out_32 bigint,
    out_33 bigint,
    out_34 bigint,
    out_35 bigint,
    out_36 bigint,
    out_37 bigint,
    out_38 bigint,
    out_39 bigint,
    out_40 bigint,
    out_41 bigint,
    out_42 bigint,
    out_43 bigint,
    out_44 bigint,
    out_45 bigint,
    out_46 bigint,
    out_47 bigint,
    out_48 bigint,
    out_49 bigint,
    out_50 bigint,
    out_51 bigint,
    out_52 bigint,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.switch_octet_count_tbl OWNER TO pithadmin;

--
-- Name: task_note_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE task_note_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_note_seq OWNER TO pithadmin;

--
-- Name: task_note_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE task_note_tbl (
    content text,
    created timestamp(0) without time zone DEFAULT now() NOT NULL,
    employee_id integer NOT NULL,
    id integer DEFAULT nextval('task_note_seq'::regclass) NOT NULL,
    modified timestamp(0) with time zone DEFAULT now(),
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now(),
    ms_access_random_num integer,
    task_id integer NOT NULL,
    modifier_id integer
);


ALTER TABLE public.task_note_tbl OWNER TO pithadmin;

--
-- Name: COLUMN task_note_tbl.employee_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN task_note_tbl.employee_id IS 'author of this note';


--
-- Name: task_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE task_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_seq OWNER TO wchao;

--
-- Name: task_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE task_tbl (
    id integer DEFAULT nextval('task_seq'::regclass) NOT NULL,
    description text,
    assigned_employee_id integer NOT NULL,
    requestedby_employee_id integer,
    requestedby_cust_pers_id integer,
    requestedby_cust_acct_id integer,
    creation_date date DEFAULT ('now'::text)::date NOT NULL,
    requested_completion_date date DEFAULT ('now'::text)::date NOT NULL,
    completion_date date,
    status character varying(30),
    urgency character varying(30),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    floor_id integer,
    area character varying(32),
    jumble character varying(30),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.task_tbl OWNER TO pithadmin;

--
-- Name: tax_document_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE tax_document_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tax_document_seq OWNER TO wchao;

--
-- Name: tax_document_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE tax_document_tbl (
    id integer DEFAULT nextval('tax_document_seq'::regclass) NOT NULL,
    investor_id integer NOT NULL,
    form_name character varying(255) NOT NULL,
    tax_year character varying(4) NOT NULL,
    file_id integer NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    company_id integer NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.tax_document_tbl OWNER TO pithadmin;

--
-- Name: token_seq; Type: SEQUENCE; Schema: public; Owner: czhang
--

CREATE SEQUENCE token_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.token_seq OWNER TO czhang;

--
-- Name: token_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE token_tbl (
    created timestamp with time zone,
    expires timestamp with time zone,
    id integer DEFAULT nextval('token_seq'::regclass) NOT NULL,
    is_spent boolean,
    jumble character varying(45),
    type character varying(45),
    user_id integer,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.token_tbl OWNER TO pithadmin;

--
-- Name: user_seq; Type: SEQUENCE; Schema: public; Owner: czhang
--

CREATE SEQUENCE user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.user_seq OWNER TO czhang;

--
-- Name: user_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE user_tbl (
    created timestamp with time zone,
    id integer DEFAULT nextval('user_seq'::regclass) NOT NULL,
    last_logged_in timestamp with time zone,
    modified timestamp without time zone,
    password character varying(50),
    role smallint,
    username character varying(128),
    modifier_id integer
);


ALTER TABLE public.user_tbl OWNER TO pithadmin;

--
-- Name: vend_acct_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE vend_acct_tbl (
    id integer DEFAULT nextval('account_seq'::regclass) NOT NULL,
    vend_pers_id integer,
    name character varying(255) NOT NULL,
    description text,
    web_site_url text,
    tax_id character varying(16),
    note text,
    is_active boolean DEFAULT true NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT msaccessrandomnum() NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.vend_acct_tbl OWNER TO pithadmin;

--
-- Name: TABLE vend_acct_tbl; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON TABLE vend_acct_tbl IS 'vendor accounts';


--
-- Name: COLUMN vend_acct_tbl.vend_pers_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_acct_tbl.vend_pers_id IS 'link to vend_pers_tbl.id: primary contact person for vendor account';


--
-- Name: COLUMN vend_acct_tbl.description; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_acct_tbl.description IS 'description of the business or company';


--
-- Name: COLUMN vend_acct_tbl.tax_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_acct_tbl.tax_id IS 'social security number of the individual or tax ID of the business';


--
-- Name: COLUMN vend_acct_tbl.note; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_acct_tbl.note IS 'note viewable by Micro Office staff and vendor';


--
-- Name: COLUMN vend_acct_tbl.is_active; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_acct_tbl.is_active IS 'if not(is_active), then record is hidden from most queries';


--
-- Name: vend_pers_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE vend_pers_tbl (
    id integer DEFAULT nextval('person_seq'::regclass) NOT NULL,
    vend_acct_id integer,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(48),
    last_name character varying(48) NOT NULL,
    suffix character varying(32),
    nickname character varying(32),
    title character varying(80),
    email character varying(255),
    password character varying(255),
    email_address_2 character varying(255),
    email_address_3 character varying(255),
    work_phone_number character varying(48),
    work_fax_number character varying(48),
    home_phone_number character varying(48),
    cell_phone_number character varying(48),
    other_phone_number character varying(48),
    work_address_1 character varying(255),
    work_address_2 character varying(255),
    work_city character varying(64),
    work_state character varying(64),
    work_zip_code character varying(32),
    work_country character varying(96),
    home_address_1 character varying(255),
    home_address_2 character varying(255),
    home_city character varying(64),
    home_state character varying(64),
    home_zip_code character varying(32),
    home_country character varying(96),
    birthday date,
    note text,
    is_active boolean DEFAULT true NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT msaccessrandomnum() NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.vend_pers_tbl OWNER TO pithadmin;

--
-- Name: TABLE vend_pers_tbl; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON TABLE vend_pers_tbl IS 'vendor persons';


--
-- Name: COLUMN vend_pers_tbl.vend_acct_id; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.vend_acct_id IS 'link to vend_acct_tbl.id: vendor account that this person belongs to';


--
-- Name: COLUMN vend_pers_tbl.email; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.email IS 'main email; used to communicate and for vendor login username';


--
-- Name: COLUMN vend_pers_tbl.password; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.password IS 'vendor login password; set NULL to disable login';


--
-- Name: COLUMN vend_pers_tbl.email_address_2; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.email_address_2 IS 'additional email address for communication';


--
-- Name: COLUMN vend_pers_tbl.email_address_3; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.email_address_3 IS 'additional email address for communication';


--
-- Name: COLUMN vend_pers_tbl.work_state; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.work_state IS 'enum_state_tbl for possible values; can also be free-form state/province';


--
-- Name: COLUMN vend_pers_tbl.work_country; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.work_country IS 'enum_country_tbl for possible values';


--
-- Name: COLUMN vend_pers_tbl.home_state; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.home_state IS 'enum_state_tbl for possible values; can also be free-form state/province';


--
-- Name: COLUMN vend_pers_tbl.home_country; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.home_country IS 'enum_country_tbl for possible values';


--
-- Name: COLUMN vend_pers_tbl.note; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.note IS 'note viewable by Micro Office staff and vendor';


--
-- Name: COLUMN vend_pers_tbl.is_active; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN vend_pers_tbl.is_active IS 'if not(is_active), then record is hidden from most queries';


--
-- Name: virtual_office_application_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE virtual_office_application_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.virtual_office_application_seq OWNER TO wchao;

SET default_with_oids = true;

--
-- Name: virtual_office_application_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE virtual_office_application_tbl (
    id integer DEFAULT nextval(('virtual_office_application_seq'::text)::regclass) NOT NULL,
    request_datetime timestamp(0) with time zone DEFAULT now() NOT NULL,
    referrer_url character varying(255),
    remote_host character varying(255),
    user_agent text,
    salutation character varying(16),
    first_name character varying(32) NOT NULL,
    middle_name character varying(32),
    last_name character varying(48) NOT NULL,
    company character varying(128),
    business_description text,
    mailing_address_1 character varying(128) NOT NULL,
    mailing_address_2 character varying(128),
    mailing_city character varying(64) NOT NULL,
    mailing_state character varying(64),
    mailing_zip_code character varying(16) NOT NULL,
    work_phone_number character varying(32) NOT NULL,
    home_phone_number character varying(32),
    cell_phone_number character varying(32),
    email_address character varying(64) NOT NULL,
    web_site character varying(255),
    name_on_card character varying(64) NOT NULL,
    credit_card_type character varying(16) NOT NULL,
    credit_card_number character varying(24) NOT NULL,
    card_security_code character varying(8),
    billing_address_1 character varying(128) NOT NULL,
    billing_address_2 character varying(128),
    billing_city character varying(48) NOT NULL,
    billing_state character varying(64),
    billing_zip_code character varying(16) NOT NULL,
    service_plan character varying(24) NOT NULL,
    marketing_source character varying(255),
    processed boolean DEFAULT false NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    initial_page_url character varying(255),
    title character varying(80),
    total_amount numeric(6,2),
    company_on_card character varying(64),
    billing_country character varying(64),
    fax_number character varying(32),
    mailing_country character varying(64),
    notes text,
    remote_addr character varying(15),
    expiration_month character(2) NOT NULL,
    expiration_year character(4) NOT NULL,
    other_phone_number character varying(32),
    suffix character varying(32),
    cancelled boolean DEFAULT false NOT NULL,
    desired_address character varying(128),
    domain_name character varying(80),
    x_forwarded_for text DEFAULT ''::text NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.virtual_office_application_tbl OWNER TO pithadmin;

--
-- Name: voicecloud_txn_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE voicecloud_txn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.voicecloud_txn_seq OWNER TO wchao;

SET default_with_oids = false;

--
-- Name: voicecloud_txn_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE voicecloud_txn_tbl (
    id integer DEFAULT nextval('voicecloud_txn_seq'::regclass) NOT NULL,
    transaction_uid integer,
    transcribed_text text DEFAULT ''::text NOT NULL,
    download_dt timestamp(0) with time zone,
    file_id integer NOT NULL,
    caller_id_name character varying(32) DEFAULT ''::character varying NOT NULL,
    caller_id_number character varying(32) DEFAULT ''::character varying NOT NULL,
    mailbox_account character varying(255) DEFAULT ''::character varying NOT NULL,
    message_timestamp timestamp(0) with time zone NOT NULL,
    message_duration character varying(8) DEFAULT ''::character varying NOT NULL,
    email_from character varying(255) DEFAULT ''::character varying NOT NULL,
    email_to character varying(255) DEFAULT ''::character varying NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.voicecloud_txn_tbl OWNER TO pithadmin;

--
-- Name: voicemail_msgs_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE voicemail_msgs_tbl (
    created_epoch integer,
    read_epoch integer,
    username character varying(255),
    domain character varying(255),
    uuid character varying(255),
    cid_name character varying(255),
    cid_number character varying(255),
    in_folder character varying(255),
    file_path character varying(255),
    message_len integer,
    flags character varying(255),
    read_flags character varying(255),
    forwarded_by character varying(255),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.voicemail_msgs_tbl OWNER TO pithadmin;

--
-- Name: voicemail_prefs; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE voicemail_prefs (
    username character varying(255),
    domain character varying(255),
    name_path character varying(255),
    greeting_path character varying(255),
    password character varying(255),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.voicemail_prefs OWNER TO pithadmin;

--
-- Name: voicemail_prefs_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE voicemail_prefs_tbl (
    domain character varying(255),
    greeting_path character varying(255),
    name_path character varying(255),
    password character varying(255),
    username character varying(255),
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.voicemail_prefs_tbl OWNER TO pithadmin;

--
-- Name: watched_domain_name_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE watched_domain_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.watched_domain_name_seq OWNER TO wchao;

--
-- Name: watched_domain_name_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE watched_domain_name_tbl (
    id integer DEFAULT nextval('watched_domain_name_seq'::regclass) NOT NULL,
    domain_name character varying(255) NOT NULL,
    expire_date date,
    owner_name character varying(255),
    owner_phone_number character varying(48),
    owner_email_address character varying(255),
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    show_rank boolean DEFAULT true NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.watched_domain_name_tbl OWNER TO pithadmin;

--
-- Name: web_page_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE web_page_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_page_seq OWNER TO wchao;

--
-- Name: web_page_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE web_page_tbl (
    id integer DEFAULT nextval('web_page_seq'::regclass) NOT NULL,
    owned_domain_name_id integer NOT NULL,
    domain_prefix character varying(255),
    uri_scheme character varying(255) DEFAULT 'http'::character varying NOT NULL,
    uri_path text NOT NULL,
    note text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.web_page_tbl OWNER TO pithadmin;

--
-- Name: web_posting_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE web_posting_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_posting_seq OWNER TO wchao;

--
-- Name: web_posting_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE web_posting_tbl (
    id integer DEFAULT nextval('web_posting_seq'::regclass) NOT NULL,
    site_name character varying(32) NOT NULL,
    user_agent bytea,
    url text,
    param_map bytea,
    internal_map bytea,
    queued_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    processed_dt timestamp(0) with time zone,
    notes text,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)) NOT NULL,
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    auth_token integer,
    web_spider_start_dt timestamp(0) with time zone,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.web_posting_tbl OWNER TO pithadmin;

--
-- Name: yahoo_query_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE yahoo_query_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.yahoo_query_seq OWNER TO wchao;

--
-- Name: yahoo_query_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE yahoo_query_tbl (
    id integer DEFAULT nextval('yahoo_query_seq'::regclass) NOT NULL,
    query text NOT NULL,
    lr character varying(128),
    note text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.yahoo_query_tbl OWNER TO pithadmin;

--
-- Name: yahoo_search_result_seq; Type: SEQUENCE; Schema: public; Owner: wchao
--

CREATE SEQUENCE yahoo_search_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.yahoo_search_result_seq OWNER TO wchao;

--
-- Name: yahoo_search_result_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE yahoo_search_result_tbl (
    id integer DEFAULT nextval('yahoo_search_result_seq'::regclass) NOT NULL,
    yahoo_query_id integer NOT NULL,
    search_rank integer NOT NULL,
    content text,
    title text,
    title_no_formatting text,
    url text NOT NULL,
    unescaped_url text NOT NULL,
    visible_url text,
    cache_url text,
    note text DEFAULT ''::text NOT NULL,
    ms_access_creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    ms_access_random_num integer DEFAULT round(((random() * (4294967295::bigint)::double precision) - (2147483648::bigint)::double precision)),
    creation_dt timestamp(0) with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    modifier_id integer
);


ALTER TABLE public.yahoo_search_result_tbl OWNER TO pithadmin;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pithadmin
--

ALTER TABLE ONLY prospects_sales_tags_tbl ALTER COLUMN id SET DEFAULT nextval('prospects_sales_tags_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pithadmin
--

ALTER TABLE ONLY ss_prospective_customer_x_sales_tag_tbl ALTER COLUMN id SET DEFAULT nextval('ss_prospective_customer_x_sales_tag_seq'::regclass);


--
-- Name: access_level_tbl_floor_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY access_level_tbl
    ADD CONSTRAINT access_level_tbl_floor_id_key UNIQUE (floor_id, name);


--
-- Name: access_level_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY access_level_tbl
    ADD CONSTRAINT access_level_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: access_level_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY access_level_tbl
    ADD CONSTRAINT access_level_tbl_pkey PRIMARY KEY (id);


--
-- Name: account_tbl_name_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_acct_tbl
    ADD CONSTRAINT account_tbl_name_key UNIQUE (name);


--
-- Name: acct_reconcile_check_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY acct_reconcile_check_tbl
    ADD CONSTRAINT acct_reconcile_check_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: acct_reconcile_check_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY acct_reconcile_check_tbl
    ADD CONSTRAINT acct_reconcile_check_tbl_pkey PRIMARY KEY (id);


--
-- Name: ach_debit_txn_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ach_debit_txn_tbl
    ADD CONSTRAINT ach_debit_txn_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: ach_debit_txn_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ach_debit_txn_tbl
    ADD CONSTRAINT ach_debit_txn_tbl_pkey PRIMARY KEY (id);


--
-- Name: alias_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY alias_tbl
    ADD CONSTRAINT alias_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: alias_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY alias_tbl
    ADD CONSTRAINT alias_tbl_pkey PRIMARY KEY (id);


--
-- Name: amendment_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY amendment_tbl
    ADD CONSTRAINT amendment_tbl_pkey PRIMARY KEY (id);


--
-- Name: bank_account_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY bank_account_tbl
    ADD CONSTRAINT bank_account_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: bank_account_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY bank_account_tbl
    ADD CONSTRAINT bank_account_tbl_pkey PRIMARY KEY (id);


--
-- Name: bing_query_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY bing_query_tbl
    ADD CONSTRAINT bing_query_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: bing_query_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY bing_query_tbl
    ADD CONSTRAINT bing_query_tbl_pkey PRIMARY KEY (id);


--
-- Name: bing_search_result_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY bing_search_result_tbl
    ADD CONSTRAINT bing_search_result_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: bing_search_result_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY bing_search_result_tbl
    ADD CONSTRAINT bing_search_result_tbl_pkey PRIMARY KEY (id);


--
-- Name: broker_referral_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY broker_referral_tbl
    ADD CONSTRAINT broker_referral_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: broker_referral_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY broker_referral_tbl
    ADD CONSTRAINT broker_referral_tbl_pkey PRIMARY KEY (id);


--
-- Name: building_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY building_tbl
    ADD CONSTRAINT building_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: building_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY building_tbl
    ADD CONSTRAINT building_tbl_pkey PRIMARY KEY (id);


--
-- Name: camera_tbl_mac_address_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY camera_tbl
    ADD CONSTRAINT camera_tbl_mac_address_key UNIQUE (mac_address);


--
-- Name: camera_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY camera_tbl
    ADD CONSTRAINT camera_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: camera_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY camera_tbl
    ADD CONSTRAINT camera_tbl_pkey PRIMARY KEY (id);


--
-- Name: company_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY company_tbl
    ADD CONSTRAINT company_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: company_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY company_tbl
    ADD CONSTRAINT company_tbl_pkey PRIMARY KEY (id);


--
-- Name: country_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY country_tbl
    ADD CONSTRAINT country_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: country_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY country_tbl
    ADD CONSTRAINT country_tbl_pkey PRIMARY KEY (id);


--
-- Name: credit_card_payment_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY credit_card_payment_tbl
    ADD CONSTRAINT credit_card_payment_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: credit_card_payment_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY credit_card_payment_tbl
    ADD CONSTRAINT credit_card_payment_tbl_pkey PRIMARY KEY (id);


--
-- Name: credit_card_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY credit_card_tbl
    ADD CONSTRAINT credit_card_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: credit_card_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY credit_card_tbl
    ADD CONSTRAINT credit_card_tbl_pkey PRIMARY KEY (id);


--
-- Name: credit_card_txn_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY credit_card_txn_tbl
    ADD CONSTRAINT credit_card_txn_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: credit_card_txn_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY credit_card_txn_tbl
    ADD CONSTRAINT credit_card_txn_tbl_pkey PRIMARY KEY (id);


--
-- Name: cust_pers_tbl_email_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_pers_tbl
    ADD CONSTRAINT cust_pers_tbl_email_key UNIQUE (email);


--
-- Name: cust_pers_tbl_user_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_pers_tbl
    ADD CONSTRAINT cust_pers_tbl_user_id_key UNIQUE (user_id);


--
-- Name: customer_account_table_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_acct_tbl
    ADD CONSTRAINT customer_account_table_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: customer_account_table_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_acct_tbl
    ADD CONSTRAINT customer_account_table_pkey PRIMARY KEY (id);


--
-- Name: customer_person_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_pers_tbl
    ADD CONSTRAINT customer_person_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: customer_person_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_pers_tbl
    ADD CONSTRAINT customer_person_tbl_pkey PRIMARY KEY (id);


--
-- Name: deposit_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY deposit_tbl
    ADD CONSTRAINT deposit_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: deposit_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY deposit_tbl
    ADD CONSTRAINT deposit_tbl_pkey PRIMARY KEY (id);


--
-- Name: desk_tbl_floor_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY desk_tbl
    ADD CONSTRAINT desk_tbl_floor_id_key UNIQUE (floor_id, assigned_number);


--
-- Name: desk_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY desk_tbl
    ADD CONSTRAINT desk_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: desk_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY desk_tbl
    ADD CONSTRAINT desk_tbl_pkey PRIMARY KEY (id);


--
-- Name: electronic_check_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY electronic_check_tbl
    ADD CONSTRAINT electronic_check_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: electronic_check_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY electronic_check_tbl
    ADD CONSTRAINT electronic_check_tbl_pkey PRIMARY KEY (id);


--
-- Name: email_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY email_tbl
    ADD CONSTRAINT email_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: email_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY email_tbl
    ADD CONSTRAINT email_tbl_pkey PRIMARY KEY (id);


--
-- Name: employee_absence_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY employee_absence_tbl
    ADD CONSTRAINT employee_absence_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: employee_absence_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY employee_absence_tbl
    ADD CONSTRAINT employee_absence_tbl_pkey PRIMARY KEY (id);


--
-- Name: employee_bank_account_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY employee_bank_account_tbl
    ADD CONSTRAINT employee_bank_account_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: employee_bank_account_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY employee_bank_account_tbl
    ADD CONSTRAINT employee_bank_account_tbl_pkey PRIMARY KEY (id);


--
-- Name: employee_time_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY employee_time_tbl
    ADD CONSTRAINT employee_time_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: employee_time_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY employee_time_tbl
    ADD CONSTRAINT employee_time_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_agreement_template_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_agreement_template_tbl
    ADD CONSTRAINT enum_agreement_template_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_amendment_template_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_amendment_template_tbl
    ADD CONSTRAINT enum_amendment_template_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_check_action_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_check_action_tbl
    ADD CONSTRAINT enum_check_action_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: enum_check_action_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_check_action_tbl
    ADD CONSTRAINT enum_check_action_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_payment_destination_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_payment_destination_tbl
    ADD CONSTRAINT enum_payment_destination_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: enum_payment_destination_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_payment_destination_tbl
    ADD CONSTRAINT enum_payment_destination_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_proximity_card_change_cause_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_proximity_card_change_cause_tbl
    ADD CONSTRAINT enum_proximity_card_change_cause_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: enum_proximity_card_change_cause_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_proximity_card_change_cause_tbl
    ADD CONSTRAINT enum_proximity_card_change_cause_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_sales_stage_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_sales_stage_tbl
    ADD CONSTRAINT enum_sales_stage_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: enum_sales_stage_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_sales_stage_tbl
    ADD CONSTRAINT enum_sales_stage_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_sales_tag_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_sales_tag_tbl
    ADD CONSTRAINT enum_sales_tag_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: enum_sales_tag_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_sales_tag_tbl
    ADD CONSTRAINT enum_sales_tag_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_search_engine_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_search_engine_tbl
    ADD CONSTRAINT enum_search_engine_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: enum_search_engine_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_search_engine_tbl
    ADD CONSTRAINT enum_search_engine_tbl_pkey PRIMARY KEY (id);


--
-- Name: enum_search_engine_tbl_value_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_search_engine_tbl
    ADD CONSTRAINT enum_search_engine_tbl_value_key UNIQUE (value);


--
-- Name: event_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY event_tbl
    ADD CONSTRAINT event_tbl_pkey PRIMARY KEY (id);


--
-- Name: expense_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY expense_tbl
    ADD CONSTRAINT expense_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: expense_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY expense_tbl
    ADD CONSTRAINT expense_tbl_pkey PRIMARY KEY (id);


--
-- Name: fax_in_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_in_tbl
    ADD CONSTRAINT fax_in_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: fax_in_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_in_tbl
    ADD CONSTRAINT fax_in_tbl_pkey PRIMARY KEY (id);


--
-- Name: fax_machine_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_machine_tbl
    ADD CONSTRAINT fax_machine_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: fax_machine_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_machine_tbl
    ADD CONSTRAINT fax_machine_tbl_pkey PRIMARY KEY (id);


--
-- Name: fax_number_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_number_tbl
    ADD CONSTRAINT fax_number_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: fax_number_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_number_tbl
    ADD CONSTRAINT fax_number_tbl_pkey PRIMARY KEY (id);


--
-- Name: fax_out_tbl_job_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_out_tbl
    ADD CONSTRAINT fax_out_tbl_job_id_key UNIQUE (job_id);


--
-- Name: fax_out_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_out_tbl
    ADD CONSTRAINT fax_out_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: fax_out_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY fax_out_tbl
    ADD CONSTRAINT fax_out_tbl_pkey PRIMARY KEY (id);


--
-- Name: file_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY file_tbl
    ADD CONSTRAINT file_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: file_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY file_tbl
    ADD CONSTRAINT file_tbl_pkey PRIMARY KEY (id);


--
-- Name: floor_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY floor_tbl
    ADD CONSTRAINT floor_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: floor_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY floor_tbl
    ADD CONSTRAINT floor_tbl_pkey PRIMARY KEY (id);


--
-- Name: friend_referral_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY friend_referral_tbl
    ADD CONSTRAINT friend_referral_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: friend_referral_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY friend_referral_tbl
    ADD CONSTRAINT friend_referral_tbl_pkey PRIMARY KEY (id);


--
-- Name: google_query_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY google_query_tbl
    ADD CONSTRAINT google_query_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: google_query_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY google_query_tbl
    ADD CONSTRAINT google_query_tbl_pkey PRIMARY KEY (id);


--
-- Name: google_search_result_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY google_search_result_tbl
    ADD CONSTRAINT google_search_result_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: google_search_result_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY google_search_result_tbl
    ADD CONSTRAINT google_search_result_tbl_pkey PRIMARY KEY (id);


--
-- Name: guest_person_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY guest_person_tbl
    ADD CONSTRAINT guest_person_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: guest_person_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY guest_person_tbl
    ADD CONSTRAINT guest_person_tbl_pkey PRIMARY KEY (id);


--
-- Name: holiday_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY holiday_tbl
    ADD CONSTRAINT holiday_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: holiday_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY holiday_tbl
    ADD CONSTRAINT holiday_tbl_pkey PRIMARY KEY (id);


--
-- Name: inbound_link_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY inbound_link_tbl
    ADD CONSTRAINT inbound_link_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: inbound_link_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY inbound_link_tbl
    ADD CONSTRAINT inbound_link_tbl_pkey PRIMARY KEY (id);


--
-- Name: incoming_equity_investment_table_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY incoming_equity_investment_tbl
    ADD CONSTRAINT incoming_equity_investment_table_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: incoming_equity_investment_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY incoming_equity_investment_tbl
    ADD CONSTRAINT incoming_equity_investment_tbl_pkey PRIMARY KEY (id);


--
-- Name: incoming_investment_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY incoming_debt_investment_tbl
    ADD CONSTRAINT incoming_investment_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: incoming_investment_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY incoming_debt_investment_tbl
    ADD CONSTRAINT incoming_investment_tbl_pkey PRIMARY KEY (id);


--
-- Name: investor_bank_account_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_bank_account_tbl
    ADD CONSTRAINT investor_bank_account_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: investor_bank_account_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_bank_account_tbl
    ADD CONSTRAINT investor_bank_account_tbl_pkey PRIMARY KEY (id);


--
-- Name: investor_deal_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_deal_tbl
    ADD CONSTRAINT investor_deal_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: investor_deal_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_deal_tbl
    ADD CONSTRAINT investor_deal_tbl_pkey PRIMARY KEY (id);


--
-- Name: investor_participation_tbl_investor_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_participation_tbl
    ADD CONSTRAINT investor_participation_tbl_investor_id_key UNIQUE (investor_id, investor_deal_id);


--
-- Name: investor_participation_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_participation_tbl
    ADD CONSTRAINT investor_participation_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: investor_participation_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_participation_tbl
    ADD CONSTRAINT investor_participation_tbl_pkey PRIMARY KEY (id);


--
-- Name: investor_payment_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_payment_tbl
    ADD CONSTRAINT investor_payment_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: investor_payment_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_payment_tbl
    ADD CONSTRAINT investor_payment_tbl_pkey PRIMARY KEY (id);


--
-- Name: investor_tbl_email_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_tbl
    ADD CONSTRAINT investor_tbl_email_key UNIQUE (email);


--
-- Name: investor_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_tbl
    ADD CONSTRAINT investor_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: investor_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY investor_tbl
    ADD CONSTRAINT investor_tbl_pkey PRIMARY KEY (id);


--
-- Name: ip_address_table_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ip_address_tbl
    ADD CONSTRAINT ip_address_table_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: ip_address_tbl_ip_address_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ip_address_tbl
    ADD CONSTRAINT ip_address_tbl_ip_address_key UNIQUE (ip_address);


--
-- Name: ip_address_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ip_address_tbl
    ADD CONSTRAINT ip_address_tbl_pkey PRIMARY KEY (id);


--
-- Name: isp_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY isp_tbl
    ADD CONSTRAINT isp_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: isp_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY isp_tbl
    ADD CONSTRAINT isp_tbl_pkey PRIMARY KEY (id);


--
-- Name: ledger_account_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ledger_account_tbl
    ADD CONSTRAINT ledger_account_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: ledger_account_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ledger_account_tbl
    ADD CONSTRAINT ledger_account_tbl_pkey PRIMARY KEY (id);


--
-- Name: license_agreement_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY license_agreement_tbl
    ADD CONSTRAINT license_agreement_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: license_agreement_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY license_agreement_tbl
    ADD CONSTRAINT license_agreement_tbl_pkey PRIMARY KEY (id);


--
-- Name: log_event_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY log_event_tbl
    ADD CONSTRAINT log_event_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: log_event_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY log_event_tbl
    ADD CONSTRAINT log_event_tbl_pkey PRIMARY KEY (id);


--
-- Name: mail_package_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY mail_package_tbl
    ADD CONSTRAINT mail_package_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: mail_package_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY mail_package_tbl
    ADD CONSTRAINT mail_package_tbl_pkey PRIMARY KEY (id);


--
-- Name: mailbox_tbl_floor_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY mailbox_tbl
    ADD CONSTRAINT mailbox_tbl_floor_id_key UNIQUE (floor_id, mailbox_number);


--
-- Name: mailbox_tbl_floor_id_key1; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY mailbox_tbl
    ADD CONSTRAINT mailbox_tbl_floor_id_key1 UNIQUE (floor_id, mailbox_number);


--
-- Name: mailbox_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY mailbox_tbl
    ADD CONSTRAINT mailbox_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: mailbox_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY mailbox_tbl
    ADD CONSTRAINT mailbox_tbl_pkey PRIMARY KEY (id);


--
-- Name: marketing_call_source_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY marketing_call_source_tbl
    ADD CONSTRAINT marketing_call_source_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: marketing_call_source_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY marketing_call_source_tbl
    ADD CONSTRAINT marketing_call_source_tbl_pkey PRIMARY KEY (id);


--
-- Name: marketing_call_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY marketing_call_tbl
    ADD CONSTRAINT marketing_call_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: marketing_call_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY marketing_call_tbl
    ADD CONSTRAINT marketing_call_tbl_pkey PRIMARY KEY (id);


--
-- Name: misc_billing_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY misc_billing_tbl
    ADD CONSTRAINT misc_billing_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: misc_billing_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY misc_billing_tbl
    ADD CONSTRAINT misc_billing_tbl_pkey PRIMARY KEY (id);


--
-- Name: monitored_keyword_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY monitored_keyword_tbl
    ADD CONSTRAINT monitored_keyword_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: monitored_keyword_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY monitored_keyword_tbl
    ADD CONSTRAINT monitored_keyword_tbl_pkey PRIMARY KEY (id);


--
-- Name: moving_checklist_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY moving_checklist_tbl
    ADD CONSTRAINT moving_checklist_tbl_pkey PRIMARY KEY (id);


--
-- Name: network_switch_tbl_mac_address_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY network_switch_tbl
    ADD CONSTRAINT network_switch_tbl_mac_address_key UNIQUE (mac_address);


--
-- Name: network_switch_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY network_switch_tbl
    ADD CONSTRAINT network_switch_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: network_switch_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY network_switch_tbl
    ADD CONSTRAINT network_switch_tbl_pkey PRIMARY KEY (id);


--
-- Name: notification_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY notification_tbl
    ADD CONSTRAINT notification_tbl_pkey PRIMARY KEY (id);


--
-- Name: owned_domain_name_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY owned_domain_name_tbl
    ADD CONSTRAINT owned_domain_name_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: owned_domain_name_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY owned_domain_name_tbl
    ADD CONSTRAINT owned_domain_name_tbl_pkey PRIMARY KEY (id);


--
-- Name: payment_method_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY payment_method_tbl
    ADD CONSTRAINT payment_method_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: payment_method_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY payment_method_tbl
    ADD CONSTRAINT payment_method_tbl_pkey PRIMARY KEY (id);


--
-- Name: phone_carrier_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY phone_carrier_tbl
    ADD CONSTRAINT phone_carrier_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: phone_carrier_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY phone_carrier_tbl
    ADD CONSTRAINT phone_carrier_tbl_pkey PRIMARY KEY (id);


--
-- Name: port_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY port_tbl
    ADD CONSTRAINT port_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: port_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY port_tbl
    ADD CONSTRAINT port_tbl_pkey PRIMARY KEY (id);


--
-- Name: prospective_customer_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY prospective_customer_tbl
    ADD CONSTRAINT prospective_customer_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: prospective_customer_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY prospective_customer_tbl
    ADD CONSTRAINT prospective_customer_tbl_pkey PRIMARY KEY (id);


--
-- Name: prospective_investment_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY prospective_investment_tbl
    ADD CONSTRAINT prospective_investment_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: prospective_investment_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY prospective_investment_tbl
    ADD CONSTRAINT prospective_investment_tbl_pkey PRIMARY KEY (id);


--
-- Name: prospects_sales_tags_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY prospects_sales_tags_tbl
    ADD CONSTRAINT prospects_sales_tags_tbl_pkey PRIMARY KEY (id);


--
-- Name: proximity_card_edit_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY proximity_card_edit_tbl
    ADD CONSTRAINT proximity_card_edit_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: proximity_card_edit_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY proximity_card_edit_tbl
    ADD CONSTRAINT proximity_card_edit_tbl_pkey PRIMARY KEY (id);


--
-- Name: proximity_card_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY proximity_card_tbl
    ADD CONSTRAINT proximity_card_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: proximity_card_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY proximity_card_tbl
    ADD CONSTRAINT proximity_card_tbl_pkey PRIMARY KEY (id);


--
-- Name: receivable_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY receivable_tbl
    ADD CONSTRAINT receivable_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: receivable_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY receivable_tbl
    ADD CONSTRAINT receivable_tbl_pkey PRIMARY KEY (id);


--
-- Name: recur_card_txn_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY recur_card_txn_tbl
    ADD CONSTRAINT recur_card_txn_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: recur_card_txn_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY recur_card_txn_tbl
    ADD CONSTRAINT recur_card_txn_tbl_pkey PRIMARY KEY (id);


--
-- Name: recv_payment_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY recv_payment_tbl
    ADD CONSTRAINT recv_payment_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: recv_payment_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY recv_payment_tbl
    ADD CONSTRAINT recv_payment_tbl_pkey PRIMARY KEY (id);


--
-- Name: reservation_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY reservation_tbl
    ADD CONSTRAINT reservation_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: reservation_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY reservation_tbl
    ADD CONSTRAINT reservation_tbl_pkey PRIMARY KEY (id);


--
-- Name: result_item_tbl_result_list_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY result_item_tbl
    ADD CONSTRAINT result_item_tbl_result_list_id_key UNIQUE (result_list_id, index_number);


--
-- Name: result_list_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY result_list_tbl
    ADD CONSTRAINT result_list_tbl_pkey PRIMARY KEY (id);


--
-- Name: room_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY room_tbl
    ADD CONSTRAINT room_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: room_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY room_tbl
    ADD CONSTRAINT room_tbl_pkey PRIMARY KEY (id);


--
-- Name: row_update_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY row_update_tbl
    ADD CONSTRAINT row_update_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: row_update_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY row_update_tbl
    ADD CONSTRAINT row_update_tbl_pkey PRIMARY KEY (id);


--
-- Name: sales_interaction_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY sales_interaction_tbl
    ADD CONSTRAINT sales_interaction_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: sales_interaction_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY sales_interaction_tbl
    ADD CONSTRAINT sales_interaction_tbl_pkey PRIMARY KEY (id);


--
-- Name: scan_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY scan_tbl
    ADD CONSTRAINT scan_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: scan_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY scan_tbl
    ADD CONSTRAINT scan_tbl_pkey PRIMARY KEY (id);


--
-- Name: scanner_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY scanner_tbl
    ADD CONSTRAINT scanner_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: scanner_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY scanner_tbl
    ADD CONSTRAINT scanner_tbl_pkey PRIMARY KEY (id);


--
-- Name: service_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY service_tbl
    ADD CONSTRAINT service_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: service_tbl_name_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY service_tbl
    ADD CONSTRAINT service_tbl_name_key UNIQUE (name);


--
-- Name: service_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY service_tbl
    ADD CONSTRAINT service_tbl_pkey PRIMARY KEY (id);


--
-- Name: session_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY session_tbl
    ADD CONSTRAINT session_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: session_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY session_tbl
    ADD CONSTRAINT session_tbl_pkey PRIMARY KEY (id);


--
-- Name: session_tbl_public_id_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY session_tbl
    ADD CONSTRAINT session_tbl_public_id_key UNIQUE (public_id);


--
-- Name: ss_country_x_prefix_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_country_x_prefix_tbl
    ADD CONSTRAINT ss_country_x_prefix_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: ss_country_x_prefix_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_country_x_prefix_tbl
    ADD CONSTRAINT ss_country_x_prefix_tbl_pkey PRIMARY KEY (id);


--
-- Name: ss_investor_x_role_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_investor_x_role_tbl
    ADD CONSTRAINT ss_investor_x_role_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: ss_investor_x_role_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_investor_x_role_tbl
    ADD CONSTRAINT ss_investor_x_role_tbl_pkey PRIMARY KEY (id);


--
-- Name: ss_vend_pers_x_role_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_vend_pers_x_role_tbl
    ADD CONSTRAINT ss_vend_pers_x_role_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: ss_vend_pers_x_role_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_vend_pers_x_role_tbl
    ADD CONSTRAINT ss_vend_pers_x_role_tbl_pkey PRIMARY KEY (id);


--
-- Name: ss_web_page_x_keyword_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_web_page_x_keyword_tbl
    ADD CONSTRAINT ss_web_page_x_keyword_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: ss_web_page_x_keyword_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY ss_web_page_x_keyword_tbl
    ADD CONSTRAINT ss_web_page_x_keyword_tbl_pkey PRIMARY KEY (id);


--
-- Name: state_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY state_tbl
    ADD CONSTRAINT state_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: state_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY state_tbl
    ADD CONSTRAINT state_tbl_pkey PRIMARY KEY (id);


--
-- Name: subnet_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY subnet_tbl
    ADD CONSTRAINT subnet_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: subnet_tbl_network_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY subnet_tbl
    ADD CONSTRAINT subnet_tbl_network_key UNIQUE (network);


--
-- Name: subnet_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY subnet_tbl
    ADD CONSTRAINT subnet_tbl_pkey PRIMARY KEY (id);


--
-- Name: switch_octet_count_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY switch_octet_count_tbl
    ADD CONSTRAINT switch_octet_count_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: switch_octet_count_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY switch_octet_count_tbl
    ADD CONSTRAINT switch_octet_count_tbl_pkey PRIMARY KEY (id);


--
-- Name: task_note_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY task_note_tbl
    ADD CONSTRAINT task_note_tbl_pkey PRIMARY KEY (id);


--
-- Name: task_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY task_tbl
    ADD CONSTRAINT task_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: task_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY task_tbl
    ADD CONSTRAINT task_tbl_pkey PRIMARY KEY (id);


--
-- Name: tax_document_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY tax_document_tbl
    ADD CONSTRAINT tax_document_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: tax_document_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY tax_document_tbl
    ADD CONSTRAINT tax_document_tbl_pkey PRIMARY KEY (id);


--
-- Name: token_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY token_tbl
    ADD CONSTRAINT token_tbl_pkey PRIMARY KEY (id);


--
-- Name: unique_id; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY enum_intellim_group_tbl
    ADD CONSTRAINT unique_id UNIQUE (id);


--
-- Name: unique_user_id; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY cust_pers_tbl
    ADD CONSTRAINT unique_user_id UNIQUE (user_id);


--
-- Name: user_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY user_tbl
    ADD CONSTRAINT user_tbl_pkey PRIMARY KEY (id);


--
-- Name: vend_acct_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY vend_acct_tbl
    ADD CONSTRAINT vend_acct_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: vend_acct_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY vend_acct_tbl
    ADD CONSTRAINT vend_acct_tbl_pkey PRIMARY KEY (id);


--
-- Name: vend_pers_tbl_email_address_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY vend_pers_tbl
    ADD CONSTRAINT vend_pers_tbl_email_address_key UNIQUE (email);


--
-- Name: vend_pers_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY vend_pers_tbl
    ADD CONSTRAINT vend_pers_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: vend_pers_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY vend_pers_tbl
    ADD CONSTRAINT vend_pers_tbl_pkey PRIMARY KEY (id);


--
-- Name: virtual_office_application_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY virtual_office_application_tbl
    ADD CONSTRAINT virtual_office_application_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: virtual_office_application_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY virtual_office_application_tbl
    ADD CONSTRAINT virtual_office_application_tbl_pkey PRIMARY KEY (id);


--
-- Name: voicecloud_txn_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY voicecloud_txn_tbl
    ADD CONSTRAINT voicecloud_txn_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: voicecloud_txn_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY voicecloud_txn_tbl
    ADD CONSTRAINT voicecloud_txn_tbl_pkey PRIMARY KEY (id);


--
-- Name: voicecloud_txn_tbl_transaction_uid_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY voicecloud_txn_tbl
    ADD CONSTRAINT voicecloud_txn_tbl_transaction_uid_key UNIQUE (transaction_uid);


--
-- Name: watched_domain_name_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY watched_domain_name_tbl
    ADD CONSTRAINT watched_domain_name_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: watched_domain_name_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY watched_domain_name_tbl
    ADD CONSTRAINT watched_domain_name_tbl_pkey PRIMARY KEY (id);


--
-- Name: web_page_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY web_page_tbl
    ADD CONSTRAINT web_page_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: web_page_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY web_page_tbl
    ADD CONSTRAINT web_page_tbl_pkey PRIMARY KEY (id);


--
-- Name: web_posting_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY web_posting_tbl
    ADD CONSTRAINT web_posting_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: web_posting_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY web_posting_tbl
    ADD CONSTRAINT web_posting_tbl_pkey PRIMARY KEY (id);


--
-- Name: yahoo_query_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY yahoo_query_tbl
    ADD CONSTRAINT yahoo_query_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: yahoo_query_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY yahoo_query_tbl
    ADD CONSTRAINT yahoo_query_tbl_pkey PRIMARY KEY (id);


--
-- Name: yahoo_search_result_tbl_ms_access_creation_dt_key; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY yahoo_search_result_tbl
    ADD CONSTRAINT yahoo_search_result_tbl_ms_access_creation_dt_key UNIQUE (ms_access_creation_dt, ms_access_random_num);


--
-- Name: yahoo_search_result_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY yahoo_search_result_tbl
    ADD CONSTRAINT yahoo_search_result_tbl_pkey PRIMARY KEY (id);


--
-- Name: desk_assignment_history_tbl_index01; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX desk_assignment_history_tbl_index01 ON desk_assignment_history_tbl USING btree (desk_id, cust_acct_id, start_date);


--
-- Name: index_id; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX index_id ON enum_intellim_group_tbl USING btree (id);


--
-- Name: index_id1; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX index_id1 ON desk_assignment_history_tbl USING btree (id);


--
-- Name: index_id2; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX index_id2 ON enum_agreement_template_tbl USING btree (id);


--
-- Name: index_id3; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX index_id3 ON enum_amendment_template_tbl USING btree (id);


--
-- Name: index_id4; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX index_id4 ON amendment_tbl USING btree (id);


--
-- Name: voicemail_msgs_tbl_idx1; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx1 ON voicemail_msgs_tbl USING btree (created_epoch);


--
-- Name: voicemail_msgs_tbl_idx2; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx2 ON voicemail_msgs_tbl USING btree (username);


--
-- Name: voicemail_msgs_tbl_idx3; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx3 ON voicemail_msgs_tbl USING btree (domain);


--
-- Name: voicemail_msgs_tbl_idx4; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx4 ON voicemail_msgs_tbl USING btree (uuid);


--
-- Name: voicemail_msgs_tbl_idx5; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx5 ON voicemail_msgs_tbl USING btree (in_folder);


--
-- Name: voicemail_msgs_tbl_idx6; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx6 ON voicemail_msgs_tbl USING btree (read_flags);


--
-- Name: voicemail_msgs_tbl_idx7; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx7 ON voicemail_msgs_tbl USING btree (forwarded_by);


--
-- Name: voicemail_msgs_tbl_idx8; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx8 ON voicemail_msgs_tbl USING btree (read_epoch);


--
-- Name: voicemail_msgs_tbl_idx9; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_msgs_tbl_idx9 ON voicemail_msgs_tbl USING btree (flags);


--
-- Name: voicemail_prefs_idx1; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_prefs_idx1 ON voicemail_prefs_tbl USING btree (username);


--
-- Name: voicemail_prefs_idx2; Type: INDEX; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE INDEX voicemail_prefs_idx2 ON voicemail_prefs_tbl USING btree (domain);


--
-- Name: result_item_tbl_result_list_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pithadmin
--

ALTER TABLE ONLY result_item_tbl
    ADD CONSTRAINT result_item_tbl_result_list_id_fkey FOREIGN KEY (result_list_id) REFERENCES result_list_tbl(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: access_level_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE access_level_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE access_level_seq FROM wchao;
GRANT ALL ON SEQUENCE access_level_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE access_level_seq TO operations;
GRANT SELECT ON SEQUENCE access_level_seq TO czhang;


--
-- Name: access_level_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE access_level_tbl FROM PUBLIC;
REVOKE ALL ON TABLE access_level_tbl FROM pithadmin;
GRANT ALL ON TABLE access_level_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE access_level_tbl TO operations;
GRANT SELECT ON TABLE access_level_tbl TO tenants;
GRANT SELECT ON TABLE access_level_tbl TO czhang;


--
-- Name: account_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE account_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE account_seq FROM wchao;
GRANT ALL ON SEQUENCE account_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE account_seq TO operations;


--
-- Name: cust_acct_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE cust_acct_tbl FROM PUBLIC;
REVOKE ALL ON TABLE cust_acct_tbl FROM pithadmin;
GRANT ALL ON TABLE cust_acct_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE cust_acct_tbl TO operations;
GRANT SELECT ON TABLE cust_acct_tbl TO tenants;
GRANT SELECT ON TABLE cust_acct_tbl TO mo_report;
GRANT SELECT ON TABLE cust_acct_tbl TO jcho;
GRANT SELECT ON TABLE cust_acct_tbl TO mo_kiosk;
GRANT SELECT ON TABLE cust_acct_tbl TO sn_operator;
GRANT SELECT ON TABLE cust_acct_tbl TO ds_operator;
GRANT SELECT,INSERT,UPDATE ON TABLE cust_acct_tbl TO mo_tenant;


--
-- Name: account_vw; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON TABLE account_vw FROM PUBLIC;
REVOKE ALL ON TABLE account_vw FROM wchao;
GRANT ALL ON TABLE account_vw TO wchao;
GRANT SELECT ON TABLE account_vw TO tenants;
GRANT SELECT ON TABLE account_vw TO operations;
GRANT SELECT ON TABLE account_vw TO mo_message;
GRANT SELECT ON TABLE account_vw TO jcho;
GRANT SELECT ON TABLE account_vw TO mo_kiosk;
GRANT SELECT ON TABLE account_vw TO sn_operator;
GRANT SELECT ON TABLE account_vw TO mo_report;


--
-- Name: acct_reconcile_check_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE acct_reconcile_check_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE acct_reconcile_check_seq FROM wchao;
GRANT ALL ON SEQUENCE acct_reconcile_check_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE acct_reconcile_check_seq TO operations;
GRANT SELECT ON SEQUENCE acct_reconcile_check_seq TO czhang;


--
-- Name: acct_reconcile_check_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE acct_reconcile_check_tbl FROM PUBLIC;
REVOKE ALL ON TABLE acct_reconcile_check_tbl FROM pithadmin;
GRANT ALL ON TABLE acct_reconcile_check_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE acct_reconcile_check_tbl TO operations;
GRANT SELECT ON TABLE acct_reconcile_check_tbl TO czhang;


--
-- Name: ach_debit_txn_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE ach_debit_txn_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ach_debit_txn_seq FROM wchao;
GRANT ALL ON SEQUENCE ach_debit_txn_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE ach_debit_txn_seq TO operations;


--
-- Name: ach_debit_txn_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ach_debit_txn_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ach_debit_txn_tbl FROM pithadmin;
GRANT ALL ON TABLE ach_debit_txn_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ach_debit_txn_tbl TO operations;


--
-- Name: alias_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE alias_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE alias_seq FROM wchao;
GRANT ALL ON SEQUENCE alias_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE alias_seq TO tenants;
GRANT SELECT,UPDATE ON SEQUENCE alias_seq TO operations;


--
-- Name: alias_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE alias_tbl FROM PUBLIC;
REVOKE ALL ON TABLE alias_tbl FROM pithadmin;
GRANT ALL ON TABLE alias_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alias_tbl TO operations;
GRANT SELECT ON TABLE alias_tbl TO mo_report;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alias_tbl TO tenants;
GRANT SELECT ON TABLE alias_tbl TO mo_kiosk;


--
-- Name: bank_account_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE bank_account_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bank_account_seq FROM wchao;
GRANT ALL ON SEQUENCE bank_account_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE bank_account_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE bank_account_seq TO tenants;


--
-- Name: bank_account_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE bank_account_tbl FROM PUBLIC;
REVOKE ALL ON TABLE bank_account_tbl FROM pithadmin;
GRANT ALL ON TABLE bank_account_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE bank_account_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE bank_account_tbl TO tenants;


--
-- Name: bing_query_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE bing_query_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bing_query_seq FROM wchao;
GRANT ALL ON SEQUENCE bing_query_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE bing_query_seq TO mo_operator;


--
-- Name: bing_query_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE bing_query_tbl FROM PUBLIC;
REVOKE ALL ON TABLE bing_query_tbl FROM pithadmin;
GRANT ALL ON TABLE bing_query_tbl TO pithadmin;
GRANT ALL ON TABLE bing_query_tbl TO mo_operator;


--
-- Name: bing_search_result_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE bing_search_result_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bing_search_result_seq FROM wchao;
GRANT ALL ON SEQUENCE bing_search_result_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE bing_search_result_seq TO mo_operator;


--
-- Name: bing_search_result_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE bing_search_result_tbl FROM PUBLIC;
REVOKE ALL ON TABLE bing_search_result_tbl FROM pithadmin;
GRANT ALL ON TABLE bing_search_result_tbl TO pithadmin;
GRANT ALL ON TABLE bing_search_result_tbl TO mo_operator;


--
-- Name: broker_referral_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE broker_referral_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE broker_referral_seq FROM wchao;
GRANT ALL ON SEQUENCE broker_referral_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE broker_referral_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE broker_referral_seq TO mo_message;


--
-- Name: broker_referral_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE broker_referral_tbl FROM PUBLIC;
REVOKE ALL ON TABLE broker_referral_tbl FROM pithadmin;
GRANT ALL ON TABLE broker_referral_tbl TO pithadmin;
GRANT ALL ON TABLE broker_referral_tbl TO operations;
GRANT INSERT ON TABLE broker_referral_tbl TO mo_message;


--
-- Name: building_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE building_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE building_seq FROM wchao;
GRANT ALL ON SEQUENCE building_seq TO wchao;


--
-- Name: building_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE building_tbl FROM PUBLIC;
REVOKE ALL ON TABLE building_tbl FROM pithadmin;
GRANT ALL ON TABLE building_tbl TO pithadmin;
GRANT SELECT ON TABLE building_tbl TO mo_operator;
GRANT SELECT ON TABLE building_tbl TO mo_tenant;
GRANT SELECT ON TABLE building_tbl TO jcho;
GRANT SELECT ON TABLE building_tbl TO ds_operator;


--
-- Name: camera_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE camera_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE camera_seq FROM wchao;
GRANT ALL ON SEQUENCE camera_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE camera_seq TO operations;


--
-- Name: camera_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE camera_tbl FROM PUBLIC;
REVOKE ALL ON TABLE camera_tbl FROM pithadmin;
GRANT ALL ON TABLE camera_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE camera_tbl TO operations;
GRANT SELECT,UPDATE ON TABLE camera_tbl TO mo_message;


--
-- Name: company_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE company_tbl FROM PUBLIC;
REVOKE ALL ON TABLE company_tbl FROM pithadmin;
GRANT ALL ON TABLE company_tbl TO pithadmin;
GRANT SELECT ON TABLE company_tbl TO operations;
GRANT SELECT ON TABLE company_tbl TO mo_investor;


--
-- Name: country_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE country_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE country_seq FROM wchao;
GRANT ALL ON SEQUENCE country_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE country_seq TO operations;


--
-- Name: country_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE country_tbl FROM PUBLIC;
REVOKE ALL ON TABLE country_tbl FROM pithadmin;
GRANT ALL ON TABLE country_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE country_tbl TO operations;
GRANT SELECT ON TABLE country_tbl TO tenants;
GRANT SELECT ON TABLE country_tbl TO mo_investor;


--
-- Name: credit_card_payment_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE credit_card_payment_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE credit_card_payment_seq FROM wchao;
GRANT ALL ON SEQUENCE credit_card_payment_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE credit_card_payment_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE credit_card_payment_seq TO tenants;


--
-- Name: credit_card_payment_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE credit_card_payment_tbl FROM PUBLIC;
REVOKE ALL ON TABLE credit_card_payment_tbl FROM pithadmin;
GRANT ALL ON TABLE credit_card_payment_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE credit_card_payment_tbl TO operations;
GRANT SELECT,INSERT ON TABLE credit_card_payment_tbl TO tenants;
GRANT SELECT,INSERT,UPDATE ON TABLE credit_card_payment_tbl TO mo_tenant;


--
-- Name: credit_card_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE credit_card_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE credit_card_seq FROM wchao;
GRANT ALL ON SEQUENCE credit_card_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE credit_card_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE credit_card_seq TO tenants;
GRANT SELECT,UPDATE ON SEQUENCE credit_card_seq TO czhang;


--
-- Name: credit_card_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE credit_card_tbl FROM PUBLIC;
REVOKE ALL ON TABLE credit_card_tbl FROM pithadmin;
GRANT ALL ON TABLE credit_card_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE credit_card_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE credit_card_tbl TO tenants;


--
-- Name: credit_card_txn_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE credit_card_txn_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE credit_card_txn_seq FROM wchao;
GRANT ALL ON SEQUENCE credit_card_txn_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE credit_card_txn_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE credit_card_txn_seq TO tenants;


--
-- Name: credit_card_txn_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE credit_card_txn_tbl FROM PUBLIC;
REVOKE ALL ON TABLE credit_card_txn_tbl FROM pithadmin;
GRANT ALL ON TABLE credit_card_txn_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE credit_card_txn_tbl TO operations;
GRANT SELECT,INSERT ON TABLE credit_card_txn_tbl TO tenants;
GRANT SELECT ON TABLE credit_card_txn_tbl TO czhang;


--
-- Name: cust_pers_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE cust_pers_tbl FROM PUBLIC;
REVOKE ALL ON TABLE cust_pers_tbl FROM pithadmin;
GRANT ALL ON TABLE cust_pers_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE cust_pers_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE cust_pers_tbl TO tenants;
GRANT SELECT ON TABLE cust_pers_tbl TO mo_report;
GRANT SELECT ON TABLE cust_pers_tbl TO mo_message;
GRANT SELECT ON TABLE cust_pers_tbl TO mo_kiosk;
GRANT SELECT,UPDATE ON TABLE cust_pers_tbl TO sn_operator;
GRANT SELECT,UPDATE ON TABLE cust_pers_tbl TO ds_operator;
GRANT SELECT,INSERT,UPDATE ON TABLE cust_pers_tbl TO mo_tenant;


--
-- Name: deposit_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE deposit_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE deposit_seq FROM wchao;
GRANT ALL ON SEQUENCE deposit_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE deposit_seq TO mo_operator;


--
-- Name: deposit_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE deposit_tbl FROM PUBLIC;
REVOKE ALL ON TABLE deposit_tbl FROM pithadmin;
GRANT ALL ON TABLE deposit_tbl TO pithadmin;
GRANT ALL ON TABLE deposit_tbl TO mo_operator;


--
-- Name: desk_assignment_history_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE desk_assignment_history_tbl FROM PUBLIC;
REVOKE ALL ON TABLE desk_assignment_history_tbl FROM pithadmin;
GRANT ALL ON TABLE desk_assignment_history_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE desk_assignment_history_tbl TO operations;
GRANT SELECT,INSERT,UPDATE ON TABLE desk_assignment_history_tbl TO ds_operator;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE desk_assignment_history_tbl TO czhang;


--
-- Name: desk_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE desk_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE desk_seq FROM wchao;
GRANT ALL ON SEQUENCE desk_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE desk_seq TO mo_operator;


--
-- Name: desk_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE desk_tbl FROM PUBLIC;
REVOKE ALL ON TABLE desk_tbl FROM pithadmin;
GRANT ALL ON TABLE desk_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE desk_tbl TO mo_operator;
GRANT SELECT ON TABLE desk_tbl TO mo_tenant;
GRANT SELECT ON TABLE desk_tbl TO mo_report;
GRANT SELECT,INSERT,UPDATE ON TABLE desk_tbl TO ds_operator;
GRANT SELECT ON TABLE desk_tbl TO czhang;


--
-- Name: document_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE document_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE document_seq FROM wchao;
GRANT ALL ON SEQUENCE document_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE document_seq TO mo_message;


--
-- Name: fax_in_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE fax_in_tbl FROM PUBLIC;
REVOKE ALL ON TABLE fax_in_tbl FROM pithadmin;
GRANT ALL ON TABLE fax_in_tbl TO pithadmin;
GRANT INSERT ON TABLE fax_in_tbl TO mo_message;
GRANT SELECT,DELETE,UPDATE ON TABLE fax_in_tbl TO operations;
GRANT SELECT,DELETE,UPDATE ON TABLE fax_in_tbl TO tenants;


--
-- Name: scan_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE scan_tbl FROM PUBLIC;
REVOKE ALL ON TABLE scan_tbl FROM pithadmin;
GRANT ALL ON TABLE scan_tbl TO pithadmin;
GRANT INSERT ON TABLE scan_tbl TO mo_message;
GRANT SELECT,DELETE,UPDATE ON TABLE scan_tbl TO operations;
GRANT SELECT,DELETE,UPDATE ON TABLE scan_tbl TO tenants;


--
-- Name: document_vw; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON TABLE document_vw FROM PUBLIC;
REVOKE ALL ON TABLE document_vw FROM wchao;
GRANT ALL ON TABLE document_vw TO wchao;
GRANT SELECT ON TABLE document_vw TO operations;
GRANT SELECT ON TABLE document_vw TO tenants;


--
-- Name: electronic_check_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE electronic_check_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE electronic_check_seq FROM wchao;
GRANT ALL ON SEQUENCE electronic_check_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE electronic_check_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE electronic_check_seq TO tenants;


--
-- Name: electronic_check_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE electronic_check_tbl FROM PUBLIC;
REVOKE ALL ON TABLE electronic_check_tbl FROM pithadmin;
GRANT ALL ON TABLE electronic_check_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE electronic_check_tbl TO operations;
GRANT SELECT,INSERT ON TABLE electronic_check_tbl TO tenants;
GRANT SELECT ON TABLE electronic_check_tbl TO czhang;


--
-- Name: email_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE email_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE email_seq FROM wchao;
GRANT ALL ON SEQUENCE email_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE email_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE email_seq TO tenants;
GRANT SELECT,UPDATE ON SEQUENCE email_seq TO mo_investor;


--
-- Name: email_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE email_tbl FROM PUBLIC;
REVOKE ALL ON TABLE email_tbl FROM pithadmin;
GRANT ALL ON TABLE email_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE email_tbl TO operations;
GRANT INSERT ON TABLE email_tbl TO tenants;
GRANT INSERT ON TABLE email_tbl TO mo_investor;


--
-- Name: employee_absence_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE employee_absence_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE employee_absence_seq FROM wchao;
GRANT ALL ON SEQUENCE employee_absence_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE employee_absence_seq TO operations;


--
-- Name: employee_absence_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE employee_absence_tbl FROM PUBLIC;
REVOKE ALL ON TABLE employee_absence_tbl FROM pithadmin;
GRANT ALL ON TABLE employee_absence_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE employee_absence_tbl TO operations;


--
-- Name: employee_bank_account_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE employee_bank_account_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE employee_bank_account_seq FROM wchao;
GRANT ALL ON SEQUENCE employee_bank_account_seq TO wchao;


--
-- Name: employee_bank_account_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE employee_bank_account_tbl FROM PUBLIC;
REVOKE ALL ON TABLE employee_bank_account_tbl FROM pithadmin;
GRANT ALL ON TABLE employee_bank_account_tbl TO pithadmin;


--
-- Name: person_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE person_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE person_seq FROM wchao;
GRANT ALL ON SEQUENCE person_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE person_seq TO tenants;
GRANT SELECT,UPDATE ON SEQUENCE person_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE person_seq TO czhang;


--
-- Name: employee_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE employee_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE employee_seq FROM wchao;
GRANT ALL ON SEQUENCE employee_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE employee_seq TO czhang;


--
-- Name: employee_time_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE employee_time_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE employee_time_seq FROM wchao;
GRANT ALL ON SEQUENCE employee_time_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE employee_time_seq TO operations;


--
-- Name: employee_time_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE employee_time_tbl FROM PUBLIC;
REVOKE ALL ON TABLE employee_time_tbl FROM pithadmin;
GRANT ALL ON TABLE employee_time_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE employee_time_tbl TO operations;


--
-- Name: enum_check_action_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE enum_check_action_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE enum_check_action_seq FROM wchao;
GRANT ALL ON SEQUENCE enum_check_action_seq TO wchao;


--
-- Name: enum_check_action_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE enum_check_action_tbl FROM PUBLIC;
REVOKE ALL ON TABLE enum_check_action_tbl FROM pithadmin;
GRANT ALL ON TABLE enum_check_action_tbl TO pithadmin;
GRANT SELECT ON TABLE enum_check_action_tbl TO operations;


--
-- Name: enum_payment_destination_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE enum_payment_destination_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE enum_payment_destination_seq FROM wchao;
GRANT ALL ON SEQUENCE enum_payment_destination_seq TO wchao;


--
-- Name: enum_payment_destination_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE enum_payment_destination_tbl FROM PUBLIC;
REVOKE ALL ON TABLE enum_payment_destination_tbl FROM pithadmin;
GRANT ALL ON TABLE enum_payment_destination_tbl TO pithadmin;
GRANT SELECT ON TABLE enum_payment_destination_tbl TO operations;


--
-- Name: enum_proximity_card_change_cause_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE enum_proximity_card_change_cause_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE enum_proximity_card_change_cause_seq FROM wchao;
GRANT ALL ON SEQUENCE enum_proximity_card_change_cause_seq TO wchao;


--
-- Name: enum_proximity_card_change_cause_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE enum_proximity_card_change_cause_tbl FROM PUBLIC;
REVOKE ALL ON TABLE enum_proximity_card_change_cause_tbl FROM pithadmin;
GRANT ALL ON TABLE enum_proximity_card_change_cause_tbl TO pithadmin;
GRANT SELECT ON TABLE enum_proximity_card_change_cause_tbl TO operations;


--
-- Name: enum_sales_stage_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE enum_sales_stage_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE enum_sales_stage_seq FROM wchao;
GRANT ALL ON SEQUENCE enum_sales_stage_seq TO wchao;


--
-- Name: enum_sales_stage_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE enum_sales_stage_tbl FROM PUBLIC;
REVOKE ALL ON TABLE enum_sales_stage_tbl FROM pithadmin;
GRANT ALL ON TABLE enum_sales_stage_tbl TO pithadmin;
GRANT SELECT ON TABLE enum_sales_stage_tbl TO operations;


--
-- Name: enum_sales_tag_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE enum_sales_tag_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE enum_sales_tag_seq FROM wchao;
GRANT ALL ON SEQUENCE enum_sales_tag_seq TO wchao;


--
-- Name: enum_sales_tag_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE enum_sales_tag_tbl FROM PUBLIC;
REVOKE ALL ON TABLE enum_sales_tag_tbl FROM pithadmin;
GRANT ALL ON TABLE enum_sales_tag_tbl TO pithadmin;
GRANT SELECT ON TABLE enum_sales_tag_tbl TO operations;


--
-- Name: enum_search_engine_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE enum_search_engine_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE enum_search_engine_seq FROM wchao;
GRANT ALL ON SEQUENCE enum_search_engine_seq TO wchao;


--
-- Name: enum_search_engine_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE enum_search_engine_tbl FROM PUBLIC;
REVOKE ALL ON TABLE enum_search_engine_tbl FROM pithadmin;
GRANT ALL ON TABLE enum_search_engine_tbl TO pithadmin;
GRANT SELECT ON TABLE enum_search_engine_tbl TO operations;


--
-- Name: event_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE event_tbl FROM PUBLIC;
REVOKE ALL ON TABLE event_tbl FROM pithadmin;
GRANT ALL ON TABLE event_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE event_tbl TO mo_tenant;
GRANT ALL ON TABLE event_tbl TO wchao;


--
-- Name: expense_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE expense_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE expense_seq FROM wchao;
GRANT ALL ON SEQUENCE expense_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE expense_seq TO operations;


--
-- Name: expense_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE expense_tbl FROM PUBLIC;
REVOKE ALL ON TABLE expense_tbl FROM pithadmin;
GRANT ALL ON TABLE expense_tbl TO pithadmin;
GRANT ALL ON TABLE expense_tbl TO operations;


--
-- Name: fax_in_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE fax_in_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE fax_in_seq FROM wchao;
GRANT ALL ON SEQUENCE fax_in_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE fax_in_seq TO mo_message;


--
-- Name: fax_machine_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE fax_machine_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE fax_machine_seq FROM wchao;
GRANT ALL ON SEQUENCE fax_machine_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE fax_machine_seq TO mo_operator;
GRANT SELECT,UPDATE ON SEQUENCE fax_machine_seq TO sn_operator;


--
-- Name: fax_machine_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE fax_machine_tbl FROM PUBLIC;
REVOKE ALL ON TABLE fax_machine_tbl FROM pithadmin;
GRANT ALL ON TABLE fax_machine_tbl TO pithadmin;
GRANT ALL ON TABLE fax_machine_tbl TO mo_operator;
GRANT SELECT ON TABLE fax_machine_tbl TO mo_tenant;
GRANT SELECT ON TABLE fax_machine_tbl TO mo_message;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE fax_machine_tbl TO sn_operator;


--
-- Name: fax_number_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE fax_number_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE fax_number_seq FROM wchao;
GRANT ALL ON SEQUENCE fax_number_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE fax_number_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE fax_number_seq TO sn_operator;


--
-- Name: fax_number_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE fax_number_tbl FROM PUBLIC;
REVOKE ALL ON TABLE fax_number_tbl FROM pithadmin;
GRANT ALL ON TABLE fax_number_tbl TO pithadmin;
GRANT ALL ON TABLE fax_number_tbl TO operations;
GRANT SELECT ON TABLE fax_number_tbl TO tenants;
GRANT SELECT ON TABLE fax_number_tbl TO mo_message;
GRANT SELECT,INSERT,UPDATE ON TABLE fax_number_tbl TO sn_operator;


--
-- Name: fax_out_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE fax_out_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE fax_out_seq FROM wchao;
GRANT ALL ON SEQUENCE fax_out_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE fax_out_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE fax_out_seq TO tenants;
GRANT SELECT,UPDATE ON SEQUENCE fax_out_seq TO mo_message;


--
-- Name: fax_out_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE fax_out_tbl FROM PUBLIC;
REVOKE ALL ON TABLE fax_out_tbl FROM pithadmin;
GRANT ALL ON TABLE fax_out_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE fax_out_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE fax_out_tbl TO tenants;
GRANT SELECT,INSERT,UPDATE ON TABLE fax_out_tbl TO mo_message;
GRANT SELECT,INSERT,UPDATE ON TABLE fax_out_tbl TO mo_tenant;


--
-- Name: file_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE file_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE file_seq FROM wchao;
GRANT ALL ON SEQUENCE file_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE file_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE file_seq TO mo_message;
GRANT SELECT,UPDATE ON SEQUENCE file_seq TO sn_message;
GRANT SELECT,UPDATE ON SEQUENCE file_seq TO tenants;


--
-- Name: file_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE file_tbl FROM PUBLIC;
REVOKE ALL ON TABLE file_tbl FROM pithadmin;
GRANT ALL ON TABLE file_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE file_tbl TO operations;
GRANT SELECT ON TABLE file_tbl TO mo_investor;
GRANT INSERT ON TABLE file_tbl TO mo_message;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE file_tbl TO tenants;
GRANT INSERT ON TABLE file_tbl TO sn_message;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE file_tbl TO sn_operator;


--
-- Name: floor_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE floor_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE floor_seq FROM wchao;
GRANT ALL ON SEQUENCE floor_seq TO wchao;


--
-- Name: floor_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE floor_tbl FROM PUBLIC;
REVOKE ALL ON TABLE floor_tbl FROM pithadmin;
GRANT ALL ON TABLE floor_tbl TO pithadmin;
GRANT SELECT ON TABLE floor_tbl TO mo_operator;
GRANT SELECT ON TABLE floor_tbl TO mo_tenant;
GRANT SELECT ON TABLE floor_tbl TO jcho;
GRANT SELECT ON TABLE floor_tbl TO mo_kiosk;
GRANT SELECT ON TABLE floor_tbl TO sn_operator;
GRANT SELECT ON TABLE floor_tbl TO ds_operator;


--
-- Name: friend_referral_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE friend_referral_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE friend_referral_seq FROM wchao;
GRANT ALL ON SEQUENCE friend_referral_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE friend_referral_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE friend_referral_seq TO mo_message;


--
-- Name: friend_referral_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE friend_referral_tbl FROM PUBLIC;
REVOKE ALL ON TABLE friend_referral_tbl FROM pithadmin;
GRANT ALL ON TABLE friend_referral_tbl TO pithadmin;
GRANT ALL ON TABLE friend_referral_tbl TO operations;
GRANT INSERT ON TABLE friend_referral_tbl TO mo_message;


--
-- Name: google_query_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE google_query_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE google_query_seq FROM wchao;
GRANT ALL ON SEQUENCE google_query_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE google_query_seq TO operations;


--
-- Name: google_query_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE google_query_tbl FROM PUBLIC;
REVOKE ALL ON TABLE google_query_tbl FROM pithadmin;
GRANT ALL ON TABLE google_query_tbl TO pithadmin;
GRANT ALL ON TABLE google_query_tbl TO operations;
GRANT SELECT ON TABLE google_query_tbl TO mo_vendor;


--
-- Name: google_search_result_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE google_search_result_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE google_search_result_seq FROM wchao;
GRANT ALL ON SEQUENCE google_search_result_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE google_search_result_seq TO operations;


--
-- Name: google_search_result_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE google_search_result_tbl FROM PUBLIC;
REVOKE ALL ON TABLE google_search_result_tbl FROM pithadmin;
GRANT ALL ON TABLE google_search_result_tbl TO pithadmin;
GRANT ALL ON TABLE google_search_result_tbl TO operations;
GRANT SELECT ON TABLE google_search_result_tbl TO mo_vendor;


--
-- Name: guest_person_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE guest_person_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE guest_person_seq FROM wchao;
GRANT ALL ON SEQUENCE guest_person_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE guest_person_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE guest_person_seq TO tenants;


--
-- Name: guest_person_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE guest_person_tbl FROM PUBLIC;
REVOKE ALL ON TABLE guest_person_tbl FROM pithadmin;
GRANT ALL ON TABLE guest_person_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE guest_person_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE guest_person_tbl TO tenants;


--
-- Name: holiday_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE holiday_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE holiday_seq FROM wchao;
GRANT ALL ON SEQUENCE holiday_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE holiday_seq TO operations;


--
-- Name: holiday_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE holiday_tbl FROM PUBLIC;
REVOKE ALL ON TABLE holiday_tbl FROM pithadmin;
GRANT ALL ON TABLE holiday_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE holiday_tbl TO operations;
GRANT SELECT ON TABLE holiday_tbl TO mo_report;


--
-- Name: inbound_link_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE inbound_link_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE inbound_link_seq FROM wchao;
GRANT ALL ON SEQUENCE inbound_link_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE inbound_link_seq TO operations;


--
-- Name: inbound_link_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE inbound_link_tbl FROM PUBLIC;
REVOKE ALL ON TABLE inbound_link_tbl FROM pithadmin;
GRANT ALL ON TABLE inbound_link_tbl TO pithadmin;
GRANT ALL ON TABLE inbound_link_tbl TO operations;
GRANT SELECT ON TABLE inbound_link_tbl TO mo_vendor;


--
-- Name: investor_bank_account_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE investor_bank_account_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE investor_bank_account_seq FROM wchao;
GRANT ALL ON SEQUENCE investor_bank_account_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE investor_bank_account_seq TO mo_investor;


--
-- Name: investor_bank_account_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE investor_bank_account_tbl FROM PUBLIC;
REVOKE ALL ON TABLE investor_bank_account_tbl FROM pithadmin;
GRANT ALL ON TABLE investor_bank_account_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE investor_bank_account_tbl TO mo_investor;


--
-- Name: investor_deal_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE investor_deal_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE investor_deal_seq FROM wchao;
GRANT ALL ON SEQUENCE investor_deal_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE investor_deal_seq TO operations;


--
-- Name: investor_deal_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE investor_deal_tbl FROM PUBLIC;
REVOKE ALL ON TABLE investor_deal_tbl FROM pithadmin;
GRANT ALL ON TABLE investor_deal_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE investor_deal_tbl TO operations;
GRANT SELECT ON TABLE investor_deal_tbl TO mo_investor;


--
-- Name: investor_participation_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE investor_participation_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE investor_participation_seq FROM wchao;
GRANT ALL ON SEQUENCE investor_participation_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE investor_participation_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE investor_participation_seq TO mo_investor;


--
-- Name: investor_participation_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE investor_participation_tbl FROM PUBLIC;
REVOKE ALL ON TABLE investor_participation_tbl FROM pithadmin;
GRANT ALL ON TABLE investor_participation_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE investor_participation_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE investor_participation_tbl TO mo_investor;


--
-- Name: investor_payment_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE investor_payment_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE investor_payment_seq FROM wchao;
GRANT ALL ON SEQUENCE investor_payment_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE investor_payment_seq TO operations;


--
-- Name: investor_payment_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE investor_payment_tbl FROM PUBLIC;
REVOKE ALL ON TABLE investor_payment_tbl FROM pithadmin;
GRANT ALL ON TABLE investor_payment_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE investor_payment_tbl TO operations;
GRANT SELECT ON TABLE investor_payment_tbl TO mo_investor;


--
-- Name: investor_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE investor_tbl FROM PUBLIC;
REVOKE ALL ON TABLE investor_tbl FROM pithadmin;
GRANT ALL ON TABLE investor_tbl TO pithadmin;
GRANT SELECT,UPDATE ON TABLE investor_tbl TO mo_investor;


--
-- Name: ip_address_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ip_address_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ip_address_tbl FROM pithadmin;
GRANT ALL ON TABLE ip_address_tbl TO pithadmin;
GRANT SELECT,UPDATE ON TABLE ip_address_tbl TO tenants;
GRANT SELECT,UPDATE ON TABLE ip_address_tbl TO operations;
GRANT SELECT,UPDATE ON TABLE ip_address_tbl TO sn_operator;
GRANT SELECT ON TABLE ip_address_tbl TO czhang;


--
-- Name: isp_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE isp_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE isp_seq FROM wchao;
GRANT ALL ON SEQUENCE isp_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE isp_seq TO mo_operator;


--
-- Name: isp_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE isp_tbl FROM PUBLIC;
REVOKE ALL ON TABLE isp_tbl FROM pithadmin;
GRANT ALL ON TABLE isp_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE isp_tbl TO mo_operator;


--
-- Name: ledger_account_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE ledger_account_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ledger_account_seq FROM wchao;
GRANT ALL ON SEQUENCE ledger_account_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE ledger_account_seq TO operations;


--
-- Name: ledger_account_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ledger_account_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ledger_account_tbl FROM pithadmin;
GRANT ALL ON TABLE ledger_account_tbl TO pithadmin;
GRANT ALL ON TABLE ledger_account_tbl TO operations;


--
-- Name: license_agreement_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE license_agreement_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE license_agreement_seq FROM wchao;
GRANT ALL ON SEQUENCE license_agreement_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE license_agreement_seq TO operations;


--
-- Name: license_agreement_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE license_agreement_tbl FROM PUBLIC;
REVOKE ALL ON TABLE license_agreement_tbl FROM pithadmin;
GRANT ALL ON TABLE license_agreement_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE license_agreement_tbl TO operations;


--
-- Name: log_event_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE log_event_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE log_event_seq FROM wchao;
GRANT ALL ON SEQUENCE log_event_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE log_event_seq TO mo_operator;
GRANT SELECT,UPDATE ON SEQUENCE log_event_seq TO mo_message;
GRANT SELECT,UPDATE ON SEQUENCE log_event_seq TO sn_message;
GRANT SELECT,UPDATE ON SEQUENCE log_event_seq TO sn_operator;


--
-- Name: log_event_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE log_event_tbl FROM PUBLIC;
REVOKE ALL ON TABLE log_event_tbl FROM pithadmin;
GRANT ALL ON TABLE log_event_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE log_event_tbl TO mo_message;
GRANT SELECT,INSERT,UPDATE ON TABLE log_event_tbl TO sn_message;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE log_event_tbl TO mo_operator;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE log_event_tbl TO sn_operator;


--
-- Name: mail_package_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE mail_package_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_package_seq FROM wchao;
GRANT ALL ON SEQUENCE mail_package_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE mail_package_seq TO mo_operator;
GRANT SELECT,UPDATE ON SEQUENCE mail_package_seq TO czhang;


--
-- Name: mail_package_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE mail_package_tbl FROM PUBLIC;
REVOKE ALL ON TABLE mail_package_tbl FROM pithadmin;
GRANT ALL ON TABLE mail_package_tbl TO pithadmin;
GRANT ALL ON TABLE mail_package_tbl TO mo_operator;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_package_tbl TO czhang;
GRANT SELECT ON TABLE mail_package_tbl TO tenants;


--
-- Name: mailbox_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE mailbox_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mailbox_seq FROM wchao;
GRANT ALL ON SEQUENCE mailbox_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE mailbox_seq TO operations;


--
-- Name: mailbox_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE mailbox_tbl FROM PUBLIC;
REVOKE ALL ON TABLE mailbox_tbl FROM pithadmin;
GRANT ALL ON TABLE mailbox_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mailbox_tbl TO operations;
GRANT SELECT ON TABLE mailbox_tbl TO mo_report;
GRANT SELECT,UPDATE ON TABLE mailbox_tbl TO tenants;


--
-- Name: marketing_call_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE marketing_call_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE marketing_call_seq FROM wchao;
GRANT ALL ON SEQUENCE marketing_call_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE marketing_call_seq TO operations;


--
-- Name: marketing_call_source_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE marketing_call_source_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE marketing_call_source_seq FROM wchao;
GRANT ALL ON SEQUENCE marketing_call_source_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE marketing_call_source_seq TO operations;


--
-- Name: marketing_call_source_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE marketing_call_source_tbl FROM PUBLIC;
REVOKE ALL ON TABLE marketing_call_source_tbl FROM pithadmin;
GRANT ALL ON TABLE marketing_call_source_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE marketing_call_source_tbl TO operations;


--
-- Name: marketing_call_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE marketing_call_tbl FROM PUBLIC;
REVOKE ALL ON TABLE marketing_call_tbl FROM pithadmin;
GRANT ALL ON TABLE marketing_call_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE marketing_call_tbl TO operations;


--
-- Name: misc_billing_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE misc_billing_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE misc_billing_seq FROM wchao;
GRANT ALL ON SEQUENCE misc_billing_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE misc_billing_seq TO operations;


--
-- Name: misc_billing_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE misc_billing_tbl FROM PUBLIC;
REVOKE ALL ON TABLE misc_billing_tbl FROM pithadmin;
GRANT ALL ON TABLE misc_billing_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE misc_billing_tbl TO operations;


--
-- Name: monitored_keyword_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE monitored_keyword_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE monitored_keyword_seq FROM wchao;
GRANT ALL ON SEQUENCE monitored_keyword_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE monitored_keyword_seq TO operations;


--
-- Name: monitored_keyword_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE monitored_keyword_tbl FROM PUBLIC;
REVOKE ALL ON TABLE monitored_keyword_tbl FROM pithadmin;
GRANT ALL ON TABLE monitored_keyword_tbl TO pithadmin;
GRANT ALL ON TABLE monitored_keyword_tbl TO operations;
GRANT SELECT ON TABLE monitored_keyword_tbl TO mo_vendor;


--
-- Name: moving_checklist_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE moving_checklist_tbl FROM PUBLIC;
REVOKE ALL ON TABLE moving_checklist_tbl FROM pithadmin;
GRANT ALL ON TABLE moving_checklist_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE moving_checklist_tbl TO mo_tenant;
GRANT ALL ON TABLE moving_checklist_tbl TO wchao;


--
-- Name: network_switch_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE network_switch_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE network_switch_seq FROM wchao;
GRANT ALL ON SEQUENCE network_switch_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE network_switch_seq TO operations;


--
-- Name: network_switch_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE network_switch_tbl FROM PUBLIC;
REVOKE ALL ON TABLE network_switch_tbl FROM pithadmin;
GRANT ALL ON TABLE network_switch_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE network_switch_tbl TO operations;
GRANT SELECT ON TABLE network_switch_tbl TO mo_daemon;


--
-- Name: notification_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE notification_tbl FROM PUBLIC;
REVOKE ALL ON TABLE notification_tbl FROM pithadmin;
GRANT ALL ON TABLE notification_tbl TO pithadmin;
GRANT ALL ON TABLE notification_tbl TO wchao;
GRANT SELECT ON TABLE notification_tbl TO mo_tenant;


--
-- Name: owned_domain_name_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE owned_domain_name_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE owned_domain_name_seq FROM wchao;
GRANT ALL ON SEQUENCE owned_domain_name_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE owned_domain_name_seq TO operations;


--
-- Name: owned_domain_name_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE owned_domain_name_tbl FROM PUBLIC;
REVOKE ALL ON TABLE owned_domain_name_tbl FROM pithadmin;
GRANT ALL ON TABLE owned_domain_name_tbl TO pithadmin;
GRANT ALL ON TABLE owned_domain_name_tbl TO operations;
GRANT SELECT ON TABLE owned_domain_name_tbl TO mo_vendor;


--
-- Name: payment_method_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE payment_method_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE payment_method_seq FROM wchao;
GRANT ALL ON SEQUENCE payment_method_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE payment_method_seq TO mo_operator;


--
-- Name: payment_method_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE payment_method_tbl FROM PUBLIC;
REVOKE ALL ON TABLE payment_method_tbl FROM pithadmin;
GRANT ALL ON TABLE payment_method_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE payment_method_tbl TO mo_operator;


--
-- Name: phone_carrier_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE phone_carrier_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE phone_carrier_seq FROM wchao;
GRANT ALL ON SEQUENCE phone_carrier_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE phone_carrier_seq TO mo_operator;


--
-- Name: phone_carrier_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE phone_carrier_tbl FROM PUBLIC;
REVOKE ALL ON TABLE phone_carrier_tbl FROM pithadmin;
GRANT ALL ON TABLE phone_carrier_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE phone_carrier_tbl TO mo_operator;
GRANT SELECT ON TABLE phone_carrier_tbl TO tenants;


--
-- Name: port_seq; Type: ACL; Schema: public; Owner: czhang
--

REVOKE ALL ON SEQUENCE port_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE port_seq FROM czhang;
GRANT ALL ON SEQUENCE port_seq TO czhang;
GRANT ALL ON SEQUENCE port_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE port_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE port_seq TO ds_operator;


--
-- Name: port_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE port_tbl FROM PUBLIC;
REVOKE ALL ON TABLE port_tbl FROM pithadmin;
GRANT ALL ON TABLE port_tbl TO pithadmin;
GRANT ALL ON TABLE port_tbl TO wchao;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE port_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE port_tbl TO ds_operator;


--
-- Name: prospective_customer_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE prospective_customer_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE prospective_customer_seq FROM wchao;
GRANT ALL ON SEQUENCE prospective_customer_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE prospective_customer_seq TO mo_message;
GRANT SELECT,UPDATE ON SEQUENCE prospective_customer_seq TO operations;


--
-- Name: prospective_customer_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE prospective_customer_tbl FROM PUBLIC;
REVOKE ALL ON TABLE prospective_customer_tbl FROM pithadmin;
GRANT ALL ON TABLE prospective_customer_tbl TO pithadmin;
GRANT INSERT ON TABLE prospective_customer_tbl TO mo_message;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE prospective_customer_tbl TO operations;
GRANT SELECT ON TABLE prospective_customer_tbl TO mo_report;


--
-- Name: proximity_card_edit_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE proximity_card_edit_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE proximity_card_edit_seq FROM wchao;
GRANT ALL ON SEQUENCE proximity_card_edit_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE proximity_card_edit_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE proximity_card_edit_seq TO tenants;


--
-- Name: proximity_card_edit_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE proximity_card_edit_tbl FROM PUBLIC;
REVOKE ALL ON TABLE proximity_card_edit_tbl FROM pithadmin;
GRANT ALL ON TABLE proximity_card_edit_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE proximity_card_edit_tbl TO operations;


--
-- Name: proximity_card_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE proximity_card_tbl FROM PUBLIC;
REVOKE ALL ON TABLE proximity_card_tbl FROM pithadmin;
GRANT ALL ON TABLE proximity_card_tbl TO pithadmin;
GRANT SELECT,UPDATE ON TABLE proximity_card_tbl TO operations;
GRANT SELECT,UPDATE ON TABLE proximity_card_tbl TO tenants;
GRANT SELECT,INSERT ON TABLE proximity_card_tbl TO mo_tenant;


--
-- Name: receivable_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE receivable_tbl FROM PUBLIC;
REVOKE ALL ON TABLE receivable_tbl FROM pithadmin;
GRANT ALL ON TABLE receivable_tbl TO pithadmin;
GRANT SELECT ON TABLE receivable_tbl TO operations;


--
-- Name: recur_card_txn_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE recur_card_txn_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE recur_card_txn_seq FROM wchao;
GRANT ALL ON SEQUENCE recur_card_txn_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE recur_card_txn_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE recur_card_txn_seq TO czhang;


--
-- Name: recur_card_txn_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE recur_card_txn_tbl FROM PUBLIC;
REVOKE ALL ON TABLE recur_card_txn_tbl FROM pithadmin;
GRANT ALL ON TABLE recur_card_txn_tbl TO pithadmin;
GRANT ALL ON TABLE recur_card_txn_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE recur_card_txn_tbl TO czhang;


--
-- Name: recv_payment_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE recv_payment_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE recv_payment_seq FROM wchao;
GRANT ALL ON SEQUENCE recv_payment_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE recv_payment_seq TO operations;


--
-- Name: recv_payment_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE recv_payment_tbl FROM PUBLIC;
REVOKE ALL ON TABLE recv_payment_tbl FROM pithadmin;
GRANT ALL ON TABLE recv_payment_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE recv_payment_tbl TO operations;
GRANT SELECT ON TABLE recv_payment_tbl TO tenants;


--
-- Name: reservation_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE reservation_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE reservation_seq FROM wchao;
GRANT ALL ON SEQUENCE reservation_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE reservation_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE reservation_seq TO tenants;
GRANT SELECT,UPDATE ON SEQUENCE reservation_seq TO mo_kiosk;


--
-- Name: reservation_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE reservation_tbl FROM PUBLIC;
REVOKE ALL ON TABLE reservation_tbl FROM pithadmin;
GRANT ALL ON TABLE reservation_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE reservation_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE reservation_tbl TO tenants;
GRANT SELECT ON TABLE reservation_tbl TO jcho;
GRANT SELECT,INSERT ON TABLE reservation_tbl TO mo_kiosk;


--
-- Name: result_item_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE result_item_tbl FROM PUBLIC;
REVOKE ALL ON TABLE result_item_tbl FROM pithadmin;
GRANT ALL ON TABLE result_item_tbl TO pithadmin;
GRANT ALL ON TABLE result_item_tbl TO operations;
GRANT ALL ON TABLE result_item_tbl TO tenants;


--
-- Name: result_list_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE result_list_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE result_list_seq FROM wchao;
GRANT ALL ON SEQUENCE result_list_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE result_list_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE result_list_seq TO tenants;


--
-- Name: result_list_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE result_list_tbl FROM PUBLIC;
REVOKE ALL ON TABLE result_list_tbl FROM pithadmin;
GRANT ALL ON TABLE result_list_tbl TO pithadmin;
GRANT ALL ON TABLE result_list_tbl TO operations;
GRANT ALL ON TABLE result_list_tbl TO tenants;


--
-- Name: room_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE room_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE room_seq FROM wchao;
GRANT ALL ON SEQUENCE room_seq TO wchao;


--
-- Name: room_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE room_tbl FROM PUBLIC;
REVOKE ALL ON TABLE room_tbl FROM pithadmin;
GRANT ALL ON TABLE room_tbl TO pithadmin;
GRANT SELECT ON TABLE room_tbl TO operations;
GRANT SELECT ON TABLE room_tbl TO tenants;
GRANT SELECT ON TABLE room_tbl TO jcho;
GRANT SELECT ON TABLE room_tbl TO mo_kiosk;


--
-- Name: row_update_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE row_update_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE row_update_seq FROM wchao;
GRANT ALL ON SEQUENCE row_update_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE row_update_seq TO mo_operator;
GRANT SELECT,UPDATE ON SEQUENCE row_update_seq TO mo_tenant;


--
-- Name: row_update_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE row_update_tbl FROM PUBLIC;
REVOKE ALL ON TABLE row_update_tbl FROM pithadmin;
GRANT ALL ON TABLE row_update_tbl TO pithadmin;
GRANT INSERT ON TABLE row_update_tbl TO mo_operator;
GRANT INSERT ON TABLE row_update_tbl TO mo_tenant;


--
-- Name: sales_interaction_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE sales_interaction_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sales_interaction_seq FROM wchao;
GRANT ALL ON SEQUENCE sales_interaction_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE sales_interaction_seq TO operations;


--
-- Name: sales_interaction_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE sales_interaction_tbl FROM PUBLIC;
REVOKE ALL ON TABLE sales_interaction_tbl FROM pithadmin;
GRANT ALL ON TABLE sales_interaction_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE sales_interaction_tbl TO operations;
GRANT SELECT ON TABLE sales_interaction_tbl TO mo_report;


--
-- Name: scanner_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE scanner_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE scanner_seq FROM wchao;
GRANT ALL ON SEQUENCE scanner_seq TO wchao;


--
-- Name: scanner_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE scanner_tbl FROM PUBLIC;
REVOKE ALL ON TABLE scanner_tbl FROM pithadmin;
GRANT ALL ON TABLE scanner_tbl TO pithadmin;
GRANT SELECT ON TABLE scanner_tbl TO operations;
GRANT SELECT ON TABLE scanner_tbl TO tenants;


--
-- Name: service_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE service_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE service_seq FROM wchao;
GRANT ALL ON SEQUENCE service_seq TO wchao;


--
-- Name: service_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE service_tbl FROM PUBLIC;
REVOKE ALL ON TABLE service_tbl FROM pithadmin;
GRANT ALL ON TABLE service_tbl TO pithadmin;
GRANT SELECT ON TABLE service_tbl TO operations;
GRANT SELECT ON TABLE service_tbl TO ds_operator;
GRANT SELECT ON TABLE service_tbl TO tenants;


--
-- Name: session_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE session_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE session_seq FROM wchao;
GRANT ALL ON SEQUENCE session_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE session_seq TO tenants;
GRANT SELECT,UPDATE ON SEQUENCE session_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE session_seq TO mo_investor;
GRANT SELECT,UPDATE ON SEQUENCE session_seq TO mo_vendor;
GRANT SELECT,UPDATE ON SEQUENCE session_seq TO ds_operator;


--
-- Name: session_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE session_tbl FROM PUBLIC;
REVOKE ALL ON TABLE session_tbl FROM pithadmin;
GRANT ALL ON TABLE session_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE session_tbl TO tenants;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE session_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE session_tbl TO mo_investor;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE session_tbl TO mo_vendor;
GRANT SELECT,INSERT,UPDATE ON TABLE session_tbl TO ds_operator;


--
-- Name: ss_country_x_prefix_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE ss_country_x_prefix_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ss_country_x_prefix_seq FROM wchao;
GRANT ALL ON SEQUENCE ss_country_x_prefix_seq TO wchao;


--
-- Name: ss_country_x_prefix_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ss_country_x_prefix_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ss_country_x_prefix_tbl FROM pithadmin;
GRANT ALL ON TABLE ss_country_x_prefix_tbl TO pithadmin;
GRANT SELECT ON TABLE ss_country_x_prefix_tbl TO mo_operator;
GRANT SELECT ON TABLE ss_country_x_prefix_tbl TO mo_tenant;


--
-- Name: ss_employee_x_role_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE ss_employee_x_role_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ss_employee_x_role_seq FROM wchao;
GRANT ALL ON SEQUENCE ss_employee_x_role_seq TO wchao;


--
-- Name: ss_investor_x_role_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE ss_investor_x_role_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ss_investor_x_role_seq FROM wchao;
GRANT ALL ON SEQUENCE ss_investor_x_role_seq TO wchao;


--
-- Name: ss_investor_x_role_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ss_investor_x_role_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ss_investor_x_role_tbl FROM pithadmin;
GRANT ALL ON TABLE ss_investor_x_role_tbl TO pithadmin;
GRANT SELECT ON TABLE ss_investor_x_role_tbl TO mo_investor;


--
-- Name: ss_prospective_customer_x_sales_tag_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ss_prospective_customer_x_sales_tag_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ss_prospective_customer_x_sales_tag_tbl FROM pithadmin;
GRANT ALL ON TABLE ss_prospective_customer_x_sales_tag_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ss_prospective_customer_x_sales_tag_tbl TO mo_operator;


--
-- Name: ss_vend_pers_x_role_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE ss_vend_pers_x_role_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ss_vend_pers_x_role_seq FROM wchao;
GRANT SELECT,UPDATE ON SEQUENCE ss_vend_pers_x_role_seq TO wchao;


--
-- Name: ss_vend_pers_x_role_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ss_vend_pers_x_role_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ss_vend_pers_x_role_tbl FROM pithadmin;
GRANT ALL ON TABLE ss_vend_pers_x_role_tbl TO pithadmin;
GRANT SELECT ON TABLE ss_vend_pers_x_role_tbl TO mo_vendor;


--
-- Name: ss_web_page_x_keyword_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE ss_web_page_x_keyword_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ss_web_page_x_keyword_seq FROM wchao;
GRANT ALL ON SEQUENCE ss_web_page_x_keyword_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE ss_web_page_x_keyword_seq TO operations;


--
-- Name: ss_web_page_x_keyword_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE ss_web_page_x_keyword_tbl FROM PUBLIC;
REVOKE ALL ON TABLE ss_web_page_x_keyword_tbl FROM pithadmin;
GRANT ALL ON TABLE ss_web_page_x_keyword_tbl TO pithadmin;
GRANT ALL ON TABLE ss_web_page_x_keyword_tbl TO operations;
GRANT SELECT ON TABLE ss_web_page_x_keyword_tbl TO mo_vendor;


--
-- Name: state_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE state_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE state_seq FROM wchao;
GRANT ALL ON SEQUENCE state_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE state_seq TO operations;


--
-- Name: state_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE state_tbl FROM PUBLIC;
REVOKE ALL ON TABLE state_tbl FROM pithadmin;
GRANT ALL ON TABLE state_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE state_tbl TO operations;
GRANT SELECT ON TABLE state_tbl TO tenants;
GRANT SELECT ON TABLE state_tbl TO jcho;
GRANT SELECT ON TABLE state_tbl TO mo_investor;


--
-- Name: subnet_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE subnet_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE subnet_seq FROM wchao;
GRANT ALL ON SEQUENCE subnet_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE subnet_seq TO operations;


--
-- Name: subnet_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE subnet_tbl FROM PUBLIC;
REVOKE ALL ON TABLE subnet_tbl FROM pithadmin;
GRANT ALL ON TABLE subnet_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE subnet_tbl TO operations;


--
-- Name: switch_octet_count_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE switch_octet_count_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE switch_octet_count_seq FROM wchao;
GRANT ALL ON SEQUENCE switch_octet_count_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE switch_octet_count_seq TO mo_daemon;
GRANT SELECT,UPDATE ON SEQUENCE switch_octet_count_seq TO operations;


--
-- Name: switch_octet_count_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE switch_octet_count_tbl FROM PUBLIC;
REVOKE ALL ON TABLE switch_octet_count_tbl FROM pithadmin;
GRANT ALL ON TABLE switch_octet_count_tbl TO pithadmin;
GRANT ALL ON TABLE switch_octet_count_tbl TO mo_daemon;
GRANT ALL ON TABLE switch_octet_count_tbl TO operations;


--
-- Name: task_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE task_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE task_seq FROM wchao;
GRANT ALL ON SEQUENCE task_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE task_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE task_seq TO ds_operator;
GRANT SELECT,UPDATE ON SEQUENCE task_seq TO tenants;


--
-- Name: task_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE task_tbl FROM PUBLIC;
REVOKE ALL ON TABLE task_tbl FROM pithadmin;
GRANT ALL ON TABLE task_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE task_tbl TO operations;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE task_tbl TO ds_operator;
GRANT SELECT,INSERT,UPDATE ON TABLE task_tbl TO mo_tenant;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE task_tbl TO tenants;


--
-- Name: tax_document_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE tax_document_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tax_document_seq FROM wchao;
GRANT ALL ON SEQUENCE tax_document_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE tax_document_seq TO operations;


--
-- Name: tax_document_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE tax_document_tbl FROM PUBLIC;
REVOKE ALL ON TABLE tax_document_tbl FROM pithadmin;
GRANT ALL ON TABLE tax_document_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tax_document_tbl TO operations;
GRANT SELECT ON TABLE tax_document_tbl TO mo_investor;


--
-- Name: token_seq; Type: ACL; Schema: public; Owner: czhang
--

REVOKE ALL ON SEQUENCE token_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE token_seq FROM czhang;
GRANT ALL ON SEQUENCE token_seq TO czhang;
GRANT SELECT,UPDATE ON SEQUENCE token_seq TO tenants;


--
-- Name: token_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE token_tbl FROM PUBLIC;
REVOKE ALL ON TABLE token_tbl FROM pithadmin;
GRANT ALL ON TABLE token_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE token_tbl TO mo_tenant;


--
-- Name: user_seq; Type: ACL; Schema: public; Owner: czhang
--

REVOKE ALL ON SEQUENCE user_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE user_seq FROM czhang;
GRANT ALL ON SEQUENCE user_seq TO czhang;
GRANT SELECT,UPDATE ON SEQUENCE user_seq TO tenants;


--
-- Name: user_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE user_tbl FROM PUBLIC;
REVOKE ALL ON TABLE user_tbl FROM pithadmin;
GRANT ALL ON TABLE user_tbl TO pithadmin;
GRANT ALL ON TABLE user_tbl TO wchao;
GRANT SELECT,INSERT,UPDATE ON TABLE user_tbl TO mo_tenant;


--
-- Name: vend_acct_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE vend_acct_tbl FROM PUBLIC;
REVOKE ALL ON TABLE vend_acct_tbl FROM pithadmin;
GRANT ALL ON TABLE vend_acct_tbl TO pithadmin;
GRANT ALL ON TABLE vend_acct_tbl TO mo_vendor;


--
-- Name: vend_pers_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE vend_pers_tbl FROM PUBLIC;
REVOKE ALL ON TABLE vend_pers_tbl FROM pithadmin;
GRANT ALL ON TABLE vend_pers_tbl TO pithadmin;
GRANT ALL ON TABLE vend_pers_tbl TO mo_vendor;


--
-- Name: virtual_office_application_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE virtual_office_application_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE virtual_office_application_seq FROM wchao;
GRANT ALL ON SEQUENCE virtual_office_application_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE virtual_office_application_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE virtual_office_application_seq TO mo_message;
GRANT SELECT,UPDATE ON SEQUENCE virtual_office_application_seq TO czhang;


--
-- Name: virtual_office_application_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE virtual_office_application_tbl FROM PUBLIC;
REVOKE ALL ON TABLE virtual_office_application_tbl FROM pithadmin;
GRANT ALL ON TABLE virtual_office_application_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE virtual_office_application_tbl TO operations;
GRANT INSERT ON TABLE virtual_office_application_tbl TO mo_message;
GRANT SELECT ON TABLE virtual_office_application_tbl TO jcho;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE virtual_office_application_tbl TO czhang;


--
-- Name: voicecloud_txn_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE voicecloud_txn_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE voicecloud_txn_seq FROM wchao;
GRANT ALL ON SEQUENCE voicecloud_txn_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE voicecloud_txn_seq TO mo_operator;
GRANT SELECT,UPDATE ON SEQUENCE voicecloud_txn_seq TO mo_message;
GRANT SELECT,UPDATE ON SEQUENCE voicecloud_txn_seq TO sn_message;
GRANT SELECT,UPDATE ON SEQUENCE voicecloud_txn_seq TO sn_operator;


--
-- Name: voicecloud_txn_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE voicecloud_txn_tbl FROM PUBLIC;
REVOKE ALL ON TABLE voicecloud_txn_tbl FROM pithadmin;
GRANT ALL ON TABLE voicecloud_txn_tbl TO pithadmin;
GRANT SELECT,INSERT,UPDATE ON TABLE voicecloud_txn_tbl TO mo_message;
GRANT SELECT,INSERT,UPDATE ON TABLE voicecloud_txn_tbl TO sn_message;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE voicecloud_txn_tbl TO mo_operator;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE voicecloud_txn_tbl TO sn_operator;


--
-- Name: voicemail_msgs_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE voicemail_msgs_tbl FROM PUBLIC;
REVOKE ALL ON TABLE voicemail_msgs_tbl FROM pithadmin;
GRANT ALL ON TABLE voicemail_msgs_tbl TO pithadmin;
GRANT SELECT ON TABLE voicemail_msgs_tbl TO tenants;


--
-- Name: watched_domain_name_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE watched_domain_name_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE watched_domain_name_seq FROM wchao;
GRANT ALL ON SEQUENCE watched_domain_name_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE watched_domain_name_seq TO operations;


--
-- Name: watched_domain_name_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE watched_domain_name_tbl FROM PUBLIC;
REVOKE ALL ON TABLE watched_domain_name_tbl FROM pithadmin;
GRANT ALL ON TABLE watched_domain_name_tbl TO pithadmin;
GRANT ALL ON TABLE watched_domain_name_tbl TO operations;
GRANT SELECT ON TABLE watched_domain_name_tbl TO mo_vendor;


--
-- Name: web_page_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE web_page_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE web_page_seq FROM wchao;
GRANT ALL ON SEQUENCE web_page_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE web_page_seq TO operations;


--
-- Name: web_page_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE web_page_tbl FROM PUBLIC;
REVOKE ALL ON TABLE web_page_tbl FROM pithadmin;
GRANT ALL ON TABLE web_page_tbl TO pithadmin;
GRANT ALL ON TABLE web_page_tbl TO operations;
GRANT SELECT ON TABLE web_page_tbl TO mo_vendor;


--
-- Name: web_posting_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE web_posting_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE web_posting_seq FROM wchao;
GRANT ALL ON SEQUENCE web_posting_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE web_posting_seq TO operations;
GRANT SELECT,UPDATE ON SEQUENCE web_posting_seq TO mo_message;


--
-- Name: web_posting_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE web_posting_tbl FROM PUBLIC;
REVOKE ALL ON TABLE web_posting_tbl FROM pithadmin;
GRANT ALL ON TABLE web_posting_tbl TO pithadmin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE web_posting_tbl TO operations;
GRANT SELECT,DELETE,UPDATE ON TABLE web_posting_tbl TO mo_message;


--
-- Name: yahoo_query_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE yahoo_query_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE yahoo_query_seq FROM wchao;
GRANT ALL ON SEQUENCE yahoo_query_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE yahoo_query_seq TO mo_operator;


--
-- Name: yahoo_query_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE yahoo_query_tbl FROM PUBLIC;
REVOKE ALL ON TABLE yahoo_query_tbl FROM pithadmin;
GRANT ALL ON TABLE yahoo_query_tbl TO pithadmin;
GRANT ALL ON TABLE yahoo_query_tbl TO mo_operator;


--
-- Name: yahoo_search_result_seq; Type: ACL; Schema: public; Owner: wchao
--

REVOKE ALL ON SEQUENCE yahoo_search_result_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE yahoo_search_result_seq FROM wchao;
GRANT ALL ON SEQUENCE yahoo_search_result_seq TO wchao;
GRANT SELECT,UPDATE ON SEQUENCE yahoo_search_result_seq TO mo_operator;


--
-- Name: yahoo_search_result_tbl; Type: ACL; Schema: public; Owner: pithadmin
--

REVOKE ALL ON TABLE yahoo_search_result_tbl FROM PUBLIC;
REVOKE ALL ON TABLE yahoo_search_result_tbl FROM pithadmin;
GRANT ALL ON TABLE yahoo_search_result_tbl TO pithadmin;
GRANT ALL ON TABLE yahoo_search_result_tbl TO mo_operator;


--
-- PostgreSQL database dump complete
--


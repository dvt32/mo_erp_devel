CREATE SEQUENCE "public"."eum_intellim_group_tbl_id_seq" START 1;

CREATE TABLE "public"."enum_intellim_group_tbl" ( 
	"id" INTEGER DEFAULT nextval('eum_intellim_group_tbl_id_seq'::regclass) NOT NULL UNIQUE, 
	"name" CHARACTER VARYING( 60 ), 
	"intellim_id" INTEGER NOT NULL, 
	"zone_id" INTEGER
, CONSTRAINT "unique_id" UNIQUE( "id" ) );
CREATE INDEX "index_id" ON "public"."enum_intellim_group_tbl" USING btree( "id" ASC NULLS LAST );


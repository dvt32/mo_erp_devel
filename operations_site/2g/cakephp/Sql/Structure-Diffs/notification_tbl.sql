--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: notification_tbl; Type: TABLE; Schema: public; Owner: pithadmin; Tablespace: 
--

CREATE TABLE notification_tbl (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    floors character varying(45),
    created timestamp with time zone,
    modified timestamp with time zone,
    is_active boolean DEFAULT true NOT NULL,
    expires timestamp without time zone,
    employee_id integer NOT NULL
);


ALTER TABLE public.notification_tbl OWNER TO pithadmin;

--
-- Name: COLUMN notification_tbl.floors; Type: COMMENT; Schema: public; Owner: pithadmin
--

COMMENT ON COLUMN notification_tbl.floors IS 'comma-separated list of floor_ids';


--
-- Name: notification_seq; Type: SEQUENCE; Schema: public; Owner: pithadmin
--

CREATE SEQUENCE notification_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pithadmin
--

ALTER TABLE ONLY notification_tbl ALTER COLUMN id SET DEFAULT nextval('notification_seq'::regclass);


--
-- Name: notification_tbl_pkey; Type: CONSTRAINT; Schema: public; Owner: pithadmin; Tablespace: 
--

ALTER TABLE ONLY notification_tbl
    ADD CONSTRAINT notification_tbl_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--


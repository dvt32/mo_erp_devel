BEGIN;


-- CREATE FIELD "type" -----------------------------------------
ALTER TABLE "public"."building_tbl" ADD COLUMN "type" CHARACTER VARYING( 60 );
-- -------------------------------------------------------------

-- CREATE FIELD "server1" --------------------------------------
ALTER TABLE "public"."building_tbl" ADD COLUMN "server1" CHARACTER VARYING( 60 );
-- -------------------------------------------------------------

-- CREATE FIELD "server2" --------------------------------------
ALTER TABLE "public"."building_tbl" ADD COLUMN "server2" CHARACTER VARYING( 60 );
-- -------------------------------------------------------------;

COMMIT;

BEGIN;


-- CREATE FIELD "map_height" -----------------------------------
ALTER TABLE "public"."floor_tbl" ADD COLUMN "map_height" INTEGER;
-- -------------------------------------------------------------

-- CREATE FIELD "map_width" ------------------------------------
ALTER TABLE "public"."floor_tbl" ADD COLUMN "map_width" INTEGER;
-- -------------------------------------------------------------;

COMMIT;
BEGIN;


-- CHANGE "COLLATION, TYPE, LENGTH" OF "FIELD "group_intellim_id" 
ALTER TABLE "public"."proximity_card_tbl" ALTER COLUMN "group_intellim_id" TYPE CHARACTER VARYING( 60 );
-- -------------------------------------------------------------;

COMMIT;

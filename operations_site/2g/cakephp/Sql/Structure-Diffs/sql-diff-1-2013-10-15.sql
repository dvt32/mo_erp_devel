BEGIN;

CREATE SEQUENCE desk_assignment_history_tbl_id_seq START 1;

-- CREATE FIELD "id" -------------------------------------------
ALTER TABLE "public"."desk_assignment_history_tbl" ADD COLUMN "id" INTEGER DEFAULT nextval('desk_assignment_history_tbl_id_seq'::regclass) NOT NULL;
-- -------------------------------------------------------------

-- CREATE INDEX "index_id1" ------------------------------------
CREATE INDEX "index_id1" ON "public"."desk_assignment_history_tbl" USING btree( "id" ASC NULLS LAST );
-- -------------------------------------------------------------;

COMMIT;

BEGIN;


-- CREATE FIELD "is_intellim" ----------------------------------
ALTER TABLE "public"."proximity_card_tbl" ADD COLUMN "is_intellim" BOOLEAN;
-- -------------------------------------------------------------;


COMMIT;

BEGIN;

ALTER TABLE "public"."bank_account_tbl" ADD COLUMN "is_active" BOOLEAN;
ALTER TABLE "public"."credit_card_tbl" ADD COLUMN "is_active" BOOLEAN;

COMMIT;
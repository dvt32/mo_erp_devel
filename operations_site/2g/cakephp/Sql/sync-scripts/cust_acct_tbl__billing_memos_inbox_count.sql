UPDATE cust_acct_tbl
SET billing_memos_inbox_count=(
	SELECT count(*)
	FROM billing_memo_tbl
	WHERE cust_acct_id=cust_acct_tbl.id
	AND is_applied=FALSE
	AND is_active=TRUE
);

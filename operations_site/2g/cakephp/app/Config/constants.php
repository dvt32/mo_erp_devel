<?php
/**
 * Application-wide constants
 */

// re-define FULL_BASE_URL CakePHP claims it was deprecated in 2.4.*
if (!defined('FULL_BASE_URL')) {
	$s = null;
	if (env('HTTPS')) {
		$s = 's';
	}

	$httpHost = env('HTTP_HOST');

	if (isset($httpHost)) {
		define('FULL_BASE_URL', 'http' . $s . '://' . $httpHost);
		Configure::write('App.fullBaseUrl', FULL_BASE_URL);
	}
	unset($httpHost, $s);
}

// date & time
define('DATETIME_FORMAT',  'Y-m-d H:i:s');
define('DATE_FORMAT_USA',  'm/d/y');
define('DATETIME_USA',  'm/d/Y g:i A');
define('DATE_FORMAT_YMD',  'Y-m-d');
define('TIME_FORMAT',      'g:ia');

// same-day item removal request cutoff
define('ITEM_REMOVAL_CUTOFF', 'Today 3PM'); // string parseable by strtotime
define('MAX_RESERVATION_DURATION_IN_SECONDS', 21600); // 21600 sec = 6 hr * 60 min/hr * 60 sec/min

// character sets
define('ALPHA_LOWER',  'abcdefghijklmnopqrstuvwxyz');
define('ALPHA_UPPER',  'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
define('DIGITS',       '0123456789');
define('BLOWFISH',     './abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

// wifi networks
define('MICROOFFICE_SSID', '{{ wifi_ssid }}');
define('MICROOFFICE_SSPW', '{{ wifi_password }}');

// file storage server
define('DAV_SERVER_USERNAME','{{ dav_username }}');
define('DAV_SERVER_PASSWORD','{{ dav_password }}');
define('DAV_SERVER_HOST', '{{ dav_base_url }}');
define('MAX_FILE_UPLOAD_SIZE', 40960); // 40960 b = 40 kb * 1024 b/kb

// public fax number
define('PUBLIC_FAX_NUMBER',  '6462531258');
define('FAXAGE_USERNAME',    '{{ faxage_username }}');
define('FAXAGE_COMPANYID',   '{{ faxage_company }}');
define('FAXAGE_PSWD',        '{{ faxage_password }}');
define('FAXING_DEBUG', '0'); // if 1 email FAXING_DEBUG_EMAIL else email users
define('FAXING_DEBUG_EMAIL', 'bugs@microoffice.com');
define('FAXAGE_CALLBACK', FULL_BASE_URL . '/faxouts/faxageResponse');

// email configs
define('EMAIL_CONFIG', 'default');

// emails
define('EMAIL_FINANCE', 'jin@microoffice.com');
define('EMAIL_TASKS', 'mlark@microoffice.com');
define('EMAIL_NEWFAXNUMBER', 'service@microoffice.com');
define('EMAIL_FAXERROR', 'service@microoffice.com');

// INTELLI-M CREDS
define('PROXIMITY_USERNAME', '{{ intellim_username }}');
define('PROXIMITY_PASSWORD', '{{ intellim_password }}');
define('PROXIMITY_URL', '{{ intellim_base_url }}');

// user and customer roles
define('ROLE_SUPER',   9); // microoffice employees
define('ROLE_PRIMARY', 2); // primary contact
define('ROLE_ADMIN',   1); // designated customerAccount admins
define('ROLE_NORMAL',  0); // all other employees

// First Data Creds 
define('FIRST_DATA_TRANSACTIONTYPE',  '00');
define('SSL_KEY_FILE', '1001232429.pem');
define('STORE_ID', '1001232429');


// Mandrill API Key
define('MANDRILL_API_KEY', '{{ mandrill_api_key }}');
define('REALLY_SEND_EMAIL', true);
define('EMAIL_TESTING', true);
define('EMAIL_TESTER', 'czhang@microoffice.com');

/**
 * DOCUSIGN API
 * 
 */

define('DOCUSIGN_USERNAME', '{{ docusign_email }}');
define('DOCUSIGN_PASSWORD', '{{ docusign_password }}');
define('DOCUSIGN_INTEGRATORKEY', '{{ docusign_integrator_key }}');
define('DOCUSIGN_BASEURL', '{{ docusign_base_url }}');
define('DOCUSIGN_RETURNURL', FULL_BASE_URL);

/**
 * cron key to prevent bots and spammers from "accidentally" accessing cron methods
 * pass this as the first parameter to CRON-called methods
 */
define('CRON_KEY', 'uBjs0O6uc9ULETBCqAVENzlTcuTASyuMHm9k2Tt5KxexzXXz6jPr6EasU9tX');

/**
 * Email images define
 */
define('EMAIL_IMAGE_000', FULL_BASE_URL . '/img/email/image00.jpg');
define('EMAIL_IMAGE_001', FULL_BASE_URL . '/img/email/image01.jpg');
define('EMAIL_IMAGE_002', FULL_BASE_URL . '/img/email/image02.jpg');

define('BILLING_EPOCH', '2014-03-12');

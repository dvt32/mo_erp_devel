<?php

use AD7six\Dsn\Wrapper\CakePHP\DbDsn;

class DATABASE_CONFIG {

/**
 * Define connections using environment variables
 *
 * @return void
 */
	public function __construct() {
		$this->default = DbDsn::parse(env('DEBUG')==3 ? env('DATABASE_TEST_URL') : env('DATABASE_DEFAULT'));
		$this->telecom = DbDsn::parse(env('DATABASE_TELECOM'));
		$this->provender = DbDsn::parse(env('DATABASE_PROVENDER'));
		$this->test = DbDsn::parse(env('DATABASE_TEST_URL'));
	}

}

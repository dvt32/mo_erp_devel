<?php

use AD7six\Dsn\Wrapper\CakePHP\EmailDsn;

class EmailConfig {

/**
 * Define connections using environment variables
 *
 * @return void
 */
	// public function __construct() {
	// 	$this->default = DbDsn::parse(env('EMAIL_URL'));
	// }

	public $microoffice = array(
		'transport' => 'Smtp',
		'host' => 'mailout-int.microoffice.com',
		'port' => 25,
		'timeout' => 15,
		'client' => null,
		'log' => true,
	);
	
	public $default = array(
		'transport' => 'Smtp',
		'from' => array('no-reply@microoffice.com' => 'MicroOffice'),
		'sender' => array('hello@pith-studio.com'),
		'host' => 'ssl://smtp.gmail.com',
		'port' => 465,
		'timeout' => 15,
		'username' => 'hello@pith-studio.com',
		'password' => 'prosciutto',
		'client' => null,
		'log' => false,
	);

}

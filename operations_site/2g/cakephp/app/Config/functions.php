<?php
/**
 * Utility functions
 */

/**
 * Convert octal to binary
 *
 * @param Octal representation of a given value
 * @param Binary representation of the given value
 */
function octbin($oct) {
	return decbin(octdec($oct));
}


/**
 * Find the nth character in a string
 *
 * @param $str The input string
 * @param $n The position to inspect
 * @return The character at the position in the string
 */
function charat($string, $n) {
	return substr($string, $n, 1);
}


/**
 * Wrapper for the array_filter php function.
 * Removes NULL, FALSE, '' (empty string), but leaves 0 and '0'
 *
 * @param $input The array to iterate over
 * @return The filtered array
 */
function filter($array) {
	return array_filter($array,function($e){return !empty($e)||($e==='0')||($e===0);});
}


/**
 * Recursively filter an array. Removes empty arrays
 *
 * @param $input The array to iterature over
 * @return The filtered array
 */
function filter_recursive($array) {
	foreach ($array as &$elem) {
		if (is_array($elem)){
			$elem=filter_recursive($elem);
		}
	}
	return filter($array);
}


/**
 * Trim each entry in an array (not recursive)
 *
 * @param $input Array to iterate over
 */
function array_trim(&$input) {
	array_walk($input,function(&$e){if(is_string($e)){$e=trim($e);}});
}


/**
 * Trim all entries in an array (recursive)
 *
 * @param $input Array to iterate over
 */
function array_trim_recursive(&$input) {
	array_walk_recursive($input,function(&$e){if(is_string($e)){$e=trim($e);}});
}


/**
 * Generates a random string with the given length and charset
 * 
 * @param int $length length of the random string to generate
 * @param string $chars string containing charset to choose from
 * @return string the randomly generated string
 */
function random_string($length = 10, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
	$str = '';
	for ($i = 0; $i < $length; $i++)
		$str .= substr($chars, mt_rand(0,strlen($chars)-1), 1);
	return $str;
}

/**
 * Combines an array with itself. This is useful in views when selecting a dropdown value should pass 
 * that value to $this->request->data. Do NOT use this with deep arrays.
 * e.g., array_self_combine(array('a','b','xyz')) returns array('a' => 'a', 'b' => 'b', 'xyz' => 'xyz')
 * 
 * @param array $original 
 * @return array $combined array with keys identical to values
 */
function array_self_combine(array $array) {
	return array_combine($array, $array);
}


/**
 * Calculates the time (seconds since epoch) of the closest business day to the date requested.
 * i.e. a timestring that evaluates to Saturday or Sunday will return the same time-of-day on Monday
 * NOTE: be careful when the parameter evaluates to a time in the past
 * 
 * @param string $timeString a string that can be evaluated by strtotime
 * @return int $time seconds since epoch of the closest business day
 */
function strto_businessday_time($timeString = 'now') {
	$time = strtotime($timeString);
	$dow = date('w', $time);
	switch ($dow) {
		case 0: $time += 24*3600; break;
		case 6: $time += 48*3600; break;
		default: break; 
	}
	return $time;
}


/**
 * Utility function to calcuate the closest business day, before or after a given time (timestamp).
 * NOTE: This function does not take holidays into account
 * 
 * @param mix $before a boolean or string to indicate whether the next or previous business day should be found default='before'
 * @param int $time unix time of target, default=now
 * @return int $timestamp unix time of closest business datetime (preserves h:m:s)
 */
	function business_day($before = true, $time = null) {

		// param or "now"
		$time = $time ?: time();

		$dow = date('w', $time);

		if ($dow==0 || $dow==6) {

			switch (strtolower($before)) {

				// next business day
				case 'next':
				case 'after':
				case false:
					switch ($dow) {
						case 0: $time += 24*3600; break; // sun + 24hr = mon
						case 6: $time += 48*3600; break; // sat + 48hr = mon
					}
				break;

				// previous business day
				case 'previous':
				case 'prior':
				case 'before':
				case true:
					switch ($dow) {
						case 0: $time -= 48*3600; break; // sun - 48hr = fri
						case 6: $time -= 24*3600; break; // sat - 24hr = fri
					}
				break;
			}
		}

		return $time;
	}

/**
 * Convert an array to a stdClass object (non-recursive)
 * 
 * @param array $array the array to convert
 * @return stdClass $object the converted object
 */
	function arrayToObject($array = array()) {
		$object = new stdClass;
		foreach ($array as $key => $value) {
			$object->$key = $value;
		}
		return $object;
	}

/**
 * Truncates long text to a given length  
 * 
 * @param string $string string to truncate
 * @param int $limit length to truncate the string to
 * @return truncated string
 */  
	function truncate($string, $limit = 100) {
		if (strlen($string) > $limit) {
			return substr($string, 0, $limit) . "...";
		}
		return $string;
	}

	function minify_html($buffer) {
		$search = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s');
		$replace = array('>', '<', '\\1');
		if (preg_match("/\<html/i",$buffer) == 1 && preg_match("/\<\/html\>/i",$buffer) == 1) {
			$buffer = preg_replace($search, $replace, $buffer);
		}
		return $buffer;
	}

/**
 * Conditional echo statement:  echoes the formatted string if the first arg is !empty()
 */
	function _e($singular, $args = null) {
		if (!$singular) { return; }
		$args = array_values(array_slice(func_get_args(), 1));
		return !empty($args[0]) ? vsprintf($singular, $args) : '&nbsp;';
	}


/**
 * Get datetime(s) for the start/end/both of the current month
 */
	function current_month($which = 'both', $format = 'U') {

		$start = date_create_from_format('Y-m-d H:i:s', date('Y-m-01 00:00:00'))->format($format);
		$end = date_create_from_format('Y-m-d H:i:s', date("Y-m-t 23:59:59"))->format($format);

		switch ($which) {
			case 'start':
				return $start;
			case 'end':
				return $end;
			case 'both':
				return compact('start','end');
		}

		return false;
	}
	

/**
 * Collects the values in the subarray
 * 
 * @param string $path dot notation to reach value
 * @param array $array array from which to collect
 * @return collection of values
 */
	function collect($path, array $array) {
		$coll = array();
		foreach ($array as $elem) {
			$coll[] = get($path,$elem);
		}
		return $coll;
	}

/**
 * Tally the count of matching values of a given key (path)
 * 
 * @param string $path path to the value
 * @param array $array array from which to tally
 * @return array $tally tally of the values
 */
	function tally($path, array $array) {
		$tallies = array();
		foreach ($array as $elem) {
			$val = get($path, $elem);
			if (is_string($val) || is_int($val)) {
				@$tallies[$val]++;
			} else {
				// array key must be string or int
				@$tallies[null]++;
			}
		}
		return $tallies;
	}

/**
 * Get value from array via dot notation
 * 
 * @param array $a
 * @param string $path
 * @param 
 */
	function get($path, array $a, $default = null) {
		$current = $a;
		$p = strtok($path, '.');

		while ($p !== false) {
			if (!isset($current[$p])) {
				return $default;
			}
			$current = $current[$p];
			$p = strtok('.');
		}

		return $current;
	}

	function is_numeric_array($array) {
		if (is_array($array)) {
			$keys = array_keys($array);
			for ($i=0,$flag=true;$flag&&$i<count($keys);$i++){$flag=is_int($keys[$i]);}
			return $flag;
		}
		return false;
	}

/**
 * Reindex array by a value in the subarray
 * 
 * @param string $key_name the name of the field to by which to reindex
 * @param array $before original array
 * @return reindexed array
 */
	function reindex($path, array $before) {
		$after = array();
		foreach ($before as $sub) {
			if (is_array($sub)) {
				$key = get($path,$sub);
				$after[$key] = $sub;
			}
		}
		return $after;
	}

/**
 * Start datetime of current billing cycle
 */
	function cycle_start_dt($dom_cycle_starts = 20, $now = null) {

		// for testing
		$now = $now ?: time();

		// pad
		$dom_cycle_starts = sprintf("%02d", $dom_cycle_starts); // feb 20

		// this cycle
		if (date('d', $now) > $dom_cycle_starts) {
			// today is feb 21 - mar 20
			$start_cycle = date("Y-m-{$dom_cycle_starts} 00:00:00", strtotime('+1 month', $now)); // mar 20
		} else {
			// today is jan 21 - feb 20
			$start_cycle = date("Y-m-{$dom_cycle_starts} 00:00:00", $now); // feb 20
		}
		return $start_cycle;
	}

/**
 * get datetime of current billing cycle due date
 */
	function cycle_due_dt($dom_payment_due = 1, $dom_cycle_starts = 20, $now = null) {

		// for testing
		$now = $now ?: time();

		// pad
		$dom_payment_due = sprintf("%02d", $dom_payment_due); // mar 1

		// this cycle
		if (date('d', $now) > $dom_cycle_starts) {
			// today is feb 21 - mar 20
			$due_date = date("Y-m-{$dom_payment_due} 23:59:59", strtotime('+2 month', $now)); // apr 1
		} else {
			// today is jan 21 - feb 20
			$due_date = date("Y-m-{$dom_payment_due} 23:59:59", strtotime('+1 month', $now)); // mar 1
		}
		return $due_date;
	}

/**
 * "string" implode: implodes, but uses "and" as the last delimiter
 */
	function strimplode($array = array(), $delim = ', ', $final = ' and ') {
		if (!empty($array)) {
			$last = array_pop($array); // removes last element from array
			return count($array) ? implode($delim, $array) . $final . $last : $last;
		}
	}

/**
 * convert datetime string to more human readable format (string to string)
 */
	function strtodatestring($string, $format = null) {
		$format = $format ?: DATETIME_USA;
		$time = strtotime($string);
		return empty($time) ? '' : date($format, $time);
	}

?>

<?php
App::uses('AppController', 'Controller');
/**
 * AccessLevels Controller
 *
 * @property AccessLevel $AccessLevel
 * @property PaginatorComponent $Paginator
 */
class AccessLevelsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccessLevel->recursive = 0;
		$this->set('accessLevels', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccessLevel->exists($id)) {
			throw new NotFoundException(__('Invalid access level'));
		}
		$options = array('conditions' => array('AccessLevel.' . $this->AccessLevel->primaryKey => $id));
		$this->set('accessLevel', $this->AccessLevel->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AccessLevel->create();
			if ($this->AccessLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The access level has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The access level could not be saved. Please, try again.'));
			}
		}
		$floors = $this->AccessLevel->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AccessLevel->exists($id)) {
			throw new NotFoundException(__('Invalid access level'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AccessLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The access level has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The access level could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AccessLevel.' . $this->AccessLevel->primaryKey => $id));
			$this->request->data = $this->AccessLevel->find('first', $options);
		}
		$floors = $this->AccessLevel->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AccessLevel->id = $id;
		if (!$this->AccessLevel->exists()) {
			throw new NotFoundException(__('Invalid access level'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AccessLevel->delete()) {
			$this->Session->setFlash(__('The access level has been deleted.'));
		} else {
			$this->Session->setFlash(__('The access level could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

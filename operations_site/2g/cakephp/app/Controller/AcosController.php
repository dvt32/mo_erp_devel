<?php
App::uses('AppController', 'Controller');
/**
 * Acos Controller
 *
 * @property Aco $Aco
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AcosController extends AppController {

/**
 * List of models used by this controller
 * 
 * @var array
 */
	public $uses = ['Aco','EmployeeRole','Permission'];

/**
 * index method
 *
 * @return void
 */
	public function index($role_key = null) {

		// fetch roles for use in both GET and POST
		$roles = $this->EmployeeRole->getUniqueRoles();

		// resolve the role key to name
		$role_name = @$roles[$role_key];

		if (empty($role_name))
		{
			$this->Alert->error('Please select a valid role to edit');
			return $this->redirect(['controller' => 'employeeRoles', 'action' => 'reassign']);
		}

		// save permissions
		if ($this->request->is('post'))
		{
			// update permission
			$data = $this->request->data;

			// permissions collectors
			$add_data = [];
			$del_data = [];

			// loop through and add/remove permissions according to (un)checked
			foreach ($data['Permission'] as $perm)
			{
				if (!empty($perm['allowed'])) {
					$add_data[] = array(
						'role_name' => $role_name,
						'aco_id' => $perm['aco_id']
					);
				}
				else {
					$del_data[] = $perm['aco_id'];
				}
			}

			// add checked permissions
			if (!empty($add_data))
				$this->Permission->saveMany($add_data, ['atomic' => false]);

			// remove unchecked permissions
			if (!empty($del_data))
				$this->Permission->deleteAll([
					'Permission.aco_id' => $del_data,
					'Permission.role_name' => $role_name
				]);

			// flash success message
			$this->Alert->success('Permissions successfully updated');

			// refresh the page as GET request
			return $this->redirect($_SERVER['REQUEST_URI']);
		}


		// check query
		if ($query = $this->request->query)
		{
			// filter conditions
			$conditions = $this->filterByQuery($query, [
				'fulltext' => ['controller','action']
			]);
			$this->request->data['Aco'] = @$query;
		}

		// filter
		$limit = 40;

		$permissions = $this->Permission->find('all', [
			'conditions' => array('role_name' => $role_name)
		]);
		$permissions = collect('Permission.aco_id',$permissions);

		$this->Paginator->settings = ['Aco' => compact('conditions','contain','limit')];
		$acos = $this->Paginator->paginate();

		$controllers = $this->Aco->find('list', ['fields' => ['id','controller']]);
		$controllers = array_unique($controllers);

		$this->set(compact('acos','controllers','roles','permissions'));
		
	}

/**
 * add method
 *
 * @return void
 */
	public function manage() {

		// post request
		if ($this->request->is('post')) {

			// create a new ACO
			$this->Aco->create();

			// save the new ACO
			if ($this->Aco->save($this->request->data)) {
				$this->Session->setFlash(__('The aco has been saved.'));
				return $this->redirect($_SERVER['REQUEST_URI']);
			} else {
				$this->Session->setFlash(__('The aco could not be saved. Please, try again.'));
			}
		}

		// filter by query
		if ($query = $this->request->query)
		{
			$conditions = $this->filterByQuery($query, ['fulltext' => ['controller','action']]);

			$this->request->data['Aco']['q'] = @$query['q'];
		}

		// related models to fetch
		$contain = array(
			'Permission' => ['id','role_name']
		);

		// default limit of 40 results
		$limit = 40;

		// fetch existing ACOs
		$this->Paginator->settings = ['Aco' => compact('conditions','contain','limit')];
		$acos = $this->Paginator->paginate();

		// send vars to view
		$this->set('acos', $acos);
		$this->set('_serialize', ['acos']);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Aco->id = $id;
		if (!$this->Aco->exists()) {
			throw new NotFoundException(__('Invalid aco'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Aco->delete()) {
			$this->Session->setFlash(__('The aco has been deleted.'));
		} else {
			$this->Session->setFlash(__('The aco could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'manage'));
	}

/**
 * autocomplete search
 */
	public function quickSearch()
	{
		// query
		$query = $this->request->query('query');

		// filter
		$suggestions = $this->Aco->find('list', [
			'conditions' => ['DISTINCT Aco.name LIKE' => $query],
			'fields' => ['id','controller']
		]);

		// send
		$this->set('suggestions',$suggestions);
		$this->set('_serialize',['suggestions']);
	}
}

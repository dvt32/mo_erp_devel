<?php
App::uses('AppController', 'Controller');
/**
 * AgingInteractions Controller
 *
 * @property AgingInteraction $AgingInteraction
 */
class AgingInteractionsController extends AppController {

	public $uses = ['AgingInteraction','CustomerAccount','Customer','Promise'];

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AgingInteraction->recursive = 0;
		$this->set('agingInteractions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AgingInteraction->exists($id)) {
			throw new NotFoundException(__('Invalid aging interaction'));
		}
		$options = array('conditions' => array('AgingInteraction.' . $this->AgingInteraction->primaryKey => $id));
		$this->set('agingInteraction', $this->AgingInteraction->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($cust_acct_id = null) {

		// safety check customer account id
		if (!$this->CustomerAccount->exists($cust_acct_id))
		{ // no customer account specified
			$this->Alert->error('Please choose a valid customer id to log an aging interaction');

			// redirect to the aging Customer Accounts page
			return $this->redirect(['controller' => 'customerAccounts', 'action' => 'aging']);
		}

		// process POST|PUT request
		if ($this->request->is('post')) {

			// fetch the data
			$data = $this->request->data;

			// take save actions
			switch ($data['section'])
			{
				// save the interaction
				case 'interaction':
					// reset the aging interaction model
					$this->AgingInteraction->create();

					// save a followup date if it was supplied
					$saved = !empty($data['CustomerAccount']['aging_followup_date']) ?
						$this->AgingInteraction->saveAssociated($data, ['fieldList' => ['CustomerAccount' => ['aging_followup_date']]]) :
						$this->AgingInteraction->save($data);

					// save the aging interaction
					if ($saved)
					{ // saved successfully
						$this->Alert->success('The Interaction has been saved');
						return $this->redirect($this->request->here);
					}
					else
					{ // save unsuccessful
						debug($this->AgingInteraction->validationErrors);
						$this->Alert->error('The Interaction could not be saved');
					}
				break;

				// save the promise
				case 'promise':

					// reset the promise model
					$this->Promise->create();

					if ($this->Promise->save($data))
					{ // saved successfully
						$this->Alert->success('The Promise has been saved');
						return $this->redirect($this->request->here);
					}
					else
					{ // save unsuccessful
						$this->Alert->error('The Promise could not be saved');
					}
				break;
			}
		}

		// fetch the customer account
		$customerAccount = $this->CustomerAccount->find('first', [
			'conditions' => ['CustomerAccount.id' => $cust_acct_id],
			'contain' => [
				'PrimaryCustomer'  => [
					'id',
					'full_name',
					'salutation',
					'first_name',
					'middle_name',
					'last_name',
					'nickname',
					'email',
					'work_phone_number',
					'home_phone_number',
					'cell_phone_number'
				],
				'BillingAmount'    => ['order' => 'due_date DESC', 'limit' => '5'],
				'Service'          => ['id','name'],
				'Floor'            => ['id','short_global_label'],
				'AgingInteraction' => ['id','type','creation_dt','note','Employee.full_name', 'order' => 'creation_dt DESC'],
				'Promise'          => ['id','notes','amount','expiration_date','status','Employee.full_name','Customer.full_name','order' => 'expiration_date DESC']
			]
		]);

		// customers related to this customer account
		$customers = $this->Customer->find('list', [
			'conditions' => compact('cust_acct_id')
		]);

		// get the types of aging interactions
		$types = $this->AgingInteraction->types;

		// send variables to view
		$this->set(compact(
			'customerAccount',
			'modifiers',
			'types',
			'cust_acct_id',
			'customers'
		));
		
		$this->set('title_for_layout','Aging Customer Account');
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AgingInteraction->exists($id)) {
			throw new NotFoundException(__('Invalid aging interaction'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AgingInteraction->save($this->request->data)) {
				$this->Session->setFlash(__('The aging interaction has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The aging interaction could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AgingInteraction.' . $this->AgingInteraction->primaryKey => $id));
			$this->request->data = $this->AgingInteraction->find('first', $options);
		}
		$customerAccounts = $this->AgingInteraction->CustomerAccount->find('list');
		$employees = $this->AgingInteraction->Employee->find('list');
		$modifiers = $this->AgingInteraction->Modifier->find('list');
		$this->set(compact('customerAccounts', 'employees', 'modifiers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AgingInteraction->id = $id;
		if (!$this->AgingInteraction->exists()) {
			throw new NotFoundException(__('Invalid aging interaction'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AgingInteraction->delete()) {
			$this->Session->setFlash(__('The aging interaction has been deleted.'));
		} else {
			$this->Session->setFlash(__('The aging interaction could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

}

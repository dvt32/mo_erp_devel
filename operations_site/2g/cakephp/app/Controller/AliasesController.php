<?php
App::uses('AppController', 'Controller');
/**
 * Aliases Controller
 *
 * @property Alias $Alias
 * @property PaginatorComponent $Paginator
 */
class AliasesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Alias->recursive = 0;
		$this->set('aliases', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Alias->exists($id)) {
			throw new NotFoundException(__('Invalid alias'));
		}
		$options = array('conditions' => array('Alias.' . $this->Alias->primaryKey => $id));
		$this->set('alias', $this->Alias->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Alias->create();
			if ($this->Alias->save($this->request->data)) {
				$this->Session->setFlash(__('The alias has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The alias could not be saved. Please, try again.'));
			}
		}
		$accounts = $this->Alias->Account->find('list');
		$this->set(compact('accounts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Alias->exists($id)) {
			throw new NotFoundException(__('Invalid alias'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Alias->save($this->request->data)) {
				$this->Session->setFlash(__('The alias has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The alias could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Alias.' . $this->Alias->primaryKey => $id));
			$this->request->data = $this->Alias->find('first', $options);
		}
		$accounts = $this->Alias->Account->find('list');
		$this->set(compact('accounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Alias->id = $id;
		if (!$this->Alias->exists()) {
			throw new NotFoundException(__('Invalid alias'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Alias->delete()) {
			$this->Session->setFlash(__('The alias has been deleted.'));
		} else {
			$this->Session->setFlash(__('The alias could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CrudControllerTrait', 'Crud.Lib');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	use CrudControllerTrait;

/**
 * List of global controller components
 *
 * @var array
 */
	public $components = [
		'RequestHandler',
		'Paginator' => ['settings' => ['paramType' => 'querystring', 'limit' => 30]],
		'Session',
		'DebugKit.Toolbar',
		'Auth' => array(
			'authenticate' => array(
				AuthComponent::ALL => array('userModel' => 'Employee', 'fields' => array('username' => 'windows_logon_name')),
				// 'Basic',
				'MicroOffice',
			),
			// 'loginAction' => array('controller' => 'employees', 'action' => 'login'),
			'loginRedirect' => array('controller' => 'employees', 'action' => 'dashboard'),
			// 'logoutRedirect' => array('controller' => 'employees', 'action' => 'login'),
			'authorize' => array('Controller'),
		),
		'Alert'
	];

	public $helpers = array(
		'Html',
		//'Form',
		'Session',
		'Rbac',
	);

	public function beforeRender() {
		$next = $this->request->query('next');
		if (!empty($next)) {
			$this->redirect($next);
		}
	}

	public function checkRole($required, $set_flash = true)
	{
		$assigned = $this->Auth->user('roles');
		$authorized = in_array($required, $assigned);
		return $authorized;
	}

	public function beforeFilter()
	{
		parent::beforeFilter();
		AuthComponent::$sessionKey = false;
		$this->Auth->unauthorizedRedirect = false;
	}

/**
 * Check user role for authorization level
 * @return boolean
 */
	public function isAuthorized($user) {

		// if action is cron-method
		if (substr(strtoupper($this->request->action), 0,4)==="CRON") {

			// check the cron key
			if ($this->isCronAllowed(@$this->request->pass[0])) {
				return true;
			} else {
				throw new UnauthorizedException('Invalid CRON key, check constants.php');
			}
		}

		$roles = $user['roles'];

		$this->uses[] = 'Aco';
		$this->uses[] = 'Permission';

		$aco = $this->Aco->find('first', [
			'conditions' => ['controller' => $this->name, 'action' => $this->action],
			'fields' => ['id']
		]);

		// if (!empty($aco)) {

			$aco_id = @$aco['Aco']['id'];
			// debug($aco_id);

			$permissions = $this->Permission->find('all', [
				'conditions' => ['aco_id' => $aco_id]
			]);

			// debug($permissions);

			if (empty($permissions))
			{
				// no rules, allow anyone to access
				return true;
			}
			else
			{
				$required = collect('Permission.role_name', $permissions);
				$intersection = array_intersect($roles, $required);
				// debug(compact('required','roles','intersection')); exit;
				return !empty($intersection);
			}
		// }
	}

	public function isCronAllowed($cron_key = false) {
		return ($cron_key === CRON_KEY);
	}


/**
 * Return tasks filtered by query params
 * 
 * @param array $query request query keys and values
 * @param array $options array of options
 * @return 
 */
	protected function filterByQuery($query = array(), $options = array()) {
		$filters = $this->queryToFilters($query, $options);
		$conditions = $this->filtersToConditions($filters, $options);
		return $conditions;
	}


/**
 * Transforms a query array into an array of filters.
 * 
 * @param array $query array of fields
 * @param string $model name of the model to filter
 * @return array filters to pass to method 'filterToConditions'
 */
	private function queryToFilters($query = array(), $options = array()) {

		// set defaults
		$model = @$options['model'] ?: $this->uses[0];

		// collect filters
		$filters = array();

		// add query fields to filters
		foreach ($query as $key => $val) {

			if (!empty($val) || $val === '0') {

				$plural = Inflector::pluralize($key);
				
				// if the model has a corresponding attribute...
				if (isset($this->$model->$plural)) {

					// TODO safety check for parent class attributes

					$list = $this->$model->$plural;
					$filters[$key] = @$list[$val];
				}
				// otherwise...
				else {
					$filters[$key] = $val;
				}
			}
		}

		return $filters;
	}

/**
 * True or False data type
 * @datatype
 * 
 */
	public $trueFalse = array('false', 'true');

/**
 * Transforms a filter array into an array of conditions
 * NOTE: single-letter fields are reserved for flags, e.g., 'q' for the search string
 * 
 * @param array $filters array of filters
 * @param array $options array of options
 * @param string $model name of the model to filter
 */
	private function filtersToConditions($filters = array(), $options = array()) {

		// set defaults
		$model = @$options['model'] ?: $this->uses[0];
		$fulltext = array();
		$ignore = array('limit');
		if (!empty($options['fulltext'])) {
			if (is_array($options['fulltext']))       $fulltext = $options['fulltext'];
			elseif (is_string($options['fulltext']))  $fulltext[] = $options['fulltext'];
		}
		if (!empty($options['ignore'])) {
			if (is_array($options['ignore']))       $ignore = $options['ignore'];
			elseif (is_string($options['ignore']))  $ignore[] = $options['ignore'];
		}

		
		// collect conditions
		$conditions = array();

		// convert filter to condition
		foreach ($filters as $key => $filter) {

			if (in_array($key, $ignore)) {
				continue;
			}

			// skip reserved namespace (single letter keys)
			if (strlen($key) > 1) {

				$field = strpos($key,'.') > 0 ? "{$key}" : "{$model}.{$key}";

				// numeric (exact match) or array (multi-match)
				if (is_numeric($filter) || is_array($filter)) {
					$conditions["{$field}"] = $filter;
				}
				// string (case-insensitive, exact match)
				else {
					$conditions["{$field} ILIKE"] = $filter;
				}
			}

			// 'q' is reserved for search string
			elseif (strtolower($key) === 'q' && !empty($filter)) {

				// perform case-insensitive, wildcard match on fulltext fields
				if (!empty($fulltext)) {
					foreach ($fulltext as $key) {
						$field = strpos($key,'.') > 0 ? "{$key}" : "{$model}.{$key}";
						$conditions['OR'][]["{$field} ILIKE"] = "%{$filters['q']}%";
					}
				}
			}
		}

		return $conditions;
	}

}

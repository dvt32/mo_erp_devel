<?php
App::uses('AppController', 'Controller');
/**
 * BillingAmounts Controller
 *
 * @property BillingAmount $BillingAmount
 * @property PaginatorComponent $Paginator
 */
class BillingAmountsController extends AppController {


/**
 * Allow user to login before authentication
 * 
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();

		// allow cron
		$this->Auth->allow(
			'cronEmailAging1',
			'cronEmailAging2',
			'cronEmailAging3',
			'cronEmailAging4',
			'cronEmailAging5'
		);
	}

	public $uses = array('BillingAmount','CustomerAccount');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		// require role:finance/make_deposit
		$this->checkRole('role:finance/make_deposits');

		$this->BillingAmount->recursive = 0;

		$query = $this->request->query;

		// filter/search/sort
		if (!empty($query)) {

			// build conditions
			$conditions = $this->filterByQuery($query, array(
				'fulltext' => array(
					'grand_total',
					'balance',
				),
				'ignore' => array(
					'stage',
					'due_date',
					'start_date',
					'end_date',
					'min_gt',
					'max_gt',
					'floor_id'
				)
			));

			// filter by stage
			$stage = @$query['stage'];
			if (!empty($stage)) {
				$conditions['BillingAmount.stage'] = $stage;
			}

			// filter by start-date
			$start_date = @$query['start_date'];
			if (!empty($start_date)) {
				$conditions['BillingAmount.due_date >='] = $start_date;
			}
			// filter by end-date
			$end_date = @$query['end_date'];
			if (!empty($end_date)) {
				$conditions['BillingAmount.due_date <='] = $end_date;
			}

			// filter by min_gt
			$min_gt = @$query['min_gt'];
			if (!empty($min_gt)) {
				$conditions['BillingAmount.grand_total >='] = $min_gt;
			}

			// filter by max_gt
			$max_gt = @$query['max_gt'];
			if (!empty($max_gt)) {
				$conditions['BillingAmount.grand_total <='] = $max_gt;
			}

			// filter by floor_id
			$floor_id = @$query['floor_id'];
			if (!empty($floor_id)) {
				$conditions['CustomerAccount.primary_floor_id'] = $floor_id; // TOGGLE
			}

			// set form data
			$this->request->data['BillingAmount'] = $this->request->query;
		}

		$limit = $this->request->query('limit') ?: 50;

		// fetch billing amounts
		$this->Paginator->settings = array('BillingAmount' => compact('conditions','contain','limit','order','fields'));
		$billingAmounts = $this->Paginator->paginate();

		// list of customers for dropdown
		$customerAccounts = $this->CustomerAccount->find('list', array(
			'conditions' => array('is_active' => 1),
			'fields' => array('id','name')
		));

		// list of floors for dropdown
		$floors = $this->CustomerAccount->Floor->find('list', array(
			'conditions' => array(
				'Floor.is_active' => true,
				'Building.is_active' => true
			),
			'order' => 'Floor.short_global_label ASC',
			'contain' => array('Building.is_active')
		));

		$stages = $this->BillingAmount->stages;
		
		$this->set(compact('billingAmounts','customerAccounts','stages','floors'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->BillingAmount->exists($id)) {
			throw new NotFoundException(__('Invalid billing amount'));
		}
		$billingAmount = $this->BillingAmount->find('first', array(
			'conditions' => array('BillingAmount.id' => $id),
			'contain' => array('Modifier','CustomerAccount')
		));
		$this->set(compact('billingAmount'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->BillingAmount->create();
			if ($this->BillingAmount->save($this->request->data)) {
				$this->Session->setFlash(__('The billing amount has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing amount could not be saved. Please, try again.'));
			}
		}
		$modifiers = $this->BillingAmount->Modifier->find('list');
		$custAccts = $this->BillingAmount->CustAcct->find('list');
		$this->set(compact('modifiers', 'custAccts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->BillingAmount->exists($id)) {
			throw new NotFoundException(__('Invalid billing amount'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BillingAmount->save($this->request->data)) {
				$this->Session->setFlash(__('The billing amount has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing amount could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BillingAmount.' . $this->BillingAmount->primaryKey => $id));
			$this->request->data = $this->BillingAmount->find('first', $options);
		}
		$modifiers = $this->BillingAmount->Modifier->find('list');
		$custAccts = $this->BillingAmount->CustAcct->find('list');
		$this->set(compact('modifiers', 'custAccts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->BillingAmount->id = $id;
		if (!$this->BillingAmount->exists()) {
			throw new NotFoundException(__('Invalid billing amount'));
		}
		// reset CustomerAccount.billing_last_reviewed_dt to -32 days
		$this->request->onlyAllow('post', 'delete');
		if ($this->BillingAmount->delete()) {
			$this->Session->setFlash(__('The billing amount has been deleted.'));
		} else {
			$this->Session->setFlash(__('The billing amount could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function updateAllBalances() {
		$this->BillingAmount->updateAllBalances();
	}

/**
 * List of receiving billing amounts (invoices)
 */
	public function receiving() {
		$this->BillingAmount->recursive = 0;

		$query = $this->request->query;

		// filter/search/sort
		if (!empty($query)) {

			// build conditions
			$conditions = $this->filterByQuery($query, array(
				'fulltext' => array(
					'grand_total',
					'balance',
				),
				'ignore' => array(
					'stage',
					'due_date',
					'start_date',
					'end_date',
					'min_gt',
					'max_gt',
				)
			));

			// filter by start-date
			$start_date = @$query['start_date'];
			if (!empty($start_date)) {
				$conditions['BillingAmount.due_date >='] = $start_date;
			}
			// filter by end-date
			$end_date = @$query['end_date'];
			if (!empty($end_date)) {
				$conditions['BillingAmount.due_date <='] = $end_date;
			}

			// filter by min_gt
			$min_gt = @$query['min_gt'];
			if (!empty($min_gt)) {
				$conditions['BillingAmount.grand_total >='] = $min_gt;
			}

			// filter by max_gt
			$max_gt = @$query['max_gt'];
			if (!empty($max_gt)) {
				$conditions['BillingAmount.grand_total <='] = $max_gt;
			}

			// set form data
			$this->request->data['BillingAmount'] = $this->request->query;
		}
		
		$conditions['BillingAmount.stage'] = 'receiving';

		$limit = $this->request->query('limit') ?: 50;

		// fetch billing amounts
		$this->Paginator->settings = array('BillingAmount' => compact('conditions','contain','limit','order','fields'));
		$billingAmounts = $this->Paginator->paginate();

		// list of customers for dropdown
		$customerAccounts = $this->CustomerAccount->find('list', array(
			'conditions' => array('is_active' => 1),
			'fields' => array('id','name')
		));

		$stages = $this->BillingAmount->stages;
		
		$this->set(compact('billingAmounts','customerAccounts','stages'));
	}

/**
 * aging method
 *
 * @return void
 */
	public function aging() {
		$this->BillingAmount->recursive = 0;

		$query = $this->request->query;

		if (empty($this->request->data['BillingAmount']['stage'])) {
			
			$query['stage'] = array('aging_hold', 'aging_1', 'aging_2', 'aging_3', 'aging_4');
		}

		// filter/search/sort
		if (!empty($query)) {

			// build conditions
			$conditions = $this->filterByQuery($query, array(
				'fulltext' => array(
					'grand_total',
					'balance',
				),
				'ignore' => array(
					'stage',
					'due_date',
					'start_date',
					'end_date',
					'min_gt',
					'max_gt',
					'floor_id'
				)
			));

			// filter by stage
			$stage = @$query['stage'];
			if (!empty($stage)) {
				$conditions['BillingAmount.stage'] = $stage;
			}

			// filter by start-date
			$start_date = @$query['start_date'];
			if (!empty($start_date)) {
				$conditions['BillingAmount.due_date >='] = $start_date;
			}
			// filter by end-date
			$end_date = @$query['end_date'];
			if (!empty($end_date)) {
				$conditions['BillingAmount.due_date <='] = $end_date;
			}

			// filter by min_gt
			$min_gt = @$query['min_gt'];
			if (!empty($min_gt)) {
				$conditions['BillingAmount.grand_total >='] = $min_gt;
			}

			// filter by max_gt
			$max_gt = @$query['max_gt'];
			if (!empty($max_gt)) {
				$conditions['BillingAmount.grand_total <='] = $max_gt;
			}

			// filter by floor_id
			$floor_id = @$query['floor_id'];
			if (!empty($floor_id)) {
				$conditions['CustomerAccount.primary_floor_id'] = $floor_id; // TOGGLE
			}

			// set form data
			$this->request->data['BillingAmount'] = $query;
		}

		$limit = $this->request->query('limit') ?: 50;

		// fetch billing amounts
		$this->Paginator->settings = array('BillingAmount' => compact('conditions','contain','limit','order','fields'));
		$billingAmounts = $this->Paginator->paginate();

		// list of customers for dropdown
		$customerAccounts = $this->CustomerAccount->find('list', array(
			'conditions' => array('is_active' => 1),
			'fields' => array('id','name')
		));

		// list of floors for dropdown
		$floors = $this->CustomerAccount->Floor->find('list', array(
			'conditions' => array(
				'Floor.is_active' => true,
				'Building.is_active' => true
			),
			'order' => 'Floor.short_global_label ASC',
			'contain' => array('Building.is_active')
		));

		$stages = $this->BillingAmount->stages;

		
		$this->set(compact('billingAmounts','customerAccounts','stages','floors'));
	}

/**
 * Cron Email Aging 1
 * 
 * 
 */
	public function cronEmailAging1($cust_acct_id = null ,$cron_key = false) {
		// if called by a CRON JOB
		if ($this->RequestHandler->accepts('json')) {
			if (!$this->isCronAllowed($cron_key)) {
				throw new UnauthorizedException('Invalid CRON key');
			}
		}

		// email primary customer accounts that are aging
		$accounts = $this->BillingAmount->find('all', array(
			'conditions' => array(
				'stage' => 'aging_1',
				'cust_acct_id' => $cust_acct_id
			),
			'contain' => array(
				'CustomerAccount' => 'PrimaryCustomer'
			)
		));
		if (!empty($accounts[0]['CustomerAccount']['PrimaryCustomer'])) {
			foreach ($accounts as $account) {

				// account first name
				$company_name = $account['CustomerAccount']['name'];
				$full_name = $account['CustomerAccount']['PrimaryCustomer']['full_name'];
				$amount_due = $account['CustomerAccount']['balance'];
				$today_nice =  date('l, F d, Y');

				// Email Accounts that are overdue
				$html = '<html> <style type="text/css">ol{margin:0;padding:0}.c7{line-height:1.0;padding-top:0pt;text-align:center;padding-bottom:0pt}.c0{max-width:468pt;background-color:#ffffff;padding:72pt 72pt 72pt 72pt}.c10{line-height:1.0;padding-top:0pt;padding-bottom:0pt}.c1{color:#ff6600;font-size:9pt;font-family:"Times New Roman"}.c6{color:#0563c1;text-decoration:underline}.c9{color:inherit;text-decoration:inherit}.c2{font-size:9pt;font-family:"Times New Roman"}.c3{height:10pt;direction:ltr}.c4{font-size:10pt;font-family:"Times New Roman"}.c5{direction:ltr}.c8{text-align:center}.c11{vertical-align:super}.c12{font-weight:bold}.title{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:36pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}.subtitle{padding-top:18pt;line-height:1.0;text-align:left;color:#666666;font-style:italic;font-size:24pt;font-family:"Georgia";padding-bottom:4pt;page-break-after:avoid}li{color:#000000;font-size:10pt;font-family:"Times New Roman"}p{color:#000000;font-size:10pt;margin:0;font-family:"Times New Roman"}h1{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:24pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}h2{padding-top:18pt;line-height:1.0;text-align:left;color:#000000;font-size:18pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h3{padding-top:14pt;line-height:1.0;text-align:left;color:#000000;font-size:14pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h4{padding-top:12pt;line-height:1.0;text-align:left;color:#000000;font-size:12pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h5{padding-top:11pt;line-height:1.0;text-align:left;color:#000000;font-size:11pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h6{padding-top:10pt;line-height:1.0;text-align:left;color:#000000;font-size:10pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid} </style><body class="c0"> <div> <p class="c7 c5"><img height="129" src="'.
					EMAIL_IMAGE_000.'" width="244"></p></div> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3 c8"><span></span></p> <p class="c5"><span>'.
					$today_nice.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>J. Hairston</span></p> <p class="c5"><span>136 East 55th St, 3P, New York, NY 10022</span></p> <p class="c5"><span>212-593-2422</span></p> <p class="c3"><span></span></p> <p class="c5"><span>Re: Late Payment Notice</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span>Dear '.
					$full_name.':</span></p> <p class="c3"><span></span></p> <p class="c5"><span>Thank you for choosing MicroOffice to meet your office space needs.&nbsp; We are sending this message to let you know that we have not received payment for your balance of $'.
					$amount_due.'. If you already made the payment, please call or email me today with the details so that we can cancel the automatic late fee that would be on the 6</span><span class="c11">th</span><span>&nbsp;day of the month.</span></p> <p class="c3"><span></span></p> <p class="c5"><span>As ever, if you have extenuating circumstances, please feel free to contact me with details so that we can work together to move forward.</span></p> <p class="c3"><span></span></p> <p class="c5"><a name="h.gjdgxs"></a><span>To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at </span><span class="c6"><a class="c9" href="https://bizcenter.microoffice.com/">https://bizcenter.microoffice.com</a></span><span>. &nbsp;You are also welcome to leave a check with your community manager at the front desk. </span></p> <p class="c3"><span></span></p> <p class="c5"><span>Thank you,</span></p> <p class="c5"><span>&nbsp;</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span class="c12">Jin Cho</span></p> <p class="c5"><span>Department of Finance, MicroOffice</span></p> <p class="c3"><span></span></p> <p class="c5"><span>1375 Broadway, Floor 3</span></p> <p class="c5"><span>New York, NY 10018</span></p> <p class="c5"><span class="c6"><a class="c9" href="mailto:jin@microoffice.com">jin@microoffice.com</a></span><span>&nbsp;| 212-359-4201</span></p> <p class="c3"><span></span></p> <p class="c5"><span>cc &ndash; Management, Billing</span></p> <p class="c3 c8"><span></span></p> <div> <p class="c3 c10"><span class="c2"></span></p> <p class="c7 c5"><img height="26" src="'.
					EMAIL_IMAGE_001.'" width="26"><span class="c4">&nbsp; &nbsp;</span><img height="28" src="'.
					EMAIL_IMAGE_002.'" width="28"><span class="c2">&nbsp; &nbsp;</span><img></p> <p class="c5 c7"><span class="c2">www. MicroOffice.com </span><span class="c1">&bull;</span><span class="c2">&nbsp;sales@microoffice.com </span><span class="c1">&bull; </span><span class="c2">t. 646-201-5508 </span><span class="c1">&bull;</span><span class="c2">&nbsp;f. 866-788-8109</span></p> <p class="c7 c5"><span class="c1">New York | Chicago | Boston</span></p> </div> </body> </html>';
				$text = 'Dear '.$full_name.':'.
				"\n\n"."Thank you for choosing MicroOffice to meet your office space needs.\n We are sending this message to let you know that we have not received payment for your balance of $'.$amount_due.'. If you already made the payment, please call or email me today with the details so that we can cancel the automatic late fee that would be on the 6th day of the month. \n".
				"\n\n"."As ever, if you have extenuating circumstances, please feel free to contact me with details so that we can work together to move forward.".
				"\n\n"."To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at https://bizcenter.microoffice.com You are also welcome to leave a check with your community manager at the front desk.".
				"\n\n"."Thank you,".
				"\n\n"."Jin Cho".
				"\n"."Department of Finance, MicroOffice".
				"\n"."1375 Broadway, Floor 3".
				"\n"."New York, NY 10018".
				"\n"."jin@microoffice.com".
				"\n"."212-359-4201";
				

				// build email message
				$email = array(
					'to' => array(
						array(
							'email' => $account['CustomerAccount']['PrimaryCustomer']['email'],
							// 'email' => 'jj@pith-studio.com',
							'name' => $account['CustomerAccount']['PrimaryCustomer']['full_name'],
							'type' => 'to'
						)
					),
					'subject' => 'MicroOffice Payment Overdue: '.$company_name,
					'text' => $text,
					'html' => $html
				);
				
				// add to outgoing list
				$emails[] = $email;
			}
			if (!empty($emails)) {
				// send email
				if (REALLY_SEND_EMAIL === true) {
					foreach ($emails as $email) {
						$sent = $this->BillingAmount->sendEmail($email);
					}
				}
			}
			$return = true;

		} else {
			$return = false;
		}
		
		$this->set(compact('return'));
		$this->set('_serialize', array('return'));

	}

/**
 * Cron Email Aging 2
 * Method per customer due to large list
 * 
 */
	public function cronEmailAging2($cust_acct_id = null ,$cron_key = false) {
		// if called by a CRON JOB
		if ($this->RequestHandler->accepts('json')) {
			if (!$this->isCronAllowed($cron_key)) {
				$return = false;

			}
		}

		$this->uses[] = 'CustomerAccount';
		// cust account doesn't exists
		if (!$this->CustomerAccount->exists($cust_acct_id)) {

			$return = false;

		} else {
			// email primary customer accounts that are aging
			$accounts = $this->BillingAmount->find('all', array(
				'conditions' => array(
					'stage' => 'aging_2',
					'cust_acct_id' => $cust_acct_id
				),
				'contain' => array(
					'CustomerAccount' => 'PrimaryCustomer'
				)
			));

			if (!empty($accounts[0]['CustomerAccount']['PrimaryCustomer'])) { 

				foreach ($accounts as $account) {

					// debug($account); die;

					// account first name
					@$company_name = $account['CustomerAccount']['name'];
					@$full_name = $account['CustomerAccount']['PrimaryCustomer']['full_name'];
					@$address = $account['CustomerAccount']['PrimaryCustomer']['work_address_1']." ".$account['CustomerAccount']['PrimaryCustomer']['work_address_2']." ".$account['CustomerAccount']['PrimaryCustomer']['work_city'].", ".$account['CustomerAccount']['PrimaryCustomer']['work_state']." ".$account['CustomerAccount']['PrimaryCustomer']['work_zip_code'];
					@$phone = $account['CustomerAccount']['PrimaryCustomer']['work_phone_number'];
					@$amount_due = $account['CustomerAccount']['balance'];
					$title = 'Re: Suspension Pending Notice';
					$today_nice = date(DATE_FORMAT_USA);
					// Email Accounts that are overdue
					$html = '<html> <style type="text/css">ol{margin:0;padding:0}.c7{line-height:1.0;padding-top:0pt;text-align:center;padding-bottom:0pt}.c0{max-width:468pt;background-color:#ffffff;padding:72pt 72pt 72pt 72pt}.c10{line-height:1.0;padding-top:0pt;padding-bottom:0pt}.c1{color:#ff6600;font-size:9pt;font-family:"Times New Roman"}.c6{color:#0563c1;text-decoration:underline}.c9{color:inherit;text-decoration:inherit}.c2{font-size:9pt;font-family:"Times New Roman"}.c3{height:10pt;direction:ltr}.c4{font-size:10pt;font-family:"Times New Roman"}.c5{direction:ltr}.c8{text-align:center}.c11{vertical-align:super}.c12{font-weight:bold}.title{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:36pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}.subtitle{padding-top:18pt;line-height:1.0;text-align:left;color:#666666;font-style:italic;font-size:24pt;font-family:"Georgia";padding-bottom:4pt;page-break-after:avoid}li{color:#000000;font-size:10pt;font-family:"Times New Roman"}p{color:#000000;font-size:10pt;margin:0;font-family:"Times New Roman"}h1{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:24pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}h2{padding-top:18pt;line-height:1.0;text-align:left;color:#000000;font-size:18pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h3{padding-top:14pt;line-height:1.0;text-align:left;color:#000000;font-size:14pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h4{padding-top:12pt;line-height:1.0;text-align:left;color:#000000;font-size:12pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h5{padding-top:11pt;line-height:1.0;text-align:left;color:#000000;font-size:11pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h6{padding-top:10pt;line-height:1.0;text-align:left;color:#000000;font-size:10pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid} </style><body class="c0"> <div> <p class="c7 c5"><img height="129" src="'.
						EMAIL_IMAGE_000.'" width="244"></p></div> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3 c8"><span></span></p> <p class="c5"><span>'.
						$today_nice.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$full_name.'</span></p> <p class="c5"><span>'.
						$address.'</span></p> <p class="c5"><span>'.
						$phone.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$title.'</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span>Dear '.
						$full_name.':</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
							'<p class="c5"><span>'.'This communication is to notify you that, according to our records, your organization holds an outstanding balance that is at least 30 days past-due. As you are aware, collection of payments enables us to serve all of the businesses in our communities. To avoid an interruption of service, please review this message and contact us as soon as possible.'.'</span></p><br>'.
							'<p class="c5"><span>'.'If you have already sent payment and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward. You can find our contact information below.'.'</span></p><br>'.
							'<p class="c5"><span>'.'To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at https://bizcenter.microoffice.com. You are also welcome to leave a check with your community manager at the front desk. '.'</span></p><br>'.
							'<p class="c5"><span>'.'As of today, your company has an unpaid balance of '.
						$amount_due.'. To avoid suspension of your account and an interruption of service, please make a full payment by the 15th of next month or contact us immediately.</span></p> <p class="c3"><span></span></p> <p class="c5"><span>As ever, if you have extenuating circumstances, please feel free to contact me with details so that we can work together to move forward.</span></p> <p class="c3"><span></span></p> <p class="c5"><a name="h.gjdgxs"></a><span>To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at </span><span class="c6"><a class="c9" href="https://bizcenter.microoffice.com/">https://bizcenter.microoffice.com</a></span><span>. &nbsp;You are also welcome to leave a check with your community manager at the front desk. </span></p> <p class="c3"><span></span></p> <p class="c5"><span>Thank you,</span></p> <p class="c5"><span>&nbsp;</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span class="c12">Jin Cho</span></p> <p class="c5"><span>Department of Finance, MicroOffice</span></p> <p class="c3"><span></span></p> <p class="c5"><span>1375 Broadway, Floor 3</span></p> <p class="c5"><span>New York, NY 10018</span></p> <p class="c5"><span class="c6"><a class="c9" href="mailto:jin@microoffice.com">jin@microoffice.com</a></span><span>&nbsp;| 212-359-4201</span></p> <p class="c3"><span></span></p> <p class="c5"><span>cc &ndash; Management, Billing</span></p> <p class="c3 c8"><span></span></p> <div> <p class="c3 c10"><span class="c2"></span></p> <p class="c7 c5"><img height="26" src="'.
						EMAIL_IMAGE_001.'" width="26"><span class="c4">&nbsp; &nbsp;</span><img height="28" src="'.
						EMAIL_IMAGE_002.'" width="28"><span class="c2">&nbsp; &nbsp;</span><img></p> <p class="c5 c7"><span class="c2">www. MicroOffice.com </span><span class="c1">&bull;</span><span class="c2">&nbsp;sales@microoffice.com </span><span class="c1">&bull; </span><span class="c2">t. 646-201-5508 </span><span class="c1">&bull;</span><span class="c2">&nbsp;f. 866-788-8109</span></p> <p class="c7 c5"><span class="c1">New York | Chicago | Boston</span></p> </div> </body> </html>';
					$text = 'Dear '.$full_name.':'.
					"\n\n"."This communication is to notify you that, according to our records, your organization holds an outstanding balance that is at least 30 days past-due. As you are aware, collection of payments enables us to serve all of the businesses in our communities. To avoid an interruption of service, please review this message and contact us as soon as possible. \n".
					"\n\n"."If you have already sent payment and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward. You can find our contact information below.".
					"\n\n"."To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at https://bizcenter.microoffice.com. You are also welcome to leave a check with your community manager at the front desk. ".
					"\n\n"."As of today, your company has an unpaid balance of '.$amount_due.'. To avoid suspension of your account and an interruption of service, please make a full payment by the 15th of next month or contact us immediately. ".
					"\n\n"."Thank you,".
					"\n\n"."Jin Cho".
					"\n"."Department of Finance, MicroOffice".
					"\n"."1375 Broadway, Floor 3".
					"\n"."New York, NY 10018".
					"\n"."jin@microoffice.com".
					"\n"."212-359-4201";
					

					// build email message
					$email = array(
						'to' => array(
							array(
								'email' => $account['CustomerAccount']['PrimaryCustomer']['email'],
								// 'email' => 'jj@pith-studio.com',
								'name' => $account['CustomerAccount']['PrimaryCustomer']['full_name'],
								'type' => 'to'
							)
						),
						'subject' => 'MicroOffice Payment Overdue: '.$company_name,
						'text' => $text,
						'html' => $html
					);
					
					// add to outgoing list
					$emails[] = $email;
				}
				
			} else {
				$return = false;
			}


			// send email
			if (REALLY_SEND_EMAIL === true) {
				foreach ($emails as $email) {

					$sent = $this->BillingAmount->sendEmail($email);

				}
			}
			// if an emails array is not empty
			if (!empty($emails)) {
				$return = true;
			} else {
				$return = false;
			}
		}

		$this->set(compact('return'));
		$this->set('_serialize', array('return'));

	}

/**
 * Cron Email Aging 3
 * Method per customer due to large list
 * 
 */
	public function cronEmailAging3($cust_acct_id = null ,$cron_key = false) {
		// if called by a CRON JOB
		if ($this->RequestHandler->accepts('json')) {
			if (!$this->isCronAllowed($cron_key)) {
				$return = false;

			}
		}

		$this->uses[] = 'CustomerAccount';
		// cust account doesn't exists
		if (!$this->CustomerAccount->exists($cust_acct_id)) {

			$return = false;

		} else {
			// email primary customer accounts that are aging
			$accounts = $this->BillingAmount->find('all', array(
				'conditions' => array(
					'stage' => 'aging_3',
					'cust_acct_id' => $cust_acct_id
				),
				'contain' => array(
					'CustomerAccount' => 'PrimaryCustomer'
				)
			));

			if (!empty($accounts[0]['CustomerAccount']['PrimaryCustomer'])) { 

				foreach ($accounts as $account) {

					// debug($account); die;

					// account first name
					@$company_name = $account['CustomerAccount']['name'];
					@$full_name = $account['CustomerAccount']['PrimaryCustomer']['full_name'];
					@$address = $account['CustomerAccount']['PrimaryCustomer']['work_address_1']." ".$account['CustomerAccount']['PrimaryCustomer']['work_address_2']." ".$account['CustomerAccount']['PrimaryCustomer']['work_city'].", ".$account['CustomerAccount']['PrimaryCustomer']['work_state']." ".$account['CustomerAccount']['PrimaryCustomer']['work_zip_code'];
					@$phone = $account['CustomerAccount']['PrimaryCustomer']['work_phone_number'];
					@$amount_due = $account['CustomerAccount']['balance'];
					$title = 'Re: Warning of Suspension of Service';
					$today_nice = date(DATE_FORMAT_USA);
					// Email Accounts that are overdue
					$html = '<html> <style type="text/css">ol{margin:0;padding:0}.c7{line-height:1.0;padding-top:0pt;text-align:center;padding-bottom:0pt}.c0{max-width:468pt;background-color:#ffffff;padding:72pt 72pt 72pt 72pt}.c10{line-height:1.0;padding-top:0pt;padding-bottom:0pt}.c1{color:#ff6600;font-size:9pt;font-family:"Times New Roman"}.c6{color:#0563c1;text-decoration:underline}.c9{color:inherit;text-decoration:inherit}.c2{font-size:9pt;font-family:"Times New Roman"}.c3{height:10pt;direction:ltr}.c4{font-size:10pt;font-family:"Times New Roman"}.c5{direction:ltr}.c8{text-align:center}.c11{vertical-align:super}.c12{font-weight:bold}.title{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:36pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}.subtitle{padding-top:18pt;line-height:1.0;text-align:left;color:#666666;font-style:italic;font-size:24pt;font-family:"Georgia";padding-bottom:4pt;page-break-after:avoid}li{color:#000000;font-size:10pt;font-family:"Times New Roman"}p{color:#000000;font-size:10pt;margin:0;font-family:"Times New Roman"}h1{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:24pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}h2{padding-top:18pt;line-height:1.0;text-align:left;color:#000000;font-size:18pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h3{padding-top:14pt;line-height:1.0;text-align:left;color:#000000;font-size:14pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h4{padding-top:12pt;line-height:1.0;text-align:left;color:#000000;font-size:12pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h5{padding-top:11pt;line-height:1.0;text-align:left;color:#000000;font-size:11pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h6{padding-top:10pt;line-height:1.0;text-align:left;color:#000000;font-size:10pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid} </style><body class="c0"> <div> <p class="c7 c5"><img height="129" src="'.
						EMAIL_IMAGE_000.'" width="244"></p></div> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3 c8"><span></span></p> <p class="c5"><span>'.
						$today_nice.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$full_name.'</span></p> <p class="c5"><span>'.
						$address.'</span></p> <p class="c5"><span>'.
						$phone.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$title.'</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span>Dear '.
						$full_name.':</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
							'<p class="c5"><span>'.'This communication is to notify you that, according to our records, your organization holds an outstanding balance that is at least </span><span class="c5">40 days</span><span>&nbsp;past-due. As you are aware, collection of payments enables us to serve all of the businesses in our communities. To avoid an interruption of service, please review this message and remit payment as soon as possible.'.'</span></p><br>'.
							'<p class="c5"><span>'.'If you have already sent payment in full and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward.'.'</span></p><br>'.
							'<p class="c5"><span>'.'As of today, your company has an unpaid balance of </span><span class="c5">'.
						$amount_due.'</span><span>. &nbsp;To avoid suspension of your account and an interruption of service(s), please make a payment. &nbsp;</span><span class="c5">Please understand that if action is not taken within </span><span class="c5 c10">5 days</span><span class="c5">&nbsp;of this notice, all access cards on your account will be suspended and access to your space will not be provided until payment is made.</span><span>&nbsp; Additionally, your account will be charged a </span><span class="c5">$75</span><span>&nbsp;fee for reactivation of service(s). &nbsp;A non-response to this letter will result in further collection actions which may include the forwarding of your account to our attorneys. In the event of legal action, you will be liable for any attorney&rsquo;s fees and costs incurred as part of the collection process.'.'</span></p><br>'.
							'<p class="c3"><span></span></p> <p class="c5"><a name="h.gjdgxs"></a><span>To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at </span><span class="c6"><a class="c9" href="https://bizcenter.microoffice.com/">https://bizcenter.microoffice.com</a></span><span>. &nbsp;You are also welcome to leave a check with your community manager at the front desk. </span></p> <p class="c3"><span></span></p> <p class="c5"><span>Thank you,</span></p> <p class="c5"><span>&nbsp;</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span class="c12">Jin Cho</span></p> <p class="c5"><span>Department of Finance, MicroOffice</span></p> <p class="c3"><span></span></p> <p class="c5"><span>1375 Broadway, Floor 3</span></p> <p class="c5"><span>New York, NY 10018</span></p> <p class="c5"><span class="c6"><a class="c9" href="mailto:jin@microoffice.com">jin@microoffice.com</a></span><span>&nbsp;| 212-359-4201</span></p> <p class="c3"><span></span></p> <p class="c5"><span>cc &ndash; Management, Billing</span></p> <p class="c3 c8"><span></span></p> <div> <p class="c3 c10"><span class="c2"></span></p> <p class="c7 c5"><img height="26" src="'.
						EMAIL_IMAGE_001.'" width="26"><span class="c4">&nbsp; &nbsp;</span><img height="28" src="'.
						EMAIL_IMAGE_002.'" width="28"><span class="c2">&nbsp; &nbsp;</span><img></p> <p class="c5 c7"><span class="c2">www. MicroOffice.com </span><span class="c1">&bull;</span><span class="c2">&nbsp;sales@microoffice.com </span><span class="c1">&bull; </span><span class="c2">t. 646-201-5508 </span><span class="c1">&bull;</span><span class="c2">&nbsp;f. 866-788-8109</span></p> <p class="c7 c5"><span class="c1">New York | Chicago | Boston</span></p> </div> </body> </html>';
					$text = 'Dear '.$full_name.':'.
					"\n\n"."This communication is to notify you that, according to our records, your organization holds an outstanding balance that is at least 40 days past-due. As you are aware, collection of payments enables us to serve all of the businesses in our communities. To avoid an interruption of service, please review this message and remit payment as soon as possible. \n".
					"\n\n"."If you have already sent payment in full and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward.".
					"\n\n"."As of today, your company has an unpaid balance of  '.$amount_due.'. .  To avoid suspension of your account and an interruption of service(s), please make a payment.  Please understand that if action is not taken within 5 days of this notice, all access cards on your account will be suspended and access to your space will not be provided until payment is made.  Additionally, your account will be charged a $75 fee for reactivation of service(s).  A non-response to this letter will result in further collection actions which may include the forwarding of your account to our attorneys. In the event of legal action, you will be liable for any attorney’s fees and costs incurred as part of the collection process. ".
					"\n\n"."To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at https://bizcenter.microoffice.com.  You are also welcome to leave a check with your community manager at the front desk. ".
					"\n\n"."Thank you,".
					"\n\n"."Jin Cho".
					"\n"."Department of Finance, MicroOffice".
					"\n"."1375 Broadway, Floor 3".
					"\n"."New York, NY 10018".
					"\n"."jin@microoffice.com".
					"\n"."212-359-4201";
					

					// build email message
					$email = array(
						'to' => array(
							array(
								'email' => $account['CustomerAccount']['PrimaryCustomer']['email'],
								// 'email' => 'jj@pith-studio.com',
								'name' => $account['CustomerAccount']['PrimaryCustomer']['full_name'],
								'type' => 'to'
							)
						),
						'subject' => 'MicroOffice Payment Overdue: '.$company_name,
						'text' => $text,
						'html' => $html
					);
					
					// add to outgoing list
					$emails[] = $email;
				}
				
			} else {
				$return = false;
			}


			// send email
			if (REALLY_SEND_EMAIL === true) {
				foreach ($emails as $email) {

					$sent = $this->BillingAmount->sendEmail($email);

				}
			}
			// if an emails array is not empty
			if (!empty($emails)) {
				$return = true;
			} else {
				$return = false;
			}
		}

		$this->set(compact('return'));
		$this->set('_serialize', array('return'));

	}


/**
 * Cron Email Aging 4 - suspended
 * Method per customer due to large list
 * 
 */
	public function cronEmailAging4($cust_acct_id = null ,$cron_key = false) {
		// if called by a CRON JOB
		if ($this->RequestHandler->accepts('json')) {
			if (!$this->isCronAllowed($cron_key)) {
				$return = false;

			}
		}

		$this->uses[] = 'CustomerAccount';
		// cust account doesn't exists
		if (!$this->CustomerAccount->exists($cust_acct_id)) {

			$return = false;

		} else {
			// email primary customer accounts that are aging
			$accounts = $this->BillingAmount->find('all', array(
				'conditions' => array(
					'stage' => 'aging_4',
					'cust_acct_id' => $cust_acct_id
				),
				'contain' => array(
					'CustomerAccount' => 'PrimaryCustomer'
				)
			));

			if (!empty($accounts[0]['CustomerAccount']['PrimaryCustomer'])) { 

				foreach ($accounts as $account) {

					// debug($account); die;

					// account first name
					@$company_name = $account['CustomerAccount']['name'];
					@$full_name = $account['CustomerAccount']['PrimaryCustomer']['full_name'];
					@$address = $account['CustomerAccount']['PrimaryCustomer']['work_address_1']." ".$account['CustomerAccount']['PrimaryCustomer']['work_address_2']." ".$account['CustomerAccount']['PrimaryCustomer']['work_city'].", ".$account['CustomerAccount']['PrimaryCustomer']['work_state']." ".$account['CustomerAccount']['PrimaryCustomer']['work_zip_code'];
					@$phone = $account['CustomerAccount']['PrimaryCustomer']['work_phone_number'];
					@$amount_due = $account['CustomerAccount']['balance'];
					$title = 'Re: Warning of Suspension of Service';
					$today_nice = date(DATE_FORMAT_USA);
					// Email Accounts that are overdue
					$html = '<html> <style type="text/css">ol{margin:0;padding:0}.c7{line-height:1.0;padding-top:0pt;text-align:center;padding-bottom:0pt}.c0{max-width:468pt;background-color:#ffffff;padding:72pt 72pt 72pt 72pt}.c10{line-height:1.0;padding-top:0pt;padding-bottom:0pt}.c1{color:#ff6600;font-size:9pt;font-family:"Times New Roman"}.c6{color:#0563c1;text-decoration:underline}.c9{color:inherit;text-decoration:inherit}.c2{font-size:9pt;font-family:"Times New Roman"}.c3{height:10pt;direction:ltr}.c4{font-size:10pt;font-family:"Times New Roman"}.c5{direction:ltr}.c8{text-align:center}.c11{vertical-align:super}.c12{font-weight:bold}.title{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:36pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}.subtitle{padding-top:18pt;line-height:1.0;text-align:left;color:#666666;font-style:italic;font-size:24pt;font-family:"Georgia";padding-bottom:4pt;page-break-after:avoid}li{color:#000000;font-size:10pt;font-family:"Times New Roman"}p{color:#000000;font-size:10pt;margin:0;font-family:"Times New Roman"}h1{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:24pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}h2{padding-top:18pt;line-height:1.0;text-align:left;color:#000000;font-size:18pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h3{padding-top:14pt;line-height:1.0;text-align:left;color:#000000;font-size:14pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h4{padding-top:12pt;line-height:1.0;text-align:left;color:#000000;font-size:12pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h5{padding-top:11pt;line-height:1.0;text-align:left;color:#000000;font-size:11pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h6{padding-top:10pt;line-height:1.0;text-align:left;color:#000000;font-size:10pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid} </style><body class="c0"> <div> <p class="c7 c5"><img height="129" src="'.
						EMAIL_IMAGE_000.'" width="244"></p></div> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3 c8"><span></span></p> <p class="c5"><span>'.
						$today_nice.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$full_name.'</span></p> <p class="c5"><span>'.
						$address.'</span></p> <p class="c5"><span>'.
						$phone.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$title.'</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span>Dear '.
						$full_name.':</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
							'<p class="c5"><span>'.'This communication is to notify you that your account has been suspended and you no longer have access to your workspace. Access cannot be restored until your account is brought current and a $75 reactivation fee has been paid.'.'</span></p><br>'.
							'<p class="c5"><span>'.'As you are aware, collection of payments enables us to serve all of the businesses in our communities. If you have already sent payment in full and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward.'.'</span></p><br>'.
							'<p class="c5"><span>'.'As of today, your company has an unpaid balance of </span><span class="c6">'.
						$amount_due.'</span><span>. &nbsp;A non-response to this notice will result in the forwarding of your account to a collections agency, and possible legal action. In the event that legal action is necessary, you will be liable for any attorney&rsquo;s fees and costs incurred as part of the collection process.'.'</span></p><br>'.
							'<p class="c3"><span></span></p> <p class="c5"><a name="h.gjdgxs"></a><span>To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at </span><span class="c6"><a class="c9" href="https://bizcenter.microoffice.com/">https://bizcenter.microoffice.com</a></span><span>. &nbsp;You are also welcome to leave a check with your community manager at the front desk. </span></p> <p class="c3"><span></span></p> <p class="c5"><span>Thank you,</span></p> <p class="c5"><span>&nbsp;</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span class="c12">Jin Cho</span></p> <p class="c5"><span>Department of Finance, MicroOffice</span></p> <p class="c3"><span></span></p> <p class="c5"><span>1375 Broadway, Floor 3</span></p> <p class="c5"><span>New York, NY 10018</span></p> <p class="c5"><span class="c6"><a class="c9" href="mailto:jin@microoffice.com">jin@microoffice.com</a></span><span>&nbsp;| 212-359-4201</span></p> <p class="c3"><span></span></p> <p class="c5"><span>cc &ndash; Management, Billing</span></p> <p class="c3 c8"><span></span></p> <div> <p class="c3 c10"><span class="c2"></span></p> <p class="c7 c5"><img height="26" src="'.
						EMAIL_IMAGE_001.'" width="26"><span class="c4">&nbsp; &nbsp;</span><img height="28" src="'.
						EMAIL_IMAGE_002.'" width="28"><span class="c2">&nbsp; &nbsp;</span><img></p> <p class="c5 c7"><span class="c2">www. MicroOffice.com </span><span class="c1">&bull;</span><span class="c2">&nbsp;sales@microoffice.com </span><span class="c1">&bull; </span><span class="c2">t. 646-201-5508 </span><span class="c1">&bull;</span><span class="c2">&nbsp;f. 866-788-8109</span></p> <p class="c7 c5"><span class="c1">New York | Chicago | Boston</span></p> </div> </body> </html>';
					$text = 'Dear '.$full_name.':'.
					"\n\n"."This communication is to notify you that your account has been suspended and you no longer have access to your workspace. Access cannot be restored until your account is brought current and a $75 reactivation fee has been paid. \n".
					"\n\n"."As you are aware, collection of payments enables us to serve all of the businesses in our communities. If you have already sent payment in full and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward.".
					"\n\n"."As of today, your company has an unpaid balance of  '.$amount_due.'. A non-response to this notice will result in the forwarding of your account to a collections agency, and possible legal action. In the event that legal action is necessary, you will be liable for any attorney’s fees and costs incurred as part of the collection process. ".
					"\n\n"."To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at https://bizcenter.microoffice.com.  You are also welcome to leave a check with your community manager at the front desk. ".
					"\n\n"."Thank you,".
					"\n\n"."Jin Cho".
					"\n"."Department of Finance, MicroOffice".
					"\n"."1375 Broadway, Floor 3".
					"\n"."New York, NY 10018".
					"\n"."jin@microoffice.com".
					"\n"."212-359-4201";
					

					// build email message
					$email = array(
						'to' => array(
							array(
								'email' => $account['CustomerAccount']['PrimaryCustomer']['email'],
								// 'email' => 'jj@pith-studio.com',
								'name' => $account['CustomerAccount']['PrimaryCustomer']['full_name'],
								'type' => 'to'
							)
						),
						'subject' => 'MicroOffice Payment Overdue: '.$company_name,
						'text' => $text,
						'html' => $html
					);
					
					// add to outgoing list
					$emails[] = $email;
				}
				
			} else {
				$return = false;
			}


			// send email
			if (REALLY_SEND_EMAIL === true) {
				foreach ($emails as $email) {

					$sent = $this->BillingAmount->sendEmail($email);

				}
			}
			// if an emails array is not empty
			if (!empty($emails)) {
				$return = true;
			} else {
				$return = false;
			}
		}

		$this->set(compact('return'));
		$this->set('_serialize', array('return'));

	}



/**
 * Cron Email Aging 5 - suspended
 * Method per customer due to large list
 * 
 */
	public function cronEmailAging5($cust_acct_id = null ,$cron_key = false) {
		// if called by a CRON JOB
		if ($this->RequestHandler->accepts('json')) {
			if (!$this->isCronAllowed($cron_key)) {
				$return = false;

			}
		}

		$this->uses[] = 'CustomerAccount';
		// cust account doesn't exists
		if (!$this->CustomerAccount->exists($cust_acct_id)) {

			$return = false;

		} else {
			// email primary customer accounts that are aging
			$accounts = $this->BillingAmount->find('all', array(
				'conditions' => array(
					'stage' => 'aging_5',
					'cust_acct_id' => $cust_acct_id
				),
				'contain' => array(
					'CustomerAccount' => 'PrimaryCustomer'
				)
			));

			if (!empty($accounts[0]['CustomerAccount']['PrimaryCustomer'])) { 

				foreach ($accounts as $account) {

					// debug($account); die;

					// account first name
					@$company_name = $account['CustomerAccount']['name'];
					@$full_name = $account['CustomerAccount']['PrimaryCustomer']['full_name'];
					@$address = $account['CustomerAccount']['PrimaryCustomer']['work_address_1']." ".$account['CustomerAccount']['PrimaryCustomer']['work_address_2']." ".$account['CustomerAccount']['PrimaryCustomer']['work_city'].", ".$account['CustomerAccount']['PrimaryCustomer']['work_state']." ".$account['CustomerAccount']['PrimaryCustomer']['work_zip_code'];
					@$phone = $account['CustomerAccount']['PrimaryCustomer']['work_phone_number'];
					@$amount_due = $account['CustomerAccount']['balance'];
					$title = 'Re: Warning of Suspension of Service';
					$today_nice = date(DATE_FORMAT_USA);
					// Email Accounts that are overdue
					$html = '<html> <style type="text/css">ol{margin:0;padding:0}.c7{line-height:1.0;padding-top:0pt;text-align:center;padding-bottom:0pt}.c0{max-width:468pt;background-color:#ffffff;padding:72pt 72pt 72pt 72pt}.c10{line-height:1.0;padding-top:0pt;padding-bottom:0pt}.c1{color:#ff6600;font-size:9pt;font-family:"Times New Roman"}.c6{color:#0563c1;text-decoration:underline}.c9{color:inherit;text-decoration:inherit}.c2{font-size:9pt;font-family:"Times New Roman"}.c3{height:10pt;direction:ltr}.c4{font-size:10pt;font-family:"Times New Roman"}.c5{direction:ltr}.c8{text-align:center}.c11{vertical-align:super}.c12{font-weight:bold}.title{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:36pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}.subtitle{padding-top:18pt;line-height:1.0;text-align:left;color:#666666;font-style:italic;font-size:24pt;font-family:"Georgia";padding-bottom:4pt;page-break-after:avoid}li{color:#000000;font-size:10pt;font-family:"Times New Roman"}p{color:#000000;font-size:10pt;margin:0;font-family:"Times New Roman"}h1{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:24pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}h2{padding-top:18pt;line-height:1.0;text-align:left;color:#000000;font-size:18pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h3{padding-top:14pt;line-height:1.0;text-align:left;color:#000000;font-size:14pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h4{padding-top:12pt;line-height:1.0;text-align:left;color:#000000;font-size:12pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h5{padding-top:11pt;line-height:1.0;text-align:left;color:#000000;font-size:11pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h6{padding-top:10pt;line-height:1.0;text-align:left;color:#000000;font-size:10pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid} </style><body class="c0"> <div> <p class="c7 c5"><img height="129" src="'.
						EMAIL_IMAGE_000.'" width="244"></p></div> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3 c8"><span></span></p> <p class="c5"><span>'.
						$today_nice.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$full_name.'</span></p> <p class="c5"><span>'.
						$address.'</span></p> <p class="c5"><span>'.
						$phone.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
						$title.'</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span>Dear '.
						$full_name.':</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
							'<p class="c5"><span>'.'After numerous attempts to contact you regarding your company&rsquo;s outstanding account balance, our records indicate that as of the date of this letter, you have failed to bring your account current or contact us to make payment arrangements. Your account is currently </span><span class="c4">70 days</span><span>&nbsp;past due with an outstanding balance in the amount of </span><span class="c4">'.
						$amount_due.'</span><span>. &nbsp;</span></p><br>'.						
							'<p class="c5"><span>'.'This is the final communication you will receive prior to legal action</span><span class="c4 c10">.</span><span>&nbsp;In the event you fail to take action and your account is forwarded to our attorneys for collection, you will be liable for any and all attorneys&rsquo; fees and costs incurred in connection with the collection of the past due balance. To avoid incurring additional charges please take immediate action to bring your account current. In the event you are unable to make payment for the total outstanding amount, please contact me to arrange a payment plan. If you fail to take any action within </span><span class="c4">5 days</span><span>&nbsp;of the date of this letter, your account will be deactivated, your license agreement terminated, and your file will be forwarded to our attorneys to initiate collection action. '.'</span></p><br>'.
							'<p class="c5"><span>'.'If you have already sent payment and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward. '.'</span></p><br>'.
							'<p class="c5"><span></span></p> <p class="c5"><a name="h.gjdgxs"></a><span>To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at </span><span class="c6"><a class="c9" href="https://bizcenter.microoffice.com/">https://bizcenter.microoffice.com</a></span><span>. &nbsp;You are also welcome to leave a check with your community manager at the front desk. </span></p> <p class="c3"><span></span></p> <p class="c5"><span>Thank you,</span></p> <p class="c5"><span>&nbsp;</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span class="c12">Jin Cho</span></p> <p class="c5"><span>Department of Finance, MicroOffice</span></p> <p class="c3"><span></span></p> <p class="c5"><span>1375 Broadway, Floor 3</span></p> <p class="c5"><span>New York, NY 10018</span></p> <p class="c5"><span class="c6"><a class="c9" href="mailto:jin@microoffice.com">jin@microoffice.com</a></span><span>&nbsp;| 212-359-4201</span></p> <p class="c3"><span></span></p> <p class="c5"><span>cc &ndash; Management, Billing</span></p> <p class="c3 c8"><span></span></p> <div> <p class="c3 c10"><span class="c2"></span></p> <p class="c7 c5"><img height="26" src="'.
						EMAIL_IMAGE_001.'" width="26"><span class="c4">&nbsp; &nbsp;</span><img height="28" src="'.
						EMAIL_IMAGE_002.'" width="28"><span class="c2">&nbsp; &nbsp;</span><img></p> <p class="c5 c7"><span class="c2">www. MicroOffice.com </span><span class="c1">&bull;</span><span class="c2">&nbsp;sales@microoffice.com </span><span class="c1">&bull; </span><span class="c2">t. 646-201-5508 </span><span class="c1">&bull;</span><span class="c2">&nbsp;f. 866-788-8109</span></p> <p class="c7 c5"><span class="c1">New York | Chicago | Boston</span></p> </div> </body> </html>';
					$text = 'Dear '.$full_name.':'.
					"\n\n"."After numerous attempts to contact you regarding your company’s outstanding account balance, our records indicate that as of the date of this letter, you have failed to bring your account current or contact us to make payment arrangements. Your account is currently 70 days past due with an outstanding balance in the amount of ".$amount_due." \n".
					"\n\n"."This is the final communication you will receive prior to legal action. In the event you fail to take action and your account is forwarded to our attorneys for collection, you will be liable for any and all attorneys’ fees and costs incurred in connection with the collection of the past due balance. To avoid incurring additional charges please take immediate action to bring your account current. In the event you are unable to make payment for the total outstanding amount, please contact me to arrange a payment plan. If you fail to take any action within 5 days of the date of this letter, your account will be deactivated, your license agreement terminated, and your file will be forwarded to our attorneys to initiate collection action. ".
					"\n\n"."If you have already sent payment and it has not posted to your account, please contact us so that we can investigate the matter and update our records. If you have been withholding payment due to a disputed item on your invoice, please pay the remaining balance, less the disputed item, and bring it to our attention. If you are experiencing financial duress, please contact us immediately so that we can work together to move forward. ".
					"\n\n"."To facilitate an efficient payment process for you, we have developed several ways for you to make payments for your space. You can go online to pay by credit card or electronic ACH transfer by logging into the Biz Center at https://bizcenter.microoffice.com.  You are also welcome to leave a check with your community manager at the front desk. ".
					"\n\n"."Thank you,".
					"\n\n"."Jin Cho".
					"\n"."Department of Finance, MicroOffice".
					"\n"."1375 Broadway, Floor 3".
					"\n"."New York, NY 10018".
					"\n"."jin@microoffice.com".
					"\n"."212-359-4201";
					

					// build email message
					$email = array(
						'to' => array(
							array(
								'email' => $account['CustomerAccount']['PrimaryCustomer']['email'],
								// 'email' => 'jj@pith-studio.com',
								'name' => $account['CustomerAccount']['PrimaryCustomer']['full_name'],
								'type' => 'to'
							)
						),
						'subject' => 'MicroOffice Payment Overdue: '.$company_name,
						'text' => $text,
						'html' => $html
					);
					
					// add to outgoing list
					$emails[] = $email;
				}
				
			} else {
				$return = false;
			}


			// send email
			if (REALLY_SEND_EMAIL === true) {
				foreach ($emails as $email) {

					$sent = $this->BillingAmount->sendEmail($email);

				}
			}
			// if an emails array is not empty
			if (!empty($emails)) {
				$return = true;
			} else {
				$return = false;
			}
		}

		$this->set(compact('return'));
		$this->set('_serialize', array('return'));

	}

	/**
	 * Call via Python to remove holds on aging billing amounts for a given customer account
	 * 
	 * @param int $cust_acct_id customer account id
	 * @return boolean true if the billing amounts were updated
	 */
	public function resumeAging($cust_acct_id) // FIXME add cron key
	{
		$resume_aging = true;
		$this->BillingAmount->updateStatusForCustomerAccount($cust_acct_id, $resume_aging);
	}
}

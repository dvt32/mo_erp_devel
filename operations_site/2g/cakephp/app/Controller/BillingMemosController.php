<?php
App::uses('AppController', 'Controller');
/**
 * BillingMemos Controller
 *
 * @property BillingMemo $BillingMemo
 * @property PaginatorComponent $Paginator
 */
class BillingMemosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * Models used by this controller
 * 
 * @var array list of model names used by this controller
 */
	public $uses = array('BillingMemo', 'BillingMemoTemplate', 'BillingAmount');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->BillingMemo->recursive = 0;
		$this->set('billingMemos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function review($cust_acct_id = null) {

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('put')) {

			// copy data
			$data = $this->request->data;

			// handle different forms on the same page
			switch ($data['section']) {

				// update status of billing memos from "inbox" to "archived"
				case 'archive_billing_memos':

					// handle checkboxes
					$collect = array();
					foreach ($data['BillingMemo'] as &$memo) {
						if (!empty($memo['is_applied'])) {
							$collect[] = $memo['id'];
						}
					}

					// mark checked billing memos as applied (archive)
					$saved = $this->BillingMemo->archive($collect);
					
					// save the customer accounts
					if ($saved) {
						$this->Alert->success('Billing memos have been updated');
						$next = $this->request->query('next');
						$this->request->data = null;
					} else {
						$this->Alert->error('Billing memos could not be updated');
					}
					
				break;


				case 'edit_billing_amounts':

					$this->BillingAmount->create();

					$data['BillingAmount'] = $data['BillingAmount'][0];
					$saved = $this->BillingAmount->save($data);

					if ($saved) {
						$this->Alert->success('Billing amounts have been updated');
						$next = $this->request->query('next');
					}
				break;

				// save a new billing memo
				case 'new_billing_memo':
					$saved = $this->BillingMemo->save($data);
					if ($saved) {
						$this->Alert->success('Billing memo has been saved');
					} else {
						$this->Alert->error('Billing memo could not be saved');
					}
				break;
			}
		}

		// fetch customer account information
		$customerAccount = $this->BillingMemo->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'BillingAmount' => array(
					'order' => 'created DESC',
					'limit' => 1
				),
				'BillingMemo' => array(
					'conditions' => array(
						'is_applied' => false,
						'is_active' => true
					),
					'Modifier' => array('fields' => array('id','full_name')),
				),
				'LicenseAgreement.id',
				'PrimaryCustomer'
			)
		));

		$cycleStart = $this->BillingAmount->getCurrentCycleStartDatetime($cust_acct_id);

		if ( !( $this->request->is('post') && empty($saved) ) ) {
			$this->request->data = $customerAccount;

			$billingAmountId = $this->BillingAmount->idForCurrentBillingCycle($cust_acct_id);
			$this->request->data['BillingAmount'][0]['id'] = $billingAmountId;
		}

		// fetch info from previous billing amount
		$previous = $this->BillingAmount->find('first', array(
			'conditions' => array(
				'cust_acct_id' => $cust_acct_id,
				'due_date <' => date('Y-m-d')
			),
			'order' => 'created DESC',
		));
		$this->request->data['BillingAmount'][9] = @$previous['BillingAmount'];

		// applied memos ("archived")
		$this->Paginator->settings = array('BillingMemo' => array(
			'contain' => array(
				'Modifier' => array('fields' => array('id','full_name'))
			),
			'conditions' => array(
				'BillingMemo.cust_acct_id' => $cust_acct_id,
				'BillingMemo.is_applied' => true,
				'BillingMemo.is_active' => true
			),
			'order' => array(
				'created DESC'
			),
			'limit' => 25
		));
		$archivedMemos = $this->Paginator->paginate();

		// fetch list of billing memo templates
		$templates = $this->BillingMemoTemplate->find('list');

		$this->set(compact('customerAccount','archivedMemos','templates'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($cust_acct_id = null) {

		// process POST request
		if ($this->request->is('post')) {

			$data = $this->request->data;
			$section = @$data['section'];
			unset($data['section']);

			switch ($section) {

				// UPDATE BILLING AMOUNT
				case 'edit_billing_amounts':
					$this->BillingAmount->create();

					$saved = $this->BillingAmount->saveMany($data['BillingAmount']);

					if ($saved) {
						$this->Alert->success(__('Customer billing information has been saved'));
						$next = $this->request->query('next');
					} else {
						$this->Alert->error(__('The billing memo could not be saved.'));
					}
				break;

				// ADD NEW BILLING MEMO
				case 'new_billing_memo':
					$this->BillingMemo->create();
					$saved = $this->BillingMemo->save($data);

					if ($saved) {
						$this->Alert->success(__('The billing memo has been saved.'));
						$next = $this->request->query('next');
					} else {
						$this->Alert->error(__('The billing memo could not be saved.'));
					}
				break;

				default:
					throw new NotFoundException(__('Invalid form section given: "%s"', $section));
				break;
			}
		}

		// fetch customer account information
		$customerAccount = $this->BillingMemo->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'BillingMemo' => array(
					'conditions' => array('is_applied' => false),
					'Modifier' => array('fields' => array('id','full_name'))
				),
				'BillingAmount' => array(
					'order' => 'created DESC',
					'limit' => 1
				),
				'PrimaryCustomer' => array(
					'fields' => array('id','full_name','email','work_phone_number')
				)
			)
		));
		$this->request->data = $customerAccount;

		// applied memos ("archived")
		$this->Paginator->settings = array('BillingMemo' => array(
			'contain' => array(
				'Modifier' => array('fields' => array('id','full_name'))
			),
			'conditions' => array(
				'BillingMemo.cust_acct_id' => $cust_acct_id,
				'BillingMemo.is_applied' => true,
				'BillingMemo.is_active' => true
			),
			'order' => array(
				'created DESC'
			),
			'limit' => 25
		));
		$archivedMemos = $this->Paginator->paginate();

		if ($this->request->is('get')) {
			$billingAmountId = $this->BillingAmount->idForCurrentBillingCycle($cust_acct_id);
			$this->request->data['BillingAmount'][0]['id'] = $billingAmountId;
		}

		// fetch list of billing memo templates
		$templates = $this->BillingMemoTemplate->find('list');

		$this->set(compact('customerAccount','templates','archivedMemos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->BillingMemo->exists($id)) {
			throw new NotFoundException(__('Invalid billing memo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BillingMemo->save($this->request->data)) {
				$this->Session->setFlash(__('The billing memo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing memo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BillingMemo.' . $this->BillingMemo->primaryKey => $id));
			$this->request->data = $this->BillingMemo->find('first', $options);
		}
		$modifiers = $this->BillingMemo->Modifier->find('list');
		$customerAccounts = $this->BillingMemo->CustomerAccount->find('list');
		$this->set(compact('modifiers', 'customerAccounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->BillingMemo->id = $id;
		if (!$this->BillingMemo->exists()) {
			throw new NotFoundException(__('Invalid billing memo'));
		}
		$this->request->onlyAllow('post', 'delete');
		
		if ($this->BillingMemo->delete()) {
			$this->Alert->success(__('The billing memo has been deleted.'));
		} else {
			$this->Alert->error(__('The billing memo could not be deleted. Please, try again.'));
		}
		if (empty($this->request->query['next'])) {
			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function deactivate($id = null) {
		$this->BillingMemo->id = $id;
		if (!$this->BillingMemo->exists()) {
			throw new NotFoundException(__('Invalid billing memo'));
		}
		$this->request->onlyAllow('post', 'delete');
		
		if ($this->BillingMemo->deactivate()) {
			$this->Session->setFlash(__('The billing memo has been deactivated.'));
		} else {
			$this->Session->setFlash(__('The billing memo could not be deactivated. Please, try again.'));
		}
		if (empty($this->request->query['next'])) {
			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * Get the template body for the given billing memo template
 * @param int $template_id id of the billing memo template
 */
	public function getTemplate($template_id = 0) {

		$template = $this->BillingMemoTemplate->field(
			'body',
			array('id' => $template_id)
		);

		$this->set(compact('template'));
		$this->set('_serialize', array('template'));
		$this->response->statusCode(200);
	}

/**
 * Add note to billing amount in aging
 */
	public function aging($billing_amount_id = null) {

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('put')) {

			// copy data
			$data = $this->request->data;

			// handle different forms on the same page
			switch ($data['section']) {

				// update status of billing memos from "inbox" to "archived"
				case 'archive_billing_memos':

					// handle checkboxes
					$collect = array();
					foreach ($data['BillingMemo'] as &$memo) {
						if (!empty($memo['is_applied'])) {
							$collect[] = $memo['id'];
						}
					}

					// mark checked billing memos as applied (archive)
					$saved = $this->BillingMemo->archive($collect);
					
					// save the customer accounts
					if ($saved) {
						$this->Alert->success('Billing memos have been updated');
						$next = $this->request->query('next');
						$this->request->data = null;
					} else {
						$this->Alert->error('Billing memos could not be updated');
					}
					
				break;


				case 'edit_billing_amounts':

					$this->BillingAmount->create();

					// $data['BillingAmount'] = $data['BillingAmount'][0];
					// $saved = $this->BillingAmount->save($data);
					
					$saved = $this->BillingAmount->saveMany($data['BillingAmount']);

					if ($saved) {
						$this->Alert->success('Billing amounts have been updated');
						$next = $this->request->query('next');
					}
				break;

				// save a new billing memo
				case 'new_billing_memo':
					$saved = $this->BillingMemo->save($data);
					if ($saved) {
						$this->Alert->success('Billing memo has been saved');
					} else {
						$this->Alert->error('Billing memo could not be saved');
					}
				break;

				case 'new_promise_note':

					// prepend expiration date to memo
					$data['BillingMemo']['memo'] = sprintf(
						"PROMISE (expires %s): %s",
						$data['BillingMemo']['hold_expires'],
						$data['BillingMemo']['memo']
					);
					$saved = $this->BillingMemo->save($data);
					if ($saved) {
						$this->Alert->success('Promise note has been saved');
						unset($this->request->data['BillingMemo']);
					}
					else {
						$this->Alert->success('Promise note could not be saved');
					}
				break;
			}
		}

		$cust_acct_id = $this->BillingAmount->field('cust_acct_id', array(
			'BillingAmount.id' => $billing_amount_id
		));

		// fetch customer account information
		$customerAccount = $this->BillingMemo->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'BillingAmount' => array(
					'order' => 'created DESC',
					'limit' => 1
				),
				'BillingMemo' => array(
					'conditions' => array(
						'is_applied' => false,
						'is_active' => true
					),
					'Modifier' => array('fields' => array('id','full_name')),
				),
				'LicenseAgreement.id',
				'PrimaryCustomer'
			)
		));

		$cycleStart = $this->BillingAmount->getCurrentCycleStartDatetime($cust_acct_id);


		// fetch info from previous billing amount
		$previousBillingAmount = $this->BillingAmount->find('first', array(
			'conditions' => array(
				'cust_acct_id' => $cust_acct_id,
				'due_date <' => date('Y-m-d')
			),
			'order' => 'created DESC',
		));

		if ( !( $this->request->is('post') && empty($saved) ) ) {
			$this->request->data = $customerAccount;

			$billingAmountId = $this->BillingAmount->idForCurrentBillingCycle($cust_acct_id);
			$this->request->data['BillingAmount'][0]['id'] = $billingAmountId;
		}

		// applied memos ("archived")
		$this->Paginator->settings = array('BillingMemo' => array(
			'contain' => array(
				'Modifier' => array('fields' => array('id','full_name'))
			),
			'conditions' => array(
				'BillingMemo.cust_acct_id' => $cust_acct_id,
				'BillingMemo.is_applied' => true,
				'BillingMemo.is_active' => true
			),
			'order' => array(
				'created DESC'
			),
			'limit' => 25
		));
		$archivedMemos = $this->Paginator->paginate();

		// fetch list of billing memo templates
		$templates = $this->BillingMemoTemplate->find('list');

		$this->set(compact('customerAccount','archivedMemos','templates','previousBillingAmount'));
	}


}

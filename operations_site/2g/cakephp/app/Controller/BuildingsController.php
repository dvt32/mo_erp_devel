<?php
App::uses('AppController', 'Controller');
/**
 * Buildings Controller
 *
 * @property Building $Building
 * @property PaginatorComponent $Paginator
 */
class BuildingsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {

		// if filter params are set...
		if (!empty($this->request->query)) {

			// build array of conditions using the query
			$conditions = $this->filterByQuery($this->request->query, array(
				'fulltext' => array(
					'full_address',
					'note',
					'short_global_label'
				)
			));

			// set form input values to passed params
			$this->request->data['Building'] = $this->request->query;
		}

		// if status is not specified
		if (!isset($conditions['Building.type ILIKE'])) {

			// default paginator settings
			$conditions['Building.type'] = 'officesuite';
			$conditions['Building.is_active'] = 1;

		}

		// Dropdown list
		$types = $this->Building->types;

		$this->Paginator->settings = array('Building' => compact('conditions','contain','order'));

		$buildings = $this->Paginator->paginate();
		$this->set(compact('buildings', 'types', 'isActive'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Building->exists($id)) {
			throw new NotFoundException(__('Invalid building'));
		}
		$options = array('conditions' => array(
			'Building.' . $this->Building->primaryKey => $id),
			'recursive' => '1',
			'contain' => array(
				'Floor' => array(
					'OfficeManager' => 'full_name'
				)
			)
		);
		$this->set('building', $this->Building->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Building->create();
			if ($this->Building->save($this->request->data)) {
				$this->Session->setFlash(__('The building has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The building could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Building->exists($id)) {
			throw new NotFoundException(__('Invalid building'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Building->save($this->request->data)) {
				$this->Session->setFlash(__('The building has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The building could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Building.' . $this->Building->primaryKey => $id));
			$this->request->data = $this->Building->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Building->id = $id;
		if (!$this->Building->exists()) {
			throw new NotFoundException(__('Invalid building'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Building->delete()) {
			$this->Session->setFlash(__('The building has been deleted.'));
		} else {
			$this->Session->setFlash(__('The building could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
/**
 * operations_index method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function networkMonitor() {

	    $servers = $this->Building->find('all', array(
	    	'conditions' => array(
	    		'Building.is_active' => 1,
	    		'NOT' => array('Building.server1' => null)
	    	)
	    ));

	    $this->set(compact('servers'));
	    $this->set('_serialize', array('servers'));

	}

/**
 * Executes ping to $host
 *
 * Ping return code, 0 if OK, 2 if not found
 *
 * @param string $host
 */
	public function ping($host = null) {

		$message = '0';
		if (!empty($host)) {
			exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
			if (!$rval) {
			    $message = '1';
			}
		}
	    
	    die("$message");
	}
}

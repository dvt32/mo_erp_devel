<?php
App::uses('AppController', 'Controller');
/**
 * Cameras Controller
 *
 * @property Camera $Camera
 * @property PaginatorComponent $Paginator
 */
class CamerasController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Camera->recursive = 0;
		$this->set('cameras', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Camera->exists($id)) {
			throw new NotFoundException(__('Invalid camera'));
		}
		$options = array('conditions' => array('Camera.' . $this->Camera->primaryKey => $id));
		$this->set('camera', $this->Camera->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Camera->create();
			if ($this->Camera->save($this->request->data)) {
				$this->Session->setFlash(__('The camera has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The camera could not be saved. Please, try again.'));
			}
		}
		$floors = $this->Camera->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Camera->exists($id)) {
			throw new NotFoundException(__('Invalid camera'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Camera->save($this->request->data)) {
				$this->Session->setFlash(__('The camera has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The camera could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Camera.' . $this->Camera->primaryKey => $id));
			$this->request->data = $this->Camera->find('first', $options);
		}
		$floors = $this->Camera->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Camera->id = $id;
		if (!$this->Camera->exists()) {
			throw new NotFoundException(__('Invalid camera'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Camera->delete()) {
			$this->Session->setFlash(__('The camera has been deleted.'));
		} else {
			$this->Session->setFlash(__('The camera could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

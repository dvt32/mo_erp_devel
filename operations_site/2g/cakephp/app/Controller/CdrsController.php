<?php
App::uses('AppController', 'Controller');
/**
 * Cdrs Controller
 *
 * @property Cdr $Cdr
 */
class CdrsController extends AppController {

/**
 * Pagination rules
 *
 * @var array
 */
	public $paginate = array(
		'limit' => 20
	);

/**
 * recording method
 *
 * @return void
 */
	// public function recording() {

	// 	// TO DOWNLOAD A RECORDING
	// 	// http://puresource.microoffice.com/dav/recordings/6466528418/20130809155914.6466528418.6466605046.wav
	// 	// /dav/recordings/phonenumberto/yyyymmdddhhmmss.phonenumberfrom.phonenumberto.wav



	// 	$cpid = AuthComponent::user('cust_pers_id');
	// 	$caid = AuthComponent::user('cust_acct_id');

	// 	$this->paginate['conditions'] = array('Cdr.account_id' => 1538);

	// 	$this->Cdr->recursive = 0;
 //                $this->set('cdrs', $this->paginate());
	// }

/**
 * search for a call log record
 * @return void
 */
	public function recording() {

		$caid = AuthComponent::user('cust_acct_id');

		$query = @$this->request->query['q'] ?: '';
		if (strlen($query) >= 5) {
			$query = "%{$query}%";
			$this->paginate = array(
				'conditions' => array(
					array('OR' => array(
						'Cdr.dest_num ILIKE' => $query,
						'Cdr.src_num ILIKE' => $query,
					)),
					'Cdr.account_id' => $caid,
					'Cdr.start_stamp ILIKE' => $query,
					'Cdr.end_stamp ILIKE' => $query,
				),
				'orderBy' => array('Cdr.start_stamp DESC'),
			);
			// this includes callLogs from inactive customerAccounts
		} else {

			// Uncomment to hide by default
			$this->paginate = array(
				'conditions' => array(
					'Cdr.account_id' => $caid,
				),
				'orderBy' => array('Cdr.start_stamp DESC'),
			);
		}
		$cdrs = $this->paginate();
		$this->set(compact('cdrs'));
	}

/**
 * method to download a voice recording directly.
 *
 * @param integer $id
 * @return void
 */
	public function download($id=null) {
		if (!$this->Cdr->exists($id)) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		$recording = $this->Cdr->findById($id);
		$originalTimeStamp = $recording['Cdr']['start_stamp'];

		// generate the path of the recording then pass it to the download_file function in order to retrieve it
		$timeDate = str_replace('-','',substr($originalTimeStamp, 0, strpos($recording['Cdr']['start_stamp'], ' ')));
		$timeHours = substr(str_replace(':','',substr($originalTimeStamp, strpos($originalTimeStamp, ' '))), 1, 6);
		$timeStamp = $timeDate.$timeHours;
		$recordingUrl = DAV_SERVER_HOST.'recordings/'.$recording['Cdr']['dest_num'].'/'.$timeStamp.'.'.$recording['Cdr']['dest_num'].'.'.$recording['Cdr']['src_num'].'.wav';
		// The function will return the filename after either downloading, or finding it on disk
		$fileUrl = download_file($recordingUrl, './files/');
		$fileName = substr(strrchr($fileUrl, '/'), 1);
		// Afterwards, send it to the user for download
		$this->response->file($fileUrl, array('name' => $fileName, 'download' => true));
		return $this->response;
	}

/**
 * account method
 *
 * @return void
 */
	public function account() {

		$this->Paginator->settings = array(
			'Cdr' => array(
				'recursive' => 0,
				'limit' => 20,
				'order' => 'Cdr.start_stamp DESC'
			)
		);
		$this->set('cdrs', $this->Paginator->paginate());

	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Cdr->recursive = 0;
		$this->set('cdrs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cdr->exists($id)) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		$options = array(
			'conditions' => array('Cdr.' . $this->Cdr->primaryKey => $id),
			'recursive' => 1
		);
		$cdr = $this->Cdr->find('first', $options);

		$this->set(compact('cdr'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cdr->create();
			if ($this->Cdr->save($this->request->data)) {
				$this->Session->setFlash(__('The cdr has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cdr could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Cdr->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cdr->exists($id)) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Cdr->save($this->request->data)) {
				$this->Session->setFlash(__('The cdr has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cdr could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cdr.' . $this->Cdr->primaryKey => $id));
			$this->request->data = $this->Cdr->find('first', $options);
		}
		$customerAccounts = $this->Cdr->CustomerAccount->find('list');
		$this->set(compact('customerAccounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cdr->id = $id;
		if (!$this->Cdr->exists()) {
			throw new NotFoundException(__('Invalid cdr'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Cdr->delete()) {
			$this->Session->setFlash(__('Cdr deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Cdr was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

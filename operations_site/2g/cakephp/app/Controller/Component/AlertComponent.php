<?php

App::uses('Component', 'Controller');
App::uses('CakeSession', 'Model/Datasource');

class AlertComponent extends Component {

	public function flash($message, $heading = null, $type = null) {
		$element = 'alert';
		$params = compact('heading', 'type');
		CakeSession::write('Message.flash', compact('message', 'element', 'params'));
	}

	public function error($message, $heading = null) {
		$this->flash($message, $heading, 'error');
	}

	public function success($message, $heading = null) {
		$this->flash($message, $heading, 'success');
	}

	public function info($message, $heading = null) {
		$this->flash($message, $heading, 'info');
	}
}

<?php

App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class MicroOfficeAuthenticate extends BasicAuthenticate {

	public function getUser(CakeRequest $request) {
		$username = strtolower(preg_replace('/@.*$/', '', env('REMOTE_USER')));
		return $this->_findUser($username);
	}

	protected function _findUser($username, $password = null) {
		$userModel = $this->settings['userModel'];
		list(, $model) = pluginSplit($userModel);
		$fields = $this->settings['fields'];

		if (is_array($username)) {
			$conditions = $username;
		} else {
			$conditions = array(
				$model . '.' . $fields['username'] => $username
			);
		}

		if (!empty($this->settings['scope'])) {
			$conditions = array_merge($conditions, $this->settings['scope']);
		}

		$userFields = $this->settings['userFields'];
		if ($password !== null && $userFields !== null) {
			$userFields[] = $model . '.' . $fields['password'];
		}

		$result = ClassRegistry::init($userModel)->find('first', array(
			'conditions' => $conditions,
			'recursive' => $this->settings['recursive'],
			'fields' => $userFields,
			'contain' => $this->settings['contain'],
		));

		if (empty($result[$model])) {
			// $this->passwordHasher()->hash($password);
			return false;
		}

		$user = $result[$model];

		// is_active check, copied from 1g.
		if (!$user['is_active']) {
			return false;
		}

		// we need a roles field
		$user["roles"] = ClassRegistry::init($userModel)->roles($user['id']);

		unset($user[$fields['password']]);
		unset($result[$model]);

		return array_merge($user, $result);
	}
}

?>

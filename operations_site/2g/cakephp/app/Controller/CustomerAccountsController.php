<?php
App::uses('AppController', 'Controller');
/**
 * CustomerAccounts Controller
 *
 * @property CustomerAccount $CustomerAccount
 * @property PaginatorComponent $Paginator
 */
class CustomerAccountsController extends AppController {

	public $uses = array('CustomerAccount','BillingAmount');

/**
 * Private utility function for filter/search/sort
 * 
 * @param array $query the request query parameters
 */
	public function fss($query) {

		// no conditions
		$conditions = array();

		$query = $this->request->query;

		// filters set
		if (!empty($query)) {

			// filter by query
			$conditions = $this->filterByQuery($query, array(
				'fulltext' => array('name'),
				'ignore' => array(
					'show_bill_as_is',
					'show_paid'
				)
			));
			$this->request->data['CustomerAccount'] = $query;

			// show customer accounts marked "bill as is"
			if (empty($query['show_bill_as_is'])) {
				$monthago = date(DATETIME_FORMAT, strtotime('-1 month'));
				$conditions[] = array(
					'OR' => array(
						'CustomerAccount.billing_last_reviewed_dt <' => $monthago,
						'CustomerAccount.billing_last_reviewed_dt' => NULL,
						'CustomerAccount.billing_memos_inbox_count >' => 0
					)
				);
			}
		}

		// hide customer accounts that have been reviewed in the past 30 days
		if (empty($query['show_bill_as_is'])) {
			$monthago = date(DATETIME_FORMAT, strtotime('-30 days'));
			$conditions[] = array(
				'OR' => array(
					'CustomerAccount.billing_last_reviewed_dt <' => $monthago,
					'CustomerAccount.billing_last_reviewed_dt' => NULL,
					'CustomerAccount.billing_memos_inbox_count >' => 0
				)
			);
		}

		// default filters
		$conditions['CustomerAccount.type'] = 'customer'; // always exclude vendors
		$conditions['CustomerAccount.is_active'] = true; // always exclude inactive accounts

		// set default limit to 50 results per page
		$limit = @$query['limit'] ?: 50;

		$order = @$query['order'] ?: 'billing_memos_inbox_count DESC';

		$contain = array(
			'Service',
			'Floor.short_global_label',
			'BillingReviewer.full_name',
			'PrimaryCustomer.id',
			'PrimaryCustomer.full_name',
			'BillingAmount' => array(
				'order' => 'created DESC',
				'limit' => 1
			),
		);

		// fetch rows from datasource
		$this->Paginator->settings = array('CustomerAccount' => compact('conditions','contain','fields','order','limit','cust_accts'));
		$customerAccounts = $this->Paginator->paginate();

		return $customerAccounts;
	}

/**
 * Utility function shared by "billing" and "invoicing" to handle checkboxes
 * 
 * @param array $data $this->request->data sent as-is from post request
 * @return true if updated successfully, false otherwise
 */
	private function updateReviewed($data) {

		// collector array
		$collect = array();

		if (!empty($data['CustomerAccount'])) {

			// collect data to save
			foreach ($data['CustomerAccount'] as &$acct) {
				if (!empty($acct['reviewed'])) {
					$cust_acct_ids[] = $acct['id'];
				}
			}

			// save the customer accounts
			$saved = $this->CustomerAccount->updateReviewed($cust_acct_ids);

			// collect customer account ids to create billing amounts
			$billingAmounts = array();
			foreach ($cust_acct_ids as $cust_acct_id) {
				$billingAmounts[] = compact('cust_acct_id');
			}
			$this->BillingAmount->create();
			$this->BillingAmount->saveMany($billingAmounts);

			return $saved;
		}

		return false;
	}

	public function billing() {
		$this->set('title_for_layout','Customer Accounts (Billing)');

		// handle checkboxes
		if ($this->request->is('post') || $this->request->is('put')) {
			
			// prep data to save
			$data = $this->request->data;

			// send this request data to the shared utility function updateReviewed
			$saved = $this->updateReviewed($data);

			// report success/failure
			if ($saved) {
				$this->Alert->success('Customer accounts have been updated');
				$this->request->data = null;
			} else {
				$this->Alert->error('Customer accounts could not be updated');
			}
		}

		// no conditions
		$conditions = array();

		$query = $this->request->query;

		// filters set
		if (!empty($query)) {

			// filter by query
			$conditions = $this->filterByQuery($query, array(
				'fulltext' => array('name'),
				'ignore' => array(
					'show_bill_as_is',
					'show_recently_reviewed'
				)
			));
			$this->request->data['CustomerAccount'] = $query;

			// show customer accounts marked "bill as is"
			$time_ago = date(DATETIME_FORMAT, strtotime('-20 days'));

			if (empty($query['show_recently_reviewed']))
			{
				$conditions[] = array(
					'OR' => array(
						'CustomerAccount.billing_last_reviewed_dt <' => $time_ago,
						'CustomerAccount.billing_last_reviewed_dt' => NULL,
						// 'CustomerAccount.billing_memos_inbox_count >' => 0
					)
				);
			}
		}

		// default conditions
		$conditions['CustomerAccount.is_active'] = true; // exclude inactive customer accounts

		$contain = array(
			'Service',
			'Floor.short_global_label',
			'BillingReviewer.full_name',
			'PrimaryCustomer.id',
			'PrimaryCustomer.full_name',
			'BillingAmount' => array(
				'order' => 'created ASC',
				'limit' => 1
			),
		);

		$order = 'billing_last_reviewed_dt ASC';

		$limit = '40';

		// use available virtual field "days_since_last_review"
		$this->CustomerAccount->virtualFields['days_since_last_review'] =
			$this->CustomerAccount->virtualFields_available['days_since_last_review'];

		// fetch rows from datasource
		$this->Paginator->settings = array('CustomerAccount' => compact(
			'conditions',
			'contain',
			'fields',
			'order',
			'limit'
		));
		$customerAccounts = $this->Paginator->paginate();

		// dropdown lists
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'Service.is_active'));
		$types = $this->CustomerAccount->types;
		$floors = $this->CustomerAccount->Floor->find('list', array(
			'conditions' => array(
				'Floor.is_active' => true,
				'Building.is_active' => true
			),
			'order' => 'Floor.short_global_label ASC',
			'contain' => array('Building.is_active')
		));
		// progress bar counts
		if (!empty($this->request->data['CustomerAccount']['primary_floor_id'])) {
			$progress = $this->CustomerAccount->billingCount($this->request->data['CustomerAccount']['primary_floor_id']);
		} else {
			$progress = $this->CustomerAccount->billingCount();
		}

		$this->set(compact('services','types','floors','customerAccounts', 'progress'));
	}

	public function invoicing() {
		$this->set('title_for_layout','Customer Accounts (Invoicing)');

		// handle checkboxes
		if ($this->request->is('post') || $this->request->is('put')) {
			
			// prep data to save
			$data = $this->request->data;

			// send this request data to the shared utility function updateReviewed
			$saved = $this->updateReviewed($data);

			// report success/failure
			if ($saved) {
				$this->Alert->success('Customer accounts have been updated');
				$this->request->data = null;
			} else {
				$this->Alert->error('Customer accounts could not be updated');
			}
		}

		// no conditions
		$conditions = array();

		// use invoicing_count virtual field
		$this->CustomerAccount->virtualFields['invoicing_count'] = 
			$this->CustomerAccount->virtualFields_available['invoicing_count'];
		// include customer accounts with one or more inbox billing memos
		$this->CustomerAccount->virtualFields['billing_memos_count'] = 
			$this->CustomerAccount->virtualFields_available['billing_memos_count'];

		$query = $this->request->query;

		// filters set
		if (!empty($query)) {

			// filter by query
			$conditions = $this->filterByQuery($query, array(
				'fulltext' => array('name'),
				'ignore' => array(
					'show_bill_as_is',
					'show_paid'
				)
			));
			$this->request->data['CustomerAccount'] = $query;
		}

		// default filters
		$conditions['or'] = array(
			'CustomerAccount.invoicing_count >' => 0,
			'CustomerAccount.billing_memos_count >' => 0
		);
		$conditions['CustomerAccount.type'] = 'customer'; // always exclude vendors
		$conditions['CustomerAccount.is_active'] = true; // always exclude inactive accounts

		// set default limit to 50 results per page
		$limit = @$query['limit'] ?: 50;

		$contain = array(
			'BillingReviewer' => array('id','full_name'),
			'PrimaryCustomer' => array('id','full_name')
		);

		// fetch rows from datasource
		$this->Paginator->settings = array('CustomerAccount' => compact('conditions','contain','fields','order','limit'));
		$customerAccounts = $this->Paginator->paginate();

		// dropdown lists
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'Service.is_active'));
		$types = $this->CustomerAccount->types;
		$floors = $this->CustomerAccount->Floor->find('list', array(
			'conditions' => array(
				'Floor.is_active' => true,
				'Building.is_active' => true
			),
			'order' => 'Floor.short_global_label ASC',
			'contain' => array('Building.is_active')
		));
		// progress bar counts
		$progress = $this->CustomerAccount->invoicingCount(@$conditions, @$limit);

		$this->set(compact('services','types','floors','customerAccounts','progress','cust_accts'));
	}


	public function receiving() {
		$this->set('title_for_layout','Accounts Receivable (Customer Accounts)');

		// no conditions
		$conditions = array();

		$query = $this->request->query;

		if (!empty($query)) {
			$conditions = $this->filterByQuery($query, array(
				'fulltext' => array(
					'CustomerAccount.name'
				),
				'ignore' => array(
					'show_paid',
					'show_unpaid'
				)
			));

			$this->request->data['CustomerAccount'] = $this->request->query;
		}

		// default conditions
		if (!isset($this->request->query['show_unpaid'])) {
			$this->request->query['show_unpaid'] = 1;
		}
		if (!isset($this->request->query['show_paid'])) {
			$this->request->query['show_paid'] = 0;
		}

		// filter by paid/unpaid
		$show_paid = $this->request->query('show_paid');
		$show_unpaid = $this->request->query('show_unpaid');
		if ($show_paid xor $show_unpaid) {
			if ($show_paid) {
				$conditions['CustomerAccount.balance'] = 0;
				$this->request->data['CustomerAccount']['show_paid'] = 1;
			} else {
				$conditions['CustomerAccount.balance >'] = 0;
				$this->request->data['CustomerAccount']['show_unpaid'] = 1;
			}
		}

		// fetch related models
		$contain = array(
			'BillingAmount' => array(
				'order' => 'due_date DESC',
				'limit' => 1
			)
		);

		// fetch customer accounts
		$this->Paginator->settings = array('CustomerAccount' => compact('conditions','contain','fields','limit','order'));
		$customerAccounts = $this->Paginator->paginate();

		// dropdown lists
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'Service.is_active'));
		$types = $this->CustomerAccount->types;
		$floors = $this->CustomerAccount->Floor->find('list', array(
			'conditions' => array(
				'Floor.is_active' => true,
				'Building.is_active' => true
			),
			'order' => 'Floor.short_global_label ASC',
			'contain' => array('Building.is_active')
		));
		$this->CustomerAccount->virtualFields['receiving_count'] =
			$this->CustomerAccount->virtualFields_available['receiving_count'];
		$cust_accts = $this->CustomerAccount->find('list', array(
			'conditions' => array('CustomerAccount.receiving_count >' => 0),
			'fields' => array('id','name')
		));

		// progress bar counts
		if (!empty($this->request->data['CustomerAccount']['primary_floor_id'])) {
			$progress = $this->CustomerAccount->receivingCount($this->request->data['CustomerAccount']['primary_floor_id']);
		} else {
			$progress = $this->CustomerAccount->receivingCount();
		}

		$this->set(compact('services','types','floors','customerAccounts', 'progress'));
	}

	public function aging() {

		// set page title
		$this->set('title_for_layout','Customer Accounts (Aging)');

		// if filter params are set...
		if (!empty($this->request->query))
		{

			// filter results by query
			$conditions = $this->filterByQuery($this->request->query, array(
				'fulltext' => array('name'),
				'ignore' => array('show_paid')
			));
			$this->request->data['CustomerAccount'] = $this->request->query;
		}

		// exclude inactive customer accounts
		$conditions['CustomerAccount.is_active'] = true;

		// exclude customer accounts with balance <= 0
		$conditions['CustomerAccount.balance >'] = 0;

		// fields to fetch
		$fields = array(
			'id',
			'name',
			'primary_contact_cust_pers_id',
			'preferred_payment_method',
			'primary_floor_id',
			'balance',
			'pay_terms',
			'aging_count',
			'days_overdue',
			'aging_followup_date'
		);

		// related models to fetch
		$contain = array(
			'Floor' => array('fields' => array(
				'id',
				'short_global_label',
				'dom_payment_due',
				'dom_billing_cycle_starts'
			)),
			'PrimaryCustomer',
			'AgingInteraction' => array(
				'limit' => 1,
				'order' => 'creation_dt DESC'
			)
		);

		// only find customer accounts with an aging billing amount
		$conditions['CustomerAccount.aging_count >'] = 0;

		// set default limit to 50 results per page
		$limit = @$query['limit'] ?: 50;

		// use 'days_overdue' virtual field
		$this->CustomerAccount->virtualFields['days_overdue'] = $this->CustomerAccount->virtualFields_available['days_overdue'];
		$this->CustomerAccount->virtualFields['aging_count'] = $this->CustomerAccount->virtualFields_available['aging_count'];

		// default sort order
		if (empty($this->request->named['sort'])) {
			$order = 'days_overdue DESC';
		}

		// set associations
		$this->uses[] = 'Floor';
		
		// fetch rows from datasource
		$this->Paginator->settings = array('CustomerAccount' => compact('conditions','contain','fields','limit','order'));
		$customerAccounts = $this->Paginator->paginate();

		// list of CustomerAccounts with aging billing amounts
		$cust_accts = $this->CustomerAccount->getAgingList();

		// dropdown lists
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'Service.is_active'));
		$types = $this->CustomerAccount->types;
		$floors = $this->CustomerAccount->Floor->find('list', array(
			'conditions' => array(
				'Floor.is_active' => true,
				'Building.is_active' => true
			),
			'order' => 'Floor.short_global_label ASC',
			'contain' => array('Building.is_active')
		));
		
		$this->set(compact('services','types','floors','customerAccounts','cust_accts'));
	}

	public function admin() {

		$this->set('title_for_layout','Customer Accounts (Admin)');

		// handle checkboxes
		if ($this->request->is('post') || $this->request->is('put')) {
			
			// prep data to save
			$data = $this->request->data;
			// $datetime = date(DATETIME_FORMAT);
			$employee_id = $this->Auth->user('id');

			// collect data to save
			foreach ($data['CustomerAccount'] as &$acct) {
				if (!empty($acct['reviewed'])) {
					$cust_acct_ids[] = $acct['id'];
				}
			}

			$saved = $this->CustomerAccount->updateReviewed($cust_acct_ids);

			// save the customer accounts
			if ($saved) {
				$this->Alert->success('Customer accounts have been updated');
				$this->request->data = null;
			} else {
				$this->Alert->error('Customer accounts could not be updated');
			}
		}

		$customerAccounts = $this->fss($this->request->query);
		// debug($customerAccounts);
		$counts = $this->CustomerAccount->billCounts();

		$graph = $this->CustomerAccount->graph();
		

		$this->set(compact('customerAccounts', 'counts', 'graph'));
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		// jump to customerAccount with matching id
		if (!empty($this->request->query['id'])) {
			$this->CustomerAccount->id = $this->request->query['id'];
			if ($this->CustomerAccount->exists()) {
				return $this->redirect(array('action' => 'view', $this->request->query['id']));
			} else {
				$this->Alert->error('Invalid company id');
				$this->request->query['id'] = null;
			}
		}

		// no conditions
		$conditions = array();

		// if filter params are set...
		if (!empty($this->request->query)) {
			$conditions = $this->filterByQuery($this->request->query, array('fulltext' => 'name'));
			$this->request->data['CustomerAccount'] = $this->request->query;
		}

		// default paginator settings
		if (!isset($this->request->query['is_active'])) {
			$conditions['CustomerAccount.is_active'] = true;
		}

		// set associations
		$this->uses[] = 'Floor';
		
		// fetch rows from datasource
		$this->CustomerAccount->recursive = 0;
		$this->Paginator->settings = array('CustomerAccount' => compact('conditions','contain','fields','order'));
		$customerAccounts = $this->Paginator->paginate();
		$this->set(compact('customerAccounts'));

		// dropdown lists
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'Service.is_active'));
		$types = $this->CustomerAccount->types;
		
		$floors = $this->Floor->find('list', array(
			'conditions' => array('Floor.is_active' => true),
			'order' => 'Floor.short_global_label ASC'
		));
		$this->set(compact('services','types','floors'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		$options = array(
			'conditions' => array('CustomerAccount.' . $this->CustomerAccount->primaryKey => $id),
			'contain' => array(
				'Customer',
				'Mailbox',
				'LicenseAgreement',
				'PhoneNumber',
				'ProximityCard',
				'Task',
				'PrimaryCustomer',
				'Floor',
				'Service',
				'Desk'
			),
			'recursive' => 0
		);

		$customerAccount = $this->CustomerAccount->find('first', $options);
		// debug($customerAccount); die;
		$this->set(compact('customerAccount'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		// POST request
		if ($this->request->is('post')) {
			$this->CustomerAccount->create();
			if ($this->CustomerAccount->save($this->request->data)) {
				$this->Session->setFlash(__('The customer account has been saved.'));
				return $this->redirect(array(
					'controller' => 'customers',
					'action' => 'add'));
			} else {
				$this->Session->setFlash(__('The customer account could not be saved. Please, try again.'));
			}
		}

		// fetch dropdown lists from model
		$types = $this->CustomerAccount->types;
		$room_minutes = $this->CustomerAccount->room_minutes;
		$this->set(compact('types','room_minutes'));

		// fetch primary floors from model
		$this->uses[] = 'Floor';
		$floors = $this->Floor->listActiveOfficeFloors();
		$this->set(compact('floors'));

		// fetch rows from datasource
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'is_active'));
		$this->set(compact('services', 'customer'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}

		// POST|PUT request
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomerAccount->save($this->request->data)) {
				$this->Session->setFlash(__('The customer account has been saved.'));
				$this->redirect(['action' => $this->action, $id]);
			} else {
				$this->Session->setFlash(__('The customer account could not be saved. Please, try again.'));
			}
		}
		// GET request
		else {
			$options = array('conditions' => array('CustomerAccount.' . $this->CustomerAccount->primaryKey => $id));
			$this->request->data = $this->CustomerAccount->find('first', $options);
		}

		$customers = $this->CustomerAccount->PrimaryCustomer->find('list', [
			'conditions' => ['cust_acct_id' => $id],
			'order' => 'full_name ASC'
		]);
		$preferred_payment_methods = $this->CustomerAccount->preferred_payment_methods;
		$types = $this->CustomerAccount->types;
		// $taxes = $this->CustomerAccount->Tax->find('list');
		$services = $this->CustomerAccount->Service->find('list');
		$primaryFloors = $this->CustomerAccount->Floor->find('list');
		$this->set(compact('customers', 'taxes', 'services', 'primaryFloors','types','preferred_payment_methods'));
	}

/**
 * deactivate method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function deactivate($id = null) {

		$this->CustomerAccount->id = $id;
		
		if (!$this->CustomerAccount->exists()) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		
		$this->request->onlyAllow('post', 'delete');

		if ($this->CustomerAccount->deactivate()) {
			$this->Alert->success(__('The customer account has been deactivated.'));
		} else {
			$this->Alert->error(__('The customer account could not be deactivated. Please, try again.'));
		}
		
		return $this->redirect(array('action' => 'index'));
	}

	public function search() {

		$conditions = array('CustomerAccount.is_active' => true);
		$fields = null;
		$contain = null;
		$order = 'CustomerAccount.name ASC';

		// parse the query
		if (!empty($this->request->query)) {

			$query = @$this->request->query['query'];
			if (!empty($query)) {
				$conditions['CustomerAccount.name ILIKE'] = "%{$query}%";
			}

			$fields =       @$this->request->query['fields'];
			$contain =      @$this->request->query['contain'];
			$order =        @$this->request->query['order'];
			$limit =        @$this->request->query['limit'];
		}

		// set conditions

		// fetch matching companies
		$companies = $this->CustomerAccount->find('all', compact('conditions','contain','order','fields','limit'));

		$suggestions = array();
		foreach($companies as $company) {
			$suggestion = new stdClass();
			$suggestion->data = arrayToObject($company);
			$suggestion->value = $company['CustomerAccount']['name'];
			$suggestions[] = $suggestion;
		}

		$this->set('suggestions', $suggestions);
		$this->set('_serialize', array('suggestions'));
	}

/**
 * Updates all billing information
 */
	public function updateAllBillingInformation($cron_key = false) {

		// if called by a CRON JOB
		if ($this->RequestHandler->accepts('json')) {
			if (!$this->isCronAllowed($cron_key)) {
				throw new UnauthorizedException('Invalid CRON key');
			}
		}

		$this->CustomerAccount->updateAllBillingInformation();
		$this->Alert->success('All billing-related information has been updated');

		$return = true;
		$this->set(compact('return'));
		$this->set('_serialize', array('return'));
	}

	/**
	 * List of customer accounts to contact about aging billing amounts
	 */
	public function agingAccountsToContact()
	{
		$this->redirect(['action' => 'aging']);
	}

	/**
	 * Checks whether the dynamically calculated balance and the cached match or not
	 * 
	 * @param int $cust_acct_id customer account id
	 * @return boolean true if the values match, false otherwise
	 */
	public function getBalance($cust_acct_id)
	{
		if (!$this->CustomerAccount->exists($cust_acct_id))
		{
			throw new NotFoundException('Invalid customer account id');
		}

		$calcBalance = $this->CustomerAccount->getCalcBalance($cust_acct_id);
		$cacheBalance = $this->CustomerAccount->getCachedBalance($cust_acct_id);
		debug(compact('calcBalance','cacheBalance'));
		return $calcBalance == $cacheBalance;
	}


/**
 * statement method
 *
 * @author John Jung
 * @throws NotFoundException
 * @param string cust+acct_id $id
 * @return void
 */
	public function statement($id = null) {

		if (!$this->CustomerAccount->exists($id)) {
			throw new NotFoundException(__('Invalid customer account'));
		}
		$options = array(
			'conditions' => array('CustomerAccount.' . $this->CustomerAccount->primaryKey => $id),
			'contain' => array(
				'Customer',
				'ProximityCard',
				'PrimaryCustomer',
				'Service',
				'Floor',
				'BillingAmount' => array(
					'conditions' => array(
						'BillingAmount.created >' => BILLING_EPOCH 
					)
				),
				'ReceivedPayment' => array(
					'conditions' => array(
						'ReceivedPayment.creation_dt >' => BILLING_EPOCH,
						'ReceivedPayment.process_status' => 'complete'
					)
				),
				'CreditCardTxn' => array(
					'conditions' => array(
						'CreditCardTxn.processed_dt >' => BILLING_EPOCH,
						'CreditCardTxn.response_message' => 'APPROVED'
					)
				),
				'AchDebitTransaction' => array(
					'conditions' => array(
						'AchDebitTransaction.creation_dt >' => BILLING_EPOCH,
						'AchDebitTransaction.cancelled' => false
					)
				)
			),
			'recursive' => 0
		);

		$customerAccount = $this->CustomerAccount->find('first', $options);

		$payments = [];

		// aggregate the payments of different types into one master-list
		// NOTE: the below foreach loops assume that there are no datetime collisions
		foreach ($customerAccount['ReceivedPayment'] as $recv_payment)
		{
			$dt = $recv_payment['process_txn_after_dt']; // FIXME ?
			$payments[$dt] = array(
				'method' => Inflector::humanize($recv_payment['payment_method']),
				'amount' => $recv_payment['amount'],
				'date' => strtodatestring($dt,DATE_FORMAT_USA)
			);
		}
		foreach ($customerAccount['CreditCardTxn'] as $cc_txn)
		{
			$dt = $cc_txn['processed_dt'];

			// prevent duplicates payments from appearing
			$needle = array(
				'method' => 'Card',
				'amount' => $cc_txn['total_charge'],
				'date' => strtodatestring($dt,DATE_FORMAT_USA)
			);
			$exists_in_recv_payment = in_array($needle, $payments);

			if (!$exists_in_recv_payment) {
				$payments[$dt] = array(
					'method' => 'Credit Card',
					'amount' => $cc_txn['total_charge'],
					'date' => strtodatestring($dt,DATE_FORMAT_USA)
				);
			}
		}

		foreach ($customerAccount['AchDebitTransaction'] as $ach_txn)
		{
			$dt = $ach_txn['processed_dt'];
			$payments[$dt] = array(
				'method' => 'ACH Debit',
				'amount' => $ach_txn['amount'],
				'date' => strtodatestring($dt,DATE_FORMAT_USA)
			);
		}

		ksort($payments);

		if ($this->request->query('print'))
		{
			// use the 'print' layout
			$this->layout = 'print';
		}

		// send variables to view
		$this->set(compact('customerAccount','payments'));
	}

}

<?php
App::uses('AppController', 'Controller');
/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class CustomersController extends AppController {



/**
 * Change a customer's password
 * 
 * @param int $id customer id
 * @return void
 */
	public function changePassword($id = null) {

		$this->Customer->id = $id;

		// check customer id
		if (!$this->Customer->exists()) {
			$this->Alert->error('Choose a valid customer to change their password');
			return $this->redirect(array('action' => 'index'));
			// throw new NotFoundException("Invalid customer id");
		}

		$this->uses[] = 'User';

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('put')) {

			// copy data
			$data = $this->request->data;

			// modify data
			$data['User']['password'] = $data['Customer']['password'];
			$data['User']['confirm_password'] = $data['Customer']['confirm_password'];
			// limit fields to save
			$fieldList = array(
				'User' => array('id','password','confirm_password'),
			);

			if ($this->User->save($data, compact('fieldList'))) {
				$this->Alert->success('Customer password has been changed');
				return $this->redirect(array('action' => 'view', $data['Customer']['id']));
			} else {
				$this->Alert->error('The password change could not be saved');
			}
		}

		$this->uses[] = 'User';
		// fetch dropdown lists from User model
		$role = $this->User->role;
		$this->set(compact('role'));

		// fetch customer
		$customer = $this->Customer->find('first', array(
			'fields' => array('id','full_name','email', 'hash'),
			'conditions' => array('Customer.id' => $id),
			'contain' => array(
				'CustomerAccount' => array('fields' => array('id','name')),
				'User' => array('fields' => array('id','username', 'password'))
			)
		));

		// GET request
		if ($this->request->is('get')) {
			$this->request->data = $customer;
		}

		$this->set(compact('customer'));
	}

/**
 * Change a customer's password
 * 
 * @param int $id customer id
 * @return void
 */
	public function activateBizcenter($id = null) {

		$this->Customer->id = $id;

		// check customer id
		if (!$this->Customer->exists()) {
			$this->Alert->error('Choose a valid customer to change their password');
			return $this->redirect(array('action' => 'index'));
			// throw new NotFoundException("Invalid customer id");
		}

		// FIXME STOPGAP: temporarily associate customer with user
		$this->Customer->User->useTable = 'user_tbl';

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('put')) {

			// copy data
			$data = $this->request->data;

			// modify data
			$data['User']['password'] = $data['Customer']['password'];
			$data['User']['confirm_password'] = $data['Customer']['confirm_password'];
			$data['User']['username'] = $data['Customer']['email'];
			// limit fields to save
			$fieldList = array(
				// EDITED BY CATHY TO SAVE TO USER_TBL
				// I basically changed this function from just changing someone's password to 'activating' their BizCenter account
				// Stop gap, needs refractoring
				'User' => array('id','password','confirm_password','username','role'),
				'Customer' => array('user_id', 'hash', 'salt')
			);

			// FIXME STOPGAP: save password in both cust_pers_tbl and user_tbl
			if ($this->Customer->saveAssociated($data, compact('fieldList'))) {
				$this->Alert->success('Customer password has been changed');
				return $this->redirect(array('action' => 'view', $data['Customer']['id']));
			} else {
				$this->Alert->error('The password change could not be saved');
			}
		}

		$this->uses[] = 'User';
		// fetch dropdown lists from User model
		$role = $this->User->role;
		$this->set(compact('role'));

		// fetch customer
		$customer = $this->Customer->find('first', array(
			'fields' => array('id','full_name','email', 'hash'),
			'conditions' => array('Customer.id' => $id),
			'contain' => array(
				'CustomerAccount' => array('fields' => array('id','name')),
				'User' => array('fields' => array('id','username', 'password'))
			)
		));

		// GET request
		if ($this->request->is('get')) {
			$this->request->data = $customer;
		}

		$this->set(compact('customer'));
	}


/**
 * Filter and sort all customers
 * FIXME searching from the nth page when there are fewer than n pages of results causes view to break
 *   searching from the nth page when the number of results on the nth page is less than the limit may cause confusion
 * 
 * @return void
 */
	public function index() {

		// jump to customerAccount
		if (!empty($this->request->query['id'])) {
			$id = $this->request->query['id'];
			if ($this->Customer->exists($id)) {
				return $this->redirect(array('action' => 'view', $id));
			} else {
				$this->Alert->error('Invalid customer id');
			}
		}

		// build query
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery(
				$this->request->query,
				array('fulltext' => array(
					'first_name',
					'last_name',
					'full_name',
					'nickname',
					'email'
				))
			);
			// $conditions['OR'][] = array('CustomerAccount.name ILIKE' => "%{$this->request->query['q']}%");
			$this->request->data['Customer'] = $this->request->query;
		}

		// if is_active is not specified
		if (!isset($conditions['Customer.is_active'])) {

			// exclude inactive customers
			$conditions['Customer.is_active'] = 1;
		}


		// fetch related models
		$this->Customer->User->useTable = 'user_tbl';
		$contain = array(
			'CustomerAccount' => array('fields' => array('id','name')),
			'User' => array('fields' => array('id','username'))
		);

		// define paginator settings
		$this->Paginator->settings = array('Customer' => compact('conditions','contain','order'));

		$customers = $this->Paginator->paginate();

		$this->set(compact('customers'));
	}

/**
 * Search customers
 * 
 * @return 
 */
	public function search() {

		// parse the query
		$query = @$this->request->query['query'];

		// set conditions
		$conditions = array('Customer.is_active' => true);
		if (!empty($query)) {
			$conditions['OR']['CustomerAccount.name ILIKE'] = 
			$conditions['OR']['Customer.full_name ILIKE'] = "%{$query}%";
		}

		// fetch matching customers
		$customers = $this->Customer->find('all', array(
			'conditions' => $conditions,
			'contain' => array('CustomerAccount.name','CustomerAccount.pkg_notify_email'),
			'fields' => array('id','full_name','email'),
			'order' => 'full_name ASC'
		));

		// build suggestions list
		$suggestions = array();
		foreach ($customers as $customer) {
			
			// build suggestion
			$suggestion = new stdClass;
			$suggestion->data = arrayToObject($customer);
			$suggestion->value = "{$customer['Customer']['full_name']} ({$customer['CustomerAccount']['name']})";

			// add to list
			$suggestions[] = $suggestion;
		}
		
		// serialize the suggestions
		$this->set('suggestions',$suggestions);
		$this->set('_serialize', array('suggestions'));
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		if (!$this->Customer->exists($id)) {
			$this->Alert->error('Choose a valid customer to view their details');
			return $this->redirect(array('action' => 'index'));
			// throw new NotFoundException(__('Invalid customer'));
		}

		$conditions = array('Customer.id' => $id);
		$contain = array('CustomerAccount' => array('fields' => array('id','name')));

		$customer = $this->Customer->find('first', compact('conditions','fields','contain'));

		$this->request->data = $customer;
		$cust_acct_id = $this->Customer->CustomerAccount->find('list', array('order' => 'name ASC'));

		$this->set(compact('customer', 'cust_acct_id'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($license_agreement_id = null) {

		// convert action
		if (!empty($license_agreement_id)) {

			$this->uses[] = 'LicenseAgreement';
			$customer = $this->LicenseAgreement->find('first', array(
				'conditions' => array(
					'LicenseAgreement.id' => $license_agreement_id,
				)
			));
			unset($customer['LicenseAgreement']['license_agreement_rtf']);
			unset($customer['LicenseAgreement']['license_agreement_pdf']);
			unset($customer['LicenseAgreement']['license_agreement_html']);

			// Prefill Company Info
			$customer['CustomerAccount']['name'] = $customer['LicenseAgreement']['company'];
			$customer['CustomerAccount']['external_name'] = $customer['LicenseAgreement']['company'];
			$customer['CustomerAccount']['description'] = $customer['LicenseAgreement']['business_type'];
			$customer['CustomerAccount']['floor_id'] = $customer['LicenseAgreement']['floor_id'];
			$customer['CustomerAccount']['pkg_notify_email'] = $customer['LicenseAgreement']['email'];

			// Prefil Customer Info
			$customer['Customer']['first_name'] = $customer['LicenseAgreement']['first_name'];
			$customer['Customer']['last_name'] = $customer['LicenseAgreement']['last_name'];
			$customer['Customer']['title'] = $customer['LicenseAgreement']['title'];
			$customer['Customer']['email'] = $customer['LicenseAgreement']['email'];

			$this->request->data = $customer;

		}

		// POST request
		if ($this->request->is('post')) {

			// copy data
			$data = $this->request->data;
			// $data = filter_recursive($data); // filter out empty fields

			// clear model
			$this->Customer->create();
			$this->uses[] = 'CustomerAccount';

			// add customer to newly created company
			if ($data['create_new_company'] == true) {

				$data['Customer']['make_primary'] = 1;

				// echo 'creating new company...';
				unset($data['Customer']['cust_acct_id']);
				
				$this->Customer->set($data);
				$this->CustomerAccount->set($data);

				// build fieldList
				$fieldList = array(
					'Customer' => array('salutation','first_name','middle_name','last_name','is_active','nickname','email', 'note',),
					'CustomerAccount' => array('is_active','name','external_name','description','note','type','room_minutes','visible','service_id','primary_floor_id')
				);

				if ($this->Customer->saveAssociated($data, compact('fieldList'))) {
					$this->Alert->success('Customer saved successfully');

					// mark new customer as primary contact
					$this->CustomerAccount->saveField('primary_contact_cust_pers_id', $this->Customer->id);

					return $this->redirect(array('controller' => 'customers', 'action' => 'view', $this->Customer->id));
				} else {
					$this->Alert->error('Customer could not be saved');
				}
			}
			
			// add customer to existing company
			else {

				// echo 'adding customer to existing company...';
				unset($data['CustomerAccount']);
				$this->Customer->set($data);

				// build fieldList
				$fieldList = array(
					'Customer' => array('cust_acct_id', 'salutation', 'first_name', 'middle_name', 'last_name', 'is_active', 'nickname', 'email', 'note')
				);
				if ($this->Customer->save($data, compact('fieldList'))) {
					$this->Alert->success('Customer saved successfully');

					// mark new customer as primary contact
					if ($data['Customer']['make_primary']) {
						$this->CustomerAccount->id = $data['Customer']['cust_acct_id'];
						$this->CustomerAccount->saveField('primary_contact_cust_pers_id', $this->Customer->id);
					}
		
					return $this->redirect(array('controller' => 'customers', 'action' => 'view', $this->Customer->id));
				} else {
					$this->Alert->error('Customer could not be saved');
				}
			}
		}
		
		// GET request
		else {
			$this->request->data['Customer']['confirm_password'] = 
			$this->request->data['Customer']['password'] = random_string(16);
		}
	
		$this->uses[] = 'CustomerAccount';
		$this->uses[] = 'Floor';

		// fetch dropdown lists from model
		$types = $this->CustomerAccount->types;
		$room_minutes = $this->CustomerAccount->room_minutes;
		$this->set(compact('types','room_minutes'));

		// fetch rows from datasource
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'is_active'));
		$this->set(compact('services'));

		// fetch primary floors from model
		$this->uses[] = 'Floor';
		$floors = $this->Floor->listActiveOfficeFloors();
		$this->set(compact('floors'));

		$buildings = $this->Floor->Building->find(
			'list', array(
				'conditions' => 'is_active', 
				'fields' => array('id','short_global_label')
		));

		foreach ($floors as $building_id => $building) {
			@$floors[$buildings[$building_id]] = $building;
			unset($floors[$building_id]);
		}
		$customerAccounts = $this->CustomerAccount->find('list', array('conditions' => array('CustomerAccount.is_active' => true)));
		$this->set(compact('customerAccounts','floors', 'buildings'));
		
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		// check customer id
		if (!$this->Customer->exists($id)) {
			throw new NotFoundException(__('Invalid customer'));
		}

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('put')) {

			// copy the data
			$data = $this->request->data;

			// determine which section is being edited
			switch ($data['Customer']['editing_section']) {

				case 'basic':
					$fieldList = array(
						'id',
						'is_active',
						'salutation',
						'first_name',
						'middle_name',
						'last_name',
						'nickname',
						'email',
						'change_password',
						'work_phone_number',
						'home_phone_number',
						'cell_phone_number',
						'note',
						'cust_acct_id'
					);
				break;

				case 'additional':
					$fieldList = array(
						'id',
						'register_with_building',
						'other_phone_number',
						'directory_1',
						'directory_2',
						'directory_3',
						'title',
						'work_fax_number',
						'work_address_1',
						'work_address_2',
						'work_city',
						'work_state',
						'work_zip_code',
						'work_country',
						'home_address_1',
						'home_address_2',
						'home_city',
						'home_state',
						'home_zip_code',
						'home_country',
						'birthday',
						'visibility',
						'suffix',
						'email2',
						'email3',
						'inbound_fax_alert_cutoff_dt',
						'enable_cc_pay',
						'receive_email',
						'fax_sender_name',
						'profile_picture_url'
					);
				break;

				default:
					throw new NotFoundException("There was a problem processing the form you submitted");
				break;
			}

			// attempt to save the customer information
			if ($this->Customer->save($data, compact('fieldList'))) {
				$this->Alert->success(__('The customer has been saved.'));
			} else {
				$this->Alert->error(__('The customer information could not be saved. Please, try again.'));
			}

			// redirect to the manually specified page
			if (!empty($data['Customer']['next'])) {
				return $this->redirect($data['Customer']['next']);
			}
		}

		// GET request
		else {
			$conditions = array('Customer.id' => $id);
			$this->request->data = 
			$customer = $this->Customer->find('first', compact('conditions','fields','contain'));
		}
	}
	

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Customer->id = $id;
		if (!$this->Customer->exists()) {
			throw new NotFoundException(__('Invalid customer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Customer->delete()) {
			$this->Session->setFlash(__('The customer has been deleted.'));
		} else {
			$this->Session->setFlash(__('The customer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

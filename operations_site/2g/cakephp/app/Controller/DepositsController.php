<?php
App::uses('AppController', 'Controller');
/**
 * Deposits Controller
 *
 * @property Deposit $Deposit
 * @property PaginatorComponent $Paginator
 */
class DepositsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $uses = array('Deposit','ReceivedPayment');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$query = $this->request->query;

		if (!empty($query)) {
			$conditions = $this->filterByQuery($query);
		}

		// deposits
		$this->Deposit->recursive = 0;
		$limit = $this->request->query('limit') ?: 40;
		$order = 'deposit_dt DESC';
		$contain = array('ReceivedPayment' => array('fields' => 'id'));
		$this->Paginator->settings = array('Deposit' => compact('conditions','order','limit','contain'));
		$deposits = $this->Paginator->paginate();
		// TODO use deposit.received_payments_count in view
		$this->set(compact('deposits'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Deposit->exists($id)) {
			throw new NotFoundException(__('Invalid deposit'));
		}
		$deposit = $this->Deposit->find('first', array(
			'conditions' => array('Deposit.' . $this->Deposit->primaryKey => $id),
			'contain' => array(
				'ReceivedPayment' => array(
					'fields' => array(
						'id',
						'cust_acct_id',
						'employee_id',
						'amount',
						'recv_dt',
						'payment_method',
						'invoice_number',
						'note',
						'check_name',
						'check_number'
					),
					'Employee' => array('fields' => array('id','full_name')),
					'CustomerAccount' => array('fields' => array('id','name'))
				),
				'Employee' => array('fields' => array('id','full_name'))
			)
		));
		$this->set(compact('deposit'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$data = $this->request->data;

			// collect ReceivedPayment
			foreach ($data['ReceivedPayment'] as $payment) {
				if ($payment['is_used']) {
					$payment_ids[] = $payment['id'];
				}
			}

			// fill in bank account information
			$bankAccount = $this->Deposit->bank_accounts[$data['Deposit']['bank_account']];

			$data['Deposit']['bank_account_number'] = $bankAccount['account_number'];
			$data['Deposit']['bank_routing_number'] = $bankAccount['routing_number'];
			$data['Deposit']['bank_name'] = $bankAccount['name'];

			$this->Deposit->create();
			if ($this->Deposit->save($data)) {
				
				// set deposit_id of checkboxed ReceivedPayments to newly created deposit
				$updated = $this->ReceivedPayment->updateAll(
					array(
						'deposit_id' => $this->Deposit->id,
						'process_status' => "'complete'"
					),
					array('id' => $payment_ids)
				);

				$this->Session->setFlash(__('The deposit has been saved.'));
				$this->request->data = null;
			} else {
				$this->Session->setFlash(__('The deposit could not be saved. Please, try again.'));
			}
		}
		$employees = $this->Deposit->Employee->find('list', array(
			'conditions' => array('is_active' => true),
			'order' => 'full_name DESC'
		));

		$this->ReceivedPayment->recursive = -1;
		$receivedPayments = $this->ReceivedPayment->find('all', array(
			'conditions' => array(
				'deposit_id' => NULL,
				'process_status' => 'pending'
			),
			'fields' => array(
				'id',
				'cust_acct_id',
				'employee_id',
				'amount',
				'recv_dt',
				'payment_method',
				'process_txn_after_dt',
				'process_mode',
				'process_status',
				'payment_destination',
			),
			'contain' => array(
				'Employee' => array('id','first_name','last_name'),
				'CustomerAccount' => array('id','name')
			),
			'order' => 'process_txn_after_dt ASC'
		));
		$bankAccounts = $this->Deposit->bankAccounts();
		$this->set(compact('employees','receivedPayments','bankAccounts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Deposit->exists($id)) {
			throw new NotFoundException(__('Invalid deposit'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Deposit->save($this->request->data)) {
				$this->Session->setFlash(__('The deposit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The deposit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Deposit.' . $this->Deposit->primaryKey => $id));
			$this->request->data = $this->Deposit->find('first', $options);
		}
		$employees = $this->Deposit->Employee->find('list');
		$this->set(compact('employees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	// public function remove($id = null) {
	// 	$this->Deposit->id = $id;
	// 	if (!$this->Deposit->exists()) {
	// 		throw new NotFoundException(__('Invalid deposit'));
	// 	}
	// 	$this->request->onlyAllow('post', 'delete');
	// 	if ($this->Deposit->remove()) {
	// 		$this->Session->setFlash(__('The deposit has been removed.'));
	// 	} else {
	// 		$this->Session->setFlash(__('The deposit could not be removed. Please, try again.'));
	// 	}
	// 	return $this->redirect(array('action' => 'index'));
	// }
}

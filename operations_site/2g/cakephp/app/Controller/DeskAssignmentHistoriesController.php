<?php
App::uses('AppController', 'Controller');
/**
 * DeskAssignmentHistories Controller
 *
 * @property DeskAssignmentHistory $DeskAssignmentHistory
 * @property PaginatorComponent $Paginator
 */
class DeskAssignmentHistoriesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->DeskAssignmentHistory->recursive = 0;
		$this->set('deskAssignmentHistories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->DeskAssignmentHistory->exists($id)) {
			throw new NotFoundException(__('Invalid desk assignment history'));
		}
		$options = array('conditions' => array('DeskAssignmentHistory.' . $this->DeskAssignmentHistory->primaryKey => $id));
		$this->set('deskAssignmentHistory', $this->DeskAssignmentHistory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->DeskAssignmentHistory->create();
			if ($this->DeskAssignmentHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The desk assignment history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The desk assignment history could not be saved. Please, try again.'));
			}
		}
		$desks = $this->DeskAssignmentHistory->Desk->find('list');
		$customerAccounts = $this->DeskAssignmentHistory->CustomerAccount->find('list');
		$customers = $this->DeskAssignmentHistory->Customer->find('list');
		$this->set(compact('desks', 'customerAccounts', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->DeskAssignmentHistory->exists($id)) {
			throw new NotFoundException(__('Invalid desk assignment history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->DeskAssignmentHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The desk assignment history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The desk assignment history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('DeskAssignmentHistory.' . $this->DeskAssignmentHistory->primaryKey => $id));
			$this->request->data = $this->DeskAssignmentHistory->find('first', $options);
		}
		$desks = $this->DeskAssignmentHistory->Desk->find('list');
		$customerAccounts = $this->DeskAssignmentHistory->CustomerAccount->find('list');
		$customers = $this->DeskAssignmentHistory->Customer->find('list');
		$this->set(compact('desks', 'customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->DeskAssignmentHistory->id = $id;
		if (!$this->DeskAssignmentHistory->exists()) {
			throw new NotFoundException(__('Invalid desk assignment history'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->DeskAssignmentHistory->delete()) {
			$this->Session->setFlash(__('The desk assignment history has been deleted.'));
		} else {
			$this->Session->setFlash(__('The desk assignment history could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

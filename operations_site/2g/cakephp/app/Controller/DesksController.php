<?php
App::uses('AppController', 'Controller');
/**
 * Desks Controller
 *
 * @property Desk $Desk
 * @property PaginatorComponent $Paginator
 */
class DesksController extends AppController {



/**
 * index method
 *
 * @return void
 */
	public function index() {

		$conditions = array();

		$query = $this->request->query;

		// if filter params are set...
		if (!empty($query)) {

			$conditions = $this->filterByQuery($query, array(
				'ignore' => array(
					'is_taken','is_enclosed'
				)
			));

			if (strlen(@$query['is_taken']) > 0) {
				if ($query['is_taken'] == 1) {
					// occupied
					$conditions['NOT']['Desk.account_id']= null;
				} else {
					// vacant
					$conditions['Desk.account_id'] = null;
				}
			}
			if (strlen(@$query['is_enclosed']) > 0) {
				if ($query['is_enclosed'] == 0) {
					$conditions['NOT']['Desk.is_enclosed'] = true;
				} else {
					$conditions['Desk.is_enclosed'] = true;
				}
			}


			// workaround for datatype numbers only
			$conditions['OR'][]['CAST(Desk.assigned_number AS TEXT) LIKE'] = "%{$this->request->query['q']}%";
			// $conditions['OR'][]['CAST(Desk.current_price AS TEXT) LIKE'] = "%{$this->request->query['q']}%";
			// $conditions['OR'][]['CAST(Desk.list_price AS TEXT) LIKE'] = "%{$this->request->query['q']}%";
			$conditions['OR'][]['CustomerAccount.name ILIKE'] = "%{$this->request->query['q']}%";
			
			// set form input values to passed params
			$this->request->data['Desk'] = $this->request->query;
		}


		// if status is not specified
		if (!isset($conditions['Desk.status ILIKE'])) {

			// default paginator settings
			$conditions['Desk.is_active'] = true;

		}

		// Dropdown list
		$is_encloseds = $this->Desk->is_encloseds;
		$not_taken = $this->Desk->not_taken;

		// Building - Floor array
		$floors = $this->Desk->Floor->listActiveOfficeFloors();

		$this->Desk->order = 'Desk.start_date DESC';
		$this->Desk->recursive = 0;
		$this->Paginator->settings = array('Desk' => compact('conditions','contain','order'));
		$desks = $this->Paginator->paginate();

		$this->set(compact('desks', 'types', 'is_encloseds', 'floors'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Desk->exists($id)) {
			throw new NotFoundException(__('Invalid desk'));
		}
		$options = array(
			'conditions' => array('Desk.' . $this->Desk->primaryKey => $id),
			'recursive' => 1,
			'contain' => array(
				'Floor' => 'Building',
				'Customer',
				'CustomerAccount',
				'DeskAssignmentHistory' => array('CustomerAccount', 'Customer')
			)
		);
		$this->set('desk', $this->Desk->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Desk->create();
			if ($this->Desk->save($this->request->data)) {
				$this->Session->setFlash(__('The desk has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The desk could not be saved. Please, try again.'));
			}
		}
		$floors = $this->Desk->Floor->find('list');
		$customerAccounts = $this->Desk->CustomerAccount->find('list');
		$customers = $this->Desk->Customer->find('list');
		$this->set(compact('floors', 'customerAccounts', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Desk->exists($id)) {
			throw new NotFoundException(__('Invalid desk'));
		}


		if ($this->request->is(array('post', 'put'))) {
			if ($this->Desk->save($this->request->data)) {
				$this->Session->setFlash(__('The desk has been saved.'));


				return $this->redirect(array('action' => 'view', $this->request->data['Desk']['id']));
			} else {
				$this->Session->setFlash(__('The desk could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Desk.' . $this->Desk->primaryKey => $id));
			$this->request->data = $this->Desk->find('first', $options);
		}

		$options = array(
			'conditions' => array('Desk.' . $this->Desk->primaryKey => $id),
			'recursive' => 1,
			'contain' => array(
				'Floor' => 'Building',
				'Customer',
				'CustomerAccount',
				'DeskAssignmentHistory'
			)
		);
		$this->set('desk', $this->Desk->find('first', $options));

		$floors = $this->Desk->Floor->find('list');
		$customerAccounts = $this->Desk->CustomerAccount->find('list', [
			'order' => 'name ASC']);
		// $customers = $this->Desk->Customer->find('list');
		$customers = $this->Desk->CustomerAccount->PrimaryCustomer->find('list', [
			'order' => 'full_name ASC'
		]);
		$this->set(compact('floors', 'customerAccounts', 'customers'));
	}


/**
 * move method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function move($id = null) {
		if (!$this->Desk->exists($id)) {
			throw new NotFoundException(__('Invalid desk'));
		}

		$moveOut = $this->Desk->find('first', array('conditions' => array('Desk.' . $this->Desk->primaryKey => $id), 'recursive' => 1));

		$deskAssignmentHistory = array(
			'DeskAssignmentHistory' => array(
				'desk_id' => $moveOut['Desk']['id'],
				'cust_acct_id' => $moveOut['Desk']['account_id'],
				'cust_pers_id' => $moveOut['Desk']['cust_pers_id'],
				'start_date' => $moveOut['Desk']['start_date'],
				'end_date' => $moveOut['Desk']['end_date'],
				'current_price' => $moveOut['Desk']['current_price'],
				'note' => $moveOut['Desk']['note']
			)
		);

		if ($this->request->is(array('post', 'put'))) {

			$moveOut = $this->Desk->find('first', array('conditions' => array('Desk.' . $this->Desk->primaryKey => $id), 'recursive' => 1));

			$deskAssignmentHistory = array(
				'DeskAssignmentHistory' => array(
					'desk_id' => $moveOut['Desk']['id'],
					'cust_acct_id' => $moveOut['Desk']['account_id'],
					'cust_pers_id' => $moveOut['Desk']['cust_pers_id'],
					'start_date' => $moveOut['Desk']['start_date'],
					'end_date' => $moveOut['Desk']['end_date'],
					'current_price' => $moveOut['Desk']['current_price'],
					'note' => $moveOut['Desk']['note']
				)
			);

			// clear desk entry
			$moveOut['Desk']['account_id'] = '';
			$moveOut['Desk']['cust_pers_id'] = '';
			$moveOut['Desk']['start_date'] = '';
			$moveOut['Desk']['end_date'] = '';
			$moveOut['Desk']['note'] = '';

			$this->uses[] = 'DeskAssignmentHistory';
			if ($this->DeskAssignmentHistory->save($deskAssignmentHistory)) {

				if ($this->Desk->save($moveOut)) {

					// saved successfully
					$this->Session->setFlash(__('The desk has been saved.'));
					return $this->redirect(array('action' => 'view', $moveOut['Desk']['id']));

				}  else {
					$this->Session->setFlash(__('The desk could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The desk could not be saved. Please, try again.'));
			}
		}
		$options = array(
			'conditions' => array('Desk.' . $this->Desk->primaryKey => $id),
			'recursive' => 1,
			'contain' => array(
				'Floor' => 'Building',
				'Customer',
				'CustomerAccount',
				'DeskAssignmentHistory'
			)
		);
		$this->set('desk', $this->Desk->find('first', $options));

		$customerAccounts = $this->Desk->CustomerAccount->find('list');
		$customers = $this->Desk->Customer->find('list');
		$this->set(compact('deskAssignmentHistory', 'customers', 'customerAccounts'));
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Desk->id = $id;
		if (!$this->Desk->exists()) {
			throw new NotFoundException(__('Invalid desk'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Desk->delete()) {
			$this->Session->setFlash(__('The desk has been deleted.'));
		} else {
			$this->Session->setFlash(__('The desk could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

<?php
App::uses('AppController', 'Controller');
/**
 * Documents Controller
 *
 * @property Document $Document
 */
class DocumentsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// if ($this->Auth->user('role') < ROLE_SUPER) {
		// 	$this->redirect(array('action' => 'account', $this->Auth->user('cust_acct_id')));
		// }

		$this->Paginator->settings = array(
			'Document' => array(
				'recursive' => 0,
				'limit' => 20,
				'order' => 'Document.file_timestamp DESC'
			)
		);
		$this->set('documents', $this->Paginator->paginate());

	}


/**
 * See documents related to the current user's customerAccount
 * 
 * @param $id id of the customerAccount
 * @return void
 */
	public function account($id = null) {

		if ($this->Auth->user('role') < ROLE_SUPER) {
			if ($id != 1) {
				throw new ForbiddenException('You cannot view someone else\'s documents');
			}
		}

		$this->Paginator->settings = array(
			'Document' => array(
				'conditions' => array(
					'account_id' => 1
				),
				'recursive' => 0,
				'limit' => 20,
				'order' => 'Document.file_timestamp DESC'
			)
		);
		$this->set('documents', $this->Paginator->paginate());

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		$options = array('conditions' => array(
			'Document.' . $this->Document->primaryKey => $id),
			'recursive' => 1
		);
		$this->set('document', $this->Document->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Document->create();
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Document->CustomerAccount->find('list');
		$customers = $this->Document->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
			$this->request->data = $this->Document->find('first', $options);
		}
		$customerAccounts = $this->Document->CustomerAccount->find('list');
		$customers = $this->Document->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Document->id = $id;
		if (!$this->Document->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		$this->request->onlyAllow('post', 'delete');

		$type = $this->Document->field('type');

		switch ($type) {

			case 'scan':
				$this->uses[] = 'Scan';
				if ($this->Scan->delete($id)) {
					$this->Alert->success(__('Scan deleted'));
					return $this->redirect(array('action' => 'index'));
				}
			break;

			case 'fax':
				$this->uses[] = 'FaxIn';
				if ($this->FaxIn->delete($id)) {
					$this->Alert->success(__('Fax deleted'));
					return $this->redirect(array('action' => 'index'));
				}
			break;
		}
		$this->Alert->error(__('Document was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * EmployeeRoles Controller
 *
 * @property EmployeeRole $EmployeeRole
 * @property PaginatorComponent $Paginator
 */
class EmployeeRolesController extends AppController {

/**
 * Models used by this controller
 * 
 * @var array
 */
	public $uses = ['EmployeeRole','Permission','User'];

/**
 * index method
 * 
 * @return void
 */
	public function index() {

		// if filter params are set
		if (!empty($this->request->query)) {

			// build conditions from query
			$conditions = $this->filterByQuery($this->request->query, array('fulltext' => 'element_value'));
			$this->request->data['EmployeeRole'] = $this->request->query;
		}

		$contain = array('Employee' => array('fields' => array('id','full_name')));
		$this->Paginator->settings = array('EmployeeRole' => compact('conditions','contain','fields'));
		$roles = $this->Paginator->paginate();
		$this->set(compact('roles'));

		// fetch dropdown lists from model
		$element_values = $this->EmployeeRole->element_values;

		// fetch rows from datasource
		$this->uses[] = 'Employee';
		$employees = $this->Employee->find('list', array('conditions' => array('is_active' => true), 'order' => 'first_name ASC'));
		$this->set(compact('employees','element_values'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->EmployeeRole->exists($id)) {
			throw new NotFoundException(__('Invalid employee role'));
		}
		$options = array('conditions' => array('EmployeeRole.' . $this->EmployeeRole->primaryKey => $id));
		$this->set('employeeRole', $this->EmployeeRole->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// POST request
		if ($this->request->is('post')) {

			$this->EmployeeRole->create();

			// save element_value
			$element_value = $this->request->data['EmployeeRole']['element_value'];

			// reindex for saveMany
			foreach ($this->request->data['EmployeeRole']['Employee'] as $parent_id) {
				$data[]['EmployeeRole'] = compact('parent_id','element_value');
			}

			// the role exists
			if (in_array($this->request->data['EmployeeRole']['element_value'], $this->EmployeeRole->element_values)) {
				$this->Alert->error('That role already exists');
			}
			// create the role
			elseif ($this->EmployeeRole->saveMany($data)) {
				$this->Alert->success('The role has been saved and assigned to employees');
				return $this->redirect(array('action' => 'index', '?' => array('q' => $element_value)));
			}
			// failed to create the role
			else {
				debug($this->EmployeeRole->validationErrors);
				$this->Alert->error('The role could not be saved. Please, try again.');
			}
		}

		// set of all roles
		$roles = $this->EmployeeRole->getUniqueRoles();

		// list of active employees
		$employees = $this->EmployeeRole->Employee->find('list', array(
			'conditions' => array('Employee.is_active'),
			'order' => 'Employee.full_name ASC'
		));
		$this->set(compact('employees','roles'));
	}

/**
 * reassign employee roles
 * 
 * @return void
 */
	public function reassign($role_key = null) {

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('post')) {
			
			// save element_value
			$element_value = $this->request->data['EmployeeRole']['element_value'];

			// old = (fetched from db)
			$old_assignees = $this->EmployeeRole->find('list', array(
				'conditions' => compact('element_value'),
				'fields' => ['id','parent_id']
			));

			// new = (form data)
			$new_assignees = $this->request->data['EmployeeRole']['Employee'];

			// diff = old - new
			$diff_assignees = array_diff($old_assignees, $new_assignees);

			// reindex for saveMany
			foreach ($new_assignees as $parent_id) {

				// find the role id if it exists
				$role_id = $this->EmployeeRole->field('id', compact('parent_id','element_value'));

				// if it does not already exist...
				if (empty($role_id))
				{
					// add it to the list of roles to be added 
					$roles_to_add[] = compact('parent_id','element_value');
				}
			}

			// default both flags to true
			$removed = $added = true;

			// remove unchecked employees
			if (!empty($diff_assignees))
			{
				// get the role ids from the array diff
				$role_ids_to_delete = array_keys($diff_assignees);

				// delete the roles that were unassigned
				$removed = $this->EmployeeRole->deleteAll(['EmployeeRole.id' => $role_ids_to_delete]);
			}

			// add (newly) checked employees
			if (!empty($roles_to_add))
			{
				// add the roles that were newly assigned
				$added = $this->EmployeeRole->saveMany($roles_to_add);
			}

			// save the roles
			if ($removed && $added)
			{
				$this->Alert->success('Role assignments have been saved');
				return $this->redirect($_SERVER['REQUEST_URI']);
			}
			else
			{
				$this->Alert->error('The role assignments could not be saved');
			}
		}

		// GET request
		if ($this->request->is('get')) {
			
			// set the form data
			$ev = $this->request->data['EmployeeRole']['element_value'] = @$this->EmployeeRole->element_values[$role_key];

			// fill in existing assignees' checkboxes
			if (!empty($ev))
			{
				foreach ($this->EmployeeRole->assignees($ev) as $employee_id) {
					$this->request->data['Employee'][]['id'] = $employee_id;
				}
			}
		}

		// fetch set of all roles
		$roles = $this->EmployeeRole->getUniqueRoles();

		// enable the checklist iff a role is selected
		$enable_employee_checklist = array_key_exists($role_key, $roles);

		// list of active employees
		$employees = $this->EmployeeRole->Employee->find('list', array(
			'conditions' => array('Employee.is_active'),
			'order' => 'Employee.full_name ASC'
		));

		// send vars to view
		$this->set(compact('employees','roles','enable_employee_checklist'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->EmployeeRole->id = $id;
		if (!$this->EmployeeRole->exists()) {
			throw new NotFoundException(__('Invalid employee role'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->EmployeeRole->delete()) {
			$this->Session->setFlash(__('The employee role has been deleted.'));
		} else {
			$this->Session->setFlash(__('The employee role could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

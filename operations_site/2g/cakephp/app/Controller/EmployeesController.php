<?php
App::uses('AppController', 'Controller');
/**
 * Employees Controller
 *
 * @property Employee $Employee
 * @property PaginatorComponent $Paginator
 */
class EmployeesController extends AppController {


/**
 * Allow user to login before authentication
 * 
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('cronEmailBillingReviewPrompt');
	}

/**
 * FOR DEVELOPMENT PURPOSES ONLY
 * Generate and save a hash+salt combo from the user's existing cleartext password,
 * or generate and save hash+salt combos from a universal password 
 * 
 * @param string $cleartext universal password
 * @return void
 */
	public function hashify($cleartext = null) {
		
		// // set all user passwords to $cleartext
		// if (!empty($cleartext)) {
		// 	$employees = $this->Employee->find('list');
		// 	debug($employees);

		// 	$data = array();
		// 	foreach ($employees as $id => $employee_name) {

				$hashed = $this->Employee->hash($cleartext);
				debug($hashed); die;

		// 		$data[] = array(
		// 			'id' => $id,
		// 			'hash' => $hashed['hash'],
		// 			'salt' => $hashed['salt']
		// 		);
		// 	}

		// 	$fieldList = array('id','hash','salt');
		// 	$saved = $this->Employee->saveMany($data, compact('fieldList'));
		// 	debug($saved);
		// }
		// // set each user password to $password
		// else {
		// 	$passwords = $this->Employee->find('list', array('fields' => array('id','password')));
		// 	foreach ($passwords as $id => $password) {
		// 		$hashed = $this->Employee->hash($password);
		// 		extract($hashed);
		// 		$data[] = compact('id','hash','salt');
		// 	}

		// 	$fieldList = array('id','hash','salt');
		// 	$saved = $this->Employee->saveMany($data, compact('fieldList'));
		// 	debug($saved);
		// }
	}


/**
 * Login as an employee
 * 
 */
	public function login() {

		// redirect if user is already logged in
		if ($this->Auth->loggedIn()) {
			$this->redirect($this->Auth->redirect());
		}

		// post request
		if ($this->request->is('post')) {

			// copy request data
			$data = $this->request->data;

			// use Employee model
			$this->uses[] = 'Employee';

			// reverse lookup the id from the username
			$this->Employee->id = $id = $this->Employee->lookupId('email',$data['Employee']['username']);

			// check credentials
			if ($this->Employee->authenticate($id, $data['Employee']['password'])) {

				// fetch fields
				$fields = array('id','email','title');
				$conditions = array('Employee.id' => $id);
				$employee = $this->Employee->find('first',compact('conditions','fields'));

				// compile list of roles
				$roles = $this->Employee->roles($id);

				// set user data to store in auth
				$user = compact('id','roles');
				$user['username'] = $employee['Employee']['email'];

				// load data into AuthComponent
				$this->Auth->login($user);

				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Alert->error('Invalid username and/or password');
			}
		}

		$this->layout = 'login';
	}

	public function logout() {
		$this->redirect($this->Auth->logout());
	}

/**
 * Dashboard after login
 *
 * @return void
 */
	public function dashboard() {}

/**
 * Directory of employees
 *
 * @return void
 */
	public function index() {

		if (!empty($this->request->query['id'])) {
			// jump to matching employee

			$id = $this->request->query['id'];
			if ($this->Employee->exists($id)) {
				return $this->redirect(array('action' => 'view', $id));
			} else {
				unset($this->request->query['id']);
				$this->Alert->error('Invalid employee id #');
			}
		}

		elseif (!empty($this->request->query)) {

			// build array of conditions using the query
			$conditions = $this->filterByQuery($this->request->query, array('fulltext' => array('first_name', 'last_name', 'full_name')));

			// set form input values to passed params
			$this->request->data['Employee'] = $this->request->query;
		}

		if (!isset($this->request->query['is_active'])) {
			$conditions['is_active'] = 1;
		}

		$this->Paginator->settings = array('Employee' => compact('conditions','contain','fields','order'));
		$employees = $this->Paginator->paginate();

		$companies = $this->Employee->companies;
		$titles = $this->Employee->titles;
		$types = $this->Employee->types;
		$phone = $this->Employee->work_phone_number;
		$address = $this->Employee->work_address_1;
		

		$this->set(compact('employees','titles','companies','types'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		$this->Employee->id = $id;

		if (!$this->Employee->exists($id)) {
			$this->Alert->error('Choose a valid employee to view his or her details');
			return $this->redirect(array('action' => 'index'));
		}
		$employee = $this->Employee->find('first', array('conditions' => array('Employee.id' => $id)));
		foreach ($this->Employee->roles($id) as $role) {
			$employee['Role'][]['id'] = $this->Employee->Role->role_map[$role];
		}
		$this->request->data = $employee;	

		$roles = $this->Employee->Role->getUniqueRoles();
		$this->set(compact('employee','roles'));
	}


/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {

		// $this->Employee->id = $id;

		// if (!$this->Employee->exists($id)) {
		// 	$this->Alert->error('Choose a valid employee to view his or her details');
		// 	return $this->redirect(array('action' => 'index'));
		// }
		// $employee = $this->Employee->find('first', array('conditions' => array('Employee.id' => $id)));
		// foreach ($this->Employee->roles($id) as $role) {
		// 	$employee['Role'][]['id'] = $this->Employee->Role->role_map[$role];
		// }
		// $this->request->data = $employee;	

		// $roles = $this->Employee->Role->getUniqueRoles();
		// $this->set(compact('employee','roles'));
	}	

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// POST request
		if ($this->request->is('post')) {
			$this->Employee->create();
			if ($this->Employee->save($this->request->data)) {
				$this->Session->setFlash(__('The employee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The employee could not be saved. Please, try again.'));
			}
		}
		// GET request
		else {
			$this->request->data['Employee']['is_active'] = true;
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid employee'));
		}

		// POST|PUT request
		if ($this->request->is(array('post', 'put'))) {

			// copy data
			$data = $this->request->data;

			switch ($data['Meta']['editing_section']) {
				case 'basic':
					$fieldList = array('Employee' => array(
						'id',
						'is_active',
						'salutation',
						'first_name',
						'middle_name',
						'last_name',
						'suffix',
						'nickname',
						'email',
						'work_phone_number',
						'work_fax_number',
						'company',
						'title',
						'note'
					));
					break;
				case 'additional':
					$fieldList = array('Employee' => array(
						'id',
						'home_phone_number',
						'cell_phone_number',
						'other_phone_number',
						'work_address_1',
						'work_address_2',
						'work_city',
						'work_state',
						'work_zip_code',
						'work_country',
						'home_address_1',
						'home_address_2',
						'home_city',
						'home_state',
						'home_zip_code',
						'home_country',
						'birthday',
						'tax_id',
						'email2',
						'email3',
						'type',
						'in_directory',
						'allowed_ip_addr',
						'phone_pin',
						'vacation_days'
					));
					break;
				case 'roles':
					// $fieldList = array('EmployeeRole' => array('parent_id','element_value','created','modified','creation_dt'));
					break;
				default:
					throw new NotFoundException('There was an error processing the form');
					break;
			}

			// save the employee
			if ($this->Employee->save($data)) {
				$this->Alert->success(__('The employee has been saved.'));
				return $this->redirect(array('action' => 'view', $this->Employee->id));
			} else {
				$this->Alert->success(__('The employee could not be saved. Please, try again.'));
			}
		}
		// GET request
		else {
			$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
			$this->request->data = $this->Employee->find('first', $options);
		}
	}

/**
 * Add and remove roles for an employee
 * 
 * @param int $id employee id
 * @return void
 */
	public function assignRoles($id = null) {

		// set id
		$this->Employee->id = $id;

		// check that employee id is valid
		if (!$this->Employee->exists()) {
			throw new NotFoundException("Invalid employee id");
		}

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('put')) {

			// copy data
			$data = $this->request->data;
			if ($this->Employee->Role->assignRolesToEmployee($id, $data['Employee']['Role'])) {
				$this->Alert->success('Employee roles have been updated');
			} else {
				$this->Alert->error('Employee roles could not be updated');
			}
			return $this->redirect(array('action' => 'view', $id));
		}

		// set of all roles
		$roles = $this->Employee->Role->element_values;

		$this->set(compact('employee','roles'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Employee->id = $id;
		if (!$this->Employee->exists()) {
			throw new NotFoundException(__('Invalid employee'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Employee->delete()) {
			$this->Session->setFlash(__('The employee has been deleted.'));
		} else {
			$this->Session->setFlash(__('The employee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * Send emails to OMs, sales, and facilities to review billing details 
 * for customers they are responsible for. (CRON method)
 */
	public function cronEmailBillingReviewPrompt($cron_key = false) {

		if (!$this->isCronAllowed($cron_key)) {
			throw new UnauthorizedException('Invalid CRON key');
		}

		// non-OM employees who can help review billing changes
		$billing_reviewers = array(
			2762,  // Zoila P
			115,   // Matt P
		);

		// add "floor_count" virtual field
		$this->Employee->virtualFields['floor_count'] = 
			"SELECT COUNT(*)
			FROM floor_tbl
			WHERE floor_tbl.office_manager_id=Employee.id";

		// fetch the OMs
		$oms = $this->Employee->find('all', array(
			'conditions' => array(
				'is_active' => true,
				'floor_count >' => 0
			),
			'fields' => array('id', 'email', 'first_name', 'full_name', 'title', 'floor_count'),
			'contain' => array(
				'Floor' => array('id', 'short_local_label', 'dom_billing_cycle_starts')
			),
		));

		// non-OM billing reviewers
		$others = $this->Employee->find('all', array(
			'conditions' => array('id' => $billing_reviewers),
			'fields' => array('id', 'email', 'first_name', 'full_name', 'title', 'floor_count'),
		));

		// OMs and others
		$employees = reindex('Employee.id', $oms) + reindex('Employee.id', $others);
		
		// email the OMs a link to review their tenants' billing information
		foreach ($employees as $employee) {

			// employee first name
			$first_name = $employee['Employee']['first_name'];

			// OM employee
			if (!empty($employee['Floor'])) {

				// billing reviews due date
				$due_dates = array();
				foreach ($employee['Floor']as $floor) {
					$due_date = date(__('m/%02d', $floor['dom_billing_cycle_starts'] - 2));
					$label = $floor['short_local_label'];
					$due_dates[$due_date] = "{$due_date} ($label)";
				}
				$due_dates = strimplode($due_dates);

				// build link
				$floors = collect('id',$employee['Floor']);
				$filters = array(
					'is_active' => '1',
					'type' => 'customer',
					'primary_floor_id' => $floors
				);
				$link = Router::url(array('controller' => 'customerAccounts', 'action' => 'billing', '?' => $filters), true);

				// email body
				$html = "<p>Hi {$first_name},</p> <p> This notice is to remind you to conduct a review of the customer accounts on your floor(s) and to indicate any changes to their service or applicable fees by adding a billing memo.<br /> Your review must be completed by <strong>{$due_dates}</strong> to leave sufficient time for Accounting to review and apply the changes to these customers' invoices. </p> <p> The following link has been preset to filter customer accounts by your floor(s),<br /> {$link} </p>";
				$text = "This notice is to remind you to conduct a review of the customer accounts on your floor(s) and to indicate any changes to their service or applicable fees by adding a billing memo.\n\n Your review must be completed by {$due_dates} to leave sufficient time for Accounting to review and apply the changes to these customers' invoices.\n\n The following link has been preset to filter customer accounts on your floor(s),\n\n '{$link}'";
			}
			// NON-OM employee
			else {

				// build link
				$link = Router::url(array('controller' => 'customerAccounts', 'action' => 'billing'), true);

				// define due date
				$due_date = date('m/20'); // 20th of the month

				// email body
				$html = "<p>Hi {$first_name},</p> <p> This notice is to remind you to conduct a review of customer accounts, and to indicate any changes to their service or applicable fees by adding a billing memo.<br /> Your review must be completed by <strong>{$due_date}</strong> to leave sufficient time for Accounting to review and apply the changes to these customers' invoices.<br />{$link} </p>";
				$text = "This notice is to remind you to conduct a review of customer accounts, and to indicate any applicable fees or changes to their service by adding a billing memo.\n\n Your review must be completed by {$due_date} to leave sufficient time for Accounting to review and apply the changes to these customers' invoices.\n\n {$link}";
			}

			// build email message
			$email = array(
				'to' => array(
					array(
						'email' => $employee['Employee']['email'],
						'name' => $employee['Employee']['full_name'],
						'type' => 'to'
					)
				),
				'subject' => 'Monthly Billing Change Reviews (Floor Sweep)', // FIXME
				'text' => $text,
				'html' => $html
			);
			
			// add to outgoing list
			$emails[] = $email;
		}

		$return = array();

		// override emails
		// $link = Router::url(array('controller' => 'customerAccounts', 'action' => 'billing'), true);
		// $html = "<p>Hi John,</p> <p> This notice is to remind you to conduct a review of the customer accounts on your floor(s) and to indicate any changes to their service or applicable fees by adding a billing memo.<br /> Your review must be completed by <strong>3/18</strong> to leave sufficient time for Accounting to review and apply the changes to these customers' invoices. </p> <p> The following link has been preset to filter customer accounts by your floor(s),<br /> {$link} </p>";
		// $text = "This notice is to remind you to conduct a review of the customer accounts on your floor(s) and to indicate any changes to their service or applicable fees by adding a billing memo.\n\n Your review must be completed by {$due_dates} to leave sufficient time for Accounting to review and apply the changes to these customers' invoices.\n\n The following link has been preset to filter customer accounts on your floor(s),\n\n <a href='{$link}'>Click Here to Login and Review</a>";
		// $emails = array(
		// 	'to' => array(
		// 		array(
		// 			'email' => 'jj@pith-studio.com',
		// 			'name' => 'John Jung',
		// 			'type' => 'to'
		// 		)
		// 	),
		// 	'subject' => 'Monthly Billing Change Reviews (Floor Sweep)', // FIXME
		// 	'text' => $text,
		// 	'html' => $html
		// );
		// $return = $emails;

		// send email
		if (REALLY_SEND_EMAIL === true) {
			foreach ($emails as $email) {

				$sent = $this->Employee->sendEmail($email);
				array_push($return, $sent);
			}
		}
		$return = true;
		$this->set(compact('return'));
		$this->set('_serialize', array('return'));

	}



}

<?php
App::uses('AppController', 'Controller');
/**
 * FaxIns Controller
 *
 * @property FaxIn $FaxIn
 */
class FaxInsController extends AppController {

	public $paginate = array(
		'limit' => '50',
		'order' => 'FaxIn.creation_dt DESC',
	);

		public $uses = array('FaxIn');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FaxIn->recursive = 0;
		$this->paginate['conditions'] = array(
			'FaxIn.fax_number' => PUBLIC_FAX_NUMBER,
			'OR' => array(
				'FaxIn.account_id' => null, 
				'FaxIn.person_id' => null
			)
		);
		$faxIns = $this->paginate();
		$this->set('faxIns', $faxIns);
	}


/**
 * admin method
 *
 * @return void
 */
	public function admin() {
		$this->FaxIn->recursive = 0;
		$this->paginate['conditions'] = array(
			'OR' => array(
				'FaxIn.account_id' => null, 
				'FaxIn.person_id' => null
			)
		);

		$faxIns = $this->paginate();
		$this->set('faxIns', $faxIns);
	}

   

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FaxIn->exists($id)) {
			throw new NotFoundException(__('Invalid fax in'));
		}
		$options = array('conditions' => array('FaxIn.' . $this->FaxIn->primaryKey => $id));
		$this->set('faxIn', $this->FaxIn->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FaxIn->create();
			if ($this->FaxIn->save($this->request->data)) {
				$this->Session->setFlash(__('The fax in has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax in could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->FaxIn->CustomerAccount->find('list');
		$customers = $this->FaxIn->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FaxIn->exists($id)) {
			throw new NotFoundException(__('Invalid fax in'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxIn->save($this->request->data)) {
				$this->Session->setFlash(__('The fax in has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax in could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxIn.' . $this->FaxIn->primaryKey => $id));
			$this->request->data = $this->FaxIn->find('first', $options);
		}
		$customerAccounts = $this->FaxIn->CustomerAccount->find('list');
		$customers = $this->FaxIn->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FaxIn->id = $id;
		if (!$this->FaxIn->exists()) {
			throw new NotFoundException(__('Invalid fax in'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxIn->delete()) {
			$this->Session->setFlash(__('Fax in deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax in was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * claim method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
		public function claim($id = null) {
			
			if (!$this->FaxIn->exists($id)) {
					throw new NotFoundException(__('File wasn\'t found'));
			}

			$this->FaxIn->id = $id;
			$data = array(
					'account_id' => 1,
					'person_id' => $this->Auth->user('id')
			);
			if ($this->FaxIn->save($data)) {
				$this->Alert->success("Your file has been claimed");
			} else {
				$this->Session->setFlash(__('Your file has not been claimed', ''));
			}
			$this->redirect(array('action' => 'index'));
		}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FaxIn->recursive = 0;
		$this->set('faxIns', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FaxIn->exists($id)) {
			throw new NotFoundException(__('Invalid fax in'));
		}
		$options = array('conditions' => array('FaxIn.' . $this->FaxIn->primaryKey => $id));
		$this->set('faxIn', $this->FaxIn->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FaxIn->create();
			if ($this->FaxIn->save($this->request->data)) {
				$this->Session->setFlash(__('The fax in has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax in could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->FaxIn->CustomerAccount->find('list');
		$customers = $this->FaxIn->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FaxIn->exists($id)) {
			throw new NotFoundException(__('Invalid fax in'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxIn->save($this->request->data)) {
				$this->Session->setFlash(__('The fax in has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax in could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxIn.' . $this->FaxIn->primaryKey => $id));
			$this->request->data = $this->FaxIn->find('first', $options);
		}
		$customerAccounts = $this->FaxIn->CustomerAccount->find('list');
		$customers = $this->FaxIn->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FaxIn->id = $id;
		if (!$this->FaxIn->exists()) {
			throw new NotFoundException(__('Invalid fax in'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxIn->delete()) {
			$this->Session->setFlash(__('Fax in deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax in was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

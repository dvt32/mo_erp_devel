<?php
App::uses('AppController', 'Controller');
/**
 * FaxMachines Controller
 *
 * @property FaxMachine $FaxMachine
 */
class FaxMachinesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FaxMachine->recursive = 0;
		$this->set('faxMachines', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FaxMachine->exists($id)) {
			throw new NotFoundException(__('Invalid fax machine'));
		}
		$options = array('conditions' => array('FaxMachine.' . $this->FaxMachine->primaryKey => $id));
		$this->set('faxMachine', $this->FaxMachine->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FaxMachine->create();
			if ($this->FaxMachine->save($this->request->data)) {
				$this->Session->setFlash(__('The fax machine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax machine could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->FaxMachine->CustomerAccount->find('list');
		$customers = $this->FaxMachine->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FaxMachine->exists($id)) {
			throw new NotFoundException(__('Invalid fax machine'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxMachine->save($this->request->data)) {
				$this->Session->setFlash(__('The fax machine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax machine could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxMachine.' . $this->FaxMachine->primaryKey => $id));
			$this->request->data = $this->FaxMachine->find('first', $options);
		}
		$customerAccounts = $this->FaxMachine->CustomerAccount->find('list');
		$customers = $this->FaxMachine->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FaxMachine->id = $id;
		if (!$this->FaxMachine->exists()) {
			throw new NotFoundException(__('Invalid fax machine'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxMachine->delete()) {
			$this->Session->setFlash(__('Fax machine deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax machine was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FaxMachine->recursive = 0;
		$this->set('faxMachines', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FaxMachine->exists($id)) {
			throw new NotFoundException(__('Invalid fax machine'));
		}
		$options = array('conditions' => array('FaxMachine.' . $this->FaxMachine->primaryKey => $id));
		$this->set('faxMachine', $this->FaxMachine->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FaxMachine->create();
			if ($this->FaxMachine->save($this->request->data)) {
				$this->Session->setFlash(__('The fax machine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax machine could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->FaxMachine->CustomerAccount->find('list');
		$customers = $this->FaxMachine->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FaxMachine->exists($id)) {
			throw new NotFoundException(__('Invalid fax machine'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxMachine->save($this->request->data)) {
				$this->Session->setFlash(__('The fax machine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax machine could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxMachine.' . $this->FaxMachine->primaryKey => $id));
			$this->request->data = $this->FaxMachine->find('first', $options);
		}
		$customerAccounts = $this->FaxMachine->CustomerAccount->find('list');
		$customers = $this->FaxMachine->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FaxMachine->id = $id;
		if (!$this->FaxMachine->exists()) {
			throw new NotFoundException(__('Invalid fax machine'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxMachine->delete()) {
			$this->Session->setFlash(__('Fax machine deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax machine was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

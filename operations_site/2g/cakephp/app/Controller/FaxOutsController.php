<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');

/**
 * FaxOuts Controller
 *
 * @property FaxOut $FaxOut
 */
class FaxOutsController extends AppController {

	public $uses = array('FaxOut', 'Employee', 'CustomerAccount', 'FaxNumber', 'MoFile', 'User', 'Customer');

	public $paginate = array(
		'limit' => '50',
		'order' => 'FaxOut.creation_dt DESC',
	);



/**
 * status method
 * @author John Jung
 */
	public function status() {

		$faxage = $this->FaxOut->faxageStatus();

		// sort by sendtime
		usort($faxage, function($a, $b) {
		   return strcmp($b['Faxage']['sendtime'], $a['Faxage']['sendtime']);
		});
		
		$this->set(compact('faxage'));

	}



/**
 * history method
 * @author John Jung
 * @param $cust_acct_id from AuthComponent
 * @return void
 */
	public function history() {

		// refresh flag will refresh the page, but redirect loop problem
		$refresh = false;

		$this->Paginator->settings = array(
			'FaxOut' => array(
				'limit' => '10',
				'order' => 'FaxOut.creation_dt DESC'
			)
		);

		$faxOuts = $this->Paginator->paginate();

		foreach ($faxOuts as $key => $value) {

			$jobstatus = $this->FaxOut->updateJob($value['FaxOut']['job_id']);

			// if toSave is true, update the db
			$toSave = 0;

			if ($value['FaxOut']['comm_id'] != $jobstatus['Faxage']['comm_id']) { 
				$value['FaxOut']['comm_id'] = $jobstatus['Faxage']['comm_id'];
				$toSave = 1;
			}
			if ($value['FaxOut']['short_status'] != $jobstatus['Faxage']['shortstatus']) { 
				$value['FaxOut']['short_status'] = $jobstatus['Faxage']['shortstatus'];
				$toSave = 1;
			}
			if ($value['FaxOut']['long_status'] != $jobstatus['Faxage']['longstatus']) { 
				$value['FaxOut']['long_status'] = $jobstatus['Faxage']['longstatus'];
				$toSave = 1;
			}
			if ($value['FaxOut']['complete_time'] != $jobstatus['Faxage']['completetime']) { 
				$value['FaxOut']['complete_time'] = $jobstatus['Faxage']['completetime'];
				$toSave = 1;
			}
			if ($value['FaxOut']['xmit_time'] != $jobstatus['Faxage']['xmittime']) { 
				$value['FaxOut']['xmit_time'] = $jobstatus['Faxage']['xmittime'];
				$toSave = 1;
			}
			if ($value['FaxOut']['page_count'] != $jobstatus['Faxage']['pagecount']) { 
				$value['FaxOut']['page_count'] = $jobstatus['Faxage']['pagecount'];
				$toSave = 1;
			}

			// if toSave is true, resave it
			if ($toSave === 1) {
				$value['FaxOut']['complete_time'] = null;
				$value['FaxOut']['xmit_time'] = null;
				if ($this->FaxOut->save($value)) {
					$refresh = true;
				} else {
					$this->Session->setFlash(__('Fax Status Updates failed'));
				}
			}

		}

		// causes redirect loop
		// if ($refresh === true) {
		// 	$this->redirect(array('action' => 'history'));
		// }

		$this->set('faxOuts', $faxOuts);
	}




/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FaxOut->recursive = 0;
		$this->set('faxOuts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FaxOut->exists($id)) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		$options = array('conditions' => array('FaxOut.' . $this->FaxOut->primaryKey => $id));
		$this->set('faxOut', $this->FaxOut->find('first', $options));
	}


/**
 * add method
 *
 * @return void
 */
	public function add($file_id = null) {
		
		$auth_email = AuthComponent::user('username');

		if ($this->request->is('post')) {

			$faxing = $this->request->data;

			$faxnumber = $faxing['FaxOut']['callerid'];
			$files = $faxing['FaxOut']['file'];

			// Debug array shown
			// array(
			// 	'FaxOut' => array(
			// 		'recipient_name' => 'John test',
			// 		'fax_number' => '6462531258',
			// 		'send_email_alert' => '0',
			// 		'short_status' => 'pending',
			// 		'file' => array(
			// 			(int) 0 => array(
			// 				'name' => 'Ops.jpg',
			// 				'type' => 'image/jpeg',
			// 				'tmp_name' => '/Applications/MAMP/tmp/php/phph6S850',
			// 				'error' => (int) 0,
			// 				'size' => (int) 2097710
			// 			),
			// 			(int) 1 => array(
			// 				'name' => 'Ops.pdf',
			// 				'type' => 'application/pdf',
			// 				'tmp_name' => '/Applications/MAMP/tmp/php/phpJi1CE2',
			// 				'error' => (int) 0,
			// 				'size' => (int) 8784996
			// 			),
			// 			(int) 2 => array(
			// 				'name' => '',
			// 				'type' => '',
			// 				'tmp_name' => '',
			// 				'error' => (int) 4,
			// 				'size' => (int) 0
			// 			)
			// 		)
			// 	)
			// )

			// param array
			$param = array();

			$param['faxno'] = $faxing['FaxOut']['fax_number'];
			$param['callerid'] = $faxnumber;
			$param['recipname'] = $faxing['FaxOut']['recipient_name'];;
			$param['tagname'] = 'MicroOffice Fax';
			$param['tagnumber'] = $faxnumber;
			$param['faxfiledata'] = array();
			$param['faxfilenames'] = array();

			// Handling the filedata
			if ($files[0]['size'] == 0 && $files[1]['size'] == 0 && $files[2]['size'] == 0) {

				// No file was attached
				$this->Session->setFlash(__('The fax could not be sent. No file was attached. '));

			} else {

				// Handle all files
				foreach ($files as $attachment) {

					// attachment exists
					if ($attachment['size'] != 0) {

						$filetmpname = $attachment['tmp_name'];
						$fh = fopen($filetmpname, "r");
						$fdata = fread($fh, filesize($filetmpname));
						fclose($fh);
						$b64data = base64_encode($fdata);

						array_push($param['faxfiledata'], $b64data);
						array_push($param['faxfilenames'], $attachment['name']);

					}

					// Upload the file to the DAV Server
					// $data = array(
					// 	'filename' => $fileInfo['name'],
					// 	'mime_type' => $fileInfo['type'],
					// 	'namespace' => 'fax/out'
					// );
					// $file = $this->MoFile->save($data);
					// upload_file(DAV_SERVER_HOST.pathFromId($file['MoFile']['id']), $fileInfo);
					// $this->request->data['FaxOut']['file_id'] = $file['MoFile']['id'];
				
				}

				// Faxage API Call
				$response = $this->FaxOut->faxageSend($param);

				$jobid = null;

				// gets the jobID
				preg_match('/[0-9]+/' ,$response->body, $jobid);

				$this->request->data['FaxOut']['job_id'] = $jobid[0];
				$this->request->data['FaxOut']['send_time'] = $response->headers['Date'];


				// SEND EMAIL START
				if ($this->request->data['FaxOut']['send_email_alert'] == 1) {
					
					// send email to debug email
					$Email = new CakeEmail();
					$Email->config('default');
					$Email->from(
						array('service@microoffice.com' => 'Fax Genie'))
					    ->to($auth_email)
					    ->subject('Your Fax is Processing!')
				    	->send("The fax Job ID is ".$this->request->data['FaxOut']['job_id']);
				}
				// SEND EMAIL END				

				$this->FaxOut->create();
				$this->request->data['FaxOut']['person_id'] = '1526';
				$this->request->data['FaxOut']['account_id'] = '1526';
				$this->request->data['FaxOut']['complete_time'] = null;
				$this->request->data['FaxOut']['xmit_time'] = null;

				if ($this->FaxOut->save($this->request->data)) {

					$this->Session->setFlash(__('The fax has been sent'));
					$this->redirect(array('action' => 'history'));

				} else {
					$this->Session->setFlash(__('The fax could not be sent. Please, try again or contact Micro Office for assistance.'));
				}

			}
			
		}

		// If a file id gets passed
		// $fid = $this->FaxOut->find('first', array('conditions' => array('FaxOut.file_id' => $file_id)));

	}



/**
 * faxageResponse method - faxage callback url
 *
 * @throws NotFoundException
 * @author John Jung
 * @param post jobid=105915248&commid=380384&destname=Test&destnum=(646)253-1258&shortstatus=success&longstatus=Success&sendtime=2013-08-09 21:07:16&completetime=2013-08-09 21:09:35&xmittime=00:01:52&pagecount=1&xmitpages=1
 * @return void
 */
	public function faxageResponse() {

		$this->autoRender = false;
		if($_SERVER['REQUEST_METHOD']=='POST'){

	    	if(!empty($_POST)){
				// receive the post, fit it into CakePHP array
				$faxes = file_get_contents('php://input');
				$faxes = urldecode($faxes);
				$faxdata['FaxOut'] = array();
				foreach (explode('&', $faxes) as $chunk) {
				    $param = explode("=", $chunk);

				    if ($param) {
				    	$faxdata['FaxOut'][urldecode($param[0])] = urldecode($param[1]);
				    }
				}
				// Find the fax by the job_id
				$faxjob = $this->FaxOut->find('first',
					array('conditions' => array('FaxOut.job_id' => $faxdata['FaxOut']['jobid']),
						'recursive' => -1
				));
				$isEmail = $faxjob['FaxOut']['send_email_alert'];

				// update the arrays
				$faxjob['FaxOut']['comm_id'] = $faxdata['FaxOut']['commid'];
				$faxjob['FaxOut']['short_status'] = $faxdata['FaxOut']['shortstatus'];
				$faxjob['FaxOut']['long_status'] = $faxdata['FaxOut']['longstatus'];
				$faxjob['FaxOut']['send_time'] = $faxdata['FaxOut']['sendtime'];
				$faxjob['FaxOut']['complete_time'] = $faxdata['FaxOut']['completetime'];
				$faxjob['FaxOut']['xmit_time'] = $faxdata['FaxOut']['xmittime'];
				$faxjob['FaxOut']['page_count'] = $faxdata['FaxOut']['pagecount'];

				// $this->FaxOut->set($faxjob);
				// debug($this->FaxOut->validates());
				debug($faxjob); die;

				// save it
				if ($this->FaxOut->save($faxjob)) {

					// If the email flag is true
					if ($isEmail) {
						// send email to person
						$Email = new CakeEmail();
						$Email->config('default');
						$Email->from(
							array('service@microoffice.com' => 'Micro Office Fax Genie'))
						    ->to(AuthComponent::user('username'))
						    ->subject('Your Fax Has Been Sent!')
					    	->send('Yay!');
					}
					return true;


				} else {
					// debug($faxjob); die;
					return false;
					// send email to tech support that fax didn't send
					$Email = new CakeEmail();
					$Email->config('default');
					$Email->from(
						array('hello@pith-studio.com' => 'Micro Office Fax Genie'))
					    ->to(EMAIL_FAXERROR)
					    ->subject('Boo, fax didn\'t save properly and something is terribly wrong.')
				    	->send(file_get_contents('php://input'));

				}
			}
		}
	}


/**
 * get a new number
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function getaNumber() {

		// non-admin tenants cannot order a fax number
		if ($this->Auth->user('role') < ROLE_ADMIN) {
			$this->Alert->error('Sorry, please contact your admin or primary account holder to order a private fax');
			$this->redirect(array('action' => 'add'));
		}

		$email = true;
		$caid = AuthComponent::user('cust_acct_id');
		$cpid = AuthComponent::user('cust_pers_id');

		$account = $this->CustomerAccount->find('first', array('conditions' => array('CustomerAccount.id' => $caid)));

		// send email to person
		$Email = new CakeEmail();
		$Email->config('default');
		$Email->from(
			array('hello@pith-studio.com' => 'Micro Office Fax Genie'))
		    ->to(EMAIL_NEWFAXNUMBER)
		    ->subject('Someone has requested a fax Number')
	    	->send(
	    		"The person below has requested a new private fax number"."\r\n".
	    		"Company: ".$account['CustomerAccount']['name']."\r\n".
			    "Primary Contact: ".$account['PrimaryCustomer']['first_name']." ".$account['PrimaryCustomer']['last_name']."\r\n".
			    "Email: ".$account['PrimaryCustomer']['email']);


		if ($email) {
			$this->Session->setFlash(__('Your fax number will be sent to you via email '));
		} else {
			$this->Session->setFlash(__('Please Contact Micro Office for assistance '));
		}
		$this->redirect(array('action' => 'add'));
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FaxOut->exists($id)) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FaxOut->save($this->request->data)) {
				$this->Session->setFlash(__('The fax out has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fax out could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FaxOut.' . $this->FaxOut->primaryKey => $id));
			$this->request->data = $this->FaxOut->find('first', $options);
		}
		$customerAccounts = $this->FaxOut->CustomerAccount->find('list');
		$customers = $this->FaxOut->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FaxOut->id = $id;
		if (!$this->FaxOut->exists()) {
			throw new NotFoundException(__('Invalid fax out'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FaxOut->delete()) {
			$this->Session->setFlash(__('Fax out deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Fax out was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}

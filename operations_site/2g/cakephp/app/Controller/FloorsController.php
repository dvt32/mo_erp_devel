<?php
App::uses('AppController', 'Controller');
App::uses('CakeNumber', 'Utility'); 

/**
 * Floors Controller
 *
 * @property Floor $Floor
 * @property PaginatorComponent $Paginator
 */
class FloorsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {

		// if filter params are set...
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query);

			// workaround for datatype numbers only
			$conditions['OR'][]['Building.address_1 ILIKE'] = "%{$this->request->query['q']}%";

			// set form input values to passed params
			$this->request->data['Floor'] = $this->request->query;
		}

		// Dropdown list
		$buildings = $this->Floor->Building->find('list', array(
			'conditions' => array(
				'is_active' => true, 
				'type' => 'officesuite'
			)
		));

		$this->Floor->order = 'Floor.creation_dt DESC';
		$this->Floor->recursive = 0;
		$this->Paginator->settings = array('Floor' => compact('conditions','contain','order'));

		$floors = $this->Paginator->paginate();
		$this->set(compact('floors', 'types', 'isActive', 'buildings'));

	}

/**
 * building method
 *
 * @return void
 */
	public function building($id = null) {
		
		$this->paginate = array(
			'conditions' => array(
				'Floor.building_id' => $id,
				'is_active' => true
			)
		);

		$this->set('floors', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		if (!$this->Floor->exists($id)) {
			throw new NotFoundException(__('Invalid floor'));
		}

		$options = array(
			'conditions' => array(
				'Floor.' . $this->Floor->primaryKey => $id,
				'Floor.is_active' => 1
			),
			'contain' => array(
				'Desk' => array(
					'CustomerAccount.id',
					'CustomerAccount.name'
				), 
				'OfficeManager', 
				'Building.id', 
				'Building.address_1',
				'Building.city'
			),
		);
		$floor = $this->Floor->find('first', $options);
		// debug($floor);
		// debug($floor['Floor']['bounding_polygon']);

		// parsed Floor bounding polygon
		$floor_bounds = $this->boundingPolyToArray($floor['Floor']['bounding_polygon']);
		// debug($floor_bounds);
		$floor['Floor']['floor_bounds'] = $floor_bounds;
		// exit;

		// get bounding coordingates of all coordinates
		$coords = $this->coordsToArray($floor['Desk']);
		$boundingCoords = $this->bounding_box($coords);
		// debug($boundingCoords); exit;
		
		// // Determing relative aspect ratios
		$fm_height = $floor['Floor']['map_height'];
		$fm_width = $floor['Floor']['map_width'];
		// Aspect Ratios
		$fm_aspectRatio = $fm_width/$fm_height;

		// Determine scaling factor
		$floor_plan_width = $floor['Floor']['floor_bounds'][1][0];
		$floor_plan_height = $floor['Floor']['floor_bounds'][2][1];
		$scaling_factor = $fm_width/$floor_plan_width;

		// Resize based on scaling factor		
		foreach ($floor['Desk'] as $key => $desk) {

			// currency conversions
			$desk['list_price'] = CakeNumber::currency($desk['list_price']);
			$desk['current_price'] = CakeNumber::currency($desk['current_price']);
			$desk['internet_telephone_revenue'] = CakeNumber::currency($desk['internet_telephone_revenue']);

			// dissassembly
			$before = $desk['bounding_polygon'];
			$before = explode(',',$before);

			// removes "( & )"
			array_walk($before, function(&$c) {
				$c = str_replace('(','',$c);
				$c = str_replace(')','',$c);
			});

			// breaks x and y into pairs
			$pairs = array();
			for ($i = 0; $i < count($before); $i+=2) {
				$pairs[] = array($before[$i],$before[$i+1]);
			}

			// Scaling and transformations
			$after = array();
			foreach ($pairs as $pair) {
				
				$after[] = $x = ($pair[0]*$scaling_factor);
				// transformation from 0,0 lower left to upper left
				$after[] = $y = (($floor_plan_height - $pair[1]) * $scaling_factor);
			}
			

			// reassembly
			$after = implode(',',$after);
			$floor['Desk'][$key]['areas'] = $after;
		}
		// financial calculations
		$financial = array(
			'total_current_revenue' => 0,
			'potential_revenue' => 0,
			'revenue_lost' => 0,
			'internet_phone_revenue' => 0,
			'new_business_30' => 0,
			'new_business_60' => 0,
			'new_business_this' => 0,
			'new_business_last' => 0,

		);

		$now = date(DATETIME_FORMAT);
		$thirty = date(DATETIME_FORMAT,strtotime('-30 days'));
		$sixty = date(DATETIME_FORMAT,strtotime('-60 days'));
		$firstofthis = date(DATETIME_FORMAT,strtotime(date('m/01/y')));
		$firstoflast = date(DATETIME_FORMAT,strtotime('-1 month',strtotime(date('m/01/y'))));

		// New Business in the last 30 days
		foreach ($floor['Desk'] as $key => $value) {

			// Total current revenue
			if ($value['is_active'] == true && isset($value['account_id'])) {
				$financial['total_current_revenue'] = $financial['total_current_revenue'] + $value['current_price'];
			}

			// Total potential revenue
			$financial['potential_revenue'] = $financial['potential_revenue'] + $value['list_price'];

			// Total internet/phone revenue
			$financial['internet_phone_revenue'] = $financial['internet_phone_revenue'] + $value['internet_telephone_revenue'];

			// Revenue within last 30 days
			if ($thirty <= $value['start_date'] && $value['start_date'] <= $now) {
				if ($value['is_active'] == true && isset($value['account_id'])) {
					$financial['new_business_30'] = $financial['new_business_30'] + $value['current_price'];
				}
			}
			// Revenue within last 60 days
			if ($sixty <= $value['start_date'] && $value['start_date'] <= $now) {
				if ($value['is_active'] == true && isset($value['account_id'])) {
					$financial['new_business_60'] = $financial['new_business_60'] + $value['current_price'];
				}
			}
			// Revenue since first of this month
			if ($firstofthis <= $value['start_date'] && $value['start_date'] <= $now) {
				if ($value['is_active'] == true && isset($value['account_id'])) {
					$financial['new_business_this'] = $financial['new_business_this'] + $value['current_price'];
				}
			}
			// Revenue since the first of last month
			if ($firstoflast <= $value['start_date'] && $value['start_date'] <= $now) {
				if ($value['is_active'] == true && isset($value['account_id'])) {
					$financial['new_business_last'] = $financial['new_business_last'] + $value['current_price'];
				}
			}


		}
		// Revenue Lost
		$financial['revenue_lost'] = $financial['potential_revenue'] - $financial['total_current_revenue'];

		// Convert to currency
		foreach ($financial as $key => $value) {
			$financial[$key] = CakeNumber::currency($value);
		}

		
		$this->set(compact('floor', 'financial'));

	}

/**
 * bounding_box
 * 
 * @param coordsToArray array
 * @return array(array(x1,y1), array(x2,y2))
 */

	private function bounding_box($coordsToArray = array()) {

		$boundingArray = array(
			'x1' => 0, // Left
			'y1' => 0, // Bottom
			'x2' => null, // Right
			'y2' => null  // Top
		);

		// sets first within the entire array
		// $boundingArray['x1'] = $coordsToArray[0][0][0];
		// $boundingArray['y1'] = $coordsToArray[0][0][1];
		$boundingArray['x2'] = $coordsToArray[0][2][0];
		$boundingArray['y2'] = $coordsToArray[0][2][1];

		foreach ($coordsToArray as $key => $value) {

			if ($boundingArray['x1'] > $value[0][0]) { 
				$boundingArray['x1'] = $value[0][0];
			}
			if ($boundingArray['y1'] > $value[0][1]) { 
				$boundingArray['y1'] = $value[0][1];
			}
			if ($boundingArray['x2'] < $value[2][0]) { 
				$boundingArray['x2'] = $value[2][0];
			}
			if ($boundingArray['y2'] < $value[2][1]) { 
				$boundingArray['y2'] = $value[2][1];
			}

		}

		return $boundingArray;

	}

/**
 * coords to pairs array
 *
 * @param array of '[Floor']['Desk']
 * @return array of pairs or coordinates
 */
	private function coordsToArray($floor_desk = array()) {


		$pairs_array = array();
		// spits out pairs of coords in array
		foreach ($floor_desk as $key => $desk) {

			$before = $desk['bounding_polygon'];
			$before = explode(',',$before);

			// removes "( & )"
			array_walk($before, function(&$c) {
				$c = str_replace('(','',$c);
				$c = str_replace(')','',$c);
			});

			// breaks x and y into pairs
			$pairs = array();
			for ($i = 0; $i < count($before); $i+=2) {
				$pairs[] = array($before[$i],$before[$i+1]);
			}

			array_push($pairs_array, $pairs);
		}

		return $pairs_array;

	}

/**
 * bounding poly to array
 *
 * 
 */
	private function boundingPolyToArray($bounding_polygon) {

			$before = $bounding_polygon;
			$before = explode(',',$before);

			// removes "( & )"
			array_walk($before, function(&$c) {
				$c = str_replace('(','',$c);
				$c = str_replace(')','',$c);
			});

			// breaks x and y into pairs
			$pairs = array();
			for ($i = 0; $i < count($before); $i+=2) {
				$pairs[] = array($before[$i],$before[$i+1]);
			}

			return $pairs;
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Floor->create();
			if ($this->Floor->save($this->request->data)) {
				$this->Session->setFlash(__('The floor has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The floor could not be saved. Please, try again.'));
			}
		}
		$buildings = $this->Floor->Building->find('list');
		$employees = $this->Floor->OfficeManager->find('list');
		$this->set(compact('buildings', 'employees'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Floor->exists($id)) {
			throw new NotFoundException(__('Invalid floor'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Floor->save($this->request->data)) {
				$this->Session->setFlash(__('The floor has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The floor could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Floor.' . $this->Floor->primaryKey => $id));
			$this->request->data = $this->Floor->find('first', $options);
		}
		$buildings = $this->Floor->Building->find('list');
		$employees = $this->Floor->Employee->find('list');
		$this->set(compact('buildings', 'employees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Floor->id = $id;
		if (!$this->Floor->exists()) {
			throw new NotFoundException(__('Invalid floor'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Floor->delete()) {
			$this->Session->setFlash(__('The floor has been deleted.'));
		} else {
			$this->Session->setFlash(__('The floor could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

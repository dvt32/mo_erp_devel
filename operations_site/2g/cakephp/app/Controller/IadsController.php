<?php
App::uses('AppController', 'Controller');
/**
 * Iads Controller
 *
 * @property Iad $Iad
 */
class IadsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Iad->recursive = 0;
		$this->set('iads', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Iad->id = $id;
		if (!$this->Iad->exists()) {
			throw new NotFoundException(__('Invalid iad'));
		}
		$this->set('iad', $this->Iad->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Iad->create();
			if ($this->Iad->save($this->request->data)) {
				$this->Session->setFlash(__('The iad has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The iad could not be saved. Please, try again.'));
			}
		}
		$floors = $this->Iad->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Iad->id = $id;
		if (!$this->Iad->exists()) {
			throw new NotFoundException(__('Invalid iad'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Iad->save($this->request->data)) {
				$this->Session->setFlash(__('The iad has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The iad could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Iad->read(null, $id);
		}
		$floors = $this->Iad->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Iad->id = $id;
		if (!$this->Iad->exists()) {
			throw new NotFoundException(__('Invalid iad'));
		}
		if ($this->Iad->delete()) {
			$this->Session->setFlash(__('Iad deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Iad was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

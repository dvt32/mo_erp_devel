<?php
App::uses('AppController', 'Controller');
/**
 * IntellimGroups Controller
 *
 * @property IntellimGroup $IntellimGroup
 * @property PaginatorComponent $Paginator
 */
class IntellimGroupsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->IntellimGroup->recursive = 0;
		$this->set('intellimGroups', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->IntellimGroup->exists($id)) {
			throw new NotFoundException(__('Invalid intellim group'));
		}
		$options = array('conditions' => array('IntellimGroup.' . $this->IntellimGroup->primaryKey => $id));
		$this->set('intellimGroup', $this->IntellimGroup->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->IntellimGroup->create();
			if ($this->IntellimGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The intellim group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The intellim group could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->IntellimGroup->exists($id)) {
			throw new NotFoundException(__('Invalid intellim group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->IntellimGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The intellim group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The intellim group could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('IntellimGroup.' . $this->IntellimGroup->primaryKey => $id));
			$this->request->data = $this->IntellimGroup->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->IntellimGroup->id = $id;
		if (!$this->IntellimGroup->exists()) {
			throw new NotFoundException(__('Invalid intellim group'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->IntellimGroup->delete()) {
			$this->Session->setFlash(__('The intellim group has been deleted.'));
		} else {
			$this->Session->setFlash(__('The intellim group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

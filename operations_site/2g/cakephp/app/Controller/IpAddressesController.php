<?php
App::uses('AppController', 'Controller');
/**
 * IpAddresses Controller
 *
 * @property IpAddress $IpAddress
 * @property PaginatorComponent $Paginator
 */
class IpAddressesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		// if filter params are set...
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query);

			$querystring = @$this->request->query['q'];

			// workaround for datatype numbers only
			$conditions['OR'][]['IpAddress.ip_address ILIKE'] = 
			$conditions['OR'][]['Isp.name ILIKE'] = 
			$conditions['OR'][]['Building.address_1 ILIKE'] = "%{$querystring}%";

			// set form input values to passed params
			$this->request->data['IpAddress'] = $this->request->query;
		}

		// Dropdown list
		$buildings = $this->IpAddress->Building->find('list');
		$isps = $this->IpAddress->Isp->find('list');
		$limit = @$this->request->query['limit'] ?: 50;

		$this->IpAddress->order = 'IpAddress.creation_dt DESC';
		$this->IpAddress->recursive = 0;
		$this->Paginator->settings = array('IpAddress' => compact('conditions','contain','order','limit'));

		$ipAddresses = $this->Paginator->paginate();
		$this->set(compact('ipAddresses', 'buildings','isps'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->IpAddress->exists($id)) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		$options = array(
			'conditions' => array('IpAddress.' . $this->IpAddress->primaryKey => $id),
			'recursive' => 1
		);
		$this->set('ipAddress', $this->IpAddress->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->IpAddress->create();
			if ($this->IpAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The ip address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip address could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->IpAddress->CustomerAccount->find('list');
		$buildings = $this->IpAddress->Building->find('list');
		$this->set(compact('customerAccounts', 'buildings'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->IpAddress->exists($id)) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->IpAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The ip address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip address could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('IpAddress.' . $this->IpAddress->primaryKey => $id));
			$this->request->data = $this->IpAddress->find('first', $options);
		}
		$customerAccounts = $this->IpAddress->CustomerAccount->find('list');
		$buildings = $this->IpAddress->Building->find('list');
		$isps = $this->IpAddress->Isp->find('list');
		$this->set(compact('customerAccounts', 'buildings', 'isps'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->IpAddress->id = $id;
		if (!$this->IpAddress->exists()) {
			throw new NotFoundException(__('Invalid ip address'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->IpAddress->delete()) {
			$this->Session->setFlash(__('The ip address has been deleted.'));
		} else {
			$this->Session->setFlash(__('The ip address could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

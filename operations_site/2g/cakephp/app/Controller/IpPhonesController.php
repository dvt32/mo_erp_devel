<?php
App::uses('AppController', 'Controller');
/**
 * IpPhones Controller
 *
 * @property IpPhone $IpPhone
 */
class IpPhonesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		// if filter params are set...
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query);

			$querystring = @$this->request->query['q'];

			// workaround for datatype numbers only
			$conditions['OR'][]['CAST(IpPhone.model AS TEXT) LIKE'] = 
			$conditions['OR'][]['IpPhone.mac_address ILIKE'] = 
			$conditions['OR'][]['IpPhone.note ILIKE'] = "%{$querystring}%";

			// set form input values to passed params
			$this->request->data['IpPhone'] = $this->request->query;
		}

		// Dropdown list
		$manufacturers = $this->IpPhone->manufacturers;
		$models = $this->IpPhone->models;
		$ownerships = $this->IpPhone->ownerships;

		$limit = @$this->request->query['limit'] ?: 50;
		$order = 'IpPhone.creation_dt DESC';
		$this->IpPhone->recursive = 0;
		$this->Paginator->settings = array('IpPhone' => compact('conditions','contain','order','limit'));

		$ipPhones = $this->Paginator->paginate();
		$this->set(compact('ipPhones', 'manufacturers', 'ownerships', 'models'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone '));
		}
		$this->set('ipPhone', $this->IpPhone->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->IpPhone->create();
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone  has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone  could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone '));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone  has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone  could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->IpPhone->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone '));
		}
		if ($this->IpPhone->delete()) {
			$this->Session->setFlash(__('Ip phone  deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ip phone  was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->IpPhone->recursive = 0;
		$this->set('ipPhones', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone '));
		}
		$this->set('ipPhone', $this->IpPhone->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->IpPhone->create();
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone  has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone  could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone '));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->IpPhone->save($this->request->data)) {
				$this->Session->setFlash(__('The ip phone  has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ip phone  could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->IpPhone->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->IpPhone->id = $id;
		if (!$this->IpPhone->exists()) {
			throw new NotFoundException(__('Invalid ip phone '));
		}
		if ($this->IpPhone->delete()) {
			$this->Session->setFlash(__('Ip phone  deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ip phone  was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Isps Controller
 *
 * @property Isp $Isp
 * @property PaginatorComponent $Paginator
 */
class IspsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Isp->recursive = 0;
		$this->set('isps', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Isp->exists($id)) {
			throw new NotFoundException(__('Invalid isp'));
		}
		$options = array('conditions' => array('Isp.' . $this->Isp->primaryKey => $id));
		$this->set('isp', $this->Isp->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Isp->create();
			if ($this->Isp->save($this->request->data)) {
				$this->Session->setFlash(__('The isp has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The isp could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Isp->exists($id)) {
			throw new NotFoundException(__('Invalid isp'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Isp->save($this->request->data)) {
				$this->Session->setFlash(__('The isp has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The isp could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Isp.' . $this->Isp->primaryKey => $id));
			$this->request->data = $this->Isp->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Isp->id = $id;
		if (!$this->Isp->exists()) {
			throw new NotFoundException(__('Invalid isp'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Isp->delete()) {
			$this->Session->setFlash(__('The isp has been deleted.'));
		} else {
			$this->Session->setFlash(__('The isp could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

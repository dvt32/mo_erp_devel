<?php
App::uses('AppController', 'Controller');
App::uses('CakePdf', 'CakePdf.Pdf');
/**
 * LicenseAgreementTemplates Controller
 *
 * @property LicenseAgreementTemplate $LicenseAgreementTemplate
 * @property PaginatorComponent $Paginator
 */
class LicenseAgreementTemplatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LicenseAgreementTemplate->recursive = 0;
		$this->set('licenseAgreementTemplates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LicenseAgreementTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement template'));
		}
		$licenseAgreementTemplate = $this->LicenseAgreementTemplate->find('first', array(
			'conditions' => array('LicenseAgreementTemplate.' . $this->LicenseAgreementTemplate->primaryKey => $id),
			'contain' => array(
				'Employee.id',
				'Employee.full_name',
				'Building.id',
				'Building.full_address',
			)
		));
		$this->set(compact('licenseAgreementTemplate','id'));
	}

	public function printOut($id = null) {
		if (!$this->LicenseAgreementTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement template'));
		}
		$licenseAgreementTemplate = $this->LicenseAgreementTemplate->find('first', array(
			'conditions' => array('LicenseAgreementTemplate.' . $this->LicenseAgreementTemplate->primaryKey => $id),
		));
		// outputs the html of the template
		echo '<!DOCTYPE html><html lang="en"><head><style>* {font-size:11px;}</style></head><body>';
		echo $licenseAgreementTemplate['LicenseAgreementTemplate']['template'];
		echo "</body></html>";
		exit;
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			// $this->uses[] = 'EmployeeRole';
			// $role = $this->EmployeeRole->find('list', array(
			// 	'conditions' => array('EmployeeRole.element_value' => 'role:license/edit_agreement_template')))
			// $this->Auth->user('id') = $role;

			$data = $this->request->data;

			$this->LicenseAgreementTemplate->create();
			if ($this->LicenseAgreementTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The license agreement template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				debug($this->LicenseAgreementTemplate->validationErrors);
				exit;
				$this->Session->setFlash(__('The license agreement template could not be saved. Please, try again.'));
			}
		}
		$this->uses[] = 'Building';
		$buildings = $this->Building->officeSuites();
		$this->set(compact('buildings'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LicenseAgreementTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LicenseAgreementTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The license agreement template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The license agreement template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LicenseAgreementTemplate.' . $this->LicenseAgreementTemplate->primaryKey => $id));
			$this->request->data = $this->LicenseAgreementTemplate->find('first', $options);
		}
		$buildings = $this->LicenseAgreementTemplate->Building->officeSuites();
		$this->set(compact('buildings', 'id'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LicenseAgreementTemplate->id = $id;
		if (!$this->LicenseAgreementTemplate->exists()) {
			throw new NotFoundException(__('Invalid license agreement template'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->LicenseAgreementTemplate->delete()) {
			$this->Session->setFlash(__('The license agreement template has been deleted.'));
		} else {
			$this->Session->setFlash(__('The license agreement template could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
/**
 * printTest method
 *
 * @param  string $id of LicenseAgreement
 * @return  void
 *
 * 
 */	
	
	

	public function printTest($id = null) {

        $this->pdfConfig = array(
        	'orientation' => 'portrait',
        	'options' => array(
	            'print-media-type' => false,
	            'outline' => true,
	            'dpi' => 96
	        ),
	        'margin' => array(
                'bottom' => 15,
                'left' => 50,
                'right' => 30,
                'top' => 45
            ),
        	'download' => false,
        	'paper' => 'A4',
        	'filename' => 'test.pdf'
        );

        $agreement = $this->LicenseAgreementTemplate->find('first', array(
        	'conditions' => array('LicenseAgreementTemplate.id' => $id)
        ));
        $agreement = $agreement['LicenseAgreementTemplate']['template'];

        // increase php memory
        ini_set('memory_limit', '32M');
	    $this->set(compact('agreement'));

        
    }

}

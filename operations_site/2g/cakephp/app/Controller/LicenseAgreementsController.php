<?php
App::uses('AppController', 'Controller');
/**
 * LicenseAgreements Controller
 *
 * @property LicenseAgreement $LicenseAgreement
 * @property PaginatorComponent $Paginator
 */
class LicenseAgreementsController extends AppController {


	/**
	 * Allow user to login before authentication
	 * 
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('apitests');
		}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// $conditions = array('LicenseAgreement.is_active' => true);
		$fields = array(
			'id',
			'company',
			'business_type',
			'creation_dt',
			'start_date',
			'end_date',
			'license_fee',
			'licensor_entity',
			'signing_entity',
			'account_id',
		);
		$order = 'LicenseAgreement.creation_dt DESC';
		$limit = 50;
		$this->Paginator->settings = compact('conditions','fields','contain','order','limit');
		$licenseAgreements = $this->Paginator->paginate();
		$this->set(compact('licenseAgreements'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LicenseAgreement->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement'));
		}
		$licenseAgreement = $this->LicenseAgreement->find('first', array(
			'conditions' => array('LicenseAgreement.id' => $id),
			'contain' => array(
				'Amendment' => array(
					'order' => 'Amendment.created ASC'
				),
				'Desk' => array(
					'fields' => array('id','assigned_number'),
					'Floor' => array(
						'fields' => array('id','short_global_label'),
						'Building' => array(
							'fields' => array('id','short_global_label')
						)
					)
				),

			),
			'fields' => array(
					'id',
					'license_date',
					'licensor_entity',
					'prospective_customer_id',
					'signing_entity',
					'company',
					'first_name',
					'last_name',
					'title',
					'address1',
					'address2',
					'city',
					'state',
					'zip',
					'phone',
					'email',
					'business_type',
					'desks',
					'offices',
					'phone_lines',
					'persons',
					'months',
					'start_date',
					'end_date',
					'license_fee',
					'security',
					'license_agreement_pdf',
					'ms_access_creation_dt',
					'ms_access_random_num',
					'creation_dt',
					'filename',
					'spacenumber',
					'phoneservice',
					'confroomhrs',
					'tax_number',
					'created',
					'modified',
					'modifier_id',
					'agreement_rtf_url',
					'agreement_pdf_url',
					'desk_id',
					'account_id',
					'person_id',
					'ds_envelopeID',
					'ds_json_envelope_assets',
					'ds_json_envelope_documents',
					'ds_json_audit_events',
					'ds_json_envelope_recipients',
					'ds_json_envelope',
					'floor_id',
					// CATHY added the following for Jerome's feature
					'desk_amount',
					'office_amount',
					'phone_amount',
					'internet_amount',
					'credit',
					'personal_guarantee'

				)
		));
		$this->set(compact('licenseAgreement'));
	}

	public function printOut($id = null) {
		$this->LicenseAgreement->id = $id;
		if (!$this->LicenseAgreement->exists()) {
			throw new NotFoundException(__('Invalid license agreement'));
		}
		$this->layout = false;
		$this->set('license_agreement_html',$this->LicenseAgreement->field('license_agreement_html'));
	}

/**
 * Adds a new license agreement
 * 
 * @param int $id prospective customer id
 * @return void
 */
	public function add() {

		$this->uses[] = 'Prospect';
		$this->uses[] = 'Building';

		// if prospective customer id is specified
		if (!empty($prospective_customer_id)) {
			if (!$this->Prospect->exists($prospective_customer_id)) {
				throw new NotFoundException(__('Invalid Prospective Customer'));
			}
			$prospective_customer = $this->Prospect->find('first', array(
				'conditions' => array(
					'Prospect.id' => $prospective_customer_id
				),
				// 'fields' => 'Prospect.company'
			));

			$la = array();
			$pc = $prospective_customer['Prospect'];

			// mapping
			$la['prospective_customer_id'] = $pc['id'];
			$la['title'] = $pc['title'];
			$la['prospective_customer'] = $pc['company'];
			$la['building_id'] = 
				@$pc['building_id'] ?: 
				$this->Building->lookupId('short_global_label',$pc['desired_location']);
			$la['company'] = $pc['company'];
			$la['first_name'] = $pc['first_name'];
			$la['last_name'] = $pc['last_name'];
			$la['phone'] = $pc['work_phone_number'];
			$la['email'] = $pc['email_address'];
			$la['address1'] = $pc['address_1'];
			$la['address2'] = $pc['address_2'];
			$la['city'] = $pc['city'];
			$la['state'] = $pc['state'];
			$la['zip'] = $pc['zip_code'];

			$this->set('prospect', $prospective_customer);
			$this->request->data['LicenseAgreement'] = $la;
		}

		if ($this->request->is('post')) {

			$data = $this->request->data;

			$this->uses[] = 'Building';
			$this->Building->id = $data['LicenseAgreement']['building_id'];
			

			// CATHY edits: MOS2 should not be the only entity and license_date was not being saved. 
			$licensor_entity = $this->Building->field('licensor_entity');
			// $licensor_entity = 'mos2'; // FIXME
			$licensor_signer = 'Matt Rosanvallon'; // FIXME
			$months = $data['LicenseAgreement']['months'];
			$start_date = $data['LicenseAgreement']['start_date'];
			$end_date = date('Y-m-d',strtotime("+{$months} months",strtotime($start_date)));
			$data['LicenseAgreement']['licensor_entity'] = $licensor_entity;
			$data['LicenseAgreement']['licensor_signer'] = $licensor_signer;
			$data['LicenseAgreement']['end_date'] = $end_date;
		

			// FIXME actually saved as html
			$this->uses[] = 'LicenseAgreementTemplate';
			$template_id = $this->LicenseAgreementTemplate->lookupId('building_id', $data['LicenseAgreement']['building_id']);

			$data['LicenseAgreement']['license_agreement_html'] = $this->fillTemplate($template_id, $data['LicenseAgreement']);


			// FIXME generate pdf from html in licenseAgreements/edit
			$data['LicenseAgreement']['license_agreement_rtf'] = '';
			$data['LicenseAgreement']['license_agreement_pdf'] = '';


			$this->LicenseAgreement->create();
			if ($this->LicenseAgreement->save($data)) {
				$this->Session->setFlash(__('The license agreement has been saved.'));
				return $this->redirect(array('action' => 'view', $this->LicenseAgreement->id));
			} else {
				$this->log($this->LicenseAgreement->validationErrors,'debug');
				$this->Session->setFlash(__('The license agreement could not be saved. Please, try again.'));
			}
		}
		$this->uses[] = 'Building';
		$buildings = $this->Building->officeSuites();
		$desks = $this->LicenseAgreement->Desk->find('list', array(
			'fields' => array('id','assigned_number','Floor.short_global_label'),
			'contain' => 'Floor.short_global_label',
			'order' => array('floor_id ASC', 'assigned_number ASC')
		));
		$customerAccounts = $this->LicenseAgreement->CustomerAccount->find('list');
		$customers = $this->LicenseAgreement->Customer->find('list');
		$this->set(compact('desks', 'customerAccounts', 'customers', 'buildings'));
	}

/**
 * Fills out the license agreement template with the passed values
 *
 * @param int $template_id id of the template to complete
 * @param int $fields list of fields to use
 * @return string completed template
 */
	public function fillTemplate($template_id, $fields = array()) {

		$this->uses[] = 'LicenseAgreementTemplate';
		$this->LicenseAgreementTemplate->id = $template_id;

		if (!$this->LicenseAgreementTemplate->exists()) {
			throw new NotFoundException('Invalid license agreement template id');
		}

		$result = $this->LicenseAgreementTemplate->field('template');
		foreach ($fields as $key => $value) {
			$target = '{{' . $key . '}}';
			$result = str_replace($target,$value,$result);
		}
		$result = minify_html($result);
		return $result;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LicenseAgreement->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement'));
		}
		$licenseAgreement = $this->LicenseAgreement->find('first', array(
			'conditions' => array('LicenseAgreement.id' => $id)
		));

		// PUT|POST request
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;
			if ($this->LicenseAgreement->save($data)) {
				$this->Alert->success('License agreement has been saved successfully');
			} else {
				debug($this->LicenseAgreement->validationErrors);
				$this->Alert->error('License agreement could not be saved');
			}
		}
		// GET request
 		elseif ($this->request->is('get')) {
			$this->LicenseAgreement->id = $id;
			$this->request->data = $this->LicenseAgreement->read();
		}
		$phone_services = array_self_combine(array('Analog','None','VOIP'));
		$this->set(compact('licenseAgreement','phone_services'));

		$this->uses[] = 'Building';
		$buildings = $this->Building->officeSuites();
		$desks = $this->LicenseAgreement->Desk->find('list', array(
			'conditions' => array('Desk.is_active' => true),
			'fields' => array('id','assigned_number','Floor.short_global_label'),
			'contain' => array('Floor.short_global_label'),
			'order' => array('floor_id ASC', 'assigned_number ASC')
		));
		$customerAccounts = $this->LicenseAgreement->CustomerAccount->find('list');
		$customers = $this->LicenseAgreement->Customer->find('list');
		$this->set(compact('desks', 'customerAccounts', 'customers', 'buildings'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LicenseAgreement->id = $id;
		if (!$this->LicenseAgreement->exists()) {
			throw new NotFoundException(__('Invalid license agreement'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->LicenseAgreement->delete()) {
			$this->Session->setFlash(__('The license agreement has been deleted.'));
		} else {
			$this->Session->setFlash(__('The license agreement could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * 
 * apitests method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function apitests() {

			$response = $this->LicenseAgreement->docusign_sender_view();
			$this->set(compact('response'));

	}


/**
 * 
 * apidownload method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function apidownload($env_id, $doc_id) {

			$response = $this->LicenseAgreement->get_docusign_envelope_document($env_id, $doc_id);
			$this->set(compact('response'));

	}


/**
 * 
 * docusign_final_download method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function docusign_final_download($env_id) {

		$response = $this->LicenseAgreement->get_docusign_allinone_document($env_id);

		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="'."agreement".'.pdf"');
		print $response; exit;

	}

/**
 * upload method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function upload($id = null) {
		if (!$this->LicenseAgreement->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement'));
		}
		if ($this->request->is(array('post', 'put'))) {

			// Send files to Docusign
			$post_data = $this->request->data;

			$agreement = $this->LicenseAgreement->find('first', array(
				'conditions' => array('LicenseAgreement.id' => $id)
			));

			$agreement['LicenseAgreement']['file'] = $post_data['LicenseAgreement']['file'];
			$agreement['LicenseAgreement']['modifier_id'] = $post_data['LicenseAgreement']['modifier_id'];

			// Make Sure Files are not empty
			if ($post_data['LicenseAgreement']['file'][0]['size'] === 0) {
				$this->Session->setFlash(__('No Files Have Been Attached.'));
			}
			$response = $this->LicenseAgreement->create_docusign_envelope($agreement['LicenseAgreement']);

			// save envelopeID
			$agreement['LicenseAgreement']['ds_envelopeID'] = $response['envelopeId'];

			$sender = $this->LicenseAgreement->docusign_sender_view($response['envelopeId'], $id);

			if ($this->LicenseAgreement->save($agreement)) {
				$this->Session->setFlash(__('The license agreement has been sent out!'));
				$this->redirect($sender['url']);
			} else {
				$this->Session->setFlash(__('The license agreement could not be saved. Please, try again.'));
			}
			
		}
		
	}


/**
 * correction method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function correction($id = null) {

		if (!$this->LicenseAgreement->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement'));
		}

		$agreement = $this->LicenseAgreement->find('first', array(
			'conditions' => array('LicenseAgreement.id' => $id)
		));

		if (!empty($agreement['LicenseAgreement']['ds_envelopeID'])) {
			$response = $this->LicenseAgreement->docusign_correct_view($agreement['LicenseAgreement']['ds_envelopeID'], $id);

			$this->redirect($response['url']);

		} else {
			throw new NotFoundException(__('No Envelope Exists'));
		}
	}



/**
 * convert method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function convert($id = null) {
		return $this->redirect(array('controller' => 'customers', 'action' => 'add', $id));
	}

/**
 * ajax to model method
 *
 * 
 * 
 */

	public function ajaxLicenseAgreement($method = null, $param1 = null, $param2 = null) {

		$response = $this->LicenseAgreement->$method($param1, $param2);

		// lookup License Agreement
		$agreement = $this->LicenseAgreement->find('first' , array(
			'conditions' => array(
				'LicenseAgreement.ds_envelopeID' => $param1,
			)
		));

		if ($method == 'get_docusign_envelope') {

			// save field
			$agreement['LicenseAgreement']['ds_json_envelope'] = $response;
			$this->LicenseAgreement->save($agreement);

		} else if ($method == 'ds_json_envelope_documents') {

			// save field
			$agreement['LicenseAgreement']['ds_json_envelope_assets'] = $response;
			$this->LicenseAgreement->save($agreement);

		} else if ($method == 'get_docusign_envelope_recipients') {

			// save field
			$agreement['LicenseAgreement']['ds_json_envelope_recipients'] = $response;
			$this->LicenseAgreement->save($agreement);

		}

		print $response; exit;

	}
}

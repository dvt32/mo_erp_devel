<?php
App::uses('AppController', 'Controller');
/**
 * LicenseAmendmentTemplates Controller
 *
 * @property LicenseAmendmentTemplate $LicenseAmendmentTemplate
 * @property PaginatorComponent $Paginator
 */
class LicenseAmendmentTemplatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LicenseAmendmentTemplate->recursive = 0;
		$this->set('licenseAmendmentTemplates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LicenseAmendmentTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid license amendment template'));
		}
		$options = array(
			'conditions' => array(
				'LicenseAmendmentTemplate.' . $this->LicenseAmendmentTemplate->primaryKey => $id
			)
		);
		$licenseAmendmentTemplate = $this->LicenseAmendmentTemplate->find('first', $options);

		// debug($licenseAmendmentTemplate);
		$this->set(compact('licenseAmendmentTemplate'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->LicenseAmendmentTemplate->create();
			if ($this->LicenseAmendmentTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The license amendment template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The license amendment template could not be saved. Please, try again.'));
			}
		}
		$employees = $this->LicenseAmendmentTemplate->Employee->find('list');
		$this->set(compact('employees'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LicenseAmendmentTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid license amendment template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LicenseAmendmentTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The license amendment template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The license amendment template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LicenseAmendmentTemplate.' . $this->LicenseAmendmentTemplate->primaryKey => $id));
			$this->request->data = $this->LicenseAmendmentTemplate->find('first', $options);
		}
		$employees = $this->LicenseAmendmentTemplate->Employee->find('list');
		$this->set(compact('employees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LicenseAmendmentTemplate->id = $id;
		if (!$this->LicenseAmendmentTemplate->exists()) {
			throw new NotFoundException(__('Invalid license amendment template'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->LicenseAmendmentTemplate->delete()) {
			$this->Session->setFlash(__('The license amendment template has been deleted.'));
		} else {
			$this->Session->setFlash(__('The license amendment template could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * Search for license amendment template by name
 * 
 * @return void
 */
	public function search() {
		$query = $this->request->query['query'];
		$templates = $this->LicenseAmendmentTemplate->find('all', array(
			'conditions' => array('LicenseAmendmentTemplate.name ILIKE' => "%{$query}%")
		));
		$suggestions = array();
		foreach ($templates as $template) {
			
			// build suggestion
			$suggestion = new stdClass;
			$suggestion->data = arrayToObject($template);
			$suggestion->value = "{$template['LicenseAmendmentTemplate']['name']}";

			// add to list
			$suggestions[] = $suggestion;
		}
		
		// serialize the suggestions
		$this->set(compact('suggestions'));
		$this->set('_serialize', array('suggestions'));
	}

}

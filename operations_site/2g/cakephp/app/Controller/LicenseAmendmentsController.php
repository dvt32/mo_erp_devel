<?php
App::uses('AppController', 'Controller');
/**
 * LicenseAmendments Controller
 *
 * @property LicenseAmendment $LicenseAmendment
 * @property PaginatorComponent $Paginator
 */
class LicenseAmendmentsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LicenseAmendment->recursive = 0;
		$licenseAmendments = $this->Paginator->paginate();
		$this->set(compact('licenseAmendments'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LicenseAmendment->exists($id)) {
			throw new NotFoundException(__('Invalid license amendment'));
		}
		$licenseAmendment = $this->LicenseAmendment->find('first', array(
			'conditions' => array('LicenseAmendment.id' => $id),
			'contain' => array('Agreement' => array(
				'fields' => array(
					'id',
					'company',
					'license_date',
					'modifier_id',
					'modified',
					'created',
					'ds_envelopeID'
				)
			)),
			'fields' => array(
				'id',
				'modifier_id',
				'modified',
				'created',
				'ds_envelope_id'
			)
		));
		$this->set(compact('licenseAmendment'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->LicenseAmendment->create();
			if ($this->LicenseAmendment->save($this->request->data)) {
				$this->Session->setFlash(__('The license amendment has been saved.'));
				$license_agreement_id = $this->LicenseAmendment->field('license_agreement_id');
				return $this->redirect(array('controller' => 'LicenseAgreements', 'action' => 'view', $license_agreement_id));
			} else {
				$this->Session->setFlash(__('The license amendment could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LicenseAmendment->exists($id)) {
			throw new NotFoundException(__('Invalid license amendment'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->LicenseAmendment->save($this->request->data)) {
				$this->Session->setFlash(__('The license amendment has been saved.'));
			} else {
				$this->Session->setFlash(__('The license amendment could not be saved. Please, try again.'));
			}
			return $this->redirect(array('action' => 'edit', $id));

		} else {

			$options = array('conditions' => array('LicenseAmendment.' . $this->LicenseAmendment->primaryKey => $id));
			$this->request->data = $this->LicenseAmendment->find('first', $options);

		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LicenseAmendment->id = $id;
		if (!$this->LicenseAmendment->exists()) {
			throw new NotFoundException(__('Invalid license amendment'));
		}

		$agreement_id = $this->LicenseAmendment->find('first', array(
			'conditions' => array('LicenseAmendment.id' => $id),
			'fields' => 'license_agreement_id'
		));
		$agreement_id = $agreement_id['LicenseAmendment']['license_agreement_id'];

		$this->request->onlyAllow('post', 'delete');
		if ($this->LicenseAmendment->delete()) {
			$this->Session->setFlash(__('The license amendment has been deleted.'));
		} else {
			$this->Session->setFlash(__('The license amendment could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('controller' => 'LicenseAgreements', 'action' => 'view', $agreement_id));
	}

/**
 * printOut method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function printOut($id = null) {
		$this->LicenseAmendment->id = $id;
		if (!$this->LicenseAmendment->exists()) {
			throw new NotFoundException(__('Invalid license amendment'));
		}
		$this->layout = false;
		$this->set('amendment',$this->LicenseAmendment->field('amendment'));
	}


/**
 * upload method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function upload($id = null) {
		if (!$this->LicenseAmendment->exists($id)) {
			throw new NotFoundException(__('Invalid license amendment'));
		}
		if ($this->request->is(array('post', 'put'))) {

			// Send files to Docusign
			$post_data = $this->request->data;

			// Get Amendment Array
			$amendment = $this->LicenseAmendment->find('first', array(
				'conditions' => array('LicenseAmendment.id' => $id)
			));

			$this->uses[] = 'LicenseAgreement';
			$agreement = $this->LicenseAgreement->find('first', array(
				'conditions' => array('LicenseAgreement.id' => $amendment['LicenseAmendment']['license_agreement_id'])
			));

			// for file transfer
			$agreement['LicenseAgreement']['file'] = $post_data['LicenseAmendment']['file'];

			// Make Sure Files are not empty
			if ($post_data['LicenseAmendment']['file'][0]['size'] != 0) {
				$this->Session->setFlash(__('No Files Have Been Attached.'));
			}

			$response = $this->LicenseAmendment->create_docusign_envelope($agreement['LicenseAgreement']);

			// save envelopeID
			$amendment['LicenseAmendment']['ds_envelope_id'] = $response['envelopeId'];
			$amendment['LicenseAmendment']['modifier_id'] = $post_data['LicenseAmendment']['modifier_id'];

			$sender = $this->LicenseAmendment->docusign_sender_view($response['envelopeId'], $id);

			if ($this->LicenseAmendment->save($amendment)) {
				$this->Session->setFlash(__('The license agreement has been saved'));
			} else {
				$this->Session->setFlash(__('The license agreement could not be saved. Please, try again.'));
			}
			return $this->redirect(array('controller' => 'LicenseAgreements', 'action' => 'view', $agreement['LicenseAgreement']['id']));
			
		}
		
	}


/**
 * correction method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function correction($id = null) {

		if (!$this->LicenseAmendment->exists($id)) {
			throw new NotFoundException(__('Invalid license agreement'));
		}

		$agreement = $this->LicenseAmendment->find('first', array(
			'conditions' => array('LicenseAmendment.id' => $id)
		));

		if (!empty($agreement['LicenseAmendment']['ds_envelope_id'])) {
			$response = $this->LicenseAmendment->docusign_correct_view($agreement['LicenseAmendment']['ds_envelope_id'], $id);

			$this->redirect($response['url']);

		} else {
			throw new NotFoundException(__('No Envelope Exists'));
		}
	}

/**
 * ajax to model method
 *
 * 
 * 
 */

	public function ajaxLicenseAmendment($method = null, $param1 = null, $param2 = null) {

		$response = $this->LicenseAmendment->$method($param1, $param2);

		// lookup License Agreement
		$agreement = $this->LicenseAmendment->find('first' , array(
			'conditions' => array(
				'LicenseAmendment.ds_envelope_id' => $param1,
			)
		));

		$this->set(compact('response'));

	}



/**
 * 
 * apidownload method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function apidownload($env_id, $doc_id) {

			$response = $this->LicenseAmendment->get_docusign_envelope_document($env_id, $doc_id);
			$this->set(compact('response'));

	}

}

<?php
App::uses('AppController', 'Controller');
/**
 * MailPackages Controller
 *
 * @property MailPackage $MailPackage
 * @property PaginatorComponent $Paginator
 */
class MailPackagesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {

		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query, array(
				'fulltext' => array(
					'sender',
					'CustomerAccount.name',
					'CustomerAccount.pkg_notify_email'
				),
				'ignore' => array(
					'date'
				)
			));

			$date = @$this->request->query['date'];

			// convert array to string
			if (!empty($date) && is_array($date)) {
				
				// ensure all keys are set
				if (!empty($date['year']) && !empty($date['month']) && !empty($date['day'])) {

					$date = @array_filter($date);
					$date = $date['year'] . '-' . $date['month'] . '-' . $date['day'];

					// convert date to datetime
					if (!empty($date)) {

						$start_dt =  $date . ' 00:00:00';
						$end_dt =    $date . ' 23:59:59';

						$conditions["MailPackage.date >="] = $start_dt;
						$conditions["MailPackage.date <="] = $end_dt;
					}
				} else {
					$this->request->query['date'] = null;
				}
			}

			// set request query
			$this->request->data['MailPackage'] = $this->request->query;
		
		}

		if (empty($this->request->named['order'])) {
			$order = 'date DESC';
		}

		$contain = array('CustomerAccount.name');
		$this->Paginator->settings = array(
			'MailPackage' => compact('conditions','contain','fields','order','limit')
		);
		$mailPackages = $this->Paginator->paginate();

		$carriers = $this->MailPackage->carriers;
		$package_types = $this->MailPackage->package_types;
		$this->uses[] = 'Employee';
		$employees = $this->Employee->find('list', array(
			'conditions' => array('Employee.is_active' => true),
			'fields' => array('id','full_name'),
			'order' => 'Employee.full_name ASC'
		));

		$this->set(compact('mailPackages','carriers','package_types','employees'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MailPackage->exists($id)) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		$options = array('conditions' => array('MailPackage.' . $this->MailPackage->primaryKey => $id));
		$this->set('mailPackage', $this->MailPackage->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// POST request
		if ($this->request->is('post')) {

			// TODO save `email_message` by filling in a template with data from the form

			$this->MailPackage->create();
			if ($this->MailPackage->save($this->request->data)) {
				$this->Alert->success(__('The mail package has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Alert->error(__('The mail package could not be saved. Please, try again.'));
			}
		}
		// GET request
		else {
			$this->request->data['MailPackage']['email_message'] = "A package has arrived for you.";
			$this->request->data['MailPackage']['sender'] = $this->Auth->user('id');
		}

		$this->uses[] = 'Employee';
		$employees = $this->Employee->find('list', array(
			'fields' => array('full_name','full_name'),
			'conditions' => array('is_active' => true),
			'order' => 'full_name ASC'
		));

		// get my name for defualt selection
		$employee_id = $this->Auth->user('id');
		$my_name = $this->Employee->field('full_name', ['id' => $employee_id]);

		$this->set(compact('employees','my_name'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MailPackage->exists($id)) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MailPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The mail package has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mail package could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MailPackage.' . $this->MailPackage->primaryKey => $id));
			$this->request->data = $this->MailPackage->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MailPackage->id = $id;
		if (!$this->MailPackage->exists()) {
			throw new NotFoundException(__('Invalid mail package'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MailPackage->delete()) {
			$this->Session->setFlash(__('The mail package has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mail package could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

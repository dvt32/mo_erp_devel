<?php
App::uses('AppController', 'Controller');
/**
 * Mailboxes Controller
 *
 * @property Mailbox $Mailbox
 * @property PaginatorComponent $Paginator
 */
class MailboxesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		// if filter params are set...
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query);

			// workaround for datatype numbers only
			$conditions['OR'][]['CAST(Mailbox.mailbox_number AS TEXT) LIKE'] = "%{$this->request->query['q']}%";
			$conditions['OR'][]['CustomerAccount.name ILIKE'] = "%{$this->request->query['q']}%";

			// set form input values to passed params
			$this->request->data['Mailbox'] = $this->request->query;
		}

		// Building - Floor array
		$floors = $this->Mailbox->Floor->find('list', array(
			'fields' => array('id', 'short_global_label'),
			'conditions' => array('is_active' => true)
		));


		$limit = @$this->request->query['limit'] ?: 50;
		$this->Mailbox->recursive = 0;
		$this->Paginator->settings = array('Mailbox' => compact('conditions','contain','order','limit'));

		$mailboxes = $this->Paginator->paginate();
		$this->set(compact('mailboxes', 'types', 'floors'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Mailbox->exists($id)) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		$options = array(
			'conditions' => array(
				'Mailbox.' . $this->Mailbox->primaryKey => $id,
			),
			'recursive' => 1
		);
		$this->set('mailbox', $this->Mailbox->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Mailbox->create();
			if ($this->Mailbox->save($this->request->data)) {
				$this->Session->setFlash(__('The mailbox has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mailbox could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Mailbox->CustomerAccount->find('list');
		$floors = $this->Mailbox->Floor->find('list');
		$this->set(compact('customerAccounts', 'floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		// safety check
		if (!$this->Mailbox->exists($id)) {
			throw new NotFoundException(__('Invalid mailbox'));
		}

		// POST|PUT request
		if ($this->request->is(array('post', 'put'))) {

			$data = $this->request->data;
			$fieldList = array('id','account_id');

			if ($this->Mailbox->save($this->request->data)) {
				$this->Session->setFlash(__('The mailbox has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mailbox could not be saved. Please, try again.'));
			}
		}

		// GET request
		else {
			$options = array('conditions' => array('Mailbox.' . $this->Mailbox->primaryKey => $id));
			$this->request->data = $this->Mailbox->find('first', $options);
		}

		$customerAccounts = $this->Mailbox->CustomerAccount->find('list');
		$floors = $this->Mailbox->Floor->listActiveOfficeFloors();

		$this->set(compact('customerAccounts', 'floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Mailbox->id = $id;
		if (!$this->Mailbox->exists()) {
			throw new NotFoundException(__('Invalid mailbox'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Mailbox->delete()) {
			$this->Session->setFlash(__('The mailbox has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mailbox could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

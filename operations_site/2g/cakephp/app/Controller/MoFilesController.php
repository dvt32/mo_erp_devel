<?php
App::uses('AppController', 'Controller');
/**
 * MoFile Controller
 *
 * @property MoFile $MoFile
 */
class MoFilesController extends AppController {

	public $uses = array('MoFile', 'Scan', 'FaxIn');	

	public $paginate = array(
		'limit' => '50',
		'order' => 'MoFile.creation_dt DESC',
	);


/**
 * download method
 *
 * @throws NotFoundException
 * @param integer $id
 * @return void
 */
	public function download($id = null) {
		$this->MoFile->id = $id;
		if (!$this->MoFile->exists()) {
				throw new NotFoundException(__('MoFile wasn\'t found'));
		}
		$fretrieve = $this->MoFile->find('first', array(
			'conditions' => array('MoFile.id' => $id)
		));
		$url = DAV_SERVER_HOST.pathFromId($id);
		$filename = urlencode($fretrieve['MoFile']['filename']);
		$fileUrl = download_file($url, './files/'.$filename);
		$filename = substr(strrchr($fileUrl, '/'), 1);
		$this->response->file($fileUrl, array('name' => $filename, 'download' => true));
		return $this->response;
	}

	public function show($id) {
		// Get Media In Browser
		$fretrieve = $this->MoFile->find('first', array(
			'conditions' => array('MoFile.id' => $id)
		));
		$url = DAV_SERVER_HOST.pathFromId($id).urlencode($fretrieve['MoFile']['filename']);

		$fileUrl = download_file($url, './files/');

		$filename = substr(strrchr('/'), 1);
		$this->response->file($fileUrl, array('name' => $fileName, 'download' => false));
		return $this->response;
	}

/**
 * account method
 *
 * @return void
 */
	public function account() {

		$caid = AuthComponent::user('cust_acct_id');

		// $this->paginate['conditions'] = array('Scan.account_id' => $caid, 'FaxIn.account_id' => $caid);
		// $this->paginate['contain'] = array('Scan.account_id' => $caid, 'FaxIn.account_id' => $caid);
		$this->set('files', $this->paginate());
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->Paginator->settings = array(
			'MoFile' => array(
				'recursive' => 0,
				'limit' => 20,
				'order' => 'MoFile.creation_dt DESC'
			)
		);
		$files = $this->paginate();
		$this->set(compact('files'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->MoFile->id = $id;
		if (!$this->MoFile->exists()) {
				throw new NotFoundException(__('MoFile wasn\'t found'));
		}
		$fretrieve = $this->MoFile->find('first', array(
			'conditions' => array('MoFile.id' => $id)
		));
		$url = DAV_SERVER_HOST.pathFromId($id);
		$filename = urlencode($fretrieve['MoFile']['filename']);
		$fileUrl = download_file($url, './files/'.$filename);
		$filename = substr(strrchr($fileUrl, '/'), 1);
		$this->response->file($fileUrl, array('name' => $filename, 'download' => false));
		return $this->response;

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MoFile->create();
			if ($this->MoFile->save($this->request->data)) {
				$this->Session->setFlash(__('The file tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The file tbl could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MoFile->exists($id)) {
			throw new NotFoundException(__('Invalid file tbl'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MoFile->save($this->request->data)) {
				$this->Session->setFlash(__('The file tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The file tbl could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MoFile.' . $this->MoFile->primaryKey => $id));
			$this->request->data = $this->MoFile->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MoFile->id = $id;
		if (!$this->MoFile->exists()) {
			throw new NotFoundException(__('Invalid file tbl'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MoFile->delete()) {
			$this->Session->setFlash(__('MoFile tbl deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('MoFile tbl was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MoFile->recursive = 0;
		$this->set('file', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MoFile->exists($id)) {
			throw new NotFoundException(__('Invalid file tbl'));
		}
		$options = array('conditions' => array('MoFile.' . $this->MoFile->primaryKey => $id));
		$this->set('file', $this->MoFile->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->MoFile->create();
			if ($this->MoFile->save($this->request->data)) {
				$this->Session->setFlash(__('The file tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The file tbl could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MoFile->exists($id)) {
			throw new NotFoundException(__('Invalid file tbl'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MoFile->save($this->request->data)) {
				$this->Session->setFlash(__('The file tbl has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The file tbl could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MoFile.' . $this->MoFile->primaryKey => $id));
			$this->request->data = $this->MoFile->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MoFile->id = $id;
		if (!$this->MoFile->exists()) {
			throw new NotFoundException(__('Invalid file tbl'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MoFile->delete()) {
			$this->Session->setFlash(__('MoFile tbl deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('MoFile tbl was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * MoveoutRequests Controller
 *
 * @property MoveoutRequest $MoveoutRequest
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MoveoutRequestsController extends AppController {

/**
 * Models used by this controller
 * 
 * @var array
 */
	public $uses = array('MoveoutRequest','CustomerAccount','Employee');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		// user input query
		$query = $this->request->query;

		// TODO filter by move-out date-range
		// TODO filter by close-out date-range

		// if filter params are set...
		if (!empty($query)) {

			// define conditions
			$conditions = $this->filterByQuery(
				$this->request->query,
				array(
					'fulltext' => 'name',
					'ignore' => array(
						'floor_id',
						'move_out_start_range',
						'move_out_end_range',
						'close_out_start_range',
						'close_out_end_range',
						'flags'
					)
				)
			);

			// filter by floor(s)
			if ($query['floor_id'])
			{
				$conditions['CustomerAccount.primary_floor_id'] = $query['floor_id'];
			}

			// move out date
			if ($query['move_out_start_range'])
			{
				// after move_out_start_range inclusive
				$conditions['MoveoutRequest.move_out_date >='] = $query['move_out_start_range'];
			}
			if ($query['move_out_end_range'])
			{
				// before move_out_end_range inclusive
				$conditions['MoveoutRequest.move_out_date <='] = $query['move_out_end_range'];
			}
			// close out date
			if ($query['close_out_start_range'])
			{
				// after close_out_start_range inclusive
				$conditions['MoveoutRequest.close_out_date >='] = $query['close_out_start_range'];
			}
			if ($query['close_out_end_range'])
			{
				// before close_out_end_range inclusive
				$conditions['MoveoutRequest.close_out_date <='] = $query['close_out_end_range'];
			}
			// filter by flags
			if (!empty($query['flags']))
			{
				foreach ($query['flags'] as $flag)
				{
					$conditions['NOT']["MoveoutRequest.{$flag}_dt"] = null;
				}
			}

			// form-data set to existing query
			$this->request->data['MoveoutRequest'] = $query;
		}

		// related models to fetch
		$contain = ['CustomerAccount' => ['id','name']];
		
		// fetch move-out requests
		$this->Paginator->settings = ['MoveoutRequest' => compact('conditions','contain')];
		$moveoutRequests = $this->Paginator->paginate();

		// fetch values to populate dropdowns
		$types = $this->CustomerAccount->types;
		$flags = $this->MoveoutRequest->flags;
		$services = $this->CustomerAccount->Service->find('list', array('conditions' => 'Service.is_active'));
		$floors = $this->CustomerAccount->Floor->find('list', array(
			'conditions' => array('Floor.is_active' => true),
			'order' => 'Floor.short_global_label ASC'
		));
		$customerAccounts = $this->CustomerAccount->find('list', array(
			'conditions' => ['CustomerAccount.is_active' => true], // FIXME? should inactive customer accounts be listed?
			'order' => 'CustomerAccount.name ASC'
		));

		// send vars to view
		$this->set(compact(
			'moveoutRequests',
			'customerAccounts',
			'services',
			'types',
			'floors',
			'flags'
		));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		// safety check
		if (!$this->MoveoutRequest->exists($id)) {
			throw new NotFoundException(__('Invalid moveout customer'));
		}

		// fetch the move out request
		$moveoutRequest = $this->MoveoutRequest->find('first', array(
			'conditions' => array('MoveoutRequest.id' => $id),
			'contain' => 'CustomerAccount'
		));

		// reference list of employees
		$employees = $this->Employee->getActiveList();

		// send variables to the view
		$this->set(compact('moveoutRequest','employees'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// POST request
		if ($this->request->is('post'))
		{

			// reset the model
			$this->MoveoutRequest->create();

			// save the moveout request
			if ($this->MoveoutRequest->save($this->request->data))
			{
				// report success
				$this->Alert->success('Thank you for submitting your move-out request'); // FIXME
				return $this->redirect(['controller' => 'customers', 'action' => 'dashboard']);
			}
			else
			{
				// report failure
				$this->Alert->error('Your move-out request could not be saved, please review the form below and correct any errors');
			}
		}
		// set the view title
		$this->set('title_for_layout', 'Start Move-Out Request');
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {

		// fetch the move-out request associated with the customer account
		$cust_acct_id = AuthComponent::user('cust_acct_id');
		$moveoutrequest = $this->MoveoutRequest->find('first', array(
			'conditions' => compact('cust_acct_id')
		));

		// if no move-out request associated with this customer account
		if (empty($moveoutrequest))
		{
			// redirect to "add move-out request"
			return $this->redirect(['action' => 'add']);
		}

		// grab the model id
		$id = $moveoutrequest['MoveoutRequest']['id'];

		// POST|PUT request
		if ($this->request->is(array('post', 'put'))) {

			// gather the data
			$data = $this->request->data;

			// save the moveout request
			if ($this->MoveoutRequest->save($data)) {

				// report success
				$this->Alert->success(__('Your move-out request has been successfully updated')); // FIXME
				return $this->redirect(['controller' => 'customers', 'action' => 'dashboard']);
			}
			else
			{
				// report failure
				$this->Alert->error('Your move-out request could not be updated. Please review the form below and try again');
			}
		}
		else
		{
			// set the form data to match the existing move-out request
			$this->request->data = $this->MoveoutRequest->find('first', ['conditions' => ['MoveoutRequest.id' => $id]]);
		}

		// set the view title
		$this->set('title_for_layout', 'Edit Move-Out Request');
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MoveoutRequest->id = $id;
		if (!$this->MoveoutRequest->exists()) {
			throw new NotFoundException(__('Invalid moveout customer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MoveoutRequest->delete()) {
			$this->Session->setFlash(__('The moveout customer has been deleted.'));
		} else {
			$this->Session->setFlash(__('The moveout customer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function updateFlag($id, $flag_name)
	{
		// set the model id
		$this->MoveoutRequest->id = $id;

		// safety check
		if (!$this->MoveoutRequest->exists()) {
			throw new NotFoundException(__('Invalid move-out request id'));
		}

		// restrict to post
		$this->request->onlyAllow('post');

		// update the request
		$result = $this->MoveoutRequest->updateFlag($flag_name);
	}

	public function resetFlag($id, $flag_name)
	{
		// set the model id
		$this->MoveoutRequest->id = $id;

		// safety check
		if (!$this->MoveoutRequest->exists()) {
			throw new NotFoundException(__('Invalid move-out request id'));
		}

		// restrict to post
		$this->request->onlyAllow('post');

		// update the request
		$result = $this->MoveoutRequest->resetFlag($flag_name);
	}
}


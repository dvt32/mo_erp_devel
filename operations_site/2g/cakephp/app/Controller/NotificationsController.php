<?php
App::uses('AppController', 'Controller');
/**
 * Notifications Controller
 *
 * @property Notification $Notification
 * @property PaginatorComponent $Paginator
 */
class NotificationsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {

		// filter params set
		if (!empty($this->request->query)) {
			$conditions = $this->filterByQuery($this->request->query, array('fulltext' => array('name','description')));
			$this->request->data = $this->request->query;
		}
		$contain = array('Employee.id','Employee.full_name');

		$this->uses[] = 'Floor';
		$floors = $this->Floor->find('all', array(
			'conditions' => array('Floor.is_active' => true),
			'fields' => array('id','label','building_id'),
			'contain' => array('Building.short_global_label')
		));
		$floors_by_id = array();
		foreach ($floors as $floor) {
			$floors_by_id[$floor['Floor']['id']] = $floor;
		}
		// debug($floors_by_id);

		$this->Paginator->settings = array('Notification' => compact('conditions','fields','contain'));
		$notifications = $this->Paginator->paginate();

		foreach ($notifications as &$notification) {
			foreach ($notification['Notification']['floors'] as &$floor) {
				$floor = $floors_by_id[$floor]['Building']['short_global_label'] . ' - ' . $floors_by_id[$floor]['Floor']['label'];
			}
		}

		$this->set(compact('notifications'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Notification->exists($id)) {
			throw new NotFoundException(__('Invalid notification'));
		}
		$options = array('conditions' => array('Notification.' . $this->Notification->primaryKey => $id));
		$this->set('notification', $this->Notification->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// POST request
		if ($this->request->is('post')) {

			$data = $this->request->data;

			$this->Notification->create();
			if ($this->Notification->save($data)) {
				$this->Session->setFlash(__('The notification has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				debug($this->Notification->validationErrors);
				$this->Session->setFlash(__('The notification could not be saved. Please, try again.'));
			}
		}
		// GET request
		else {

			// default expiration datetime to 24 hours from now
			$this->request->data['Notification']['expires'] = strtotime("+24 hours");
		}

		// fetch rows from datasource
		$this->uses[] = 'Floor';
		$floors = $this->Floor->find('list',array(
			'conditions' => array('Floor.is_active' => true),
			'contain' => 'Building',
			'fields' => array('Floor.id','Floor.short_local_label','Building.short_global_label'),
			'order' => 'Floor.short_global_label ASC'
		));
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Notification->exists($id)) {
			throw new NotFoundException(__('Invalid notification'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Notification->save($this->request->data)) {
				$this->Session->setFlash(__('The notification has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The notification could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Notification.' . $this->Notification->primaryKey => $id));
			$this->request->data = $this->Notification->find('first', $options);
		}
		$rooms = $this->Notification->Room->find('list');
		$floors = $this->Notification->Floor->find('list');
		$buildings = $this->Notification->Building->find('list');
		$this->set(compact('rooms', 'floors', 'buildings'));
	}

/**
 * Deactivates a notification by setting its expiration to the current datetime.
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Notification->id = $id;
		if (!$this->Notification->exists()) {
			throw new NotFoundException(__('Invalid notification'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Notification->saveField('expires', date(DATETIME_FORMAT))) {
			$this->Session->setFlash(__('The notification has been deactivated.'));
		} else {
			$this->Session->setFlash(__('The notification could not be deactivated. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * PhoneNumbers Controller
 *
 * @property PhoneNumber $PhoneNumber
 */
class PhoneNumbersController extends AppController {

/**
 * account method
 * @author John Jung
 * @return void
 */
	public function account() {

		$options = array(
			'order' => 'PhoneNumber.creation_dt ASC',
		);

		$this->PhoneNumber->recursive = 0;
		$phoneNumbers = $this->paginate();
		$this->set(compact('phoneNumbers'));
	}

/**
 * search method
 * @author John Jung
 * @param string typing string of phonenumbers to be searching
 * @return array json array to pass to view
 * 
 */
	public function search($typing = null) {

		$alias = $this->PhoneNumber->alias;

		// parse the query
		if (!empty($this->request->query)) {

			$query = @$this->request->query['query'];

			$conditions['OR']["{$alias}.phone_number ILIKE"] = "%{$query}%";
			$limit = 10;
		}

		$prospects = $this->PhoneNumber->find(
			'all', 
			compact('conditions','contain','order','fields','limit'
		));

		$suggestions = array();
		foreach($prospects as $prospect) {
			$suggestion = new stdClass();
			$suggestion->data = arrayToObject($prospect);
			$suggestion->value = "{$prospect['PhoneNumber']['phone_number']}";
			$suggestions[] = $suggestion;
		}

		$this->set('suggestions', $suggestions);
		$this->set('_serialize', array('suggestions'));

	}


/**
 * forward method
 * @author John Jung
 * @param id of the phonenumber
 * @return void
 */
	public function forward($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['do_call_forward'] = true;
			$number['PhoneNumber']['call_forward_delay'] = $this->request->data['PhoneNumber']['call_forward_delay'];
			$number['PhoneNumber']['call_forward_dst_num'] = $this->request->data['PhoneNumber']['call_forward_dst_num'];

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}

		} else {
			$options = array('conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id));
			$this->request->data = $this->PhoneNumber->find('first', $options);
		}

	}



/**
 * unforward method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function unforward($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['do_call_forward'] = false;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Call fowarding has been disabled'));
			} else {
				$this->Session->setFlash(__('Call fowarding has been disabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'index'));
		}

	}


/**
 * voiceon method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function voiceon($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['is_voicemail_on'] = true;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Voicemail has been enabled'));
			} else {
				$this->Session->setFlash(__('Voicemail was not enabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'index'));
		}

	}


/**
 * voiceoff method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function voiceoff($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));

			$number['PhoneNumber']['is_voicemail_on'] = false;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Voicemail has been disabled'));
			} else {
				$this->Session->setFlash(__('Voicemail was not disabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'index'));
		}

	}

/**
 * recordingon method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function recordingon($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['is_recording_on'] = 1;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Recording has been enabled'));
			} else {
				$this->Session->setFlash(__('Recording was not enabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'index'));
		}

	}


/**
 * recordingoff method
 * @author John Jung
 * @param id of the phone number
 * @todo make sure the number belongs to the account holder
 * @return void
 */
	public function recordingoff($id) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			// Check to belong to customer
			$number = $this->PhoneNumber->find('first', array(
				'conditions' => array('PhoneNumber.id' => $id)
			));
			$number['PhoneNumber']['is_recording_on'] = 0;

			if ($this->PhoneNumber->save($number)) {
				$this->Session->setFlash(__('Recording has been disabled'));
			} else {
				$this->Session->setFlash(__('Recording was not disabled. Please, try again.'));
			}
			$this->redirect(array('action' => 'index'));
		}

	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		// if filter params are set...
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query);

			$querystring = @$this->request->query['q'];

			// workaround for datatype numbers only
			$conditions['OR'][]['CAST(PhoneNumber.phone_number AS TEXT) LIKE'] = "%{$querystring}%";
			$conditions['OR'][]['CAST(PhoneNumber.extension AS TEXT) LIKE'] = "%{$querystring}%";

			// set form input values to passed params
			$this->request->data['PhoneNumber'] = $this->request->query;
		}

		// $conditions['PhoneNumber.is_active'] = true;
		$limit = @$this->request->query['limit'] ?: 50;

		$this->PhoneNumber->order = 'PhoneNumber.creation_dt DESC';
		$this->PhoneNumber->recursive = 0;
		$this->Paginator->settings = array('PhoneNumber' => compact('conditions','contain','order','limit'));

		$phoneNumbers = $this->Paginator->paginate();

	
		$this->set(compact('phoneNumbers'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}

		$options = array(
			'conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id),
			'recursive' => 1
		);
		$this->set('phoneNumber', $this->PhoneNumber->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PhoneNumber->create();
			if ($this->PhoneNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->PhoneNumber->CustomerAccount->find('list', array('order' => 'name ASC'));
		$customers = $this->PhoneNumber->Customer->find('list', array('order' => 'full_name ASC'));
		$floors = $this->PhoneNumber->Floor->find('list');
		$iads = $this->PhoneNumber->Iad->find('list');
		$ipPhones = $this->PhoneNumber->IpPhone->find('list');
		$inboundTrunks = $this->PhoneNumber->InboundTrunk->find('list');
		$outboundTrunks = $this->PhoneNumber->OutboundTrunk->find('list');
		$this->set(compact('customerAccounts', 'customers', 'floors', 'iads', 'ipPhones', 'inboundTrunks', 'outboundTrunks'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PhoneNumber->exists($id)) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PhoneNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The phone number has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone number could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PhoneNumber.' . $this->PhoneNumber->primaryKey => $id));
			$this->request->data = $this->PhoneNumber->find('first', $options);
		}

		$customerAccounts = $this->PhoneNumber->CustomerAccount->find('list', array('order' => 'name ASC'));
		$customers = $this->PhoneNumber->Customer->find('list', array('order' => 'full_name ASC'));
		$floors = $this->PhoneNumber->Floor->find('list');
		$iads = $this->PhoneNumber->Iad->find('list');
		$ipPhones = $this->PhoneNumber->IpPhone->find('list');
		$inboundTrunks = $this->PhoneNumber->InboundTrunk->find('list');
		$outboundTrunks = $this->PhoneNumber->OutboundTrunk->find('list');
		$this->set(compact('customerAccounts', 'customers', 'floors', 'iads', 'ipPhones', 'inboundTrunks', 'outboundTrunks'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PhoneNumber->id = $id;
		if (!$this->PhoneNumber->exists()) {
			throw new NotFoundException(__('Invalid phone number'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PhoneNumber->delete()) {
			$this->Session->setFlash(__('Phone number deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phone number was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sendSms() {

		// Send Text
		if ($this->request->is('post') || $this->request->is('put')) {

			$send_array = $this->request->data;
			// configure email
			$email = new CakeEmail();
			$email->config(EMAIL_CONFIG)
				->to($send_array['PhoneNumber']['carrier'])
				->subject();

			// send the email
			$result = $email->send($send_array['PhoneNumber']['message']);
		}

		// Carriers
		$carriers = array(
			"message.alltel.com" => "Alltell",
			"txt.att.net" => "AT&T",
			"mymetropcs.com" => "MetroPCS",
			"messaging.nextel.com" => "Nextel",
			"ptel.net" => "Powertel",
			"messaging.sprintpcs.com" => "Sprint",
			"tms.suncom.com" => "SunCom",
			"tmomail.net" => "T-Mobile",
			"email.uscc.net" => "US Cellular",
			"vtext.com" => "Verizon",
			"vmobl.com" => "Virgin Mobile",
		);

		$this->set(compact('carriers'));
		

	}



}

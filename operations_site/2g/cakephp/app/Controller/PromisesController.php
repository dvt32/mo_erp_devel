<?php
App::uses('AppController', 'Controller');
/**
 * Promises Controller
 *
 * @property Promise $Promise
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PromisesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Promise->recursive = 0;

		$query = $this->request->query;

		// filter/search/sort
		if (!empty($query)) {

			// filter by query
			$conditions = $this->filterByQuery($query, array(
				'ignore' => 'primary_floor_id'
			));

			// filter by status
			$status = @$query['status'];
			if (!empty($status)) {
				$conditions['Promise.status'] = $status;
			}

			// filter by start-date
			$start_date = @$query['start_date'];
			if (!empty($start_date)) {
				$conditions['Promise.due_date >='] = $start_date;
			}
			// filter by end-date
			$end_date = @$query['end_date'];
			if (!empty($end_date)) {
				$conditions['Promise.due_date <='] = $end_date;
			}

			// filter by customer accounts' floor id
			$primary_floor_id = @$query['primary_floor_id'];
			if (!empty($primary_floor_id))
			{
				$conditions['CustomerAccount.primary_floor_id'] = $primary_floor_id;
			}

			$this->request->data['Promise'] = $query;
		}

		$limit = $this->request->query('limit') ?: 50;

		// fetch billing amounts
		$this->Paginator->settings = array('Promise' => compact('conditions','contain','limit','order','fields'));
		$promises = $this->Paginator->paginate();

		// list of customers for dropdown
		$this->uses[] = 'CustomerAccount';
		$customerAccounts = $this->CustomerAccount->find('list', array(
			'conditions' => array('is_active' => 1),
			'fields' => array('id','name')
		));

		// list of active floors
		$floors = $this->CustomerAccount->Floor->listActiveOfficeFloors();

		$this->set(compact('promises', 'customerAccounts','floors'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Promise->exists($id)) {
			throw new NotFoundException(__('Invalid promise'));
		}
		$options = array('conditions' => array('Promise.' . $this->Promise->primaryKey => $id));
		$this->set('promise', $this->Promise->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Promise->create();
			if ($this->Promise->save($this->request->data)) {

				

				$this->Session->setFlash(__('The promise has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The promise could not be saved. Please, try again.'));
			}
		}
		$custAccts = $this->Promise->CustomerAccount->find('list');
		$custPers = $this->Promise->Customer->find('list');
		$employees = $this->Promise->Employee->find('list');
		$modifiers = $this->Promise->Modifier->find('list');
		$this->set(compact('custAccts', 'custPers', 'employees', 'modifiers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Promise->exists($id)) {
			throw new NotFoundException(__('Invalid promise'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Promise->save($this->request->data)) {
				$this->Session->setFlash(__('The promise has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The promise could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Promise.' . $this->Promise->primaryKey => $id));
			$this->request->data = $this->Promise->find('first', $options);
		}
		$custAccts = $this->Promise->CustAcct->find('list');
		$custPers = $this->Promise->CustPer->find('list');
		$employees = $this->Promise->Employee->find('list');
		$modifiers = $this->Promise->Modifier->find('list');
		$this->set(compact('custAccts', 'custPers', 'employees', 'modifiers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Promise->id = $id;
		if (!$this->Promise->exists()) {
			throw new NotFoundException(__('Invalid promise'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Promise->delete()) {
			$this->Session->setFlash(__('The promise has been deleted.'));
		} else {
			$this->Session->setFlash(__('The promise could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function approve($id = null)
	{

		// set the promise id
		$this->Promise->id = $id;

		// safety check
		if (!$this->Promise->exists())
		{
			throw new NotFoundException('Invalid promise');
		}

		// limit the http methods allowed to call this method
		$this->request->onlyAllow('post','put');

		$saved = $this->Promise->approve();

		if ($saved)
		{ // successfully approved the promise and related models
			$this->Alert->success('Promise proposal has been marked as "approved"');
		}
		else
		{ // failed to approve the promise and/or related models
			$this->Alert->error('The Promise proposal could not be marked as "approved"');
		}
		// FIXME
		return $this->redirect(['action' => 'index']);
	}

	public function reject($id = null)
	{
		// set the promise id
		$this->Promise->id = $id;

		// safety check
		if (!$this->Promise->exists())
		{
			throw new NotFoundException('Invalid promise');
		}

		// limit the http methods allowed to call this method
		$this->request->onlyAllow('post','put','delete');
		$saved = $this->Promise->reject();

		if ($saved)
		{ // successfully approved the promise and related models
			$this->Alert->success('Promise proposal has been marked as "rejected"');
		}
		else
		{ // failed to approve the promise and/or related models
			$this->Alert->error('The Promise proposal could not be marked as "rejected"');
		}
		// FIXME
		return $this->redirect(['action' => 'index']);
	}

	/**
	 * CRON promise expiration date
	 *
	 * @param string promise_id 
	 * @param string cron_key
	 */
	public function promiseExpirationDate($promise_id = null,$cron_key = false) {
		
		// if called by a CRON JOB
		if ($this->RequestHandler->accepts('json')) {
			if (!$this->isCronAllowed($cron_key)) {
				$return = false;

			}
		}

		// set the promise id
		$this->Promise->id = $promise_id;

		// safety check
		if (!$this->Promise->exists())
		{
			throw new NotFoundException('Invalid promise');
		}
		
		// calculate if the promise has been fulfilled
		if ($this->Promise->isFulfilled($promise_id) === true) {
			// if approved promise has been fulfilled
			
			// email finance that promise has been fulfilled

			// email customer that promise has been fulfilled
			$this->Promise->sendPromiseNotFulfilledCustomer($promise_id);
			
		} else {
			// approved promise has not been fulfilled

			// email finance that promise has been fulfilled

			// email customer that promise has been fulfilled
			
		}

		// if email sent
		$return = true;

		$this->set(compact('return'));
		$this->set('_serialize', array('return'));

	}
}

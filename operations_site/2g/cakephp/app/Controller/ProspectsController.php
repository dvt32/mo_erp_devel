<?php
App::uses('AppController', 'Controller');
/**
 * Prospects Controller
 *
 * @property Prospect $Prospect
 * @property PaginatorComponent $Paginator
 */
class ProspectsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {

		// build query
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query, array(
				'fulltext' => array(
					'first_name',
					'last_name',
					'full_name',
					'email_address',
					'company',
				),
				'ignore' => array('followup')
			));

			$followup = @$this->request->query['followup'];
			if (!empty($followup)) {
				switch ($followup) {
					case 'today':
						$conditions['follow_up_date'] = date('Y-m-d');
						break;
					case 'tomorrow':
						$conditions['follow_up_date'] = date('Y-m-d',strtotime('tomorrow'));
						break;
					case 'upcoming':
						$conditions['follow_up_date >='] = date('Y-m-d');
						break;
					case 'past':
						$conditions['follow_up_date <'] = date('Y-m-d');
						break;
				}
			}

			// filter by query
			$querystring = @$this->request->query['q'];
			$conditions['OR'][]['Prospect.sales_stage ILIKE'] = "%{$querystring}%";
			$conditions['OR'][]['Prospect.sales_status ILIKE'] = "%{$querystring}%";

			$this->request->data['Prospect'] = $this->request->query;
		}
		
		// fetch rows from datasource
		$sales_statuses = $this->Prospect->sales_statuses;
		$sales_stages = $this->Prospect->sales_stages;

		$this->Prospect->order = 'Prospect.creation_dt DESC';
		$this->Prospect->recursive = 0;

		$buildings = $this->Prospect->Building->find('list', array(
			'conditions' => array(
				'Building.type' => 'officesuite',
				'Building.is_active' => true
			),
			'fields' => array(
				'id',
				'short_global_label',
				'city'
			),
			'order' => 'Building.short_global_label ASC'
		));

		$this->Paginator->settings = array('Prospect' => compact('conditions','contain','order'));
		$prospects = $this->Paginator->paginate();
		$this->set(compact('prospects', 'sales_statuses', 'sales_stages', 'buildings'));

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// POST request
		if ($this->request->is('post')) {

			$data = $this->request->data;
			$data['SalesTag'] = $data['SalesTag']['SalesTag'];

			$this->Prospect->create();
			if ($this->Prospect->saveAssociated($data)) {
				$this->Session->setFlash(__('The prospective customer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prospective customer could not be saved. Please, try again.'));
			}
		}

		$this->uses[] = 'Building';
		$buildings = $this->Building->officeSuites();
		$stages = $this->Prospect->SalesStage->find('list', array('order' => 'name ASC')); // FIXME
		$stages = array_self_combine($stages);
		$tags = $this->Prospect->SalesTag->find('list');

		$this->set(compact('buildings','stages','tags'));
	}



/**
 * convert method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function convert($id = null) {
		return $this->redirect(array('controller' => 'licenseAgreements', 'action' => 'add', $id));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$this->Prospect->id = $id;

		// safety check prospect
		if (!$this->Prospect->exists()) {
			throw new NotFoundException(__('Invalid prospective customer'));
		}

		// fetch existing prospect
		$prospect = $this->Prospect->find('first', array(
			'conditions' => array('Prospect.id' => $id),
			'contain' => array(
				'SalesInteraction',
				'SalesTag' => array('fields' => array('id'))
			)
		));

		// GET request
		if ($this->request->is('get')) {
			$this->request->data['Prospect'] = $prospect['Prospect'];
			$this->request->data['SalesTag'] = $prospect['SalesTag'];
		}

		// POST|PUT request
		if ($this->request->is(array('post', 'put'))) {

			$data = $this->request->data;
			$saved = false;

			// reindex for habtm save
			$data['SalesTag'] = $data['Prospect']['SalesTag'];
			unset($data['Prospect']['SalesTag']);

			// limit fields to save
			$fieldList = array(
				'Prospect' => array('id','follow_up_date','reminder_email_address','sales_status','sales_stage'),
			);
			$salesInteraction = filter_recursive($data['SalesInteraction']);
			if (!empty($salesInteraction)) {
				$fieldList['SalesInteraction'] = array('id','interaction_type','interaction_dt','notes','modifier_id');
				$saved = $this->Prospect->saveAssociated($data,compact('fieldList'));
			}
			else {
				unset($data['SalesInteraction']);
				$saved = $this->Prospect->save($data,compact('fieldList'));
			}

			// attempt save
			if ($saved) {
				$this->Alert->success(__('The prospective customer has been saved.'));

				// fetch existing prospect
				$prospect = $this->Prospect->find('first', array(
					'conditions' => array('Prospect.id' => $id),
					'contain' => array('SalesInteraction','SalesTag')
				));

			} else {
				$this->Alert->error(__('The prospective customer could not be saved. Please, try again.'));
			}
		}

		$stages = $this->Prospect->SalesStage->find('list', array('order' => 'name ASC')); // FIXME
		$stages = array_self_combine($stages);
		$tags = $this->Prospect->SalesTag->find('list');

		$this->set(compact('prospect','stages','tags'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Prospect->id = $id;
		if (!$this->Prospect->exists()) {
			throw new NotFoundException(__('Invalid prospective customer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Prospect->delete()) {
			$this->Session->setFlash(__('The prospective customer has been deleted.'));
		} else {
			$this->Session->setFlash(__('The prospective customer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * Search for a prospective customer
 * 
 * @return void
 */
	public function search($fs_key = null) {

		// lookup params
		if (!empty($fieldset)) {
			$fieldset = $this->fieldsets[$fs_key];
			$conditions = @$fieldset['conditions'];
			$fields = @$fieldset['fields'];
			$contain = @$fieldset['contain'];
			$order = @$fieldset['order'];
			$limit = @$fieldset['limit'];
		}
		// default parameters
		else {
			$conditions = array();
			$fields = null;
			$contain = null;
			$order = "";
			$limit = "";
		}
		$alias = $this->Prospect->alias;


		// parse the query
		if (!empty($this->request->query)) {

			$query = @$this->request->query['query'];
			if (!empty($query)) {
				$conditions['OR']["{$alias}.first_name ILIKE"] = "%{$query}%";
				$conditions['OR']["{$alias}.last_name ILIKE"] = "%{$query}%";
				$conditions['OR']["{$alias}.company ILIKE"] = "%{$query}%";
			}

			$fields =       @$this->request->query['fields']  ?: $fields;
			$contain =      @$this->request->query['contain'] ?: $contain;
			$order =        @$this->request->query['order']   ?: $order;
			$limit =        @$this->request->query['limit']   ?: $limit;
		}

		// fetch matching prospects
		$prospects = $this->Prospect->find('all', compact('conditions','contain','order','fields','limit'));

		$suggestions = array();
		foreach($prospects as $prospect) {
			$suggestion = new stdClass();
			$suggestion->data = arrayToObject($prospect);
			$suggestion->value = "{$prospect['Prospect']['first_name']} {$prospect['Prospect']['last_name']} ({$prospect['Prospect']['company']})";
			$suggestions[] = $suggestion;
		}

		$this->set('suggestions', $suggestions);
		$this->set('_serialize', array('suggestions'));
	}

	private $fieldsets = array(
		'prospectsAdd' => array(
			'conditions' => array('first_name','last_name','company'),
			'fields' => array(
				'desired_location',
				'company',
				'first_name',
				'last_name',
				'title',
				'work_phone_number',
				'email_address',
				'address_1',
				'address_2',
				'city',
				'state',
				'zip_code'
			)
		)
	);
}

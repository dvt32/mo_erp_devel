<?php
App::uses('AppController', 'Controller');
/**
 * ProximityCardEdits Controller
 *
 * @property ProximityCardEdit $ProximityCardEdit
 * @property PaginatorComponent $Paginator
 */
class ProximityCardEditsController extends AppController {



/**
 * index method
 *
 * @return void
 */
	public function index() {

		// if filter params are set...
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query, array(
				'ignore' => array('floor_id')
			));

			// workaround for datatype numbers only
			$conditions['OR'][]['CAST(ProximityCardEdit.number AS TEXT) LIKE'] = "%{$this->request->query['q']}%";
			$conditions['OR'][]['CustomerAccount.name ILIKE'] = "%{$this->request->query['q']}%";

			// set form input values to passed params
			$this->request->data['ProximityCardEdit'] = $this->request->query;

			$floor_id = @$this->request->query['floor_id'];
			if (!empty($floor_id)) {
				$conditions += array('CustomerAccount.primary_floor_id' => $floor_id);
			}
		}

		// Dropdown list
		$action_requesteds = $this->ProximityCardEdit->action_requesteds;
		$causes = $this->ProximityCardEdit->causes;

		$this->ProximityCardEdit->recursive = 0;
		$this->ProximityCardEdit->order = 'ProximityCardEdit.creation_dt DESC';
		$this->Paginator->settings = array('ProximityCardEdit' => compact('conditions','contain','order'));

		$proximityCardEdits = $this->Paginator->paginate();
		$this->uses[] = 'Floor';
		$floors = $this->Floor->listActiveOfficeFloors();
		$this->set(compact('proximityCardEdits', 'types', 'isActive', 'action_requesteds', 'causes', 'floors'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProximityCardEdit->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		$options = array(
			'conditions' => array(
				'ProximityCardEdit.' . $this->ProximityCardEdit->primaryKey => $id
			),
			'recursive' => 1
		);
		$this->set('proximityCardEdit', $this->ProximityCardEdit->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProximityCardEdit->create();
			if ($this->ProximityCardEdit->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card edit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
			}
		}
		// list of all active customer names
		$customers = $this->ProximityCardEdit->Customer->find('list', array(
			'fields' => array(
				'Customer.id', 
				'Customer.full_name'
				),
			'conditions' => array(
				'Customer.is_active' => true)
			)
		);
		// list of access levels
		$accessLevels = $this->ProximityCardEdit->AccessLevel->find('list');
		$this->set(compact('customers', 'accessLevels'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProximityCardEdit->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProximityCardEdit->save($this->request->data)) {
				$this->Session->setFlash(__('The proximity card edit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProximityCardEdit.' . $this->ProximityCardEdit->primaryKey => $id));
			$this->request->data = $this->ProximityCardEdit->find('first', $options);
		}
		$customerAccounts = $this->ProximityCardEdit->CustomerAccount->find('list');
		$customers = $this->ProximityCardEdit->Customer->find('list');
		$accessLevels = $this->ProximityCardEdit->AccessLevel->find('list');
		$this->set(compact('customerAccounts', 'customers', 'accessLevels'));
		
	}


/**
 * reject method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function reject($id = null) {
		if (!$this->ProximityCardEdit->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		if ($this->request->is(array('post', 'put'))) {

			$options = array(
				'conditions' => array(
					'ProximityCardEdit.id' => $id
				)
			);
			$proxcard = $this->ProximityCardEdit->find('first', $options);

			$proxcard['ProximityCardEdit']['rejected'] = true;
			// $proxcard['ProximityCardEdit']['rejected'] = date(DATETIME_FORMAT);

			if ($this->ProximityCardEdit->save($proxcard)) {
				$this->Session->setFlash(__('Access Card Request has been rejected'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
				return $this->redirect(array('action' => 'index'));
			}
		}
	}


/**
 * approve method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function approve($id = null) {
			if (!$this->ProximityCardEdit->exists($id)) {
				throw new NotFoundException(__('Invalid proximity card edit'));
			}
			if ($this->request->is(array('post', 'put'))) {

				$options = array(
					'conditions' => array(
						'ProximityCardEdit.id' => $id
					)
				);
				$proxcard = $this->ProximityCardEdit->find('first', $options);

				$proxcard['ProximityCardEdit']['completed'] = true;
				// $proxcard['ProximityCardEdit']['rejected'] = date(DATETIME_FORMAT);

				if ($this->ProximityCardEdit->save($proxcard)) {
					$this->Session->setFlash(__('The proximity card edit has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The proximity card edit could not be saved. Please, try again.'));
					return $this->redirect(array('action' => 'index'));
				}
			}
		}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProximityCardEdit->id = $id;
		if (!$this->ProximityCardEdit->exists()) {
			throw new NotFoundException(__('Invalid proximity card edit'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProximityCardEdit->delete()) {
			$this->Session->setFlash(__('The proximity card edit has been deleted.'));
		} else {
			$this->Session->setFlash(__('The proximity card edit could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

<?php
App::uses('AppController', 'Controller');



/**
 * ProximityCards Controller
 *
 * @property ProximityCard $ProximityCard
 * @property PaginatorComponent $Paginator
 */
class ProximityCardsController extends AppController {


	public $uses = array('ProximityCard', 'Customer', 'CustomerAccount', 'IntellimGroup');



/**
 * index method
 *
 * @return void
 */
	public function index() {


		// if filter params are set...
		if (!empty($this->request->query)) {

			$conditions = $this->filterByQuery($this->request->query);

			// workaround for datatype numbers only
			$conditions['OR'][]['CAST(ProximityCard.number AS TEXT) LIKE'] = "%{$this->request->query['q']}%";
			$conditions['OR'][]['CustomerAccount.name ILIKE'] = "%{$this->request->query['q']}%";

			// set form input values to passed params
			$this->request->data['ProximityCard'] = $this->request->query;
		}


		// if status is not specified
		if (!isset($conditions['ProximityCard.status ILIKE'])) {

			// default paginator settings
			$conditions['ProximityCard.technology'] = 'proximity';
			$conditions['ProximityCard.status'] = 'active';

		}

		// Dropdown list
		$statuses = $this->ProximityCard->statuses;
		$technologies = $this->ProximityCard->technologies;

		// Building - Floor array
		$floors = $this->ProximityCard->Floor->find('list', array(
			'fields' => array('id', 'short_global_label'),
			'conditions' => array('is_active' => true)
		));

		$this->ProximityCard->order = 'ProximityCard.creation_dt DESC';
		$this->ProximityCard->recursive = 0;
		$this->Paginator->settings = array('ProximityCard' => compact('conditions','contain','order'));

		$proximityCards = $this->Paginator->paginate();
		$this->set(compact('proximityCards', 'types', 'isActive', 'statuses', 'technologies', 'floors'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProximityCard->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card'));
		}
		$options = array(
			'conditions' => array(
				'ProximityCard.' . $this->ProximityCard->primaryKey => $id
			),
			'recursive' => 1
		);
		$this->set('proximityCard', $this->ProximityCard->find('first', $options));
	}


/**
 * approve method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function approve($id = null) {

			$this->uses[] = 'ProximityCardEdit';
			if (!$this->ProximityCardEdit->exists($id)) {
				throw new NotFoundException(__('Invalid proximity card edit'));
			}


			// HANDLE POST REQUEST
			if ($this->request->is('post')) {

				$post_data = $this->request->data;

				// If under smart card system
				if ($post_data['ProximityCard']['technology'] == 'smart') {

					// save to psql database
					$this->ProximityCard->create();
					if ($this->ProximityCard->save($post_data)) {
						$this->Session->setFlash(__('Access Card has been created! Please Enter in the details in Smart Card System. '));
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
					}

				// If under intellim system
				} else {

					// Intelim system deactivate
					if ($post_data['ProximityCard']['action_requested'] == 'deactivate') {

						$intellim_id = $post_data['ProximityCard']['intellim_id'];

						// Send request to lemur
						try {

							// response from intellim api call
							$response = $this->ProximityCard->deleteIntellimPerson($intellim_id);

							// set intellim id
							$post_data['ProximityCard']['intellim_id'] = $response['RecordId'];

							// set inactive in the database
							$proximity_card['ProximityCard']['status'] = 'inactive';
							$proximity_card['ProximityCard']['is_active'] = false;

							// save to psql database
							if ($this->ProximityCard->save($proximity_card)) {
								$this->Session->setFlash(__('The proximity card has been removed in the IntelliM System.'));
							} else {
								$this->Session->setFlash(__('The proximity card could not be deleted. Please, try again.'));
							}
							return $this->redirect(array('action' => 'index'));

						}	
						catch (Exception $e) {
							$this->Session->setFlash($e->getMessage());
						}

					// Activate card
					} else {

						// IntelliM Groups
						$groupsIntelliM = array();
						if (count($post_data['ProximityCard']['group_intellim_id']) == 1) {
							$groupsIntelliM = $post_data['ProximityCard']['group_intellim_id'][0].',94';
						} else {
							$groupsIntelliM = implode(",", $post_data['ProximityCard']['group_intellim_id']);
						}
						$post_data['ProximityCard']['group_intellim_id'] = $groupsIntelliM;


						$post_data['ProximityCard']['card_number'] = $post_data['ProximityCard']['site_code']."-".$post_data['ProximityCard']['number'];

						$person = $this->ProximityCard->Customer->find('first', array(
							'conditions' => array('Customer.id' => $post_data['ProximityCard']['person_id']),
							'contain' => 'CustomerAccount'
						));

						// Required Info
						$person_array = array(
							'PersonalInfo.FirstName' => $person['Customer']['first_name'],
							'PersonalInfo.LastName' => $person['Customer']['last_name'],
							'PersonalInfo.EmployeeId' => $person['CustomerAccount']['name'],
							'BadgeInfo.ActivationDate' => date("m/d/Y"),
							'BadgeInfo.ActivationTime' => date("h:i A"),
							'BadgeInfo.SiteCode' => $post_data['ProximityCard']['site_code'],
							'BadgeInfo.CardIssueCode' => $post_data['ProximityCard']['number'],
							'GroupInfo.GroupsToAdd' => $post_data['ProximityCard']['group_intellim_id']
						);

						// Send request to lemur
						try {

							// response from intellim api call
							$response = $this->ProximityCard->createIntellimPerson($person_array);

							// set intellim id
							$post_data['ProximityCard']['intellim_id'] = $response['RecordId'];

							// save to psql database
							$this->ProximityCard->create();
							if ($this->ProximityCard->save($post_data)) {
								$this->Session->setFlash(__('Access Card has been created!'));
								return $this->redirect(array('action' => 'index'));
							} else {
								$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
							}

						}	
						catch (Exception $e) {
							$this->Session->setFlash($e->getMessage());
						}
					}

				}

			}

			// Choose a person
			$customerAccounts = $this->CustomerAccount->find('list', array(
				'conditions' => array(
					'is_active' => true
				),
				'order' => 'name ASC'
			));

			// get list of access levels
			$accessLevels = $this->ProximityCard->AccessLevel->find('list');

			// Building - Floor array
			$floors = $this->ProximityCard->Floor->Building->find('list', array(
				'fields' => array('id', 'short_global_label'),
				'conditions' => array('is_active' => true)
			));

			// get a list of groups from IntelliM
			$groups = $this->IntellimGroup->find('list', array(
				'fields' => array('intellim_id', 'name')
			));


			$this->set(compact('customerAccounts', 'accessLevels', 'floors', 'groups'));
			$proximityCardEdit = $this->ProximityCardEdit->find('first', array(
					'conditions' => array('ProximityCardEdit.id' => $id),
					'contain' => array('Customer', 'CustomerAccount', 'AccessLevel')
				)
			);

			// Choose a person
			$customerAccounts = $this->CustomerAccount->find('list', array(
				'conditions' => array(
					'is_active' => true
				)
			));

			// get list of access levels
			$accessLevels = $this->ProximityCard->AccessLevel->find('list');

			// Building - Floor array
			$floors = $this->ProximityCard->Floor->Building->find('list', array(
				'fields' => array('id', 'short_global_label'),
				'conditions' => array(
					'is_active' => true,
					'type' => 'officesuite'
				)
			));

			// get a list of groups from IntelliM
			$groups = $this->IntellimGroup->find('list', array(
				'fields' => array('intellim_id', 'name')
			));
			$this->set(compact('proximityCardEdit', 'customerAccounts', 'accessLevels', 'floors', 'groups'));
	}
/**
 * add method
 * 
 * @todo check if person/proximity card exists
 * @return void
 */
	public function add() {

		// HANDLE POST REQUEST
		if ($this->request->is('post')) {

			$post_data = $this->request->data;

			// If under smart card system
			if ($post_data['ProximityCard']['technology'] == 'smart') {

				// save to psql database
				$this->ProximityCard->create();
				if ($this->ProximityCard->save($post_data)) {
					$this->Session->setFlash(__('Access Card has been created! Please Enter in the details in Smart Card System. '));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
				}

			// If under intellim system
			} else {

				// IntelliM Groups
				$groupsIntelliM = array();
				if (count($post_data['ProximityCard']['group_intellim_id']) == 1) {
					$groupsIntelliM = $post_data['ProximityCard']['group_intellim_id'][0].',94';
				} else {
					$groupsIntelliM = implode(",", $post_data['ProximityCard']['group_intellim_id']);
				}
				$post_data['ProximityCard']['group_intellim_id'] = $groupsIntelliM;


				$post_data['ProximityCard']['card_number'] = $post_data['ProximityCard']['site_code']."-".$post_data['ProximityCard']['number'];

				$person = $this->ProximityCard->Customer->find('first', array(
					'conditions' => array('Customer.id' => $post_data['ProximityCard']['person_id']),
					'contain' => 'CustomerAccount'
				));

				// Required Info
				$person_array = array(
					'PersonalInfo.FirstName' => $person['Customer']['first_name'],
					'PersonalInfo.LastName' => $person['Customer']['last_name'],
					'PersonalInfo.EmployeeId' => $person['CustomerAccount']['id'],
					'BadgeInfo.ActivationDate' => date("m/d/Y"),
					'BadgeInfo.ActivationTime' => date("h:i A"),
					'BadgeInfo.SiteCode' => $post_data['ProximityCard']['site_code'],
					'BadgeInfo.CardIssueCode' => $post_data['ProximityCard']['number'],
					'GroupInfo.GroupsToAdd' => $post_data['ProximityCard']['group_intellim_id']
				);

				// Send request to lemur
				try {

					// response from intellim api call
					$response = $this->ProximityCard->createIntellimPerson($person_array);

					// set intellim id
					$post_data['ProximityCard']['intellim_id'] = $response['RecordId'];

					// save to psql database
					$this->ProximityCard->create();
					if ($this->ProximityCard->save($post_data)) {
						$this->Session->setFlash(__('Access Card has been created!'));
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
					}

				}	
				catch (Exception $e) {
					$this->Session->setFlash($e->getMessage());
				}

			}

		}

		// Choose a person
		$customerAccounts = $this->CustomerAccount->find('list', array(
			'conditions' => array(
				'is_active' => true
			),
			'order' => 'name ASC'
		));

		// get list of access levels
		$accessLevels = $this->ProximityCard->AccessLevel->find('list');

	    // Building - Floor array
		$floors = $this->ProximityCard->Floor->Building->find('list', array(
			'fields' => array('id', 'short_global_label'),
			'conditions' => array(
				'is_active' => true,
				'type' => 'officesuite')));

		// get a list of groups from IntelliM
		$groups = $this->IntellimGroup->find('list', array(
			'fields' => array('intellim_id', 'name')
		));


		$this->set(compact('customerAccounts', 'accessLevels', 'floors', 'groups'));
	}


/**
 * 
 * 
 * 
 */
	public function addcards() {

		// POST request
		if ($this->request->is('post')) {
			$data = $this->request->data;
			debug($data);

			$range_start = $data['ProximityCard']['range_start'];
			$range_end = $data['ProximityCard']['range_end'];

			$saveme = array();
			foreach (range($range_start,$range_end) as $i) {
				$saveme[$i] = $data['ProximityCard'];
				$saveme[$i]['number'] = $i;
			}
			// debug($saveme);
			// debug($this->ProximityCard->set(array('ProximityCard'=> $saveme[1])));
			// debug($this->ProximityCard->validates() ?: $this->ProximityCard->validationErrors);

			if ($this->ProximityCard->saveMany($saveme)) {
				$num = $range_end - $range_start;
				$this->Alert->success("{$num} proximity cards ({$range_start} - {$range_end}) have been saved.");
			} else {
				$this->Alert->error("The proximity cards could not be saved.");
			}
			exit;
		}

		$this->uses[] = 'Floor';
		$floors = $this->Floor->find('list', array(
			'fields' => array('Floor.id','Floor.short_global_label','Building.short_global_label'),
			'contain' => 'Building.short_global_label',
			'order' => 'Floor.short_global_label'
		));
		$this->set(compact('floors'));
	}

/**
 * addcards method
 *
 * @todo for loop - not a priority
 * @return void
 */
	public function addcards2() {

		if ($this->request->is('post')) {

			$post_data = $this->request->data;
			$saveArray = $this->ProximityCard->saveCardsFromTo($post_data['ProximityCard']['range_start'], $post_data['ProximityCard']['range_end'], $post_data);

			// debug($saveArray); die;

			// saves
			$saved = true;

			foreach ($saveArray as $key => $value) {

				$this->ProximityCard->create();
				$save = $this->ProximityCard->save($value);

				debug($save);
				debug($value);

				if ($save == false) {
					$saved = false;
				}

			}
			

			// debug($saveArray);
			// debug($save);

			if ($saved == true) {
				$this->Session->setFlash(__('All cards have been added!'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Cards could not be saved. Please, try again.'));
			}

			exit;
		}

		// Building - Floor array
		$floors = $this->ProximityCard->Floor->find('list', array(
			'fields' => array('id', 'short_global_label'),
			'conditions' => array(
				'is_active' => true,
			)
		));

		$this->set(compact('floors'));
	}

/**
 * lookup method
 * @param string $account_id
 * @return json array of keys id, values full names
 */
	public function lookup($account_id) {

		$customers = $this->Customer->find('list', array(
			'conditions' => array(
				'cust_acct_id' => $account_id,
				'is_active' => true
			),
			'fields' => array('Customer.id', 'Customer.full_name'),
		));
		echo json_encode($customers);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->ProximityCard->exists($id)) {
			throw new NotFoundException(__('Invalid proximity card'));
		}

		// HANDLE PUT/POST REQUEST
		if ($this->request->is(array('post', 'put'))) {

			$post_data = $this->request->data;

			// If under smart card system
			if ($post_data['ProximityCard']['technology'] == 'smart') {

				// save to psql database
				$this->ProximityCard->create();
				if ($this->ProximityCard->save($post_data)) {
					$this->Session->setFlash(__('Access Card has been modified! Please Enter in the details into the Smart Card System. '));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
				}

			// If under intellim system
			} else {

				// Groups to Remove logic
				$posted_groups = $post_data['ProximityCard']['group_intellim_id'];
				$before_groups = explode(",",$post_data['ProximityCard']['group_intellim_id']);

				// IntelliM Groups - for removing
				if (!is_array($before_groups)) {
					$before_groups = array();
				} else if (!is_array($posted_groups)) {
					$posted_groups = array();
				}
				$remove_groups = array_diff($before_groups, $posted_groups);
				

				if (!empty($remove_groups)) {

					$groupsIntelliM = array();
					if (count($remove_groups) == 1) {
						$remove_groups = array_values($remove_groups);
						$groupsIntelliM = $remove_groups[0].',94';
					} else {
						$groupsIntelliM = implode(",", $remove_groups);
					}
					$remove_groups = $groupsIntelliM;
				} else {
					$remove_groups = '';
				}
			

				// IntelliM Groups - for adding
				$add_groups = array_diff($posted_groups, $before_groups);

				if (!empty($add_groups)) {
					$addingGroups = array();
					if (count($add_groups) == 1) {
						$add_groups = array_values($add_groups);
						$addingGroups = $add_groups[0].',94';
					} else {
						$addingGroups = implode(",", $add_groups);
					}
					$add_groups = $addingGroups;
				} else {
					$add_groups = '';
				}

				// Construct proper card number to match intellim
				$post_data['ProximityCard']['card_number'] = $post_data['ProximityCard']['site_code']."-".$post_data['ProximityCard']['number'];

				// Get person first and last names
				$person = $this->ProximityCard->Customer->find('first', array(
					'conditions' => array('Customer.id' => $post_data['ProximityCard']['person_id']),
					'contain' => 'CustomerAccount'
				));

				// Get proximity card records
				$prox_card = $this->ProximityCard->find('first', array(
					'conditions' => array('ProximityCard.id' => $id)
				));

				// Update records to be saved
				$prox_card['ProximityCard']['number'] = $post_data['ProximityCard']['number'];
				$prox_card['ProximityCard']['type'] = $post_data['ProximityCard']['type'];
				$prox_card['ProximityCard']['status'] = $post_data['ProximityCard']['status'];
				$prox_card['ProximityCard']['account_id'] = $post_data['ProximityCard']['account_id'];
				$prox_card['ProximityCard']['person_id'] = $post_data['ProximityCard']['person_id'];
				$prox_card['ProximityCard']['note'] = $post_data['ProximityCard']['note'];
				$prox_card['ProximityCard']['access_level_id'] = $post_data['ProximityCard']['access_level_id'];
				$prox_card['ProximityCard']['floor_id'] = $post_data['ProximityCard']['floor_id'];
				$prox_card['ProximityCard']['technology'] = $post_data['ProximityCard']['technology'];
				$prox_card['ProximityCard']['site_code'] = $post_data['ProximityCard']['site_code'];
				$prox_card['ProximityCard']['group_intellim_id'] = implode(",", $posted_groups);
				
				// if the intellim_id is not set, it'll create a new record
				if (isset($prox_card['ProximityCard']['intellim_id'])) {
					// Construct Person Array
					$person_array = array(
						'Id' => $prox_card['ProximityCard']['intellim_id'],
						'PersonalInfo.FirstName' => $person['Customer']['first_name'],
						'PersonalInfo.LastName' => $person['Customer']['last_name'],
						'BadgeInfo.SiteCode' => $post_data['ProximityCard']['site_code'],
						'BadgeInfo.CardIssueCode' => $post_data['ProximityCard']['number'],
						'GroupInfo.GroupsToAdd' => $add_groups,
						'GroupInfo.GroupsToRemove' => $remove_groups
					);

					// Send request to lemur
					try {

						// response from intellim api call
						$response = $this->ProximityCard->updateIntellimPerson($person_array);

						// save to psql database
						$this->ProximityCard->create();
						if ($this->ProximityCard->save($prox_card)) {
							$this->Session->setFlash(__('Access Card has been edit successfully!'));
							return $this->redirect(array('action' => 'index'));
						} else {
							$this->Session->setFlash(__('The proximity card could not be saved. Please, try again.'));
						}

					}	
					catch (Exception $e) {
						$this->Session->setFlash($e->getMessage());
					}
				} else {
					$this->Session->setFlash(__('Access Card cannot be edited because it does not exist in the IntelliM database'));
				}
			}


		}

		// GET REQUEST
		$options = array('conditions' => array('ProximityCard.' . $this->ProximityCard->primaryKey => $id));
		$this->request->data = $this->ProximityCard->find('first', $options);

		// IntelliM Groups
		if (!empty($this->request->data['ProximityCard']['group_intellim_id'])) {
			$this->request->data['ProximityCard']['group_intellim_id'] = explode(",", $this->request->data['ProximityCard']['group_intellim_id']);
			if (count($this->request->data['ProximityCard']['group_intellim_id']) == 2) {
				// unset the place holder group id 94
				if(($key = array_search('94', $this->request->data['ProximityCard']['group_intellim_id'])) !== false) {
				    unset($this->request->data['ProximityCard']['group_intellim_id'][$key]);
				}
			}
		} else {
			$this->request->data['ProximityCard']['group_intellim_id'] = null;
		}
		
		// Choose a person
		$customerAccounts = $this->CustomerAccount->find('list', array(
			'conditions' => array(
				'is_active' => true
			),
			'order' => 'name ASC'
		));
		// Choose a person
		$customers = $this->Customer->find('list', array(
			'conditions' => array(
				'is_active' => true
			)
		));

		// get list of access levels
		$accessLevels = $this->ProximityCard->AccessLevel->find('list');

		// Building - Floor array
		$floors = $this->ProximityCard->Floor->Building->find('list', array(
			'fields' => array('id', 'short_global_label'),
			'conditions' => array('is_active' => true)
		));

		if ($this->request->data['ProximityCard']['technology'] == 'proximity') {
			// get a list of groups from IntelliM
			$groups = $this->IntellimGroup->find('list', array(
				'fields' => array('intellim_id', 'name')
			));


			$selectedGroups = $this->request->data['ProximityCard']['group_intellim_id'];

			$tmp = array();
			foreach ($selectedGroups as $group_key) {
				if (isset($groups[$group_key])) {
					$tmp[$group_key] = $groups[$group_key];
					unset($groups[$group_key]);
				}
			}
			$selectedGroups = $tmp;
		}

		$groups = null;
		$selectedGroups = null;

		$this->set(compact('customerAccounts', 'accessLevels', 'floors', 'customers', 'groups', 'selectedGroups'));

		// END GET REQUEST
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		$this->ProximityCard->id = $id;
		if (!$this->ProximityCard->exists()) {
			throw new NotFoundException(__('Invalid proximity card'));
		}

		$proximity_card = $this->ProximityCard->find('first', array(
			'conditions' => array('ProximityCard.id' => $id)
		));

		// If under smart card system
		if ($proximity_card['ProximityCard']['technology'] == 'smart') {

			// set inactive in the database
			$proximity_card['ProximityCard']['status'] = 'inactive';
			$proximity_card['ProximityCard']['is_active'] = false;

			// save to psql database
			if ($this->ProximityCard->save($proximity_card)) {
				$this->Session->setFlash(__('The Smart Card record as been removed. Please remove them from the Smart Card System as well. '));
			} else {
				$this->Session->setFlash(__('The Access Card could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));

		// If under intellim system
		} else {

			$intellim_id = $proximity_card['ProximityCard']['intellim_id'];

			// Send request to lemur
			try {

				// response from intellim api call
				$response = $this->ProximityCard->deleteIntellimPerson($intellim_id);

				// set intellim id
				$post_data['ProximityCard']['intellim_id'] = $response['RecordId'];

				// set inactive in the database
				$proximity_card['ProximityCard']['status'] = 'inacitve';
				$proximity_card['ProximityCard']['is_active'] = false;

				// save to psql database
				if ($this->ProximityCard->save($proximity_card)) {
					$this->Session->setFlash(__('The proximity card has been removed in the IntelliM System.'));
				} else {
					$this->Session->setFlash(__('The proximity card could not be deleted. Please, try again.'));
				}
				return $this->redirect(array('action' => 'index'));

			}	
			catch (Exception $e) {
				$this->Session->setFlash($e->getMessage());
			}
		}

	}


/**
 * Clear person_id and account_id
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function clear($id = null) {

		$this->ProximityCard->id = $id;
		if (!$this->ProximityCard->exists()) {
			throw new NotFoundException(__('Invalid proximity card id'));
		}

		$this->request->onlyAllow('post');
		if ($this->ProximityCard->clear()) {
			$this->Alert->success(__('The person and account associated with this proximity card have been cleared'));
		} else {
			$this->Alert->error('The person and account information could not be cleared from this card');
		}
		return $this->redirect(array('action' => 'index'));
	}



/**
 * test method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function test($id = null) {
		// $this->ProximityCard->createIntellimPerson();
		// echo $this->ProximityCard->createIntellimPerson();
		// $this->ProximityCard->deleteIntellimPerson(1526);
	}



}


<?php
App::uses('AppController', 'Controller');
/**
 * ReceivedPayments Controller
 *
 * @property ReceivedPayment $ReceivedPayment
 * @property PaginatorComponent $Paginator
 */
class ReceivedPaymentsController extends AppController {

	public $uses = array('ReceivedPayment','CustomerAccount');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ReceivedPayment->recursive = 0;
		// TODO add 'modified' DATETIME column to recv_payment_tbl
		$this->Paginator->settings = array('ReceivedPayment' => array('order' => 'ReceivedPayment.modified DESC'));
		$receivedPayments = $this->Paginator->paginate();
		
		
		$this->set('receivedPayments', $receivedPayments);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ReceivedPayment->exists($id)) {
			throw new NotFoundException(__('Invalid received payment'));
		}
		$options = array(
			'conditions' => array('ReceivedPayment.' . $this->ReceivedPayment->primaryKey => $id),
			'contain' => array(
				'CustomerAccount' => array('id','name'),
				'Employee' => array('id','full_name'),
				'Deposit' => array('id','deposit_dt')
			)
		);
		$this->set('receivedPayment', $this->ReceivedPayment->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($cust_acct_id = null) {

		$this->CustomerAccount->id = $cust_acct_id;

		if (!$this->CustomerAccount->exists()) {
			$this->Alert->error(__('Invalid customer account id'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post')) {

			// prep
			$data = $this->request->data;

			// reset the received payment model
			$this->ReceivedPayment->create();

			// save the received payment
			if ($this->ReceivedPayment->save($data)) {

				// report success
				$this->Alert->success(__('The received payment has been saved.'));

				// redirect as get request
				$this->redirect($this->request->here);
			} else {

				// report failure
				$this->Session->setFlash(__('The received payment could not be saved. Please, try again.'));
			}
		}

		$customerAccount = $this->ReceivedPayment->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'PrimaryCustomer',
				'Service',
				'Floor',
				'BillingAmount' => array(
					// 'conditions' => array('balance >' => 0),
					'order' => 'due_date DESC',
					'limit' => 10
				)
			)
		));
		$employees = $this->ReceivedPayment->Employee->find('list');
		$deposits = $this->ReceivedPayment->Deposit->find('list');
		$paymentDestinations = $this->ReceivedPayment->destinations;

		$paymentMethods = $this->ReceivedPayment->paymentMethods;

		$this->set(compact('customerAccount', 'employees', 'deposits', 'paymentMethods','paymentDestinations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ReceivedPayment->id = $id;
		if (!$this->ReceivedPayment->exists()) {
			throw new NotFoundException(__('Invalid received payment'));
		}
		if ($this->request->is(array('post'))) {
			if ($this->ReceivedPayment->save($this->request->data)) {
				$this->Session->setFlash(__('The received payment has been edited.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The received payment could not be edited. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ReceivedPayment.' . $this->ReceivedPayment->primaryKey => $id));
			$this->request->data = $this->ReceivedPayment->find('first', $options);
		}
		$customerAccounts = $this->ReceivedPayment->CustomerAccount->find('list');
		$employees = $this->ReceivedPayment->Employee->find('list');
		$deposits = $this->ReceivedPayment->Deposit->find('list');
		$this->set(compact('customerAccounts', 'employees', 'deposits'));
	}

/**
 * cancel method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cancel($id = null) {
		$this->ReceivedPayment->id = $id;
		if (!$this->ReceivedPayment->exists()) {
			throw new NotFoundException(__('Invalid received payment'));
		}
		// if ($this->request->is(array('post'))) {
			if ($this->ReceivedPayment->cancel($id)) {
				$this->Session->setFlash(__('The received payment has been cancelled.'));
			} else {
				$this->Session->setFlash(__('The received payment could not be cancelled. Please, try again.'));
			}
		// }
		$next = $this->request->query('next') ?: array('action' => 'index');
		return $this->redirect($next);
	}

/**
 * remove method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function removeFromDeposit($id = null) {
		$this->ReceivedPayment->id = $id;
		if (!$this->ReceivedPayment->exists()) {
			throw new NotFoundException(__('Invalid received payment'));
		}
		if ($this->request->is(array('post'))) {
			if ($this->ReceivedPayment->removeFromDeposit($id)) {
				$this->Session->setFlash(__('The received payment has been removed.'));
			} else {
				$this->Session->setFlash(__('The received payment could not be removed. Please, try again.'));
			}
		}
		return $this->redirect($this->request->query('next') ?: array('action' => 'index'));
	}



/**
 * aging method
 *
 * @return void
 */
	public function aging($cust_acct_id = null) {

		$this->CustomerAccount->id = $cust_acct_id;

		if (!$this->CustomerAccount->exists()) {
			$this->Alert->error(__('Invalid customer account id'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post')) {

			// TODO fill in client_hostname, client_ip_addr

			$data = $this->request->data;

			$data['ReceivedPayment']['billing_amount_id'] = $data['ReceivedPayment']['billing_amount_id'] ?: null;

			$this->ReceivedPayment->create();
			if ($this->ReceivedPayment->save($data)) {
				$this->Session->setFlash(__('The received payment has been saved.'));
				$this->request->data = null;
			} else {
				$this->Session->setFlash(__('The received payment could not be saved. Please, try again.'));
			}
		}

		$customerAccount = $this->ReceivedPayment->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'contain' => array(
				'PrimaryCustomer',
				'Service',
				'Floor',
				'BillingAmount' => array(
					'conditions' => array('balance >' => 0),
					'order' => 'modified DESC',
					'limit' => false,
				),
				'BillingMemo' => array(
					'conditions' => array('is_active' => true, 'is_applied' => false),
					'order' => 'created DESC',
					'Modifier' => array('fields' => array('id','full_name'))
				)
			)
		));
		$employees = $this->ReceivedPayment->Employee->find('list');
		$deposits = $this->ReceivedPayment->Deposit->find('list');
		$paymentDestinations = $this->ReceivedPayment->destinations;

		$paymentMethods = $this->ReceivedPayment->paymentMethods;

		$this->set(compact('customerAccount', 'employees', 'deposits', 'paymentMethods','paymentDestinations'));
	}
}

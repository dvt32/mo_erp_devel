<?php
App::uses('AppController', 'Controller');
/**
 * Reservations Controller
 *
 * @property Reservation $Reservation
 */
class ReservationsController extends AppController {

/**
 * authorization rules
 * 
 * @var array
 */
	public $allowed = array(
		'primary' =>  array('week','month','upcoming','schedule','view','addajax','editajax','edit','delete','book','add','remove','feed'),
		'admin' =>    array('week','month','upcoming','schedule','view','addajax','editajax','edit','delete','book','add','remove','feed'),
		'normal' =>   array('week','month','upcoming','schedule','view','addajax','editajax','edit','delete','book','add','remove','feed'),
	);

	public $paginate = array(
		'limit' => 50,
		'order' => array('Reservation.start_dt' => 'desc')
  );

	/**
	 * Utility method. Set the timezone prior to timezone-sensitive functions
	 * 
	 * @return void
	 */
  private function setTimezone()
  {
		// customer account timezone
		$cust_acct_id = AuthComponent::user('cust_acct_id');
		$timezone = $this->Reservation->CustomerAccount->getTimezone($cust_acct_id);

		// set timezone
		date_default_timezone_set($timezone);
  }

/**
 * week method
 * @author John Jung
 * @param [CustomerAccount][id]
 * @return array reservations associated with that account
 */
	public function week() {

		$this->Reservation->recursive = 0;
		$start = date(DATETIME_FORMAT,strtotime('now'));
		$end = date(DATETIME_FORMAT,strtotime('+1 week'));
		$this->paginate['conditions'] = array(
			'Reservation.start_dt >=' => $start,
			'Reservation.start_dt <' => $end,
		);
		$week = $this->Paginator->paginate();
		// debug($week); exit;
		$this->set('reservations', $this->Paginator->paginate());
		$this->set('_serialize', array('reservations'));
	}


/**
 * month method
 * @author John Jung
 * @param [CustomerAccount][id]
 * @return array reservations associated with that account
 */
	public function month($room_id) {

		$start_time = $this->request->query('start') ?: strtotime('-2 months');
		$end_time = $this->request->query('end') ?: strtotime('+2 months');

		// filter by start/end
		$start_dt = date('Y-m-d H:i:s', $start_time);
		$end_dt = date('Y-m-d H:i:s', $end_time);

		$conditions = array(
			'Reservation.start_dt >=' => $start_dt,
			'Reservation.start_dt <' => $end_dt,
			'Reservation.room_id' => $room_id
		);

		$fields = array(
			'Reservation.id', 
			'Reservation.start_dt', 
			'Reservation.end_dt',
			'Reservation.room_id',
			'Reservation.note'
		);

		$contain = array(
			'CustomerAccount.id',
			'CustomerAccount.name'
		);

		$month = $this->Reservation->find('all', compact('conditions','contain','fields'));

		$timezone = $this->Reservation->Room->find('first', array(
			'conditions' => ['Room.id' => $room_id],
			'contain' => ['Floor.Building.timezone']
		));
		$timezone = $timezone['Floor']['Building']['timezone'];
		$shift = ($timezone == 'America/Chicago') ? '-1 hour' : null;

		foreach ($month as &$value) {

			if ($value['CustomerAccount']['id'] == 1  || $value['CustomerAccount']['id'] == null) {
				$value['CustomerAccount']['id'] = 1;
				$value['CustomerAccount']['name'] = 'Micro Office Operations';
			}

			if (!empty($shift))
			{
				// subtract 1 hour from start_dt
				$start_dt = DateTime::createFromFormat('Y-m-d H:i:sO', $value['Reservation']['start_dt']);
				$start_dt->modify($shift);
				$value['Reservation']['start_dt'] = $start_dt->format('Y-m-d H:i:sO');

				// subtract 1 hour from end_dt
				$end_dt = DateTime::createFromFormat('Y-m-d H:i:sO', $value['Reservation']['end_dt']);
				$end_dt->modify($shift);
				$value['Reservation']['end_dt'] = $end_dt->format('Y-m-d H:i:sO');
			}

			$value['Reservation']['start_nice'] = CakeTime::nice($value['Reservation']['start_dt']);
			$value['Reservation']['end_nice'] = CakeTime::nice($value['Reservation']['end_dt']);
		}

		$this->set('reservations', $month);
		$this->set('_serialize', array('reservations'));
	}


/**
 * upcoming method
 * @author John Jung
 * @param $cust_acct_id from AuthComponent
 * @return void
 */
	public function upcoming() {

		$customer_account_id = AuthComponent::user('cust_acct_id');

		$this->Reservation->recursive = 0;
		$now = date(DATETIME_FORMAT);
		
		$conditions = array('Reservation.account_id' => $customer_account_id);
		$contain = array('Room.Floor.label');
		$order = 'start_dt DESC';

		$this->Paginator->settings = array('Reservation' => compact('conditions','contain', 'order'));
		$reservations = $this->Paginator->paginate();

		$this->set(compact('reservations'));
		$this->set('_serialize', array('reservations'));
	}



/**
 * schedule method
 * @author John Jung
 * @return void
 */
	public function schedule($current_room = null) {

		$customer_account_id = AuthComponent::user('cust_acct_id');
		$this->Reservation->recursive = 0;

		$start = date(DATETIME_FORMAT,strtotime('now'));
		$end = date(DATETIME_FORMAT,strtotime('+1 week'));

		$this->uses[] = "CustomerAccount";
		$myWeek = $this->CustomerAccount->find('all', array(
			'conditions' => array('CustomerAccount.id' => $customer_account_id),
			'contain' => array(
				'Reservation' => array(
					'conditions' => array(
						'start_dt >=' => $start,
						'start_dt <' => $end
					)
				)
			)
		));
		$roomMinutesRemaining = $this->CustomerAccount->roomMinutesRemaining($this->Auth->user('cust_acct_id'));
		foreach ($myWeek as &$x) {
			$minutes_remaining = $this->CustomerAccount->roomMinutesRemaining($x['CustomerAccount']['id']);
			$x['CustomerAccount']['room_minutes_remaining'] = $minutes_remaining[0]['minutes_remaining'];
		}

		// find list of rooms relating to active user
		$result = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $this->Auth->user('cust_acct_id')),
			'fields' => null,
			'contain' => array(
				'Floor.building_id'
			)
		));
		$building_id = $result['Floor']['building_id'];

		$this->uses[] = 'Building';
		$building = $this->Building->find('first', array(
			'conditions' => array('Building.id' => $building_id),
			'contain' => array(
				'Floor.label',
				'Floor' => array(
					'fields' => 'label',
					'Room' => array(
						'fields' => array('id','name'),
						'conditions' => array('is_active' => true)
					)
				)
			)
		));

		$rooms = array();
		foreach ($building['Floor'] as $floor) {
			foreach ($floor['Room'] as $room) {
				$rooms[$room['id']] = "{$floor['label']} - {$room['name']}";
			}
		}

		$this->set(compact('building','rooms','myWeek','roomMinutesRemaining'));
		$this->set('current_room', $current_room);

	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Reservation->recursive = 0;
		$this->set('reservations', $this->Paginator->paginate());
	}


/**
 * addajax method
 * @author John Jung
 * @param ajax required request: room_id, start_dt, end_dt
 * @return void
 */
	public function addajax() {
		if ($this->request->is('post')) {

			$data = $this->request->data;

			$data['Reservation']['start_dt'] = $this->convertCalendarFormat($data['Reservation']['start_dt']);
			$data['Reservation']['end_dt'] = $this->convertCalendarFormat($data['Reservation']['end_dt']);

			// reset the reservation model
			$this->Reservation->create();
			echo $this->Reservation->save($data) ? 1 : 0;
		}
	}

/**
 * Converts either of two datetime formats to the proper timestamp+timezone format
 * 
 * @param string $original_dt
 * @return string timestamp with timezone
 */
	private function convertCalendarFormat($original_dt) {

		// handles 2 different formats
		$time = strtotime($original_dt);
		if (!empty($time)) {

			// convert from format: 2013-09-06 21:30
			$converted_dt = substr(date('Y-m-d H:i:sO', strtotime($original_dt)),0,22);

		} else {

			// convert from format: Sat Sep 07 2013 09:30:00 GMT-0400 (EDT)
			$datetime = substr($original_dt,0,24); // Sat Sep 07 2013 09:30:00
			$timezone = substr($original_dt,28,5); // -0400
			$original_dt = $datetime . ' ' . $timezone;
			$datetime = DateTime::createFromFormat('D M d Y H:i:s O', $original_dt);

			$converted_dt = substr($datetime->format('Y-m-d H:i:sO'),0,22);
		}

		// return converted_dt datetime
		return !empty($converted_dt) ? $converted_dt : false;
	}


/**
 * editajax method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function editajax($id = null) {

		// no such reseration
		if (!$this->Reservation->exists($id))
		{
			echo 0;
			return;
		}
		if ($this->request->is('post'))
		{
			// convert from GMT to DATETIME
			$data = $this->request->data;
			$data['Reservation']['start_dt'] = $this->convertCalendarFormat($data['Reservation']['start_dt']);
			$data['Reservation']['end_dt'] = $this->convertCalendarFormat($data['Reservation']['end_dt']);
			echo $this->Reservation->save($data) ? 1 : 0;
			return $this->redirect(array('action' => 'byFloor'));
		}
		else
		{
			return $this->redirect(array('action' => 'byFloor'));
		}
		return;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Reservation->id = $id;
			if (!$this->Reservation->exists()) {
				echo 0;
			}
			if ($this->Reservation->delete()) {
				echo 1;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}

/**
 * Normal delete method
 * 
 * 
 */
	public function remove($id = null) {
		$this->request->onlyAllow('post', 'delete');

		$this->Reservation->id = $id;

		if ($this->Reservation->exists()) {

			// $authed = $this->Reservation->field('account_id') == AuthComponent::user('cust_acct_id');
			
			if ($this->Reservation->delete()) {
				$this->Alert->success(__('Reservation deleted'));
			} else {
				$this->Alert->error(__('Reservation could not be deleted'));
			}
		} else {
			$this->Alert->error('Invalid reservation id');
		}
		if (!$this->request->query('next')) {
			$this->redirect(['action' => 'byFloor']);
		}
	}

/**
 * View and book reservations
 * 
 * @param int $floor_id id of floor to look up
 * @param strin $date date to look up in YYYY-MM-DD format, defaults to "today"
 * @return void
 * @author lkchoi
 */
	public function byFloor($floor_id = null, $date = null) {

		$this->uses[] = 'Floor';
		$this->Floor->id = $floor_id;

		// FIXME only allow a user to book a room in the same building and if is_shared is true

		// redirect to primary floor by default
		if (!$this->Floor->exists()) {
			$this->uses[] = 'CustomerAccount';
			// $this->CustomerAccount->id = $this->Auth->user('cust_acct_id');
			$floor_id = $this->CustomerAccount->field('primary_floor_id');
			return $this->redirect(array('action' => 'byFloor', $floor_id, $date));
		}

		// redirect to today by default
		if (empty($date)) {
			return $this->redirect(array('action' => 'byFloor', $floor_id, date('Y-m-d')));
		}

		// POST|PUT request
		if ($this->request->is('post')) {

			// data
			$data = $this->request->data;
			// debug($data); exit;

			// attempt to save the reservation
			$this->Reservation->create();
			if ($this->Reservation->save($data)) {
				$this->Alert->success('The reservation has been saved');
				$this->request->data = null;
			} else {
				$errors = array_values($this->Reservation->validationErrors);
				$errors = $errors[0];
				$error = array_values($errors);
				$error = $error[0];
				$this->Alert->error($error);
			}
		}

		// fetch rooms in this building
		$rooms = $this->Reservation->Room->find('list', array(
			'conditions' => array(
				'Room.floor_id' => $floor_id,
				'Room.is_active' => true,
			),
			'fields' => array('id','short_name'),
			'order' => array('floor_id ASC', 'short_name ASC')
		));

		// fetch reservations
		$reservations = $this->Reservation->find('all', array(
			'conditions' => array(
				'Reservation.room_id' => array_keys($rooms),
				'Reservation.start_dt >=' => "{$date} 00:00:00-04",
				'Reservation.start_dt <=' =>  "{$date} 23:59:59-04"
			),
			'fields' => array('id','room_id','start_dt','end_dt','account_id'),
			'contain' => 'CustomerAccount.name'
		));

		// calcuation helper variables
		$start_of_day = strtotime($date);

		// FORMAT: slots = array(timeslot => array(room_id => reservation_id ), ... );
		$slots = array_fill_keys(range($start_of_day + 9*3600, $start_of_day + 17*3600, 15*60), array());

		// insert reservations into slots
		foreach ($reservations as $reservation) {
			$start_time = strtotime($reservation['Reservation']['start_dt']);
			$end_time = strtotime($reservation['Reservation']['end_dt']);
			$duration = $end_time - $start_time;
			for ($time = $start_time; $time < $end_time; $time += 15*60) {
				$room_id = $reservation['Reservation']['room_id'];
				$slots[$time][$room_id] = $reservation;
				$slots[$time][$room_id]['Reservation']['is_start'] = ($time === $start_time);
				$slots[$time][$room_id]['Reservation']['duration'] = $duration / 60;
			}
		}

		// fetch dropdown and form data for view
		$this->uses[] = 'CustomerAccount';
		$customerAccount = $this->CustomerAccount->find('list', array(
			'conditions' => array(
				'CustomerAccount.is_active' => true
			),
			'fields' => array('id','name'),
			'order' => 'name ASC'
		));
		$this->uses[] = 'Floor';
		$floors = $this->Floor->find('list', array(
			'conditions' => array(
				'Floor.is_active' => true
			),
			'fields' => array('id','short_global_label'),
			'contain' => array(),
			'order' => 'short_global_label ASC'
		));
		$floor = $this->Floor->field('short_global_label');
		$this->set(compact('floor_id','date','rooms','slots','floor','date','floors','customerAccount'));
	}

}

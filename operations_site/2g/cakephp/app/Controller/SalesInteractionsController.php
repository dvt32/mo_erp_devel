<?php
App::uses('AppController', 'Controller');
/**
 * SalesInteractions Controller
 *
 * @property SalesInteraction $SalesInteraction
 * @property PaginatorComponent $Paginator
 */
class SalesInteractionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SalesInteraction->recursive = 0;
		$this->set('salesInteractions', $this->Paginator->paginate());
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SalesInteraction->exists($id)) {
			throw new NotFoundException(__('Invalid sales interaction'));
		}

		// POST|PUT request
		if ($this->request->is(array('post', 'put'))) {
			// copy data
			$data = $this->request->data;

			// Set the interaction ID
			$this->SalesInteraction->id = $id;

			if ($this->SalesInteraction->save($this->request->data)) {
				$this->Session->setFlash(__('The sales interaction has been saved.'));
				// return $this->redirect(array('controller' => 'prospects', 'action' => 'edit', $this->request->data['SalesInteraction']['prospective_customer_id']));
			} else {
				$this->Session->setFlash(__('The sales interaction could not be saved. Please, try again.'));
			}
		} 

		// fetch sales interaction
		$salesInteraction = $this->SalesInteraction->find('first', array(
			'fields' => array('id', 'prospective_customer_id', 'interaction_dt', 'notes', 'interaction_type', 'salesperson'),
			'conditions' => array('SalesInteraction.' . $this->SalesInteraction->primaryKey => $id),
));
		$this->set(compact('salesInteraction'));
		
		// GET request
		if ($this->request->is('get')) {
			$this->request->data = $salesInteraction;
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SalesInteraction->id = $id;
		if (!$this->SalesInteraction->exists()) {
			throw new NotFoundException(__('Invalid sales interaction'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->SalesInteraction->delete()) {
			$this->Session->setFlash(__('The sales interaction has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sales interaction could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

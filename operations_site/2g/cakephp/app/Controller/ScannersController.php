<?php
App::uses('AppController', 'Controller');
/**
 * Scanners Controller
 *
 * @property Scanner $Scanner
 */
class ScannersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Scanner->recursive = 0;
		$this->paginate['order'] = array('Scan.creation_dt DESC');
		$this->set('scanners', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Scanner->exists($id)) {
			throw new NotFoundException(__('Invalid scanner'));
		}
		$options = array('conditions' => array('Scanner.' . $this->Scanner->primaryKey => $id));
		$this->set('scanner', $this->Scanner->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Scanner->create();
			if ($this->Scanner->save($this->request->data)) {
				$this->Session->setFlash(__('The scanner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scanner could not be saved. Please, try again.'));
			}
		}
		$floors = $this->Scanner->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Scanner->exists($id)) {
			throw new NotFoundException(__('Invalid scanner'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Scanner->save($this->request->data)) {
				$this->Session->setFlash(__('The scanner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scanner could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Scanner.' . $this->Scanner->primaryKey => $id));
			$this->request->data = $this->Scanner->find('first', $options);
		}
		$floors = $this->Scanner->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Scanner->id = $id;
		if (!$this->Scanner->exists()) {
			throw new NotFoundException(__('Invalid scanner'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Scanner->delete()) {
			$this->Session->setFlash(__('Scanner deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Scanner was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Scanner->recursive = 0;
		$this->set('scanners', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Scanner->exists($id)) {
			throw new NotFoundException(__('Invalid scanner'));
		}
		$options = array('conditions' => array('Scanner.' . $this->Scanner->primaryKey => $id));
		$this->set('scanner', $this->Scanner->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Scanner->create();
			if ($this->Scanner->save($this->request->data)) {
				$this->Session->setFlash(__('The scanner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scanner could not be saved. Please, try again.'));
			}
		}
		$floors = $this->Scanner->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Scanner->exists($id)) {
			throw new NotFoundException(__('Invalid scanner'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Scanner->save($this->request->data)) {
				$this->Session->setFlash(__('The scanner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scanner could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Scanner.' . $this->Scanner->primaryKey => $id));
			$this->request->data = $this->Scanner->find('first', $options);
		}
		$floors = $this->Scanner->Floor->find('list');
		$this->set(compact('floors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Scanner->id = $id;
		if (!$this->Scanner->exists()) {
			throw new NotFoundException(__('Invalid scanner'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Scanner->delete()) {
			$this->Session->setFlash(__('Scanner deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Scanner was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Scans Controller
 *
 * @property Scan $Scan
 */
class ScansController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Scan->recursive = 0;

        $this->paginate = array(
                'conditions' => array(
        	"OR" => array(
        		'Scan.account_id' => null,
        		'Scan.person_id' => null,
                        )),
        	'limit' => 50,
        	'order' => 'Scan.creation_dt DESC'
		);
		$this->set('scans', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Scan->exists($id)) {
			throw new NotFoundException(__('Invalid scan'));
		}
		$options = array('conditions' => array('Scan.' . $this->Scan->primaryKey => $id));
		$this->set('scan', $this->Scan->find('first', $options));
	}

/**
 * download method
 *
 * @throws NotFoundException
 * @param integer $id
 * @return void
 */
        public function download($id = null) {
                if (!$this->Scan->exists($id)) {
                        throw new NotFoundException(__('Invalid scan'));
                }
                $scan = $this->Scan->find('first', array('conditions' => array('Scan.id' => $id)));
                $scanUrl = DAV_SERVER_HOST.pathFromID($scan['Scan']['file_id']).urlencode($scan['Scan']['filename']);

                $fileUrl = download_file($scanUrl, './files');

                $filename = substr(strrchr($fileUrl, '/'), 1);
                $this->response->file($fileUrl, array('name' => $fileName, 'download' => true));
                return $this->response;
        }
/**
 * claim method
 *
 * @throws NotFoundException
 * @param integer $id
 * @return void
 *
 */
        public function claim($id = null) {
            if (!$this->Scan->exists($id)) {
                    throw new NotFoundException(__('Invalid scan'));
            }
            $scan = $this->Scan->find('first', array('conditions' => array('Scan.id' => $id)));
            $this->Scan->id = $id;
            $data = array(
                    'person_id' => $this->Auth->user('id'),
                    'account_id' => 1
            );
            if ($this->Scan->save($data)) {
            	$this->Alert->info(__('Your file has been claimed'));
            } else {
            	$this->Alert->info(__('Your file has not been claimed'));
            }
            $this->redirect(array('action' => 'index'));
        }
/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Scan->create();
			if ($this->Scan->save($this->request->data)) {
				$this->Alert->info(__('The scan has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Alert->info(__('The scan could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Scan->CustomerAccount->find('list');
		$customers = $this->Scan->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Scan->exists($id)) {
			throw new NotFoundException(__('Invalid scan'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Scan->save($this->request->data)) {
				$this->Alert->info(__('The scan has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Alert->info(__('The scan could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Scan.' . $this->Scan->primaryKey => $id));
			$this->request->data = $this->Scan->find('first', $options);
		}
		$customerAccounts = $this->Scan->CustomerAccount->find('list');
		$customers = $this->Scan->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Scan->id = $id;
		if (!$this->Scan->exists()) {
			throw new NotFoundException(__('Invalid scan'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Scan->delete()) {
			$this->Alert->info(__('Scan deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Alert->info(__('Scan was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Scan->recursive = 0;
		$this->set('scans', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Scan->exists($id)) {
			throw new NotFoundException(__('Invalid scan'));
		}
		$options = array('conditions' => array('Scan.' . $this->Scan->primaryKey => $id));
		$this->set('scan', $this->Scan->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Scan->create();
			if ($this->Scan->save($this->request->data)) {
				$this->Alert->info(__('The scan has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Alert->info(__('The scan could not be saved. Please, try again.'));
			}
		}
		$customerAccounts = $this->Scan->CustomerAccount->find('list');
		$customers = $this->Scan->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Scan->exists($id)) {
			throw new NotFoundException(__('Invalid scan'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Scan->save($this->request->data)) {
				$this->Alert->info(__('The scan has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Alert->info(__('The scan could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Scan.' . $this->Scan->primaryKey => $id));
			$this->request->data = $this->Scan->find('first', $options);
		}
		$customerAccounts = $this->Scan->CustomerAccount->find('list');
		$customers = $this->Scan->Customer->find('list');
		$this->set(compact('customerAccounts', 'customers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Scan->id = $id;
		if (!$this->Scan->exists()) {
			throw new NotFoundException(__('Invalid scan'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Scan->delete()) {
			$this->Alert->info(__('Scan deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Alert->info(__('Scan was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

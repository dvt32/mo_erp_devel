<?php
App::uses('AppController', 'Controller');
/**
 * SipTermCarr Controller
 *
 * @property SipTermCarr $SipTermCarr
 */
class SipTermCarrsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SipTermCarr->recursive = 0;
		$this->set('sipTermCarrs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->SipTermCarr->id = $id;
		if (!$this->SipTermCarr->exists()) {
			throw new NotFoundException(__('Invalid sip term carr'));
		}
		$this->set('sipTermCarr', $this->SipTermCarr->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SipTermCarr->create();
			if ($this->SipTermCarr->save($this->request->data)) {
				$this->Session->setFlash(__('The sip term carr has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sip term carr could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->SipTermCarr->id = $id;
		if (!$this->SipTermCarr->exists()) {
			throw new NotFoundException(__('Invalid sip term carr'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SipTermCarr->save($this->request->data)) {
				$this->Session->setFlash(__('The sip term carr has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sip term carr could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SipTermCarr->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SipTermCarr->id = $id;
		if (!$this->SipTermCarr->exists()) {
			throw new NotFoundException(__('Invalid sip term carr'));
		}
		if ($this->SipTermCarr->delete()) {
			$this->Session->setFlash(__('Sip term carr deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Sip term carr was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

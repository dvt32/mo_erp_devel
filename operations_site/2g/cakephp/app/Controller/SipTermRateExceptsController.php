<?php
App::uses('AppController', 'Controller');
/**
 * SipTermRateExcept Controller
 *
 * @property SipTermRateExcept $SipTermRateExcept
 */
class SipTermRateExceptsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SipTermRateExcept->recursive = 0;
		$this->set('sipTermRateExcepts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->SipTermRateExcept->id = $id;
		if (!$this->SipTermRateExcept->exists()) {
			throw new NotFoundException(__('Invalid sip term rate except'));
		}
		$this->set('sipTermRateExcept', $this->SipTermRateExcept->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SipTermRateExcept->create();
			if ($this->SipTermRateExcept->save($this->request->data)) {
				$this->Session->setFlash(__('The sip term rate except has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sip term rate except could not be saved. Please, try again.'));
			}
		}
		$sipTermCarrs = $this->SipTermRateExcept->SipTermCarr->find('list');
		$this->set(compact('sipTermCarrs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->SipTermRateExcept->id = $id;
		if (!$this->SipTermRateExcept->exists()) {
			throw new NotFoundException(__('Invalid sip term rate except'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SipTermRateExcept->save($this->request->data)) {
				$this->Session->setFlash(__('The sip term rate except has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sip term rate except could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SipTermRateExcept->read(null, $id);
		}
		$sipTermCarrs = $this->SipTermRateExcept->SipTermCarr->find('list');
		$this->set(compact('sipTermCarrs'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SipTermRateExcept->id = $id;
		if (!$this->SipTermRateExcept->exists()) {
			throw new NotFoundException(__('Invalid sip term rate except'));
		}
		if ($this->SipTermRateExcept->delete()) {
			$this->Session->setFlash(__('Sip term rate except deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Sip term rate except was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

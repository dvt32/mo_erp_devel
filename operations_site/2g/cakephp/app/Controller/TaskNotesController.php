<?php
App::uses('AppController', 'Controller');
/**
 * TaskNotes Controller
 *
 * @property TaskNote $TaskNote
 * @property PaginatorComponent $Paginator
 */
class TaskNotesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TaskNote->recursive = 0;
		$this->set('taskNotes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TaskNote->exists($id)) {
			throw new NotFoundException(__('Invalid task note'));
		}
		$options = array('conditions' => array('TaskNote.' . $this->TaskNote->primaryKey => $id));
		$this->set('taskNote', $this->TaskNote->find('first', $options));
	}


/**
 * stream of task notes related to the specified task
 * 
 * @param int $task_id id of task to stream
 * @return void
 */
	public function stream($task_id) {

		$conditions = array('TaskNote.task_id' => $task_id);
		$fields = array('id','content','created');
		$order = array('TaskNote.created ASC');
		$contain = array('Employee' => array('fields' => array('id','first_name','last_name')));

		$notes = $this->TaskNote->find('all', compact('conditions','fields','contain','order'));

		$this->set(compact('notes'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			// record the logged in user's employee_id
			$data = $this->request->data;
			$data['TaskNote']['employee_id'] = $this->Auth->user('id');

			// reset the model
			$this->TaskNote->create();

			// save the note
			if (!empty($data['TaskNote']['content']) && $this->TaskNote->save($data)) {

				// redirect to the stream
				if ($this->request->params['isAjax']) {

					// json response: succeeded
					$this->response->body(json_encode(array('result' => 'true', 'id' => $this->TaskNote->id)));
					return;
				} else {
					$this->redirect(array('action' => 'stream', $data['Task']['id']));
				}
			}
			// note failed to save
			else {

				if ($this->request->params['isAjax']) {

					// json response: failed
					$this->response->body(json_encode(array('result' => 'false')));
					return;
				} else {
					$this->Session->setFlash(__('The task note could not be saved. Please, try again.'));
				}
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TaskNote->exists($id)) {
			throw new NotFoundException(__('Invalid task note'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TaskNote->save($this->request->data)) {
				$this->Session->setFlash(__('The task note has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The task note could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TaskNote.' . $this->TaskNote->primaryKey => $id));
			$this->request->data = $this->TaskNote->find('first', $options);
		}
		$tasks = $this->TaskNote->Task->find('list');
		$employees = $this->TaskNote->Employee->find('list');
		$this->set(compact('tasks', 'employees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TaskNote->id = $id;
		if (!$this->TaskNote->exists()) {
			throw new NotFoundException(__('Invalid task note'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TaskNote->delete()) {
			$this->Session->setFlash(__('The task note has been deleted.'));
		} else {
			$this->Session->setFlash(__('The task note could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

}

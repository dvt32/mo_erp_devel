<?php
App::uses('AppController', 'Controller');
/**
 * Tasks Controller
 *
 * @property Task $Task
 * @property PaginatorComponent $Paginator
 */
class TasksController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {

		// if task id is submitted...
		if (!empty($this->request->query['id'])) {

			// jump to matching task
			$id = $this->request->query['id'];
			if ($this->Task->exists($id)) {
				return $this->redirect(array('action' => 'view', $id));
			} else {
				unset($this->request->query['id']);
				$this->Alert->error('Invalid task id #');
			}
		}

		// if filter params are set...
		if (!empty($this->request->query)) {

			// build array of conditions using the query
			$conditions = $this->filterByQuery($this->request->query, array('fulltext' => 'description'));

			// set form input values to passed params
			$this->request->data['Task'] = $this->request->query;
		}

		// if status is not specified
		if (!isset($conditions['Task.status ILIKE'])) {

			// default paginator settings
			$conditions['Task.completion_date'] = null;
			// exclude cancelled & complete
			$conditions['NOT'] = array('Task.status ILIKE' => 'Cancelled', array('Task.status ILIKE' => 'Confirmed Complete'));
		}

		// related models to retrieve
		$contain = array(

			// assigned employee info
			'AssignedtoEmployee' => array('fields' => array('id','full_name')),

			// floor
			'Floor' => array(
				'fields' => array('id','short_global_label'),
				'Building' => array('fields' => array('id','short_global_label'))
			),

			// most recent note
			'Note' => array(
				'limit' => 1,
				'fields' => array('id','content','modified'),
				'order' => 'Note.modified DESC'
			),

			// requestor can be an employee... 
			'RequestedbyEmployee' => array('fields' => array('id','full_name')),

			// ... or customer (and parent company)
			'RequestedbyCustomer' => array('fields' => array('id','full_name')),
			'RequestedbyCustomerAccount' => array('fields' => array('id','name')),
		);


		$this->Paginator->settings = array('Task' => compact('conditions','contain','order'));

		// fetch dropdown lists from model
		$areas = $this->Task->areas;
		$statuses = $this->Task->statuses;
		$urgencies = $this->Task->urgencies;
		$this->set(compact('areas','statuses','urgencies'));

		// fetch rows from datasource
		$this->uses[] = 'Floor';
		$this->uses[] = 'Employee';
		$tasks = $this->Paginator->paginate();
		$floors = $this->Floor->listActiveOfficeFloors(array('fields' => 'short_global_label'));
		$employees = $this->Employee->getActiveList();
		$this->set(compact('tasks','floors','employees'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		// invalid task id
		if (!$this->Task->exists($id)) {
			$this->Session->setFlash('The task you are looking for cannot be found');
			return $this->redirect(array('action' => 'index'));
		}

		// set find options
		$conditions = array('Task.id' => $id);
		$contain = array(

			// assigned employee info
			'AssignedtoEmployee' => array('fields' => array('id','full_name')),

			// floor
			'Floor' => array(
				'fields' => array('id','short_global_label'),
				// 'Building' => array('fields' => array('id','short_global_label'))
			),

			// most recent note
			'Note' => array(
				'fields' => array('id','content','modified'),
				'order' => 'Note.modified DESC'
			),

			// requestor can be an employee... 
			'RequestedbyEmployee' => array('fields' => array('id','full_name')),

			// ... or customer (and parent company)
			'RequestedbyCustomer' => array('fields' => array('id','full_name')),
			'RequestedbyCustomerAccount' => array('fields' => array('id','name')),
		);

		// fetch short lists from model
		$areas = $this->Task->areas;
		$statuses = $this->Task->statuses;

		$urgencies = $this->Task->urgencies;
		$this->set(compact('areas','statuses','urgencies'));

		// fetch rows from db
		$task = $this->Task->find('first', compact('conditions','contain','fields','order'));
		$employees = $this->Task->AssignedtoEmployee->getActiveList();
		$this->set(compact('task','employees'));

		// post request
		if ($this->request->is('post')) {
			// TODO implement form processing
		} elseif ($this->request->is('get')) {
			$this->request->data = $task;
		}

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		// POST request
		if ($this->request->is('post')) {
			$this->Task->create();
			if ($this->Task->save($this->request->data)) {
				$this->Session->setFlash(__('The task has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The task could not be saved. Please, try again.'));
			}
		}

		// GET request
		else {

			// set default requested completion date to 2 business days from now
			$this->request->data['Task']['requested_completion_date'] = strto_businessday_time('+2 days');

			// FIXME make dynamic if/when this default is no longer valid
			$this->request->data['Task']['assigned_employee_id'] = 2762; // hardcoded employee_id for Zoila
		}
		
		// fetch rows from datasource
		$this->uses[] = 'Employee';
		$employees = $this->Employee->getActiveList();
		$floors = $this->Task->Floor->listActiveOfficeFloors();
		$areas = $this->Task->areas;
		$urgencies = $this->Task->urgencies;
		$this->set(compact('employees', 'floors', 'areas', 'urgencies'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->Task->exists($id)) {
			throw new NotFoundException(__('Invalid task'));
		}

		// POST|PUT request
		if ($this->request->is('post') || $this->request->is('put')) {

			// copy data
			$data = $this->request->data;

			// debug($requestor); die;

			// save the task
			if ($this->Task->save($data)) {

				// on success email task status updates
				$task = $this->Task->find('first', array(
					'conditions' => array(
						'id' => $id
					)
				));

				if (!empty($task['Task']['requestedby_cust_pers_id'])) {
					$this->uses[] = 'Customer';
					$customer = $this->Customer->find('first', array(
						'conditions' => array(
							'Customer.id' => $task['Task']['requestedby_cust_pers_id']
						)
					));
					$requestor['email'] = $customer['Customer']['email'];
					$requestor['full_name'] = $customer['Customer']['full_name'];
					// text
					$text = 'Task ID: '.$task['Task']['id'].' has been updated to status: '.$task['Task']['status']."\n".
					"https://bizcenter.microoffice.com"."/tasks/view/".$task['Task']['id'];
					$html = 'Task ID: '.$task['Task']['id'].' has been updated to status: '.$task['Task']['status']."\n".
					"<a href=\"https://bizcenter.microoffice.com/tasks/view/".$task['Task']['id']."\">Click to See Task</a>";

				} else if (!empty($task['Task']['requestedby_employee_id'])) {
					$this->uses[] = 'Employee';
					$employee = $this->Employee->find('first', array(
						'conditions' => array(
							'Employee.id' => $task['Task']['requestedby_employee_id']
						)
					));
					$requestor['email'] = $employee['Employee']['email'];
					$requestor['full_name'] = $employee['Employee']['full_name'];

					$text = 'Task ID: '.$task['Task']['id'].' has been updated to status: '.$task['Task']['status']."\n".
					FULL_BASE_URL."/tasks/view/".$task['Task']['id'];
					$html = 'Task ID: '.$task['Task']['id'].' has been updated to status: '.$task['Task']['status']."\n".
					"<a href=\"".FULL_BASE_URL."/tasks/view/".$task['Task']['id']."\">Click to See Task</a>";
				}

				if (!empty($task['Task']['assigned_employee_id'])) {
					
					$this->uses[] = 'Employee';
					$employee = $this->Employee->find('first', array(
						'conditions' => array(
							'Employee.id' => $task['Task']['assigned_employee_id']
						)
					));
					$delegated['email'] = $employee['Employee']['email'];
					$delegated['full_name'] = $employee['Employee']['full_name'];

				}

				// debug($requestor); die;

				$email = array(
					'to' => array(
						array(
							// 'email' => 'jj@pith-studio.com',
							'email' => $requestor['email'],
							'name' => $requestor['full_name'],
							'type' => 'to'
						),
						array(
							// 'email' => 'jj@pith-studio.com',
							'email' => $delegated['email'],
							'name' => $delegated['full_name'],
							'type' => 'to'
						)
					),
					'subject' => 'Task Request Status Updated',
					'text' => $text,
					'html' => $html
				);

				$this->Task->sendEmail($email, false);

				$this->Alert->success(__('The task has been saved.'));
				$this->redirect(array('action' => 'view', $this->Task->id));
			} else {
				$this->Alert->error(__('The task could not be saved. Please, try again.'));
			}
		}
		// GET request
		else {
			$options = array('conditions' => array('Task.' . $this->Task->primaryKey => $id));
			$this->request->data = $this->Task->find('first', $options);
		}
	}

/**
 * Cancel a task
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cancel($id = null) {
		$this->Task->id = $id;
		if (!$this->Task->exists()) {
			throw new NotFoundException(__('Invalid task'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Task->saveField('status', 'Cancelled')) {
			$this->Alert->success(__('The task has been cancelled.'));
		} else {
			$this->Alert->error(__('The task could not be cancelled. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');

/**
 * Tokens Controller
 *
 * @property Token $Token
 */
class TokensController extends AppController {

/**
 * Allow without authentication:
 * request, resetPassword, customerJoin
 * 
 * @return void
 */
	public function beforeFilter() {
		$this->Auth->allow('request', 'resetPassword', 'customerJoin');
	}


/**
 * Changes layout to 'login'
 * 
 * @return void
 */
	public function beforeRender() {
		$this->layout = 'login';
	}

/**
 * Sends an email with the appropriate token type
 * 
 * @return void
 */
	public function request($type = null) {

		if ($this->request->isPost()) {

			// copy data
			$data = $this->request->data;

			// find employee id by email
			$this->uses[] = 'Employee';
			$this->Employee->id = $id = $this->Employee->lookupId('email', $data['Employee']['email']);

			// valid username?
			if ($this->Employee->exists($id)) {

				// NOTE: because the employee_tbl, cust_pers_tbl, and others share the person_seq,
				//   the possibility of id collision is not an issue

				// set the associated user_id 
				$data['Employee']['id'] = $data['Token']['user_id'] = $id;

				// save the token (also sends the email)
				$token = $this->Token->save($data);
				$this->Alert->success('A password-reset link has been sent to your inbox. Check your spam folder if you do not receive the email after a few minutes', 'Email Sent');
				$this->redirect('/');
			} else {
				$this->Alert->error(__('Invalid email address')); // FIXME security vulnerability? tmi? throttle?
			}
		} else {
			$this->Alert->info(__("We'll send a password-reset link so you can regain access to your account.", ''));
		}
	}

/**
 * user clicks link in email that leads here
 * 
 * @param $jumble the unique identifier for the token
 */
	public function resetPassword($jumble = null) {
		// check that token is valid
		$token = $this->Token->findValid($jumble, $this->action);

		// invalid token
		if (empty($token)) {
			$this->set('valid_token', 0);
			$errors = "Possible Reasons: <ul> <li>link was typed incorrectly</li> <li>link has expired</li> <li>link has already been used</li> </ul>";
			$this->Alert->error($errors, 'Invalid Reset Password Link');
		}
		
		// valid token
		else {
			$this->set('valid_token', 1);

			// Form-data posted
			if ($this->request->isPost() || $this->request->isPut()) {

				// user data to be saved
				$data = array(
					'Token' => array(
						'id' => $token['Token']['id'],
						'is_spent' => true
					),
					'User' => array(
						'id' => $token['User']['id'],
						'password' => $this->request->data['User']['password'],
						'confirm_password' => $this->request->data['User']['confirm_password'],
					)
				);

				// Validate new password+confirmation
				$this->uses[] = 'User';
				if ($this->User->save($data, array('fieldList' => array('password')))) {

					// Mark token as used
					$this->Token->markAllUsed($data['User']['id'], 'resetPassword');

					// Report success
					$this->Alert->success("Login using new password", 'Changes saved');
					$this->redirect(array('controller' => 'users', 'action' => 'login'));
				} else {
					$this->Alert->error("Changes failed to save.");
				}
			}
		}
	}

	public function customerJoin($jumble = null) {
		// check that token is valid
		$token = $this->Token->findValid($jumble, $this->action);

		if (empty($token)) {
			$this->set('valid_token', false);
			$errors = "Possible Reasons: <ul> <li>link was typed incorrectly</li> <li>link has expired</li> <li>link has already been used</li> </ul>";
			$this->Alert->error($errors, 'Invalid invitation acceptance link');
		}

		else {
			$this->set('valid_token', true);
			if ($this->request->is('post') || $this->request->is('put')) {

				// copy request data
				$data = $this->request->data;

				// TODO define fieldList
				$fieldList = array(
					'User' => array('id','password','confirm_password'),
					'Customer' => array('id','first_name','last_name','is_active')
				);

				$this->uses[] = 'User';
				// debug($data); exit;

				// save the user
				if ($this->User->saveAssociated($data, compact('fieldList'))) {
					$this->Token->markAllUsed($data['User']['id'], 'customerJoin');
					$this->Alert->success("You're all set, sign in to get started", "Great!");
					$this->redirect(array('controller' => 'users', 'action' => 'login'));
				} else {
					debug($this->User->validationErrors);
					$this->Alert->error('Your information could not be saved');
				}
			} else {
				$this->uses[] = 'User';
				$this->request->data = $this->User->find('first', array(
					'conditions' => array('User.id' => $token['User']['id']),
					'fields' => array('id','username'),
					'contain' => array(
						'Customer' => array(
							'fields' => array('id','first_name','last_name'),
						)
					)
				));
			}
		}
	}

}

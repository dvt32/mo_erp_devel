<?php
App::uses('AppModel', 'Model');
/**
 * AccessLevel Model
 *
 * @property Floor $Floor
 * @property ProximityCardEditTbl $ProximityCardEditTbl
 * @property ProximityCardTbl $ProximityCardTbl
 */
class AccessLevel extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'access_level_tbl';

/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.access_level_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ProximityCardEdit' => array(
			'className' => 'ProximityCardEdit',
			'foreignKey' => 'access_level_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProximityCard' => array(
			'className' => 'ProximityCard',
			'foreignKey' => 'access_level_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

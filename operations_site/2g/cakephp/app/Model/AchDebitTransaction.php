<?php
App::uses('AppModel', 'Model');
/**
 * AchDebitTransaction Model
 *
 * @property CustAcct $CustAcct
 * @property CustomerBank $CustomerBank
 * @property ElectronicCheck $ElectronicCheck
 * @property RecvPayment $RecvPayment
 * @property Modifier $Modifier
 */
class AchDebitTransaction extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ach_debit_txn_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustAcct' => array(
			'className' => 'CustAcct',
			'foreignKey' => 'cust_acct_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerBank' => array(
			'className' => 'CustomerBank',
			'foreignKey' => 'customer_bank_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ElectronicCheck' => array(
			'className' => 'ElectronicCheck',
			'foreignKey' => 'electronic_check_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RecvPayment' => array(
			'className' => 'RecvPayment',
			'foreignKey' => 'recv_payment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Modifier' => array(
			'className' => 'Modifier',
			'foreignKey' => 'modifier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

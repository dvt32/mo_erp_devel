<?php
App::uses('AppModel', 'Model');
/**
 * Aco Model
 *
 */
class Aco extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */

	public $useTable = 'aco_tbl';

/**
 * Use sequence
 * 
 * @var string sequence name
 */
	public $sequence = 'public.aco_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'action';

/**
 * hasMany associations
 * 
 * @var array
 */
	public $hasMany = array(
		'Permission' => array(
			'className' => 'Permission',
			'foreignKey' => 'aco_id'
		)
	);

/**
 * Validation rules
 * 
 * @var array
 */
	public $validate = array(

		// controller
		'controller' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Controller name is required'
			),
			'alphaNumeric' => array(
				'rule' => ['alphaNumeric'],
				'message' => 'Controller name must be alpha-numeric'
			)
		),
		
		// action
		'action' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Action name is required'
			),
			'alphaNumeric' => array(
				'rule' => ['alphaNumeric'],
				'message' => 'Action name must be alpha-numeric'
			)
		)

	);

	public function beforeSave($options = array())
	{
		// fetch controller and action
		$controller = @$this->data['Aco']['controller'];
		$action = @$this->data['Aco']['action'];

		// see if a duplicate exists
		$count = $this->find('count', [
			'conditions' => compact('controller','action')
		]);
		$this->log($count,'debug');

		if (!empty($count))
		{
			$this->validationErrors['controller'][] = 'Controller/Action combo must be unique';
			$this->validationErrors['action'][] = 'Controller/Action combo must be unique';
			return false;
		}

		return true;
	}

}

<?php

App::uses('AppModel', 'Model');

/**
 * AgingInteraction Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Employee $Employee
 * @property Modifier $Modifier
 */
class AgingInteraction extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'aging_interaction_tbl';

	public $sequence = 'public.aging_interaction_seq';

	public $types = [
		'phone_call' => 'Phone Call',
		'email' => 'Email',
		'in_person' => 'In-Person',
	];

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		),
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
		),
		'Modifier' => array(
			'className' => 'Employee',
			'foreignKey' => 'modifier_id',
		)
	);

	/**
	 * Update the customer account's aging_last_contact_dt
	 */
	public function afterSave($created, $options = array())
	{
		// update the customer account's aging_last_contacted_dt
		if ($created)
		{
			$interaction = $this->data[$this->alias];
			$this->CustomerAccount->id = $interaction['cust_acct_id'];
			$this->CustomerAccount->saveField('aging_last_contacted_dt', $interaction['creation_dt']);
		}
	}
}

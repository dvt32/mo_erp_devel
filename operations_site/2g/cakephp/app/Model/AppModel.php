<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Model', 'Model');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

/**
 * Disable recursive data retrieval by default
 * 
 * @var int
 */
	public $recursive = -1;

/**
 * Use Containable behavior for all models by default
 * 
 * @var array
 */
	public $actsAs = array('Containable');

/**
 * Set MS Access info. This must be called by any child-class overloads.
 * 
 * @param array $options beforeSave options
 * @return boolean true
 */
	public function beforeSave($options = array()) {
		// generate a random integer
		$this->data[$this->alias]['ms_access_random_num'] = pow(-1,rand(0,3)) * mt_rand();
		$this->data[$this->alias]['ms_access_creation_dt'] = date(DATETIME_FORMAT);
		$this->data[$this->alias]['creation_dt'] = date(DATETIME_FORMAT);
		$this->data[$this->alias]['modifier_id'] = AuthComponent::user('id');
		return true;
	}

/**
 * reverse lookup method
 * 
 * @return mix id
 */
	public function lookupId($field, $value) {
		$row = $this->find('first', array('conditions' => array($field => $value), $this->primaryKey));
		if (empty($row)) {
			return false;
		}
		return $row[$this->alias][$this->primaryKey];
	}


/**
 * matches method (custom validation rule)
 * 
 * @return void
 */
	public function matches($check, $ctrl) {
		return array_pop($check) === $this->data[$this->alias][$ctrl];
	}


/**
 * Dynamically adds attributes to the model with distinct values in datasource
 * 
 * @param array of fields
 * @return void
 */
	protected function loadFields($fields = array()) {
		$alias = $this->alias;
		foreach ($fields as $field) {
			$plural = $field;
			$singular = Inflector::singularize($field);
			$this->$plural = filter_recursive($this->find('all', array(
				'fields' => "DISTINCT {$alias}.{$singular}",
				'order' => "{$alias}.{$singular} ASC"
			)));
			foreach ($this->$plural as &$val) { $val = $val[$alias][$singular]; }
		}
	}


/**
 * Sends an email via Mandrill.
 * 
 * @param array $emails list of email recipients
 * @param mix $send_at strtotime parseable string indicating when to schedule the email to send (e.g., '+4 days') or a UTC time (seconds since epoch) in the future
 * @return array $result result of the call to the Mandrill API via MandrillTransport
 */
	public function sendEmail($message = array(), $send_at = false) {

		// check required fields
		if (empty($message['to'])) {
			throw new Exception('Missing email recipient(s)');
		}
		elseif (empty($message['subject'])) {
			throw new Exception('Missing email subject');
		}
		elseif (empty($message['text']) && empty($message['html'])) {
			throw new Exception('Missing email body');
		}

		// set default from_email
		$message['from_email'] = @$message['from_email'] ?: 'no-reply@microoffice.com';
		
		// create cakeemail object with params
		$Email = new CakeEmail('microoffice');
		
		$to = [];
		foreach($message['to'] as $key => $to_array) {
			if (array_key_exists('name', $to_array)) {
				$to[$to_array['email']] = $to[$to_array['name']];
			} else {
				$to[] = $to_array['email'];
			}
		}
		
		try {
			$Email->from($message['from_email'])
	    	->to($to)
	    	->subject($message['subject']);
		} catch (SocketException $e) {
	    $errorMessage = $e->getMessage();
			debug($errorMessage);
			return false;
		}
		
		if (!empty($message['html'])) {
			$Email->emailFormat('html')->message($message['html']);
		} else {
			$Email->emailFormat('text')->message($message['text']);
		}
			
		// create mandrill object
		// $mandrill = new Mandrill(MANDRILL_API_KEY);
		// var_dump($mandrill);

		// send immediately
		try {
    	$results = $Email->send();
			debug($results);
			return true;
		} catch (SocketException $e) {
	    $errorMessage = $e->getMessage();
			debug($errorMessage);
			return false;
		}
		/* schedule email to be sent
		else {

			// convert to time if $send_at is a string
			$time = is_int($send_at) ? $send_at : strtotime($send_at);

			// convert time to utc datetime
			$utc_datetime = gmdate(DATETIME_FORMAT, $time);

			// schedule message
			$results = $mandrill->messages->send($message, false, null, $utc_datetime);
		}*/
		//debug($results);

		// filter our successful sends, leaving just the failed sends
		//$failed = array_filter($results,function($result){ return $result['status']==='invalid' || //$result['status']==='rejected'; });

		// report failed sends if debug is on
		// debug($failed);

	}

/**
 * Undo a soft-delete of a record
 * @param int $id record id
 * @return boolean true if is_active was set to true
 */
 	public function activate($id = null) {
		$this->id = $id = $id ?: $this->id;
		return $this->saveField('is_active',true);
 	}

/**
 * Soft-delete a record
 * @param int $id record id
 * @return boolean true if is_active was set to false
 */
	public function deactivate($id = null) {
		$this->id = $id = $id ?: $this->id;
		return $this->saveField('is_active',false);
	}



/**
 * Finance Email
 *
 * @param string body_html html of email
 * @param string title title of email
 * @param string body_text text email
 * @param array account CustomerAccount + Primary Customer Account (contained)
 * @return bool if email sends true
 */
	public function financeEmail($account = array(), $subject = null, $body_text = null, $body_html = null) {
		
		if (!empty($account)) {

			// account first name
			$company_name = $account['CustomerAccount']['name'];
			$full_name = $account['Customer']['full_name'];
			$today_nice = date(DATETIME_FORMAT);

			// Email Accounts that are overdue
			$html = '<html> <style type="text/css">ol{margin:0;padding:0}.c7{line-height:1.0;padding-top:0pt;text-align:center;padding-bottom:0pt}.c0{max-width:468pt;background-color:#ffffff;padding:72pt 72pt 72pt 72pt}.c10{line-height:1.0;padding-top:0pt;padding-bottom:0pt}.c1{color:#ff6600;font-size:9pt;font-family:"Times New Roman"}.c6{color:#0563c1;text-decoration:underline}.c9{color:inherit;text-decoration:inherit}.c2{font-size:9pt;font-family:"Times New Roman"}.c3{height:10pt;direction:ltr}.c4{font-size:10pt;font-family:"Times New Roman"}.c5{direction:ltr}.c8{text-align:center}.c11{vertical-align:super}.c12{font-weight:bold}.title{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:36pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}.subtitle{padding-top:18pt;line-height:1.0;text-align:left;color:#666666;font-style:italic;font-size:24pt;font-family:"Georgia";padding-bottom:4pt;page-break-after:avoid}li{color:#000000;font-size:10pt;font-family:"Times New Roman"}p{color:#000000;font-size:10pt;margin:0;font-family:"Times New Roman"}h1{padding-top:24pt;line-height:1.0;text-align:left;color:#000000;font-size:24pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:6pt;page-break-after:avoid}h2{padding-top:18pt;line-height:1.0;text-align:left;color:#000000;font-size:18pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h3{padding-top:14pt;line-height:1.0;text-align:left;color:#000000;font-size:14pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:4pt;page-break-after:avoid}h4{padding-top:12pt;line-height:1.0;text-align:left;color:#000000;font-size:12pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h5{padding-top:11pt;line-height:1.0;text-align:left;color:#000000;font-size:11pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid}h6{padding-top:10pt;line-height:1.0;text-align:left;color:#000000;font-size:10pt;font-family:"Times New Roman";font-weight:bold;padding-bottom:2pt;page-break-after:avoid} </style><body class="c0"> <div> <p class="c7 c5"><img height="129" src="'.
				EMAIL_IMAGE_000.'" width="244"></p></div> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3 c8"><span></span></p> <p class="c5"><span>'.
				$today_nice.'</span></p> <p class="c3"><span></span></p> <p class="c5"><span>J. Hairston</span></p> <p class="c5"><span>136 East 55th St, 3P, New York, NY 10022</span></p> <p class="c5"><span>212-593-2422</span></p> <p class="c3"><span></span></p> <p class="c5"><span>'.
				$subject.'</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span>
				Dear '.$full_name.':</span></p> <p class="c3"><span></span></p> 
				<p class="c5"><span>'.$body_html.'</span></p>'.
				'<p class="c3"><span></span></p> <p class="c5"><span>Thank you,</span></p> <p class="c5"><span>&nbsp;</span></p> <p class="c3"><span></span></p> <p class="c3"><span></span></p> <p class="c5"><span class="c12">Jin Cho</span></p> <p class="c5"><span>Department of Finance, MicroOffice</span></p> <p class="c3"><span></span></p> <p class="c5"><span>1375 Broadway, Floor 3</span></p> <p class="c5"><span>New York, NY 10018</span></p> <p class="c5"><span class="c6"><a class="c9" href="mailto:jin@microoffice.com">jin@microoffice.com</a></span><span>&nbsp;| 212-359-4201</span></p> <p class="c3"><span></span></p> <p class="c5"><span>cc &ndash; Management, Billing</span></p> <p class="c3 c8"><span></span></p> <div> <p class="c3 c10"><span class="c2"></span></p> <p class="c7 c5"><img height="26" src="'.
				EMAIL_IMAGE_001.'" width="26"><span class="c4">&nbsp; &nbsp;</span><img height="28" src="'.
				EMAIL_IMAGE_002.'" width="28"><span class="c2">&nbsp; &nbsp;</span><img></p> <p class="c5 c7"><span class="c2">www. MicroOffice.com </span><span class="c1">&bull;</span><span class="c2">&nbsp;sales@microoffice.com </span><span class="c1">&bull; </span><span class="c2">t. 646-201-5508 </span><span class="c1">&bull;</span><span class="c2">&nbsp;f. 866-788-8109</span></p> <p class="c7 c5"><span class="c1">New York | Chicago | Boston</span></p> </div> </body> </html>';
			
			$text = 'Dear '.$full_name.':'.
			"\n".$body_text.
			"\n\n"."Thank you,".
			"\n\n"."Jin Cho".
			"\n"."Department of Finance, MicroOffice".
			"\n"."1375 Broadway, Floor 3".
			"\n"."New York, NY 10018".
			"\n"."jin@microoffice.com".
			"\n"."212-359-4201";
			

			if (EMAIL_TESTING === true) {
				// build email message
				$email = array(
					'to' => array(
						array(
							'email' => EMAIL_TESTER,
							'name' => $account['Customer']['full_name'],
							'type' => 'to'
						)
					),
					'subject' => $subject.': '.$company_name,
					'text' => $text,
					'html' => $html
				);
			} else {
				// build email message
				$email = array(
					'to' => array(
						array(
							'email' => $account['CustomerAccount']['Customer']['email'],
							'name' => $account['Customer']['full_name'],
							'type' => 'to'
						)
					),
					'subject' => $subject.': '.$company_name,
					'text' => $text,
					'html' => $html
				);
			}

			
			

			// send email
			if (REALLY_SEND_EMAIL === true) {
				$this->sendEmail($email);
				return true;
			}


		} else {
			return false;
		}
	}

}

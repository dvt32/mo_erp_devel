<?php
App::uses('AppModel', 'Model');
/**
 * BillingAmount Model
 *
 * @property Modifier $Modifier
 * @property CustAcct $CustAcct
 */
class BillingAmount extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'billing_amount_tbl';


/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = "public.billing_amount_seq";

/**
 * Virtual fields
 * @var array
 */
	public $virtualFields = array(
		'days_overdue' => 'DATE_PART(\'day\', now() - BillingAmount.due_date)',
		'days_underdue' => 'DATE_PART(\'day\', BillingAmount.due_date - now())'
	);

/**
 * Possible stages of a given BillingAmount
 * @var array
 */
 	public $stages = array(
 		'paid' => 'Paid',
 		'invoicing' => 'Invoicing',
 		'receiving' => 'Receiving',
 		'disputed' => 'Disputed',
 		'aging_hold' => 'Aging Hold',
 		'aging_closed' => 'Aging Closed',
 		'aging_1' => 'Aging 1',
 		'aging_2' => 'Aging 2',
 		'aging_3' => 'Aging 3',
 		'aging_4' => 'Aging 4',
 	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Modifier' => array(
			'className' => 'Employee',
			'foreignKey' => 'modifier_id',
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		)
	);

/**
 * hasMany associations
 * 
 * @var array
 */
	public $hasMany = array(
		'ReceivedPayment' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'billing_amount_id',
			'dependent' => false
		)
	);

/**
 * Validation rules
 * 
 * @var array
 */
	public $validate = array(
		'num_desks' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber',true),
				'allowEmpty' => true,
				'required' => false,
				'message' => 'Invalid number of desks'
			)
		),
		'num_offices' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber',true),
				'allowEmpty' => true,
				'required' => false,
				'message' => 'Invalid number of offices'
			)
		),
		'num_phone_lines' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber',true),
				'allowEmpty' => true,
				'required' => false,
				'message' => 'Invalid number of phone lines'
			)
		),
		'num_persons' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber',true),
				'allowEmpty' => true,
				'required' => false,
				'message' => 'Invalid number of internet lines'
			)
		)
	);

	public function idForCurrentBillingCycle($cust_acct_id) {
		$currentCycleDueDatetime = $this->getCurrentCycleDueDatetime($cust_acct_id);
		$id = $this->field('id', array(
			'cust_acct_id' => $cust_acct_id,
			'due_date' => $currentCycleDueDatetime
		));
		return $id ?: null;
	}

	private function getCurrentCycleStartDatetime($cust_acct_id) {
		$customerAccount = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'fields' => array('id','primary_floor_id'),
			'contain' => array('Floor.dom_billing_cycle_starts')
		));
		$domCycleStarts = $customerAccount['Floor']['dom_billing_cycle_starts'];
		$cycleStartDate = cycle_start_dt($domCycleStarts);
		return $cycleStartDate;
	}

	private function getCurrentCycleDueDatetime($cust_acct_id) {
		$customerAccount = $this->CustomerAccount->find('first', array(
			'conditions' => array('CustomerAccount.id' => $cust_acct_id),
			'fields' => array('id','primary_floor_id'),
			'contain' => array('Floor.dom_payment_due','Floor.dom_billing_cycle_starts')
		));
		// pad to 2 digits
		$domPaymentDueDate = __('%02d',$customerAccount['Floor']['dom_payment_due']);
		$domCycleStarts = __('%02d',$customerAccount['Floor']['dom_billing_cycle_starts']);

		$cycleDueDate = cycle_due_dt($domPaymentDueDate, $domCycleStarts);
		return $cycleDueDate;
	}

	public function beforeValidate($options = array()) {

		// customer account id, used throughout this callback 
		$cust_acct_id = $this->data[$this->alias]['cust_acct_id'];

		// get the existing billing amount id
		$currentCycleStartDatetime = $this->getCurrentCycleStartDatetime($cust_acct_id);
		$currentCycleDueDatetime = $this->getCurrentCycleDueDatetime($cust_acct_id);

		// set billing amount due date
		if (empty($this->data[$this->alias]['due_date'])) {
			$currentCycleDueDatetime = $this->getCurrentCycleDueDatetime($cust_acct_id);
			$this->data[$this->alias]['due_date'] = substr($currentCycleDueDatetime,0,10);
		}

		// fetch info from previous billing amount
		$previous = $this->find('first', array(
			'conditions' => array('cust_acct_id' => $cust_acct_id),
			'order' => 'created DESC'
		));

		// set the stage
		$this->data[$this->alias]['stage'] = 'invoicing';

		// set the due date
		if (isset($this->data[$this->alias]['due_date'])) {
			$this->data[$this->alias]['due_date'] = substr(cycle_due_dt(),0,10);
		}

		$this->saveDoppChangeAutoMessage($this->data);

		return true;
	}

	public function saveDoppChangeAutoMessage($data) {

		$id = @$data[$this->alias]['id'];
		$cust_acct_id = $data[$this->alias]['cust_acct_id'];

		if (!empty($id))
		{ // billing amount exists for current billing cycle

			// fetch current cycle's billing amount
			$current = $this->find('first', array(
				'conditions' => array('id' => $id)
			));

			// set the current (existing) billing amount as the control
			$control = $current;
		}
		else
		{ // billing amount does not exist for current billing cycle

			// beginning of current cycle
			$currentCycleDueDatetime = $this->getCurrentCycleDueDatetime($cust_acct_id);

			// fetch previous cycle's billing amount
			$previous = $this->find('first', array(
				'conditions' => array(
					'BillingAmount.due_date <' => $currentCycleDueDatetime,
					'BillingAmount.cust_acct_id' => $cust_acct_id
				),
				'order' => 'due_date DESC'
			));

			// set the previous billing amount as the control
			$control = $previous;
		}

		if (!empty($control) && !empty($data['BillingAmount']))
		{ // check if changes have been made
			$dopp = array(
				'num_desks' => 'DESKS',
				'num_offices' => 'OFFICES',
				'num_phone_lines' => 'PHONE LINES',
				'num_persons' => 'PERSONS'
			);
			$diff = array();
			foreach ($dopp as $field => $name) {
				
				if (!isset($data['BillingAmount'][$field]))
					continue;

				$from_val = @$control['BillingAmount'][$field];
				$to_val = @$data['BillingAmount'][$field];


				if ($from_val != $to_val) {
					$diff[] = "{$name} from {$from_val} to {$to_val}";
				}
			}

			if (!empty($diff))
			{ // changes have been made
				$this->CustomerAccount->BillingMemo->save(
					array('BillingMemo' => array(
						'cust_acct_id' => $cust_acct_id,
						'is_auto' => true,
						'memo' => "Automated Message: The following changes have been made \n" . implode(",\n", $diff)
					))
				);
			}
		}
		else
		{
			$this->CustomerAccount->BillingMemo->save(
				array('BillingMemo' => array(
					'cust_acct_id' => $cust_acct_id,
					'is_auto' => true,
					'memo' => "Automated Message: This customer account's billing information is new"
				))
			);
		}
	}

	public function beforeSave($options = array()) {
		parent::beforeSave($options);

		// default any dopp field to zero to prevent db error
		$dopp = array('num_desks','num_offices','num_phone_lines','num_persons');
		foreach ($dopp as $field) {
			$this->data[$field] = @$this->data[$field] ?: 0;
		}
		return true;
	}

/**
 * Update the billing amount status if the balance is changed
 * 
 * @param boolean $created true if a new record was created
 */
	public function afterSave($created, $options = array()) {

		// call superclass method
		parent::afterSave($created, $options);

		// update the customer account balance
		$this->CustomerAccount->updateBalance($this->field('cust_acct_id'));

		// update the billing amount status
		$this->updateStatus($this->id);
	}

/**
 * Update all billing amount statuses
 */
	public function updateAllStatuses() {
		return $this->updateStatus();
	}

/**
 * Update billing amount status(es)
 */
	public function updateStatus($id = null, $options = array()) {

		// safety check billing amount id
		if (!empty($id) && !$this->exists($id)) {
			return false; // throw new NotFoundException('Invalid billing amount id');
		}

		// determine which billing amounts to update
		$where_clause = null; // default: all billing amounts
		if (!empty($id))
		{
			if (is_array($id))
			{ // multiple billing amounts, i.e. for a given customer account
				$csv = implode(',',$id);
				$where_clause = "WHERE billing_amount_tbl.id IN ({$csv})";
			}
			else
			{ // single billing amount, i.e. when a payment is made
				$where_clause = "WHERE billing_amount_tbl.id={$id}";
			}
		}

		// determine whether or not aging holds should be removed
		$aging_hold_clause = "WHEN billing_amount_tbl.stage='aging_hold' THEN 'aging_hold'"; // default: leave aging holds as is
		if (@$options['remove_holds'] === true)
		{ // revert stage=aging_hold to aging_# as appropriate

			// remove the clause to leave aging_holds as is
			$aging_hold_clause = null;
		}

		// build the query
		$query = 
			"UPDATE billing_amount_tbl
			SET stage=(
				CASE

					-- paid
					WHEN billing_amount_tbl.balance<=0
						THEN 'paid'

					-- disputed
					WHEN billing_amount_tbl.stage='disputed'
						THEN 'disputed'

					-- aging_hold
					{$aging_hold_clause}

					WHEN CURRENT_DATE > billing_amount_tbl.due_date

						THEN CASE

							-- aging_closed
							WHEN (DATE_PART('day', now() - billing_amount_tbl.due_date)>=74)
								THEN 'aging_closed'

							-- aging 4
							WHEN (DATE_PART('day', now() - billing_amount_tbl.due_date)>=44)
								THEN 'aging_4'

							-- aging_3
							WHEN (
								-- (DATE_PART('month', now() - billing_amount_tbl.due_date)=1) AND
								-- (EXTRACT(day from now() - billing_amount_tbl.due_date)>=7)
								(DATE_PART('day', now() - billing_amount_tbl.due_date)>=35) -- FIXME
							)
								THEN 'aging_3'

							-- aging_2
							WHEN DATE_PART('day', age(billing_amount_tbl.due_date))>=7
								THEN 'aging_2'

							-- aging_1
							WHEN DATE_PART('day', now() - billing_amount_tbl.due_date)>=6
								THEN 'aging_1'

						END -- inner CASE

					WHEN 0=(
						SELECT billing_memos_inbox_count
						FROM cust_acct_tbl AS CustomerAccount
						WHERE CustomerAccount.id=billing_amount_tbl.cust_acct_id
					)
						THEN 'receiving'
					ELSE 'invoicing'
				END -- outer CASE
			) {$where_clause}";

		// debug($query);

		// run the query
		$results = $this->query($query);

		return true;
	}

	/**
	 * Update billing amount statuses for a given customer account
	 * 
	 * @param int $cust_acct_id customer account id
	 * @param boolean $resume_aging whether 
	 * @return
	 */
	public function updateStatusForCustomerAccount($cust_acct_id = null, $options = array())
	{
		// fetch billing amounts for this customer account
		$billing_amount_ids = $this->find('list', array(
			'conditions' => array(
				'BillingAmount.cust_acct_id' => $cust_acct_id
			),
			'fields' => array('id','id')
		));

		// update the billing amount statuses
		return $this->updateStatus($billing_amount_ids, $options);
	}

	public function beforeDelete($cascade = true) {
		$cust_acct_id = $this->CustomerAccount->id = $this->field('cust_acct_id');
		$this->CustomerAccount->saveField('billing_last_reviewed_dt', date(DATETIME_FORMAT, strtotime('-32 days')));
		return true;
	}
}

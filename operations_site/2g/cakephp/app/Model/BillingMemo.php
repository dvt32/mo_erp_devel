<?php
App::uses('AppModel', 'Model');
/**
 * BillingMemo Model
 */
class BillingMemo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'billing_memo_tbl';

	public $sequence = 'public.billing_memo_seq';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Modifier' => array(
			'className' => 'Employee',
			'foreignKey' => 'modifier_id',
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			'counterCache' => array(
				'billing_memos_inbox_count' =>  array(
					'BillingMemo.is_active' => true,
					'BillingMemo.is_applied' => false // not yet applied
				),
				'billing_memos_archived_count' => array(
					'BillingMemo.is_active' => true,
					'BillingMemo.is_applied' => true // already applied
				),
			),
		)
	);

	public function afterSave($created, $options = array()) {
		if ($created) {
			$cust_acct_id = $this->field('cust_acct_id');
			$this->CustomerAccount->updateReviewed($cust_acct_id);
			$this->CustomerAccount->updateBillingMemosInboxCount($cust_acct_id);
			$this->CustomerAccount->updateBillingMemosArchivedCount($cust_acct_id);
			$this->CustomerAccount->BillingAmount->updateStatusForCustomerAccount($cust_acct_id);
		}
	}

/**
 * Count the number of active billing memos for a given customer accouant
 * @param int $cust_account_id id of the customer account to check
 * @return int $count number of active billing memos
 */
	public function getActiveCount($cust_acct_id) {
		return $this->find('count', array(
			'conditions' => array('cust_acct_id' => $cust_acct_id)
		));
	}


/**
 * Fetch archived memos for a given customer account
 * 
 * @param int $cust_acct_id customer account id
 * @return array $memos archived memos
 */
	public function archivedMemos($cust_acct_id = array()) {
		$memos = $this->BillingMemo->find('all', array(
			'conditions' => array('is_applied' => true),
		));
		return $memos;
	}

/**
 * Mark billing memo as applied
 * @param int $billing_memo_id id of memos to mark as applied
 * @return boolean $saved whether or not the memos were marked
 */
	public function archive($memo_ids = array()) {

		// update fields
		$fields = array(
			'BillingMemo.is_applied' => true
		);

		// update conditions
		$conditions = array(
			'BillingMemo.id' => $memo_ids
		);

		// execute update
		$saved = $this->updateAll($fields, $conditions);

		// update the counter caches
		$cust_acct_ids = $this->find('list', array(
			'conditions' => array('id' => $memo_ids),
			'fields' => array('cust_acct_id')
		));

		// update the billing memo counts in CustomerAccounts
		$this->CustomerAccount->updateBillingMemoCounts(array_unique($cust_acct_ids));

		foreach ($cust_acct_ids as $cust_acct_id) {
			$this->CustomerAccount->BillingAmount->updateStatusForCustomerAccount($cust_acct_id);
		}

		return $saved;
	}

	public function unarchive($memo_ids = array()) {

		// update billing memos
		$fields = array('BillingMemo.is_applied' => false);
		$conditions = array('BillingMemo.id' => $memo_ids);
		$saved = $this->updateAll($fields,$conditions);


		$this->CustomerAccount->updateBillingMemoCounts($cust_acct_ids);


		return $saved;
	}
}

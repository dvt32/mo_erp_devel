<?php
App::uses('AppModel', 'Model');
/**
 * BillingMemoTemplate Model
 *
 */
class BillingMemoTemplate extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'billing_memo_template_tbl';

/**
 * Use sequence
 * 
 * @var string sequence name
 */
	public $sequence = 'public.billing_memo_template_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}

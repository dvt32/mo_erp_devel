<?php
App::uses('AppModel', 'Model');
/**
 * BillingState Model
 *
 * @property Modifier $Modifier
 * @property CustAcct $CustAcct
 */
class BillingState extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'billing_state_tbl';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'license_agreement_amount' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'License agreement amount is required',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'recurring_fees_subtotal' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Recurring fees must be a numeric value',
				'allowEmpty' => true,
				'required' => false,
			),
		),
		'one_time_fees_subtotal' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'One-time fee must be a numeric value',
				'allowEmpty' => true,
				'required' => false,
			),
		),
		'misc_adjustment_amount' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Miscellaneous adjustment amount must be a numeric value',
				'allowEmpty' => true,
				'required' => false,
			),
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Modifier' => array(
			'className' => 'Employee',
			'foreignKey' => 'modifier_id',
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		)
	);

/**
 * Fetch the latest billing state for a given customer account
 * 
 * @param int $cust_acct_id customer account id
 * @param array $options optional values to pass to find()
 * @return array a customer account's latest billing state
 */
	public function getLatest($cust_acct_id = null, $options) {
		$params = array(
			'conditions' => compact('cust_acct_id'),
			'order' => 'created DESC',
		);
		$state = $this->find('first', $params + $options);
		return $state;
	}

}

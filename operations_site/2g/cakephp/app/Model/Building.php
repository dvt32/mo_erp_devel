<?php
App::uses('AppModel', 'Model');
/**
 * Building Model
 *
 * @property NotificationTbl $NotificationTbl
 * @property FloorTbl $FloorTbl
 * @property IpAddressTbl $IpAddressTbl
 */
class Building extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'building_tbl';


/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.building_seq';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'full_address';

/**
 * Building types
 * 
 */
	public $types = array('officesuite', 'datacenter');


/**
 * Overload default constructor to add `full_name` virtual field
 * 
 * @link http://book.cakephp.org/2.0/en/models/virtual-fields.html#virtual-fields-and-model-aliases
 */
	public function __construct($id = false, $table = null, $ds = null) {
	    parent::__construct($id, $table, $ds);
	    $alias = $this->alias;
	    $this->virtualFields['full_address'] = 
	    	"{$alias}.address_1 ||  ', ' || {$alias}.city || ', ' || {$alias}.state || ' ' || {$alias}.zip_code";
	}

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'IpAddress' => array(
			'className' => 'IpAddress',
			'foreignKey' => 'building_id',
			'dependent' => false,
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'building_id',
			'dependent' => false,
		)
	);


/**
 * Return list of buildings that are office suites (as opposed to datacenters)
 * 
 * @return array list of officesuites
 */
	public function officeSuites() {
		$buildings = $this->find('list', array(
			'conditions' => array(
				'Building.type' => 'officesuite',
				'Building.is_active' => true
			),
			'order' => 'Building.short_global_label ASC'
		));
		return $buildings;
	}
}

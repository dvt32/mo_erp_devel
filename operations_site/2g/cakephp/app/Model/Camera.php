<?php
App::uses('AppModel', 'Model');
/**
 * Camera Model
 *
 * @property Floor $Floor
 */
class Camera extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'camera_tbl';

/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.camera_seq';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

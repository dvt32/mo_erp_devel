<?php
App::uses('AppModel', 'Model');
/**
 * Cdr Model
 *
 * @property Account $Account
 */
class Cdr extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'provender';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cdr';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

<?php
App::uses('AppModel', 'Model');
/**
 * Mailbox Model
 *
 * @property Account $Account
 * @property Floor $Floor
 */
class CreditCardTxn extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'credit_card_txn_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'public.credit_card_txn_seq';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		
	);
}

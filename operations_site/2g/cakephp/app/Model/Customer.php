<?php
App::uses('User', 'Model');
/**
 * Customer Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property User $User
 * @property MailPackage $MailPackage
 * @property ProximityCard $ProximityCard
 * @property CreditCard $CreditCard
 */
class Customer extends User {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cust_pers_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'public.person_seq';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'full_name';

/**
 * overload constructor to set virtual field(s)
 *
 * @link http://book.cakephp.org/2.0/en/models/virtual-fields.html#virtual-fields-and-model-aliases
 */
	public function __construct($id = false, $table = null, $ds = null) {
	    parent::__construct($id, $table, $ds);
	    $this->virtualFields['full_name'] = sprintf('%s.first_name || \' \' || %s.last_name', $this->alias, $this->alias);
	}


/**
 * validation rules
 *
 * @var array
 */
	public $validate = array(

		// first_name
		'first_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter your first name'
			),
		),

		// last_name
		'last_name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter your last name'
			),
		),

		// email address
		'email' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid email address'
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please enter a valid email address'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'That email address has already been registered'
			)
		),

    // password
    'password' => array(
      'minlength' => array(
        'rule' => array('minlength',8),
        'message' => 'Password must be at least 8 characters long.',
      ),
    ),

    // confirm password
    'confirm_password' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'required' => true,
        'message' => 'Please make sure you typed in both passwords correctly. ',
      ),
      'matches' => array(
        'rule' => array('matches','password'),
        'message' => 'Please make sure you typed in both passwords correctly. ',
      )
    )
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			
			// enabling this causes customers to be associated with null customerAccounts
			// 'conditions' => array('CustomerAccount.is_active' => true),
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
	);

/**
 * hasOne associations
 * 
 * @var array
 */
	public $hasOne = array(
		// PrimaryAccount points to a CustomerAccount object, 
		// but must be named differenctly because associaton names must be unique
		'PrimaryAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'primary_contact_cust_pers_id',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ProximityCard' => array(
			'className' => 'ProximityCard',
			'foreignKey' => 'person_id',
			'dependent' => false,
		),
		'CreditCard' => array(
			'className' => 'CreditCard',
			'foreignKey' => 'person_id',
			'dependent' => false,
		),
		'FaxNumber' => array(
			'className' => 'FaxNumber',
			'foreignKey' => 'person_id',
			'dependent' => false,
		),
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'requestedby_cust_pers_id',
			'dependent' => false
		),
		'PhoneNumber' => array(
			'className' => 'PhoneNumber',
			'foreignKey' => 'person_id',
			'dependent' => false,
		),
	);

/**
 * Find all customers on a floor
 * 
 * @return array $customers list of customers whose customerAccount's primary floor matches
 */
	public function findOnFloor($floor_id = null, $field = 'email') {

		// check that floor id is valid
		if (!$this->CustomerAccount->Floor->exists($floor_id)) {
			return false;
		}

		// customers related to this floor
		$customers = $this->find('list', array(
			'conditions' => array(
				'CustomerAccount.is_active' => true,
				'CustomerAccount.primary_floor_id' => $floor_id
			),
			'fields' => array('id',$field),
			'contain' => array('CustomerAccount')
		));
		return $customers;
	}

}

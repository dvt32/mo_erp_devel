<?php
App::uses('AppModel', 'Model');
App::uses('CakeTime', 'Utility');
/**
 * CustomerAccount Model
 *
 * @property PrimaryContact $PrimaryContact
 * @property Service $Service
 * @property Floor $Floor
 * @property BankAccount $BankAccount
 * @property CreditCard $CreditCard
 * @property Mailbox $Mailbox
 * @property ProximityCard $ProximityCard
 * @property Reservation $Reservation
 * @property Alias $Alias
 * @property ProximityCardEdit $ProximityCardEdit
 * @property IpAddress $IpAddress
 * @property Customer $Customer
 */
class CustomerAccount extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'default';


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cust_acct_tbl';


/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'public.account_seq';

/**
 * Use id as the primary key
 * 
 * @var string
 */
	public $primaryKey = 'id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * CustomerAccount types
 * 
 * @var array
 */
	public $types = array(
		'customer' => 'Customer',
		'vendor' => 'Vendor'
	);

/**
 * Payment methods available
 */
	public $preferred_payment_methods = [
		'ach' => 'ACH',
		'check' => 'Check',
		'credit card' => 'Credit Card',
	];


/**
 * Set of available virtual fields
 * @var array
 */
	public $virtualFields_available = array(

		'days_since_last_review' => 
			"SELECT DATE_PART('day', now() - billing_last_reviewed_dt)",

		// days_overdue of oldest (undisputed) aging billing amount
		'days_overdue' => 
			"SELECT (DATE_PART('day', now() - (
				-- oldest 'aging' billing amount for this customer
				SELECT due_date
				FROM billing_amount_tbl
				WHERE billing_amount_tbl.cust_acct_id=CustomerAccount.id
				AND billing_amount_tbl.stage LIKE 'aging%'
				ORDER BY due_date ASC
				LIMIT 1
			)))",

		// number of billing amounts with status 'receiving'
		'receiving_count' =>
			"SELECT COUNT(*)
			FROM billing_amount_tbl
			WHERE billing_amount_tbl.cust_acct_id=CustomerAccount.id
			AND billing_amount_tbl.stage='receiving'",

		// number billing amounts with status 'aging'
		'aging_count' =>
			"SELECT COUNT(*)
			FROM billing_amount_tbl
			WHERE billing_amount_tbl.cust_acct_id=CustomerAccount.id
			AND billing_amount_tbl.stage LIKE 'aging%'",

		// number of billing amounts with status 'invoicing'
		'invoicing_count' =>
			"SELECT COUNT(*)
			FROM billing_amount_tbl
			WHERE billing_amount_tbl.cust_acct_id=CustomerAccount.id
			AND billing_amount_tbl.stage LIKE 'invoicing%'",

		// number of billing amounts with status 'paid'
		'paid_count' =>
			"SELECT COUNT(*)
			FROM billing_amount_tbl
			WHERE billing_amount_tbl.cust_acct_id=CustomerAccount.id
			AND billing_amount_tbl.stage LIKE 'paid%'",
		
		// number of billing amounts with status 'disputed'
		'disputed_count' =>
			"SELECT COUNT(*)
			FROM billing_amount_tbl
			WHERE billing_amount_tbl.cust_acct_id=CustomerAccount.id
			AND billing_amount_tbl.stage LIKE 'disputed%'",

		'billing_memos_count' =>
			"SELECT COUNT(*)
			FROM billing_memo_tbl
			WHERE billing_memo_tbl.cust_acct_id=CustomerAccount.id
			AND billing_memo_tbl.is_active=true
			AND billing_memo_tbl.is_applied=false"
	);


/**
 * CustomerAccount room minutes
 * 
 * @var array
 */
	public $room_minutes = array(
		'0' => '0',
		'240' => '240',
		'600' => '600',
		'900' => '900',
		'1200' => '1200',
		'1500' => '1500',
		'1800' => '1800',
		'2400' => '2400',
		'9600' => '9600',
	);


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(

		// account name
		'name' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'A customer account with that name already exists.'
			)
		),
		
		// primary contact
		'primary_contact_cust_pers_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'on' => 'update',
				'message' => 'A primary contact is required'
			)
		),

		// website url
		'web_site' => array(
			'url' => array(
				'rule' => 'url',
				'required' => false,
				'allowEmpty' => true,
				'message' => 'Please enter properly formatted website url'
			)
		),
		// active flag
		'is_active' => array(
			'boolean' => array(
				'rule' => 'boolean',
			),
		),

		'service_id' => array(
			'numeric' => array(
				'rule' => 'numeric',
				'required' => true,
				'message' => 'Please select a service'
			)
		),

		// primary floor
		'primary_floor_id' => array(
			'numeric' => array(
				'rule' => 'numeric',
				'required' => true,
				'message' => 'Please select a floor'
			)
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PrimaryCustomer' => array(
			'className' => 'Customer',
			'foreignKey' => 'primary_contact_cust_pers_id',
		),
		'Service' => array(
			'className' => 'Service',
			'foreignKey' => 'service_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'primary_floor_id',
		),
		'BillingReviewer' => array(
			'className' => 'Employee',
			'foreignKey' => 'billing_reviewer_id'
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Alias' => array(
			'className' => 'Alias',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'cust_acct_id',
			'conditions' => array('Customer.is_active' => true),
			'dependent' => false,
		),
		'FaxNumber' => array(
			'className' => 'FaxNumber',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Desk' => array(
			'className' => 'Desk',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Mailbox' => array(
			'className' => 'Mailbox',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'MailPackage' => array(
			'className' => 'MailPackage',
			'foreignKey' => 'customer_id',
			'dependent' => false,
		),
		'Reservation' => array(
			'className' => 'Reservation',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'IpAddress' => array(
			'className' => 'IpAddress',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'PhoneNumber' => array(
			'className' => 'PhoneNumber',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'ProximityCard' => array(
			'className' => 'ProximityCard',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Cdr' => array(
			'className' => 'Cdr',
			'foreignKey' => 'account_id',
			'dependent' => false,
		),
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'requestedby_cust_acct_id',
			'dependent' => false,
		),
		'BillingMemo' => array(
			'className' => 'BillingMemo',
			'foreignKey' => 'cust_acct_id',
			'conditions' => array('is_active' => true)
		),
		'BillingAmount' => array(
			'className' => 'BillingAmount',
			'foreignKey' => 'cust_acct_id',
			'order' => 'created DESC',
			'limit' => 1
		),

		// Received Payments
		'PaymentsThisCycle' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'cust_acct_id',
			'order' => 'recv_dt DESC'
		),
		
		'ReceivedPaymentThisQuarter' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'cust_acct_id',
			'order' => 'recv_dt DESC'
		),
		'ReceivedPaymentThisHalfYear' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'cust_acct_id',
			'order' => 'recv_dt DESC'
		),
		'ReceivedPaymentThisYear' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'cust_acct_id',
			'order' => 'recv_dt DESC'
		),
		'ReceivedPaymentThisTwoYear' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'cust_acct_id',
			'order' => 'recv_dt DESC'
		),

		'ReceivedPayment' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'cust_acct_id',
			'order' => 'recv_dt DESC'
		),

		// Licence Agreements
		'LicenseAgreement' => array(
			'className' => 'LicenseAgreement',
			'foreignKey' => 'account_id',
		),

		// Aged Receivables
		'Promise' => array(
			'className' => 'Promise',
			'foreignKey' => 'cust_acct_id'
		),
		'AgingInteraction' => array(
			'className' => 'AgingInteraction',
			'foreignKey' => 'cust_acct_id'
		),

		'CreditCardTxn' => array(
			'className' => 'CreditCardTxn',
			'foreignKey' => 'cust_acct_id'
		),
		'AchDebitTransaction' => array(
			'className' => 'AchDebitTransaction',
			'foreignKey' => 'cust_acct_id'
		)
	);

/**
 * Overload constructor
 */
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);


		/* Set conditions for the different types of payment ranges */
		$dayOfMonthCycleStarts = $dom = 20; // or 1 for Harlem Garage

		// this cycle
		$start_cycle = cycle_start_dt(20); // functions.php
		$this->hasMany['PaymentsThisCycle']['conditions'] = array('recv_dt >' => $start_cycle);


		// this quarter
		$quarter = CakeTime::toQuarter('today',true);
		$this->hasMany['ReceivedPaymentThisQuarter']['conditions'] = array(
			'recv_dt >' => $quarter[0] . ' 00:00:00',
		);

		$last_year = (date('Y')-1) . '-01-01 00:00:00';
		$new_year = date('Y-01-01 00:00:00');
		$half_year = date('Y-07-01 00:00:00');
		$end_year = date('Y-12-31 23:59:59');
		
		// this half-year
		$today_is_first_half = date('Y-m-d H:i:s') < $half_year;
		if ($today_is_first_half) {
			$this->hasMany['ReceivedPaymentThisHalfYear']['conditions'] = array(
				'recv_dt >' => $new_year,
				'recv_dt <' => $half_year
			);
		} else {
			$this->hasMany['ReceivedPaymentThisHalfYear']['conditions'] = array(
				'recv_dt >' => $half_year
			);
		}
		// this year
		$this->hasMany['ReceivedPaymentThisYear']['conditions'] = array(
			'recv_dt >' => $new_year
		);

	}

/**
 * Calculates room minutes remaining
 * @return int
 */
	public function roomMinutesRemaining($id = null) {

		// fetch recurring room minutes per month
		$this->id = $id;
		$room_minutes = $this->field('room_minutes');

		// get room minutes used this month
		$minutes_used = $this->roomMinutesUsed($id);

		// calculate minutes remaining this month
		$minutes_remaining = $room_minutes - $minutes_used;
		
		return $minutes_remaining;
	}

/**
 * Fetch room minutes used this month
 * 
 * @param int $cust_acct_id id of the account to lookup
 * @return int $minutes_used minutes used this month
 */
	public function roomMinutesUsed($cust_acct_id = null) {

		// current year and month
		$year = date('Y');
		$month = date('m');

		$results = $this->query(
			"SELECT SUM( (EXTRACT(epoch FROM end_dt) - EXTRACT(epoch FROM start_dt)) / 60 ) AS minutes_used
			FROM reservation_tbl
			WHERE account_id = '{$cust_acct_id}'

			AND EXTRACT(year FROM end_dt) = '{$year}'
			AND EXTRACT(year FROM start_dt) = '{$year}'

			AND EXTRACT(month FROM end_dt) = '{$month}'
			AND EXTRACT(month FROM start_dt) = '{$month}'"
		);

		return $results[0][0]['minutes_used'] ?: 0;
	}


/**
 * Deactivate a customer account and all associated customers
 * 
 * @param int $id id of customer account to deactivate
 * @return boolean true if all updates were successful
 */
	public function deactivate($id = null) {
		$this->setIsActive($id,0);
	}

/**
 * Reactivate a customer acccount and all associated customers
 * 
 * @param int $id id of customer account to reactivate
 * @return boolean true if all updates were successful
 */
	public function reactivate($id = null) {
		$this->setIsActive($id, 1);
	}

/**
 * Set is_active for a customer account and its associated customers
 * NOTE: previous customer statuses cannot be restored
 * 
 * @param int $id id of customer account
 * @param boolean $value value to set
 * @return boolean true if all updates were successful
 */
	private function setIsActive($id = null, $value) {

		// set active record id
		$this->id = $id ?: $this->id;

		// check customerAccount id
		if ($this->exists()) {
			return false;
		}

		// fetch associated customers
		$aok = $this->saveField('is_active', $value);
		$aok = $aok && $this->Customer->updateAll(
			array('is_active' => $value),
			array('conditions' => array('Customer.cust_acct_id' => $this->id))
		);

		return $aok;
	}

/**
 * Marks the customer accounts "bill as is"
 */
	public function updateReviewed($cust_acct_ids = array()) {

		$employee_id = AuthComponent::user('id');

		// mark these customer account as "reviewed"
		$fields = array(
			'CustomerAccount.billing_last_reviewed_dt' => 'now()',
			'CustomerAccount.billing_reviewer_id' => $employee_id
		);
		$conditions = array('CustomerAccount.id' => $cust_acct_ids);
		$saved = $this->updateAll($fields, $conditions);

		// save a new billing amount
		$billingAmounts = array();

		return $saved;
	}
	
/**
 *
 * Billing Cycle Counts
 *
 * @param string month-year month and year of that billing cycle
 * @return array counts of each billing status
 * 
 */
	public function billCounts() {

		$billingStatuses = array(
			'billing' => $this->billingCount(),
			'invoicing' => $this->invoicingCount(),
			'receiving' => $this->receivingCount(),
			'aging' => $this->agingCount(),
			'total' => $this->find('count', array(
				'conditions' => array(
					'is_active' => 1
				)
			))
		);

		return $billingStatuses;
	}
/**
 *
 * BillingCount
 *
 * @param array floor_array pass query from controller of floors
 */
	public function billingCount($floor_array = array()) {

		if (!empty($floor_array)) {

			$ba = array(
				// if null or if the datetime is greater than a 1 month
				'not_reviewed' => $this->find('count', array(
					'conditions' => array(
						'is_active' => 1,
						'OR' => array(
							'billing_last_reviewed_dt <' => date(DATETIME_FORMAT, strtotime("-1 month")),
							'billing_last_reviewed_dt' => null,
						),
						'primary_floor_id' => $floor_array
					)
				)),
				// if datetime is within this month's billing cycle
				'reviewed' => $this->find('count', array(
					'conditions' => array(
						'NOT' => array(
							'billing_last_reviewed_dt' => null,
						),
						'billing_last_reviewed_dt >' => date(DATETIME_FORMAT, strtotime("-1 month")),
						'is_active' => 1,
						'primary_floor_id' => $floor_array
					)
				))
			);

			return $ba;
			

		} else {

			$ba = array(
				// if null or if the datetime is greater than a 1 month
				'not_reviewed' => $this->find('count', array(
					'conditions' => array(
						'is_active' => 1,
						'OR' => array(
							'billing_last_reviewed_dt <' => date(DATETIME_FORMAT, strtotime("-1 month")),
							'billing_last_reviewed_dt' => null,
						)
					)
				)),
				// if datetime is within this month's billing cycle
				'reviewed' => $this->find('count', array(
					'conditions' => array(
						'NOT' => array(
							'billing_last_reviewed_dt' => null,
						),
						'billing_last_reviewed_dt >' => date(DATETIME_FORMAT, strtotime("-1 month")),
						'is_active' => 1
					)
				))
			);

			return $ba;

		}

	}

	public function invoicingCount($conditions = array(), $limit = array()) {

		$ba = array(
			// if the inbox is not zero
			'not_reviewed' => $this->find('count', array(
				'conditions' => $conditions,
				'limit' => $limit
			)),
			// if inbox is zero
			'reviewed' => $this->find('count', array(
				'conditions' => array(
					'OR' => array(
						'billing_memos_archived_count !=' => 0,
					),
					'is_active' => 1
				),
				'limit' => $limit
			))
		);

		return $ba;
	}

/**
 * gets number of customer accounts that have a balance > 0
 * @param mix $floor_id null, 
 * @return array $counts array with 'received' and 'not_received' numbers
 */
	public function receivingCount($floor_id = null) {

		if (!empty($floor_id)) {

			$billingAmount = array(
				// if customer account balance is zero
				'received' => $this->find('count', array(
					'conditions' => array(
						'is_active' => 1,
						'balance =' => 0,
						'primary_floor_id' => $floor_id
					),
				)),
				// if datetime is within this month's billing cycle
				'not_received' => $this->find('count', array(
					'conditions' => array(
						'OR' => array(
							'balance' => null,
							'balance >' => 0,
						),
						'is_active' => 1,
						'primary_floor_id' => $floor_id
					)
				))
			);

		} else {

			$billingAmount = array(
				// if customer account balance is zero
				'received' => $this->find('count', array(
					'conditions' => array(
						'is_active' => 1,
						'balance =' => 0
					),
				)),
				// if datetime is within this month's billing cycle
				'not_received' => $this->find('count', array(
					'conditions' => array(
						'OR' => array(
							'balance' => null,
							'balance >' => 0,
						),
						'is_active' => 1
					)
				))
			);

		}
		return $billingAmount;

	}

/**
 * AgingCount
 */
	public function agingCount() {

		$ba = array(
			// if customer account balance is zero
			'aging_1' => $this->find('count', array(
				'conditions' => array(
					'is_active' => 1,
					'aging_count =' => 0
				),
			)),
			// if customer account balance is zero
			'aging_2' => $this->find('count', array(
				'conditions' => array(
					'is_active' => 1,
					'aging_count =' => 1
				),
			)),
			// if customer account balance is zero
			'aging_3' => $this->find('count', array(
				'conditions' => array(
					'is_active' => 1,
					'aging_count =' => 2
				),
			)),
			// if customer account balance is zero
			'aging_4' => $this->find('count', array(
				'conditions' => array(
					'is_active' => 1,
					'aging_count =' => 3
				),
			)),
			// if customer account balance is zero
			'total' => $this->find('count', array(
				'conditions' => array(
					'is_active' => 1,
					'aging_count >' => 0
				),
			))
		);

		return $ba;

	}

/**
 * Graph Snapshot
 */

	public function graph() {

		$monthsToDateThisYear = array();

		$start = new DateTime(date('Y')."-01-01");
		$end      = new DateTime(date('Y-m-d'));
		$end->modify('first day of next month');
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);

		$months = array();

		foreach ($period as $dt) {
		    array_push($months, $dt);
		}
		// debug($months);
		$billed = array();
		$received = array();

		$this->bindModel(array(
			'hasMany' => array(
				'BillingAmount' => array(
					'className' => 'BillingAmount'
				)
			)
		));
		// grab all the billing amounts by created
		foreach($months as $ba) {
			array_push($billed,
				array((int) $ba->format('n'), $this->BillingAmount->find('count', array(
					'conditions' => array(
						'created >' => $ba->format('Y-m-01 00:00:00'),
						'created <=' => $ba->format('Y-m-t 23:59:59')
				))
			)));
			array_push($received,
				array((int) $ba->format('n'), $this->BillingAmount->find('count', array(
					'conditions' => array(
						'created >' => $ba->format('Y-m-01 00:00:00'),
						'created <=' => $ba->format('Y-m-t 23:59:59'),
						'balance <=' => 0
				))
			)));
		}
		$result = array('billed' => $billed, 'received' => $received);
		return $result;

	}

	public function updateBillingInformation($id)
	{
		$balanced_updated = $this->updateBalance($id);
		$aging_count_updated = $this->updateAgingCount($id);
		$billing_memos_count_updates = $this->updateBillingMemosInboxCount($id);

		return $balanced_updated && $aging_count_updated && $billing_memos_count_updates;
	}

/**
 * Convenience function, calls all billing-related upate function in cascading order
 */
	public function updateAllBillingInformation() {

		// 1. update BillingAmount balances
		$this->BillingAmount->updateAllBalances(false); // false means do not cascade
		
		// 2. update CustomerAccount balances
		$this->updateAllBalances();

		// 3. update CustomerAccount aging-counts
		$this->updateAllAgingCounts();

		// 4. update CustomerAccount memo-counts
		$this->updateAllBillingMemosInboxCounts();
		$this->updateAllBillingMemosArchivedCounts();

		// 5. update BillingAmount statuses
		$this->BillingAmount->updateAllStatuses();
	}

/**
 * Updates the CustomerAccount balance taking into account
 * BillingAmount, ReceivedPayment (check and cash), CreditCardTransaction, AchDebitTransaction values
 * NOTE: identical method found in BizCenter CustomerAccount model
 *
 * @param int $id customer account id
 * @return void
 */
	public function updateBalance($id = false) {

		// where clause for query
		$where_clause = '';
		if (!empty($id)) {
			if (is_array($id)) {
				$ids = implode(',',$id);
				$where_clause = "WHERE cust_acct_tbl.id IN ({$ids})";
			} else {
				$where_clause = "WHERE cust_acct_tbl.id={$id}";
			}
		}

		// define billing epoch
		$billing_epoch = BILLING_EPOCH;

		/*
		 *   sum(billing_amount.grand_total)
		 * - sum(recv_payment.amount) [checks]
		 * - sum(recv_payment.amount) [cash]
		 * - sum(credit_card_txn.total_charge)
		 * - sum(ach_debit_txn.amount)
		 * ___________________________________
		 * = cust_acct.balance
		 */
		// FIXME balance not updated correctly
		$query = 
			"UPDATE cust_acct_tbl
			SET balance=(
				-- sum of billing amount grand total
				COALESCE((SELECT SUM(billing_amount_tbl.grand_total)
						FROM billing_amount_tbl
						WHERE billing_amount_tbl.cust_acct_id=cust_acct_tbl.id), 0)
				-
				-- sum of cash/check payments in recv_payments
				COALESCE((SELECT SUM (amount)
						FROM recv_payment_tbl
						WHERE recv_payment_tbl.cust_acct_id=cust_acct_tbl.id
						AND recv_payment_tbl.payment_method IN ('cash','check')
						AND process_status IN ('pending','complete') -- remove 'pending'
						AND recv_payment_tbl.recv_dt >= '{$billing_epoch}'), 0)
				-
				-- sum of credit_card_txn_tbl.total_charge
				COALESCE((SELECT SUM (total_charge)
						FROM credit_card_txn_tbl
						WHERE credit_card_txn_tbl.cust_acct_id=cust_acct_tbl.id
						AND credit_card_txn_tbl.response_approved='APPROVED'
						AND credit_card_txn_tbl.response_txn_dt >= '{$billing_epoch}'), 0)
				-
				-- sum of ach_debit_txn_tbl
				COALESCE((SELECT SUM (amount)
						FROM ach_debit_txn_tbl
						WHERE ach_debit_txn_tbl.cust_acct_id=cust_acct_tbl.id
						AND ach_debit_txn_tbl.cancelled=false
						AND ach_debit_txn_tbl.processed_dt >= '{$billing_epoch}'), 0)
			) {$where_clause}";

		$saved = $this->query($query);
		return true;
	}

	public function getCalcBalance($id = null)
	{
		// set the id
		$this->id = $id = $id ?: $this->id;

		// fetch sums
		$owed = $this->sumBalanceGrandTotals($id);
		$ach  = $this->sumAchDebitPayments($id);
		$cc   = $this->sumCreditCardPayments($id);
		$recv = $this->sumReceivedPayments($id);

		debug(compact('owed', 'ach', 'cc', 'recv'));

		// calculate the balance
		$balance = $owed - $ach - $cc - $recv;

		return $balance;
	}

	public function getCachedBalance($id = null)
	{
		// set the id
		$this->id = $id = $id ?: $this->id;

		// update the balance
		$this->updateBalance($id);

		// fetch the updated balance
		$balance = $this->field('balance');
		return $balance;
	}

	public function sumBalanceGrandTotals($cust_acct_id, $start_dt = false, $end_dt = false)
	{
		$start_dt = $start_dt ?: BILLING_EPOCH;
		$end_dt = $end_dt ?: date(DATETIME_FORMAT);

		$billing_amount_grand_total_sum = $this->query(
			"SELECT SUM(billing_amount_tbl.grand_total)
			FROM billing_amount_tbl
			WHERE billing_amount_tbl.cust_acct_id={$cust_acct_id}"
		);
		$billing_amount_grand_total_sum = $billing_amount_grand_total_sum[0][0]['sum'] ?: 0;
		return $billing_amount_grand_total_sum;
	}

	public function sumAchDebitPayments($cust_acct_id, $start_dt = false, $end_dt = false)
	{
		$start_dt = $start_dt ?: BILLING_EPOCH;
		$end_dt = $end_dt ?: date(DATETIME_FORMAT);

		$ach_debit_txn_sum = $this->query(
			"SELECT SUM (amount)
			FROM ach_debit_txn_tbl
			WHERE ach_debit_txn_tbl.cust_acct_id='{$cust_acct_id}'
			AND ach_debit_txn_tbl.cancelled=false
			AND ach_debit_txn_tbl.processed_dt >= '{$start_dt}'
			AND ach_debit_txn_tbl.processed_dt <= '{$end_dt}'");
		$ach_debit_txn_sum = $ach_debit_txn_sum[0][0]['sum'] ?: 0;
		return $ach_debit_txn_sum;
	}

	public function sumCreditCardPayments($cust_acct_id, $start_dt = false, $end_dt = false)
	{
		$start_dt = $start_dt ?: BILLING_EPOCH;
		$end_dt = $end_dt ?: date(DATETIME_FORMAT);

		$credit_card_txn_sum = $this->query(
			"SELECT SUM (total_charge)
			FROM credit_card_txn_tbl
			WHERE credit_card_txn_tbl.cust_acct_id='{$cust_acct_id}'
			AND credit_card_txn_tbl.response_approved='APPROVED'
			AND credit_card_txn_tbl.response_txn_dt >= '{$start_dt}'
			AND credit_card_txn_tbl.response_txn_dt <= '{$end_dt}'");
		$credit_card_txn_sum = $credit_card_txn_sum[0][0]['sum'] ?: 0;
		return $credit_card_txn_sum;
	}

	public function sumReceivedPayments($cust_acct_id, $start_dt = false, $end_dt = false)
	{
		$start_dt = $start_dt ?: BILLING_EPOCH;
		$end_dt = $end_dt ?: date(DATETIME_FORMAT);

		$recv_payment_sum = $this->query(
			"SELECT SUM (amount)
			FROM recv_payment_tbl
			WHERE recv_payment_tbl.cust_acct_id='{$cust_acct_id}'
			AND recv_payment_tbl.payment_method IN ('cash','check')
			AND process_status IN ('pending','complete')
			AND recv_payment_tbl.recv_dt >= '{$start_dt}'
			AND recv_payment_tbl.recv_dt <= '{$end_dt}'");
		$recv_payment_sum = $recv_payment_sum[0][0]['sum'] ?: 0;
		return $recv_payment_sum;
	}

/**
 * Update all CustomerAccount balances
 */
	public function updateAllBalances() {
		$this->updateBalance();
	}

/**
 * Update CustomerAccount aging count
 * Depends on accurate billing_amount_tbl.stage
 */
	public function updateAgingCount($id = false) {

		// where clause for query
		$where_clause = '';
		if (!empty($id)) {
			if (is_array($id)) {
				$ids = implode(',',$id);
				$where_clause = "WHERE cust_acct_tbl.id IN ({$ids})";
			} else {
				$where_clause = "WHERE cust_acct_tbl.id={$id}";
			}
		}

		// run the query
		$results = $this->query(
			"UPDATE cust_acct_tbl
			SET aging_count=(
				SELECT COUNT(*)
				FROM billing_amount_tbl
				WHERE billing_amount_tbl.stage LIKE 'aging_%'
				AND billing_amount_tbl.cust_acct_id = cust_acct_tbl.id
			) " . $where_clause
		);
	}

/**
 * Update all CustomerAccount aging counts
 */
	public function updateAllAgingCounts() {
		$this->updateAgingCount();
	}
	
/**
 * Update CustomerAccount billing memo inbox count
 */
	public function updateBillingMemosInboxCount($id = false) {

		// where clause for query
		$where_clause = '';
		if (!empty($id)) {
			if (is_array($id)) {
				$ids = implode(',',$id);
				$where_clause = "WHERE cust_acct_tbl.id IN ({$ids})";
			} else {
				$where_clause = "WHERE cust_acct_tbl.id={$id}";
			}
		}

		// run the query
		$this->query(
			"UPDATE cust_acct_tbl
			SET billing_memos_inbox_count=(
				SELECT count(*)
				FROM billing_memo_tbl
				WHERE cust_acct_id=cust_acct_tbl.id
				AND is_applied=FALSE
				AND is_active=TRUE
			) " . $where_clause
		);
	}
	
/**
 * Update CustomerAccount billing memo inbox count
 */
	public function updateAllBillingMemosInboxCounts() {
		$this->updateBillingMemosArchivedCount();
	}

/**
 * Update CustomerAccount billing memo archived count
 */
	public function updateBillingMemosArchivedCount($id = false) {

		// where clause for query
		$where_clause = '';
		if (!empty($id)) {
			if (is_array($id)) {
				$ids = implode(',',$id);
				$where_clause = "WHERE cust_acct_tbl.id IN ({$ids})";
			} else {
				$where_clause = "WHERE cust_acct_tbl.id={$id}";
			}
		}

		// run the query
		$this->query(
			"UPDATE cust_acct_tbl
			SET billing_memos_archived_count=(
				SELECT count(*)
				FROM billing_memo_tbl
				WHERE cust_acct_id=cust_acct_tbl.id
				AND is_applied=TRUE
				AND is_active=TRUE
			) " . $where_clause
		);
	}

/**
 * Update all CustomerAccount billing memo archive counts
 */
	public function updateAllBillingMemosArchivedCounts() {
		$this->updateBillingMemosArchivedCount();
	}

/**
 * Convenience query for dropdowns relating to A/R. 
 * Get list of aging customer accounts
 */
	public function getAgingList() {

		// add virtual field name and aging count
		$this->virtualFields['name_and_aging_count'] = 
			"CustomerAccount.name || ' (' || CustomerAccount.aging_count || ')'";

		// fetch list of CustomerAccounts for dropdown
		$customerAccounts = $this->find('list', array(
			'conditions' => array(

				// exclude inactive customer accounts
				'is_active' => 1,

				// exclude customer accounts with no aging invoices
				'aging_count >' => 0
			),

			// define dropdown key/value pair
			'fields' => array('id','name_and_aging_count'),

			// sort by aging count descending
			'order' => 'aging_count DESC'
		));

		return $customerAccounts;
	}

/**
 * Get list of CustomerAccounts with a billingAmount with stage=receiving
 * 
 */
	public function getReceivingList() {

		// counts number of billing amounts 
		$this->virtualFields['receiving_count'] = $this->virtualFields_available['receiving_count'];

		$customerAccounts = $this->find('all', array(
			'conditions' => array(

				// exclude inactive customer accounts
				'is_active' => 1,

				// exclude customer accounts with no receiving invoices
				'receiving_count >' => 0
			),

			'fields' => array('id','name','receiving_count'),

			'order' => 'name ASC'
		));

		return $customerAccounts;
	}


/**
 * Update CustomerAccount.billing_memos_inbox_count
 */
	public function updateBillingMemoCounts($cust_acct_id = null) {

		if (!empty($cust_acct_id)) {
			$this->updateBillingMemosInboxCount($cust_acct_id);
			$this->updateBillingMemosArchivedCount($cust_acct_id);
		}
	}

}

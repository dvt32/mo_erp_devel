<?php
App::uses('AppModel', 'Model');
/**
 * Deposit Model
 *
 * @property Employee $Employee
 * @property ReceivedPayment $ReceivedPayment
 */
class Deposit extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'deposit_tbl';


/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.deposit_seq';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ReceivedPayment' => array(
			'className' => 'ReceivedPayment',
			'foreignKey' => 'deposit_id',
			'dependent' => false,
		)
	);

/**
 * MicroOffice Bank Accounts
 */
	public $bank_accounts = array(
		'MOM' => array(
			'name' => 'Citibank',
			'routing_number' => '021000089',
			'account_number' => '9962340795'
		),
		'MOM' => array(
			'name' => 'Chase',
			'routing_number' => '021000021',
			'account_number' => '966373888'
		),
		'MOS2' => array(
			'name' => 'Chase',
			'routing_number' => '021000021',
			'account_number' => '914025903'
		),
		'US902S' => array(
			'name' => 'Chase',
			'routing_number' => '021000021',
			'account_number' => '973118904'
		),
		'1601BwyS' => array(
			'name' => 'Chase',
			'routing_number' => '021000021',
			'account_number' => '157786150'
		)
	);

	public function bankAccounts() {
		$list = array();
		foreach ($this->bank_accounts as $entity => $account) {
			$list[$entity] = sprintf(
				'%s - %s - *****%s',
				$entity,
				$account['name'],
				substr($account['account_number'],-4)
			);
		}
		return $list;
	}

}

<?php
App::uses('AppModel', 'Model');
/**
 * Desk Model
 *
 * @property Floor $Floor
 * @property Account $Account
 * @property CustPers $CustPers
 * @property DeskAssignmentHistoryTbl $DeskAssignmentHistoryTbl
 */
class Desk extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'desk_tbl';


/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.desk_seq';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $is_encloseds = array("1", "");


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'cust_pers_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'DeskAssignmentHistory' => array(
			'className' => 'DeskAssignmentHistory',
			'foreignKey' => 'desk_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

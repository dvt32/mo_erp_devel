<?php
App::uses('AppModel', 'Model');
/**
 * DeskAssignmentHistory Model
 *
 * @property Desk $Desk
 * @property CustAcct $CustAcct
 * @property CustPers $CustPers
 */
class DeskAssignmentHistory extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'desk_assignment_history_tbl';


/**
 * Use sequence
 * 
 * @var string
 */
	// NOTICE id is reindexed with serial
	public $sequence = 'public.desk_assignment_history_seq';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Desk' => array(
			'className' => 'Desk',
			'foreignKey' => 'desk_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'cust_pers_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

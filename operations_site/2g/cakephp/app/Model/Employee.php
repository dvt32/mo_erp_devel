<?php
App::uses('User', 'Model');


/**
 * Employee Model
 */
class Employee extends User {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'employee_tbl';


/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'public.person_seq';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'full_name';


/**
 * Username field
 *
 * @var string
 */
	public $usernameField = 'email';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		// TODO add belongsTo associations
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Role' => array(
			'className' => 'EmployeeRole',
			'foreignKey' => 'parent_id',
			'dependent' => true
		),
		'AssignedTask' => array(
			'className' => 'Task',
			'foreignKey' => 'assigned_employee_id',
			'dependent' => false
		),
		'RequestedTask' => array(
			'className' => 'Task',
			'foreignKey' => 'requestedby_employee_id',
			'dependent' => false
		),
		'LicenseAmendmentTemplate' => array(
			'className' => 'LicenseAmendmentTemplate',
			'foreignKey' => 'modifier_id',
			'dependent' => false
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'office_manager_id',
			'dependent' => false
		)
	);


/**
 * Overload constructor
 */
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['full_name'] = sprintf('%s.first_name || \' \' || %s.last_name', $this->alias, $this->alias);
		$this->loadFields(array('titles','types','companies'));
	}


/**
 * Authenticates a user by checking that the password hash against the one stored in the database
 * IMPORTANT: this authentication method is different from the BizCenter implementation.
 * 
 * @return boolean true if user credentials are correct, false otherwise
 */
	public function authenticate($id, $password) {

		// if the $id exists
		if ($this->exists($id)) {

			// retrieve the salt and make the target hash
			$hash = crypt($password, $this->field('salt'));

			// count number of distinct matches
			$count = $this->find('count', array(
				'fields' => 'DISTINCT Employee.id',
				'conditions' => array('Employee.id' => $id, 'Employee.hash' => $hash))
			);

			// exactly 1 row should match the given credentials
			return 1 === $count;
		}

		// no such user
		return false;
	}


/**
 * Get a list of roles associated with employee of the given id
 * 
 * @param $id id of the employee
 * @return array list of assigned roles
 */
	public function roles($id = null) {
		$id = $id ?: $this->id;
		$conditions = array('Role.parent_id' => $id);
		$roles = $this->Role->find('list',compact('conditions'));
		return $roles;
	}

/**
 * Sets a flag forcing an employee to reload roles
 * 
 * @return boolean true if the save was successful
 */
	public function setReloadRolesFlag($id = null) {
		$this->id = $id ?: $this->id;
		return $this->saveField('reload_roles',true);
	}

/**
 * Unsets the flag forcing an employee to reload roles
 * 
 * @return boolean true if the save was successful
 */
	public function unsetReloadRolesFlag($id = null) {
		$this->id = $id ?: $this->id;
		return $this->saveField('reload_roles',false);
	}

/**
 * Check if the flag forcing an employee to reload roles is set
 * 
 * @return boolean true if the flag is set
 */
	public function checkReloadRolesFlag($id = null) {
		$this->id = $id ?: $this->id;
		return $this->field('reload_roles');
	}

/**
 * Fetches a list of active employees
 * 
 * @param $options additional parameters for find, e.g., 'fields'
 * @return list of active employees
 */
	public function getActiveList($options = array()) {
		$conditions = array('conditions' => array('is_active' => true));
		$order = array('order' => array('first_name ASC', 'last_name ASC'));
		$params = $conditions + $options + $order;
		return $this->find('list',$params);
	}

/**
 * Get floors managed by employee (typically an om)
 */
	public function myFloors($employee_id = null, $options = array()) {

		// fetch employee id
		$this->id = $employee_id = $employee_id ?: AuthComponent::user('id');

		// filters
		$floors = $this->Floor->find('list', array(
			'conditions' => array('office_manager_id' => $employee_id),
			'fields' => array('id', 'short_global_label')
		));

		return $floors;
	}


}

<?php
App::uses('AppModel', 'Model');
/**
 * EmployeeRole Model
 *
 */
class EmployeeRole extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ss_employee_x_role_tbl';

/**
 * Use sequence
 *
 * @var string "schema.sequencename"
 */
	public $sequence = 'public.ss_employee_x_role_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'element_value';


/**
 * belongsTo associations
 * 
 * @var array
 */
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'parent_id',
		)
	);


/**
 * Validation rules
 * 
 * @var array
 */
	public $validate = array(

		// assignee (employee)
		'parent_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => "One or more assignees is required"
			)
		),

		// name of role
		'element_value' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => "An element value for the role is required"
			)
		)
	);


/**
 * Overload constructor, creates the attribute $element_values and $role_map
 */
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->loadFields(array('element_values'));
		$this->role_map = array_flip($this->element_values);
	}


/**
 * Fetch the set of unique roles
 * 
 * @return array $roles list of roles
 */
	public function getUniqueRoles() {
		$alias = $this->alias;
		$roles = $this->find('all', array(
			'fields' => array("DISTINCT {$alias}.element_value"),
			'order' => "{$alias}.element_value ASC"
		));
		foreach ($roles as &$role) {$role = $role[$alias]['element_value']; }
		return $roles;
	}


/**
 * Checks if an employee already has the target role
 * 
 * @return boolean true if the employee already possesses role
 */
	public function hasRole($employee_id, $role) {

		if (is_int($role)) {
			$role = $this->role_map[$role];
		}
		$count = $this->find('count', array(
			'conditions' => array(
				$this->alias . '.parent_id' => $employee_id,
				$this->alias . '.element_value' => $role
			)
		));
		return $count === 1;
	}


/**
 * Assign roles to an employee
 * 
 * @param int $employee_id Id of employee to assign these roles
 * @param array $after Complete set of roles to assign
 * @return boolean True if roles were added and removed successfully, false otherwise
 */
	public function assignRolesToEmployee($employee_id, $after = array()) {

		// roles currently assigned
		$before = $this->Employee->roles();

		// map role numbers to names
		foreach ($after as &$role) {
			$role = $this->element_values[intval($role)];
		}

		// determine roles to add, remove, and leave-as-is
		$roles_to_add = array_diff($after,$before);
		$roles_to_del = array_diff($before,$after);

		// flag
		$aok = true;
		$reload = false;

		// add roles
		if (!empty($roles_to_add)) {
			// convert to saveMany convention
			foreach($roles_to_add as &$role_to_add) {
				$role_to_add = array('parent_id' => $employee_id, 'element_value' => $role_to_add);
			}
			// execute saveMany
			$aok = $this->saveMany($roles_to_add);
			$reload = true;
		}

		// delete roles
		if (!empty($roles_to_del)) {
			
			// convert to deleteAll convention
			$roles_to_del = array_keys($roles_to_del);
			
			// update assigned roles
			$aok = $aok && $this->deleteAll(array($this->alias . '.id' => $roles_to_del));
			$reload = true;
		}

		// set the reload flag if the employee should be forced to reload roles
		if ($reload) {
			$this->Employee->setReloadRolesFlag($employee_id);
		}

		return $aok;
	}


/**
 * Get list of employees assigned a specified role
 * 
 * @param mix $role either the role key or role name
 * @return array $employees list of employees assigned this role
 */
	public function assignees($role) {

		// converts role key to role name
		$element_value = @$this->element_values[$role] ?: $role;
		$assignments = $this->assignments(array("{$this->alias}.element_value" => $element_value));
		$employees = array();
		foreach ($assignments as $assignment) {
			$employees[] = $assignment['Employee']['id'];
		}
		return $employees;
	}


/**
 * Get all of the "HABTM join table" records between roles and employees
 * 
 * @return array $assignments list of habtm associations between employees and roles
 */
	public function assignments($conditions = array()) {
		return $this->find('all', array(
			'conditions' => $conditions,
			'fields' => 'id',
			'contain' => 'Employee.id',
			'order' => 'Employee.full_name ASC'
		));
	}

}


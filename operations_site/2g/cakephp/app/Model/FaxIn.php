<?php
App::uses('AppModel', 'Model');
/**
 * FaxIn Model
 *
 * @property Account $Account
 * @property Person $Person
 */
class FaxIn extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'fax_in_tbl';

/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.fax_in_seq';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);
}

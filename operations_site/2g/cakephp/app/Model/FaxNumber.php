<?php
App::uses('AppModel', 'Model');
/**
 * FaxNumber Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Customer $Customer
 */
class FaxNumber extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'fax_number_tbl';

/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.fax_number_seq';

/**
 * Email Action
 *
 * @array 
 */
	public $email_actions = array('notify', 'attach_delete', 'attach');


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

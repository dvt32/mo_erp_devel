<?php
App::uses('AppModel', 'Model');
/**
 * Floor Model
 *
 * @property Building $Building
 * @property Customer $Customer
 * @property Room $Room
 * @property ProximityCard $ProximityCard
 * @property Mailbox $Mailbox
 * @property AccessLevel $AccessLevel
 */
class Floor extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'floor_tbl';

/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.floor_seq';

/**
 * Display field
 */
	public $displayField = 'short_global_label';

/**
 * Proximity Card status
 * 
 */

	public $is_actives = array('false', 'true');

/**
 * belongsTo associations
 * @var array
 */
	public $belongsTo = array(
		'Building' => array(
			'className' => 'Building',
			'foreignKey' => 'building_id',
		),
		'OfficeManager' => array(
			'className' => 'Employee',
			'foreignKey' => 'office_manager_id',
			'fields' => array(
				'id','full_name','work_phone_number','email'
			)
		)
	);

/**
 * hasMany associations
 * @var array
 */
	public $hasMany = array(
		'NetworkSwitch' => array(
			'className' => 'NetworkSwitch',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'primary_floor_id',
			'dependent' => false,
		),
		'Room' => array(
			'className' => 'Room',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'Desk' => array(
			'className' => 'Desk',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'Mailbox' => array(
			'className' => 'Mailbox',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'AccessLevel' => array(
			'className' => 'AccessLevel',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
		'PhoneNumber' => array(
			'className' => 'PhoneNumber',
			'foreignKey' => 'floor_id',
			'dependent' => false,
		),
	);

/**
 * Fetches a list of active floors
 * 
 * @param $options additional parameters for find, e.g., 'fields'
 * @return list of active floors
 */
	public function listActiveOfficeFloors($options = array()) {
		$offices = $this->Building->find('list', array(
			'conditions' => array(
				'Building.is_active' => true,
				'Building.type' => 'officesuite'
			)
		));
		$conditions = array(
			'conditions' => array(
				'building_id' => array_keys($offices),
				'is_active' => true,
			)
		);
		$order = array('order' => 'short_global_label ASC');
		$params = $conditions + $options + $order;
		return $this->find('list',$params);
	}

}

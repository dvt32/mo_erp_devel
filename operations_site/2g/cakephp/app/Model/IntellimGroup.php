<?php
App::uses('AppModel', 'Model');
/**
 * IntellimGroup Model
 *
 */
class IntellimGroup extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'enum_intellim_group_tbl';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}

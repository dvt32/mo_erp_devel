<?php
App::uses('AppModel', 'Model');
/**
 * IpAddress Model
 *
 * @property Account $Account
 * @property Isp $Isp
 * @property Building $Building
 * @property RirOwner $RirOwner
 */
class IpAddress extends AppModel {


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ip_address_tbl';

	//The Associations below have been created with all possible keys, those that are not needed can be removed


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Isp' => array(
			'className' => 'Isp',
			'foreignKey' => 'isp_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Building' => array(
			'className' => 'Building',
			'foreignKey' => 'building_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		// 'RirOwner' => array(
		// 	'className' => 'RirOwner',
		// 	'foreignKey' => 'rir_owner_id',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => ''
		// )
	);
}

<?php
App::uses('AppModel', 'Model');
/**
 * IpPhone Model
 *
 */
class IpPhone extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'telecom';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ip_phone_tbl';

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->loadFields(array('manufacturers', 'models', 'ownerships'));
	}


// /**
//  * hasMany associations
//  *
//  * @var array
//  */
// 	public $hasMany = array(
// 		'PhoneNumber' => array(
// 			'className' => 'PhoneNumber',
// 			'foreignKey' => 'ip_phone_id',
// 			'dependent' => false,
// 		),

}

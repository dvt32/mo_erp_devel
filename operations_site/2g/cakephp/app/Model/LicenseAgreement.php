<?php
App::uses('AppModel', 'Model');
App::uses('HttpSocket', 'Network/Http');
use Guzzle\Http\Client;
use Guzzle\Http\EntityBody;

/**
 *
 * README TODO ALWAYS SANATIZE USER INPUT
 * 
 */

// // Prevent users from accessing sensitive files by sanitizing input
// $_POST = array('firstname' => '@/etc/passwd');
// $request = $client->post('http://www.example.com', array(), array (
//     'firstname' => str_replace('@', '', $_POST['firstname'])
// ));


/**
 * LicenseAgreement Model
 *
 * @property Prospect $Prospect
 * @property Modifier $Modifier
 * @property Desk $Desk
 * @property Account $Account
 * @property Person $Person
 */
class LicenseAgreement extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'license_agreement_tbl';

/**
 * Use sequence
 *
 * @var mixed False or sequence name
 */
	public $sequence = 'public.license_agreement_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'license_date' => array(
			'date' => array(
				'rule' => array('date'),
			),
		),
		'licensor_entity' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'signing_entity' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'You must assign a Signing Entity',
				'allowEmpty' => false,
				'required' => false,
			),
		),
		'company' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'first_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'last_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'spacenumber' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please specify a desk id!',
				'allowEmpty' => false,
			
			),
		),
		'offices' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a numeric value',
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'phone_lines' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a numeric value',
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'persons' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a numeric value',
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'months' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a numeric value',
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'start_date' => array(
			'date' => array(
				'rule' => array('date'),
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'license_fee' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a numeric value',
			),
		),	
		'desks' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a numeric value',
			),
		),
		'security' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a numeric value',
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'building_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'A building must be specified',
			),
		),

		// not float values
		'office_amount' => [
			'naturalNumber' => [
				'rule' => ['naturalNumber',true],
				'allowEmpty' => true,
				'message' => 'Please enter a whole number, not a decimal'
			]
		],
		'desk_amount' => [
			'naturalNumber' => [
				'rule' => ['naturalNumber',true],
				'allowEmpty' => true,
				'message' => 'Please enter a whole number, not a decimal'
			]
		],
		'phone_amount' => [
			'naturalNumber' => [
				'rule' => ['naturalNumber',true],
				'allowEmpty' => false,
				'message' => 'Please enter a whole number, not a decimal'
			],
		],
		'internet_amount' => [
			'naturalNumber' => [
				'rule' => ['naturalNumber',true],
				'allowEmpty' => false,
				'message' => 'Please enter a whole number, not a decimal'
			],
		],
		'credit' => [
			'naturalNumber' => [
				'rule' => ['naturalNumber',true],
				'allowEmpty' => false,
				'message' => 'Please enter a whole number, not a decimal'
			],
		],
	);

	public function beforeValidate($options = [])
	{
		// 
		$this->log($this->data,'debug');
		return true;
	}

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Prospect' => array(
			'className' => 'Prospect',
			'foreignKey' => 'prospective_customer_id',
		),
		'Desk' => array(
			'className' => 'Desk',
			'foreignKey' => 'desk_id',
		),
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
		)
	);

/**
 * hasMany associations
 * 
 * @return void
 */
	public $hasMany = array(
		'Amendment' => array(
			'className' => 'LicenseAmendment',
			'foreignKey' => 'license_agreement_id',
		)
	);

/**
 * Update the customer sales stage when a license agreement is added
 */
	public function afterSave($created, $options = array()) {
		if ($created) {
			$this->Prospect->id = $prospect_id = @$this->data[$this->alias]['prospective_customer_id'];
			if (!empty($prospect_id) && $this->Prospect->exists()) {
				// TODO test that prospective_customer_tbl.sales_stage and sales_stage_id are updated
				$fieldList = array('id','sales_stage_id','sales_stage');
				$saved = $this->Prospect->save(array('Prospect' => array(
					'id' => $prospect_id,
					'sales_stage_id' => 6,
					'sales_stage' => '6-sent agreement'
				)), compact('fieldList'));
			}
		}
	}

	
/**
 * Docusign - Get login information
 * v2/login_information
 * @author John Jung
 * @param [type] [varname] [description]
 *
 * 
 */

	public function login_information() {

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		$client = new Client('https://na2.docusign.net/restapi/v2/login_information');

		$request = $client->get('https://na2.docusign.net/restapi/v2/login_information', array(
			'X-Docusign-Authentication' => $login_array
		));

		// sends the get request
		$response = $request->send();

		// To get additional settings set params to true and all
		// https://demo.docusign.net/restapi/v2/login_information?api_password=true&include_account_id_guid=true&login_settings=all

		if ($response->getStatusCode() == 200) {

			return $response->json();

		} else {

			$response = $response->json();
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$docusign_response['errorCode']." : ".$docusign_response['message']);			
		}

		// RESPONSE ARRAYS
		// 
		// SUCCESS
		// 
		// {
		//   "loginAccounts": [
		//     {
		//       "name": "Pith Studio",
		//       "accountId": "373328",
		//       "baseUrl": "https://demo.docusign.net/restapi/v2/accounts/373328",
		//       "isDefault": "true",
		//       "userName": "John Jung",
		//       "userId": "9590cc51-d9f2-436d-bee4-6fcf6cb8db55",
		//       "email": "jj@pith-studio.com",
		//       "siteDescription": ""
		//     }
		//   ]
		// }
		// 
		// ERROR
		// {
		//	  "errorCode": "PARTNER_AUTHENTICATION_FAILED",
		//	  "message": "The specified Integrator Key was not found or is disabled. An Integrator key was not specified."
		//	}

	}

/**
 * Docusign - Send a document for signature
 * v2/accounts/{accountId}/envelopes
 * @author John Jung
 * @param array $licenseAgreement $var['LicenseAgreement']
 *
 * 
 */

	public function create_docusign_envelope($licenseAgreement = array()) {

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$baseUrl = $accountID['loginAccounts'][0]['baseUrl'];
		$accountID = $accountID['loginAccounts'][0]['accountId'];

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		/////////////////////////////////////////////////////////////////////////////////////////////////
		// STEP 2 - Create an envelope with one recipient, one tab, and one document and send
		/////////////////////////////////////////////////////////////////////////////////////////////////

		$data = array (
            "emailSubject" => "MicroOffice License Agreement - Signature Request on Document",
            "documents" => array( 
            	array( 
            		"documentId" => "1",
            		"name" => $licenseAgreement['file'][0]['name']
            	)
            ),
            "recipients" => array( "signers" => array(
                array(  
                	"email" => AuthComponent::user('username'),
                    "name" => 'MicroOffice LLC',
                    "recipientId" => "2",
                    "routingOrder" => "2"
                ),
                array(  
                	"email" => $licenseAgreement['email'],
                    "name" => $licenseAgreement['first_name']." ".$licenseAgreement['last_name'],
                    "recipientId" => "1",
                    "routingOrder" => "1"
                )
            ),
		    "status" => "created"
		));
		// debug($data);
		$data_string = json_encode($data);  
		
		$file_contents = file_get_contents($licenseAgreement['file'][0]['tmp_name']);
		
		$requestBody = "\r\n"
		."\r\n"
		."--myboundary\r\n"
		."Content-Type: application/json\r\n"
		."Content-Disposition: form-data\r\n"
		."\r\n"
		."$data_string\r\n"
		."--myboundary\r\n"
		."Content-Type:".$licenseAgreement['file'][0]['type']."\r\n"
		."Content-Disposition: file; filename=".$licenseAgreement['file'][0]['name']."; documentid=1 \r\n"
		."\r\n"
		."$file_contents\r\n"
		."--myboundary--\r\n"
		."\r\n";

		$requestHeader = array(
			'Content-Type' => 'multipart/form-data;boundary=myboundary',
			'Content-Length' => strlen($requestBody),
			'X-DocuSign-Authentication' => $login_array
		);
		// debug($requestHeader);

		$client = new Client($baseUrl);

		$request = $client->post($baseUrl."/envelopes" , 
			$requestHeader,
			$requestBody
		);

		// sends the get request
		$response = $request->send();

		// To get additional settings set params to true and all
		// https://demo.docusign.net/restapi/v2/login_information?api_password=true&include_account_id_guid=true&login_settings=all

		if ($response->getStatusCode() == 201) {

			// Successful Response
			// array(
			// 	'envelopeId' => '8e70f991-1e84-40e6-a0a5-e2edd30d6727',
			// 	'uri' => '/envelopes/8e70f991-1e84-40e6-a0a5-e2edd30d6727',
			// 	'statusDateTime' => '2013-11-15T02:47:25.8249140Z',
			// 	'status' => 'sent'
			// )

			// debug($response->getBody('true'));

			return $response->json();

		} else {

			$response = $response->json();
			// debug($response->json());
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$response['errorCode']." : ".$response['message']);			
		}

	}


	
/**
 * Docusign - Get Docusign Envelope
 * v2/update_password
 * @author John Jung
 * @param array envelopeInfo accountId, envelopeId
 *
 * 
 */

	public function get_docusign_envelope($envelopeID) {

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$accountID = $accountID['loginAccounts'][0]['accountId'];

		$params['accountId'] = $accountID;
		$params['envelopeId'] = $envelopeID;

		// $params['accountId'] = '373328';
		// $params['envelopeId'] = '0fc89029-a203-4ad2-8c8d-6fb6bad7b07d';

		$client = new Client('https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$params['envelopeId']);

		$request = $client->get('https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$params['envelopeId'], array(
			'X-Docusign-Authentication' => $login_array
		));

		// sends the get request
		$response = $request->send();

		

		// To get additional settings set params to true and all
		// https://demo.docusign.net/restapi/v2/login_information?api_password=true&include_account_id_guid=true&login_settings=all

		if ($response->getStatusCode() == 200) {

			// debug($response->json());
			return $response->getBody();

		} else {

			debug($response->json());
			$response = $response->json();
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$docusign_response['errorCode']." : ".$docusign_response['message']);

		}


	}

	
/**
 * Docusign - Get Docusign Envelope Recipients
 * v2/update_password
 * @author John Jung
 * @param array envelopeInfo accountId, envelopeId
 *
 * 
 */

	public function get_docusign_envelope_recipients($envelopeID) {

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$accountID = $accountID['loginAccounts'][0]['accountId'];

		// $params['accountId'] = '373328';
		// $params['envelopeID'] = '0fc89029-a203-4ad2-8c8d-6fb6bad7b07d';

		$client = new Client('https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeID."/recipients");

		$request = $client->get(
			'https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeID."/recipients", 
			array(
				'X-Docusign-Authentication' => $login_array,
				'query' => array('include_tabs' => true)
			)
		);

		// sends the get request
		$response = $request->send();

		if ($response->getStatusCode() == 200) {

			// debug($response->json());
			return $response->getBody();

		} else {

			debug($response->json());
			$response = $response->json();
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$docusign_response['errorCode']." : ".$docusign_response['message']);

		}


	}


/**
 * Docusign - Get Docusign Envelope Recipient Tabs
 * v2/update_password
 * @author John Jung
 * @param array envelopeInfo accountId, envelopeId
 *
 * 
 */

	public function get_docusign_envelope_recipient_tabs($envelopeInfo = array()) {

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$accountID = $accountID['loginAccounts'][0]['accountId'];

		$params['accountId'] = $envelopeInfo['accountId'];
		$params['envelopeId'] = $envelopeInfo['envelopeId'];
		$params['recipientId'] = $envelopeInfo['recipientId'];

		// $params['accountId'] = '373328';
		// $params['envelopeId'] = '0fc89029-a203-4ad2-8c8d-6fb6bad7b07d';
		// $params['recipientId'] = '366cfc39-e104-4b34-926f-ce25e80f9f7d';

		$client = new Client('https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$params['envelopeId']."/recipients/".$params['recipientId']."/tabs");

		$request = $client->get(
			'https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$params['envelopeId']."/recipients/".$params['recipientId']."/tabs", 
			array(
				'X-Docusign-Authentication' => $login_array
			)
		);

		// sends the get request
		$response = $request->send();

		if ($response->getStatusCode() == 200) {

			// debug($response->json());
			return $response->json();

		} else {

			debug($response->json());
			$response = $response->json();
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$docusign_response['errorCode']." : ".$docusign_response['message']);

		}


	}


/**
 * Docusign - Get Docusign Envelope Assets
 * v2/update_password
 * @author John Jung
 * @param array envelopeInfo accountId, envelopeId
 * @url v2/accounts/:accountId/envelopes/:envelopeId/:envelopeAssets
 * 
 */

	public function get_docusign_envelope_assets($envelopeId, $asset) {

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$accountID = $accountID['loginAccounts'][0]['accountId'];	

		// $envelopeId = '0fc89029-a203-4ad2-8c8d-6fb6bad7b07d';
		// $asset = 'audit_events';
		// $asset = 'documents';
		// $asset = 'custom_fields';
		// $asset = 'notification';

		$client = new Client('https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeId."/".$asset);

		$request = $client->get(
			'https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeId."/".$asset, 
			array(
				'X-Docusign-Authentication' => $login_array
			)
		);

		// sends the get request
		$response = $request->send();

		if ($response->getStatusCode() == 200) {

			// debug($response->json());
			return $response->getBody();

		} else {

			debug($response->json());
			$response = $response->json();
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$docusign_response['errorCode']." : ".$docusign_response['message']);

		}


	}


/**
 * Docusign - Get Docusign Envelope Document
 * v2/update_password
 * @author John Jung
 * @param array envelopeInfo accountId, envelopeId
 * @url v2/accounts/:accountId/envelopes/:envelopeId/:envelopeAssets
 * 
 */

	public function get_docusign_envelope_document($envelopeId, $documentId) {

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$accountID = $accountID['loginAccounts'][0]['accountId'];

		// $params['accountId'] = $envelopeInfo['accountId'];
		// $params['envelopeId'] = $envelopeInfo['envelopeId'];
		// $params['documentId'] = $envelopeInfo['documentId'];
		
		$client = new Client('https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeId."/documents/".$documentId);

		$request = $client->get(
			'https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeId."/documents/".$documentId, 
			array(
				'X-Docusign-Authentication' => $login_array,
				'query' => array(
					'show_changes' => true
				)
			)
		);

		// sends the get request
		$response = $request->send();

		if ($response->isSuccessful()) {

			return $response->getBody();

		} else {

			// debug($response->json());
			$response = $response->json();
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$docusign_response['errorCode']." : ".$docusign_response['message']);

		}


	}


/**
 * Docusign - Send a document for signature
 * v2/accounts/{accountId}/envelopes
 * @author John Jung
 * @param array $licenseAgreement $var['LicenseAgreement']
 *
 * 
 */

	public function docusign_sender_view($envelope_id, $license_agreement_id) {

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$accountID = $accountID['loginAccounts'][0]['accountId'];
		$baseUrl = DOCUSIGN_BASEURL."/accounts/".$accountID."/envelopes/$envelope_id/views/sender";

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		$client = new Client($baseUrl);

		$id = $license_agreement_id;

		$requestBody = array(
			'returnUrl' => DOCUSIGN_RETURNURL."/licenseAgreements/view/$id"
		);
		$requestBody = json_encode($requestBody);

		$requestHeader = array(
			'Content-Length' => strlen($requestBody),
			'X-DocuSign-Authentication' => $login_array
		);
		// debug($requestHeader);

		$request = $client->post($baseUrl, 
			$requestHeader,
			$requestBody
		);

		// sends the get request
		$response = $request->send();

		// To get additional settings set params to true and all
		// https://demo.docusign.net/restapi/v2/login_information?api_password=true&include_account_id_guid=true&login_settings=all

		if ($response->isSuccessful() == true) {

			// Successful Response
			// array(
			// 	'envelopeId' => '8e70f991-1e84-40e6-a0a5-e2edd30d6727',
			// 	'uri' => '/envelopes/8e70f991-1e84-40e6-a0a5-e2edd30d6727',
			// 	'statusDateTime' => '2013-11-15T02:47:25.8249140Z',
			// 	'status' => 'sent'
			// )

			// debug($response->json());
			return $response->json();

		} else {

			$statusCode = $response->getStatusCode();
			$reasonPhrase = $response->getReasonPhrase();

			// Throw Docusign Error
			throw new Exception("Docusign Error ".$statusCode." : ".$reasonPhrase);			
		}

	}


/**
 * Docusign - Send a document for signature
 * v2/accounts/{accountId}/envelopes
 * @author John Jung
 * @param array $licenseAgreement $var['LicenseAgreement']
 *
 * 
 */

	public function docusign_correct_view($envelope_id, $license_agreement_id) {

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$baseUrl = "https://na2.docusign.net/restapi/v2/accounts/69d22ebb-d51d-49d2-af97-b95130243b55/envelopes/$envelope_id/views/correct";
		$accountID = $accountID['loginAccounts'][0]['accountId'];

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		$client = new Client($baseUrl);

		$id = $license_agreement_id;

		$requestBody = array(
			'returnUrl' => DOCUSIGN_RETURNURL."/licenseAgreements/view/$id"
		);
		$requestBody = json_encode($requestBody);

		$requestHeader = array(
			'Content-Length' => strlen($requestBody),
			'X-DocuSign-Authentication' => $login_array
		);
		// debug($requestHeader);

		$request = $client->post($baseUrl, 
			$requestHeader,
			$requestBody
		);

		// sends the get request
		$response = $request->send();

		// To get additional settings set params to true and all
		// https://demo.docusign.net/restapi/v2/login_information?api_password=true&include_account_id_guid=true&login_settings=all

		if ($response->isSuccessful() == true) {

			// Successful Response
			// array(
			// 	'envelopeId' => '8e70f991-1e84-40e6-a0a5-e2edd30d6727',
			// 	'uri' => '/envelopes/8e70f991-1e84-40e6-a0a5-e2edd30d6727',
			// 	'statusDateTime' => '2013-11-15T02:47:25.8249140Z',
			// 	'status' => 'sent'
			// )

			return $response->json();

		} else {

			$response = $response->json();
			debug($response->json());
			// Throw Docusign Error
			throw new Exception("Docusign Error ".$response['errorCode']." : ".$response['message']);			
		}

	}


/**
 * Docusign - Get Docusign Envelope Document
 * v2/update_password
 * @author John Jung
 * @param array envelopeInfo accountId, envelopeId
 * @url v2/accounts/:accountId/envelopes/:envelopeId/:envelopeAssets
 * 
 */

	public function get_docusign_allinone_document($envelopeId) {

		$login_array = array(
			'Username' => DOCUSIGN_USERNAME,
			'Password' => DOCUSIGN_PASSWORD,
			'IntegratorKey' => DOCUSIGN_INTEGRATORKEY
		);
		$login_array = json_encode($login_array);

		// Get Docusign Account ID
		$accountID = $this->login_information();
		$accountID = $accountID['loginAccounts'][0]['accountId'];

		// $params['accountId'] = $envelopeInfo['accountId'];
		// $params['envelopeId'] = $envelopeInfo['envelopeId'];
		// $params['documentId'] = $envelopeInfo['documentId'];
		
		try {
			$client = new Client('https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeId."/documents/combined");

			$request = $client->get(
				'https://na2.docusign.net/restapi/v2/accounts/'.$accountID.'/envelopes/'.$envelopeId."/documents/combined", 
				array(
					'X-Docusign-Authentication' => $login_array,
					'query' => array(
						'show_changes' => true,
						'watermark' => true,
						'show_changes' => true,
					)
				)
			);
			// sends the get request
			$response = $request->send();
			return $response->getBody();
		}
		catch (Guzzle\Http\Exception\ClientErrorResponseException $e) {

			$req = $e->getRequest();
            $resp =$e->getResponse();

            debug($req);
            debug($resp);
		}
		

	}

}

<?php
App::uses('AppModel', 'Model');
/**
 * LicenseAgreementTemplate Model
 *
 * @property Building $Building
 * @property Modifier $Modifier
 */
class LicenseAgreementTemplate extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'enum_agreement_template_tbl';

/**
 * Use sequence
 *
 * @var mixed False or sequence name
 */
	public $sequence = 'public.enum_agreement_template_seq';


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Building' => array(
			'className' => 'Building',
			'foreignKey' => 'building_id',
		),
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'modifier_id',
		)
	);

/**
 * Data validation rules
 * 
 */
	public $validate = array(
		'building_id' => array('notEmpty','numeric'),
		'name' => array('notEmpty','isUnique'),
		'template' => array('notEmpty'),
	);
}

<?php
App::uses('AppModel', 'Model');
/**
 * LicenseAmendmentTemplate Model
 *
 * @property Modifier $Modifier
 */
class LicenseAmendmentTemplate extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'enum_amendment_template_tbl';

	/**
 * Use sequence
 *
 * @var mixed False or sequence name
 */
	public $sequence = 'public.amendment_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'modifier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

<?php
App::uses('AppModel', 'Model');
/**
 * MailPackage Model
 *
 * @property Customer $Customer
 */
class MailPackage extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mail_package_tbl';


/**
 * Use sequence
 * 
 * @var array
 */
	public $sequence = 'public.mail_package_seq';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'customer_id',
		),
		'Sender' => array(
			'className' => 'Employee',
			'foreignKey' => 'sender_id'
		)
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Date time seems to be improperly formatted',
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'customer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a valid customer id',
				'allowEmpty' => false,
				'required' => true,
			),
		),
		'email_message' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter an email message',
				'allowEmpty' => true,
				'required' => false,
			),
		),
		'carrier' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
		'package_type' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
		'sender' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
	);


/**
 * Overload constructor
 */
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->loadFields(array('package_types','carriers'));
	}

/**
 * 
 * 
 */
	public function afterSave($created, $options = array()) {

		if ($created) {
			$emails = $this->data[$this->alias]['recipient_email'];

			// strip spaces
			$emails = preg_replace('/\s+/', '', $emails);

			// delimit by commas
			$emails = explode(',',$emails);

			// reformat for mandrill message
			$to = array();
			foreach ($emails as $email) {
				$to[]['email'] = $email;
			}

			$cust_acct_name = $this->CustomerAccount->field('name',array(
				'id' => $this->data[$this->alias]['customer_id']
			));
			$timestamp = date('g:ia', strtotime($this->data[$this->alias]['date']));
			$carrier_name = $this->data[$this->alias]['carrier'];
			$package_type = $this->data[$this->alias]['package_type'];
			$sender = $this->data[$this->alias]['sender'];

			$text =
				"Dear {$cust_acct_name},

				Today at {$timestamp} we have received a package for your company.
				The package was delivered by {$carrier_name} in the form of a {$package_type} and it was signed for by {$sender}.
				Please make sure the package is retrieved before the end of the day at 5pm.
				After the end of the business day MicroOffice will hold your package for you, however we are not responsible for the long term safe keeping of this item.
				Should you have any questions, please contact the office manager at your location.

				We appreciate your business and have a pleasant day.

				MicroOffice Management
			";


			// build message
			$message = array(
				'subject' => 'A Package Has Arrived For You',
				'text' => $text,
				'to' => $to,
			);

			// send the email
			if (REALLY_SEND_EMAIL) {
				$this->sendEmail($message);
			}
		}
	}
}

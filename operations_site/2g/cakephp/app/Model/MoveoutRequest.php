<?php
App::uses('AppModel', 'Model');
/**
 * MoveoutRequest Model
 *
 * @property CustAcct $CustAcct
 */
class MoveoutRequest extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'moveout_request_tbl';

/**
 * Sequence used by this model
 * 
 * @var string
 */
	public $sequence = 'public.moveout_request_seq';

/**
 * Each of these flags have a _dt component and a _employee_id component
 * 
 * @var array
 */
	public $flags = array(
		'all_keycards_returned'  => 'All Keycards Returned',
		'all_metalkeys_returned' => 'All Metal Keys Returned',
		'coi_submitted'          => 'COI Submitted',
		'coi_approved'           => 'COI Approved',
		'moved_out'              => 'Move Out',
		'security_deposit_sent'  => 'Security Deposit Sent'
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'cust_acct_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'A customer account id required'
			)
		),

		/* USER INPUT VALUES */
		'move_out_date' => array(
			'date' => array(
				'rule' => array('date','ymd'),
			),
			'compareTime' => array(
				'rule' => array('compareTime','gt', 'Y-m-d', 'now'), // time in the future
				'message' => 'You have selected an invalid move-out date',
				'on' => 'create'
			)
		),
		'close_out_date' => array(
			'date' => array(
				'rule' => array('date'),
			),
		),
		'num_keycards_returned' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber', true), // allow zero
			),
		),
		'num_metal_keys_returned' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber', true), // allow zero
			),
		),
		'is_using_moving_company' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
		),
		'move_out_requester_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),


		/* EMPLOYEE INPUT VALUES */
		'all_keycards_returned_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'allowEmpty' => true
			),
		),
		'all_keycards_returned_employee_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'all_metalkeys_returned_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'allowEmpty' => true
			),
		),
		'all_metalkeys_returned_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'coi_submitted_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'allowEmpty' => true
			),
		),
		'coi_submitted_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'coi_approved_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'allowEmpty' => true
			),
		),
		'coi_approved_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'moved_out_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'allowEmpty' => true
			),
		),
		'moved_out_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'security_deposit_sent_dt' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'allowEmpty' => true
			),
		),
		'security_deposit_sent_employee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),

	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		),
		'Requester' => array(
			'className' => 'Customer',
			'foreignKey' => 'move_out_requester_id'
		)
	);

/**
 * Assign the cust_acct_id and cust_pers_id
 * 
 * @param array $options save options
 * @return boolean true if the save operation should continue
 */ 
	public function beforeSave($options = array())
	{
		// call the parent method
		parent::beforeSave($options);

		// assign customer account id
		$this->data['MoveoutRequest']['cust_acct_id'] = AuthComponent::user('cust_acct_id');

		// assign customer person id
		$this->data['MoveoutRequest']['move_out_requester_id'] = AuthComponent::user('cust_pers_id');

		// TODO determine the account-close-out date from the move-out date

		return true;
	}

	/**
	 * Activate a flag indicating one of the steps of the 
	 * 
	 * @param string $flag_val the name of the flag without the suffix, e.g. coi_approved
	 * @return void
	 */
	public function updateFlag($flag_val) {

		// safety check
		if (!in_array($flag_val, array_keys($this->flags)))
		{
			throw new InvalidArgumentException('Invalid flag name specified');
		}

		// get the employee id
		$employee_id = AuthComponent::user('id');

		// set dt to now
		$dt = date(DATETIME_FORMAT);

		// save the model
		$saved = $this->save(
			array($this->alias => array(
				'id' => $this->id,
				"{$flag_val}_employee_id" => $employee_id,
				"{$flag_val}_dt" => $dt
			)),
			array('fieldList' => array(
				'id',"{$flag_val}_employee_id", "{$flag_val}_dt"
			))
		);

		if ($saved && $flag_val == 'coi_approved')
		{
			$this->emailIfCoiApproved($this->id);
		}

		return $saved;
	}

	/**
	 * Reset the datetime, and update the employee_id of the flag
	 * 
	 * @param string $flag_val the name of the flag without the suffix, e.g. all_keycards_returned
	 */
	public function resetFlag($flag_val) {

		// safety check
		if (!in_array($flag_val, array_keys($this->flags)))
		{
			throw new InvalidArgumentException('Invalid flag name specified');
		}

		// get the employee id
		$employee_id = AuthComponent::user('id');

		// set dt to null
		$dt = null;

		// save the model
		return $this->save(
			array($this->alias => array(
				'id' => $this->id,
				"{$flag_val}_employee_id" => $employee_id,
				"{$flag_val}_dt" => $dt
			)),
			array('fieldList' => array(
				'id',"{$flag_val}_employee_id", "{$flag_val}_dt"
			))
		);
	}

	private function fetch($id = null)
	{
		// set the model id
		$this->id = $id = $id ?: $this->id;

		// fetch request
		$request = $this->find('first', array(
			'conditions' => ['MoveoutRequest.id' => $id],
			'contain' => array(
				'Requester' => ['id','full_name','email'],
				'CustomerAccount' => array(
					'fields' => ['id','name','primary_floor_id'],
					'PrimaryCustomer' => ['id','full_name','email'],
					'Floor'  => array(
						'fields' => ['id','short_global_label','label','office_manager_id'],
						'Building',
						'OfficeManager' => array(
							'fields' => ['id','full_name','email']
						),
					)
				)
			)
		));

		return array(
			'request'          => $request['MoveoutRequest'],
			'requester'        => $request['Requester'],
			'customerAccount'  => $request['CustomerAccount'],
			'primaryCustomer'  => $request['CustomerAccount']['PrimaryCustomer'],
			'floor'            => $request['CustomerAccount']['Floor'],
			'officeManager'    => $request['CustomerAccount']['Floor']['OfficeManager'],
			'building'         => $request['CustomerAccount']['Floor']['Building']
		);
	}

	public function emailIfCoiApproved($id)
	{
		extract($this->fetch($id));

		$email_body = 
			"Your Certificate of Insurance has been approved and your Move Out Date is listed below.\n
			Please familiarize yourself with the Package Removal policy. Please also note that this policy varies by building,
			and that building fines issued for violations will be passed to your account. For details, please visit 
			www.MicroOffice.com/help, and click on “6. Package Removals and Move Out Policy.”\n\n\n
			Notification: Member Move Out Requested\n
			Account Name: {$customerAccount['name']}\n
			Name of Person Requesting: {$requester['full_name']}\n
			Date of Request: {$request['created']}\n
			Location: {$building['address_1']}, {$building['city']}, {$building['state']} - {$floor['label']}\n
			Requested Move-Out Date: {$request['move_out_date']}\n\n

			Cordially,\n
			The MicroOffice Team";

		// primaryCustomer
		$sent = $this->sendEmail(array(
			'to' => array(
				['name' => $primaryCustomer['full_name'], 'email' => $primaryCustomer['email']]
			),
			'subject' => 'Move-Out Request Confirmation',
			'text' => "{$primaryCustomer['full_name']},\n\n$email_body"
		));
		
		if ($primaryCustomer['id'] != $requester['id'])
		{
			// requester
			$sent += $this->sendEmail(array(
				'to' => array(
					['name' => $requester['full_name'], 'email' => $requester['email']]
				),
				'subject' => 'Move-Out Request Confirmation',
				'text' =>
					"{$requester['full_name']},\n\n$email_body"
			));
		}

		return $sent;
	}
}

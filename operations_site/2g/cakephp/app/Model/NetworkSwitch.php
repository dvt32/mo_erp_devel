<?php
App::uses('AppModel', 'Model');
/**
 * NetworkSwitch Model
 *
 * @property Floor $Floor
 * @property SwitchOctetCountTbl $SwitchOctetCountTbl
 */
class NetworkSwitch extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'network_switch_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		// 'SwitchOctetCount' => array(
		// 	'className' => 'SwitchOctetCount',
		// 	'foreignKey' => 'network_switch_id',
		// 	'dependent' => false,
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'exclusive' => '',
		// 	'finderQuery' => '',
		// 	'counterQuery' => ''
		// )
	);

}

<?php
App::uses('AppModel', 'Model');

/**
 * Notification Model
 *
 * @property Room $Room
 * @property Floor $Floor
 * @property Building $Building
 */
class Notification extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'notification_tbl';

	public $sequence = 'public.notification_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id'
		)
	);

/**
 * Validation rules
 * 
 * @var array
 */
	public $validate = array(

		// notification title/header
		'name' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'A notification title is required'
			)
		),

		// notification description
		'description' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'A notification description is required'
			)
		),

		// notification expiration
		'expires' => array(
			'datetime' => array(
				'rule' => array('datetime','ymd'),
				'required' => false,
				'allowEmpty' => true,
				'message' => 'Invalid datetime format'
			),
			// 'future' => array(
			// 	'rule' => array('datetimeInFuture', false), // TODO implement datetimeInFuture custom validation rule
			// 	'message' => 'Expiration date-time must be in the future'
			// )
		),

		// employee id (author of this notification)
		'employee_id' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'required' => true,
				'message' => 'Employee Id is required'
			)
		),
	);


/**
 * Converts list of emails to proper format before sending/scheduling emails
 * 
 * @return boolean true if the save should continue
 */
	public function beforeSave($options = array()) {

		$notification = $this->data[$this->alias];

		if (!empty($notification['floors'])) {

			if ($notification['is_emailed'] == true) {
				// build message
				$message = array(
					'subject' => $notification['name'],
					'text' => $notification['description'],
					'to' => $this->buildEmailList($notification['floors'])
				);

				// send email immediately
				if (REALLY_SEND_EMAIL) {
					$this->sendEmail($message);
				}
			}

			$floors = implode(',',$notification['floors']);
			$floors = '{' . $floors . '}';

			$this->data[$this->alias]['floors'] = $floors;
		}
		return true;
	}

/**
 * Build list of emails in Mandrill format for customers on specified floor(s)
 * 
 * @param array $floors floor ids
 * @return array $emails array of emails and names in Mandrill format
 */
	public function buildEmailList($floors) {

		// add association to customer on the fly
		$this->bindModel(array('hasMany' => array('Customer')));
		$customers = $this->Customer->find('all', array(
			'conditions' => array(
				'CustomerAccount.is_active' => true,
				'Customer.is_active' => true,
				'CustomerAccount.primary_floor_id' => $floors
			),
			'fields' => array('full_name','email'),
			'contain' => array('CustomerAccount')
		));

		// reindex query result to mandrill message format
		foreach ($customers as &$customer) {
			if (!empty($customer['Customer']['email'])) {
				$customer = array(
					'email' => $customer['Customer']['email'],
					'name' => $customer['Customer']['full_name']
				);
			}
		}
		array_walk($customers, function(&$customer) {});
		// debug($customers); die;

		return $customers;
	}

	public function afterFind($results = array(), $primary = false) {
		if (!empty($results)) {
			foreach ($results as &$result) {

				$floors = @$result['Notification']['floors'];

				// trim away curly braces
				$floors = trim($floors, '{}');

				if (!empty($floors)) {
					$result['Notification']['floors'] = explode(',',$floors);
				}
			}
		}
		return $results;
	}
}

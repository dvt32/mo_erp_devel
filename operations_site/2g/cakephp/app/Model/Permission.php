<?php
App::uses('AppModel', 'Model');
/**
 * Permission Model
 *
 * @property Aco $Aco
 */
class Permission extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'permission_tbl';

/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.permission_seq';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Aco' => array(
			'className' => 'Aco',
			'foreignKey' => 'aco_id',
		)
	);

/**
 * Validation rules
 * 
 * @var array
 */
	public $validate = array(
		'role_name' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty'
			),
		),
		'aco_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty'
			)
		)
	);

	public function beforeSave($options = array())
	{
		// role name + aco id must be unique
		$role_name = @$this->data[$this->alias]['role_name'];
		$aco_id = @$this->data[$this->alias]['aco_id'];

		return 0 == $this->find('count', ['conditions' => compact('role_name','aco_id')]);
	}
}

<?php
App::uses('AppModel', 'Model');
/**
 * PhoneNumber Model
 *
 * @property Account $Account
 * @property Person $Person
 * @property Floor $Floor
 * @property Iad $Iad
 * @property IpPhone $IpPhone
 * @property BlockOutboundCaller $BlockOutboundCaller
 * @property SipRegisterUser $SipRegisterUser
 * @property InboundTrunk $InboundTrunk
 * @property OutboundTrunk $OutboundTrunk
 */
class PhoneNumber extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'telecom';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'phone_number_tbl';


	public $validate = array(

		'message' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a message'
			),
		),
		'phone_number' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a phone number'
			),
			'phone' => array(
		        'rule' => array('phone', null, 'us')
		    )
		),
		'from_email' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a phone number'
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please enter a valid email address'
			)
		),
		'carrier' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Please enter a valid email address'
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please enter a valid email address'
			)
		),
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed


// /**
//  * hasMany associations
//  *
//  * @var array
//  */
// 	public $hasMany = array(
// 		'Voicemail' => array(
// 			'className' => 'Voicemail',
// 			'foreignKey' => 'username',
// 			'finderQuery' => 'SELECT Voicemail.* FROM voicemail'
// 		)
// 	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
		),
		'Iad' => array(
			'className' => 'Iad',
			'foreignKey' => 'iad_id',
		),
		'IpPhone' => array(
			'className' => 'IpPhone',
			'foreignKey' => 'ip_phone_id',
		),
		'InboundTrunk' => array(
			'className' => 'InboundTrunk',
			'foreignKey' => 'inbound_trunk_id',
		),
		'OutboundTrunk' => array(
			'className' => 'OutboundTrunk',
			'foreignKey' => 'outbound_trunk_id',
		)
	);

	
	public function beforeSave($options = array()) {
		// set ms access
		parent::beforeSave();

		return true;
	}
}

<?php
App::uses('AppModel', 'Model');
/**
 * Promise Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Customer $Customer
 * @property Employee $Employee
 * @property Modifier $Modifier
 */
class Promise extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'promise_tbl';

	public $sequence = 'public.promise_seq';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'cust_pers_id',
		),
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
		),
		'Modifier' => array(
			'className' => 'Employee',
			'foreignKey' => 'modifier_id',
		)
	);

/**
 * validation rules
 * @var array
 */
	public $validate = array(

		// customer account id
		'cust_acct_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'allowEmpty' => false,
				'message' => 'Customer account id is required'
			),
		),

		// expiration date
		'expiration_date' => array(
			'date' => array(
				'rule' => 'date',
				'allowEmpty' => false,
				'message' => 'Expiration date is required'
			)
		),

		'status' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'allowEmpty' => false,
				'message' => 'Status is required'
			),
			'inList' => array(
				'rule' => array('inList', ['proposed','approved','rejected','past_due','fulfilled']),
				'message' => 'Invalid status, use "proposed", "approved", "rejected", "past_due", or "fulfilled"'
			)
		),

	);

	/**
	 * Approve a promise, i.e. after proposed by customer
	 * @param int $id promise id
	 * @return boolean true if the promise and associated billing amounts were updated
	 */
	public function approve($id = null)
	{
		$this->id = $id = $id ?: $this->id;
		$cust_acct_id = $this->field('cust_acct_id');

		// approve the promise
		if ($this->saveField('status', 'approved'))
		{
			// put all aging billing amounts for a given customer account on hold
			$billing_amounts_updated = $this->CustomerAccount->BillingAmount->updateAll(
				array('stage' => '\'aging_hold\''),
				array(
					'cust_acct_id' => $cust_acct_id,
					'stage' => ['aging_1','aging_2','aging_3','aging_4']
				)
			);

			// update the customer account billing-related caches
			$this->CustomerAccount->updateBillingInformation($cust_acct_id);

			// email after Promise is approved
			$account = $this->find('first', array(
				'conditions' => array(
					'Promise.id' => $id
				),
				'contain' => array('CustomerAccount', 'Customer')
			));
			$subject = 'MicroOffice Promise Approved';
			$text = 'Your Promise has been Approved! You agree to pay $'.$account['Promise']['amount'].', by '.$account['Promise']['expiration_date'];
			$html = '<p>Your Promise has been Approved! You agree to pay $'.$account['Promise']['amount'].', by '.$account['Promise']['expiration_date'].'</p>';
			$this->financeEmail($account, $subject, $text, $html);

			// return
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Reject a proposed promise
	 * @param int $id promise id
	 * @return boolean true if the promise and associated billing amounts were updated
	 */
	public function reject($id = null)
	{
		$this->id = $id = $id ?: $this->id;
		$cust_acct_id = $this->field('cust_acct_id');

		$promise_updated = $this->saveField('status', 'rejected');

		if ($promise_updated)
		{
			$account = $this->find('first', array(
				'conditions' => array(
					'Promise.id' => $id
				),
				'contain' => array('CustomerAccount', 'Customer')
			));
			$subject = 'MicroOffice Promise Not Approved';
			$text = 'Your Promise has NOT been Approved! Please pay your full balance for uninterrupted service and avoid legal action.';
			$html = '<p>Your Promise has NOT been Approved! Please pay your full balance for uninterrupted service and avoid legal action.</p>';

			$this->financeEmail($account, $subject, $text, $html);
		}

		return !empty($promise_updated);
	}

	/**
	 * After Save Callback for Promises
	 *
	 *
	 * 
	 */
	public function afterSave($created, $options = array()) {

		// only email if it's a new promise
		if ($created === true) {

			$id = $this->id;

			// email the customer that the promise has been noted and pending review
			$account = $this->find('first', array(
				'conditions' => array(
					'Promise.id' => $id
				),
				'contain' => array('CustomerAccount', 'Customer')
			));
			$subject = 'MicroOffice Promise Pending Approval';
			$text = 'Your Promise has been Created and Pending Approval! This does not mean your promise is granted, and your account is still considered aging. Please pay your full balance for uninterrupted service and avoid legal action, unless you receive an email of Promise Approval.';
			$html = '<p>Your Promise has been Created and Pending Approval! This does not mean your promise is granted, and your account is still considered aging. Please pay your full balance for uninterrupted service and avoid legal action, unless you receive an email of Promise Approval.</p>';

			$this->financeEmail($account, $subject, $text, $html);	

		}
	}

	/**
	 * Reports whether a promise is/was fulfilled by looking at the payments made 
	 * between its creation_dt and its expiration_date
	 * 
	 * @param int $id promise id
	 * @return boolean true if the promise was fulfilled
	 */
	public function isFulfilled($id = null)
	{
		// fetch promise
		$promise = $this->find('first', array(
			'conditions' => array('id' => $id),
			'fields' => array(
				'id',
				'amount',
				'created',
				'expiration_date',
				'status',
				'cust_acct_id'
			)
		));

		// sanity check, this method should only be called on approved or fulfilled promises
		if ($promise['Promise']['status'] === 'rejected')
		{
			throw new NotFoundException('This promise was rejected');
		}
		if ($promise['Promise']['status'] === 'proposed')
		{
			throw new NotFoundException('This promise was never approved');
		}

		// start of range
		$start_dt = $promise['Promise']['created'];

		// end of range
		$end_dt = __("%s 23:59:59", $promise['Promise']['expiration_date']);

		// customer account id
		$cust_acct_id = $promise['Promise']['cust_acct_id'];

		$recv_payment_sum = $this->query(
			"SELECT SUM (amount)
			FROM recv_payment_tbl
			WHERE recv_payment_tbl.cust_acct_id='{$cust_acct_id}'
			AND recv_payment_tbl.payment_method IN ('cash','check')
			AND process_status IN ('pending','complete')
			AND recv_payment_tbl.recv_dt >= '{$start_dt}'
			AND recv_payment_tbl.recv_dt <= '{$end_dt}'");
		$recv_payment_sum = $recv_payment_sum[0][0]['sum'] ?: 0;

		$credit_card_txn_sum = $this->query(
			"SELECT SUM (total_charge)
			FROM credit_card_txn_tbl
			WHERE credit_card_txn_tbl.cust_acct_id='{$cust_acct_id}'
			AND credit_card_txn_tbl.response_approved='APPROVED'
			AND credit_card_txn_tbl.response_txn_dt >= '{$start_dt}'
			AND credit_card_txn_tbl.response_txn_dt <= '{$end_dt}'");
		$credit_card_txn_sum = $credit_card_txn_sum[0][0]['sum'] ?: 0;

		$ach_debit_txn_sum = $this->query(
			"SELECT SUM (amount)
			FROM ach_debit_txn_tbl
			WHERE ach_debit_txn_tbl.cust_acct_id='{$cust_acct_id}'
			AND ach_debit_txn_tbl.cancelled=false
			AND ach_debit_txn_tbl.processed_dt >= '{$start_dt}'
			AND ach_debit_txn_tbl.processed_dt <= '{$end_dt}'");
		$ach_debit_txn_sum = $ach_debit_txn_sum[0][0]['sum'] ?: 0;

		// debug(compact('recv_payment_sum', 'credit_card_txn_sum', 'ach_debit_txn_sum'));

		// soap = sum of all payments
		$soap = $recv_payment_sum + $credit_card_txn_sum + $ach_debit_txn_sum;

		// true if the soap matched or exceeded the promised amount
		return $promise['Promise']['amount'] <= $soap;
	}

	public function markPromiseFulfilled($id = null)
	{
		$this->id = $id;
		return $this->saveField('status','fulfilled');
	}

	public function sendPromiseNotFulfilledCustomer($id = null) {

		$account = $this->find('first', array(
			'conditions' => array(
				'Promise.id' => $id
			),
			'contain' => array('CustomerAccount', 'Customer')
		));
		$subject = 'MicroOffice Promise NOT Fulfilled!';
		$text = 'Your Promise has been NOT been fulfilled! ';
		$html = '<p>Your Promise has been NOT been fulfilled! </p>';

		return $this->financeEmail($account, $subject, $text, $html);

	}
}

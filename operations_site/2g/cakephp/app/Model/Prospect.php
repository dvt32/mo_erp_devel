<?php
App::uses('AppModel', 'Model');
/**
 * Prospect Model
 *
 * @property LicenseAgreement $LicenseAgreement
 * @property SalesInteraction $SalesInteraction
 */
class Prospect extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'prospective_customer_tbl';

/**
 * Use sequence
 * 
 * @var mixed False or sequence name
 */
	public $sequence = 'prospective_customer_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'full_name';


	public $sales_statuses = array(
		'customer' => 'customer',
		'dead' => 'dead',
		'live' => 'live'
	);


	public $sales_stages = array(
		1 => '1-awaiting response from us',
		2 => '2-left message or sent email',
		3 => '3-established conversation',
		4 => '4-scheduled meeting',
		5 => '5-meeting completed',
		6 => '6-sent agreement',
		7 => '7-received agreement'
	);


	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		// $this->loadFields(array('sales_stage', 'sales_status'));
		$this->virtualFields['full_name'] = sprintf('%s.first_name || \' \' || %s.last_name', $this->alias, $this->alias);
	}

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'LicenseAgreement' => array(
			'className' => 'LicenseAgreement',
			'foreignKey' => 'prospective_customer_id',
			'dependent' => false,
		),
		'SalesInteraction' => array(
			'className' => 'SalesInteraction',
			'foreignKey' => 'prospective_customer_id',
			'dependent' => false,
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SalesStage' => array(
			'className' => 'SalesStage',
			'foreignKey' => 'sales_stage_id',
			'dependent' => true
		),
		'Building' => array(
			'className' => 'Building',
			'foreignKey' => 'building_id',
		)
	);

/**
 * hasAndBelongsToMany associations
 * 
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'SalesTag' => array(
			'className' => 'SalesTag',
			'joinTable' => 'prospects_sales_tags_tbl',
			'foreignKey' => 'prospect_id',
			'associationForeignKey' => 'sales_tag_id',
		)
	);


/**
 * Validation Rules
 * 
 * @var array
 */
	public $validate = array(
		
		// building_id (duplicate of desired_location)
		'building_id' => array(
			'numeric' => array(
				'rule' => 'numeric',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Please specify a building id'
			)
		),
		
		// customer name
		'first_name' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Prospect first name is required'
			)
		),
		'last_name' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Prospect last name is required'
			)
		),

		// email address
		'email_address' => array(
			'email' => array(
				'rule' => 'email',
				'required' => true,
				'allowEmpty' => true,
				'message' => 'Please enter an email address'
			)
		)
	);

	public function beforeValidate($options = array()) {

		// sales_stage ~ sales_stage_id
		$sales_stage_id = $this->SalesStage->id = @$this->data[$this->alias]['sales_stage_id'];
		if (!empty($sales_stage_id) && $this->SalesStage->exists()) {
			$this->data[$this->alias]['sales_stage'] = $this->SalesStage->field('name');
		}

		// desired_location ~ building_id
		$building_id = $this->Building->id = @$this->data[$this->alias]['building_id'];
		if (!empty($building_id) && $this->Building->exists()) {
			$this->data[$this->alias]['desired_location'] = $this->Building->field('short_global_label');
		}

		return true;
	}

	// public function beforeSave($options = array()) {
	// 	debug($this->data);
	// 	exit;
	// }

}

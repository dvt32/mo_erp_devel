<?php
App::uses('AppModel', 'Model');
App::uses('HttpSocket', 'Network/Http');


/**
 * ProximityCard Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Customer $Customer
 * @property AccessLevel $AccessLevel
 * @property Floor $Floor
 */
class ProximityCard extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'proximity_card_tbl';

/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'public.proximity_card_seq';

/**
 * Proximity Card status
 * 
 */

	public $statuses = array('active', 'inactive', 'lost');

/**
 * Proximity Card status
 * 
 */

	public $technologies = array('smart', 'proximity');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AccessLevel' => array(
			'className' => 'AccessLevel',
			'foreignKey' => 'access_level_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $validate = array(
		'number' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Card number is required'
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Card Number must be numeric',
			),
			// TODO enforce data integrity
			// 'isUnique' => array(
			// 	'rule' => 'isUnique',
			// 	'message' => 'Card number must be unique'
			// )
		),
		'person_id' => array(
			'numeric' => array(
				'rule' => array('notempty'),
				'message' => 'Please select an Account and Choose a Person',
			),
			// 'notEmpty' => array(
			// 	'rule' => 'notEmpty',
			// 	'required' => true,
			// 	'allowEmpty' => false,
			// 	'message' => 'Customer id is required'
			// ),
		),
		'account_id' => array(
			'numeric' => array(
				'rule' => array('notempty'),
				'message' => 'Please select an Account and Choose a Person',
			),
			// 'notEmpty' => array(
			// 	'rule' => 'notEmpty',
			// 	'required' => true,
			// 	'allowEmpty' => false,
			// 	'message' => 'Customer account id is required'
			// ),
		),
		'technology' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'A technology is required',
				'required' => true,
				'allowEmpty' => false,
			)
		),
	);


/**
 * check connection method
 * 
 * @author John Jung
 * @todo return true or false
 * @return $response array
 */
	public function checkIntellim() {

		$check_array['Username'] = PROXIMITY_USERNAME;
		$check_array['Password'] = PROXIMITY_PASSWORD;
		$check_array['Start'] = 0;
		$check_array['Limit'] = 50;

		// SENDS TO LEMUR
		$HttpSocket = new HttpSocket();

		// sends the post request
		$results = $HttpSocket->get(
		    PROXIMITY_URL.'/infinias/ia/People',
		    $check_array
		);
		$presult = json_decode($results->body, true);

		if ($presult['success'] == true) {
			return true;
		} else {
			return false;
		}

	}



/**
 * groups method
 * 
 * @author John Jung
 * @todo return true or false
 * @return $response array
 * @url http://doors-int.microoffice.com:18779/intelliM/PeopleGroup/List.ashx
 */
	public function groupsIntellim() {

		$person_array['Username'] = PROXIMITY_USERNAME;
		$person_array['Password'] = PROXIMITY_PASSWORD;

		// url encode
		$post_data = http_build_query($person_array);

		// SENDS TO LEMUR
		$HttpSocket = new HttpSocket();

		// // sends the post request
		$results = $HttpSocket->get(
		    PROXIMITY_URL.'/infinias/ia/People',
		    $person_array
		);

		$presult = json_decode($results->body, true);

		return $presult;

		// Check Connection
		if ($this->ProximityCard->checkIntellim()) {

			// Lemur connection working
			throw new Exception("Lemur server is connecting but requests are being refused. Check constants.php for creds or priveleges.");

		} else {
			
			// Lemur isn't connected
			throw new Exception("Lemur server either down or not connected to Cake");
			

		}

	}

/**
 * create method
 * 
 * @author John Jung
 * @todo check if person/proximity card exists
 * @return $response array
 */
	public function createIntellimPerson($person_array = array()) {

		// EXAMPLE
		// $person_array = array(
		// 	'PersonalInfo.FirstName' => 'John',
		// 	'PersonalInfo.LastName' => 'John',
		// 	'BadgeInfo.ActivationDate' => '09/23/2013',
		// 	'BadgeInfo.ActivationTime' => '07:00 PM',
		// 	'BadgeInfo.SiteCode' => '1',
		// 	'BadgeInfo.CardIssueCode' => '42651',
		// );

		$person_array['Username'] = PROXIMITY_USERNAME;
		$person_array['Password'] = PROXIMITY_PASSWORD;

		// SENDS TO LEMUR
		$HttpSocket = new HttpSocket();

		// sends the post request
		$results = $HttpSocket->post(
		    PROXIMITY_URL.'/infinias/ia/People',
		    $person_array
		);


		$presult = json_decode($results->body, true);

		// If successful returns json array:
		// {
		// success: true
		// RecordId: 1515
		// wrap: false
		// logout: false
		// errors: [0]
		// }
		if ($presult['success'] == true) {

			return $presult;

		} else {

			$errors = $presult['errors'];
			if (!empty($errors[0])) {
				throw new Exception("IntelliM Error: ".$errors[0]['msg']);
			}
			
		}

	}


/**
 * update method
 * 
 * @author John Jung
 * @return $response array
 */
	public function updateIntellimPerson($person_array = array()) {

		if (!empty($person_array['Id'])) {

			$person_array['Username'] = PROXIMITY_USERNAME;
			$person_array['Password'] = PROXIMITY_PASSWORD;

			// SENDS TO LEMUR
			$HttpSocket = new HttpSocket();

			// sends the put request
			$results = $HttpSocket->put(
			    PROXIMITY_URL.'/infinias/ia/People',
			    $person_array
			);

			$presult = json_decode($results->body, true);

			if ($presult['success'] == true) {
				return $presult;

			} else {

				$errors = $presult['errors'];

				if (!empty($errors[0])) {
					throw new Exception("IntelliM Error: ".$errors[0]['msg']);
				}

			}

		} else {

			throw new Exception("Id is not set for intellim update");

		}

	}


/**
 * delete method
 * @author John Jung
 * @throws NotFoundException
 * @param $person_id string - single Customer.id
 * @return $response array
 */
	public function deleteIntellimPerson($intellim_id = null) {

		// request to send
		$info = array(
			'Username' => PROXIMITY_USERNAME,
			'Password' => PROXIMITY_PASSWORD,
			'Id' => $intellim_id
		);
		// url encode
		$info = http_build_query($info);

		// Create socket
		$HttpSocket = new HttpSocket();
		// sends the post request
		$results = $HttpSocket->delete(
		    PROXIMITY_URL.'/infinias/ia/People?'.$info
		);
		$presult = json_decode($results->body, true);

		if ($presult['success'] == true) {

			return $presult;

		} else {

			$errors = $presult['errors'];
			if (isset($errors[0])) {
				throw new Exception("IntelliM Error: ".$errors[0]['msg']);
			}
			
		}
	}

/**
 * add cards method
 * @author John Jung
 * @param string start_card, string end_card, array $post_data posted array
 * @return $saveArray
 */

	public function saveCardsFromTo($start_card = null, $end_card = null, $post_data = array()) {

		// Validations
		if ($start_card == null || $end_card == null) {
			throw new Exception("start and end values must be defined");
		} else if($start_card == 0 || $end_card == 0) {
			throw new Exception("Start and End values cannot be zero");
		} else if ($start_card >= $end_card) {
			throw new Exception("Start value must be greater than end value");
		} else if (empty($post_data)) {
			throw new Exception("Posted array was empty");
		}

		// if all loops without problems
		$saveArray = array();

		// unset
		unset($post_data['ProximityCard']['range_start']);
		unset($post_data['ProximityCard']['range_end']);

		for ($i=$start_card; $i <= $end_card; $i++) { 
			
			$template = $post_data;
			// set card numbers
			$template['ProximityCard']['type'] = 'card';
			$template['ProximityCard']['account_id'] = '';
			$template['ProximityCard']['person_id'] = '';
			$template['ProximityCard']['access_level_id'] = '';
			$template['ProximityCard']['status'] = 'active';
			$template['ProximityCard']['is_active'] = true;
			$template['ProximityCard']['number'] = $i;
			$template['ProximityCard']['card_number'] = $post_data['ProximityCard']['site_code']."-".$i;

			array_push($saveArray, $template);

		}

		return $saveArray;

	}

/**
 * Clears the person_id and account_id on a proximity card
 * 
 * @param int $id proximity card id
 * @return boolean true if both fields were cleared successfully
 */
	public function clear($id = null) {
		$this->id = $id ?: $this->id;
		return $this->saveField('person_id',null) && $this->saveField('account_id',null);
	}

}

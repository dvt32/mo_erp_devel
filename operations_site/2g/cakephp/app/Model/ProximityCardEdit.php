<?php
App::uses('AppModel', 'Model');
/**
 * ProximityCardEdit Model
 *
 * @property CustomerAccount $CustomerAccount
 * @property Customer $Customer
 * @property AccessLevel $AccessLevel
 */
class ProximityCardEdit extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'proximity_card_edit_tbl';

/**
 * Proximity Card status
 * 
 */

	public $action_requesteds = array('activate', 'deactivate');

/**
 * Proximity Card status
 * 
 */

	public $causes = array('lost card', 'defective card', 'damaged card', 'closing account', 'new tenant', 'additional card for account');

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'account_id',
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'person_id',
		),
		'AccessLevel' => array(
			'className' => 'AccessLevel',
			'foreignKey' => 'access_level_id',
		),
	);
}

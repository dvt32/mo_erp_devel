<?php
App::uses('AppModel', 'Model');
/**
 * ReceivedPayment Model
 *
 * @property CustAcct $CustAcct
 * @property Employee $Employee
 * @property Deposit $Deposit
 */
class ReceivedPayment extends AppModel {

/**
 * Use table
 * @var mixed False or table name
 */
	public $useTable = 'recv_payment_tbl';

	public $sequence = 'public.recv_payment_seq';

/**
 * Payment destinations
 */
	public $destinations = array(
		// 'lockbox' => 'Lockbox',
		'manager' => 'Handed to Manager',
		// 'office' => 'Office',
		'bank' => 'Deposit to Bank',
	);

/**
 * belongsTo associations
 * @var array
 */
	public $belongsTo = array(
		'CustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'cust_acct_id',
		),
		'BillingAmount' => array(
			'className' => 'BillingAmount',
			'foreignKey' => 'billing_amount_id'
		),
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
		),
		'Deposit' => array(
			'className' => 'Deposit',
			'foreignKey' => 'deposit_id',
		)
	);

	public $validate = array(
		'amount' => array(
			'numeric' => array(
				'rule' => 'numeric',
				'required' => true,
				'allowEmpty' => false,
				'message' => "Invalid payment amount"
			)
		),
		'recv_dt' => array(
			'datetime' => array(
				'rule' => 'datetime',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Invalid received date/time'
			)
		)
	);

/**
 * Payment methods
 * @var array
 */
	public $paymentMethods = array(
		'check' => 'Check',
		'ach' => 'ACH',
		'card' => 'Credit Card',
		'cash' => 'Cash'
	);

	/**
	 * Apply a payment amount to the outstanding balances for a customer account
	 * 
	 * @param float $payment_amount the received payment amount to apply
	 * @param int $cust_acct_id customer account id to which the payment should be applied
	 */
	public function applyPaymentToCustomerAccount($payment_amount, $cust_acct_id)
	{
		// FIXME "apply the payments to the current invoice first, and then the oldest/most aged open invoices"

		// fetch the outstanding balances
		$orig_balances = $this->BillingAmount->find('list', array(
			'conditions' => array(
				'cust_acct_id' => $cust_acct_id,
				'stage' => array(
					// exclude 'paid',
					'invoicing',
					'receiving',
					// exclude 'disputed',
					'aging_hold',
					// exclude 'aging_closed',
					'aging_1',
					'aging_2',
					'aging_3',
					'aging_4',
				)
			),
			'fields' => ['id','balance'],
			'order' => 'due_date ASC', // FIXME? oldest first
		));

		// affected balances (TODO save these)
		$new_balances = $this->applyPaymentToBillingAmounts($payment_amount, $orig_balances);

		// remainder of payment amount after applied to balances (TODO save credit to customer account? or is it handled automatically?)
		$remainder = $this->remainder($payment_amount, $orig_balances);

		// fieldList
		$fieldList = ['id','balance'];

		// reindex calculated balances
		foreach ($new_balances as $id => $balance)
		{
			$balances[] = compact('id','balance','cust_acct_id');
		}

		// update the balances
		return $this->BillingAmount->saveMany($balances, ['fieldList' => $fieldList, 'validate' => false]);
	}

	/**
	 * Apply a payment amount to the billing amounts in order of oldest to newest outstanding balances
	 * @param float $payment_amount the amount to apply to the balances
	 * @param array $orig_balances the original balances before the payment is applied
	 * @return array $new_balances the new balances to be saved by the caller method
	 */
	public function applyPaymentToBillingAmounts($payment_amount, $orig_balances) {
		// FIXME "apply the payments to the current invoice first, and then the oldest/most aged open invoices"

		// sanity check
		if ($payment_amount <= 0)
		{
			throw new NotFoundException('Payment Amount is required and must be positive');
		}
		if (!is_array($orig_balances) || empty($orig_balances))
		{
			throw new NotFoundException('Original Balances must be an array of outstanding balances');
		}

		// collector
		$new_balances = array();

		// helper variable
		$remaining = $payment_amount;

		foreach ($orig_balances as $billing_amount_id => $orig_balance)
		{
			$new_balance = max(0, $orig_balance - $remaining);
			$remaining -= $orig_balance;

			// debug(compact('billing_amount_id','orig_balance','new_balance','remaining'));
			$new_balances[$billing_amount_id] = $new_balance;

			if ($remaining <= 0)
				break; // stop looping, skip unaffected balances
		}

		// return the array of new balances to be saved
		return $new_balances;
	}

/**
 * Updates the balances of the customer account and its billing amounts
 * 
 */
	public function afterSave($created, $options = array())
	{
		$cust_acct_id = $this->data[$this->alias]['cust_acct_id'];
		$amount = $this->data[$this->alias]['amount'];
		
		// apply payments to customer account billing amounts
		// $this->applyPaymentToCustomeraccount($amount, $cust_acct_id); // FIXME enable once the "apply payment" algorithm is corrected

		// update the status of customer account's billing amounts
		// $this->BillingAmount->updateStatusForCustomerAccount($cust_acct_id); // FIXME enable once the "apply payment" algorithm is corrected

		// update the customer account balance
		$this->CustomerAccount->updateBalance($cust_acct_id);

		// update the customer account aging count
		$this->CustomerAccount->updateAgingCount($cust_acct_id);
	}

/**
 * cancel
 */
	public function cancel($id = null) {
		$this->id = $id = $id ?: $this->id;
		return $this->saveField('process_status', 'cancelled', array('callbacks' => false));
	}

/**
 * Clears the deposit_id and changes status to pending
 * @param $id of the ReceivedPayment
 * @return boolean true if the save succeeds
 */
	public function removeFromDeposit($id = null) {
		
		// set model id
		$this->id = $id = $id ?: $this->id;

		// values to save
		$data = array($this->alias => array(
			'id' => $id,
			'process_status' => 'pending',
			'deposit_id' => null
		));

		// limit fields saved
		$options = array('fieldList' => array(
			$this->alias => array('id','process_status','deposit_id')
		));

		// save deposit
		return $this->save($data, $options);
	}
}

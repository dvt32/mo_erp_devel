<?php
App::uses('AppModel', 'Model');
/**
 * Room Model
 *
 * @property Floor $Floor
 * @property Notification $Notification
 * @property Reservation $Reservation
 */
class Room extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'room_tbl';

/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.room_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Notification' => array(
			'className' => 'Notification',
			'foreignKey' => 'room_id',
			'dependent' => false,
		),
		'Reservation' => array(
			'className' => 'Reservation',
			'foreignKey' => 'room_id',
			'dependent' => false,
		)
	);
	
}

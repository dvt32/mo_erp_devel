<?php
App::uses('AppModel', 'Model');
/**
 * SalesInteraction Model
 *
 */
class SalesInteraction extends AppModel {

/**
 * Sequence
 * 
 * @var string
 */
	public $sequence = 'sales_interaction_seq';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sales_interaction_tbl';


/**
 * belongsTo associations
 * 
 * @var array
 */
	public $belongsTo = array(
		'Prospect' => array(
			'className' => 'Prospect',
			'foreignKey' => 'prospective_customer_id',
			'counterCache' => true,
			'dependent' => false
		)
	);
}

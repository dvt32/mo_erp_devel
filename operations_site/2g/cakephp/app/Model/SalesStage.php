<?php
App::uses('AppModel', 'Model');
/**
 * SalesStage Model
 *
 */
class SalesStage extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'enum_sales_stage_tbl';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $hasMany = array(
		'Prospect' => array(
			'className' => 'Prospect',
			'foreignKey' => 'sales_stage_id'
		)
	);

}

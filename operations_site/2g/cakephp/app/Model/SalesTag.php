<?php
App::uses('AppModel', 'Model');
/**
 * SalesTag Model
 *
 */
class SalesTag extends AppModel {

	public $useTable = 'enum_sales_tag_tbl';
	public $displayField = 'name';
	public $hasAndBelongsToMany = array(
		'Prospect' => array(
			'className' => 'Prospect',
			'joinTable' => 'prospects_sales_tags_tbl',
			'foreignKey' => 'sales_tag_id',
			'associationForeignKey' => 'prospect_id',
			'unique' => true
		)
	);

}

<?php
App::uses('AppModel', 'Model');
/**
 * Scanner Model
 *
 * @property Floor $Floor
 */
class Scanner extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'scanner_tbl';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

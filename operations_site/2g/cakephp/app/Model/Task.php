<?php
App::uses('AppModel', 'Model');
/**
 * Task Model
 *
 * @property AssignedEmployee $AssignedEmployee
 * @property RequestedbyEmployee $RequestedbyEmployee
 * @property RequestedbyCustPers $RequestedbyCustPers
 * @property RequestedbyCustAcct $RequestedbyCustAcct
 * @property Floor $Floor
 * @property TaskNoteTbl $TaskNoteTbl
 */
class Task extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'task_tbl';


/**
 * Use sequence
 * 
 * @var string
 */
	public $sequence = 'public.task_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';


/**
 * Valid statuses for a task
 * NOTE: these keys are explicitly set (intentionally) to prevent problems when phasing out the old statuses
 * 
 * @var array
 */
	public $statuses = array(

		// old statuses
		// 0 => 'Requested',
		// 1 => 'In Progress',
		// 2 => 'Completed',
		3 => 'Cancelled',

		// new statuses
		4 => 'Awaiting Response',
		5 => 'Left Message or Sent Email',
		6 => 'Repair Scheduled',
		7 => 'Confirmed Complete',
	);


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AssignedtoEmployee' => array(
			'className' => 'Employee',
			'foreignKey' => 'assigned_employee_id',
		),
		'RequestedbyEmployee' => array(
			'className' => 'Employee',
			'foreignKey' => 'requestedby_employee_id',
		),
		'RequestedbyCustomer' => array(
			'className' => 'Customer',
			'foreignKey' => 'requestedby_cust_pers_id',
		),
		'RequestedbyCustomerAccount' => array(
			'className' => 'CustomerAccount',
			'foreignKey' => 'requestedby_cust_acct_id',
		),
		'Floor' => array(
			'className' => 'Floor',
			'foreignKey' => 'floor_id',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Note' => array(
			'className' => 'TaskNote',
			'foreignKey' => 'task_id',
			'dependent' => false,
		)
	);


	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		// $fields = array('areas','urgencies');
		$this->loadFields(array('areas','urgencies'));
	}



/**
 * Automatically record completion date when changing task status to complete, and discard the 
 * completion date when changing the status to anything else.
 * 
 * @param array $options
 * @return boolean True if the save operation should continue
 */
	public function beforeSave($options = array()) {
		parent::beforeSave($options);

		// check the 'status' field
		$status = @$this->data[$this->alias]['status'];

		// if the 'status' field is being saved
		if (!empty($status)) {

			// also record or discard the completion date
			$this->data[$this->alias]['completion_date'] = (strcasecmp($status,'Complete') == 0) ? date('Y-m-d') : null;
		}

		// continue the save
		return true;
	}

}

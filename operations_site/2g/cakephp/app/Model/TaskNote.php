<?php
App::uses('AppModel', 'Model');
/**
 * TaskNote Model
 *
 * @property Task $Task
 * @property Employee $Employee
 */
class TaskNote extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'task_note_tbl';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $sequence = 'public.task_note_seq';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'content';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'task_id',
		),
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
		)
	);
}

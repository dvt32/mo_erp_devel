<?php
App::uses('CakeEmail', 'Network/Email');


/**
 * Token Model
 * Used for all user-specific email communications,
 * e.g., forgot password, email address confirmation
 * 
 * @property User $User
 */
class Token extends AppModel {


/**
 * Uses the token_tbl table
 * 
 * @var string
 */
	public $useTable = "token_tbl";


/**
 * sequence the pk of this model uses
 * 
 * @var string
 */
	public $sequence = 'token_seq';


/**
 * belongsTo associations
 * 
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
	);


/**
 * validation rules
 * 
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'required' => true
			)
		),
		'expires' => 'datetime',
		'jumble' => 'isUnique',
	);


/**
 * Generates a strongly random string. Guaranteed to be unique.
 * 
 * @param $len desired length of string
 * @return Random url-safe string of desired length
 */
	public function generateJumble($len = 16) {
		do {
			$fp = @fopen('/dev/urandom','rb');
			$jumble = '';
			if ($fp !== FALSE) {
				$bytes = ((int) ($len * 3 / 4)) + 1;
				$jumble .= @fread($fp, $bytes);
				@fclose($fp);
			} else {
				// fallback
				$times = ((int) ($len / 56)) + 1;
				while ($times--) {
					$jumble .= sha1(mt_rand());
				}
			}
			$jumble = base64_encode($jumble); // convert from binary to string
			$jumble = strtr($jumble, '+/', '-_'); // replace non-url chars
			$jumble = str_replace('=', ' ', $jumble); // remove '=' from the end
			$jumble = substr($jumble, 0, $len); // truncate to desired length
		} while ($this->find('count', array('conditions' => array('jumble' => $jumble))) > 0);
		return $jumble;
	}


/**
 * Finds a valid token given a jumble-type combination.
 * A token is valid iff the expiration date has not passed, and it has not been used.
 * 
 * @return $token A valid token or null if no such valid token exists
 */
	public function findValid($jumble, $type) {
		// fetch the requested token
		$requested_token = $this->findByJumbleAndType($jumble, $type);

		// continue iff the requested token is found
		if (!empty($requested_token)) {
			// continue iff the requested token is unused and not expired
			if (empty($requested_token['Token']['is_spent']) && $requested_token['Token']['expires'] > date(DATETIME_FORMAT)) {
				// a user has only one valid token of a particular type
				$valid_token = $this->find(
					'first',
					array(
						'order' => 'Token.created DESC',
						'conditions' => array(
							'Token.user_id' => $requested_token['Token']['user_id'],
							'Token.type' => $requested_token['Token']['type']
						),
						'fields' => 'Token.id'
					)
				);

				// return the token iff the requested token is the valid token
				if ($requested_token['Token']['id'] === $valid_token['Token']['id']) {
					return $requested_token;
				}
			}
		}
		return null;
	}


/**
 * Marks all tokens with a particular user-and-type combo as used, 
 * i.e., each user only has one valid token of a particular type.
 * 
 * @param int $user_id 
 * @param string $type
 * @return boolean true if the relevant tokens were properly marked as used
 */
	public function markAllUsed($user_id, $type = 'resetPassword') {
		$fields = array('Token.is_spent' => true);
		$conditions = array('Token.user_id' => $user_id, 'Token.type' => $type);
		return $this->updateAll($fields, $conditions);
	}


/**
 * Generate and set a jumble to be saved for this token.
 * Also set the token expiration date.
 * 
 * @param array $options options for beforeValidate callback
 * @return boolean true
 */
	public function beforeValidate($options = array()) {

		// generate and set the jumble
		if (!isset($this->data['Token']['jumble'])) {
			$this->jumble = $this->data['Token']['jumble'] = $this->generateJumble();
		}

		// set the token expiration datetime
		if (!isset($this->data['Token']['expires'])) {
			$this->expires = $this->data['Token']['expires'] = date(DATETIME_FORMAT, strtotime('+1 month'));
		}

		return true;
	}


/**
 * Set is_spent to false for the token.
 * 
 * @return boolean true
 */
	public function beforeSave($options = array()) {
		$this->data[$this->alias]['is_spent'] = 0;
		return true;
	}


/**
 * If the token was newly created, send an email with the token link
 * 
 * @param boolean $created True if this save created a new record
 * @param array $options Options passed from Model::save().
 */
	public function afterSave($created, $options = array()) {
		// token saved?
		if ($created) {
			// fetch just created token info
			$token = $this->read();
			
			$this->send($token['Token']['type'], $token['Token']['jumble'], $token['User']['username']);
		}
	}


/**
 * Send an email with the token link, and a brief message
 * 
 * @param string $type The type of the token being sent
 * @param string $jumble The randomly generated, unique identifier
 * @param string $username The email address to which the token should be sent
 * @return void
 */
	public function _send($type, $jumble, $username) {

		// build url
		$host = FULL_BASE_URL;
		$url = "{$host}/tokens/{$type}/{$jumble}";

		// send the email
		if (REALLY_SEND_EMAIL === true) {

			// configure email
			$email = new CakeEmail();
			$email->config(EMAIL_CONFIG)
				->to($username)
				->subject($this->_types[$type]['email']['subject'] ?: $type);

			// send the email
			$result = $email->send($this->_types[$type]['email']['message'] . "\n" . "{$url}");
			debug($result);
		}
		// do NOT send the email, display token link on screen instead
		else {
			echo $url;
			exit;
		}
	}

}


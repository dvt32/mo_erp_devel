<?php
App::uses('AppModel','Model');

class User extends AppModel {

	public $useTable = 'user_tbl';

  /**
 * Use sequence
 * 
 * @var string
 */
  public $sequence = 'public.user_seq';

/**
 * Hash the password before saving the user
 * @return boolean
 */
	public function beforeSave($options = array()) {

    parent::beforeSave();

		// only perform if the password field is set
		if (isset($this->data[$this->alias]['password'])) {

			// generate a hash and salt combo
      // EDITED BY CATHY as stopgap. // fix me
      // Same hash as BizCenter so users can login
			$hash_salt = AuthComponent::password($this->data[$this->alias]['password']);

			// set the fields to be saved
			// $this->data[$this->alias]['hash'] = $hash_salt['hash'];
			// $this->data[$this->alias]['salt'] = $hash_salt['salt'];

		  // unset the password field (comment out to save as cleartext)
		  $this->data[$this->alias]['password'] = $hash_salt; // TODO toggle per business policy
		}

		return true;
	}


/**
 * Generate a hash-salt combo for a cleartext string
 * 
 * @param string $cleartext string to hash
 * @return array hash and salt values
 */
	public static function hash($cleartext, $salt = "") {

		// blowfish salt charset
		static $charset = './abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

	  // generate a random salt if one isn't provided
          if ($salt == "") {
            $salt = '$2a$08$' . random_string(22, $charset);
          }

	  // hash the password using the salt
	  $hash = crypt($cleartext, $salt);

	  return compact('hash','salt');
	}

  /**
 * hasOne associations
 * 
 * @var array
 */
  //public $hasOne = array(
  //  // EDITED BY CATHY: User should have one associated cust_pers_id
  //  'Customer' => array(
  //    'className' => 'Customer',
  //    'foreignKey' => 'cust_pers_id',
  //  )
  //);

/**
 * validation rules
 * 
 * @var array
 */
  public $validate = array(

    // email address
    'username' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'Please enter a valid email address'
      ),
      'email' => array(
        'rule' => array('email'),
        'message' => 'Please enter a valid email address'
      ),
      'isUnique' => array(
        'rule' => array('isUnique'),
        'message' => 'That email address has already been registered'
      )
    ),

    // first name
    'first_name' => array(
      'rule' => array('notEmpty'),
      'message' => 'Please enter your first name',
    ),

    // last name
    'last_name' => array(
      'rule' => array('notEmpty'),
      'message' => 'Please enter your last name',
    ),

    // current password    
    'current_password' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'required' => true,
        'on' => 'update',
      ),
      'matchesCurrent' => array(
        'rule' => 'matchesCurrentPassword',
        'matches' => 'The password you entered does not match your existing password'
      )
    ),

    // password
    'password' => array(
      'minlength' => array(
        'rule' => array('minlength',8),
        'message' => 'Password must be at least 8 characters long.',
      ),
    ),

    // confirm password
    'confirm_password' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'required' => true,
        'message' => 'Please make sure you typed in both passwords correctly. ',
      ),
      'matches' => array(
        'rule' => array('matches','password'),
        'message' => 'Please make sure you typed in both passwords correctly. ',
      )
    )
  );

/**
 * 
 * @var array
 */
  public $role = array(
    '0' => 'Normal',
    '1' => 'Admin',
    '2' => 'Primary',
  );
}

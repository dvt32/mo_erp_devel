<?php
App::uses('AppModel', 'Model');
/**
 * Voicemail Model
 *
 */
class Voicemail extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'voicemail_msgs_tbl';

// /**
//  * belongsTo associations
//  *
//  * @var array
//  */
// 	public $belongsTo = array(
// 		'PhoneNumber' => array(
// 			'className' => 'PhoneNumber',
// 			'foreignKey' => 'username',
// 		),
// 	);

}

#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SQL=$DIR'/routes.sql'
TMP=$DIR'/routes.sql.tmp'

rm $SQL
rm $TMP

cd $DIR'/../Controller'

echo "TRUNCATE aco_tbl;"
echo "INSERT INTO aco_tbl (\"controller\",\"action\") VALUES"

for CONTROLLER in *Controller.php; do
	cat $CONTROLLER | grep 'public function' | awk '{print $3}' | awk -F'(' '{print $1}' | while read ACTION; do
		echo "('${CONTROLLER/Controller.php/}','$ACTION'),"
		echo "('${CONTROLLER/Controller.php/}','$ACTION')," >> $TMP
	done
done

sed -e '$s/,$/;/' < $TMP > $SQL
rm $TMP

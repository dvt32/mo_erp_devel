<?php
/**
 * AcoFixture
 *
 */
class AcoFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'aco_tbl';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => 'nextval((\'aco_seq\'', 'length' => 11, 'key' => 'primary'),
		'controller' => array('type' => 'string', 'null' => true, 'length' => 45),
		'action' => array('type' => 'string', 'null' => true, 'length' => 45),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'controller' => 'Lorem ipsum dolor sit amet',
			'action' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-06-10 18:52:24',
			'modified' => '2014-06-10 18:52:24'
		),
	);

}

<?php
/**
 * PermissionFixture
 *
 */
class PermissionFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'permission_tbl';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'role_name' => array('type' => 'string', 'null' => true, 'length' => 45),
		'aco_id' => array('type' => 'integer', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'indexes' => array(
			
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'role_name' => 'Lorem ipsum dolor sit amet',
			'aco_id' => 1,
			'created' => '2014-06-10 22:43:26',
			'modified' => '2014-06-10 22:43:26'
		),
	);

}

<div class="accessLevels view">
<h2><?php echo __('Access Level'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accessLevel['AccessLevel']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($accessLevel['AccessLevel']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($accessLevel['AccessLevel']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($accessLevel['AccessLevel']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($accessLevel['AccessLevel']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($accessLevel['AccessLevel']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accessLevel['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $accessLevel['Floor']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Access Level'), array('action' => 'edit', $accessLevel['AccessLevel']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Access Level'), array('action' => 'delete', $accessLevel['AccessLevel']['id']), null, __('Are you sure you want to delete # %s?', $accessLevel['AccessLevel']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Card Edits'), array('controller' => 'proximity_card_edits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('controller' => 'proximity_card_edits', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Proximity Card Edits'); ?></h3>
	<?php if (!empty($accessLevel['ProximityCardEdit'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Number'); ?></th>
		<th><?php echo __('Account Id'); ?></th>
		<th><?php echo __('Person Id'); ?></th>
		<th><?php echo __('Action Requested'); ?></th>
		<th><?php echo __('Cause'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th><?php echo __('Operator'); ?></th>
		<th><?php echo __('Access Level Id'); ?></th>
		<th><?php echo __('Completed'); ?></th>
		<th><?php echo __('Rejected'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($accessLevel['ProximityCardEdit'] as $proximityCardEdit): ?>
		<tr>
			<td><?php echo $proximityCardEdit['id']; ?></td>
			<td><?php echo $proximityCardEdit['number']; ?></td>
			<td><?php echo $proximityCardEdit['account_id']; ?></td>
			<td><?php echo $proximityCardEdit['person_id']; ?></td>
			<td><?php echo $proximityCardEdit['action_requested']; ?></td>
			<td><?php echo $proximityCardEdit['cause']; ?></td>
			<td><?php echo $proximityCardEdit['notes']; ?></td>
			<td><?php echo $proximityCardEdit['ms_access_creation_dt']; ?></td>
			<td><?php echo $proximityCardEdit['ms_access_random_num']; ?></td>
			<td><?php echo $proximityCardEdit['creation_dt']; ?></td>
			<td><?php echo $proximityCardEdit['operator']; ?></td>
			<td><?php echo $proximityCardEdit['access_level_id']; ?></td>
			<td><?php echo $proximityCardEdit['completed']; ?></td>
			<td><?php echo $proximityCardEdit['rejected']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proximity_card_edits', 'action' => 'view', $proximityCardEdit['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proximity_card_edits', 'action' => 'edit', $proximityCardEdit['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proximity_card_edits', 'action' => 'delete', $proximityCardEdit['id']), null, __('Are you sure you want to delete # %s?', $proximityCardEdit['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proximity Card Edit'), array('controller' => 'proximity_card_edits', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Proximity Cards'); ?></h3>
	<?php if (!empty($accessLevel['ProximityCard'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Number'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Account Id'); ?></th>
		<th><?php echo __('Person Id'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th><?php echo __('Access Level Id'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Technology'); ?></th>
		<th><?php echo __('Site Code'); ?></th>
		<th><?php echo __('Intellim Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($accessLevel['ProximityCard'] as $proximityCard): ?>
		<tr>
			<td><?php echo $proximityCard['id']; ?></td>
			<td><?php echo $proximityCard['number']; ?></td>
			<td><?php echo $proximityCard['type']; ?></td>
			<td><?php echo $proximityCard['status']; ?></td>
			<td><?php echo $proximityCard['account_id']; ?></td>
			<td><?php echo $proximityCard['person_id']; ?></td>
			<td><?php echo $proximityCard['note']; ?></td>
			<td><?php echo $proximityCard['ms_access_creation_dt']; ?></td>
			<td><?php echo $proximityCard['ms_access_random_num']; ?></td>
			<td><?php echo $proximityCard['creation_dt']; ?></td>
			<td><?php echo $proximityCard['access_level_id']; ?></td>
			<td><?php echo $proximityCard['floor_id']; ?></td>
			<td><?php echo $proximityCard['technology']; ?></td>
			<td><?php echo $proximityCard['site_code']; ?></td>
			<td><?php echo $proximityCard['intellim_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proximity_cards', 'action' => 'view', $proximityCard['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proximity_cards', 'action' => 'edit', $proximityCard['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proximity_cards', 'action' => 'delete', $proximityCard['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

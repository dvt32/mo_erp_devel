<div class="acos form">
<?php echo $this->Form->create('Aco'); ?>
	<fieldset>
		<legend><?php echo __('Edit Aco'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('controller');
		echo $this->Form->input('action');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Aco.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Aco.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Acos'), array('action' => 'index')); ?></li>
	</ul>
</div>

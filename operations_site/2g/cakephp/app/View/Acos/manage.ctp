<div class="row-fluid">

	<div class="span6">
		<legend>Register Route</legend>
		<?= $this->Form->create('Aco', ['inputDefaults' => ['class' => 'input-block-level']]) ?>
			<div class="row-fluid">
				<?= $this->Form->input('controller', ['placeholder' => 'Controller Name (camel-cased + plural), without "Controller" suffix']) ?>
				<?= $this->Form->input('action', ['placeholder' => 'Action Name (as defined in controller class)']) ?>
			</div>
			<?= $this->Form->button('Save', ['class' => 'btn btn-primary']) ?>
		<?= $this->Form->end() ?>
	</div>

	<div class="span6">
		<legend>Search Routes</legend>
		<?= $this->Form->create('Aco', ['type' => 'get', 'inputDefaults' => ['class' => 'input-block-level']]) ?>
			<?= $this->Form->input('q', ['label' => false]) ?>
			<?= $this->Form->button('Search', ['class' => 'btn btn-primary']) ?>
		<?= $this->Form->end() ?>
	</div>

</div>

<div class="row-fluid">
	<div class="span12">
		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th class="shrink"><?= $this->Paginator->sort('controller') ?></th>
					<th class="shrink"><?= $this->Paginator->sort('action') ?></th>
					<th class="stretch">Role(s)</th>
					<th class="shrink">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($acos as $aco): ?>
				<tr>
					<td><?= $aco['Aco']['controller'] ?></td>
					<td><?= $aco['Aco']['action'] ?></td>
					<td class="stretch">
						<?php if ($aco['Permission']): ?>
							<?= implode(',',collect('role_name',$aco['Permission'])) ?>
						<?php else: ?>
							<span class="muted">Universal Access</span>
						<?php endif ?>
					</td>
					<td><?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $aco['Aco']['id']), null, __('Are you sure you want to delete # %s?', $aco['Aco']['id'])); ?> </td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>

		<?= $this->element('paging') ?>

	</div>
</div>

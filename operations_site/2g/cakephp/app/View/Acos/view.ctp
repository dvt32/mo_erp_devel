<div class="acos view">
<h2><?php echo __('Aco'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($aco['Aco']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Controller'); ?></dt>
		<dd>
			<?php echo h($aco['Aco']['controller']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action'); ?></dt>
		<dd>
			<?php echo h($aco['Aco']['action']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($aco['Aco']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($aco['Aco']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Aco'), array('action' => 'edit', $aco['Aco']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Aco'), array('action' => 'delete', $aco['Aco']['id']), null, __('Are you sure you want to delete # %s?', $aco['Aco']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Acos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aco'), array('action' => 'add')); ?> </li>
	</ul>
</div>

<div class="row-fluid">
	<section class="module span6">
		<div class="module-head">
			<b>Account Information</b>
		</div>
		<div class="module-body">
			<dl>
				<dt><?= __('Name') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['name']) ?>&nbsp;</dd>
				<dt><?= __('Primary Customer') ?></dt>
				<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?>&nbsp;</dd>
				<dt><?= __('Is Active') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['is_active'] ? 'yes' : 'no') ?>&nbsp;</dd>
				<dt><?= __('Creation Dt') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['creation_dt']) ?>&nbsp;</dd>
				<dt><?= __('External Name') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['external_name']) ?>&nbsp;</dd>
				<dt><?= __('Description') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['description']) ?>&nbsp;</dd>
				<dt><?= __('Type') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['type']) ?>&nbsp;</dd>
				<dt><?= __('Note') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['note']) ?>&nbsp;</dd>
				<dt><?= __('Tax Id') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['tax_id']) ?>&nbsp;</dd>
				<dt><?= __('Service') ?></dt>
				<dd><?= $customerAccount['Service']['name'] ?>&nbsp;</dd>
				<dt><?= __('Preferred Payment Method') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['preferred_payment_method']) ?>&nbsp;</dd>
				<dt><?= __('Room Minutes') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['room_minutes']) ?>&nbsp;</dd>
				<dt><?= __('Floor') ?></dt>
				<dd><?= $customerAccount['Floor']['short_global_label'] ?>&nbsp;</dd>
				<dt><?= __('Status') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['status']) ?>&nbsp;</dd>
				<dt><?= __('Followup Date') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['aging_followup_date']) ?>&nbsp;</dd>
			</dl>
		</div>
	</section>

	<?php $pc = $customerAccount['PrimaryCustomer'] ?>
	<section class="module span6">
		<div class="module-head">
			<b>Primary Contact Information</b>
		</div>
		<div class="module-body">
			<dl>
				<dt>Name</dt>
				<dd><?= $pc['salutation'] . ' ' . $pc['first_name'] . ' ' . $pc['middle_name'] . ' ' . $pc['last_name'] ?></dd>
				<dt>Email Address</dt>
				<dd><?= $pc['email'] ?></dd>
				<dt>Work Phone</dt>
				<dd><?= $pc['work_phone_number'] ?></dd>
				<dt>Home Phone</dt>
				<dd><?= $pc['home_phone_number'] ?></dd>
				<dt>Cell Phone</dt>
				<dd><?= $pc['cell_phone_number'] ?></dd>
			</dl>
		</div>
	</section>
</div>

<div class="row-fluid">
	<section class="module">
			<div class="module-head">
				<b>Recent Billing Amounts</b>
			</div>
			<div class="module-body">
				<table class="table table-condensed table-striped">
					<thead>
						<tr>
							<th>Due Date</th>
							<th>Grand Total</th>
							<th>Balance</th>
							<th>Recurring Amount</th>
							<th>Stage</th>
							<th>Days Overdue</th>
							<th>Desks</th>
							<th>Offices</th>
							<th>Phone Lines</th>
							<th>Internet Lines</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($customerAccount['BillingAmount'] as $billingAmount): ?>
							<tr>
								<td><?= $billingAmount['due_date'] ?></td>
								<td><?= $billingAmount['grand_total'] ?></td>
								<td><?= $billingAmount['balance'] ?></td>
								<td><?= $billingAmount['recurring_amount'] ?></td>
								<td><?= $billingAmount['stage'] ?></td>
								<td><?= $billingAmount['days_overdue'] ?></td>
								<td><?= $billingAmount['num_desks'] ?></td>
								<td><?= $billingAmount['num_offices'] ?></td>
								<td><?= $billingAmount['num_phone_lines'] ?></td>
								<td><?= $billingAmount['num_persons'] ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</section>	
</div>

<div class="row-fluid">
	<div class="span6">
		<section class="module">
			<div class="module-head">
				<b>Add Aging Interaction</b>
			</div>
			<div class="module-body">

				<div class="row-fluid">

					<div class="span12">
						<?= $this->Form->create('AgingInteraction', ['inputDefaults' => ['class' => 'input-block-level']]) ?>

						<label>Customer Account</label>
						<?= $this->form->text('customer_account', ['value' => $customerAccount['CustomerAccount']['name'], 'disabled']) ?>
						<label>Interaction Type</label>
						<?= $this->Form->input('AgingInteraction.type', ['type' => 'radio', 'legend' => false, 'required' => true, 'value' => 'phone_call']) ?>
						<div class="row-fluid">
							<?= $this->Form->input('AgingInteraction.creation_dt', [
								'type' => 'text',
								'class' => 'form-control datetime',
								'value' => date(DATETIME_FORMAT),
								'label' => 'Contacted At',
								'required' => true,
								'div' => 'span6'
							]) ?>
							<?= $this->Form->hidden('CustomerAccount.id', ['value' => $cust_acct_id]) ?>
							<?= $this->Form->input('CustomerAccount.aging_followup_date', [
									'type' => 'text',
									'class' => 'form-control date',
									'label' => 'Follow Up On',
									'title' => 'This wil override an existing follow-up date for the customer account',
									'required' => false,
									'div' => 'span6'
							]) ?>
						</div>
						<?= $this->Form->hidden('AgingInteraction.cust_acct_id', ['value' => $cust_acct_id]) ?>
						<?= $this->Form->input('AgingInteraction.employee_id', ['type' => 'hidden', 'value' => AuthComponent::user('id')]) ?>
						<?= $this->Form->input('AgingInteraction.note', ['type' => 'textarea', 'rows' => 2]) ?>
					</div>
					
					<?= $this->Form->button('Save Interaction', ['class' => 'btn btn-primary']) ?>
					<input type="hidden" name="section" value="interaction">
					<?= $this->Form->end() ?>
				</div>
			</div>
		</section>

		<section class="module">
			<div class="module-head">
				<b>Previous Interactions</b>
			</div>
			<div class="module-body">
				<?php foreach($customerAccount['AgingInteraction'] as $interaction): ?>
					<strong><?= $interaction['Employee']['full_name'] ?></strong> @
					<span class="small"> <?= $interaction['creation_dt'] // FIXME change format to usa 12-hour ?> </span>
				 <p><?= $interaction['note'] ?></p>
				<?php endforeach ?>
			</div>
		</section>
	</div>
	<div class="span6">
		<section class="module">
			<div class="module-head">
				<b>Add Promise</b>
			</div>
			<div class="module-body">
				<div class="row-fluid">

					<div class="span12">
						<?= $this->Form->create('Promise', ['inputDefaults' => ['class' => 'input-block-level']]) ?>

						<label>Customer Account</label>
						<?= $this->form->text('customer_account', ['value' => $customerAccount['CustomerAccount']['name'], 'disabled']) ?>
						<?= $this->Form->hidden('Promise.cust_acct_id', ['value' => $cust_acct_id]) ?>
						<?= $this->Form->hidden('Promise.status', ['value' => 'pending']) ?>

						<div class="row-fluid">
							<?= $this->Form->input('Promise.cust_pers_id', ['options' => $customers, 'class' => 'chosen input-block-level', 'div' => 'span6', 'label' => 'Person']) ?>
						</div>

						<div class="row-fluid">
							<?= $this->Form->input('Promise.expiration_date', ['type' => 'text', 'class' => 'date', 'required']) ?>

							<label for="PromiseAmount">Promised Amount</label>
							<div class="input-prepend">
								<span class="add-on">$</span>
								<?= $this->Form->input('Promise.amount', ['label' => false, 'div' => false, 'required']) ?>
							</div>
							<?= $this->Form->input('Promise.notes', ['type' => 'textarea', 'rows' => '2']) ?>
							<?= $this->Form->input('Promise.employee_id', ['type' => 'hidden', 'value' => AuthComponent::user('id')]) ?>
							<?= $this->Form->input('Promise.status', ['type' => 'hidden', 'value' => 'proposed']) ?>
							<?= $this->Form->button('Save Promise', ['class' => 'btn btn-info']) ?>
							<input type="hidden" name="section" value="promise">
							<?= $this->Form->end() ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="module">
			<div class="module-head">
				<b>Previous Promise Proposals</b>
			</div>
			<div class="module-body no-padding">
					<table class="table table-condensed table-striped">
						<thead>
							<tr>
								<th>Employee</th>
								<th>Customer</th>
								<th>Amount</th>
								<th>Expires</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($customerAccount['Promise'] as $promise): ?>
								<?= ($promise['expiration_date'] > date('Y-m-d')) ? '<tr>' : '<tr class="muted">' ?>
								<td><?= $promise['Employee']['full_name'] ?></td>
								<td><?= $promise['Customer']['full_name'] ?></td>
								<td>$<?= __('%.2f', $promise['amount']) ?></td>
								<td><?= $promise['expiration_date'] ?></td>
								<td><?= $promise['status'] ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</section>
	</div>
</div>

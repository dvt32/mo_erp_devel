<div class="agingInteractions form">
<?php echo $this->Form->create('AgingInteraction'); ?>
	<fieldset>
		<legend><?php echo __('Edit Aging Interaction'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('employee_id');
		echo $this->Form->input('type');
		echo $this->Form->input('note');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('modifier_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AgingInteraction.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AgingInteraction.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Aging Interactions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

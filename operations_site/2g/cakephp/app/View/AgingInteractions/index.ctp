<div class="agingInteractions index">
	<h2><?= __('Aging Interactions') ?></h2>
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('cust_acct_id') ?></th>
				<th><?= $this->Paginator->sort('employee_id') ?></th>
				<th><?= $this->Paginator->sort('type') ?></th>
				<th><?= $this->Paginator->sort('note') ?></th>
				<th><?= $this->Paginator->sort('creation_dt') ?></th>
				<th><?= $this->Paginator->sort('modified') ?></th>
				<th><?= $this->Paginator->sort('modifier_id') ?></th>
				<th class="actions"><?= __('Actions') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($agingInteractions as $agingInteraction): ?>
				<tr>
					<td><?= $this->Html->link($agingInteraction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $agingInteraction['CustomerAccount']['id'])) ?></td>
					<td><?= $this->Html->link($agingInteraction['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $agingInteraction['Employee']['id'])) ?></td>
					<td><?= h($agingInteraction['AgingInteraction']['type']) ?>&nbsp;</td>
					<td><?= h($agingInteraction['AgingInteraction']['note']) ?>&nbsp;</td>
					<td><?= h($agingInteraction['AgingInteraction']['creation_dt']) ?>&nbsp;</td>
					<td><?= h($agingInteraction['AgingInteraction']['modified']) ?>&nbsp;</td>
					<td>
						<?= $this->Html->link($agingInteraction['Modifier']['full_name'], array('controller' => 'employees', 'action' => 'view', $agingInteraction['Modifier']['id'])) ?>
					</td>
					<td class="actions">
						<?= $this->Html->link(__('View'), array('action' => 'view', $agingInteraction['AgingInteraction']['id'])) ?>
						<?= $this->Html->link(__('Edit'), array('action' => 'edit', $agingInteraction['AgingInteraction']['id'])) ?>
						<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $agingInteraction['AgingInteraction']['id']), null, __('Are you sure you want to delete # %s?', $agingInteraction['AgingInteraction']['id'])) ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?= $this->element('paging') ?>
</div>

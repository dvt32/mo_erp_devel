<div class="agingInteractions view">
<h2><?php echo __('Aging Interaction'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($agingInteraction['AgingInteraction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($agingInteraction['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $agingInteraction['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($agingInteraction['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $agingInteraction['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($agingInteraction['AgingInteraction']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($agingInteraction['AgingInteraction']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($agingInteraction['AgingInteraction']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($agingInteraction['AgingInteraction']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($agingInteraction['AgingInteraction']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($agingInteraction['AgingInteraction']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modifier'); ?></dt>
		<dd>
			<?php echo $this->Html->link($agingInteraction['Modifier']['full_name'], array('controller' => 'employees', 'action' => 'view', $agingInteraction['Modifier']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Aging Interaction'), array('action' => 'edit', $agingInteraction['AgingInteraction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Aging Interaction'), array('action' => 'delete', $agingInteraction['AgingInteraction']['id']), null, __('Are you sure you want to delete # %s?', $agingInteraction['AgingInteraction']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Aging Interactions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aging Interaction'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

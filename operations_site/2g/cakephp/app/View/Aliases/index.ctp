<div class="aliases index">
	<h2><?php echo __('Aliases'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($aliases as $alias): ?>
	<tr>
		<td><?php echo h($alias['Alias']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($alias['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $alias['CustomerAccount']['id'])); ?>
		</td>
		<td><?php echo h($alias['Alias']['name']); ?>&nbsp;</td>
		<td><?php echo h($alias['Alias']['note']); ?>&nbsp;</td>
		<td><?php echo h($alias['Alias']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($alias['Alias']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($alias['Alias']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $alias['Alias']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $alias['Alias']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $alias['Alias']['id']), null, __('Are you sure you want to delete # %s?', $alias['Alias']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Alias'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

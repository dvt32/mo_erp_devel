<legend><?= __('Aged Receivables') ?></legend>
<ul class="nav nav-tabs">
	<li><?= $this->Html->link('Customer Account', array('controller' => 'customerAccounts', 'action' => 'aging')) ?></li>
	<li class="active"><?= $this->Html->link('Billing Amount', '#') ?></li>
</ul>
<div class="row-fluid">
	<div class="span3">
		<?= $this->Form->create('BillingAmount', array(
			'type' => 'get',
			'inputDefaults' => array(
				'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
				'div' => array('class' => 'control-group'),
				'label' => array('class' => 'control-label'),
				'between' => '<div class="controls">',
				'after' => '</div>',
				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
				'required' => false
			)
		)) ?>
			<?= $this->Form->input('cust_acct_id', array(
				'options' => $customerAccounts,
				'class' => 'input-block-level chosen click-submit',
				'empty' => '',
				'label' => 'Customer Account'
			)) ?>
			<?= $this->Form->input('stage', array(
				'options' => $stages,
				'multiple' => 'checkbox'
			)) ?>
			<div class="row-fluid">
				<?= $this->Form->input('floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
			</div>
			<div class="row-fluid">
				<?= $this->Form->input('start_date', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6'
				)) ?>
				<?= $this->Form->input('end_date', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6',
				)) ?>
			</div>
			<div class="row-fluid">
				<?= $this->Form->input('min_gt', array(
					'type' => 'text',
					'class' => 'input-block-level',
					'div' => 'span6'
				)) ?>
				<?= $this->Form->input('max_gt', array(
					'type' => 'text',
					'class' => 'input-block-level',
					'div' => 'span6',
				)) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>
	</div>
	<div class="span9">
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th class="left shrink"><?= $this->Paginator->sort('stage') ?></th>
					<th class="left shrink"><?= $this->Paginator->sort('due_date') ?></th>
					<th><?= $this->Paginator->sort('cust_acct_id', 'Customer Account') ?></th>
					<th class="center shrink"><?= $this->Paginator->sort('num_desks', 'Desks') ?></th>
					<th class="center shrink"><?= $this->Paginator->sort('num_offices', 'Offices') ?></th>
					<th class="center shrink"><?= $this->Paginator->sort('num_phone_lines', 'Phones') ?></th>
					<th class="center shrink"><?= $this->Paginator->sort('num_persons', 'Persons') ?></th>
					<th><?= $this->Paginator->sort('recurring_amount', 'Recurring') ?></th>
					<th><?= $this->Paginator->sort('grand_total') ?></th>
					<th class="right shrink"><?= $this->Paginator->sort('balance') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($billingAmounts as $billingAmount): ?>
				<tr>
					<td class="left shrink"><?= $this->Html->link($billingAmount['BillingAmount']['stage'], array('action' => 'view', $billingAmount['BillingAmount']['id'])) ?>&nbsp;</td>
					<td class="left shrink"><?= h($billingAmount['BillingAmount']['due_date']) ?>&nbsp;</td>
					<td class="stretch"><?= $this->Html->link($billingAmount['CustomerAccount']['name'], array('controller' => 'agingInteractions', 'action' => 'add', $billingAmount['CustomerAccount']['id'])) ?></td>
					<td class="center"><?= h($billingAmount['BillingAmount']['num_desks']) ?>&nbsp;</td>
					<td class="center"><?= h($billingAmount['BillingAmount']['num_offices']) ?>&nbsp;</td>
					<td class="center"><?= h($billingAmount['BillingAmount']['num_phone_lines']) ?>&nbsp;</td>
					<td class="center"><?= h($billingAmount['BillingAmount']['num_persons']) ?>&nbsp;</td>
					<td class="right shrink"><?= h($billingAmount['BillingAmount']['recurring_amount']) ?>&nbsp;</td>
					<td class="right shrink"><?= h($billingAmount['BillingAmount']['grand_total']) ?>&nbsp;</td>
					<td class="right shrink"><?= h($billingAmount['BillingAmount']['balance']) ?>&nbsp;</td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
	<?= $this->element('paging') ?>
</div>
<style>
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #ccc solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
</style>

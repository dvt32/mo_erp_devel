<div class="billingAmounts form">
<?php echo $this->Form->create('BillingAmount'); ?>
	<fieldset>
		<legend><?php echo __('Edit Billing Amount'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('recurring_amount');
		echo $this->Form->input('grand_total');
		echo $this->Form->input('modifier_id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('num_desks');
		echo $this->Form->input('num_offices');
		echo $this->Form->input('num_phone_lines');
		echo $this->Form->input('num_persons');
		echo $this->Form->input('balance');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BillingAmount.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('BillingAmount.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Billing Amounts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modifier'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

<legend><?= __('Accounts Receivable') ?></legend>
<ul class="nav nav-tabs">
	<li><?= $this->Html->link('Customer Account', array('controller' => 'customerAccounts', 'action' => 'receiving')) ?></li>
	<li class="active"><?= $this->Html->link('Billing Amount', '#') ?></li>
</ul>
<div class="row-fluid">
	<div class="span3">
		<?= $this->Form->create('BillingAmount', array('type' => 'get', 'inputDefaults' => array('class' => 'input-block-level'))) ?>
			<?= $this->Form->input('cust_acct_id', array(
				'options' => $customerAccounts,
				'class' => 'input-block-level chosen click-submit',
				'empty' => '',
				'label' => 'Customer Account'
			)) ?>
			<div class="row-fluid">
				<?php $billing_cycle_start_date = substr(cycle_start_dt(),0,10) ?>
				<?php $billing_cycle_end_date = substr(cycle_due_dt(),0,10) ?>
				<?= $this->Form->input('start_date', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6',
					'value' => $billing_cycle_start_date
				)) ?>
				<?= $this->Form->input('end_date', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6',
					'value' => $billing_cycle_end_date
				)) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>
	</div>
	<div class="span9">
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th class="stretch"><?= $this->Paginator->sort('cust_acct_id') ?></th>
					<th class="right"><?= $this->Paginator->sort('recurring_amount', 'Recurring') ?></th>
					<th class="right"><?= $this->Paginator->sort('grand_total', 'Total') ?></th>
					<th class="right"><?= $this->Paginator->sort('balance') ?></th>
					<th class=""><?= $this->Paginator->sort('due_date') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($billingAmounts as $billingAmount): ?>
				<tr>
					<td class="stretch"><?= $this->Html->link($billingAmount['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $billingAmount['CustomerAccount']['id'])) ?></td>
					<td class="right shrink"><?= h($billingAmount['BillingAmount']['recurring_amount']) ?>&nbsp;</td>
					<td class="right shrink"><?= h($billingAmount['BillingAmount']['grand_total']) ?>&nbsp;</td>
					<td class="right shrink"><?= h($billingAmount['BillingAmount']['balance']) ?>&nbsp;</td>
					<td class="left shrink nowrap">
						<?= h($billingAmount['BillingAmount']['due_date']) ?>
						<?= __("(%d days)", $billingAmount['BillingAmount']['days_underdue']) ?>
					</td>
					<td class="right shrink nowrap">
						<?= $this->Html->link('Add Payment', array(
							'controller' => 'receivedPayments',
							'action' => 'add',
							$billingAmount['BillingAmount']['cust_acct_id']
						)) ?>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
	<?= $this->element('paging') ?>
</div>

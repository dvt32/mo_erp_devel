<div class="billingAmounts view">
<h2><?= __('Billing Cycle Invoice'); ?></h2>
	<dl>
		<dt><?= __('Id'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['id']); ?> &nbsp; </dd>
		<dt><?= __('Recurring Amount'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['recurring_amount']); ?> &nbsp; </dd>
		<dt><?= __('Grand Total'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['grand_total']); ?> &nbsp; </dd>
		<dt><?= __('Created'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['created']); ?> &nbsp; </dd>
		<dt><?= __('Modified'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['modified']); ?> &nbsp; </dd>
		<dt><?= __('Modifier'); ?></dt>
		<dd> <?= $this->Html->link($billingAmount['Modifier']['full_name'], array('controller' => 'employees', 'action' => 'view', $billingAmount['Modifier']['id'])); ?> &nbsp; </dd>
		<dt><?= __('Customer Account'); ?></dt>
		<dd> <?= $this->Html->link($billingAmount['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $billingAmount['CustomerAccount']['id'])); ?> &nbsp; </dd>
		<dt><?= __('No Desks'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['num_desks']); ?> &nbsp; </dd>
		<dt><?= __('No Offices'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['num_offices']); ?> &nbsp; </dd>
		<dt><?= __('No Phone Lines'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['num_phone_lines']); ?> &nbsp; </dd>
		<dt><?= __('No Persons'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['num_persons']); ?> &nbsp; </dd>
		<dt><?= __('Balance'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['balance']); ?> &nbsp; </dd>
		<dt><?= __('Stage'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['stage']); ?> &nbsp; </dd>
		<dt><?= __('Due Date'); ?></dt>
		<dd> <?= h($billingAmount['BillingAmount']['due_date']); ?> &nbsp; </dd>
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<li><?= $this->Form->postLink(__('Delete Billing Amount'), array('action' => 'delete', $billingAmount['BillingAmount']['id']), null, __('Are you sure you want to delete # %s?', $billingAmount['BillingAmount']['id'])); ?> </li>
	</ul>
</div>

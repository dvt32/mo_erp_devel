<div class="row-fluid">
	<?php $backUrl = @$this->request->query('back') ?: array('controller' => 'customerAccounts', 'action' => 'billing') ?>
	<?= $this->Html->link('&larr;', $backUrl, array('escape' => false, 'class' => 'btn btn-info')) ?>
	<legend>
		<strong>
			<?= $customerAccount['CustomerAccount']['name'] ?>
		</strong>
	</legend>
</div>


<div class="row-fluid">

	<!-- LEFT HALF -->
	<div class="span6">

		<!-- CUSTOMER DETAILS -->
		<section class="module row-fluid">
			<div class="module-head">
				<b>Account Details</b>
			</div>
			<div class="module-body">
				<dl>
					<dt>Name</dt>
					<dd><?= $this->Html->link($customerAccount['CustomerAccount']['name'], array('controller' => 'customerAccounts', 'action' => 'view', $customerAccount['CustomerAccount']['id'])) ?>&nbsp;</dd>

					<dt>License Agreement</dt>
					<?php if (empty($customerAccount['LicenseAgreement'])): ?>
						<dd><span class="muted">No license agreement found.</span>&nbsp;</dd>
					<?php else: ?>
						<dd><?= $this->Html->link('License Agreement', array('controller' => 'licenseAgreements', 'action' => 'view', $customerAccount['LicenseAgreement']['id'])) ?>&nbsp;</dd>
					<?php endif ?>

					<dt>Primary Contact Name</dt>
					<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?>&nbsp;</dd>
					
					<dt>Primary Contact Email</dt>
					<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['email'], "mailto:{$customerAccount['PrimaryCustomer']['email']}") ?>&nbsp;</dd>
					
					<dt>Primary Contact Phone</dt>
					<dd><a><?= $customerAccount['PrimaryCustomer']['work_phone_number'] ?></a>&nbsp;</dd>
				</dl>
			</div>
		</section>

		<section class="module row-fluid">
			<div class="module-head">
				<b>Billing Amount</b>
			</div>
			<div class="module-body">
				<?= $this->Form->create('BillingAmount', array(
					'inputDefaults' => array(
						'class' => 'input-block-level'
					)
				)) ?>

				<!-- THIS BILLING CYCLE -->
				<div class="hide">
					<?= $this->Form->hidden('0.id') ?>
					<?= $this->Form->hidden('0.cust_acct_id', array('value' => $customerAccount['CustomerAccount']['id'])) ?>
				</div>
				<div class="row-fluid">
					<?= $this->Form->input('0.num_desks',        array('value' => @$num_desks, 'label' => 'Desks', 'div' => 'span3' )) ?>
					<?= $this->Form->input('0.num_offices',      array('value' => @$num_offices, 'label' => 'Offices', 'div' => 'span3' )) ?>
					<?= $this->Form->input('0.num_phone_lines',  array('value' => @$num_phone_lines, 'label' => 'Phone Lines', 'div' => 'span3' )) ?>
					<?= $this->Form->input('0.num_persons',      array('value' => @$num_persons, 'label' => 'Internet Lines', 'div' => 'span3' )) ?>
				</div>

				<input type="hidden" name="section" value="edit_billing_amounts" />
				<?= $this->Form->button('Save', array('class' => 'btn btn-info')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
	
	<!-- RIGHT HALF -->
	<div class="span6">

		<!-- NEW MEMO FORM -->
		<section class="module row-fluid">
			<div class="module-head">
				<b>Add Billing Memo</b>
			</div>
			<div class="module-body">
				<?= $this->Form->create('BillingMemo', array(
					'url' => Router::url($this->request->pass + array('?' => $this->request->query), true),
					'inputDefaults' => array(
						'class' => 'input-block-level',
						'label' => false
					)
				)) ?>
					<?= $this->Form->hidden('cust_acct_id', array('value' => $customerAccount['CustomerAccount']['id'])) ?>
					<input type="hidden" name="section" value="new_billing_memo" />

					<?= $this->Form->input('template_id', array('empty' => 'Other, please specify in the field below', 'class' => 'chosen input-block-level')) ?>
					<?= $this->Form->input('memo', array('rows' => '2', 'label' => false)) ?>

					<?= $this->Form->button('Add Billing Memo', array('class' => 'btn btn-primary')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>


		<!-- INBOX MEMOS -->
		<section class="module row-fluid">
			<div class="module-head">
				<b>Billing Memo Inbox</b>
			</div>
			<div class="module-body">
				<?php $memos = @$customerAccount['BillingMemo'] ?>
				<?php $employee_id = AuthComponent::user('id') ?>
				<?php if (!empty($memos)): ?>

					<?= $this->Form->create('BillingMemo') ?>
					<?php foreach ($memos as $i => $memo): ?>
						<?= __(
							"<p><strong>%s:</strong> (%s) <pre class=\"billing-memo\">%s</pre></p>",
							$memo['is_auto'] ? 'Auto' : $memo['Modifier']['full_name'],
							date(DATETIME_USA, strtotime($memo['created'])),
							h($memo['memo'])
						) ?>
					<?php endforeach ?>
						<input type="hidden" name="section" value="archive_billing_memos" />
						<?//= $this->Form->button('Archive', array('class' => 'btn btn-info', 'style' => 'margin-top:1.5em;')) ?>
					<?= $this->Form->end() ?>
				<?php else: ?>
					<p class="muted text-center">
						No "inbox" billing memos for this customer account
					</p>
				<?php endif ?>
			</div>
		</section>

		<!-- ARCHIVED MEMOS -->
		<section class="module row-fluid expandable">
			<div class="module-head clickable" title="Click to expand">
				<b>Archived Billing Memos</b>
			</div>
			<div class="module-body" style="display:none;">
				<?php if (!empty($archivedMemos)): ?>
					<?php foreach ($archivedMemos as $archivedMemo): ?>
						<?= __(
							"<p><strong>%s:</strong> (%s) <pre class=\"billing-memo\">%s</pre></p>",
							$archivedMemo['BillingMemo']['is_auto'] ? 'Auto' : $archivedMemo['Modifier']['full_name'],
							date(DATETIME_USA, strtotime($archivedMemo['BillingMemo']['created'])),
							h($archivedMemo['BillingMemo']['memo'])
						) ?>
					<?php endforeach ?>
				<?php else: ?>
					<p class="muted text-center">
						No archived billing memos found
					</p>
				<?php endif ?>
			</div>
		</section>
	</div>
</div>

<?php $base = Router::url(array('controller' => 'billingMemos', 'action' => 'getTemplate')) ?>
<script>
var templateUrl = "<?= $base ?>";
console.log(templateUrl);
$(function() {
	$('#BillingMemoTemplateId').change(function() {
		var x = templateUrl + '/' + $(this).val() + '.json';
		console.log(x);
		$.ajax({
			url: x,
		}).success(function(data, text, response) {
			$("#BillingMemoMemo").val(data.template);
		})
	})
});

// auto-expand
$(".expandable").each(function(i) {
	this.setAttribute("id","expandable-"+i);
	var section="#expandable-"+i;
	$(section+" div.module-head").addClass("clickable").click(function(){
		$(section+" div.module-body").toggle();
	});
});
</script>

<style>
	.well form {
		margin-bottom:0px;
	}
	.billing-memo {
		font-family:sans-serif;
		background:none;
	}
</style>


<!-- breadcrumbs and header -->
<div class="row-fluid">
	<ul class="breadcrumb">
		<li><a href="{{ operations_2g_urlprefix }}/employees/dashboard">Dashboard</a> <span class="divider"><i class="icon-angle-right"></i></span></li>
		<li><a href="{{ operations_2g_urlprefix }}/customerAccounts/invoicing">Invoice Changes</a> <span class="divider"><i class="icon-angle-right"></i></span></li>
		<li class="active"><?= $customerAccount['CustomerAccount']['name'] ?></li>
	</ul>
	<?php $backUrl = @$this->request->query('back') ?: array('controller' => 'customerAccounts', 'action' => 'billing') ?>
	<h2 style="display:inline">
		<?= $this->Html->link('&larr; Back', $backUrl, array('escape' => false, 'class' => 'btn btn-info')) ?>
		<?= $customerAccount['CustomerAccount']['name'] ?>
	</h2>
</div>

<div class="row-fluid">

	<div class="span6">
		
		<!-- new billing memo -->
		<div class="row-fluid">
			<div class="span12">
				<legend>Add New Billing memo</legend>
				<!-- <label for="BillingMemoTemplateId"><strong>Add New Billing Memo</strong></label> -->

				<div class="row-fluid">
					<div class="span5">
						<!-- new billing memo -->
						<label id="create-new-billing-memo-label" class="radio inline">
							<input id="create-new-billing-memo-radio" type="radio" name="memo_type" value="1" />
							<a id="create-new-billing-memo">Add Memo</a>
						</label>
					</div>
					<div class="span2">or</div>
					<div class="span5">
						<label id="create-new-promise-note-label" class="radio inline">
							<!-- new promise note -->
							<input id="create-new-promise-note-radio" type="radio" name="memo_type" value="0" />
							<a id="create-new-promise-note">Add Promise Note</a>
						</label>
					</div>
				</div>

				<div id="create-new-billing-memo-form" class="row-fluid" style="display:none">
					<?php $url = Router::url($this->request->pass + array('?' => $this->request->query), true) ?>
					<?= $this->Form->create('BillingMemo', array('url' => $url, 'inputDefaults' => array('class' => 'input-block-level', 'label' => false ) )) ?>
					<?= $this->Form->hidden('cust_acct_id', array('value' => $customerAccount['CustomerAccount']['id'])) ?>
					<input type="hidden" name="section" value="new_billing_memo" />
					<?= $this->Form->input('memo', array('rows' => '2', 'label' => false)) ?>
					<?= $this->Form->button('Add Memo', array('class' => 'btn btn-primary', 'memo')) ?>
					<?= $this->Form->end() ?>

				</div>
				<div id="create-new-promise-note-form" class="row-fluid" style="display:none">
					<?php $url = Router::url($this->request->pass + array('?' => $this->request->query), true) ?>
					<?= $this->Form->create('BillingMemo', array('url' => $url, 'inputDefaults' => array('class' => 'input-block-level', 'label' => false ) )) ?>
					<?= $this->Form->hidden('cust_acct_id', array('value' => $customerAccount['CustomerAccount']['id'])) ?>
					<input type="hidden" name="section" value="new_promise_note" />
					<?= $this->Form->input('memo', array('rows' => '2', 'label' => false)) ?>
					<?= $this->Form->input('hold_expires', array('type' => 'text', 'class' => 'date', 'label' => 'Hold Expires')) ?>
					<?= $this->Form->button('Add Promise Note', array('class' => 'btn btn-info', 'value' => 'promise')) ?>
					<?= $this->Form->end() ?>
				</div>

			</div>
		</div>

		<!-- inbox memos -->
		<legend>"Inbox" Memos</legend>
		<div class="row-fluid">
			<?php $memos = @$customerAccount['BillingMemo'] ?>
			<?php $employee_id = AuthComponent::user('id') ?>
			<div class="row-fluid">
				<?php if (!empty($memos)): ?>
				 <label><input type="checkbox" class="checkall" />Check All</label>
					<?= $this->Form->create('BillingMemo') ?>
						<?php foreach ($memos as $i => $memo): ?>
							<?php $checkbox = $this->Form->input("{$i}.id", array('value' => $memo['id'])) ?>
							<?php $checkbox.= $this->Form->input("{$i}.is_applied", array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'checkable')) ?>
							<?= __(
								"<label>%s <strong>%s:</strong> <span class=\"small\">(%s)</span> <pre class=\"billing-memo\">%s</pre></label>",
								$checkbox,
								$memo['Modifier']['full_name'],
								CakeTime::timeAgoInWords($memo['created']),
								h($memo['memo'])
								) ?>
						<?php endforeach ?>
						<input type="hidden" name="section" value="archive_billing_memos" />
						<?= $this->Form->button('<i class="icon-check"></i> Mark as Applied', array('class' => 'btn btn-small btn-info', 'style' => 'margin-top:1.5em;')) ?>
					<?= $this->Form->end() ?>
				<?php else: ?>
					<span class="muted">
						No "inbox" billing memos for this customer account
					</span>
				<?php endif ?>
			</div>
		</div>

	</div>

	<!-- customer details -->
	<div class="span6">

		<!-- EDIT BILLING AMOUNTS -->
		<div class="row-fluid">
			<?= $this->Form->create('BillingAmount', array('inputDefaults' => array('div' => 'span3', 'class' => 'input-block-level') )) ?>
				<?= $this->Form->hidden('0.id') ?>
				<?=	$this->Form->hidden('0.cust_acct_id', array('value' => $customerAccount['CustomerAccount']['id'])) ?>
				<input type="hidden" name="section" value="edit_billing_amounts" />
				<?//= $this->Form->hidden('section', array('value' => 'billing_amounts')) ?>
				<legend> Customer Details </legend>
				<dl>
					<dt>Name</dt>
					<dd><?= $this->Html->link($customerAccount['CustomerAccount']['name'], array('controller' => 'customerAccounts', 'action' => 'view', $customerAccount['CustomerAccount']['id'])) ?></dd>

					<dt>License Agreement</dt>
					<?php if (empty($customerAccount['LicenseAgreement'])): ?>
						<dd>There is no license agreement associated with this customer account!</dd>
					<?php else: ?>
						<dd><?= $this->Html->link('License Agreement', array('controller' => 'licenseAgreements', 'action' => 'view', $customerAccount['LicenseAgreement']['id'])) ?></dd>
					<?php endif ?>

					<dt>Primary Customer</dt>
					<dd>
						<div><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?></div>
						<div><?= $this->Html->link($customerAccount['PrimaryCustomer']['email'], "mailto:{$customerAccount['PrimaryCustomer']['email']}") ?></div>
						<div><?= $customerAccount['PrimaryCustomer']['work_phone_number'] ?></div>
					</dd>
				</dl>
				<legend>Last Month</legend>
				<table class="table">
					<tr>
						<th>No. Desks</th>
						<th>No. Offices</th>
						<th>No. Phone Lines</th>
						<th>No. Persons</th>
					</tr>
					<tr>
						<td><?= $previousBillingAmount['BillingAmount']['num_desks'] ?></td>
						<td><?= $previousBillingAmount['BillingAmount']['num_offices'] ?></td>
						<td><?= $previousBillingAmount['BillingAmount']['num_phone_lines'] ?></td>
						<td><?= $previousBillingAmount['BillingAmount']['num_persons'] ?></td>
					</tr>
				</table>

				<legend>This Month</legend>
				<div class="row-fluid">
					<?= $this->Form->input('0.num_desks',       array('label' => "No. Desks")) ?>
					<?= $this->Form->input('0.num_offices',     array('label' => "No. Offices")) ?>
					<?= $this->Form->input('0.num_phone_lines', array('label' => "No. Phone Lines")) ?>
					<?= $this->Form->input('0.num_persons',     array('label' => "No. Persons")) ?>
				</div>
				<div class="row-fluid">
					<?= $this->Form->input('0.recurring_amount') ?>
					<?= $this->Form->input('0.grand_total') ?>
					<?= $this->Form->input('0.balance', array('readonly')) ?>
				</div>
				<div class="pull-right">
					<?php $created = @$customerAccount['BillingAmount'][0]['created'] ?>
					<?php if (!empty($created)): ?>
						<?php $time = strtotime($created) ?>
						<?php $month_start = current_month('start','U') ?>
						<?= 'Last updated ' . CakeTime::timeAgoInWords($created) ?>
					<?php endif ?>
				</div>
				<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>


<?php $base = Router::url(array('controller' => 'billingMemos', 'action' => 'getTemplate')) ?>
<script>
	var templateUrl = "<?= $base ?>";

	if ($("#create-new-billing-memo-radio").attr('checked')) { showNewBillingMemoForm(); }
	else if ($("#create-new-promise-note-radio").attr('checked')) { shoeNewPromiseNoteForm(); }

	$(function() {
		$('#BillingMemoTemplateId').change(function() {
			var x = templateUrl + '/' + $(this).val() + '.json';
			console.log(x);
			$.ajax({
				url: x,
			}).success(function(data, text, response) {
				$("#BillingMemoMemo").val(data.template);
			})
		})

		// toggle billing memo vs promise note form
		$("#create-new-billing-memo-label").click(showNewBillingMemoForm);
		$("#create-new-promise-note-label").click(shoeNewPromiseNoteForm);
	})
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});
	function showNewBillingMemoForm() {
		$("#create-new-billing-memo-form").show();
		$("#create-new-promise-note-form").hide();
	}
	function shoeNewPromiseNoteForm() {
		$("#create-new-billing-memo-form").hide();
		$("#create-new-promise-note-form").show();
	}
</script>
<style>
	.well form {
		margin-bottom:0px;
	}
	.billing-memo {
		font-family:sans-serif;
		background:none;
	}
</style>

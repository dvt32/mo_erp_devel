<div class="billingMemos form">
<?php echo $this->Form->create('BillingMemo'); ?>
	<fieldset>
		<legend><?php echo __('Edit Billing Memo'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('modifier_id');
		echo $this->Form->input('memo');
		echo $this->Form->input('cust_acct_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BillingMemo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('BillingMemo.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Billing Memos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modifier'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="billingMemos index">
	<h2><?= __('Billing Memos'); ?></h2>
	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('id'); ?></th>
				<th><?= $this->Paginator->sort('modifier_id'); ?></th>
				<th><?= $this->Paginator->sort('memo'); ?></th>
				<th><?= $this->Paginator->sort('created'); ?></th>
				<th><?= $this->Paginator->sort('modified'); ?></th>
				<th><?= $this->Paginator->sort('cust_acct_id'); ?></th>
				<th class="actions"><?= __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($billingMemos as $billingMemo): ?>
			<tr>
				<td><?= h($billingMemo['BillingMemo']['id']); ?>&nbsp;</td>
				<td>
					<?= $this->Html->link($billingMemo['Modifier']['full_name'], array('controller' => 'employees', 'action' => 'view', $billingMemo['Modifier']['id'])); ?>
				</td>
				<td><?= h($billingMemo['BillingMemo']['memo']); ?>&nbsp;</td>
				<td><?= h($billingMemo['BillingMemo']['created']); ?>&nbsp;</td>
				<td><?= h($billingMemo['BillingMemo']['modified']); ?>&nbsp;</td>
				<td>
					<?= $this->Html->link($billingMemo['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $billingMemo['CustomerAccount']['id'])); ?>
				</td>
				<td class="actions">
					<?= $this->Html->link(__('View'), array('action' => 'view', $billingMemo['BillingMemo']['id'])); ?>
					<?= $this->Html->link(__('Edit'), array('action' => 'edit', $billingMemo['BillingMemo']['id'])); ?>
					<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $billingMemo['BillingMemo']['id']), null, __('Are you sure you want to delete # %s?', $billingMemo['BillingMemo']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<?= $this->element('paging') ?>
</div>

<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Billing Memo'), array('action' => 'add')); ?></li>
		<li><?= $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Modifier'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?= $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

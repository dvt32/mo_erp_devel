<!-- BREADCRUMBS -->
<div class="row-fluid">

	<ul class="breadcrumb">
		<li><a href="{{ operations_2g_urlprefix }}/employees/dashboard">Dashboard</a><span class="divider"><i class="icon-angle-right"></i></span></li>
		<li><a href="{{ operations_2g_urlprefix }}/customerAccounts/invoicing">Invoice Changes</a><span class="divider"><i class="icon-angle-right"></i></span></li>
		<li class="active"><?= $customerAccount['CustomerAccount']['name'] ?></li>
	</ul>

	<?php $backUrl = @$this->request->query('back') ?: array('controller' => 'customerAccounts', 'action' => 'billing') ?>
	<?= $this->Html->link('&larr;', $backUrl, array('escape' => false, 'class' => 'btn btn-small btn-info pull-left', 'style' => 'vertical-align:baseline')) ?>
	&nbsp;
	<h2>
		<?= $customerAccount['CustomerAccount']['name'] ?>
	</h2>

</div>

<div class="row-fluid">

	<!-- LEFT HALF -->
	<div class="span6">

		<!-- CUSTOMER DETAILS -->
		<section class="module row-fluid">
			<div class="module-head">
				<b>Account Details</b>
			</div>
			<div class="module-body">
				<dl>
					<dt>Name</dt>
					<dd><?= $this->Html->link($customerAccount['CustomerAccount']['name'], array('controller' => 'customerAccounts', 'action' => 'view', $customerAccount['CustomerAccount']['id'])) ?>&nbsp;</dd>

					<dt>License Agreement</dt>
					<?php if (empty($customerAccount['LicenseAgreement'])): ?>
						<dd><span class="muted">No license agreement found.</span>&nbsp;</dd>
					<?php else: ?>
						<dd><?= $this->Html->link('License Agreement', array('controller' => 'licenseAgreements', 'action' => 'view', $customerAccount['LicenseAgreement']['id'])) ?>&nbsp;</dd>
					<?php endif ?>

					<dt>Primary Contact Name</dt>
					<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?>&nbsp;</dd>
					
					<dt>Primary Contact Email</dt>
					<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['email'], "mailto:{$customerAccount['PrimaryCustomer']['email']}") ?>&nbsp;</dd>
					
					<dt>Primary Contact Phone</dt>
					<dd><a><?= $customerAccount['PrimaryCustomer']['work_phone_number'] ?></a>&nbsp;</dd>
				</dl>
			</div>
		</section>

		<section class="module row-fluid">
			<div class="module-head">
				<b>Billing Amount</b>
			</div>
			<div class="module-body">
				<?= $this->Form->create('BillingAmount', array(
					'inputDefaults' => array(
						'div' => false,
						'class' => 'input-block-level'
					)
				)) ?>

				<!-- PREVIOUS BILLING AMOUNT -->
				<fieldset class="well">
					<legend>Previous Billing Cycle (Reference)</legend>

					<?php if (empty($this->request->data['BillingAmount']['9'])): ?>
						<p class="text-center muted">
							No previous billing amount found
						</p>
					<?php else: ?>
					<div class="row-fluid">
						<?= $this->Form->input('9.num_desks',        array('readonly', 'label' => 'Desks', 'div' => 'span3' )) ?>
						<?= $this->Form->input('9.num_offices',      array('readonly', 'label' => 'Offices', 'div' => 'span3' )) ?>
						<?= $this->Form->input('9.num_phone_lines',  array('readonly', 'label' => 'Phone Lines', 'div' => 'span3' )) ?>
						<?= $this->Form->input('9.num_persons',      array('readonly', 'label' => 'Internet Lines', 'div' => 'span3' )) ?>
					</div>
					<div class="row-fluid">
						<?= $this->Form->input('9.recurring_amount', array('readonly', 'div' => 'span4')) ?>
						<?= $this->Form->input('9.grand_total',      array('readonly', 'div' => 'span4')) ?>
						<?= $this->Form->input('9.balance',          array('readonly', 'div' => 'span4')) ?>
					</div>
					<?php endif ?>
				</fieldset>

				<!-- THIS BILLING CYCLE -->
				<fieldset>
					<legend>Current Billing Cycle</legend>
					<div class="hide">
						<?= $this->Form->hidden('0.id') ?>
						<?= $this->Form->hidden('0.cust_acct_id', array('value' => $customerAccount['CustomerAccount']['id'])) ?>
					</div>
					<div class="row-fluid">
						<?= $this->Form->input('0.num_desks',        array('label' => 'Desks', 'div' => 'span3' )) ?>
						<?= $this->Form->input('0.num_offices',      array('label' => 'Offices', 'div' => 'span3' )) ?>
						<?= $this->Form->input('0.num_phone_lines',  array('label' => 'Phone Lines', 'div' => 'span3' )) ?>
						<?= $this->Form->input('0.num_persons',      array('label' => 'Internet Lines', 'div' => 'span3' )) ?>
					</div>
					<div class="row-fluid">
						<?= $this->Form->input('0.recurring_amount', array('div' => 'span4')) ?>
						<?= $this->Form->input('0.grand_total',      array('div' => 'span4')) ?>
						<?= $this->Form->input('0.balance',          array('div' => 'span4', 'readonly', 'placeholder' => 'Updates on save')) ?>
					</div>
				</fieldset>
				<input type="hidden" name="section" value="edit_billing_amounts" />
				<?= $this->Form->button('Save', array('class' => 'btn btn-info')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>

	</div>

	<!-- RIGHT HALF -->
	<div class="span6">

		<!-- NEW MEMO FORM -->
		<section class="module row-fluid">
			<div class="module-head">
				<b>Add Billing Memo</b>
			</div>
			<div class="module-body">
				<?= $this->Form->create('BillingMemo', array(
					'url' => Router::url($this->request->pass + array('?' => $this->request->query), true),
					'inputDefaults' => array(
						'class' => 'input-block-level',
						'label' => false
					)
				)) ?>
					<?= $this->Form->hidden('cust_acct_id', array('value' => $customerAccount['CustomerAccount']['id'])) ?>
					<input type="hidden" name="section" value="new_billing_memo" />

					<?= $this->Form->input('template_id', array('empty' => 'Other, please specify in the field below')) ?>
					<?= $this->Form->input('memo', array('rows' => '2', 'label' => false)) ?>

					<?= $this->Form->button('Add Billing Memo', array('class' => 'btn btn-primary')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>

		<!-- INBOX MEMOS -->
		<section class="module row-fluid">
			<div class="module-head">
				<b>Billing Memo Inbox</b>
			</div>
			<div class="module-body">
				<?php $memos = @$customerAccount['BillingMemo'] ?>
				<?php $employee_id = AuthComponent::user('id') ?>
				<?php if (!empty($memos)): ?>
					
					<label>
						<input type="checkbox" class="checkall" />
						Check All
					</label>

					<?= $this->Form->create('BillingMemo') ?>
					<?php foreach ($memos as $i => $memo): ?>
						<?php $checkbox = $this->Form->input("{$i}.id", array('value' => $memo['id'])) ?>
						<?php $checkbox.= $this->Form->input("{$i}.is_applied", array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'checkable')) ?>
						<?= __(
							"<label>%s <strong>%s:</strong> <span class=\"small\">(%s)</span> <pre class=\"billing-memo\">%s</pre></label>",
							$checkbox,
							$memo['Modifier']['full_name'],
							date(DATETIME_USA, strtotime($memo['created'])),
							h($memo['memo'])
						) ?>
					<?php endforeach ?>
						<input type="hidden" name="section" value="archive_billing_memos" />
						<?= $this->Form->button('Archive', array('class' => 'btn btn-info', 'style' => 'margin-top:1.5em;')) ?>
					<?= $this->Form->end() ?>
				<?php else: ?>
					<p class="muted text-center">
						No "inbox" billing memos for this customer account
					</p>
				<?php endif ?>
			</div>
		</section>

		<!-- ARCHIVED MEMOS -->
		<section class="module row-fluid expandable">
			<div class="module-head clickable" title="Click to expand">
				<b>Archived Billing Memos</b>
			</div>
			<div class="module-body" style="display:none;">
				<?php if (!empty($archivedMemos)): ?>
					<?php foreach ($archivedMemos as $archivedMemo): ?>
						<?= __(
							"<p><strong>%s:</strong> <span class=\"small\">(%s)</span><pre class=\"billing-memo\">%s</pre></p>",
							$archivedMemo['Modifier']['full_name'],
							date(DATETIME_USA, strtotime($archivedMemo['BillingMemo']['created'])),
							h($archivedMemo['BillingMemo']['memo'])
						) ?>
					<?php endforeach ?>
				<?php else: ?>
					<p class="muted text-center">
						No archived billing memos found
					</p>
				<?php endif ?>
			</div>
		</section>

	</div>

</div>
<style>
	legend {
		font-size:1.25em;
		border:none;
		width:auto;
		padding:0px 1ex;
		margin:0px!important;
	}
</style>
<style>
	.well form {
		margin-bottom:0px;
	}
	.billing-memo {
		font-family:sans-serif;
		background:none;
	}
</style>

<?php $base = Router::url(array('controller' => 'billingMemos', 'action' => 'getTemplate')) ?>
<script>
var templateUrl = "<?= $base ?>";
console.log(templateUrl);

$(function() {

	$('#BillingMemoTemplateId').change(function() {
		var x = templateUrl + '/' + $(this).val() + '.json';
		console.log(x);
		$.ajax({
			url: x,
		}).success(function(data, text, response) {
			$("#BillingMemoMemo").val(data.template);
		})
	})
	// checkall
	$('.checkall').click(function() {
		$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
	});

	// auto-expand
	$(".expandable").each(function(i) {
		this.setAttribute("id","expandable-"+i);
		var section="#expandable-"+i;
		$(section+" div.module-head").addClass("clickable").click(function(){
			$(section+" div.module-body").toggle();
		});
	});


	if (! $("#BillingAmount0GrandTotal").val()) {
		$("#BillingAmount0GrandTotal").val($("#BillingAmount0RecurringAmount").val());
	}
	$("#BillingAmount0Balance").val($("#BillingAmount0GrandTotal").val());
	
	$("#BillingAmount0GrandTotal").keyup(function() {
		$("#BillingAmount0Balance").val($("#BillingAmount0GrandTotal").val());
	});
});
</script>

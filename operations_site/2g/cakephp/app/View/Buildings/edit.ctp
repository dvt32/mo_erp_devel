<div class="buildings form">
<?php echo $this->Form->create('Building'); ?>
	<fieldset>
		<legend><?php echo __('Edit Building'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('address_1');
		echo $this->Form->input('address_2');
		echo $this->Form->input('city');
		echo $this->Form->input('state');
		echo $this->Form->input('zip_code');
		echo $this->Form->input('country');
		echo $this->Form->input('is_active');
		echo $this->Form->input('note');
		echo $this->Form->input('short_global_label');
		echo $this->Form->input('longitude');
		echo $this->Form->input('latitude');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-primary')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Building.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Building.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Notifications'), array('controller' => 'notifications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Notification'), array('controller' => 'notifications', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>

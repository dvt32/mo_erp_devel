<h2>
	<?= $this->Html->image('icons/flat_miniset/png/256/companies.png', array('style' => 'height: 3em;')) ?>
	<?php echo __('Buildings'); ?>
</h2>
<!-- search & filter forms -->
<div class="row-fluid">
		<legend>Filter &amp; Search</legend>
		<!-- filter and search -->
		<?= $this->Form->create('Building', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
		<div class="span8">

				<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge search-query')) ?>
				<?= $this->Form->input('type', array(
					'options' => $types, 
					'empty' => '-- Building Type --',
					'class' => 'input-auto',
					'style' => 'margin-bottom:0px;')
				) ?>

			<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
			<?= $this->Html->link('Clear', array('action' => 'index')) ?>
		</div>
		<?= $this->Form->end() ?>
</div>
<!-- /search & filter forms -->

<div class="buildings index">
	<br>
	<!-- Table -->
	<div class="row-fluid">
		<div class="span12">
			<table class="table table-condensed table-striped">
				<tr>
					<th>Address</th>
					<th><?= $this->Paginator->sort('type'); ?></th>
					<th><?= $this->Paginator->sort('short_global_label'); ?></th>
					<th class="actions"><?= __('Actions'); ?></th>
				</tr>
				<?php foreach ($buildings as $building): ?>
				<tr>
					<td class="address">
						<a href="{{ operations_2g_urlprefix }}/buildings/view/<?= $building['Building']['id']; ?>">
							<address>
								<?= h($building['Building']['address_1']); ?><br/>
								<?php if (isset($building['Building']['address_2'])) { echo $building['Building']['address_2']; } ?>
								<?= $building['Building']['city'].", ".$building['Building']['state']." ".$building['Building']['zip_code'] ?><br/>
								<?= h($building['Building']['country']); ?><br/>
								<br/>
							</address>
						</a>
					</td>
					<td><?= h($building['Building']['type']); ?>&nbsp;</td>
					<td class="short_global_label"><?= h($building['Building']['short_global_label']); ?>&nbsp;</td>
					<td class="actions">
						<?= $this->Html->link(__('View'), array('action' => 'view', $building['Building']['id'])); ?>
						<?= $this->Html->link(__('Floors'), array('controller' => 'floors', 'action' => 'building', $building['Building']['id'])); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
			<p>
			<?= $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>	
			</p>
			<div class="paging">
				<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
			</div>
		</div>
	</div>
	<!-- /Table -->
</div>

<style>
	.highlight { background-color: yellow }
</style>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<script type="text/javascript">

	$("td.address, td.note, td.short_global_label").highlight(QueryString.q);

</script>

<?php
/**
 *
 *
 * @package    MicroOffice
 * @subpackage MicroOffice.NetworkMontiro
 * @category   view
 */

/* @var $this View */
?>

<script type="text/javascript">
    $(function(){
        $('#server-list li .status').each(function(index, element){
            var address = $(element).data('address');
            $.get('{{ operations_2g_urlprefix }}/buildings/ping/'+address,
                function(data, textStatus, jqXHR) {
                    $(element).html(data);
                }
                );
        });
    });
</script>

<style type="text/css">
    #server-list .name {width: 150px; display: inline-block;}
    .status span {padding: 0 10px;}
    .status .down {background: pink;}
    .status .OK {background: lime;}

</style>

<div class="row-fluid">
    <div class="span12">

        <div class="row-fluid">

            <div class="span4 offset4 centered">
                <?= $this->Html->image('icons/flat_miniset/png/256/antenna2.png', array('style' => 'height:10em;')) ?>
                <h1>Networks</h1>
                <ul id="server-list" class="nav nav-pills nav-stacked">
                    <?php foreach ($servers as $server) : ?>
                    <li class="building">
                        <span class="name"><?=$server['Building']['full_address']?></span>
                        <span class="status" data-address="<?= $server['Building']['server1']?>">pending...</span>
                        <span class="status" data-address="<?= $server['Building']['server2']?>">pending...</span>
                    </li>
                <?php endforeach ?>
            </ul>            

        </div>

    </div>

</div>
</div>

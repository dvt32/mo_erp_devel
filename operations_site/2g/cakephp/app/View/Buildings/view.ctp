<style type="text/css">
	
	#map-canvas, #map_canvas {
	  height: 100%;
	}

	@media print {

	  #map-canvas, #map_canvas {
	    height: 650px;
	  }
	}

	#panel {
	  position: absolute;
	  top: 5px;
	  left: 50%;
	  margin-left: -180px;
	  z-index: 5;
	  background-color: #fff;
	  padding: 5px;
	  border: 1px solid #999;

	}

</style>
<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="{{ operations_2g_urlprefix }}/buildings"><?=  $building['Building']['city'] ?></a> <span class="divider">/</span></li>
			<li class="active"><?=  $building['Building']['address_1'] ?></li>
		</ul>
	</div>
</div>
<div class="buildings view">
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				<div class="span6">
					<div id="map-canvas" style="height:650px;"></div>
					<br/>
				</div>
				<div class="span6">
					<h2>
						<?= $this->Html->image('icons/flat_miniset/png/256/company.png', array('style' => 'height: 3em;')) ?>
					</h2>
					<p>
						<address class="lead">
							<?= h($building['Building']['address_1']); ?><br/>
							<?php if(isset($building['Building']['address_2'])) { echo $building['Building']['address_2'].'<br/>'; } ?>
							<?= $building['Building']['city'].", ".$building['Building']['state']." ".$building['Building']['zip_code'] ?><br/>
							<?= h($building['Building']['country']); ?><br/>
						</address>
						&nbsp;
					</p>
				</div>
				<div class="span6 details" style="display:none;">
					<dl>
						<dt><?= __('Is Active'); ?></dt>
						<dd>
							<?= h($building['Building']['is_active']); ?>
							&nbsp;
						</dd>
						<dt><?= __('Note'); ?></dt>
						<dd>
							<?= h($building['Building']['note']); ?>
							&nbsp;
						</dd>
						<dt><?= __('Created On'); ?></dt>
						<dd>
							<?= h($building['Building']['creation_dt']); ?>
							&nbsp;
						</dd>
						<dt><?= __('Short Global Label'); ?></dt>
						<dd>
							<?= h($building['Building']['short_global_label']); ?>
							&nbsp;
						</dd>
						<dt><?= __('Longitude'); ?></dt>
						<dd>
							<?= h($building['Building']['longitude']); ?>
							&nbsp;
						</dd>
						<dt><?= __('Latitude'); ?></dt>
						<dd>
							<?= h($building['Building']['latitude']); ?>
							&nbsp;
						</dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?= $this->Html->link(__('Edit Building'), array('action' => 'edit', $building['Building']['id'])); ?> </li>
		<li class="active"><?= $this->Form->postLink(__('Delete Building'), array('action' => 'delete', $building['Building']['id']), null, __('Are you sure you want to delete # %s?', $building['Building']['id'])); ?> </li>
		<li class="active"><?= $this->Html->link(__('List Buildings'), array('action' => 'index')); ?> </li>
		<li><a href="#/details" id="details">Building Details</a></li>
		<li><a href="#/notifications" id="notifications">Building Notifications</a></li>
	</ul>
</div>
<div class="related notifications" style="display:none;">
	<h3><?= __('Building Notifications'); ?></h3>
	<?php if (!empty($building['Notification'])): ?>
		<table class="table table-condensed">
			<tr>
				<th><?= __('Name'); ?></th>
				<th><?= __('Description'); ?></th>
				<th><?= __('Room Id'); ?></th>
				<th><?= __('Floor Id'); ?></th>
				<th><?= __('Created'); ?></th>
				<th><?= __('Modified'); ?></th>
				<th><?= __('Is Active'); ?></th>
				<th class="actions"><?= __('Actions'); ?></th>
			</tr>
			<?php foreach ($building['Notification'] as $notification): ?>
				<tr>
					<td><?= $notification['name']; ?></td>
					<td><?= $notification['description']; ?></td>
					<td><?= $notification['room_id']; ?></td>
					<td><?= $notification['floor_id']; ?></td>
					<td><?= $notification['created']; ?></td>
					<td><?= $notification['modified']; ?></td>
					<td><?= $notification['is_active']; ?></td>
					<td class="actions">
						<?= $this->Html->link(__('View'), array('controller' => 'notifications', 'action' => 'view', $notification['id'])); ?>
						<?= $this->Html->link(__('Edit'), array('controller' => 'notifications', 'action' => 'edit', $notification['id'])); ?>
						<?= $this->Form->postLink(__('Delete'), array('controller' => 'notifications', 'action' => 'delete', $notification['id']), null, __('Are you sure you want to delete # %s?', $notification['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<?php else: ?>
		<div class="hero-unit">
			<h1>No Notifications</h1>
		</div>
		<?php endif; ?>

	<div class="actions">
		<ul class="nav nav-pills">
			<li><?= $this->Html->link(__('New Notification'), array('controller' => 'notifications', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?= __('Floors in this Building'); ?></h3>
	<?php if (!empty($building['Floor'])): ?>
	<table class="table table-condensed">
		<tr>
			<th><?= __('Label'); ?></th>
			<th><?= __('Is Active'); ?></th>
			<th><?= __('Note'); ?></th>
			<th><?= __('Created'); ?></th>
			<th><?= __('Short Global Label'); ?></th>
			<th><?= __('Short Local Label'); ?></th>
			<th><?= __('Office Manager'); ?></th>
		</tr>
		<?php foreach ($building['Floor'] as $floor): ?>
			<tr>
				<td>
					<?= $this->Html->link($floor['label'], array('controller' => 'floors', 'action' => 'view', $floor['id'])); ?>
					<td><?= $floor['is_active']; ?></td>
					<td><?= $floor['note']; ?></td>
					<td><?= $floor['creation_dt']; ?></td>
					<td><?= $floor['short_global_label']; ?></td>
					<td><?= $floor['short_local_label']; ?></td>
					<td><?= $floor['OfficeManager']['full_name']; ?></td>
				</tr>
		<?php endforeach; ?>
	</table>
	<?php else: ?>
	<div class="hero-unit">
		<h1>No Floors</h1>
	</div>
	<?php endif; ?>

</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">


	var lat = <?= h($building['Building']['latitude']); ?>;
	var lng = <?= h($building['Building']['longitude']); ?>;


	function initialize() {

	  var myLatlng = new google.maps.LatLng(lat,lng);
	  var mapOptions = {
	    zoom: 15,
	    center: myLatlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  }
	  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	  var marker = new google.maps.Marker({
	      position: myLatlng,
	      map: map,
	      title: 'Hello World!'
	  });

	}

	google.maps.event.addDomListener(window, 'load', initialize);

	compact('details', 'toggle');
	compact('notifications', 'toggle');
</script>

<div class="cameras form">
<?php echo $this->Form->create('Camera'); ?>
	<fieldset>
		<legend><?php echo __('Add Camera'); ?></legend>
	<?php
		echo $this->Form->input('mac_address');
		echo $this->Form->input('model');
		echo $this->Form->input('version');
		echo $this->Form->input('lan_ip_addr');
		echo $this->Form->input('lan_port');
		echo $this->Form->input('username_admin');
		echo $this->Form->input('password_admin');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('floor_id');
		echo $this->Form->input('description');
		echo $this->Form->input('last_recorded_date');
		echo $this->Form->input('do_date_monitoring');
		echo $this->Form->input('storage_server');
		echo $this->Form->input('storage_base_path');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-primary')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Cameras'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="cameras index">
	<h2><?php echo __('Cameras'); ?></h2>
</div>
<table class="table table-condensed">
	<tr>
		<th><?php echo $this->Paginator->sort('description'); ?></th>
		<th><?php echo $this->Paginator->sort('lan_ip_addr', 'IP Address'); ?></th>
		<th><?php echo $this->Paginator->sort('lan_port', 'Port'); ?></th>
		<th><?php echo $this->Paginator->sort('creation_dt', 'Created'); ?></th>
		<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
		<th><?php echo $this->Paginator->sort('last_recorded_date'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cameras as $camera): ?>
	<tr>
		<td><?php echo h($camera['Camera']['description']); ?>&nbsp;</td>
		<td><?php echo h($camera['Camera']['lan_ip_addr']); ?>&nbsp;</td>
		<td><?php echo h($camera['Camera']['lan_port']); ?>&nbsp;</td>
		<td><?php echo h($camera['Camera']['creation_dt']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($camera['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $camera['Floor']['id'])); ?>
		</td>
		<td><?php echo h($camera['Camera']['last_recorded_date']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $camera['Camera']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $camera['Camera']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $camera['Camera']['id']), null, __('Are you sure you want to delete # %s?', $camera['Camera']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	
	</p>
	<div class="paging">
		<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>


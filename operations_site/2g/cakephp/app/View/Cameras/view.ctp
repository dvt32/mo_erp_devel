<div class="cameras view">
<h2><?php echo __('Camera'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mac Address'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['mac_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Version'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['version']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lan Ip Addr'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['lan_ip_addr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lan Port'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['lan_port']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username Admin'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['username_admin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password Admin'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['password_admin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($camera['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $camera['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Recorded Date'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['last_recorded_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Do Date Monitoring'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['do_date_monitoring']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Storage Server'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['storage_server']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Storage Base Path'); ?></dt>
		<dd>
			<?php echo h($camera['Camera']['storage_base_path']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Camera'), array('action' => 'edit', $camera['Camera']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Camera'), array('action' => 'delete', $camera['Camera']['id']), null, __('Are you sure you want to delete # %s?', $camera['Camera']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cameras'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Camera'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

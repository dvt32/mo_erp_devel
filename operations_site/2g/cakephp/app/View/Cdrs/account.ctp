<div class="cdrs index">
	<h2><?php echo __('Call Logs'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('src_num', 'Call From'); ?></th>
			<th><?php echo $this->Paginator->sort('dest_num', 'Call To'); ?></th>
			<th><?php echo $this->Paginator->sort('duration'); ?></th>
			<th><?php echo $this->Paginator->sort('start_stamp'); ?></th>
			<th><?php echo $this->Paginator->sort('end_stamp'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cdrs as $cdr): ?>
	<tr>
		<td><?php echo h($cdr['Cdr']['src_num']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['dest_num']); ?>&nbsp;</td>
		<td><?php echo h($cdr['Cdr']['duration']); ?> seconds</td>
		<td><?php echo CakeTime::niceShort($cdr['Cdr']['start_stamp']); ?>&nbsp;</td>
		<td><?php echo CakeTime::niceShort($cdr['Cdr']['end_stamp']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $cdr['Cdr']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cdr['Cdr']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cdr['Cdr']['id']), null, __('Are you sure you want to delete # %s?', $cdr['Cdr']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

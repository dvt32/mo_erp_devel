<div class="cdrs form">
<?php echo $this->Form->create('Cdr'); ?>
	<fieldset>
		<legend><?php echo __('Edit Cdr'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('account_id', array(
			'options' => $customerAccounts
		));
		echo $this->Form->input('account_name');
		echo $this->Form->input('src_num');
		echo $this->Form->input('dest_num');
		echo $this->Form->input('duration');
		echo $this->Form->input('billsec');
		echo $this->Form->input('ef_billsec');
		echo $this->Form->input('ef_billmin');
		echo $this->Form->input('ef_rate');
		echo $this->Form->input('cost');
		echo $this->Form->input('start_stamp');
		echo $this->Form->input('answer_stamp');
		echo $this->Form->input('end_stamp');
		echo $this->Form->input('xml_cdr');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Cdr.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Cdr.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Cdrs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

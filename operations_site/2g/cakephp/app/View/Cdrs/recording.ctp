<?php //debug($cdrs); ?>
<div class="cdrs index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/radio_microphone.png' , array('style' => 'height:3em;')) ?>
		<?= __('Call Logs'); ?>
	</h2>
	<?= $this->Form->create('Cdr', array('type' => 'get', 'class' => 'form-search')) ?>
		<div class="input-append">
			<input name="q" placeholder="Search by phone number" class="span10 search-query" style="min-height:41px!important;" type="text" id="CdrQ">
			<button class="btn btn-success" type="submit">Search</button>
		</div>
	<?= $this->Form->end() ?>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('src_num', 'Call From'); ?></th>
			<th><?= $this->Paginator->sort('dest_num', 'Call To'); ?></th>
			<th><?= $this->Paginator->sort('duration'); ?></th>
			<th><?= $this->Paginator->sort('start_stamp', 'Called On'); ?></th>
			<!-- <th class="actions"><?= __('Recordings'); ?></th> -->
	</tr>
	<?php foreach ($cdrs as $cdr): ?>
	<tr>
		<td><?= h($cdr['Cdr']['src_num']); ?>&nbsp;</td>
		<td><?= h($cdr['Cdr']['dest_num']); ?>&nbsp;</td>
		<td><?= h($cdr['Cdr']['duration']); ?> minutes</td>
        <td><?= CakeTime::nice($cdr['Cdr']['start_stamp']); ?>&nbsp;</td>
        <!-- <td><?= $this->Html->image('recording.jpg', array(
                'style' => 'height:20px;',
                'url' => array(
                        'controller' => 'Cdrs',
                        'action' => 'download',
                        $cdr['Cdr']['id'])
                        )); ?></a></td> -->
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

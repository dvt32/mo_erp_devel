<div class="cdrs view">
<h2><?php echo __('Cdr'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cdr['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $cdr['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Account Name'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['account_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src Num'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['src_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dest Num'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['dest_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billsec'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['billsec']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ef Billsec'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['ef_billsec']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ef Billmin'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['ef_billmin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ef Rate'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['ef_rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Stamp'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['start_stamp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer Stamp'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['answer_stamp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Stamp'); ?></dt>
		<dd>
			<?php echo h($cdr['Cdr']['end_stamp']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cdr'), array('action' => 'edit', $cdr['Cdr']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cdr'), array('action' => 'delete', $cdr['Cdr']['id']), null, __('Are you sure you want to delete # %s?', $cdr['Cdr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cdrs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cdr'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

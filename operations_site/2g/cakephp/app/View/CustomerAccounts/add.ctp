<div class="row-fluid">
	<div class="span8 offset2">
		<section class="module">
			<div class="module-head">
				<b>New Company</b>
			</div>
			<div class="module-body">
				<?= $this->Form->create('CustomerAccount', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
					<?= $this->Form->hidden('CustomerAccount.is_active', array('value' => true)) ?>
					<div class="row-fluid">
						<div class="span6">
							<?= $this->Form->input('CustomerAccount.name') // internal ?>
						</div>
						<div class="span6">
							<?= $this->Form->input('CustomerAccount.external_name') // external ?>
						</div>
					</div>
					<?= $this->Form->input('CustomerAccount.description') // external ?>
					<?= $this->Form->input('CustomerAccount.note', array('placeholder' => 'For internal use only')) // internal ?>
					<?= $this->Form->input('CustomerAccount.type') // customer or vendor ?>
					<?= $this->Form->input('CustomerAccount.room_minutes', array('label' => 'Conference Room Minutes', 'options' => $room_minutes, 'empty' => '')) // per month ?>
					<?= $this->Form->input('CustomerAccount.visible', array('checked')) // visible in tenant directory ?>
					<?= $this->Form->input('CustomerAccount.service_id') // regular, virtual, therapy, etc.? ?>
					<?= $this->Form->input('CustomerAccount.primary_floor_id', array('label' => 'Primary Floor', 'options' => $floors, 'empty' => '')) // per month ?>
					
					<div class="hide">
						<?= $this->Form->input('CustomerAccount.web_site') ?>
						<?= $this->Form->input('CustomerAccount.tax_id') ?>
						<?= $this->Form->input('CustomerAccount.preferred_payment_method') ?>
						<?= $this->Form->input('CustomerAccount.pkg_notify_email') ?>
						<?= $this->Form->input('CustomerAccount.logo_url') ?>
						<?= $this->Form->input('CustomerAccount.status') ?>
					</div>
					<?= $this->Form->button('Save', array('class' => 'btn')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
</div>


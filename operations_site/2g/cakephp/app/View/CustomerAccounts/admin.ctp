<h1>Billing Cycle Workflow</h1>
<!-- progress bars -->
<div class="row-fluid">
	<div class="span12">
		<section class="module">
			<div class="module-head">
				<b>Billing Cycle Workflow</b>
			</div><!--/.module-head-->
			<div class="module-body">
				<div class="row-fluid">
					<div class="span12">
						<?php $arp = ($counts['billing']['reviewed']/$counts['total'])*100 ?>
						<p class="muted">
							<?= $this->Html->link('Accounts Reviewed', array('action' => 'billing')) ?>
							 - <?= round($arp, 0) ?>%
						</p>
						<div class="progress progress">
							<div class="bar" style="width: <?= $arp ?>%;"></div>
						</div>
						<?php $aap = ($counts['invoicing']['reviewed']/($counts['invoicing']['not_reviewed'] + $counts['invoicing']['reviewed']))*100 ?>
						<p class="muted">
							<?= $this->Html->link('Account Adjustments', array('action' => 'invoicing')) ?>
							 - <?= round($aap, 0) ?>%
						</p>
						<div class="progress progress-warning">
							<div class="bar" style="width: <?= $aap ?>%;"></div>
						</div>
						<?php $arep = ($counts['receiving']['received']/$counts['total'])*100 ?>
						<p class="muted">
							<?= $this->Html->link('Accounts Receivables', array('action' => 'receiving')) ?>
							 - <?= round($arep, 0) ?>%
						</p>
						<div class="progress progress-success">
							<div class="bar" style="width: <?= $arep ?>%;"></div>
						</div>
						<?php if ($counts['aging']['total'] != 0) : ?>
						<?php $aagp = ($counts['aging']['total']/$counts['aging']['total'])*100 ?>
						<p class="muted">
							<?= $this->Html->link('Accounts Aging', array('action' => 'aging')) ?>
							 - <?= round($aagp, 0) ?>%
						</p>
						<div class="progress progress-danger">
							<div class="bar" style="width: <?= $aagp ?>%;"></div>
						</div>
						<?php else : ?>
						<p class="muted">
							<?= $this->Html->link('Accounts Aging', array('action' => 'aging')) ?>
							 - 100%
						</p>
						<div class="progress progress-danger">
							<div class="bar" style="width: 100%;"></div>
						</div>
						<?php endif ?>
					</div>

					<div class="stats-overview row-fluid">
						<div class="span3">
							<section class="stats">
								<p class="muted">Reviewing Accounts</p>
								<h3 class="text-blue">
									<?= $counts['billing']['reviewed']." / ".$counts['total']?>
								</h3>
							</section><!--/.module-->
						</div>
						<div class="span3">
							<section class="stats">
								<p class="muted">Adjusting Accounts</p>
								<h3 class="text-orange">
									<?= $counts['invoicing']['reviewed']." / ".($counts['invoicing']['not_reviewed'] + $counts['invoicing']['reviewed']) ?>
								</h3>
							</section><!--/.module-->
						</div>
						<div class="span3">
							<section class="stats">
								<p class="muted">Receiving Invoices</p>
								<h3 class="text-green">
									<?= $counts['aging']['total']." / ".$counts['aging']['total'] ?>
								</h3>
							</section><!--/.module-->
						</div>
						<div class="span3">
							<section class="stats">
								<p class="muted">Aging Invoices</p>
								<h3 class="text-red">
									<?= $counts['aging']['total']." / ".$counts['aging']['total'] ?>
								</h3>
							</section><!--/.module-->
						</div>
					</div>
				</div>
			</div><!--/.module-body-->
		</section>
	</div>
</div>
<!-- /progress bars -->

<!-- graph -->
<div class="module">
	<div class="module-head">
		<b>Billed vs Received</b>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid">
			<div id="placeholder2" class="chart"></div>
		</div>
	</div>
</div>
<!-- /graph -->

<style>
	.highlight { background-color: yellow }
	.shrink { width:0px; }
	.stretch { width:auto; }
	.nowrap { white-space:nowrap }
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #999 solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
</style>

<?php $query = @$this->request->query['q'] ?: null ?>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<!-- FLOT CHART -->
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.js"></script>
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.pie.js"></script>
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.resize.js"></script>
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/mixitup/jquery.mixitup.min.js"></script>

<!-- SCRIPTS: FLOT GRAPH ADVANCE -->
<script type="text/javascript">
$(function () {

	// Month Billed
	var d1 = <?= json_encode($graph['received']) ?>;



	// Month Received
	var d2 = <?= json_encode($graph['billed']) ?>;

	console.log(d1);

	var plot = $.plot($("#placeholder2"),
		   [ { data: d1, label: "Received"}, { data: d2, label: "Billed" } ], {
				lines: { 
					show: true, 
					fill: true, /*SWITCHED*/
					lineWidth: 2 
				},
				points: { 
					show: true, 
					lineWidth: 3
				},
				grid: {
					clickable: true,
					hoverable: true,
					autoHighlight: true,
					mouseActiveRadius: 10,
					aboveData: true,
					backgroundColor: "#fafafa",
					borderWidth: 0,
					minBorderMargin: 25,
				},
				colors: [ "#090", "#099",  "#609", "#900"],
				shadowSize: 0
			 });

	function showTooltip(x, y, contents) {
		$('<div id="gridtip">' + contents + '</div>').css( {
			position: 'absolute',
			display: 'none',
			top: y + 5,
			left: x + 5
		}).appendTo("body").fadeIn(300);
	}

	var previousPoint = null;
	$("#placeholder2").bind("plothover", function (event, pos, item) {
		$("#x").text(pos.x.toFixed(2));
		$("#y").text(pos.y.toFixed(2));

		if (item) {
			if (previousPoint != item.dataIndex) {
				previousPoint = item.dataIndex;
				
				$("#gridtip").remove();
				var x = item.datapoint[0].toFixed(0),
					y = item.datapoint[1].toFixed(0);
				
				showTooltip(item.pageX, item.pageY,
							"month : " + x + "&nbsp;&nbsp;&nbsp; number of bills : " + y);
			}
		}
		else {
			$("#gridtip").remove();
			previousPoint = null;            
		}
	});
});
</script>

<script>
	var query = "<?= $query ?>";
	$("td.searchable").highlight(query);

	// checkall
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});



</script>



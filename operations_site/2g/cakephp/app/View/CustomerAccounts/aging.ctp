<legend><?= __('Aged Receivables') ?></legend>
<ul class="nav nav-tabs">
	<li class="active"><?= $this->Html->link('Customer Account', '#') ?></li>
	<li><?= $this->Html->link('Billing Amount', array('controller' => 'billingAmounts', 'action' => 'aging')) ?></li>
</ul>
<div class="row-fluid">
	<div class="span3">

		<!-- search & filter forms -->
		<?= $this->Form->create('CustomerAccount', array(
			'type' => 'GET',
			'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
					'required' => false
			)))
			?>
			<div class="controls-row">
				<?= $this->Form->input('id', array('options' => $cust_accts, 'class' => 'input-block-level chosen click-submit', 'empty' => '', 'label' => 'Customer Account')) ?>
				<?= $this->Form->input('service_id', array('multiple' => 'checkbox')) ?>
				<?= $this->Form->hidden('type', array('value' => 'customer')) ?>
				<?= $this->Form->input('primary_floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>

	</div>
	<div class="span9">
		<?php if (!empty($customerAccounts)) : ?>
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="shrink nowrap"><?= $this->Paginator->sort('aging_followup_date', 'Followup') ?></th>
						<th><?= $this->Paginator->sort('name') ?></th>
						<th class="shrink nowrap"><?= $this->Paginator->sort('days_overdue','Overdue') ?></th>
						<th class="shrink nowrap"><?= $this->Paginator->sort('aging_count','Aging Count') ?></th>
						<th class="shrink nowrap"><?= $this->Paginator->sort('balance') ?></th>
						<th class="">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
				<?php $sum = $accts = 0 ?>
				<?php foreach ($customerAccounts as $i => $acct): ?>
					<tr>

						<!-- follow up on -->
						<td>
							<?php if (!empty($acct['CustomerAccount']['aging_followup_date'])): ?>
								<?= @date(DATE_FORMAT_USA, strtotime($acct['CustomerAccount']['aging_followup_date'])) ?>
							<?php else: ?>
								<span class="muted">none</span>
							<?php endif ?>
							&nbsp;
						</td>

						<!-- company name -->
						<td class="searchable shrink" style="width:auto;">
							<?= $this->Html->link($acct['CustomerAccount']['name'], array(
								'controller' => 'agingInteractions',
								'action' => 'add',
								$acct['CustomerAccount']['id']
							)) ?>
							<?//= $this->Html->link(, array('action' => 'view', $acct['CustomerAccount']['id'])) ?>
						</td>

						<!-- days overdue -->
						<td><?= @$acct['CustomerAccount']['days_overdue'] ?> days</td>

						<!-- customerAccount.aging_count -->
						<td class="shrink nowrap">
							<?= $this->Html->link(
								@$acct['CustomerAccount']['aging_count'] . ' invoice',
								array(
									'controller' => 'billingAmounts',
									'action' => 'aging',
									'?' => array('cust_acct_id' => $acct['CustomerAccount']['id'])
								)
							) ?>
						</td>

						<!-- customerAccount.balance -->
						<td class="right"><?= @$acct['CustomerAccount']['balance'] ?></td>

						<!-- add payment button -->
						<td class="shrink nowrap">

							<?= $this->Html->link('+Payment', array(
								'controller' => 'receivedPayments',
								'action' => 'add',
								$acct['CustomerAccount']['id']
							), array('class' => 'btn btn-mini')) ?>
						</td>

					</tr>
					<?php $sum += @$acct['CustomerAccount']['balance'] ?>
				<?php endforeach ?>
				</tbody>
				<tfoot>
			    <tr>
			    	<td>Total</td>
			    	<td></td>
			    	<td></td>
			    	<td></td>
			    	<td><?= money_format('%(#10n', $sum) ?></td>
					<td></td>
			    </tr>
			  </tfoot>
			</table>
		<?= $this->element('paging') ?>
		<?php else : ?>
		<div class="hero-unit">
			<h1>No Results Found</h1>
		</div>
		<?php endif ?>
	</div>
</div>
<style>
	.highlight { background-color: yellow }
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #ccc solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
	tfoot { border-top: #333 2px solid; }
</style>

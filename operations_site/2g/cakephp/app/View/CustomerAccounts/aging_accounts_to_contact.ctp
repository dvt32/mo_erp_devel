<legend><?= __('Aging Customer Accounts (Contact List)') ?></legend>

<div class="row-fluid">
	<div class="span3">

		<!-- search & filter forms -->
		<?= $this->Form->create('CustomerAccount', array(
			'type' => 'GET',
			'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
					'required' => false
			)))
			?>
			<div class="controls-row">
				<?= $this->Form->input('id', array('options' => $cust_accts, 'class' => 'input-block-level chosen click-submit', 'empty' => '', 'label' => 'Customer Account')) ?>
				<?= $this->Form->hidden('type', array('value' => 'customer')) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>

	</div>
	<div class="span9">
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="shrink nowrap"><?= $this->Paginator->sort('aging_followup_date', 'Follow Up') ?></th>
						<th class="stretch"><?= $this->Paginator->sort('name') ?></th>
						<th class="shrink nowrap"><?= $this->Paginator->sort('days_overdue', 'Overdue') ?></th>
						<th class="shrink nowrap"><?= $this->Paginator->sort('aging_count','Open Invoices') ?></th>
						<th class="shrink nowrap"><?= $this->Paginator->sort('balance') ?></th>
						
						<th class="shrink">Preferred</th>
						<th class="shrink nowrap">Recurring Amount</th>
						<th class="stretch">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($customerAccounts as $i => $acct): ?>
					<tr>

						<!-- follow up on -->
						<td>
							<?= @$acct['CustomerAccount']['aging_followup_date'] ?>
						</td>

						<!-- company name -->
						<td class="searchable shrink" style="width:auto;">
							<?= $this->Html->link($acct['CustomerAccount']['name'], [
								'controller' => 'agingInteractions',
								'action' => 'add',
								$acct['CustomerAccount']['id']
							]) ?>
						</td>

						<!-- days overdue -->
						<td><?= @$acct['CustomerAccount']['days_overdue'] ?> days</td>

						<!-- customerAccount.aging_count -->
						<td class="shrink nowrap">
							<?= $this->Html->link(
								@$acct['CustomerAccount']['aging_count'] . ' Aging',
								array(
									'controller' => 'billingAmounts',
									'action' => 'aging',
									'?' => array('cust_acct_id' => $acct['CustomerAccount']['id'])
								)
							) ?>
						</td>

						<!-- customerAccount.balance -->
						<td class="right"><?= @$acct['CustomerAccount']['balance'] ?></td>

						<!-- preferred payment method -->
						<td><?= @$acct['CustomerAccount']['preferred_payment_method'] ?></td>

						<!-- recurring billing amount -->
						<td class="shrink nowrap right">
							<?= @$acct['BillingAmount'][0]['recurring_amount'] ?>
						</td>

						<!-- add payment button -->
						<td class="shrink">

							<?= $this->Html->link('Review', array(
								'controller' => 'receivedPayments',
								'action' => 'aging',
								$acct['CustomerAccount']['id']
							), array('class' => 'btn btn-mini btn-default')) ?>

							<?/*= $this->Html->link('+Payment', array(
								'controller' => 'receivedPayments',
								'action' => 'add',
								$acct['CustomerAccount']['id']
							), array('class' => 'btn btn-mini btn-default')) */?>

						</td>

					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
		<?= $this->element('paging') ?>
	</div>
</div>
<style>
	.highlight { background-color: yellow }
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #ccc solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
</style>

<?php $query = @$this->request->query['q'] ?: null ?>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<script>
	var query = "<?= $query ?>";
	$("td.searchable").highlight(query);

	// checkall
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});
</script>


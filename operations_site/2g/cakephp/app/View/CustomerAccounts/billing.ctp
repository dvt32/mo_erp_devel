
<div class="row-fluid">
	<div class="content-head">
		<ul class="breadcrumb">
			<li>
				<a href="{{ operations_2g_urlprefix }}/employees/dashboard">Dashboard</a> <span class="divider"><i class="icon-angle-right"></i></span>
			</li>
			<li class="active">Billing Review</li>
		</ul>
		<h2>Billing Review</h2>
	</div>
</div>
<!-- progress bars -->
<div class="row-fluid">
	<div class="span12">
		<section class="module">
			<div class="module-body">
				<div class="row-fluid">
					
					<?php if (($progress['reviewed'] + $progress['not_reviewed']) != 0) : ?>
					<div class="span12">
						<?php $arp = ($progress['reviewed']/($progress['reviewed'] + $progress['not_reviewed']))*100 ?>
						<p class="muted">
							<?= $this->Html->link('Accounts Reviewed', array('action' => 'invoicing')) ?>
							 - <?= ceil($arp) ?>%
						</p>
						<div class="progress progress">
							<div class="bar" style="width: <?= $arp ?>%;"></div>
						</div>
					</div>
					<?php else: ?>
					<div class="span12">
						<p class="muted">
							<?= $this->Html->link('Accounts Reviewed', array('action' => 'invoicing')) ?>
							 - 100%
						</p>
						<div class="progress progress">
							<div class="bar" style="width: 100%;"></div>
						</div>
					</div>
					<?php endif ?>

				</div>
			</div><!--/.module-body-->
		</section>
	</div>
</div>
<!-- /progress bars -->
<div class="row-fluid">
	<div class="span2">
		<!-- search & filter forms -->
		<?= $this->Form->create('CustomerAccount', array(
			'type' => 'GET',
			'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
					'required' => false
			)))
			?>
			<div class="controls-row">
				<?= $this->Form->input('q', array('placeholder' => 'Search', 'label' => false, 'style' => 'width:auto;')) ?>

				<label class="checkbox" for="CustomerAccountShowRecentlyReviewed">
					<?= $this->Form->input('show_recently_reviewed', ['type' => 'checkbox', 'label' => false, 'div' => false]) ?>
					Show Recently Reviewed
				</label>
					

				<?= $this->Form->input('service_id', array('multiple' => 'checkbox')) ?>
				<?= $this->Form->hidden('type', array('value' => 'customer')) ?>
				<?= $this->Form->input('primary_floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>
	</div>
	<div class="span10">
		<?php if (!empty($customerAccounts)) : ?>
		<?= $this->Form->create('BillingMemo', array('inputDefaults' => array('label' => false, 'div' => false))) ?>
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="shrink"><label class="nowrap"><input class="checkall" type="checkbox" /></label></th>
						<th><?= $this->Paginator->sort('name') ?></th>
						<th class="shrink">Memos</th>
						<th class="shrink">&nbsp;</th>
						<th class="shrink"><?= $this->Paginator->sort('billing_last_reviewed_dt', 'Reviewed') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('service_id') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('primary_floor_id', 'Floor') ?></th>

						<th class="shrink"><?= $this->Paginator->sort('num_desks', 'Desks') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('num_offices', 'Offices') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('no_phones', 'Phones') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('num_persons', 'Persons') ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($customerAccounts as $i => $acct): ?>
					<?php
					$days = $acct['CustomerAccount']['days_since_last_review'];
					if ($days == null || $days >= 30)
						$color_class = 'error'; // red
					elseif ($days >= 20)
						$color_class = 'warning'; // yellow
					elseif ($days < 20)
						$color_class = ''; // green
					?>
					<tr class="<?= $color_class ?>">
						<?php $count = $acct['CustomerAccount']['billing_memos_inbox_count'] ?>

						<!-- checkbox -->
						<td>
							<?= $this->Form->hidden("CustomerAccount.{$i}.id", array('value' => $acct['CustomerAccount']['id'])) ?>
							<?= $this->Form->input("CustomerAccount.{$i}.reviewed", array('class' => 'checkable', 'type' => 'checkbox', 'disabled' => ($count > 0 ? 'disabled' : ''))) ?>
						</td>

						<!-- company name -->
						<td class="searchable shrink" style="width:auto;">
							<?php $back = Router::url($_SERVER['REQUEST_URI'],true) ?>
							<!-- link to customer account billing memos -->
							<?= $this->Html->link(
								$acct['CustomerAccount']['name'],
								array(
									'controller' => 'billingMemos',
									'action' => 'add',
									$acct['CustomerAccount']['id'],
									'?' => compact('back')
								)
							) ?>
						</td>

						<!-- billing memos -->
						<td class="nowrap">
							<?php if ($count > 0): ?>
								<?= __("<span class=\"badge badge-info\">%d</span>", $count) ?>
							<?php else: ?>
								<?= __("<span class=\"badge\">%d</span>", $count) ?>
							<?php endif ?>
							<?php $back = Router::url($_SERVER['REQUEST_URI'],true) ?>
						</td>

						<td class="shrink nowrap">
							<?php
							switch ($color_class)
							{
								case 'error':
									echo ' <span class="label label-important">+30</span> ';
								break;
								case 'warning':
									echo ' <span class="label label-warning">+20</span> ';
								break;
							}
							?>
						</td>

						<!-- last reviewed -->
						<td class="nowrap shrink">
						<?php
							$reviewed = strtotime(@$acct['CustomerAccount']['billing_last_reviewed_dt']);
							$monthago = strtotime('-1 month'); // FIXME reset hms to 00:00:00
							echo ($reviewed > $monthago) ? 
								__("<a title=\"%s\">%s</a>", $acct['BillingReviewer']['full_name'], date('m/d',$reviewed))
								 : '<i class="icon-remove"></i>';
							?>
						</td>

						<!-- service -->
						<td><?= $acct['Service']['name'] ?></td>

						<!-- floor -->
						<td><?= $acct['Floor']['short_global_label'] ?></td>

						<!-- desks -->
						<td><?= @$acct['BillingAmount'][0]['num_desks'] ?></td>

						<!-- offices -->
						<td><?= @$acct['BillingAmount'][0]['num_offices'] ?></td>

						<!-- phone_lines -->
						<td><?= @$acct['BillingAmount'][0]['num_phone_lines'] ?></td>

						<!-- persons -->
						<td><?= @$acct['BillingAmount'][0]['num_persons'] ?></td>

					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
			<?= $this->Form->button('<i class="icon-check"></i> Bill As Is', array('class' => 'btn btn-primary', 'escape' => false)) ?>
			<?= $this->Form->end() ?>
		<?= $this->element('paging') ?>
		<?php else: ?>
		<div class="hero-unit centered">
			<h1>Cleared!</h1>
			<img src="{{ operations_2g_assetprefix }}/img/dance/dance_<?= rand(1,5) ?>.gif" >
		</div>
		<?php endif ?>
	</div>
</div>
<style>
	.highlight { background-color: yellow }
	.shrink { width:0px; }
	.stretch { width:auto; }
	.nowrap { white-space:nowrap }
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #999 solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
</style>

<?php $query = @$this->request->query['q'] ?: null ?>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<script>
	var query = "<?= $query ?>";
	$("td.searchable").highlight(query);

	// checkall
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});
</script>

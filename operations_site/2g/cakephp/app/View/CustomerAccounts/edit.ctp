<section class="module">
	<div class="module-head">
		<b>Edit Customer Account</b>
	</div>
	<div class="module-body">
		<div class="row-fluid">
			<?= $this->Form->create('CustomerAccount', [
				'class' => 'form-horizontal',
				'inputDefaults' => [
					'class' => 'input-block-level',
				]
			]) ?>
				<fieldset>
					<div class="row-fluid">
						<div class="span2">
							<?= $this->Form->input('id') ?>
							<?= $this->Form->input('is_active') ?>
							<?= $this->Form->input('visible') ?>
							<?= $this->Form->input('type') ?>

						</div>
						<div class="span4">
							<?= $this->Form->input('name') ?>
							<?= $this->Form->input('external_name') ?>
							<?= $this->Form->input('description') ?>
							<?= $this->Form->input('web_site') ?>
						</div>

						<div class="span6">
							<?= $this->Form->input('primary_contact_cust_pers_id', ['class' => 'chosen input-block-level', 'options' => $customers, 'label' => ['text' => 'Primary Contact']]) ?>
							<?= $this->Form->input('note') ?>
						</div>
					</div>
				</fieldset>


				<fieldset>
					<div class="span6">
						<?= $this->Form->input('service_id') ?>
						<?= $this->Form->input('preferred_payment_method', ['options' => $preferred_payment_methods]) ?>
						<?= $this->Form->input('room_minutes') ?>
						<?= $this->Form->input('primary_floor_id') ?>
					</div>
					<div class="span6">
						<?= $this->Form->input('pkg_notify_email',['rows' => 2]) ?>
						<?= $this->Form->input('logo_url') ?>
						<?= $this->Form->input('status', ['readonly']) ?>
					</div>
				</fieldset>

				<fieldset>
					<div class="row-fluid">
						<?= $this->Form->input('ms_access_creation_dt', ['readonly','type' => 'text', 'div' => 'span4']) ?>
						<?= $this->Form->input('ms_access_random_num', ['readonly', 'div' => 'span4']) ?>
						<?= $this->Form->input('creation_dt', ['readonly', 'type' => 'text', 'div' => 'span4']) ?>
					</div>
				</fieldset>

				<?= $this->Form->submit('Submit', ['class' => 'btn btn-primary']) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
	<div class="actions">
	</div>
</section>

<h2>
	<?= $this->Html->image('icons/flat_miniset/png/256/pyramid.png', array('style' => 'height: 3em;')) ?>
	<?= __('Customer Companies') ?>
</h2>
<!-- tabs for customer/company directory -->
<ul class="nav nav-tabs">
	<li class="active"><?= $this->Html->link('Company', '#') ?></li>
	<li><?= $this->Html->link('Person', array('controller' => 'customers', 'action' => 'index')) ?></li>
</ul>

<div class="row-fluid">
	<!-- jump to customer id -->
	<div class="span2">
		<?= $this->Form->create('CustomerAccount', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'input-block-level'))) ?>
			<legend><?= __('Jump To') ?></legend>
			<?= $this->Form->input('id', array('type' => 'text', 'placeholder' => 'Company Id #')) ?>
			<?= $this->Form->button('Go', array('class' => 'btn')) ?>
		<?= $this->Form->end() ?>
	</div>

	<!-- search & filter forms -->
	<div class="span10">
		<?= $this->Form->create('CustomerAccount', array('type' => 'GET', 'inputDefaults' => array('class' => 'input-auto', 'label' => false, 'div' => false, 'required' => false))) ?>
			<div class="controls-row">
				<legend><?= __('Filter & Search') ?></legend>
				<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge')) ?>
				<?= $this->Form->input('service_id', array('empty' => '-- Service Type --')) ?>
				<?= $this->Form->input('type', array('empty' => '-- Type --')) ?>
				<?= $this->Form->input('primary_floor_id', array('empty' => '-- Primary Floor --', 'options' => $floors)) ?>
				<?= $this->Form->input('is_active', array('options' => array(1 => 'Active', 0 => 'Inactive'), 'empty' => '-- In/Active --')) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', array('action' => 'index')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

<div class="row-fluid">
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('id') ?></th>
				<th><?= $this->Paginator->sort('name') ?></th>
				<th><?= $this->Paginator->sort('primary_contact_cust_pers_id', 'Primary') ?></th>
				<th><?= $this->Paginator->sort('primary_floor_id') ?></th>
				<th><?= $this->Paginator->sort('type') ?></th>
				<th><?= $this->Paginator->sort('service_id') ?></th>
				<th><?= $this->Paginator->sort('room_minutes') ?></th>
				<th><?= $this->Paginator->sort('status') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($customerAccounts as $acct): ?>
				<tr>
					<td><strong class="big"><?= $this->Html->link($acct['CustomerAccount']['id'], array('action' => 'view', $acct['CustomerAccount']['id'])) ?></strong>&nbsp;</td>
					<td class="searchable"><?= $this->Html->link($acct['CustomerAccount']['name'], array('action' => 'view', $acct['CustomerAccount']['id'])) ?>&nbsp;</td>
					<td class=""><?= $this->Html->link($acct['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $acct['PrimaryCustomer']['id'])) ?></td>
					<td class=""><?= $this->Html->link($acct['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $acct['Floor']['id'])) ?></td>
					<td class=""><?= $acct['CustomerAccount']['type'] ?>&nbsp;</td>
					<td class=""><?= $acct['Service']['name'] ?></td>
					<td class=""><?= $acct['CustomerAccount']['room_minutes'] ?>&nbsp;</td>
					<td class=""><?= $acct['CustomerAccount']['status'] ?: ($acct['CustomerAccount']['is_active'] ? 'active' : 'inactive') ?>&nbsp;</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )) ?></p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>

<style>
	.highlight { background-color: yellow }
</style>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<script>
	var query = <?= "\"{$_GET['q']}\"" ?>;
	$("td.searchable").highlight(query);
</script>


<div class="row-fluid">
	<div class="content-head">
		<ul class="breadcrumb">
			<li>
				<a href="{{ operations_2g_urlprefix }}/employees/dashboard">Dashboard</a> <span class="divider"><i class="icon-angle-right"></i></span>
			</li>
			<li class="active">Invoice Changes</li>
		</ul>
		<h2>Invoice Changes</h2>
	</div>
</div>
<!-- progress bars -->
<div class="row-fluid">
	<div class="span12">
		<section class="module">
			<div class="module-body">
				<div class="row-fluid">
					<?php if (($progress['reviewed'] + $progress['not_reviewed']) != 0) : ?>
					<div class="span12">
						<?php $arp = ($progress['reviewed']/($progress['reviewed'] + $progress['not_reviewed']))*100 ?>
						<p class="muted">
							<?= $this->Html->link('Invoice Changes', array('action' => 'invoicing')) ?>
							 - <?= ceil($arp) ?>%
						</p>
						<div class="progress progress">
							<div class="bar" style="width: <?= $arp ?>%;"></div>
						</div>
					</div>
					<?php else: ?>
					<div class="span12">
						<p class="muted">
							<?= $this->Html->link('Accounts Reviewed', array('action' => 'invoicing')) ?>
							 - 100%
						</p>
						<div class="progress progress">
							<div class="bar" style="width: 100%;"></div>
						</div>
					</div>
					<?php endif ?>

				</div>
			</div><!--/.module-body-->
		</section>
	</div>
</div>
<!-- /progress bars -->
<section class="row-fluid">
	<div class="span3">
		<section class="module">
			<div class="module-head">
				<b>Search</b>
			</div><!--/.module-head-->
			<div class="module-body">
				<div class="row-fluid">
					<div class="span12">
						<!-- search & filter forms -->
						<?= $this->Form->create('CustomerAccount', array(
							'type' => 'GET',
							'inputDefaults' => array(
									'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
									'div' => array('class' => 'control-group'),
									'label' => array('class' => 'control-label'),
									'between' => '<div class="controls">',
									'after' => '</div>',
									'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
									'required' => false,
							)))
							?>
							<div class="controls-row">
								<?= $this->Form->input('q', array('placeholder' => 'Search', 'label' => false, 'style' => 'width:auto;')) ?>
								<label class="checkbox" style="margin-top:-1em;" for="CustomerAccountShowBillAsIs">
									<?= $this->Form->input('show_bill_as_is', array('type' => 'checkbox','label' => false, 'div' => false)) ?>
									Show Reviewed
								</label>
								<?= $this->Form->input('service_id', array('multiple' => 'checkbox')) ?>
								<?= $this->Form->hidden('type', array('value' => 'customer')) ?>
								<?= $this->Form->input('primary_floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
							</div>
							<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
							<?= $this->Html->link('Clear', Router::url()) ?>
						<?= $this->Form->end() ?>
					</div>
				</div>
			</div><!--/.module-body-->
		</section><!--/.module-->
	</div>
	<div class="span9">
		<section class="module">
			<div class="module-head">
				<b>Invoice Changes</b>
			</div><!--/.module-head-->
			<div class="module-body">
				<div class="row-fluid">
					<?php if (!empty($customerAccounts)) : ?>
					<div class="span12">
						<?= $this->Form->create('CustomerAccount', array('inputDefaults' => array('div' => false, 'label' => false))) ?>
							<table class="table table-condensed table-striped">
								<thead>
									<tr>
										<th class="shrink">
											<?= $this->Paginator->sort('billing_last_reviewed_dt', ' ') ?>
											<input type="checkbox" class="checkall" />
										</th>
										<th class="stretch"><?= $this->Paginator->sort('name') ?></th>
										<th class="shrink">Primary Contact</th>
										<th class="shrink"><?= $this->Paginator->sort('num_desks', 'Desks') ?></th>
										<th class="shrink"><?= $this->Paginator->sort('num_offices', 'Offices') ?></th>
										<th class="shrink"><?= $this->Paginator->sort('no_phones', 'Phones') ?></th>
										<th class="shrink"><?= $this->Paginator->sort('num_persons', 'Persons') ?></th>
										<th class="shrink"><?= $this->Paginator->sort('preferred_payment_method','Method') ?></th>
										<th class="shrink">Recurring Amount</th>
										<th class="shrink">Billing Memos</th>
										<!-- <th class="shrink"><?= $this->Paginator->sort('primary_floor_id', 'Floor') ?></th> -->
									</tr>
								</thead>
								<tbody>
								<?php foreach ($customerAccounts as $i => $acct): ?>
									<tr>
										<?php $count = $acct['CustomerAccount']['billing_memos_inbox_count'] ?>
										<?= ($count > 0) ? '<tr class="error">' : '<tr>' ?>

										<!-- checkbox -->
										<td>
											<?php
												$reviewed = strtotime(@$acct['CustomerAccount']['billing_last_reviewed_dt']);
												$monthago = strtotime('-1 month'); // FIXME reset hms to 00:00:00
												if ($reviewed > $monthago): ?>
												<?= __("<a title=\"%s\">%s</a>", $acct['BillingReviewer']['full_name'], date('m/d',$reviewed)) ?>
											<?php else: ?>
												<?= $this->Form->hidden("CustomerAccount.{$i}.id", array('value' => $acct['CustomerAccount']['id'])) ?>
												<?= $this->Form->input("CustomerAccount.{$i}.reviewed", array('class' => 'checkable', 'type' => 'checkbox', 'disabled' => ($count > 0 ? 'disabled' : ''))) ?>
											<?php endif ?>
										</td>

										<!-- company name -->
										<td class="searchable shrink" style="width:auto;">
											<?php $back = Router::url($_SERVER['REQUEST_URI'],true) ?>
											<!-- link to customer account billing memos -->
											<?= $this->Html->link($acct['CustomerAccount']['name'], array(
												'controller' => 'billingMemos',
												'action' => 'review',
												$acct['CustomerAccount']['id'],
												'?' => array('back' => $back)
											)) ?>
										</td>

										<!-- primary contact -->
										<td class="shrink nowrap">
											<?= $this->Html->link(@$acct['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $acct['PrimaryCustomer']['id'])) ?>
										</td>

										<!-- desks -->
										<td><?= @$acct['BillingAmount'][0]['num_desks'] ?></td>

										<!-- offices -->
										<td><?= @$acct['BillingAmount'][0]['num_offices'] ?></td>

										<!-- phone_lines -->
										<td><?= @$acct['BillingAmount'][0]['num_phone_lines'] ?></td>

										<!-- persons -->
										<td><?= @$acct['BillingAmount'][0]['num_persons'] ?></td>

										<!-- preferred payment method -->
										<td class="shrink nowrap">
											<?= @$acct['CustomerAccount']['preferred_payment_method'] ?>
										</td>

										<!-- recurring billing amount -->
										<td class="shrink nowrap">
											<?= @$acct['BillingAmount'][0]['recurring_amount'] ?>
										</td>

										<!-- billing memos -->
										<td class="nowrap">
											<?php if ($count > 0): ?>
												<?= $badge = __("<span class=\"badge badge-info\">%d</span>", $count) ?>
											<?php else: ?>
												<?= $badge = __("<span class=\"badge\">%d</span>", $count) ?>
											<?php endif ?>
										</td>


										<!-- floor -->
										<!-- <td><?= $acct['Floor']['short_global_label'] ?></td> -->
									</tr>
								<?php endforeach ?>
								</tbody>
							</table>
							<?= $this->Form->button('Bill As Is', array('class' => 'btn btn-primary btn-large')) ?>
						<?= $this->Form->end() ?>
						<?= $this->element('paging') ?>
					</div>
					<?php else: ?>
					<div class="hero-unit centered">
						<h1>Cleared!</h1>
						<img src="{{ operations_2g_assetprefix }}/img/dance/dance_<?= rand(1,5) ?>.gif" >
					</div>
					<?php endif ?>
				</div>
			</div>
		</section>
	</div>

</section>

<style>
	.highlight { background-color: yellow }
	.shrink { width:0px; }
	.stretch { width:auto; }
	.nowrap { white-space:nowrap }
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #ccc solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
</style>

<?php $query = @$this->request->query['q'] ?: null ?>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<script>
	var query = "<?= $query ?>";
	$("td.searchable").highlight(query);

	// checkall
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});
</script>

<div class="row-fluid">
	<ul class="breadcrumb">
		<li>
			<a href="{{ operations_2g_urlprefix }}/employees/dashboard">Dashboard</a> <span class="divider"><i class="icon-angle-right"></i></span>
		</li>
		<li class="active">Accounts Receivables</li>
	</ul>
</div>
<legend><?= __('Account Receivables') ?></legend>
<ul class="nav nav-tabs">
	<li class="active"><?= $this->Html->link('Customer Account', '#') ?></li>
	<li><?= $this->Html->link('Billing Amount', array('controller' => 'billingAmounts', 'action' => 'receiving')) ?></li>
</ul>
<!-- progress bars -->
<div class="row-fluid">
	<div class="span12">
		<section class="module">
			<div class="module-body">
				<div class="row-fluid">
					
					<?php if (($progress['received'] + $progress['not_received']) != 0) : ?>
					<div class="span12">
						<?php $arp = ($progress['received']/($progress['received'] + $progress['not_received']))*100 ?>
						<p class="muted">
							<?= $this->Html->link('Accounts Reviewed', array('action' => 'invoicing')) ?>
							 - <?= ceil($arp) ?>%
						</p>
						<div class="progress progress">
							<div class="bar" style="width: <?= $arp ?>%;"></div>
						</div>
					</div>
					<?php else: ?>
					<div class="span12">
						<p class="muted">
							<?= $this->Html->link('Accounts Reviewed', array('action' => 'invoicing')) ?>
							 - 100%
						</p>
						<div class="progress progress">
							<div class="bar" style="width: 100%;"></div>
						</div>
					</div>
					<?php endif ?>

				</div>
			</div><!--/.module-body-->
		</section>
	</div>
</div>
<!-- /progress bars -->
<h2 style="border-bottom:1px #ccc solid;">Receiving</h2>
<div class="row-fluid">
	<div class="span2">

		<!-- search & filter forms -->
		<?= $this->Form->create('CustomerAccount', array(
			'type' => 'GET',
			'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
					'required' => false
			)))
			?>
			<div class="controls-row">
				<?= $this->Form->input('q', array('placeholder' => 'Search', 'label' => false, 'style' => 'width:auto;')) ?>
				
				<label class="checkbox" style="margin-top:-1em;">
					<?= $this->Form->input('show_paid', array('type' => 'checkbox','label' => false, 'div' => false)) ?> Show Paid
				</label>
				<label class="checkbox">
					<?= $this->Form->input('show_unpaid', array('type' => 'checkbox','label' => false, 'div' => false)) ?> Show Unpaid
				</label>

				<?= $this->Form->input('service_id', array('multiple' => 'checkbox')) ?>
				<?= $this->Form->hidden('type', array('value' => 'customer')) ?>
				<?= $this->Form->input('primary_floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>

	</div>
	<div class="span10">
		<?php if (!empty($customerAccounts)) : ?>
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th><?= $this->Paginator->sort('name') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('due_date') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('preferred_payment_method', 'Preferred') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('balance') ?></th>
						<th class="stretch">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($customerAccounts as $i => $acct): ?>
					<tr>

						<!-- company name -->
						<td class="searchable shrink" style="width:auto;">
							<?= $this->Html->link(
								$acct['CustomerAccount']['name'],
								array(
									'controller' => 'receivedPayments',
									'action' => 'add',
									$acct['CustomerAccount']['id']
								)
							) ?>
						</td>

						<td class="shrink">
							<?= @$acct['BillingAmount'][0]['due_date'] ?: 'n/a' ?>
						</td>

						<!-- preferred payment method -->
						<td><?= @$acct['CustomerAccount']['preferred_payment_method'] ?></td>

						<!-- balance -->
						<td class="right">
							<?php $balance = $acct['CustomerAccount']['balance'] ?>
							<?php if ($balance > 0) {
								$badgeclass = 'important';
							} elseif($balance == 0) {
								$badgeclass = 'success';
							} else {
								$badgeclass = 'info';
							} ?>
							<?= __("<span class=\"badge badge-{$badgeclass}\">%.2f</span>", $balance) ?>
						</td>

					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
			<?= $this->element('paging') ?>
		<?php else: ?>
		<div class="hero-unit centered">
			<h1>Cleared!</h1>
			<img src="{{ operations_2g_assetprefix }}/img/dance/dance_<?= rand(1,5) ?>.gif" >
		</div>
		<?php endif ?>
	</div>
</div>
<style>
	.highlight { background-color: yellow }
	.shrink { width:0px; }
	.stretch { width:auto; }
	.nowrap { white-space:nowrap }
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #ccc solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
	.left { text-align:left!important; }
	.center { text-align:center!important; }
	.right { text-align:right!important; }
</style>

<?php $query = @$this->request->query['q'] ?: null ?>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<script>
	var query = "<?= $query ?>";
	$("td.searchable").highlight(query);

	// checkall
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});
</script>

<h1>Billing Cycle Workflow</h1>

<!-- progress bars -->
<div class="row-fluid">
	<div class="span12">
		<section class="module">
			<div class="module-head">
				<b>Billing Cycle Workflow</b>
			</div><!--/.module-head-->
			<div class="module-body">
				<div class="row-fluid">
					<div class="span12">
						<p class="muted">Overall Progress</p>
						<div class="progress">
							<div class="bar" style="width: 40%;"></div>
						</div>
						<p class="muted">
							<?= $this->Html->link('Accounts Reviewed', array('action' => 'billing')) ?>
						</p>
						<div class="progress progress-info">
							<div class="bar" style="width: 50%;"></div>
						</div>
						<p class="muted">
							<?= $this->Html->link('Account Adjustments', array('action' => 'invoicing')) ?>
						</p>
						<div class="progress progress-success">
							<div class="bar" style="width: 60%;"></div>
						</div>
						<p class="muted">
							<?= $this->Html->link('Accounts Receivables', array('action' => 'receiving')) ?>
						</p>
						<div class="progress progress-warning">
							<div class="bar" style="width: 30%;"></div>
						</div>
						<p class="muted">
							<?= $this->Html->link('Accounts Aging', array('action' => 'aging')) ?>
						</p>
						<div class="progress progress-danger">
							<div class="bar" style="width: 40%;"></div>
						</div>
					</div>

				</div>
			</div><!--/.module-body-->
		</section>
	</div>
</div>
<!-- /progress bars -->

<!-- graph -->
<div class="module">
	<div class="module-head">
		<b>Billed vs Received</b>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid">
			<div id="placeholder2" class="chart"></div>
		</div>
	</div>
</div>
<!-- /graph -->

<div class="row-fluid">
	<div class="span2">
		<!-- search & filter forms -->
		<?= $this->Form->create('CustomerAccount', array(
			'type' => 'GET',
			'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
					'required' => false
			)))
			?>
			<div class="controls-row">
				<legend><?= __('Filter & Search') ?></legend>
				<?= $this->Form->input('q', array('placeholder' => 'Search', 'label' => false, 'style' => 'width:auto;')) ?>
				<label class="checkbox" style="margin-top:-1em;" for="CustomerAccountShowBillAsIs">
					<?= $this->Form->input('show_bill_as_is', array('type' => 'checkbox','label' => false, 'div' => false)) ?>
					Show Unchanged
				</label>
				<?= $this->Form->input('service_id', array('multiple' => 'checkbox')) ?>
				<?= $this->Form->hidden('type', array('value' => 'customer')) ?>
				<?= $this->Form->input('primary_floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
				<?= $this->Form->input('is_active', array('options' => array(1 => 'Active', 0 => 'Inactive'), 'multiple' => 'checkbox', 'label' => 'Status')) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>
	</div>
	<div class="span10">
		<?= $this->Form->create('BillingMemo', array('inputDefaults' => array('label' => false, 'div' => false))) ?>
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="shrink"><label class="nowrap"><input class="checkall" type="checkbox" /></label></th>
						<th><?= $this->Paginator->sort('name') ?></th>
						<th class="stretch">Billing Memos</th>
						<th class="shrink"><?= $this->Paginator->sort('billing_reviewer_id', 'Reviewer') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('billing_last_reviewed_dt', 'Last Reviewed') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('service_id') ?></th>
						<th class="shrink"><?= $this->Paginator->sort('primary_floor_id', 'Floor') ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($customerAccounts as $i => $acct): ?>
					<tr>
						<?php $count = $acct['CustomerAccount']['billing_memos_inbox_count'] ?>
						<?= ($count > 0) ? '<tr class="error">' : '<tr>' ?>

						<!-- checkbox -->
						<td>
							<?= $this->Form->hidden("CustomerAccount.{$i}.id", array('value' => $acct['CustomerAccount']['id'])) ?>
							<?= $this->Form->input("CustomerAccount.{$i}.reviewed", array('class' => 'checkable', 'type' => 'checkbox', 'disabled' => ($count > 0 ? 'disabled' : ''))) ?>
						</td>

						<!-- company name -->
						<td class="searchable shrink" style="width:auto;">
							<?= $this->Html->link($acct['CustomerAccount']['name'], array('action' => 'view', $acct['CustomerAccount']['id'])) ?>
						</td>

						<!-- billing memos -->
						<td class="nowrap">
							<?php if ($count > 0): ?>
								<?= __("<span class=\"badge badge-info\">%d</span>", $count) ?>
							<?php else: ?>
								<?= __("<span class=\"badge\">%d</span>", $count) ?>
							<?php endif ?>
							<?php $back = Router::url($_SERVER['REQUEST_URI'],true) ?>
							<?= $this->Html->link(
								'+ Update',
								array(
									'controller' => 'billingMemos',
									'action' => 'add',
									$acct['CustomerAccount']['id'],
									'?' => compact('back')
								),
								array('class' => 'btn btn-small btn-success')
							) ?>
						</td>

						<!-- reviewer -->
						<td class="nowrap shrink"><?= @$acct['BillingReviewer']['full_name'] ?></td>

						<!-- last reviewed -->
						<td class="nowrap shrink"><?= @$acct['CustomerAccount']['billing_last_reviewed_dt'] ?></td>
						<td><?= $acct['Service']['name'] ?></td>
						<td>
							<?= $acct['Floor']['short_global_label'] ?>
						</td>
					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
			<?= $this->Form->button('<i class="icon-check"></i> Bill As Is', array('class' => 'btn btn-primary', 'escape' => false)) ?>
			<?= $this->Form->end() ?>
		<?= $this->element('paging') ?>
	</div>
</div>
<style>
	.highlight { background-color: yellow }
	.shrink { width:0px; }
	.stretch { width:auto; }
	.nowrap { white-space:nowrap }
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #999 solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
</style>

<?php $query = @$this->request->query['q'] ?: null ?>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<!-- FLOT CHART -->
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.js"></script>
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.pie.js"></script>
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.resize.js"></script>
<script src="{{ operations_2g_assetprefix }}/js/libs/libero/mixitup/jquery.mixitup.min.js"></script>

<!-- SCRIPTS: FLOT GRAPH ADVANCE -->
<script type="text/javascript">
$(function () {

	// Month Billed
	var d1 = [ [0, 1], [1, 14], [2, 5], [3, 4], [4, 5], [5, 1], [6, 14], [7, 5],  [8, 5] ];
	// Month Received
	var d2 = [ [0, 5], [1, 2], [2, 10], [3, 1], [4, 9],  [5, 5], [6, 2], [7, 10], [8, 8] ];

	var plot = $.plot($("#placeholder2"),
		   [ { data: d1, label: "Billed"}, { data: d2, label: "Received" } ], {
				lines: { 
					show: true, 
					fill: true, /*SWITCHED*/
					lineWidth: 2 
				},
				points: { 
					show: true, 
					lineWidth: 3
				},
				grid: {
					clickable: true,
					hoverable: true,
					autoHighlight: true,
					mouseActiveRadius: 10,
					aboveData: true,
					backgroundColor: "#fafafa",
					borderWidth: 0,
					minBorderMargin: 25,
				},
				colors: [ "#090", "#099",  "#609", "#900"],
				shadowSize: 0
			 });

	function showTooltip(x, y, contents) {
		$('<div id="gridtip">' + contents + '</div>').css( {
			position: 'absolute',
			display: 'none',
			top: y + 5,
			left: x + 5
		}).appendTo("body").fadeIn(300);
	}

	var previousPoint = null;
	$("#placeholder2").bind("plothover", function (event, pos, item) {
		$("#x").text(pos.x.toFixed(2));
		$("#y").text(pos.y.toFixed(2));

		if (item) {
			if (previousPoint != item.dataIndex) {
				previousPoint = item.dataIndex;
				
				$("#gridtip").remove();
				var x = item.datapoint[0].toFixed(0),
					y = item.datapoint[1].toFixed(0);
				
				showTooltip(item.pageX, item.pageY,
							"x : " + x + "&nbsp;&nbsp;&nbsp; y : " + y);
			}
		}
		else {
			$("#gridtip").remove();
			previousPoint = null;            
		}
	});
});
</script>

<script>
	var query = "<?= $query ?>";
	$("td.searchable").highlight(query);

	// checkall
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});



</script>



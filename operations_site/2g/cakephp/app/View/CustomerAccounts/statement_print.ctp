<!-- Print dialogue -->
<script type="text/javascript">
	javascript:window.print()
</script>


<div class="container">
<div class="row-fluid">
	<div class="span6">
		Statement Issued: <?= date(DATE_FORMAT_USA) ?>
	</div>
	<div class="span6">
		<span class="pull-right">From: Mar-2014 to <?= date('M-Y') ?></span>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<h3>MicroOffice Statement</h3>
	</div>
</div>
<div class="row-fluid">

	<!-- Customer Account Information -->
	<section class="module span6">
		<div class="module-head">
			<b>Account Information</b>
		</div>
		<div class="module-body">
			<dl>
				<dt><?= __('Name') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['name']) ?>&nbsp;</dd>
				<dt><?= __('Primary Customer') ?></dt>
				<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?>&nbsp;</dd>
				<dt><?= __('Account Created Date') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['creation_dt']) ?>&nbsp;</dd>
				<dt><?= __('Service') ?></dt>
				<dd><?= $customerAccount['Service']['name'] ?>&nbsp;</dd>
				<dt><?= __('Preferred Payment Method') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['preferred_payment_method']) ?>&nbsp;</dd>
				<dt><?= __('Room Minutes') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['room_minutes']) ?>&nbsp;</dd>
				<dt><?= __('Floor') ?></dt>
				<dd><?= $customerAccount['Floor']['short_global_label'] ?>&nbsp;</dd>
			</dl>
		</div>
	</section>

	<?php $pc = $customerAccount['PrimaryCustomer'] ?>
	<!-- Primary Customer Information -->
	<section class="module span6">
		<div class="module-head">
			<b>Primary Contact Information</b>
		</div>
		<div class="module-body">
			<dl>
				<dt>Name</dt>
				<dd><?= $pc['salutation'] . ' ' . $pc['first_name'] . ' ' . $pc['middle_name'] . ' ' . $pc['last_name'] ?></dd>
				<dt>Email Address</dt>
				<dd><?= $pc['email'] ?></dd>
				<dt>Work Phone</dt>
				<dd><?= $pc['work_phone_number'] ?></dd>
				<dt>Home Phone</dt>
				<dd><?= $pc['home_phone_number'] ?></dd>
				<dt>Cell Phone</dt>
				<dd><?= $pc['cell_phone_number'] ?></dd>
			</dl>
		</div>
	</section>
</div>

<!-- List Billing Amounts -->
<div class="row-fluid">
	<section class="module span6">
		<div class="module-head">
			<b>Invoices</b>
		</div>
		<div class="module-body">
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>Due Date</th>
						<th>Grand Total</th>
						<th>Balance</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($customerAccount['BillingAmount'] as $billingAmount): ?>
						<tr>
							<td><?= $billingAmount['due_date'] ?></td>
							<td><?= $billingAmount['grand_total'] ?></td>
							<td><?= $billingAmount['balance'] ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</section>	

	<!-- List Payments -->
	<section class="module span6">
		<div class="module-head">
			<b>Payments</b>
		</div>
		<div class="module-body">
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>Amount</th>
						<th>Payment Method</th>
						<th>Processed Date</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($payments as $payment): ?>
						<tr>
							<td><?= $payment['amount'] ?></td>
							<td><?= $payment['method'] ?></td>
							<td><?= $payment['date'] ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</section>	
</div>
<div class="row-fluid">
	<div class="span6">
		Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nulla vitae elit libero, a pharetra augue.
	</div>
	<div class="span6">
		<h3>
			Total Balance Due: $ <?= $customerAccount['CustomerAccount']['balance']?>
		</h3>
	</div>
</div>

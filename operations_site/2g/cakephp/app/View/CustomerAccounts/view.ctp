<!-- breadcrumbs -->
<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="{{ operations_2g_urlprefix }}/customerAccounts">Companies</a> <span class="divider">/</span></li>
			<li class="active"><?= $customerAccount['CustomerAccount']['name'] ?></li>
		</ul>
	</div>
</div>
<!-- /breadcrumbs -->

<!-- checked by OM -->
<?php $monthago = strtotime('-1 month'); ?>
<?php $reviewed_dt = @$customerAccount['CustomerAccount']['billing_last_reviewed_dt'] ?>
<?php $reviewed_time = strtotime($reviewed_dt) ?>
<?php $reviewer_id = @$customerAccount['CustomerAccount']['billing_reviewer_id'] ?>

<?php if (!empty($reviewed_time)): // has been reviewed ?>
	<?php if ($reviewed_time > $monthago): // reviewed recently ?>
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
			<h4>Account is up-to-date </h4>
			Reviewed on <strong><?= $reviewed_dt ?></strong><br />
			by <strong><?= @$customerAccount['BillingReviewer']['full_name'] ?></strong>
		</div>
	<?php else: // not reviewed recently ?>
		<div class="alert">
			<a href="#" class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
			<strong>
				This Account has not been reviewed in the current billing cycle</strong>
				Lasted Reviewed on: <?= @$reviewed_dt ?><br />
				by <strong><?= @$customerAccount['BillingReviewer']['full_name'] ?></strong>
		</div>
	<?php endif ?>
<?php else: // never reviewed ?>
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
		<strong>This Account has Never been Reviewed!</strong>
	</div>
<?php endif ?>

<div class="customerAccounts view">
	<dl>
		<dt><?= __('Id'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['id']); ?>&nbsp;</dd>
		<dt><?= __('Name'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['name']); ?>&nbsp;</dd>
		<dt><?= __('Primary Customer'); ?></dt>
		<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])); ?>&nbsp;</dd>
		<dt><?= __('Is Active'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['is_active']); ?>&nbsp;</dd>
		<dt><?= __('Creation Dt'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['creation_dt']); ?>&nbsp;</dd>
		<dt><?= __('External Name'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['external_name']); ?>&nbsp;</dd>
		<dt><?= __('Description'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['description']); ?>&nbsp;</dd>
		<dt><?= __('Web Site'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['web_site']); ?>&nbsp;</dd>
		<dt><?= __('Visible'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['visible']); ?>&nbsp;</dd>
		<dt><?= __('Type'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['type']); ?>&nbsp;</dd>
		<dt><?= __('Note'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['note']); ?>&nbsp;</dd>
		<dt><?= __('Tax Id'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['tax_id']); ?>&nbsp;</dd>
		<dt><?= __('Service'); ?></dt>
		<dd><?= $this->Html->link($customerAccount['Service']['name'], array('controller' => 'services', 'action' => 'view', $customerAccount['Service']['id'])); ?>&nbsp;</dd>
		<dt><?= __('Preferred Payment Method'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['preferred_payment_method']); ?>&nbsp;</dd>
		<dt><?= __('Room Minutes'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['room_minutes']); ?>&nbsp;</dd>
		<dt><?= __('Floor'); ?></dt>
		<dd><?= $this->Html->link($customerAccount['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $customerAccount['Floor']['id'])); ?>&nbsp;</dd>
		<dt><?= __('Pkg Notify Email'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['pkg_notify_email']); ?>&nbsp;</dd>
		<dt><?= __('Created'); ?></dt>
		<dd><?= @($customerAccount['CustomerAccount']['creation_dt']); ?>&nbsp;</dd>
		<dt><?= __('Modified'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['modified']); ?>&nbsp;</dd>
		<!-- <dt><?= __('Logo Url'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['logo_url']); ?>&nbsp;</dd> -->
		<!-- <dt><?= __('Status'); ?></dt>
		<dd><?= h($customerAccount['CustomerAccount']['status']); ?>&nbsp;</dd> -->
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<!-- Edit Link -->
		<li class="active"><?= $this->Html->link(__('Edit Customer Account'), array('action' => 'edit', $customerAccount['CustomerAccount']['id'])); ?> </li>
		<!-- Billing Amounts -->
		<li class="active"><?= $this->Html->link(__('List Billing Amounts'), array('controller' => 'billingAmounts', 'action' => 'receiving', '?' => array('cust_acct_id' =>  $customerAccount['CustomerAccount']['id']))); ?></li>
		<!-- Billing Memos -->
		<li class="active"><?= $this->Html->link(__('Add Billing Memos'), array('controller' => 'billingMemos', 'action' => 'review', $customerAccount['CustomerAccount']['id'])); ?></li>
		<!-- Receive Payment -->
		<li class="active"><?= $this->Html->link(__('Add Receive Payment'), array('controller' => 'receivedPayments', 'action' => 'add', $customerAccount['CustomerAccount']['id'])); ?></li>
		<!-- Aging Interactions -->
		<li class="active"><?= $this->Html->link(__('Add Aging Interactions'), array('controller' => 'agingInteractions', 'action' => 'add', $customerAccount['CustomerAccount']['id'])); ?></li>
		<!-- Aging Interactions -->
		<li class="active"><?= $this->Html->link(__('Customer Statements'), array('controller' => 'customerAccounts', 'action' => 'statement', $customerAccount['CustomerAccount']['id'])); ?></li>
	</ul>
</div>
<div class="related">
	<h3><?= __('Related Desks'); ?></h3>
	<table class="table table-condensed">
	<thead>
		<th>Desk Assigned Number</th>
		<th>Number of People</th>
		<th>Current Price</th>
		<th>Start Date</th>
		<th>End Date</th>
	</thead>
	<?php foreach ($customerAccount['Desk'] as $desk) : ?>
		<tr>
			<td>
				<?= $this->Html->link('Desk Number '.$desk['assigned_number'], array('controller' => 'desks', 'action' => 'view', $desk['id'])); ?>
			</td>
			<td><?= $desk['num_people'] ?></td>
			<td><?= $desk['current_price'] ?></td>
			<td><?= $desk['start_date'] ?></td>
			<td><?= $desk['end_date'] ?></td>
		</tr>
	<?php endforeach ?>
	</table>
</div>
<div class="related">
	<h3><?= __('Mailboxes'); ?></h3>
	<?php if (!empty($customerAccount['Mailbox'])): ?>
	<table class="table table-condensed">
	<?php foreach ($customerAccount['Mailbox'] as $mail): ?>
	<tr>
			<td><?= $this->Html->link($mail['mailbox_number'], array('controller' => 'mailboxes', 'action' => 'view', $mail['id'])); ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
<div class="related">
	<h3><?= __('License Agreements'); ?></h3>
	<?php if (!empty($customerAccount['LicenseAgreement'])): ?>
	<table class="table table-condensed">
	<tr>
		<th><?= __('Id'); ?></th>
	</tr>
	<?php foreach ($customerAccount['LicenseAgreement'] as $license): ?>
	<tr>
			<td><?= $this->Html->link($license['id'], array('controller' => 'licenseAgreements', 'action' => 'view', $license['id'])); ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
<div class="related">
	<h3><?= __('Phone Numbers'); ?></h3>
	<?php if (!empty($customerAccount['PhoneNumber'])): ?>
	<table class="table table-condensed">
	<?php foreach ($customerAccount['PhoneNumber'] as $phone): ?>
	<tr>
			<td><?= $phone['phone_number']; ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
<div class="related">
	<h3><?= __('Proximity Cards'); ?></h3>
	<?php if (!empty($customerAccount['ProximityCard'])): ?>
	<table class="table table-condensed">
	<tr>
		<th><?= __('Number'); ?></th>
		<th><?= __('Type'); ?></th>
		<th><?= __('Status'); ?></th>
		<th class="actions"><?= __('Actions'); ?></th>
	</tr>	
	<?php foreach ($customerAccount['ProximityCard'] as $card): ?>
	<tr>
			<td><?= $card['number']; ?></td>
			<td><?= $card['type']; ?></td>
			<td><?= $card['status']; ?></td>
			<td class="actions">
				<?= $this->Html->link(__('View'), array('controller' => 'proximityCards', 'action' => 'view', $card['id'])); ?>
				<?= $this->Html->link(__('Edit'), array('controller' => 'proximityCards', 'action' => 'edit', $card['id'])); ?>
				<?= $this->Form->postLink(__('Deactivate'), array('controller' => 'proximityCards', 'action' => 'clear', $card['id']), null, __('Are you sure you want to deactivate # %s?', $card['id'])); ?>
			</td>
	</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?= __('Related Customers'); ?></h3>
	<?php if (!empty($customerAccount['Customer'])): ?>
	<table class="table table-condensed">
	<tr>
		<th><?= __('Id'); ?></th>
		<th><?= __('Name'); ?></th>
		<th><?= __('Email'); ?></th>
		<th><?= __('Creation Dt'); ?></th>
		<th><?= __('Is Active'); ?></th>
		<th><?= __('Register With Building'); ?></th>
		<th><?= __('Title'); ?></th>
		<th><?= __('Enable Cc Pay'); ?></th>
		<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($customerAccount['Customer'] as $customer): ?>
		<tr>
			<td><?= @$customer['id']; ?></td>
			<td><?= @$customer['full_name']; ?></td>
			<td><?= @$customer['email']; ?></td>
			<td><?= @$customer['creation_dt']; ?></td>
			<td><?= @$customer['is_active']; ?></td>
			<td><?= @$customer['register_with_building']; ?></td>
			<td><?= @$customer['title']; ?></td>
			<td><?= @$customer['enable_cc_pay']; ?></td>
			<td class="actions">
				<?= $this->Html->link(__('View'), array('controller' => 'customers', 'action' => 'view', $customer['id'])); ?>
				<?= $this->Form->postLink(__('Deactivate'), array('controller' => 'customers', 'action' => 'deactivate', $customer['id']), null, __('Are you sure you want to deactivate # %s?', $customer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?= $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?= __('Related Tasks'); ?></h3>
	<?php if (!empty($customerAccount['Task'])): ?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th><?= __('Id','#'); ?></th>
				<th><?= __('Description'); ?></th>
				<th><?= __('Assigned Employee Id'); ?></th>
				<th><?= __('Creation Date'); ?></th>
				<th><?= __('Requested Completion Date'); ?></th>
				<th><?= __('Completion Date'); ?></th>
				<th><?= __('Status'); ?></th>
				<th><?= __('Floor Id'); ?></th>
				<th><?= __('Area'); ?></th>
				<th class="actions"><?= __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($customerAccount['Task'] as $task): ?>
				<tr>
					<td><?= @$task['id']; ?></td>
					<td><?= @$task['description']; ?></td>
					<td><?= @$task['assigned_employee_id']; ?></td>
					<td><?= @$task['creation_date']; ?></td>
					<td><?= @$task['requested_completion_date']; ?></td>
					<td><?= @$task['completion_date']; ?></td>
					<td><?= @$task['status']; ?></td>
					<td><?= @$task['floor_id']; ?></td>
					<td><?= @$task['area']; ?></td>
					<td class="actions">
						<?= $this->Html->link(__('View'), array('controller' => 'tasks', 'action' => 'view', $task['id'])); ?>
						<?= $this->Html->link(__('Edit'), array('controller' => 'tasks', 'action' => 'edit', $task['id'])); ?>
						<?= $this->Form->postLink(__('Delete'), array('controller' => 'tasks', 'action' => 'delete', $task['id']), null, __('Are you sure you want to delete # %s?', $task['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?= $this->Html->link(__('New Task'), array('controller' => 'tasks', 'action' => 'add')); ?> </li>
		</ul>
	</div>

<div class="span12 creditcards" style="display:none;">
		<div class="related">
			<h3>CC Transactions</h3>
				<?php if (!empty($customerAccount['CreditCardTxn'])): ?>
					<table class="table table-condensed creditcards_toggle">
	<tr>
		<th><?= __('Processed'); ?></th>
		<th><?= __('Card Number'); ?></th>
		<th><?= __('Expiration'); ?></th>
		<th><?= __('Amount'); ?></th>
		<th><?= __('Response'); ?></th>
	</tr>
	<?php foreach ($customerAccount['CreditCardTxn'] as $creditcard): ?>
	<tr>
			<td><?= $creditcard['creation_dt']; ?></td>
			<td><?= $creditcard['card_number']; ?></td>
			<td><?= $creditcard['card_expiration_month']; ?>&nbsp;|&nbsp;<?= $creditcard['card_expiration_year']; ?></td>
			<td><?= $creditcard['total_charge']; ?></td>
			<td><?= $creditcard['response_approved']; ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php else: ?>
			<div class="hero-unit creditcards_toggle">
				<h2>No Transactions</h2>
			</div>
</div>
<?php endif; ?>
</div>
<script type="text/javascript">
compact('creditcards', 'toggle');

</script>
<div class="span12 achdebit" style="display:none;">
		<div class="related">
			<h3>Ach Transactions</h3>
				<?php if (!empty($customerAccount['AchDebitTxn'])): ?>
					<table class="table table-condensed achdebit_toggle">
	<tr>
		<th><?= __('Processed'); ?></th>
		<th><?= __('Bank Account'); ?></th>
		<th><?= __('Amount'); ?></th>
	</tr>
	<?php foreach ($customerAccount['AchDebitTxn'] as $creditcard): ?>
	<tr>
			<td><?= $creditcard['processed_dt']; ?></td>
			<td><?= $creditcard['company_bank_acct_num']; ?></td>
			<td><?= $creditcard['amount']; ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php else: ?>
			<div class="hero-unit achdebit_toggle">
				<h2>No Transactions</h2>
			</div>
</div>
<?php endif; ?>
</div>
</div>
<script type="text/javascript">
compact('achdebit', 'toggle');

</script>

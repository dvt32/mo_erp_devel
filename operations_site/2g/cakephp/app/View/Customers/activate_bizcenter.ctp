<div class="row-fluid">
	<div class="span8 offset2">

		<?= $this->Session->flash() ?>

		<!-- customer information -->
		<section class="module" id="CustomerChangePasswordSection">
			<div class="module-head clickable" id="TaskFormHeader">
				<b><?= __('Customer Information') ?></b>
			</div>
			<div class="module-body no-padding">
				<div class="stream composer">
					<dl>
						<dt>Customer Id</dt>
						<dd><?= $customer['Customer']['id'] ?>&nbsp;</dd>
						<dt>Customer Name</dt>
						<dd><?= $this->Html->link($customer['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customer['Customer']['id'])) ?>&nbsp;</dd>
						<dt>Email Address</dt>
						<dd>
							<?php $href = sprintf("mailto:%s?cc=%s&subject=%s", $customer['Customer']['email'], AuthComponent::user('username'), 'Your%20Temporary%20BizCenter%20Password') ?>
							<?= $this->Html->link($customer['Customer']['email'], $href) ?>
						&nbsp;</dd>
						<dt>Company Id</dt>
						<dd><?= $customer['CustomerAccount']['id'] ?>&nbsp;</dd>
						<dt>Company Name</dt>
						<dd><?= $this->Html->link($customer['CustomerAccount']['name'], array('controller' => 'customerAccounts', 'action' => 'view', $customer['CustomerAccount']['id'])) ?>&nbsp;</dd>
					</dl>

			</div>
		</section>

		<!-- change customer password -->
		<section class="module" id="CustomerChangePasswordSection">
			<div class="module-head clickable" id="TaskFormHeader">
				<b><?= __('Activate Bizcenter Account') ?></b>
			</div>
			<div class="module-body no-padding">
				<div class="stream composer">

					<p>
						Make sure to inform the customer of the change being made, and send an
						email (or phone call) explaining the steps they need to take to change
						their password.
					</p>

					<input class="input-large" id="generated_password" type="text" readonly>
					<button id="button_generate_password" class="btn">Generate Random</button>
					
					<!-- change password form -->
					<?= $this->Form->create('Customer', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
						<?= $this->Form->input('Customer.id') ?>
						<?= $this->Form->input('User.id') ?>
						<?= $this->Form->input('Customer.email') ?>
						<?= $this->Form->input('User.role', array('label' => 'User Role', 'options' => $role)) ?>
						<?= $this->Form->input('Customer.password') ?>
						<?= $this->Form->input('Customer.confirm_password') ?>
						<?= $this->Form->button('Change Password', array('class' => 'btn')) ?>
					<?= $this->Form->end() ?>
				</div>

			</div>
		</section>


	</div>
</div>


<script>
	function randomString(length, chars) {
	    var result = '';
	    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
	    return result;
	}
	$(function() {
		$("#button_generate_password").click(function() {
			var rand = randomString(10,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
			$("#generated_password").val(rand);
			$("#CustomerPassword").val(rand);
			$("#CustomerConfirmPassword").val(rand);
		})
	});
</script>

<div class="span8 offset2">
	<section class="module">
		<div class="module-head">
			<b>New Customer</b>
		</div>
		<div class="module-body">

			<?= $this->Session->flash() ?>

			<?= $this->Form->create('Customer', array('inputDefaults' => array('class' => 'input-block-level', 'required' => false))) ?>

				<h4>Company Information</h4>

					<div id="add-to-existing-company-form" class="row-fluid" style="">
						<?= $this->Form->input('Customer.cust_acct_id', array('options' => $customerAccounts, 'label' => false, 'empty' => '-- Choose Company --')) ?>
						<?= $this->Form->input('Customer.make_primary', array('label' => 'Set customer as primary contact', 'type' => 'checkbox', 'div' => array('class' => 'checkbox inline'))) ?>
					</div>

					<div id="create-new-company-form" class="row-fluid" style="display:none">
						<legend>New Company</legend>
						<?= $this->Form->hidden('CustomerAccount.is_active', array('value' => true)) ?>
						<div class="row-fluid">
							<div class="span6"><?= $this->Form->input('CustomerAccount.name') // internal ?></div>
							<div class="span6"><?= $this->Form->input('CustomerAccount.external_name') // external ?></div>
						</div>
						<?= $this->Form->input('CustomerAccount.description') // external ?>
						<?= $this->Form->input('CustomerAccount.note', array('placeholder' => 'For internal use only')) // internal ?>
						<?= $this->Form->input('CustomerAccount.type', array('options' => $types, 'empty' => '')) ?>
						<?= $this->Form->input('CustomerAccount.room_minutes', array('label' => 'Conference Room Minutes', 'options' => $room_minutes, 'empty' => '')) // per month ?>
						<?= $this->Form->input('CustomerAccount.visible', array('checked')) // visible in tenant directory ?>
						<?= $this->Form->input('CustomerAccount.service_id') // regular, virtual, therapy, etc.? ?>
						<?= $this->Form->input('CustomerAccount.primary_floor_id', array('label' => 'Primary Floor', 'options' => $floors, 'empty' => '')) ?>
						<?= $this->Form->input('CustomerAccount.pkg_notify_email', array('label' => 'Package Notify Email(s)')) ?>
					</div>

				</fieldset>

				<h4>Basic Customer Information</h4>
				<fieldset>
					<!-- name -->
					<label>Customer Name</label>
					<div class="controls-row">
						<?= $this->Form->input('Customer.salutation', array('class' => 'input-small', 'div' => false, 'label' => false, 'placeholder' => 'Salutation')) ?>
						<?= $this->Form->input('Customer.first_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'First Name')) ?>
						<?= $this->Form->input('Customer.middle_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'Middle Name')) ?>
						<?= $this->Form->input('Customer.last_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'Last Name')) ?>
					</div>

					<!-- extra -->
					<?= $this->Form->hidden('Customer.is_active', array('value' => true)) ?>
					<?= $this->Form->input('Customer.nickname') ?>

					<!-- credentials -->
					<div class="row-fluid">
						<div class="span6">
							<?= $this->Form->input('Customer.email') ?>
						</div>
						<div class="span6">
							<?= $this->Form->hidden('Customer.password', array('type' => 'text', 'readonly')) ?>
							<?= $this->Form->hidden('Customer.confirm_password') ?>
						</div>
					</div>
					<?= $this->Form->input('Customer.note', array('placeholder' => 'For internal use only')) ?>
				</fieldset>

				<?= $this->Form->button('Save Customer', array('class' => 'btn')) ?>
			<?= $this->Form->end() ?>
		</div>
	</section>
</div>
<?= $this->Html->script("libs/jquery.maskedinput.min.js") ?>
<?= $this->Html->script("libs/chosen/chosen.jquery.min") ?>
<?= $this->Html->css("libs/chosen/chosen.css") ?>

<style>
	fieldset { margin-bottom:1.5em; }
</style>

<script type="text/javascript">

	$("#CustomerCustAcctId").chosen();
	function showCreateNewCompany() {
		$("#create-new-company-form").show();
		$("#add-to-existing-company-form").hide();
	}
	function showExistingCompanies() {
		$("#create-new-company-form").hide();
		$("#add-to-existing-company-form").show();
	}
	$(function() {
		$("#create-new-company-label").click(showCreateNewCompany);
		$("#add-to-existing-company-label").click(showExistingCompanies);
		if ($("#create-new-company-radio").attr('checked')) { showCreateNewCompany(); }
		else if ($("#add-to-existing-company-radio").attr('checked')) { showExistingCompanies(); }
	});

</script>

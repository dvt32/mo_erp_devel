<div class="customers form">
<?php echo $this->Form->create('Customer'); ?>
	<fieldset>
		<legend><?php echo __('Edit Customer'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('salutation');
		echo $this->Form->input('first_name');
		echo $this->Form->input('middle_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('nickname');
		echo $this->Form->input('email');
		echo $this->Form->input('work_phone_number');
		echo $this->Form->input('home_phone_number');
		echo $this->Form->input('cell_phone_number');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('is_active');
		echo $this->Form->input('note');
		echo $this->Form->input('register_with_building');
		echo $this->Form->input('other_phone_number');
		echo $this->Form->input('directory_1');
		echo $this->Form->input('directory_2');
		echo $this->Form->input('directory_3');
		echo $this->Form->input('title');
		echo $this->Form->input('work_fax_number');
		echo $this->Form->input('work_address_1');
		echo $this->Form->input('work_address_2');
		echo $this->Form->input('work_city');
		echo $this->Form->input('work_state');
		echo $this->Form->input('work_zip_code');
		echo $this->Form->input('work_country');
		echo $this->Form->input('home_address_1');
		echo $this->Form->input('home_address_2');
		echo $this->Form->input('home_city');
		echo $this->Form->input('home_state');
		echo $this->Form->input('home_zip_code');
		echo $this->Form->input('home_country');
		echo $this->Form->input('birthday');
		echo $this->Form->input('company');
		echo $this->Form->input('visibility');
		echo $this->Form->input('suffix');
		echo $this->Form->input('password');
		echo $this->Form->input('email2');
		echo $this->Form->input('email3');
		echo $this->Form->input('inbound_fax_alert_cutoff_dt');
		echo $this->Form->input('enable_cc_pay');
		echo $this->Form->input('receive_email');
		echo $this->Form->input('fax_sender_name');
		echo $this->Form->input('user_id');
		echo $this->Form->input('profile_picture_url');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-primary')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Customer.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Customer.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mail Packages'), array('controller' => 'mail_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mail Package'), array('controller' => 'mail_packages', 'action' => 'add')); ?> </li>
	</ul>
</div>

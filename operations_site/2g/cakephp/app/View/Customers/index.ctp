<h2>
	<?= $this->Html->image('icons/flat_miniset/png/256/smiley.png', array('style' => 'height: 3em;')) ?>
	<?= __('Customer Persons') ?>
</h2>
<ul class="nav nav-tabs">
	<li><?= $this->Html->link('Company', array('controller' => 'customerAccounts', 'action' => 'index')) ?></li>
	<li class="active"><?= $this->Html->link('Person', '#') ?></li>
</ul>

<div class="row-fluid">
	<!-- jump to customer id -->
	<div class="span2">
		<?= $this->Form->create('Customer', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'input-block-level'))) ?>
			<legend><?= __('Jump To') ?></legend>
			<?= $this->Form->input('id', array('type' => 'text', 'placeholder' => 'Customer Id #')) ?>
			<?= $this->Form->button('Go', array('class' => 'btn')) ?>
		<?= $this->Form->end() ?>
	</div>

	<!-- search & filter forms -->
	<div class="span10">
		<?= $this->Form->create('Customer', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false))) ?>
			<legend><?= __('Filter & Search') ?></legend>
			<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge')) ?>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>


<div class="row-fluid">
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('id', 'Customer Id #') ?></th>
				<th><?= __('Name (%s, %s, %s)', $this->Paginator->sort('first_name', 'First'), $this->Paginator->sort('middle_name', 'Middle'), $this->Paginator->sort('last_name','Last')) ?></th>
				<th><?= $this->Paginator->sort('cust_acct_id', 'Company') ?></th>
				<th><?= $this->Paginator->sort('nickname') ?></th>
				<th><?= $this->Paginator->sort('email') ?></th>
				<th><?= $this->Paginator->sort('work_phone_number') ?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($customers as $customer): ?>
			<tr>
				<td><strong class="big"><?= $this->Html->link($customer['Customer']['id'],array('action' => 'view', $customer['Customer']['id'])) ?></strong></td>
				<td class="full_name"><?= $this->Html->link($customer['Customer']['full_name'],array('action' => 'view', $customer['Customer']['id'])) ?></td>
				<td class="company"><?= $this->Html->link($customer['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $customer['CustomerAccount']['id'])) ?></td>
				<td class="nickname"><?= h($customer['Customer']['nickname']) ?>&nbsp;</td>
				<td class="email"><?= h($customer['Customer']['email']) ?>&nbsp;</td>
				<td class="work_phone_number"><?= h($customer['Customer']['work_phone_number']) ?>&nbsp;</td>
			</tr>
	<?php endforeach ?>
		</tbody>
	</table>
	<p> <?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?> </p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>


<style>
	.highlight { background-color: yellow }
</style>
<?= $this->Html->script('libs/jquery.highlight.min') ?>
<script>
	var query = <?= "\"{$this->request->data['Customer']['q']}\"" ?>;
	$("td.description").highlight(query);
	$("td.full_name").highlight(query);
	$("td.email").highlight(query);
	$("td.nickname").highlight(query);
	// $("td.company").highlight(query);
</script>

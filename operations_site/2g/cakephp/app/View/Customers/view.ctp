<div class="row-fluid">
	<div class="offset2 span8">
		<?= $this->Session->flash() ?>

		<!-- basic customer information -->
		<section class="module expandable">
			<div class="module-head"> <b><?= __('Basic Customer Information') ?></b> </div>
			<div class="module-body">
				<dl>
					<dt><?= __('Id') ?></dt>
					<dd><?= h($customer['Customer']['id']) ?>&nbsp;</dd>
					<dt><?= __('Customer Account') ?></dt>
					<dd><?= $this->Html->link($customer['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $customer['CustomerAccount']['id'])) ?>&nbsp;</dd>
					<dt><?= __('Salutation') ?></dt>
					<dd><?= h($customer['Customer']['salutation']) ?>&nbsp;</dd>
					<dt><?= __('First Name') ?></dt>
					<dd><?= h($customer['Customer']['first_name']) ?>&nbsp;</dd>
					<dt><?= __('Middle Name') ?></dt>
					<dd><?= h($customer['Customer']['middle_name']) ?>&nbsp;</dd>
					<dt><?= __('Last Name') ?></dt>
					<dd><?= h($customer['Customer']['last_name']) ?>&nbsp;</dd>
					<dt><?= __('Nickname') ?></dt>
					<dd><?= h($customer['Customer']['nickname']) ?>&nbsp;</dd>
					<dt><?= __('Email') ?></dt>
					<dd><?= h($customer['Customer']['email']) ?>&nbsp;</dd>
					<dt><?= __('Work Phone Number') ?></dt>
					<dd><?= h($customer['Customer']['work_phone_number']) ?>&nbsp;</dd>
					<dt><?= __('Note') ?></dt>
					<dd><?= h($customer['Customer']['note']) ?>&nbsp;</dd>
				</dl>
			</div>
		</section>

		<!-- edit basic customer info -->
		<section class="module expandable">
			<div class="module-head clickable"><b><?= __('Edit Basic Customer Information') ?></b></div>
			<div class="module-body" style="display:none">
				<?= $this->Form->create('Customer', array('action' => 'edit', 'inputDefaults' => array('class' => 'input-block-level'))) ?>
					<?= $this->Form->input('Customer.id') ?>
					<?= $this->Form->input('Customer.is_active') ?>
					<?//= $this->Form->input('Customer.cust_acct_id') // should not be editable? ?>

					<?= $this->Form->input('Customer.salutation', array('class' => 'input-mini', 'div' => false, 'label' => false, 'placeholder' => 'Salutation')) ?>
					<?= $this->Form->input('Customer.first_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'First Name')) ?>
					<?= $this->Form->input('Customer.middle_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'Middle Name')) ?>
					<?= $this->Form->input('Customer.last_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'Last Name')) ?>

					<?= $this->Form->input('Customer.nickname') ?>
					<?= $this->Form->input('Customer.cust_acct_id', array('options' => $cust_acct_id)) ?>

					<?= $this->Form->input('Customer.email') ?>
					<?= $this->Html->link('Change Customer Password', array('action' => 'changePassword', $customer['Customer']['id']), ['class' => 'btn btn-primary']) ?>
					<?= $this->Html->link('Activate BizCenter Account', array('action' => 'activateBizcenter', $customer['Customer']['id']), ['class' => 'btn btn-info']) ?>
					
					<?= $this->Form->input('Customer.work_phone_number') ?>
					<?= $this->Form->input('Customer.home_phone_number') ?>
					<?= $this->Form->input('Customer.cell_phone_number') ?>
					<?= $this->Form->input('Customer.note') ?>
					
					<?= $this->Form->hidden('Customer.next', array('value' => Router::url())) ?>
					<?= $this->Form->hidden('Customer.editing_section', array('value' => 'basic')) ?>

					<?= $this->Form->button('Update Customer Information', array('class' => 'btn')) ?>
					<?= $this->Form->button('Reset Form', array('type' => 'reset', 'class' => 'btn')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>

		<!-- additional customer details -->
		<section class="module expandable">
			<div class="module-head clickable"> <b><?= __('Additional Customer Details') ?></b> </div>
			<div class="module-body" style="display:none">
				<dl>
					<dt><?= __('Home Phone Number') ?></dt>
					<dd><?= h($customer['Customer']['home_phone_number']) ?>&nbsp;</dd>
					<dt><?= __('Cell Phone Number') ?></dt>
					<dd><?= h($customer['Customer']['cell_phone_number']) ?>&nbsp;</dd>
					<dt><?= __('Creation Dt') ?></dt>
					<dd><?= h($customer['Customer']['creation_dt']) ?>&nbsp;</dd>
					<dt><?= __('Is Active') ?></dt>
					<dd><?= h($customer['Customer']['is_active'] ? 'yes' : 'no') ?>&nbsp;</dd>
					<dt><?= __('Note') ?></dt>
					<dd><?= h($customer['Customer']['note']) ?>&nbsp;</dd>
					<dt><?= __('Other Phone Number') ?></dt>
					<dd><?= h($customer['Customer']['other_phone_number']) ?>&nbsp;</dd>
					<dt><?= __('Directory 1') ?></dt>
					<dd><?= h($customer['Customer']['directory_1']) ?>&nbsp;</dd>
					<dt><?= __('Directory 2') ?></dt>
					<dd><?= h($customer['Customer']['directory_2']) ?>&nbsp;</dd>
					<dt><?= __('Directory 3') ?></dt>
					<dd><?= h($customer['Customer']['directory_3']) ?>&nbsp;</dd>
					<dt><?= __('Title') ?></dt>
					<dd><?= h($customer['Customer']['title']) ?>&nbsp;</dd>
					<dt><?= __('Work Fax Number') ?></dt>
					<dd><?= h($customer['Customer']['work_fax_number']) ?>&nbsp;</dd>
					<dt><?= __('Work Address 1') ?></dt>
					<dd><?= h($customer['Customer']['work_address_1']) ?>&nbsp;</dd>
					<dt><?= __('Work Address 2') ?></dt>
					<dd><?= h($customer['Customer']['work_address_2']) ?>&nbsp;</dd>
					<dt><?= __('Work City') ?></dt>
					<dd><?= h($customer['Customer']['work_city']) ?>&nbsp;</dd>
					<dt><?= __('Work State') ?></dt>
					<dd><?= h($customer['Customer']['work_state']) ?>&nbsp;</dd>
					<dt><?= __('Work Zip Code') ?></dt>
					<dd><?= h($customer['Customer']['work_zip_code']) ?>&nbsp;</dd>
					<dt><?= __('Work Country') ?></dt>
					<dd><?= h($customer['Customer']['work_country']) ?>&nbsp;</dd>
					<dt><?= __('Home Address 1') ?></dt>
					<dd><?= h($customer['Customer']['home_address_1']) ?>&nbsp;</dd>
					<dt><?= __('Home Address 2') ?></dt>
					<dd><?= h($customer['Customer']['home_address_2']) ?>&nbsp;</dd>
					<dt><?= __('Home City') ?></dt>
					<dd><?= h($customer['Customer']['home_city']) ?>&nbsp;</dd>
					<dt><?= __('Home State') ?></dt>
					<dd><?= h($customer['Customer']['home_state']) ?>&nbsp;</dd>
					<dt><?= __('Home Zip Code') ?></dt>
					<dd><?= h($customer['Customer']['home_zip_code']) ?>&nbsp;</dd>
					<dt><?= __('Home Country') ?></dt>
					<dd><?= h($customer['Customer']['home_country']) ?>&nbsp;</dd>
					<dt><?= __('Birthday') ?></dt>
					<dd><?= h($customer['Customer']['birthday']) ?>&nbsp;</dd>
					<dt><?= __('Company') ?></dt>
					<dd><?= h($customer['Customer']['company']) ?>&nbsp;</dd>
					<dt><?= __('Visibility') ?></dt>
					<dd><?= h($customer['Customer']['visibility']) ?>&nbsp;</dd>
					<dt><?= __('Suffix') ?></dt>
					<dd><?= h($customer['Customer']['suffix']) ?>&nbsp;</dd>
					<dt><?= __('Email2') ?></dt>
					<dd><?= h($customer['Customer']['email2']) ?>&nbsp;</dd>
					<dt><?= __('Email3') ?></dt>
					<dd><?= h($customer['Customer']['email3']) ?>&nbsp;</dd>
					<dt><?= __('Inbound Fax Alert Cutoff Dt') ?></dt>
					<dd><?= h($customer['Customer']['inbound_fax_alert_cutoff_dt']) ?>&nbsp;</dd>
					<dt><?= __('Enable Cc Pay') ?></dt>
					<dd><?= h($customer['Customer']['enable_cc_pay'] ? 'yes' : 'no') ?>&nbsp;</dd>
					<dt><?= __('Receive Email') ?></dt>
					<dd><?= h($customer['Customer']['receive_email'] ? 'yes' : 'no') ?>&nbsp;</dd>
					<dt><?= __('Fax Sender Name') ?></dt>
					<dd><?= h($customer['Customer']['fax_sender_name']) ?>&nbsp;</dd>
				</dl>
			</div>
		</section>

					
		<!-- edit additional customer details -->
		<section class="module expandable">
			<div class="module-head clickable"><b><?= __('Edit Additional Customer Details') ?></b></div>
			<div class="module-body" style="display:none;">
				<?= $this->Form->create('Customer', array('action' => 'edit', 'inputDefaults' => array('class' => 'input-block-level'))) ?>
					<?= $this->Form->input('Customer.id') ?>
					<?= $this->Form->input('Customer.register_with_building') ?>
					<?= $this->Form->input('Customer.other_phone_number') ?>
					<?= $this->Form->input('Customer.directory_1') ?>
					<?= $this->Form->input('Customer.directory_2') ?>
					<?= $this->Form->input('Customer.directory_3') ?>
					<?= $this->Form->input('Customer.title') ?>
					<?= $this->Form->input('Customer.work_fax_number') ?>
					<?= $this->Form->input('Customer.work_address_1') ?>
					<?= $this->Form->input('Customer.work_address_2') ?>
					<?= $this->Form->input('Customer.work_city') ?>
					<?= $this->Form->input('Customer.work_state') ?>
					<?= $this->Form->input('Customer.work_zip_code') ?>
					<?= $this->Form->input('Customer.work_country') ?>
					<?= $this->Form->input('Customer.home_address_1') ?>
					<?= $this->Form->input('Customer.home_address_2') ?>
					<?= $this->Form->input('Customer.home_city') ?>
					<?= $this->Form->input('Customer.home_state') ?>
					<?= $this->Form->input('Customer.home_zip_code') ?>
					<?= $this->Form->input('Customer.home_country') ?>
					<?= $this->Form->input('Customer.birthday') ?>
					<?//= $this->Form->input('Customer.company') // deprecated field? ?>
					<?= $this->Form->input('Customer.visibility') ?>
					<?= $this->Form->input('Customer.suffix') ?>
					<?= $this->Form->input('Customer.email2', array('label' => 'Email 2')) ?>
					<?= $this->Form->input('Customer.email3', array('label' => 'Email 3')) ?>
					<?= $this->Form->input('Customer.inbound_fax_alert_cutoff_dt', array('label' => 'Inbound Fax Alert Cutoff DateTime')) ?>
					<?= $this->Form->input('Customer.enable_cc_pay', array('label' => 'Enable Credit Card Payment')) ?>
					<?= $this->Form->input('Customer.receive_email') ?>
					<?= $this->Form->input('Customer.fax_sender_name') ?>
					<?= $this->Form->input('Customer.profile_picture_url') ?>
					<?= $this->Form->hidden('Customer.editing_section', array('value' => 'additional')) ?>
					<?= $this->Form->button('Update Customer Details', array('class' => 'btn')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>

	</div>
</div>

<script type="text/javascript">
// auto-expand
$(".expandable").each(function(i) {
	this.setAttribute("id","expandable-"+i);
	var section="#expandable-"+i;
	$(section+" div.module-head").addClass("clickable").click(function(){
		$(section+" div.module-body").toggle();
	});
});
</script>

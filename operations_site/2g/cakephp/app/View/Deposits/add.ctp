<!-- breadcrumb trail -->
<ul class="breadcrumb">
	<li><a href="{{ operations_2g_urlprefix }}/">Dashboard</a><span class="divider">/</span></li>
	<li><a href="{{ operations_2g_urlprefix }}/deposits">Deposits</a><span class="divider">/</span></li>
	<li class="active">Make Deposit</li>
</ul>


<?= $this->Form->create('Deposit', array()); ?>

<!-- list of received payments -->
<div class="row-fluid">
	<legend>1. Choose pending received payment(s)</legend>
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th><input class="checkall" id="" type="checkbox" title="Check All" /></th>
				<th>Customer</th>
				<th>Employee</th>
				<th>Amount</th>
				<th>Received</th>
				<th>Check Date</th>
				<th>Method</th>
				<th>Mode</th>
				<th>Status</th>
				<th class="actions">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($receivedPayments as $i => $receivedPayment): ?>
			<?php $allow_deposit = $receivedPayment['ReceivedPayment']['process_txn_after_dt'] < date(DATETIME_FORMAT) ?>
			<tr>
				<td>
					<?php if ($allow_deposit): ?>
						<?= $this->Form->hidden("ReceivedPayment.{$i}.id", array('value' => $receivedPayment['ReceivedPayment']['id'])) ?>
						<?= $this->Form->input("ReceivedPayment.{$i}.is_used", array('class' => 'checkable', 'type' => 'checkbox', 'label' => false)) ?>
					<?php else: ?>
						<i class="red">wait</i>
					<?php endif ?>
				</td>
				<td><?= $this->Html->link($receivedPayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $receivedPayment['CustomerAccount']['id'])); ?></td>
				<td><?= $this->Html->link($receivedPayment['Employee']['first_name'], array('controller' => 'employees', 'action' => 'view', $receivedPayment['Employee']['first_name'])); ?></td>
				<td><?= h($receivedPayment['ReceivedPayment']['amount']); ?>&nbsp;</td>
				<td><?= h($receivedPayment['ReceivedPayment']['recv_dt']); ?>&nbsp;</td>
				<td>
					<?php if (!$allow_deposit): ?>
						<i class="red"><?= h($receivedPayment['ReceivedPayment']['process_txn_after_dt']); ?></i>
					<?php else: ?>
						<?= h($receivedPayment['ReceivedPayment']['process_txn_after_dt']); ?>
					<?php endif ?>
					&nbsp;
				</td>
				<td><?= h($receivedPayment['ReceivedPayment']['payment_method']); ?>&nbsp;</td>
				<td><?= h($receivedPayment['ReceivedPayment']['process_mode']); ?>&nbsp;</td>
				<td><?= h($receivedPayment['ReceivedPayment']['process_status']); ?>&nbsp;</td>
				<td class="actions">
					<?= $this->Html->link(
						__('Cancel'),
						array('controller' => 'receivedPayments', 'action' => 'cancel', $receivedPayment['ReceivedPayment']['id'], '?' => array('next' => Router::url()))
				); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>

<!-- deposit form -->
<div class="row-fluid">
	<legend>2. Enter deposit details</legend>
	<fieldset>
		<!-- FIXME editable? -->
		<?= $this->Form->input('employee_id', array('options' => @$employees, 'value' => AuthComponent::user('id'))) ?>
		<?= $this->Form->input('deposit_dt', array('type' => 'text', 'class' => 'datetime', 'value' => date(DATETIME_FORMAT))) ?>
		<?= $this->Form->input('bank_account', array('options' => @$bankAccounts)) ?>
		<?= $this->Form->input('note') ?>
			<?= $this->Form->button('Submit', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end(); ?>
	</fieldset>
</div>

<script>
	// checkall
	$(function () {
		$('.checkall').click(function() {
			$('.checkable').not(this).not(":disabled").prop('checked', this.checked);
		});
	});
</script>
<style>
	.red { color: #c00; }
</style>

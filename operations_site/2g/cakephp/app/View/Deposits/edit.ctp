<div class="deposits edit">
<fieldset>
		<!-- FIXME editable? -->
		<?= $this->Form->input('employee_id', array('options' => @$employees, 'value' => AuthComponent::user('id'))) ?>
		<?= $this->Form->input('deposit_dt', array('type' => 'text', 'class' => 'datetime', 'value' => date(DATETIME_FORMAT))) ?>
		<?= $this->Form->input('bank_account', array('options' => @$bankAccounts)) ?>
		<?= $this->Form->input('note') ?>
			<?= $this->Form->button('Submit', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end(); ?>
	</fieldset>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Deposit.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Deposit.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Deposits'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Received Payments'), array('controller' => 'received_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Received Payment'), array('controller' => 'received_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>

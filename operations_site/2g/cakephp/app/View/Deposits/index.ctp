<h2><?= __('Deposits') ?></h2>

<?= $this->Html->link('Make Deposit', array('action' => 'add'), array('class' => 'btn btn-primary')) ?>

<div class="deposits index">
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('deposit_dt', 'Deposited') ?></th>
				<th><?= $this->Paginator->sort('bank_name') ?></th>
				<th><?= $this->Paginator->sort('bank_account_number', 'Account') ?></th>
				<th class="stretch"><?= $this->Paginator->sort('note') ?></th>
				<!-- <th><?= $this->Paginator->sort('employee_id') ?></th> -->
				<th class="actions"><?= __('Actions') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($deposits as $deposit): ?>
				<tr>
					<td class="shrink nowrap"><?= date('m/d/Y (g:ia)', strtotime($deposit['Deposit']['deposit_dt'])) ?>&nbsp;</td>
					<td class="shrink nowrap"><?= h($deposit['Deposit']['bank_name']) ?>&nbsp;</td>
					<td class="shrink nowrap"><?= '*****' . substr($deposit['Deposit']['bank_account_number'], -4) ?>&nbsp;</td>
					<td class="stretch"><?= h($deposit['Deposit']['note']) ?>&nbsp;</td>
					<!-- <td class="shrink nowrap"><?= $this->Html->link($deposit['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $deposit['Deposit']['employee_id'])) ?> </td> -->
					<td class="actions">
						<?= $this->Html->link(__('View'), array('action' => 'view', $deposit['Deposit']['id'])) ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<?= $this->element('paging') ?>
</div>


<!-- breadcrumb trail -->
<ul class="breadcrumb">
	<li><a href="{{ operations_2g_urlprefix }}/">Dashboard</a><span class="divider">/</span></li>
	<li><a href="{{ operations_2g_urlprefix }}/deposits">Deposits</a><span class="divider">/</span></li>
	<li class="active">Deposit Details</li>
</ul>

<div class="deposits view">
<h2><?= __('Deposit'); ?></h2>
	<dl>
		<dt><?= __('Employee'); ?></dt>
		<dd><?= $this->Html->link($deposit['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $deposit['Employee']['id'])); ?> &nbsp; </dd>
		<dt><?= __('Deposit Dt'); ?></dt>
		<dd><?= h($deposit['Deposit']['deposit_dt']); ?> &nbsp; </dd>
		<dt><?= __('Bank Account Number'); ?></dt>
		<dd><?= '*****' . substr($deposit['Deposit']['bank_account_number'], -4); ?> &nbsp; </dd>
		<dt><?= __('Note'); ?></dt>
		<dd><?= h($deposit['Deposit']['note']); ?> &nbsp; </dd>
		<dt><?= __('Creation Dt'); ?></dt>
		<dd><?= h($deposit['Deposit']['creation_dt']); ?> &nbsp; </dd>
		<dt><?= __('Bank Name'); ?></dt>
		<dd><?= h($deposit['Deposit']['bank_name']); ?> &nbsp; </dd>
		<dt><?= __('Modifier Id'); ?></dt>
		<dd><?= h($deposit['Deposit']['modifier_id']); ?> &nbsp; </dd>
		<dt><?= __('Modified'); ?></dt>
		<dd><?= h($deposit['Deposit']['modified']); ?> &nbsp; </dd>
	</dl>
</div>
<div class="related">
	<h3><?= __('Related Received Payments'); ?></h3>
	<?php if (!empty($deposit['ReceivedPayment'])): ?>
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th><?= __('Id'); ?></th>
				<th><?= __('Customer Account'); ?></th>
				<th><?= __('Employee'); ?></th>
				<th><?= __('Amount'); ?></th>
				<th><?= __('Recv Dt'); ?></th>
				<th><?= __('Payment Method'); ?></th>
				<th><?= __('Invoice Number'); ?></th>
				<th><?= __('Note'); ?></th>
				<th><?= __('Check Name'); ?></th>
				<th><?= __('Check Number'); ?></th>
				<th class="actions"><?= __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($deposit['ReceivedPayment'] as $receivedPayment): ?>
			<tr>
				<td><?= $receivedPayment['id']; ?></td>
				<td><?= $receivedPayment['CustomerAccount']['name']; ?></td>
				<td><?= $receivedPayment['Employee']['full_name']; ?></td>
				<td><?= $receivedPayment['amount']; ?></td>
				<td><?= $receivedPayment['recv_dt']; ?></td>
				<td><?= $receivedPayment['payment_method']; ?></td>
				<td><?= $receivedPayment['invoice_number']; ?></td>
				<td><?= $receivedPayment['note']; ?></td>
				<td><?= $receivedPayment['check_name']; ?></td>
				<td><?= $receivedPayment['check_number']; ?></td>
				<td class="actions">
					<?= $this->Html->link(__('Edit'), array('controller' => 'receivedPayments', 'action' => 'edit', $receivedPayment['id'])) ?>
					<?= $this->Form->postLink(
						__('Remove'),
						array(
							'controller' => 'received_payments',
							'action' => 'removeFromDeposit',
							$receivedPayment['id'],
							'?' => array('next' => Router::url(null,true))
						),
						null,
						"Are you sure you want to remove this payment from this deposit?\n" . 
						"It will be reverted from \"completed\" to \"pending\""
					) ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>

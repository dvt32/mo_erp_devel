<div class="deskAssignmentHistories form">
<?php echo $this->Form->create('DeskAssignmentHistory'); ?>
	<fieldset>
		<legend><?php echo __('Edit Desk Assignment History'); ?></legend>
	<?php
		echo $this->Form->input('desk_id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('cust_pers_id');
		echo $this->Form->input('start_date');
		echo $this->Form->input('end_date');
		echo $this->Form->input('current_price');
		echo $this->Form->input('note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('DeskAssignmentHistory.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('DeskAssignmentHistory.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Desk Assignment Histories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Desks'), array('controller' => 'desks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Desk'), array('controller' => 'desks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

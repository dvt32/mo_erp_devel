<div class="deskAssignmentHistories index">
	<h2><?php echo __('Desk Assignment Histories'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('desk_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cust_acct_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cust_pers_id'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('end_date'); ?></th>
			<th><?php echo $this->Paginator->sort('current_price'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($deskAssignmentHistories as $deskAssignmentHistory): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($deskAssignmentHistory['Desk']['id'], array('controller' => 'desks', 'action' => 'view', $deskAssignmentHistory['Desk']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($deskAssignmentHistory['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $deskAssignmentHistory['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($deskAssignmentHistory['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $deskAssignmentHistory['Customer']['id'])); ?>
		</td>
		<td><?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['end_date']); ?>&nbsp;</td>
		<td><?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['current_price']); ?>&nbsp;</td>
		<td><?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['note']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $deskAssignmentHistory['DeskAssignmentHistory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $deskAssignmentHistory['DeskAssignmentHistory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $deskAssignmentHistory['DeskAssignmentHistory']['id']), null, __('Are you sure you want to delete # %s?', $deskAssignmentHistory['DeskAssignmentHistory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Desk Assignment History'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Desks'), array('controller' => 'desks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Desk'), array('controller' => 'desks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

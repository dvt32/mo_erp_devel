<div class="deskAssignmentHistories view">
<h2><?php echo __('Desk Assignment History'); ?></h2>
	<dl>
		<dt><?php echo __('Desk'); ?></dt>
		<dd>
			<?php echo $this->Html->link($deskAssignmentHistory['Desk']['id'], array('controller' => 'desks', 'action' => 'view', $deskAssignmentHistory['Desk']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($deskAssignmentHistory['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $deskAssignmentHistory['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($deskAssignmentHistory['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $deskAssignmentHistory['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Date'); ?></dt>
		<dd>
			<?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['end_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Current Price'); ?></dt>
		<dd>
			<?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['current_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($deskAssignmentHistory['DeskAssignmentHistory']['note']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Desk Assignment History'), array('action' => 'edit', $deskAssignmentHistory['DeskAssignmentHistory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Desk Assignment History'), array('action' => 'delete', $deskAssignmentHistory['DeskAssignmentHistory']['id']), null, __('Are you sure you want to delete # %s?', $deskAssignmentHistory['DeskAssignmentHistory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Desk Assignment Histories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Desk Assignment History'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Desks'), array('controller' => 'desks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Desk'), array('controller' => 'desks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

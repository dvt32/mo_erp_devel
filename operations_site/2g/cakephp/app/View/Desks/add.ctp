<div class="desks form">
<?php echo $this->Form->create('Desk'); ?>
	<fieldset>
		<legend><?php echo __('Add Desk'); ?></legend>
	<?php
		echo $this->Form->input('floor_id');
		echo $this->Form->input('assigned_number');
		echo $this->Form->input('account_id');
		echo $this->Form->input('num_people');
		echo $this->Form->input('is_enclosed');
		echo $this->Form->input('list_price');
		echo $this->Form->input('when_available');
		echo $this->Form->input('bounding_polygon');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('is_active');
		echo $this->Form->input('waiting_account');
		echo $this->Form->input('current_price');
		echo $this->Form->input('start_date');
		echo $this->Form->input('end_date');
		echo $this->Form->input('internet_telephone_revenue');
		echo $this->Form->input('increase_date');
		echo $this->Form->input('cust_pers_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-primary')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Desks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Desk Assignment Histories'), array('controller' => 'desk_assignment_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Desk Assignment History'), array('controller' => 'desk_assignment_histories', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<ul class="breadcrumb">
			  <li><a href="{{ operations_2g_urlprefix }}/buildings"><?=  $desk['Floor']['Building']['city'] ?></a> <span class="divider">/</span></li>
			  <li><a href="{{ operations_2g_urlprefix }}/buildings/view/<?= $desk['Floor']['building_id'] ?>"><?= $desk['Floor']['Building']['address_1'] ?></a> <span class="divider">/</span
			  ></li>
			  <li><a href="{{ operations_2g_urlprefix }}/floors/view/<?= $desk['Desk']['floor_id'] ?>"><?= $desk['Floor']['label'] ?></a> <span class="divider">/</span></li>
			  <li><a href="{{ operations_2g_urlprefix }}/desks/view/<?= $desk['Desk']['id'] ?>"> Desk <?= $desk['Desk']['assigned_number'] ?></a> <span class="divider">/</span></li>
			  <li class="active">Assign this desk</li>
			</ul>
		</div>
	</div>
</div>
<div class="desks form">
<?php echo $this->Form->create('Desk'); ?>
	<fieldset>
		<legend><?php echo __('Assign this Desk'); ?></legend>
		<div class="span4">
	<?php
		echo $this->Form->hidden('id');
		echo $this->Form->hidden('floor_id');
		echo $this->Form->hidden('assigned_number');
		echo $this->Form->input('account_id', array('class' => 'chosen input-block-level', 'options' => $customerAccounts, 'empty' => '-- Company Accounts --'));
		// echo $this->Form->input('cust_pers_id', array('label' => 'Primary Customer Name', 'options' => $customers, 'empty' => '-- Customer Persons --'));
		echo $this->Form->input('cust_pers_id', array('class' => 'chosen input-block-level', 'options' => $customers, 'empty' => '-- Primary Contact --'));
		echo $this->Form->input('num_people',  array('label' => 'Number of Persons'));
		echo $this->Form->hidden('is_enclosed');
		echo $this->Form->input('list_price');
		echo $this->Form->input('when_available', array('type' => 'text', 'class' => 'datetime'));
		echo $this->Form->input('note');
		echo $this->Form->hidden('is_active');
		echo $this->Form->input('waiting_account', array('options' => $customerAccounts, 'empty' => '-- Company Accounts --'));
		echo $this->Form->input('current_price');
		echo $this->Form->input('start_date', array('type' => 'text', 'class' => 'datetime'));
		echo $this->Form->input('end_date', array('type' => 'text', 'class' => 'datetime'));
		echo $this->Form->input('internet_telephone_revenue');
		echo $this->Form->input('increase_date', array('type' => 'text', 'class' => 'datetime'));
	?>
</div>
	</fieldset>
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<div class="desks index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/addons/256/boxes_checkmark.png', array('style' => 'height: 3em;')) ?>
		<?= __('Desks'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				<legend>Filter &amp; Search</legend>
				<!-- filter and search -->
				<?= $this->Form->create('Desk', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
				<div class="span8">

					<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge search-query')) ?>
					<?= $this->Form->input('floor_id', array(
						'options' => $floors, 
						'empty' => '-- Floor --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>

					<?= $this->Form->input('is_enclosed', array(
						'options' => array('Not Enclosed', 'Enclosed'), 
						'empty' => '-- Enclosed --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>

					<?= $this->Form->input('is_taken', array(
						'options' => array(
							'Vacant',
							'Occupied'
						), 
						'empty' => '-- Occupied --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>
					
						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
						<?= $this->Form->end() ?>
					</div>
				</div>
			</div>

	</div>
	<br/>
	<!-- /search & filter forms -->
	<table class="table table-condensed table-striped">
	<tr>
			<th><?= $this->Paginator->sort('assigned_number'); ?></th>
			<th><?= $this->Paginator->sort('account_id'); ?></th>
			<th><?= $this->Paginator->sort('floor_id'); ?></th>
			<th><?= $this->Paginator->sort('num_people', 'Persons'); ?></th>
			<th><?= $this->Paginator->sort('is_enclosed'); ?></th>
			<th><?= $this->Paginator->sort('list_price'); ?></th>
			<th><?= $this->Paginator->sort('current_price'); ?></th>
			<th><?= $this->Paginator->sort('internet_telephone_revenue'); ?></th>
			<th><?= $this->Paginator->sort('when_available', 'Availability'); ?></th>
			<th><?= $this->Paginator->sort('start_date'); ?></th>
			<th><?= $this->Paginator->sort('end_date'); ?></th>
			<th><?= $this->Paginator->sort('increase_date'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($desks as $desk): ?>
	<tr>
		<td><?= h($desk['Desk']['assigned_number']); ?>&nbsp;</td>
		<td>
			<?= ($desk['Desk']['account_id'])? $this->Html->link($desk['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $desk['CustomerAccount']['id'])) : " - Vacant - ";

			 ?>
		</td>
		<td><?= h($desk['Floor']['short_global_label']); ?>&nbsp;</td>
		<td><?= h($desk['Desk']['num_people']); ?>&nbsp;</td>
		<td><?= ($desk['Desk']['is_enclosed'])? "Enclosed" : "Open"; ?>&nbsp;</td>
		<td><?= h($desk['Desk']['list_price']); ?>&nbsp;</td>
		<td><?= h($desk['Desk']['current_price']); ?>&nbsp;</td>
		<td><?= h($desk['Desk']['internet_telephone_revenue']); ?>&nbsp;</td>
		<td><?= h($desk['Desk']['when_available']); ?>&nbsp;</td>
		<td><?= h($desk['Desk']['start_date']); ?>&nbsp;</td>
		<td><?= h($desk['Desk']['end_date']); ?>&nbsp;</td>
		<td><?= h($desk['Desk']['increase_date']); ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('View'), array('action' => 'view', $desk['Desk']['id'])); ?>
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $desk['Desk']['id'])); ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $desk['Desk']['id']), null, __('Are you sure you want to delete # %s?', $desk['Desk']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

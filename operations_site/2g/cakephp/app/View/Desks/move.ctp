<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<ul class="breadcrumb">
			  <li><a href="{{ operations_2g_urlprefix }}/buildings"><?=  $desk['Floor']['Building']['city'] ?></a> <span class="divider">/</span></li>
			  <li><a href="{{ operations_2g_urlprefix }}/buildings/view/<?= $desk['Floor']['building_id'] ?>"><?= $desk['Floor']['Building']['address_1'] ?></a> <span class="divider">/</span
			  ></li>
			  <li><a href="{{ operations_2g_urlprefix }}/floors/view/<?= $desk['Desk']['floor_id'] ?>"><?= $desk['Floor']['label'] ?></a> <span class="divider">/</span></li>
			  <li><a href="{{ operations_2g_urlprefix }}/desks/view/<?= $desk['Desk']['id'] ?>"> Desk <?= $desk['Desk']['assigned_number'] ?></a> <span class="divider">/</span></li>
			  <li class="active">Move Out</li>
			</ul>
		</div>
	</div>
</div>
<div class="deskAssignmentHistories form">
<?php echo $this->Form->create('DeskAssignmentHistory'); ?>
	<fieldset>
		<legend><?php echo __('Move this Desk Out'); ?></legend>
	<?php
		echo $this->Form->hidden('desk_id', array('type' => 'text', 'value' => $deskAssignmentHistory['DeskAssignmentHistory']['desk_id']));
		echo $this->Form->input('cust_acct_id', array('options' => $customerAccounts, 'empty' =>  '-- Person --', 'value' => $deskAssignmentHistory['DeskAssignmentHistory']['cust_acct_id']));
		echo $this->Form->input('cust_pers_id', array('options' => $customers, 'empty' =>  '-- Person --', 'value' => $deskAssignmentHistory['DeskAssignmentHistory']['cust_pers_id']));
		echo $this->Form->input('start_date', array('type' => 'text', 'value' => $deskAssignmentHistory['DeskAssignmentHistory']['start_date'], 'class' => 'datetime'));
		echo $this->Form->input('end_date', array('type' => 'text', 'value' => $deskAssignmentHistory['DeskAssignmentHistory']['end_date'], 'class' => 'datetime'));
		echo $this->Form->input('current_price', array('type' => 'text', 'value' => $deskAssignmentHistory['DeskAssignmentHistory']['current_price']));
		echo $this->Form->input('note', array('type' => 'text', 'value' => $deskAssignmentHistory['DeskAssignmentHistory']['note']));
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<ul class="breadcrumb">
			  <li><a href="{{ operations_2g_urlprefix }}/buildings"><?=  $desk['Floor']['Building']['city'] ?></a> <span class="divider">/</span></li>
			  <li><a href="{{ operations_2g_urlprefix }}/buildings/view/<?= $desk['Floor']['building_id'] ?>"><?= $desk['Floor']['Building']['address_1'] ?></a> <span class="divider">/</span
			  ></li>
			  <li><a href="{{ operations_2g_urlprefix }}/floors/view/<?= $desk['Desk']['floor_id'] ?>"><?= $desk['Floor']['label'] ?></a> <span class="divider">/</span></li>
			  <li class="active">Desk <?= $desk['Desk']['assigned_number'] ?></li>
			</ul>
		</div>
	</div>
</div>

<div class="desks view">
<h2><?= __('Desk'); ?></h2>
	<dl>
		<dt><?= __('Id'); ?></dt>
		<dd>
			<?= h($desk['Desk']['id']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Floor'); ?></dt>
		<dd>
			<?= $this->Html->link($desk['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $desk['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Assigned Number'); ?></dt>
		<dd>
			<?= h($desk['Desk']['assigned_number']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Customer Account'); ?></dt>
		<dd>
			<?= $this->Html->link($desk['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $desk['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Num People'); ?></dt>
		<dd>
			<?= h($desk['Desk']['num_people']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Is Enclosed'); ?></dt>
		<dd>
			<?= h($desk['Desk']['is_enclosed']); ?>
			&nbsp;
		</dd>
		<dt><?= __('List Price'); ?></dt>
		<dd>
			<?= h($desk['Desk']['list_price']); ?>
			&nbsp;
		</dd>
		<dt><?= __('When Available'); ?></dt>
		<dd>
			<?= h($desk['Desk']['when_available']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Note'); ?></dt>
		<dd>
			<?= h($desk['Desk']['note']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Creation Dt'); ?></dt>
		<dd>
			<?= h($desk['Desk']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Is Active'); ?></dt>
		<dd>
			<?= h($desk['Desk']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Waiting Account'); ?></dt>
		<dd>
			<?= h($desk['Desk']['waiting_account']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Current Price'); ?></dt>
		<dd>
			<?= h($desk['Desk']['current_price']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Start Date'); ?></dt>
		<dd>
			<?= h($desk['Desk']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?= __('End Date'); ?></dt>
		<dd>
			<?= h($desk['Desk']['end_date']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Internet Telephone Revenue'); ?></dt>
		<dd>
			<?= h($desk['Desk']['internet_telephone_revenue']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Increase Date'); ?></dt>
		<dd>
			<?= h($desk['Desk']['increase_date']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Customer'); ?></dt>
		<dd>
			<?= $this->Html->link($desk['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $desk['Customer']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
<ul class="nav nav-pills">
	<li class="active">
		<?= $this->Form->postLink(__('Delete Desk'), array(
			'action' => 'delete', 
			$desk['Desk']['id']), 
			null, __('Are you sure you want to delete # %s?', 
			$desk['Desk']['id'])); 
		?>
	</li>
	<li class="active">
		<?= $this->Html->link(__('Edit Desk'), array('action' => 'edit', $desk['Desk']['id'])); ?>
	</li>
	<?php if (!empty($desk['Desk']['account_id'])): ?>
	<li class="active">
		<?= $this->Html->link(__('Space Move Out'), array('action' => 'move', $desk['Desk']['id'])); ?>
	</li>
	<?php else : ?>
	<li class="active">
		<?= $this->Html->link(__('Assign this Desk'), array('action' => 'edit', $desk['Desk']['id'])); ?>
	</li>
	<?php endif ?>

</ul>


</div>

<div class="related">
	<h3><?= __('Past Desk Assignments'); ?></h3>
	<?php if (!empty($desk['DeskAssignmentHistory'])): ?>
	<table class="table table-condensed">
	<tr>
		<th><?= __('Cust Acct Id'); ?></th>
		<th><?= __('Cust Pers Id'); ?></th>
		<th><?= __('Start Date'); ?></th>
		<th><?= __('End Date'); ?></th>
		<th><?= __('Current Price'); ?></th>
		<th><?= __('Note'); ?></th>
		<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($desk['DeskAssignmentHistory'] as $deskAssignmentHistory): ?>

		<tr>
			<td>
				<?= @$this->Html->link($deskAssignmentHistory['CustomerAccount']['name'], array('controller' => 'customerAccounts', 'action' => 'view', $deskAssignmentHistory['CustomerAccount']['id'])); ?>
			</td>
			<td>
				<?= @$this->Html->link($deskAssignmentHistory['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $deskAssignmentHistory['Customer']['id'])); ?>
			</td>
			<td><?= $deskAssignmentHistory['start_date']; ?></td>
			<td><?= $deskAssignmentHistory['end_date']; ?></td>
			<td><?= $deskAssignmentHistory['current_price']; ?></td>
			<td><?= $deskAssignmentHistory['note']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php else: ?>
	<div class="hero-unit">
		<h1>No Desk History</h1>
	</div>
	<?php endif; ?>
</div>

<ul class="nav pull-right">
  <?php if (AuthComponent::user('id') == null): ?>
    <li><?= $this->Html->link('Log In', array('controller' => 'employees', 'action' => 'login')) ?></li>
  <?php else: ?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= AuthComponent::user('username') ?> <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><?= $this->Html->link('Dashboard', array('controller' => 'employees', 'action' => 'dashboard')) ?></li>
      <li><?= $this->Html->link('Log Out', array('controller' => 'employees', 'action' => 'logout')) ?></li>
    </ul>
  </li>
  <?php endif ?>
</ul>


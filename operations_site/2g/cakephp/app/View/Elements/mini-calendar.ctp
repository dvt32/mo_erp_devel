<?php
$month = @$month ?: date('m');
$year = @$year ?: date('Y');
$dateFormat = @$dateFormat ?: 'Y-m-d';
$linkFormat = @$linkFormat ?: '%s';

// first of month
$fom_dt = DateTime::createFromFormat('Y-m-d H:i:s', "{$year}-{$month}-01 03:00:00");
$fom = $fom_dt->format('U');
?>
<?= date('M Y', $fom) ?>
<table>
	<thead>
		<tr>
			<th>S</th>
			<th>M</th>
			<th>T</th>
			<th>W</th>
			<th>T</th>
			<th>F</th>
			<th>S</th>
		</tr>
	</thead>
	<tbody>
	<?php
		echo "<tr>";

		// blanks at beginning
		for ($i = 0; $i < date('w', $fom); $i++) {
			echo "<td>&nbsp;</td>";
		}
		
		// days of the month
		for ($d = 1; $d <= date('t', $fom); $d++) {
			
			$time = $fom + ($d-1) * 86400;
			$date = date($dateFormat, $time);
			$dow = date('w',$time);

			echo $dow=='0' ? "<tr>" : '';
			echo "<td>" . $this->Html->link($d, sprintf($linkFormat, date($dateFormat, $time))) . "</td>";
			echo $dow=='6' ? "</tr>" : '';
		}
		
		// blanks at end
		for ($i = $dow; $i < 6; $i++) {
			echo "<td>&nbsp;</td>";
		}

		echo "</tr>";
	?>
	</tbody>
</table>

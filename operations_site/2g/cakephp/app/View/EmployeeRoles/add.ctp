<?php $id = @$this->request->pass[0] ?>
<ul class="nav nav-tabs">
	<li class="active"><?= $this->Html->link('Create New Role', '#') ?></li>
	<li><?= $this->Html->link('Reassign Existing Role', array('controller' => 'employeeRoles', 'action' => 'reassign')) ?></li>
	<li><?= $this->Html->link('Edit Role', ['controller' => 'acos', 'action' => 'index', @$role_key]) ?></li>
</ul>

<div class="row-fluid">
	<?= $this->Form->create('EmployeeRole') ?>
		<div class="row-fluid">
			<div class="span7">
				<?= $this->Form->input('EmployeeRole.element_value', array('class' => 'input-block-level', 'label' => '1. Give a name to the new role')) ?>
			</div>
			<div class="span5">
				<?= $this->Form->input('Employee', array('options' => $employees, 'multiple' => 'checkbox', 'required' => false, 'label' => '2. Assign the new role to employees')) ?>
			</div>
		</div>
		<div class="row-fluid">
			<?= $this->Form->button('Save Employee Roles', array('class' => 'btn btn-primary pull-right')) ?>
		</div>
	<?= $this->Form->end() ?>
</div>

<div class="row-fluid">

	<div class="span3">
		<?= $this->Form->create('Aco', [
			'type' => 'get',
			'inputDefaults' => [
				'label' => false,
				'class' => 'input-block-level',
				'div' => 'form-group',
				'required' => false
			] 
		]) ?>
		<legend>Search Routes</legend>
		<div class="row-fluid">
			<?= $this->Form->input('q',  ['placeholder' => 'Controller and/or action']) ?>
			<?= $this->Form->button('Search', ['class' => 'btn btn-primary']) ?>
			<?= $this->Html->link('clear', $this->request->here) ?>
			<?= $this->Form->end() ?>
		</div>

		<legend>Filter by Controller</legend>
		<ul>
		<?php foreach ($controllers as $controller): ?>
			<li><?= $this->Html->link($controller, ['?' => ['q' => $controller]]) ?></li>
		<?php endforeach ?>
		</ul>
	</div>

	<div class="span9">

		<?= $this->Form->create('Aco', ['inputDefaults' => [
				'class' => 'input-block-level'
		]]) ?>
		<legend>Role</legend>
		<?= $this->Form->input('Role.key',  ['options' => @$roles, 'empty' => '']) ?>

		<div class="row-fluid">
			<table class="table table-striped table-condensed">
				<thead>
					<tr>
						<th><?= $this->Paginator->sort('controller'); ?></th>
						<th><?= $this->Paginator->sort('action'); ?></th>
						<th class="shrink"><?= __('Allowed'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($acos as $i => $aco): ?>
					<tr>
						<td><?= h($aco['Aco']['controller']); ?>&nbsp;</td>
						<td><?= h($aco['Aco']['action']); ?>&nbsp;</td>
						<td class="shrink">
							<?= $this->Form->input("Permission.{$i}.aco_id",['type' => 'hidden', 'value' => $aco['Aco']['id']]) ?>
							<?= $this->Form->input("Permission.{$i}.allowed",['type' => 'checkbox', 'label' => false, 'div' => false]) ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<?= $this->Form->button('Apply', ['class' => 'btn btn-primary pull-right']) ?>
			<?= $this->element('paging') ?>
		</div>

		<?= $this->Form->end() ?>

	</div>

</div>


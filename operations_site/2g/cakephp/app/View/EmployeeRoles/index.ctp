<div class="row-fluid">
	<h2><?= __('Employee Roles') ?></h2>

	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span2">
			<legend>Categories</legend>
			<ul>
				<li><?= $this->Html->link('sales', array('action' => 'index', '?' => array('q' => 'role:sales/'))) ?></li>
				<li><?= $this->Html->link('finance', array('action' => 'index', '?' => array('q' => 'role:finance/'))) ?></li>
				<li><?= $this->Html->link('security', array('action' => 'index', '?' => array('q' => 'role:security/'))) ?></li>
				<li><?= $this->Html->link('hr', array('action' => 'index', '?' => array('q' => 'role:hr/'))) ?></li>
				<li><?= $this->Html->link('marketing', array('action' => 'index', '?' => array('q' => 'role:marketing/'))) ?></li>
				<li><?= $this->Html->link('telecom', array('action' => 'index', '?' => array('q' => 'role:telecom/'))) ?></li>
			</ul>
		</div>
		<div class="span10">
			<legend>Filter &amp; Search</legend>

			<!-- filter and search -->
			<?= $this->Form->create('EmployeeRole', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'input-auto', 'required' => false))) ?>

				<div class="controls-row">
					<?= $this->Form->input('q', array('placeholder' => 'Search Description', 'class' => 'input-xlarge')) ?>
					<?= $this->Form->input('parent_id', array('empty' => '-- Employee --', 'options' => $employees)) ?>
					<?= $this->Form->input('element_value', array('options' => $element_values, 'empty' => '-- Role Name --')) ?>
				</div>

				<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
				<?= $this->Html->link('Clear', array('action' => 'index')) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>

	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('parent_id', 'Employee') ?></th>
				<th><?= $this->Paginator->sort('element_value', 'Role') ?></th>
				<th class="actions"><?= __('Actions') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($roles as $role): ?>
			<tr>
				<td>
					<?= $this->Html->link($role['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $role['Employee']['id'])) ?>
				</td>
				<td><?= h($role['EmployeeRole']['element_value']) ?>&nbsp;</td>
				<td>
					<?= $this->Form->postLink(__('Revoke'), array('action' => 'delete', $role['EmployeeRole']['id']), null, __('Are you sure you want to revoke "%s" from "%s"?', $role['EmployeeRole']['element_value'], $role['Employee']['full_name'])) ?>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>

	<?= $this->element('paging') ?>

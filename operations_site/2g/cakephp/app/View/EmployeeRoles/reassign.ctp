<?php $role_key = @$this->request->pass[0] ?>


<div class="row-fluid">
	<ul class="nav nav-tabs">
		<li><?= $this->Html->link('Create New Role', array('controller' => 'employeeRoles', 'action' => 'add')) ?></li>
		<li class="active"><?= $this->Html->link('Reassign Existing Role', '#') ?></li>
		<?php if ($role_key): ?>
			<li><?= $this->Html->link('Edit Role', ['controller' => 'acos', 'action' => 'index', $role_key]) ?></li>
		<?php else: ?>
			<li><?= $this->Html->link('Edit Role', ['controller' => 'acos', 'action' => 'index', $role_key]) ?></li>
		<?php endif ?>
	</ul>
</div>
<div class="row-fluid">
</div>

<div class="row-fluid">
	<?= $this->Form->create('EmployeeRole') ?>
		<div class="row-fluid">
			<div class="span7">
				<?= $this->Form->input('EmployeeRole.element_value', array('class' => 'input-block-level', 'readonly')) ?>
				<label>1. Choose an existing role</label>
				<?php $links = $roles ?>
				<?php array_walk($links, function(&$role,$role_key) {
					$role=$this->Html->link($role, array(
						'controller'=>'employeeRoles',
						'action'=>'reassign',
						$role_key
					));	
				}) ?>
				<?= $this->Html->nestedList($links) ?>

				<?= $this->Form->button('Save Employee Roles', array('class' => 'btn btn-primary')) ?>
			</div>
			<div class="span5">
				<?php $disabled = @$enable_employee_checklist ? '' : 'disabled' ?>
				<?= $this->Form->input('Employee', array('options' => $employees, 'multiple' => 'checkbox', 'disabled' =>  $disabled, 'required' => false, 'label' => '2. Choose employees to assign this role')) ?>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>

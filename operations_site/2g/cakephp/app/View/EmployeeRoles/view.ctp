<div class="employeeRoles view">
<h2><?php echo __('Employee Role'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($employeeRole['EmployeeRole']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($employeeRole['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $employeeRole['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Element Value'); ?></dt>
		<dd>
			<?php echo h($employeeRole['EmployeeRole']['element_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($employeeRole['EmployeeRole']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($employeeRole['EmployeeRole']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($employeeRole['EmployeeRole']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Employee Role'), array('action' => 'edit', $employeeRole['EmployeeRole']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Employee Role'), array('action' => 'delete', $employeeRole['EmployeeRole']['id']), null, __('Are you sure you want to delete # %s?', $employeeRole['EmployeeRole']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Employee Roles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Role'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>

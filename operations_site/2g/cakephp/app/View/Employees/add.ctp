<div class="row-fluid">
	<div class="span10 offset1">
		<section class="module">
			<div class="module-head"><b>Create New Employee</b></div>
			<div class="module-body">
				<?= $this->Form->create('Employee', array('action' => 'edit', 'inputDefaults' => array('class' => 'input-block-level'))) ?>
					<div class="row-fluid">
						<?= $this->Form->input('is_active') ?>
						<div class="controls-row">
							<label>Employee Name</label>
							<?= $this->Form->input('salutation', array('class' => 'input-mini', 'placeholder' => 'Salutation', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('first_name', array('class' => 'input-auto', 'placeholder' => 'First Name', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('middle_name', array('class' => 'input-auto', 'placeholder' => 'Middle Name', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('last_name', array('class' => 'input-auto', 'placeholder' => 'Last Name', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('suffix', array('class' => 'input-mini', 'placeholder' => 'Suffix', 'div' => false, 'label' => false)) ?>
						</div>
						<?= $this->Form->input('nickname') ?>
					</div>
					<div class="row-fluid">
						<div class="span6">
							<h4>Contact Info</h4>
							<?= $this->Form->input('email') ?>
							<?= $this->Form->input('work_phone_number') ?>
							<?= $this->Form->input('work_fax_number') ?>
						</div>
						<div class="span6">
							<h4>Position</h4>
							<?= $this->Form->input('company') ?>
							<?= $this->Form->input('title') ?>
						</div>
					</div>
					<h4>Phone Numbers</h4>
					<?= $this->Form->input('home_phone_number') ?>
					<?= $this->Form->input('cell_phone_number') ?>
					<?= $this->Form->input('other_phone_number') ?>
					<h4>Contact Information</h4>
					<div class="row-fluid">
						<label>Work Address</label>
						<?= $this->Form->input('work_address_1', array('label' => false, 'div' => false, 'placeholder' => 'Work Address Line 1')) ?>
						<?= $this->Form->input('work_address_2', array('label' => false, 'div' => false, 'placeholder' => 'Work Address Line 2')) ?>
						<div class="controls-row">
							<?= $this->Form->input('work_city', array('class' => 'input-auto', 'placeholder' => 'City', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('work_state', array('class' => 'input-auto', 'placeholder' => 'State ', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('work_zip_code', array('class' => 'input-mini', 'placeholder' => 'Zipcode', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('work_country', array('class' => 'input-auto', 'placeholder' => 'Country', 'div' => false, 'label' => false)) ?>
						</div>
					</div>
					<div class="row-fluid">
						<label>Home Address</label>
						<?= $this->Form->input('home_address_1', array('label' => false, 'div' => false, 'placeholder' => 'Home Address Line 1')) ?>
						<?= $this->Form->input('home_address_2', array('label' => false, 'div' => false, 'placeholder' => 'home Address Line 2')) ?>
						<div class="controls-row">
							<?= $this->Form->input('home_city', array('class' => 'input-auto', 'placeholder' => 'City', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('home_state', array('class' => 'input-auto', 'placeholder' => 'State', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('home_zip_code', array('class' => 'input-mini', 'placeholder' => 'Zipcode', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('home_country', array('class' => 'input-auto', 'placeholder' => 'Country', 'div' => false, 'label' => false)) ?>
						</div>
					</div>
					<h4>Personal Information</h4>
					<div class="row-fluid">
						<?= $this->Form->input('birthday') ?>
						<?= $this->Form->input('tax_id', array('type' => 'text', 'label' => 'Tax ID')) ?>
						<?= $this->Form->input('email2') ?>
						<?= $this->Form->input('email3') ?>
						<?= $this->Form->input('type') ?>
						<?= $this->Form->input('in_directory') ?>
						<?= $this->Form->input('allowed_ip_addr') ?>
						<?= $this->Form->input('phone_pin') ?>
						<?= $this->Form->input('vacation_days') ?>
					</div>
					<?= $this->Form->button('Update Employee Information', array('class' => 'btn')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
</div>

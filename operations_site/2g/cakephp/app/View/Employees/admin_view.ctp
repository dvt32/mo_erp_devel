<div class="row-fluid">
	<div class="offset2 span8">
		<?= $this->Session->flash() ?>

		<!-- basic employee information -->
		<section class="module expandable">
			<div class="module-head"><b>Basic Employee Information</b></div>
			<div class="module-body">
				<dl>
					<dt><?= __('Id') ?></dt>
					<dd><?= h($employee['Employee']['id']) ?> &nbsp; </dd>
					<dt><?= __('Is Active') ?></dt>
					<dd><?= h($employee['Employee']['is_active'] ? 'yes' : 'no') ?> &nbsp; </dd>
					<dt><?= __('Salutation') ?></dt>
					<dd><?= h($employee['Employee']['salutation']) ?> &nbsp; </dd>
					<dt><?= __('First Name') ?></dt>
					<dd><?= h($employee['Employee']['first_name']) ?> &nbsp; </dd>
					<dt><?= __('Middle Name') ?></dt>
					<dd><?= h($employee['Employee']['middle_name']) ?> &nbsp; </dd>
					<dt><?= __('Last Name') ?></dt>
					<dd><?= h($employee['Employee']['last_name']) ?> &nbsp; </dd>
					<dt><?= __('Suffix') ?></dt>
					<dd><?= h($employee['Employee']['suffix']) ?> &nbsp; </dd>
					<dt><?= __('Nickname') ?></dt>
					<dd><?= h($employee['Employee']['nickname']) ?> &nbsp; </dd>
					<dt><?= __('Work Phone Number') ?></dt>
					<dd><?= h($employee['Employee']['work_phone_number']) ?> &nbsp; </dd>
					<dt><?= __('Work Fax Number') ?></dt>
					<dd><?= h($employee['Employee']['work_fax_number']) ?> &nbsp; </dd>
					<dt><?= __('Email') ?></dt>
					<dd><?= h($employee['Employee']['email']) ?> &nbsp; </dd>
					<dt><?= __('Company') ?></dt>
					<dd><?= h($employee['Employee']['company']) ?> &nbsp; </dd>
					<dt><?= __('Title') ?></dt>
					<dd><?= h($employee['Employee']['title']) ?> &nbsp; </dd>
					<dt><?= __('Has Assigned Task') ?></dt>
					<dd><?= h($employee['Employee']['has_assigned_task'] ? 'yes' : 'no') ?> &nbsp; </dd>
					<dt><?= __('Note') ?></dt>
					<dd><?= h($employee['Employee']['note']) ?> &nbsp; </dd>
				</dl>
			</div>
		</section>

		<!-- edit basic employee information -->
		<section class="module expandable">
			<div class="module-head clickable"><b>Edit Basic Employee Information</b></div>
			<div class="module-body" style="display:none;">
				<?= $this->Form->create('Employee', array('action' => 'edit', 'inputDefaults' => array('class' => 'input-block-level'))) ?>
					<div class="row-fluid">
						<?= $this->Form->input('id', array()) ?>
						<?= $this->Form->input('is_active', array()) ?>
						<div class="controls-row">
							<label>Employee Name</label>
							<?= $this->Form->input('salutation', array('class' => 'input-mini', 'placeholder' => 'salutation', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('first_name', array('class' => 'input-auto', 'placeholder' => 'first_name', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('middle_name', array('class' => 'input-auto', 'placeholder' => 'middle_name', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('last_name', array('class' => 'input-auto', 'placeholder' => 'last_name', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('suffix', array('class' => 'input-mini', 'placeholder' => 'suffix', 'div' => false, 'label' => false)) ?>
						</div>
						<?= $this->Form->input('nickname', array()) ?>
					</div>
					<div class="row-fluid">
						<div class="span6">
							<h4>Contact Info</h4>
							<?= $this->Form->input('email', array()) ?>
							<?= $this->Form->input('work_phone_number', array()) ?>
							<?= $this->Form->input('work_fax_number', array()) ?>
						</div>
						<div class="span6">
							<h4>Position</h4>
							<?= $this->Form->input('company', array()) ?>
							<?= $this->Form->input('title', array()) ?>
						</div>
					</div>
					<?= $this->Form->hidden('Meta.editing_section', array('value' => 'basic')) ?>
					<?= $this->Form->input('note', array()) ?>
				<?= $this->Form->button('Update Employee Information', array('class' => 'btn btn-primary')) ?>
			</div>
		</section>

		<!-- additional employee information -->
		<section class="module expandable">
			<div class="module-head clickable"><b>Additional Employee Information</b></div>
			<div class="module-body" style="display:none;">
				<dl>
					<dt><?= __('Home Phone Number') ?></dt>
					<dd><?= h($employee['Employee']['home_phone_number']) ?> &nbsp; </dd>
					<dt><?= __('Cell Phone Number') ?></dt>
					<dd><?= h($employee['Employee']['cell_phone_number']) ?> &nbsp; </dd>
					<dt><?= __('Other Phone Number') ?></dt>
					<dd><?= h($employee['Employee']['other_phone_number']) ?> &nbsp; </dd>
					<dt><?= __('Work Address 1') ?></dt>
					<dd><?= h($employee['Employee']['work_address_1']) ?> &nbsp; </dd>
					<dt><?= __('Work Address 2') ?></dt>
					<dd><?= h($employee['Employee']['work_address_2']) ?> &nbsp; </dd>
					<dt><?= __('Work City') ?></dt>
					<dd><?= h($employee['Employee']['work_city']) ?> &nbsp; </dd>
					<dt><?= __('Work State') ?></dt>
					<dd><?= h($employee['Employee']['work_state']) ?> &nbsp; </dd>
					<dt><?= __('Work Zip Code') ?></dt>
					<dd><?= h($employee['Employee']['work_zip_code']) ?> &nbsp; </dd>
					<dt><?= __('Work Country') ?></dt>
					<dd><?= h($employee['Employee']['work_country']) ?> &nbsp; </dd>
					<dt><?= __('Home Address 1') ?></dt>
					<dd><?= h($employee['Employee']['home_address_1']) ?> &nbsp; </dd>
					<dt><?= __('Home Address 2') ?></dt>
					<dd><?= h($employee['Employee']['home_address_2']) ?> &nbsp; </dd>
					<dt><?= __('Home City') ?></dt>
					<dd><?= h($employee['Employee']['home_city']) ?> &nbsp; </dd>
					<dt><?= __('Home State') ?></dt>
					<dd><?= h($employee['Employee']['home_state']) ?> &nbsp; </dd>
					<dt><?= __('Home Zip Code') ?></dt>
					<dd><?= h($employee['Employee']['home_zip_code']) ?> &nbsp; </dd>
					<dt><?= __('Home Country') ?></dt>
					<dd><?= h($employee['Employee']['home_country']) ?> &nbsp; </dd>
					<dt><?= __('Birthday') ?></dt>
					<dd><?= h($employee['Employee']['birthday']) ?> &nbsp; </dd>
					<dt><?= __('Tax Id') ?></dt>
					<dd><?= h($employee['Employee']['tax_id']) ?> &nbsp; </dd>
					<dt><?= __('Email2') ?></dt>
					<dd><?= h($employee['Employee']['email2']) ?> &nbsp; </dd>
					<dt><?= __('Email3') ?></dt>
					<dd><?= h($employee['Employee']['email3']) ?> &nbsp; </dd>
					<dt><?= __('Type') ?></dt>
					<dd><?= h($employee['Employee']['type']) ?> &nbsp; </dd>
					<dt><?= __('In Directory') ?></dt>
					<dd><?= h($employee['Employee']['in_directory'] ? 'yes' : 'no') ?> &nbsp; </dd>
					<dt><?= __('Password') ?></dt>
					<dd><?= h($employee['Employee']['password']) ?> &nbsp; </dd>
					<dt><?= __('Allowed Ip Addr') ?></dt>
					<dd><?= h($employee['Employee']['allowed_ip_addr']) ?> &nbsp; </dd>
					<dt><?= __('Phone Pin') ?></dt>
					<dd><?= h($employee['Employee']['phone_pin']) ?> &nbsp; </dd>
					<dt><?= __('Vacation Days') ?></dt>
					<dd><?= h($employee['Employee']['vacation_days']) ?> &nbsp; </dd>
				</dl>
			</div>
		</section>

		<!-- edit additional employee information -->
		<section class="module expandable">
			<div class="module-head clickable"><b>Edit Additional Employee Information</b></div>
			<div class="module-body" style="display:none;">
				<?= $this->Form->create('Employee', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
					<?= $this->Form->hidden('Meta.editing_section',array('value'=>'additional')) ?>
					<?= $this->Form->input('id') ?>
					<h4>Phone Numbers</h4>
					<?= $this->Form->input('home_phone_number', array()) ?>
					<?= $this->Form->input('cell_phone_number', array()) ?>
					<?= $this->Form->input('other_phone_number', array()) ?>
					<h4>Contact Information</h4>
					<div class="row-fluid">
						<label>Work Address</label>
						<?= $this->Form->input('work_address_1', array('label' => false, 'div' => false, 'placeholder' => 'work_address_1')) ?>
						<?= $this->Form->input('work_address_2', array('label' => false, 'div' => false, 'placeholder' => 'work_address_2')) ?>
						<div class="controls-row">
							<?= $this->Form->input('work_city', array('class' => 'input-auto', 'placeholder' => 'work_city', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('work_state', array('class' => 'input-auto', 'placeholder' => 'work_state', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('work_zip_code', array('class' => 'input-mini', 'placeholder' => 'work_zip_code', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('work_country', array('class' => 'input-auto', 'placeholder' => 'work_country', 'div' => false, 'label' => false)) ?>
						</div>
					</div>
					<div class="row-fluid">
						<label>Home Address</label>
						<?= $this->Form->input('home_address_1', array('label' => false, 'div' => false, 'placeholder' => 'home_address_1')) ?>
						<?= $this->Form->input('home_address_2', array('label' => false, 'div' => false, 'placeholder' => 'home_address_2')) ?>
						<div class="controls-row">
							<?= $this->Form->input('home_city', array('class' => 'input-auto', 'placeholder' => 'home_city', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('home_state', array('class' => 'input-auto', 'placeholder' => 'home_state', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('home_zip_code', array('class' => 'input-mini', 'placeholder' => 'home_zip_code', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('home_country', array('class' => 'input-auto', 'placeholder' => 'home_country', 'div' => false, 'label' => false)) ?>
						</div>
					</div>
					<h4>Personal Information</h4>
					<div class="row-fluid">
						<?= $this->Form->input('birthday', array()) ?>
						<?= $this->Form->input('tax_id', array('type' => 'text', 'label' => 'Tax ID')) ?>
						<?= $this->Form->input('email2', array()) ?>
						<?= $this->Form->input('email3', array()) ?>
						<?= $this->Form->input('type', array()) ?>
						<?= $this->Form->input('in_directory', array()) ?>
						<?= $this->Form->input('allowed_ip_addr', array()) ?>
						<?= $this->Form->input('phone_pin', array()) ?>
						<?= $this->Form->input('vacation_days', array()) ?>
					</div>
					<?= $this->Form->button('Update Employee Information', array('class' => 'btn')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>

		<!-- employee roles -->
		<section class="module expandable">
			<div class="module-head clickable">
				<b>Employee Roles</b>
			</div>
			<div class="module-body" style="display:none;">
				<h4>Currently Assigned</h4>
				<?= $this->Form->create('Employee', array('action' => 'assignRoles')) ?>
					<?= $this->Form->input('Employee.id') ?>
					<?= $this->Form->input('Employee.Role', array('multiple' => 'checkbox')) ?>
					<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>

	</div>
</div>

<script>
// auto-expand
$(".expandable").each(function(i) {
	this.setAttribute("id","expandable-"+i);
	var section="#expandable-"+i;
	$(section+" div.module-head").addClass("clickable").click(function(){
		$(section+" div.module-body").toggle();
	});
});
</script>

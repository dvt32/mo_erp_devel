<div class="row-fluid">
	<div class="span8">
		<?= $this->Form->create('Employee') ?>
			<?= $this->Form->input('Employee.id') ?>
			<?= $this->Form->input('Role', array('multiple' => 'checkbox')) ?>
			<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

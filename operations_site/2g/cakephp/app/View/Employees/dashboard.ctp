<section class="row-fluid">

	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/star.png', array('style' => 'height: 2.5em;')) ?>
				<b>Sales</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>  
						<a href="{{ operations_2g_urlprefix }}/prospects">Prospective Customers</a>
						<a href="{{ operations_2g_urlprefix }}/prospects/add">Add New Prospect</a>
						<a href="{{ operations_2g_urlprefix }}/customerAccounts/add">Add New Customer</a>
					</li>
				</ul>
			</div>
		</section>
	</div>


	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/binoculars.png', array('style' => 'height: 2.5em;')) ?>
				<b>Billing</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<a href="{{ operations_2g_urlprefix }}/customerAccounts/billing">Accounts Review</a>
						<a href="{{ operations_2g_urlprefix }}/customerAccounts/invoicing">Invoice Changes</a>
						<a href="{{ operations_2g_urlprefix }}/customerAccounts/receiving">Accounts Receivables</a>
						<a href="{{ operations_2g_urlprefix }}/customerAccounts/aging">Aging Accounts</a>
						<a href="{{ operations_2g_urlprefix }}/customerAccounts/admin">Billing Cycle Overview</a>
						<a href="{{ operations_2g_urlprefix }}/deposits">List Deposits</a>
						<a href="{{ operations_2g_urlprefix }}/deposits/add">Make Deposits</a>
						<a href="{{ operations_2g_urlprefix }}/promises">List Promises</a>
					</li>
				</ul>
			</div>
		</section>
	</div>

 
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/addons/256/brush_checkmark.png', array('style' => 'height: 2.5em;')) ?>
				<b>License Agreements</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<a href="{{ operations_2g_urlprefix }}/licenseAgreements">License Management</a>
						<a href="{{ operations_2g_urlprefix }}/licenseAgreements/add">Create New Agreement</a>
						<a href="{{ operations_2g_urlprefix }}/licenseAgreementTemplates">License Agreement Templates</a>
						<a href="{{ operations_2g_urlprefix }}/licenseAmendmentTemplates">License Amendement Templates</a>
					</li>
				</ul>
			</div>
		</section>
	</div>

</section>

<!-- /end row 1 -->

<!-- Row 2 -->
<section class="row-fluid">
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/book.png', array('style' => 'height: 2.5em;')) ?>
				<b>Directory</b>
			</div>
			<div class="module-body text-center">
				<?= $this->Form->create(null, array('type' => 'get', 'inputDefaults' => array('div' => false, 'label' => false) )) ?>
					<?= $this->Form->input('q', array('placeholder' => 'Search Directories', 'class' => 'input-block-level')) ?>
					<div class="row-fluid">
						<div class="span12">
							<div class="row-fluid">
								<div class="span4">
									<button id='searchCustomersButton' class="btn btn-block btn-info">Customers</button>
								</div>
								<div class="span4">
									<button id='searchCompaniesButton' class="btn btn-block btn-primary">Companies</button>
								</div>
								<div class="span4">
									<button id='searchEmployeesButton' class="btn btn-block ">Employees</button>
								</div>
							</div>
						</div>
					</div>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/technical_wrench.png', array('style' => 'height: 2.5em;')) ?>
				<b>Physical Resources</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<?= $this->Html->link(__('Access Card Administration'), array('controller' => 'proximityCards', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Access Card Requests'), array('controller' => 'proximityCards', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Mailboxes'), array('controller' => 'mailboxes', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Reservations'), array('controller' => 'reservations', 'action' => 'byFloor', '2')); // FIXME change hardcoded 2 to employee's primary floor ?>
						<?= $this->Html->link(__('Buildings'), array('controller' => 'buildings', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Floors'), array('controller' => 'floors', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Desks'), array('controller' => 'desks', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Cameras'), array('controller' => 'pages', 'action' => 'display', 'cameras')); ?>
					</li>
				</ul>
			</div>
		</section>
	</div>
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/dashboard/tasks2.png', array('style' => 'height: 2.5em;')) ?>
				<b>Tasks &amp; Notifications</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<?= $this->Html->link('Move Out Requests', ['controller' => 'moveoutRequests', 'action' => 'index']) ?>
						<?= $this->Html->link(__('Manage Tasks'), array('controller' => 'tasks', 'action' => 'index')); ?>
						<?= $this->Html->link('Phone Tasks', 'http://www.edata.ca/ticket/scp/index.php'); ?>
						<?= $this->Html->link(__('Manage Notifications'), array('controller' => 'notifications', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Package Notifications'), array('controller' => 'mailPackages', 'action' => 'index')); ?>
					</li>
				</ul>
			</div>
		</section>
	</div>
</section>
<!-- /end row 2 -->


<!-- Row 3 -->
<section class="row-fluid">
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/antenna.png', array('style' => 'height: 2.5em;')) ?>
				<b>Communication &amp; Imaging</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<?= $this->Html->link(__('Phone Numbers'), array('controller' => 'phoneNumbers', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Fax Numbers'), array('controller' => 'faxNumbers', 'action' => 'index')); ?>
						<?= $this->Html->link(__('IP Phones'), array('controller' => 'ipPhones', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Check Telecommunication Systems'), array('controller' => 'buildings', 'action' => 'networkMonitor')); ?>
						<?= $this->Html->link(__('IP Addresses'), array('controller' => 'ipAddresses', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Public Scans'), array('controller' => 'scans', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Public Faxes'), array('controller' => 'faxIns', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Send Fax'), array('controller' => 'faxOuts', 'action' => 'add')); ?>
						<?= $this->Html->link(__('Faxage Status'), array('controller' => 'faxOuts', 'action' => 'status')); ?>
						<?= $this->Html->link(__('Claimed Documents'), array('controller' => 'documents', 'action' => 'account', 1)); ?>
					</li>
				</ul>
			</div>
		</section>
	</div>
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/help.png', array('style' => 'height: 2.5em;')) ?>
				<b>Documentation</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<?= $this->Html->link(__('Operations Features'), array('controller' => 'pages', 'action' => 'display', 'features')); ?>
						<?= $this->Html->link(__('Technical Docs'), array('controller' => 'pages', 'action' => 'display', 'technical')); ?>
						<?= $this->Html->link(__('Report Bugs'), array('controller' => 'pages', 'action' => 'display', 'bugs')); ?>
					</li>
				</ul>
			</div>
		</section>
	</div>
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/lock.png', array('style' => 'height: 2.5em;')) ?>
				<b>Authorization</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li><?= $this->Html->link(__('Employee Management'), array('controller' => 'employeeRoles#', 'action' => 'index')) ?></li>
					<li><?= $this->Html->link(__('Employee Role Management'), array('controller' => 'employeeRoles', 'action' => 'reassign')) ?></li>
					<li><?= $this->Html->link('Route Registration', ['controller' => 'acos', 'action' => 'manage']) ?></li>
				</ul>
			</div>
		</section>
	</div>
	
</section>
<!-- /end row 3 -->


<!-- Row 4 -->

<section class="row-fluid">
<!-- 
	<div class="span3">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/dashboard/collaborate.jpg', array('style' => 'height: 2.5em;')) ?>
				<b>Human Resources</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<a href="{{ operations_2g_urlprefix }}/timesheets">Timesheets</a>
						<a href="{{ operations_2g_urlprefix }}/payrolls">Payroll</a>
						<a href="{{ operations_2g_urlprefix }}/benefits">Benefits</a>
						<a href="{{ operations_2g_urlprefix }}/expenses">Expenses</a>
					</li>
				</ul>
			</div>
		</section>
	</div>
	<div class="span3">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/addons/256/bill_shield.png', array('style' => 'height: 2.5em;')) ?>
				<b>Finance</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<a href="{{ operations_2g_urlprefix }}/receivePayments">Receive Payments</a>
						<a href="{{ operations_2g_urlprefix }}/transactions">Transactions</a>
						<a href="{{ operations_2g_urlprefix }}/reconcile">Reconcile Accounts</a>
						<a href="{{ operations_2g_urlprefix }}/expenses">Employee Expenses</a>
						<a href="{{ operations_2g_urlprefix }}/desks/maps">Manage Desks</a>
					</li>
				</ul>
			</div>
		</section>
	</div>
	<div class="span3">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/clipboard.png', array('style' => 'height: 2.5em;')) ?>
				<b>Reports</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li>
						<a href="{{ operations_2g_urlprefix }}/reports/weekend">Weekend Access</a>
						<a href="{{ operations_2g_urlprefix }}/reports/guest">Guest Access</a>
						<a href="{{ operations_2g_urlprefix }}/reports/deliveries">Guest Access</a>
						<a href="{{ operations_2g_urlprefix }}/reports/intercom">Guest Access</a>
						<a href="{{ operations_2g_urlprefix }}/reports/mailboxes">Guest Access</a>
						<a href="{{ operations_2g_urlprefix }}/reports/floor">Guest Access</a>
					</li>
				</ul>
			</div>
		</section>
	</div>
	<div class="span4">
		<section class="module">
			<div class="module-head">
				<?= $this->Html->image('icons/flat_miniset/png/256/gear.png', array('style' => 'height: 2.5em;')) ?>
				<b>Administration</b>
			</div>
			<div class="module-body text-center">
				<ul class="nav nav-stacked">
					<li><?= $this->Html->link(__('Administrator Dashboard'), array('controller' => 'pages', 'action' => 'display', 'admin')); ?></li>
					<li><?= $this->Html->link('Access Management', ['controller' => 'employeeRoles', 'action' => 'reassign']) ?></li>
					<li><?= $this->Html->link('Route Registration', ['controller' => 'acos', 'action' => 'manage']) ?></li>
				</ul>
			</div>
		</section>
	</div>
</section>
-->

<!-- /end row 4 -->

<style>
/*increase spacing between stacked menu items*/
ul.nav-stacked li { line-height:2em; }
</style>

<script>
// change action of directory search form depending on button clicked
$("#searchEmployeesButton").click(function() {$("#EmployeeDashboardForm").attr("action", "{{ operations_2g_urlprefix }}/employees/index"); });
$("#searchCustomersButton").click(function() {$("#EmployeeDashboardForm").attr("action", "{{ operations_2g_urlprefix }}/customers/index"); });
$("#searchCompaniesButton").click(function() {$("#EmployeeDashboardForm").attr("action", "{{ operations_2g_urlprefix }}/customerAccounts/index"); });
</script>

<div class="employees form">
<?php echo $this->Form->create('Employee'); ?>
	<fieldset>
		<legend><?php echo __('Edit Employee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('salutation');
		echo $this->Form->input('first_name');
		echo $this->Form->input('middle_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('nickname');
		echo $this->Form->input('email');
		echo $this->Form->input('work_phone_number');
		echo $this->Form->input('work_fax_number');
		echo $this->Form->input('home_phone_number');
		echo $this->Form->input('cell_phone_number');
		echo $this->Form->input('other_phone_number');
		echo $this->Form->input('title');
		echo $this->Form->input('company');
		echo $this->Form->input('work_address_1');
		echo $this->Form->input('work_address_2');
		echo $this->Form->input('work_city');
		echo $this->Form->input('work_state');
		echo $this->Form->input('work_zip_code');
		echo $this->Form->input('work_country');
		echo $this->Form->input('home_address_1');
		echo $this->Form->input('home_address_2');
		echo $this->Form->input('home_city');
		echo $this->Form->input('home_state');
		echo $this->Form->input('home_zip_code');
		echo $this->Form->input('home_country');
		echo $this->Form->input('birthday');
		echo $this->Form->input('tax_id');
		echo $this->Form->input('is_active');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('email2');
		echo $this->Form->input('email3');
		echo $this->Form->input('type');
		echo $this->Form->input('in_directory');
		echo $this->Form->input('password');
		echo $this->Form->input('suffix');
		echo $this->Form->input('allowed_ip_addr');
		echo $this->Form->input('phone_pin');
		echo $this->Form->input('vacation_days');
		echo $this->Form->input('has_assigned_task');
		echo $this->Form->input('hash');
		echo $this->Form->input('salt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-primary')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Employee.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Employee.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Employees'), array('action' => 'index')); ?></li>
	</ul>
</div>

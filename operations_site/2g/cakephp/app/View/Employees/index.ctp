<h3><?= __('Employees') ?></h3>

<div class="row-fluid">
	<!-- jump to customer id -->
	<div class="span2">
		<?= $this->Form->create('Employee', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'input-block-level'))) ?>
			<legend><?= __('Jump To') ?></legend>
			<?= $this->Form->input('id', array('type' => 'text', 'placeholder' => 'Employee Id #')) ?>
			<?= $this->Form->button('Go', array('class' => 'btn btn-success')) ?>
		<?= $this->Form->end() ?>
	</div>

	<!-- search & filter forms -->
	<div class="span10">
		<?= $this->Form->create('Employee', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'input-auto'))) ?>
			<legend><?= __('Filter & Search') ?></legend>
			<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge')) ?>
			<?= $this->Form->input('company', array('empty' => '-- Company --')) ?>
			<?= $this->Form->input('title', array('empty' => '-- Title --')) ?>
			<?= $this->Form->input('type', array('empty' => '-- Type --')) ?>
			<div>
				<?= $this->Form->button('Filter', array('class' => 'btn btn-primary')) ?>
				<?= $this->Html->link('Clear', array('action' => 'index')) ?>
			</div>
		<?= $this->Form->end() ?>
	</div>
</div>

<div class="employees index">
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('id') ?></th>
				<th>Name (<?= $this->Paginator->sort('first_name','First') ?>, <?= $this->Paginator->sort('middle_name','Middle') ?>, <?= $this->Paginator->sort('last_name','Last') ?>)</th>
				<th><?= $this->Paginator->sort('title') ?></th>
				<th><?= $this->Paginator->sort('email') ?></th>
				<th><?= $this->Paginator->sort('work_address_1') ?></th>
				<th><?= $this->Paginator->sort('work_phone_number') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($employees as $employee): ?>
			<tr>
				<td><strong class="big"><?= $this->Html->link($employee['Employee']['id'], array('action' => 'view', $employee['Employee']['id'])) ?></strong>&nbsp;</td>
				<td class="searchable"><?= h($employee['Employee']['first_name'] . ' ' . $employee['Employee']['middle_name'] . ' ' . $employee['Employee']['last_name']) ?>&nbsp;</td>
				<td><?= h($employee['Employee']['title']) ?>&nbsp;</td>
				<td><?= h($employee['Employee']['email']) ?>&nbsp;</td>
				<td><?= h($employee['Employee']['work_address_1']) ?>&nbsp;</td>
				<td><?= h($employee['Employee']['work_phone_number']) ?>&nbsp;</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )) ?>	</p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>

<script>
$("td.searchable").highlight(QueryString.q);
</script>

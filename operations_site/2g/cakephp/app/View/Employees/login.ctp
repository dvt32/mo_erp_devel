<div class="row-fluid">
  <?= $this->Form->create('Employee', array('class' => 'form-signin', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'input-block-level') )) ?>
    <div class="text-center">
      <img src="{{ operations_2g_assetprefix }}/img/logo1.jpg">
      <h3>Operations</h3>
    </div>
    <?= $this->Session->flash() ?>
    <?= $this->Form->input('Employee.username', array('placeholder' => __('Email Address'), 'autofocus' => '1')) ?>
    <?= $this->Form->input('Employee.password', array('placeholder' => __('Password'))) ?>
    <?= $this->Form->button('Sign In', array('class' => 'btn btn-large btn-block btn-primary', 'id' => 'login-button')) ?>
  <?= $this->Form->end() ?>
</div>
<div class="text-center">
  Forgot your password?
  <?= $this->Html->link('Recover Password', array('controller' => 'tokens', 'action' => 'request', 'resetPassword')) ?>
</div>
 

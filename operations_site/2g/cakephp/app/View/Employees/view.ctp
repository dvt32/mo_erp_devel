<div class="row-fluid">
	<div class="offset2 span8">
		<?= $this->Session->flash() ?>

		<!-- basic employee information -->
		<section class="module expandable">
			<div class="module-head"><b>Basic Employee Information</b></div>
			<div class="module-body">
				<dl>
					<dt><?= __('Id') ?></dt>
					<dd><?= h($employee['Employee']['id']) ?> &nbsp; </dd>
					<dt><?= __('Is Active') ?></dt>
					<dd><?= h($employee['Employee']['is_active'] ? 'yes' : 'no') ?> &nbsp; </dd>
					<dt><?= __('Salutation') ?></dt>
					<dd><?= h($employee['Employee']['salutation']) ?> &nbsp; </dd>
					<dt><?= __('First Name') ?></dt>
					<dd><?= h($employee['Employee']['first_name']) ?> &nbsp; </dd>
					<dt><?= __('Middle Name') ?></dt>
					<dd><?= h($employee['Employee']['middle_name']) ?> &nbsp; </dd>
					<dt><?= __('Last Name') ?></dt>
					<dd><?= h($employee['Employee']['last_name']) ?> &nbsp; </dd>
					<dt><?= __('Suffix') ?></dt>
					<dd><?= h($employee['Employee']['suffix']) ?> &nbsp; </dd>
					<dt><?= __('Nickname') ?></dt>
					<dd><?= h($employee['Employee']['nickname']) ?> &nbsp; </dd>
					<dt><?= __('Work Phone Number') ?></dt>
					<dd><?= h($employee['Employee']['work_phone_number']) ?> &nbsp; </dd>
					<dt><?= __('Work Fax Number') ?></dt>
					<dd><?= h($employee['Employee']['work_fax_number']) ?> &nbsp; </dd>
					<dt><?= __('Email') ?></dt>
					<dd><?= h($employee['Employee']['email']) ?> &nbsp; </dd>
					<dt><?= __('Company') ?></dt>
					<dd><?= h($employee['Employee']['company']) ?> &nbsp; </dd>
					<dt><?= __('Title') ?></dt>
					<dd><?= h($employee['Employee']['title']) ?> &nbsp; </dd>
					<dt><?= __('Has Assigned Task') ?></dt>
					<dd><?= h($employee['Employee']['has_assigned_task'] ? 'yes' : 'no') ?> &nbsp; </dd>
					<dt><?= __('Note') ?></dt>
					<dd><?= h($employee['Employee']['note']) ?> &nbsp; </dd>
				</dl>
			</div>
		</section>
	</div>
</div>

<script>
// auto-expand
$(".expandable").each(function(i) {
	this.setAttribute("id","expandable-"+i);
	var section="#expandable-"+i;
	$(section+" div.module-head").addClass("clickable").click(function(){
		$(section+" div.module-body").toggle();
	});
});
</script>

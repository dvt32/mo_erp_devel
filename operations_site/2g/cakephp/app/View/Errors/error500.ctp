
<?php
if (Configure::read('debug') > 0 ):
	echo $this->element('exception_stack_trace');
endif; ?>

<?php if (Configure::read('debug') == 0 ): ?>
<div class="row-fluid">
	<div class="span12 message">
		<div class="centered row-fluid">
			<h1>Uh Oh, something went wrong </h1>
			<p class="lead">
				Please contact <a href="mailto:bugs@microoffice.com">bugs@microoffice.com</a> with a screenshot and short description of what happened.
			</p>
			<p class="lead">Let's go</p>
			<a href="{{ operations_2g_urlprefix }}/" class="btn btn-primary btn-large">
				Home
			</a>
		</div>
	</div>
</div>
<?php endif; ?>


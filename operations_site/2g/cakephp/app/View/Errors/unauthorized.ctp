<div class="row-fluid">
	<p>You are logged in, but not authorized to access the protected page you requested.</p>
	<p>If you think you should have authorization, speak to a supervisor.</p>
	<p>Click to return to the <?= $this->Html->link('main menu', '/') ?></p>
</div>

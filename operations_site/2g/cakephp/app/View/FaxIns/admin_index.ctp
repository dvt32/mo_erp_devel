<div class="faxIns index">
	<h2><?php echo __('Fax Ins'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('person_id'); ?></th>
			<th><?php echo $this->Paginator->sort('file_id'); ?></th>
			<th><?php echo $this->Paginator->sort('file_size'); ?></th>
			<th><?php echo $this->Paginator->sort('file_timestamp'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('filename'); ?></th>
			<th><?php echo $this->Paginator->sort('fax_number'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($faxIns as $faxIn): ?>
	<tr>
		<td><?php echo h($faxIn['FaxIn']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($faxIn['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxIn['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($faxIn['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxIn['Customer']['id'])); ?>
		</td>
		<td><?php echo h($faxIn['FaxIn']['file_id']); ?>&nbsp;</td>
		<td><?php echo h($faxIn['FaxIn']['file_size']); ?>&nbsp;</td>
		<td><?php echo h($faxIn['FaxIn']['file_timestamp']); ?>&nbsp;</td>
		<td><?php echo h($faxIn['FaxIn']['note']); ?>&nbsp;</td>
		<td><?php echo h($faxIn['FaxIn']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($faxIn['FaxIn']['filename']); ?>&nbsp;</td>
		<td><?php echo h($faxIn['FaxIn']['fax_number']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $faxIn['FaxIn']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $faxIn['FaxIn']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $faxIn['FaxIn']['id']), null, __('Are you sure you want to delete # %s?', $faxIn['FaxIn']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Fax In'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

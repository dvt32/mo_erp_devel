<div class="faxIns view">
<h2><?php echo __('Fax In'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxIn['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxIn['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxIn['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxIn['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Id'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['file_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Size'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['file_size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Timestamp'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['file_timestamp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filename'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['filename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax Number'); ?></dt>
		<dd>
			<?php echo h($faxIn['FaxIn']['fax_number']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Fax In'), array('action' => 'edit', $faxIn['FaxIn']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Fax In'), array('action' => 'delete', $faxIn['FaxIn']['id']), null, __('Are you sure you want to delete # %s?', $faxIn['FaxIn']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Fax Ins'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fax In'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

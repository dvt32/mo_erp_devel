<div class="faxNumbers view">
<h2><?php echo __('Fax Number'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxNumber['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxNumber['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxNumber['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax Number'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Action'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['email_action']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cc Email'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['cc_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('In Device Action'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['in_device_action']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('In Device Target'); ?></dt>
		<dd>
			<?php echo h($faxNumber['FaxNumber']['in_device_target']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Fax Number'), array('action' => 'edit', $faxNumber['FaxNumber']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Fax Number'), array('action' => 'delete', $faxNumber['FaxNumber']['id']), null, __('Are you sure you want to delete # %s?', $faxNumber['FaxNumber']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Fax Numbers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fax Number'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

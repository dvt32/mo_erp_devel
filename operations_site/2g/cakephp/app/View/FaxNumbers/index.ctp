<div class="faxNumbers index">
	<h2>
		<?= $this->Html->image('icons/dashboard/file.png', array('style' => 'height: 3em;')) ?>
		<?= __('Fax Numbers'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<legend>Filter &amp; Search</legend>
			<div class="row-fluid">
				<!-- filter and search -->
				<div class="span8">
					<?= $this->Form->create('PhoneNumber', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
						<?= $this->Form->input('q', array('placeholder' => 'Search Numbers', 'class' => 'input-xlarge search-query')) ?>
						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
					<?= $this->Form->end() ?>
				</div>
				<div class="span4">
					<?= __('Results per page') ?>
					<?= $this->Html->link('10', array('?' => array('limit' => 10) + $this->request->query)) ?>
					<?= $this->Html->link('20', array('?' => array('limit' => 20) + $this->request->query)) ?>
					<?= $this->Html->link('50', array('?' => array('limit' => 50) + $this->request->query)) ?>
					<?= $this->Html->link('100', array('?' => array('limit' => 100) + $this->request->query)) ?>
				</div>
			</div>
		</div>
	</div>
	<br>
	<!-- /search & filter forms -->
	<table class="table">
	<tr>	
		<th><?= $this->Paginator->sort('fax_number'); ?></th>
		<th><?= $this->Paginator->sort('account_id'); ?></th>
		<th><?= $this->Paginator->sort('person_id'); ?></th>
		<th><?= $this->Paginator->sort('email_action'); ?></th>
		<th><?= $this->Paginator->sort('creation_dt', 'Created'); ?></th>
		<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($faxNumbers as $faxNumber): ?>
	<tr>
		<td><?= $this->Html->link($faxNumber['FaxNumber']['fax_number'], array('action' => 'view', $faxNumber['FaxNumber']['id'])); ?>&nbsp;</td>
		<td>
			<?= $this->Html->link($faxNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxNumber['CustomerAccount']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($faxNumber['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $faxNumber['Customer']['id'])); ?>
		</td>
		<td><?= h($faxNumber['FaxNumber']['email_action']); ?>&nbsp;</td>
		
		<td><?= h($faxNumber['FaxNumber']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $faxNumber['FaxNumber']['id'])); ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $faxNumber['FaxNumber']['id']), null, __('Are you sure you want to delete # %s?', $faxNumber['FaxNumber']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>


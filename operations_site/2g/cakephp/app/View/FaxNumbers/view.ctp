<div class="faxNumbers view">
<h2><?= __('Fax Number'); ?></h2>
	<dl>
		<dt><?= __('Id'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['id']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Customer Account'); ?></dt>
		<dd>
			<?= @$this->Html->link($faxNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxNumber['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Customer'); ?></dt>
		<dd>
			<?= @$this->Html->link($faxNumber['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxNumber['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Fax Number'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Email Action'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['email_action']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Note'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['note']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Creation Dt'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Cc Email'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['cc_email']); ?>
			&nbsp;
		</dd>
		<dt><?= __('In Device Action'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['in_device_action']); ?>
			&nbsp;
		</dd>
		<dt><?= __('In Device Target'); ?></dt>
		<dd>
			<?= h($faxNumber['FaxNumber']['in_device_target']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<li class="active"><?= $this->Html->link(__('Edit Fax Number'), array('action' => 'edit', $faxNumber['FaxNumber']['id'])); ?> </li>
		<li class="active"><?= $this->Html->link(__('List Fax Numbers'), array('action' => 'index')); ?> </li>
	</ul>
</div>

<div class="faxOuts index">
	<h2>
		<?= $this->Html->image('icons/dashboard/others/paste.png' , array('style' => 'height:3em;')) ?>
		<?php echo __('All Sent Faxes'); ?>
	</h2>
	<table class="table">
		<tr>
			<th><?php echo $this->Paginator->sort('job_id', 'Job ID'); ?></th>
			<th><?php echo $this->Paginator->sort('recipient_name'); ?></th>
			<th><?php echo $this->Paginator->sort('fax_number'); ?></th>
			<th><?php echo $this->Paginator->sort('short_status', 'Status'); ?></th>
			<th><?php echo $this->Paginator->sort('send_time'); ?></th>
			<th><?php echo $this->Paginator->sort('page_count'); ?></th>
		</tr>
		<?php foreach ($faxOuts as $faxOut): ?>
			<tr>
				<td><?php echo h($faxOut['FaxOut']['job_id']); ?>&nbsp;</td>
				<td><?php echo h($faxOut['FaxOut']['recipient_name']); ?>&nbsp;</td>
				<td><?php echo h($faxOut['FaxOut']['fax_number']); ?>&nbsp;</td>
				<td><?php echo h($faxOut['FaxOut']['short_status']); ?>&nbsp;</td>
				<td><?php echo h($faxOut['FaxOut']['send_time']); ?>&nbsp;</td>
				<td><?php echo h($faxOut['FaxOut']['page_count']); ?>&nbsp;</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<div class="paging">
				<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
			</div>
		</div>
		<div class="actions">
			<h3><?php echo __('Actions'); ?></h3>
			<ul>
				<li><?php echo $this->Html->link(__('Send a Fax'), array('action' => 'add')); ?></li>
			</ul>
		</div>

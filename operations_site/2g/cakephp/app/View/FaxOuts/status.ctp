<div class="faxOuts index">
	<h2>
		<?= $this->Html->image('icons/dashboard/others/paste.png' , array('style' => 'height:3em;')) ?>
		<?php echo __('Faxage Status'); ?>
	</h2>
	<table class="table">
		<tr>
			<th>Job ID</th>
			<th>Comm ID</th>
			<th>Destination</th>
			<th>Destination #</th>
			<th>Short Status</th>
			<th>Long Status</th>
			<th>Send Time</th>
			<th>Complete Time</th>
			<th>Xmit Time</th>
			<th>Page Count</th>
		</tr>
		<?php foreach ($faxage as $fax): ?>
			<tr>
				<td><?= $fax['Faxage']['job_id'] ?></td>
				<td><?= $fax['Faxage']['comm_id'] ?></td>
				<td><?= $fax['Faxage']['destname'] ?></td>
				<td><?= $fax['Faxage']['destnum'] ?></td>
				<td><?= $fax['Faxage']['shortstatus'] ?></td>
				<td><?= $fax['Faxage']['longstatus'] ?></td>
				<td><?= $fax['Faxage']['sendtime'] ?></td>
				<td><?= $fax['Faxage']['completetime'] ?></td>
				<td><?= $fax['Faxage']['xmittime'] ?></td>
				<td><?= $fax['Faxage']['pagecount'] ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Send a Fax'), array('action' => 'add')); ?></li>
	</ul>
</div>

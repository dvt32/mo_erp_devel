<div class="faxOuts view">
<h2><?php echo __('Fax Out'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxOut['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $faxOut['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($faxOut['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $faxOut['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Job Id'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['job_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comm Id'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['comm_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recipient Name'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['recipient_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax Number'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['fax_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Status'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['short_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Long Status'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['long_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Send Time'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['send_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Complete Time'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['complete_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Xmit Time'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['xmit_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Page Count'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['page_count']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Send Email Alert'); ?></dt>
		<dd>
			<?php echo h($faxOut['FaxOut']['send_email_alert']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Fax Out'), array('action' => 'edit', $faxOut['FaxOut']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Fax Out'), array('action' => 'delete', $faxOut['FaxOut']['id']), null, __('Are you sure you want to delete # %s?', $faxOut['FaxOut']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Fax Outs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fax Out'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

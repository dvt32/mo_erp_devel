<div class="floors form">
<?php echo $this->Form->create('Floor'); ?>
	<fieldset>
		<legend><?php echo __('Add Floor'); ?></legend>
	<?php
		echo $this->Form->input('building_id');
		echo $this->Form->input('label');
		echo $this->Form->input('is_active');
		echo $this->Form->input('note');
		echo $this->Form->input('short_global_label');
		echo $this->Form->input('short_local_label');
		echo $this->Form->input('employee_id');
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
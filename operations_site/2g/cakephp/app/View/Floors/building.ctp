<div class="floors index">
	<h2><?php echo __('Floors'); ?></h2>
	<table class="table table-condensed table-striped">
	<tr>
			<th><?php echo $this->Paginator->sort('label'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt', 'Created'); ?></th>
			<th><?php echo $this->Paginator->sort('short_global_label'); ?></th>
			<th><?php echo $this->Paginator->sort('short_local_label'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($floors as $floor): ?>
	<tr>
		<td><?php echo h($floor['Floor']['label']); ?>&nbsp;</td>
		<td><?php echo h($floor['Floor']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($floor['Floor']['note']); ?>&nbsp;</td>
		<td><?php echo h($floor['Floor']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($floor['Floor']['short_global_label']); ?>&nbsp;</td>
		<td><?php echo h($floor['Floor']['short_local_label']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $floor['Floor']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $floor['Floor']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $floor['Floor']['id']), null, __('Are you sure you want to delete # %s?', $floor['Floor']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
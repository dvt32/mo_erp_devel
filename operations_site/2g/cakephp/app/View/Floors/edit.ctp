<div class="floors form">
<?php echo $this->Form->create('Floor'); ?>
	<fieldset>
		<legend><?php echo __('Edit Floor'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('building_id');
		echo $this->Form->input('label');
		echo $this->Form->input('is_active');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('bounding_polygon');
		echo $this->Form->input('short_global_label');
		echo $this->Form->input('short_local_label');
		echo $this->Form->input('employee_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-primary')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Floor.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Floor.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Buildings'), array('controller' => 'buildings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Building'), array('controller' => 'buildings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Iads'), array('controller' => 'iads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Iad'), array('controller' => 'iads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Notifications'), array('controller' => 'notifications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Notification'), array('controller' => 'notifications', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('controller' => 'phone_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Access Levels'), array('controller' => 'access_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Access Level'), array('controller' => 'access_levels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cameras'), array('controller' => 'cameras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Camera'), array('controller' => 'cameras', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Desks'), array('controller' => 'desks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Desk'), array('controller' => 'desks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mailboxes'), array('controller' => 'mailboxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Network Switches'), array('controller' => 'network_switches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Network Switch'), array('controller' => 'network_switches', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proximity Cards'), array('controller' => 'proximity_cards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proximity Card'), array('controller' => 'proximity_cards', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rooms'), array('controller' => 'rooms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Room'), array('controller' => 'rooms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Scanners'), array('controller' => 'scanners', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scanner'), array('controller' => 'scanners', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Subnets'), array('controller' => 'subnets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subnet'), array('controller' => 'subnets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tasks'), array('controller' => 'tasks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Task'), array('controller' => 'tasks', 'action' => 'add')); ?> </li>
	</ul>
</div>

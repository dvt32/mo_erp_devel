<div class="floors index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/addons/256/company_info.png', array('style' => 'height: 3em;')) ?>
		Floors
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				<legend>Filter &amp; Search</legend>
				<!-- filter and search -->
				<?= $this->Form->create('ProximityCard', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
				<div class="span8">

					<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge search-query')) ?>
					<?= $this->Form->input('building_id', array(
						'options' => $buildings,
						'empty' => '-- Building --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>

						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
						<?= $this->Form->end() ?>
					</div>
				</div>
			</div>
	</div>
	<br>
	<!-- /search & filter forms -->

	<table class="table table-condensed table-striped">
		<tr>
			<th><?php echo $this->Paginator->sort('label', 'Floor'); ?></th>
			<th><?php echo $this->Paginator->sort('building_id'); ?></th>
			<th><?php echo $this->Paginator->sort('short_global_label'); ?></th>
			<th><?php echo $this->Paginator->sort('employee_id', 'Office Manager'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt', 'Added'); ?></th>
		</tr>
		<?php foreach ($floors as $floor): ?>
			<tr>
				<td><?php echo $this->Html->link($floor['Floor']['label'], array('action' => 'view', $floor['Floor']['id'])); ?>&nbsp;</td>
				<td class="building">
				<?php echo $this->Html->link($floor['Building']['address_1'], array('controller' => 'buildings', 'action' => 'view', $floor['Building']['id'])); ?>
				</td>
				<td><?php echo h($floor['Floor']['short_global_label']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($floor['OfficeManager']['full_name'], array('controller' => 'employees', 'action' => 'view', $floor['OfficeManager']['id'])); ?>
				</td>
				<td><?php echo h($floor['Floor']['creation_dt']); ?>&nbsp;</td>
			</tr>
		<?php endforeach ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<div class="paging">
				<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
			</div>
		</div>
<script type="text/javascript">

	$("td.building").highlight(QueryString.q);

</script>

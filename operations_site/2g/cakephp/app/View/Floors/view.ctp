<script type="text/javascript" src="{{ operations_2g_assetprefix }}/js/libs/jquery.imagemapster.min.js"></script>
<style type="text/css">
	.desk-select {
		background-color: #eee;
		opacity:0.8;
		filter:alpha(opacity=80); /* For IE8 and earlier */
	}
	#dtable tr:hover {
		background-color: #ddd;
	}
</style>
<!-- Breadcrumbs -->
<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<div class="span12">
				<ul class="breadcrumb">
					<li><a href="{{ operations_2g_urlprefix }}/buildings"><?= $floor['Building']['city'] ?></a> <span class="divider">/</span></li>
					<li><a href="{{ operations_2g_urlprefix }}/buildings/view/<?= $floor['Floor']['building_id'] ?>"><?= $floor['Building']['address_1'] ?></a> <span class="divider">/</span></li>
					<li class="active"><?= $floor['Floor']['label'] ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- /Breadcrumbs -->
<!-- Info -->
<h3>
	<?= $this->Html->image('icons/flat_miniset/addons/256/company_info.png', array('style' => 'height: 2.5em;')) ?>
	<?= h($floor['Floor']['label']); ?>
</h3>
<p><?= $this->Html->image('icons/flat_miniset/png/256/front_desk.png' , array('style' => 'height:2em;')) ?> Office Manager: <a href="mailto:<?= $floor['OfficeManager']['email'] ?>">
		<?= $floor['OfficeManager']['full_name'] ?>
	</a>
	&nbsp;
</p>
<ul class="nav nav-pills">
	<li><a href="#/financials" id="financials">Financials</a></li>
	<li><a href="#/notifications" id="notifications">Notifications</a></li>
	<li><a href="#/deskmapper" id="deskmapper">Desk Map</a></li>
</ul>
<!-- /Info -->
<!-- Imagemap -->
<div class="row-fluid">
	<div class="span12 centered deskmapper">
		<div class="row-fluid">
			<div id="resize_map" class="span12">
				
				<?= $this->Html->image('deskmaps/floor_id_'.$floor['Floor']['id'].'.png', array(
					'usemap' => '#deskmap', 
					'id' => 'imagemap',
					'border' => 0,
					'width' => $floor['Floor']['map_width'],
					'height' => $floor['Floor']['map_height']
				) ); ?>
				<map name="deskmap" border="0">
					<?php foreach ($floor['Desk'] as $desk): ?>
					<area 
						shape="poly" 
						coords="<?= $desk['areas'] ?>" 
						href="#" 
						alink="{{ operations_2g_urlprefix }}/desks/view/<?= $desk['id'] ?>" 
						anumber="<?= $desk['assigned_number'] ?>" 
						istaken="<?= isset($desk['account_id']) ? 'true' : 'false' ; ?>"
						title="<?= $desk['assigned_number'] ?>"
					/>
					<?php endforeach ?>
				</map>
			</div>
		</div>
		<br/>
	</div>
</div>
<div class="desk-bar">
	<div class="desk-info"></div>
</div>
<!-- /Imagemap -->
<!-- Floor info -->
<div class="row-fluid">
	<div class="span12 pull-left">
		<div class="row-fluid">
			<div class="span12 financials" style="display:none;">
				<div class="hero-unit">

					<div class="row-fluid">
						<div class="span6">
							<table class="table">
								<tbody>
									<tr>
										<th>Total Current Revenue</th>
										<td><?= $financial['total_current_revenue'] ?>&nbsp;</td>
									</tr>
									<tr>
										<th>Potential Revenue</th>
										<td><?= $financial['potential_revenue'] ?>&nbsp;</td>
									</tr>
									<tr>
										<th>Revenue Lost</th>
										<td><?= $financial['revenue_lost'] ?>&nbsp;</td>
									</tr>
									<tr>
										<th>Internet/Telephone Revenue</th>
										<td><?= $financial['internet_phone_revenue'] ?>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="span6">
							<table class="table">
								<tbody>
									<tr>
										<th>New Business in the past 30 Days</th>
										<td><?= $financial['new_business_30'] ?>&nbsp;</td>
									</tr>
									<tr>
										<th>New Business in the past 60 Days</th>
										<td><?= $financial['new_business_60'] ?>&nbsp;</td>
									</tr>
									<tr>
										<th>New Business since the 1st of this Month</th>
										<td><?= $financial['new_business_this'] ?>&nbsp;</td>
									</tr>
									<tr>
										<th>New Business since the 1st of this Month</th>
										<td><?= $financial['new_business_last'] ?>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12 notifications" style="display:none;">
				<div class="related">
					<h3>Notifications</h3>
					<?php if (!empty($floor['Notification'])): ?>
						<table class="table table-condensed notifications_toggle" style="display:none;">
							<tr>
								<th><?= __('Name'); ?></th>
								<th><?= __('Description'); ?></th>
								<th><?= __('Room Id'); ?></th>
								<th><?= __('Floor Id'); ?></th>
								<th><?= __('Building Id'); ?></th>
								<th><?= __('Created'); ?></th>
								<th><?= __('Modified'); ?></th>
								<th><?= __('Is Active'); ?></th>
								<th class="actions"><?= __('Actions'); ?></th>
							</tr>
							<?php foreach ($floor['Notification'] as $notification): ?>
								<tr>
									<td><?= $notification['name']; ?></td>
									<td><?= $notification['description']; ?></td>
									<td><?= $notification['room_id']; ?></td>
									<td><?= $notification['floor_id']; ?></td>
									<td><?= $notification['building_id']; ?></td>
									<td><?= $notification['created']; ?></td>
									<td><?= $notification['modified']; ?></td>
									<td><?= $notification['is_active']; ?></td>
									<td class="actions">
										<?= $this->Html->link(__('View'), array('controller' => 'notifications', 'action' => 'view', $notification['id'])); ?>
										<?= $this->Html->link(__('Edit'), array('controller' => 'notifications', 'action' => 'edit', $notification['id'])); ?>
										<?= $this->Form->postLink(__('Delete'), array('controller' => 'notifications', 'action' => 'delete', $notification['id']), null, __('Are you sure you want to delete # %s?', $notification['id'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
						<?php else: ?>
						<div class="hero-unit notifications_toggle">
							<h1>No Notifications</h1>
						</div>
						<?php endif; ?>

					<div class="actions">
						<ul class="nav nav-pills">
							<li><?= $this->Html->link(__('New Notification'), array('controller' => 'notifications', 'action' => 'add')); ?> </li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="related">
	<h3><?= __('Desks on this Floor'); ?></h3>
	<button type="button" id="clear_filter" class="btn btn-inverse">Clear</button>
	<?php if (!empty($floor['Desk'])): ?>
		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?= __('Assigned Number'); ?></th>
					<th><?= __('Account'); ?></th>
					<th><?= __('Num People'); ?></th>
					<th><?= __('IsEnclosed'); ?></th>
					<th><?= __('List Price'); ?></th>
					<th><?= __('When Available'); ?></th>
					<th><?= __('Note'); ?></th>
					<th><?= __('Waiting Account'); ?></th>
					<th><?= __('Current Price'); ?></th>
					<th><?= __('Start Date'); ?></th>
					<th><?= __('End Date'); ?></th>
					<th><?= __('Comm Revenue'); ?></th>
					<th><?= __('Increase Date'); ?></th>
				</tr>
			</thead>
			<tbody id="dtable">
				<?php foreach ($floor['Desk'] as $desk): ?>
				<tr>
					<td><?= $this->Html->link($desk['assigned_number'], array('controller' => 'desks', 'action' => 'view', $desk['id'])); ?></td>
					<td>
						<?= (!empty($desk['CustomerAccount']['id'])) ? $this->Html->link(@$desk['CustomerAccount']['name'], array('controller' => 'customerAccounts', 'action' => 'view', @$desk['CustomerAccount']['id'])) : $this->Html->link(__('-- VACANT --'), array('controller' => 'desks', 'action' => 'edit', $desk['id'])); ?>
					</td>
					<td><?= $desk['num_people']; ?></td>
					<td><?= $desk['is_enclosed']; ?></td>
					<td><?= $desk['list_price']; ?></td>
					<td><?= $desk['when_available']; ?></td>
					<td><?= $desk['note']; ?></td>
					<td><?= $desk['waiting_account']; ?></td>
					<td><?= $desk['current_price']; ?></td>
					<td><?= $desk['start_date']; ?></td>
					<td><?= $desk['end_date']; ?></td>
					<td><?= $desk['internet_telephone_revenue']; ?></td>
					<td><?= $desk['increase_date']; ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php else: ?>
		<div class="hero-unit">
			<h1>No Floors Yet</h1>
			<div class="actions">
				<ul class="nav nav-pills">
					<li><?= $this->Html->link(__('Add a New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
				</ul>
			</div>
		</div>

		<?php endif; ?>

</div>
<script type="text/javascript">
	
	// height and width of the imagemap
	var imgH = $('#imagemap').height();
	var imgW = $('#imagemap').width();
	var aspectRatio = imgW/imgH;
	var resizeWidth = $('#resize_map').width();

	// Identify if a value is even
	function isEven(value) {
		if (value%2 == 0)
			return true;
		else
			return false;
	}

	$(document).ready(function() {

		$('#imagemap').mapster(
			{ 
				fillColor: '6DBCDB', 
				stroke: true,
				strokeColor: 'F7F9FA',
				strokeWidth: 0,
				setStatic: true,
				mapKey: 'istaken',
				fillOpacity: 0.4,
				areas: [
					{
						key: 'false',
				        staticState: true,
				        render_select: {
				            fillOpacity: 0.6,
				            fillColor: 'FC4349',
				        }
					},
					{
						key: 'true',
				        staticState: true,
				        render_select: {
				            fillOpacity: 0.6,
				            fillColor: '6DBCDB',
				        }
					}
				]
			}
		);
		$('#imagemap')
			.mapster('snapshot')
			.mapster('rebind', {
				clickNavigate: false,
				fillColor: '45C9C3', 
				strokeColor: '45C9C3',
				stroke: true,
				fillOpacity: 0.8,
				strokeWidth: 4,
				setStatic: true,
				mapKey: 'istaken',
   				mapKey: 'anumber',
			}, true);

		// resize to span10
		$('#imagemap').mapster('resize', resizeWidth, 0, 100);


		// Resize the map to fit within the boundaries provided
		var resizeTime = 100;     // total duration of the resize effect, 0 is instant
		var resizeDelay = 100;    // time to wait before checking the window size again
		                          // the shorter the time, the more reactive it will be.
		                          // short or 0 times could cause problems with old browsers.
		function resize(maxWidth,maxHeight) {
			var image =  $('#imagemap'),
			imgWidth = image.width(),
			imgHeight = image.height(),
			newWidth=0,
			newHeight=0;

			if (imgWidth/maxWidth>imgHeight/maxHeight) {
				newWidth = maxWidth;
			} else {
				newHeight = maxHeight;
			}
			image.mapster('resize',newWidth,newHeight,resizeTime);   
		}

		// Track window resizing events, but only actually call the map resize when the
		// window isn't being resized any more

		function onWindowResize() {

			var curWidth = $(window).width(),
			curHeight = $(window).height(),
			checking=false;
			if (checking) {
				return;
			}
			checking = true;
			window.setTimeout(function() {
				var newWidth = $(window).width(),
				newHeight = $(window).height();
				if (newWidth === curWidth && newHeight === curHeight) {
					resize(newWidth,newHeight); 
				}
			checking=false;
			},resizeDelay );
		}

		$(window).bind('resize',onWindowResize);
	});

	// On area click if selected
	$('area').click(function() {
		var anum = $(this).attr('anumber');
		$('tbody#dtable').children().each(function() {

			// if tr has class, deselect
			if ($(this).hasClass('desk-select')) {
				// first td
				if ($(this).children()[0].innerText == anum) {
					$(this).removeClass('desk-select');
				}
			} else {
				// first td
				if ($(this).children()[0].innerText == anum) {
					$(this).addClass('desk-select');
				}
			}

			tableShow();
			
		});
	});

	$('#clear_filter').click(function(event) {
		$('area').mapster('deselect');
		$('tr.desk-select').removeClass('desk-select');
		tableShow();
	});

	function tableShow() {
		// if no tr is selected show all
		if ($('tr.desk-select').length == 0) {
			$('tbody#dtable').children().show();
		} else {
			// only show the selected
			$('tbody#dtable').children().hide();
			$('tr.desk-select').show();
		}
	}

	compact('notifications', 'toggle');
	compact('financials', 'toggle');
	compact('deskmapper', 'toggle');

	// image map functions
	// http://www.outsharked.com/imagemapster/default.aspx?docs.html#select
	// will get all the assigned desk id's clicked on the map
	// $('#imagemap').mapster('get', 'anumber');
	// be able to set areas with specific options
	// $('area').mapster('set', 1, 'anumber', options);

</script>

<?php

App::uses('AppHelper', 'View/Helper');

class RbacHelper extends AppHelper {

/**
 * Check if the logged in user has the required role
 * 
 * @param string $required_role element value to detect
 * @return boolean true if the logged in user has the required role
 */
	public function checkRole($required_role) {
		$roles = AuthComponent::user('roles');
		return in_array($required_role, $roles);
	}

}

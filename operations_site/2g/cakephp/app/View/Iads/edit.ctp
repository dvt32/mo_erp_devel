<div class="iads form">
<?php echo $this->Form->create('Iad'); ?>
	<fieldset>
		<legend><?php echo __('Edit Iad'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('floor_id');
		echo $this->Form->input('manufacturer');
		echo $this->Form->input('model');
		echo $this->Form->input('num_ports');
		echo $this->Form->input('mac_address');
		echo $this->Form->input('gateway_name');
		echo $this->Form->input('ip_address');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('device_name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Iad.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Iad.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Iads'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('controller' => 'phone_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
	</ul>
</div>

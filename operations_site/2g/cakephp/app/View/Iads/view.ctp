<div class="iads view">
<h2><?php  echo __('Iad'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($iad['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $iad['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Manufacturer'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['manufacturer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Num Ports'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['num_ports']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mac Address'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['mac_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gateway Name'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['gateway_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Device Name'); ?></dt>
		<dd>
			<?php echo h($iad['Iad']['device_name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Iad'), array('action' => 'edit', $iad['Iad']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Iad'), array('action' => 'delete', $iad['Iad']['id']), null, __('Are you sure you want to delete # %s?', $iad['Iad']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Iads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Iad'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('controller' => 'phone_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Phone Numbers'); ?></h3>
	<?php if (!empty($iad['PhoneNumber'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Account Id'); ?></th>
		<th><?php echo __('Person Id'); ?></th>
		<th><?php echo __('Floor Id'); ?></th>
		<th><?php echo __('Phone Number'); ?></th>
		<th><?php echo __('Purpose'); ?></th>
		<th><?php echo __('Visibility'); ?></th>
		<th><?php echo __('Domain'); ?></th>
		<th><?php echo __('Extension'); ?></th>
		<th><?php echo __('Device Type'); ?></th>
		<th><?php echo __('Iad Id'); ?></th>
		<th><?php echo __('Ip Phone Id'); ?></th>
		<th><?php echo __('Device Port'); ?></th>
		<th><?php echo __('Is Custom Inbound Dialplan'); ?></th>
		<th><?php echo __('Is Custom Outbound Dialplan'); ?></th>
		<th><?php echo __('Do Call Forward'); ?></th>
		<th><?php echo __('Call Forward Delay'); ?></th>
		<th><?php echo __('Call Forward Dst Num'); ?></th>
		<th><?php echo __('Do Simring'); ?></th>
		<th><?php echo __('Simring Dst Num List'); ?></th>
		<th><?php echo __('Block Outbound Caller Id'); ?></th>
		<th><?php echo __('Is Voicemail On'); ?></th>
		<th><?php echo __('Voicemail Box Num'); ?></th>
		<th><?php echo __('Voicemail Pin'); ?></th>
		<th><?php echo __('Sip Register User Id'); ?></th>
		<th><?php echo __('Sip Register Password'); ?></th>
		<th><?php echo __('Voicemail As Email'); ?></th>
		<th><?php echo __('Email Addr For Vm'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th><?php echo __('Modification Dt'); ?></th>
		<th><?php echo __('Voicemail Delay'); ?></th>
		<th><?php echo __('Fax As Email'); ?></th>
		<th><?php echo __('Email Addr For Fax'); ?></th>
		<th><?php echo __('Hold Music'); ?></th>
		<th><?php echo __('Emit Debug Msg'); ?></th>
		<th><?php echo __('Inbound Trunk Id'); ?></th>
		<th><?php echo __('Outbound Trunk Id'); ?></th>
		<th><?php echo __('Sip Profile'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($iad['PhoneNumber'] as $phoneNumber): ?>
		<tr>
			<td><?php echo $phoneNumber['id']; ?></td>
			<td><?php echo $phoneNumber['account_id']; ?></td>
			<td><?php echo $phoneNumber['person_id']; ?></td>
			<td><?php echo $phoneNumber['floor_id']; ?></td>
			<td><?php echo $phoneNumber['phone_number']; ?></td>
			<td><?php echo $phoneNumber['purpose']; ?></td>
			<td><?php echo $phoneNumber['visibility']; ?></td>
			<td><?php echo $phoneNumber['domain']; ?></td>
			<td><?php echo $phoneNumber['extension']; ?></td>
			<td><?php echo $phoneNumber['device_type']; ?></td>
			<td><?php echo $phoneNumber['iad_id']; ?></td>
			<td><?php echo $phoneNumber['ip_phone_id']; ?></td>
			<td><?php echo $phoneNumber['device_port']; ?></td>
			<td><?php echo $phoneNumber['is_custom_inbound_dialplan']; ?></td>
			<td><?php echo $phoneNumber['is_custom_outbound_dialplan']; ?></td>
			<td><?php echo $phoneNumber['do_call_forward']; ?></td>
			<td><?php echo $phoneNumber['call_forward_delay']; ?></td>
			<td><?php echo $phoneNumber['call_forward_dst_num']; ?></td>
			<td><?php echo $phoneNumber['do_simring']; ?></td>
			<td><?php echo $phoneNumber['simring_dst_num_list']; ?></td>
			<td><?php echo $phoneNumber['block_outbound_caller_id']; ?></td>
			<td><?php echo $phoneNumber['is_voicemail_on']; ?></td>
			<td><?php echo $phoneNumber['voicemail_box_num']; ?></td>
			<td><?php echo $phoneNumber['voicemail_pin']; ?></td>
			<td><?php echo $phoneNumber['sip_register_user_id']; ?></td>
			<td><?php echo $phoneNumber['sip_register_password']; ?></td>
			<td><?php echo $phoneNumber['voicemail_as_email']; ?></td>
			<td><?php echo $phoneNumber['email_addr_for_vm']; ?></td>
			<td><?php echo $phoneNumber['is_active']; ?></td>
			<td><?php echo $phoneNumber['note']; ?></td>
			<td><?php echo $phoneNumber['ms_access_creation_dt']; ?></td>
			<td><?php echo $phoneNumber['ms_access_random_num']; ?></td>
			<td><?php echo $phoneNumber['creation_dt']; ?></td>
			<td><?php echo $phoneNumber['modification_dt']; ?></td>
			<td><?php echo $phoneNumber['voicemail_delay']; ?></td>
			<td><?php echo $phoneNumber['fax_as_email']; ?></td>
			<td><?php echo $phoneNumber['email_addr_for_fax']; ?></td>
			<td><?php echo $phoneNumber['hold_music']; ?></td>
			<td><?php echo $phoneNumber['emit_debug_msg']; ?></td>
			<td><?php echo $phoneNumber['inbound_trunk_id']; ?></td>
			<td><?php echo $phoneNumber['outbound_trunk_id']; ?></td>
			<td><?php echo $phoneNumber['sip_profile']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'phone_numbers', 'action' => 'view', $phoneNumber['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'phone_numbers', 'action' => 'edit', $phoneNumber['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'phone_numbers', 'action' => 'delete', $phoneNumber['id']), null, __('Are you sure you want to delete # %s?', $phoneNumber['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

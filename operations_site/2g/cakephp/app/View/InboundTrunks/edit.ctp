<div class="inboundTrunks form">
<?php echo $this->Form->create('InboundTrunk'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inbound Trunk'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('trunk_name');
		echo $this->Form->input('trunk_type');
		echo $this->Form->input('location');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InboundTrunk.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('InboundTrunk.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inbound Trunks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Phone Numbers'), array('controller' => 'phone_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('controller' => 'phone_numbers', 'action' => 'add')); ?> </li>
	</ul>
</div>

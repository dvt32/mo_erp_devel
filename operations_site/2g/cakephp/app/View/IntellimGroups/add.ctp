<div class="intellimGroups form">
<?php echo $this->Form->create('IntellimGroup'); ?>
	<fieldset>
		<legend><?php echo __('Add Intellim Group'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('intellim_id');
		echo $this->Form->input('zone_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Intellim Groups'), array('action' => 'index')); ?></li>
	</ul>
</div>

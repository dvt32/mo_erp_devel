<div class="intellimGroups form">
<?php echo $this->Form->create('IntellimGroup'); ?>
	<fieldset>
		<legend><?php echo __('Edit Intellim Group'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('intellim_id');
		echo $this->Form->input('zone_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('IntellimGroup.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('IntellimGroup.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Intellim Groups'), array('action' => 'index')); ?></li>
	</ul>
</div>

<div class="intellimGroups view">
<h2><?php echo __('Intellim Group'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($intellimGroup['IntellimGroup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($intellimGroup['IntellimGroup']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Intellim Id'); ?></dt>
		<dd>
			<?php echo h($intellimGroup['IntellimGroup']['intellim_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zone Id'); ?></dt>
		<dd>
			<?php echo h($intellimGroup['IntellimGroup']['zone_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Intellim Group'), array('action' => 'edit', $intellimGroup['IntellimGroup']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Intellim Group'), array('action' => 'delete', $intellimGroup['IntellimGroup']['id']), null, __('Are you sure you want to delete # %s?', $intellimGroup['IntellimGroup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Intellim Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Intellim Group'), array('action' => 'add')); ?> </li>
	</ul>
</div>

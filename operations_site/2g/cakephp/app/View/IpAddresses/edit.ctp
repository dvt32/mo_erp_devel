<div class="ipAddresses form">
<?php echo $this->Form->create('IpAddress'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ip Address'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('account_id', array('options' => $customerAccounts, 'empty' => 'Choose an Account'));
		echo $this->Form->input('ip_address');
		echo $this->Form->input('note');
		echo $this->Form->input('hostname');
		echo $this->Form->input('isp_id', array('options' => $isps));
		echo $this->Form->input('building_id');
		echo $this->Form->input('rir_owner_id', array('type' => 'text'));
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<div class="ipAddresses index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/server.png', array('style' => 'height: 3em;')) ?>
		<?= __('IP Addresses'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<legend>Filter &amp; Search</legend>
			<div class="row-fluid">
				<!-- filter and search -->
				<div class="span8">
					<?= $this->Form->create('IpAddress', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
						<?= $this->Form->input('q', array('placeholder' => 'Search Numbers', 'class' => 'input-xlarge search-query')) ?>
						<?= $this->Form->input('building_id', array(
							'options' => $buildings, 
							'empty' => '-- Building --',
							'class' => 'input-auto',
							'style' => 'margin-bottom:0px;')
						) ?>
						<?= $this->Form->input('isp_id', array(
								'options' => $isps, 
								'empty' => '-- ISP --',
								'class' => 'input-auto',
								'style' => 'margin-bottom:0px;')
						) ?>
						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
					<?= $this->Form->end() ?>
				</div>
				<div class="span4">
					<?= __('Results per page') ?>
					<?= $this->Html->link('10', array('?' => array('limit' => 10) + $this->request->query)) ?>
					<?= $this->Html->link('20', array('?' => array('limit' => 20) + $this->request->query)) ?>
					<?= $this->Html->link('50', array('?' => array('limit' => 50) + $this->request->query)) ?>
					<?= $this->Html->link('100', array('?' => array('limit' => 100) + $this->request->query)) ?>
				</div>
			</div>
		</div>
	</div>
	<br>
	<!-- /search & filter forms -->
	<table class="table table-condensed">
		<tr>
			<th><?= $this->Paginator->sort('account_id'); ?></th>
			<th><?= $this->Paginator->sort('ip_address'); ?></th>
			<th><?= $this->Paginator->sort('isp_id'); ?></th>
			<th><?= $this->Paginator->sort('building_id'); ?></th>
			<th><?= $this->Paginator->sort('creation_dt', 'Created'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
		</tr>
		<?php foreach ($ipAddresses as $ipAddress): ?>
			<tr>
				<td>
					<?= $this->Html->link($ipAddress['CustomerAccount']['name'], array('controller' => 'customerAccount', 'action' => 'view', $ipAddress['CustomerAccount']['id'])); ?>&nbsp;
				</td>
				<td><?= h($ipAddress['IpAddress']['ip_address']); ?>&nbsp;</td>
				<td>
					<?= $this->Html->link($ipAddress['Isp']['name'], array('controller' => 'isps', 'action' => 'view', $ipAddress['IpAddress']['isp_id'])); ?>&nbsp;
				</td>
				<td>
					<?= $this->Html->link($ipAddress['Building']['address_1'], array('controller' => 'buildings', 'action' => 'view', $ipAddress['Building']['id'])); ?>
				</td>
				<td><?= h($ipAddress['IpAddress']['creation_dt']); ?>&nbsp;</td>
				<td class="actions">
					<?= $this->Html->link(__('View'), array('action' => 'view', $ipAddress['IpAddress']['id'])); ?>
					<?= $this->Html->link(__('Edit'), array('action' => 'edit', $ipAddress['IpAddress']['id'])); ?>
					<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $ipAddress['IpAddress']['id']), null, __('Are you sure you want to delete # %s?', $ipAddress['IpAddress']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<div class="paging">
				<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
			</div>
		</div>

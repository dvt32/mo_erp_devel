<div class="ipAddresses view">
<h2><?php echo __('Ip Address'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ipAddress['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $ipAddress['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hostname'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['hostname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Isp Id'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['isp_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Building'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ipAddress['Building']['full_address'], array('controller' => 'buildings', 'action' => 'view', $ipAddress['Building']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rir Owner Id'); ?></dt>
		<dd>
			<?php echo h($ipAddress['IpAddress']['rir_owner_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Edit Ip Address'), array('action' => 'edit', $ipAddress['IpAddress']['id'])); ?> </li>
		<li class="active"><?php echo $this->Html->link(__('List Ip Addresses'), array('action' => 'index')); ?> </li>
	</ul>
</div>

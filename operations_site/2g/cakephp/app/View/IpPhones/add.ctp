<div class="ipPhoneTbls form">
<?php echo $this->Form->create('IpPhone'); ?>
	<fieldset>
		<legend><?php echo __('Add Ip Phone Tbl'); ?></legend>
	<?php
		echo $this->Form->input('manufacturer');
		echo $this->Form->input('model');
		echo $this->Form->input('mac_address');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('ownership');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ip Phone Tbls'), array('action' => 'index')); ?></li>
	</ul>
</div>

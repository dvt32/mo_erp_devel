<div class="ipPhones index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/phone.png', array('style' => 'height: 3em;')) ?>
		<?= __('IP Phones'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<legend>Filter &amp; Search</legend>
			<div class="row-fluid">
				<!-- filter and search -->
				<div class="span8">
					<?= $this->Form->create('PhoneNumber', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
						<?= $this->Form->input('q', array('placeholder' => 'Search Numbers', 'class' => 'input-xlarge search-query')) ?>
						<?= $this->Form->input('manufacturer', array(
							'options' => $manufacturers, 
							'empty' => '-- Manufacturer --',
							'class' => 'input-auto',
							'style' => 'margin-bottom:0px;')
							) ?>
						<?= $this->Form->input('ownership', array(
							'options' => $ownerships, 
							'empty' => '-- Ownership --',
							'class' => 'input-auto',
							'style' => 'margin-bottom:0px;')
							) ?>
						<?= $this->Form->input('model', array(
							'options' => $models, 
							'empty' => '-- Models --',
							'class' => 'input-auto',
							'style' => 'margin-bottom:0px;')
							) ?>
						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
					<?= $this->Form->end() ?>
				</div>
				<div class="span4">
					<?= __('Results per page') ?>
					<?= $this->Html->link('10', array('?' => array('limit' => 10) + $this->request->query)) ?>
					<?= $this->Html->link('20', array('?' => array('limit' => 20) + $this->request->query)) ?>
					<?= $this->Html->link('50', array('?' => array('limit' => 50) + $this->request->query)) ?>
					<?= $this->Html->link('100', array('?' => array('limit' => 100) + $this->request->query)) ?>
				</div>
			</div>
		</div>
	</div>
	<br>
	<!-- /search & filter forms -->
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('manufacturer'); ?></th>
			<th><?= $this->Paginator->sort('model'); ?></th>
			<th><?= $this->Paginator->sort('mac_address'); ?></th>
			<th><?= $this->Paginator->sort('note'); ?></th>
			<th><?= $this->Paginator->sort('ownership'); ?></th>
			<th><?= $this->Paginator->sort('creation_dt', 'Created'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($ipPhones as $ipPhone): ?>
	<tr>
		<td><?= h($ipPhone['IpPhone']['manufacturer']); ?>&nbsp;</td>
		<td><?= h($ipPhone['IpPhone']['model']); ?>&nbsp;</td>
		<td><?= h($ipPhone['IpPhone']['mac_address']); ?>&nbsp;</td>
		<td><?= h($ipPhone['IpPhone']['note']); ?>&nbsp;</td>
		<td><?= h($ipPhone['IpPhone']['ownership']); ?>&nbsp;</td>
		<td><?= h($ipPhone['IpPhone']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('View'), array('action' => 'view', $ipPhone['IpPhone']['id'])); ?>
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $ipPhone['IpPhone']['id'])); ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $ipPhone['IpPhone']['id']), null, __('Are you sure you want to delete # %s?', $ipPhone['IpPhone']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Ip Phone '), array('action' => 'add')); ?></li>
	</ul>
</div>

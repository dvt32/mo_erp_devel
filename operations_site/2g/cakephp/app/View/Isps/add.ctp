<div class="isps form">
<?php echo $this->Form->create('Isp'); ?>
	<fieldset>
		<legend><?php echo __('Add Isp'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('short_name');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Isps'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>

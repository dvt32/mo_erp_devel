<div class="isps index">
	<h2><?php echo __('Isps'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('short_name'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($isps as $isp): ?>
	<tr>
		<td><?php echo h($isp['Isp']['id']); ?>&nbsp;</td>
		<td><?php echo h($isp['Isp']['name']); ?>&nbsp;</td>
		<td><?php echo h($isp['Isp']['short_name']); ?>&nbsp;</td>
		<td><?php echo h($isp['Isp']['note']); ?>&nbsp;</td>
		<td><?php echo h($isp['Isp']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($isp['Isp']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($isp['Isp']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $isp['Isp']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $isp['Isp']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $isp['Isp']['id']), null, __('Are you sure you want to delete # %s?', $isp['Isp']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Isp'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>

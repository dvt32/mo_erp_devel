<div class="isps view">
<h2><?php echo __('Isp'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($isp['Isp']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($isp['Isp']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Name'); ?></dt>
		<dd>
			<?php echo h($isp['Isp']['short_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($isp['Isp']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($isp['Isp']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($isp['Isp']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($isp['Isp']['creation_dt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Isp'), array('action' => 'edit', $isp['Isp']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Isp'), array('action' => 'delete', $isp['Isp']['id']), null, __('Are you sure you want to delete # %s?', $isp['Isp']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Isps'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Isp'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Addresses'), array('controller' => 'ip_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Ip Addresses'); ?></h3>
	<?php if (!empty($isp['IpAddress'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Account Id'); ?></th>
		<th><?php echo __('Ip Address'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Creation Dt'); ?></th>
		<th><?php echo __('Hostname'); ?></th>
		<th><?php echo __('Isp Id'); ?></th>
		<th><?php echo __('Building Id'); ?></th>
		<th><?php echo __('Rir Owner Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($isp['IpAddress'] as $ipAddress): ?>
		<tr>
			<td><?php echo $ipAddress['id']; ?></td>
			<td><?php echo $ipAddress['account_id']; ?></td>
			<td><?php echo $ipAddress['ip_address']; ?></td>
			<td><?php echo $ipAddress['note']; ?></td>
			<td><?php echo $ipAddress['ms_access_creation_dt']; ?></td>
			<td><?php echo $ipAddress['ms_access_random_num']; ?></td>
			<td><?php echo $ipAddress['creation_dt']; ?></td>
			<td><?php echo $ipAddress['hostname']; ?></td>
			<td><?php echo $ipAddress['isp_id']; ?></td>
			<td><?php echo $ipAddress['building_id']; ?></td>
			<td><?php echo $ipAddress['rir_owner_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ip_addresses', 'action' => 'view', $ipAddress['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ip_addresses', 'action' => 'edit', $ipAddress['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ip_addresses', 'action' => 'delete', $ipAddress['id']), null, __('Are you sure you want to delete # %s?', $ipAddress['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ip Address'), array('controller' => 'ip_addresses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

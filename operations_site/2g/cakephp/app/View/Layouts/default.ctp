<!DOCTYPE html>
<html lang="en">
<head>
	<?= $this->Html->charset() ?>
	<?= $this->fetch('meta') ?>
	<title>MicroOffice Ops | <?= $title_for_layout ?></title>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Pith Studio">

	<?= $this->Html->meta('icon') ?>

	<!-- libero -->
	<?= $this->Html->css('libs/libero/theme') ?>
	<?= $this->Html->css('libs/libero/icons/font-awesome/css/font-awesome') ?>
	
	<!-- bootstrap -->
	<?= $this->Html->css('libs/bootstrap.custom') ?>
	<?= $this->Html->css('libs/datetimepicker') ?>
	<?= $this->Html->css('libs/bootstrap-responsive.custom') ?>
	<?= $this->Html->css('style') ?>
	<?= $this->Html->css('nav-bar') ?>
	<?= $this->Html->css('flat-ui-btn') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('libs/script') ?>
	<?= __('<script>var baseUrl = "%s";</script>', Router::url('/',true)) ?>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]> <script src="{{ operations_2g_assetprefix }}/js/html5shiv.js"></script> <![endif]-->
	 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-144-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-72-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-57-precomposed.png" />
	
	<link rel="shortcut icon" href="{{ operations_2g_assetprefix }}/favicon.ico" />

	<!--JAVASCRIPTS-->
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/jquery-1.9.1.min.js"></script>
	<!-- <script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script> -->
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/underscore-min.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap-datetimepicker.min.js"></script>
	<?= $this->Html->script('libs/jquery.highlight.min') ?>
	<script src="{{ operations_2g_assetprefix }}/js/utilities.min.js"></script>

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-50113963-1', 'microoffice.com');
	ga('send', 'pageview');

	</script>
</head>

<body>
	<div class="frame">
		<div class="sidebar nav-leave">
			<div class="wrapper">

				<!-- Brand -->
				<a href="{{ operations_2g_urlprefix }}/" class="profile">
					<img src="{{ operations_2g_assetprefix }}/img/logo.png" class="avatar pull-left" style="margin-right: 15px; height:2.5em; " alt="MicroOffice" >
				</a>
				<!-- /Brand -->

				<!-- Navigation links -->
				<ul class="nav nav-list">
					<li class="nav-header">Navigation</li>
					<li>
						<a href="{{ operations_2g_urlprefix }}/employees/dashboard">
							<i class="icon-dashboard"></i>Dashboard
						</a>
					</li>
					<!-- Sales -->
					<li id="nav_sales" class="nav-sub">
						<a href="#/sales">
							<i class="icon-star"></i>Sales
						</a>
					</li>
					<!-- /Sales -->

					<!-- Billing -->
					<li id="nav_billing" class="nav-sub">
						<a href="#/billing">
							<i class="icon-group"></i>Billing
						</a>
					</li>
					<!-- /Billing -->

					<!-- Marketing -->
					<!--      
					<li id="nav_marketing" class="nav-sub">
						<a href="#/marketing">
							<i class="icon-group"></i>Marketing
						</a>
					</li>
					-->
					<!-- /Marketing -->
					<!-- Licenses -->
					<li id="nav_licenses" class="nav-sub">
						<a href="#/licenses">
							<i class="icon-file-alt"></i>Licenses
						</a>
					</li>
					<!-- /Licenses -->
					<!-- Directory -->
					<li id="nav_dir" class="nav-sub">
						<a href="#/directories">
							<i class="icon-book"></i>Directory
						</a>
					</li>
					<!-- /Directory -->
					<!-- Resources -->
					<li id="nav_resources" class="nav-sub">
						<a href="#/resources">
							<i class="icon-wrench"></i>Resources
						</a>
					</li>
					<!-- /Resources -->
					<!-- Tasks -->
					<li id="nav_tasks" class="nav-sub">
						<a href="#/tasks">
							<i class="icon-check"></i>Processes and Tasks
						</a>
					</li>
					<!-- /Tasks -->
					<!-- Comm -->
					<li id="nav_comm" class="nav-sub">
						<a href="#/comm">
							<i class="icon-signal"></i>Communications and Imaging
						</a>
					</li>
					<!-- /Comm -->
					
					<!-- Finances -->
<!-- 
					<li id="nav_finances" class="nav-sub">
						<a href="#/finances">
							<i class="icon-credit-card"></i>Finance
						</a>
					</li>
 -->
					<!-- /Finances -->
					<!-- Reports -->
<!--           
					<li id="nav_reports" class="nav-sub">
						<a href="#/reports">
							<i class="icon-print"></i>Printed Reports
						</a>
					</li>
 -->          
					<!-- /Reports -->
					<!-- Docs -->
					<li id="nav_docs" class="nav-sub">
						<a href="#/docs">
							<i class="icon-question"></i>Documentation and Help
						</a>
					</li>
					<!-- /Docs -->

					<!-- Administration -->
          
					<li id="nav_authorization" class="nav-sub">
						<a href="#/admin">
							<i class="icon-lock"></i>Authorization
						</a>
					</li>
          
					<!-- /Administration -->
					<li class="nav-sub">
						<a href="{{ operations_2g_urlprefix }}/employees/logout">
							<i class="icon-signout"></i>
							Log Out
						</a>
					</li>
				</ul>

			</div>
		</div>
		<div class="content">
			<div id="nav-expand" class="sidebar2 nav-leave" style="display:none;">
				<div class="wrapper">
					<!-- submenu -->
					<div class="nav-expanded"></div>
					<!-- /submenu -->
					<div class="footer">
						<p>
							&copy; 2014 Pith Studio LLC
						</p>
					</div>
				</div>
			</div>
			<!-- nav views -->
			<div class="navbar">
				<div class="navbar-inner">
					<a href="javascript:void(0);" class="pull-left toggle-sidebar hidden-desktop">
						<i class="icon-reorder"></i>
					</a>
					<!-- <a class="brand">MicroOffice</a> -->
				</div>
			</div>
			<!-- /.nav views -->

			<!-- Content Body -->
			<div class="content-body">
				<?= $this->Session->flash() ?>
				<?= $this->fetch('content') ?>
				<!-- <div id="push"></div> -->
			</div>

		</div><!--/.content-->

	</div><!-- /.frame -->

	<!-- Le javascript -->
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap.min.js"></script>
	<?= $this->Html->script("libs/chosen/chosen.jquery.min") ?>
	<?= $this->Html->css("libs/chosen/chosen.css") ?>

<!-- 
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-transition.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-alert.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-modal.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-dropdown.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-scrollspy.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-tab.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-tooltip.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-popover.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-button.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-collapse.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-carousel.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap-typeahead.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap-datetimepicker.min.js"></script>
-->

	<!-- WYSIHTML5 TEXT EDITOR -->
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/wysihtml5/advanced.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/wysihtml5/wysihtml5-0.3.0.min.js"></script>
	<!-- AUTOSIZE TEXTAREA -->
	<!-- <script src="{{ operations_2g_assetprefix }}/js/libs/libero/autosize/jquery.autosize-min.js"></script> -->
	<!-- BOOTSTRAP DATEPICKER -->
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/bootstrap-datepicker.js"></script>
	<!-- FLOT CHART -->
<!--
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.pie.js"></script>
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/flot/jquery.flot.resize.js"></script>
-->
	<!-- MIXITUP -->
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/mixitup/jquery.mixitup.min.js"></script>
	<!-- THEME -->
	<script src="{{ operations_2g_assetprefix }}/js/libs/libero/theme.js"></script>
	<!-- Pith -->
	<script src="{{ operations_2g_assetprefix }}/js/nav.js"></script>
	<!-- JS Activate helpers -->
	<script type="text/javascript">

		// use chosen plugin
		$(".chosen").chosen({
			allow_single_deselect: true
		});

		// turn inputs with time/date/datetime class into datetime pickers
		$(function() {
			$("input.date").datepicker({
				format: 'yyyy-mm-dd',
				language: 'en',
				pickTime: false,
				autoClose: true,
				todayBtn: true
			}).attr('readonly',true);

			$("input.datetime").datetimepicker({
				format: 'yyyy-mm-dd hh:mm:ss',
				language: 'en',
				autoClose: true,
				todayBtn: true
			}).attr('readonly',true);
		});

		// popovers
		$('.help').popover({trigger:'hover'});

		// submits form when dropdown value is selected
		$(".click-submit").change(function() {
			this.form.submit();
		});
</script>
</body>
</html>

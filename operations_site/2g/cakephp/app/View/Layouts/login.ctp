<!DOCTYPE html>
<html lang="en">
<head>
  <?= $this->Html->charset() ?>
  <title><?= $title_for_layout ?> </title>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Pith Studio">

  <?= $this->Html->meta('icon') ?>
  <?= $this->Html->css('libs/bootstrap.custom') ?>
  <?= $this->Html->css('libs/datetimepicker') ?>
  <?= $this->Html->css('libs/bootstrap-responsive.custom') ?>
  <?= $this->Html->css('style') ?>
  <!-- libero -->
  <?= $this->Html->css('libs/libero/theme') ?>
  <?= $this->Html->css('libs/libero/icons/font-awesome/css/font-awesome') ?>
  <?= $this->Html->css('style') ?>

  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('libs/script') ?>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]> <script src="{{ operations_2g_assetprefix }}/js/html5shiv.js"></script> <![endif]-->
<!-- 
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="{{ operations_2g_assetprefix }}/ico/apple-touch-icon-57-precomposed.png">
 -->  
  <link rel="shortcut icon" href="{{ operations_2g_assetprefix }}/favicon.ico">

  <!--JAVASCRIPTS-->
  <script src="{{ operations_2g_assetprefix }}/js/libs/libero/jquery-1.9.1.min.js"></script>
  <script src="{{ operations_2g_assetprefix }}/js/libs/libero/jquery-ui-1.10.1.custom.min.js"></script>
  <script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap/bootstrap.min.js"></script>
  <script src="{{ operations_2g_assetprefix }}/js/libs/underscore-min.js"></script>
  <script src="{{ operations_2g_assetprefix }}/js/libs/bootstrap-datetimepicker.min.js"></script>

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50113963-1', 'microoffice.com');
  ga('send', 'pageview');

  </script>
</head>

<body>

  <div class="frame">
    
    <?= $this->Session->flash() ?>
    <?= $this->fetch('content') ?>
    <div id="push"></div>

  </div><!-- /.frame -->



  <!-- WYSIHTML5 TEXT EDITOR -->
  <script src="js/wysihtml5/advanced.js"></script>
  <script src="js/wysihtml5/wysihtml5-0.3.0.min.js"></script>
  <!-- AUTOSIZE TEXTAREA -->
  <script src='js/autosize/jquery.autosize-min.js'></script>
  <!-- MIXITUP -->
  <script src="js/mixitup/jquery.mixitup.min.js"></script>
  <!-- THEME -->
  <script src="js/theme.js"></script>

  </script>
  </body>

</body>
</html>

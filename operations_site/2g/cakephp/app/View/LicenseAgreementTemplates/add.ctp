<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements">License Agreements</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreementTemplates">License Agreements Templates</a> <span class="divider">/</span></li>
	  <li class="active">License Agreements Add<span class="divider">/</span></li>
	</ul>
	<div class="span8 offset2">
		<?= $this->Form->create('LicenseAgreementTemplate', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>	
				<legend>New License Agreement Template</legend>
				<?= $this->Form->input('name', array('type' => 'text')) ?>
				<?= $this->Form->input('building_id', array('empty' => '')) ?>
				<?= $this->Form->input('template') ?>
				<?= $this->Form->hidden('modifier_id', array('value' => AuthComponent::user('id'))) ?>
			</fieldset>
			<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>

<script>
	$("#LicenseAgreementTemplateTemplate").ckeditor();
</script>

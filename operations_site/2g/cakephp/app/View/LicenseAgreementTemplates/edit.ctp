<div class="row-fluid">
	<ul class="nav nav-tabs">
		<li><?= $this->Html->link('View', array('action' => 'view', $id)) ?></li>
		<li class="active"><?= $this->Html->link('Edit', '#') ?></li>
		<li class="pull-right"><?= $this->Html->link('See All', array('action' => 'index')) ?></li>
	</ul>
</div>

<div class="row-fluid">
	<div class="span12">
		<?= $this->Form->create('LicenseAgreementTemplate', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>	
				<legend>Edit License Agreement Template</legend>
				<?= $this->Form->input('id') ?>
				<?= $this->Form->input('name', array('type' => 'text')) ?>
				<?= $this->Form->input('building_id', array('empty' => '')) ?>
				<?= $this->Form->input('template') ?>
				<?= $this->Form->hidden('modifier_id', array('value' => AuthComponent::user('id'))) ?>
			</fieldset>
			<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>
<?= $this->Html->css('libs/ckeditor/contents.css') ?>
<script>
	$("#LicenseAgreementTemplateTemplate").ckeditor();
</script>

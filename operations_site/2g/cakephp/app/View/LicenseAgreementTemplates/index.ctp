<div class="licenseAgreementTemplates index">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements">License Agreements</a> <span class="divider">/</span></li>
	  <li class="active">License Agreements Templates<span class="divider">/</span></li>
	</ul>
	<h2><?= __('License Agreement Templates') ?></h2>
	<?= $this->Html->link(__('Create New Agreement Template'), array('action' => 'add'), array('class' => 'btn btn-primary')) ?>
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('name') ?></th>
				<th><?= $this->Paginator->sort('building_id') ?></th>
				<th><?= $this->Paginator->sort('created') ?></th>
				<th><?= $this->Paginator->sort('modified') ?></th>
				<th><?= $this->Paginator->sort('modifier_id') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($licenseAgreementTemplates as $licenseAgreementTemplate): ?>
				<tr>
					<td><?= $this->Html->link(__($licenseAgreementTemplate['LicenseAgreementTemplate']['name']), array('action' => 'edit', $licenseAgreementTemplate['LicenseAgreementTemplate']['id'])) ?></td>
					<td><?= $licenseAgreementTemplate['Building']['full_address'] ?>&nbsp;</td>
					<td><?= h($licenseAgreementTemplate['LicenseAgreementTemplate']['created']) ?>&nbsp;</td>
					<td><?= h($licenseAgreementTemplate['LicenseAgreementTemplate']['modified']) ?>&nbsp;</td>
					<td><?= $this->Html->link($licenseAgreementTemplate['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $licenseAgreementTemplate['Employee']['id'])) ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>


<div class="row-fluid">
	<ul class="nav nav-tabs">
		<li class="active"><?= $this->Html->link('View', '#') ?></li>
		<li><?= $this->Html->link('Edit', array('action' => 'edit', $id)) ?></li>
	</ul>
</div>

<section class="module">
	<div class="module-head">
		<b><?= __('License Agreement Template') ?></b>
	</div>
	<div class="module-body">
		<dl>
			<dt><?= __('Building') ?></dt>
			<dd><?= $this->Html->link($licenseAgreementTemplate['Building']['full_address'], array('controller' => 'buildings', 'action' => 'view', $licenseAgreementTemplate['Building']['id'])) ?> &nbsp; </dd>
			<dt><?= __('Created') ?></dt>
			<dd><?= h($licenseAgreementTemplate['LicenseAgreementTemplate']['created']) ?> &nbsp; </dd>
			<dt><?= __('Id') ?></dt>
			<dd><?= h($licenseAgreementTemplate['LicenseAgreementTemplate']['id']) ?> &nbsp; </dd>
			<dt><?= __('Modified') ?></dt>
			<dd><?= h($licenseAgreementTemplate['LicenseAgreementTemplate']['modified']) ?> &nbsp; </dd>
			<dt><?= __('Employee') ?></dt>
			<dd><?= $this->Html->link($licenseAgreementTemplate['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $licenseAgreementTemplate['Employee']['id'])) ?> &nbsp; </dd>
			<dt><?= __('Name') ?></dt>
			<dd><?= h($licenseAgreementTemplate['LicenseAgreementTemplate']['name']) ?> &nbsp; </dd>
		</dl>
	</div>
</section>
<section class="module">
	<div class="module-head">
		<b>Template Body</b>
	</div>
	<div class="module-body">
		<article><?= ($licenseAgreementTemplate['LicenseAgreementTemplate']['template']) ?> &nbsp; </article>
	</div>
</section>

<?php $phone_services = array_self_combine(array('Analog','None','VOIP')) ?>
<?php $signing_entity = array_self_combine(array('Company','Person')) ?>
<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements">License Agreements</a> <span class="divider">/</span></li>
	  <li class="active">Create New License Agreement<span class="divider">/</span></li>
	</ul>
	<div class="span12">
		<?= $this->Form->create('LicenseAgreement', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>
				<legend><?= __('Create License Agreement') ?></legend>
				<div class="row-fluid">
					<div class="span4">
						
						<?php if (empty($this->request->data['LicenseAgreement']['prospective_customer_id'])): ?>
							<?= $this->Form->input('prospect') ?>
							<p class="muted"> Begin by typing a prospective customer's name or company. Or <?= $this->Html->link('Add New Prospect', array('controller' => 'prospects', 'action' => 'add')) ?></p>
						<?php endif ?>

						<!-- allowed is shown only after a prospect is selected -->
						<div class="allowed">


						<?= $this->Form->input('building_id', array('empty' => '-- Building --')) ?>
						<?= $this->Form->input('spacenumber', array('label' => 'Office / Space Number')) ?>

						<?= $this->Form->input('signing_entity', array('options' => $signing_entity, 'empty' => '')) ?>
						<?= $this->Form->input('company') ?>
						<?= $this->Form->input('first_name') ?>
						<?= $this->Form->input('last_name') ?>

						<?= $this->Form->input('title') ?>

						<?= $this->Form->input('phone') ?>
						<?= $this->Form->input('email') ?>

						</div>
					</div>
					<div class="span4">
						<div class="allowed">
							<?= $this->Form->input('business_type') ?>
							<?= $this->Form->input('desks', array('label' => 'Number of desks needed', 'type' => 'text')) ?>
							<?= $this->Form->input('desk_amount', array('label' => 'Price per desk', 'type' => 'text')) ?>
							<?= $this->Form->input('offices', array('label' => 'Number of offices needed', 'type' => 'text')) ?>
							<?= $this->Form->input('office_amount', array('label' => 'Price per office', 'type' => 'text')) ?>
							<?= $this->Form->input('phone_lines', array('label' => 'Number of phone lines needed', 'type' => 'text')) ?>
							<?= $this->Form->input('phone_amount', array('label' => 'Phone Fee', 'type' => 'text')) ?>
							<?= $this->Form->input('persons', array('label' => 'Number of people with internet', 'type' => 'text')) ?>
							<?= $this->Form->input('internet_amount', array('label' => 'Internet Fee', 'type' => 'text')) ?>
							<?= $this->Form->input('months', array('type' => 'text')) ?>
							<?= $this->Form->input('confroomhrs', array('label' => 'Conference room hours', 'type' => 'text')) ?>
						
						</div>
					</div>
					<div class="span4">
						<div class="allowed">
							<?= $this->Form->input('start_date', array('class' => 'date', 'type' => 'text', 'div' => false, 'required' => 'required')) ?>
							<?= $this->Form->input('tax_number', array('label' => 'Tax ID')) ?>
							<?= $this->Form->input('license_fee', array('label' => 'Monthly Fee', 'type' => 'text')) ?>
							<?= $this->Form->input('security', array('label' => 'Security Deposit', 'type' => 'text')) ?>
							<?= $this->Form->input('credit', array('label' => 'Credit Applied', 'type' => 'text')) ?>
							<?= $this->Form->input('phoneservice', array('label' => 'Phone Service', 'options' => $phone_services, 'empty' => '')) ?>
							<?= $this->Form->input('personal_guarantee', array('type' => 'select', 'options' => array('0' => 'No', '1' => 'Yes'))); ?>
							<?= $this->Form->input('social_security_number', array('label' => 'Social Security Number', 'type' => 'text')) ?>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="allowed">
						<label>Address</label>
						<?= $this->Form->input('address1', array('label' => false, 'div' => false, 'placeholder' => 'Address Line 1')) ?>
						<?= $this->Form->input('address2', array('label' => false, 'div' => false, 'placeholder' => 'Address Line 2')) ?>
						<div class="controls-row">
							<?= $this->Form->input('city', array('class' => 'input-auto', 'placeholder' => 'City', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('state', array('class' => 'input-auto', 'placeholder' => 'State', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('zip', array('class' => 'input-mini', 'placeholder' => 'Zip Code', 'div' => false, 'label' => false)) ?>
						</div>
					</div>
				</div>
				<?= $this->Form->hidden('license_date', array('value' => date('Y-m-d'))) ?>
				<?= $this->Form->hidden('modifier_id', array('value' => AuthComponent::user('id'))) ?>
				<?= $this->Form->hidden('end_date', array('value' => null)) ?>
			</fieldset>
			<div class="allowed">
				<?= $this->Form->button(__('Create Agreement'), array('class' => 'btn btn-primary')) ?>
			</div>
		<?= $this->Form->end() ?>
	</div>
</div>
<style>
	.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
	.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
	.autocomplete-selected { background: #F0F0F0; }
	.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
</style>
<?= $this->Html->script("libs/jquery.autocomplete.min") ?>
<script type="text/javascript">

$(document).ready(function() {

	$(function() {
		$("#LicenseAgreementProspect").autocomplete({
			serviceUrl: "{{ operations_2g_urlprefix }}/prospects/search/prospectsAdd.json" +
									// too many fields for dynamic fetch, use fieldset instead
									"",
			onSelect: function(suggestion) {
				$("#LicenseAgreementBuildingId").val(suggestion.data.Prospect.desired_location);
				$("#LicenseAgreementCompany").val(suggestion.data.Prospect.company);
				$("#LicenseAgreementFirstName").val(suggestion.data.Prospect.first_name);
				$("#LicenseAgreementLastName").val(suggestion.data.Prospect.last_name);
				$("#LicenseAgreementTitle").val(suggestion.data.Prospect.title);
				$("#LicenseAgreementPhone").val(suggestion.data.Prospect.work_phone_number);
				$("#LicenseAgreementEmail").val(suggestion.data.Prospect.email_address);
				$("#LicenseAgreementAddress1").val(suggestion.data.Prospect.address_1);
				$("#LicenseAgreementAddress2").val(suggestion.data.Prospect.address_2);
				$("#LicenseAgreementCity").val(suggestion.data.Prospect.city);
				$("#LicenseAgreementState").val(suggestion.data.Prospect.state);
				$("#LicenseAgreementZip").val(suggestion.data.Prospect.zip_code);
			}
		});
	});

	// show rest of form on load
	var prospect_name = $('#LicenseAgreementProspect').val();
	if (!_.isEmpty(prospect_name)) {
		$('.allowed').show();
	}

	// show rest of form on selection
	$('#LicenseAgreementProspect').change(function() {
		if ($(this).val == "") {
			$('.content-body').append('<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Warning!</strong> You must select a prospect, if not create one using the link below.</div>');
		} else {
			$('.allowed').show();
		}
	});

});

	$("input.datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		autoclose: true,
		todayBtn: true,
		forceParse: true,
		minView: 0
	})
	.addClass("input-medium clickable")
	.wrap('<div class="input-append"></div>')
	.after('<span class="add-on"><i class="icon-calendar"></i></span>');

	$("input.date").datetimepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayBtn: true,
		forceParse: true,
		minView: 2
	})
	.addClass("input-small clickable")
	.wrap('<div class="input-append"></div>')
	.after('<span class="add-on"><i class="icon-calendar"></i></span>');

	
</script>
<style>
	.allowed { display:none; }
</style>

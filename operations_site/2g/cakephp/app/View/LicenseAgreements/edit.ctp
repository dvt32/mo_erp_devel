<?php $la = $licenseAgreement['LicenseAgreement'] ?>

<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements">License Agreements</a> <span class="divider">/</span></li>
	  <li class="active"><?= $this->request->data['LicenseAgreement']['company'] ?></li>
	</ul>
	<ul class="nav nav-tabs">
		<li><?= $this->Html->link('View', array('action' => 'view', $la['id'])) ?></li>
		<li class="active"><?= $this->Html->link('Edit', '#') ?></li>
		<li class="pull-right"><?= $this->Html->link('See All', array('action' => 'index')) ?></li>
	</ul>
</div>

<h2><?= __('License Agreement') ?></h2>
<?= $this->Form->create('LicenseAgreement', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
<div class="row-fluid">
	<div class="span6">
		<fieldset>
			<div class="row-fluid">
				<div class="span4">
					<?= $this->Form->input('id') ?>
					<?= $this->Form->input('building_id') ?>
					<?= $this->Form->input('spacenumber', array('label' => 'Office / Space Number')) ?>
					<?= $this->Form->input('signing_entity', array('options' => array_self_combine(array('company','person')))) ?>
					<?= $this->Form->input('company') ?>
					<?= $this->Form->input('first_name') ?>
					<?= $this->Form->input('last_name') ?>
					<?= $this->Form->input('title') ?>
					<?= $this->Form->input('phone') ?>
					<?= $this->Form->input('email') ?>
				</div>
				<div class="span4">
					<?= $this->Form->input('business_type') ?>
					<?= $this->Form->input('desks', array('label' => 'Number of desks needed', 'type' => 'text')) ?>
					<?= $this->Form->input('offices', array('label' => 'Number of offices needed')) ?>
					<?= $this->Form->input('phone_lines', array('label' => 'Number of phone lines needed')) ?>
					<?= $this->Form->input('persons', array('label' => 'Number of people with internet')) ?>
					<?= $this->Form->input('months') ?>
					<?= $this->Form->input('confroomhrs', array('label' => 'Conference room hours')) ?>
					<?= $this->Form->input('start_date', array('class' => 'date', 'type' => 'text', 'div' => false)) ?>
					<?= $this->Form->input('desk_id', array('empty' => '')) ?>
				</div>
				<div class="span4">
					<?= $this->Form->input('tax_number', array('label' => 'Tax ID')) ?>
					<?= $this->Form->input('license_fee', array('label' => 'Monthly Fee')) ?>
					<?= $this->Form->input('security', array('label' => 'Security Deposit')) ?>
					<?= $this->Form->input('phoneservice', array('label' => 'Phone Service', 'options' => $phone_services, 'empty' => '')) ?>

					<?= $this->Form->hidden('license_date', array('value' => date('Y-m-d'))) ?>
					<?= $this->Form->hidden('modifier_id', array('value' => AuthComponent::user('id'))) ?>
					<?= $this->Form->hidden('end_date', array('value' => null)) ?>
				</div>
			</div>
			<div class="row-fluid">
				<label>Address</label>
				<?= $this->Form->input('address1', array('label' => false, 'div' => false, 'placeholder' => 'Address Line 1')) ?>
				<?= $this->Form->input('address2', array('label' => false, 'div' => false, 'placeholder' => 'Address Line 2')) ?>
				<div class="controls-row">
					<?= $this->Form->input('city', array('class' => 'input-auto', 'placeholder' => 'City', 'div' => false, 'label' => false)) ?>
					<?= $this->Form->input('state', array('class' => 'input-auto', 'placeholder' => 'State', 'div' => false, 'label' => false)) ?>
					<?= $this->Form->input('zip', array('class' => 'input-mini', 'placeholder' => 'Zip Code', 'div' => false, 'label' => false)) ?>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="span6">
		<?php if (!empty($la['license_agreement_html'])): ?>
			<?= $this->Form->input('LicenseAgreement.license_agreement_html', array('label' => false, 'div' => false)) ?>
			<?= $this->Form->end() ?>
		<?php else: ?>
			<?= $this->Form->input('LicenseAgreement.license_agreement_html', array('label' => false, 'div' => false)) ?>
			<?= $this->Form->end() ?>
		<?php endif ?>
		<br />
		<?= $this->Form->button('Save', array('class' => 'btn btn-primary btn-block')) ?>
	</div>
	<?= $this->Form->end() ?>

</div>



<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>

<script>
	
	// license agreement editor
	$("#LicenseAgreementLicenseAgreementHtml").ckeditor();


	// datetime pickers
	$("input.datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		autoclose: true,
		todayBtn: true,
		forceParse: true,
		minView: 0
	})
	.addClass("input-medium clickable")
	.wrap('<div class="input-append"></div>')
	.after('<span class="add-on"><i class="icon-calendar"></i></span>');

	$("input.date").datetimepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayBtn: true,
		forceParse: true,
		minView: 2
	})
	.addClass("input-small clickable")
	.wrap('<div class="input-append"></div>')
	.after('<span class="add-on"><i class="icon-calendar"></i></span>');
</script>
<style>
#cke_LicenseAgreementLicenseAgreementHtml+* { min-height:100em; }
</style>

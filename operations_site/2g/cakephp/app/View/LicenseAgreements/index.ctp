<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li class="active">License Agreements<span class="divider">/</span></li>
	</ul>
	<div class="span12">
		<h2><?= __('License Agreements'); ?></h2>
		<?= $this->Html->link('Create New Agreement' , array('controller' => 'licenseAgreements', 'action' => 'add'), array('class' => 'btn btn-primary')); ?>
		<table class="table">
			<thead>
				<tr>
					<th><?= $this->Paginator->sort('company') ?></th>
					<th><?= $this->Paginator->sort('business_type') ?></th>
					<th><?= $this->Paginator->sort('creation_dt') ?></th>
					<th><?= $this->Paginator->sort('start_date') ?></th>
					<th><?= $this->Paginator->sort('end_date') ?></th>
					<th><?= $this->Paginator->sort('license_fee') ?></th>
					<th><?= $this->Paginator->sort('licensor_entity') ?></th>
					<th><?= $this->Paginator->sort('signing_entity') ?></th>
					<th><?= $this->Paginator->sort('account_id') ?></th>
					<th><?= $this->Paginator->sort('person_id') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($licenseAgreements as $licenseAgreement): ?>
				<?php $la = $licenseAgreement['LicenseAgreement'] ?>
				<tr>
					<td><?= $this->Html->link($la['company'], array('action' => 'view', $la['id'])) ?>&nbsp;</td>
					<td><?= @$la['business_type'] ?>&nbsp;</td>
					<td><?= @$la['creation_dt'] ?>&nbsp;</td>
					<td><?= @$la['start_date'] ?>&nbsp;</td>
					<td><?= @$la['end_date'] ?>&nbsp;</td>
					<td><?= @$la['license_fee'] ?>&nbsp;</td>
					<td><?= @$la['licensor_entity'] ?>&nbsp;</td>
					<td><?= @$la['signing_entity'] ?>&nbsp;</td>
					<td><?= @$la['account_id'] ?>&nbsp;</td>
					<td><?= @$la['person_id'] ?>&nbsp;</td>
				</tr>
			<?php endforeach ?>
			</tbody>	
		</table>
		<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?>	</p>
		<div class="paging">
			<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
			<?= $this->Paginator->numbers(array('separator' => '')) ?>
			<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
		</div>
	</div>
</div>

<div class="licenseAgreements form">
<?php echo $this->Form->create('LicenseAgreement', array('type' => 'file')); ?>
	
	<legend><?php echo __('Send Out Agreement to be Signed'); ?></legend>
	<p>Uploading the files here to be sent out to get signed via Docusign</p>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->hidden('modifier_id', array('value' => AuthComponent::User('id')));
	?>
	<div class="span4">
		<input type="file" name="data[LicenseAgreement][file][0]" style="line-height:0px;" />
	</div>
	
</fieldset>
<?php echo $this->Form->button(__('Send Out'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
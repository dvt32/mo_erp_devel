<?php $agreement = $licenseAgreement['LicenseAgreement'] ?>

<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements">License Agreements</a> <span class="divider">/</span></li>
	  <li class="active"><?= h(@$agreement['company']) ?></li>
	</ul>
	<ul class="nav nav-tabs">
		<li class="active"><?= $this->Html->link('View', '#') ?></li>
		<li><?= $this->Html->link('Edit', array('action' => 'edit', $agreement['id'])) ?></li>
		<li class="pull-right"><?= $this->Html->link('See All', array('action' => 'index')) ?></li>
	</ul>
</div>

<div class="row-fluid">
	<div class="span6">
		<!-- basic license agreement information -->
		<section class="module expandable">
			<div class="module-head"> <b><?= __('License Agreement') ?></b> </div>
			<div class="module-body" style="display:block;">
				<dl>
					<dl>
					<dt><?= __('Company') ?></dt>
					<dd><?= h(@$agreement['company']) ?>&nbsp;</dd>
					<dt><?= __('License Date') ?></dt>
					<dd><?= h(@$agreement['license_date']) ?>&nbsp;</dd>
					<dt><?= __('Licensor Entity') ?></dt>
					<dd><?= h(@$agreement['licensor_entity']) ?>&nbsp;</dd>
					<dt><?= __('Signing Entity') ?></dt>
					<dd><?= h(@$agreement['signing_entity']) ?>&nbsp;</dd>
					<dt><?= __('First Name') ?></dt>
					<dd><?= h(@$agreement['first_name']) ?>&nbsp;</dd>
					<dt><?= __('Last Name') ?></dt>
					<dd><?= h(@$agreement['last_name']) ?>&nbsp;</dd>
					<dt><?= __('Title') ?></dt>
					<dd><?= h($agreement['title']) ?>&nbsp;</dd>
					<dt><?= __('Address') ?></dt>
					<dd>
						<div><?= @$agreement['address1'] ?></div>
						<div><?= @$agreement['address2'] ?></div>
						<div><?= @$agreement['city'] ?>, <?= @$agreement['state'] ?> <?= @$agreement['zip'] ?></div>
					</dd>
					<dt><?= __('Phone') ?></dt>
					<dd><?= h(@$agreement['phone']) ?>&nbsp;</dd>
					<dt><?= __('Email') ?></dt>
					<dd><?= h(@$agreement['email']) ?>&nbsp;</dd>
					<dt><?= __('Business Type') ?></dt>
					<dd><?= h(@$agreement['business_type']) ?>&nbsp;</dd>
					<dt><?= __('Desks') ?></dt> 
					<dd><?= h(@$agreement['desks']) ?>&nbsp; desks</dd>
					<dt><?= __('Desk Price') ?></dt> 
					<dd>$<?= h(@$agreement['desk_amount']) ?>&nbsp;each</dd>
					<dt><?= __('Offices') ?></dt>
					<dd><?= h(@$agreement['offices']) ?>&nbsp; offices</dd>
					<dt><?= __('Office Price') ?></dt> 
					<dd>$<?= h(@$agreement['office_amount']) ?>&nbsp;each</dd>
					<dt><?= __('Phone Lines') ?></dt>
					<dd><?= h(@$agreement['phone_lines']) ?>&nbsp; phones</dd>
					<dt><?= __('Phone Fee') ?></dt> 
					<dd>$<?= h(@$agreement['phone_amount']) ?>&nbsp;each</dd>
					<dt><?= __('Persons') ?></dt>
					<dd><?= h(@$agreement['persons']) ?>&nbsp; people</dd>
					<dt><?= __('Internet Fee') ?></dt> 
					<dd>$<?= h(@$agreement['internet_amount']) ?>&nbsp;each</dd>
					<dt><?= __('Months') ?></dt>
					<dd><?= h(@$agreement['months']) ?>&nbsp; months</dd>
					<dt><?= __('Start Date') ?></dt>
					<dd><?= h(@$agreement['start_date']) ?>&nbsp;</dd>
					<dt><?= __('End Date') ?></dt>
					<dd><?= h(@$agreement['end_date']) ?>&nbsp;</dd>
					<dt><?= __('License Fee') ?></dt>
					<dd>$<?= h(@$agreement['license_fee']) ?>&nbsp;</dd>
					<dt><?= __('Security') ?></dt>
					<dd>$<?= h(@$agreement['security']) ?>&nbsp;</dd>
					<dt><?= __('Credit') ?></dt>
					<dd>$<?= h(@$agreement['credit']) ?>&nbsp;</dd>
					<dt><?= __('Creation Dt') ?></dt>
					<dd><?= h(@$agreement['creation_dt']) ?>&nbsp;</dd>
					<dt><?= __('Spacenumber') ?></dt>
					<dd>#<?= h(@$agreement['spacenumber']) ?>&nbsp;</dd>
					<dt><?= __('Phoneservice') ?></dt>
					<dd><?= h(@$agreement['phoneservice']) ?>&nbsp;</dd>
					<dt><?= __('Conference Room Hours') ?></dt>
					<dd><?= h(@$agreement['confroomhrs']) ?>&nbsp; minutes</dd>
					<dt><?= __('Tax Number') ?></dt>
					<dd><?= h(@$agreement['tax_number']) ?>&nbsp;</dd>
					<dt><?= __('Personal Guarantee') ?></dt> 
					<dd><?= h(@$agreement['personal_guarantee']) ?>&nbsp;</dd>
					<!-- <dt><//?= __('Desk') ?></dt> -->
					<!-- <dd>
						<//?= $this->Html->link(@"Desk #{$licenseAgreement['Desk']['assigned_number']}", array('controller' => 'desks', 'action' => 'view', @$licenseAgreement['Desk']['id'])) ?>
						(<//?= $licenseAgreement['Desk']['Floor']['short_global_label'] ?>)
						&nbsp;
					</dd> -->
					<dt><?= __('Customer Account') ?></dt>
					<dd><?= $this->Html->link(@$licenseAgreement['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', @$licenseAgreement['CustomerAccount']['id'])) ?>&nbsp;</dd>
					<dt><?= __('Customer') ?></dt>
					<dd><?= $this->Html->link(@$licenseAgreement['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', @$licenseAgreement['Customer']['id'])) ?>&nbsp;</dd>
					</dl>
				</dl>
			</div>
		</section>
	</div>
	<div class="span6">
		<!-- edit license agreement -->
		<section class="module expandable">
			<div id="agreement_docs" class="module-head clickable"><b><?= __('Agreement Documents') ?></b></div>
			<div class="module-body">

			<?php if(empty($agreement['ds_envelopeID'])) : ?>
				<?= $this->Html->link('Download Pending Agreement', array('controller' => 'licenseAgreements', 'action' => 'printOut', $agreement['id']), array('class' => 'btn btn-info btn-block', 'target' => '_blank'))  ?>
				<?= $this->Html->link('Upload and Send Out For DocuSign Signatures', array('controller' => 'licenseAgreements', 'action' => 'upload', $agreement['id']), array('class' => 'btn btn-primary btn-block'))  ?>
			<?php else : ?>
				<a href="#status" id="get_docusign_envelope" class="btn btn-info btn-block docuSignInitialize">
					Docusign Envelope Status
				</a>
				<div class="get_docusign_envelope"></div>
				<a href="#documents" id="get_docusign_envelope_assets_documents" class="btn btn-info btn-block docuSignInitialize">
					Docusign Documents
				</a>
				<div class="get_docusign_envelope_assets_documents"></div>
				<a href="#status" id="get_docusign_envelope_recipients" class="btn btn-info btn-block docuSignInitialize">
					Docusign Recipients
				</a>
				<div class="get_docusign_envelope_recipients"></div>
				<div class="envelope_buttons">
				<?= $this->Html->link('Make Corrections in DocuSign', array('controller' => 'licenseAgreements', 'action' => 'correction', $agreement['id']), array('class' => 'btn btn-danger btn-block', 'id' => 'correction_btn')) ?>
				<?= $this->Html->link('License Agreement Signed - Convert', array('controller' => 'licenseAgreements', 'action' => 'convert', $agreement['id']), array('class' => 'btn btn-primary btn-block', 'id' => 'convert_btn', 'style' => 'display:none;'))  ?>
				</div>
			<?php endif ?>
			</div>
		</section>

		<!-- license amendment index -->
		<section class="module expandable">
			<div class="module-head clickable"><b><?= __('License Amendments') ?></b></div>
			<div class="module-body">
			<?php if (!empty($licenseAgreement['Amendment'])): ?>
				<table class="table">
					<thead>
						<tr>
							<th>Type</th>
							<th>Created</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?php //debug($licenseAgreement['Amendment']) ?>
					<?php foreach ($licenseAgreement['Amendment'] as $amendment): ?>
						<tr>
							<td>
								<?= $this->Html->link($amendment['type'], array('controller' => 'licenseAmendments', 'action' => 'view', $amendment['id'])) ?>
							</td>
							<td>
								<?= $amendment['created'] ?>
							</td>
							<td>
								<?php if(empty($amendment['ds_envelope_id'])) : ?>
									<?= $this->Html->link($this->Html->image('icons/flat_miniset/addons/256/clipboard_pencil.png' , array('style' => 'height:1.5em;')), array('controller' => 'licenseAmendments', 'action' => 'upload', $amendment['id']), array('escape' => false)) ?>
								<?php else : ?>
									<?= $this->Html->link($this->Html->image('icons/flat_miniset/addons/256/clipboard_push_pin.png' , array('style' => 'height:1.5em;')), array('controller' => 'licenseAmendments', 'action' => 'correction', $amendment['id']), array('escape' => false)) ?>
								<?php endif ?>
								<?= $this->Html->link($this->Html->image('icons/flat_miniset/png/256/download.png' , array('style' => 'height:1.5em;')), array('controller' => 'licenseAmendments', 'action' => 'printOut', $amendment['id']), array('escape' => false)) ?>
								<?= $this->Form->postLink($this->Html->image('icons/flat_miniset/png/256/cross.png', array('style' => 'height:2em;')), array('controller' => 'licenseAmendments', 'action' => 'delete', $amendment['id']), array('escape' => false));?>

							</td>
						</tr>
					<?php endforeach ?>
					</tbody>
				</table>
			<?php else: ?>
				<span class="muted">no amendments</span>
			<?php endif ?>
			</div>
		</section>

		<!-- create new amendments -->
		<section class="module expandable">
			<div class="module-head clickable"><b><?= __("Add New Amendment") ?></b></div>
			<div class="module-body" style="display:none;">
				<div class="row-fluid">
					<?= $this->Form->create('LicenseAmendment', array('url' => array('controller' => 'LicenseAmendments', 'action' => 'add'))) ?>
						<?= $this->Form->hidden('LicenseAmendment.license_agreement_id', array('value' =>  $agreement['id'])) ?>
						<?= $this->Form->input('LicenseAmendment.type', array('type' => 'text')) ?>
						<?= $this->Form->input('LicenseAmendment.amendment', array('type' => 'textarea')) ?>
						<br />
						<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</section>
	</div>
</div>


<?= $this->Html->script("libs/jquery.autocomplete.min") ?>
<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>
<style>
	.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
	.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
	.autocomplete-selected { background: #F0F0F0; }
	.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
</style>
<script type="text/javascript">
	$(function() {
		$("#LicenseAmendmentType").autocomplete({
			serviceUrl:"{{ operations_2g_urlprefix }}/licenseAmendmentTemplates/search.json",
			onSelect: function (suggestion) {
				$("#LicenseAmendmentAmendment").val(suggestion.data.LicenseAmendmentTemplate.amendment);
			}
		})
	});
	$("#LicenseAmendmentAmendment").ckeditor();
</script>


<script type="text/javascript">

var ds_envelopeID = "<?= $agreement['ds_envelopeID']; ?>";

if (ds_envelopeID != "") {

	$(document).ready(function() {
		$('#get_docusign_envelope').click();
		$('#get_docusign_envelope_assets_documents').click();
		$('#get_docusign_envelope_recipients').click();
	});

}



// auto-expand
$(".expandable").each(function(i) {
	this.setAttribute("id","expandable-"+i);
	var section="#expandable-"+i;
	$(section+" div.module-head").addClass("clickable").click(function(){
		$(section+" div.module-body").toggle();
	});
});

$('#get_docusign_envelope').click(function() {
	$.getJSON('{{ operations_2g_urlprefix }}/licenseAgreements/ajaxLicenseAgreement/get_docusign_envelope/'+ds_envelopeID, function(data) {

		$('.get_docusign_envelope').append('<dl id="env_info"></dl>');
		$('#env_info').append('<h3>Envelope Information</h3>');
		$('#env_info').append('<dt>Status</dt><dd>'+data.status+'</dd>');
		$('#env_info').append('<dt>Created</dt><dd>'+data.createdDateTime+'</dd>');
		$('#env_info').append('<dt>Last Checked</dt><dd>'+data.statusChangedDateTime+'</dd>');

		if (data.status == 'completed') {
			$('#convert_btn').show();
			$('#correction_btn').hide();
			$('.envelope_buttons').append('<a href="{{ operations_2g_urlprefix }}/licenseAgreements/docusign_final_download/'+ds_envelopeID+'" class="btn btn-info btn-block">Download Final Agreement</a>');
		}
	});
	$(this).hide();

});

$('#get_docusign_envelope_assets_documents').click(function() {
	var makeCall = $(this).attr('id');
	console.log(makeCall);
	$.getJSON('{{ operations_2g_urlprefix }}/licenseAgreements/ajaxLicenseAgreement/get_docusign_envelope_assets/'+ds_envelopeID+"/documents", function(data) {

		$('.get_docusign_envelope_assets_documents').append('<h3>Envelope Documents</h3>');
		$('.get_docusign_envelope_assets_documents').append('<table id="env_docs" class="table table-striped"><thead><tr><th>ID</th><th>Name</th><th>Pages</th><th></th></thead><tbody class="env_docs"></tbody></table>');
		_.each(data.envelopeDocuments,function(v, k) {
			$('tbody.env_docs').append('<tr id=doc_'+v.documentId+'></tr>');
			$('tr#doc_'+v.documentId).append('<td>'+v.documentId+'</td>');
			$('tr#doc_'+v.ds_envelopeID).append('<td>'+v.name+'</td>');
			$('tr#doc_'+v.documentId).append('<td>'+v.pages+'</td>');
			$('tr#doc_'+v.documentId).append('<td><a href="{{ operations_2g_urlprefix }}/licenseAgreements/apidownload/'+ds_envelopeID+'/'+v.documentId+'">Download</a></td>');
		});
	});
	$(this).hide();

});

$('#get_docusign_envelope_recipients').click(function() {
	var makeCall = $(this).attr('id');
	console.log(makeCall);
	$.getJSON('{{ operations_2g_urlprefix }}/licenseAgreements/ajaxLicenseAgreement/get_docusign_envelope_recipients/'+ds_envelopeID, function(data) {

		$('.get_docusign_envelope_assets_documents').append('<h3>Envelope Documents</h3>');
		$('.get_docusign_envelope_assets_documents').append('<table id="env_recp" class="table table-striped"> <thead> <tr> <th>ID</th> <th>Name</th> <th>Email</th> </tr> </thead> <tbody class="env_recp"></tbody> </table>');

		_.each(data.signers,function(v, k) {
			$('tbody.env_recp').append('<tr id=recp_'+v.recipientId+'></tr>');
			$('tr#recp_'+v.recipientId).append('<td>'+v.recipientId+'</td>');
			$('tr#recp_'+v.recipientId).append('<td>'+v.name+'</td>');
			$('tr#recp_'+v.recipientId).append('<td>'+v.email+'</td>');
		});
	});
	$(this).hide();

});

</script>

<ul class="breadcrumb">
  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
  <li><a href="{{ operations_2g_urlprefix }}/amendments">Amendments</a> <span class="divider">/</span></li>
  <li><a href="{{ operations_2g_urlprefix }}/licenseAmendmentTemplates">Amendment Templates</a> <span class="divider">/</span></li>
  <li class="active">Create New Amendment Template</li>
</ul>

<div class="row-fluid">
	<div class="span10 offset1">
		<?= $this->Form->create('LicenseAmendmentTemplate', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>
				<legend>New License Amendment Template</legend>
				<?= $this->Form->input('name', array('type' => 'text')) ?>
				<?= $this->Form->input('amendment', array('label' => false)) ?>
				<?= $this->Form->hidden('is_active', array('value' => true)) ?>
				<?= $this->Form->hidden('modifier_id', array('value' => AuthComponent::User('id'))) ?>
				<?= $this->Form->button(__('Save'), array('class' => 'btn btn-primary')); ?>
			</fieldset>
		<?= $this->Form->end() ?>
	</div>
</div>

<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>
<script>
	// license amendment editor
	$("#LicenseAmendmentTemplateAmendment").ckeditor();
</script>

<div class="row-fluid">
	<ul class="nav nav-tabs">
		<li class="active"><?= $this->Html->link('Edit', '#') ?></li>
		<li class="pull-right"><?= $this->Html->link('See All', array('action' => 'index')) ?></li>
	</ul>
</div>

<div class="row-fluid">
	<div class="span10 offset1">
		<?= $this->Form->create('LicenseAmendmentTemplate', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<fieldset>
				<legend>Edit License Amendment Template</legend>
				<?= $this->Form->input('id') ?>
				<?= $this->Form->input('name', array('type' => 'text')) ?>
				<?= $this->Form->input('amendment', array('label' => false)) ?>
				<?= $this->Form->hidden('is_active', array('value' => true)) ?>
				<?= $this->Form->hidden('modifier_id', array('value' => AuthComponent::User('id'))) ?>
				<?= $this->Form->button(__('Save'), array('class' => 'btn btn-primary')); ?>
			</fieldset>
		<?= $this->Form->end() ?>
	</div>
</div>

<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>
<script>
	// license amendment editor
	$("#LicenseAmendmentTemplateAmendment").ckeditor();
</script>
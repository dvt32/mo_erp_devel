<ul class="breadcrumb">
  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
  <li><a href="{{ operations_2g_urlprefix }}/licenseAmendments">Amendments</a> <span class="divider">/</span></li>
  <li class="active">Amendment Templates<span class="divider">/</span></li>
</ul>
<?= $this->Html->link(__('Create New Amendment Template'), array('action' => 'add'), array('class' => 'btn btn-primary')) ?>
<div class="licenseAmendmentTemplates index">
	<h2><?= __('License Amendment Templates') ?></h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('name') ?></th>
			<th><?= $this->Paginator->sort('created') ?></th>
			<th><?= $this->Paginator->sort('modifier_id', 'Creator') ?></th>
	</tr>
	<?php foreach ($licenseAmendmentTemplates as $licenseAmendmentTemplate): ?>
	<tr>
		<td>
			<?= $this->Html->link($licenseAmendmentTemplate['LicenseAmendmentTemplate']['name'], array('action' => 'edit', $licenseAmendmentTemplate['LicenseAmendmentTemplate']['id'])) ?>
		</td>
		<td><?= h($licenseAmendmentTemplate['LicenseAmendmentTemplate']['modified']) ?>&nbsp;</td>
		<td>
			<?= $this->Html->link($licenseAmendmentTemplate['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $licenseAmendmentTemplate['Employee']['id'])) ?>
		</td>
	</tr>
<?php endforeach ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

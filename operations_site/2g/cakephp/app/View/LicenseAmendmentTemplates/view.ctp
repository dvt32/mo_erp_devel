<ul class="breadcrumb">
  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
  <li><a href="{{ operations_2g_urlprefix }}/licenseAmendments">Amendments</a> <span class="divider">/</span></li>
  <li><a href="{{ operations_2g_urlprefix }}/licenseAmendmentTemplates">Amendment Templates</a> <span class="divider">/</span></li>
  <li class="active"><?= $licenseAmendmentTemplate['LicenseAmendmentTemplate']['name']?></li>
</ul>
<div class="licenseAmendmentTemplates view">
<h2><?php echo __('License Amendment Template'); ?></h2>
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($licenseAmendmentTemplate['LicenseAmendmentTemplate']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amendment'); ?></dt>
		<dd>
			<?php echo h($licenseAmendmentTemplate['LicenseAmendmentTemplate']['amendment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			
			<?php echo h($licenseAmendmentTemplate['LicenseAmendmentTemplate']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($licenseAmendmentTemplate['LicenseAmendmentTemplate']['modified']); ?>
			&nbsp;
		</dd>
<!-- 		<dt><?php echo __('Employee'); ?></dt>
		<dd>
			<?php echo h($licenseAmendmentTemplate['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $licenseAmendmentTemplate['Employee']['id'])); ?>
			&nbsp;
		</dd> -->
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<li class="active">
			<?php echo $this->Form->postLink(__('Delete License Amendment Template'), array('action' => 'delete', $licenseAmendmentTemplate['LicenseAmendmentTemplate']['id']), null, __('Are you sure you want to delete # %s?', $licenseAmendmentTemplate['LicenseAmendmentTemplate']['name'])); ?> 
		</li>
	</ul>
</div>

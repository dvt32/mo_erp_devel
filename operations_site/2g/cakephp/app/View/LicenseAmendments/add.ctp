<div class="row-fluid">
<?= $this->Form->create('LicenseAmendment', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
	<fieldset>
		<legend><?= __('Add License Amendment') ?></legend>
		<?= $this->Form->input('template') ?>
		<?= $this->Form->input('amendment') ?>
	</fieldset>
	<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
<?= $this->Form->end() ?>
</div>

<?= $this->Html->script("libs/jquery.autocomplete.min") ?>
<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>
<style>
	.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
	.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
	.autocomplete-selected { background: #F0F0F0; }
	.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
</style>
<script>
	$(function() {
		$("#LicenseAmendmentTemplate").autocomplete({
			serviceUrl:"{{ operations_2g_urlprefix }}/licenseAmendmentTemplates/search.json",
			onSelect: function (suggestion) {
				$("#LicenseAmendmentAmendment").val(suggestion.data.LicenseAmendmentTemplate.amendment);
			}
		})
	});
	$("#LicenseAmendmentAmendment").ckeditor();
</script>

<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAmendments">License Amendments</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements/view/<?= $this->request->data['LicenseAmendment']['license_agreement_id'] ?>">License Agreement</a> <span class="divider">/</span></li>
	  <li class="active">License Amendment Edit<span class="divider">/</span></li>
	</ul>
</div>
<div class="licenseAmendments form">
<?= $this->Form->create('LicenseAmendment', array('url' => array('controller' => 'LicenseAmendments', 'action' => 'edit'))) ?>
	<?= $this->Form->hidden('id') ?>
	<?= $this->Form->input('type', array('type' => 'text')) ?>
	<?= $this->Form->input('amendment', array('type' => 'textarea')) ?>
	<br />
	<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
<?= $this->Form->end() ?>
</div>
<div class="actions">
	<ul class="nav">
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('LicenseAmendment.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('LicenseAmendment.id'))); ?></li>
	</ul>
</div>


<?= $this->Html->script("libs/jquery.autocomplete.min") ?>
<?= $this->Html->script('libs/ckeditor/ckeditor') ?>
<?= $this->Html->script('libs/ckeditor/adapters/jquery') ?>
<style>
	.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
	.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
	.autocomplete-selected { background: #F0F0F0; }
	.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
</style>
<script type="text/javascript">
	$(function() {
		$("#LicenseAmendmentType").autocomplete({
			serviceUrl:"{{ operations_2g_urlprefix }}/licenseAmendmentTemplates/search.json",
			onSelect: function (suggestion) {
				$("#LicenseAmendmentAmendment").val(suggestion.data.LicenseAmendmentTemplate.amendment);
			}
		})
	});
	$("#LicenseAmendmentAmendment").ckeditor();
</script>

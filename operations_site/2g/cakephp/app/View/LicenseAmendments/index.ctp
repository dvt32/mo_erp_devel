<div class="row-fluid">
	<h2><?= __('License Amendments') ?></h2>
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('type', 'Name') ?></th>
				<th><?= $this->Paginator->sort('license_agreement_id') ?></th>
				<th><?= $this->Paginator->sort('modifier_id') ?></th>
				<th><?= $this->Paginator->sort('modified') ?></th>
				<th><?= $this->Paginator->sort('created') ?></th>
				<th><?= $this->Paginator->sort('ds_envelope_id') ?></th>
				<th class="actions">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($licenseAmendments as $licenseAmendment): ?>
			<tr>
				<td><?= $this->Html->link($licenseAmendment['LicenseAmendment']['type'],array('controller' => 'licenseAmendments', 'action' => 'edit', $licenseAmendment['LicenseAmendment']['id'])) ?>&nbsp;</td>

				<td><?= $this->Html->link($licenseAmendment['LicenseAmendment']['license_agreement_id'],array('controller' => 'licenseAgreements', 'action' => 'view', $licenseAmendment['LicenseAmendment']['license_agreement_id'])) ?>&nbsp;</td>
				<td><?= h($licenseAmendment['LicenseAmendment']['modifier_id']) ?>&nbsp;</td>
				<td><?= h($licenseAmendment['LicenseAmendment']['modified']) ?>&nbsp;</td>
				<td><?= h($licenseAmendment['LicenseAmendment']['created']) ?>&nbsp;</td>
				<td><?= h($licenseAmendment['LicenseAmendment']['ds_envelope_id']) ?>&nbsp;</td>
				<td class="actions">
					<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $licenseAmendment['LicenseAmendment']['id']), null, __('Are you sure you want to delete # %s?', $licenseAmendment['LicenseAmendment']['id'])) ?>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	<p> <?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )) ?>	</p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>

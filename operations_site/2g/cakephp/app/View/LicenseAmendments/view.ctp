<?php $amendment = $licenseAmendment['LicenseAmendment'] ?>
<?php $agreement = $licenseAmendment['Agreement'] ?>

<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements">License Agreements</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/licenseAgreements/view/<?= h(@$agreement['id']) ?>"><?= h(@$agreement['company']) ?></a> <span class="divider">/</span></li>
	  <li class="active"><?= @$amendment['id'] ?></li>
	</ul>
</div>
<h2><?= __('License Amendment') ?></h2>
<div class="row-fluid">
	<div class="span6">
		<dl>
			<dt><?= __('Id') ?></dt>
			<dd><?= h(@$amendment['id']) ?>&nbsp;</dd>
			<dt><?= __('License Agreement') ?></dt>
			<dd><?= $this->Html->link("{$agreement['company']} ({$agreement['license_date']})", array('controller' => 'licenseAgreements', 'action' => 'view', $agreement['id'])) ?>&nbsp;</dd>
			<dt><?= __('Modifier Id') ?></dt>
			<dd><?= h(@$amendment['modifier_id']) ?>&nbsp;</dd>
			<dt><?= __('Modified') ?></dt>
			<dd><?= h(@$amendment['modified']) ?>&nbsp;</dd>
			<dt><?= __('Created') ?></dt>
			<dd><?= h(@$amendment['created']) ?>&nbsp;</dd>
			<dt><?= __('Ds Envelope Id') ?></dt>
			<dd><?= h(@$amendment['ds_envelope_id']) ?>&nbsp;</dd>
		</dl>
	</div>
	<div class="span6">
		<?php if(empty($amendment['ds_envelope_id'])) : ?>
			<?= $this->Html->link('Download Pending Agreement', array('controller' => 'licenseAmendments', 'action' => 'printOut', $amendment['id']), array('class' => 'btn btn-info btn-block', 'target' => '_blank'))  ?>
			<?= $this->Html->link('Upload and Send Out For DocuSign Signatures', array('controller' => 'licenseAmendments', 'action' => 'upload', $amendment['id']), array('class' => 'btn btn-primary btn-block'))  ?>
		<?php else : ?>
			<a href="#status" id="get_docusign_envelope" class="btn btn-info btn-block docuSignInitialize">
				Docusign Envelope Status
			</a>
			<div class="get_docusign_envelope"></div>
			<a href="#documents" id="get_docusign_envelope_assets_documents" class="btn btn-info btn-block docuSignInitialize">
				Docusign Documents
			</a>
			<div class="get_docusign_envelope_assets_documents"></div>
			<a href="#status" id="get_docusign_envelope_recipients" class="btn btn-info btn-block docuSignInitialize">
				Docusign Recipients
			</a>
			<div class="get_docusign_envelope_recipients"></div>
			<?= $this->Html->link('Make Corrections in DocuSign', array('controller' => 'licenseAmendments', 'action' => 'correction', $amendment['id']), array('class' => 'btn btn-danger btn-block', 'id' => 'correction_btn'))  ?>
			<?= $this->Html->link('License Agreement Signed - Convert', array('controller' => 'licenseAmendments', 'action' => 'convert', $amendment['id']), array('class' => 'btn btn-primary btn-block', 'id' => 'convert_btn', 'style' => 'display:none;'))  ?>
		<?php endif ?>
	</div>
</div>
<script>


var ds_envelopeID = "<?= $amendment['ds_envelope_id']; ?>";

if (ds_envelopeID != "") {

	$(document).ready(function() {
		$('#get_docusign_envelope').click();
		$('#get_docusign_envelope_assets_documents').click();
		$('#get_docusign_envelope_recipients').click();
	});

}

$('#get_docusign_envelope').click(function() {
	$.getJSON('{{ operations_2g_urlprefix }}/licenseAmendments/ajaxLicenseAmendment/get_docusign_envelope/'+ds_envelopeID, function(data) {

		$('.get_docusign_envelope').append('<dl id="env_info"></dl>');
		$('#env_info').append('<h3>Envelope Information</h3>');
		$('#env_info').append('<dt>Status</dt><dd>'+data.status+'</dd>');
		$('#env_info').append('<dt>Created</dt><dd>'+data.createdDateTime+'</dd>');
		$('#env_info').append('<dt>Last Checked</dt><dd>'+data.statusChangedDateTime+'</dd>');

		if (data.status == 'completed') {
			$('#convert_btn').show();
			$('#correction_btn').hide();
		}
	});
	$(this).hide();

});

$('#get_docusign_envelope_assets_documents').click(function() {
	var makeCall = $(this).attr('id');
	console.log(makeCall);
	$.getJSON('{{ operations_2g_urlprefix }}/licenseAmendments/ajaxLicenseAmendment/get_docusign_envelope_assets/'+ds_envelopeID+"/documents", function(data) {

		$('.get_docusign_envelope_assets_documents').append('<h3>Envelope Documents</h3>');
		$('.get_docusign_envelope_assets_documents').append('<table id="env_docs" class="table table-striped"><thead><tr><th>ID</th><th>Name</th><th>Pages</th><th></th></thead><tbody class="env_docs"></tbody></table>');
		_.each(data.envelopeDocuments,function(v, k) {
			$('tbody.env_docs').append('<tr id=doc_'+v.documentId+'></tr>');
			$('tr#doc_'+v.documentId).append('<td>'+v.documentId+'</td>');
			$('tr#doc_'+v.ds_envelopeID).append('<td>'+v.name+'</td>');
			$('tr#doc_'+v.documentId).append('<td>'+v.pages+'</td>');
			$('tr#doc_'+v.documentId).append('<td><a href="{{ operations_2g_urlprefix }}/licenseAmendments/apidownload/'+ds_envelopeID+'/'+v.documentId+'">Download</a></td>');
		});
	});
	$(this).hide();

});

$('#get_docusign_envelope_recipients').click(function() {
	var makeCall = $(this).attr('id');
	console.log(makeCall);
	$.getJSON('{{ operations_2g_urlprefix }}/licenseAmendments/ajaxLicenseAmendment/get_docusign_envelope_recipients/'+ds_envelopeID, function(data) {

		$('.get_docusign_envelope_assets_documents').append('<h3>Envelope Documents</h3>');
		$('.get_docusign_envelope_assets_documents').append('<table id="env_recp" class="table table-striped"> <thead> <tr> <th>ID</th> <th>Name</th> <th>Email</th> </tr> </thead> <tbody class="env_recp"></tbody> </table>');

		_.each(data.signers,function(v, k) {
			$('tbody.env_recp').append('<tr id=recp_'+v.recipientId+'></tr>');
			$('tr#recp_'+v.recipientId).append('<td>'+v.recipientId+'</td>');
			$('tr#recp_'+v.recipientId).append('<td>'+v.name+'</td>');
			$('tr#recp_'+v.recipientId).append('<td>'+v.email+'</td>');
		});
	});
	$(this).hide();

});

</script>


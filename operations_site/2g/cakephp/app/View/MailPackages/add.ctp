<div class="row-fluid">
	<div class="span8 offset2">
		<section class="module">
			<div class="module-head">
				<b>New Mail Package Notification</b>
			</div>
			<div class="module-body">
				<?= $this->Form->create('MailPackage', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
					<legend><?= __('Add Mail Package') ?></legend>
					<?= $this->Form->input('MailPackage.date') ?>
					<div class="row-fluid">
						<?= $this->Form->input('MailPackage.Customer', array('div' => 'span10', 'label' => 'Company Name')) ?>
						<?= $this->Form->input('MailPackage.customer_id', array('type' => 'text', 'div' => 'span2', 'label' => 'Company Id', 'title' => 'This field is autofilled when a customer name is selected')) ?>
					</div>
					<?= $this->Form->input('MailPackage.recipient_email', array('type' => 'text', 'title' => 'This field is autofilled when a customer name is selected', 'placeholder' => 'Separate multiple emails by commas')) ?>
					<div class="row-fluid">
						<div class="span4">
							<?= $this->Form->input('MailPackage.carrier') ?>
						</div>
						<div class="span4">
							<?= $this->Form->input('MailPackage.package_type') ?>
						</div>
						<div class="span4">
							<?= $this->Form->input('MailPackage.sender', array(
								'options' => $employees,
								'empty' => '-- Signed For By --',
								'label' => 'Signed For By',
								'value' => @$my_name
							)) ?>
						</div>
					</div>
					<?= $this->Form->button(h('Save & Send'), array('class' => 'btn btn-primary')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
</div>

<style>
	.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
	.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
	.autocomplete-selected { background: #F0F0F0; }
	.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
	
</style>
<?= $this->Html->script("libs/jquery.autocomplete.min") ?>
<script>
	$(function() {
		$("#MailPackageCustomer").autocomplete({
			serviceUrl:"{{ operations_2g_urlprefix }}/customers/search.json?fields[]=id&fields[]=name&fields[]=email",
			onSelect: function(suggestion) {
				$("#MailPackageCustomerId").val(suggestion.data.CustomerAccount.id);
				$("#MailPackageRecipientEmail").val(suggestion.data.Customer.email);
			}
		});
	});
</script>

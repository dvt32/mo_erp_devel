<div class="mailPackages form">
<?php echo $this->Form->create('MailPackage'); ?>
	<fieldset>
		<legend><?php echo __('Edit Mail Package'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('date');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('recipient_email');
		echo $this->Form->input('email_message');
		echo $this->Form->input('carrier');
		echo $this->Form->input('package_type');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('sender');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('MailPackage.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('MailPackage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Mail Packages'), array('action' => 'index')); ?></li>
	</ul>
</div>

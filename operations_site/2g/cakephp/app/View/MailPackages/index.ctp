<h2><?= __('Mail Packages') ?></h2>
<div class="row-fluid">
	<div class="span3">
		<legend><?= __('Shortcuts') ?></legend>
		<?php $myId = AuthComponent::user('id') ?>
		<ul class="nav nav-pills nav-stacked">
			<li class="active"><?= $this->Html->link('Add Package Notification', array('action' => 'add')) ?></li>
			<li><?= $this->Html->link('Packages Today', array('action' => 'index', '?' => array('date' => date('Y-m-d')))) ?></li>
			<li><?= $this->Html->link('Packages Yesterday', array('action' => 'index', '?' => array('date' => date('Y-m-d',strtotime('yesterday'))))) ?></li>
		</ul>
	</div>
	<div class="span9">
		<legend>Filter &amp; Search</legend>

		<!-- filter and search -->
		<?= $this->Form->create('MailPackage', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'input-auto', 'required' => false))) ?>

			<div class="controls-row">
				<?= $this->Form->input('q', array('placeholder' => 'Search customer and sender', 'class' => 'input-xlarge ')) ?>
				<?= $this->Form->input('carrier', array('empty' => '-- Carrier --')) ?>
				<?= $this->Form->input('package_type', array('empty' => '-- Package Type --', 'options' => $package_types)) ?>
			</div>
			<div class="input datetime">
				<?= $this->Form->dateTime('date', 'MDY', null, array('required' => false)) ?>
			</div>

			<?= $this->Form->button('Filter', array('class' => 'btn btn-success')) ?>
			<?= $this->Html->link('Clear', array('action' => 'index')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>
<div class="mailPackages index">
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('date') ?></th>
				<th><?= $this->Paginator->sort('customer_id') ?></th>
				<th><?= $this->Paginator->sort('recipient_email') ?></th>
				<th><?= $this->Paginator->sort('carrier') ?></th>
				<th><?= $this->Paginator->sort('package_type') ?></th>
				<th><?= $this->Paginator->sort('sender') ?></th>
				<th class="actions"><?= __('Actions') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($mailPackages as $mailPackage): ?>
				<tr>
					<td><?= CakeTime::timeAgoInWords($mailPackage['MailPackage']['date']) ?>&nbsp;</td>
					<td class="searchable"><?= $this->Html->link($mailPackage['CustomerAccount']['name'], array('controller' => 'customers', 'action' => 'view', $mailPackage['MailPackage']['customer_id'])) ?>&nbsp;</td>
					<td><?= h($mailPackage['MailPackage']['recipient_email']) ?>&nbsp;</td>
					<td><?= h($mailPackage['MailPackage']['carrier']) ?>&nbsp;</td>
					<td><?= h($mailPackage['MailPackage']['package_type']) ?>&nbsp;</td>
					<td class="searchable"><?= h($mailPackage['MailPackage']['sender']) ?>&nbsp;</td>
					<td class="actions">
						<?= $this->Html->link(__('View'), array('action' => 'view', $mailPackage['MailPackage']['id'])) ?>
						<?= $this->Html->link(__('Edit'), array('action' => 'edit', $mailPackage['MailPackage']['id'])) ?>
						<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $mailPackage['MailPackage']['id']), null, __('Are you sure you want to delete # %s?', $mailPackage['MailPackage']['id'])) ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<div id="push"></div>
	<div class="paging text-center">
		<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?>	</p>
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>

<script>
$(".searchable").highlight(QueryString.q);
</script>

<div class="mailPackages view">
<h2><?php echo __('Mail Package'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Id'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['customer_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recipient Email'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['recipient_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Message'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['email_message']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Carrier'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['carrier']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Package Type'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['package_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sender'); ?></dt>
		<dd>
			<?php echo h($mailPackage['MailPackage']['sender']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mail Package'), array('action' => 'edit', $mailPackage['MailPackage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mail Package'), array('action' => 'delete', $mailPackage['MailPackage']['id']), null, __('Are you sure you want to delete # %s?', $mailPackage['MailPackage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mail Packages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mail Package'), array('action' => 'add')); ?> </li>
	</ul>
</div>

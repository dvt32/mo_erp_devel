<div class="mailboxes form">
<?php echo $this->Form->create('Mailbox'); ?>
	<fieldset>
		<legend><?php echo __('Add Mailbox'); ?></legend>
	<?php
		echo $this->Form->input('mailbox_number');
		echo $this->Form->input('note');
		echo $this->Form->input('floor_id');
	?>
	</fieldset>
<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
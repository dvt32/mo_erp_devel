<section class="module">
	<div class="module-head"><b><?= __('Assign Mailbox') ?></b></div>
	<div class="module-body">
		<?= $this->Form->create('Mailbox', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
		<legend><?= __('Edit Mailbox') ?></legend>
		<?= $this->Form->input('id') ?>
		<?= $this->Form->input('floor_id',        array('readonly')) ?>
		<?= $this->Form->input('mailbox_number',  array('readonly')) ?>
		<?= $this->Form->input('account_id',      array('options' => $customerAccounts, 'class' => 'chosen input-block-level', 'empty' => '-- Unassigned --')) ?>
		<?= $this->Form->input('note',            array()) ?>
		<?= $this->Form->button(__('Submit'),     array('class' => 'btn btn-primary')) ?>
		<?= $this->Form->end();?>
	</div>
</section>

<?= $this->Html->script("libs/chosen/chosen.jquery.min") ?>
<?= $this->Html->css("libs/chosen/chosen.css") ?>
<script>
$(".chosen").chosen();
</script>

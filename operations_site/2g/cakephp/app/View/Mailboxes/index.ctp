<div class="mailboxes index">
	<h2>
		<?= $this->Html->image('icons/dashboard/mail.png', array('style' => 'height: 3em;')) ?>
		<?= __('Mailboxes'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12 well well-mini">
			<div class="row-fluid">
				<!-- filter and search -->
				<?= $this->Form->create('ProximityCardEdit', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
				<div class="span8">
					<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge search-query')) ?>
					<?= $this->Form->input('floor_id', array(
						'empty' => '-- Floor --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>
						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
						<?= $this->Form->end() ?>
					</div>
				</div>
			</div>

	</div>
	<?= $this->Html->link(__('New Mailbox'), array('controller' => 'mailboxes', 'action' => 'add')); ?>
	<br/>
	<!-- /search & filter forms -->
	
	<table class="table table-striped table-condensed">
		<tr>
			<th><?= $this->Paginator->sort('account_id'); ?></th>
			<th><?= $this->Paginator->sort('mailbox_number'); ?></th>
			<th><?= $this->Paginator->sort('creation_dt', 'Assigned'); ?></th>
			<th><?= $this->Paginator->sort('floor_id'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
		</tr>
		<?php foreach ($mailboxes as $mailbox): ?>
			<?php $account = @$mailbox['CustomerAccount'] ?>
			<?php $is_assigned = !empty($account['name']) ?>
		<tr>
			<td>
				<?=
				$is_assigned ?
					$this->Html->link($account['name'], array('controller' => 'customerAccounts', 'action' => 'view', $account['id'])) :
					'<span class="muted">unassigned</span>'
				?>
			</td>
			<td><?= $this->Html->link($mailbox['Mailbox']['mailbox_number'], array('action' => 'view', $mailbox['Mailbox']['id'])); ?>&nbsp;</td>
			<td><?= h($mailbox['Mailbox']['creation_dt']); ?>&nbsp;</td>
			<td><?= $this->Html->link($mailbox['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $mailbox['Floor']['id'])); ?></td>
			<td class="actions">
				<?= $this->Html->link(__('Re-Assign'), array('action' => 'edit', $mailbox['Mailbox']['id'])) ?>
				<?//= $this->Form->postLink(__('Delete'), array('action' => 'delete', $mailbox['Mailbox']['id']), null, __('Are you sure you want to delete # %s?', $mailbox['Mailbox']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
		<div class="paging">
			<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
			?>
		</div>
	</div>

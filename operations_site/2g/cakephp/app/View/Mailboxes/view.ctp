<div class="mailboxes view">
<h2><?php echo __('Mailbox'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mailbox['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $mailbox['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mailbox Number'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['mailbox_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mailbox['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $mailbox['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bool Mail Present'); ?></dt>
		<dd>
			<?php echo h($mailbox['Mailbox']['bool_mail_present']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Assign Mailbox'), array('action' => 'edit', $mailbox['Mailbox']['id'])); ?> </li>
		<li class="active"><?php echo $this->Form->postLink(__('Delete Mailbox'), array('action' => 'delete', $mailbox['Mailbox']['id']), null, __('Are you sure you want to delete # %s?', $mailbox['Mailbox']['id'])); ?> </li>
		<li class="active"><?php echo $this->Html->link(__('List Mailboxes'), array('action' => 'index')); ?> </li>
	</ul>
</div>

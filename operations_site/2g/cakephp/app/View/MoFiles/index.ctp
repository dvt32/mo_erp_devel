
<div class="files index">
	<h2><?php echo __('Files'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('filename'); ?></th>
			<th><?php echo $this->Paginator->sort('mime_type'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('namespace'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($files as $file): ?>
	<tr>
		<td><?php echo h($file['MoFile']['filename']); ?>&nbsp;</td>
		<td><?php echo h($file['MoFile']['mime_type']); ?>&nbsp;</td>
		<td><?php echo h($file['MoFile']['note']); ?>&nbsp;</td>
		<td><?php echo h($file['MoFile']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($file['MoFile']['namespace']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $file['MoFile']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $file['MoFile']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $file['MoFile']['id']), null, __('Are you sure you want to delete # %s?', $file['MoFile']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New File Record'), array('action' => 'add')); ?></li>
	</ul>
</div>

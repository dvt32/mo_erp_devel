<div class="moveoutRequests form">
<?php echo $this->Form->create('MoveoutRequest'); ?>
	<fieldset>
		<legend><?php echo __('Add Moveout Request'); ?></legend>
	<?php
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('move_out_date');
		echo $this->Form->input('close_out_date');
		echo $this->Form->input('security_deposit_address_line_1');
		echo $this->Form->input('security_deposit_address_line_2');
		echo $this->Form->input('security_deposit_city');
		echo $this->Form->input('security_deposit_state');
		echo $this->Form->input('security_deposit_zipcode');
		echo $this->Form->input('num_keycards_returned');
		echo $this->Form->input('num_metal_keys_returned');
		echo $this->Form->input('is_using_moving_company');
		echo $this->Form->input('move_out_requester_id');
		echo $this->Form->input('all_keycards_returned_dt');
		echo $this->Form->input('all_keycards_returned_employee_id');
		echo $this->Form->input('all_metalkeys_returned_dt');
		echo $this->Form->input('all_metalkeys_returned_employee_id');
		echo $this->Form->input('coi_submitted_dt');
		echo $this->Form->input('coi_submitted_employee_id');
		echo $this->Form->input('coi_approved_dt');
		echo $this->Form->input('coi_approved_employee_id');
		echo $this->Form->input('moved_out_dt');
		echo $this->Form->input('moved_out_employee_id');
		echo $this->Form->input('security_deposit_sent_dt');
		echo $this->Form->input('security_deposit_sent_employee_id');
		echo $this->Form->input('security_deposit_notes');
		echo $this->Form->input('security_deposit_payable_to');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Moveout Requests'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="row-fluid">
	<div class="span2">
		<?= $this->Form->create('MoveoutRequest', array(
			'type' => 'get',
			'inputDefaults' => array(
				'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
				'div' => array('class' => 'control-group'),
				'label' => array('class' => 'control-label'),
				'between' => '<div class="controls">',
				'after' => '</div>',
				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
				'required' => false
			)
		)) ?>
			<?= $this->Form->input('cust_acct_id', array(
				'options' => $customerAccounts,
				'class' => 'input-block-level chosen click-submit',
				'empty' => '',
				'label' => 'Customer Account'
			)) ?>
			<div class="form-group">
				<?= $this->Form->input('floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
			</div>
			<label class="control-label">Move-Out Date</label>
			<div class="form-group">
				<?= $this->Form->input('move_out_start_range', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6',
					'label' => false,
					'placeholder' => 'From'
				)) ?>
				<?= $this->Form->input('move_out_end_range', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6',
					'label' => false,
					'placeholder' => 'To'
				)) ?>
			</div>
			<label class="control-label">Close-Out Date</label>
			<div class="form-group">
				<?= $this->Form->input('close_out_start_range', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6',
					'label' => false,
					'placeholder' => 'From'
				)) ?>
				<?= $this->Form->input('close_out_end_range', array(
					'type' => 'text',
					'class' => 'date input-block-level',
					'div' => 'span6',
					'label' => false,
					'placeholder' => 'To'
				)) ?>
			</div>
			<div class="form-group">
				<?= $this->Form->input('flags', ['options' => $flags, 'multiple' => 'checkbox']) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>
	</div>
	<div class="span10">
		<legend><?= __('Move-Out Requests') ?></legend>
		<table class="table table-striped table-condensed`">
			<tr>
				<thead>
					<th><?= $this->Paginator->sort('cust_acct_id', 'Customer Account') ?></th>
					<th><?= $this->Paginator->sort('move_out_date', 'Move-Out') ?></th>
					<th><?= $this->Paginator->sort('close_out_date', 'Close-Out') ?></th>
					<th><?= $this->Paginator->sort('num_keycards_returned', 'Keycards') ?></th>
					<th><?= $this->Paginator->sort('num_metal_keys_returned', 'Metal Keys') ?></th>
					<th><?= $this->Paginator->sort('is_using_moving_company', 'Moving Company?') ?></th>
					<th><?= $this->Paginator->sort('created') ?></th>
					<th>Checklist</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($moveoutRequests as $moveoutRequest): ?>
					<?php $request = $moveoutRequest['MoveoutRequest'] // alias ?>
					<tr>
						<td>
							<?//= $this->Html->link($moveoutRequest['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $moveoutRequest['CustomerAccount']['id'])) ?>
							<?= $this->Html->link($moveoutRequest['CustomerAccount']['name'], array('action' => 'view', $moveoutRequest['MoveoutRequest']['id'])) ?>
						</td>
						<td><?= h($request['move_out_date']) ?>&nbsp;</td>
						<td><?= h($request['close_out_date']) ?>&nbsp;</td>
						<td><?= h($request['num_keycards_returned']) ?>&nbsp;</td>
						<td><?= h($request['num_metal_keys_returned']) ?>&nbsp;</td>
						<td><?= h($request['is_using_moving_company'] ? 'Yes' : 'No') ?>&nbsp;</td>
						<td class="shrink nowrap">
							<?php $created = substr($request['created'],0,19) ?>
							<?php $dt = date_create_from_format('Y-m-d H:i:s', $created) ?>
							<?= $dt->format(DATETIME_USA) ?>
						</td>
						<td class="shrink nowrap">
							<?php $next = $_SERVER['REQUEST_URI'] ?>
							<ul class="unstyled">
								<?php foreach ($flags as $flag => $flag_name): ?>
									<?php if (empty($request["{$flag}_dt"])): ?>
										<li>
											<?php $icon = '<i class="icon-check-empty"></i> ' ?>
											<?= $this->Form->postLink(
												$icon . $flag_name,
												array(
													'action' => 'updateFlag',
													$request['id'],
													$flag,
													'?' => compact('next')
												),
												['escape' => false]
											) ?>
										</li>
									<?php else: ?>
										<li>
											<?php $icon = '<i class="icon-check-sign"></i> ' ?>
											<?= $this->Form->postLink(
												$icon . $flag_name,
												array(
													'action' => 'resetFlag',
													$request['id'],
													$flag,
													'?' => compact('next')
												),
												['escape' => false]
											) ?>
										</li>
									<?php endif ?>
								<?php endforeach ?>
							</ul>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<?= $this->element('paging') ?>
	</div>
</div>


<style>
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #ccc solid; }
	fieldset {
		border:none;
		padding:none;
		margin:none;
		line-height:1.2em;
	}
</style>

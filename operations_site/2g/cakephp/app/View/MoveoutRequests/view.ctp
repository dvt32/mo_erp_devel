<?php $request = $moveoutRequest['MoveoutRequest'] ?>
<section class="module">
	<div class="module-head">
		<b class="pull-right">id=<?= $request['id'] ?></b>
		<b>Move Out Request</b>
	</div>
	<div class="module-body">

		<dl>
			
			<dt>Customer Account</dt>
			<dd><?= $this->Html->link($moveoutRequest['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $moveoutRequest['CustomerAccount']['id'])) ?>&nbsp;</dd>
			
			<dt>Security Deposit</dt>
			<dd>
				<div><?= $request['security_deposit_payable_to'] ?></div>
				<div><?= $request['security_deposit_address_line_1'] ?></div>
				<div><?= $request['security_deposit_address_line_2'] ?></div>
				<div>
					<?= $request['security_deposit_city'] ?>,
					<?= $request['security_deposit_state'] ?>
					<?= $request['security_deposit_zipcode'] ?>
				</div>
			</dd>

			<dt>Security Deposit Notes</dt>
			<dd><?= $request['security_deposit_notes'] ?>&nbsp;</dd>

			<dt>Move Out Date</dt>
			<dd><?= $request['move_out_date'] ?>&nbsp;</dd>

			<dt>Close Out Date</dt>
			<dd><?= $request['close_out_date'] ?>&nbsp;</dd>

			<dt>Created</dt>
			<dd><?= $request['created'] ?>&nbsp;</dd>
			
			<dt>Modified</dt>
			<dd><?= $request['modified'] ?>&nbsp;</dd>


			<dt>All Keycards Returned</dt>
			<dd>
				<?= @$employees[$request['all_keycards_returned_employee_id']] ?>&nbsp;
				<?= strtodatestring($request['all_keycards_returned_dt']) ?: null ?>
			</dd>

			<dt>All Metal Keys Returned</dt>
			<dd>
				<?= @$employees[$request['all_metalkeys_returned_employee_id']] ?>&nbsp;
				<?= strtodatestring($request['all_metalkeys_returned_dt'], DATETIME_USA) ?: null ?>
			</dd>

			<dt>COI Submitted</dt>
			<dd>
				<?= @$employees[$request['coi_submitted_employee_id']] ?>&nbsp;
				<?= strtodatestring($request['coi_submitted_dt']) ?: null ?>
			</dd>

			<dt>COI Approved</dt>
			<dd>
				<?= @$employees[$request['coi_approved_employee_id']] ?>&nbsp;
				<?= strtodatestring($request['coi_approved_dt']) ?: null ?>
			</dd>

			<dt>Moved Out</dt>
			<dd>
				<?= @$employees[$request['moved_out_employee_id']] ?>&nbsp;
				<?= strtodatestring($request['moved_out_dt']) ?: null ?>
			</dd>

			<dt>Security Deposit Sent</dt>
			<dd>
				<?= @$employees[$request['security_deposit_sent_employee_id']] ?>&nbsp;
				<?= strtodatestring($request['security_deposit_sent_dt']) ?: null ?>
			</dd>
		</dl>

	</div>
</section>

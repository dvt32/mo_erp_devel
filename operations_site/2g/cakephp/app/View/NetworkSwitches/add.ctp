<div class="networkSwitches form">
<?php echo $this->Form->create('NetworkSwitch'); ?>
	<fieldset>
		<legend><?php echo __('Add Network Switch'); ?></legend>
	<?php
		echo $this->Form->input('mac_address');
		echo $this->Form->input('model');
		echo $this->Form->input('version');
		echo $this->Form->input('ip_address');
		echo $this->Form->input('notes');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('port');
		echo $this->Form->input('floor_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Network Switches'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

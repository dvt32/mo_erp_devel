<div class="networkSwitches form">
<?php echo $this->Form->create('NetworkSwitch'); ?>
	<fieldset>
		<legend><?php echo __('Edit Network Switch'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('mac_address');
		echo $this->Form->input('model');
		echo $this->Form->input('version');
		echo $this->Form->input('ip_address');
		echo $this->Form->input('notes');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('port');
		echo $this->Form->input('floor_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('NetworkSwitch.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('NetworkSwitch.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Network Switches'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

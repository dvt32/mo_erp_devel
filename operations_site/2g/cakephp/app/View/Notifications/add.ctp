<div class="row-fluid">
	<div class="span8 offset2">
		<section class="module">
			<div class="module-head">
				<b>New Notification</b>
			</div>
			<div class="module-body">
				<?= $this->Form->create('Notification', array('inputDefaults' => array('class' => 'input-block-level', 'required' => false))) ?>
					<div class="row-fluid">
						<div id="NotificationFloors" class="span4">
							<?= $this->Form->select('Notification.floors',$floors,array('multiple' => 'checkbox', 'label' => 'floors')) ?>
						</div>
						<div class="span8">
							<?= $this->Form->input('Notification.name', array('label' => 'Subject')) ?>
							<?= $this->Form->input('Notification.description', array('label' => 'Body')) ?>
							<?= $this->Form->hidden('Notification.is_active', array('value' => true)) ?>
							<?= $this->Form->hidden('Notification.employee_id', array('value' => AuthComponent::user('id'))) ?>
							<?= $this->Form->input('Notification.is_emailed', array('type' => 'checkbox', 'div' => array('class' => 'checkbox inline'), 'label' => 'Send Email')) ?>
							
							<?//= $this->Form->input('Notification.duration') ?>
							<p>
							<?= $this->Form->input('Notification.expires', array('class' => 'datetime', 'label' => 'Expires', 'type' => 'text', 'div' => false, 'value' => date(DATETIME_FORMAT))) ?>
							<div><?= $this->Form->button('Save Notification', array('class' => 'btn')) ?></div>
						</div>
					</div>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
</div>

<style>
#NotificationFloors legend {
	font-size:medium;
	font-weight:bold;
	text-decoration:none;
	border:none;
	margin:0!important;
}
#NotificationFloors fieldset {
	padding:0!important;
	border:none;
}
#NotificationFloors div.checkbox {
	margin-left:1em;
}
</style>

<?= $this->Html->script("libs/jquery.maskedinput.min.js") ?>
<?= $this->Html->script("libs/chosen/chosen.jquery.min") ?>
<?= $this->Html->css("libs/chosen/chosen.css") ?>
<script>
$("input.datetime").datetimepicker({
	format: 'yyyy-mm-dd hh:ii',
	autoclose: true,
	todayBtn: true,
	forceParse: true,
	minView: 0
})
.addClass("input-medium clickable")
.wrap('<div class="input-append"></div>')
.after('<span class="add-on"><i class="icon-calendar"></i></span>');
</script>

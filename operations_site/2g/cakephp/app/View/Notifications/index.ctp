<?php if (!empty($notifications)): ?> 
<div class="notifications index">
	<h2><?= __('Notifications') ?></h2>	
	<div align="right"><a href="{{ operations_2g_urlprefix }}/notifications/add" class="btn btn-primary">Add a Notification</a></div>
	</div>
	<p>
	<table class="table">
		<thead>
			<tr> 
				<th><?= $this->Paginator->sort('id') ?></th>
				<th><?= $this->Paginator->sort('name') ?></th>
				<th>Description</th>
				<th>Notified</th>
				<th><?= $this->Paginator->sort('created') ?></th>
				<th><?= $this->Paginator->sort('employee_id') ?></th>
				<th><?= $this->Paginator->sort('expires') ?></th>
				<th><?= $this->Paginator->sort('send_email', 'Email Sent?') ?></th>
				<th class="actions">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($notifications as $notification): ?>
				<?php $len = strlen($description = $notification['Notification']['description']) ?>
				<?php $description = substr($description, 0, 10) . ($len > 10 ? '...' : null) ?>
				<tr>
					<td><strong class="big"><?= sprintf("%03d", $notification['Notification']['id']) ?></strong></td>
					<td><?= $this->Html->link($notification['Notification']['name'], array('action' => 'view', $notification['Notification']['id'])) ?>&nbsp;</td>
					<td><?= $description ?>&nbsp;</td>
					<td><?= $this->Html->nestedList($notification['Notification']['floors']) ?></td>
					<td><?= CakeTime::nice($notification['Notification']['created']) ?>&nbsp;</td>
					<td><?= h($notification['Employee']['full_name']) ?>&nbsp;</td>
					<td><?= CakeTime::timeAgoInWords($notification['Notification']['expires']) ?>&nbsp;</td>
					<td><?= @$notification['Notification']['is_emailed'] ? 'yes' : 'no' ?></td>
					<td class="actions">
						<?= $this->Html->link(__('Edit'), array('action' => 'edit', $notification['Notification']['id'])) ?> |
						<?= $this->Form->postLink(__('Deactivate'), array('action' => 'delete', $notification['Notification']['id']), null, __('Are you sure you want to delete # %s?', $notification['Notification']['id'])) ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )) ?></p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>
<?php else : ?>
<div class="row-fluid">
	<div class="hero-unit">
		<h1>No Notifications</h1>
		<br/>
		<a href="{{ operations_2g_urlprefix }}/notifications/add" class="btn btn-primary">Add a Notification</a>
	</div>
</div>
<?php endif ?>

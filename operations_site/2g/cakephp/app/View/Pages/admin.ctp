<div class="row-fluid">
	<div class="12">
		<div class="row-fluid">
			<div class="span12 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/gear.png', array('style' => 'height: 10em;')) ?>
				<h2>
					Administrator Dashboard
				</h2>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/document.png' , array('style' => 'height:5em;')) ?>
				<h3>Templates</h3>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<?= $this->Html->link(__('License Agreement Templates'), array('controller' => 'licenseAgreementTemplates', 'action' => 'index')); ?>
						<?= $this->Html->link(__('Amendment Templates'), array('controller' => 'amendmentTemplates', 'action' => 'index')); ?>						
					</li>

				</ul>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/antenna.png' , array('style' => 'height:5em;')) ?>
				<h3>Telecommunications</h3>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<?= $this->Html->link(__('Admin Phone Numbers'), array('controller' => 'phoneNumbers', 'action' => 'admin')); ?>
						<?= $this->Html->link(__('Admin Fax Numbers'), array('controller' => 'faxNumbers', 'action' => 'admin')); ?>
						<?= $this->Html->link(__('Admin IP Phones'), array('controller' => 'ipPhones', 'action' => 'admin')); ?>
						
					</li>

				</ul>
			</div>
			<div class="span4 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/server.png' , array('style' => 'height:5em;')) ?>
				<h3>Server</h3>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<?= $this->Html->link(__('Admin IP Addresses'), array('controller' => 'ipAddresses', 'action' => 'admin')); ?>
						<?= $this->Html->link(__('Admin Server Pings'), array('controller' => 'buildings', 'action' => 'networkMonitorAdmin')); ?>
					</li>
				</ul>
			</div>
			<div class="span4 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/documents.png' , array('style' => 'height:5em;')) ?>
				<h3>Imaging</h3>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<?= $this->Html->link(__('Admin Public Scans'), array('controller' => 'scans', 'action' => 'admin')); ?>
						<?= $this->Html->link(__('Admin Public Faxes'), array('controller' => 'faxIns', 'action' => 'admin')); ?>
						<?= $this->Html->link(__('Admin Claimed Documents'), array('controller' => 'documents', 'action' => 'admin')); ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>



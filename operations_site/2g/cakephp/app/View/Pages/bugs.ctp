<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<div class="centered span4 offset4">
				<h1>Reporting Issues</h1>
				<p>
					Bugs should be reported with a description of what pages, links, and circumstance.
				</p>
				<a href="mailto:cathy@microoffice.com" class="btn btn-danger">
					Report an Issue
				</a>
			</div>
		</div>
	</div>
</div>



<div class="row-fluid">
	<div class="12">
		<div class="row-fluid">
			<div class="span12 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/webcam.png', array('style' => 'height: 10em;')) ?>
				<h2>
					Cameras
				</h2>
				<?= $this->Html->image('icons/flat_miniset/png/256/password.png', array('style' => 'width: 5em;', 'id' => 'creds')) ?>
				<div class="hero-unit creds" style="display:none;">
					<p class="lead">
						Username: mouser
					</p>
					<p class="lead">
						Password: cameras
					</p>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<!-- 1375 Broadway -->
			<div class="span4 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/company.png' , array('style' => 'height:5em;')) ?>
				<h3>1375 Broadway</h3>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="http://rune.loc2.microoffice.com/zm">
							Floor 3
						</a>
					</li>
					<li>
						<a href="http://eagle.loc2.microoffice.com/zm">
							Floor 6
						</a>
					</li>
					<li>
						<a href="http://wren.loc2.microoffice.com/zm">
							Floor 10
						</a>
					</li>
					<li>
						<a href="http://hume.loc2.microoffice.com/zm">
							Floor 11
						</a>
					</li>

				</ul>
			</div>

			<!-- 122 W 27th Street -->
			<div class="span4 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/company.png' , array('style' => 'height:5em;')) ?>
				<h3>122 W 27th Street</h3>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="http://cheops.loc3.microoffice.com/zm">
							Floor 10 and 12
						</a>
					</li>
				</ul>
			</div>
			<!-- 902 Broadway -->
			<div class="span4 centered">
				<?= $this->Html->image('icons/flat_miniset/png/256/company.png' , array('style' => 'height:5em;')) ?>
				<h3>902 Broadway</h3>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="http://egret.loc4.microoffice.com/zm">
							Floor 6 and 7
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script>

compact('creds', 'hover');
</script>


<div class="row-fluid">
	<div class="span12">
		<h1>Operations Platform</h1>
		<h3>Over 100 New Features</h3>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<h1>Dashboard</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Transition</h3>
				<p>Names and labels were kept as similar as possible to the previous operations site. </p>
			</div>
			<div class="span4">
				<h3>Icons and Themes</h3>
				<p>Icons to quickly distinguish each section for users to find quickly.</p>
			</div>
			<div class="span4">
				<h3>Coming Soon</h3>
				<p>Themes and color palettes for each section so each section is easily distinguished from others.</p>
			</div>
		</div>
	</div>
</div>  

<div class="row-fluid">
	<div class="span12">
		<h1>Directory</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Customers</h3>
				<p>Filter, search, sort for any customer. </p>
			</div>
			<div class="span4">
				<h3>Companies</h3>
				<p>Filter, search, sort for any company. </p>
			</div>
			<div class="span4">
				<h3>Employees</h3>
				<p>Filter, search, sort for any employee. </p>
			</div>
		</div>
	</div>
</div>  

<div class="row-fluid">
	<div class="span12">
		<h1>Deskmaps</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>New Maps</h3>
				<p>Desk maps are overlayed with new colors: blue indicating taken spaces, red indicating free spaces, green indicatinged selected spaces.</p>
			</div>
			<div class="span4">
				<h3>Dynamic Table-Map Interactions</h3>
				<p>Selecting a desk on the desk map will filter the table and only display selected desks. Clear button will deselect the spaces and show all rows in the table.</p>
			</div>
			<div class="span4">
				<h3>Tabbed show hides</h3>
				<p>Sensitive financial, notifications, and desk maps are able to be shown or hidden without refreshing the page. Simply click on the pilled linkeds. </p>
			</div>
		</div>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<h1>Mailboxes</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Centralized database of mailboxes</h3>
				<p>Filter, search and sort all mailboxes by company name, mailbox number, and building-floor. </p>
			</div>
			<div class="span4">
				<h3>Deprecated Feature</h3>
				<p>Checking mailboxes will no longer be in effect. </p>
			</div>
			<div class="span4">
				<h3>Assign or Un-Assign mailboxes</h3>
				<p>Add or remove customer accounts easily. </p>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<h1>Access Cards</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Intelli-M Integration</h3>
				<p>Create, edit, and delete access cards within a unified system. Creating, editing, and deactivating access cards will automatically update the Intelli-M access card systems.</p>
			</div>
			<div class="span4">
				<h3>Filter, Search and Sort</h3>
				<p>Search access cards by Company name, Person name, Access Card number. Filter the access card table by building-floor, card status, and technology. Sort any column by ascending or descending order. </p>
			</div>
			<div class="span4">
				<h3>Tabbed show hides</h3>
				<p>Sensitive financial, notifications, and desk maps are able to be shown or hidden without refreshing the page. Simply click on the pilled linkeds. </p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<h3>Bulk add access cards</h3>
				<p>Add hundreds of access cards from a range field. </p>
			</div>
			<div class="span4">
				<h3>Access Cards Requests</h3>
				<p>Automatically detects whether the card is in the intellim system or with any other unintegrated technology. If it is an older access card system without the ability to automatically carry out actions, a notice will be shown with details to guide the user to enter in the data into the older system.</p>
			</div>
			<div class="span4">
				<h3>Pith Drone</h3>
				<p>Drone sweeps the database on a daily basis and cleans up and highlights data integrity issues.</p>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<h1>Navigation</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Universal Navigation</h3>
				<p>Jump from desk maps to access cards to phone numbers, from any page.</p>
			</div>
			<div class="span4">
				<h3>Wikipedia-like links</h3>
				<p>Clicking on linked names will pull up information.</p>
			</div>
			<div class="span4">
				<h3>Phone, Tablet, Computer, TV Screen compatible</h3>
				<p>Screen compatibility tested on 5 different resolutions</p>
			</div>
		</div>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<h1>Cameras</h1>
		<div class="row-fluid">

			<div class="span4">
				<h3>Username and Password</h3>
				<p>Hover over the password icon to reveal the username and password for the camera systems. </p>
			</div>
			<div class="span4">
				<h3>Camera Links</h3>
				<p>Clicking on linked floors will take you to the appropriate building/floor.</p>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<h1>Phone Numbers</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Search and Sort</h3>
				<p>Search phone numbers by Phone number, Company name, and Person name. Sort any column by ascending or descending order. </p>
			</div>
			<div class="span4">
				<h3>Call Forwarding</h3>
				<p>Enabled call forwarding numbers will have an icon distinguishing it to turn it on or off. Turning the call forwarding on will take you to a different page where you confirm the forwarding number. </p>
			</div>
			<div class="span4">
				<h3>Recordings</h3>
				<p>Toggle call recording on or off by clicking on the icons.</p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<h3>Voicemails</h3>
				<p>Toggle voicemails on or off by clicking on the icons.</p>
			</div>
			<div class="span4">
				<h3>Edit</h3>
				<p>Edit any field necessary for each individual phone number. </p>
			</div>
		</div>
	</div>
</div> 

<div class="row-fluid">
	<div class="span12">
		<h1>Fax Numbers</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Filter, Search and Sort</h3>
				<p>Search fax numbers by Company name, Person name, Access Card number. Filter the access card table by building-floor, card status, and technology. Sort any column by ascending or descending order. </p>
			</div>
			<div class="span4">
				<h3>Administration</h3>
				<p>Easily create, modify, or delete fax numbers.</p>
			</div>
			
		</div>
	</div>
</div> 

<div class="row-fluid">
	<div class="span12">
		<h1>IP Phones</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Filter, Search and Sort</h3>
				<p>Search IP phones manufactuer, model, mac address, and notes. Filter by manufactuer, ownership, and models. Sort any column by ascending or descending order. </p>
			</div>
			<div class="span4">
				<h3>Administration</h3>
				<p>Easily create, modify, or delete ip phones and necessary records associated.</p>
			</div>
			
		</div>
	</div>
</div> 

<div class="row-fluid">
	<div class="span12">
		<h1>IP Addresses</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Filter, Search and Sort</h3>
				<p>Search IP addresses by company name, ip address, isp. Filter by building and/or internet service provider (ISP). Sort any column by ascending or descending order. </p>
			</div>
			<div class="span4">
				<h3>Administration</h3>
				<p>Easily create, modify, or delete ip addresses and necessary records associated.</p>
			</div>
			
		</div>
	</div>
</div> 

<div class="row-fluid">
	<div class="span12">
		<h1>Centralized Help and Policies</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Technical Help</h3>
				<p>Report bugs or get help setting up VPN on your computer under technical help.</p>
			</div>
			<div class="span4">
				<h3>Features</h3>
				<p>Document that shows every change as development cycle grows.</p>
			</div>
			
		</div>
	</div>
</div> 


<div class="row-fluid">
	<div class="span12">
		<h1>Faxes</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Filter, Search and Sort</h3>
				<p>Search public faxes by filename. Sort any column by ascending or descending order. </p>
			</div>
			<div class="span4">
				<h3>Claim</h3>
				<p>Simply click claim to claim the document and move it to your account.</p>
			</div>
			<div class="span4">
				<h3>Download</h3>
				<p>Click on the download icon to download your documents via your browser.</p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<h3>Mutiple files</h3>
				<p>Ability to send multiple faxes at once</p>
			</div>
			<div class="span4">
				<h3>Faxage status</h3>
				<p>See all faxage statuses together in a single table</p>
			</div>
		</div>
	</div>
</div> 

<div class="row-fluid">
	<div class="span12">
		<h1>Scans</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Filter, Search and Sort</h3>
				<p>Search public scans by filename. Sort any column by ascending or descending order. </p>
			</div>
			<div class="span4">
				<h3>Claim</h3>
				<p>Simply click claim to claim the document and move it to your account.</p>
			</div>
			<div class="span4">
				<h3>Download</h3>
				<p>Click on the download icon to download your documents via your browser.</p>
			</div>
			
		</div>
	</div>
</div> 


<div class="row-fluid">
	<div class="span12">
		<h1>Networks</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>On Demand Network Tests</h3>
				<p>Ping and check network statuses of internet upon page load.</p>
			</div>
			<div class="span4">
				<h3>Buildings</h3>
				<p>Automatically load buildings that are active and have networks to ping.</p>
			</div>
			
		</div>
	</div>
</div>  

<div class="row-fluid">
	<div class="span12">
		<h1>Conference Rooms are back!</h1>
		<div class="row-fluid">
			<div class="span4">
				<h3>Retro</h3>
				<p>All conference rooms are shown via day view and bookable from a single page. </p>
			</div>
			<div class="span4">
				<h3>Editable on click</h3>
				<p>Clicking on a reservation will take you to a page where you can edit the reservation.</p>
			</div>
			<div class="span4">
				<h3>Time Travel</h3>
				<p>As part of the Micro Office Team, you'll have the privileged ability to jump back and forth in time and edit reservations when necessary. However, keep in mind, your employee_id is tracked with every change. </p>
			</div>
		</div>
	</div>
</div>

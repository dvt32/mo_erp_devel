<p><a href="{{ operations_2g_urlprefix }}/pages/technical">Technical Documents</a></p>
Remote desktop access
</h3><p>
We have a remote desktop computer so you can access some of our applications from outside the office (in case you don't have
OneNote on your desktop computer or laptop).
</p><ol><li>Open the "Remote Desktop Connection" program on your computer.</li><li>Enter nemo.microoffice.com:9999 as the host.</li><li>
Log in as <?= $employee['Employee']['email'] ?>
(or MICROOFFICE\<?= $employee['Employee']['windows_logon_name'] ?>) and your password.
</li></ol><p></p><p>
You'll be able to use a full Outlook client and get <a href="map_network_drive.cgi">file server access</a>. You'll also be
able to use OneNote.
</p><p>
Sometimes it is helpful to be able to use or access local resources (such as printers or your local hard drive) while connected to the remote machine. You might be connected to a remote computer and want to print something to your local printer. Or, you might be connected to a remote computer and want to conveniently copy a file from that remote computer to your local computer. Here is how you can do that:
</p><ol><li>
Start the Remote Desktop Connection program (in Windows 7 or Windows 8, press the Start key on the keyboard and start typing remote desktop).
</li><li>
If you do not see any tabs at the top of the Remote Desktop Connection window, find the button that says "Show Options" in the lower left and click it.
</li><li>
Click the "Local Resources" tab.
</li><li>
Under "Local devices and resources", check the boxes for Printers if you want to be able to print to your local printer and for Clipboard if you want to be able to copy and paste between your local machine and the remote machine. Usually both boxes are checked, so you do not have to do anything.
</li><li>
Click the button that says "More...".
</li><li>
Expand the plus sign to the left of Drives.
</li><li>
Choose the drives you want to use on the remote computer. Usually this will be C:, but it might also be a USB flash drive if you have one plugged in.
</li><li>
Click OK.
</li><li>
Go back to the "General" tab.
</li><li>
Enter the computer and your username, then click on the Connect at the bottom of the window.
</li></ol><p><img src="{{ operations_2g_assetprefix }}/img/micro_office_logo_256x68.png" alt="Micro Office logo"></p>

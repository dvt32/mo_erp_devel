<h1>Micro Office Technical Documentations</h1>
<p class="lead">
	This page holds useful links and documentation on all things technical.
</p>
<ul class="nav nav-pills nav-stacked">
	<li><a href="{{ operations_2g_urlprefix }}/pages/vpn">Virtual Private Network Access</a></li>
	<li><a href="{{ operations_2g_urlprefix }}/pages/screenconnect">Screen Connect</a></li>
	<li><a href="{{ operations_2g_urlprefix }}/pages/rda">Remote Desktop Access</a></li>
	<li><a href="{{ operations_2g_urlprefix }}/pages/msra">Microsoft Remote Assistance(MSRA)</a></li>
</ul>

<p><a href="{{ operations_2g_urlprefix }}/pages/technical">Technical Documents</a></p>
<h3>Virtual Private Network (VPN) access</h3>

<p> Google for "Windows 7 VPN setup" or "Windows 8 VPN setup", depending on whether you are running Windows 7 or 8. Use the following settings: </p> <ul>
  <li>username: <?= $ename['Employee']['email'] ?></li>
  <li> domain: MICROOFFICE (if that gives you problems, try MICROOFFICE.local)</li> 
  <li>VPN server: winvpn.microoffice.com</li> 
</ul>
<p></p>
<p>
  Also, please configure the following: <a href="http://ferhatindi.com/2012/03/using-your-own-internet-during-vpn-connection/"> http://ferhatindi.com/2012/03/using-your-own-internet-during-vpn-connection/</a>
</p>
<p> If you don't, all your traffic will first flow through our 1375 Broadway location, which isn't a big deal for us since we have ample bandwidth, but it will make your traffic needlessly delayed unless you uncheck the box so that your traffic doesn't get proxied by our VPN server. </p>
<p>
  <img src="{{ operations_2g_assetprefix }}/img/micro_office_logo_256x68.png" alt="Micro Office logo">
</p>

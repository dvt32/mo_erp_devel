<div class="phoneNumbers index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/antenna.png' , array('style' => 'height:3em;')) ?>
		<?= __('Phone Numbers'); ?>
	</h2>
	<p>
		<?= $this->Html->link((__('Add Number')), array('controller' => 'pages', 'action' => 'display', 'addNumber'), array('class' => 'btn btn-primary')); ?>
		<?= $this->Html->link((__('Reset PIN')), array('controller' => 'pages', 'action' => 'display', 'resetPin'), array('class' => 'btn btn-info')); ?>
	</p>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('extension'); ?></th>
			<th><?= $this->Paginator->sort('phone_number'); ?></th>
			<th><?= $this->Paginator->sort('do_call_forward', 'Call Forwarding'); ?></th>
			<th><?= $this->Paginator->sort('call_forward_dst_num', 'Call Forward Number'); ?></th>
			<th><?= $this->Paginator->sort('is_voicemail_on', 'Voicemail'); ?></th>
			<th><?= $this->Paginator->sort('is_recording_on', 'Recording'); ?></th>
			<th></th>
	</tr>
	<?php foreach ($phoneNumbers as $phoneNumber): ?>
	<tr>
		<td><?= $phoneNumber['PhoneNumber']['extension'] ?>&nbsp;</td>
		<td><?= h($phoneNumber['PhoneNumber']['phone_number']); ?>&nbsp;</td>
		<td><?php echo($phoneNumber['PhoneNumber']['do_call_forward'] == 0) ?
				$this->Html->link(
					$this->Html->image('icons/flat_miniset/png/256/right.png', array('style' => 'height:2em;')), 
						array('action' => 'forward', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false)): 
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/png/256/close.png', array('style' => 'height:2em;')), 
						array('action' => 'unforward', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false));
			?>
		</td>
		<td><?= h($phoneNumber['PhoneNumber']['call_forward_dst_num']); ?>&nbsp;</td>
		<td><?php echo($phoneNumber['PhoneNumber']['is_voicemail_on'] == 0) ?
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/ringtone_checkmark.png', array('style' => 'height:2em;')), 
						array('action' => 'voiceon', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false)): 
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/ringtone_cross.png', array('style' => 'height:2em;')), 
						array('action' => 'voiceoff', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false));
			?>
		</td>
		<td><?php echo($phoneNumber['PhoneNumber']['is_recording_on'] == 0) ?
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/radio_microphone_cross.png', array('style' => 'height:2em;')), 
						array('action' => 'recordingon', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false)): 
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/radio_microphone_checkmark.png', array('style' => 'height:2em;')), 
						array('action' => 'recordingoff', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false));
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

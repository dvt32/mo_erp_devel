<section class="module">
	<div class="module-head">
		<b>Edit Phone Number</b>
	</div>
	<div class="module-body">
		<?= $this->Form->create('PhoneNumber', [
			'inputDefaults' => [
				'class' => 'input-block-level'
			]
		]); ?>
		<fieldset>
			<legend><?= __('Edit Phone Number'); ?></legend>
			<?= $this->Form->input('account_id', array('options' => $customerAccounts, 'empty' => '(choose one)') ) ?>
			<?= $this->Form->input('person_id', array('options' => $customers, 'empty' => '(choose one)') ) ?>
			<?= $this->Form->input('floor_id') ?>
			<?= $this->Form->input('phone_number') ?>
			<?= $this->Form->input('purpose') ?>
			<?= $this->Form->input('visibility') ?>
			<?= $this->Form->input('domain') ?>
			<?= $this->Form->input('extension') ?>
			<?= $this->Form->input('device_type') ?>
			<?= $this->Form->input('iad_id') ?>
			<?= $this->Form->input('ip_phone_id') ?>
			<?= $this->Form->input('device_port') ?>
			<?= $this->Form->input('is_custom_inbound_dialplan') ?>
			<?= $this->Form->input('is_custom_outbound_dialplan') ?>
			<?= $this->Form->input('do_call_forward') ?>
			<?= $this->Form->input('call_forward_delay') ?>
			<?= $this->Form->input('call_forward_dst_num') ?>
			<?= $this->Form->input('do_simring') ?>
			<?= $this->Form->input('simring_dst_num_list') ?>
			<?= $this->Form->input('block_outbound_caller_id') ?>
			<?= $this->Form->input('is_voicemail_on') ?>
			<?= $this->Form->input('voicemail_box_num') ?>
			<?= $this->Form->input('voicemail_pin') ?>
			<?= $this->Form->input('sip_register_user_id') ?>
			<?= $this->Form->input('sip_register_password') ?>
			<?= $this->Form->input('voicemail_as_email') ?>
			<?= $this->Form->input('email_addr_for_vm') ?>
			<?= $this->Form->input('is_active') ?>
			<?= $this->Form->input('note') ?>
			<?= $this->Form->input('voicemail_delay') ?>
			<?= $this->Form->input('fax_as_email') ?>
			<?= $this->Form->input('email_addr_for_fax') ?>
			<?= $this->Form->input('hold_music') ?>
			<?= $this->Form->input('emit_debug_msg') ?>
			<?= $this->Form->input('inbound_trunk_id') ?>
			<?= $this->Form->input('outbound_trunk_id') ?>
			<?= $this->Form->input('sip_profile') ?>
		</fieldset>

		<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary', 'data-loading-text' => 'Processing...')); ?>
		<?= $this->Form->end(); ?>
	</div>
</section>


<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('PhoneNumber.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('PhoneNumber.id'))); ?></li>
		<li><?= $this->Html->link(__('List Phone Numbers'), array('action' => 'index')); ?></li>
		<li><?= $this->Html->link(__('List Iads'), array('controller' => 'iads', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Iad'), array('controller' => 'iads', 'action' => 'add')); ?> </li>
		<li><?= $this->Html->link(__('List Ip Phones'), array('controller' => 'ip_phones', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Ip Phone'), array('controller' => 'ip_phones', 'action' => 'add')); ?> </li>
		<li><?= $this->Html->link(__('List Inbound Trunks'), array('controller' => 'inbound_trunks', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Inbound Trunk'), array('controller' => 'inbound_trunks', 'action' => 'add')); ?> </li>
		<li><?= $this->Html->link(__('List Outbound Trunks'), array('controller' => 'outbound_trunks', 'action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('New Outbound Trunk'), array('controller' => 'outbound_trunks', 'action' => 'add')); ?> </li>
	</ul>
</div>




<div class="phoneNumbers index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/connect.png', array('style' => 'height: 3em;')) ?>
		<?= __('Phone Numbers'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<legend>Filter &amp; Search</legend>
			<div class="row-fluid">
				<!-- filter and search -->
				<div class="span8">
					<?= $this->Form->create('PhoneNumber', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
						<?= $this->Form->input('q', array('placeholder' => 'Search Numbers', 'class' => 'input-xlarge search-query')) ?>
						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
					<?= $this->Form->end() ?>
				</div>
				<div class="span4">
					<?= __('Results per page') ?>
					<?= $this->Html->link('10', array('?' => array('limit' => 10) + $this->request->query)) ?>
					<?= $this->Html->link('20', array('?' => array('limit' => 20) + $this->request->query)) ?>
					<?= $this->Html->link('50', array('?' => array('limit' => 50) + $this->request->query)) ?>
					<?= $this->Html->link('100', array('?' => array('limit' => 100) + $this->request->query)) ?>
				</div>
			</div>
		</div>
	</div>
	<br>
	<!-- /search & filter forms -->
	<table class="table table-condensed">
	<tr>
			<th><?= $this->Paginator->sort('phone_number'); ?></th>
			<th><?= $this->Paginator->sort('account_id', 'Account'); ?></th>
			<th><?= $this->Paginator->sort('person_id', 'Person'); ?></th>
			<th><?= $this->Paginator->sort('extension'); ?></th>
			<th><?= $this->Paginator->sort('do_call_forward', 'Call Forwarding'); ?></th>
			<th><?= $this->Paginator->sort('is_voicemail_on', 'Voicemail'); ?></th>
			<th><?= $this->Paginator->sort('is_recording_on', 'Recording'); ?></th>
			<th><?= $this->Paginator->sort('creation_dt', 'Activated'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($phoneNumbers as $phoneNumber): ?>
	<tr>
		<td><?= $this->Html->link($phoneNumber['PhoneNumber']['phone_number'], array('action' => 'view', $phoneNumber['PhoneNumber']['id'])); ?></td>
		<!-- account -->
			<?php if (isset($phoneNumber['CustomerAccount']['id'])) :?>
			<td>
				<?= $this->Html->link($phoneNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $phoneNumber['CustomerAccount']['id'])); ?>
			</td>
			<?php else: ?>
			<td>&nbsp;</td>
			<?php endif; ?>
		<!-- /account -->
		<!-- customer -->
			<?php if (isset($phoneNumber['Customer']['id'])) :?>
			<td>
				<?= $this->Html->link($phoneNumber['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $phoneNumber['Customer']['id'])); ?>
			</td>
			<?php else: ?>
			<td>&nbsp;</td>
			<?php endif; ?>
		<!-- /customer -->
		<td><?= h($phoneNumber['PhoneNumber']['extension']); ?>&nbsp;</td>
		<td><?php echo($phoneNumber['PhoneNumber']['do_call_forward'] == 0) ?
				$this->Html->link(
					$this->Html->image('icons/flat_miniset/png/256/right.png', array('style' => 'height:2em;')), 
						array('action' => 'forward', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false)): 
				$this->Form->postLink($this->Html->image('icons/flat_miniset/png/256/close.png', array('style' => 'height:2em;')), 
						array('action' => 'unforward', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false));
			?>
		</td>
		<td><?php echo($phoneNumber['PhoneNumber']['is_voicemail_on'] == 0) ?
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/ringtone_checkmark.png', array('style' => 'height:2em;')), 
						array('action' => 'voiceon', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false)): 
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/ringtone_cross.png', array('style' => 'height:2em;')), 
						array('action' => 'voiceoff', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false));
			?>
		</td>
		<td><?php echo($phoneNumber['PhoneNumber']['is_recording_on'] == 0) ?
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/radio_microphone_cross.png', array('style' => 'height:2em;')), 
						array('action' => 'recordingon', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false)): 
				$this->Form->postLink(
					$this->Html->image('icons/flat_miniset/addons/256/radio_microphone_checkmark.png', array('style' => 'height:2em;')), 
						array('action' => 'recordingoff', $phoneNumber['PhoneNumber']['id']), 
						array('escape' => false));
			?>
		</td>
		<td><?= h($phoneNumber['PhoneNumber']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('Edit'), array('action' => 'edit', $phoneNumber['PhoneNumber']['id'])); ?>
			<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $phoneNumber['PhoneNumber']['id']), null, __('Are you sure you want to delete # %s?', $phoneNumber['PhoneNumber']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<style>
	.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
	.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
	.autocomplete-selected { background: #F0F0F0; }
	.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
</style>
<?= $this->Html->script("libs/jquery.autocomplete.min") ?>
<script type="text/javascript">

$(document).ready(function() {

	$(function() {
		$("#PhoneNumberQ").autocomplete({
			serviceUrl: "{{ operations_2g_urlprefix }}/phoneNumbers/search.json" +
				// too many fields for dynamic fetch, use fieldset instead
				""
		});
	});

});

	
</script>

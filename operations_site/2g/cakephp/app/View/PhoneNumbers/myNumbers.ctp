
<div class="phoneNumbers index">
	<h2><?php echo __('Phone Numbers'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('person_id'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('phone_number'); ?></th>
			<th><?php echo $this->Paginator->sort('purpose'); ?></th>
			<th><?php echo $this->Paginator->sort('visibility'); ?></th>
			<th><?php echo $this->Paginator->sort('domain'); ?></th>
			<th><?php echo $this->Paginator->sort('extension'); ?></th>
			<th><?php echo $this->Paginator->sort('device_type'); ?></th>
			<th><?php echo $this->Paginator->sort('iad_id'); ?></th>
			<th><?php echo $this->Paginator->sort('ip_phone_id'); ?></th>
			<th><?php echo $this->Paginator->sort('device_port'); ?></th>
			<th><?php echo $this->Paginator->sort('is_custom_inbound_dialplan'); ?></th>
			<th><?php echo $this->Paginator->sort('is_custom_outbound_dialplan'); ?></th>
			<th><?php echo $this->Paginator->sort('do_call_forward'); ?></th>
			<th><?php echo $this->Paginator->sort('call_forward_delay'); ?></th>
			<th><?php echo $this->Paginator->sort('call_forward_dst_num'); ?></th>
			<th><?php echo $this->Paginator->sort('do_simring'); ?></th>
			<th><?php echo $this->Paginator->sort('simring_dst_num_list'); ?></th>
			<th><?php echo $this->Paginator->sort('block_outbound_caller_id'); ?></th>
			<th><?php echo $this->Paginator->sort('is_voicemail_on'); ?></th>
			<th><?php echo $this->Paginator->sort('voicemail_box_num'); ?></th>
			<th><?php echo $this->Paginator->sort('voicemail_pin'); ?></th>
			<th><?php echo $this->Paginator->sort('sip_register_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sip_register_password'); ?></th>
			<th><?php echo $this->Paginator->sort('voicemail_as_email'); ?></th>
			<th><?php echo $this->Paginator->sort('email_addr_for_vm'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('modification_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('voicemail_delay'); ?></th>
			<th><?php echo $this->Paginator->sort('fax_as_email'); ?></th>
			<th><?php echo $this->Paginator->sort('email_addr_for_fax'); ?></th>
			<th><?php echo $this->Paginator->sort('hold_music'); ?></th>
			<th><?php echo $this->Paginator->sort('emit_debug_msg'); ?></th>
			<th><?php echo $this->Paginator->sort('inbound_trunk_id'); ?></th>
			<th><?php echo $this->Paginator->sort('outbound_trunk_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sip_profile'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($phoneNumbers as $phoneNumber): ?>
	<tr>
		<!-- account -->
		<?php if (isset($phoneNumber['CustomerAccount']['id'])) :?>
		<td>
			<?php echo $this->Html->link($phoneNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $phoneNumber['CustomerAccount']['id'])); ?>
		</td>
		<?php else: ?>
		<td>&nbsp;</td>
		<?php endif; ?>
		<!-- /account -->
		<!-- customer -->
		<?php if (isset($phoneNumber['Customer']['id'])) :?>
		<td>
			<?php echo $this->Html->link($phoneNumber['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $phoneNumber['Customer']['id'])); ?>
		</td>
		<?php else: ?>
		<td>&nbsp;</td>
		<?php endif; ?>
		<!-- /customer -->

		<!-- floor -->
		<?php if (isset($phoneNumber['Floor']['id'])) :?>
		<td>
			<?php echo $this->Html->link($phoneNumber['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $phoneNumber['Floor']['id'])); ?>
		</td>
		<?php else: ?>
		<td>&nbsp;</td>
		<?php endif; ?>
		<!-- /floor -->
		<td><?php echo h($phoneNumber['PhoneNumber']['phone_number']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['purpose']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['visibility']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['domain']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['extension']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['device_type']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($phoneNumber['Iad']['id'], array('controller' => 'iads', 'action' => 'view', $phoneNumber['Iad']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($phoneNumber['IpPhone']['id'], array('controller' => 'ip_phones', 'action' => 'view', $phoneNumber['IpPhone']['id'])); ?>
		</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['device_port']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['is_custom_inbound_dialplan']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['is_custom_outbound_dialplan']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['do_call_forward']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['call_forward_delay']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['call_forward_dst_num']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['do_simring']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['simring_dst_num_list']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['block_outbound_caller_id']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['is_voicemail_on']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['voicemail_box_num']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['voicemail_pin']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['sip_register_user_id']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['sip_register_password']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['voicemail_as_email']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['email_addr_for_vm']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['note']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['modification_dt']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['voicemail_delay']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['fax_as_email']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['email_addr_for_fax']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['hold_music']); ?>&nbsp;</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['emit_debug_msg']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($phoneNumber['InboundTrunk']['id'], array('controller' => 'inbound_trunks', 'action' => 'view', $phoneNumber['InboundTrunk']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($phoneNumber['OutboundTrunk']['id'], array('controller' => 'outbound_trunks', 'action' => 'view', $phoneNumber['OutboundTrunk']['id'])); ?>
		</td>
		<td><?php echo h($phoneNumber['PhoneNumber']['sip_profile']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $phoneNumber['PhoneNumber']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $phoneNumber['PhoneNumber']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $phoneNumber['PhoneNumber']['id']), null, __('Are you sure you want to delete # %s?', $phoneNumber['PhoneNumber']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Phone Number'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Iads'), array('controller' => 'iads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Iad'), array('controller' => 'iads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ip Phones'), array('controller' => 'ip_phones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ip Phone'), array('controller' => 'ip_phones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inbound Trunks'), array('controller' => 'inbound_trunks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inbound Trunk'), array('controller' => 'inbound_trunks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Outbound Trunks'), array('controller' => 'outbound_trunks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outbound Trunk'), array('controller' => 'outbound_trunks', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="span12">
  <div class="row-fluid">
    <div class="span4 centered offset4">
      <?php echo $this->Form->create('PhoneNumber'); ?>
      <h3>Send SMS</h3>
      <div>
        <?= $this->Form->input('from_email', array('value' => 'service@microoffice.com')) ?>
        <?= $this->Form->input('message', array('type' => 'textarea', 'maxlength' => 160)) ?>
        <?= $this->Form->input('phone_number', array('maxlength' => 10)) ?>
        <?= $this->Form->input('carrier', array('options' => $carriers, 'empty' => 'Pick a Carrier')) ?>
        <?= $this->Form->button('Send Text', array('class' => 'btn btn-primary')) ?>
      <?= $this->Form->end();?>
      </div>
    </div>
  </div>
</div>
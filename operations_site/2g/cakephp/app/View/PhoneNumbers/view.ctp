<div class="phoneNumbers view">
<h2><?= __('Phone Number'); ?></h2>
	<dl>
		<dt><?= __('Id'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['id']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Customer Account'); ?></dt>
		<dd>
			<?= @$this->Html->link($phoneNumber['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $phoneNumber['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Customer'); ?></dt>
		<dd>
			<?= @$this->Html->link($phoneNumber['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $phoneNumber['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Floor'); ?></dt>
		<dd>
			<?= @$this->Html->link($phoneNumber['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $phoneNumber['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Phone Number'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['phone_number']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Purpose'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['purpose']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Visibility'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['visibility']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Domain'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['domain']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Extension'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['extension']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Device Type'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['device_type']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Iad'); ?></dt>
		<dd>
			<?= $this->Html->link($phoneNumber['Iad']['id'], array('controller' => 'iads', 'action' => 'view', $phoneNumber['Iad']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Ip Phone'); ?></dt>
		<dd>
			<?= $this->Html->link($phoneNumber['IpPhone']['id'], array('controller' => 'ip_phones', 'action' => 'view', $phoneNumber['IpPhone']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Device Port'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['device_port']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Is Custom Inbound Dialplan'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['is_custom_inbound_dialplan']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Is Custom Outbound Dialplan'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['is_custom_outbound_dialplan']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Do Call Forward'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['do_call_forward']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Call Forward Delay'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['call_forward_delay']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Call Forward Dst Num'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['call_forward_dst_num']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Do Simring'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['do_simring']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Simring Dst Num List'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['simring_dst_num_list']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Block Outbound Caller Id'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['block_outbound_caller_id']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Is Voicemail On'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['is_voicemail_on']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Voicemail Box Num'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['voicemail_box_num']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Voicemail Pin'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['voicemail_pin']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Sip Register User Id'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['sip_register_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Sip Register Password'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['sip_register_password']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Voicemail As Email'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['voicemail_as_email']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Email Addr For Vm'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['email_addr_for_vm']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Is Active'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Note'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['note']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Creation Dt'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Modification Dt'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['modification_dt']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Voicemail Delay'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['voicemail_delay']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Fax As Email'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['fax_as_email']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Email Addr For Fax'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['email_addr_for_fax']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Hold Music'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['hold_music']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Emit Debug Msg'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['emit_debug_msg']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Inbound Trunk'); ?></dt>
		<dd>
			<?= $this->Html->link($phoneNumber['InboundTrunk']['id'], array('controller' => 'inbound_trunks', 'action' => 'view', $phoneNumber['InboundTrunk']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Outbound Trunk'); ?></dt>
		<dd>
			<?= $this->Html->link($phoneNumber['OutboundTrunk']['id'], array('controller' => 'outbound_trunks', 'action' => 'view', $phoneNumber['OutboundTrunk']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Sip Profile'); ?></dt>
		<dd>
			<?= h($phoneNumber['PhoneNumber']['sip_profile']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul class="nav nav-pills">
		<li class="active"><?= $this->Html->link(__('Edit Phone Number'), array('action' => 'edit', $phoneNumber['PhoneNumber']['id'])); ?> </li>
		<li class="active"><?= $this->Html->link(__('List Phone Numbers'), array('action' => 'index')); ?> </li>
		<li class="active"><?= $this->Html->link(__('List Ip Phones'), array('controller' => 'ip_phones', 'action' => 'index')); ?> </li>
	</ul>
</div>

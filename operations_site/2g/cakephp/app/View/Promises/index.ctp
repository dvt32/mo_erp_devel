<div class="row-fluid">

	<div class="span3">

		<!-- search & filter forms -->
		<?= $this->Form->create('Promise', array(
			'type' => 'GET',
			'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
					'required' => false
			)))
			?>
			<div class="controls-row">
				<?= $this->Form->input('q', ['label' => 'Search']) ?>
				<?= $this->Form->input('cust_acct_id', array(
					'options' => $customerAccounts,
					'class' => 'input-block-level chosen click-submit',
					'empty' => '',
					'label' => 'Customer Account'
				)) ?>
				
				<label>Expiration Date</label>
				<div class="row-fluid">
					<?= $this->Form->input('expiration_date.start', ['class' => 'date input-block-level', 'type' => 'text', 'div' => 'span6', 'label' => false, 'placeholder' => 'Start']) ?>
					<?= $this->Form->input('expiration_date.end',   ['class' => 'date input-block-level', 'type' => 'text', 'div' => 'span6', 'label' => false, 'placeholder' => 'End']) ?>
				</div>

				<?= $this->Form->input('primary_floor_id', array('multiple' => 'checkbox', 'options' => $floors)) ?>
			</div>
			<?= $this->Form->button('Filter', array('class' => 'btn btn-primary')) ?>
			<?= $this->Html->link('Clear', Router::url()) ?>
		<?= $this->Form->end() ?>

	</div>

	<div class="span9">
		<div class="row-fluid">
			<h2><?= __('Promises') ?></h2>
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th><?= $this->Paginator->sort('cust_acct_id', 'Customer') ?></th>
						<!-- <th><?= $this->Paginator->sort('cust_pers_id', 'Person') ?></th> -->
						<th><?= $this->Paginator->sort('expiration_date', 'Expires') ?></th>
						<th><?= $this->Paginator->sort('amount') ?></th>
						<th><?= $this->Paginator->sort('notes') ?></th>
						<th><?= $this->Paginator->sort('creation_dt') ?></th>
						<th><?= $this->Paginator->sort('employee_id') ?></th>
						<th><?= $this->Paginator->sort('status') ?></th>
						<th class="actions"><?= __('Actions') ?></th>
					</tr>
				</thead>
				<?php $subtotal = 0; ?>
				<tbody>
					<?php foreach ($promises as $promise): ?>
						<?php  
							if ($promise['Promise']['status'] == 'proposed') {
								$rowcolor = null;
							} elseif ($promise['Promise']['status'] == 'rejected') {
								$rowcolor = 'error';
							} elseif ($promise['Promise']['status'] == 'approved') {
								$rowcolor = 'success';
							}
						?>
						<tr class="<?= $rowcolor ?>">
							<td><?= $this->Html->link($promise['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $promise['CustomerAccount']['id'])) ?>
							</td>
							<td><?= h($promise['Promise']['expiration_date']) ?>&nbsp;</td>
							<td><?= h($promise['Promise']['amount']) ?>&nbsp;</td>
							<td><?= h($promise['Promise']['notes']) ?>&nbsp;</td>
							<td><?= h($promise['Promise']['creation_dt']) ?>&nbsp;</td>
							<td><?= $this->Html->link($promise['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $promise['Employee']['id'])) ?></td>
							<td><?= h($promise['Promise']['status']) ?>&nbsp;</td>
							<td class="actions">
								<?php if ($promise['Promise']['status'] === 'proposed'): ?>
									<?= $this->Form->postLink('Approve', ['action' => 'approve', $promise['Promise']['id']], ['class' => 'btn btn-mini btn-success'], 'Confirm proposal approval') ?>
									<?= $this->Form->postLink('Reject', ['action' => 'reject', $promise['Promise']['id']], ['class' => 'btn btn-mini btn-danger'], 'Confirm proposal rejection') ?>
								<?php endif ?>
							</td>
						</tr>

						<?php $subtotal = $subtotal + $promise['Promise']['amount'] ?>
					<?php endforeach ?>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><?= money_format('%(#10n', $subtotal) ?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="row-fluid">
			<?= $this->element('paging') ?>
		</div>
	</div>
</div>

<?php $statuses = array_self_combine(array('live', 'customer', 'dead')) ?>
<?php $methods = array_self_combine(array('call', 'email', 'incoming', 'meeting', 'web')) ?>

<div class="row-fluid">
	<ul class="breadcrumb">
	  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
	  <li><a href="{{ operations_2g_urlprefix }}/prospects">Prospective Customers</a><span class="divider">/</span></li>
	  <li class="active">New Prospect<span class="divider">/</span></li>
	</ul>
	<div class="span10 offset1">
		<section class="module">
			<div class="module-head">
				<b>Add New Prospect</b>
			</div>
			<div class="module-body">

				<?= $this->Session->flash() ?>
			
				<?= $this->Form->create('Prospect', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
				<?= $this->Form->hidden('modifier_id', array('value' => AuthComponent::user('id'))) ?>
				<?= $this->Form->hidden('is_active', array('value' => true)) ?>
				<?= $this->Form->hidden('domain_name', array('value' => 'operations')) ?>
				<fieldset>
					<legend>Prospect Information</legend>
					<label>Customer Name</label>
					<div class="controls-row">
						<?= $this->Form->input('salutation', array('class' => 'input-small', 'div' => false, 'label' => false, 'placeholder' => 'Salutation')) ?>
						<?= $this->Form->input('first_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'First Name')) ?>
						<?= $this->Form->input('middle_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'Middle Name')) ?>
						<?= $this->Form->input('last_name', array('class' => 'input-auto', 'div' => false, 'label' => false, 'placeholder' => 'Last Name')) ?>
					</div>

					<div class="row-fluid">
						<div class="span6">
							<?= $this->Form->input('company') ?>
							<?= $this->Form->input('title', array('label' => 'Title / Position at Company')) ?>
							<?= $this->Form->input('business_description', array('type' => 'text')) ?>
							<?= $this->Form->input('web_site') ?>
						</div>
						<div class="span6">
							<?= $this->Form->input('notes', array('placeholder' => 'For internal use only')) ?>
						</div>
					</div>
				</fieldset>
				<!-- / prospect information -->

				<fieldset>
					<legend>Contact Information</legend>
					<div class="row-fluid">
						<div class="span6">
							<?= $this->Form->input('email_address', array('required')) ?>
						</div>
						<div class="span6">
							<label>Phone Numbers</label>
							<?= $this->Form->input('work_phone_number', array('placeholder' => 'Work', 'label' => false)) ?>
							<?= $this->Form->input('home_phone_number', array('placeholder' => 'Home', 'label' => false)) ?>
							<?= $this->Form->input('cell_phone_number', array('placeholder' => 'Cell', 'label' => false)) ?>
						</div>
					</div>
					<div class="row-fluid">
						<label>Work Address</label>
						<?= $this->Form->input('address_1', array('label' => false, 'div' => false, 'placeholder' => 'Address Line 1')) ?>
						<?= $this->Form->input('address_2', array('label' => false, 'div' => false, 'placeholder' => 'Address Line 2')) ?>
						<div class="controls-row">
							<?= $this->Form->input('city', array('class' => 'input-auto', 'placeholder' => 'City', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('state', array('class' => 'input-auto', 'placeholder' => 'State ', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('zip_code', array('class' => 'input-mini', 'placeholder' => 'Zipcode', 'div' => false, 'label' => false)) ?>
							<?= $this->Form->input('country', array('class' => 'input-auto', 'placeholder' => 'Country', 'div' => false, 'label' => false)) ?>
						</div>
					</div>
				</fieldset>
				<!-- / contact information -->

				<fieldset>
					<legend>Sales Information</legend>

					<div class="row-fluid">
						<div class="span6">
							<?= $this->Form->input('amount_of_space') ?>
							<?= $this->Form->input('building_id', array('options' => $buildings, 'empty' => '-- Choose Location --')) ?>
							<div class="row-fluid">
								<div class="span5">
									<?= $this->Form->input('duration_of_rental', array('class' => 'input-small', 'div' => false)) ?>
								</div>
								<div class="span7">
									<?= $this->Form->input('rental_start_date', array('class' => 'date', 'label' => 'Rental Start', 'type' => 'text', 'div' => false)) ?>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span5">
									<?= $this->Form->input('initial_contact_method', array('options' => $methods, 'empty' => '-- Choose One --')) ?>
								</div>
								<div class="span7">
									<!-- TODO set initial contact to now() -->
									<?= $this->Form->input('initial_contact_dt', array('class' => 'datetime', 'label' => 'Initial Contact', 'type' => 'text', 'div' => false, 'value' => date(DATETIME_FORMAT))) ?>
									<?= $this->Form->input('follow_up_date', array('class' => 'date', 'label' => 'Follow Up', 'type' => 'text', 'div' => false, 'value' => date(DATETIME_FORMAT, strto_businessday_time("+3 days")))) ?>
								</div>
							</div>
						</div>
						<div class="span6">
							<?= $this->Form->input('sales_status', array('options' => $statuses, 'value' => '')) ?>
							<?= $this->Form->input('sales_stage', array('options' => $stages)) ?>
							<?= $this->Form->input('SalesTag', array('options' => $tags, 'escape' => false, 'multiple' => true, 'class' => 'input-block-level')) ?>
							<?= $this->Form->input('marketing_source') ?>
						</div>
					</div>
				</fieldset>
				<!-- / sales information -->

				<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
</div>

<?= $this->Html->script("libs/jquery.maskedinput.min.js") ?>
<?= $this->Html->script("libs/chosen/chosen.jquery.min") ?>
<?= $this->Html->css("libs/chosen/chosen.css") ?>
<script>
$("#SalesTagSalesTag").chosen();
$("input.datetime").datetimepicker({
	format: 'yyyy-mm-dd hh:ii',
	autoclose: true,
	todayBtn: true,
	forceParse: true,
	minView: 0
})
.addClass("input-medium clickable")
.wrap('<div class="input-append"></div>')
.after('<span class="add-on"><i class="icon-calendar"></i></span>');

$("input.date").datetimepicker({
	format: 'yyyy-mm-dd',
	autoclose: true,
	todayBtn: true,
	forceParse: true,
	minView: 2
})
.addClass("input-small clickable")
.wrap('<div class="input-append"></div>')
.after('<span class="add-on"><i class="icon-calendar"></i></span>');

// input masks
$("#ProspectWorkPhoneNumber").mask("999-999-9999");
$("#ProspectHomePhoneNumber").mask("999-999-9999");
$("#ProspectCellPhoneNumber").mask("999-999-9999");
$("#ProspectZipCode").mask("99999");
</script>

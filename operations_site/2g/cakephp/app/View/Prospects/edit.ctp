<?php $statuses = array_self_combine(array(
	'live',
	'customer',
	'dead'
)) ?>
<?php $methods = array_self_combine(array(
	'call',
	'email',
	'incoming',
	'meeting',
	'web'
)) ?>
<?php $pc = $prospect['Prospect'] ?>

<div class="row-fluid">
	<ul class="breadcrumb">
		<li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
		<li><a href="{{ operations_2g_urlprefix }}/prospects">Prospective Customers</a><span class="divider">/</span></li>
		<li class="active"><?= $pc['company']?><span class="divider">/</span></li>
	</ul>
	<section class="module">
		<div class="module-head"><b>Prospective Customer Details</b></div>
		<div class="module-body">
			<div class="row-fluid">

				<div class="span6">
					<table class="dl table table-striped">
						<tbody>
							<tr>
								<th><?= __('Name') ?></th>
								<td> <?= implode(' ',array($pc['salutation'],$pc['first_name'],$pc['middle_name'],$pc['last_name'],$pc['suffix'])) ?> </td>
							</tr>
							<tr>
								<th><?= __('Title') ?></th>
								<td><?= $pc['title'] ?: '<span class="muted">(no title)</span>' ?></td>
							</tr>
							<tr>
								<th><?= __('Company') ?></th>
								<td>
									<div><?= ($string = $pc['company']) ? "Name: {$string}" : '<span class="muted">(no company)</span>' ?></div>
									<div><?= ($string = $pc['business_description']) ? "Description: {$string}" : '<span class="muted">(no business description)</span>' ?></div>
								</td>
							</tr>
							<tr>
								<th><?= __('Email Address') ?></th>
								<td><?= $pc['email_address'] ?></td>
							</tr>
							<tr>
								<th><?= __('Phone Numbers') ?></th>
								<td>
									<?= _e('<div>(w) %s</div>', $pc['work_phone_number']) ?>
									<?= _e('<div>(c) %s</div>', $pc['cell_phone_number']) ?>
									<?= _e('<div>(h) %s</div>', $pc['home_phone_number']) ?>
								</td>
							</tr>
							<tr>
								<th><?= __('Address') ?></th>
								<td>
									<?= _e('<div>%s</div>', $pc['address_1']) ?>
									<?= _e('<div>%s</div>', $pc['address_2']) ?>
									<?= __('%s, %s %s', $pc['city'], $pc['state'], $pc['zip_code']) ?>
									&nbsp;
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="span6">
					<table class="dl table table-striped">
						<tbody>
							<tr>
								<th><?= __('Status & Stage') ?></th>
								<td><?= $pc['sales_status'] ?>&nbsp;</td>
							</tr>
							<tr>
								<th><?= __('Space Needs') ?></th>
								<td>
									<?= _e('<div>Size: %s</div>', $pc['amount_of_space']) ?>
									<?= _e('<div>Duration: %s</div>', $pc['duration_of_rental']) ?>
									<?= _e('<div>Location: %s</div>', $pc['desired_location']) ?>
								</td>
							</tr>
							<tr>
								<th><?= __('Notes') ?></th>
								<td> <div>Rental Start Date: <?= $pc['rental_start_date'] ?>&nbsp;</div></td>
							</tr>
							<tr>
								<th><?= __('Followup Date') ?></th>
								<td><?= _e('<div>%s</div>', $pc['follow_up_date']) ?></td>
							</tr>
							<tr>
								<th><?= __('Marketing Source') ?></th>
								<td><?= _e('<div>%s</div', $pc['marketing_source']) ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="text-right">
				<?= $this->Html->link(__('Convert to Customer'), array('action' => 'convert', $pc['id']), array('class' => 'btn btn-success')) ?>
			</div>
		</div>
	</section>

	<section class="module">
		<div class="module-head"><b>Sales interaction (<?= @$pc['sales_interaction_count'] ?> total)</b></div>
		<div class="module-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Time</th>
						<th>Type</th>
						<th>Notes</th>
						<th>Sales Person</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1 ?>
					<?php foreach ($prospect['SalesInteraction'] as $interaction): ?>
						<tr>
							<td><?= $i++ ?></td>
							<td><?= $interaction['interaction_dt'] ?></td>
							<td><?= $interaction['interaction_type'] ?></td>
							<td><?= $interaction['notes'] ?></td>
							<td><?= $interaction['salesperson'] ?></td>
							<td>
								<?= $this->Html->link(__('Edit'), array('controller' => 'salesInteractions', 'action' => 'edit', $interaction['id'])) ?> | 
								<?= $this->Form->postLink(__('Delete'), array('controller' => 'salesInteractions', 'action' => 'delete', $interaction['id']), null, __('Are you sure you want to delete # %s?', $interaction['id'])) ?>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</section>

	<section class="module">
		<div class="module-head"><b>Quick Edit</b></div>
		<div class="module-body">

			<?= $this->Session->flash() ?>
		
			<?= $this->Form->create('Prospect', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
			<!-- sales information -->
			<div class="row-fluid">
				<fieldset class="span6">
					<?= $this->Form->input('id') ?>
					<div class="row-fluid">
						<div class="span4">
							<?= $this->Form->input('follow_up_date', array('class' => 'date', 'label' => 'Follow Up', 'type' => 'text', 'div' => false)) ?>
						</div>
						<div class="span8">
							<label id="followupIcalendarLabel">Export to Calendar</label>
							<div id="followupIcalendar"></div>
						</div>
					</div>
					<?= $this->Form->input('reminder_email_address') ?>
					<div class="row-fluid">
						<div class="span5">
							<label>Sales Status</label>
							<?= $this->Form->input('sales_status', array('type' => 'radio', 'options' => $statuses, 'fieldset' => false, 'legend' => false)) ?>
						</div>
						<div class="span7">
							<label>Sales Stage</label>
							<?= $this->Form->input('sales_stage', array('type' => 'radio', 'options' => $stages, 'fieldset' => false, 'legend' => false)) ?>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span11">
							<?= $this->Form->input('Prospect.SalesTag',array('options'=>$tags,'class'=>'input-block-level','multiple'=>true)) ?>
						</div>
					</div>
				</fieldset>
				<fieldset class="span6">
					<div class="row-fluid">
						<div class="span7">
							<?= $this->Form->input('SalesInteraction.0.interaction_dt', array('type' => 'text', 'class' => 'datetime', 'label' => 'Interaction Time'/*, 'value' => date(DATETIME_FORMAT)*/)) ?>
						</div>
						<div class="span5">
							<label>Interaction type</label>
							<?= $this->Form->input('SalesInteraction.0.interaction_type', array('type' => 'radio', 'options' => array_self_combine(array('call','email','meeting')), 'fieldset' => false, 'legend' => false)) ?>
						</div>
					</div>
					<?= $this->Form->input('SalesInteraction.0.notes', array('type' => 'textarea')) ?>
				</fieldset>
			</div>
			<!-- / sales information -->
			<div class="row-fluid">
				<?= $this->Form->button('Save', array('class' => 'btn btn-primary span4 offset8')) ?>
			</div>
			<?= $this->Form->end() ?>
		</div>
	</section>
</div>

<?php $followup_title = "Followup: {$pc['first_name']} {$pc['last_name']}" ?>
<?php $followup_description =
	Router::url($this->here, true) . '\n' .
	"Name: {$pc['first_name']} {$pc['last_name']}" . '\n' .
	"Title: {$pc['title']}" . '\n' .
	"Company: {$pc['company']}" . '\n' .
	"Email: {$pc['email_address']}" . '\n' .
	"Phone" . '\n' .
	" (W): {$pc['work_phone_number']}" . '\n' .
	" (C): {$pc['cell_phone_number']}" . '\n' .
	" (H): {$pc['home_phone_number']}" . '\n' .
	"Space: {$pc['amount_of_space']}" . '\n' .
	"Location: {$pc['desired_location']}"
?>

<?= $this->Html->script("libs/jquery-migrate-1.2.1.js") ?>
<?= $this->Html->script("libs/chosen/chosen.jquery.min") ?>
<?= $this->Html->css("libs/chosen/chosen.css") ?>
<?= $this->Html->css("libs/icalendar/jquery.icalendar.css") ?>
<?= $this->Html->script("libs/icalendar/jquery.icalendar.js") ?>
<script>
	function convertDateTime(dateTime){
		dateTime = dateTime.split(" ");

		var date = dateTime[0].split("-");
		var yyyy = date[0];
		var mm = date[1]-1;
		var dd = date[2];

		var h = '9';
		var m = '00';
		var s = '00';

		if (dateTime[1]) {
			var time = dateTime[1].split(":");
			if (time) {
				var h = time[0];
				var m = time[1];
				var s = parseInt(time[2]); //get rid of that 00.0;
			}
		}
		return new Date(yyyy,mm,dd,h,m,s);
	}

	$(function() {
		if ($("#ProspectFollowUpDate").val()) {
			var followup_start_dt = convertDateTime($("#ProspectFollowUpDate").val());
			$('#followupIcalendar').icalendar({
				start: followup_start_dt,
				end: followup_start_dt,
				title: '<?= $followup_title ?>',
				description: '<?= $followup_description ?>'
			});
		}
	});

	$("#ProspectFollowUpDate").change(function() {
		var followup_start_dt = convertDateTime($(this).val());
		$('#followupIcalendar').icalendar({
			start: followup_start_dt,
			end: followup_start_dt,
			title: '<?= $followup_title ?>',
			description: '<?= $followup_description ?>'
		});
	});
</script>


<script>
$("#ProspectSalesTag").chosen();

$("input.datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss',
	autoclose: true,
	todayBtn: true,
	forceParse: true,
	minView: 0
})
.addClass("input-medium clickable")
.wrap('<div class="input-append"></div>')
.after('<span class="add-on"><i class="icon-calendar"></i></span>');

$("input.date").datetimepicker({
		format: 'yyyy-mm-dd',
	autoclose: true,
	todayBtn: true,
	forceParse: true,
	minView: 2
})
.addClass("input-small clickable")
.wrap('<div class="input-append"></div>')
.after('<span class="add-on"><i class="icon-calendar"></i></span>');

</script>

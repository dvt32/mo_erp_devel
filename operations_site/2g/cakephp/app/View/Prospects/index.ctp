<?php @$query = $this->request->query ?>

<ul class="breadcrumb">
  <li><a href="{{ operations_2g_urlprefix }}/">Home</a> <span class="divider">/</span></li>
  <li class="active">Prospective Customers<span class="divider">/</span></li>
</ul>

<h2><?= __('Prospective Customers') ?></h2>

<div class="row-fluid">
	<div class="span2">
		<?= $this->Html->link('Add New Prospect', array('action' => 'add'), array('class' => 'btn btn-info btn-block')) ?>

		<?= $this->Form->create('Prospect', array(
			'type' => 'get',
			'inputDefaults' => array(
				'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
				'div' => array('class' => 'control-group'),
				'label' => array('class' => 'control-label'),
				'between' => '<div class="controls">',
				'after' => '</div>',
				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
				'required' => false
			)
		)) ?>
		<legend>Search</legend>
		<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-block-level', 'label' => false)) ?>
		<?php $building_ids = $this->request->query('building_id') ?: [] ?>
		<?php foreach ($buildings as $city_name => $city): ?>
			<?php $city_slug = strtolower(preg_replace('/[\s]+/mu', null, $city_name)) ?>
			<label class="checkbox">
				<input type="checkbox" class="city-checkbox" data-city="<?= $city_slug ?>">
				<?= $city_name ?>
			</label>
			<?php foreach ($city as $building_id => $short_global_label): ?>
				<label class="checkbox" style="margin-left:1em;">
					<?= $this->Form->input('building_id[]', [
						'type' => 'checkbox',
						'value' => $building_id,
						'label' => false,
						'hiddenField' => false,
						'class' => 'building-checkbox city-' . $city_slug,
						'checked' => in_array($building_id, $building_ids)
					]) ?>
					<?= $short_global_label ?>
				</label>
			<?php endforeach ?>
		<?php endforeach ?>

		<?//= $this->Form->input('building_id', ['multiple' => 'checkbox']) ?>
		<?= $this->Form->button('Search', array('class' => 'btn btn-primary')) ?>
		<?= $this->Html->link('Clear', array('action' => 'index')) ?>
		<?= $this->Form->end() ?>

		<legend>Stages</legend>
		<ul class="nav nav-pills nav-stacked">
			<?php $active_stage = @$query['sales_stage'] ?>
			<?php foreach ($sales_stages as $stage_id => $stage): ?>
				<li class="<?= ($active_stage == substr($stage,0,1)) ? 'active' : null ?>">
					<?= $this->Html->link($stage, array('?' => array('sales_stage' => $stage_id) + $query) ) ?>
				</li>
			<?php endforeach ?>
		</ul>

		<legend>Shortcuts</legend>
		<ul>
			<li><?= $this->Html->link('Today',    array('?' => array('followup' => 'today') + $query)) ?></li>
			<li><?= $this->Html->link('Tomorrow', array('?' => array('followup' => 'tomorrow') + $query)) ?></li>
			<li><?= $this->Html->link('Upcoming', array('?' => array('followup' => 'upcoming') + $query)) ?></li>
			<li><?= $this->Html->link('Past',     array('?' => array('followup' => 'past') + $query)) ?></li>
		</ul>
	</div>
	<div class="span10">
		<table class="table table-hover">
			<thead>
				<tr>
					<th><?= $this->Paginator->sort('sales_status',       'Status') ?></th>
					<th><?= $this->Paginator->sort('sales_stage',        'Stage') ?></th>
					<th><?= $this->Paginator->sort('full_name',          'Name') ?></th>
					<th><?= $this->Paginator->sort('company',            'Company') ?></th>
					<th><?= $this->Paginator->sort('email_address',      'Email') ?></th>
					<th><?= $this->Paginator->sort('desired_location',   'Location') ?></th>
					<th><?= $this->Paginator->sort('amount_of_space',    'Space') ?></th>
					<th><?= $this->Paginator->sort('follow_up_date',     'Follow Up') ?></th>
				</tr>
			</thead>
		<?php foreach ($prospects as $prospect): ?>
			<tbody>
				<tr>
					<td><?= h($prospect['Prospect']['sales_status']) ?>&nbsp;</td>
					<td><?= substr($prospect['Prospect']['sales_stage'],0,1) ?>&nbsp;</td>
					<td><?= $this->Html->link(__($prospect['Prospect']['full_name']), array('action' => 'edit', $prospect['Prospect']['id'])) ?>&nbsp;</td>
					<td><?= h($prospect['Prospect']['company']) ?>&nbsp;</td>
					<td><?= h($prospect['Prospect']['email_address']) ?>&nbsp;</td>
					<td><?= h($prospect['Prospect']['desired_location']) ?>&nbsp;</td>
					<td><?= h($prospect['Prospect']['amount_of_space']) ?>&nbsp;</td>
					<td><?= h($prospect['Prospect']['follow_up_date']) ?>&nbsp;</td>
				</tr>
			</tbody>
		<?php endforeach ?>
		</table>
	</div>
</div>

<?= $this->element('paging') ?>



<style>
	.form-group { display:inline-block; }
	.control-label { border-bottom:1px #ccc solid; }
	.controls fieldset {
		border:none;
		padding:none!important;
		margin:none;
		line-height:1.2em;
	}
	.controls fieldset legend {
		border-bottom:none;
		font-size:1em;
		margin-bottom:none!important;
	}
</style>

<script type="text/javascript">
	$(".city-checkbox").change(function() {
		var cityslug = $(this).attr('data-city');
		if (this.checked) {
			$('.building-checkbox.city-' + cityslug).prop('checked', true);
		}
		else
		{
			console.log($('.building-checkbox .city-' + cityslug));
			$('.building-checkbox.city-' + cityslug).prop('checked', false);
		}
	})
</script>

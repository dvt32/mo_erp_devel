<?php $pc = $prospect['Prospect'] ?>

<section class="module">
	<div class="module-head"><b>Prospective Customer</b></div>
	<div class="module-body">
		<dl>
			<dt><?= __('Status & Stage') ?></dt>
			<dd><?= $pc['sales_status'] ?>&nbsp;</dd>
			<dt><?= __('Name') ?></dt>
			<dd> <?= implode(' ',array($pc['salutation'],$pc['first_name'],$pc['middle_name'],$pc['last_name'],$pc['suffix'])) ?> </dd>
			<dt><?= __('Company') ?></dt>
			<dd>
				<div><?= ($string = $pc['company']) ? "Name: {$string}" : '<span class="muted">(no company)</span>' ?></div>
				<div><?= ($string = $pc['business_description']) ? "Description: {$string}" : '<span class="muted">(no business description)</span>' ?></div>
			</dd>
			<dt><?= __('Title') ?></dt>
			<dd><?= $pc['title'] ?: '<span class="muted">(no title)</span>' ?></dd>
			<dt><?= __('Phone Numbers') ?></dt>
			<dd>
				<?= _e('<div>(w) %s</div>', $pc['work_phone_number']) ?>
				<?= _e('<div>(c) %s</div>', $pc['cell_phone_number']) ?>
				<?= _e('<div>(h) %s</div>', $pc['home_phone_number']) ?>
			</dd>
			<dt><?= __('Email Address') ?></dt>
			<dd><?= $pc['email_address'] ?></dd>
			<dt><?= __('Address') ?></dt>
			<dd>
				<?= _e('<div>%s</div>', $pc['address_1']) ?>
				<?= _e('<div>%s</div>', $pc['address_2']) ?>
				<?= __('%s, %s %s', $pc['city'], $pc['state'], $pc['zip_code']) ?>
				&nbsp;
			</dd>
			<dt><?= __('Space Needs') ?></dt>
			<dd>
				<?= _e('<div>Size: %s</div>', $pc['amount_of_space']) ?>
				<?= _e('<div>Duration: %s</div>', $pc['duration_of_rental']) ?>
				<?= _e('<div>Location: %s</div>', $pc['desired_location']) ?>
			</dd>
			<dt><?= __('Notes') ?></dt>
			<dd>
				<div>Rental Start Date: <?= $pc['rental_start_date'] ?>&nbsp;</div>
			</dd>
			<dt><?= __('Followup Date') ?></dt>
			<dd><?= _e('<div>%s</div>', $pc['follow_up_date']) ?></dd>
		</dl>
	</div>
</section>

<section class="module">
	<div class="module-head"><b>Quick Edit</b></div>
	<div class="module-body">
		<div class="row-fluid">
			<div class="span6">

			</div>
			<div class="span6">

			</div>
		</div>
	</div>
</section>

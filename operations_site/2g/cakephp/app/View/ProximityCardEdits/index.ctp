<div class="proximityCardEdits index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/addons/256/hourglass_shield.png', array('style' => 'height: 3em;')) ?>
		<?= __('Access Cards Requests'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<legend>Filter &amp; Search</legend>
			<div class="row-fluid">
				<!-- filter and search -->
				<?= $this->Form->create('ProximityCardEdit', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
				<div class="span12 well well-mini">
					<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge search-query')) ?>
					<?= $this->Form->input('action_requested', array(
						'options' => $action_requesteds, 
						'empty' => '-- Requested --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;'
					)) ?>
					<?= $this->Form->input('cause', array(
						'options' => $causes, 
						'empty' => '-- Causes --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;'
					)) ?>
					<?= $this->Form->input('floor_id', array('empty' => '-- Floor --', 'style' => 'margin-bottom:0px;')) ?>
						<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
						<?= $this->Html->link('Clear', array('action' => 'index')) ?>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</div>
	</div>
	<br/>
	<!-- /search & filter forms -->
	
	<table class="table table-striped table-condensed">
		<tr>
			<th><?= $this->Paginator->sort('account_id'); ?></th>
			<th><?= $this->Paginator->sort('person_id'); ?></th>
			<th><?= $this->Paginator->sort('number', 'Card Number'); ?></th>
			<th><?= $this->Paginator->sort('action_requested'); ?></th>
			<th><?= $this->Paginator->sort('cause'); ?></th>
			<th><?= $this->Paginator->sort('creation_dt', 'Requested'); ?></th>
			<th><?= $this->Paginator->sort('operator', 'Requested By'); ?></th>
			<th><?= $this->Paginator->sort('completed', 'Status'); ?></th>
			<th class="actions"><?= __('Actions'); ?></th>
		</tr>
		<?php foreach ($proximityCardEdits as $proximityCardEdit): ?>
			<tr>
				<td>
					<?= $this->Html->link($proximityCardEdit['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCardEdit['CustomerAccount']['id'])); ?>
				</td>
				<td>
					<?= $this->Html->link($proximityCardEdit['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCardEdit['Customer']['id'])); ?>
				</td>
				<td><?= h($proximityCardEdit['ProximityCardEdit']['number']); ?>&nbsp;</td>
				<td><?= h($proximityCardEdit['ProximityCardEdit']['action_requested']); ?>&nbsp;</td>
				<td><?= h($proximityCardEdit['ProximityCardEdit']['cause']); ?>&nbsp;</td>
				<td><?= h($proximityCardEdit['ProximityCardEdit']['creation_dt']); ?>&nbsp;</td>
				<td><?= h($proximityCardEdit['ProximityCardEdit']['operator']); ?>&nbsp;</td>
				<!-- Status -->
				<td>
					<?php if ($proximityCardEdit['ProximityCardEdit']['completed'] == 1) : ?>
						<?= $this->Html->image('icons/flat_miniset/png/256/checkmark.png', array('style' => 'height:2em;')); ?>
					<?php elseif ($proximityCardEdit['ProximityCardEdit']['rejected'] == 1) : ?>
						<?= $this->Html->image('icons/flat_miniset/png/256/cancel.png', array('style' => 'height:2em;')); ?>
					<?php else: ?> 
						<?= $this->Html->image('icons/flat_miniset/png/256/info.png', array('style' => 'height:2em;')); ?>
					<?php endif ?>
					&nbsp;
				</td>				
				<td class="actions">
					<?= $this->Html->link(__('Validate'), array('controller' => 'ProximityCardEdits', 'action' => 'view', $proximityCardEdit['ProximityCardEdit']['id'])); ?>
					<?= $this->Html->link(__('Approve'), array('controller' => 'proximityCards', 'action' => 'approve', $proximityCardEdit['ProximityCardEdit']['id'])); ?>
					<?= $this->Form->postLink(__('Rejected'), array('action' => 'reject', $proximityCardEdit['ProximityCardEdit']['id']), null, __('Are you sure you want to reject # %s?', $proximityCardEdit['ProximityCardEdit']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<div class="paging">
				<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
			</div>
		</div>

<div class="proximityCardEdits view">
<h2><?php echo __('Proximity Card Edit'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCardEdit['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCardEdit['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCardEdit['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCardEdit['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action Requested'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['action_requested']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cause'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['cause']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Operator'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['operator']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Access Level'); ?></dt>
		<dd>
			<?php echo $this->Html->link($proximityCardEdit['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCardEdit['AccessLevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Completed'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['completed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rejected'); ?></dt>
		<dd>
			<?php echo h($proximityCardEdit['ProximityCardEdit']['rejected']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<ul class="nav nav-pills">
	<li> <a href="{{ operations_2g_urlprefix }}/proximityCards/approve/<?= $proximityCardEdit['ProximityCardEdit']['id'] ?>" class="btn btn-success">Approve</a></li>
	<li> <a href="{{ operations_2g_urlprefix }}/proximity_card_edits/reject" class="btn btn-danger">Reject</a></li>
</ul>

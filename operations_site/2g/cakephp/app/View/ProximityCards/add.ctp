<div class="proximityCards form">
	<?= $this->Form->create('ProximityCard'); ?>
	<fieldset>
		<legend><?= __('Add Proximity Card'); ?></legend>
		<?php
		echo $this->Form->input('number', array('label' => 'Card Number'));
		echo $this->Form->hidden('type', array('value' => 'card'));
		echo $this->Form->input('type', array(
			'options' => array(
				'card' => 'Card',
				'fob' => 'Key Fob',
				),
			'card' => 'Card'
			));
		echo $this->Form->hidden('status', array(
			'value' => 'active'
			));
		echo $this->Form->input('account_id', array(
			'options' => $customerAccounts,
			'empty' => '(Choose and Account and Person)'
			)); ?>
			<div class="input select" style="display:none;" id="persons_show">
				<label for="ProximityCardPersonId">Person</label>
				<select name="data[ProximityCard][person_id]" id="ProximityCardPersonId" ></select>
			</div>

			<?php
			echo $this->Form->input('note');
			echo $this->Form->input('access_level_id');
			echo $this->Form->input('floor_id', array(
				'options' => $floors
				));
			echo $this->Form->input('group_intellim_id', array(
				'options' => $groups,
				'multiple' => true
			));
			echo $this->Form->input('technology', array(
				'options' => array(
					'proximity' => 'Proximity Card - IntelliM',
					'smart' => 'Smart Card',
					),
				'proximity' => 'Proximity Card - IntelliM'
				));
			echo $this->Form->input('site_code', array('value' => '1'));
			?>
		</fieldset>
		<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary', 'data-loading-text' => 'Processing...')); ?>
		<?= $this->Form->end(); ?>


	</div>
	<script>
		$('#ProximityCardAccountId').change(function() {
			var account_id = $(this).val();
			$.getJSON("{{ operations_2g_urlprefix }}/proximityCards/lookup/"+account_id, function(d) {
				$('#persons_show').show();
				$('#ProximityCardPersonId').empty();
				_.each(d, function(v, k) {
					$('#ProximityCardPersonId').append('<option value="'+k+'"">'+v+'</option>');
				});
			});

		});
	</script>

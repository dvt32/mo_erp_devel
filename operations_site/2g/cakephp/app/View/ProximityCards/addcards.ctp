<div class="row-fluid">
	<div class="span8 offset2">
		<section class="module">
			<div class="module-head"> <b>Add Batch of Proximity Cards</b> </div>
			<div class="module-body">
				<?= $this->Form->create('ProximityCard', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
					<?= $this->Form->hidden('site_code', array('value' => '1')) ?>
					<?= $this->Form->hidden('type', array('value' => 'card')) ?>
					<div class="controls-row">
						<label>Card Number Range</label>
						<?= $this->Form->input('range_start', array('placeholder' => 'First', 'label' => false, 'div' => false, 'class' => 'input-small')) ?>
						 - 
						<?= $this->Form->input('range_end', array('placeholder' => 'Last', 'label' => false, 'div' => false, 'class' => 'input-small')) ?>
					</div>
					<?= $this->Form->input('note') ?>
					<div class="row-fluid">
						<div class="span6">
							<?= $this->Form->input('floor_id', array('options' => $floors, 'empty' => '-- Floor --', 'required' => true)) ?>
						</div>
						<div class="span6">
							<?= $this->Form->input('technology', array('options' => array('smart' => 'Smart Card', 'proximity' => 'Proximity Card (IntelliM)'), 'proximity' => 'Proximity Card (IntelliM)')) ?>
						</div>
					</div>
					<?= $this->Form->button('Save Cards', array('class' => 'btn btn-primary')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
</div>

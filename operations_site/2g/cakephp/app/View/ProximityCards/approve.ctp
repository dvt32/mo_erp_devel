<div class="row">
	<div class="span12">
	<h2><?= __('Proximity Card Approval Form'); ?></h2>
		<div class="row">
			<div class="span5">
				<h3>Tenant Info</h3>
				<!-- Tenant added info -->
					<dl>
						<dt><?= __('Customer Account'); ?></dt>
						<dd>
							<?= $this->Html->link($proximityCardEdit['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCardEdit['CustomerAccount']['id'])); ?>
							&nbsp;
						</dd>
						<dt><?= __('Customer'); ?></dt>
						<dd>
							<?= $proximityCardEdit['Customer']['full_name']; ?>
							&nbsp;
						</dd>
						<dt><?= __('Action Requested'); ?></dt>
						<dd>
							<?= h($proximityCardEdit['ProximityCardEdit']['action_requested']); ?>
							&nbsp;
						</dd>
						<dt><?= __('Cause'); ?></dt>
						<dd>
							<?= h($proximityCardEdit['ProximityCardEdit']['cause']); ?>
							&nbsp;
						</dd>
						
						<dt><?= __('Access Level'); ?></dt>
						<dd>
							<?= $this->Html->link($proximityCardEdit['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCardEdit['AccessLevel']['id'])); ?>
							&nbsp;
						</dd>
					</dl>
				
			</div>
			<div class="span5">
				<h3>MicroOffice Approval Form</h3>
				<!-- Approval add info -->
				<?= $this->Form->create('ProximityCard'); ?>
					<?php
						echo $this->Form->input('number', array('label' => 'Card Number'));
						echo $this->Form->hidden('type', array('value' => 'card'));
						echo $this->Form->input('type', array(
							'options' => array(
								'card' => 'Card',
								'fob' => 'Key Fob',
								),
							'card' => 'Card'
							));
						echo $this->Form->input('status', array(
							'options' => array(
								'active' => 'Active',
								'inactive' => 'Inactive',
								'lost' => 'Lost'
								),
							'active' => 'active'
							));
						echo $this->Form->input('account_id', array(
							'options' => $customerAccounts,
							'selected' => $proximityCardEdit['CustomerAccount']['id'],
							'empty' => '(Choose and Account and Person)'
							)); 
					?>
					<div class="input select" style="display:none;" id="persons_show">
						<label for="ProximityCardPersonId">Person</label>
						<select name="data[ProximityCard][person_id]" id="ProximityCardPersonId" ></select>
					</div>
					<?php
						echo $this->Form->input('floor_id', array(
							'options' => $floors
							));
						echo $this->Form->hidden('intellim_id');
						echo $this->Form->input('group_intellim_id', array(
							'options' => $groups,
							'multiple' => true
						));
					?>
					<?php
						echo $this->Form->input('technology', array(
							'options' => array(
								'proximity' => 'Proximity Card - IntelliM',
								'smart' => 'Smart Card',
								),
							'proximity' => 'Proximity Card - IntelliM'
							));
						echo $this->Form->input('site_code', array('value' => '1'));
						echo $this->Form->input('action_requested', array(
							'options' => array(
								'activate' => 'activate',
								'deactivate' => 'deactivate'),
							'selected' => $proximityCardEdit['ProximityCardEdit']['action_requested']
						));
						echo $this->Form->input('cause', array(
								'options' => array(
									'additional card for account' => 'additional card for account',
									'lost card' => 'lost card',
									'damaged card' => 'damaged card',
									'defective card' => 'defective card',
								),
								'selected' => $proximityCardEdit['ProximityCardEdit']['cause']
							)
						);
						echo $this->Form->input('accessLevels');
						echo $this->Form->input('notes');
					?>
				<?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
				<?= $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {

		if (_.isEmpty($('#persons_show').val())) {

			var account_id = $('#ProximityCardAccountId').val();
			$.getJSON("{{ operations_2g_urlprefix }}/proximityCards/lookup/"+account_id, function(d) {
				$('#persons_show').show();
				$('#ProximityCardPersonId').empty();
				_.each(d, function(v, k) {
					var person = <?= $proximityCardEdit['Customer']['id']?>;
					if (typeof person != 'undefined' && person == k) {
						$('#ProximityCardPersonId').append('<option selected="selected" value="'+k+'"">'+v+'</option>');
					} else {
						$('#ProximityCardPersonId').append('<option value="'+k+'"">'+v+'</option>');
					}

				});
			});
			

		}
	});
	$('#ProximityCardAccountId').change(function() {
		var account_id = $(this).val();
		$.getJSON("{{ operations_2g_urlprefix }}/proximityCards/lookup/"+account_id, function(d) {
			$('#persons_show').show();
			$('#ProximityCardPersonId').empty();			_.each(d, function(v, k) {
				$('#ProximityCardPersonId').append('<option value="'+k+'"">'+v+'</option>');
			});
		});
	});
</script>

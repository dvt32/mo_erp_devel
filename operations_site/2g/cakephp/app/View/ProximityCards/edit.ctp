<script type="text/javascript" src="{{ operations_2g_assetprefix }}/js/libs/tandem_select.js"></script>
<?= $this->Html->css('libs/tandem_select') ?>


<div class="proximityCards form">
<?= $this->Form->create('ProximityCard'); ?>
	<fieldset>
		<legend><?= __('Proximity Card'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('number', array('label' => 'Card Number'));
		echo $this->Form->hidden('type', array('value' => 'card'));
		echo $this->Form->input('type', array(
		    'options' => array(
		    	'card' => 'Card',
		    	'fob' => 'Key Fob',
		    ),
		    'card' => 'Card'
		));
		echo $this->Form->input('status', array(
		    'options' => array(
		    	'active' => 'Active',
		    	'inactive' => 'Inactive',
		    	'lost' => 'Lost'
		    ),
		    'active' => 'Active'
		));
		echo $this->Form->input('account_id', array(
			'options' => $customerAccounts,
			'empty' => '(Choose and Account and Person)'
		));

		echo $this->Form->input('person_id', array(
			'options' => $customers,
			'empty' => '(Choose Person)'
		));

		echo $this->Form->input('note');
		echo $this->Form->input('access_level_id');
		echo $this->Form->input('floor_id', array(
		    'options' => $floors
		));
		echo $this->Form->input('technology', array(
		    'options' => array(
		    	'proximity' => 'Proximity Card - IntelliM',
		    	'smart' => 'Smart Card',
		    ),
		    'proximity' => 'Proximity Card - IntelliM'
		));
		echo $this->Form->input('site_code', array('value' => '1'));
		echo $this->Form->input('group_intellim_id', array(
			'type' => 'hidden',
			'value' => implode(",", $this->request->data['ProximityCard']['group_intellim_id'])
		));
		?>
		<?php if ($this->request->data['ProximityCard']['technology'] == 'proximity') : ?>
		<!-- tandem_select jQuery plugin - html block start -->
		<div class="row-fluid">
		<div class='tandem-select-container span12'>
			<div class="row-fluid">
				<!-- List of groups -->
				<div class='tandem-select-src-div span4'>
				
					<?php
					echo $this->Form->input('intellim_groups', array(
						'options' => $groups,
						'multiple' => true,
						'label' => 'Available Groups',
						'class' => 'tandem-select-src-select'
					));
					?>
				</div>

				<div class='tandem-select-controls-div span4'>

					<p class="lead">Group Controls</p>
					<br/> <br/>

					<input type="button" class="btn btn-inverse tandem-select-move-to-src" value="&nbsp;&lt;&nbsp;Remove" />
				    &nbsp; &nbsp; 
					<input type="button" class="btn btn-inverse tandem-select-move-to-dst" value="Add&nbsp;&gt;&nbsp;" />	
					
					<br/> <br/>
				</div>


				<div class='tandem-select-dst-div span4'>

					<?php
					echo $this->Form->input('selected_groups', array(
						'options' => $selectedGroups,
						'multiple' => true,
						'label' => 'Group Membership',
						'class' => 'tandem-select-dst-select',
						'selected' => true
					));
					?>
					
				</div>
			</div>
		</div>
		<div style="clear: both;"></div>
		</div>
		<?php endif ?>
	</fieldset>
<?= $this->Form->button(__('Submit'), array(
'class' => 'btn btn-primary', 'data-loading-text' => 'Processing...', 'id' => 'submitForm')); ?>
<?= $this->Form->end(); ?>

</div>
<script type="text/javascript">
	
	$('#ProximityCardSelectedGroups').tandemSelect();

</script>

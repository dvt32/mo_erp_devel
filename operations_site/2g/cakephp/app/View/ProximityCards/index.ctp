<div class="proximityCards index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/shield.png', array('style' => 'height: 3em;')) ?>
		<?php echo __('Access Cards'); ?>
	</h2>
	<!-- search & filter forms -->
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				<legend>Filter &amp; Search</legend>
				<!-- filter and search -->
				<?= $this->Form->create('ProximityCard', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-inline'))) ?>
				<div class="span8">

					<?= $this->Form->input('q', array('placeholder' => 'Search', 'class' => 'input-xlarge search-query')) ?>
					<?= $this->Form->input('status', array(
						'options' => $statuses, 
						'empty' => '-- Status --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>
					<?= $this->Form->input('technology', array(
						'options' => $technologies, 
						'empty' => '-- Technology --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>
					<?= $this->Form->input('floor_id', array(
						'options' => $floors, 
						'empty' => '-- Floors --',
						'class' => 'input-auto',
						'style' => 'margin-bottom:0px;')
						) ?>

					<?= $this->Form->button('Search', array('class' => 'btn btn-success')) ?>
					<?= $this->Html->link('Clear', array('action' => 'index')) ?>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</div>
	</div>
	<?php echo $this->Html->link(__('New Access Card'), array('controller' => 'ProximityCards', 'action' => 'add')); ?>

	<!-- /search & filter forms -->
	<div class="row-fluid">
		<br>
		<table class="table table-hover table-condensed">
			<tr>
				<th><?php echo $this->Paginator->sort('account_id', 'Company'); ?></th>
				<th><?php echo $this->Paginator->sort('person_id', 'Person'); ?></th>
				<th><?php echo $this->Paginator->sort('status'); ?></th>
				<th><?php echo $this->Paginator->sort('number', 'Card Number'); ?></th>
				<th><?php echo $this->Paginator->sort('creation_dt', 'Created'); ?></th>
				<th><?php echo $this->Paginator->sort('access_level_id'); ?></th>
				<th><?php echo $this->Paginator->sort('floor_id', 'Building-Floor'); ?></th>
				<th><?php echo $this->Paginator->sort('technology'); ?></th>
				<th><?php echo $this->Paginator->sort('intellim_id'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php foreach ($proximityCards as $proximityCard): ?>
				<tr>
					<td>
						<?php echo $this->Html->link($proximityCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCard['CustomerAccount']['id'])); ?>
					</td>
					<td>
						<?php echo $this->Html->link($proximityCard['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $proximityCard['Customer']['id'])); ?>
					</td>
					<td><?php echo h($proximityCard['ProximityCard']['status']); ?>&nbsp;</td>
					<td><?php echo h($proximityCard['ProximityCard']['number']); ?>&nbsp;</td>
					<td><?php echo h($proximityCard['ProximityCard']['creation_dt']); ?>&nbsp;</td>
					<td>
						<?php echo $this->Html->link($proximityCard['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCard['AccessLevel']['id'])); ?>
					</td>
					<td>
						<?php echo $this->Html->link($proximityCard['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $proximityCard['Floor']['id'])); ?>
					</td>
					<td><?php echo h($proximityCard['ProximityCard']['technology']); ?>&nbsp;</td>
					<td><?php echo h($proximityCard['ProximityCard']['intellim_id']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link(__('View'), array('action' => 'view', $proximityCard['ProximityCard']['id'])); ?>
						<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $proximityCard['ProximityCard']['id'])); ?>
						<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $proximityCard['ProximityCard']['id']), null, __('Are you sure you want to delete # %s?', $proximityCard['ProximityCard']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<p>
			<?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?>
		</p>
		<div class="paging">
			<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
			?>
		</div>
	</div>
</div>
<script type="text/javascript">

	$("td.number, td.company, td.short_global_label").highlight(QueryString.q);

</script>


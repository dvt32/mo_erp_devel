<div class="proximityCards view">
<h2><?= __('Proximity Card') ?></h2>
	<dl>
		<dt><?= __('Id') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['id']) ?>&nbsp;</dd>
		<dt><?= __('Number') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['number']) ?>&nbsp;</dd>
		<dt><?= __('Type') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['type']) ?>&nbsp;</dd>
		<dt><?= __('Status') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['status']) ?>&nbsp;</dd>
		<dt><?= __('Customer Account') ?></dt>
		<dd><?= $this->Html->link($proximityCard['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $proximityCard['CustomerAccount']['id'])) ?>&nbsp;</dd>
		<dt><?= __('Customer') ?></dt>
		<dd><?= $this->Html->link($proximityCard['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $proximityCard['Customer']['id'])) ?>&nbsp;</dd>
		<dt><?= __('Note') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['note']) ?>&nbsp;</dd>
		<dt><?= __('Creation Dt') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['creation_dt']) ?>&nbsp;</dd>
		<dt><?= __('Access Level') ?></dt>
		<dd><?= $this->Html->link($proximityCard['AccessLevel']['name'], array('controller' => 'access_levels', 'action' => 'view', $proximityCard['AccessLevel']['id'])) ?>&nbsp;</dd>
		<dt><?= __('Floor') ?></dt>
		<dd><?= $this->Html->link($proximityCard['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $proximityCard['Floor']['id'])) ?>&nbsp;</dd>
		<dt><?= __('Technology') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['technology']) ?>&nbsp;</dd>
		<dt><?= __('Site Code') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['site_code']) ?>&nbsp;</dd>
		<dt><?= __('Intelli-M Card Number') ?></dt>
		<dd><?= h($proximityCard['ProximityCard']['intellim_id']) ?>&nbsp;</dd>
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul class="nav nav-pills">
		<li class="active"><?= $this->Html->link(__('Edit Card'), array('action' => 'edit', $proximityCard['ProximityCard']['id'])) ?> </li>
		<li class="active"><?= $this->Form->postLink(__('Deactivate Card'), array('action' => 'delete', $proximityCard['ProximityCard']['id']), null, __('Are you sure you want to deactivate this card?')) ?> </li>
		<li class="active"><?= $this->Form->postLink(__('Clear Card'), array('action' => 'clear', $proximityCard['ProximityCard']['id']), null, __('Are you sure you want to clear the person and account information from this card?')) ?> </li>
	</ul>
</div>

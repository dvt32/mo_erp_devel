<div class="row-fluid">
	<section class="module span6">
		<div class="module-head">
			<b>Customer Account Info</b>
		</div>
		<div class="module-body">
			<dl>
				<dt><?= __('Id') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['id']) ?>&nbsp;</dd>
				<dt><?= __('Name') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['name']) ?>&nbsp;</dd>
				<dt><?= __('Floor') ?></dt>
				<dd><?= $customerAccount['Floor']['short_global_label'] ?>&nbsp;</dd>
				<dt><?= __('Primary Customer') ?></dt>
				<dd><?= $this->Html->link($customerAccount['PrimaryCustomer']['full_name'], array('controller' => 'customers', 'action' => 'view', $customerAccount['PrimaryCustomer']['id'])) ?>&nbsp;</dd>
				<dt><?= __('Is Active') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['is_active'] ? 'yes' : 'no') ?>&nbsp;</dd>
				<dt><?= __('Description') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['description']) ?>&nbsp;</dd>
				<dt><?= __('Type') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['type']) ?>&nbsp;</dd>
				<dt><?= __('Note') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['note']) ?>&nbsp;</dd>
				<dt><?= __('Service') ?></dt>
				<dd><?= $customerAccount['Service']['name'] ?>&nbsp;</dd>
				<dt><?= __('Preferred Payment Method') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['preferred_payment_method']) ?>&nbsp;</dd>
				<dt><?= __('Room Minutes') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['room_minutes']) ?>&nbsp;</dd>
<!-- 
				<dt><?= __('Status') ?></dt>
				<dd><?= h($customerAccount['CustomerAccount']['status']) ?>&nbsp;</dd>
 -->				
			</dl>
		</div>
	</section>
	<div class="span6">
		<section class="module row-fluid">
			<div class="module-head">
				<b>Customer Balance Information</b>
			</div>
			<div class="module-body">
				<dl>
					<dt>Balance Owed</dt>
					<dd><?= __(@$customerAccount['CustomerAccount']['balance']) ?>&nbsp;</dd>
					<dt>Projected Reveneue</dt>
					<dd><?= __(@$customerAccount['CustomerAccount']['projected_revenue']) ?>&nbsp;</dd>
					<dt>QuickBooks Name</dt>
					<dd><?= __(@$customerAccount['CustomerAccount']['qb_name']) ?>&nbsp;</dd>
				</dl>
			</div>
		</section>
	</div>
</div>

<!-- billing amounts -->
<!-- 
<div class="row-fluid">
	<section class="module span12">
		<div class="module-head"><b>Billing Amount</b></div>
		<div class="module-body">
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="left shrink">Due Date</th>
						<th class="shrink nowrap">Stage</th>
						<th class="right shrink">Balance</th>
						<th class="right shrink nowrap">Total</th>
						<th class="right shrink nowrap">Recurring</th>
						<th class="left shrink">Days Overdue</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($customerAccount['BillingAmount'] as $billingAmount): ?>
					<tr>
						<td class="left shrink"><?= h($billingAmount['due_date']) ?>&nbsp;</td>
						<td class="shrink"><?= $this->Html->link($billingAmount['stage'], array('controller' => 'billingMemos', 'action' => 'aging', $billingAmount['id'])) ?></td>
						<td class="right shrink"><?= h($billingAmount['balance']) ?>&nbsp;</td>
						<td class="right shrink muted"><?= h($billingAmount['grand_total']) ?>&nbsp;</td>
						<td class="right shrink muted"><?= h($billingAmount['recurring_amount']) ?>&nbsp;</td>
						<td class="left shrink"><?= __("%d days", $billingAmount['days_overdue']) ?></td>
					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</section>
</div>
 -->

<div class="row-fluid">
	<section class="module span12">
		<div class="module-head"><b>Add Received Payment</b></div>
		<div class="module-body">
			<?= $this->Form->create('ReceivedPayment', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
				<div class="row-fluid">
					<div class="span6">
						<?=$this->Form->hidden('cust_acct_id', array('type' => 'text', 'value' => $this->request->pass[0])) ?>
						<?=$this->Form->hidden('employee_id', array('value' => AuthComponent::user('id'))) ?>

						<label>Amount</label>
						<div class="input-prepend">
							<span class="add-on">$</span>
							<?=$this->Form->input('amount', array('label' => false, 'div' => false)) ?>
						</div>
						<?=$this->Form->input('recv_dt', array('type' => 'text', 'class' => 'datetime', 'label' => 'Received', 'value' => date(DATETIME_FORMAT))); ?>
						<?=$this->Form->input('process_txn_after_dt', array('type' => 'text', 'class' => 'datetime', 'label' => 'Check Date', 'value' => date(DATETIME_FORMAT))); ?>
						<?=$this->Form->input('payment_method', array('options' => $paymentMethods)); ?>
						<?=$this->Form->input('invoice_number'); ?>
						<?=$this->Form->input('notify_email', array('value' => $customerAccount['PrimaryCustomer']['email'])) ?>
					</div>
					<div class="span6">
						<?=$this->Form->input('check_name') ?>
						<?=$this->Form->input('check_number') ?>
						<?=$this->Form->input('note', array('rows' => 2)) ?>
						<?=$this->Form->input('payment_destination') ?>
						<?= $this->Form->button('Save Received Payment', array('class' => 'btn btn-primary btn-large pull-right')) ?>
					</div>
				</div>
			<?= $this->Form->end() ?>
		</div>
	</section>
</div>

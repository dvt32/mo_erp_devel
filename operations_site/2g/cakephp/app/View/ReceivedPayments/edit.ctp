<div class="receivedPayments form">
<?php echo $this->Form->create('ReceivedPayment'); ?>
	<fieldset>
		<legend><?php echo __('Edit Received Payment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('cust_acct_id');
		echo $this->Form->input('employee_id');
		echo $this->Form->input('amount');
		echo $this->Form->input('recv_dt');
		echo $this->Form->input('payment_method');
		echo $this->Form->input('process_txn_after_dt');
		echo $this->Form->input('invoice_number');
		echo $this->Form->input('note');
		echo $this->Form->input('check_name');
		echo $this->Form->input('check_number');
		echo $this->Form->input('bank_account_name');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ReceivedPayment.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ReceivedPayment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Received Payments'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Deposits'), array('controller' => 'deposits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Deposit'), array('controller' => 'deposits', 'action' => 'add')); ?> </li>
	</ul>
</div>

<!-- TODO breadcrumbs -->
<h2><?= __('Received Payments'); ?></h2>
<?= $this->Html->link('Add Payment', array('controller' => 'customerAccounts', 'action' => 'receiving'), array('class' => 'btn btn-primary')) ?>


<div class="receivedPayments index">
	<table class="table table-striped table-condensed">
	<tr>
			<th><?= $this->Paginator->sort('cust_acct_id', 'Customer'); ?></th>
			<th><?= $this->Paginator->sort('employee_id'); ?></th>
			<th><?= $this->Paginator->sort('amount'); ?></th>
			<th><?= $this->Paginator->sort('recv_dt', 'Received'); ?></th>
			<th><?= $this->Paginator->sort('payment_method', 'Method'); ?></th>
			<th><?= $this->Paginator->sort('process_mode', 'Mode'); ?></th>
			<th><?= $this->Paginator->sort('process_status', 'Status'); ?></th>
			<th><?= $this->Paginator->sort('payment_destination', 'Destination'); ?></th>
			<th><?= $this->Paginator->sort('deposit_id'); ?></th>
			<th class="actions">&nbsp;</th>
	</tr>
	<?php foreach ($receivedPayments as $receivedPayment): ?>
	<tr>
		<td> <?= $this->Html->link($receivedPayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $receivedPayment['CustomerAccount']['id'])); ?> </td>
		<td> <?= $this->Html->link($receivedPayment['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $receivedPayment['Employee']['id'])); ?> </td>
		<td><?= h($receivedPayment['ReceivedPayment']['amount']); ?>&nbsp;</td>
		<td><?= h($receivedPayment['ReceivedPayment']['recv_dt']); ?>&nbsp;</td>
		<td><?= h($receivedPayment['ReceivedPayment']['payment_method']); ?>&nbsp;</td>
		<td><?= h($receivedPayment['ReceivedPayment']['process_mode']); ?>&nbsp;</td>
		<td><?= h($receivedPayment['ReceivedPayment']['process_status']); ?>&nbsp;</td>
		<td><?= h($receivedPayment['ReceivedPayment']['payment_destination']); ?>&nbsp;</td>
		<td> <?= $this->Html->link($receivedPayment['Deposit']['id'], array('controller' => 'deposits', 'action' => 'view', $receivedPayment['Deposit']['id'])); ?> </td>
		<td class="actions">
			<?= $this->Html->link(__('View'), array('action' => 'view', $receivedPayment['ReceivedPayment']['id'])); ?>
			<?= $this->Form->postLink(
				__('Cancel'),
				array('action' => 'cancel', $receivedPayment['ReceivedPayment']['id'], '?' => array('next' => Router::url())),
				null,
				__('Are you sure you want to cancel # %s?', $receivedPayment['ReceivedPayment']['id'])
		); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?= $this->element('paging') ?>
</div>

<ul class="breadcrumb">
<li><a href="{{ operations_2g_urlprefix }}/">Dashboard</a> <span class="divider">/</span></li>
<li><a href="{{ operations_2g_urlprefix }}/receivedPayments/index">Received Payments</a> <span class="divider">/</span></li>
<li class="active"><?= $this->Html->link($receivedPayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $receivedPayment['CustomerAccount']['id'])); ?></li>
</ul>
<div class="receivedPayments view">
<?= $this->Form->postLink(__('Cancel Received Payment'), array('action' => 'cancel', $receivedPayment['ReceivedPayment']['id']), array('class' => 'pull-right btn btn-danger'), 'Are you sure you want to cancel this payment?'); ?>
<h2><?= __('Received Payment'); ?></h2>
	<dl>
		<dt><?= __('Customer Account'); ?></dt>
		<dd> <?= $this->Html->link($receivedPayment['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $receivedPayment['CustomerAccount']['id'])); ?> &nbsp; </dd>
		<dt><?= __('Employee'); ?></dt>
		<dd> <?= $this->Html->link($receivedPayment['Employee']['full_name'], array('controller' => 'employees', 'action' => 'view', $receivedPayment['Employee']['id'])); ?> &nbsp; </dd>
		<dt><?= __('Amount'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['amount']); ?> &nbsp; </dd>
		<dt><?= __('Recv Dt'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['recv_dt']); ?> &nbsp; </dd>
		<dt><?= __('Payment Method'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['payment_method']); ?> &nbsp; </dd>
		<dt><?= __('Process Txn After Dt'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['process_txn_after_dt']); ?> &nbsp; </dd>
		<dt><?= __('Process Mode'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['process_mode']); ?> &nbsp; </dd>
		<dt><?= __('Process Status'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['process_status']); ?> &nbsp; </dd>
		<dt><?= __('Notify Email'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['notify_email']); ?> &nbsp; </dd>
		<dt><?= __('Client Hostname'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['client_hostname']); ?> &nbsp; </dd>
		<dt><?= __('Client Ip Addr'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['client_ip_addr']); ?> &nbsp; </dd>
		<dt><?= __('Invoice Number'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['invoice_number']); ?> &nbsp; </dd>
		<dt><?= __('Note'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['note']); ?> &nbsp; </dd>
		<dt><?= __('Is Reconciled'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['is_reconciled']); ?> &nbsp; </dd>
		<dt><?= __('Payment Destination'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['payment_destination']); ?> &nbsp; </dd>
		<dt><?= __('Check Name'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['check_name']); ?> &nbsp; </dd>
		<dt><?= __('Check Number'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['check_number']); ?> &nbsp; </dd>
		<dt><?= __('Bank Account Name'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['bank_account_name']); ?> &nbsp; </dd>
		<dt><?= __('Bank Account Type'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['bank_account_type']); ?> &nbsp; </dd>
		<dt><?= __('Bank Routing Number'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['bank_routing_number']); ?> &nbsp; </dd>
		<dt><?= __('Bank Account Number'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['bank_account_number']); ?> &nbsp; </dd>
		<dt><?= __('Card Number'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_number']); ?> &nbsp; </dd>
		<dt><?= __('Card Expiration Month'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_expiration_month']); ?> &nbsp; </dd>
		<dt><?= __('Card Expiration Year'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_expiration_year']); ?> &nbsp; </dd>
		<dt><?= __('Card Security Code'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_security_code']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Name'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_name']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Company'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_company']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Address Num'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_address_num']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Zip Code'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_zip_code']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Address 1'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_address_1']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Address 2'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_address_2']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill City'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_city']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill State'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_state']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Country'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_country']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Phone Number'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_phone_number']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Fax Number'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_fax_number']); ?> &nbsp; </dd>
		<dt><?= __('Card Bill Email Address'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['card_bill_email_address']); ?> &nbsp; </dd>
		<dt><?= __('Ms Access Creation Dt'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['ms_access_creation_dt']); ?> &nbsp; </dd>
		<dt><?= __('Ms Access Random Num'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['ms_access_random_num']); ?> &nbsp; </dd>
		<dt><?= __('Creation Dt'); ?></dt>
		<dd> <?= h($receivedPayment['ReceivedPayment']['creation_dt']); ?> &nbsp; </dd>
		<dt><?= __('Deposit'); ?></dt>
		<dd> <?= $this->Html->link($receivedPayment['Deposit']['deposit_dt'], array('controller' => 'deposits', 'action' => 'view', $receivedPayment['Deposit']['id'])); ?> &nbsp; </dd>
	</dl>
</div>

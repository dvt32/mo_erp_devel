<div class="reservations form">
<?= $this->Form->create('Reservation') ?>
	<fieldset>
		<legend><?= __('Add Reservation') ?></legend>
		<?= $this->Form->input('Reservation.room_id') ?>
		<?= $this->Form->input('Reservation.customer_account_id') ?>
		<?= $this->Form->input('Reservation.start_dt', array('type' => 'text')) ?>
		<?= $this->Form->input('Reservation.end_dt', array('type' => 'text')) ?>
		<?= $this->Form->input('Reservation.note') ?>
	</fieldset>
<?= $this->Form->end(__('Submit')) ?>
</div>
<div class="actions">
	<h3><?= __('Actions') ?></h3>
	<ul>

		<li><?= $this->Html->link(__('List Reservations'), array('action' => 'index')) ?></li>
		<li><?= $this->Html->link(__('List Rooms'), array('controller' => 'rooms', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Room'), array('controller' => 'rooms', 'action' => 'add')) ?> </li>
		<li><?= $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')) ?> </li>
		<li><?= $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')) ?> </li>
	</ul>
</div>

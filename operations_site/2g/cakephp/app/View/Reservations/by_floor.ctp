<?php
// populate durations dropdown
$durations = array_self_combine(range(15,180,15));
array_walk($durations,function(&$duration) {
	$h = (int) ($duration / 60);
	$m = (int) ($duration % 60);
	$duration = sprintf("%d hours, %02d mins",$h,$m);
});

$datetime = DateTime::createFromFormat('Y-m-d', $date);
$datetime_clone = clone $datetime;
$disabled = strcmp(date('Y-m-d'),$datetime->format('Y-m-d')) > 0;
// $cust_acct_id = AuthComponent::user('cust_acct_id');
?>

<div class="row-fluid" style="margin-bottom:1.5em;">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/clock.png' , array('style' => 'height:3em;')) ?>
		<?php echo __('Reservations'); ?>
	</h2>
	<a href="{{ operations_2g_urlprefix }}/reservations/schedule" class="btn btn-info">Modern Calendar</a>
	<h3><?= "{$floor}" ?></h3>
	<span class="big"><?= $datetime->format('F j, Y') ?></span>
	<br />
	<?= $this->Html->link('<<',    array('action' => 'byFloor', $floor_id, $datetime_clone->modify('-1 day')->format('Y-m-d'))) ?>
	<?= $this->Html->link('Today', array('action' => 'byFloor', $floor_id, date('Y-m-d'))) ?>
	<?= $this->Html->link('>>',    array('action' => 'byFloor', $floor_id, $datetime_clone->modify('+2 day')->format('Y-m-d'))) ?>
</div>
<div class="row-fluid">
	<?= $this->Session->flash() ?>
	<table id="RoomReservations" class="text-center" style="width:100%">
		<thead>
			<tr>
				<th>Time Slot</th>
				<?php foreach ($rooms as $room): ?>
					<th><?= $room ?></th>
				<?php endforeach ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($slots as $time => $slot): ?>
				<tr>
					<th style="width:0; padding-right:1em;"><?= date('g:ia', $time) ?></th>
					<?php foreach ($rooms as $room_id => $room): ?>
						<?php $reservation = @$slot[$room_id] ?>
						<?php $hour = date('h',$time); $min = date('i',$time); $mer = date('a',$time) ?>
					<?php if (!empty($reservation)): ?>
						<?php if ($reservation['Reservation']['is_start']): ?>
							<td class="booked" style="border-top:#70803F 2px solid;">
							<?// var_dump($reservation['Reservation']['account_id']) ?>
							<?php $duration = $reservation['Reservation']['duration'] ?>
								<?= $this->Html->link(
										$reservation['CustomerAccount']['name'],
										'#ReservationBookForm',
										array(
											'class' => 'autofill',
											'data-id' => $reservation['Reservation']['id'],
											'data-hour' => $hour,
											'data-min' => $min,
											'data-mer' => $mer,
											'data-room' => $room_id,
											'data-dur' => $duration
										)
									)
								?>
								<?=
								$this->Form->postLink(
									__(' &times '),
									array('action' => 'remove', $reservation['Reservation']['id'], '?' => ['next' => $this->Html->url(array('action' => 'byFloor', $floor_id, date('Y-m-d')), true)]),
									array('escape' => false, 'class' =>'pull-right', 'style' => 'width:1.5em;background:#A4BA5D'),
									__('Are you sure you want to delete your %s reservation?', date('g:ia',$time))
								)
								?>
							</td>
						<?php else: ?>
							<td class="booked">&nbsp;</td>
						<?php endif ?>
						<?php else: ?>
							<td class="avail">
								<?php if ($time > time()): ?>
									<?= $this->Html->link('reserve', '#ReservationBookForm',
										array(
											'class' => 'autofill',
											'data-id' => '',
											'data-hour' => $hour,
											'data-min' => $min,
											'data-mer' => $mer,
											'data-room' => $room_id
										)
									)	 ?>
								<?php else: ?>
									<a>time has passed</a>
								<?php endif ?>
							</td>
						<?php endif ?>
					<?php endforeach ?>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<div class="row-fluid" style="margin-top:2em;">
	<div class="span2">
		<section class="module">
			<div class="module-body">
				<ul class="nav nav-pills nav-stacked">
					<?php foreach ($floors as $fid => $floor_label): ?>
						<li class="<?= $fid == $floor_id ? 'active' : '' ?>"><?= $this->Html->link($floor_label, array('action' => 'byFloor', $fid, $datetime->format('Y-m-d'))) ?></li>
					<?php endforeach ?>
				</ul>
			</div>
		</section>
	</div>
	<div class="span5">
		<section class="module" id="ReserveRoomSection">
			<div class="module-head"><b>Reserve a room</b></div>
			<div class="module-body">
				<?= $this->Form->create('Reservation', array('inputDefaults' => array('class' => 'input-block-level', 'disabled' => $disabled))) ?>
					<?php if ($disabled): ?>
						<div class="row-fluid">
							<div class="alert alert-error">
								<strong>Sorry!</strong> You cannot reserve a room in the past
							</div>
						</div>
					<?php endif ?>
					<div class="row-fluid">
						<div class="span12"><?= $this->Form->input('Reservation.room_id', array('empty' => '-- room --')) ?></div>
						<div class="span12"><?= $this->Form->input('Reservation.account_id', array('options' => $customerAccount, 'empty' => '-- Account --')) ?></div>
					</div>
					<div class="row-fluid">
						<div class="span5">
							<div class="datetime">
								<?= $this->Form->hidden('Reservation.start_date', array('value' => $date, 'div' => false)) ?>
								<?= $this->Form->input('Reservation.start_time', array('type' => 'time', 'div' => false, 'interval' => '15')) ?>
							</div>
							<?= $this->Form->input('Reservation.duration', array('options' => $durations)) ?>
						</div>
						<div class="span7">
							<?= $this->Form->input('Reservation.note', array('rows' => 4)) ?>
						</div>
					</div>
					<?= $this->Form->hidden('Reservation.end_date', array('value' => $date)) ?>
					<?= $this->Form->hidden('Reservation.id') ?>
					<?= $this->Form->button('Reserve Room', array('class' => 'btn btn-primary', 'disabled' => $disabled)) ?>
					<?= $this->Form->button('Clear', array('class' => 'btn-link pull-right', 'type' => 'reset')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>
	<div class="span5">
		<section class="module">
			<div class="module-head"><b>View Another Day</b></div>
			<div class="module-body">
				<div class="text-center">
					<?= $this->Html->link('Today', array('action' => 'byFloor', $floor_id, date('Y-m-d'))) ?>
				</div>
				<div class="row-fluid">
					<?php $linkFormat = Router::url(array('controller' => $this->request->controller, 'action' => $this->request->action, 'full_base' => true, $floor_id)) . '/%s'; ?>
					<div class="span4"><?= $this->element('mini-calendar', array('linkFormat' => $linkFormat, 'month' => $datetime->format('m')-1)) ?></div>
					<div class="span4"><?= $this->element('mini-calendar', array('linkFormat' => $linkFormat, 'month' => $datetime->format('m')+0, 'current' => $datetime->format('U'))) ?></div>
					<div class="span4"><?= $this->element('mini-calendar', array('linkFormat' => $linkFormat, 'month' => $datetime->format('m')+1)) ?></div>
				</div>
			</div>
		</section>
	</div>
</div>

<?= $this->Html->script('libs/chosen/chosen.jquery.min') ?>
<?= $this->Html->css('libs/chosen/chosen.css') ?>
<script>
$(".chosen").chosen();
</script>
<style>
	tbody td { border:1px #eee solid; }
	td.booked {
		font-size:small;
		background:#c6e170;
		border-bottom:1px #c6e170 solid;
	}
	div.datetime select { width:auto!important; display:inline-block!important; }
	td.avail:hover { background:#eee; }
	td.avail a { display:block; height:100%; color:transparent; }
	td.avail a:hover { color:rgb(0, 136, 204); cursor:pointer; }
</style>
<script>
// autofill form fields when a reservation timeslot is clicked
$(".autofill").click(function() {
	$("#ReserveRoomSection").hide().fadeIn('slow');
	$("#ReservationId").val($(this).attr("data-id"));
	$("#ReservationStartTimeHour").val($(this).attr("data-hour"));
	$("#ReservationStartTimeMin").val($(this).attr("data-min"));
	$("#ReservationStartTimeMeridian").val($(this).attr("data-mer"));
	$("#ReservationDuration").val($(this).attr("data-dur"));
	$("#ReservationRoomId").val($(this).attr("data-room"));
});
</script>

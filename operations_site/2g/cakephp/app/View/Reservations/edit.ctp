<div class="reservations form">
<?php echo $this->Form->create('Reservation'); ?>
	<fieldset>
		<legend><?php echo __('Edit Reservation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('room_id');
		echo $this->Form->input('start_dt', array('type' => 'text', 'class' => 'datetime', 'readonly'));
		echo $this->Form->input('end_dt', array('type' => 'text', 'class' => 'datetime', 'readonly'));
		echo $this->Form->input('note');
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

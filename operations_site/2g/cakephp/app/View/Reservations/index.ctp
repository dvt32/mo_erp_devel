<div class="row-fluid">
	<!-- TODO implement filter sort and search -->
</div>
<div class="row-fluid">
	<h2><?= __('Reservations') ?></h2>
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('room_id') ?></th>
				<th><?= $this->Paginator->sort('customer_account_id') ?></th>
				<th><?= $this->Paginator->sort('created') ?></th>
				<th><?= $this->Paginator->sort('start') ?></th>
				<th><?= $this->Paginator->sort('end') ?></th>
				<th><?= $this->Paginator->sort('note') ?></th>
				<th class="actions"><?= __('Actions') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($reservations as $reservation): ?>
				<tr>
					<td><?= $this->Html->link($reservation['Room']['name'], array('controller' => 'reservations', 'action' => 'byFloor', $reservation['Room']['id'])) ?></td>
					<td><?= $this->Html->link($reservation['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $reservation['CustomerAccount']['id'])) ?></td>
					<td><?= h($reservation['Reservation']['creation_dt']) ?>&nbsp;</td>
					<td><?= h($reservation['Reservation']['start_dt']) ?>&nbsp;</td>
					<td><?= h($reservation['Reservation']['end_dt']) ?>&nbsp;</td>
					<td><?= h($reservation['Reservation']['note']) ?>&nbsp;</td>
					<td class="actions">
						<?= $this->Html->link(__('View'), array('action' => 'view', $reservation['Reservation']['id'])) ?>
						<?= $this->Html->link(__('Edit'), array('action' => 'edit', $reservation['Reservation']['id'])) ?>
						<?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $reservation['Reservation']['id']), null, __('Are you sure you want to delete # %s?', $reservation['Reservation']['id'])) ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<p><?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )); ?>	</p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>

<!-- calendar -->
<link href='/js/libs/calendar/fullcalendar.css' rel='stylesheet' />
<link href='/js/libs/calendar/fullcalendar.print.css' rel='stylesheet' />
<script src='/js/libs/calendar/jquery-ui.js'></script>
<script src='/js/libs/calendar/fullcalendar.min.js'></script>
<!-- /calendar -->

<style>

	#calendar_reservations {
		width: 100%;
	}

</style>

<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<div class="span12 centered">
				<h2>
					<?= $this->Html->image('icons/flat_miniset/png/256/clock.png' , array('style' => 'height:3em;')) ?>
					<?php echo __('Reservations'); ?>
				</h2>
			</div>
		</div>
		<div class="row-fluid">
			<!-- calendar -->
			<div class="span12 centered">
				<h1>Calendar</h1>
				<div class="row-fluid">
					<div class="span12">
						<table id="reserve" class="table table-grid">
							<thead>
								<?php foreach ($rooms as $room) : ?>
								<th><?= $room ?></th>
								<?php endforeach ?>
							</thead>
							<tbody>
							
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /calendar -->

		</div>
	</div>
</div>
<script>
		
		// DECLARE GLOBAL VARIABLES
		var evt = [];
		var reservations;
		var myCompany;
		var buildingID = 2;
		var crayola;


		$.getJSON("{{ operations_2g_urlprefix }}/js/libs/crayola.json", function(d) {
			crayola = d;
		});

		// LOAD DATA
		$.getJSON("{{ operations_2g_urlprefix }}/reservations/day/"+buildingID+".json", function(d) {

			_.each(d.day, function(num, key) {
				
				var res = [];
				res = {
					title: num.CustomerAccount.name,
					start: num.Reservation.start_dt,
					end: num.Reservation.end_dt,
					start_nice: num.Reservation.start_nice,
					end_nice: num.Reservation.end_nice,
					roomID: num.Reservation.room_id,
					allDay: false,
					note: num.Reservation.note,
					color: '#072740',
					editable: true,
					disableResizing: false,
					resid: num.Reservation.id
					// url: "/reservations/view/"+num.Reservation.id
				};
				evt.push(res);
			});


			$('#reserve').append(function() {

			});

		});


		

</script>

<!-- calendar -->
<link href='/js/libs/calendar/fullcalendar.css' rel='stylesheet' />
<link href='/js/libs/calendar/fullcalendar.print.css' rel='stylesheet' />
<script src='/js/libs/calendar/jquery-ui.js'></script>
<script src='/js/libs/calendar/fullcalendar.min.js'></script>
<!-- /calendar -->

<style>

	#calendar_reservations {
		width: 100%;
	}
</style>

<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<div class="span12 centered">
				<h2>
					<?= $this->Html->image('icons/flat_miniset/png/256/clock.png' , array('style' => 'height:3em;')) ?>
					<?php echo __('Reservations'); ?>
				</h2>
			</div>
		</div>
		<div class="row-fluid">
			<!-- calendar -->
			<div class="span4 centered">
				<h1>Calendar</h1>
				<div class="row-fluid">
					<div class="span12">
						<div id='reservation_cal_1'></div>	
					</div>
				</div>
			</div>
			<!-- /calendar -->

			<!-- calendar -->
			<div class="span4 centered">
				<h1>Calendar</h1>
				<div class="row-fluid">
					<div class="span12">
						<div id='reservation_cal_2'></div>	
					</div>
				</div>
			</div>
			<!-- /calendar -->

			<!-- calendar -->
			<div class="span4 centered">
				<h1>Calendar</h1>
				<div class="row-fluid">
					<div class="span12">
						<div id='reservation_cal_3'></div>	
					</div>
				</div>
			</div>
			<!-- /calendar -->
		</div>
	</div>
</div>
<script>
		
		// DECLARE GLOBAL VARIABLES
		var evt = [];
		var reservations;
		var myCompany;
		var buildingID = 2;
		var crayola;
		var rooms = [];
		var urooms = [];
		var res_room = new Object();

		$.getJSON("{{ operations_2g_urlprefix }}/js/libs/crayola.json", function(d) {
			crayola = d;
		});

		// function roomColor(room) {

		// 	if (rooms.Contains(room)) {
		// 		return room.colour;
		// 	} else {
		// 		rooms.push("id");
		// 		rooms.id = room;
		// 		rooms.push("colour");
		// 		rooms.colour = crayola[_.random(1,300)].hex;
		// 	}
			
		// }

		// LOAD DATA
		$.getJSON("{{ operations_2g_urlprefix }}/reservations/day/"+buildingID+".json", function(d) {

			_.each(d.day, function(num, key) {
				
				var res = [];
				res = {
					title: num.CustomerAccount.name,
					start: num.Reservation.start_dt,
					end: num.Reservation.end_dt,
					start_nice: num.Reservation.start_nice,
					end_nice: num.Reservation.end_nice,
					roomID: num.Reservation.room_id,
					allDay: false,
					note: num.Reservation.note,
					color: '#072740',
					editable: true,
					disableResizing: false,
					resid: num.Reservation.id
					// url: "/reservations/view/"+num.Reservation.id
				};
				evt.push(res);
			});

			// get array of unique rooms
			_.each(evt, function(num, key) {
				urooms.push(num.roomID);
			});
			urooms = _.unique(urooms);

			// check if unique room exists


			// get seperate arrays of each room
			_.each(evt, function(val, kei) {

				if (!_.has(res_room, val.roomID)) {

					var rm_id = val.roomID;

					// create a new key with the room ID
					res_room.roomID = [];
					res_room.roomID.push(rm_id);
					// push res object into the key
					// res_room.roomID
					// res_room.roomID.rm_id.push(val);
				}

			});

			// LOAD CALENDAR
			var calendar_reservations = $('#reservation_cal_1').fullCalendar({
				header: {
					left: 'prev',
					center: 'title',
					right: 'next'
				},
				selectable: true,
				selectHelper: true,
				timeFormat: 'h:mm TT',
				slotMinutes: 15,
				allDaySlot:false,
				firstHour: '7',
				defaultView: 'agendaDay',
				events: evt
			});

		});


		

</script>

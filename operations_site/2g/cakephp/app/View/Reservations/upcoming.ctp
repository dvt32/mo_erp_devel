
<div class="reservations index">
	<h2><?php echo __('Reservations'); ?></h2>
	<div class="actions">
		<?= $this->Html->link('Schedule & Reservations', array('controller' => 'reservations', 'action' => 'schedule')) ?>
	</div>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('room_id'); ?></th>
			<th><?php echo $this->Paginator->sort('start_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('end_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th class="actions">&nbsp;</th>
	</tr>
	<?php foreach ($reservations as $reservation): ?>
		<tr>
			<td> <?= "{$reservation['Room']['name']} ({$reservation['Room']['Floor']['label']})" ?> </td>
			<td><?php echo CakeTime::nice($reservation['Reservation']['start_dt']); ?>&nbsp;</td>
			<td><?php echo CakeTime::nice($reservation['Reservation']['end_dt']); ?>&nbsp;</td>
			<td><?php echo h($reservation['Reservation']['note']); ?>&nbsp;</td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('action' => 'view', $reservation['Reservation']['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $reservation['Reservation']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>


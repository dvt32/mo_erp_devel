<div class="rooms view">
<h2><?php echo __('Room'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($room['Room']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($room['Room']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Name'); ?></dt>
		<dd>
			<?php echo h($room['Room']['short_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($room['Room']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($room['Room']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($room['Room']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($room['Room']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($room['Room']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Floor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($room['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $room['Floor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Shared Floor'); ?></dt>
		<dd>
			<?php echo h($room['Room']['is_shared_floor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Room Type'); ?></dt>
		<dd>
			<?php echo h($room['Room']['room_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($room['Room']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Room'), array('action' => 'edit', $room['Room']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Room'), array('action' => 'delete', $room['Room']['id']), null, __('Are you sure you want to delete # %s?', $room['Room']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Rooms'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Room'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reservations'), array('controller' => 'reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reservation'), array('controller' => 'reservations', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Reservations'); ?></h3>
	<?php if (!empty($room['Reservation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Room Id'); ?></th>
		<th><?php echo __('Customer Account Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Ms Access Creation Dt'); ?></th>
		<th><?php echo __('Ms Access Random Num'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($room['Reservation'] as $reservation): ?>
		<tr>
			<td><?php echo $reservation['id']; ?></td>
			<td><?php echo $reservation['room_id']; ?></td>
			<td><?php echo $reservation['customer_account_id']; ?></td>
			<td><?php echo $reservation['created']; ?></td>
			<td><?php echo $reservation['end']; ?></td>
			<td><?php echo $reservation['note']; ?></td>
			<td><?php echo $reservation['ms_access_creation_dt']; ?></td>
			<td><?php echo $reservation['ms_access_random_num']; ?></td>
			<td><?php echo $reservation['start']; ?></td>
			<td><?php echo $reservation['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'reservations', 'action' => 'view', $reservation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'reservations', 'action' => 'edit', $reservation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'reservations', 'action' => 'delete', $reservation['id']), null, __('Are you sure you want to delete # %s?', $reservation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Reservation'), array('controller' => 'reservations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

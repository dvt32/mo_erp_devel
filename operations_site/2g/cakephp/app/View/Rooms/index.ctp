<div class="rooms index">
	<h2><?php echo __('Conference Rooms'); ?></h2>
	<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<div class="span3">
							<?php echo $this->Html->link(__('New Mailbox'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?>
						</div>
						<div class="span3">
							<div class="btn-group">
								<a class="btn btn-info" href="#">Button</a>
								<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>
						</div>
						<div class="span3">
							<?= $this->Form->create('Mailbox', array('type' => 'get', 'class' => 'form-search')) ?>
								<div class="input-append">
									<input name="q" placeholder="Search" type="text" id="ProximityCardQ" class="span12 search-query" style="min-height:40px!important;" >
									<button class="btn btn-success" type="submit">Search</button>
								</div>
							<?= $this->Form->end() ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<table class="table table-striped table-condensed">
			<tr>
				<th><?php echo $this->Paginator->sort('name'); ?></th>
				<th><?php echo $this->Paginator->sort('short_name'); ?></th>
				<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
				<th><?php echo $this->Paginator->sort('is_active'); ?></th>
				<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
				<th><?php echo $this->Paginator->sort('is_shared_floor'); ?></th>
				<th><?php echo $this->Paginator->sort('room_type'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php foreach ($rooms as $room): ?>
			<tr>
				<td><?php echo h($room['Room']['name']); ?>&nbsp;</td>
				<td><?php echo h($room['Room']['short_name']); ?>&nbsp;</td>
				<td><?php echo h($room['Room']['creation_dt']); ?>&nbsp;</td>
				<td><?php echo h($room['Room']['is_active']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($room['Floor']['id'], array('controller' => 'floors', 'action' => 'view', $room['Floor']['id'])); ?>
				</td>
				<td><?php echo h($room['Room']['is_shared_floor']); ?>&nbsp;</td>
				<td><?php echo h($room['Room']['room_type']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $room['Room']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $room['Room']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $room['Room']['id']), null, __('Are you sure you want to delete # %s?', $room['Room']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<div class="paging">
				<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
			</div>
		</div>
		<div class="actions">
			<h3><?php echo __('Actions'); ?></h3>
			<ul>
				<li><?php echo $this->Html->link(__('New Room'), array('action' => 'add')); ?></li>
				<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
				<li><?php echo $this->Html->link(__('List Notifications'), array('controller' => 'notifications', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Notification'), array('controller' => 'notifications', 'action' => 'add')); ?> </li>
				<li><?php echo $this->Html->link(__('List Reservations'), array('controller' => 'reservations', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Reservation'), array('controller' => 'reservations', 'action' => 'add')); ?> </li>
			</ul>
		</div>

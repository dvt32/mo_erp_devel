<div class="row-fluid">

	<?= $this->Html->link(
		'&larr; Back to Prospective Customer',
		array('controller' => 'prospects', 'action' => 'edit', $salesInteraction['SalesInteraction']['prospective_customer_id']),
		array('escape' => false)
	) ?>
	<br />
	<br />

	<section class="module">
		<div class="module-head"><b>Edit Sales Information</b></div>
		<div class="module-body">
			<div class="row-fluid">
			<?= $this->Form->create('SalesInteraction', array('inputDefaults' => array('class' => 'input-block-level'))) ?>
				<div class="row-fluid">
					<div class="span7">
						<?= $this->Form->hidden('prospective_customer_id') ?>
						<?= $this->Form->input('interaction_dt', array('type' => 'text', 'class' => 'datetime', 'label' => 'Interaction Time')) ?>
					</div>
					<div class="span5">
						<label>Interaction type</label>
						<?= $this->Form->input('interaction_type', array('type' => 'radio', 'options' => array_self_combine(array('call','email','meeting')), 'fieldset' => false, 'legend' => false)) ?>
					</div>
				</div>
				<?= $this->Form->input('notes', array('type' => 'textarea')) ?>
				<?= $this->Form->button('Save', array('class' => 'btn btn-primary')) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</section>
</div>



<script>
	$("input.datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss',
		autoclose: true,
		todayBtn: true,
		forceParse: true,
		minView: 0
	})
	.addClass("input-medium clickable")
	.wrap('<div class="input-append"></div>')
	.after('<span class="add-on"><i class="icon-calendar"></i></span>');

	$("input.date").datetimepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayBtn: true,
		forceParse: true,
		minView: 2
	})
	.addClass("input-small clickable")
	.wrap('<div class="input-append"></div>')
	.after('<span class="add-on"><i class="icon-calendar"></i></span>');
</script>

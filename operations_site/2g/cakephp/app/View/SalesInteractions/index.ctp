<div class="salesInteractions index">
	<h2><?php echo __('Sales Interactions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('prospective_customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('interaction_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('salesperson'); ?></th>
			<th><?php echo $this->Paginator->sort('interaction_type'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('modifier_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($salesInteractions as $salesInteraction): ?>
	<tr>
		<td><?php echo h($salesInteraction['SalesInteraction']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($salesInteraction['Prospect']['full_name'], array('controller' => 'prospects', 'action' => 'view', $salesInteraction['Prospect']['id'])); ?>
		</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['interaction_dt']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['salesperson']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['interaction_type']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['notes']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['created']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['modified']); ?>&nbsp;</td>
		<td><?php echo h($salesInteraction['SalesInteraction']['modifier_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $salesInteraction['SalesInteraction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $salesInteraction['SalesInteraction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $salesInteraction['SalesInteraction']['id']), null, __('Are you sure you want to delete # %s?', $salesInteraction['SalesInteraction']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sales Interaction'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Prospects'), array('controller' => 'prospects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prospect'), array('controller' => 'prospects', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="scanners index">
	<h2><?php echo __('Scanners'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('mac_address'); ?></th>
			<th><?php echo $this->Paginator->sort('model'); ?></th>
			<th><?php echo $this->Paginator->sort('version'); ?></th>
			<th><?php echo $this->Paginator->sort('floor_id'); ?></th>
			<th><?php echo $this->Paginator->sort('ip_address'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('password'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($scanners as $scanner): ?>
	<tr>
		<td><?php echo h($scanner['Scanner']['id']); ?>&nbsp;</td>
		<td><?php echo h($scanner['Scanner']['mac_address']); ?>&nbsp;</td>
		<td><?php echo h($scanner['Scanner']['model']); ?>&nbsp;</td>
		<td><?php echo h($scanner['Scanner']['version']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($scanner['Floor']['short_global_label'], array('controller' => 'floors', 'action' => 'view', $scanner['Floor']['id'])); ?>
		</td>
		<td><?php echo h($scanner['Scanner']['ip_address']); ?>&nbsp;</td>
		<td><?php echo h($scanner['Scanner']['username']); ?>&nbsp;</td>
		<td><?php echo h($scanner['Scanner']['password']); ?>&nbsp;</td>
		<td><?php echo h($scanner['Scanner']['note']); ?>&nbsp;</td>
		<td><?php echo h($scanner['Scanner']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $scanner['Scanner']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $scanner['Scanner']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $scanner['Scanner']['id']), null, __('Are you sure you want to delete # %s?', $scanner['Scanner']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Scanner'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Floors'), array('controller' => 'floors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Floor'), array('controller' => 'floors', 'action' => 'add')); ?> </li>
	</ul>
</div>

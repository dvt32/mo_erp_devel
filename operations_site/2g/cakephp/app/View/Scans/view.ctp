<div class="scans view">
<h2><?php echo __('Scan'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($scan['CustomerAccount']['name'], array('controller' => 'customer_accounts', 'action' => 'view', $scan['CustomerAccount']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($scan['Customer']['title'], array('controller' => 'customers', 'action' => 'view', $scan['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Id'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['file_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Scanner Name'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['scanner_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Creation Dt'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['ms_access_creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ms Access Random Num'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['ms_access_random_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creation Dt'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['creation_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Size'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['file_size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Timestamp'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['file_timestamp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filename'); ?></dt>
		<dd>
			<?php echo h($scan['Scan']['filename']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Scan'), array('action' => 'edit', $scan['Scan']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Scan'), array('action' => 'delete', $scan['Scan']['id']), null, __('Are you sure you want to delete # %s?', $scan['Scan']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Scans'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scan'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Accounts'), array('controller' => 'customer_accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Account'), array('controller' => 'customer_accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="sipTermRate form">
<?php echo $this->Form->create('SipTermRate'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sip Term Rate'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('sip_term_carr_id');
		echo $this->Form->input('prefix');
		echo $this->Form->input('description');
		echo $this->Form->input('first_interval');
		echo $this->Form->input('subsequent_interval');
		echo $this->Form->input('rate');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SipTermRate.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('SipTermRate.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sip Term Rate'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sip Term Carrs'), array('controller' => 'sip_term_carrs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Carr'), array('controller' => 'sip_term_carrs', 'action' => 'add')); ?> </li>
	</ul>
</div>

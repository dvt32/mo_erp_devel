<div class="sipTermRate index">
	<h2><?php echo __('Sip Term Rate'); ?></h2>
	<table class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sip_term_carr_id'); ?></th>
			<th><?php echo $this->Paginator->sort('prefix'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('first_interval'); ?></th>
			<th><?php echo $this->Paginator->sort('subsequent_interval'); ?></th>
			<th><?php echo $this->Paginator->sort('rate'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_creation_dt'); ?></th>
			<th><?php echo $this->Paginator->sort('ms_access_random_num'); ?></th>
			<th><?php echo $this->Paginator->sort('creation_dt'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($sipTermRates as $sipTermRate): ?>
	<tr>
		<td><?php echo h($sipTermRate['SipTermRate']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($sipTermRate['SipTermCarr']['name'], array('controller' => 'sip_term_carrs', 'action' => 'view', $sipTermRate['SipTermCarr']['id'])); ?>
		</td>
		<td><?php echo h($sipTermRate['SipTermRate']['prefix']); ?>&nbsp;</td>
		<td><?php echo h($sipTermRate['SipTermRate']['description']); ?>&nbsp;</td>
		<td><?php echo h($sipTermRate['SipTermRate']['first_interval']); ?>&nbsp;</td>
		<td><?php echo h($sipTermRate['SipTermRate']['subsequent_interval']); ?>&nbsp;</td>
		<td><?php echo h($sipTermRate['SipTermRate']['rate']); ?>&nbsp;</td>
		<td><?php echo h($sipTermRate['SipTermRate']['ms_access_creation_dt']); ?>&nbsp;</td>
		<td><?php echo h($sipTermRate['SipTermRate']['ms_access_random_num']); ?>&nbsp;</td>
		<td><?php echo h($sipTermRate['SipTermRate']['creation_dt']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $sipTermRate['SipTermRate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $sipTermRate['SipTermRate']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $sipTermRate['SipTermRate']['id']), null, __('Are you sure you want to delete # %s?', $sipTermRate['SipTermRate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sip Term Rate'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Sip Term Carrs'), array('controller' => 'sip_term_carrs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sip Term Carr'), array('controller' => 'sip_term_carrs', 'action' => 'add')); ?> </li>
	</ul>
</div>

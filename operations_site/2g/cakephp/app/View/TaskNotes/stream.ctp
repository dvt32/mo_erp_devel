<?php if (!empty($notes)): ?>
	<?php foreach ($notes as $note): ?>
			<div class="media-body">
				<h3>
					<?= $note['Employee']['first_name'] ?>
					<?= $note['Employee']['last_name'] ?>
					<small><?= CakeTime::nice($note['TaskNote']['created']) ?></small>
				</h3>
				<div> <?= $note['TaskNote']['content'] ?> </div>
			</div>
	<?php endforeach ?>
<?php else: ?>
	<div class="muted text-center"><?= __("No task notes") ?></div>
<?php endif ?>

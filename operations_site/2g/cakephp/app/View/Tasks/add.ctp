
<div class="row-fluid">

	<div class="span8 offset2">
		<section class="module">
			<div class="module-head">
				<b><?= __('Add Task') ?></b>
			</div>
			<div class="module-body">

				<?= $this->Form->create('Task', array('inputDefaults' => array('class' => 'input-block-level'))) ?>

					<?= $this->Form->input('Task.description') ?>

					<?= $this->Form->input('Task.urgency', array('options' => array_self_combine($urgencies))) ?>
					<?= $this->Form->input('Task.floor_id', array('empty' => '-- Floor --')) ?>
					<?= $this->Form->input('Task.area', array('options' => array_self_combine($areas), 'empty' => '-- Task Type --')) ?>
					<?= $this->Form->input('Task.requested_completion_date') ?>

					<?= $this->Form->hidden('Task.status', array('value' => 'Awaiting Response', 'readonly')) ?>
					<?= $this->Form->hidden('Task.assigned_employee_id', array('options' => $employees, 'label' => 'Assign To')) ?>
					<?= $this->Form->hidden('Task.requestedby_employee_id', array('value' => AuthComponent::user('id'))) ?>
					<?= $this->Form->hidden('Task.completion_date', array('value' => null)) ?>
					<?= $this->Form->hidden('Task.creation_date', array('value' => date(DATETIME_FORMAT))) ?>
					<?= $this->Form->hidden('Task.creation_dt', array('value' => date(DATETIME_FORMAT))) ?>

					<?= $this->Form->button('Create Task', array('class' => 'btn')) ?>
				<?= $this->Form->end() ?>
			</div>
		</section>
	</div>

</div>

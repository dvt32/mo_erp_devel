<div class="tasks form">
<?php echo $this->Form->create('Task'); ?>
	<fieldset>
		<legend><?php echo __('Edit Task'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('description');
		echo $this->Form->input('assigned_employee_id');
		echo $this->Form->input('requestedby_employee_id');
		echo $this->Form->input('requestedby_cust_pers_id');
		echo $this->Form->input('requestedby_cust_acct_id');
		echo $this->Form->input('creation_date');
		echo $this->Form->input('requested_completion_date');
		echo $this->Form->input('completion_date');
		echo $this->Form->input('status');
		echo $this->Form->input('urgency');
		echo $this->Form->input('note');
		echo $this->Form->input('ms_access_creation_dt');
		echo $this->Form->input('ms_access_random_num');
		echo $this->Form->input('creation_dt');
		echo $this->Form->input('floor_id');
		echo $this->Form->input('area');
		echo $this->Form->input('jumble');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Task.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Task.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Tasks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Employees'), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Assignedto Employee'), array('controller' => 'employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Task Notes'), array('controller' => 'task_notes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Note'), array('controller' => 'task_notes', 'action' => 'add')); ?> </li>
	</ul>
</div>

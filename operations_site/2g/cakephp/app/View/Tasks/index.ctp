<!-- search & filter forms -->
<div class="row-fluid">
	<div class="span2">
		<legend>Jump To</legend>

		<!-- jump to task id -->
		<?= $this->Form->create('Task', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false))) ?>
				<?= $this->Form->input('id', array('type' => 'text', 'placeholder' => 'Task Id #', 'class' => 'input-block-level ')) ?>
				<?= $this->Form->button('Go', array('class' => 'btn btn-success')) ?>
		<?= $this->Form->end() ?>
	</div>
	<div class="span2">
		<legend><?= __('Shortcuts') ?></legend>
		<?php $myId = AuthComponent::user('id') ?>
		<ul class="nav nav-pills nav-stacked">
			<li class="active">
				<?= $this->Html->link('Request a Task', array('action' => 'add')) ?>
			</li>
			<li class="active">
				<?= $this->Html->link('Assigned To Me', array('action' => 'index', '?' => array('assigned_employee_id' => $myId))) ?>
				<ul class="nav nav-pills nav-stacked">
					<li><?= $this->Html->link('Urgent', array('action' => 'index', '?' => array('assigned_employee_id' => $myId, 'urgency' => 1))) ?></li>
					<li><?= $this->Html->link('Requested', array('action' => 'index', '?' => array('assigned_employee_id' => $myId, 'status' => 0))) ?></li>
					<li><?= $this->Html->link('In Progress', array('action' => 'index', '?' => array('assigned_employee_id' => $myId, 'status' => 1))) ?></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="span8">
		<legend>Filter &amp; Search</legend>

		<!-- filter and search -->
		<?= $this->Form->create('Task', array('type' => 'GET', 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'input-auto'))) ?>

			<div class="controls-row">
				<?= $this->Form->input('q', array('placeholder' => 'Search Description', 'class' => 'input-xlarge ')) ?>
				<?= $this->Form->input('status', array('options' => $statuses, 'empty' => '-- Task Status --')) ?>
				<?= $this->Form->input('requestedby_employee_id', array('options' => $employees, 'empty' => '-- Requested By Employee --')) ?>
				<?= $this->Form->input('assigned_employee_id', array('options' => $employees, 'empty' => '-- Assigned Employee --')) ?>
				<?= $this->Form->input('floor_id', array('empty' => '-- Floor --', 'class' => 'input-auto')) ?>
			</div>

			<?= $this->Form->button('Filter', array('class' => 'btn btn-success')) ?>
			<?= $this->Html->link('Clear', array('action' => 'index')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>


<div class="row-fluid">
	<table class="table">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('id', 'Task #') ?></th>
				<th><?= $this->Paginator->sort('floor_id') ?></th>
				<th><?= $this->Paginator->sort('assigned_employee_id') ?></th>
				<th><?= $this->Paginator->sort('requestedby_employee_id', 'Employee') ?></th>
				<th><?= $this->Paginator->sort('requestedby_cust_pers_id', 'Person') ?></th>
				<th><?= $this->Paginator->sort('requestedby_cust_acct_id', 'Account') ?></th>
				<th><?= $this->Paginator->sort('area') ?></th>
				<th><?= $this->Paginator->sort('creation_date') ?></th>
				<th><?= $this->Paginator->sort('requested_completion_date') ?></th>
				<th><?= $this->Paginator->sort('completion_date') ?></th>
				<th><?= $this->Paginator->sort('status') ?></th>
				<th><?= $this->Paginator->sort('urgency') ?></th>
				<th><?= $this->Paginator->sort('creation_dt', 'Created') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($tasks as $task): ?>
			<tr>
				<td><strong style="font-size:1.5em;"><?= $this->Html->link("{$task['Task']['id']}", array('action' => 'view', $task['Task']['id'])) ?></strong>&nbsp;</td>
				<td><?= h(@$task['Floor']['short_global_label']) ?>&nbsp;</td>
				<td><?= @$task['AssignedtoEmployee']['id'] ? $this->Html->link($task['AssignedtoEmployee']['full_name'], array('controller' => 'employees', 'action' => 'view', $task['AssignedtoEmployee']['id'])) : '&nbsp;' ?></td>
				<td><?= @$task['RequestedbyEmployee']['id'] ? $this->Html->link($task['RequestedbyEmployee']['full_name'], array('controller' => 'employees', 'action' => 'view', $task['RequestedbyEmployee']['id'])) : '&nbsp;' ?></td>
				<td><?= h(@$task['RequestedbyCustomer']['full_name']) ?>&nbsp;</td>
				<td><?= h(@$task['RequestedbyCustomerAccount']['name']) ?>&nbsp;</td>
				<td><?= h(@$task['Task']['area']) ?>&nbsp;</td>
				<td><?= h(@$task['Task']['creation_date']) ?>&nbsp;</td>
				<td><?= h(@$task['Task']['requested_completion_date']) ?>&nbsp;</td>
				<td><?= h(@$task['Task']['completion_date']) ?>&nbsp;</td>
				<td><?= h(@$task['Task']['status']) ?>&nbsp;</td>
				<td><?= h(@$task['Task']['urgency']) ?>&nbsp;</td>
				<td><?= h(@$task['Task']['creation_dt']) ?>&nbsp;</td>
			</tr>
			<tr>
				<td style="border-top:none;">&nbsp;</td>
				<td class="description" colspan="0" style="border-top:none;"><?= h(@$task['Task']['description']) ?>&nbsp;</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	<p>
		<?= $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') )) ?>
	</p>
	<div class="paging">
		<?= $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) ?>
		<?= $this->Paginator->numbers(array('separator' => '')) ?>
		<?= $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) ?>
	</div>
</div>

<script>
	$("td.description").highlight(QueryString.q);
</script>

<div class="row-fluid">
	<ul class="breadcrumb">
		<li><a href="{{ operations_2g_urlprefix }}/">Home</a><span class="divider">/</span></li>
		<li><a href="{{ operations_2g_urlprefix }}/tasks">Tasks</a><span class="divider">/</span></li>
		<li class="active">Task #<?php printf("%03d",$task['Task']['id']) ?><span class="divider">/</span></li>
	</ul>
</div>
<div class="row-fluid">
	<div class="offset2 span8">
		<?= $this->Session->flash() ?>

		<!-- task details -->
		<section class="module">
			<div class="module-head"> <b><?= __('Task Details') ?></b> </div>
			<div class="row-fluid">
				<dl>
					<dt><?= __('Id') ?></dt>
					<dd><?= h($task['Task']['id']) ?>&nbsp;</dd>
					<dt><?= __('Assigned To') ?></dt>
					<dd><?= $this->Html->link($task['AssignedtoEmployee']['full_name'], array('controller' => 'employees', 'action' => 'view', $task['AssignedtoEmployee']['id'])) ?>&nbsp;</dd>
					<?php if (!empty($task['RequestedbyEmployee'])): ?>
						<dt><?= __('Requested By') ?></dt>
						<dd><?= $this->Html->link($task['RequestedbyEmployee']['full_name'], array('controller' => 'employees', 'action' => 'view', $task['RequestedbyEmployee']['id'])) ?>&nbsp;</dd>
					<?php else: ?>
						<dt><?= __('Requested By') ?></dt>
						<dd><?= h($task['RequestedbyCustomer']['full_name']) ?>&nbsp;</dd>
						<dt><?= __('Company') ?></dt>
						<dd><?= h($task['RequestedbyCustomerAccount']['name']) ?>&nbsp;</dd>
					<?php endif ?>
					<dt><?= __('Creation Date') ?></dt>
					<dd><?= h($task['Task']['creation_date']) ?>&nbsp;</dd>
					<dt><?= __('Requested Completion Date') ?></dt>
					<dd><?= h($task['Task']['requested_completion_date']) ?>&nbsp;</dd>
					<dt><?= __('Completion Date') ?></dt>
					<dd><?= h($task['Task']['completion_date']) ?>&nbsp;</dd>
					<dt><?= __('Status') ?></dt>
					<dd><?= h($task['Task']['status']) ?>&nbsp;</dd>
					<dt><?= __('Urgency') ?></dt>
					<dd><?= h($task['Task']['urgency']) ?>&nbsp;</dd>
					<dt><?= __('Note') ?></dt>
					<dd><?= h($task['Task']['note']) ?>&nbsp;</dd>
					<dt><?= __('Creation Datetime') ?></dt>
					<dd><?= CakeTime::nice($task['Task']['creation_dt']) ?>&nbsp;</dd>
					<dt><?= __('Floor') ?></dt>
					<dd><?= h($task['Floor']['short_global_label']) ?>&nbsp;</dd>
					<dt><?= __('Area') ?></dt>
					<dd><?= h($task['Task']['area']) ?>&nbsp;</dd>
					<dt><?= __('Created') ?></dt>
					<dd><?= h(@$task['Task']['created']) ?>&nbsp;</dd>
					<dt><?= __('Modified') ?></dt>
					<dd><?= h(@$task['Task']['modified']) ?>&nbsp;</dd>
					<dt><?= __('Description') ?></dt>
					<dd><?= h($task['Task']['description']) ?>&nbsp;</dd>
				</dl>
			</div>
		</section>

		<!-- task edit -->
		<section class="module" id="TaskEdit">
			<div class="module-head clickable" id="TaskFormHeader"> <b><?= __('Edit Task') ?></b></div>
			<div class="module-body no-padding" style="display:none;">
				<div class="stream composer">
					<?= $this->Form->create('Task', array('action' => 'edit', 'inputDefaults' => array('class' => 'input-block-level'))) ?>
						<?= $this->Form->input('Task.id') ?>
						<?= $this->Form->input('Task.description') ?>
						<?= $this->Form->input('Task.status',               array('options' => array_combine($statuses,$statuses))) ?>
						<?= $this->Form->input('Task.urgency',              array('options' => array_combine($urgencies,$urgencies))) ?>
						<?= $this->Form->input('Task.area',                 array('options' => array_combine($areas,$areas), 'empty' => '-- Task Area --')) ?>
						<?= $this->Form->input('Task.assigned_employee_id', array('options' => $employees, 'empty' => '-- Assigned Employee --')) ?>
						<br />
						<?= $this->Form->hidden('Route.next', array('value' => Router::url())) ?>
						<?= $this->Form->button('Edit Task', array('class' => 'btn')) ?>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</section>

		<!-- task notes -->
		<section class="module">
			<div class="module-head" id="TaskNoteStreamHeader"> <b><?= __('Task Notes') ?></b> </div>
			<div class="module-body no-padding">

				<div id="TaskNoteStream" class="stream media"> </div>

				<div class="stream composer">
					<?= $this->Form->create('TaskNote', array('action' => 'add', 'class' => 'form-absolute', 'inputDefaults' => array('label' => false, 'div' => false))) ?>
						<?= $this->Form->hidden('TaskNote.task_id', array('value' => $task['Task']['id'])) ?>
						<?= $this->Form->input('TaskNote.content', array('type' => 'textarea', 'class' => 'input-block-level autosize-normal')) ?>
						<?= $this->Form->button('Add Note', array('id' => 'AddTaskNoteButton', 'class' => 'btn')) ?>
					<?= $this->Form->end() ?>
				</div>

			</div>
		</section>
	</div>
</div>


<script>
	// update task notes
	function updateTaskNoteStream() {
		$.ajax({
			url: "{{ operations_2g_urlprefix }}/taskNotes/stream/" + <?= $task['Task']['id'] ?>,
			cache: false,
			type: 'GET',
			dataType: 'HTML',
			success: function (data) {
				$("#TaskNoteStream").html(data)
			}
		});
	}
	
	// add task note via ajax
	$(function() {
		
		updateTaskNoteStream();
		$("div.alert").fadeOut(2500);

		$("#AddTaskNoteButton").click(function() {
			var task_id = $("#TaskNoteTaskId").val();
			var task_note_content = $("#TaskNoteContent").val();
			var datastring = 'data[TaskNote][task_id]=' + task_id + "&data[TaskNote][content]=" + task_note_content;
			$.ajax({
				type: 'POST',
				url: '/taskNotes/add',
				data: datastring,
				success: function() {
					updateTaskNoteStream();
					$("#TaskNoteContent").val('');
				}
			});
			return false;
		});
	});

	// reload every 2500 ms
	setInterval(updateTaskNoteStream, 2500);

	// expandable task edit form
	$("#TaskFormHeader").click(function() {$("#TaskEdit div.module-body").toggle();});

</script>



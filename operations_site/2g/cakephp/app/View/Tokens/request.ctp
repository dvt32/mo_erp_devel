<div class="row-fluid">
	<?= $this->Form->create('Token', array('class' => 'centered-form', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'input-block-level') )) ?>
		<h2 class="centered-form-heading align-center"><?= __('Forgot Your Password?') ?></h2>
		<?= $this->Session->flash() ?>
		<?= $this->Form->input('Employee.email', array('placeholder' => 'Email Address', 'autofocus' => 'autofocus')) ?>
		<?= $this->Form->hidden('Token.type', array('value' => 'resetPassword')) ?>
		<?= $this->Form->button('Submit', array('class' => 'btn btn-large btn-block btn-primary')) ?>
	<?= $this->Form->end() ?>
</div>
<div class="text-center">
	Remember your password?
	<?= $this->Html->link('Back to login', array('controller' => 'employees', 'action' => 'login')) ?></a>
</div>

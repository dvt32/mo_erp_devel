<div class="voicemails index">
	<h2>
		<?= $this->Html->image('icons/flat_miniset/png/256/speech_bubble.png' , array('style' => 'height:3em;')) ?>
		<?= __('Voicemails'); ?>
	</h2>
	<table class="table">
	<tr>
			<th><?= $this->Paginator->sort('created_epoch', 'Recieved'); ?></th>
			<th><?= $this->Paginator->sort('username', 'Phone Number'); ?></th>
			<th><?= $this->Paginator->sort('cid_name', 'Caller ID'); ?></th>
			<th><?= $this->Paginator->sort('cid_number', 'Caller ID Number'); ?></th>
			<th><?= $this->Paginator->sort('message_len', 'Message Length'); ?></th>
			<th></th>
	</tr>
	<?php foreach ($voicemails as $voicemail): ?>
	<tr>
		<td><?= CakeTime::nice(date(DATETIME_FORMAT, $voicemail['Voicemail']['created_epoch'])) ?>&nbsp;</td>
		<td><?= h($voicemail['Voicemail']['username']); ?>&nbsp;</td>
		<td><?= h($voicemail['Voicemail']['cid_name']); ?>&nbsp;</td>
		<td><?= h($voicemail['Voicemail']['cid_number']); ?>&nbsp;</td>
		<td><?= h($voicemail['Voicemail']['message_len']); ?>&nbsp;</td>
		<td><?= $this->Html->image('recording.jpg', array(
			'style' => 'height:20px;',
			'url' => array(
				'controller' => 'Voicemails',
				'action' => 'download',
				$voicemail['Voicemail']['uuid']
			)
		)); ?></a></td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>


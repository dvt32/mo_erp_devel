var navigation = 
{
    "Nav": {
        "nav_marketing": {
            "Campaign Management": "{{ operations_2g_urlprefix }}/campaigns",
            "Title": "Marketing"
        },
        "nav_sales": {
            "Title": "Sales",
            "Prospective Customers": "{{ operations_2g_urlprefix }}/prospects",
            "Add New Prospect": "{{ operations_2g_urlprefix }}/prospects/add",
        },
        "nav_billing": {
            "Title": "Billing",
            "Accounts Review": "{{ operations_2g_urlprefix }}/customerAccounts/billing",
            "Invoice Changes": "{{ operations_2g_urlprefix }}/customerAccounts/invoicing",
            "Accounts Receivables": "{{ operations_2g_urlprefix }}/customerAccounts/receiving",
            "Aging Accounts": "{{ operations_2g_urlprefix }}/customerAccounts/aging",
            "List Deposits": "{{ operations_2g_urlprefix }}/deposits",
            "Make Deposits": "{{ operations_2g_urlprefix }}/deposits/add"
        },
        "nav_licenses": {
            "Title": "License",
            "Manage": "{{ operations_2g_urlprefix }}/licenseAgreements",
            "Create": "{{ operations_2g_urlprefix }}/licenseAgreements/add",
            "Agreement Templates": "{{ operations_2g_urlprefix }}/licenseAgreementTemplates",
            "Amendment Templates": "{{ operations_2g_urlprefix }}/licenseAmendmentTemplates",
        },
        "nav_dir": {
            "Title": "Directories",
            "Customers": "{{ operations_2g_urlprefix }}/customers",
            "Companies": "{{ operations_2g_urlprefix }}/customerAccounts",
            "Employees": "{{ operations_2g_urlprefix }}/employees"
        },
        "nav_resources": {
            "Title": "Resources",
            "Access Cards Admin": "{{ operations_2g_urlprefix }}/proximityCards",
            "Access Cards Approve": "{{ operations_2g_urlprefix }}/proximityCardEdits",
            "Mailboxes": "{{ operations_2g_urlprefix }}/mailboxes",
            "Reservations": "{{ operations_2g_urlprefix }}/reservations/byFloor/2",
            "Buildings": "{{ operations_2g_urlprefix }}/buildings",
            "Floors": "{{ operations_2g_urlprefix }}/floors",
            "Desks": "{{ operations_2g_urlprefix }}/desks/index",
            "Cameras": "{{ operations_2g_urlprefix }}/pages/cameras"
        },
        "nav_admin": {
            "Title": "Administration",
            "Phone Numbers": "{{ operations_2g_urlprefix }}/phoneNumbers",
            "Fax Numbers": "{{ operations_2g_urlprefix }}/faxNumbers",
            "Mailboxes": "{{ operations_2g_urlprefix }}/mailboxes",
            "Reservations": "{{ operations_2g_urlprefix }}/reservations",
            "Buildings": "{{ operations_2g_urlprefix }}/buildings",
            "Floors": "{{ operations_2g_urlprefix }}/floors",
            "Desks": "{{ operations_2g_urlprefix }}/desks",
            "Employees": "{{ operations_2g_urlprefix }}/employees",
            "Scanner": "{{ operations_2g_urlprefix }}/scanners",
            "All Scans": "{{ operations_2g_urlprefix }}/scans/admin",
            "All Faxes": "{{ operations_2g_urlprefix }}/faxIns/admin",      
        },
        "nav_tasks": {
            "Manage Tasks": "{{ operations_2g_urlprefix }}/tasks",
            "Title": "Task Management",
            "Notifications": "{{ operations_2g_urlprefix }}/notifications"
        },
        "nav_comm": {
            "Title": "Communications and Imaging",
            "Phone Numbers": "{{ operations_2g_urlprefix }}/phoneNumbers",
            "Fax Numbers": "{{ operations_2g_urlprefix }}/faxNumbers",
            "Network Monitoring": "{{ operations_2g_urlprefix }}/buildings/networkMonitor",
            "IP Phones": "{{ operations_2g_urlprefix }}/ipPhones",
            "IP Addresses": "{{ operations_2g_urlprefix }}/ipAddresses",
            "Public Scans": "{{ operations_2g_urlprefix }}/scans",
            "Public Faxes": "{{ operations_2g_urlprefix }}/faxIns",
            "My Documents": "{{ operations_2g_urlprefix }}/documents"
        },
        "nav_hr": {
            "Title": "Human Resources",
            "Timesheets": "{{ operations_2g_urlprefix }}/timesheets",
            "Payroll": "{{ operations_2g_urlprefix }}/payroll",
            "Benefits": "{{ operations_2g_urlprefix }}/benefits",
            "Expenses": "{{ operations_2g_urlprefix }}/expenses"
        },
        "nav_finances": {
            "Title": "Finances",
            "Receive Payment": "{{ operations_2g_urlprefix }}/recievePayment",
            "Transactions": "{{ operations_2g_urlprefix }}/transactions",
            "Reconcile Accounts": "{{ operations_2g_urlprefix }}/reconciles",
            "Customer Accounts": "{{ operations_2g_urlprefix }}/accounts",
            "Expenses": "{{ operations_2g_urlprefix }}/expenses/admin"
        },
        "nav_reports": {
            "Title": "Reports",
            "Weekend Access": "{{ operations_2g_urlprefix }}/reports/access",
            "Guests": "{{ operations_2g_urlprefix }}/reports/guests",
            "Deliveries": "{{ operations_2g_urlprefix }}/reports/deliveries",
            "Intercom": "{{ operations_2g_urlprefix }}/reports/intercom",
            "Mailboxes": "{{ operations_2g_urlprefix }}/reports/mailboxes",
            "Floor": "{{ operations_2g_urlprefix }}/reports/deskmap"
        },
        "nav_docs": {
            "Title": "Documentation",
            "Technical": "{{ operations_2g_urlprefix }}/pages/technical",
            "Bug Reports": "{{ operations_2g_urlprefix }}/pages/bugs",
            "Help": "{{ operations_2g_urlprefix }}/pages/help",
            "Features": "{{ operations_2g_urlprefix }}/pages/features"
        },
        "nav_authorization": {
            "Employee Management" : "{{ operations_2g_urlprefix }}/employeeRoles#/admin",
            "Employee Roles" : "{{ operations_2g_urlprefix }}/employeeRoles/reassign",
            "Register Route" : "{{ operations_2g_urlprefix }}/acos/manage"
        }
    }
};

var subopen = false;

$('.nav-sub').click(function() {

    if (subopen == false) {

        // list append
        var subsection = $(this).attr("id");
        var navsub = navigation.Nav[subsection];
        var salesub = _.keys(navsub);
        // clear submenu
        $(".nav-sub").removeClass('nav-selected');
        $(".nav-down").remove();

        subopen  = true;

        // selected
        $(this).addClass('nav-selected');
        // append new links from json array
        $(this).append('<ul id="submenu" class="nav-down">');
        _.each(salesub, function(num) {
            if (num != "Title") {
             $("#submenu").append("<a href="+navsub[num]+" class='nav-link'>"+"<li class='nav-bullet'>"+num+"</li></a>");
            }
        });
    } else {
        closesubmenu();
    }
});


$(".content-body").click(function() {
    closesubmenu();
});

function closesubmenu() {

    // check to see if menu is open
    if (subopen == true) {
        subopen = false;
        $(".nav-sub").removeClass('nav-selected');
        $(".nav-down").remove();
        
    }
}

// // expand submenu
// var subopen = false;
// var menuopen;
// var nextopen;
// $(".nav-sub").click(function() {

// 	// check to see if the same submenu is open
// 	if (menuopen == nextopen) {

// 		// check to see if menu is open
// 		if (subopen == true) {

// 			closesubmenu();

// 		} else if (subopen == false) {

			
// 			menuopen = $(this).attr("id");
// 			subopen = true;
// 			$(".content-body").animate({ 'margin-left':'300px'},200);
// 			$(".navbar-static-tops").animate({ 'margin-left':'280px' }, 200);
// 			$("#nav-expand").show("slide", { direction: "left" }, 200);

// 			// list append
// 			var subsection = $(this).attr("id");
// 			var navsub = navigation.Nav[subsection];
// 			var salesub = _.keys(navsub);
// 			// clear submenu
// 			$(".nav-expanded").empty();

//             // append header
//             $(".nav-expanded").append('<li class="nav-header">'+navsub['Title']+'</li>');
// 			// append new links from json array
// 			$(".nav-expanded").append('<ul id="submenu" class="nav nav-list">');
// 			_.each(salesub, function(num) {
//                 if (num != "Title") {
//     				$("#submenu").append("<a href="+navsub[num]+">"+"<li>"+num+"</li></a>");
//                 }
// 			});

// 		}

// 	} else {

// 		nextopen = $(this).attr("id");
// 		subopen = true;
// 		$(".content-body").animate({ 'margin-left':'300px'},200);
// 		$(".navbar-static-tops").animate({ 'margin-left':'280px'},200);
// 		$("#nav-expand").hide();
// 		$("#nav-expand").show("slide", { direction: "left" }, 200);

// 		// list append
// 		var subsection = $(this).attr("id");
// 		var navsub = navigation.Nav[subsection];
// 		var salesub = _.keys(navsub);
// 		// clear submenu
// 		$(".nav-expanded").empty();

//         // append header
//         $(".nav-expanded").append('<li class="nav-header">'+navsub['Title']+'</li>');
// 		// append new links from json array
// 		$(".nav-expanded").append('<ul id="submenu" class="nav nav-list">');
// 		_.each(salesub, function(num) {
//             if (num != "Title") {
//     			$("#submenu").append("<a href="+navsub[num]+">"+"<li>"+num+"</li></a>");
//             }
// 		});


// 	}

// });


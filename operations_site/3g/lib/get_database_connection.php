<?php
    // connect to server
    $connection = pg_connect("host={{main_database_hostname}} dbname={{main_database_name}} user={{main_database_operations_username}} password={{main_database_operations_password}} sslmode={{main_database_sslmode}}") 
    or die('Could not connect to database: ' . pg_last_error());
?>

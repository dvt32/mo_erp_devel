var inputFile = null;

function submit_request() {

	var formData = new FormData();
	var date = document.getElementById('date').value;
	var id = document.getElementById('id').value;

	var stop = false;
	
	// check ID
	if(id == "")
	{
		error("Please enter a customer id" );
		return false;
	}
	else
	{
		$.ajax({
			url: "submit.php", 	  
			type: "POST",            
			data: "check_cust=" + id, 	            
			async:false,   
			success: function(data)   
			{
				if(data != "succes")
				{
					error("Please enter a valid ID!");
					stop = true;
				}
			}
		});
	}
	
	if (stop)
	{
		return false;
	}
	
	// check date	
	if(date == "")
	{
		error("Please enter a termination date" );
		return false;
	}
	else
	{
		var dateArray = date.split("-");
		if (dateArray.length != 3)
		{
			error("Please enter a valid date (12-31-2015)");
			return false;
		}
		else
		{
			if(dateArray[0] > 0 && dateArray[0] < 13 
			&& dateArray[1] > 0 && dateArray[1] < 32 
			&& dateArray[2] > 2000 && dateArray[0] < 3000)
			{
				date = dateArray[2] + "-" 
					+  dateArray[0] + "-" 
						+ dateArray[1];
			}
			else
			{
				error("Please enter a valid date (12-31-2015)");
				return false;
			}
		}
		
	}


	if(inputFile != null)
	{	
		formData.append("file", inputFile);
	}
	
	formData.append("id", id);
	formData.append("date", date);

	
	$.ajax({
		url: "submit.php", 	  
		type: "POST",            
		data: formData, 	  
		contentType: false,      
		cache: false,             
		processData:false,
		async:false,    
		success: function(data)   
		{
			if(data == "succes")
			{
				document.getElementById('comment').style.display = "block";
				document.getElementById('comment').style.backgroundColor = "green";
				document.getElementById('comment').innerHTML = "Succes!";
			}
			else
			{
				error("database error!");
			}
		}
	});
}

function error(text)
{
	document.getElementById('comment').style.display = "block";
	document.getElementById('comment').style.backgroundColor = "red";
	document.getElementById('comment').innerHTML = text;
}

(function() {

	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}


	// output information
	function Output(msg) {
		var m = $id("messages");
		m.innerHTML = msg;
	}


	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}


	// file selection
	function FileSelectHandler(e) {

		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		var files = e.target.files || e.dataTransfer.files;

		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
			ParseFile(f);
		}

	}


	// output file information
	function ParseFile(file) {
			inputFile = file;			
			alert(inputFile.name);
		Output(
			"<p>File information: <strong>" + file.name +
			"</strong> type: <strong>" + file.type +
			"</strong> size: <strong>" + file.size +
			"</strong> bytes</p>"
		);

	}


	// initialize
	function Init() {

		var fileselect = 
			filedrag = $id("filedrag"),
			submitbutton = $id("submitbutton");

		// file select
		fileselect.addEventListener("change", FileSelectHandler, false);

		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {

			// file drop
			filedrag.addEventListener("dragover", FileDragHover, false);
			filedrag.addEventListener("dragleave", FileDragHover, false);
			filedrag.addEventListener("drop", FileSelectHandler, false);
			filedrag.style.display = "block";

		}

	}

	// call initialization file
	window.onload = function() {	
		if (window.File && window.FileList && window.FileReader) {
			Init();
		}
	}


})();

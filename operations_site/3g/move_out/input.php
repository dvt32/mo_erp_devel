<?php
	
	// get database info
	require '../lib/get_database_connection.php';

	function getEmployeeName($employeeID)
	{
		require '../lib/get_database_connection.php';
		$employee = pg_query_params($connection, 
			"select windows_logon_name from employee_tbl where id = $1;", 
				array($employeeID));
		$employee = pg_fetch_row($employee);
		return $employee[0];
	}
	
	function getCustomerName($customerID)
	{
		require '../lib/get_database_connection.php';
		$customer = pg_query_params($connection, 
			"select name from cust_acct_tbl where id = $1", array($customerID));
		$customer = pg_fetch_array($customer);
		return $customer[0];
	}	


	$get_queue = pg_query($connection, "SELECT * from move_out_queue_tbl where isActive;");
	$result = pg_fetch_row($get_queue);
	
	
	while($result !== false)
	{
		$id = $result[0];
		$requestDate = $result[1];
		$customer_id = $result[2];
		$termination_date = $result[3];
		$confirmed_date = $result[4];
		$confirmed_employee = $result[5];
		$notes = $result[6];
		$closed_date = $result[7];
		$closed_employee = $result[8];
		$deposit_returned = $result[9];
		
		$customerName = getCustomerName($customer_id);
		$confirmed_employee = getEmployeeName($confirmed_employee);
		$closed_employee = getEmployeeName($closed_employee);

		if ($deposit_returned == null)
		{
			$deposit_returned = "pending";
		}
		if($notes == null)
		{
			$notes = "-";
		}
	
		$queue[] = array($id, $requestDate, $customerName, $termination_date, $confirmed_date, $confirmed_employee, $notes,$closed_date, $closed_employee, $deposit_returned);
		
		$result = pg_fetch_row($get_queue);
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<title> Move-out queue </title>
		<link href="style.css" rel="stylesheet" media="all" type="text/css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="filedrag.js"></script>
	</head>
	<body>
		<h3> Move-out queue </h3><br>
		<div id="comment"> Saved to queue! </div>
		<fieldset>
		<legend>New Request</legend>
		Customer id:<input type="text" name="id" id="id" size="3" placeholder="ID" /> 
		Termination date: <input type="text" size="9" name="date" id="date" placeholder="mm-dd-yyyy"/> 
			<div>
				
				<div id="filedrag">drop written notification file here</div>

				<div id="messages">
					<p></p>
				</div>
			</div>
			<button onclick="submit_request();">Save</button>
		</fieldset>

		<h4> Queue </h4>
		<table border="1">
			<tr>
				<td></td>				
				<td><b> Date </b></td>
				<td><b> Customer </b></td>
				<td><b> Termination request Date </b></td>
				<td><b> Confirmation date </b></td>
				<td><b> Confirmed by </b></td>
				<td><b> Notes </b></td>	
				<td><b> Account closed </b></td>	
				<td><b> Account closed by</b></td>	
				<td><b> Deposit returned </b></td>	
				<td><b> Notification file </b></td>
			</tr>
			<?php foreach($queue as $row): ?>
			<tr id="<?= $row[0]?>">
				<td><a onclick="remove_row('<?= $row[0]?>');"><b> edit </b></a></td>
				<td><?= $row[1]?></td>
				<td><?= $row[2]?></td>
				<td><?= $row[3]?></td>
				<td><?= $row[4]?></td>
				<td><?= $row[5]?></td>
				<td><?= $row[6]?></td>	
				<td><?= $row[7]?></td>		
				<td><?= $row[8]?></td>		
				<td><?= $row[9]?></td>	
				<td><a href=""> file </a> </td>	
			</tr>
			<?php endforeach; ?>
			<tr>
				<td><a href=""><b> save </b></a> / 
					<a href=""><b> remove </b></a></td>				
				<td>May-13</td>
				<td>ABC-carpets</td>
				<td>March 31</td>
				<td>-</td>
				<td> 	<select>
						<option value=""></option>
						<option value="mdevogel">mdevogel</option>
					</select> 
				</td>
				<td> <input type="text" /></td>	
				<td> - </td>	
				<td>
					<select>
						<option value=""></option>
						<option value="mdevogel">mdevogel</option>
					</select> 
				</td>	
				<td> <input type="text" size="9" placeholder="mm-dd-yyyy"/> </td>
				<td><a href=""> file </a> </td>		
			</tr>
		</table> <br>
		<button> Load More </button> <br>		

	</body>
	<script>
		function remove_row(num)
		{
			// set inactive
			// TODO

			// remove from list
			var row = document.getElementById(num);
			row.parentNode.removeChild(row);
		}
	</script>
</html>

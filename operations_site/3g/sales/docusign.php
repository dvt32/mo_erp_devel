<?php
    
    /*
    *
    * 	File is called from order_result.php
    *
    *	This file expects a $documentName variable with the name 
    *	& directory of the chosen pdf file
    *
    */
    
	// add these in SOF constants -> Docusign account
	$email = "";											
	$password = "";												
	$integratorKey = "";			
	
	// signer employee
	$employee_email = "mariodv@hotmail.nl";
	$employee_name = "Jan Jansen";
	
	// receipient
	$recipientemail = "Mariusdv@outlook.com";
	$recipientName = "Test User";				
			
	// adress which will be notified when order is signed
	$completedmail = "mariusdv@outlook.com";
	
	// construct the authentication header:
	$header = "<DocuSignCredentials><Username>" . $email . "</Username><Password>" . $password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";
	
	// get Base URL & account ID
	$url = "https://demo.docusign.net/restapi/v2/login_information";
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: $header"));
	
	$json_response = curl_exec($curl);
	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
	if ( $status != 200 ) {
		echo "error calling webservice, status is:" . $status;
		exit(-1);
	}
	
	$response = json_decode($json_response, true);
	$accountId = $response["loginAccounts"][0]["accountId"];
	$baseUrl = $response["loginAccounts"][0]["baseUrl"];
	curl_close($curl);
	
	
	// add signing tabs & email
	$data = array (
            "emailSubject" => "Micro Office - Signature Request on Order Form #" . $id,
            "documents" => array( array( "documentId" => "1", "name" => $documentName)),
            "recipients" => array( "signers" => array(
            	 // customer
                array(  "email" => $recipientemail,
                        "name" => $recipientName,
                        "recipientId" => "1",
                        "tabs" => array(
                            "signHereTabs" => array(
                                array( "xPosition" => "45",
                                       "yPosition" => "395",
                                       "documentId" => "1",
                                       "pageNumber" => "1" )
                            ),
                            "dateSignedTabs" => array(
                            	array( 	"xPosition" => "295",
	                                   	"yPosition" => "442",
	                                   	"documentId" => "1",
	                                   	"pageNumber" => "1",
	                                   	"tabLabel" => "Date Signed Customer",
  										"name" => "Date Signed Customer")
                            ))
                 ),
                 // employee
                 array(	"email" => $employee_email,
                        "name" => $employee_name,
                        "recipientId" => "2",
                        "tabs" => array(
                            "signHereTabs" => array(
                                array( "xPosition" => "568",
                                       "yPosition" => "395",
                                       "documentId" => "1",
                                       "pageNumber" => "1" )
                            ),
                            "dateSignedTabs" => array(
                            	array( 	"xPosition" => "752",
	                                   	"yPosition" => "442",
	                                   	"documentId" => "1",
	                                   	"pageNumber" => "1",
	                                   	"tabLabel" => "Date Signed Employee",
  										"name" => "Date Signed Employee")
                            )))
                 ),
                 "carbonCopies" => array(
                 array(
                 		"email" => $completedmail,
                        "name" => "CARBON",
                        "recipientId" => "3",
                        "emailNotification" => array(
    						"emailBody" => "email text",
    						"emailSubject" => $id,
    					),
                    ),
                 ),
            ),
	    "status" => "sent"
	);
	$data_string = json_encode($data);  
	
	$file_contents = file_get_contents( $_SERVER['DOCUMENT_ROOT'] . $documentName);

	$requestBody = "\r\n"
	."\r\n"
	."--myboundary\r\n"
	."Content-Type: application/json\r\n"
	."Content-Disposition: form-data\r\n"
	."\r\n"
	."$data_string\r\n"
	."--myboundary\r\n"
	."Content-Type:application/pdf\r\n"
	."Content-Disposition: file; filename=\"$documentName\"; documentid=1 \r\n"
	."\r\n"
	."$file_contents\r\n"
	."--myboundary--\r\n"
	."\r\n";
	
	$curl = curl_init($baseUrl . "/envelopes" );
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);                                                                  
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: multipart/form-data;boundary=myboundary',
		'Content-Length: ' . strlen($requestBody),
		"X-DocuSign-Authentication: $header" )                                                                       
	);
	
	$json_response = curl_exec($curl);
	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if ( $status != 201 ) {
		echo "error calling webservice, status is:" . $status . "\nerror text is --> ";
		print_r($json_response); echo "\n";
		exit(-1);
	}
	
	$response = json_decode($json_response, true);
	$envelopeId = $response["envelopeId"];
	
	echo "Order is sent! Envelope ID = " . $envelopeId . "<br><br> You can find the document in your inbox at: " . $employee_email; 
?>

CREATE LANGUAGE plpgsql;

-- function to update modify_ts column whenever rows are modified
CREATE OR REPLACE FUNCTION update_modify_ts()
RETURNS TRIGGER AS $$
BEGIN
  NEW.modify_ts = now();
  RETURN NEW;
END;
$$ language 'plpgsql';

CREATE SEQUENCE order_form_preprocess_seq;

CREATE TABLE order_form_preprocess_tbl (
  id integer PRIMARY KEY DEFAULT nextval('order_form_preprocess_seq') NOT NULL,
  form_data jsonb NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
);

CREATE SEQUENCE licensor_seq;

CREATE TABLE licensor_tbl (
  id integer PRIMARY KEY DEFAULT nextval('licensor_seq') NOT NULL,
  name varchar(128) NOT NULL,
  short_name varchar(32) NOT NULL,
  ownership_pct numeric(7, 4) NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER licensor_modts BEFORE UPDATE ON licensor_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();
COMMENT ON COLUMN licensor_tbl.short_name IS 'An abbreviation for the licensor name. Makes it easier for people to reference our companies by shortening what needs to be displayed.';
COMMENT ON COLUMN licensor_tbl.ownership_pct IS 'Percentage ownership of the subsidiary.';

CREATE SEQUENCE building_seq;

CREATE TABLE building_tbl (
  id integer PRIMARY KEY DEFAULT nextval('building_seq') NOT NULL,
  address_1 varchar(255) NOT NULL,
  address_2 varchar(255),
  city varchar(255) NOT NULL,
  state varchar(255) NOT NULL,
  postal_code varchar(24),
  country char(2) NOT NULL,
  short_global_label varchar(32) NOT NULL,
  longitude double precision NOT NULL,
  latitude double precision NOT NULL,
  licensor_id integer NOT NULL,
  time_zone varchar(38) NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE,
  FOREIGN KEY (licensor_id) REFERENCES licensor_tbl (id)
);

CREATE TRIGGER building_modts BEFORE UPDATE ON building_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();
COMMENT ON TABLE building_tbl IS 'Stores information on the buildings that are rented out to customers.';
COMMENT ON COLUMN building_tbl.short_global_label IS 'An abbreviation for the building name or address. For example, 1375 Broadway, New York, NY 10018 would be NY-1375Bwy; 405 W. Superior St., Chicago, IL 60654 would be CH-405WSup. Makes it easier to reference buildings for people by shortening what needs to be displayed.';
COMMENT ON COLUMN building_tbl.time_zone IS 'The time zone of the building in standard Linux time zone database format. For example, America/New_York, Europe/London, Europa/Amsterdam, Asia/Shanghai are all valid time zones.';

CREATE SEQUENCE floor_seq;

CREATE TABLE floor_tbl (
  id integer PRIMARY KEY DEFAULT nextval('floor_seq') NOT NULL,
  building_id integer NOT NULL,
  label varchar(32) NOT NULL,
  short_global_label varchar(64) NOT NULL,
  short_local_label varchar(16) NOT NULL,
  bounding_polygon polygon NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE,
  FOREIGN KEY (building_id) REFERENCES building_tbl (id)
);

CREATE TRIGGER floor_modts BEFORE UPDATE ON floor_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();

-- foreign key referencing view not supported, so must manually define trigger to enforce referential integrity
CREATE OR REPLACE FUNCTION check_fk_cust_acct_tbl_from_desk_tbl()
RETURNS TRIGGER AS $$
BEGIN
  IF (((TG_OP = 'UPDATE') AND (OLD.id != NEW.id)) OR (TG_OP = 'DELETE')) THEN
    IF EXISTS (SELECT 1 FROM desk_tbl WHERE desk_tbl.account_id = OLD.id) THEN
      RAISE EXCEPTION 'update or delete on table "cust_acct_tbl" violates foreign key trigger "cust_acct_fk_from_desk_tbl" referencing table "desk_tbl"'
        USING DETAIL = 'Key (id)=(' || OLD.id || ') is still referenced from table "desk_tbl".';
    END IF;
  END IF;
  RETURN OLD;
END;
$$ language 'plpgsql';

CREATE SEQUENCE account_seq;

-- stub for cust_acct_tbl. will add the rest of the columns later after sales order form work is done.
CREATE TABLE cust_acct_tbl (
  id integer PRIMARY KEY DEFAULT nextval('account_seq') NOT NULL,
  name varchar(255) NOT NULL,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE,
  UNIQUE(name)
);

CREATE TRIGGER cust_acct_modts BEFORE UPDATE ON cust_acct_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();
CREATE TRIGGER cust_acct_fk_from_desk_tbl BEFORE UPDATE OR DELETE ON cust_acct_tbl FOR EACH ROW EXECUTE PROCEDURE check_fk_cust_acct_tbl_from_desk_tbl();

CREATE VIEW account_vw AS
  SELECT cust_acct_tbl.id,
    'customer'::character varying(16) AS type,
    cust_acct_tbl.name,
    cust_acct_tbl.is_active
  FROM cust_acct_tbl
UNION ALL
  SELECT 1 AS id,
    'corporate'::character varying(16) AS type,
    'Micro Office'::character varying AS name,
    true AS is_active;

-- foreign key referencing view not supported, so must manually define trigger to enforce referential integrity
CREATE OR REPLACE FUNCTION check_fk_cust_pers_tbl_from_desk_tbl()
RETURNS TRIGGER AS $$
BEGIN
  IF (((TG_OP = 'UPDATE') AND (OLD.id != NEW.id)) OR (TG_OP = 'DELETE')) THEN
    IF EXISTS (SELECT 1 FROM desk_tbl WHERE desk_tbl.person_id = OLD.id) THEN
      RAISE EXCEPTION 'update or delete on table "cust_pers_tbl" violates foreign key trigger "cust_pers_fk_from_desk_tbl" referencing table "desk_tbl"'
        USING DETAIL = 'Key (id)=(' || OLD.id || ') is still referenced from table "desk_tbl".';
    END IF;
  END IF;
  RETURN OLD;
END;
$$ language 'plpgsql';

CREATE SEQUENCE person_seq;

CREATE TABLE cust_pers_tbl (
  id integer PRIMARY KEY DEFAULT nextval('person_seq') NOT NULL,
  cust_acct_id integer NOT NULL,
  salutation varchar(16),
  first_name varchar(32) NOT NULL,
  middle_name varchar(32),
  last_name varchar(48) NOT NULL,
  title varchar(80),
  email varchar(254),
  note text,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE,
  UNIQUE(email),
  FOREIGN KEY (cust_acct_id) REFERENCES cust_acct_tbl (id)
);

CREATE TRIGGER cust_pers_modts BEFORE UPDATE ON cust_pers_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();
CREATE TRIGGER cust_pers_fk_from_desk_tbl BEFORE UPDATE OR DELETE ON cust_pers_tbl FOR EACH ROW EXECUTE PROCEDURE check_fk_cust_pers_tbl_from_desk_tbl();

-- foreign key referencing view not supported, so must manually define trigger to enforce referential integrity
CREATE OR REPLACE FUNCTION check_fk_employee_tbl_from_desk_tbl()
RETURNS TRIGGER AS $$
BEGIN
  IF (((TG_OP = 'UPDATE') AND (OLD.id != NEW.id)) OR (TG_OP = 'DELETE')) THEN
    IF EXISTS (SELECT 1 FROM desk_tbl WHERE desk_tbl.person_id = OLD.id) THEN
      RAISE EXCEPTION 'update or delete on table "employee_tbl" violates foreign key trigger "employee_fk_from_desk_tbl" referencing table "desk_tbl"'
        USING DETAIL = 'Key (id)=(' || OLD.id || ') is still referenced from table "desk_tbl".';
    END IF;
  END IF;
  RETURN OLD;
END;
$$ language 'plpgsql';

CREATE SEQUENCE employee_seq;

CREATE TABLE employee_tbl (
  id integer PRIMARY KEY DEFAULT nextval('employee_seq') NOT NULL,
  salutation varchar(16),
  first_name varchar(32) NOT NULL,
  middle_name varchar(32),
  last_name varchar(48) NOT NULL,
  email varchar(254) NOT NULL,
  title varchar(80) NOT NULL,
  company varchar(128),
  username varchar(128) NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER employee_modts BEFORE UPDATE ON employee_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();
CREATE TRIGGER employee_fk_from_desk_tbl BEFORE UPDATE OR DELETE ON employee_tbl FOR EACH ROW EXECUTE PROCEDURE check_fk_employee_tbl_from_desk_tbl();
COMMENT ON TABLE employee_tbl IS 'Employee table. Note there is no password column because authentication is handled outside the application, so the password is handled by Active Directory, htpasswd file, or some other means that is outside the web application.';

CREATE VIEW person_vw AS
  SELECT cust_pers_tbl.id,
    cust_pers_tbl.cust_acct_id AS account_id,
    cust_pers_tbl.salutation,
    cust_pers_tbl.first_name,
    cust_pers_tbl.middle_name,
    cust_pers_tbl.last_name,
    cust_pers_tbl.title,
    cust_pers_tbl.email,
    cust_pers_tbl.note,
    cust_pers_tbl.is_active
  FROM cust_pers_tbl
UNION ALL
  SELECT employee_tbl.id,
    1 AS account_id,
    employee_tbl.salutation,
    employee_tbl.first_name,
    employee_tbl.middle_name,
    employee_tbl.last_name,
    employee_tbl.title,
    employee_tbl.email,
    employee_tbl.note,
    employee_tbl.is_active
  FROM employee_tbl;

-- foreign key referencing view not supported, so must manually define trigger to enforce referential integrity
CREATE OR REPLACE FUNCTION check_fk_desk_tbl_views()
RETURNS TRIGGER AS $$
BEGIN
  IF (NEW.account_id IS NOT NULL) AND (NOT EXISTS (SELECT 1 FROM account_vw WHERE id = NEW.account_id)) THEN
    RAISE EXCEPTION 'insert or update on table "desk_tbl" violates foreign key trigger "desk_tbl_fk_views"'
      USING DETAIL = 'Key (account_id)=(' || NEW.account_id || ') is not present in view "account_vw"';
  END IF;
  IF (NEW.person_id IS NOT NULL) AND (NOT EXISTS (SELECT 1 FROM person_vw WHERE id = NEW.person_id)) THEN
    RAISE EXCEPTION 'insert or update on table "desk_tbl" violates foreign key trigger "desk_tbl_fk_views"'
      USING DETAIL = 'Key (person_id)=(' || NEW.person_id || ') is not present in view "person_vw"';
  END IF;
  RETURN NEW;
END;
$$ language 'plpgsql';

CREATE SEQUENCE desk_seq;

CREATE TABLE desk_tbl (
  id integer PRIMARY KEY DEFAULT nextval('desk_seq') NOT NULL,
  floor_id integer NOT NULL,
  assigned_number integer NOT NULL,
  account_id integer,
  person_id integer,
  num_people integer NOT NULL,
  is_enclosed boolean DEFAULT false NOT NULL,
  list_price numeric(8,2) NOT NULL,
  current_price numeric(8,2) NOT NULL,
  bounding_polygon polygon NOT NULL,
  note text,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE,
  UNIQUE(floor_id, assigned_number),
  FOREIGN KEY (floor_id) REFERENCES floor_tbl (id)
  /*
    PostgreSQL does not allow foreign keys to views, so implement them with a trigger in each of desk_tbl, cust_acct_tbl,
    cust_pers_tbl, and employee_tbl.
  */
  --FOREIGN KEY (account_id) REFERENCES account_vw (id),
  --FOREIGN KEY (person_id) REFERENCES person_vw (id)
);

CREATE TRIGGER desk_modts BEFORE UPDATE ON desk_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();
CREATE TRIGGER desk_tbl_fk_views BEFORE INSERT OR UPDATE ON desk_tbl FOR EACH ROW EXECUTE PROCEDURE check_fk_desK_tbl_views();

CREATE SEQUENCE sales_order_seq;

CREATE TABLE sales_order_tbl (
  id integer PRIMARY KEY DEFAULT nextval('sales_order_seq') NOT NULL,
  replaced_sales_order_id integer,
  licensee_name varchar(255) NOT NULL,
  business_type varchar(255),
  ein varchar(9),
  ssn varchar(9),
  address_1 varchar(255) NOT NULL,
  address_2 varchar(255),
  city varchar(255) NOT NULL,
  state varchar(255) NOT NULL,
  postal_code varchar(24),
  country char(2) NOT NULL,
  phone_number varchar(32),
  authorized_signer_first_name varchar(32) NOT NULL,
  authorized_signer_last_name varchar(48) NOT NULL,
  authorized_signer_title varchar(64),
  authorized_signer_email varchar(254),
  building_id integer NOT NULL,
  licensor_employee_id integer NOT NULL,
  start_date date NOT NULL,
  end_date date NOT NULL,
  mtm_pct numeric(4, 2),
  mtm_amount numeric(10, 2),
  mtm_start_date date NOT NULL,
  security_deposit numeric(10, 2) NOT NULL,
  first_rent numeric(10, 2) NOT NULL,
  last_rent numeric(10, 2) NOT NULL,
  is_signed boolean DEFAULT false NOT NULL,
  licensee_signed_date date,
  licensor_signed_date date,
  is_active boolean DEFAULT true NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  modify_ts TIMESTAMP WITH TIME ZONE,
  FOREIGN KEY (building_id) REFERENCES building_tbl (id),
  FOREIGN KEY (licensor_employee_id) REFERENCES employee_tbl (id)
);

CREATE TRIGGER sales_order_modts BEFORE UPDATE ON sales_order_tbl FOR EACH ROW EXECUTE PROCEDURE update_modify_ts();

COMMENT ON COLUMN sales_order_tbl.replaced_sales_order_id IS 'Can be NULL. If not NULL, then the current sales order completely replaces the sales order with an ID value of <replaced_sales_order_id>.';
COMMENT ON COLUMN sales_order_tbl.ein IS 'Employer Identification Number. Strip out dashes and store only the numbers. Usual format is 12-3456789.';
COMMENT ON COLUMN sales_order_tbl.ssn IS 'Social Security Number. Strip out dashes and store only the numbers. Usual format is 123-45-6789.';
COMMENT ON COLUMN sales_order_tbl.country IS 'ISO 3166-1 alpha-2 code.';
COMMENT ON COLUMN sales_order_tbl.phone_number IS 'Phone number in ITU-T E.123 format.';
COMMENT ON COLUMN sales_order_tbl.is_signed IS 'Has the sales order been signed?';
COMMENT ON COLUMN sales_order_tbl.is_active IS 'Is the sales order currently active?';
COMMENT ON COLUMN sales_order_tbl.create_ts IS 'Creation timestamp of the row. Set by database automatically if not provided by application.';
COMMENT ON COLUMN sales_order_tbl.modify_ts IS 'Last modification timestamp of the row. Updated by database via trigger.';

CREATE TABLE sales_order_x_desk_tbl (
  sales_order_id integer NOT NULL,
  desk_id integer NOT NULL,
  FOREIGN KEY (sales_order_id) REFERENCES sales_order_tbl (id),
  FOREIGN KEY (desk_id) REFERENCES desk_tbl (id)
);

CREATE SEQUENCE prodsvc_item_seq;

CREATE TABLE prodsvc_item_tbl  (
  id integer PRIMARY KEY DEFAULT nextval('prodsvc_item_seq') NOT NULL,
  description text NOT NULL,
  unit_price numeric(10, 2) NOT NULL
);

CREATE SEQUENCE so_li_itm_seq;

CREATE TABLE so_li_itm_tbl (
  id integer PRIMARY KEY DEFAULT nextval('so_li_itm_seq') NOT NULL,
  sales_order_id integer NOT NULL,
  position integer NOT NULL,
  prodsvc_item_id integer NOT NULL,
  description text,
  quantity numeric(11, 6) NOT NULL,
  price numeric(10, 2) NOT NULL,
  amount numeric(10, 2) NOT NULL,
  start_date date,
  end_date date,
  -- sales order should never have two line items with same position
  UNIQUE(sales_order_id, position),
  FOREIGN KEY (sales_order_id) REFERENCES sales_order_tbl (id),
  FOREIGN KEY (prodsvc_item_id) REFERENCES prodsvc_item_tbl (id)
);

COMMENT ON TABLE so_li_itm_tbl IS 'Sales order line item table.';
COMMENT ON COLUMN so_li_itm_tbl.position IS 'Sort order in list of line items. Starts at 0 and increases as you go down the sales order form. No two line items in same sales order should have same position. Can skip over numbers (e.g. go from 3 to 5) if there are blank lines in the order form.';

CREATE TABLE employee_x_role_tbl (
  employee_id integer NOT NULL,
  role varchar(255) NOT NULL,
  create_ts TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  UNIQUE(employee_id, role),
  FOREIGN KEY (employee_id) REFERENCES employee_tbl (id)
);


#!/bin/sh

for i in `grep -i 'create table' create.sql | awk '{print $3}'`;
do
  # the next line assumes PGHOST, PGDATABASE environment variables in ~/.bash_profile and row in ~/.pgpass
  psql -c "DROP TABLE $i CASCADE;"
done
for i in `grep -i 'create sequence' create.sql | awk '{gsub(";$","", $3); print $3}'`;
do
  # the next line assumes PGHOST, PGDATABASE environment variables in ~/.bash_profile and row in ~/.pgpass
  psql -c "DROP SEQUENCE $i CASCADE;"
done

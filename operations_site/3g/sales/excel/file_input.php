<?php

	// get functions
	require 'functions.php';
	
	// check if user is autherized
	authorize_role($_SERVER['REMOTE_USER'], "role:sales/create_sales_order");

?>
<!DOCTYPE html>
<html>
<head>
  <title>Excel to Database</title>
</head>

<body>

 <form action="order_input.php" method="post" enctype="multipart/form-data">
  Select sale order to upload:
    <input type="file" name="excelFile" id="excelFile"> <br>
  <input type="submit" value="Submit">
</form>

</body>

</html>

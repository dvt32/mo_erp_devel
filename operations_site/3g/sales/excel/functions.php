<?php
	/*
		Functions used for the SaleOrderForm
	*/
	
	
	// uses id, role and database connection
	function authorize_role($username, $role)
	{
		
		// get database info
		require '../../lib/get_database_connection.php';
		
		// get country & state codes
		require 'abbreviations.php';
	
		// die if no connection
		if ($connection == false)
		{
			die ("can't connect to the database");
		}
		
		// check if all variables are filled in
		if ( !empty($username) && !empty($role) )
		{
		
			// use username to get user_id
			$user_id = pg_query_params($connection, "SELECT id FROM employee_tbl WHERE windows_logon_name = $1", 
			array( $username) );
			
			$user_id = pg_fetch_row($user_id);
			$user_id = $user_id[0];
			
			
			// check database
			$query = pg_query_params($connection, "SELECT * FROM ss_employee_x_role_tbl WHERE Parent_id = $1 and element_value = $2", 
			array( $user_id, $role ) );
			
			// false if query returns error (example: user id is not an int )
			if ($query == false)
			{
				die ("something wrong with the database / wrong input given");
			}
			
			$query = pg_fetch_row($query);
			
			// deny access to unauthorized users
			if ($query == null)
			{
				die("You do not have the access privilege to view this page");
			}
		}
		else
		{
			die("Authorization went wrong.");
		}
	}
	
	// check if country code is valid
	function country_code($input)
	{
		
		// get country & state codes
		require 'abbreviations.php';
		
		// if country exists
		if ( $countries[$input] != null)
		{
			return true;
		}
		// else
		else
		{
			return false;
		}
	}
	
	// check if state code is valid
	function state_code($input)
	{
		// get country & state codes
		require 'abbreviations.php';
		
		return in_array ( $input , $states);
	}
	
?>

<?php

	/** info to be parsed: **/

	// personal info
	$personal = array(
		"rLicensee",
		"rAddress1",
		"rAddress2",
		"rCity",
		"rState",
		"rPostalCode",
		"rCountry",
		"rPhone",
		"rAuthorizedSignerFirstName",
		"rAuthorizedSignerLastName",
		"rAuthorizedTitle",
		"rAuthorizedEmail",
		"rBusinessType",
		"rEIN",
		"rSSN");

	// office spaces
	$office_input = "rOfficeSpaces";
	
	// order info
	$order = array(
		"rFloorID",
		"rMOMEmployee",
		"rOrderID",
		"rOrderIDReplaced");
		
	// one time items
	$requirements = array(
	"rSecRequirement",
	"rFirstMonthRent",
	"rFinalMonthRent");

	// dates
	$dates = array(
		array("rStartDate", "Initial Term Start"),
		array("rEndDate", "Initial Term End") );
		
	// not null or other criteria
	$check = array(
		"rLicensee",
		"rAddress1",
		"rCity",
		"rState",
		"rOrderID",
		"rCountry",
		"rAuthorizedSignerFirstName",
		"rAuthorizedSignerLastName",
		"rMOMEmployee",
		"rSecRequirement",
		"rFirstMonthRent",
		"rFinalMonthRent",
		"rStartDate",
		"rEndDate",
		"rFloorID");
		
	// Json array
	$Json;
	
	// The resulting HTML page with excel info
	$page;
	
	// back button
	$back = '<br> <a href="" onClick="history.go(-1);return true;">Back</a>';

?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>

		<?php
		
			// get functions
			require 'functions.php';
			
			// get database info
			require '../../lib/get_database_connection.php';

			// check if user is autherized
			authorize_role($_SERVER['REMOTE_USER'], "role:sales/create_sales_order");
			
			$errors = '<ol>';
			
			// if page is reached through form	
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				
				// Include path 
				set_include_path(get_include_path() . PATH_SEPARATOR . '/usr/share/php');
				
				// PHPExcel_IOFactory
				include 'PHPExcel/IOFactory.php';

				
				// check if file existst
				if ($_FILES['excelFile']['name'] == null)
				{
					die('no file selected'. $back);
				}
				
				
				// check file extension
				$ext = pathinfo($_FILES['excelFile']['name'], PATHINFO_EXTENSION);

				if ( stripos ( $ext , "xls") === false )
				{
					die('Error loading file. Wrong file extension' . $back);
				}				
				
				// check input
				$inputFileName = $_FILES['excelFile']['tmp_name'];
				try 
				{
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} 
				catch(Exception $e) 
				{
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage() . $back);
				}
				
				// fixes wrong date bug. Library uses UTC so set time to utc to prevent wrong dates while converting
				date_default_timezone_set('UTC');
				
				$page = "<div class='wrapper'>";
				
				// check if mandatory fields are filled in
				foreach ($check as $info)
				{
					// check country (ISO 3166-1 alpha-2 code)
					if ($info == "rCountry")
					{
						// get code & remove spaces
						$code = $objPHPExcel->getActiveSheet()->getCell($info)->getValue();
						$code = trim($code);
						
						// check if country code is valid
						if (!country_code($code) )
						{
							$code_cell = $objPHPExcel->getActiveSheet()->getCell($info)->getColumn();
							$code_cell = $code_cell . $objPHPExcel->getActiveSheet()->getCell($info)->getRow();
							
							$errors = $errors . '<li>Sorry, Invalid country in Cell ' . $code_cell . 
							'. Please check "ISO 3166-1 alpha-2 code" for the correct input type</li>';
						}
						
						// validate state
						if ($code == "US")
						{
							// get code & remove spaces
							$code = $objPHPExcel->getActiveSheet()->getCell('rState')->getValue();
							$code = trim($code);
							
							// check if state exists
							if (!state_code($code) )
							{
								$state_cell = $objPHPExcel->getActiveSheet()->getCell('rState')->getColumn();
								$state_cell = $state_cell . $objPHPExcel->getActiveSheet()->getCell('rState')->getRow();
								
								$errors = $errors . '<li>Sorry, Invalid state abbreviation in cell ' . $state_cell . 
								'. Please check the usps standard <a href="http://en.wikipedia.org/wiki/List_of_U.S._state_abbreviations"> Here </a> </li>';
							}
						}
					}
						
					// validate order id
					if ($info ==  "rOrderID")
					{
						// get code & remove spaces
						$id = $objPHPExcel->getActiveSheet()->getCell('rOrderID')->getValue();
						$id = trim($id);
						$result = pg_query_params($connection, "SELECT id FROM sales_order_tbl WHERE id = $1", array($id) );
						$result = pg_fetch_row($result);
						
						if ( $result != NULL)
						{
							$errors = $errors . '<li>Sorry, an order already exists in the database with the same order ID</li>';
						}
						
						
						
						
					}
					
					// check if building exists
					else if ($info == "rFloorID")
					{
						$building = $objPHPExcel->getActiveSheet()->getCell($info)->getValue();

						// search floor with same short global label
						$result = pg_query_params($connection, "SELECT building_id FROM floor_tbl WHERE short_global_label = $1", 
									array( $building ) );
						
						$building_id = pg_fetch_row($result);
						
						// if query gives error or no results
						if ( $result == false || $building_id == null)
						{
							$building_cell = $objPHPExcel->getActiveSheet()->getCell($info)->getColumn();
							$building_cell = $building_cell . $objPHPExcel->getActiveSheet()->getCell($info)->getRow();
							
							$errors = $errors . '<li>Sorry, Invalid building selected in Cell ' . $building_cell. ' .</li>';
						}
						
						// save building id for office spaces
						$building_id = $building_id[0];
					}
					
					// check if employee is set
					else if ($info == "rMOMEmployee")
					{
						$name = $objPHPExcel->getActiveSheet()->getCell($info)->getValue();
						
						
						// split first/last name
						$name = explode( ',' , $name );
						
						// trim whitespace
						$name[0] = trim ($name[0]);
						$name[1] = trim ($name[1]);
						
						// search employees list
						$result = pg_query_params($connection, "SELECT * FROM employee_tbl WHERE first_name = $2 and last_name = $1", $name );
						
						// if query gives error or no results
						if ( $result == false || pg_fetch_row($result) == null)
						{
							$errors = $errors . '<li>Sorry, Employee ' . $name[0] . ',' . $name[1] . ' does not exist.</li>';
						}
						
						
					}
					
					// check for null
					else if ( $objPHPExcel->getActiveSheet()->getCell($info)->getValue() == NULL )
					{
						$tmp_cell = $objPHPExcel->getActiveSheet()->getCell($info)->getColumn();
						$tmp_cell = $tmp_cell . $objPHPExcel->getActiveSheet()->getCell($info)->getRow();
						$errors = $errors . "<li> Sorry, " . $info .' is not filled in in cell ' . $tmp_cell . '. </li>';
					}
					
				}
				
				// Gets licensee info
				$page = $page . "<div class='info'> <h2> Licensee info </h2>";
				foreach ($personal as $info)
				{
					
					$tmp = $objPHPExcel->getActiveSheet()->getCell($info)->getFormattedValue(); 
					
					// remove whitespaces on beginning & end
					$tmp = trim($tmp);
					
					// replace all instances of multiple whitespace with a single space
					$tmp = preg_replace( '/\s+/' , " " , $tmp);
					
					// save info
					$page = $page . $info . ": " . $tmp . "<br>";
					$Json[] = array($info, $tmp );
					
				}
				$page = $page . '</div>';
	
				// Get order info
				$page = $page . "<div class='info'> <h2> Office info </h2>";
				foreach ($order as $info)
				{
					
					$tmp = $objPHPExcel->getActiveSheet()->getCell($info)->getFormattedValue(); 
					
					// remove whitespaces on beginning & end
					$tmp = trim($tmp);
					
					// replace all instances of multiple whitespace with a single space
					$tmp = preg_replace( '/\s+/' , " " , $tmp);
					
					// save info
					$page = $page . $info . ": " . $tmp . "<br>";
					$Json[] = array($info, $tmp );
				}
				
				// validate offices
				$offices = $objPHPExcel->getActiveSheet()->getCell($office_input)->getFormattedValue(); 
				
				//get cell location
				$offices_cell = $objPHPExcel->getActiveSheet()->getCell($office_input)->getColumn();
				$offices_cell = $offices_cell . $objPHPExcel->getActiveSheet()->getCell($office_input)->getRow();
				
				
				$page = $page . $office_input . ": " . $offices . "<br>";
				
				// remove spaces
				$offices = str_replace ( ' ' , "" , $offices );
				
				//split offices
				$offices = explode(",", $offices);
				
				
				$desks;
				// for each office
				foreach($offices as $office)
				{
					if ($office != "")
					{
						// split floor and desk
						$split = explode("D", $office);
					

						// remove the F
						$split[0] =  str_replace ('F' , "" , $split[0] );
					
						// removes all zeros before first real digit
						$split[0] = ltrim( $split[0] , "0");
					
					
						// if Floor is les then 10
						if ($split[0] < 10)
						{
						
							$split[0] = '0' . $split[0];
						
						}
						// add F 
						$split[0] = 'F' . $split[0];
					
						// get floor id
						$tmp = pg_query_params($connection, "SELECT id FROM floor_tbl WHERE short_local_label = $1 and building_id = $2", array($split[0], $building_id) );
					
						$floor_id = pg_fetch_row($tmp);
					
						// get desk id
						$result = pg_query_params($connection, "SELECT id FROM desk_tbl WHERE floor_id = $1 and assigned_number = $2", array($floor_id[0], $split[1]) );
					
						$tmp2 = pg_fetch_row($result);
					
						if ($result == false || $tmp2 == null || $tmp == false || $floor_id == null)
						{
							$errors = $errors . '<li>You entered a desk incorrectly in Cell ' . $offices_cell . '. Your entry was: '. $office . '. <br>
							Please enter desks in the format FxDy where x is the floor number and y is the desk number on that floor. <br>
							You can enter multiple desks by separating them with commas. For example: F3D2, F3D20, F6D17.</li>';
						}
					
						$desks[] = $tmp2[0];
					}
				}
				// save desk numbers
				$Json[] = array($office_input, $desks );

				
				$page = $page . '</div>';
				
				
				// Gets dates info (Q16, T21, U21)
				$page = $page . " <div class='info'> <h2> Dates </h2>";
				foreach ($dates as $info)
				{
					$tmp = $objPHPExcel->getActiveSheet()->getCell($info[0])->getCalculatedValue(); 
					$page = $page . $info[1] . ": " . date( "m-d-y" , PHPExcel_Shared_Date::ExcelToPHP($tmp) ) . "<br>" ;
					
					$Json[] = array($info[1], date( "m-d-y" , PHPExcel_Shared_Date::ExcelToPHP($tmp) ) );
				}
				
				
				// get row/columns from range
			 	$var =  $objPHPExcel->getActiveSheet()->namedrangeToArray('rRecurringItems', NULL, FALSE, TRUE, TRUE);
			 	
			 	// get first row
			 	reset($var);
				$first_row = key($var);
				
				// get last row
				end($var);         
				$last_row = key($var);
				
				// get firt column
				reset($var[$first_row]);
				$first_column = key($var[$first_row]);
				
				// get last column
				end($var[$last_row]);
				$last_column = key($var[$last_row]);
				
				// all recurring items
				$column = $first_column;
				$row = $first_row;
				
				// start table
				$page = $page . " </div> <div class='table'> <h2>Items </h2>";
				$page = $page . "<table> 
				<tr> 
					<td>name</td>
					<td>description</td>
					<td>duration</td>
					<td>QTY</td>
					<td>price</td>
					<td>amount</td>
					<td>begindate</td>
					<td>enddate</td>
				</tr>";
				
				// start line items
				$Json[] = array("begin line-items");
				
				// loop through items
				while($column != $last_column && $row != $last_row)
				{
					
					// reset column
					$column = $first_column;
					
					// name
					$name = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getValue() ;
					
					// if row is not empty
					if ($name != "")
					{
						$page = $page . '<tr><td>' .$name . '</td>';
						$column++;
					
						// description
						$desc = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
						$page = $page . '<td>'. $desc . '</td>';
						$column++;

						// duration
						$duration = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getValue();
						
						if ($duration != "")
						{
							$page = $page . '<td>'. $duration . ' Months</td>';
						}
						
						else
						{
							$page = $page . '<td></td>';
						}
						$column++;
						
						
						// Qty
						$qty = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getValue();
						
						// if qty is empty
						if ($qty == "")
						{
							// quantity is 1
							$qty = 1;
						}
						
						$page = $page . '<td>'. $qty . '</td>';
						$column++;
						$column++;
						
						// price
						$price = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
						$page = $page . '<td align="right"> $ '. $price . '</td>';
						$column++; 
				

						// amount
						$amount = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
						$page = $page . '<td align="right"> $ '. $amount . '</td>';
						$column++; 
						$column++; 
						
						// dates
						for ($i = 0; $i < 2; $i++)
						{
							$tmp =  $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
							$page = $page . "<td>";
							
							if ($tmp != "")
							{
								$tmp = date( "m-d-y" , PHPExcel_Shared_Date::ExcelToPHP($tmp) ) ;
								$page = $page . $tmp;
							}
							else
							{
								$tmp = null;
							}
							
							// save date for Json
							if ($i == 0)
							{
								$begin = $tmp;
							}
							if ($i == 1)
							{
								$end = $tmp;
							}
							
							$page = $page . "</td>";
							$column++;
						}
						$page = $page . "</tr>";
						
						// check for NULL
						if ($qty == "" || $price == "" || $amount == "")
						{
							$errors = $errors . '<li>Not all list items are completely filled in (item, quantity, price & amount) in Row: ' .
							$row . '.</li>';
						}
						
						$Json[] = array($name, $desc, $duration, $qty, $price, $amount, $begin, $end);
					}
						// advance one row
						$row++;
				}
				$page = $page . "<tr>";
				
				// end line items
				$Json[] = array("end line-items");
				
				// get premium line
				$column = $objPHPExcel->getActiveSheet()->getCell("rM2MPremium")->getColumn() ;
				$row = $objPHPExcel->getActiveSheet()->getCell("rM2MPremium")->getRow();
				
				// name
				$page = $page . "<td></td>";
				
				// description
				$name = "Premium for Month-to-Month Flexibility Begins after";
				$page = $page . "<td> Premium for Month-to-Month Flexibility Begins after </td>";
				
				
				// duration
				$desc = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
				$page = $page . "<td>" . $desc . " Months</td>";
				$column++;
			
				// QTY
				$qty = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getFormattedValue();
				$page = $page . "<td>" .  $qty . "</td>";
				$column++;
				$column++;
				
				// price
				$price = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
				$page = $page . "<td align='right'>$  " .  $price . "</td>";
				$column++; 
				
				// amount
				$amount = $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
				$page = $page ."<td align='right'>$ ". $amount  . "</td>";
				$column++; 
				$column++; 
				
				// get dates
				for ($i = 0; $i < 2; $i++)
				{
					$tmp =  $objPHPExcel->getActiveSheet()->getCell( $column . $row )->getOldCalculatedValue();
					
					if ($tmp != null)
					{
						$tmp = date( "m-d-y" , PHPExcel_Shared_Date::ExcelToPHP($tmp) );
						
					}

					
					// save dates 
					if ($i == 0)
					{
						$begin = $tmp;
					}
					if ($i == 1)
					{
						$end = $tmp;
					}
					
					$page = $page . "<td>" .  $tmp . "</td>";
					$column++;
				}
				
				if ($begin == "")
				{
					//die('Month to Month premium has no begin date' . $back);
				}
				$page = $page ."</tr>  </table> <br>";
				
				// add premium to Json
				$Json[] = array($name, $desc, $qty, $price, $amount, $begin, $end);
				
				// get one-time items
				$page = $page . "<table class='table2'>";
				
				foreach( $requirements as $info)
				{
					$page = $page . "<tr>";
					
					
					$tmp = $objPHPExcel->getActiveSheet()->getCell($info)->getOldCalculatedValue();
					
					$Json[] = array($info, $tmp);
					
					$page = $page ."<td>" . $info . " </td> <td> $ " . $tmp . "</td>";
					
					$page = $page . "</tr>";
				}
				
				$page = $page . "</table>";
				
				$notes = $objPHPExcel->getActiveSheet()->getCell("rNotes");
				
				// remove "Notes:" from notes
				if (strrpos ( $notes, "Notes: ") == 0)
				{
					$notes = substr($notes, 7);
				}
				
				// print notes
				$page = $page . $notes . "<br>" ;
				
				// add notes
				$Json[] = array("notes", $notes);
				
				$Json = json_encode($Json) ;
				
			
				
				// get id
				$id = pg_query($connection, "SELECT nextval('order_form_preprocess_seq') as id;");
				$id = pg_fetch_array($id);
				$id = $id["id"];
				
				// save json in database
				$result = pg_query_params($connection, "INSERT INTO order_form_preprocess_tbl(id, form_data)
				VALUES ($1, $2)" , array($id , $Json) );
				
				// if query did not work

				if (!$result) 
				{
  					die( 'An error occurred with the database :(' . $back);
  				}
  				
				
			}
			else
			{
				// if not accessed through post, redirect
				header("Location: file_input.php");
				exit;
			}
			
			if ($errors != '<ol>')
			{
				die( $errors . '</ol>' . $back);
			}
			else
			{
				echo $page;	
			}
		?>
			<br>
			<form action="../order_result.php" method="post">
				<input type="hidden" name="order_form_preprocess_id" value="<?= $id ?>">
				<input type="submit" value="Save">
				<input Type="button" value="Back" onClick="history.go(-1);return true;">
			</form> 
			
		</div>
	</body>
</html>

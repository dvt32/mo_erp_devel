<?php

	/*	
	* 	This file is called from the SOF result page. 
	*	The variable declarations are in there so the 
	*	same query & parsing is not done twice.
	*
	*	After creating the pdf, 
	*	it sends the file to Docusign.php & removes the local copy
	*/	
	
	
	// Include the main TCPDF library (search for installation path).
	require_once('/usr/share/php/tcpdf/tcpdf.php');
	
	$agreement = 
	'By signing this order form, Licensee agrees to be bound by the License Agreement at: https://www.microoffice.com/agreement/licenseny20150113v1.pdf with SHA‐512 digest
	d0f2689c32a4d13b78e3b0ed26cb131eaa48b847c4058dc0637662a737b5b203403d13ced4df71ad03e9d50b05fa440e32d0a955f8c5275193bc04520a8f7adb';
	
	$phone_number =  str_replace(' ', '', $phone_number);
	
	// first licensee line
	$licensee1 =
	'Licensee: ' .
	$licensee_name .'- ' . 
	$address_1 . 
	$address_2. ', ' .
	$city . ', ' .
 	$state . ', '. 
 	$postal_code . ', '.  
 	$country . ', ' . 
 	$phone_number . ', ' .
 	$authorized_signer_email;
	
	// second licensee line
	$Licensee2 = 
	"Authorized Signer: " . $authorized_signer_first_name . ' ' . $authorized_signer_last_name . '; Phone: ' . $phone_number;
	
	// third licensee line	
	
	$officeprint;
	foreach ($offices as $office)
	{
		$query = pg_query_params($connection, "SELECT floor_id, assigned_number FROM desk_tbl WHERE id = $1",array($office) );
		$desk = pg_fetch_row($query);
		if ($desk == null)
		{
			die("Desk not found");
		}
		$query = pg_query_params($connection, "SELECT short_local_label from floor_tbl where id = $1", array($desk[0]) ); 
		$floor = pg_fetch_row($query);
		$officeprint = $officeprint . $floor[0] . 'D' . $desk[1]. ',';
	}
	
	
	$licensee3 = 
	"Shared Office Space Number(s): " . rtrim($officeprint, ",") ."; Business Type: " . $business_type;
	
	// initial start date
	$tmp = date_create_from_format("m-d-y" ,$start_date);
	$initStart = $tmp->format('n/j/Y');
	$tmp = date_create_from_format("m-d-y" ,$end_date);
	$initEnd = $tmp->format('n/j/Y');
	
	// one time items
	$onetime = array(
	array("Security Requirement: ", $security_deposit),
	array("First Month's Rent Requirement: ", $first_rent),
	array("Final Month's Rent Requirement: ", $last_rent),
	);
	
	// custom Header & remove footer
	class MYPDF extends TCPDF 
	{

		//Page header
		public function Header() 
		{
	
			date_default_timezone_set('America/New_York');
		    // Set font
		    $this->SetFont('helvetica', 'b', 9);
		    
		    // header
		    $this->ln(2);
		    $this->MultiCell(100, 0, 'Micro Office Sales Order Form 6.4', 0, 'L', 0, 0, '', '', false);
		    $this->MultiCell(120, 0, 'Order Form Creation Date: ' . date('n/j/Y g:i A'), 0, 'R', 0, 0, '150', '', false);
		}
		
		// overide footer -> empty
		public function footer() 
		{

		}
		
	   
		
		
		}
	// create new PDF document
	$pdf = new MYPDF("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Marius de Vogel');
	$pdf->SetTitle('Micro Office Sales Order Form 6.4');
	$pdf->SetSubject('Micro Office Sales Order Form 6.4');

	// set header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// Line Length
	$lineLeft = 15;
	$lineRight = 282;

	// -------- PAGE START --------



	// add a page
	$pdf->AddPage();


	// licensee info
	$pdf->SetFont('helvetica', 'b', 9);
	

 	
	$licenseePrintableInfo = <<<EOD
$licensee1

$Licensee2
$licensee3
EOD;

	$licensor = "Licensor: Millennium Park Suites LLC   -   Licensor Address: 405 W Superior Street, Chicago, IL 60654";


	// print text
	$pdf->Write(0, $licenseePrintableInfo, '', 0, 'L', true, 0, false, false, 0);
	$pdf->SetFont('helvetica', '', 9);
	$pdf->Write(0, $licensor, '', 0, 'L', true, 0, false, false, 0);
	$pdf->MultiCell(100, 0, 'Main Landlord: 405 W Superior Property Investors LLC', 0, 'L', 0, 0, '', '', false);
	$pdf->MultiCell(30, 0, 'Order ID #' . $id, 0, 'R', 0, 0, '145', '', false);
	$pdf->ln();

	$pdf->Image('/var/www/html/3g/pdf/tcpdf/logo.png',200,20,0,30,'','','',false,300,'',false,false, 0,false,false,false,false, array() );

	$pdf->SetFont('helvetica',"", 6);		
	$pdf->MultiCell(100, 0, 'Designation of Overlease Premises: Entire Third Floor', 0, 'L', 0, 0, '', '', false);
	$pdf->MultiCell(120, 0, 'Building Address: 405 W Superior Street, Chicago, IL 60654', 0, 'R', 0, 0, '150', '', false);
	$pdf->SetFont('helvetica', '', 9);

	// HTML as input
	$start = '
	<table>
		<tr>
		    <td width="19%"><u>Item</u></td>
		    <td width="30%"><u>Description</u></td>
		    <td width="10%"><u>Duration</u></td>
		    <td width="5%"><u>Qty</u></td>
		    <td width="10%"><u>Price, $</u></td>
		    <td width="10%"><u>Amount, $</u></td>
		    <td width="8%"><u>Start</u></td>
		    <td width="8%"><u>End</u></td>
		</tr>
		<tr>
		    <td colspan="9"></td>
		</tr>
		<tr bgcolor="#F2F2F2">
		    <td colspan="5"><b>Recurring Items</b></td>
		    <td>Initial Term:</td>
		    <td>'. $initStart .'</td>
		    <td>'. $initEnd .'</td>
		</tr>
		<tr>
		    <td colspan="9"></td>
		</tr>
	';

	$pdf->ln(7);



	
	foreach($lines as $item)
	{
		$start = $start . 
		'<tr>
		<td color="#2020FF">' . $item[0] . '</td> 
		<td>' . $item[1] . '</td> 
		<td color="#2020FF">' . $item[2] . '</td> 
		<td color="#2020FF"> ' . $item[3] . '</td> 
		<td>' . number_format($item[4] , 2, '.', ' '). '</td> 
		<td>' . number_format($item[5] , 2, '.', ','). '</td> 
		<td>' . $item[6] . '</td> 
		<td>' . $item[7] . '</td> 
		</tr>';

	}
	$count = count($lines) + 1;
	while ($count < 10)
	{
		$start = $start . 
		"<tr><td colspan='8'></td></tr>";
		$count++;
	}

	// line style
	$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0) );

	// print lines
	$pdf->Line($lineLeft, 55, $lineRight, 55, $style);
	$pdf->Line($lineLeft, 70, $lineRight, 70, $style);
	$pdf->Line($lineLeft, 117, $lineRight, 117, $style);
	
	$tmp = date_create_from_format("m-d-y" ,$mtm_start_date);
	$mtm_start = $tmp->format('n/j/Y');
	
	
	$d1 = date_create_from_format("m-d-y" ,$start_date);
	$d2 = date_create_from_format("m-d-y" ,$mtm_start_date);
	$difference = $d1->diff($d2);
	
	// M2M
		$start = $start . 
		'<tr>
			<td></td> 
			<td>Premium for Month-to-Month Flexibility Begins after</td> 
			<td color="#2020FF">' . $difference->format("%m Months") . '</td> 
			<td>'. $mtm_pct . '%</td> 
			<td></td> 
			<td>'.  number_format($mtm_amount , 2, '.', ' ').'</td> 
			<td>'. $mtm_start .'</td> 
			<td></td> 
		</tr>';

	// start One-time items
	 $start = $start . '
	 
		<tr bgcolor="#F2F2F2">
		    <td colspan="8"><b>One-Time Items</b></td>
		</tr>
		<tr>
			<td colspan="8"></td>
		</tr>';
		
	$totalReq;
	   foreach($onetime as $item)
	   {
	   		$start = $start . '
		<tr>
		    <td colspan="4" align="right">' . $item[0].'</td>
		    <td></td>
		    <td>'. number_format($item[1], 2, '.', ' ') .'</td>
		    <td></td>
		    <td></td>
		</tr>';
		
		$totalReq += $item[1];
	   }

	// print html
	$pdf->writeHTML($start . "</table>", true, false, false, false, '');

	// total
	$pdf->Line($lineLeft, 137, $lineRight, 137, $style);
	$pdf->SetFont('', 'B');
	$pdf->MultiCell(100, 0, 'Total Required for Move-in: ', 0, 'L', 0, 0, '100', '', false);
	$pdf->MultiCell(50, 0, '$' .  number_format($totalReq, 2, '.', ' ') , 0, 'R', 0, 0, '115', '', false);
	$pdf->Line($lineLeft, 142, $lineRight, 142, $style);
	$pdf->ln(5);

	// signing lines
	$pdf->Line(200, 160, $lineRight, 160, $style);
	$pdf->Line($lineLeft, 160, 120, 160, $style);

	// signer info
	$pdf->SetFont('',"", 6);		
	$pdf->MultiCell(100, 0, 'Agreed by Authorized Signer for Licensee: ' . $licensee_name , 0, 'L', 0, 0, '', '', false);
	$pdf->MultiCell(120, 0, 'Agreed by Authorized Signer: Millennium Park Suites LLC', 0, 'R', 0, 0, '150', '', false);
	$pdf->SetFont('',"", 9);	
	$pdf->ln(14);
	$pdf->MultiCell(30, 0, 'Date:', 0, 'R', 0, 0, '75', '', false);
	$pdf->MultiCell(30, 0, 'Date:', 0, 'R', 0, 0, '235', '', false);
	$pdf->ln(4);
	$pdf->MultiCell(100, 0, $authorized_signer_first_name . ', ' . $authorized_signer_title , 0, 'L', 0, 0, '16', '', false);
	$pdf->MultiCell(81, 0, 'Gibson, Kyle, Community Manager', 0, 'L', 0, 0, '200', '', false);
	$pdf->ln(15);

	// agreement
	$pdf->SetFont('',"", 6);	
	$pdf->Write(0, $agreement, '', 0, 'L', true, 0, false, false, 0);
	$pdf->SetFont('',"", 9);	


	// notes
	$pdf->SetFillColor(5,5,5,0,false, '');	
	$pdf->Cell(0,0,"Notes: ",0,0,'L',true,0);
	// -------- PAGE END ---------

	//Close and output PDF document
	$pdf->Output($_SERVER['DOCUMENT_ROOT'].'OrderForm' . $id, 'F');
		
	// call docusign
	$documentName = 'OrderForm' . $id;	
	include("docusign.php");
	
	// delete local file
	unlink($_SERVER['DOCUMENT_ROOT'].'OrderForm' . $id);
	
?>

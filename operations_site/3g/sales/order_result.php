<?php
			
			// get functions
			require 'functions.php';
			
			// get database info
			require '../lib/get_database_connection.php';
			
			// check if user is autherized
			authorize_role($_SERVER['REMOTE_USER'], "role:sales/create_sales_order");
			
			// if page is reached through form
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				
				
				// Get Json if preprocess id exists
				$result = pg_query_params($connection, "SELECT form_data FROM order_form_preprocess_tbl WHERE id = $1", array( $_POST["order_form_preprocess_id"] ) );
				
				// if no result
				if (!$result) 
				{
					// give error
  					die("An error occurred while getting the order. ");
  				}
  				
  				// start splitting Json into variables
  				else
  				{
  					$result = pg_fetch_row($result);
  					$result =  json_decode( $result[0]);
  				
  					
  					// change format & split info for sales_order_tbl
  					foreach ($result as $line)
  					{
  						// licensee name
  						if ($line[0] == "rLicensee")
  						{
  							$licensee_name = $line[1];						
  						}
  						
  						// business type
  						else if ($line[0] == "rBusinessType")
  						{
  							$business_type = $line[1];
  						}
  						
  						// adress1
  						else if ($line[0] == "rAddress1")
  						{
  							$address_1 = $line[1];
  						}
  						
  						// adress2
  						else if ($line[0] == "rAddress2")
  						{
  							$address_2 = $line[1];
  						}
  						
  						// city
  						else if ($line[0] == "rCity")
  						{
  							$city = $line[1];
  						}
  						
  						// state
  						else if ($line[0] == "rState")
  						{
  							$state = $line[1];
  						}
  						
  						// postal code
  						else if ($line[0] == "rPostalCode")
  						{
  							$postal_code = $line[1];
  						}
  						
  						// country
  						else if ($line[0] == "rCountry")
  						{
  							$country = $line[1];
  						}
  						
  						// ein
  						else if ($line[0] == "rEIN")
  						{
  							// remove "-" from EIN
  							$ein = str_replace('-', '', $line[1]);
  							
  						}
  						// ssn
  						else if ($line[0] == "rSSN")
  						{
  							// remove "-" from SSN
  							$ssn = str_replace('-', '',$line[1]);
  							
  						}
  						
  						// phone number
  						else if ($line[0] == "rPhone")
  						{
  							// remove all spaces
  							$info[$count] = str_replace(' ', '', $line[1]);
  							
  							$length = strlen($line[1]) - 4;
  							
  							// add spaces in correct places
  							while ($length > 0)
  							{
  								$line[1] = substr_replace($line[1], " ", $length, 0);
  								$length = $length - 4;
  							}
  							$phone_number = $line[1];
  						}
  						
  						// replaced_sales_order_id
  						else if ($line[0] == "rOrderIDReplaced")
  						{
  							if ($line[1] != "")
  							{
  								$replaced_sales_order_id = $line[1];
							}
							else
							{
								$replaced_sales_order_id = NULL;
							}
  						}
  						
  						// signer name
  						else if ($line[0] == "rAuthorizedSignerFirstName")
  						{
  							$authorized_signer_first_name = $line[1];
  						}
  						
  						// signer name
  						else if ($line[0] == "rAuthorizedSignerLastName")
  						{
  							$authorized_signer_last_name = $line[1];
  						}
  						
  						// signer title
  						else if ($line[0] == "rAuthorizedTitle")
  						{
  							$authorized_signer_title = $line[1];
  						}
  						
  						// signer mail
  						else if ($line[0] == "rAuthorizedEmail")
  						{
  							$authorized_signer_email = $line[1];
  						}
  						
  						// employee id
  						else if ($line[0] == "rMOMEmployee")
  						{
  							if ( is_numeric($line[1]))
  							{
  								$licensor_employee_id = $line[1];
  							}
  							else
  							{
	  							// split first/last name
								$name = explode( ',' , $line[1]);
						
								// trim whitespace
								$name[0] = trim ($name[0]);
								$name[1] = trim ($name[1]);
						
								// search employees list
								$query = pg_query_params($connection, "SELECT id FROM employee_tbl WHERE first_name = $2 and last_name = $1", $name );
						
								$name = pg_fetch_row($query);
							
								// if query gives error or no results
								if ( $query == false || $name == null)
								{
									die('Sorry, Employee does not exist.' . $back);
								}
							
								// save id
								$licensor_employee_id = $name[0];
							}
  						}	
  						
  						// office spaces
  						else if ($line[0] == "rOfficeSpaces")
  						{
	  						
	  						$offices = $line[1];
	  						
	  					
	  					}
  						// order id
  						else if ($line[0] == "rOrderID")
  						{
	  						$id = $line[1];
	  					}
	  					
	  					// building adress
  						else if ($line[0] == "rFloorID")
  						{

							// search floor with same short global label
							$label = pg_query_params($connection, "SELECT building_id FROM floor_tbl WHERE short_global_label = $1", 
										array( $line[1] ) );
						
							$tmp = pg_fetch_row($label);
							
							// if query gives error or no results
							if ( $label == false || $tmp == null)
							{
								echo pg_last_error($connection) . "<br>";
								die('Sorry, Invalid building.' . $back);
							}
							
							$building_id = $tmp[0];
	  					}
	  					
	  					// start date
  						else if ($line[0] == "Initial Term Start")
  						{
	  						$start_date = $line[1];
	  					}
	  					
	  					// end date
  						else if ($line[0] == "Initial Term End")
  						{
	  						$end_date = $line[1];
	  					}
	  					
	  					// mtm info
  						else if ( strrpos($line[0],"Premium for Month-to-Month Flexibility Begins after") !== false)
  						{
							
							$mtm_pct =  preg_replace("/[^0-9,]/", "", $line[2]);
							$mtm_amount = $line[4];
							$mtm_start_date = $line[5];
							
	  					}
	  					
	  					// security deposit
  						else if ($line[0] == "rSecRequirement")
  						{
							$security_deposit = $line[1];
	  					}
	  					
	  					// first rent
  						else if ($line[0] == "rFirstMonthRent")
  						{
							$first_rent = $line[1];
	  					}
	  					
	  					// last rent
  						else if ($line[0] == "rFinalMonthRent")
  						{
							$last_rent = $line[1];
	  					}

  					}

  					// find all line items
  					$begin = array_search( array("begin line-items"), $result) + 1;
  					$end = array_search(array("end line-items"), $result); 
  					
  					// line items array
  					$lines = array_slice($result, $begin, ($end - $begin) );
					
					
					// get prodsvc_item_tbl ids before transaction
					$prodsvc;
					foreach($lines as $line)
  					{
						// get item id from prodsvc_item_tbl
						$id_query = pg_query_params($connection, "select id from prodsvc_item_tbl where description = $1", array( $line[1] ) );
					
					
						$tmp = pg_fetch_row($id_query);
						
						// if query gives error or no results
						if ( $id_query == false || $tmp == null)
						{
							$prodsvc = false;
							die( "Prodsvc_item_tbl does not contain '". $line[1] . "'<br>");
						}
						
						// add prodsvc_item_tbl_id to array
						$prodsvc[] = $tmp[0];
						
  					}

  					// if query 2 & 3 are not reached, still execute query1
  					$query2 = true;
  					$query3 = true;
  					
					// start transaction
					pg_query("BEGIN") or die("Could not start transaction\n");

					// sales_order_tbl
					$query1 = pg_query_params($connection, "INSERT INTO sales_order_tbl(
						id,
						replaced_sales_order_id,
						licensee_name,
						business_type,
						ein,
						ssn,
						address_1,
						address_2,
						city,
						state,
						postal_code,
						country,
						phone_number,
						authorized_signer_first_name,
						authorized_signer_last_name,
						authorized_signer_title,
						authorized_signer_email,
						licensor_employee_id,
						building_id,
						start_date,
						end_date,
						mtm_pct,
						mtm_amount,
						mtm_start_date,
						security_deposit,
						first_rent,
						last_rent
						)
					VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27);" , array( 
						$id, 
						$replaced_sales_order_id , 
						$licensee_name, 
	  					$business_type, 
	  					$ein,
	  					$ssn ,
	  					$address_1 ,
	  					$address_2,
	  					$city ,
	  					$state ,
						$postal_code, 
						$country ,
						$phone_number , 
						$authorized_signer_first_name,
						$authorized_signer_last_name,
						$authorized_signer_title ,
						$authorized_signer_email ,
						$licensor_employee_id, 
						$building_id, 
						$start_date,
						$end_date,
						$mtm_pct,
						$mtm_amount,
						$mtm_start_date,
						$security_deposit,
						$first_rent,
						$last_rent
						) );
					
					// print error if it occurs
					echo pg_last_error($connection);
					
					
								
					
					
					
					// sales_order_x_desk_tbl
					foreach( $offices as $office)
					{
						$query2 = pg_query_params($connection, "INSERT INTO sales_order_x_desk_tbl( sales_order_id,desk_id)
						VALUES($1, $2);", array($id ,$office) );
						
					
						// print error if it occurs
						echo pg_last_error($connection);
					}
					
						
					
					//Sales order line item table.
					$counter = 1;
										
					
					// all order lines
					foreach($lines as $line)
  					{
  						
  						
  						$query3 = pg_query_params($connection, "INSERT INTO so_li_itm_tbl( 
  						sales_order_id, 
  						position, 
  						prodsvc_item_id, 
  						description, 
  						quantity, 
  						price, 
  						amount, 
  						start_date, 
  						end_date)
						VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9);", array( $id, $counter, $prodsvc[$counter - 1] , $line[1], $line[3], $line[4], $line[5], $line[6], $line[7]) );
						$counter++;
						
						// print error if it occurs
						echo pg_last_error($connection);
	
  					}
					
						
					
					// if all queries are true
					if ($query1 && $query2 && $query3) 
					{
						
						// commit
						pg_query("COMMIT") or die("Transaction commit failed\n");
						
						// make and send PDF
						include("order_pdf.php");
						
						
					} 
					else 
					{
						// rollback
						echo pg_last_error($connection);
						pg_query("ROLLBACK") or die("Transaction rollback failed\n");
						
					}
  				}
			}
			else
			{
				// if not accessed through post, redirect
				header("Location: web/order_input.php");
				exit;
			}
		?>

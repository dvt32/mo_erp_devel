<?php

		
	if (!empty($_GET["floor"]) && ctype_digit($_GET["floor"]))
	{	
		if (file_exists("floors/floor_" . $_GET["floor"] .".png"))
		{

			// setting the image header in order to proper display the image
			header("Content-Type: image/png");
			
			require '../../lib/get_database_connection.php';
			
			
			// get image dimensions
			$floor = pg_query_params($connection, 
				"select bounding_polygon from floor_tbl where id = $1", 
					array(  $_GET["floor"] ) );			
			
			// get floor width			
			$highest = 0;
			while($line = pg_fetch_array($floor, null, PGSQL_ASSOC))
			{
				preg_match_all('!\d+!',$line['bounding_polygon'], $coordinates);

				$length = count($coordinates[0]);
				for($i = 0; $i < $length; $i++)
				{
					if($i % 2 == 0)
					{
						if ($highest < $coordinates[0][$i])
						{
							$highest = $coordinates[0][$i];
						}
					}
				}
			}

			// get desks
			$desks = pg_query_params($connection, 
				"select bounding_polygon, account_id, start_date, end_date, cust_pers_id, assigned_number from desk_tbl where is_active = true and floor_id = $1", 
					array(  $_GET["floor"] ) );	

			// get background
			$im = @imagecreatefrompng("floors/floor_" . $_GET["floor"] . ".png")
			or die("Cannot Initialize new GD image stream");

			$im = imagescale($im , $highest * 0.999);	
			
			$red_pink = imagecolorallocatealpha($im,255,150,150,60);
			$yellow = imagecolorallocatealpha($im,255,255,150,60);
			$black = imagecolorallocatealpha($im,0,0,0,255);
			$red = imagecolorallocate($im,255,150,150);
			
			// draw all desks
			while($line = pg_fetch_array($desks, null, PGSQL_ASSOC))
			{
				// get coordinates
				preg_match_all('/\b\d+(?:\.\d{0,}|)/',$line['bounding_polygon'], $coordinates);
				$length = count($coordinates[0]);

				$width = 0;
				$height = 0;
				for($i = 0; $i < $length; $i++)
				{
					// mirror the coordinations to match the images
					if($i % 2 == 0)
					{
						$width += $coordinates[0][$i];				
					}
					else
					{
						$coordinates[0][$i] = imagesy($im) - $coordinates[0][$i];
						$height += $coordinates[0][$i];
					}
				}
				
				// if there is no account ID
				if($line['account_id'] == null)
				{
					// Desk is transparent
					imagepolygon($im, $coordinates[0], $length / 2, $black);				

				}
				// start- and enddate are valid or no end_date given
				else if( current_Tenant($line['start_date'],$line['end_date']))
				{
					// Desk is yellow
					imagefilledpolygon($im, $coordinates[0], count($coordinates[0]) / 2, $yellow);
					imagepolygon($im, $coordinates[0], $length / 2, $black);
				}
				// desk has account_id & invalid dates
				else
				{
					// Desk is red
					imagefilledpolygon($im, $coordinates[0], count($coordinates[0]) / 2, $red_pink);
					imagepolygon($im, $coordinates[0], $length / 2, $black);
				}
				
				
				// desk numbers
				imagestring ($im , 6 , $width / ($length / 2) - 5 , $height / ($length / 2) , $line['assigned_number'], $black );

			}
			
			imagepng($im);
			
			// frees any memory associated with the image 
			imagedestroy($im);
		}
	}

	function current_Tenant($start,$end)
	{
		if($start == null)
		{
			return false;
		}
		else if($end == null)
		{
			return true;
		}
		$start = date_create_from_format("Y-m-d",$start);				
		$end = date_create_from_format("Y-m-d",$end);	
		$today = new DateTime();
		return (($today > $start) && ($end > $today));
	}
	



?>


<?php
	$show = false;
	$floor_label;
	if (!empty($_GET["floor"]) && ctype_digit($_GET["floor"]))
	{	
		if (file_exists("floors/floor_" . $_GET["floor"] .".png"))
		{

			require '../../lib/get_database_connection.php';
			$show = true;
			
			// get image x and y
			$floor = pg_query_params($connection, 
				"select bounding_polygon from floor_tbl where id = $1", 
					array(  $_GET["floor"] ) );			
			
			$height = 0;
			while($line = pg_fetch_array($floor, null, PGSQL_ASSOC))
			{
				preg_match_all('!\d+!',$line['bounding_polygon'], $coordinates);

				$length = count($coordinates[0]);
				for($i = 0; $i < $length; $i++)
				{
					if($i % 2 == 1)
					{
						if ($height < $coordinates[0][$i])
						{
							$height = $coordinates[0][$i];
						}
					}
				}
			}
			// get desks
			$desks = pg_query_params($connection, 
				"select bounding_polygon, assigned_number from desk_tbl where is_active = true and floor_id = $1", 
					array(  $_GET["floor"] ) );	

			// set img map
			echo '<map name="floormap">';
			while($line = pg_fetch_array($desks, null, PGSQL_ASSOC))
			{
				
				preg_match_all('/\b\d+(?:\.\d{0,}|)/',$line['bounding_polygon'], $coordinates);
				$length = count($coordinates[0]);

				$polyCoordinates = "";
				for($i = 0; $i < $length; $i++)
				{
					if($i % 2 == 1)
					{
						$coordinates[0][$i] = $height - $coordinates[0][$i];
					}
					$polyCoordinates =  $polyCoordinates . "," . ($coordinates[0][$i]);
				}
				$polyCoordinates = trim($polyCoordinates, ',');
				echo '<area shape="poly" coords="' . $polyCoordinates . 
					'" href="" onClick="save('. $line['assigned_number'] .');" >';
			}
			echo '</map>';

			$floor = pg_query_params($connection, 
				"select short_local_label from floor_tbl where id = $1", 
					array($_GET["floor"]) );

			$floor =  pg_fetch_array($floor, null, PGSQL_ASSOC);
			$floor_label = $floor['short_local_label'];
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="style.css">

	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="js/resizableMap.min.js"></script>
</head>
<body>
	<?php if($show) : ?>
       <img src="create_floor_image.php?floor=<?= $_GET["floor"]?>" usemap="#floormap">
	<?php else : ?>
	<p> Sorry. Floor does not exist </p>
	<?php endif; ?>

</body>
<script>
	$(document).ready(function(e) {
	   	$('img[usemap]').rwdImageMaps();
	});

	function save(val)
	{
		var input = "<?=$_GET["desks"]?>";
		var fl = "<?=$floor_label?>";
		var label = fl + 'D' + val;
	
		if (input.indexOf(label + ',') != -1)
		{
			input = input.replace(',' + label + ',', ",");
			input = input.replace(label + ',', "");
			targetitem.value = input;
		}		
		else
		{
			targetitem.value = input + label + ',';
		}
		window.close();
	}
</script>
</html> 

       
       
        function check()
        {
            
             //check input
            var licensee = document.getElementById('Licensee').value.trim();
            var address1 = document.getElementById('Address1').value.trim();
            var address2 = document.getElementById('Address2').value.trim();
           	var city  = document.getElementById('City').value.trim();
			
			
			var state = "";
			if (document.getElementById('State_dropdown').style.display == 'none')
			{
				state = document.getElementById('State_text').value.trim().toUpperCase();
			}
			else
			{
				state = document.getElementById('State_dropdown').value.trim().toUpperCase();
			}
			
			var postalcode = document.getElementById('Postalcode').value.trim();
			var country = document.getElementById('Country').value.trim().toUpperCase();
			var phone = document.getElementById('Phone').value.trim();
			var signer_first = document.getElementById('Signer_first').value.trim();
			var signer_last = document.getElementById('Signer_last').value.trim();
			var signer_title = document.getElementById('Signer_title').value.trim();
			var signer_email = document.getElementById('Signer_email').value.trim();
			var businesstype = document.getElementById('Businesstype').value.trim();
			var EIN = document.getElementById('EIN').value.trim();
			var SSN = document.getElementById('SSN').value.trim();
			var spaces = document.getElementById('Spaces').value.trim();
			var floorID = document.getElementById('floor').value.trim();
			var employee = document.getElementById('employee').value.trim();
			var startdate = document.getElementById('startdate').value.trim();
			var enddate = document.getElementById('enddate').innerHTML.trim();
			var orderID = document.getElementById('OrderID').value.trim();
			var replacedID = document.getElementById('replacedID').value.trim();
			
            if (licensee == "")
            {
            	alert("please enter a licensee name");
            	return false;
            }
            if (address1 == "")
            {
            	alert("please enter an address");
            	return false;
            }
            if (city == "")
            {
            	alert("please enter a City");
            	return false;
            }
           
        	//check if country exists
        	if (countries.indexOf(country.toUpperCase()) != -1)
        	{
        		if (state == "")
	        	{
			    	alert("please enter a State");
			    	return false;
	        	}
        		else if (country == "US")
        		{
        			if (states.indexOf(state.toUpperCase()) == -1)
        			{
        				alert("state abbreviation does not exist.");
        				return false;
        			}
        		}
        		
        	}
        	else
        	{
        		alert("Country abbreviation does not exist.");
        		return false;
        	}
            

            if (postalcode == "")
            {
            	alert("please enter a postal code");
            	return false;
            }
            if (phone == "")
            {
            	alert("please enter a Phone number");
            	return false;
            }
             if (signer_first == "")
            {
            	alert("please enter the signers first name");
            	return false;
            }
             if (signer_last == "")
            {
            	alert("please enter the signers last name");
            	return false;
            }
             if (signer_title == "")
            {
            	alert("please enter the signers title");
            	return false;
            }
             if (signer_email == "")
            {
            	alert("please enter the signers email");
            	return false;
            }
            else
            {
            	var pattern =  /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

            	if ( signer_email.search(pattern) != 0)
            	{
            		alert("please enter a valid signers email");
            		return false;
            	}
            }
             if (businesstype  == "")
            {
            	alert("please enter a businesstype");
            	return false;
            }
             if (EIN == "")
            {
            	alert("please enter an EIN");
            	return false;
            }
             if (SSN == "")
            {
            	alert("please enter an SSN");
            	return false;
            }
             if (floorID  == "")
            {
            	alert("please enter a floor");
            	return false;
            }
             if (employee  == "")
            {
            	alert("please enter an employee");
            	return false;
            }
             if (startdate  == "")
            {
            	alert("please enter a startdate");
            	return false;
            }
             if (enddate  == "Unknown")
            {
            	alert("please enter a duration");
            	return false;
            }
            var exist = false;
             if (orderID == "")
            {
            	alert("please enter an order ID");
            	return false;
            }
            else
            {
            	if (isNaN(Number(orderID)))
            	{
            		alert("please enter a number as order ID");
            		return false;
            	}
            	else
            	{
            		
            		
			    	$.ajax({
						type: 'POST',
						url: 'query.php',
						async: false,
						data: 'id=' + orderID,
						success: function(data)
						{
				       
				    	    if (data == "EXISTS")
						    {
						    	alert("order ID already exists");
		        				exist = true;
						    }
				    }});
            	}
            	

			}
			if (isNaN(Number(replacedID)) && replacedID != "")
			{
				alert("please enter a number as replaced order ID");
				return false;
			}
			return (! exist)

			
       }

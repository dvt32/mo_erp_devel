
<html>

	<head>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	</head>
	<style>
		html {
			display: table;
			margin: auto;
		}

		body {
			display: table-cell;
			vertical-align: middle;
		}
	</style>
	<body>
	<br><br>
		<input id="text" />
		<select id="buildings">	
		</select>
		
		<select id="employees">	
		</select>
		
		<select id="item" onChange="select();">
			<option value=""></option>
  			<option value="licensee">Licensee</option>
			<option value="email">Email</option>
  			<option value="id">ID</option>
  			<option value="employee">MO Employee</option>
  			<option value="building">Building</option>
		</select>
		<button onClick="getOrders();">Search</button>
		<br>
		
		<span id="results"> </span>
		
		<br><button id="next" onClick="addOrders();">More</button>
	</body>
</html>
<script>

	var pos = 1;
	var parent = document.getElementById('results');
	var searchVal;
	var criteria;
	
	function getOrders()
	{
	 	criteria = document.getElementById('item').value
	 	var term = document.getElementById('text').value
	 	var selBuilding = document.getElementById('buildings').value;
	 	var selEmployee = document.getElementById('employees').value;
	 	
	 	// check if everything is filled in
	 	if (criteria == "")
	 	{
	 		alert("Please enter a search criteria");
	 		return false;
	 	}
	 	else if (criteria == "building" && selBuilding == "")
	 	{
	 		alert("Please select a building");
	 		return false;
	 	}
	 	else if (criteria == "employee" && selEmployee == "")
	 	{
	 		alert("Please select an employee");
	 		return false;
	 	}
	 	else if (criteria != "building" && criteria != "employee" && term == "")
	 	{
	 		alert("Please enter a search string");
	 		return false;
	 	}
	 	
	 	//GET ORDERS
	 	if (criteria == "building")
	 	{
	 		searchVal = selBuilding;
	 	}
	 	else if (criteria == "employee")
	 	{
	 		searchVal = selEmployee;
	 	}
	 	else
	 	{
	 		searchVal = term;
	 	}
	 	pos = 1;
	 	parent.innerHTML = ''; 	
	 	addOrders();
	}
	
	function addOrders(){
	
		$.ajax({
				type: 'POST',
				url: 'query.php',
				async: false,
				data: 'criteria=' + criteria + '&term=' + searchVal + "&offset=" + pos,
				success: function(data)
				{	
			   		document.getElementById('next').style.display = 'none';
			   		if (data == 'false')
			   		{
			   			if (pos == 1)
			   			{
			   				parent.innerHTML = 'No results found.';
			   			}
			   			else
			   			{
			   				parent.innerHTML = parent.innerHTML + 'No more results.';
			   			}
			   		}
			   		else if (data == 'null')
			   		{
			   			if (pos == 1)
			   			{
			   				parent.innerHTML = 'Invalid argument given';
			   			}
			   			else
			   			{
			   				parent.innerHTML = parent.innerHTML + 'No more results.';
			   			}
			   	
			   		}
			   		else
			   		{
			   			data = JSON.parse(data);
			   			
			   			for (var i = 0; i < data.length; i++)
			   			{
				   			var html = 
				   			" <b> ID: </b> <a href='' onClick='save(" + data[i]["id"] + ")'> " + data[i]["id"] + 
				   			" </a> <br> <b> Licensee: </b>" + data[i]["licensee_name"] + 
				   			"<br> <b> Signer: </b>" + data[i]["authorized_signer_title"] + " " + data[i]["authorized_signer_first_name"] + " " + data[i]["authorized_signer_last_name"] + "<br>" +
				   			"<b> Signer Email: </b>" + data[i]["authorized_signer_email"] + "<br>" +
				   			"<b> Start Date: </b>" + data[i]["start_date"] + "<br> <b> End Date: </b>" + data[i]["end_date"] + "<br>" +
				   			"<b> Building: </b>" + getBuilding( data[i]["building_id"] ) + "<br> "+
				   			"<b> Employee: </b>" + getEmployee( data[i]["licensor_employee_id"] ) + "<br> <hr>";
					
							parent.innerHTML = parent.innerHTML + html;
						}
						document.getElementById('next').style.display = 'inline';
			   		}

			}});
		pos += 5;
	}
	
	function getBuilding(id)
	{
	
		var tmp;
		$.ajax({
				type: 'POST',
				url: 'query.php',
				async: false,
				data: 'building=' + id,
				success: function(data)
				{	
			   		tmp = data;
			}});
			
		return tmp;
			
	}
	
	function getEmployee(id)
	{
		var tmp;
		$.ajax({
				type: 'POST',
				url: 'query.php',
				async: false,
				data: 'employee=' + id,
				success: function(data)
				{	
			   		tmp = data;
			}});
		return tmp;
	}
	
	function getBuildings()
	{
		$.ajax({
				type: 'POST',
				url: 'query.php',
				async: true,
				data: 'getBuilding=tmp',
				success: function(data)
				{	
			   		data = JSON.parse(data);
			   		var buildings = document.getElementById('buildings');
			   		
			   		for( var i = 0; i < data.length; i++)
			   		{
						buildings.options[i + 1] = new Option(data[i]['short_global_label'], data[i]['id']);
					} 
			   		
			   	   	
			}});
			
	}
	
	function getEmployees()
	{
		$.ajax({
				type: 'POST',
				url: 'query.php',
				async: true,
				data: 'getEmployee=tmp',
				success: function(data)
				{	
			   		data = JSON.parse(data);
			   		var employees = document.getElementById('employees');
			   		
			   		for( var i = 0; i < data.length; i++)
			   		{
						employees.options[i + 1] = new Option( (getEmployee(data[i]['id'])) , data[i]['id']);
					} 
			   		
			   	   	
			}});
	}
	
	function save(val)
	{
		targetitem.value = val;
		window.close();
	}
	
	
	function choose_employee()
	{
			var employee = document.getElementById('employees');
			var text = document.getElementById('text');
			var buildings = document.getElementById('buildings');
	
				text.style.display = 'none';
				buildings.style.display = 'none';
				employee.style.display = 'inline';
	}
	
	function choose_building()
	{
			var employee = document.getElementById('employees');
			var text = document.getElementById('text');
			var buildings = document.getElementById('buildings');
	
				text.style.display = 'none';
				buildings.style.display = 'inline';
				employee.style.display = 'none';
	}
	
	function choose_text()
	{
			var employee = document.getElementById('employees');
			var text = document.getElementById('text');
			var buildings = document.getElementById('buildings');
	
				text.style.display = 'inline';
				buildings.style.display = 'none';
				employee.style.display = 'none';
	}
	
	function select()
	{
		var selected = document.getElementById('item');
		
		if (selected.value == "building")
		{
			choose_building();
		}
		else if (selected.value == "employee")
		{
			choose_employee();
		}
		else
		{
			choose_text();
		}
	}
	function start()
	{
		getBuildings();
		getEmployees();
		document.getElementById('next').style.display = 'none';
		select();
	}
	
	window.onload = start();
</script>

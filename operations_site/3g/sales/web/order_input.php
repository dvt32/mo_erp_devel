
<!DOCTYPE html>
<html>
	<head>
		<title>New Order</title>
		<link rel="stylesheet" href="style.css">
		
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		
		<script src="js/date.js"></script>
		<script src="js/prices.js"></script>
		<script src="js/check.js"></script>
		<script src="js/abbreviations.js"></script>
		
		

	</head>

	<body>
		<div class="wrapper">
			<h2> Sales Order Form</h2>
			<form method="post" id="SOF" >
				<div class="leftside">
					<p>Licensee: <input id="Licensee" placeholder="title" /> </p>
					<p>Address 1: <input id="Address1" placeholder="title" /></p>
					<p>Address 2:<input id="Address2" placeholder="title" /></p>
					<p>City: <input id="City" placeholder="title" /></p>
					State: <span class="state">  <select id="State_dropdown" placeholder="title" onChange="State()"></select>
						<a id="State_return" onClick="State_init()"> inside US? </a>
						<input id="State_text" size="3" />
						
					</span>
					<p>Postal Code: <input id="Postalcode" size="5" placeholder="title" /></p>
					
					<p>Country: <input id="Country" placeholder="title" /></p>
					
					
					<p>Phone: <input id="Phone" placeholder="title" /></p>
					<p>Authorized Signer First Name:  <input id="Signer_first" placeholder="title" /></p>
					<p>Authorized Signer Last Name:  <input id="Signer_last" placeholder="title" /></p>
					<p>Authorized Signer Title: <input id="Signer_title" placeholder="title" /></p>
					<p>Authorized Signer Email: <input id="Signer_email" placeholder="title" /></p>
				</div>
				<div class="rightside">
					<p>Licensee Business Type:  <input  id="Businesstype" placeholder="title" /> </p>
				 	<p>Licensee's EIN: <input id="EIN" size='8' placeholder="title" /> </p>
				 	<p> SSN: <input id="SSN" size='8' placeholder="title" /> </p>
					<p>
						Floor: 
						<select id="floor" onblur="getprice();rmSpaces();"/> 
							<option selected="selected" value=""></option>
						</select> 
					</p>
					<p>Office Space(s) (#):  <input type="text" id="Spaces"  placeholder="spaces" onClick='chooseDesk();' readonly/> </p>
					<p>
						Micro Office Employee: 
						<select id="employee" /> 
							<option selected="selected" value=""></option>	
						</select> 
					</p>
			 		<p>Start Date: <input id="startdate" size='8' onblur="enddate(); begindate(); ItemsEnd(); Onetime();" placeholder="title" /> </p>
			 		<p>
				 		Duration:
				 		<select  id="C-Duration"   onblur="enddate(); Onetime();">
							  <option selected="selected" value="0"></option>
							  <option value="1">1 Month</option>
							  <option value="3">3 Months</option>
							  <option value="6">6 Months</option>
							  <option value="9">9 Months</option>
							  <option value="12">12 Months</option>
						</select>
					</p>
			 		<p>Order ID:  <input id="OrderID" size='4' placeholder="title" /> 
			 		<input type="button" onClick="getID();" value="Get new order ID"> </p>
			 		<p>Order ID Replaced:  <input id="replacedID" size='4' placeholder="title" onblur="Onetime();" /> 
			 		<input type=button 
 						onClick='targetitem = document.forms[0].replacedID; dataitem = window.open("orderSelect.php",
 						"dataitem", "toolbar=no,menubar=no,scrollbars=yes,resizable=1,width=500,height=700"); dataitem.targetitem = targetitem' 
 						value="Search Orders">
					</p></form>
				</div>
				<div class="line">
				<div class="middle">
				<p> <b>Recurring Items</b> Initial Term:  <span id="begindate"> </span> - <span id="enddate"> </span> </p>
				<p> 
					<span style="padding-left:20px"> item </span> 
					<span style="padding-left:130px">Description </span> 
					<span style="padding-left:70px">Duration </span> 
					<span style="padding-left:20px">QTY </span> 
					<span style="padding-left:20px">Price </span> 
					<span style="padding-left:32px">Amount </span> 
					<span style="padding-left:24px">Start </span> 
					<span style="padding-left:45px">End </span> 
				</p>
				<div id="Lineitems" style="display: none">

					<select name="Item" id="items" onblur="getprice();">
						 <option selected="selected" value="0">tmp</option>
					</select>
					<input size='16' name="Description" id="Description" placeholder="Description" />
					<select name="Duration" id="Duration" placeholder="Duration" onblur="ItemsEnd();"/>
						<option selected="selected" value="0"></option>
				  		<option value="1">1 Month</option>
					  	<option value="3">3 Months</option>
					  	<option value="6">6 Months</option>
					  	<option value="9">9 Months</option>
					  	<option value="12">12 Months</option>
				  	</select>
					<input size='4' name="QTY"  id="QTY" onblur="calcAmount();" placeholder="QTY" />
					<input size='6' name="Price" id="Price" onblur="calcAmount();" placeholder="Price" />
					<input size='6' name="Amount" id="Amount" onblur="calcAmount();" placeholder="Amount" />
					<input size='8' name="Start" id="Start" placeholder="Start" onblur="ItemsEnd();" />
					<input size='8' name="End"  id="End" placeholder="End" /> 

				</div>
				<span id="writeroot"></span><br>
				<input type="button"  value="Add line" onclick="moreFields()"/>
				<br>
				<br>
				Premium for Month-to-Month Flexibility Begins after 
				<input size='1' id="m2mDur" onblur="costumM2Mdate();" ></input> Months
				with <input size='1' onblur="calcM2m();" name="M2Mp"  id="M2Mp" value="5" />%
				<br>Total: <span id="m2m"></span>
				<br>Begindate: <span id="m2mDate"></span>
				<p> <b>One-Time Items</b></p>
				<span id="security">Security Requirement: $0</span><br>
				<span id="firstmonth">First Month's Rent Requirement: $0</span><br>
				<span id="finalmonth">Final Month's Rent Requirement: $0</span><br><br>
				<b><span id="TOTAL"> Total Required for Move-in: </span> </b> <br>
				
				<textarea cols="35" name="Note"  id="Note" placeholder="Note:" ></textarea> <br><br>
				<button onClick="saveForm();"> Save form </button>
			</form>
		</div>
		</div>
	<script>

		var id;
		var floorID;
		
		function rmSpaces()
		{
			document.getElementById('Spaces').value = "";

			var floorname = document.getElementById('floor').value;

			$.ajax({
					type: 'POST',
					url: 'query.php',
					async: false,
					data: 'getFloorID=' + floorname,
					success: function(data)
					{	
				   		floorID = String(data);	
				}});
		}		

		function getID()
		{
			// you can only claim 1 ID per page reload
			if (id == null)
			{
				$.ajax({
					type: 'POST',
					url: 'query.php',
					async: false,
					data: 'getID=tmp',
					success: function(data)
					{	
				   		id = String(data);  	
				}});
			}
			document.getElementById('OrderID').value = id; 
		}
		function chooseDesk()
		{

			targetitem = document.forms[0].Spaces; 
			
			dataitem = window.open("desk_selection.php?floor=" + floorID + "&desks=" + document.getElementById('Spaces').value,
 						"dataitem", "toolbar=no,menubar=no,scrollbars=yes,resizable=1,width=500,height=700");
 	
			dataitem.targetitem = targetitem;
		}

		function InitFloors()
		{
			$.ajax({
					type: 'POST',
					url: 'query.php',
					async: true,
					data: 'getFloors=tmp',
					success: function(data)
					{	
				   		data = JSON.parse(data);
				   		var floors = document.getElementById('floor');
				   		
				   		for( var i = 0; i < data.length; i++)
				   		{
							floors.options[i + 1] = new Option( data[i]['short_global_label'] , data[i]['short_global_label']);
						} 
				   		
				   	   	
					}
			});
		}
		function InitEmployees()
		{
			$.ajax({
					type: 'POST',
					url: 'query.php',
					async: true,
					data: 'getEmployee=tmp',
					success: function(data)
					{	
				   		data = JSON.parse(data);
				   		var employees = document.getElementById('employee');
				   		
				   		for( var i = 0; i < data.length; i++)
				   		{
							employees.options[i + 1] = new Option( (getEmployeeName(data[i]['id'])) , data[i]['id']);
						} 
				   		
				   	   	
				}});
		}
		
		function getEmployeeName(id)
		{
			var tmp;
			$.ajax({
					type: 'POST',
					url: 'query.php',
					async: false,
					data: 'employee=' + id,
					success: function(data)
					{	
				   		tmp = data;
				}});
			return tmp;
		}
		
		function State()
		{
		
			var states_US = document.getElementById('State_dropdown');
			var states_Other = document.getElementById('State_text');
			var return_button = document.getElementById('State_return');
	
			
			
			if (states_US.value == "OUTSIDE_US")
			{
				states_US.style.display = 'none';
				states_Other.style.display = 'inline';
				return_button.style.display = 'inline';
			}
		}
		function State_init()
		{
			var states_US = document.getElementById('State_dropdown');
			var states_Other = document.getElementById('State_text');
			var return_button = document.getElementById('State_return');
			
			return_button.style.display = 'none';
			states_Other.style.display = 'none';
			states_US.style.display = 'inline';
			states_US.options[0] = new Option("","");
			for(var i = 0; i < states.length; i++)
			{
				states_US.options[i + 1] = new Option(states[i], states[i]);
			} 
			states_US.options[states.length + 1] = new Option("Other", "OUTSIDE_US");
		}
		
		function validDate(text) {

			var date = Date.parse(text);

			if (isNaN(date)) {
				return false;
			}

			var comp = text.split('-');

			if (comp.length !== 3) {
				return false;
			}

			var m = parseInt(comp[0], 10);
			var d = parseInt(comp[1], 10);
			var y = parseInt(comp[2], 10);
			var date = new Date(y, m - 1, d);
			return (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d);
		}

		var counter = 0;

		function moreFields() {
			counter++;
			var newFields = document.getElementById('Lineitems').cloneNode(true);
			newFields.id = '';
			newFields.style.display = 'block';
			var newField = newFields.childNodes;
			for (var i=0;i<newField.length;i++) {
				var theName = newField[i].name
				if (theName)
					newField[i].name = theName + counter;
					newField[i].id = theName + counter;
			}
			var insertHere = document.getElementById('writeroot');
			insertHere.parentNode.insertBefore(newFields,insertHere);			
			addItems(counter);
		}
		
		function enddate() {
			
			var duration = document.forms["SOF"]["C-Duration"].value;
			var startdate = document.forms["SOF"]["startdate"].value;
			duration = parseInt(duration);
			
			if (validDate(startdate) && duration != 0)
			{
				document.getElementById('startdate').style.backgroundColor = "white";
				
				document.getElementById('m2mDur').value = (duration);
				
				var comp = startdate.split('-');
				var m = comp[0];
				var d = comp[1];
				var y = comp[2];
				var date = Date.parse(m + "-" + d + "-"+ y);
				
				if (d > 1)
				{
					duration += 1;
					
				}
				
				
				date = date.add(duration).months();
				
				var m = date.getMonth();
				if (m < 1)
				{
					m = 12;
				}
				var y = date.getFullYear();
				d = Date.getDaysInMonth(y, (m-1));
				
				if (m == 12 & d == 31)
				{
					y = y - 1
				}
						
				document.getElementById('enddate').innerHTML = (m) +"-"+ d +"-"+ y;
				
				
				// add 1 day for m2m
				date = date.add(1).months();
				
				var m = date.getMonth();
				if (m < 1)
				{
					m = 12;
				}
				var y = date.getFullYear();
				d = Date.getDaysInMonth(y, (m-1));
				
				document.getElementById('m2mDate').innerHTML = (m) +"-"+ '1' +"-"+ y; 
			}
			else
			{
				if ( !validDate(startdate) && startdate != "")
				{
					document.getElementById('startdate').style.backgroundColor = "red";
				}
				else
				{
					document.getElementById('startdate').style.backgroundColor = "white";
				}
				
				document.getElementById('enddate').innerHTML = "Unknown";
				document.getElementById('m2mDur').innerHTML = "Unknown";
			}
			
			
		}
		
		function begindate() 
		{
			
			var startdate = document.forms["SOF"]["startdate"].value;

			if (validDate(startdate))
			{
				
				document.getElementById('begindate').innerHTML = startdate;
			}
			else
			{
				document.getElementById('begindate').innerHTML = "Unknown";
			}
		}
		function compbuilding(building)
		{
			buildingshort = building.slice(0, - 3);
			
			// compare with all buildings
			if (buildingshort == "NY-1375Bwy")
			{
				return NY1375Bwy;
			}
			else if (buildingshort == "NY-122W27")
			{
				return NY122W27;
			}
			else if (buildingshort == "NY-902Bwy")
			{
				if (building == "NY-902BwyF06")
				{
					return NY902BwyF06;
				}
				else if	(building == "NY-902BwyF07")
				{
					return NY902BwyF07;
				}
			}
			else if (buildingshort == "NY-1601Bwy")
			{
				return NY1601BwyF12;
			}
			else if (buildingshort == "NY-318W118")
			{
				return NY318W118F01;
			}
			else if (buildingshort == "BOS-101ArchF")
			{
				return BOS101ArchFl19;
			}
			else if (buildingshort == "CH-18SMich")
			{
				return CH18SMichF12;
			}
			else if (buildingshort == "CH-405WSuperior")
			{
				return CH405WSuperior;
			}
			else
			{
				return [["Please choose a building"]];
			}
		}
		function addItems(x)
		{
				var current = document.getElementById('Item' + x).value;
				
				var building = document.getElementById("floor").value;
				itemlist = compbuilding(building);	
				
				
				
				var select = document.getElementById('Item' + x);
				select.options[0] = new Option("","");
				for (j = 0; j < itemlist.length; j++)
				{ 
				
					select.options[j + 1] = new Option(itemlist[j][0], itemlist[j][0]);
				}
				
				// save latest input
				document.getElementById('Item' + x).value = current;
				
				// if building does not have this item
				if(document.getElementById('Item' + x).value == "" && current != "0" && current != "")
				{
				
					select.options[0] = new Option(current, current);	
					select.style.backgroundColor = "red";
					document.getElementById('Item' + x).value = current;
					document.getElementById('Price' + x).value = "";
					document.getElementById('Amount' + x).value = "";
					document.getElementById('Description' + x).value = "";
				}
				else if (current != "0" && current != "")
				{
					select.options[0] = new Option("", "");	
					select.style.backgroundColor = "white";
				}
				Onetime();
				
		}
		
		function calcAmount()
		{
			for (i = 1; i <= counter; i++)
			{ 
				var QTY = document.getElementById('QTY' + i).value; 
				var price = document.getElementById('Price' + i).value;
			
				if (QTY != "" && price != "")
				{
						document.getElementById('Amount' + i).value = QTY * price;
				}
			}
			calcM2m();
		}
		
		
		function getprice()
		{
			
			var building = document.getElementById("floor").value;
			itemlist = compbuilding(building);
				
			for (i = 1; i <= counter; i++)
			{ 
				var descr = null;
				var price = null;
				var selected = document.getElementById('Item' + i).value;
				
				for (j = 0; j < itemlist.length; j++)
				{ 
					if (itemlist[j][0] == selected)
					{
						descr = itemlist[j][1];
						price = itemlist[j][2];
						// paste descr and price
						document.getElementById('Description' + i).value = descr;
						document.getElementById('Price' + i).value = price;
						addItems(i);
					}
				}
				// if not found
				if (price == null )
				{
					addItems(i);
				}
			}
			calcAmount();
		}
		
		function calcM2mTotal()
		{
			var total = 0;
			
			for (i = 1; i <= counter; i++)
			{ 
				
				if (document.getElementById('Duration' + i).value == "0")
				{
					total += Number(document.getElementById('Amount' + i).value);
				}
			}
			return total;
		}
		
		function calcM2m()
		{
			var total = calcM2mTotal();
			var percent = document.getElementById('M2Mp').value;
			var premium = (total / 100) * Number(percent);
			document.getElementById('m2m').innerHTML = premium;
			Onetime();
		}
		
		function ItemsEnd()
		{
			for (i = 1; i <= counter; i++)
			{ 
				var begindate = "";
				
				if (document.getElementById('Start' + i).value == "")
				{
					begindate = document.getElementById('startdate').value;
				}
				else
				{
					begindate = document.getElementById('Start' + i).value;
				}
				
				if ( validDate(begindate) )
				{
					document.getElementById('Start' + i).style.backgroundColor = "white";
					
					// set enddate
					if (  document.getElementById('Duration' + i).value > 0 )
					{
						var duration = Number(document.getElementById('Duration' + i).value);
						
						var comp = begindate.split('-');
						var m = comp[0];
						var d = comp[1];
						var y = comp[2];
						var date = Date.parse(m + "-" + d + "-"+ y);
				
						if (d > 1)
						{
							duration += 1;
					
						}
				
						date = date.add(duration).months();
				
						var m = date.getMonth();
						if (m < 1)
						{
							m = 12;
						}
						var y = date.getFullYear();
						d = Date.getDaysInMonth(y, (m-1));
						
						if (m == 12 & d == 31)
						{
							y = y - 1
						}
						
						document.getElementById('End' + i).value = (m) +"-"+ d +"-"+ y;
					}
					else
					{
						document.getElementById('End' + i).value = "";
					}
				}
				else if (document.getElementById('Start' + i).value != "")
				{
					document.getElementById('Start' + i).style.backgroundColor = "red";
				}
			}
			calcM2m();
		}
		
		function Onetime()
		{
			var firstMonth = 0;
			var lastMonth = 0;
			
			var EndOrder = document.getElementById('enddate').innerHTML;
			var comp = EndOrder.split('-');
			var m = Number(comp[0]);
			var d = Number(comp[1]);
			var y = Number(comp[2]);
			
			
			for (c = 1; c <= counter; c++)
			{
				
				var EndDate = document.getElementById('End' + c).value;
				if (EndDate == "")
				{
					var m2 = m;
					var d2 = d;
					var y2 = y;
				}
				else
				{
					comp = EndDate.split('-');
					var m2 = Number(comp[0]);
					var d2 = Number(comp[1]);
					var y2 = Number(comp[2]);

				}
				
				
				var Compare = 0;
				
				if (y2 > y)
				{
					Compare = 1;
				}
				else if(y2 < y)
				{
				 	Compare = -1;
				}
				else if (m2 > m)
				{
					Compare = 1;
				}
				else if (m2 < m)
				{
					Compare = -1;
				}
				else if (d2 > d)
				{
					Compare = 1;
				}
				else if (d2 < d)
				{	
					Compare = -1;
				}
				
				if ( Compare >= 0 )
				{
					lastMonth += Number(document.getElementById('Amount' + c).value);
				}
			
				if (document.getElementById('Start' + c).value == "")
				{
					firstMonth += Number(document.getElementById('Amount' + c).value);
				}
			}
			// not a replacement
			if (document.getElementById('replacedID').value == "")
			{
				
				document.getElementById('security').innerHTML = "Security Requirement: $" + lastMonth;
				document.getElementById('firstmonth').innerHTML = "First Month's Rent Requirement: $" + firstMonth;
				document.getElementById('finalmonth').innerHTML = "Final Month's Rent Requirement: $" + lastMonth;
				document.getElementById('TOTAL').innerHTML = "Total Required for Move-in: $" + ( (lastMonth * 2) + firstMonth) ;
			}
			// if order is replacement
			else
			{
				document.getElementById('security').innerHTML = "Change in Security Requirement: $" + lastMonth;
				document.getElementById('firstmonth').innerHTML = "";
				document.getElementById('finalmonth').innerHTML = "Change in Final Month's Rent Requirement: $" + lastMonth;
				
				document.getElementById('TOTAL').innerHTML = "Total Required for Move-in: $" + (lastMonth * 2);
			}
		}
		
		function costumM2Mdate()
		{
			var m2mbegin = Number(document.getElementById('m2mDur').value);
			
			var startdate = document.getElementById('startdate').value;
				
				
			var comp = startdate.split('-');
			var m = comp[0];
			var d = comp[1];
			var y = comp[2];
			var date = Date.parse(m + "-" + d + "-"+ y);
			
			if (d > 1)
			{
				duration += 1;
				
			}
			
			
			date = date.add(m2mbegin).months();
			
			// add 1 day for m2m
			date = date.add(1).months();
			
			var m = date.getMonth();
			if (m < 1)
			{
				m = 12;
			}
			var y = date.getFullYear();
			d = Date.getDaysInMonth(y, (m-1));
			
			document.getElementById('m2mDate').innerHTML = (m) +"-"+ '1' +"-"+ y; 
		}
		
	  function saveForm()
        {    
            //check input
            var licensee = document.getElementById('Licensee').value.trim();
            var address1 = document.getElementById('Address1').value.trim();
            var address2 = document.getElementById('Address2').value.trim();
           	var city  = document.getElementById('City').value.trim();
			
			var state = "";
			if (document.getElementById('State_dropdown').style.display == 'none')
			{
				state = document.getElementById('State_text').value.trim().toUpperCase();
			}
			else
			{
				state = document.getElementById('State_dropdown').value.trim().toUpperCase();
			}
			
			var postalcode = document.getElementById('Postalcode').value.trim();
			var country = document.getElementById('Country').value.trim().toUpperCase();
			var phone = document.getElementById('Phone').value.trim();
			var signer_first = document.getElementById('Signer_first').value.trim();
			var signer_last = document.getElementById('Signer_last').value.trim();
			var signer_title = document.getElementById('Signer_title').value.trim();
			var signer_email = document.getElementById('Signer_email').value.trim();
			var businesstype = document.getElementById('Businesstype').value.trim();
			var EIN = document.getElementById('EIN').value.trim();
			var SSN = document.getElementById('SSN').value.trim();
			var spaces = document.getElementById('Spaces').value.trim();
			var floorID = document.getElementById('floor').value.trim();
			var employee = document.getElementById('employee').value.trim();
			var startdate = document.getElementById('startdate').value.trim();
			var enddate = document.getElementById('enddate').innerHTML.trim();
			var orderID = document.getElementById('OrderID').value.trim();
			var replacedID = document.getElementById('replacedID').value.trim();
            
            if ( !check())
            {
            	return false;
            }
            
           	// get office space ids
           	var Desks = false;
           	
           	if (spaces != "")
           	{

		       	$.ajax({
							type: 'POST',
							url: 'query.php',
							async: false,
							data: 'offices=' + spaces + '&building=' + floorID,
							success: function(data)
							{
								
						   	   	data = JSON.parse(data);
							   	if (data[0] == "Error")
							   	{
							   		alert(data[1]);
							   		Desks = "ERROR";
							   	}
							   	else
							  	{
							   		data.shift();
							   		Desks = data.toString();
							   	}
						}});
		        	
		        	
           	}
           	else
           	{
           		Desks = "";
           	}
           	if (Desks == "ERROR")
           	{
           		return false;
           	}
          
           	
            var infoArray = '[ ["rLicensee","' + licensee + '"],';
				infoArray += '["rAddress1","' + address1 + '"],';
				infoArray += '["rAddress2","' + address2 + '"],';
				infoArray += '["rCity","' + city + '"],';
				infoArray += '["rState","' + state + '"],';
				infoArray += '["rPostalCode","'+ postalcode + '"],';
				infoArray += '["rCountry","' + country + '"],';
				infoArray += '["rPhone","' + phone + '"],';
				infoArray += '["rAuthorizedSignerFirstName","' + signer_first + '"],';
				infoArray += '["rAuthorizedSignerLastName","' + signer_last + '"],';
				infoArray += '["rAuthorizedTitle","' + signer_title + '"],';
				infoArray += '["rAuthorizedEmail","' + signer_email + '"],';
				infoArray += '["rBusinessType","' + businesstype + '"],';
				infoArray += '["rEIN","' + EIN + '"],';
				infoArray += '["rSSN","' + SSN + '"],';
				infoArray += '["rFloorID","' + floorID + '"],';
				infoArray += '["rMOMEmployee","' + employee + '"],';
				infoArray += '["rOrderID","' + orderID + '"],';
				infoArray += '["rOrderIDReplaced","' + replacedID + '"],';
				infoArray += '["rOfficeSpaces",[' + Desks + ']],';
				infoArray += '["Initial Term Start","' + startdate + '"],';
				infoArray += '["Initial Term End","' + enddate + '"],';
				infoArray += '["begin line-items"],';
						
				for (z = 1; z <= counter; z++)
				{ 		
					var item = new Array(8);
					item[0] = '"' + document.getElementById('Item' + z).value + '"';
					item[1] = '"' + document.getElementById('Description' + z).value + '"';
					item[2] = document.getElementById('Duration' + z).value;
					item[3] = document.getElementById('QTY' + z).value;
					item[4] = document.getElementById('Price' + z).value;
					item[5] = document.getElementById('Amount' + z).value;
					item[6] = '"' + document.getElementById('Start' + z).value + '"';
					item[7] = '"' + document.getElementById('End' + z).value + '"';
					
					if (item[0] != '""')
					{
						var line = "[";
					
						if (item[5] == "")
						{
							alert("please enter QTY's with your items");
							return false;
						}
						
						for  (j = 0; j < 8; j++)
						{
							if (item[j] == "" || (j == 2 && item[j] == 0) || item[j] == '""')
							{
								if (j == 3)
								{
									line += '0,';
								}
								else
								{
									line += 'null,';
								}
							}
							else
							{
								line += item[j] + ',';
							}
						}
						line = line.slice(0, - 1);
						line += '],';
						infoArray += line;
					}
				}
				infoArray += '["end line-items"],';
				
				// M2m
				
				var OneTimeFirstmonth = document.getElementById('firstmonth').innerHTML;
				OneTimeFirstmonth = OneTimeFirstmonth.split('$');
				OneTimeFirstmonth = OneTimeFirstmonth[1];
				
				if (OneTimeFirstmonth == null)
				{
					OneTimeFirstmonth = 0;
				}
				var SecurityDeposit = document.getElementById('security').innerHTML;
				SecurityDeposit = SecurityDeposit.split('$');
				SecurityDeposit = SecurityDeposit[1];
				
				var LastMonth = document.getElementById('finalmonth').innerHTML;
				LastMonth = LastMonth.split('$');
				LastMonth = LastMonth[1];

				
				var after = document.getElementById('m2mDur').value;
				var percent = document.getElementById('M2Mp').value;
				var total =  document.getElementById('m2m').innerHTML;
				var beginm2m = document.getElementById('m2mDate').innerHTML;
				
				infoArray += '["Premium for Month-to-Month Flexibility Begins after",' + after + ',"' + percent + '%",' + (total * percent) + ',' + total + ',' + '"' + beginm2m + '",null],';
				infoArray += '["rSecRequirement",' + Number(SecurityDeposit) + '],';
				infoArray += '["rFirstMonthRent",' + Number(OneTimeFirstmonth) + '],';
				infoArray += '["rFinalMonthRent",' + Number(LastMonth) + '],';
				
				
				// notes
				var note = document.getElementById('Note').value;
				
       			if (note == "")
       			{
       				infoArray += '["notes",false]]';
       			}
       			else
       			{
       				infoArray += '["notes","' + note + '"]]';
       			}
            
            // save to database
           $.ajax({
							type: 'POST',
							url: 'query.php',
							async: false,
							data: 'saved=' + infoArray,
							success: function(data)
							{
								
						   	   	if (data == "ERROR")
						   	   	{
						   	   		alert("Something went wrong in the database");
						   	   	}
						   	   	else
						   	   	{
						   	   		
						   	   		var form = document.createElement("form");
									form.setAttribute("method", 'post');
									form.setAttribute("action", "../order_result.php");
									
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", 'order_form_preprocess_id');
									hiddenField.setAttribute("value", Number(data) );

									form.appendChild(hiddenField);
									
									document.body.appendChild(form);
									
    								form.submit();
    								
						   	   	}
						}});
           
        }
		
		function start()
		{
			
			moreFields();
			moreFields();
			moreFields();
			begindate();
			enddate();
			ItemsEnd();
			Onetime();
			State_init();
			InitEmployees();
			InitFloors();
			
			
		}
		window.onload = start;
	</script>

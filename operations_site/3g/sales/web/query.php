<?php
		
	
	// get database info
	require '../../lib/get_database_connection.php';
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		if ( !empty($_POST["id"]))
		{
			$result = pg_query_params($connection, "SELECT id from sales_order_tbl where id = $1;", array($_POST["id"]));
			$id = pg_fetch_array($result);
			$id = $id["id"];
			
			if ($id != "")
			{
				echo "EXISTS";
			}
			else
			{
				echo "SAFE";
			}
		}
		
		else if ( !empty($_POST["offices"]))
		{
			// get building id
			$building = substr_replace($_POST["building"] ,"", -4);
			
			$tmp = pg_query_params($connection, "SELECT id FROM building_tbl where short_global_label like $1", array($building . "%") );
				
			$building = pg_fetch_array($tmp);
			$building = $building["id"];
			
			// validate offices
			$offices = $_POST["offices"];
			
			
			// remove spaces
			$offices = str_replace ( ' ' , "" , $offices );
			
			//split offices
			$offices = explode(",", $offices);
			
			
			$desks = '["Succes"';
			// for each office
			foreach($offices as $office)
			{
				if ($office != "")
				{
					// split floor and desk
					$split = explode("D", $office);
				

					// remove the F
					$split[0] =  str_replace ('F' , "" , $split[0] );
				
					// removes all zeros before first real digit
					$split[0] = ltrim( $split[0] , "0");
				
				
					// if Floor is les then 10
					if ($split[0] < 10)
					{
					
						$split[0] = '0' . $split[0];
					
					}
					// add F 
					$split[0] = 'F' . $split[0];
				
					// get floor id
					$tmp = pg_query_params($connection, "SELECT id FROM floor_tbl WHERE short_local_label = $1 and building_id = $2", array($split[0], $building) );
				
					$floor_id = pg_fetch_row($tmp);
				
					if ($tmp == false || $floor_id == null)
					{
						echo '["Error", "the floor with this office: ' . $office . ' does not exist in this building"]';
						return false;
					}
				
					// get desk id
					$result = pg_query_params($connection, "SELECT id FROM desk_tbl WHERE floor_id = $1 and assigned_number = $2", array($floor_id[0], $split[1]) );
				
					$tmp = pg_fetch_row($result);
				
					if ($result == false || $tmp == null)
					{
						echo '["Error", "Office space: ' . $office . ' does not exist in this building"]';
						return false;
					}
				
					$desks = $desks .',"' .$tmp[0] . '"';
				}
				
			}
			echo $desks . "]";
			return true;
		}
		else if ( !empty($_POST["saved"]))
		{
			// get id
			$id = pg_query($connection, "SELECT nextval('order_form_preprocess_seq') as id;");
			$id = pg_fetch_array($id);
			$id = $id["id"];
			
			// save json in database
			$result = pg_query_params($connection, "INSERT INTO order_form_preprocess_tbl(id, form_data)
			VALUES ($1, $2)" , array($id , $_POST["saved"]) );
			
			// if query did not work
			if (!$result) 
			{
				echo 'ERROR';
			}
			else
			{
				echo $id;
			}
			return true;
  				
		}
		
		else if ( !empty($_POST["getBuilding"]))
		{
			// get short global labels & building ID's
			$buildings = pg_query($connection, "SELECT id, short_global_label from building_tbl");
			
			print( json_encode( pg_fetch_all($buildings)) );
			return true;
  				
		}
		else if(!empty($_POST["getFloors"]))
		{
			
			$floors = pg_query($connection, "select short_global_label from floor_tbl");
			
			print( json_encode( pg_fetch_all($floors)) );
			return true;
		}
		else if ( !empty($_POST["getEmployee"]))
		{
			// get employee IDs
			$employees = pg_query($connection, "SELECT id from employee_tbl");
			
			print( json_encode( pg_fetch_all($employees)) );
			return true;
  				
		}
		
		else if ( !empty($_POST["building"]))
		{
			// get building
			$building = pg_query_params($connection, "SELECT short_global_label from building_tbl where id = $1" , array($_POST["building"]) );
			$building = pg_fetch_array($building);
			echo $building["short_global_label"];
			return true;
  				
		}
		
		else if ( !empty($_POST["getID"]))
		{
			$id = pg_query($connection, "SELECT nextval('sales_order_seq') as id;");
			$id = pg_fetch_array($id);
			echo $id["id"];
			return true;
  				
		}
		else if ( !empty($_POST["getFloorID"]))
		{
			$buildingID = pg_query_params($connection, 
				"SELECT id from floor_tbl where short_global_label = $1" ,
				 	array($_POST["getFloorID"]) );
			$buildingID = pg_fetch_array($buildingID);
			echo $buildingID["id"];
			return true;	
				
		}
		
		else if ( !empty($_POST["employee"]))
		{
			// get employee username
			$employee = pg_query_params($connection, "SELECT windows_logon_name from employee_tbl where id = $1", array($_POST["employee"]) );
			$employee = pg_fetch_array($employee);
			echo $employee["windows_logon_name"];
			return true;
  				
		}
		
		else if ( !empty($_POST["criteria"]) && !empty($_POST["term"])  && !empty($_POST["offset"]))
		{

			$orders;
			switch ($_POST["criteria"]) 
			{
    			case "licensee":
        			$orders = pg_query_params($connection, "SELECT * from sales_order_tbl where licensee_name = $1 ORDER BY id DESC LIMIT 5 OFFSET $2", 
        			array($_POST["term"], ($_POST["offset"] - 1) ));
        			print( json_encode( pg_fetch_all($orders)) );
       			break;
    			case "email":
        			$orders = pg_query_params($connection, "SELECT * from sales_order_tbl where authorized_signer_email = $1 ORDER BY id DESC LIMIT 5 OFFSET $2",
        			array($_POST["term"], ($_POST["offset"] - 1) ));
        			print( json_encode( pg_fetch_all($orders)) );
        		break;
    			case "id":
        			$orders = pg_query_params($connection, "SELECT * from sales_order_tbl where id = $1 ORDER BY id DESC LIMIT 5 OFFSET $2",
        			array($_POST["term"], ($_POST["offset"] - 1) ));
        			print( json_encode( pg_fetch_all($orders)) );
        		break;
        		case "employee":
        			$orders = pg_query_params($connection, "SELECT * from sales_order_tbl where licensor_employee_id = $1 ORDER BY id DESC LIMIT 5 OFFSET $2", 
        			array($_POST["term"], ($_POST["offset"] - 1) ));
        			print( json_encode( pg_fetch_all($orders)) );
        		break;
        		case "building":
        			$orders = pg_query_params($connection, "SELECT * from sales_order_tbl where building_id = $1 ORDER BY id DESC LIMIT 5 OFFSET $2", 
        			array($_POST["term"], ($_POST["offset"] - 1) ));
        			print( json_encode( pg_fetch_all($orders)) );
        			
        			
        		break;
			}
			return true;
  				
		}
	}
?>

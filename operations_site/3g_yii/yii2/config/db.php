<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host={{ main_database_hostname }};dbname={{ main_database_name }}',
    'username' => '{{ main_database_operations_username }}',
    'password' => '{{ main_database_operations_password }}',
    'charset' => 'utf8',
];

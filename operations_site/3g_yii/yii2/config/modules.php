<?php

use \kartik\datecontrol\Module;

return [
   'datecontrol' =>  [
        'class' => 'kartik\datecontrol\Module',
        // format settings for displaying each date attribute.
        'displaySettings' => [
            Module::FORMAT_DATE => 'MM/dd/yyyy',
            Module::FORMAT_TIME => 'HH:mm:ss a',
            Module::FORMAT_DATETIME => 'MM/dd/yyyy HH:mm:ss a', 
        ],
        // format settings for saving each date attribute.
        'saveSettings' => [
            Module::FORMAT_DATE => 'php:Y-m-d',
            Module::FORMAT_TIME => 'php:H:i:s',
            Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
        ]
    ]
];
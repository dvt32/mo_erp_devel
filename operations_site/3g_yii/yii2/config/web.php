<?php

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'homeUrl' => '/',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '{{ operations_3g_yii_cookie_validation_key }}'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Employee',
            'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'base/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
			'enablePrettyUrl' => true,
            'showScriptName' => false,
			'rules' => [
				'' => 'dashboard/index',
				'employees' => 'employee/index',
				'employees/add' => 'employee/create',
				'employees/<id:\d+>' => 'employee/update',
                'customers/cards' => 'customer/cards',
                'customers/cards/add' => 'customer/card-create',
                'customers/cards/<id:\d+>' => 'customer/card-update'
			]
        ],
        'formatter' => [
            'dateFormat' => 'php:m/d/Y',
            'datetimeFormat' => 'php:m/d/Y H:i:s'
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => require(__DIR__ . '/params.php'),
    'modules' => require(__DIR__ . '/modules.php')
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;

<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\auth\HttpBasicAuth;

class BaseController extends Controller
{	
	public function behaviors()
    {
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [    
			'class' => HttpBasicAuth::className()        
		];                
		return $behaviors;
	}
	
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction'
			]
		];
	}
}

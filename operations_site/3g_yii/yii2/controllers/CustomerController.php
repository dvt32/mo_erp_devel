<?php

namespace app\controllers;

use Yii;
use app\models\RecurCreditCardTxn;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class CustomerController extends BaseController
{
    /**
     * Renders customer credit card list.
     *
     * @route /customers/cards
     */
    public function actionCards()
    {
        return $this->render('cards');
    }
    
    /**
     * Renders new credit card page.
     *
     * @route /customers/cards/add
     */
    public function actionCardCreate()
    {
        $cc = new RecurCreditCardTxn();
        $cc->loadDefaultValues();
        return $this->actionCreditCard($cc);
    }
    
    /**
     * Renders edit/view credit card page.
     *
     * @route /customers/cards/<ID>
     */
    public function actionCardUpdate($id)
    {
        $cc = RecurCreditCardTxn::findOne($id);
        if (!$cc)
        {
            throw new NotFoundHttpException('Credit Card with ID <' . $id . '> does not exist.');
        }
        return $this->actionCreditCard($cc);
    }
    
    /**
     * Creates, updates a credit card or renders the page HTML.
     */
    private function actionCreditCard($model)
    {
        $isNew = $model->isNewRecord;
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', 'Credit card has been successfully ' . ($isNew ? ' created.' : 'updated.'));
                if ($isNew)
                {
                    return $this->redirect(['cardUpdate', 'id' => $model->id]);
                }
                return $this->refresh();
            }
        }
        return $this->render('cardDetails', ['model' => $model]);
    }
}

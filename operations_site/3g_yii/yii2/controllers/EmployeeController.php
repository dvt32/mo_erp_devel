<?php

namespace app\controllers;

use Yii;
use app\models\Employee;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class EmployeeController extends BaseController
{
    /**
     * Renders employee list.
     *
     * @route /employees
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /**
     * Renders new employee page.
     *
     * @route /employees/add
     */
    public function actionCreate()
    {
        $employee = new Employee();
        $employee->loadDefaultValues();
        return $this->actionEmployee($employee);
    }
    
    /**
     * Renders edit/view employee page.
     *
     * @route /employees/<ID>
     */
    public function actionUpdate($id)
    {
        $employee = Employee::findOne($id);
        if (!$employee)
        {
            throw new NotFoundHttpException('Employee with ID <' . $id . '> does not exist.');
        }
        return $this->actionEmployee($employee);
    }
    
    /**
     * Creates, updates an employee or renders the page HTML.
     */
    private function actionEmployee($model)
    {
        if (!Yii::$app->user->identity->hasRole('hr/edit_employee'))
        {
            throw new ForbiddenHttpException('You are not allowed to perform the requested action.');
        }
        $isNew = $model->isNewRecord;
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', 'Employee has been successfully ' . ($isNew ? ' created.' : 'updated.'));
                if ($isNew)
                {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
                return $this->refresh();
            }
            Yii::$app->session->setFlash('error', 'While saving Employee one or few errors have appeared. Check field values and try again.');
        }
        return $this->render('details', ['model' => $model]);
    }
}

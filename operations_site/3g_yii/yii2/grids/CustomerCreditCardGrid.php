<?php

namespace app\grids;

use Yii;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\RecurCreditCardTxn;

class CustomerCreditCardGrid extends RecurCreditCardTxn
{
    // Custom fields.
    public $customer_name;

	public static function widget()
	{
        $searchModel = new self();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
		return GridView::widget([
			'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'formatter' => Yii::$app->formatter,
            'tableOptions' => ['class' => 'table table-striped table-hover table-cc'],
			'columns' => [
				[
					'attribute' => 'id',
					'format' => 'html',
					'value' => function($model, $index, $widget)
					{
						return '<a href="' . Url::toRoute(['customer/card-update', 'id' => $model->id]) . '">' . $model->id . '</a>';
					}
				],
                [
                    'attribute' => 'customer_name',
                    'value' => 'account.name',
                    'label' => 'Customer'
                ],
				'card_number',
                [
                    'attribute' => 'amount',
                    'filter' => [100 => '100+', 1000 => '1000+', 10000 => '10000+', 100000 => '100000+', 1000000 => '1000000+'],
                ],
                [
                    'attribute' => 'next_date',
                    'format' => 'date',
                    'filter' => false
                ],
				[
                    'attribute' => 'creation_dt',
                    'format' => 'datetime',
                    'filter' => false
                ]
			]
		]);
	}
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['id', 'customer_name'], 'safe'];
        return $rules;
    }
	
	public function search($params)
	{
        $query = RecurCreditCardTxn::find()->joinWith('account');
        if ($this->load($params))
        {
            $query->andFilterWhere(['recur_card_txn_tbl.id' => strlen($this->id) ? (int)$this->id : '']);
            $query->andFilterWhere(['card_number' => $this->card_number]);
            $query->andFilterWhere(['like', 'cust_acct_tbl.name', $this->customer_name]);
            $query->andFilterWhere(['>=', 'amount', $this->amount]);
        }
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['creation_dt' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			]
		]);
        $dataProvider->sort->attributes['customer_name'] = [
            'asc' => ['cust_acct_tbl.name' => SORT_ASC],
            'desc' => ['cust_acct_tbl.name' => SORT_DESC]
        ];
        return $dataProvider;
	}
}

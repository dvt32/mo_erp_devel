<?php

namespace app\grids;

use Yii;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Employee;

class EmployeeGrid extends Employee
{
	public static function widget()
	{
        $searchModel = new self();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
		return GridView::widget([
			'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-hover table-employee'],
			'columns' => [
				[
					'attribute' => 'id',
					'format' => 'html',
					'value' => function($model, $index, $widget)
					{
						if (Yii::$app->user->identity->hasRole('hr/edit_employee'))
						{
							return '<a href="' . Url::toRoute(['employee/update', 'id' => $model->id]) . '">' . $model->id . '</a>';
						}
						return $model->id;
					}
				],
				'first_name',
				'last_name',
				'email',
				[
					'attribute' => 'is_active',
					'format' => 'html',
                    'filter' => [1 => 'Yes', 0 => 'No'],
					'value' => function($model, $index, $widget)
					{
						return $model->is_active ? 'Yes' : 'No';
					}
				]
			]
		]);
	}
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['id', 'safe'];
        return $rules;
    }
	
	public function search($params)
	{
        $query = self::find();
        if ($this->load($params))
        {
            $query->andFilterWhere(['id' => strlen($this->id) ? (int)$this->id : '']);
            $query->andFilterWhere(['is_active' => $this->is_active]);
            $query->andFilterWhere(['like', 'first_name', $this->first_name]);
            $query->andFilterWhere(['like', 'last_name', $this->last_name]);
            $query->andFilterWhere(['like', 'email', $this->email]);
        }
		return new ActiveDataProvider([
			'query' => $query,
			'sort'=> ['defaultOrder' => ['last_name' => SORT_ASC]],
			'pagination' => [
				'pageSize' => 20
			]
		]);
	}
}

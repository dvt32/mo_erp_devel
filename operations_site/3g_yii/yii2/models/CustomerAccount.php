<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cust_acct_tbl".
 *
 * @property integer $id
 * @property string $name
 * @property integer $primary_contact_cust_pers_id
 * @property boolean $is_active
 * @property string $ms_access_creation_dt
 * @property integer $ms_access_random_num
 * @property string $creation_dt
 * @property string $external_name
 * @property string $description
 * @property string $web_site
 * @property boolean $visible
 * @property string $type
 * @property string $note
 * @property string $tax_id
 * @property integer $service_id
 * @property string $preferred_payment_method
 * @property integer $room_minutes
 * @property integer $primary_floor_id
 * @property string $pkg_notify_email
 * @property string $logo_url
 * @property string $status
 * @property integer $modifier_id
 * @property string $brand
 * @property string $modified
 * @property integer $billing_reviewer_id
 * @property string $billing_last_reviewed_dt
 * @property integer $billing_memos_inbox_count
 * @property integer $billing_memos_archived_count
 * @property string $effective_end_date
 * @property string $potential_revenue
 * @property string $amount_due
 * @property string $amount_paid
 * @property string $balance
 * @property string $pay_terms
 * @property string $qb_name
 * @property integer $aging_count
 * @property double $new_balance
 * @property string $aging_last_contacted_dt
 * @property string $aging_followup_date
 */
class CustomerAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_acct_tbl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'primary_floor_id'], 'required'],
            [['primary_contact_cust_pers_id', 'ms_access_random_num', 'service_id', 'room_minutes', 'primary_floor_id', 'modifier_id', 'billing_reviewer_id', 'billing_memos_inbox_count', 'billing_memos_archived_count', 'aging_count'], 'integer'],
            [['is_active', 'visible'], 'boolean'],
            [['ms_access_creation_dt', 'creation_dt', 'modified', 'billing_last_reviewed_dt', 'effective_end_date', 'aging_last_contacted_dt', 'aging_followup_date'], 'safe'],
            [['note', 'pkg_notify_email'], 'string'],
            [['potential_revenue', 'amount_due', 'amount_paid', 'balance', 'new_balance'], 'number'],
            [['name', 'description', 'web_site', 'logo_url', 'pay_terms', 'qb_name'], 'string', 'max' => 255],
            [['external_name'], 'string', 'max' => 64],
            [['type', 'tax_id'], 'string', 'max' => 32],
            [['preferred_payment_method'], 'string', 'max' => 24],
            [['status'], 'string', 'max' => 20],
            [['brand'], 'string', 'max' => 16],
            [['name'], 'unique'],
            [['ms_access_creation_dt', 'ms_access_random_num'], 'unique', 'targetAttribute' => ['ms_access_creation_dt', 'ms_access_random_num'], 'message' => 'The combination of Ms Access Creation Dt and Ms Access Random Num has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'primary_contact_cust_pers_id' => 'Primary Contact Cust Pers ID',
            'is_active' => 'Is Active',
            'ms_access_creation_dt' => 'Ms Access Creation Dt',
            'ms_access_random_num' => 'Ms Access Random Num',
            'creation_dt' => 'Creation Dt',
            'external_name' => 'External Name',
            'description' => 'Description',
            'web_site' => 'Web Site',
            'visible' => 'Visible',
            'type' => 'Type',
            'note' => 'Note',
            'tax_id' => 'Tax ID',
            'service_id' => 'Service ID',
            'preferred_payment_method' => 'Preferred Payment Method',
            'room_minutes' => 'Room Minutes',
            'primary_floor_id' => 'Primary Floor ID',
            'pkg_notify_email' => 'Pkg Notify Email',
            'logo_url' => 'Logo Url',
            'status' => 'Status',
            'modifier_id' => 'Modifier ID',
            'brand' => 'Brand',
            'modified' => 'Modified',
            'billing_reviewer_id' => 'Billing Reviewer ID',
            'billing_last_reviewed_dt' => 'Billing Last Reviewed Dt',
            'billing_memos_inbox_count' => 'Billing Memos Inbox Count',
            'billing_memos_archived_count' => 'Billing Memos Archived Count',
            'effective_end_date' => 'Effective End Date',
            'potential_revenue' => 'Potential Revenue',
            'amount_due' => 'Amount Due',
            'amount_paid' => 'Amount Paid',
            'balance' => 'Balance',
            'pay_terms' => 'Pay Terms',
            'qb_name' => 'Qb Name',
            'aging_count' => 'Aging Count',
            'new_balance' => 'New Balance',
            'aging_last_contacted_dt' => 'Aging Last Contacted Dt',
            'aging_followup_date' => 'Aging Followup Date',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(RecurCreditCardTxn::className(), ['account_id' => 'id']);
    }
}

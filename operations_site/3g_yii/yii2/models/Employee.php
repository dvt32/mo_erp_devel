<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "employee_tbl".
 *
 * @property integer $id
 * @property string $salutation
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $nickname
 * @property string $email
 * @property string $work_phone_number
 * @property string $work_fax_number
 * @property string $home_phone_number
 * @property string $cell_phone_number
 * @property string $other_phone_number
 * @property string $title
 * @property string $company
 * @property string $work_address_1
 * @property string $work_address_2
 * @property string $work_city
 * @property string $work_state
 * @property string $work_zip_code
 * @property string $work_country
 * @property string $home_address_1
 * @property string $home_address_2
 * @property string $home_city
 * @property string $home_state
 * @property string $home_zip_code
 * @property string $home_country
 * @property string $birthday
 * @property string $tax_id
 * @property boolean $is_active
 * @property string $note
 * @property string $ms_access_creation_dt
 * @property integer $ms_access_random_num
 * @property string $creation_dt
 * @property string $email2
 * @property string $email3
 * @property string $type
 * @property boolean $in_directory
 * @property string $password
 * @property string $suffix
 * @property string $allowed_ip_addr
 * @property string $phone_pin
 * @property integer $vacation_days
 * @property boolean $has_assigned_task
 * @property string $windows_logon_name
 * @property string $windows_domain_name
 * @property string $windows_upn_suffix
 * @property string $hash
 * @property string $salt
 * @property integer $modifier_id
 * @property boolean $reload_roles
 * @property string $modified
 * @property boolean $in_pay_period_report
 * @property boolean $do_email_reminder
 * @property string $time_zone
 * @property string $pg_username
 * @property string $pg_password
 */
class Employee extends \yii\db\ActiveRecord implements IdentityInterface
{
	/**
	 * Authorized employee.
	 */ 
	static protected $_employee;
	
	/**
	 * Employees' roles.
	 */
	static private $_roles = [];
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_tbl';
    }
    
    /**
     * Searches for an employee by username that extracted from the REMOTE_USER environment variable.
     * 
     * @return static|null
     */ 
    public static function findByEnvironment()
    {
		if (static::$_employee === null)
		{
			$username = Yii::$app->getRequest()->getAuthUser();
			if (!empty($username))
			{
				// if Apache falls back to Basic authentication (for non-Windows machines), strip off @ suffix
				$username = strtolower(preg_replace('/@.*$/', '', $username));
				static::$_employee = static::findOne(['windows_logon_name' => $username, 'is_active' => true]);
			}
		}
		return static::$_employee;
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'tax_id'], 'required'],
            ['email', 'email'],
            ['birthday', 'string', 'max' => 10],
            [['birthday', 'ms_access_creation_dt', 'creation_dt', 'modified'], 'safe'],
            [['is_active', 'in_directory', 'has_assigned_task', 'reload_roles', 'in_pay_period_report', 'do_email_reminder'], 'boolean'],
            [['note'], 'string'],
            [['ms_access_random_num', 'vacation_days', 'modifier_id'], 'integer'],
            [['salutation', 'work_zip_code', 'home_zip_code', 'tax_id', 'type', 'phone_pin'], 'string', 'max' => 16],
            ['tax_id', 'match', 'pattern' => '/^\d{3}-\d{2}-\d{4}$/i'],
            [['first_name', 'middle_name', 'nickname', 'work_phone_number', 'work_fax_number', 'home_phone_number', 'cell_phone_number', 'other_phone_number', 'work_state', 'home_state', 'password', 'suffix', 'time_zone'], 'string', 'max' => 32],
            [['last_name'], 'string', 'max' => 48],
            [['email', 'work_city', 'work_country', 'home_city', 'home_country', 'email2', 'email3'], 'string', 'max' => 64],
            [['title'], 'string', 'max' => 80],
            [['company', 'work_address_1', 'work_address_2', 'home_address_1', 'home_address_2'], 'string', 'max' => 128],
            [['allowed_ip_addr', 'windows_domain_name'], 'string', 'max' => 15],
            [['windows_logon_name'], 'string', 'max' => 20],
            [['windows_logon_name'], 'unique', 'targetAttribute' => ['windows_logon_name'], 'message' => 'Such windows logon name has already been taken.'],
            [['windows_upn_suffix'], 'string', 'max' => 255],
            [['hash'], 'string', 'max' => 60],
            [['salt'], 'string', 'max' => 29],
            [['pg_username', 'pg_password'], 'string', 'max' => 63],
            [['ms_access_creation_dt', 'ms_access_random_num'], 'unique', 'targetAttribute' => ['ms_access_creation_dt', 'ms_access_random_num'], 'message' => 'The combination of Ms Access Creation Dt and Ms Access Random Num has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'salutation' => 'Salutation',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'nickname' => 'Nickname',
            'email' => 'Email',
            'work_phone_number' => 'Work Phone Number',
            'work_fax_number' => 'Work Fax Number',
            'home_phone_number' => 'Home Phone Number',
            'cell_phone_number' => 'Cell Phone Number',
            'other_phone_number' => 'Other Phone Number',
            'title' => 'Title',
            'company' => 'Company',
            'work_address_1' => 'Work Address',
            'work_address_2' => 'Work Address 2',
            'work_city' => 'Work City',
            'work_state' => 'Work State',
            'work_zip_code' => 'Work Zip Code',
            'work_country' => 'Work Country',
            'home_address_1' => 'Home Address',
            'home_address_2' => 'Home Address 2',
            'home_city' => 'Home City',
            'home_state' => 'Home State',
            'home_zip_code' => 'Home Zip Code',
            'home_country' => 'Home Country',
            'birthday' => 'Birthday',
            'tax_id' => 'Tax ID',
            'is_active' => 'Active',
            'note' => 'Note',
            'ms_access_creation_dt' => 'Ms Access Creation Dt',
            'ms_access_random_num' => 'Ms Access Random Num',
            'creation_dt' => 'Creation Dt',
            'email2' => 'Email2',
            'email3' => 'Email3',
            'type' => 'Type',
            'in_directory' => 'In Directory',
            'password' => 'Password',
            'suffix' => 'Suffix',
            'allowed_ip_addr' => 'Allowed Ip Addr',
            'phone_pin' => 'Phone Pin',
            'vacation_days' => 'Vacation Days',
            'has_assigned_task' => 'Has Assigned Task',
            'windows_logon_name' => 'Windows Logon Name',
            'windows_domain_name' => 'Windows Domain Name',
            'windows_upn_suffix' => 'Windows UPN Suffix',
            'hash' => 'Hash',
            'salt' => 'Salt',
            'modifier_id' => 'Modifier ID',
            'reload_roles' => 'Reload Roles',
            'modified' => 'Modified',
            'in_pay_period_report' => 'In Pay Period Report',
            'do_email_reminder' => 'Do Email Reminder',
            'time_zone' => 'Time Zone',
            'pg_username' => 'Pg Username',
            'pg_password' => 'Pg Password',
        ];
    }
    
    /**
     * Loads default values from database table schema
     *
     * @param boolean $skipIfSet whether existing value should be preserved.
     * This will only set defaults for attributes that are `null`.
     * @return static the model instance itself.
     */
    public function loadDefaultValues($skipIfSet = true)
    {
        parent::loadDefaultValues($skipIfSet);
        $this->windows_domain_name = 'microoffice';
        $this->windows_upn_suffix = 'microoffice.com';
        return $this;
    }
    
    /**
     * Returns full name of a employee.
     * 
     * @return string
     */ 
    public function getFullName()
    {
		return trim($this->first_name . ' ' . $this->last_name);
	}
	
	/**
	 * Checks if an employee already has the target role.
	 * 
	 * @param string|integer - the role name or identifier.
	 * @return boolean - returns TRUE if the employee already possesses role and FALSE otherwise.
	 */ 
	public function hasRole($role)
	{
		if (empty(self::$_roles[$this->id]))
		{
			$res = [];
			$rows = EmployeeRole::find()
						->select(['id', 'element_value'])
						->where(['parent_id' => $this->id])
						->asArray()
						->all();
			foreach ($rows as $row)
			{
				$res[$row['id']] = true;
				$res[substr($row['element_value'], 5)] = true;
			}
			self::$_roles[$this->id] = $res;
		}
		return isset(self::$_roles[$this->id][$role]);
	}
    
    // IdentityInterface's methods
    // -----------------------------------------------------------------------------------------
    
    public static function findIdentity($id)
    {		
		return static::findOne($id);	        
	}								
					
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findByEnvironment();
	}

	public function getId()								    
	{												
		return $this->id;											      
	}
	
	public function getAuthKey()														    
	{																	
		throw new NotSupportedException('"getAuthKey" is not implemented.');
	}
																				
	public function validateAuthKey($authKey)
	{
		throw new NotSupportedException('"validateAuthKey" is not implemented.');
	}
}

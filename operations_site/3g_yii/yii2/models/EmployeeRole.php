<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_employee_x_role_tbl".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $element_value
 * @property string $ms_access_creation_dt
 * @property integer $ms_access_random_num
 * @property string $creation_dt
 */
class EmployeeRole extends \yii\db\ActiveRecord
{	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ss_employee_x_role_tbl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'element_value'], 'required'],
            [['parent_id', 'ms_access_random_num'], 'integer'],
            [['ms_access_creation_dt', 'creation_dt'], 'safe'],
            [['element_value'], 'string', 'max' => 255],
            [['ms_access_creation_dt', 'ms_access_random_num'], 'unique', 'targetAttribute' => ['ms_access_creation_dt', 'ms_access_random_num'], 'message' => 'The combination of Ms Access Creation Dt and Ms Access Random Num has already been taken.'],
            [['parent_id', 'element_value'], 'unique', 'targetAttribute' => ['parent_id', 'element_value'], 'message' => 'The combination of Parent ID and Element Value has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'element_value' => 'Element Value',
            'ms_access_creation_dt' => 'Ms Access Creation Dt',
            'ms_access_random_num' => 'Ms Access Random Num',
            'creation_dt' => 'Creation Dt',
        ];
    }
}

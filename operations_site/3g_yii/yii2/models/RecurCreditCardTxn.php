<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recur_card_txn_tbl".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $employee_id
 * @property string $amount
 * @property string $process_mode
 * @property string $frequency
 * @property string $next_date
 * @property string $card_number
 * @property string $card_expiration_month
 * @property string $card_expiration_year
 * @property string $card_security_code
 * @property string $card_bill_name
 * @property string $card_bill_company
 * @property string $card_bill_address_num
 * @property string $card_bill_zip_code
 * @property string $card_bill_address_1
 * @property string $card_bill_address_2
 * @property string $card_bill_city
 * @property string $card_bill_state
 * @property string $card_bill_country
 * @property string $card_bill_phone_number
 * @property string $card_bill_fax_number
 * @property string $card_bill_email_address
 * @property string $note
 * @property string $ms_access_creation_dt
 * @property integer $ms_access_random_num
 * @property string $creation_dt
 */
class RecurCreditCardTxn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recur_card_txn_tbl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'amount', 'frequency', 'next_date', 'card_number', 'card_expiration_month', 'card_expiration_year', 'card_bill_name', 'card_bill_zip_code', 'card_bill_address_1', 'card_bill_city', 'card_bill_state', 'card_bill_country'], 'required'],
            [['account_id', 'employee_id', 'ms_access_random_num'], 'integer'],
            [['amount'], 'number'],
            [['next_date', 'ms_access_creation_dt', 'creation_dt'], 'safe'],
            [['note'], 'string'],
            [['process_mode'], 'string', 'max' => 12],
            [['frequency', 'card_bill_address_num', 'card_bill_zip_code'], 'string', 'max' => 16],
            [['card_number'], 'string', 'max' => 24],
            [['card_expiration_month'], 'string', 'max' => 2],
            [['card_expiration_year', 'card_security_code'], 'string', 'max' => 4],
            [['card_bill_name', 'card_bill_company', 'card_bill_state', 'card_bill_country', 'card_bill_email_address'], 'string', 'max' => 64],
            [['card_bill_address_1', 'card_bill_address_2'], 'string', 'max' => 128],
            [['card_bill_city'], 'string', 'max' => 48],
            [['card_bill_phone_number', 'card_bill_fax_number'], 'string', 'max' => 32],
            [['ms_access_creation_dt', 'ms_access_random_num'], 'unique', 'targetAttribute' => ['ms_access_creation_dt', 'ms_access_random_num'], 'message' => 'The combination of Ms Access Creation Dt and Ms Access Random Num has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'employee_id' => 'Employee ID',
            'amount' => 'Amount',
            'process_mode' => 'Process Mode',
            'frequency' => 'Frequency',
            'next_date' => 'Next Date',
            'card_number' => 'Card Number',
            'card_expiration_month' => 'Card Expiration Month',
            'card_expiration_year' => 'Card Expiration Year',
            'card_security_code' => 'Card Security Code',
            'card_bill_name' => 'Card Bill Name',
            'card_bill_company' => 'Card Bill Company',
            'card_bill_address_num' => 'Card Bill Address Num',
            'card_bill_zip_code' => 'Card Bill Zip Code',
            'card_bill_address_1' => 'Card Bill Address 1',
            'card_bill_address_2' => 'Card Bill Address 2',
            'card_bill_city' => 'Card Bill City',
            'card_bill_state' => 'Card Bill State',
            'card_bill_country' => 'Card Bill Country',
            'card_bill_phone_number' => 'Card Bill Phone Number',
            'card_bill_fax_number' => 'Card Bill Fax Number',
            'card_bill_email_address' => 'Card Bill Email Address',
            'note' => 'Note',
            'ms_access_creation_dt' => 'Ms Access Creation Dt',
            'ms_access_random_num' => 'Ms Access Random Num',
            'creation_dt' => 'Created At',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(CustomerAccount::className(), ['id' => 'account_id']);
    }
}

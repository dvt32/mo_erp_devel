<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use app\helpers\USAStates;

$title = ($model->id ? 'Edit' : 'Add') . ' Credit Card';
$this->title = Yii::$app->params['appName'] . ' | ' . $title;
$this->params['breadcrumbs'] = [['label' => 'Dashboard', 'url' => ['dashboard/index']], ['label' => 'Credit Cards', 'url' => ['customer/cards']], $title];

?>
<h1 class="page-header"><?= $title ?></h1>
<?php $form = ActiveForm::begin(['id' => 'customer-cc-form']); ?>
    <?= $form->field($model, 'amount') ?>
    <?= $form->field($model, 'card_number') ?>
    <?= $form->field($model, 'card_expiration_month') ?>
    <?= $form->field($model, 'card_expiration_year') ?>

    <?= $form->field($model, 'note')->textArea(['style' => 'resize:vertical;']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Url;
use app\grids\CustomerCreditCardGrid;

$this->title = Yii::$app->params['appName'] . ' | Customer Credit Cards';
$this->params['breadcrumbs'] = [['label' => 'Dashboard', 'url' => ['dashboard/index']], 'Credit Cards'];

?>
<h1 class="page-header">Credit Cards <a href="<?= Url::toRoute('customer/card-create') ?>" class="btn btn-lg btn-primary pull-right">Add Credit Card</a></h1>
<?php \yii\widgets\Pjax::begin(); ?>
<?= CustomerCreditCardGrid::widget() ?>
<?php \yii\widgets\Pjax::end(); ?>
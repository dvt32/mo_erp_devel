<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use app\helpers\USAStates;

$title = ($model->id ? 'Edit' : 'Add') . ' Employee';
$this->title = Yii::$app->params['appName'] . ' | ' . $title;
$this->params['breadcrumbs'] = [['label' => 'Dashboard', 'url' => ['dashboard/index']], ['label' => 'Employees', 'url' => ['employee/index']], $title];

$domains = [
    '' => '',
    'microoffice.com' => 'microoffice.com',
    'coalitionspace.com' => 'coalitionspace.com',
];

if (empty($domains[$model->windows_upn_suffix]))
{
    $domains[$model->windows_upn_suffix] = $model->windows_upn_suffix;
}

?>
<h1 class="page-header"><?= $title ?></h1>
<?php $form = ActiveForm::begin(['id' => 'employee-form']); ?>
    <?= $form->field($model, 'salutation')->dropDownList(['Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss']) ?>
    <?= $form->field($model, 'first_name') ?>
    <?= $form->field($model, 'last_name') ?>
    <?= $form->field($model, 'middle_name') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'birthday')->widget(DateControl::classname(), [
        'type' => DateControl::FORMAT_DATE,
        'ajaxConversion' => false,
        'options' => ['pluginOptions' => ['autoclose' => true]]
    ]) ?>
    <?= $form->field($model, 'work_phone_number')->widget(MaskedInput::classname(), ['mask' => '999-999-9999']) ?>
    <?= $form->field($model, 'work_fax_number')->widget(MaskedInput::classname(), ['mask' => '999-999-9999']) ?>
    <?= $form->field($model, 'home_phone_number')->widget(MaskedInput::classname(), ['mask' => '999-999-9999']) ?>
    <?= $form->field($model, 'cell_phone_number')->widget(MaskedInput::classname(), ['mask' => '999-999-9999']) ?>
    <?= $form->field($model, 'other_phone_number')->widget(MaskedInput::classname(), ['mask' => '999-999-9999']) ?>
    <?= $form->field($model, 'work_address_1') ?>
    <?= $form->field($model, 'work_city') ?>
    <?= $form->field($model, 'work_state')->dropDownList(USAStates::getStates(), ['prompt' => '']) ?>
    <?= $form->field($model, 'work_zip_code') ?>
    <?= $form->field($model, 'home_address_1') ?>
    <?= $form->field($model, 'home_city') ?>
    <?= $form->field($model, 'home_state')->dropDownList(USAStates::getStates(), ['prompt' => '']) ?>
    <?= $form->field($model, 'home_zip_code') ?>
    <?= $form->field($model, 'is_active')->checkbox() ?>
    <?= $form->field($model, 'in_directory')->checkbox() ?>
    <?= $form->field($model, 'tax_id')->widget(MaskedInput::classname(), ['mask' => '999-99-9999']) ?>
    <?= $form->field($model, 'windows_logon_name') ?>
    <?= $form->field($model, 'windows_domain_name')->dropDownList(['' => '', 'microoffice' => 'microoffice']) ?>
    <?= $form->field($model, 'windows_upn_suffix')->dropDownList($domains) ?>
    <?= $form->field($model, 'time_zone')->dropDownList(['America/New_York' => 'America/New_York', 'America/Chicago' => 'America/Chicago']) ?>
    <?= $form->field($model, 'note')->textArea(['style' => 'resize:vertical;']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
<?php ActiveForm::end(); ?>
<?php $this->registerJsFile('@web/js/employee_details.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]); ?>
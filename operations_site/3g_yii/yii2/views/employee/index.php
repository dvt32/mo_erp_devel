<?php

use yii\helpers\Url;
use app\grids\EmployeeGrid;

$this->title = Yii::$app->params['appName'] . ' | Employees';
$this->params['breadcrumbs'] = [['label' => 'Dashboard', 'url' => ['dashboard/index']], 'Employees'];

?>
<h1 class="page-header">Employees <a href="<?= Url::toRoute('employee/create') ?>" class="btn btn-lg btn-primary pull-right">Add Employee</a></h1>
<?php \yii\widgets\Pjax::begin(); ?>
<?= EmployeeGrid::widget() ?>
<?php \yii\widgets\Pjax::end(); ?>
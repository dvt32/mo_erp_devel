<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Alert;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>">
                <img src="/2g/webroot/img/logo.png" alt="MicroOffice" />
            </a>
            <ul class="nav pull-right">
                <li class="welcome">You are logged in as <strong><?= Yii::$app->user->identity->getFullName() ?></strong></li>
			</ul>
		</div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-3 col-md-2 sidebar">
				<div class="nav nav-sidebar">
                    <?= Menu::widget([
							'encodeLabels' => false,
							'items' => [    
								[
									'label' => '<span class="glyphicon glyphicon-dashboard"></span> Dashboard',
									'url' => ['dashboard/index']
								],
                                [
									'label' => '<span class="glyphicon glyphicon-user"></span> Employees',
									'url' => ['employee/index'],
									'items' => Yii::$app->user->identity->hasRole('hr/edit_employee') ? [
										['label' => 'Add Employee', 'url' => ['employee/create']],       
									] : []
								],
                                [
                                    'label' => '<span class="glyphicon glyphicon-usd"></span> Customer Credit Cards',
                                    'url' => ['customer/cards'],
                                    'items' => [
                                        ['label' => 'Add Credit Card', 'url' => ['customer/card-create']],       
									]
                                ]
                             ]
                         ]);
					?>
                </div>
            </div>
            <div class="col-xs-9 col-xs-offset-3 col-md-10 col-md-offset-2 main">
				<?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <?= Alert::widget(['options' => ['class' => 'alert-success'], 'body' => Yii::$app->session->getFlash('success')]) ?>
                <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                    <?= Alert::widget(['options' => ['class' => 'alert-danger'], 'body' => Yii::$app->session->getFlash('error')]) ?>
                <?php endif; ?>
				<?= $content ?>
			</div>
        </div>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
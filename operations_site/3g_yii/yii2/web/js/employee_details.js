$(function()
{
    $('#employee-birthday-disp').inputmask({mask: '99/99/9999'});
    setTimeout(function()
    {
        $('#employee-form').data('yiiActiveForm').attributes[5].validate = function(attribute, value, messages, deferred, $form)
        {
            yii.validation.regularExpression($('#employee-birthday-disp').val(), messages, {'pattern':/^\d{2}\/\d{2}\/\d{4}$/, 'not': false, message: 'Birthday date is invalid.', 'skipOnEmpty': 1});
        };
    }, 100);
});
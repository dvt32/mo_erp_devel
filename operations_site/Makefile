### Makefile to install code into the /var/www hierarchy.

include $(repo_root)/common/func.mk
include $(repo_root)/common/gmsl

subdirs := 1g 2g 3g 3g_yii $(operations_flask)

install-extra:
	@-mkdir -p $(operations_basedir)
	@cp $(CURDIR)/root/index.html $(operations_basedir)/index.html

websetup:
	@$(call install_packages,php5-curl php5-pgsql php5-json libapache2-mod-php5)
ifeq ($(is_production_deployment), 1)
ifeq ($(secret_base_dir),)
	$(error websetup-operations requires secret_base_dir on production server (i.e. when is_production_deployment is 1))
else
	@$(call check_file_exist,$(secret_base_dir)/kerberos/operationsweb.keytab)
endif
	@$(MAKE) -C $(operations_flask) websetup
# copy configs for operations and operations-ssl to Apache sites folder
# need these all in one continuously running shell so that DO_APACHE_RESTART state persists through to end
	@:; \
	OP_CONF=/etc/apache2/sites-available/operations.conf; \
	: `# if operations.conf missing or changed, copy new one`; \
	if [ ! -e "$$OP_CONF" ] || [ "$(CURDIR)/apacheconf/production/operations.conf" -nt "$$OP_CONF" ] || [ "$(CURDIR)/../system.properties" -nt "$$OP_CONF" ]; then \
		$(call copy_subst,$(CURDIR)/apacheconf/production/operations.conf,$$OP_CONF,root.root,0644,,$(system_properties)); \
		DO_APACHE_RESTART=1; \
	fi; \
	OPSSL_CONF=/etc/apache2/sites-available/operations-ssl.conf; \
	: `# if operations-ssl.conf missing or changed, copy new one`; \
	if [ ! -e "$$OPSSL_CONF" ] || [ "$(CURDIR)/apacheconf/production/operations-ssl.conf" -nt "$$OPSSL_CONF" ] || [ "$(CURDIR)/../system.properties" -nt "$$OPSSL_CONF" ]; then \
		$(call copy_subst,$(CURDIR)/apacheconf/production/operations-ssl.conf,$$OPSSL_CONF,root.root,0644,,$(system_properties)); \
		DO_APACHE_RESTART=1; \
	fi; \
	: `# enable operations and operations-ssl sites if not already enabled`; \
	if [ ! -e "/etc/apache2/sites-enabled/operations.conf" ]; then \
	        a2ensite operations; \
	        DO_APACHE_RESTART=1; \
	fi; \
	if [ ! -e "/etc/apache2/sites-enabled/operations-ssl.conf" ]; then \
	        a2ensite operations-ssl; \
	        DO_APACHE_RESTART=1; \
	fi; \
	: `# create directory for Kerberos keytab`; \
	mkdir -p /etc/apache2/kerberos; \
	: `# copy keytab to proper location`; \
	KEYTAB_FILE=/etc/apache2/kerberos/operationsweb.keytab; \
	if [ ! -e "$$KEYTAB_FILE" ] || [ "$(secret_base_dir)/kerberos/operationsweb.keytab" -nt "$$KEYTAB_FILE" ]; then \
	        cp -a $(secret_base_dir)/kerberos/operationsweb.keytab $$KEYTAB_FILE; \
	        chown www-data.www-data $$KEYTAB_FILE; \
	        chmod 0400 $$KEYTAB_FILE; \
	        DO_APACHE_RESTART=1; \
	fi; \
	: `# restart Apache if configuration files changed`; \
	if [ "$$DO_APACHE_RESTART" = "1" ]; then \
	        echo Restarting apache2; \
	        if [ -z "$(use_systemd)" ] || [ "$(use_systemd)" = "0" ]; then \
		        service apache2 restart; \
	        else \
		        systemctl restart apache2; \
	        fi; \
	fi
	@echo You must create a DNS mapping from $(operations_fqdn) to the IP address of the web server.
else
	@:; \
	OP_CONF=/etc/apache2/sites-available/operations.conf; \
	if [ ! -e "$$OP_CONF" ] || [ "$(CURDIR)/apacheconf/devel/operations.conf" -nt "$$OP_CONF" ] || [ "$(CURDIR)/../system.properties" -nt "$$OP_CONF" ]; then \
		cp -a $(CURDIR)/apacheconf/devel/operations.conf $$OP_CONF; \
		chown root.root $$OP_CONF; \
		chmod 0644 $$OP_CONF; \
		sed -i s/"{{ operations_ip_addr }}"/"$(operations_ip_addr)"/g $$OP_CONF; \
		sed -i s/"{{ operations_fqdn }}"/"$(operations_fqdn)"/g $$OP_CONF; \
		DO_APACHE_RESTART=1; \
	fi; \
	if [ ! -e "/etc/apache2/sites-enabled/operations.conf" ]; then \
		a2ensite operations; \
		DO_APACHE_RESTART=1; \
	fi; \
	if [ "$$DO_APACHE_RESTART" = "1" ]; then \
		echo Restarting apache2; \
		if [ -z "$(use_systemd)" ] || [ "$(use_systemd)" = "0" ]; then \
			service apache2 restart; \
		else \
			systemctl restart apache2; \
		fi; \
	fi
	@if [ ! -e "/etc/apache2/conf-enabled/operations_passwd" ]; then \
		touch /etc/apache2/conf-enabled/operations_passwd; \
		chmod 640 /etc/apache2/conf-enabled/operations_passwd; \
		chown root:www-data /etc/apache2/conf-enabled/operations_passwd; \
	fi
	@htpasswd -b /etc/apache2/conf-enabled/operations_passwd $(operations_employee_logon) $(operations_employee_password)
	@echo On the machine running the browser, create a DNS mapping from $(operations_fqdn) to the IP address of the web server.
endif

include ../common/site.mk

# eof

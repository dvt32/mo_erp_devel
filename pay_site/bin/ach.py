import psycopg2.extras
from flask import request, render_template
from flask import g, flash, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, BooleanField, HiddenField
from wtforms.fields.html5 import DateField, DecimalField
from wtforms import validators, ValidationError
import smtplib
import datetime
import random
import jinja2
import decimal
import re
import database
import chargefacade
import tzconvert
import json
from dateutil.relativedelta import relativedelta
from chargefacade import ChargeMail
import bank_account
import onetime
import forms
import util

class AchFields(FlaskForm):
  pmt_vault_id = HiddenField('pmt_vault_id', default='')
  routing_number = StringField('Bank Routing Number', validators=[validators.InputRequired(),validators.Regexp(r'\d{9}|\d+\*+\d+')], render_kw={'maxlength':'9', 'placeholder': 'Routing number'})
  account_number = StringField('Bank Account Number', validators=[validators.InputRequired(),validators.Regexp(r'\d{4,17}|\d+\*+\d+')], render_kw={'placeholder': 'Account number'})
  account_type = SelectField('Account Type', choices=[('checking', 'Checking'), ('savings', 'Savings')], default='checking', validators=[validators.InputRequired()])
  owned_by = SelectField('Account Ownership', choices=[('business', 'Business'), ('personal', 'Personal')], default='business', validators=[validators.InputRequired()])
  registered_name = StringField('Person or Business Named on Account', validators=[validators.InputRequired(), validators.Length(max=64)], render_kw={'maxlength':'64', 'placeholder': 'Name on account'})
  
  def validate_routing_number(form, field):
    if field.data.isdigit():
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM fedach_participant_tbl WHERE routing_number = %s', (field.data,))
      fedach_participant = curdict.fetchone()
      if not fedach_participant:
        raise ValidationError('invalid routing number: %s' % (field.data,))
      else:
        return True
    if form.pmt_vault_id.data == "" and not field.data.isdigit():
        return False
    else:
      return True

  def validate_account_number(form, field):
    if field.data.isdigit():
        return True
    if form.pmt_vault_id.data == "" and not field.data.isdigit():
        return False
    else:
      return True

class AchPmtForm(AchFields):
  amount = DecimalField('Amount', validators=[validators.InputRequired(), validators.NumberRange(min=0.01)], places=2, render_kw={'placeholder': 'Amount'})
  scheduled_date = DateField('Scheduled Date', validators=[validators.InputRequired()], default=datetime.date.today())
  note = StringField('Note', render_kw={'placeholder': 'Optional note'})
  save_pmt_method = BooleanField('save_pmt_method', description="Save Payment Method for Future Use", default=False)
  enroll_in_autopay = BooleanField('enroll_in_autopay', default=True)
  submitForm = SubmitField('Pay')
  
  def fields_append(form):
    for field in form:
      field.id = (field.id) + '-ach'
      field.label.field_id = field.label.field_id + '-ach'

  def validate_scheduled_date(form, field):
    conn = database.get_connection()
    cust_timezone = tzconvert.cust_timezone(conn, g.user.cust_acct_id)
    # get current datetime in customer's time zone
    now_local = datetime.datetime.now(cust_timezone)
    # extract date portion of datetime to obtain current date from user's perspective
    today_local = now_local.date()
    if not field.data:
      raise ValidationError('Dates must be formatted as yyyy-mm-dd in browsers without a datepicker')
    if field.data < today_local:
      raise ValidationError('scheduled date in past: %s; must be today or future; today is %s' % (str(field.data), str(today_local)))
    elif (field.data.year >= today_local.year and field.data.month > today_local.month):
      raise ValidationError('scheduled date is more than a year in the future: %s; today is %s' % (str(field.data), str(today_local)))
  
def get_form_ach():
  form = AchPmtForm(scheduled_date=forms.get_user_date())
  form.fields_append()
  return form

def get_saved_achs():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT id, account_number, routing_number, account_type, owned_by, registered_name FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s AND pmt_method = %s ORDER BY pref_rank', (g.user.cust_acct_id, True, "ach"))
  ach_list = [dict(row) for row in curdict]
  for ach in ach_list:
    curdict.execute('SELECT customer_name FROM fedach_participant_tbl WHERE routing_number = %s', (ach['routing_number'],))
    bank = curdict.fetchone()
    if not bank:
      bank = ""
    else: 
      bank = bank['customer_name'].title()
    ach['bank'] = bank
  achs = json.dumps(ach_list)
  return achs

def show_ach_form():
  form = get_form()
  return render_template('ach_form_classic.html', form=form)

def process_ach(classic=False):
  if classic:
    form = get_form_ach()
  else:
    form = forms.get_form_pmt(forms.PmtForm)

  form.account_number.data = form.account_number.data.strip()
  form.routing_number.data = form.routing_number.data.strip()
  if not form.validate_on_submit():
    if not classic:
      return onetime.show_onetime(form=form)
    else:
      return render_template('ach_form_classic.html', form=form)
  if not classic:
    pmt_vault_id = forms.id_number_mismatch_check(form, form.save_pmt_method.data)
  else:
    pmt_vault_id = -1
  # -1 indicates no pmt vault id, i.e., not a vault txn
  # False means there was an error, which was flashed
  # anything else means it's a vault transaction
  if not pmt_vault_id:
    return onetime.show_onetime(form=form)
  elif pmt_vault_id == -1:
    return process_ach_pmt(form)
  else:
    return process_ach_pmt(form, is_vault_txn=True, pmt_vault_id=pmt_vault_id)


def update_ach(form, pmt_vault_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_ach_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, pmt_vault_id))
  ach = curdict.fetchone()
  curdict.execute('UPDATE pmt_vault_ach_tbl SET (cust_acct_id, account_type, owned_by, registered_name) = (%s, %s, %s, %s) WHERE id = %s', (g.user.cust_acct_id, form.account_type.data, form.owned_by.data, form.registered_name.data, pmt_vault_id))
  conn.commit()
  # print("ACH account updated through payment form")
  return pmt_vault_id

""" Inserts new card into the database from form data
Returns the pmt_vault_id """
def save_ach(form):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('WITH max_val AS (SELECT COALESCE((SELECT MAX(pref_rank) FROM pmt_vault_vw WHERE cust_acct_id = %s), 0) AS val), new_ach AS (INSERT INTO pmt_vault_ach_tbl (cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number) VALUES (%s, %s, %s, %s, %s, %s) RETURNING *) INSERT INTO pmt_vault_pref_tbl (pmt_vault_id, pref_rank) SELECT new_ach.id, max_val.val+1 FROM new_ach, max_val RETURNING *', (g.user.cust_acct_id, g.user.cust_acct_id, form.account_type.data, form.owned_by.data, form.registered_name.data, form.routing_number.data, form.account_number.data))
  recent_ach = curdict.fetchone()
  conn.commit()
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE pref_rank IS NULL AND is_active = %s AND cust_acct_id = %s ORDER BY created_ts', (True, g.user.cust_acct_id))
  unranked = [dict(x) for x in curdict]
  conn.commit()
  for i, pmt in enumerate(unranked):
    curdict.execute('INSERT INTO pmt_vault_pref_tbl (pref_rank, pmt_vault_id) VALUES (%s, %s)', (recent_ach['pref_rank'] + 1 + i, pmt['id']))
    conn.commit()
  #print("ACH account saved through payment form")
  return recent_ach['pmt_vault_id']

def process_ach_pmt(form, is_vault_txn=False, pmt_vault_id=None, date=None, ret=False):
  """ is_vault_txn is true if the card is already in the database, therefore we must process it differently, i.e. from the payment server side. 
  Date is default none and takes from the form scheduled date field. However, if the form does not contain
  said field, date is passed in manually. 
  ret indicates whether the function should return to the callee (if true), or return a view function (if false) """
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (g.user.cust_acct_id,))
  cust_acct = curdict.fetchone()
  nmi_facade = chargefacade.NmiFacade(conn)

  is_demo_mode = False
  if form.registered_name.data == 'demo demo':
    is_demo_mode = True
    nmi_facade.set_demo_mode(True)

  # if no date is passed in, use scheduled date (this is used for charge once on recurring setup)
  if not date:
    date = form.scheduled_date.data
  if not pmt_vault_id:
    pmt_vault_id = form.pmt_vault_id.data

  # Date = today if from recur
  if is_vault_txn:
    txn = nmi_facade.insert_pmt_vault_txn(pmt_vault_id, form.amount.data, decimal.Decimal(0), date, form.note.data, g.user.email)
  else: 
    txn = nmi_facade.insert_pmt_nmi_txn_ach(g.user.cust_acct_id, form.account_type.data, form.owned_by.data, form.registered_name.data, form.routing_number.data, form.account_number.data, form.amount.data, form.scheduled_date.data, form.note.data, g.user.email)

  # Gotta get this no matter what
  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']

  more_dict = util.format_pay_prop({}, email_id)
  more_dict['cust_acct_id'] = cust_acct['id']
  more_dict['cust_acct_name'] = cust_acct['name']
  nmi_facade.set_more_tmpl_dict(more_dict)

  if tzconvert.is_future_date_for_cust(conn, g.user.cust_acct_id, date):
    # scheduled in future.

    # If subject lines need Jinja variables at run time, use:
    # .replace('[[', '{''{').replace(']]', '}''}')
    # That eliminates conflict between Jinja during build and Jinja during runtime.
    
    # Opens and sets the proper fail and success templates for a scheduled payment
    nmi_facade.setup_sch_ach_tmpls()
    nmi_facade.send_mail_ach_scheduled(txn)

    txn = dict(txn)
    txn.update(more_dict)

    return render_template('ach_pmt_scheduled.html', pmt_nmi_txn=txn)

  else:
    # scheduled date is today or earlier, so process transaction now

    # If subject lines need Jinja variables at run time, use:
    # .replace('[[', '{''{').replace(']]', '}''}')
    # That eliminates conflict between Jinja during build and Jinja during runtime.
    nmi_facade.setup_ach_tmpls()

    if is_vault_txn:
      txn = nmi_facade.process_pmt_vault_txn_as_client(txn['id'])
      nmi_facade.send_mail_vault_txn(txn)

    else: # Newly entered ach; nmi transaction
      # Sends mail as well
      txn = nmi_facade.process_nmi_txn_ach(txn, form.routing_number.data, form.account_number.data)
    
    txn = dict(txn)
    txn.update(more_dict)

    if txn['response'] == '3' and 'Duplicate' in txn['responsetext']:
      flash('If you intended to make a second payment of the same amount, please wait 5 minutes and try again. The merchant processor marked your most recent one as a duplicate.', 'error')
      if not ret:
        return redirect(url_for('index'))
      else:
        return txn
    else:
      if not ret:
        return render_template('ach_pmt_result.html', pmt_nmi_txn=txn)
      else:
        return txn

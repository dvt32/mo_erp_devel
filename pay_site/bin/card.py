import psycopg2.extras
from flask import request, render_template, redirect
from flask import g, flash, url_for
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, HiddenField, BooleanField
from wtforms.fields.html5 import DateField, DecimalField
from wtforms import validators, ValidationError
import smtplib
import datetime
import ach
import re
import calendar
import decimal
import database
import tzconvert
import solupay
import credit_card
import chargefacade
import json
import onetime
import forms
import util

class CardFields(FlaskForm):
  billing_first_name = StringField('First Name', validators=[validators.InputRequired()], render_kw={'autocomplete': 'cc-given-name', 'placeholder':'First name on card'})
  billing_last_name = StringField('Last Name', validators=[validators.InputRequired()], render_kw={'autocomplete': 'cc-family-name', 'placeholder':'Last name on card'})
  card_number = StringField('Credit Card Number', validators=[validators.InputRequired(),validators.Regexp(r'^\d{15,16}|\d+\*+\d+$')], render_kw={'autocomplete': 'cc-number', 'placeholder':'################', 'maxlength':'16'})
  expire_month = SelectField('Month', choices=util.month_choices, validators=[validators.InputRequired()], render_kw={'autocomplete': 'cc-exp-month'})
  expire_year = StringField('Year', validators=[validators.InputRequired(),validators.Regexp(r'^\d{2}$')], render_kw={'autocomplete': 'cc-exp-year', 'placeholder': 'YY', 'maxlength':'2'})
  cvv = StringField('CVV/CVC/CSC', validators=[validators.Optional(),validators.Regexp(r'^\d{3,4}|\*{3,4}$'),validators.Length(min=3,max=4)], render_kw={'autocomplete': 'cc-csc', 'placeholder': 'CVV', 'maxlength':'4'})
  billing_company = StringField('Company', render_kw={'autocomplete': 'organization', 'placeholder': 'Company (optional)'})
  billing_address_1 = StringField('Address Line 1', validators=[validators.InputRequired()], render_kw={'autocomplete': 'address-line1', 'placeholder': 'Address Line 1'})
  billing_address_2 = StringField('Address Line 2', render_kw={'autocomplete': 'address-line2', 'placeholder': 'Address Line 2 (optional)'})
  billing_city = StringField('City', validators=[validators.InputRequired()], render_kw={'autocomplete': 'address-level2', 'placeholder': 'City'})
  billing_state = SelectField('State', choices=[], validators=[validators.InputRequired()], render_kw={'autocomplete': 'address-level1', 'placeholder': 'State'})
  billing_state_nonus = StringField('State', render_kw={'autocomplete': 'address-level1', 'placeholder': 'State'})
  billing_zip_code = StringField('Zip Code', validators=[validators.InputRequired()], render_kw={'autocomplete': 'postal-code', 'placeholder': 'Zip Code', 'maxlength':'10'})
  billing_country = SelectField('Country', choices=[], validators=[validators.InputRequired()], render_kw={'autocomplete': 'country'}, default='US')

  def validate_card_number(form, field):
    if field.data.isdigit():
      if not credit_card.is_valid_luhn(field.data):
        raise ValidationError('invalid card number: %s' % (field.data,))
      else:
        return True
    if form.pmt_vault_id.data == "" and not field.data.isdigit():
        return False
    else:
      return True

  def validate_billing_zip_code(form, field):
    zip = field.data
    match = re.match(r'\d{5}(-\d{4})?', zip)
    if match == None:
      return False
    else:
      zip = zip[:5]
    state = form.billing_state.data
    conn = database.get_connection()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM zip_code_tbl WHERE zip = %s AND state = %s LIMIT 1', (zip, state))
    result = curdict.fetchone()
    if not result:
      return False
    else:
      return True

  def validate_billing_state(form, field):
    if form.billing_country.data == 'US':
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM state_tbl WHERE abbreviation = %s', (field.data,))
      state = curdict.fetchone()
      if not state:
        raise ValidationError('invalid U.S. state: %s; enter a valid two character abbreviation' % (field.data,))

  def validate_expire_year(form, field):
    today = datetime.date.today()
    if ((today.year - 2000) * 12 + today.month) > (int(field.data) * 12 + int(form.expire_month.data)):
      raise ValidationError('expire date is in the past: %s/%s' % (form.expire_month.data, field.data))

class CardPmtForm(CardFields):
  amount = DecimalField('Amount*', validators=[validators.InputRequired(), validators.NumberRange(min=0.01)], places=2, render_kw={'placeholder': 'Amount'})
  scheduled_date = DateField('Scheduled Date', validators=[validators.InputRequired()])
  note = StringField('Note', render_kw={'placeholder': 'Optional note'})
  save_pmt_method = BooleanField('Save Payment Method', description="Save Payment Method for Future Use", default=False)
  enroll_in_autopay = BooleanField('enroll_in_autopay', default=True)
  submitForm = SubmitField('Pay')

  def fields_append(form):
    for field in form:
      field.id = (field.id) + "-card"
      field.label.field_id = field.label.field_id + "-card"

  def validate_scheduled_date(form, field):
    conn = database.get_connection()
    cust_timezone = tzconvert.cust_timezone(conn, g.user.cust_acct_id)
    # get current datetime in customer's time zone
    now_local = datetime.datetime.now(cust_timezone)
    # extract date portion of datetime to obtain current date from user's perspective
    today_local = now_local.date()
    if not field.data:
      raise ValidationError('Dates must be formatted as yyyy-mm-dd in browsers without a datepicker')
    if (field.data < today_local):
      raise ValidationError('scheduled date is in the past: %s; today is %s' % (str(field.data), str(today_local)))
    elif (field.data.year >= today_local.year and field.data.month > today_local.month):
      raise ValidationError('scheduled date is more than a year in the future: %s; today is %s' % (str(field.data), str(today_local)))

def get_form_card():
  form = CardPmtForm(billing_state=forms.get_state_default(), scheduled_date=forms.get_user_date())
  form.fields_append()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.billing_country.choices = [(country['iso_2'], country['name']) for country in curdict]
  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.billing_state.choices = [(state['abbreviation'], state['name']) for state in curdict]
  return form

def get_saved_cards():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT id, card_number, expire_date, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_city, billing_state, billing_zip_code, billing_country FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s AND pmt_method = %s ORDER BY pref_rank', (g.user.cust_acct_id, True, "card"))
  card_list = [dict(row) for row in curdict]
  date_handler = lambda obj: (
      obj.isoformat()
      if isinstance(obj, datetime.datetime)
      or isinstance(obj, datetime.date)
      else None)
  cards = json.dumps(card_list, default=date_handler)
  return cards

def show_card_form():
  form = get_form_card()
  return render_template('card_form_classic.html', form=form, **util.fee_dict)

def process_card(classic=False):
  if classic:
    form = get_form_card()
  else:
    form = forms.get_form_pmt(forms.PmtForm)
  if not form.validate_on_submit():
    if not classic:
      return onetime.show_onetime(form=form)
    else:
      return render_template('card_form_classic.html', form=form, **util.fee_dict)
  if not classic:
    pmt_vault_id = forms.id_number_mismatch_check(form, form.save_pmt_method.data)
  else:
    pmt_vault_id = -1
  # -1 indicates no pmt vault id, i.e., not a vault txn
  # False means there was an error, which was flashed
  # anything else means it's a vault transaction
  if not pmt_vault_id:
    return onetime.show_onetime(form=form)
  elif pmt_vault_id == -1:
    return process_card_pmt(form)
  else:
    return process_card_pmt(form, is_vault_txn=True, pmt_vault_id=pmt_vault_id)

def update_card(form, pmt_vault_id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_card_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, pmt_vault_id))
  card = curdict.fetchone()
  expire_date = datetime.date(2000 + int(form.expire_year.data), int(form.expire_month.data), 1)
  if form.billing_country.data != 'US':
    state = form.billing_state_nonus.data
  else:
    state = form.billing_state.data
  curdict.execute('UPDATE pmt_vault_card_tbl SET (expire_date, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_address_num, billing_city, billing_state, billing_zip_code, billing_country) = (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) WHERE id = %s', (expire_date, form.billing_first_name.data, form.billing_last_name.data, form.billing_company.data, form.billing_address_1.data, form.billing_address_2.data, None, form.billing_city.data, state, form.billing_zip_code.data, form.billing_country.data, pmt_vault_id))
  conn.commit()
  # print("Card account updated through payment form")
  return pmt_vault_id

""" Inserts new card into the database from form data
Returns the pmt_vault_id """
def save_card(form):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  expire_date = datetime.date(2000 + int(form.expire_year.data), int(form.expire_month.data), 1)
  if form.billing_country.data != 'US':
    state = form.billing_state_nonus.data
  else:
    state = form.billing_state.data
  curdict.execute('WITH max_val AS (SELECT COALESCE((SELECT MAX(pref_rank) FROM pmt_vault_vw WHERE cust_acct_id = %s), 0) AS val), new_card AS (INSERT INTO pmt_vault_card_tbl (cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_address_num, billing_city, billing_state, billing_zip_code, billing_country) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *) INSERT INTO pmt_vault_pref_tbl (pmt_vault_id, pref_rank) SELECT new_card.id, max_val.val+1 FROM new_card, max_val RETURNING *', (g.user.cust_acct_id, g.user.cust_acct_id, form.card_number.data, expire_date, form.cvv.data, form.billing_first_name.data, form.billing_last_name.data, form.billing_company.data, form.billing_address_1.data, form.billing_address_2.data, None, form.billing_city.data, state, form.billing_zip_code.data, form.billing_country.data))
  recent_card = curdict.fetchone()
  conn.commit()
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE pref_rank IS NULL AND is_active = %s AND cust_acct_id = %s ORDER BY created_ts', (True, g.user.cust_acct_id))
  unranked = [dict(x) for x in curdict]
  conn.commit()
  for i, pmt in enumerate(unranked):
    curdict.execute('INSERT INTO pmt_vault_pref_tbl (pref_rank, pmt_vault_id) VALUES (%s, %s)', (recent_card['pref_rank'] + 1 + i, pmt['id']))
    conn.commit()
  #print("Card account saved through payment form")
  return recent_card['pmt_vault_id']

def process_card_pmt(form, is_vault_txn=False, pmt_vault_id=None, date=None, ret=False):
  """ is_vault_txn is true if the card is already in the database, therefore we must process it differently, i.e. from the payment server side.
  Date is default none and takes from the form scheduled date field. However, if the form does not contain
  said field, date is passed in manually.
  ret indicates whether the function should return to the callee (if true), or return a view function (if false) """
  # Set up
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (g.user.cust_acct_id,))
  cust_acct = curdict.fetchone()
  nmi_facade = chargefacade.NmiFacade(conn)

  # Set demo mode if needed
  is_demo_mode = False
  if form.billing_first_name.data == 'demo' and form.billing_last_name.data == 'demo':
    is_demo_mode = True
    nmi_facade.set_demo_mode(True)

  # For mat expire date as datetime object
  expire_date = datetime.date(2000 + int(form.expire_year.data), int(form.expire_month.data), 1)

  # Set proper state field
  if form.billing_country.data != 'US':
    state = form.billing_state_nonus.data
  else:
    state = form.billing_state.data

  # Calculate total amount to charge
  # round to two decimal places (cents)
  prefee_amount, convenience_fee, total_amount = util.get_fees(form.amount.data)

  # if no date is passed in, use scheduled date (this is used for charge once on recurring setup)
  if not date:
    date = form.scheduled_date.data
  if not pmt_vault_id:
    pmt_vault_id = form.pmt_vault_id.data

  # Date = today if from recur
  if is_vault_txn:
    txn = nmi_facade.insert_pmt_vault_txn(pmt_vault_id, total_amount, convenience_fee, date, form.note.data, g.user.email)
  else:
    txn = nmi_facade.insert_pmt_nmi_txn_card(g.user.cust_acct_id, form.card_number.data, expire_date, form.cvv.data, form.billing_first_name.data, form.billing_last_name.data, form.billing_company.data, form.billing_address_1.data, form.billing_address_2.data, None, form.billing_city.data, state, form.billing_zip_code.data, form.billing_country.data, total_amount, convenience_fee, form.scheduled_date.data, form.note.data, g.user.email)

  # Gotta get this no matter what
  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']

  more_dict = util.format_pay_prop({}, email_id)
  more_dict['cust_acct_id'] = cust_acct['id']
  more_dict['cust_acct_name'] = cust_acct['name']
  more_dict['expire_mmyyyy'] = form.expire_month.data + '/' + str(2000 + int(form.expire_year.data))
  more_dict['prefee_amount'] = prefee_amount
  more_dict['convenience_fee'] = convenience_fee
  nmi_facade.set_more_tmpl_dict(more_dict)

  # Will only happen from onetime
  if tzconvert.is_future_date_for_cust(conn, g.user.cust_acct_id, date):
    # scheduled in future

    # If subject lines need Jinja variables at run time, use:
    # .replace('[[', '{''{').replace(']]', '}''}')
    # That eliminates conflict between Jinja during build and Jinja during runtime.


    # Opens and sets the proper fail and success templates for a scheduled payment
    nmi_facade.setup_sch_card_tmpls()

    nmi_facade.send_mail_card_scheduled(txn)

    # Set up dict for page rendering
    txn = dict(txn)
    txn.update(more_dict)

    return render_template('card_pmt_scheduled.html', pmt_nmi_txn=txn)

  else:
    # scheduled date is today or earlier, so process transaction now

    # If subject lines need Jinja variables at run time, use:
    # .replace('[[', '{''{').replace(']]', '}''}')
    # That eliminates conflict between Jinja during build and Jinja during runtime.
    nmi_facade.setup_card_tmpls()

    if is_vault_txn:
      txn = nmi_facade.process_pmt_vault_txn_as_client(txn['id'])
      nmi_facade.send_mail_vault_txn(txn)

    else: # Newly entered card; nmi transaction
      # Sends mail as well
      txn = nmi_facade.process_nmi_txn_card(txn, form.card_number.data, form.cvv.data)

    # Set up dict for page rendering
    txn = dict(txn)
    txn.update(more_dict)

    if txn['response'] == '3' and 'Duplicate' in txn['responsetext']:
      flash('If you intended to make a second payment of the same amount, please wait 5 minutes and try again. The merchant processor marked your most recent one as a duplicate.', 'error')
      if not ret:
        return redirect(url_for('show_onetime'))
      else:
        return txn
    else:
      if not ret:
        return render_template('card_pmt_result.html', pmt_nmi_txn=txn)
      else:
        return txn

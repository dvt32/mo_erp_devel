import psycopg2.extras
from flask import request, render_template, redirect, url_for
from flask import g, flash
import database

def view_email():
  if (request.args):
    email_id = request.args['id']
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM email_paysite_tbl WHERE id = %s AND cust_acct_id = %s', (email_id, g.user.cust_acct_id))
  email = curdict.fetchone()
  if not email:
    return render_template("email.html", content="<h4>You are not authorized to view this email, or it does not exist in our database. Return to <a href=http://pay.microoffice.com>pay.microoffice.com</a></h4>")
  if email['html_text']:
    return render_template("email.html", content=email['html_text'])
  else:
    return render_template("email.html", content=email['plain_text'])

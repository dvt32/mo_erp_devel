import psycopg2.extras
from flask import request, render_template, redirect, url_for
from flask import g, flash
import datetime
import database
import solupay
import tzconvert
import calendar
import re
import log_access
import forms
import util

def show_history(past=None):
  log_access.log_page()
  if not past:
    past = get_past_pmts()
  return render_template('history.html', past=past)

def get_past_pmts():
  today = forms.get_user_date()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE txn_purpose = %s AND response IS NOT NULL AND request_posted_ts IS NOT NULL AND cust_acct_id = %s ORDER BY request_posted_ts DESC', ('service', g.user.cust_acct_id,))
  past = [dict(row) for row in curdict]
  curdict.execute('SELECT pmt_nmi_txn_action_tbl.* FROM pmt_nmi_txn_action_tbl, pmt_nmi_txn_vw WHERE pmt_nmi_txn_action_tbl.pmt_nmi_txn_id = pmt_nmi_txn_vw.id AND pmt_nmi_txn_vw.txn_purpose = %s AND pmt_nmi_txn_vw.response IS NOT NULL AND pmt_nmi_txn_vw.transactionid IS NOT NULL AND pmt_nmi_txn_vw.request_posted_ts IS NOT NULL AND pmt_nmi_txn_vw.cust_acct_id = %s ORDER BY pmt_nmi_txn_vw.request_posted_ts DESC', ('service', g.user.cust_acct_id))
  past_actions = [dict(row) for row in curdict]
  actions_by_id = {}
  for action in past_actions:
    if action['pmt_nmi_txn_id'] in actions_by_id:
      actions_by_id[action['pmt_nmi_txn_id']].append(action)
    else:
      actions_by_id[action['pmt_nmi_txn_id']] = [action]
  for p in past:
    if p['id'] in actions_by_id:
      bad_actions = list(filter(lambda a: (a['action_type'] != "sale" and a['action_type'] != "settle" and a['action_type'] != 'void'), actions_by_id[p['id']]))
      void = list(filter(lambda a: a['action_type'] == 'void', actions_by_id[p['id']]))
    else:
      bad_actions = None
      void = None
    if bad_actions or p['id'] not in actions_by_id or int(p['response']) != 1:
      curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE txn_purpose = %s AND cust_acct_id = %s AND response = %s AND request_posted_ts > %s', ('service', g.user.cust_acct_id, '1', p['request_posted_ts']))
      handled = curdict.fetchone()
      if handled:
        p['handled'] = True
        p['error'] = False
      else:
        p['error'] = True
    if void:
      p['cancelled'] = True
    if p['pmt_method'] == "card":
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(p['expire_date']));
      p['expire_date'] = match.group(2) + "/" + match.group(1)[-2:]
      p['number_info'] = "Credit Card<br>..." + p['card_number'][-4:] + " exp. " + p['expire_date']
    elif p['pmt_method'] == 'ach':
      p['number_info'] = "Bank Account<br> Routing: " + p['routing_number'] + "<br>Acc: " + p['account_number']
    if p['id'] in actions_by_id:
      p['actions'] = actions_by_id[p['id']]
  return past

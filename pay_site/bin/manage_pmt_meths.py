import psycopg2.extras
from flask import request, render_template, flash, redirect, url_for
from flask import g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, HiddenField, BooleanField
from wtforms import validators, ValidationError
from werkzeug.datastructures import MultiDict
import datetime
import re
import database
import card
import credit_card
import bank_account
import forms
import ach
import json
import dbutil
import decimal
from dateutil.relativedelta import relativedelta
import chargefacade
import jinja2
import debit_confirm
import sync_txn_action_lib
import solupay
from chargefacade import ChargeMail
import log_access
from email.mime.text import MIMEText
import email.utils
import email.header
import util


def show_pmt_meth(cardAddForm=None, cardEditForm=None, achAddForm=None, achEditForm=None, pendingForm=None):
  log_access.log_page()
  cards = get_cards()
  achs = get_achs()
  if not cardAddForm:
    cardAddForm = get_form_card_add(True)
  if not achAddForm:
    achAddForm = get_form_ach_add(True)
  if not cardEditForm:
    cardEditForm = get_form_card_edit(True)
  if not achEditForm:
    achEditForm = get_form_ach_edit(True)
  if not pendingForm:
    pendingForm = PendingForm(formdata=None)
  return render_template('manage_pmt_meth.html', cards=cards, achs=achs, cardAddForm=cardAddForm, achAddForm=achAddForm, cardEditForm=cardEditForm, achEditForm=achEditForm, pendingForm=pendingForm)

class CardAddForm(card.CardFields):
  submitForm = SubmitField('Save')

def get_form_card_add(new=False):
  if new:
    form = CardAddForm(formdata=None, billing_state=forms.get_state_default())
  else:
    form = CardAddForm(billing_state=forms.get_state_default())
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.billing_country.choices = [(country['iso_2'], country['name']) for country in curdict]
  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.billing_state.choices = [(state['abbreviation'], state['name']) for state in curdict]
  return form

def get_cards():
	conn = database.get_connection()
	curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	curdict.execute('SELECT * FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s AND pmt_method = %s ORDER BY pref_rank', (g.user.cust_acct_id, True, "card"))
	card_list = [row for row in curdict]
	return card_list

def add_card():
  form = get_form_card_add()
  if not form.validate_on_submit():
    flash('card', 'open')
    return show_pmt_meth(cardAddForm=form)
  if credit_card.find_cust_card(g.user.cust_acct_id, form.card_number.data) != None:
    flash('card', 'open')
    flash('Error: It seems you already have this card in our database: ' + form.card_number.data, 'error_card_add')
    return show_pmt_meth(cardAddForm=form)
  pmt_vault_id = card.save_card(form)
  flash(pmt_vault_id, 'pmt_vault_id')
  return redirect(url_for('manage_pmt_meth'))

class CardEditForm(CardAddForm):
  pmt_vault_id = HiddenField('pmt_vault_id', validators=[validators.InputRequired()])
  cvv = None
  card_number = None

  def validate_card_number(form, field):
    if form.pmt_vault_id.data != "" and field.data.isdigit():
      if not credit_card.is_valid_luhn(field.data):
        raise ValidationError('invalid card number: %s' % (field.data,))
      else:
        return True
    if form.pmt_vault_id.data == "" and not field.data.isdigit():
        return False
    else:
      return True

  def fields_append(form):
    for field in form:
      field.id = (field.id) + "-edit"
      field.label.field_id = field.label.field_id + "-edit"

def get_form_card_edit(new=False):
  if new:
    form = CardEditForm(formdata=None)
  else:
    form = CardEditForm()
  form.fields_append()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.billing_country.choices = [(country['iso_2'], country['name']) for country in curdict]
  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.billing_state.choices = [(state['abbreviation'], state['name']) for state in curdict]
  return form

def get_card_edit():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_card_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, request.args['pmt_vault_id']))
  card = curdict.fetchone()
  if not card:
    return(json.dumps("This card no longer exists or is inactive."))
  date_handler = lambda obj: (
      obj.isoformat()
      if isinstance(obj, datetime.datetime)
      or isinstance(obj, datetime.date)
      else None)
  card = json.dumps(dict(card), default=date_handler)
  return card

def update_card():
  form = get_form_card_edit()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_card_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, form.pmt_vault_id.data))
  cur_card = curdict.fetchone()
  if not cur_card:
    flash('card_edit', 'open')
    flash('You are not authorized to edit this payment method', 'error_card_edit')
    return show_pmt_meth(cardEditForm=form)
  if not form.validate_on_submit():
    flash('card_edit', 'open')
    return show_pmt_meth(cardEditForm=form)
  pmt_vault_id = card.update_card(form, form.pmt_vault_id.data)
  flash(pmt_vault_id, 'pmt_vault_id')
  return redirect( url_for('manage_pmt_meth') )


def delete_card():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_card_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, request.args['pmt_vault_id']))
  card = curdict.fetchone()
  if not card:
    flash('You are unathorized to delete this payment method', 'error')
    return json.dumps("unauthorized")
  curdict.execute('SELECT * FROM pmt_recur_month_tbl WHERE pmt_vault_id = %s AND is_active', (request.args['pmt_vault_id'],))
  recur = curdict.fetchone()
  if recur:
    match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(card['expire_date']));
    exp = match.group(2) + "/" + match.group(1)[-2:]
    msg = "Card number: ..." + str(card['card_number'])[-4:] + " exp. " + exp
    flash('You tried to delete this Credit Card: ' + msg + ', but you have a recurring payment that uses this card. Please delete the recurring payment or change its payment method.', 'error')
    return json.dumps("unauthorized")
  card_number = card['card_number']
  curdict.execute('UPDATE pmt_vault_card_tbl SET is_active = %s WHERE id = %s', (False, request.args['pmt_vault_id']))
  conn.commit()
  flash(card_number, 'deleted_card')
  return json.dumps("deleted")

class AchAddForm(ach.AchFields):
  submitForm = SubmitField('Save')

def get_form_ach_add(new=False):
  if new:
    form = AchAddForm(formdata=None)
  else:
    form = AchAddForm()
  return form

def get_achs():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s AND pmt_method = %s ORDER BY pref_rank', (g.user.cust_acct_id, True, "ach"))
  ach_accounts = [dict(row) for row in curdict]
  for ach in ach_accounts:
    if ach['confirmed_ts'] != None:
      ach['confirmed'] = 'confirmed'
    else:
      curdict.execute("SELECT DISTINCT pmt_method_confirm_tbl.id FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_method_confirm_tbl.pmt_vault_id = %s AND pmt_nmi_txn_vw.amount < 0::money AND pmt_nmi_txn_vw.request_posted_ts > now() - interval '{{pmt_confirm_window}} days'", (ach['id'],))
      confirm = curdict.fetchone()
      if not confirm:
        ach['confirmed'] = 'unconfirmed'
      else:
        ach['confirmed'] = 'pending'
        curdict.execute('SELECT * FROM pmt_nmi_txn_action_tbl WHERE pmt_nmi_txn_id IN (SELECT id FROM pmt_nmi_txn_vw WHERE pmt_method_confirm_id = %s AND pmt_method = %s AND txn_purpose = %s) ORDER BY action_ts ASC', (confirm['id'], 'ach', 'confirm'))
        ach['actions'] = [dict(row) for row in curdict]
        #if len(ach['actions']) < 2:
        # ach['error'] = True
        #if:
        bad_actions = list(filter(lambda a: a['action_type'] != "credit" and a['action_type'] != "settle", ach['actions']))
        settle = list(filter(lambda a: a['action_type'] == 'settle', ach['actions']))
        if bad_actions:
          ach['error'] = True
        else:
          ach['error'] = False
        if len(settle) == 2:
          ach['confirmed'] = 'ready to verify'
  return ach_accounts

def add_ach():
  form = get_form_ach_add()
  if not form.validate_on_submit():
    flash('ach', 'open')
    return show_pmt_meth(achAddForm=form)
  if bank_account.find_cust_ach(g.user.cust_acct_id, form.routing_number.data, form.account_number.data) != None:
    flash('ach', 'open')
    flash('Error: It seems you already have this card in our database: ' + form.account_number.data, 'error_ach_add')
    return show_pmt_meth(achAddForm=form)
  pmt_vault_id = ach.save_ach(form)
  flash(pmt_vault_id, 'pmt_vault_id')
  return redirect(url_for('manage_pmt_meth'))

class AchEditForm(AchAddForm):
  pmt_vault_id = HiddenField('pmt_vault_id', validators=[validators.InputRequired()])
  account_number = None
  routing_number = None

  def fields_append(form):
    for field in form:
      field.id = (field.id) + '-edit'
      field.label.field_id = field.label.field_id + '-edit'

  def validate_routing_number(form, field):
    if form.pmt_vault_id.data != "" and field.data.isdigit():
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM fedach_participant_tbl WHERE routing_number = %s', (field.data,))
      fedach_participant = curdict.fetchone()
      if not fedach_participant:
        raise ValidationError('invalid routing number: %s' % (field.data,))
      else:
        return True
    if form.pmt_vault_id.data == "" and not field.data.isdigit():
        return False
    else:
      return True

  def validate_account_number(form, field):
    if form.pmt_vault_id.data != "" and field.data.isdigit():
        return True
    if form.pmt_vault_id.data == "" and not field.data.isdigit():
        return False
    else:
      return True

def get_form_ach_edit(new=False):
  if new:
    form = AchEditForm(formdata=None)
  else:
    form = AchEditForm()
  form.fields_append()
  return form

def get_ach_edit():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_ach_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, request.args['pmt_vault_id']))
  ach = curdict.fetchone()
  if not ach:
    return(json.dumps("This bank accout no longer exists or is inactive."))
  date_handler = lambda obj: (
      obj.isoformat()
      if isinstance(obj, datetime.datetime)
      or isinstance(obj, datetime.date)
      else None)
  ach = json.dumps(dict(ach), default=date_handler)
  return ach

def update_ach():
  form = get_form_ach_edit()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_ach_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, form.pmt_vault_id.data))
  cur_ach = curdict.fetchone()
  if not cur_ach:
    flash('ach_edit', 'open')
    flash("You are not authorized to edit this payment method", 'error_ach_edit')
    return show_pmt_meth(achEditForm=form)
  if not form.validate_on_submit():
    flash('ach_edit', 'open')
    return show_pmt_meth(achEditForm=form)
  pmt_vault_id = ach.update_ach(form, form.pmt_vault_id.data)
  flash(pmt_vault_id, 'pmt_vault_id')
  return redirect( url_for('manage_pmt_meth') )

def delete_ach():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_ach_tbl WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, request.args['pmt_vault_id']))
  ach = curdict.fetchone()
  if not ach:
    flash('You are unauthorized to delete this payment method', 'error')
    return json.dumps("unauthorized")
  curdict.execute('SELECT * FROM pmt_recur_month_tbl WHERE pmt_vault_id = %s AND is_active', (request.args['pmt_vault_id'],))
  recur = curdict.fetchone()
  if recur:
    msg = "Routing number: " + ach['routing_number'] + "; Account number: " + ach['account_number']
    flash('You tried to delete this Bank Accont: ' + msg + ', but you have a recurring payment that uses this account. Please delete the recurring payment or change its payment method.', 'error')
    return json.dumps("unauthorized")
  ach_number = ach['account_number']
  curdict.execute('UPDATE pmt_vault_ach_tbl SET is_active = %s WHERE id = %s', (False, request.args['pmt_vault_id']))
  conn.commit()
  # below has to do with confirmation transaction cancellation
  if ach['confirmed_ts'] == None:
    nmi_facade = chargefacade.NmiFacade(conn)
    curdict.execute("SELECT * FROM pmt_method_confirm_tbl WHERE id in (SELECT DISTINCT pmt_method_confirm_tbl.id FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_method_confirm_tbl.pmt_vault_id = %s AND pmt_nmi_txn_vw.amount < 0::money AND pmt_nmi_txn_vw.request_posted_ts > now() - interval '{{pmt_confirm_window}} days')", (request.args['pmt_vault_id'],))
    confirm = curdict.fetchone()
    if confirm:
      curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE txn_purpose = %s AND pmt_method_confirm_id = %s', ('confirm', confirm['id']))
      txns = [dict(row) for row in curdict]
      if len(txns) != 2 or len(txns) != 0:
        print("On ACH deletion, only found " + str(len(txns)) + " confirmation transactions. Expecting either 0 or 2.")
        mail_body = '''
On ACH deletion, only found %s transactions
Pmt Vault ID: %s
Customer ID: %s
Routing Number: %s
Account Number: %s
Payment Transaction IDs: ''' % (str(len(txns)), ach['pmt_vault_id'], ach['cust_acct_id'], ach['routing_number'], ach['account_number'])
        for txn in txns:
          mail_body += txn['id'] + ', '
        mail_msg = MIMEText(mail_body)
        mail_msg['Date'] = email.utils.formatdate(localtime=True)
        mail_msg['From'] = '{{ payment_mail_from }}'
        mail_msg['To'] = '{{ payment_mail_to }}'
        mail_msg['Subject'] = email.header.Header('Error in Debiting Back Customer Confirmation Amounts', 'utf-8')
        mailer = smtplib.SMTP('{{ outbound_mail_server }}')
        mailer.send_message(mail_msg)
        mailer.quit()
      amt = decimal.Decimal(0)
      for txn in txns:
        sync_txn_action_lib.sync_txn_by_id(txn['id'], conn)
        curdict.execute('SELECT * FROM pmt_nmi_txn_action_tbl WHERE pmt_nmi_txn_id = %s ORDER BY action_ts ASC', (txn['id'],))
        actions = [dict(row) for row in curdict]
        if len(actions) == 0:
          if txn['response'] == '1':
            void_reponse = nmi_facade.void_confirmation_txn(txn['id'])
            if void_response != '1':
              amt += dbutil.cast_money_to_decmial(abs(txn['amount']))
        else:
          settle = list(filter(lambda a: a['action_type'] == "settle", actions))
          if not settle:
            void_response = nmi_facade.void_confirmation_txn(txn['id'])
            if void_response != '1':
              amt += dbutil.cast_money_to_decmial(abs(settle['amount']))
          else:
            amt += dbutil.cast_money_to_decmial(abs(settle['amount']))
      debit_confirm.debit_back_confirmation_amounts(request.args['pmt_vault_id'], amt)
  flash(ach_number, 'deleted_ach')
  return json.dumps("deleted")

def new_order():
  f = request.form
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('DELETE FROM pmt_vault_pref_tbl WHERE pmt_vault_id IN (SELECT id FROM pmt_vault_vw WHERE cust_acct_id = %s)', (g.user.cust_acct_id,))
  for key in f.keys():
    pmt_vault_id = [s for s in key.split("_") if s.isdigit()][0]
    for value in f.getlist(key):
      curdict.execute('SELECT * FROM pmt_vault_ƒvw WHERE cust_acct_id = %s AND is_active = %s', (g.user.cust_acct_id, True))
      valid = curdict.fetchone()
      if not valid:
        return json.dumps("invalid")
      curdict.execute('INSERT INTO pmt_vault_pref_tbl (pref_rank, pmt_vault_id) VALUES (%s, %s)', (value, pmt_vault_id))
  conn.commit()
  return json.dumps("reordered")

def check_confirm():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_ach_tbl WHERE cust_acct_id = %s AND is_active = %s AND id = %s AND confirmed_ts IS NULL', (g.user.cust_acct_id, True, request.args['pmt_vault_id']))
  ach = curdict.fetchone()
  if not ach:
    return(json.dumps("confirmed"))
  curdict.execute("SELECT DISTINCT pmt_method_confirm_tbl.id FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_method_confirm_tbl.pmt_vault_id = %s AND pmt_nmi_txn_vw.amount < 0::money AND pmt_nmi_txn_vw.request_posted_ts > now() - interval '{{pmt_confirm_window}} days'", (request.args['pmt_vault_id'],))
  confirm = curdict.fetchone()
  if not confirm:
    return(json.dumps("unconfirmed"))
  else:
    return(json.dumps("pending"))

def send_confirm_ach():
  pmt_vault_id = request.args['pmt_vault_id']
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s AND id = %s AND pmt_method = %s AND confirmed_ts IS NULL', (g.user.cust_acct_id, True, pmt_vault_id, 'ach'))
  ach = curdict.fetchone()
  if not ach:
    flash('This account is already confirmed! No need to try again.', 'confirm')
    return json.dumps("already confirmed")
  curdict.execute("SELECT COUNT(DISTINCT pmt_method_confirm_tbl.id) FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_method_confirm_tbl.pmt_vault_id IN (SELECT id FROM pmt_vault_vw WHERE cust_acct_id = %s) AND pmt_nmi_txn_vw.amount < 0::money AND pmt_nmi_txn_vw.request_posted_ts > now() - interval '30 days'", (g.user.cust_acct_id,))
  confirm_requests = curdict.fetchone()
  if confirm_requests[0] > 4:
    flash('You have reached the limit of bank accounts to confirm within a 30 day period. Please wait a few days or contact billing@microoffice.com if this is a problem.', 'error')
    return json.dumps("error")
  curdict.execute("SELECT COUNT(DISTINCT pmt_method_confirm_tbl.id) FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_method_confirm_tbl.pmt_vault_id = %s AND pmt_nmi_txn_vw.amount < 0::money AND pmt_nmi_txn_vw.request_posted_ts > now() - interval '{{pmt_confirm_window}} days'", (pmt_vault_id,))
  pending = curdict.fetchone()
  if pending[0] > 0:
    flash('You already have a pending confirmation request for this bank account. Please wait until the 15 day period has expired to attempt to send another set of confirmation deposits. You may also delete this account and try again if you entered information incorrectly. Otherwise, contact us at billing@microoffice.com for quesitons.', 'error')
    return json.dumps("already pending")
  nmi_facade = chargefacade.NmiFacade(conn)
  status = nmi_facade.process_confirm_txn_ach_as_client(pmt_vault_id)
  if status:
    with open('{{ pay_basedir }}/share/confirm_ach_succ.txt', 'r') as tmpl_file:
        text_tmpl = tmpl_file.read()
    text_template = jinja2.Template(text_tmpl)
    subject = "Confirm Bank Account Ownership at Micro Office"
    with open('{{ pay_basedir }}/share/confirm_ach_succ.html', 'r') as tmpl_file:
        html_tmpl = tmpl_file.read()
    html_template = jinja2.Template(html_tmpl)
  else:
    with open('{{ pay_basedir }}/share/confirm_ach_fail.txt', 'r') as tmpl_file:
        text_tmpl = tmpl_file.read()
    text_template = jinja2.Template(text_tmpl)
    with open('{{ pay_basedir }}/share/confirm_ach_fail.html', 'r') as tmpl_file:
        html_tmpl = tmpl_file.read()
    html_template = jinja2.Template(html_tmpl)
    subject = "Failed to Confirm Bank Account Ownership at Micro Office"
  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']
  curdict.execute('SELECT * from cust_acct_tbl WHERE id = %s', (ach['cust_acct_id'],))
  cust_acct = curdict.fetchone()
  curdict.execute('SELECT short_global_label FROM floor_tbl WHERE id = %s', (cust_acct[
  'primary_floor_id'],))
  floor = curdict.fetchone()
  property_dict = dict(ach)
  property_dict.update(cust_acct)
  property_dict.update(floor)
  property_dict['email_id'] = email_id
  property_dict['pay_base_url'] = '{{ pay_base_url }}'
  property_dict['static_base_url'] = '{{ static_base_url }}'
  text_body = text_template.render(property_dict)
  html_body = html_template.render(property_dict)
  ChargeMail.send_html_mail(g.user.email, None, subject, text_body, html_body, email_id, g.user.cust_acct_id, conn)
  flash('An email has been sent to your email address to verify your bank account confirmation request. The deposit amounts should appear within the next few days.', 'confirm')
  return json.dumps("now pending")

class PendingForm(FlaskForm):
  pmt_vault_id = HiddenField('pmt_vault_id', default='')
  amount_1 = StringField('Amount 1', validators=[validators.InputRequired(), validators.NumberRange(min=1, max=99)], render_kw={'placeholder': 'Amount 1'})
  amount_2 = StringField('Amount 2', validators=[validators.InputRequired(), validators.NumberRange(min=1, max=99)], render_kw={'placeholder': 'Amount 1'})
  submitForm = SubmitField('Verify')

def verify_ach():
  form = PendingForm()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SELECT * FROM pmt_method_confirm_tbl WHERE id in (SELECT DISTINCT pmt_method_confirm_tbl.id FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_method_confirm_tbl.pmt_vault_id = %s AND pmt_nmi_txn_vw.amount < 0::money AND pmt_nmi_txn_vw.request_posted_ts > now() - interval '{{pmt_confirm_window}} days')", (form.pmt_vault_id.data,))
  confirm = curdict.fetchone()
  if not confirm:
    flash('No confirmation request sent, cannot verify at this time.', 'error')
    return redirect( url_for ('manage_pmt_meth') )
  curdict.execute('UPDATE pmt_method_confirm_tbl SET num_confirm_attempts = %s WHERE id = %s', ((confirm['num_confirm_attempts'] + 1), confirm['id']))
  conn.commit()
  if confirm['num_confirm_attempts'] >= 3:
    flash('Too many verification attempts. Please wait until the 15 day period has expired to try again, or delete the account and create it new if you believe you made a mistake and wish to confirm it again. Otherwise, email us at billing@microoffice.com with any questions.', 'error')
    return redirect( url_for('manage_pmt_meth') )
  curdict.execute('SELECT * FROM pmt_nmi_txn_ach_tbl WHERE txn_purpose = %s AND pmt_method_confirm_id = %s', ('confirm', confirm['id']))
  txns = [dict(row) for row in curdict]
  amounts = [abs(dbutil.cast_money_to_decimal(row['amount'])) for row in txns]
  am1 = decimal.Decimal(form.amount_1.data) / decimal.Decimal(100)
  am2 = decimal.Decimal(form.amount_2.data) / decimal.Decimal(100)
  if (am1 == amounts[0] and am2 == amounts[1]) or (am2 == amounts[0] and am1 == amounts[1]):
    curdict.execute('UPDATE pmt_vault_ach_tbl SET confirmed_ts = %s WHERE id = %s', (datetime.datetime.now(), confirm['pmt_vault_id']))
    conn.commit()
    debit_confirm.debit_back_confirmation_amounts(form.pmt_vault_id.data)
    flash('Successfully confirmed ACH account.', 'confirm')
    flash(confirm['pmt_vault_id'], 'pmt_vault_id')
    return redirect( url_for ('manage_pmt_meth') )
  flash(confirm['pmt_vault_id'], 'verify_id')
  flash('Not the correct amounts. Please try again, but be aware you only have three chances.', 'verify_error')
  return show_pmt_meth(pendingForm=form)

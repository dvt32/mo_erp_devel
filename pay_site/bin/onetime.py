import psycopg2
import psycopg2.extras
from flask import g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, HiddenField, IntegerField, BooleanField, RadioField
from wtforms.fields.html5 import DateField, DecimalField
from wtforms_components import If
from wtforms import validators, ValidationError
import ach
import card
import decimal
import datetime
import tzconvert
import database
import json
from flask import render_template
import re
import upcoming
import credit_card
import forms
import util
import log_access

def show_onetime(form=None):
  log_access.log_page()
  if not form:
    form = forms.get_form_pmt(forms.PmtForm, True)
  return render_template('onetime.html', form=form, **util.fee_dict)

def show_index_classic():
  log_access.log_page()
  return render_template('index_classic.html') 

def process_pmt():
  log_access.log_page()
  form = forms.get_form_pmt(forms.PmtForm)
  if form.pmt_method.data == 'card':
    return card.process_card()
  if form.pmt_method.data == 'ach':
    return ach.process_ach()

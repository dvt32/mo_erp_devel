import psycopg2.extras
from flask import request, render_template, redirect, url_for
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, HiddenField, IntegerField, BooleanField, RadioField
from wtforms.fields.html5 import DateField, DecimalField
from wtforms_components import If
from wtforms import validators, ValidationError
from dateutil.relativedelta import relativedelta
import smtplib
import jinja2
import datetime
import database
import solupay
import tzconvert
import calendar
import json
from chargefacade import ChargeMail
import re
from upcoming import get_upcoming, UpcomingForm
import log_access
import forms
import util
import credit_card
import bank_account
import card
import ach


class RecurPmtForm(forms.PmtInfoForm):
  day_of_month = IntegerField('Day of The Month', validators=[validators.Optional(), validators.NumberRange(min=1, max=28)], render_kw={'placeholder': 'Day of the month (1-28)'})
  start_date = DateField('Start Date', validators=[validators.Optional()], render_kw={'placeholder': 'Start Date'})
  end_date = DateField('End Date', validators=[validators.Optional()], render_kw={'placeholder': 'End Date'})
  max_amount = DecimalField('Max Amount', validators=[validators.Optional(), validators.NumberRange(min=0.01)], places=2, render_kw={'placeholder': 'Defaults to invoice amount'})
  reminder = BooleanField('Send a Reminder Email?', description="Send Me an Email Reminder Before the Charge", default=True)
  remind_days_before = IntegerField('How Many Days Before?', validators=[If(lambda form, field: not form.reminder.data, validators.Optional()), If(lambda form, field: form.reminder.data, validators.InputRequired()), If(lambda form, field: form.reminder.data, validators.NumberRange(min=1, max=15))], render_kw={'placeholder': 'Number 1-15'})
  # charge_today = BooleanField('Charge Today', description="Also Charge Me Today", default=True)
  amount = DecimalField('Amount', validators=[validators.Optional(), validators.NumberRange(min=0.01)], places=2, render_kw={'placeholder': 'Amount'})
  note = StringField('Note', validators=[validators.Optional()], render_kw={'placeholder': 'Optional Note (e.g. invoice number, etc.)'})
  submitForm = SubmitField('Submit')

  def validate_start_date(form, field):
    conn = database.get_connection()
    cust_timezone = tzconvert.cust_timezone(conn, g.user.cust_acct_id)
    # get current datetime in customer's time zone
    now_local = datetime.datetime.now(cust_timezone)
    # extract date portion of datetime to obtain current date from user's perspective
    today_local = now_local.date()
    if not field.data:
      raise ValidationError('Dates must be formatted as yyyy-mm-dd in browsers without a datepicker')
    if (field.data < today_local):
      raise ValidationError('Start date is in the past: %s; today is %s' % (str(field.data), str(today_local)))

  def validate_end_date(form, field):
    conn = database.get_connection()
    cust_timezone = tzconvert.cust_timezone(conn, g.user.cust_acct_id)
    # get current datetime in customer's time zone
    now_local = datetime.datetime.now(cust_timezone)
    # extract date portion of datetime to obtain current date from user's perspective
    today_local = now_local.date()
    if (field.data < today_local):
      raise ValidationError('End date is in the past: %s; today is %s' % (str(field.data), str(today_local)))

class RecurEditForm(RecurPmtForm):
  recur_id = HiddenField('recur_id', validators=[validators.InputRequired()])
  # charge_today = None
  amount = None

  def validate_start_date(form, field):
    return True

  def fields_append(form):
    for field in form:
      field.id = field.id + "-edit"
      field.label.field_id = field.label.field_id + "-edit"

""" Set up form for display and processing """
def get_form_recur(formClass, new=False):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  # Determine if form should start as ACH or Card. If default payment exists, use that. Otherwise, card.
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s ORDER BY pref_rank', (g.user.cust_acct_id, True))
  saved_pmts = curdict.fetchall()
  if len(saved_pmts) > 0:
    default_method = saved_pmts[0]['pmt_method']
  else:
    default_method = 'ach'
  if new:
    # passing formdata=None explicitly prevents the form from automatically grabbing request data
    # which is what we want 
    form = formClass(formdata=None, pmt_method=default_method, scheduled_date=forms.get_user_date(), billing_state=forms.get_state_default())
  else:
    form = formClass(pmt_method=default_method, scheduled_date=forms.get_user_date(), billing_state=forms.get_state_default())
  if formClass == RecurEditForm:
    form.fields_append()
  # Format saved payments for display in dropdown, then add to pmt_vault_id choice field
  saved_pmts = forms.format_saved_pmts(saved_pmts)
  form.pmt_vault_id.choices = saved_pmts
  form.pmt_vault_id.choices.append(("", 'Enter New Payment Method'))
  # Get choices for countries and states from database
  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.billing_country.choices = [(country['iso_2'], country['name']) for country in curdict]
  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.billing_state.choices = [(state['abbreviation'], state['name']) for state in curdict]
  return form

""" Main function for showing the recurring payments page (currently the main page, or index) """
def show_recur(form=None, editForm=None, recurs=None, **kwargs):
  if (request.args):
    edit_id = request.args['id']
    flash(edit_id, 'edit_id')
    return redirect( url_for('show_recur') )
  if not form:
    form = get_form_recur(RecurPmtForm, True)
  if not editForm:
    editForm = get_form_recur(RecurEditForm, True)
  if not recurs:
    recurs = get_saved_recurs()
  log_access.log_page()
  # Convenience fee variables
  kwargs.update(util.fee_dict)
  return render_template('recur.html', form=form, editForm=editForm, recurs=recurs, **kwargs)

""" Utility function for appending the proper day suffix """
def day_suffix(num):
  num = str(num)
  last = num[-1]
  if last == "1":
    num += "st"
  elif last == "2":
    num += "nd"
  elif last == "3":
    num += "rd"
  else:
    num += "th"
  return num

""" Get saved recurring payments. Format for html display as well. """
def get_saved_recurs():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_recur_vw WHERE cust_acct_id = %s AND is_active ORDER BY day_of_month', (g.user.cust_acct_id,))
  recur_list = [dict(row) for row in curdict]
  for recur in recur_list:
    recur['day_of_month_suf'] = day_suffix(recur['day_of_month'])
    if recur['pmt_method'] == "card":
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(recur['expire_date']));
      recur['expire_date'] = match.group(2) + "/" + match.group(1)[-2:]
      recur['number_info'] = "Credit Card<br>..." + recur['card_number'][-4:] + " exp. " + recur['expire_date']
    elif recur['pmt_method'] == 'ach':
      recur['number_info'] = "Bank Account<br> Routing: " + recur['routing_number'] + "<br>Acc: " + recur['account_number']
  return recur_list

""" Add new recurring payment. Called by main form no reucrring payment page"""
def add_recur():
  form = get_form_recur(RecurPmtForm)
  '''if form.start_date.data or form.end_date.data or form.max_amount.data:
    type = "advanced"
  else:
    type = "basic"'''
  if not form.validate_on_submit():
    return show_recur(form=form) # no type
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s and is_active', (g.user.cust_acct_id,))
  cust_acct = curdict.fetchone()
  if not cust_acct:
    flash('Customer could not be found.', 'error')
    return show_recur(form=form) # no type
  # Saves or updates method as needed, returns pmt_vault_id on success, False on failure.
  # Also flashes error message on failure. See forms.py in lib
  pmt_vault_id = forms.id_number_mismatch_check(form, True)
  if not pmt_vault_id:
    # print("ID mismatch")
    return show_recur(form=form)

  curdict.execute('SELECT * FROM pmt_recur_vw where cust_acct_id = %s and day_of_month = %s and is_active = %s', (g.user.cust_acct_id, 1, True))
  existing = curdict.fetchone()
  if (existing):
    flash('Recurring payment on the 1st of the month already exists. If you wish to edit this, please click the edit button in the payment\'s dropdown. \nYou may only have one recurring payment per day of the month.', 'error')
    return show_recur(form=form) # no type
  # Now, we actually process the payment, if needed
  if form.amount.data:
    if form.pmt_method.data == 'card':
      txn = card.process_card_pmt(form, is_vault_txn=True, pmt_vault_id=pmt_vault_id, date=datetime.datetime.today(), ret=True)
    elif form.pmt_method.data == 'ach':
      txn = ach.process_ach_pmt(form, is_vault_txn=True, pmt_vault_id=pmt_vault_id, date=datetime.datetime.today(), ret=True)

    if int(txn['response']) != 1:
      if form.pmt_method.data == 'card':
        curdict.execute('UPDATE pmt_vault_card_tbl SET is_active = %s WHERE id = %s', (False, pmt_vault_id))
      if form.pmt_method.data == 'ach':
        curdict.execute('UPDATE pmt_vault_ach_tbl SET is_active = %s WHERE id = %s', (False, pmt_vault_id))
      flash('Onetime payment failed; your recurring payment therefore was not saved. Please use or enter a different payment method, or contact billing to resolve this issue.', 'error')
      return show_recur(form=form)

  #if form.day_of_month.data == None:
  form.day_of_month.data = 1
  # Insert new recur rule into database
  if not form.reminder.data:
    form.remind_days_before.data = None
  # if the day of the month is the same as today's and there is no start date specified,
  # we don't want the server to try to process today, so we set start date to one 
  # month from now
  if form.day_of_month.data == datetime.date.today().day and not form.start_date.data:
    form.start_date.data = datetime.date.today() + relativedelta(months=1)
  curdict.execute('INSERT INTO pmt_recur_month_tbl (pmt_vault_id, max_amount, day_of_month, start_date, end_date, note, email, remind_days_before) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING id', (pmt_vault_id, form.max_amount.data, form.day_of_month.data, form.start_date.data, form.end_date.data, form.note.data, g.user.email, form.remind_days_before.data))
  recent_recur = curdict.fetchone()
  conn.commit()
  
  # Not sure what this is for
  is_demo_mode = False
  if form.note.data == 'demo':
    is_demo_mode = True

  # send email
  # get templates as Jinja renderable tempaltes
  html_template = util.get_template('sch_recur_tmpl.html')
  text_template = util.get_template('sch_recur_tmpl.txt')

  # get next unique email id. need it here to display in email template, and to enter into database
  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']

  # get view entry for recur (includes payment info)
  curdict.execute('SELECT * FROM pmt_recur_vw WHERE id = %s AND cust_acct_id = %s AND is_active', (recent_recur['id'], g.user.cust_acct_id))
  property_dict = util.format_pay_prop(dict(curdict.fetchone()), email_id)
  property_dict['day_of_month_suf'] = day_suffix(property_dict['day_of_month'])

  # set up arguments for sending of email
  html_body = html_template.render(property_dict)
  text_body = text_template.render(property_dict)
  subject = "Created Recurring Payment to Micro Office"

  ChargeMail.send_html_mail(g.user.email, None, subject, text_body, html_body, email_id, g.user.cust_acct_id, conn)
  # make newly added recur pop
  if form.amount.data:
    return render_template('recur_result.html', pmt_nmi_txn=txn, recur=property_dict)
  else:
    flash(recent_recur['id'], 'recur_id')
    return redirect( url_for('show_recur') )


def get_recur_edit():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_recur_vw WHERE id = %s AND is_active', (request.args['id'],))
  recur = curdict.fetchone()
  if not recur:
    return(json.dumps("This recurring payment no longer exists or is inactive."))
  recur_dict = dict(recur)
  if recur_dict['remind_days_before']:
    recur_dict['reminder'] = True
  date_handler = lambda obj: (
      obj.isoformat()
      if isinstance(obj, datetime.datetime)
      or isinstance(obj, datetime.date)
      else None)
  return json.dumps(recur_dict, default=date_handler)

def update_recur():
  form = get_form_recur(RecurEditForm)
  '''if form.start_date.data or form.end_date.data or form.max_amount.data:
    type = "advanced"
  else:
    type = "basic"'''
  if not form.validate_on_submit():
    flash('recur_edit', 'open')
    return show_recur(editForm=form) # no type
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_recur_vw WHERE cust_acct_id = %s AND id = %s AND is_active', (g.user.cust_acct_id, form.recur_id.data))
  recur = curdict.fetchone()
  if not recur:
    flash('recur_edit', 'open')
    flash('You are not authorized to edit this recurring payment.', 'error_edit')
    return show_recur(editForm=form) # no type
  if int(recur['day_of_month']) != int(form.day_of_month.data):
    flash('recur_edit', 'open')
    flash('You have changed the day of the month of this payment, which you are not allowed to do. Please contact MicroOffice billing to do so.', 'error_edit')
    return show_recur(editForm=form) # no type
  if not form.reminder.data:
    form.remind_days_before.data = None
  curdict.execute('UPDATE pmt_recur_month_tbl SET (pmt_vault_id, max_amount, day_of_month, start_date, end_date, note, remind_days_before) = (%s, %s, %s, %s, %s, %s, %s) WHERE id = %s RETURNING *', (form.pmt_vault_id.data, form.max_amount.data, form.day_of_month.data, form.start_date.data, form.end_date.data, form.note.data, form.remind_days_before.data, form.recur_id.data))
  recent_recur = curdict.fetchone()
  conn.commit()


  # send email
  # get templates as Jinja renderable tempaltes
  html_template = util.get_template('update_recur_tmpl.html')
  text_template = util.get_template('update_recur_tmpl.txt')

  # get next unique email id. need it here to display in email template, and to enter into database
  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']

  # get view entry for recur (includes payment info)
  curdict.execute('SELECT * FROM pmt_recur_vw WHERE id = %s AND cust_acct_id = %s AND is_active', (recent_recur['id'], g.user.cust_acct_id))
  new = dict(curdict.fetchone())
  old = dict(recur)
  for e in [new, old]:
    util.format_date(e)
    e['day_of_month_suf'] = day_suffix(e['day_of_month'])
  props = {'old': old, 'new': new}
  props = util.format_pay_prop(props, email_id)

  # set up arguments for sending of email
  html_body = html_template.render(props)
  text_body = text_template.render(props)
  subject = "Edited Recurring Payment to Micro Office"
  
  # Send email
  ChargeMail.send_html_mail(g.user.email, None, subject, text_body, html_body, email_id, g.user.cust_acct_id, conn)
  flash(form.recur_id.data, 'recur_id')
  return redirect( url_for('show_recur') )

def delete_recur():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_recur_vw WHERE cust_acct_id = %s AND id = %s AND is_active', (g.user.cust_acct_id, request.args['recur_id']))
  recur = curdict.fetchone()
  if not recur:
    flash('You are unauthorized to delete this recurring payment', 'error')
    return json.dumps("unauthorized")
  curdict.execute("UPDATE pmt_recur_month_tbl SET is_active = false WHERE id = %s", (request.args['recur_id'],))
  conn.commit()
  if recur['pmt_method'] == 'ach':
   msg = "Routing number: " + recur['routing_number'] + "; Account number: " + recur['account_number'] + ", on the " + day_suffix(recur['day_of_month'])
  elif recur['pmt_method'] == 'card':
    match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(recur['expire_date']));
    exp = match.group(2) + "/" + match.group(1)[-2:]
    msg = "Card number: ..." + str(recur['card_number'])[-4:] + " exp. " + exp + ", on the " + day_suffix(recur['day_of_month'])
  flash(msg, 'deleted_recur')
  return json.dumps("deleted")




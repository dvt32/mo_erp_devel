import psycopg2.extras
from flask import g, flash
from flask import request, render_template
from flask_wtf import FlaskForm
from wtforms.fields.html5 import EmailField
from wtforms import StringField, SubmitField
from wtforms.validators import InputRequired, Email
from email_validator import validate_email, EmailNotValidError
import smtplib
from email.mime.text import MIMEText
import email.utils
import email.header
from itsdangerous import URLSafeTimedSerializer
import database
from chargefacade import ChargeMail
import log_access
import util
import forms

class RegisterForm(FlaskForm):
  name = StringField('Name', validators=[InputRequired()], render_kw={'placeholder':'Name'})
  company = StringField('Company', validators=[InputRequired()], render_kw={'placeholder':'Company'})
  building = StringField('Building', validators=[InputRequired()], render_kw={'placeholder': 'Building'})
  email = EmailField('Email', validators=[InputRequired(), Email()], render_kw={'placeholder':'Email'})
  submitForm = SubmitField('Register')

class PasswordEmailForm(FlaskForm):
  email = EmailField('Email', validators=[InputRequired(), Email()], render_kw={'placeholder':'Email'})
  submitForm = SubmitField('Submit')

def forgot_password():
  form = PasswordEmailForm()
  return render_template('forgot_password.html', form=form)

def password_email():
  form = PasswordEmailForm()
  if not form.validate_on_submit():
    return render_template('forgot_password.html', form=form)
  email = form.email.data
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE email = %s', (email,))
  cust_pers = curdict.fetchone()
  if not cust_pers:
    flash('There is no account associated with the email you entered in our database', 'error')
    return render_template('forgot_password.html', form=form)
  serializer = URLSafeTimedSerializer('{{ pay_itsdangerous_secret_key }}')
  token = serializer.dumps(email, salt='confirm_email')

  # send email
  # get templates as Jinja renderable tempaltes
  html_template = util.get_template('forgot_password_tmpl.html')
  text_template = util.get_template('forgot_password_tmpl.txt')

  props = util.format_pay_prop({'token': token }, None)

  # set up arguments for sending of email
  html_body = html_template.render(props)
  text_body = text_template.render(props)
  subject = "Reset Password on Micro Office Pay Portal"
  
  # Send email
  ChargeMail.send_html_mail(email, None, subject, text_body, html_body, None, None, conn, do_commit=False)
  return render_template('forgot_password_thanks.html', email=email)

def register_input():
  email = request.args.get('email')
  if email:
    email = email.strip()
    try:
      validated_email = validate_email(email)
      email = validated_email['email']
      form.email.render_kw={'readonly': 'true'}
    except EmailNotValidError as e:
      return render_template('bad_register.html', error=str(e))
  form = RegisterForm(email=email)
  return render_template('register_input.html', form=form)

def register_thanks():
  form = RegisterForm()
  if not form.validate_on_submit():
    return render_template('register_input.html', form=form)
  email_addr = form.email.data.strip().lower()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE trim(both ' ' from lower(email)) = %s', (email_addr,))
  cust_pers = curdict.fetchone()
  if cust_pers:
    if cust_pers['email'] != email_addr:
      curdict.execute('UPDATE cust_pers_tbl SET email = %s WHERE id = %s', (email_addr, cust_pers['id']))
      conn.commit()
    serializer = URLSafeTimedSerializer('{{ pay_itsdangerous_secret_key }}')
    token = serializer.dumps(email_addr, salt='confirm_email')
    mail_body = 'This email is being sent to verify that you are the owner of %s. Please visit https://pay.microoffice.com/confirm_email/%s to create a password for your account.' % (email_addr, token)
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = 'billing@microoffice.com'
    mail_msg['To'] = email_addr
    mail_msg['Subject'] = email.header.Header('Register on pay.microoffice.com - Verify email', 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
  else:
    name = form.name.data
    company = form.company.data
    building = form.building.data
    mail_body = '''User attempted to register on pay.microoffice.com, but could not find cust_pers_tbl row.

Name: %s
Company: %s
Building: %s
Email: %s
''' % (name, company, building, email_addr)
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = 'billing@microoffice.com'
    mail_msg['To'] = '{{ register_attempt_email }}'
    mail_msg['Subject'] = email.header.Header('Register attempt on pay.microoffice.com', 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
  return render_template('register_thanks.html', email=email_addr)

import json
from flask import request
import psycopg2.extras
import re
import database


def routing_check():
	routing_number = request.form['routing_number']
	match = re.match(r'\d{9}', routing_number)
	if match == None:
		return "Not a valid zip code format"
	conn = database.get_connection()
	curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	curdict.execute('SELECT * FROM fedach_participant_tbl WHERE routing_number = %s LIMIT 1', (routing_number,))
	result = curdict.fetchone()
	if not result:
		valid = "Not a valid routing number"
	else: 
		valid = "true"
	return json.dumps(valid)
import psycopg2.extras
from flask import request, render_template
from email_validator import validate_email, EmailNotValidError
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired
from flask_wtf import FlaskForm
from wtforms.fields.html5 import EmailField
from wtforms import StringField, SubmitField, PasswordField, HiddenField
from wtforms.validators import InputRequired, EqualTo
import database
import cryptpw

class PasswordForm(FlaskForm):
  token = HiddenField('token', validators=[InputRequired()])
  password = PasswordField('New Password', validators=[InputRequired(), EqualTo('confirm', message='Passwords do not match.')], render_kw={'placeholder': 'New Password'})
  confirm = PasswordField('Confirm Password', validators=[InputRequired()], render_kw={'placeholder': 'Confirm New Password'})
  submitForm = SubmitField('Submit')


def setpasswd_input(token):
  serializer = URLSafeTimedSerializer('{{ pay_itsdangerous_secret_key }}')
  try:
    # email tokens in registration/change password emails can exist for 120 minutes and then the user must request a new token
    email = serializer.loads(token, salt='confirm_email', max_age={{ pay_reg_max_age }})
  except SignatureExpired as e:
    return render_template('setpasswd_link_expired.html', error=str(e))
  except BadSignature as e:
    return render_template('setpasswd_link_badsig.html', error=str(e))
  if email:
    email = email.strip()
    try:
      validated_email = validate_email(email)
      email = validated_email['email']
    except EmailNotValidError as e:
      return render_template('bad_register.html', error=str(e))
  form_token = serializer.dumps(email, salt='set_password')
  form = PasswordForm(token=form_token)
  return render_template('setpasswd_input.html', form=form)

def setpasswd_result():
  form = PasswordForm()
  token = form.token.data
  serializer = URLSafeTimedSerializer('{{ pay_itsdangerous_secret_key }}')
  try:
    # form tokens from the set password form can exist for 120 minutes and then the user must request a new token
    email = serializer.loads(token, salt='set_password', max_age=7200)
  except BadSignature as e:
    return render_template('setpasswd_form_badsig.html', error=str(e))
  password = form.password.data
  confirm_password = form.confirm.data
  if password:
    password = password.strip()
  if not password:
    return render_template('setpasswd_missing_password.html')
  if password != confirm_password:
    return render_template('setpasswd_mismatch.html')
  if not email:
    return render_template('bad_register.html', error='missing email')
  email = email.strip()
  try:
    validated_email = validate_email(email)
    email = validated_email['email']
  except EmailNotValidError as e:
    return render_template('bad_register.html', error=str(e))
  hashed_password = cryptpw.hash_password(password)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('UPDATE cust_pers_tbl SET pay_ws_passwd = %s, pay_ws_cryptpasswd = %s WHERE email = %s', (password, hashed_password, email))
  updated_rows = curdict.rowcount
  conn.commit()
  return render_template('setpasswd_success.html', email=email)

import psycopg2.extras
from flask import request, render_template, redirect, url_for
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, HiddenField, IntegerField, BooleanField, RadioField
from wtforms.fields.html5 import DateField, DecimalField
from wtforms_components import If
from wtforms import validators, ValidationError
from dateutil.relativedelta import relativedelta
import smtplib
import jinja2
import datetime
import decimal
import card
import database
import solupay
import tzconvert
import calendar
import json
import re
from copy import deepcopy
import onetime
from chargefacade import ChargeMail
import forms
import util
import log_access

class UpcomingForm(forms.PmtForm):
  txn_id = HiddenField('txn_id', validators=[validators.InputRequired()])
  txn_type = HiddenField('txn_type', validators=[validators.InputRequired()])

def show_upcoming(form=None, upcoming=None):
  log_access.log_page()
  if form == None:
    form = forms.get_form_pmt(UpcomingForm, True)
  if upcoming == None:
    upcoming = get_upcoming()
  return render_template("upcoming.html", form=form, upcoming=upcoming, **util.fee_dict)

def get_upcoming():
  today = forms.get_user_date()
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL AND cust_acct_id = %s', (g.user.cust_acct_id,))
  scheduled_nmi = [dict(row) for row in curdict]
  for s in scheduled_nmi:
    s['txn_type'] = "nmi"
  curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL AND cust_acct_id = %s', (g.user.cust_acct_id,))
  scheduled_vault = [dict(row) for row in curdict]
  for s in scheduled_vault:
    s['txn_type'] = "vault"
  scheduled = scheduled_vault + scheduled_nmi
  for s in scheduled:
    if s['scheduled_date'] < today:
      s['past'] = True
    s['type'] = "scheduled"
  scheduled.sort(key=lambda el: el['scheduled_date'])
  curdict.execute('SELECT * FROM pmt_recur_vw WHERE cust_acct_id = %s AND (end_date >= %s OR end_date IS NULL) AND is_active ORDER BY day_of_month ASC', (g.user.cust_acct_id, today))
  upcoms = [dict(row) for row in curdict]
  upcoming = []
  cursor = today
  if len(upcoms) != 0:
    if len(scheduled) == 0:
      condition = lambda upcoming, upcoms, cursor: len(upcoming) < len(upcoms)
    else:
      condition = lambda upcoming, upcoms, cursor: (len(upcoming) < len(upcoms)) or cursor < (scheduled[len(scheduled)-1]['scheduled_date'])
    while (condition(upcoming, upcoms, cursor)):
      for up_entry in upcoms:
        up = deepcopy(up_entry)
        up['scheduled_date'] = datetime.date(cursor.year, cursor.month, up['day_of_month'])
        if up['start_date'] != None:
          if up['start_date'] > up['scheduled_date']:
            continue
        if up['end_date'] != None:
          if up['end_date'] <= up['scheduled_date']:
            continue
        if up['scheduled_date'] < cursor:
          continue
        up['type'] = "recurring"
        up['amount'] = up['max_amount']
        upcoming.append(up)
      else:
        cursor += relativedelta(months=1, day=1)
  upcoming += scheduled
  if len(upcoming) == 0:
    return None
  upcoming.sort(key=lambda el: el['scheduled_date'])
  upcoming_list = []
  for up in upcoming:
    if up['pmt_method'] == "card":
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(up['expire_date']));
      up['expire_date'] = match.group(2) + "/" + match.group(1)[-2:]
      up['number_info'] = "Credit Card<br>..." + up['card_number'][-4:] + " exp. " + up['expire_date']
    elif up['pmt_method'] == 'ach':
      up['number_info'] = "Bank Account<br> Routing: " + up['routing_number'] + "<br>Acc: " + up['account_number']
  return upcoming

def get_upcoming_edit():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  if request.args['txn_type'] == 'nmi':
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s AND cust_acct_id = %s', (request.args['id'], g.user.cust_acct_id))
    upcoming = curdict.fetchone()
    if not upcoming:
      return(json.dumps("This upcoming payment has already been processed or does not belong to your account."))
    upcoming_dict = dict(upcoming)
    upcoming_dict['txn_type'] = 'nmi'
  elif request.args['txn_type'] == 'vault':
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE id = %s AND cust_acct_id = %s', (request.args['id'], g.user.cust_acct_id))
    upcoming = curdict.fetchone()
    if not upcoming:
      return(json.dumps("This upcoming payment has already been processed or does not belong to your account."))
    upcoming_dict = dict(upcoming)
    upcoming_dict['txn_type'] = 'vault'
  date_handler = lambda obj: (
      obj.isoformat()
      if isinstance(obj, datetime.datetime)
      or isinstance(obj, datetime.date)
      else None)
  return json.dumps(upcoming_dict, default=date_handler)

def update_upcoming():
  form = forms.get_form_pmt(UpcomingForm)
  if not form.validate_on_submit():
    flash('upcoming_edit', 'open')
    return show_upcoming(form=form)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  if form.txn_type.data == 'nmi':
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, form.txn_id.data))
    upcoming = curdict.fetchone()
  elif form.txn_type.data == 'vault':
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE cust_acct_id = %s AND id = %s', (g.user.cust_acct_id, form.txn_id.data))
    upcoming = curdict.fetchone()
  if not upcoming:
    flash('upcoming_edit', 'open')
    flash('You are not authorized to edit this upcoming payment.', 'error')
    return redirect( url_for('show_upcoming') )
  if form.scheduled_date.data <= forms.get_user_date():
    flash('upcoming_edit', 'open')
    flash('You cannot reschedule an upcoming payment for today. If you want to make a payment today, visit the Make Payment page. You can also delete this one.', 'error')
    return redirect( url_for('show_upcoming') )
  if form.pmt_method.data == 'card':
    prefee_amount, convenience_fee, total_amount = util.get_fees(form.amount.data)
  elif form.pmt_method.data == 'ach':
    convenience_fee = None
  if form.txn_type.data == 'nmi':
    if form.pmt_vault_id.data == '': #no change, still an nmi
      if form.pmt_method.data == 'card':
        expire_date = datetime.date(2000 + int(form.expire_year.data), int(form.expire_month.data), 1)
        if form.billing_country.data != 'US':
          state = form.billing_state_nonus.data
        else:
          state = form.billing_state.data
        curdict.execute('UPDATE pmt_nmi_txn_card_tbl SET (card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_city, billing_state, billing_zip_code, billing_country, amount, scheduled_date, note) = (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) WHERE cust_acct_id = %s AND response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL RETURNING *', (form.card_number.data, expire_date, form.cvv.data, form.billing_first_name.data, form.billing_last_name.data, form.billing_company.data, form.billing_address_1.data, form.billing_address_2.data, form.billing_city.data, state, form.billing_zip_code.data, form.billing_country.data, form.amount.data, form.scheduled_date.data, form.note.data, g.user.cust_acct_id))
        new = curdict.fetchone()
      elif form.pmt_method.data == 'ach':
        curdict.execute('UPDATE pmt_nmi_txn_ach_tbl SET (account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note) = (%s, %s, %s, %s, %s, %s, %s, %s) WHERE cust_acct_id = %s AND response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL RETURNING *', (form.account_type.data, form.owned_by.data, form.registered_name.data, form.routing_number.data, form.account_number.data, form.amount.data, form.scheduled_date.data, form.note.data, g.user.cust_acct_id))
        new = curdict.fetchone()
    elif form.pmt_vault_id.data != '': #switched to a vault txn
      if form.pmt_method.data == 'card':
        curdict.execute('DELETE FROM pmt_nmi_txn_card_tbl WHERE cust_acct_id = %s AND response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL', (g.user.cust_acct_id,))
      elif form.pmt_method.data == 'ach':
        curdict.execute('DELETE FROM pmt_nmi_txn_ach_tbl WHERE cust_acct_id = %s AND response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL', (g.user.cust_acct_id,))
      curdict.execute('INSERT INTO pmt_vault_txn_tbl (pmt_vault_id, amount, convenience_fee, scheduled_date, note, txn_purpose) VALUES (%s, %s, %s, %s, %s, %s) RETURNING *', (form.pmt_vault_id.data, form.amount.data, convenience_fee, form.scheduled_date.data, form.note.data, 'service'))
      new = curdict.fetchone()
      curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE id = %s and cust_acct_id = %s', (new['id'], g.user.cust_acct_id))
      new = curdict.fetchone()
  elif form.txn_type.data == 'vault':
    if form.pmt_vault_id.data == '': #change to nmi txn
      curdict.execute('DELETE FROM pmt_vault_txn_tbl WHERE id = %s', (form.txn_id.data,))
      if form.pmt_method.data == 'card':
        expire_date = datetime.date(2000 + int(form.expire_year.data), int(form.expire_month.data), 1)
        if form.billing_country.data != 'US':
          state = form.billing_state_nonus.data
        else:
          state = form.billing_state.data
        curdict.execute('INSERT INTO pmt_nmi_txn_card_tbl (cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_city, billing_state, billing_zip_code, billing_country, amount, scheduled_date, note, txn_purpose) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *', (g.user.cust_acct_id, form.card_number.data, expire_date, form.cvv.data, form.billing_first_name.data, form.billing_last_name.data, form.billing_company.data, form.billing_address_1.data, form.billing_address_2.data, form.billing_city.data, state, form.billing_zip_code.data, form.billing_country.data, form.amount.data, form.scheduled_date.data, form.note.data, 'service'))
        new = curdict.fetchone()
      elif form.pmt_method.data == 'ach':
        curdict.execute('INSERT INTO pmt_nmi_txn_ach_tbl (cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, txn_purpose) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *', (g.user.cust_acct_id, form.account_type.data, form.owned_by.data, form.registered_name.data, form.routing_number.data, form.account_number.data, form.amount.data, form.scheduled_date.data, form.note.data, 'service'))
        new = curdict.fetchone()
    elif form.pmt_vault_id.data != '': #simply update vault txn
      curdict.execute('UPDATE pmt_vault_txn_tbl SET (pmt_vault_id, amount, convenience_fee, scheduled_date, note) = (%s, %s, %s, %s, %s) WHERE id = %s RETURNING *', (form.pmt_vault_id.data, form.amount.data, convenience_fee, form.scheduled_date.data, form.note.data, form.txn_id.data))
      new = curdict.fetchone()
      curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE id = %s and cust_acct_id = %s', (new['id'], g.user.cust_acct_id))
      new = curdict.fetchone()
  

  html_template = util.get_template('update_txn_tmpl.html')
  text_template = util.get_template('update_txn_tmpl.txt')

  curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
  email_id = curdict.fetchone()['next_id']

  new = dict(new)
  old = dict(upcoming)
  for e in [new, old]:
    util.format_date(e)
  props = {'old': old, 'new': new}
  props = util.format_pay_prop(props, email_id)

  html_body = html_template.render(props)
  text_body = text_template.render(props)

  ChargeMail.send_html_mail(g.user.email, None, "Edited Upcoming Payment to Micro Office", text_body, html_body, email_id, g.user.cust_acct_id, conn)
  conn.commit()
  flash(form.txn_id.data, 'upcoming_id')
  return redirect( url_for('show_upcoming'))

def delete_upcoming():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  if request.args['txn_type'] == "nmi":
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE cust_acct_id = %s AND id = %s AND response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL', (g.user.cust_acct_id, request.args['upcoming_id']))
    upcom = curdict.fetchone()
  elif request.args['txn_type'] == "vault":
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE cust_acct_id = %s AND id = %s AND response IS NULL AND transactionid IS NULL AND request_posted_ts IS NULL', (g.user.cust_acct_id, request.args['upcoming_id']))
    upcom = curdict.fetchone()
  if not upcom:
    flash('You are unauthorized to delete this scheduled payment, or it has already been processed', 'error')
    return json.dumps("unauthorized")
  if request.args['txn_type'] == "nmi":
    if upcom["pmt_method"] == "card":
      curdict.execute("DELETE FROM pmt_nmi_txn_card_tbl WHERE id = %s", (request.args['upcoming_id'],))
      conn.commit()
    elif upcom["pmt_method"] == "ach":
      curdict.execute("DELETE FROM pmt_nmi_txn_ach_tbl WHERE id = %s", (request.args['upcoming_id'],))
      conn.commit()
  elif request.args['txn_type'] == "vault":
    curdict.execute("DELETE FROM pmt_vault_txn_tbl WHERE id = %s", (request.args['upcoming_id'],))
    conn.commit()
  if upcom['pmt_method'] == 'ach':
   msg = "Routing number: " + upcom['routing_number'] + "; Account number: " + upcom['account_number'] + ", " + upcom['amount'] + " on " + upcom['scheduled_date'].strftime("%a %b %d, %Y")
  elif upcom['pmt_method'] == 'card':
    match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(upcom['expire_date']));
    exp = match.group(2) + "/" + match.group(1)[-2:]
    msg = "Card number: ..." + str(upcom['card_number'])[-4:] + " exp. " + exp + ", " + upcom['amount'] + " on " + upcom['scheduled_date'].strftime("%a %b %d, %Y")
  flash(msg, 'deleted_upcoming')
  return json.dumps("deleted")

from flask import request, abort, redirect, url_for, flash, render_template
from flask import g, flash
from flask_wtf import FlaskForm
from wtforms import SubmitField, PasswordField, HiddenField
from wtforms.fields.html5 import EmailField
from wtforms import validators, ValidationError
import warnings
import psycopg2
import psycopg2.extras
import database
import datetime
from email_validator import validate_email, EmailNotValidError
import smtplib
from email.mime.text import MIMEText
import email.utils
import email.header
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired
import cryptpw
import log_access


class ChangeEmailForm(FlaskForm):
  email = EmailField('New Email', validators=[validators.InputRequired(), validators.Email()], render_kw={'placeholder':'New Email'})
  submitForm = SubmitField('Submit')

class ChangePasswordForm(FlaskForm):
  old_pwrd = PasswordField('Old Password', validators=[validators.InputRequired()], render_kw={'placeholder': 'Old Password'})
  new_pwrd = PasswordField('New Password', validators=[validators.InputRequired(), validators.EqualTo('confirm', message='Passwords do not match.')], render_kw={'placeholder': 'New Password'})
  confirm = PasswordField('Confirm Password', validators=[validators.InputRequired()], render_kw={'placeholder': 'Confirm New Password'})
  submitForm = SubmitField('Submit')

def show_settings():
  log_access.log_page()
  emailform = ChangeEmailForm()
  pwrdform = ChangePasswordForm()
  return render_template("user_settings.html", emailform=emailform, pwrdform=pwrdform)

def change_password():
  form = ChangePasswordForm()
  if not form.validate_on_submit():
    emailform = ChangeEmailForm()
    flash('password', 'open')
    return render_template('user_settings.html', emailform=emailform, pwrdform=form)
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE email = %s', (g.user.email,))
  cust_pers = curdict.fetchone()
  if cust_pers and cryptpw.check_password(form.old_pwrd.data, cust_pers['pay_ws_cryptpasswd']):
    hashed_password = cryptpw.hash_password(form.new_pwrd.data)
    curdict.execute('UPDATE cust_pers_tbl SET pay_ws_passwd = %s, pay_ws_cryptpasswd = %s WHERE cust_acct_id = %s', (form.new_pwrd.data, hashed_password, g.user.cust_acct_id))
    conn.commit()
    # if number of bcrypt rounds has increased, update the crypted password
    if not cryptpw.hash_param_match_default(cust_pers['pay_ws_cryptpasswd']):
      update_cryptpasswd(conn, password, cust_pers['id'])
    mail_body = 'You have successfully changed your Micro Office paysite password. If this was not you, please email billing@microoffice.com and we\'ll confirm that your account is secure.'
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = '{{ payment_mail_from }}'
    mail_msg['To'] = g.user.email
    mail_msg['Subject'] = email.header.Header('Changed password on pay.microoffice.com - Notification', 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
    flash('Your password has been changed.','success')
    return redirect(url_for('show_settings'))
  else:
    emailform = ChangeEmailForm()
    form.old_pwrd.errors.append('Incorrect password')
    flash('password', 'open')
    return render_template('user_settings.html', emailform=emailform, pwrdform=form)


def new_email():
  form = ChangeEmailForm()
  if not form.validate_on_submit():
    pwrdform = ChangePasswordForm()
    flash('email', 'open')
    return render_template('user_settings.html', emailform=form, pwrdform=pwrdform)
  email_addr = form.email.data
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE email = %s', (g.user.email,))
  cust_pers = curdict.fetchone()
  if cust_pers:
    serializer = URLSafeTimedSerializer('{{ pay_itsdangerous_secret_key }}')
    token = serializer.dumps(email_addr, salt='confirm_email')
    mail_body = 'This email is being sent to verify that you are the owner of %s. Please visit {{ pay_base_url }}/set_email/%s to confirm this change of email address. If you do not want this change, ignore this message and your email will not be changed.' % (email_addr, token)
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = '{{ payment_mail_from }}'
    mail_msg['To'] = email_addr
    mail_msg['Subject'] = email.header.Header('Change email on pay.microoffice.com - Verify email', 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
    mail_body = '''Please be advised that you have requested a change of email from this address on pay.microoffice.com. If you did not request this change, you may want to change to a more secure password.
    New Email: %s
  ''' % (email_addr)
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = 'billing@microoffice.com'
    mail_msg['To'] = g.user.email
    mail_msg['Subject'] = email.header.Header('Changed email on pay.microoffice.com', 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
    flash(email_addr, 'email')
    return redirect(url_for('show_settings'))
  else:
    flash('Cannot find current email in customer database.', 'error')
    return redirect(url_for('show_settings'))

def set_email(token):
  serializer = URLSafeTimedSerializer('{{ pay_itsdangerous_secret_key }}')
  try:
    # email tokens in change email emails can exist for 120 minutes and then the user must request a new token
    email = serializer.loads(token, salt='confirm_email', max_age=7200)
  except SignatureExpired as e:
    flash (str(e), 'error')
    return redirect(url_for('show_settings'))
  except BadSignature as e:
    flash (str(e), 'error')
    return redirect(url_for('show_settings'))
  if email:
    email = email.strip()
    try:
      validated_email = validate_email(email)
      email = validated_email['email']
    except EmailNotValidError as e:
      flash (str(e), 'error')
      return redirect(url_for('show_settings'))
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('UPDATE cust_pers_tbl SET email = %s WHERE id = %s', (email, g.user.id))
  flash('Email set to ' + email, 'success')
  return redirect(url_for('show_settings'))

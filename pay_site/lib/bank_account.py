import pycurl
import urllib
import urllib.parse
import io

def find_cust_ach(cust_acct_id, routing_number, account_number):
  '''Find a bank account in pmt_vault_vw.

  Args:
    cust_acct_id (int): id value of the cust_acct.
    routing_number (str): FedACH bank routing number.
    account_number (str): account number at the bank.

  Returns:
    pmt_vault_id or None if customer does not have the bank account.
  '''
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST, 1)
  # set connect timeout to 15 seconds
  curlObj.setopt(pycurl.CONNECTTIMEOUT, 15)
  # set timeout to 15 seconds
  curlObj.setopt(pycurl.TIMEOUT, 15)
  curlObj.setopt(pycurl.HEADER, 0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION, responseIO.write)
  url = '{{ payapi_base_url }}/find_cust_ach'
  curlObj.setopt(pycurl.URL, url)
  query = ''
  query += 'cust_acct_id=' + urllib.parse.quote(str(cust_acct_id))
  query += '&'
  query += 'routing_number=' + urllib.parse.quote(str(routing_number))
  query += '&'
  query += 'account_number=' + urllib.parse.quote(str(account_number))
  curlObj.setopt(pycurl.POSTFIELDS, query)
  curlObj.perform()
  data = responseIO.getvalue().decode('utf-8')
  pmt_vault_id = None
  if data:
    pmt_vault_id = int(data)
  return pmt_vault_id

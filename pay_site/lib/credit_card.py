import pycurl
import urllib
import urllib.parse
import io

def is_valid_luhn(card_number):
  """check whether card passes a Luhn mod 10 checksum"""
  
  sum = 0
  num_digits = len(card_number)
  oddeven = num_digits & 1
  for count in range(0, num_digits):
    digit = int(card_number[count])
    if not ((count & 1) ^ oddeven):
      digit = digit * 2
    if digit > 9:
      digit = digit - 9
    sum = sum + digit
  return ((sum % 10)==0)

def find_cust_card(cust_acct_id, card_number):
  '''Find a credit card number in pmt_vault_vw.

  Args:
    cust_acct_id (int): id value of the cust_acct.
    card_number (str): valid card number.

  Returns:
    pmt_vault_id or None if customer does not have the card.
  '''
  if not is_valid_luhn(card_number):
    raise Exception('not valid card number: %s' % (card_number,))
  responseIO = io.BytesIO()
  curlObj = pycurl.Curl()
  curlObj.setopt(pycurl.POST, 1)
  # set connect timeout to 15 seconds
  curlObj.setopt(pycurl.CONNECTTIMEOUT, 15)
  # set timeout to 15 seconds
  curlObj.setopt(pycurl.TIMEOUT, 15)
  curlObj.setopt(pycurl.HEADER, 0)
  curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
  curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
  curlObj.setopt(pycurl.WRITEFUNCTION, responseIO.write)
  url = '{{ payapi_base_url }}/find_cust_card'
  curlObj.setopt(pycurl.URL, url)
  query = ''
  query += 'cust_acct_id=' + urllib.parse.quote(str(cust_acct_id))
  query += '&'
  query += 'card_number=' + urllib.parse.quote(str(card_number))
  curlObj.setopt(pycurl.POSTFIELDS, query)
  curlObj.perform()
  data = responseIO.getvalue().decode('utf-8')
  pmt_vault_id = None
  if data:
    pmt_vault_id = int(data)
  return pmt_vault_id

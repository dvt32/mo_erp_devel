from sys import version_info
import bcrypt
from werkzeug.security import safe_str_cmp

# check whether Python 3 is being used
is_python3 = version_info[0] >= 3

# number of rounds/iterations; defaults to 12 if omitted; PostgreSQL defaults to 6 as of 2016
default_rounds = 12
# use 2a instead of 2b so that it works for Python AND PostgreSQL using PostgreSQL's crypt function
default_prefix = '2a'

def hash_password(password, rounds=default_rounds, prefix=default_prefix):
  if not password:
    raise ValueError('You must specify password.')
  if is_python3:
    if isinstance(password, str):
      password = bytes(password, 'utf-8')
    if isinstance(prefix, str):
      prefix = bytes(prefix, 'utf-8')
  if not is_python3:
    if isinstance(password, unicode):
      password = password.encode('utf-8')
    if isinstance(prefix, unicode):
      prefix = prefix.encode('utf-8')
  return str(bcrypt.hashpw(password, bcrypt.gensalt(rounds, prefix=prefix)), 'utf-8')

def check_password(password, password_hash):
  if not password or not password_hash:
    return False
  if is_python3 and isinstance(password, str):
    password = bytes(password, 'utf-8')
  if is_python3 and isinstance(password_hash, str):
    password_hash = bytes(password_hash, 'utf-8')
  if not is_python3 and isinstance(password, unicode):
    password = password.encode('utf-8')
  if not is_python3 and isinstance(password_hash, unicode):
    password_hash = password_hash.encode('utf-8')
  return safe_str_cmp(bcrypt.hashpw(password, password_hash), password_hash)

# check if prefix is same in password hash and num rounds in password hash is greater than or equal to default.
# if not, caller can decide to rehash the password using new prefix and/or rounds.
def hash_param_match_default(password_hash):
  prefix = password_hash[1:3]
  rounds = int(password_hash[4:6])
  return (prefix == default_prefix) and (rounds >= default_rounds)

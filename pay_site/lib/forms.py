import psycopg2
import psycopg2.extras
from flask import g
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, HiddenField, IntegerField, BooleanField, RadioField
from wtforms.fields.html5 import DateField, DecimalField
from wtforms_components import If
from wtforms import validators, ValidationError
import decimal
import datetime
import re
import calendar
import json
import tzconvert
import database
import credit_card
import bank_account
import util
import card
import ach


# The combined ACH/Card base class.
class PmtInfoForm(FlaskForm):
  pmt_vault_id = SelectField('Select Saved Payment Method', choices=[], validators=[validators.Optional()])
  pmt_method = RadioField('Select Payment Type', choices=[('card', 'Credit Card'), ('ach', "Bank Account")])
  routing_number = StringField('Bank Routing Number', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='ach', validators.Regexp(r'\d{9}|\d+\*+\d+'))], render_kw={'maxlength':'9', 'placeholder': 'Routing number'})
  account_number = StringField('Bank Account Number', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='ach', validators.Regexp(r'\d{4,17}|\d+\*+\d+'))], render_kw={'placeholder': 'Account number'})
  account_type = SelectField('Account Type', choices=[('checking', 'Checking'), ('savings', 'Savings')], default='checking', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired())])
  owned_by = SelectField('Account Ownership', choices=[('business', 'Business'), ('personal', 'Personal')], default='business', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired())])
  registered_name = StringField('Business Name on Account', validators=[If(lambda form, field: form.pmt_method.data=='ach', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='ach', validators.Length(max=64))], render_kw={'maxlength':'64', 'placeholder': 'Name on account'})
  billing_first_name = StringField('First Name', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'cc-given-name', 'placeholder':'First name on card'})
  billing_last_name = StringField('Last Name', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'cc-family-name', 'placeholder':'Last name on card'})
  card_number = StringField('Credit Card Number', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='card', validators.Regexp(r'^\d{15,16}|\d+\*+\d+$'))], render_kw={'autocomplete': 'cc-number', 'placeholder':'################', 'maxlength':'16'})
  expire_month = SelectField('Month', choices=util.month_choices, validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'cc-exp-month'})
  expire_year = StringField('Year', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired()), If(lambda form, field: form.pmt_method.data=='card', validators.Regexp(r'^\d{2}$'))], render_kw={'autocomplete': 'cc-exp-year', 'placeholder': 'YY', 'maxlength':'2'})
  cvv = StringField('CVV/CVC/CSC', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.Optional()), If(lambda form, field: form.pmt_method.data=='card', validators.Regexp(r'^\d{3,4}|\*{3,4}$')), If(lambda form, field: form.pmt_method.data=='card', validators.Length(min=3,max=4))], render_kw={'autocomplete': 'cc-csc', 'placeholder': 'CVV', 'maxlength':'4', 'class': 'card-input'})
  billing_company = StringField('Company', render_kw={'autocomplete': 'organization', 'placeholder': 'Company (optional)'})
  billing_address_1 = StringField('Address Line 1', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'address-line1', 'placeholder': 'Address Line 1'})
  billing_address_2 = StringField('Address Line 2', render_kw={'autocomplete': 'address-line2', 'placeholder': 'Address Line 2 (optional)'})
  billing_city = StringField('City', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'address-level2', 'placeholder': 'City'})
  billing_state = SelectField('State', choices=[], validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'address-level1', 'placeholder': 'State'})
  billing_state_nonus = StringField('State', render_kw={'autocomplete': 'address-level1', 'placeholder': 'State'})
  billing_zip_code = StringField('Zip Code', validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'postal-code', 'placeholder': 'Zip Code', 'maxlength':'10'})
  billing_country = SelectField('Country', choices=[], validators=[If(lambda form, field: form.pmt_method.data=='card', validators.InputRequired())], render_kw={'autocomplete': 'country'}, default='US')
  
  def validate_card_number(form, field):
    if form.pmt_method.data == 'card':
      if form.pmt_vault_id.data != "" and field.data.isdigit():
        if not credit_card.is_valid_luhn(field.data):
          raise ValidationError('invalid card number: %s' % (field.data,))
        else:
          return True
      if form.pmt_vault_id.data == "" and not field.data.isdigit():
          return False
      else:
        return True
    else:
      return True

  def validate_billing_zip_code(form, field):
    if form.pmt_method.data == 'card':
      zip = field.data
      match = re.match(r'\d{5}(-\d{4})?', zip)
      if match == None:
        return False
      else:
        zip = zip[:5]
      state = form.billing_state.data
      conn = database.get_connection()
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('SELECT * FROM zip_code_tbl WHERE zip = %s AND state = %s LIMIT 1', (zip, state))
      result = curdict.fetchone()
      if not result:
        return False
      else: 
        return True
    else:
      return True

  def validate_billing_state(form, field):
    if form.pmt_method.data == 'card':
      if form.billing_country.data == 'US':
        conn = database.get_connection()
        curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        curdict.execute('SELECT * FROM state_tbl WHERE abbreviation = %s', (field.data,))
        state = curdict.fetchone()
        if not state:
          raise ValidationError('invalid U.S. state: %s; select a valid two character abbreviation' % (field.data,))
    else:
      return True

  def validate_expire_year(form, field):
    if form.pmt_method.data == 'card':
      today = datetime.date.today()
      if ((today.year - 2000) * 12 + today.month) > (int(field.data) * 12 + int(form.expire_month.data)):
        raise ValidationError('expire date is in the past: %s/%s' % (form.expire_month.data, field.data))
    else:
      return True

  def validate_routing_number(form, field):
    if form.pmt_method.data == 'ach':
      if form.pmt_vault_id != "" and field.data.isdigit():
        conn = database.get_connection()
        curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        curdict.execute('SELECT * FROM fedach_participant_tbl WHERE routing_number = %s', (field.data,))
        fedach_participant = curdict.fetchone()
        if not fedach_participant:
          raise ValidationError('invalid routing number: %s' % (field.data,))
        else:
          return True
      if form.pmt_vault_id.data == "" and not field.data.isdigit():
          return False
      else:
        return True
    else:
      return True

  def validate_account_number(form, field):
    if form.pmt_method.data == 'ach':
      if form.pmt_vault_id != "" and field.data.isdigit():
          return True
      if form.pmt_vault_id.data == "" and not field.data.isdigit():
          return False
      else:
        return True
    else:
      return True

class PmtForm(PmtInfoForm):
  amount = DecimalField('Amount', validators=[validators.InputRequired(), validators.NumberRange(min=0.01)], places=2, render_kw={'placeholder': 'Amount'})
  scheduled_date = DateField('Scheduled Date', validators=[validators.InputRequired()])
  note = StringField('Note', render_kw={'placeholder': 'Optional Note (e.g. invoice number, etc.)'})
  save_pmt_method = BooleanField('Save Payment Method', description="Save Payment Method for Future Use", default=True)
  submitForm = SubmitField('Submit')

  def fields_append(form):
    for field in form:
      field.id = (field.id) + "-pmt"
      field.label.field_id = field.label.field_id + "-pmt"

  def validate_scheduled_date(form, field):
    conn = database.get_connection()
    cust_timezone = tzconvert.cust_timezone(conn, g.user.cust_acct_id)
    # get current datetime in customer's time zone
    now_local = datetime.datetime.now(cust_timezone)
    # extract date portion of datetime to obtain current date from user's perspective
    today_local = now_local.date()
    if not field.data:
      raise ValidationError('Dates must be formatted as yyyy-mm-dd in browsers without a datepicker')
    if (field.data < today_local):
      raise ValidationError('scheduled date is in the past: %s; today is %s' % (str(field.data), str(today_local)))
    elif (field.data.year >= today_local.year+1 and field.data.month > today_local.month):
      raise ValidationError('scheduled date is more than a year in the future: %s; today is %s' % (str(field.data), str(today_local)))


def get_form_pmt(formClass, new=False):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s ORDER BY pref_rank LIMIT 1', (g.user.cust_acct_id, True))
  default_pmt = curdict.fetchone()
  if default_pmt:
    pmt_method = default_pmt['pmt_method']
  else:
    pmt_method = 'ach'
  if new:
    # passing formdata=None explicitly prevents the form from automatically grabbing request data
    # which is what we want
    form = formClass(formdata=None, pmt_method=pmt_method, scheduled_date=get_user_date(), billing_state=get_state_default())
  else: 
    form = formClass(pmt_method=pmt_method, scheduled_date=get_user_date(), billing_state=get_state_default())
  form.fields_append()
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s ORDER BY pref_rank', (g.user.cust_acct_id, True))
  form.pmt_vault_id.choices = []
  for i, pmt_vault in enumerate(curdict):
    pmt_vault = dict(pmt_vault)
    if pmt_vault['pmt_method'] == "card":
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(pmt_vault['expire_date']));
      pmt_vault['expire_date'] = match.group(2) + "/" + match.group(1)[-2:]
      form.pmt_vault_id.choices.append((str(pmt_vault['id']), "..." + pmt_vault['card_number'][-4:] + ' exp. ' + pmt_vault['expire_date']))
    elif pmt_vault['pmt_method'] == "ach":
      pmt_vault = dict(pmt_vault)
      form.pmt_vault_id.choices.append((str(pmt_vault['id']), "Routing: " + pmt_vault['routing_number'] + ", Acc: " + pmt_vault['account_number']))
  form.pmt_vault_id.choices.append(("", 'Enter New Payment Method'))
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM country_tbl ORDER BY name')
  form.billing_country.choices = [(country['iso_2'], country['name']) for country in curdict]
  curdict.execute('SELECT * FROM state_tbl ORDER BY name')
  form.billing_state.choices = [(state['abbreviation'], state['name']) for state in curdict]
  return form


################################
###### UTITLITY FUNCTIONS ######
################################



""" Formats a list of saved payments for selection from a ddslick dropdown. 
Takes in a list of dictionaries (rows from database) """
def format_saved_pmts(saved_pmts):
  result = []
  for i, pmt_vault in enumerate(saved_pmts):
    pmt_vault = dict(pmt_vault)
    if pmt_vault['pmt_method'] == "card":
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(pmt_vault['expire_date']));
      pmt_vault['expire_date'] = match.group(2) + "/" + match.group(1)[-2:]
      result.append((str(pmt_vault['id']), "..." + pmt_vault['card_number'][-4:] + ' exp. ' + pmt_vault['expire_date']))
    elif pmt_vault['pmt_method'] == "ach":
      pmt_vault = dict(pmt_vault)
      result.append((str(pmt_vault['id']), "Routing: " + pmt_vault['routing_number'] + ", Acc: " + pmt_vault['account_number']))
  return result

""" Gets user's data in their timezone"""
def get_user_date():
  conn = database.get_connection()
  cust_timezone = tzconvert.cust_timezone(conn, g.user.cust_acct_id)
  # get current datetime in customer's time zone
  now_local = datetime.datetime.now(cust_timezone)
  # extract date portion of datetime to obtain current date from user's perspective
  today_local = now_local.date()
  return today_local

""" Determines user's default state based on their user information"""
def get_state_default():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (g.user.cust_acct_id,))
  cust_acct = curdict.fetchone()
  curdict.execute('SELECT * FROM floor_tbl WHERE id = %s', (cust_acct['primary_floor_id'],))
  floor = curdict.fetchone()
  curdict.execute('SELECT * FROM building_tbl WHERE id = %s', (floor['building_id'],))
  building = curdict.fetchone()
  curdict.execute('SELECT * FROM state_tbl WHERE name = %s', (building['state'],))
  state = curdict.fetchone()
  return state['abbreviation']

""" For AJAX calls retrieving saved payment list as a javascript object."""
def get_saved_pmts():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE cust_acct_id = %s AND is_active = %s ORDER BY pref_rank', (g.user.cust_acct_id, True))
  pmt_list = [dict(row) for row in curdict]
  for pmt in pmt_list:
    if pmt['pmt_method'] == 'ach':
      curdict.execute('SELECT customer_name FROM fedach_participant_tbl WHERE routing_number = %s', (pmt['routing_number'],))
      bank = curdict.fetchone()
      if not bank:
        bank = ""
      else: 
        bank = bank['customer_name'].title()
      pmt['bank'] = bank
  date_handler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, datetime.datetime)
    or isinstance(obj, datetime.date)
    else None)
  if len(pmt_list) > 0:
    pmts = json.dumps(pmt_list, default=date_handler)
  else:
    pmts = json.dumps("none")
  return pmts

""" Checks if there is a mismatch between the pmt_vault_id and the card/account number entered. 
Save parameter tells whether to save the method if it is new.
Returns false on error and flashes an error message, 
the pmt_vault_id on success, or if the card is not saved, -1"""
def id_number_mismatch_check(form, save):
  # Export as part of processing function that can be used for processing a definites payment or here
  # Newly entered information; not a vault card
  pmt_vault_id = None
  if form.pmt_vault_id.data == "":
    # Check if it matches one already in database
    if form.pmt_method.data == "card":
      result = credit_card.find_cust_card(g.user.cust_acct_id, form.card_number.data)
    elif form.pmt_method.data == "ach":
      result = bank_account.find_cust_ach(g.user.cust_acct_id, form.routing_number.data, form.account_number.data)
    # It does exist; update it
    if result:
      if form.pmt_method.data == "card":
        card.update_card(form, result)
      elif form.pmt_method.data == "ach":
        ach.update_ach(form, result)
      pmt_vault_id = result
    # It does not exist, so we must save the payment method
    else:
      if save:
        if form.pmt_method.data == "card":
          pmt_vault_id = card.save_card(form)
        elif form.pmt_method.data == "ach":
          pmt_vault_id = ach.save_ach(form)
      else:
        pmt_vault_id = -1
    return pmt_vault_id
  # Selected a saved payment method
  else:
    if form.pmt_method.data == "card" and form.card_number.data.isdigit():
      # Card number is all digits. Shouldn't happen, but just in case
      # Check if this newly entered card number matches an existing one
      result = credit_card.find_cust_card(g.user.cust_acct_id, form.card_number.data) 
    elif form.pmt_method.data == "ach" and form.account_number.data.isdigit():
      # Account number is all digits. Shouldn't happen, but just in case
      # Check if this newly entered account number matches an existing one
      result = bank_account.find_cust_ach(g.user.cust_acct_id, form.routing_number.data, form.account_number.data)
    else:
      return form.pmt_vault_id.data
    # We've fallen through, so one of the first two cases was met. 
    # It does not match, it is a new card/account number. Error
    if not result:
      flash('Did you mean to enter a new card? If so, please select that option from the dropdown.', 'error')
      return False
    # It matches, but not the same card, somehow
    elif result != int(form.pmt_vault_id.data):
      flash('Card mismatch. It seems the number of the card you entered doesn\'t match the one you\'ve selected.', 'error')
      return False
    # It does match, and it's the same as the vault id. Odd, but ok. Update the card in database.
    elif result == int(form.pmt_vault_id.data):
      if form.pmt_method.data == "card":
        card.update_card(form, form.pmt_vault_id.data)
      elif form.pmt_method.data == "ach":
        ach.update_ach(form, form.pmt_vault_id.data)
    # Something else bad happened
    else:
      #print("card.py error at end of process_ach function: fell off end to catch all else case:")
      #print("result: " + result, ", pmt_vault_id: " + form.pmt_vault_id.data)
      return False
  # Process an actual payment, depending on whether or not this was selected 


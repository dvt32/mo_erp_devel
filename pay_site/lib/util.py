import jinja2
import decimal
import calendar
import datetime
import re


################################
###### OFTEN USED VALUES  ######
################################


month_choices = [(str(i), str(i) + ' - ' + calendar.month_abbr[i]) for i in range(1, 13)]

# convenience fee; use Decimal objects to preserve precision and avoid rounding errors from float/double representation
convenience_pct_str = '{{ pay_card_convenience_pct }}'
convenience_pct_multiplier = decimal.Decimal(convenience_pct_str) / decimal.Decimal(100)
hundred_plus_convenience_fee = (decimal.Decimal('100') + (decimal.Decimal('100') * convenience_pct_multiplier)).quantize(decimal.Decimal('1.00'))

fee_dict = {'convenience_pct':convenience_pct_str, 'hundred_plus_convenience_fee':hundred_plus_convenience_fee}



################################
######     FUNCTIONS      ######
################################


""" Calculates the total fee to charge after convenience fee is added
Returns tuple of total amount and convenience fee """
def get_fees(amount):
  prefee_amount = decimal.Decimal(amount).quantize(decimal.Decimal('1.00'))
  # add convenience fee
  convenience_fee = decimal.Decimal(amount) * convenience_pct_multiplier
  # round to two decimal places (cents)
  convenience_fee = convenience_fee.quantize(decimal.Decimal('1.00'))
  total_amount = prefee_amount + convenience_fee
  return (prefee_amount, convenience_fee, total_amount)

""" Opens url in pay share directory and turns it into jinja template """
def get_template(paypath):
  with open('{{ pay_basedir }}/share/' + paypath, 'r') as tmpl_file:
      file = tmpl_file.read()
  tmpl = jinja2.Template(file)
  return tmpl

"""Just adds a formatted expire date if there is one"""
def format_date(e):
  if 'expire_date' in e and e['expire_date']:
    match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(e['expire_date']))
    e['expire_mmyyyy'] = match.group(2) + "/" + match.group(1)
  return

""" Formats the properties of a dictionary for templates that involves a payment 
Takes in a method database entry dictionary """
def format_pay_prop(entry, email_id):
  format_date(entry)
  entry['convenience_pct'] = convenience_pct_str
  entry['email_id'] = email_id
  entry['pay_base_url'] = '{{ pay_base_url }}'
  entry['static_base_url'] = '{{ static_base_url }}'
  return entry


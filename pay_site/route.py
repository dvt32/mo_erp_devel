import sys
sys.path.append('/var/www/sites/{{ pay_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ pay_fqdn }}/lib')
from flask import Flask
from flask import request, abort, redirect, url_for, flash, render_template
from flask import g, send_from_directory
import flask_login
import warnings
import psycopg2
import psycopg2.extras
import os
from user import User
import database
import auth
import onetime
import ach
import card
import register
import setpasswd
import postal
import routing
import manage_pmt_meths
import user_settings
import recur
import upcoming
import history
import emails
import log_access
import forms

app = Flask(__name__)
app.secret_key = '{{ pay_flask_secret_key }}'
app.debug = {{ pay_flask_app_debug }}

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
# name of function to run when login_required is triggered and no user is logged in
login_manager.login_view = 'login'


@app.context_processor
def define_static_url():
    return dict(static_base_url='{{ static_base_url }}')

@app.before_request
def before_request():
  g.user = flask_login.current_user

@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@login_manager.user_loader
def user_loader(id):
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM cust_pers_tbl WHERE id = %s', (id,))
  cust_pers = curdict.fetchone()
  if not cust_pers:
    return
  user = User()
  user.id = id
  user.cust_acct_id = cust_pers['cust_acct_id']
  user.email = cust_pers['email'].strip()
  return user

@app.route('/recurring_payments')
@app.route('/')
@flask_login.login_required
def show_recur():
  return recur.show_recur()

@app.route('/favicon.ico')
def favicon():
  return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/classic')
@flask_login.login_required
def classic():
  return onetime.show_index_classic()

@app.route('/login', methods=['GET', 'POST'])
def login():
  return auth.login()

@app.route('/logout')
@flask_login.login_required
def logout():
  log_access.log_page()
  flask_login.logout_user()
  return render_template('logout.html')

@app.route('/register', methods=['GET', 'POST'])
def route_register():
  if request.method == 'GET':
    return register.register_input()
  else:
    return register.register_thanks()

@app.route('/instrument/<action>')
def instrument(action):
  return "OK"

@app.route('/instrument/login_attempt/<email>')
def login_attempt_log(email):
  return "OK"

@app.route('/confirm_email/<token>')
def confirm_email(token):
  return setpasswd.setpasswd_input(token)

@app.route('/setpasswd', methods=['POST'])
def route_setpasswd():
  return setpasswd.setpasswd_result()

@app.route('/onetime')
@flask_login.login_required
def show_onetime():
  return onetime.show_onetime()

@app.route('/process_pmt', methods=['POST'])
@flask_login.login_required
def process_pmt():
  return onetime.process_pmt()

@app.route('/ach_form')
@flask_login.login_required
def ach_form():
  return ach.show_ach_form()

@app.route('/classic/process_ach_pmt', methods=['POST'])
@flask_login.login_required
def process_ach_pmt_classic():
  return ach.process_ach(classic=True)

@app.route('/card_form')
@flask_login.login_required
def card_form():
  return card.show_card_form()

@app.route('/classic/process_card_pmt', methods=['POST'])
@flask_login.login_required
def process_card_pmt_classic():
  return card.process_card(classic=True)

@app.route('/zip_check', methods=['POST'])
@flask_login.login_required
def zip_check():
  return postal.zip_check()

@app.route('/routing_check', methods=['POST'])
@flask_login.login_required
def routing_check():
  return routing.routing_check()

@app.route('/manage_pmt_meth')
@flask_login.login_required
def manage_pmt_meth():
  return manage_pmt_meths.show_pmt_meth()

@app.route('/add_card', methods=['POST'])
@flask_login.login_required
def add_card():
  return manage_pmt_meths.add_card()

@app.route('/add_ach', methods=['POST'])
@flask_login.login_required
def add_ach():
  return manage_pmt_meths.add_ach()

@app.route('/edit_card')
@flask_login.login_required
def edit_card():
  return manage_pmt_meths.get_card_edit()

@app.route('/update_card', methods=['POST'])
@flask_login.login_required
def update_card():
  return manage_pmt_meths.update_card()

@app.route('/edit_ach')
@flask_login.login_required
def edit_ach():
  return manage_pmt_meths.get_ach_edit()

@app.route('/update_ach', methods=['POST'])
@flask_login.login_required
def update_ach():
  return manage_pmt_meths.update_ach()

@app.route('/delete_card', methods=['POST'])
@flask_login.login_required
def delete_card():
  return manage_pmt_meths.delete_card()

@app.route('/delete_ach', methods=['POST'])
@flask_login.login_required
def delete_ach():
  return manage_pmt_meths.delete_ach()

@app.route('/new_order', methods=['POST'])
@flask_login.login_required
def new_order():
  return manage_pmt_meths.new_order()

@app.route('/user_settings')
@flask_login.login_required
def show_settings():
  return user_settings.show_settings()

@app.route('/new_email', methods=['POST'])
@flask_login.login_required
def new_email():
  return user_settings.new_email()

@app.route('/set_email/<token>')
@flask_login.login_required
def set_email(token):
  return user_settings.set_email(token)

@app.route('/change_password', methods=['POST'])
@flask_login.login_required
def change_password():
  return user_settings.change_password()

@app.route('/add_recur', methods=['POST'])
@flask_login.login_required
def add_recur():
  return recur.add_recur()

@app.route('/edit_recur')
@flask_login.login_required
def edit_recur():
  return recur.get_recur_edit()

@app.route('/update_recur', methods=['POST'])
@flask_login.login_required
def update_recur():
  return recur.update_recur()

@app.route('/delete_recur', methods=['POST'])
@flask_login.login_required
def delete_recur():
  return recur.delete_recur()

@app.route('/upcoming')
@flask_login.login_required
def show_upcoming():
  return upcoming.show_upcoming()

@app.route('/delete_upcoming', methods=['POST'])
@flask_login.login_required
def delete_upcoming():
  return upcoming.delete_upcoming()

@app.route('/edit_upcoming')
@flask_login.login_required
def edit_upcoming():
  return upcoming.get_upcoming_edit()

@app.route('/update_upcoming', methods=['POST'])
@flask_login.login_required
def update_upcoming():
  return upcoming.update_upcoming()

@app.route('/forgot_password')
def forgot_password():
  return register.forgot_password()

@app.route('/password_email', methods=['POST'])
def password_email():
  return register.password_email()

@app.route('/reset_password/<token>')
def reset_password(token):
  return setpasswd.setpasswd_input(token)

@app.route('/history')
@flask_login.login_required
def show_history():
  return history.show_history()

@app.route('/view_email')
@flask_login.login_required
def view_email():
  return emails.view_email()

@app.route('/check_confirmed_ach', methods=['POST'])
@flask_login.login_required
def check_confirm():
  return manage_pmt_meths.check_confirm()

@app.route('/send_confirm_ach', methods=['POST'])
@flask_login.login_required
def send_confirm_ach():
  return manage_pmt_meths.send_confirm_ach()

@app.route('/verify_ach', methods=['POST'])
@flask_login.login_required
def verify_ach():
  return manage_pmt_meths.verify_ach()

# AJAX call routes
@app.route('/saved_pmts', methods=['POST'])
@flask_login.login_required
def get_saved_pmts():
  return forms.get_saved_pmts()
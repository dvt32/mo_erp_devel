Dear Member,

This email is being sent to reset your password. Please visit {{ pay_base_url }}/reset_password/{{ token }} to reset the password for your account.

Sincerely,
The Micro Office Team
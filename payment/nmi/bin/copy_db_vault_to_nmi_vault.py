#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import re
import solupay
import decrypt
import pprint

def main(argv):
  argparser = argparse.ArgumentParser(description='Copy payment method (bank account or credit card) from Micro Office database vault to NMI Solupay vault.')
  argparser.add_argument('pmt_vault_id', help='ID value of row in pmt_vault_vw', type=int)
  argparser.add_argument('--username', help='Normally NMI/Solupay username is retrieved from database, but can override it here')
  args = argparser.parse_args()
  conn = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE id = %s', (args.pmt_vault_id,))
  pmt_vault = curdict.fetchone()
  if not pmt_vault:
    conn.close()
    raise Exception('pmt_vault_vw lacks row with ID ' + str(args.pmt_vault_id))
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (pmt_vault['cust_acct_id'],))
  cust_acct = curdict.fetchone()
  if not cust_acct:
    conn.close()
    raise Exception('cust_acct_tbl lacks row with ID ' + pmt_vault['cust_acct_id'])
  if args.username:
    nmi_username = args.username
  else:
    curdict.execute('SELECT * FROM floor_tbl WHERE id = %s', (cust_acct['primary_floor_id'],))
    floor = curdict.fetchone()
    if floor == None:
      conn.close()
      raise Exception('floor_tbl lacks row with ID ' + cust_acct['primary_floor_id'])
    curdict.execute('SELECT * FROM building_tbl WHERE id = %s', (floor['building_id'],))
    building = curdict.fetchone()
    if building == None:
      conn.close()
      raise Exception('building_tbl lacks row with ID ' + floor['building_id'])
    nmi_username = building['nmi_username']
    if nmi_username == None or nmi_username == '':
      conn.close()
      raise Exception('building with row ID ' + building['id'] + ' lacks value for nmi_username column')
  pmt_gateway = solupay.Gateway(nmi_username)
  decrypt_client = decrypt.DecryptClient()
  decrypted_row = decrypt_client.decrypt_row('pmt_vault_vw', pmt_vault['id'], 'POST')
  if pmt_vault['pmt_method'] == 'ach':
    # ACH
    http_txn_dict = pmt_gateway.set_customer_ach(cust_acct['id'], pmt_vault['name'], decrypted_row['routing_number'], decrypted_row['account_number'], pmt_vault['owned_by'], pmt_vault['account_type'])
    query = http_txn_dict['query']
    # mask sensitive info
    query = re.sub(r'checkaba=([^&]*)', r'checkaba=***', query)
    query = re.sub(r'checkaccount=([^&]*)', r'checkaccount=***', query)
    query = re.sub(r'password=([^&]*)', r'password=***', query)
    curdict.execute('INSERT INTO nmi_vault_update_tbl (request, response, checkaba, checkaccount) VALUES (%s, %s, %s, %s)', (query, http_txn_dict['response_body'], decrypted_row['routing_number'], decrypted_row['account_number']))
    print('Copied entry from our vault to NMI vault.')
    print('-' * 72)
    print('pmt_vault_id: ' + str(pmt_vault['id']) + ', copied to NMI vault ID: ' + str(cust_acct['id']))
    print('Customer: ' + cust_acct['name'] + ' (cust_acct_id: ' + str(cust_acct['id']) + ')')
    print('Account type: ' + pmt_vault['account_type'] + ', Owned by: ' + pmt_vault['owned_by'])
    print('Routing number: ' + pmt_vault['routing_number'] + ', Account number: ' + pmt_vault['account_number'])
  elif pmt_vault['pmt_method'] == 'card':
    # credit card
    match = re.match(r'(\d\d)(\d\d)-(\d\d)-(\d\d)', str(pmt_vault['expire_date']))
    exp_date = match.group(3) + match.group(2)
    http_txn_dict = pmt_gateway.set_customer_card(cust_acct['id'], decrypted_row['card_number'], exp_date, decrypted_row['cvv'], pmt_vault['billing_first_name'], pmt_vault['billing_last_name'], pmt_vault['billing_company'], pmt_vault['billing_address_1'], pmt_vault['billing_address_2'], pmt_vault['billing_city'], pmt_vault['billing_state'], pmt_vault['billing_zip_code'], pmt_vault['billing_country'], pmt_vault['phone_number'], '', pmt_vault['email_address'])
    query = http_txn_dict['query']
    # mask sensitive info
    query = re.sub(r'ccnumber=([^&]*)', r'ccnumber=***', query)
    query = re.sub(r'cvv=([^&]*)', r'cvv=***', query)
    query = re.sub(r'&password=([^&]*)', r'&password=***', query)
    curdict.execute('INSERT INTO nmi_vault_update_tbl (request, response, ccnumber, cvv) VALUES (%s, %s, %s, %s)', (query, http_txn_dict['response_body'], decrypted_row['card_number'], decrypted_row['cvv']))
    print('Copied entry from our vault to NMI vault.')
    print('-' * 72)
    print('pmt_vault_id: ' + str(pmt_vault['id']) + ', copied to NMI vault ID: ' + str(cust_acct['id']))
    print('Customer: ' + cust_acct['name'] + ' (cust_acct_id: ' + str(cust_acct['id']) + ')')
    print('Card number: ' + pmt_vault['card_number'] + ', Expiration date: ' + pmt_vault['expire_date'].strftime('%m/%y'))
    print('Cardholder Name: ' + pmt_vault['billing_first_name'] + ' ' + pmt_vault['billing_last_name'])
    print('Billing address: ' + pmt_vault['billing_address_1'])
    if pmt_vault['billing_address_2']:
      print('Billing address 2: ' + pmt_vault['billing_address_2'])
    csz = pmt_vault['billing_city'] + ', '
    if pmt_vault['billing_state']:
      csz += pmt_vault['billing_state'] + ' '
    if pmt_vault['billing_zip_code']:
      csz += pmt_vault['billing_zip_code'] + ' '
    csz += pmt_vault['billing_country']
    print('Billing City State Zip Country: ' + csz)
  else:
    raise Exception('pmt_method is neither ach nor card in pmt_vault_vw row ID ' + pmt_vault['id'])
  conn.commit()
  conn.close()
  print('Query: ' + query)
  print('NMI response:')
  pprint.pprint(http_txn_dict['response_dict'])
  print('-' * 72)

if __name__ == "__main__":
  main(sys.argv[1:])

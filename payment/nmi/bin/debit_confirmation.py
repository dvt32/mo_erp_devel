#!{{ payment_deploy_basedir }}/venv/bin/python3
import psycopg2.extras
import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import debit_confirm




def main():
  conn = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SELECT * FROM pmt_method_confirm_tbl WHERE id in (SELECT DISTINCT pmt_method_confirm_tbl.id FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw, pmt_vault_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_nmi_txn_vw.amount < 0::money AND (pmt_nmi_txn_vw.request_posted_ts < (now() - interval '{{pmt_confirm_window}} days') OR pmt_vault_vw.confirmed_ts IS NOT NULL))")
  confirm = [dict(row) for row in curdict]
  for row in confirm:
    debit_confirm.debit_back_confirmation_amounts(row['pmt_vault_id'])
  conn.close()

if __name__ == "__main__":
  main()
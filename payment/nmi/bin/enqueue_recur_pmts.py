#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import re
import datetime
import pytz
import pprint
import collections

def main():
  argparser = argparse.ArgumentParser(description='Enqueue transactions from pmt_recur_* tables based on criteria.')
  argparser.add_argument('--id', help='ID value of row in pmt_recur_vw', type=int)
  argparser.add_argument('--start_ts', help='Start timestamp of range in UTC. Defaults to now in UTC if unspecified. Format: YYYYMMDD[hh[mm[ss]]].', type=valid_ts)
  argparser.add_argument('--end_ts', help='End timestamp of range in UTC. Defaults to start_ts + (24 hours - 1 second) if unspecified. Format: YYYYMMDD[hh[mm[ss]]].', type=valid_ts)
  argparser.add_argument('--allow_duplicates', help='Allow all duplicate transactions on same day, just manually entered ones (i.e. pmt_recur_id is NULL), or no duplicate transactions. Default is manual.', choices=('all', 'manual', 'none'), default='manual')
  argparser.add_argument('--ignore_hold', help='Ignore hold flag on pmt_recur_* rows and enqueue transactions even if hold is set to true. Default is false.', action='store_true')
  argparser.add_argument('--dry_run', help='Just do dry run; do not actually enqueue transactions.', action='store_true')
  argparser.add_argument('--verbose', help='Show messages as we enqueue transactions.', action='store_true')
  argparser.add_argument('--format', help='Output format. Default is json.', choices=('json','table'), default='json')
  args = argparser.parse_args()

  # define start_ts and end_ts from command line arguments or computed values.
  # start_ts defaults to now (in UTC). end_ts defaults to start_ts + 1 day - 1 second (also in UTC).
  utc_tz = pytz.timezone('UTC')
  if args.start_ts:
    start_ts = utc_tz.localize(args.start_ts)
  else:
    start_ts = utc_tz.localize(datetime.datetime.utcnow())
  if args.end_ts:
    end_ts = utc_tz.localize(args.end_ts)
  else:
    end_ts = start_ts + datetime.timedelta(days=1) - datetime.timedelta(seconds=1)

  # load pmt_vault_txn_vw rows to handle duplicates (if allow_duplications option is not 'all').
  # keys in dictionary are in format 'YYYY-MM-DD:xx' where xx is the cust_acct_id.
  dup_pmt_vault_txn_by_date_cust_acct_id = {}

  conn = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  if args.allow_duplicates == 'all':
    sql_query = None
  elif args.allow_duplicates == 'manual':
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE scheduled_ts_utc BETWEEN %s AND %s AND pmt_recur_id IS NULL', (start_ts, end_ts))
    for pmt_vault_txn in curdict:
      dup_pmt_vault_txn_by_date_cust_acct_id[str(pmt_vault_txn['scheduled_date']) + ':' + str(pmt_vault_txn['cust_acct_id'])] = pmt_vault_txn
  elif args.allow_duplicates == 'none':
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE scheduled_ts_utc BETWEEN %s AND %s', (start_ts, end_ts))
    for pmt_vault_txn in curdict:
      dup_pmt_vault_txn_by_date_cust_acct_id[str(pmt_vault_txn['scheduled_date']) + ':' + str(pmt_vault_txn['cust_acct_id'])] = pmt_vault_txn

  # create temporary tables needed for pmt_recur_vw query.
  curdict.execute('CREATE TEMPORARY TABLE used_timezone_tbl AS SELECT DISTINCT building_timezone FROM pmt_recur_vw')
  curdict.execute('CREATE TEMPORARY TABLE date_range_tbl AS SELECT DISTINCT * FROM (SELECT generate_series(%s::timestamp with time zone at time zone building_timezone, %s::timestamp with time zone at time zone building_timezone, %s)::date AS date_local, building_timezone FROM used_timezone_tbl UNION SELECT (%s::timestamp with time zone at time zone building_timezone)::date, building_timezone FROM used_timezone_tbl) AS t1', (start_ts, end_ts, '1 day', end_ts))
  # fetch pmt_recur_vw rows for dates in the range of dates
  sql_where_list = []
  sql_param_list = []
  # add parameters from ON clause
  sql_param_list.append('day')
  # WHERE clause components
  if args.id:
    sql_where_list.append('pmt_recur_vw.id = %s')
    sql_param_list.append(args.id)
  sql_where_list.append('(pmt_recur_vw.start_date IS NULL OR pmt_recur_vw.start_date <= date_range_tbl.date_local)')
  sql_where_list.append('(pmt_recur_vw.end_date IS NULL OR pmt_recur_vw.end_date >= date_range_tbl.date_local)')
  sql_where_list.append('pmt_recur_vw.is_active')
  if not args.ignore_hold:
    sql_where_list.append('NOT pmt_recur_vw.hold')
  sql_query = 'SELECT pmt_recur_vw.*, date_range_tbl.date_local FROM pmt_recur_vw JOIN date_range_tbl ON (pmt_recur_vw.building_timezone = date_range_tbl.building_timezone AND pmt_recur_vw.day_of_month = date_part(%s, date_range_tbl.date_local)) WHERE ' + ' AND '.join(sql_where_list) + ' ORDER BY pmt_recur_vw.id'
  sql_param_tuple = tuple(sql_param_list)
  curdict.execute(sql_query, sql_param_tuple)
  if args.verbose:
    print('start_ts: {}, end_ts: {}'.format(start_ts, end_ts))
    if args.format == 'table':
      print('{:>5} {:^10} {:4} {:20} {:^5} {:40.40} {}'.format('ID', 'Date', 'Meth', 'Floor', 'CstID', 'Name', 'Email'))
    print('-'*120)
  num_added_rows = 0

  # keep track of duplicate rows so we can report and allow operator to fix problems.
  dup_pmt_vault_txn_list = []

  # keep track of held pmt_vault_txn rows so that we can report the rows that we refrained from inserting (duplicate detection) AND
  # were on hold. Those rows require operator intervention to avoid missing a charge.
  held_pmt_vault_txn_list = []

  insert_curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  output_dict = collections.OrderedDict()
  for pmt_recur in curdict:
    dup_pmt_vault_txn = dup_pmt_vault_txn_by_date_cust_acct_id.get(str(pmt_recur['date_local']) + ':' + str(pmt_recur['cust_acct_id']))
    if dup_pmt_vault_txn:
      dup_pmt_vault_txn_list.append(dup_pmt_vault_txn)
      if dup_pmt_vault_txn['hold']:
        held_pmt_vault_txn_list.append(dup_pmt_vault_txn)
      continue
    if not args.dry_run:
      insert_curdict.execute('INSERT INTO pmt_vault_txn_tbl (pmt_vault_id, amount, scheduled_date, processor, email, pmt_recur_id) VALUES (%s, %s, %s, %s, %s, %s)', (pmt_recur['pmt_vault_id'], '0.01', pmt_recur['date_local'], pmt_recur['processor'], pmt_recur['email'], pmt_recur['id']))
      conn.commit()
    num_added_rows += 1
    if args.verbose:
      if args.format == 'json':
        output_dict.clear()
        for key in ['id', 'date_local', 'pmt_method', 'floor_label', 'cust_acct_id', 'name', 'email']:
          output_dict[key] = pmt_recur[key]
        pprint.pprint(output_dict)
        print('-'*120)
      elif args.format == 'table':
        print('{:>5} {:10} {:4} {:20} {:>5} {:40.40} {}'.format(str(pmt_recur['id']), str(pmt_recur['date_local']), pmt_recur['pmt_method'], pmt_recur['floor_label'], str(pmt_recur['cust_acct_id']), pmt_recur['name'], pmt_recur['email']))
  if args.verbose:
    if args.dry_run:
      print('{} row(s) would be added to pmt_vault_txn_tbl (but not added because of --dry_run flag)'.format(num_added_rows))
    else:
      print('{} row(s) added to pmt_vault_txn_tbl'.format(num_added_rows))
    if len(dup_pmt_vault_txn_list) > 0:
      print('-'*120)
      print(str(len(dup_pmt_vault_txn_list)) + ' duplicate pmt_vault_txn_tbl rows (not added):')
      if args.format == 'json':
        for pmt_vault_txn in dup_pmt_vault_txn_list:
          print('id {}, vault id {}, {}, {}, cst id {}, name {}, amount {}, {}'.format(pmt_vault_txn['id'], pmt_vault_txn['pmt_vault_id'], pmt_vault_txn['pmt_method'], pmt_vault_txn['floor_label'], pmt_vault_txn['cust_acct_id'], pmt_vault_txn['name'], pmt_vault_txn['amount'], pmt_vault_txn['scheduled_date']))
      elif args.format == 'table':
        print('{:>5} {:>5} {:4} {:20} {:>5} {:40.40} {:>10} {:10}'.format('ID', 'VltID', 'Meth', 'Floor', 'CstID', 'Name', 'Amount', 'Date'))
        for pmt_vault_txn in dup_pmt_vault_txn_list:
          print('{:>5} {:>5} {:4} {:20} {:>5} {:40.40} {:>10} {:10}'.format(pmt_vault_txn['id'], pmt_vault_txn['pmt_vault_id'], pmt_vault_txn['pmt_method'], pmt_vault_txn['floor_label'], pmt_vault_txn['cust_acct_id'], pmt_vault_txn['name'], pmt_vault_txn['amount'], str(pmt_vault_txn['scheduled_date'])))
    if len(held_pmt_vault_txn_list) > 0:
      print('-'*120)
      print(str(len(held_pmt_vault_txn_list)) + ' held pmt_vault_txn_tbl rows that were also duplicates (possibly requires action):')
      if args.format == 'json':
        for pmt_vault_txn in held_pmt_vault_txn_list:
          print('id {}, vault id {}, {}, {}, cst id {}, name {}, amount {}, {}'.format(pmt_vault_txn['id'], pmt_vault_txn['pmt_vault_id'], pmt_vault_txn['pmt_method'], pmt_vault_txn['floor_label'], pmt_vault_txn['cust_acct_id'], pmt_vault_txn['name'], pmt_vault_txn['amount'], pmt_vault_txn['scheduled_date']))
      elif args.format == 'table':
        print('{:>5} {:>5} {:4} {:20} {:>5} {:40.40} {:>10} {:10}'.format('ID', 'VltID', 'Meth', 'Floor', 'CstID', 'Name', 'Amount', 'Date'))
        for pmt_vault_txn in held_pmt_vault_txn_list:
          print('{:>5} {:>5} {:4} {:20} {:>5} {:40.40} {:>10} {:10}'.format(pmt_vault_txn['id'], pmt_vault_txn['pmt_vault_id'], pmt_vault_txn['pmt_method'], pmt_vault_txn['floor_label'], pmt_vault_txn['cust_acct_id'], pmt_vault_txn['name'], pmt_vault_txn['amount'], str(pmt_vault_txn['scheduled_date'])))
  curdict.close()
  insert_curdict.close()
  conn.close()

def valid_ts(s):
  if re.match(r'^\d{8}$', s):
    return datetime.datetime.strptime(s, '%Y%m%d')
  elif re.match(r'^\d{10}$', s):
    return datetime.datetime.strptime(s, '%Y%m%d%H')
  elif re.match(r'^\d{12}$', s):
    return datetime.datetime.strptime(s, '%Y%m%d%H%M')
  elif re.match(r'^\d{14}$', s):
    return datetime.datetime.strptime(s, '%Y%m%d%H%M%S')
  else:
    msg = 'Enter timestamp in YYYYMMDD, YYMMDDhh, YYMMDDhhmm, or YYYYMMDDhhmmss format; you entered {}'.format(s)
    raise argparse.ArgumentTypeError(msg)

if __name__ == "__main__":
  main()

#!{{ payment_deploy_basedir }}/venv/bin/python3
import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import jinja2
import re
import chargefacade
import dbutil

# how many hours ACH transactions are delayed.
# used to compare to scheduled_ts_utc and determine when we can process transactions.
ach_delay_hours = {{ pay_ach_delay_hours }}

def main():
  argparser = argparse.ArgumentParser(description='Send email confirmation of success or failure of an NMI/Solupay payment transaction.')
  argparser.add_argument('pmt_nmi_txn_id', help='ID value of row in pmt_nmi_txn_vw', type=int)
  args = argparser.parse_args()
  conn = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_nmi_txn_vw WHERE id = %s', (args.pmt_nmi_txn_id,))
  pmt_nmi_txn = curdict.fetchone()
  if not pmt_nmi_txn:
    conn.close()
    raise Exception('pmt_nmi_txn_vw lacks row with ID ' + str(args.pmt_nmi_txn_id))
  if pmt_nmi_txn['email']:
    # If subject lines need Jinja variables at run time, use:
    # .replace('[[', '{''{').replace(']]', '}''}')
    # That eliminates conflict between Jinja during build and Jinja during runtime.
    ach_succ_subj_tmpl = 'Processed ACH/bank payment to Micro Office'
    ach_succ_subj_tmpl = jinja2.Template(ach_succ_subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    ach_fail_subj_tmpl = 'ACTION REQUIRED - failed ACH/bank payment to Micro Office'
    ach_fail_subj_tmpl = jinja2.Template(ach_fail_subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    card_succ_subj_tmpl = 'Successful card payment to Micro Office'
    card_succ_subj_tmpl = jinja2.Template(card_succ_subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    card_fail_subj_tmpl = 'ACTION REQUIRED - failed card payment to Micro Office'
    card_fail_subj_tmpl = jinja2.Template(card_fail_subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/ach_success_tmpl.txt', 'r') as tmpl_file:
      ach_succ_text_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/ach_success_tmpl.html', 'r') as tmpl_file:
      ach_succ_html_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/ach_failure_tmpl.txt', 'r') as tmpl_file:
      ach_fail_text_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/ach_failure_tmpl.html', 'r') as tmpl_file:
      ach_fail_html_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/card_success_tmpl.txt', 'r') as tmpl_file:
      card_succ_text_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/card_success_tmpl.html', 'r') as tmpl_file:
      card_succ_html_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/card_failure_tmpl.txt', 'r') as tmpl_file:
      card_fail_text_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    with open('{{ payment_deploy_basedir }}/share/card_failure_tmpl.html', 'r') as tmpl_file:
      card_fail_html_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    mail_subject = None
    mail_text_body = None
    property_dict = dict(pmt_nmi_txn)
    property_dict['cust_acct_name'] = pmt_nmi_txn['name']
    property_dict['pay_base_url'] = '{{ pay_base_url }}'
    property_dict['static_base_url'] = '{{ static_base_url }}'
    curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
    email_id = curdict.fetchone()['next_id']
    property_dict['email_id'] = email_id
    if pmt_nmi_txn['pmt_method'] == 'ach':
      property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
      if pmt_nmi_txn['response'] == '1':
        if ach_succ_text_tmpl:
          mail_subject = ach_succ_subj_tmpl.render(property_dict)
          mail_text_body = ach_succ_text_tmpl.render(property_dict)
        if ach_succ_html_tmpl:
          mail_html_body = ach_succ_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
      else:
        if ach_fail_text_tmpl:
          mail_subject = ach_fail_subj_tmpl.render(property_dict)
          mail_text_body = ach_fail_text_tmpl.render(property_dict)
        if ach_fail_html_tmpl:
          mail_html_body = ach_fail_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
    elif pmt_nmi_txn['pmt_method'] == 'card':
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(property_dict['expire_date']));
      property_dict['expire_mmyyyy'] = match.group(2) + '/' + match.group(1)
      property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
      property_dict['convenience_fee_dec'] = dbutil.cast_money_to_decimal(property_dict['convenience_fee'])
      property_dict['prefee_amount_dec'] = property_dict['amount_dec'] - property_dict['convenience_fee_dec']
      property_dict['convenience_pct'] = '{{ pay_card_convenience_pct }}'
      if pmt_nmi_txn['response'] == '1':
        if card_succ_text_tmpl:
          mail_subject = card_succ_subj_tmpl.render(property_dict)
          mail_text_body = card_succ_text_tmpl.render(property_dict)
        if card_succ_html_tmpl:
          mail_html_body = card_succ_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
      else:
        if card_fail_text_tmpl:
          mail_subject = card_fail_subj_tmpl.render(property_dict)
          mail_text_body = card_fail_text_tmpl.render(property_dict)
        if card_fail_html_tmpl:
          mail_html_body = card_fail_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
    else:
      raise Exception('only know how to deal with pmt_method ach and card; pmt_method was %s for ID %s', (pmt_nmi_txn['pmt_method'], pmt_nmi_txn['id']))
    if mail_subject and mail_text_body:
      chargefacade.ChargeMail.send_html_mail(pmt_nmi_txn['email'], '{{ payment_mail_bcc }}', mail_subject, mail_text_body, mail_html_body, email_id, pmt_nmi_txn['cust_acct_id'], conn)

if __name__ == "__main__":
  main()

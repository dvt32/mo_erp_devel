#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import solupay
import re

def main():
  argparser = argparse.ArgumentParser(description='Query transactions using NMI Solupay reporting engine.')
  nmi_username_list = ', '.join(solupay.Gateway.nmi_credential_dict.keys())
  argparser.add_argument('nmi_username', help='NMI username to use: {}'.format(nmi_username_list))
  argparser.add_argument('--condition', help='A combination of values listed below can be passed and should be separated by commas. For example, to retrieve all transactions pending settlement or complete, the following could be used: condition=pendingsettlement,complete. pending: "Auth Only" transactions that are awaiting capture. pendingsettlement: This transaction is awaiting settlement. failed: This transaction has failed. canceled: This transaction has been voided. complete: This transaction has settled. unknown: An unknown error was encountered while processing this transaction.', choices=('pending','pendingsettlement','failed','canceled','complete','unknown'))
  argparser.add_argument('--transaction_type', help='Retrieves only transactions with the specified transaction type. Use one of the following to specify payment type: cc: A credit card transaction. ck: A check transaction.', choices=('cc','ck'))
  argparser.add_argument('--action_type', help='Retrieves only transactions specified action types. A combination of the values can be used and should be separated by commas. For example, to retrieve all transactions with credit or refund actions, use the following: action_type=refund,credit. sale: Sale transactions. refund: Refund transactions. credit: Credit transactions. auth: "Auth Only" transactions. capture: Captured transactions. void: Voided transactions. return: Electronic Check (ACH) transactions that have returned before (return) or after settlement (late_return).', choices=('sale','refund','credit','auth','capture','void','return'))
  argparser.add_argument('--transaction_id', help='Specify a transaction ID or a comma separated list of transaction IDs to retrieve information on.')
  argparser.add_argument('--order_id', help='Retrieves only transactions with the specified Order ID.')
  argparser.add_argument('--last_name', help='Retrieves only transactions with the specified last name.')
  argparser.add_argument('--email', help='Retrieves only transactions with the specified billing email address.')
  argparser.add_argument('--cc_number', help='Retrieves only transactions with the specified credit card number. You can use either the full number or the last 4 digits of the credit card number.')
  argparser.add_argument('--merchant_defined_field_1', help='Retrieves only transactions with the specified merchant defined field value. Replace the "#" with a field number (1-20) (Example: merchant_defined_field_12=value)')
  argparser.add_argument('--start_date', help='Only transactions that have been modified on or after this date will be retrieved. Note that any actions performed on a transaction will cause the modified date to be updated. Format: YYYYMMDDhhmmss', type=valid_date)
  argparser.add_argument('--end_date', help='Only transactions that have been modified on or before this date will be retrieved. Note that any actions performed on a transaction will cause the modified date to be updated. Format: YYYYMMDDhhmmss', type=valid_date)
  argparser.add_argument('--report_type', help='Allows customer vault information or a html receipt to be returned. Omit report_type if you want the XML data for one or more transactions based on other query criteria. If you would like to query the Customer Vault to view what customer information is stored in the Customer Vault, you must set report_type to customer_vault and you can set customer_vault_id. If you omit customer_vault_id, the system will return all customers that are stored in the vault. If you include a customer_vault_id, it will return the customer record associated with that ID. Example: report_type=customer_vault&customer_vault_id=123456789. receipt: Will return an html receipt for a specified transaction id. customer_vault: Set the Query API to return Customer Vault data. (omitted): return complete XML for one or more transactions.', choices=('receipt','customer_vault'))
  argparser.add_argument('--mobile_device_license', help='Retrieves only transactions processed using the specified mobile device. The device IDs for this parameter are available in the License Manager. Use "any_mobile" to retrieve all mobile transactions. A combination of the values can be used and should be separated by commas. Can not be used with "mobile_device_nickname". Example 1: mobile_device_license=D91AC56A-4242-3131-2323-2AE4AA6DB6EB. Example 2: mobile_device_license=any_mobile')
  argparser.add_argument('--mobile_device_nickname', help='Retrieves only transactions processed using mobile devices with the specified nickname. The nicknames for this parameter are available in the License Manager. Can not be used with "mobile_device_license". Example (URL encoded): mobile_device_nickname=Jims%%20iPhone')
  argparser.add_argument('--customer_vault_id', help='Set a specific Customer Vault record. Should only be used when report_type=customer_vault.')
  args = argparser.parse_args()
  pmt_gateway = solupay.Gateway(args.nmi_username)
  http_txn_dict = pmt_gateway.run_query(args.condition, args.transaction_type, args.action_type, args.transaction_id, args.order_id, args.last_name, args.email, args.cc_number, args.merchant_defined_field_1, args.start_date, args.end_date, args.report_type, args.mobile_device_license, args.mobile_device_nickname, args.customer_vault_id)
  query = http_txn_dict['query']
  # mask sensitive info
  query = re.sub(r'password=([^&]*)', r'password=***', query)
  print('Query: ' + query)
  print('NMI response:')
  print(http_txn_dict['response_body'])
  print('-' * 72)

def valid_date(s):
  if re.match(r'^\d{8}$', s) or re.match(r'^\d{14}$', s):
    return s
  else:
    msg = 'Enter date in YYYYMMDD or YYYYMMDDhhmmss format; you entered {}'.format(s)
    raise argparse.ArgumentTypeError(msg)

if __name__ == "__main__":
  main()

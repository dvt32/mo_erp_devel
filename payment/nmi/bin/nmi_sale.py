#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import decimal
import datetime
import re
import chargefacade
import decrypt

# how many hours ACH transactions are delayed.
# used to compare to scheduled_ts_utc and determine when we can process transactions.
ach_delay_hours = {{ pay_ach_delay_hours }}

def main():
  argparser = argparse.ArgumentParser(description='Process a payment from a customer using the NMI Solupay direct post with credit card or bank account (not through vault).')
  argparser.add_argument('pmt_nmi_txn_id', help='ID value of row in pmt_nmi_txn_vw', type=int)
  argparser.add_argument('--retry', help='Retry a failed sale transaction; aborts if transaction was successful last time', action='store_true')
  argparser.add_argument('--force', help='Retry, even if was successful last time; must specify both --retry and --force', action='store_true')
  argparser.add_argument('--demo', help='Operate in demo mode (transactions are submitted to demo gateway, not production gateway)', action='store_true')
  args = argparser.parse_args()
  conn = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_nmi_txn_vw WHERE id = %s', (args.pmt_nmi_txn_id,))
  pmt_nmi_txn = curdict.fetchone()
  if not pmt_nmi_txn:
    conn.close()
    raise Exception('pmt_nmi_txn_vw lacks row with ID ' + str(args.pmt_nmi_txn_id))
  if pmt_nmi_txn['scheduled_ts_utc'] > (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(hours=ach_delay_hours)):
    conn.close()
    raise Exception('change scheduled date if you want to process the payment; scheduled date is currently in the future: ' + str(pmt_nmi_txn['scheduled_date']))
  curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (pmt_nmi_txn['cust_acct_id'],))
  cust_acct = curdict.fetchone()
  if not cust_acct:
    conn.close()
    raise Exception('cust_acct_tbl lacks row with ID ' + pmt_nmi_txn['cust_acct_id'])
  decrypt_client = decrypt.DecryptClient()
  decrypted_row = decrypt_client.decrypt_row('pmt_nmi_txn_vw', pmt_nmi_txn['id'])
  is_retry_attempt = False
  nmi_facade = chargefacade.NmiFacade(conn)
  if args.demo:
    nmi_facade.set_demo_mode(True)
  # If subject lines need Jinja variables at run time, use:
  # .replace('[[', '{''{').replace(']]', '}''}')
  # That eliminates conflict between Jinja during build and Jinja during runtime.
  ach_succ_subj_tmpl = 'Processed ACH/bank payment to Micro Office'
  ach_fail_subj_tmpl = 'ACTION REQUIRED - failed ACH/bank payment to Micro Office'
  card_succ_subj_tmpl = 'Successful card payment to Micro Office'
  card_fail_subj_tmpl = 'ACTION REQUIRED - failed card payment to Micro Office'
  with open('{{ payment_deploy_basedir }}/share/ach_success_tmpl.txt', 'r') as tmpl_file:
    ach_succ_text_tmpl = tmpl_file.read()
  with open('{{ payment_deploy_basedir }}/share/ach_success_tmpl.html', 'r') as tmpl_file:
    ach_succ_html_tmpl = tmpl_file.read()
  with open('{{ payment_deploy_basedir }}/share/ach_failure_tmpl.txt', 'r') as tmpl_file:
    ach_fail_text_tmpl = tmpl_file.read()
  with open('{{ payment_deploy_basedir }}/share/ach_failure_tmpl.html', 'r') as tmpl_file:
    ach_fail_html_tmpl = tmpl_file.read()
  with open('{{ payment_deploy_basedir }}/share/card_success_tmpl.txt', 'r') as tmpl_file:
    card_succ_text_tmpl = tmpl_file.read()
  with open('{{ payment_deploy_basedir }}/share/card_success_tmpl.html', 'r') as tmpl_file:
    card_succ_html_tmpl = tmpl_file.read()
  with open('{{ payment_deploy_basedir }}/share/card_failure_tmpl.txt', 'r') as tmpl_file:
    card_fail_body_tmpl = tmpl_file.read()
  with open('{{ payment_deploy_basedir }}/share/card_failure_tmpl.html', 'r') as tmpl_file:
    card_fail_html_tmpl = tmpl_file.read()
  nmi_facade.set_ach_succ_tmpl(ach_succ_subj_tmpl, ach_succ_text_tmpl, ach_succ_html_tmpl)
  nmi_facade.set_ach_fail_tmpl(ach_fail_subj_tmpl, ach_fail_text_tmpl, ach_fail_html_tmpl)
  nmi_facade.set_card_succ_tmpl(card_succ_subj_tmpl, card_succ_text_tmpl, card_succ_html_tmpl)
  nmi_facade.set_card_fail_tmpl(card_fail_subj_tmpl, card_fail_body_tmpl, card_fail_html_tmpl)
  if pmt_nmi_txn['request_posted_ts'] != None or pmt_nmi_txn['response'] != None:
    if not args.retry:
      conn.close()
      raise Exception('Sale transaction row already processed; if you want to process it again, use the --retry option')
    if pmt_nmi_txn['response'] == '1' and not args.force:
      conn.close()
      raise Exception('Sale transaction row already processed and was successful last time; if you really want to process it again (meaning there could be a potential double charge), specify both --retry and --force')
    is_retry_attempt = True
    if pmt_nmi_txn['pmt_method'] == 'ach':
      pmt_nmi_txn = nmi_facade.insert_pmt_nmi_txn_ach(pmt_nmi_txn['cust_acct_id'], pmt_nmi_txn['account_type'], pmt_nmi_txn['owned_by'], pmt_nmi_txn['registered_name'], decrypted_row['routing_number'], decrypted_row['account_number'], pmt_nmi_txn['amount_dec'], pmt_nmi_txn['scheduled_date'], pmt_nmi_txn['note'], pmt_nmi_txn['email'])
    elif pmt_nmi_txn['pmt_method'] == 'card':
      pmt_nmi_txn = nmi_facade.insert_pmt_nmi_txn_card(pmt_nmi_txn['cust_acct_id'], decrypted_row['card_number'], pmt_nmi_txn['expire_date'], decrypted_row['cvv'], pmt_nmi_txn['billing_first_name'], pmt_nmi_txn['billing_last_name'], pmt_nmi_txn['billing_company'], pmt_nmi_txn['billing_address_1'], pmt_nmi_txn['billing_address_2'], pmt_nmi_txn['billing_address_num'], pmt_nmi_txn['billing_city'], pmt_nmi_txn['billing_state'], pmt_nmi_txn['billing_zip_code'], pmt_nmi_txn['billing_country'], pmt_nmi_txn['amount_dec'], pmt_nmi_txn['scheduled_date'], pmt_nmi_txn['note'], pmt_nmi_txn['email'])
    else:
      raise Exception('unknown pmt_method: ' + pmt_nmi_txn['pmt_method'])
    new_pmt_nmi_txn_id = pmt_nmi_txn['id']
    curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_nmi_txn_vw WHERE id = %s', (new_pmt_nmi_txn_id,))
    pmt_nmi_txn = curdict.fetchone()
  result_pmt_nmi_txn = nmi_facade.process_pmt_nmi_txn_row(pmt_nmi_txn)
  print('Attempted to process sale transaction.')
  print('-' * 72)
  if is_retry_attempt:
    print('Retry attempt of previous pmt_nmi_txn_id: ' + str(args.pmt_nmi_txn_id))
  print('pmt_nmi_txn_id: ' + str(pmt_nmi_txn['id']) + ', Payment method: ' + pmt_nmi_txn['pmt_method'] + ', Amount: ' + str(pmt_nmi_txn['amount_dec']))
  print('Customer: ' + cust_acct['name'] + ' (cust_acct_id: ' + str(cust_acct['id']) + ')')
  if pmt_nmi_txn['pmt_method'] == 'ach':
    print('Routing number: ' + pmt_nmi_txn['routing_number'] + ', account number: ' + pmt_nmi_txn['account_number'])
  elif pmt_nmi_txn['pmt_method'] == 'card':
    print('Card number: ' + pmt_nmi_txn['card_number'] + ', expire date: ' + pmt_nmi_txn['expire_date'])
  else:
    raise Exception('unknown pmt_method: ' + pmt_nmi_txn['pmt_method'])
  print('Query: ' + result_pmt_nmi_txn['request_body'])
  print('NMI response:')
  print(result_pmt_nmi_txn['response_body'])
  print('-' * 72)

if __name__ == "__main__":
  main()

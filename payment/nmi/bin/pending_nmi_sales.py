#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import decimal
import datetime
import re
import chargefacade

# how many hours ACH transactions are delayed.
# used to compare to scheduled_ts_utc and determine when we can process transactions.
ach_delay_hours = {{ pay_ach_delay_hours }}

def main():
  argparser = argparse.ArgumentParser(description='Show or process pending transactions in pmt_nmi_txn_vw.')
  argparser.add_argument('--id', help='ID value of row in pmt_nmi_txn_vw', type=int)
  argparser.add_argument('--process', help='Process pending transactions (without this flag, just show pending transactions)', action='store_true')
  argparser.add_argument('--demo', help='Operate in demo mode (transactions are submitted to demo gateway, not production gateway)', action='store_true')
  args = argparser.parse_args()
  conn = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  if args.id:
    curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_nmi_txn_vw WHERE id = %s AND ((pmt_method = \'ach\' AND scheduled_ts_utc <= now() + interval \'%s hours\') OR (pmt_method = \'card\' AND scheduled_ts_utc <= now())) AND hold = false AND request_posted_ts IS NULL AND response IS NULL ORDER BY created_ts', (args.id, ach_delay_hours,))
  else:
    curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_nmi_txn_vw WHERE ((pmt_method = \'ach\' AND scheduled_ts_utc <= now() + interval \'%s hours\') OR (pmt_method = \'card\' AND scheduled_ts_utc <= now())) AND hold = false AND request_posted_ts IS NULL AND response IS NULL ORDER BY created_ts', (ach_delay_hours,))
  num_rows = 0
  subtotal = decimal.Decimal(0)
  if args.process:
    print('Processing pending NMI sale transactions on ' + str(datetime.date.today()) + '.')
    if args.demo:
      print('Operating in demo mode.')
    print('-' * 72)
    num_success = 0
    num_failure = 0
    nmi_facade = chargefacade.NmiFacade(conn)
    if args.demo:
      nmi_facade.set_demo_mode(True)
    # If subject lines need Jinja variables at run time, use:
    # .replace('[[', '{''{').replace(']]', '}''}')
    # That eliminates conflict between Jinja during build and Jinja during runtime.
    ach_succ_subj_tmpl = 'Processed ACH/bank payment to Micro Office'
    ach_fail_subj_tmpl = 'ACTION REQUIRED - failed ACH/bank payment to Micro Office'
    card_succ_subj_tmpl = 'Successful card payment to Micro Office'
    card_fail_subj_tmpl = 'ACTION REQUIRED - failed card payment to Micro Office'
    with open('{{ payment_deploy_basedir }}/share/ach_success_tmpl.txt', 'r') as tmpl_file:
      ach_succ_text_tmpl = tmpl_file.read()
    with open('{{ payment_deploy_basedir }}/share/ach_success_tmpl.html', 'r') as tmpl_file:
      ach_succ_html_tmpl = tmpl_file.read()
    with open('{{ payment_deploy_basedir }}/share/ach_failure_tmpl.txt', 'r') as tmpl_file:
      ach_fail_text_tmpl = tmpl_file.read()
    with open('{{ payment_deploy_basedir }}/share/ach_failure_tmpl.html', 'r') as tmpl_file:
      ach_fail_html_tmpl = tmpl_file.read()
    with open('{{ payment_deploy_basedir }}/share/card_success_tmpl.txt', 'r') as tmpl_file:
      card_succ_text_tmpl = tmpl_file.read()
    with open('{{ payment_deploy_basedir }}/share/card_success_tmpl.html', 'r') as tmpl_file:
      card_succ_html_tmpl = tmpl_file.read()
    with open('{{ payment_deploy_basedir }}/share/card_failure_tmpl.txt', 'r') as tmpl_file:
      card_fail_body_tmpl = tmpl_file.read()
    with open('{{ payment_deploy_basedir }}/share/card_failure_tmpl.html', 'r') as tmpl_file:
      card_fail_html_tmpl = tmpl_file.read()
    nmi_facade.set_ach_succ_tmpl(ach_succ_subj_tmpl, ach_succ_text_tmpl, ach_succ_html_tmpl)
    nmi_facade.set_ach_fail_tmpl(ach_fail_subj_tmpl, ach_fail_text_tmpl, ach_fail_html_tmpl)
    nmi_facade.set_card_succ_tmpl(card_succ_subj_tmpl, card_succ_text_tmpl, card_succ_html_tmpl)
    nmi_facade.set_card_fail_tmpl(card_fail_subj_tmpl, card_fail_body_tmpl, card_fail_html_tmpl)
    for pmt_nmi_txn in curdict:
      num_rows += 1
      result_pmt_nmi_txn = nmi_facade.process_pmt_nmi_txn_row(pmt_nmi_txn)
      if result_pmt_nmi_txn['response'] == '1':
        num_success += 1
        subtotal += pmt_nmi_txn['amount_dec']
      else:
        num_failure += 1
      print('pmt_nmi_txn_id: ' + str(pmt_nmi_txn['id']) + ', Payment method: ' + pmt_nmi_txn['pmt_method'] + ', Amount: ' + str(pmt_nmi_txn['amount_dec']))
      print('Customer: ' + pmt_nmi_txn['name'] + ' (cust_acct_id: ' + str(pmt_nmi_txn['cust_acct_id']) + ')')
      if pmt_nmi_txn['pmt_method'] == 'ach':
        print('Routing number: ' + pmt_nmi_txn['routing_number'] + ', account number: ' + pmt_nmi_txn['account_number'])
      elif pmt_nmi_txn['pmt_method'] == 'card':
        print('Card number: ' + pmt_nmi_txn['card_number'] + ', expire date: ' + str(pmt_nmi_txn['expire_date']))
      else:
        raise Exception('unknown pmt_method: ' + pmt_nmi_txn['pmt_method'])
      print('Query: ' + result_pmt_nmi_txn['request_body'])
      print('NMI response:')
      print(result_pmt_nmi_txn['response_body'])
      print('-' * 72)
    print('Attempted to process ' + str(num_rows) + ' sale transactions')
    print(str(num_success) + ' preliminarily successful transactions totaling $' + str(subtotal))
    print(str(num_failure) + ' failed transactions')
  else:
    print('Pending NMI sale transactions (not in vault) as of ' + str(datetime.date.today()) + ':')
    print('-' * 90)
    print('{:^5} {:^5} {:40.40} {:4}  {:8} {}'.format('PmtID', 'CstID', 'Name', 'Meth', 'Amount', 'Floor'))
    print('-' * 90)
    for pmt_nmi_txn in curdict:
      num_rows += 1
      subtotal += pmt_nmi_txn['amount_dec']
      print('{:>5} {:>5} {:40.40} {:4} ${:>8} {}'.format(str(pmt_nmi_txn['id']), str(pmt_nmi_txn['cust_acct_id']), pmt_nmi_txn['name'], pmt_nmi_txn['pmt_method'], str(pmt_nmi_txn['amount_dec']), pmt_nmi_txn['floor_label']))
    print('-' * 90)
    print(str(num_rows) + ' sale transactions pending totaling $' + str(subtotal))
  conn.close()

if __name__ == "__main__":
  main()

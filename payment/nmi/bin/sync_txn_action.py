#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import re
import datetime
import dateutil.parser
import xml.etree.ElementTree
import solupay
import sync_txn_action_lib

# default number if days in the past if neither days nor start_date are specified.
default_days = 14

def main():
  argparser = argparse.ArgumentParser(description='Synchronize transaction actions that get generated after a transaction has initially returned.')
  nmi_username_list = ', '.join(solupay.Gateway.nmi_credential_dict.keys())
  argparser.add_argument('nmi_username', help='NMI username to use: {}'.format(nmi_username_list))
  argparser.add_argument('--transaction_id', help='Specify a transaction ID or a comma separated list of transaction IDs to retrieve information on.')
  argparser.add_argument('--days', help='Number of days in the past to go back. Mutually exclusive with --start_date. Defaults to 14 if neither --days nor --start_date are specified.', type=int)
  argparser.add_argument('--start_date', help='Only transactions that have been modified on or after this date will be retrieved. Note that any actions performed on a transaction will cause the modified date to be updated. Format: YYYYMMDDhhmmss. Mutually exclusive with --days.', type=valid_date)
  argparser.add_argument('--end_date', help='Only transactions that have been modified on or before this date will be retrieved. Note that any actions performed on a transaction will cause the modified date to be updated. Format: YYYYMMDDhhmmss', type=valid_date)
  argparser.add_argument('--verbose', help='Show status messages as we process transactions and linked actions.', action='store_true')
  argparser.add_argument('--show_old', help='When --verbose is specified, also show already-synced rows.', action='store_true')
  args = argparser.parse_args()
  if args.days and args.start_date:
    raise Exception('--days and --start_date mutually exclusive.')
  if args.start_date:
    start_date = args.start_date
  else:
    if args.days:
      days = args.days
    elif not args.transaction_id:
      # if transaction_id specified and no start_date or days, skip.
      # if no transaction_id specified and no start_date and no days specified, use default days.
      days = default_days
    # compute start_date from days if days is not None
    if days:
      start_date = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=days)
      start_date = start_date.strftime('%Y%m%d')
  # we take three steps to get all the data we need:
  #
  # 1. query Solupay with start_date, end_date, and transaction_id list. Solupay returns transactions and linked actions,
  #    but only those actions that fall within the date range.
  # 2. query pmt_nmi_txn_vw to fetch rows with transactionid values that match what we got from Solupay. We only want details from
  #    Solupay about those rows we have in pmt_nmi_txn_vw (no need to retrieve extraneous information).
  # 3. query Solupay again, just using a transaction_id list as the criteria. This will pull in all information about the
  #    transactions, including all linked actions, not just the ones in the date range.
  #
  pmt_gateway = solupay.Gateway(args.nmi_username)
  # retrieve all transactions and linked actions in specified date range and with desired transaction IDs.
  http_txn_dict = pmt_gateway.run_query(start_date=start_date, end_date=args.end_date, transaction_id=args.transaction_id)
  nm_response_tree = xml.etree.ElementTree.fromstring(http_txn_dict['response_body'])
  # collect transaction IDs from Solupay query.
  transaction_id_list = [transaction.findtext('./transaction_id') for transaction in nm_response_tree.findall('./transaction')]
  if not transaction_id_list:
    print('no transactions in Solupay match the critera.')
    return
  placeholder_str = ', '.join(['%s'] * len(transaction_id_list))
  conn = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_nmi_txn_vw WHERE transactionid IN (' + placeholder_str + ')', tuple(transaction_id_list))
  # generate list and dict of pmt_nmi_txn_vw rows.
  pmt_nmi_txn_list = [pmt_nmi_txn for pmt_nmi_txn in curdict]
  pmt_nmi_txn_by_transaction_id = { pmt_nmi_txn['transactionid']: pmt_nmi_txn for pmt_nmi_txn in pmt_nmi_txn_list }
  # assemble list of transaction IDs that are in pmt_nmi_txn_vw.
  transaction_id_list = [pmt_nmi_txn['transactionid'] for pmt_nmi_txn in pmt_nmi_txn_list]
  if not transaction_id_list:
    print('no rows in pmt_nmi_txn_vw match the criteria.')
    return
  # query Solupay for full transactions. This pulls in actions that might have been skipped if we had just used dates.
  # Thus we get full txn_xml columns for pmt_nmi_txn_vw so updates on that column have the most recent full data.
  http_txn_dict = pmt_gateway.run_query(transaction_id=','.join(transaction_id_list))
  sync_txn_action_lib.sync_txn_actions(http_txn_dict['response_body'], conn, pmt_nmi_txn_by_transaction_id, args.verbose, args.show_old)

def valid_date(s):
  if re.match(r'^\d{8}$', s) or re.match(r'^\d{14}$', s):
    return s
  else:
    msg = 'Enter date in YYYYMMDD or YYYYMMDDhhmmss format; you entered {}'.format(s)
    raise argparse.ArgumentTypeError(msg)

if __name__ == "__main__":
  main()

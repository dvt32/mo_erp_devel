import psycopg2.extras
import datetime
import pycurl
import urllib
import urllib.parse
import io
import re
import jinja2
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import email.utils
import email.header
import pytz
import json
import solupay
import tzconvert
import dbutil
import random
import decimal
import sync_txn_action_lib

class PaymentTransactionNotFoundError(Exception):
  """Exception raised when payment transaction not found."""
  pass

class UnknownPaymentMethodError(Exception):
  """Exception for unknown payment method."""
  pass

class BadPaymentMethodError(Exception):
  """Exception raised when payment transaction is the wrong payment method."""
  pass

class ScheduledFutureError(Exception):
  """Exception raised when scheduled date is in future."""
  pass

class ScheduledPastError(Exception):
  """Exception raised when scheduled date is in past."""
  pass

class HeldError(Exception):
  """Exception raised when payment transaction has hold flag set to True."""
  pass

class AlreadyProcessedError(Exception):
  """Exception raised when payment transaction has already been processed."""
  pass

class ChargeMail():
  """Convenience class to send mail for the chargefacade module.
  """

  def send_text_mail(mail_to, mail_bcc, mail_subject, mail_body, email_id, cust_acct_id, conn, do_commit=True):
    """Send a plain text email.

    Args:
      mail_to: email address of recipient
      mail_subject: subject line of the email
      mail_body: body of the email in plain text
      do_commit (bool): True = perform database commit; False = refrain from commit.
    """
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = '{{ payment_mail_from }}'
    mail_msg['To'] = mail_to
    if mail_bcc:
      mail_msg['Bcc'] = mail_bcc
    mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('INSERT INTO email_paysite_tbl (id, cust_acct_id, from_addr, to_addr, subject, plain_text) VALUES (%s, %s, %s, %s, %s, %s)', (email_id,cust_acct_id, '{{ payment_mail_from }}', mail_to, mail_subject, mail_body))
    conn.commit()

  def send_html_mail(mail_to, mail_bcc, mail_subject, text_body, html_body, email_id, cust_acct_id, conn, do_commit=True):
    """Send an html text email.

    Args:
      mail_to: email address of recipient
      mail_subject: subject line of the email
      text_body: plaintext version of the email body
      html_body: html version of the email body
      email_id: unique id for the email to be inserted into the email_paysite_tbl. gotten from a sequence query before calling this function
      cust_acct_id: cust acct id for email table
      conn: database connection for database insertion
      do_commit (bool): True = perform database commit; False = refrain from commit.
    """
    mail_msg = MIMEMultipart('alternative')
    text = MIMEText(text_body, 'plain')
    html = MIMEText(html_body, 'html')
    mail_msg.attach(text)
    mail_msg.attach(html)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = '{{ payment_mail_from }}'
    mail_msg['To'] = mail_to
    if mail_bcc:
      mail_msg['Bcc'] = mail_bcc
    mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
    if do_commit:
      curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      curdict.execute('INSERT INTO email_paysite_tbl (id, cust_acct_id, from_addr, to_addr, subject, plain_text, html_text) VALUES (%s, %s, %s, %s, %s, %s, %s)', (email_id,cust_acct_id, '{{ payment_mail_from }}', mail_to, mail_subject, text_body, html_body))
      conn.commit()

  def get_pmt_bcc_emails(conn, cust_acct_id):
    """Get string of payment bcc emails. Always returns at least {{ payment_mail_bcc }} but also includes emails listed in floor_tbl

    Args:
      conn: database connection to get pmt_notify_emails
      cust_acct_id: cust acct id to get primary_floor_id
    """
    curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT pmt_notify_email FROM floor_tbl WHERE id IN (SELECT primary_floor_id FROM cust_acct_tbl WHERE id = %s)', (cust_acct_id,))
    pmt_notify_emails = curdict.fetchone()['pmt_notify_email']
    if pmt_notify_emails is not None:
      pmt_notify_emails.replace(" ","")
    if pmt_notify_emails is not None and pmt_notify_emails != "":
      mail_bcc = pmt_notify_emails.split(',')
      mail_bcc.append('{{ payment_mail_bcc }}')
      mail_bcc = ', '.join(mail_bcc)
    else:
      mail_bcc = '{{ payment_mail_bcc }}'
    return mail_bcc

class NmiFacade():
  """Facade combining NMI/Solupay gateway, database access/update, and email.

  Can be executed in two contexts: [1] on the payment processing server and [2] on the web server facing the customer.
  """

  # maximum number of times to attempt to query NMI gateway for txn_xml.
  max_query_attempts = 3

  # how many hours ACH transactions are delayed.
  # used to compare to scheduled_ts_utc and determine when we can process transactions.
  ach_delay_hours = {{ pay_ach_delay_hours }}

  def __init__(self, dbconn):
    """Initialize an NmiFacade object.

    Args:
      dbconn: database connection object from psycopg2.connect(...)
    """
    self.set_demo_mode(False)
    self.dbconn = dbconn
    self.pmt_gateway = solupay.Gateway()
    self.clear_ach_succ_tmpl()
    self.clear_ach_fail_tmpl()
    self.clear_sch_ach_tmpl()
    self.clear_card_succ_tmpl()
    self.clear_card_fail_tmpl()
    self.clear_sch_card_tmpl()
    self.clear_more_tmpl_dict()

  def set_demo_mode(self, is_demo = False):
    """Set demo mode.

    Args:
      is_demo: True or False. If True, transactions are sent to demo processor and emails are sent to demo email inbox.
    """
    self.demo_mode = is_demo

  def set_ach_succ_tmpl(self, subj_tmpl, text_tmpl, html_tmpl=None):
    """Set the templates used for ACH transaction success.

    Args:
      subj_tmpl (str): Jinja2 template for subject of email.
      body_tmpl (str): Jinja2 template for body of email.
    """
    self.ach_succ_subj_tmpl = jinja2.Template(subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    self.ach_succ_text_tmpl = jinja2.Template(text_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    if html_tmpl:
      self.ach_succ_html_tmpl = jinja2.Template(html_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)

  def clear_ach_succ_tmpl(self):
    """Clear the templates used for ACH transaction success.
    """
    self.ach_succ_subj_tmpl = None
    self.ach_succ_text_tmpl = None
    self.ach_succ_html_tmpl = None

  def set_ach_fail_tmpl(self, subj_tmpl, text_tmpl, html_tmpl=None):
    """Set the templates used for ACH transaction failure.

    Args:
      subj_tmpl (str): Jinja2 template for subject of email.
      body_tmpl (str): Jinja2 template for body of email.
    """
    self.ach_fail_subj_tmpl = jinja2.Template(subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    self.ach_fail_text_tmpl = jinja2.Template(text_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    if html_tmpl:
      self.ach_fail_html_tmpl = jinja2.Template(html_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)

  def clear_ach_fail_tmpl(self):
    """Clear the templates used for ACH transaction failure.
    """
    self.ach_fail_subj_tmpl = None
    self.ach_fail_text_tmpl = None
    self.ach_fail_html_tmpl = None

  def set_sch_ach_tmpl(self, subj_tmpl, text_tmpl, html_tmpl=None):
    """Set the templates used for scheduled ACH transaction.

    Args:
      subj_tmpl (str): Jinja2 template for subject of email.
      body_tmpl (str): Jinja2 template for body of email.
    """
    self.sch_ach_subj_tmpl = jinja2.Template(subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    self.sch_ach_text_tmpl = jinja2.Template(text_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    if html_tmpl:
      self.sch_ach_html_tmpl = jinja2.Template(html_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)

  def clear_sch_ach_tmpl(self):
    """Clear the templates used for scheduled ACH transaction.
    """
    self.sch_ach_subj_tmpl = None
    self.sch_ach_text_tmpl = None
    self.sch_ach_html_tmpl = None

  def set_card_succ_tmpl(self, subj_tmpl, text_tmpl, html_tmpl=None):
    """Set the templates used for card transaction success.

    Args:
      subj_tmpl (str): Jinja2 template for subject of email.
      body_tmpl (str): Jinja2 template for body of email.
    """
    self.card_succ_subj_tmpl = jinja2.Template(subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    self.card_succ_text_tmpl = jinja2.Template(text_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    if html_tmpl:
      self.card_succ_html_tmpl = jinja2.Template(html_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)

  def clear_card_succ_tmpl(self):
    """Clear the templates used for card transaction success.
    """
    self.card_succ_subj_tmpl = None
    self.card_succ_text_tmpl = None
    self.card_succ_html_tmpl = None

  def set_card_fail_tmpl(self, subj_tmpl, text_tmpl, html_tmpl=None):
    """Set the templates used for card transaction failure.

    Args:
      subj_tmpl (str): Jinja2 template for subject of email.
      body_tmpl (str): Jinja2 template for body of email.
    """
    self.card_fail_subj_tmpl = jinja2.Template(subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    self.card_fail_text_tmpl = jinja2.Template(text_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    if html_tmpl:
      self.card_fail_html_tmpl = jinja2.Template(html_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)

  def clear_card_fail_tmpl(self):
    """Clear the templates used for card transaction failure.

    Args:
      subj_tmpl (str): Jinja2 template for subject of email.
      body_tmpl (str): Jinja2 template for body of email.
    """
    self.card_fail_subj_tmpl = None
    self.card_fail_text_tmpl = None
    self.card_fail_html_tmpl = None

  def set_sch_card_tmpl(self, subj_tmpl, text_tmpl, html_tmpl=None):
    """Set the templates used for scheduled card transaction.

    Args:
      subj_tmpl (str): Jinja2 template for subject of email.
      body_tmpl (str): Jinja2 template for body of email.
    """
    self.sch_card_subj_tmpl = jinja2.Template(subj_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    self.sch_card_text_tmpl = jinja2.Template(text_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    if html_tmpl:
      self.sch_card_html_tmpl = jinja2.Template(html_tmpl, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)

  def clear_sch_card_tmpl(self):
    """Clear the templates used for scheduled card transaction.
    """
    self.sch_card_subj_tmpl = None
    self.sch_card_text_tmpl = None
    self.sch_card_html_tmpl = None

  def set_more_tmpl_dict(self, more_tmpl_dict):
    """Set the additional template variables used in Jinja2 template of email.

    Args:
      more_tmpl_dict (dict): dict containing the additional template variables.
    """
    self.more_tmpl_dict = more_tmpl_dict

  def clear_more_tmpl_dict(self):
    """Clear the additional template variables used in Jinja2 template of email.
    """
    self.more_tmpl_dict = None

  def setup_sch_card_tmpls(self):
    """ Sets up templates for sch card success or failure """
    sch_card_subj_tmpl = 'Scheduled card payment to Micro Office'
    with open('{{ pay_basedir }}/share/sch_card_tmpl.txt', 'r') as tmpl_file:
      sch_card_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/sch_card_tmpl.html', 'r') as tmpl_file:
      sch_card_html_tmpl = tmpl_file.read()
    self.set_sch_card_tmpl(sch_card_subj_tmpl, sch_card_text_tmpl, sch_card_html_tmpl)

    card_fail_subj_tmpl = 'ACTION REQUIRED - Failed credit card payment to Micro Office'
    with open('{{ pay_basedir }}/share/card_failure_tmpl.txt', 'r') as tmpl_file:
      card_fail_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/card_failure_tmpl.html', 'r') as tmpl_file:
      card_fail_html_tmpl = tmpl_file.read()
    self.set_card_fail_tmpl(card_fail_subj_tmpl, card_fail_text_tmpl, card_fail_html_tmpl)

  def setup_card_tmpls(self):
    card_succ_subj_tmpl = 'Successful card payment to Micro Office'
    with open('{{ pay_basedir }}/share/card_success_tmpl.txt', 'r') as tmpl_file:
      card_succ_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/card_success_tmpl.html', 'r') as tmpl_file:
      card_succ_html_tmpl = tmpl_file.read()
    self.set_card_succ_tmpl(card_succ_subj_tmpl, card_succ_text_tmpl, card_succ_html_tmpl)
    card_fail_subj_tmpl = 'ACTION REQUIRED - Failed credit card payment to Micro Office'
    with open('{{ pay_basedir }}/share/card_failure_tmpl.txt', 'r') as tmpl_file:
      card_fail_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/card_failure_tmpl.html', 'r') as tmpl_file:
      card_fail_html_tmpl = tmpl_file.read()
    self.set_card_fail_tmpl(card_fail_subj_tmpl, card_fail_text_tmpl, card_fail_html_tmpl)

  def setup_sch_ach_tmpls(self):
    sch_ach_subj_tmpl = 'Scheduled ACH/bank payment to Micro Office'
    with open('{{ pay_basedir }}/share/sch_ach_tmpl.txt', 'r') as tmpl_file:
      sch_ach_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/sch_ach_tmpl.html', 'r') as tmpl_file:
      sch_ach_html_tmpl = tmpl_file.read()
    self.set_sch_ach_tmpl(sch_ach_subj_tmpl, sch_ach_text_tmpl, sch_ach_html_tmpl)
    ach_fail_subj_tmpl = 'ACTION REQUIRED - Failed ACH/bank payment to Micro Office'
    with open('{{ pay_basedir }}/share/ach_failure_tmpl.txt', 'r') as tmpl_file:
      ach_fail_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/ach_failure_tmpl.html', 'r') as tmpl_file:
      ach_fail_html_tmpl = tmpl_file.read()
    self.set_ach_fail_tmpl(ach_fail_subj_tmpl, ach_fail_text_tmpl, ach_fail_html_tmpl)

  def setup_ach_tmpls(self):
    ach_succ_subj_tmpl = 'Successful ACH/bank payment to Micro Office'
    with open('{{ pay_basedir }}/share/ach_success_tmpl.txt', 'r') as tmpl_file:
      ach_succ_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/ach_success_tmpl.html', 'r') as tmpl_file:
      ach_succ_html_tmpl = tmpl_file.read()
    self.set_ach_succ_tmpl(ach_succ_subj_tmpl, ach_succ_text_tmpl, ach_succ_html_tmpl)
    ach_fail_subj_tmpl = 'ACTION REQUIRED - Failed ACH/bank payment to Micro Office'
    with open('{{ pay_basedir }}/share/ach_failure_tmpl.txt', 'r') as tmpl_file:
      ach_fail_text_tmpl = tmpl_file.read()
    with open('{{ pay_basedir }}/share/ach_failure_tmpl.html', 'r') as tmpl_file:
      ach_fail_html_tmpl = tmpl_file.read()
    self.set_ach_fail_tmpl(ach_fail_subj_tmpl, ach_fail_text_tmpl, ach_fail_html_tmpl)

  def process_pmt_nmi_txn(self, pmt_nmi_txn_id, **kwargs):
    """Process an NMI/Solupay ACH or card transaction from an ID value.

    Args:
      pmt_nmi_txn_id (int): ID value of a row in pmt_nmi_txn_vw.
      routing_number (optional[str]): 9 digit routing number
      account_number (optional[str]): numeric account number
      card_number (optional[str]): card number
      cvv (optional[str]): CVV

    Returns:
      resulting row from pmt_nmi_txn_vw after transaction completed.

    Raises:
      PaymentTransactionNotFoundError: if payment transaction not found in pmt_nmi_txn_vw.
      UnknownPaymentMethodError: if payment method is unknown or unhandled.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * from pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn_id, ))
    pmt_nmi_txn = curdict.fetchone()
    curdict.close()
    if not pmt_nmi_txn:
      raise PaymentTransactionNotFoundError('row in pmt_nmi_txn_vw with ID %s not found' % pmt_nmi_txn_id)
    return self.process_pmt_nmi_txn_row(pmt_nmi_txn, **kwargs)


  def process_pmt_nmi_txn_row(self, pmt_nmi_txn, **kwargs):
    """Process an NMI/Solupay ACH or card transaction from a row.

    Args:
      pmt_nmi_txn (dict): a dict containing a row from pmt_nmi_txn_vw (must be the view, not a table).
      routing_number (optional[str]): 9 digit routing number
      account_number (optional[str]): numeric account number
      card_number (optional[str]): card number
      cvv (optional[str]): CVV

    Returns:
      resulting row from pmt_nmi_txn_vw after transaction completed.

    Raises:
      UnknownPaymentMethodError: if payment method is unknown or unhandled.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    if pmt_nmi_txn['pmt_method'] == 'ach':
      return self.process_nmi_txn_ach(pmt_nmi_txn, **kwargs)
    elif pmt_nmi_txn['pmt_method'] == 'card':
      return self.process_nmi_txn_card(pmt_nmi_txn, **kwargs)
    else:
      raise UnknownPaymentMethodError('unknown payment method %s; must be ach or card' % pmt_nmi_txn['pmt_method'])


  def process_nmi_txn_ach(self, pmt_nmi_txn, routing_number=None, account_number=None):
    """Process an NMI/Solupay ACH transaction from a row.

    If routing_number or account_number missing from args, load both from pmt_nmi_txn_vw (must load decrypt module in that case).

    Args:
      pmt_nmi_txn (dict): a dict containing a row from pmt_nmi_txn_vw (must be the view, not a table).
      routing_number (optional[str]): 9 digit routing number
      account_number (optional[str]): numeric account number

    Returns:
      resulting row from pmt_nmi_txn_vw after transaction completed.

    Raises:
      BadPaymentMethodError: If payment method is not 'ach'.
      HeldError: If payment transaction has hold flag set to True.
      AlreadyProcessedError: If payment transaction has already been processed.
      ScheduledFutureError: If payment transaction is scheduled in the future.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    if pmt_nmi_txn['pmt_method'] != 'ach':
      raise BadPaymentMethodError('payment method must be ach for process_nmi_txn_ach, not %s' % pmt_nmi_txn['pmt_method'])
    if pmt_nmi_txn['hold']:
      raise HeldError('transaction in pmt_nmi_txn_ach_tbl has hold set to True; row ID %s' % pmt_nmi_txn['id'])
    if pmt_nmi_txn['request_posted_ts'] != None or pmt_nmi_txn['response'] != None or pmt_nmi_txn['transactionid'] != None:
      raise AlreadyProcessedError('transaction in pmt_nmi_txn_ach_tbl already processed; row ID %s' % pmt_nmi_txn['id'])
    # check if current time + ACH delay is past the scheduled datetime
    if pmt_nmi_txn['scheduled_ts_utc'] > (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(hours=self.ach_delay_hours)):
      raise ScheduledFutureError('transaction in pmt_nmi_txn_ach_tbl has scheduled date in future; row ID %s' % pmt_nmi_txn['id'])
    self.pmt_gateway.clear()
    if self.demo_mode:
      self.pmt_gateway.set_username('demo')
    else:
      self.pmt_gateway.set_username(pmt_nmi_txn['nmi_username'])
    if not routing_number or not account_number:
      import decrypt
      decrypt_client = decrypt.DecryptClient()
      decrypted_row = decrypt_client.decrypt_row('pmt_nmi_txn_vw', pmt_nmi_txn['id'])
      routing_number = decrypted_row['routing_number']
      account_number = decrypted_row['account_number']
    http_txn_dict = self.pmt_gateway.do_ach_sale(pmt_nmi_txn['account_type'], pmt_nmi_txn['owned_by'], pmt_nmi_txn['registered_name'], routing_number, account_number, dbutil.cast_money_to_decimal(pmt_nmi_txn['amount']), order_id=pmt_nmi_txn['id'], order_description='cust_acct_id %s, cust_acct_name %s' % (pmt_nmi_txn['cust_acct_id'], pmt_nmi_txn['name']))
    query = http_txn_dict['query']
    # mask sensitive info
    query = re.sub(r'^password=([^&]*)|&password=([^&]*)', r'password=***', query)
    query = re.sub(r'^checkaba=([^&]*)|&checkaba=([^&]*)', r'checkaba=***', query)
    query = re.sub(r'^checkaccount=([^&]*)|&checkaccount=([^&]*)', r'checkaccount=***', query)
    response_body = http_txn_dict['response_body']
    response_dict = http_txn_dict['response_dict']
    extra_column_assign = ''
    extra_column_values = []
    for column_name in ['response', 'responsetext', 'authcode', 'transactionid', 'avsresponse', 'response_code', 'order_description', 'cvvresponse', 'orderid']:
      if column_name in response_dict:
        extra_column_assign += ', ' + column_name + ' = %s'
        extra_column_values.append(response_dict[column_name])
    cur = self.dbconn.cursor()
    cur.execute('UPDATE pmt_nmi_txn_ach_tbl SET request_posted_ts = now(), request_body = %s, response_body = %s' + extra_column_assign + ' WHERE id = %s', (query, response_body) + tuple(extra_column_values) + (pmt_nmi_txn['id'], ))
    self.dbconn.commit()
    if 'transactionid' in response_dict:
      query_returned = False
      num_query_attempts = 0
      while not query_returned and num_query_attempts < self.max_query_attempts:
        try:
          num_query_attempts += 1
          http_txn_dict = self.pmt_gateway.run_query(transaction_id=response_dict['transactionid'])
          query_returned = True
        except pycurl.error:
          pass
      if query_returned:
        txn_xml = http_txn_dict['response_body']
        sync_txn_action_lib.sync_txn_actions(txn_xml, self.dbconn)
    cur.close()
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn['id'],))
    pmt_nmi_txn = curdict.fetchone()
    if pmt_nmi_txn['email']:
      mail_subject = None
      mail_text_body = None
      property_dict = dict(pmt_nmi_txn)
      property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
      property_dict['cust_acct_name'] = pmt_nmi_txn['name']
      property_dict['pay_base_url'] = '{{ pay_base_url }}'
      property_dict['static_base_url'] = '{{ static_base_url }}'
      curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
      email_id = curdict.fetchone()['next_id']
      property_dict['email_id'] = email_id
      if self.more_tmpl_dict:
        property_dict.update(self.more_tmpl_dict)
      if response_dict['response'] == '1':
        if self.ach_succ_text_tmpl:
          mail_subject = self.ach_succ_subj_tmpl.render(property_dict)
          mail_text_body = self.ach_succ_text_tmpl.render(property_dict)
        if self.ach_succ_html_tmpl:
          mail_html_body = self.ach_succ_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
      else:
        if self.ach_fail_text_tmpl:
          mail_subject = self.ach_fail_subj_tmpl.render(property_dict)
          mail_text_body = self.ach_fail_text_tmpl.render(property_dict)
        if self.ach_fail_html_tmpl:
          mail_html_body = self.ach_fail_html_tmpl.render(property_dict)
      if mail_subject and mail_text_body:
        if self.demo_mode:
          ChargeMail.send_html_mail('{{ payment_mail_to_demo }}', None, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)
        else:
          mail_bcc = ChargeMail.get_pmt_bcc_emails(self.dbconn, property_dict['cust_acct_id'])
          ChargeMail.send_html_mail(pmt_nmi_txn['email'], mail_bcc, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)
    curdict.close()
    return pmt_nmi_txn


  def process_nmi_txn_card(self, pmt_nmi_txn, card_number=None, cvv=None):
    """Process an NMI/Solupay card transaction from a row.

    If card_number missing from args, load both card_number and cvv from pmt_nmi_txn_vw (must load decrypt module in that case).

    Args:
      pmt_nmi_txn (dict): a dict containing a row from pmt_nmi_txn_vw (must be the view, not a table).
      card_number (optional[str]): card number
      cvv (optional[str]): CVV

    Returns:
      resulting row from pmt_nmi_txn_vw after transaction completed.

    Raises:
      BadPaymentMethodError: If payment method is not 'card'.
      HeldError: If payment transaction has hold flag set to True.
      AlreadyProcessedError: If payment transaction has already been processed.
      ScheduledFutureError: If payment transaction is scheduled in the future.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    if pmt_nmi_txn['pmt_method'] != 'card':
      raise BadPaymentMethodError('payment method must be card for process_nmi_txn_card, not %s' % pmt_nmi_txn['pmt_method'])
    if pmt_nmi_txn['hold']:
      raise HeldError('transaction in pmt_nmi_txn_card_tbl has hold set to True; row ID %s' % pmt_nmi_txn['id'])
    if pmt_nmi_txn['request_posted_ts'] != None or pmt_nmi_txn['response'] != None or pmt_nmi_txn['transactionid'] != None:
      raise AlreadyProcessedError('transaction in pmt_nmi_txn_card_tbl already processed; row ID %s' % pmt_nmi_txn['id'])
    if pmt_nmi_txn['scheduled_ts_utc'] > datetime.datetime.now(datetime.timezone.utc):
      raise ScheduledFutureError('transaction in pmt_nmi_txn_card_tbl has scheduled date in future; row ID %s' % pmt_nmi_txn['id'])
    self.pmt_gateway.clear()
    if self.demo_mode:
      self.pmt_gateway.set_username('demo')
    else:
      self.pmt_gateway.set_username(pmt_nmi_txn['nmi_username'])
    if not card_number:
      import decrypt
      decrypt_client = decrypt.DecryptClient()
      decrypted_row = decrypt_client.decrypt_row('pmt_nmi_txn_vw', pmt_nmi_txn['id'])
      card_number = decrypted_row['card_number']
      cvv = decrypted_row['cvv']
    http_txn_dict = self.pmt_gateway.do_card_sale(card_number, pmt_nmi_txn['expire_date'], cvv, pmt_nmi_txn['billing_first_name'], pmt_nmi_txn['billing_last_name'], pmt_nmi_txn['billing_company'], pmt_nmi_txn['billing_address_1'], pmt_nmi_txn['billing_address_2'], pmt_nmi_txn['billing_city'], pmt_nmi_txn['billing_state'], pmt_nmi_txn['billing_zip_code'], pmt_nmi_txn['billing_country'], dbutil.cast_money_to_decimal(pmt_nmi_txn['amount']), order_id=pmt_nmi_txn['id'], order_description='cust_acct_id %s, cust_acct_name %s' % (pmt_nmi_txn['cust_acct_id'], pmt_nmi_txn['name']))
    query = http_txn_dict['query']
    # mask sensitive info
    query = re.sub(r'^password=([^&]*)|&password=([^&]*)', r'password=***', query)
    query = re.sub(r'^ccnumber=([^&]*)|&ccnumber=([^&]*)', r'ccnumber=***', query)
    query = re.sub(r'^cvv=([^&]*)|&cvv=([^&]*)', r'cvv=***', query)
    response_body = http_txn_dict['response_body']
    response_dict = http_txn_dict['response_dict']
    extra_column_assign = ''
    extra_column_values = []
    for column_name in ['response', 'responsetext', 'authcode', 'transactionid', 'avsresponse', 'response_code', 'order_description', 'cvvresponse', 'orderid']:
      if column_name in response_dict:
        extra_column_assign += ', ' + column_name + ' = %s'
        extra_column_values.append(response_dict[column_name])
    cur = self.dbconn.cursor()
    cur.execute('UPDATE pmt_nmi_txn_card_tbl SET request_posted_ts = now(), request_body = %s, response_body = %s' + extra_column_assign + ' WHERE id = %s', (query, response_body) + tuple(extra_column_values) + (pmt_nmi_txn['id'], ))
    self.dbconn.commit()
    if 'transactionid' in response_dict:
      query_returned = False
      num_query_attempts = 0
      while not query_returned and num_query_attempts < self.max_query_attempts:
        try:
          num_query_attempts += 1
          http_txn_dict = self.pmt_gateway.run_query(transaction_id=response_dict['transactionid'])
          query_returned = True
        except pycurl.error:
          pass
      if query_returned:
        txn_xml = http_txn_dict['response_body']
        sync_txn_action_lib.sync_txn_actions(txn_xml, self.dbconn)
    cur.close()
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn['id'],))
    pmt_nmi_txn = curdict.fetchone()
    if pmt_nmi_txn['email']:
      mail_subject = None
      mail_text_body = None
      property_dict = dict(pmt_nmi_txn)
      property_dict['cust_acct_name'] = pmt_nmi_txn['name']
      property_dict['pay_base_url'] = '{{ pay_base_url }}'
      property_dict['static_base_url'] = '{{ static_base_url }}'
      curdict.execute("SELECT nextval('email_paysite_seq') AS next_id")
      email_id = curdict.fetchone()['next_id']
      property_dict['email_id'] = email_id
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(property_dict['expire_date']));
      property_dict['expire_mmyyyy'] = match.group(2) + '/' + match.group(1)
      property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
      property_dict['convenience_fee_dec'] = dbutil.cast_money_to_decimal(property_dict['convenience_fee'])
      property_dict['prefee_amount_dec'] = property_dict['amount_dec'] - property_dict['convenience_fee_dec']
      property_dict['convenience_pct'] = '{{ pay_card_convenience_pct }}'
      if self.more_tmpl_dict:
        property_dict.update(self.more_tmpl_dict)
      if response_dict['response'] == '1':
        if self.card_succ_text_tmpl:
          mail_subject = self.card_succ_subj_tmpl.render(property_dict)
          mail_text_body = self.card_succ_text_tmpl.render(property_dict)
        if self.card_succ_html_tmpl:
          mail_html_body = self.card_succ_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
      else:
        if self.card_fail_text_tmpl:
          mail_subject = self.card_fail_subj_tmpl.render(property_dict)
          mail_text_body = self.card_fail_text_tmpl.render(property_dict)
        if self.card_fail_html_tmpl:
          mail_html_body = self.card_fail_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
      if mail_subject and mail_text_body:
        if self.demo_mode:
          ChargeMail.send_html_mail('{{ payment_mail_to_demo }}', None, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)
        else:
          mail_bcc = ChargeMail.get_pmt_bcc_emails(self.dbconn, property_dict['cust_acct_id'])
          ChargeMail.send_html_mail(pmt_nmi_txn['email'], mail_bcc, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)
    curdict.close()
    return pmt_nmi_txn

  def insert_pmt_nmi_txn_ach(self, cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, email, do_commit=True):
    """Create a row for an NMI/Solupay ACH transaction in pmt_nmi_txn_ach_tbl.

    Args:
      cust_acct_id (int): primary key for cust_acct_tbl that specifies the customer account.
      account_type (str): checking | savings.
      owned_by (str): personal | business.
      registered_name (str): name of owner of bank account.
      routing_number (str): 9 digit ABA routing number of bank.
      account_number (str): bank account number.
      amount (decimal.Decimal): dollar amount of ACH debit.
      scheduled_date (datetime.date): scheduled date when ACH debit should occur, in local time of the customer.
      note (str): user-entered note; can be None.
      email (str): email where confirmation of initial scheduling and subsequent payment processing result should be sent.
      do_commit (bool): True = perform database commit; False = refrain from commit.

    Returns:
      resulting row from pmt_nmi_txn_vw after transaction scheduled.
    """
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('INSERT INTO pmt_nmi_txn_ach_tbl (cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, email) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *', (cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, email))
    pmt_nmi_txn = curdict.fetchone()
    if do_commit:
      self.dbconn.commit()
    curdict.execute('SELECT * from pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn['id'],))
    pmt_nmi_txn = curdict.fetchone()
    return pmt_nmi_txn

  def schedule_nmi_txn_ach(self, cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, email):
    """Schedule an NMI/Solupay ACH transaction.

    Args:
      cust_acct_id (int): primary key for cust_acct_tbl.
      account_type (str): checking | savings.
      owned_by (str): personal | business.
      registered_name (str): name of owner of bank account.
      routing_number (str): 9 digit ABA routing number of bank.
      account_number (str): bank account number.
      amount (decimal.Decimal): dollar amount of ACH debit.
      scheduled_date (datetime.date): scheduled date when ACH debit should occur, in local time of the customer.
      note (str): user-entered note; can be None.
      email (str): email where confirmation of initial scheduling and subsequent payment processing result should be sent.

    Returns:
      resulting row from pmt_nmi_txn_ach_tbl after transaction scheduled.

    Raises:
      ScheduledPastError: If payment has scheduled date in past.
    """
    # if the scheduled date is in the past, throw an error because we can only schedule payments in the future
    if not tzconvert.is_future_date_for_cust(self.dbconn, cust_acct_id, scheduled_date):
      raise ScheduledPastError('tried to schedule ACH payment in past or today; must be in future; cust_acct_id %s, amount %s, scheduled_date %s, email %s' % (cust_acct_id, amount, scheduled_date, email))
    pmt_nmi_txn = self.insert_pmt_nmi_txn_ach(cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, email)
    self.send_mail_ach_scheduled(pmt_nmi_txn)
    return pmt_nmi_txn

  def send_mail_ach_scheduled(self, pmt_nmi_txn):
    """Send email to customer confirming a scheduled ACH payment.

    Args:
      pmt_nmi_txn (dict): a dict containing a row from pmt_nmi_txn_vw.
    """
    if pmt_nmi_txn['email']:
      mail_subject = None
      mail_text_body = None
      property_dict = dict(pmt_nmi_txn)
      property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
      if self.more_tmpl_dict:
        property_dict.update(self.more_tmpl_dict)
      if self.sch_ach_text_tmpl:
        mail_subject = self.sch_ach_subj_tmpl.render(property_dict)
        mail_text_body = self.sch_ach_text_tmpl.render(property_dict)
      if self.sch_ach_html_tmpl:
        mail_html_body = self.sch_ach_html_tmpl.render(property_dict)
      else:
        mail_html_body = None
      if mail_subject and mail_text_body:
        if self.demo_mode:
          ChargeMail.send_html_mail('{{ payment_mail_to_demo }}', None, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)
        else:
          mail_bcc = ChargeMail.get_pmt_bcc_emails(self.dbconn, property_dict['cust_acct_id'])
          ChargeMail.send_html_mail(pmt_nmi_txn['email'], mail_bcc, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)

  def insert_pmt_nmi_txn_card(self, cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_address_num, billing_city, billing_state, billing_zip_code, billing_country, amount, convenience_fee, scheduled_date, note, email, do_commit=True):
    """Create a row for an NMI/Solupay card transaction in pmt_nmi_txn_card_tbl.

    Args:
      cust_acct_id (int): primary key for cust_acct_tbl that specifies the customer account.
      card_number (str): credit/debit card number.
      expire_date (datetime.date): expiration date of the card (day portion of date is 1 if unknown).
      cvv (str): 3 or 4 digit CVV/CVC.
      billing_first_name (str): first name of cardholder.
      billing_last_name (str): last name of cardholder.
      billing_company (str): company of the cardholder.
      billing_address_1 (str): first billing address line.
      billing_address_2 (str): optional second billing address line.
      billing_address_num (str): optional numeric prefix of street address from first billing address line.
      billing_city (str): city from billing address.
      billing_state (str): state from billing address. two character abbreviation if billing_country is US.
      billing_zip_code (str): zip code from billing address.
      billing_country (str): card billing country code. ISO 3166-1 alpha-2 (2 character abbreviation).
      amount (decimal.Decimal): dollar amount of charge.
      convenience_fee (decimal.Decimal): dollar amount of convenience fee (subtotal = amount - convenience_fee).
      scheduled_date (datetime.date): scheduled date when charge should occur, in local time of the customer.
      note (str): user-entered note; can be None.
      email (str): email where confirmation of initial scheduling and subsequent payment processing result should be sent.
      do_commit (bool): True = perform database commit; False = refrain from commit.

    Returns:
      resulting row from pmt_nmi_txn_vw after transaction scheduled.
    """
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('INSERT INTO pmt_nmi_txn_card_tbl (cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_address_num, billing_city, billing_state, billing_zip_code, billing_country, amount, convenience_fee, scheduled_date, note, email) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *', (cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_address_num, billing_city, billing_state, billing_zip_code, billing_country, amount, convenience_fee, scheduled_date, note, email))
    pmt_nmi_txn = curdict.fetchone()
    if do_commit:
      self.dbconn.commit()
    curdict.execute('SELECT * from pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn['id'],))
    pmt_nmi_txn = curdict.fetchone()
    return pmt_nmi_txn

  def schedule_nmi_txn_card(self, cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_address_num, billing_city, billing_state, billing_zip_code, billing_country, amount, convenience_fee, scheduled_date, note, email):
    """Schedule an NMI/Solupay card transaction.

    Args:
      cust_acct_id (int): primary key for cust_acct_tbl that specifies the customer account.
      card_number (str): credit/debit card number.
      expire_date (datetime.date): expiration date of the card (day portion of date is 1 if unknown).
      cvv (str): 3 or 4 digit CVV/CVC.
      billing_first_name (str): first name of cardholder.
      billing_last_name (str): last name of cardholder.
      billing_company (str): company of the cardholder.
      billing_address_1 (str): first billing address line.
      billing_address_2 (str): optional second billing address line.
      billing_address_num (str): optional numeric prefix of street address from first billing address line.
      billing_city (str): city from billing address.
      billing_state (str): state from billing address. two character abbreviation if billing_country is US.
      billing_zip_code (str): zip code from billing address.
      billing_country (str): card billing country code. ISO 3166-1 alpha-2 (2 character abbreviation).
      amount (decimal.Decimal): dollar amount of charge.
      convenience_fee (decimal.Decimal): dollar amount of convenience fee (subtotal = amount - convenience_fee).
      scheduled_date (datetime.date): scheduled date when charge should occur, in local time of the customer.
      note (str): user-entered note; can be None.
      email (str): email where confirmation of initial scheduling and subsequent payment processing result should be sent.

    Returns:
      resulting row from pmt_nmi_txn_card_tbl after transaction scheduled.

    Raises:
      ScheduledPastError: If payment has scheduled date in past.
    """
    # if the scheduled date is in the past, throw an error because we can only schedule payments in the future
    if not tzconvert.is_future_date_for_cust(self.dbconn, cust_acct_id, scheduled_date):
      raise ScheduledPastError('tried to schedule card payment in past or today; must be in future; cust_acct_id %s, amount %s, scheduled_date %s, email %s' % (cust_acct_id, amount, scheduled_date, email))
    pmt_nmi_txn = self.insert_pmt_nmi_txn_card(cust_acct_id, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_address_num, billing_city, billing_state, billing_zip_code, billing_country, amount, convenience_fee, scheduled_date, note, email)
    self.send_mail_card_scheduled(pmt_nmi_txn)
    return pmt_nmi_txn

  def send_mail_card_scheduled(self, pmt_nmi_txn):
    """Send email to customer confirming a scheduled card payment.

    Args:
      pmt_nmi_txn (dict): a dict containing a row from pmt_nmi_txn_vw or pmt_nmi_txn_card_tbl.
    """
    if pmt_nmi_txn['email']:
      mail_subject = None
      mail_text_body = None
      property_dict = dict(pmt_nmi_txn)
      match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(property_dict['expire_date']));
      property_dict['expire_mmyyyy'] = match.group(2) + '/' + match.group(1)
      property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
      property_dict['convenience_fee_dec'] = dbutil.cast_money_to_decimal(property_dict['convenience_fee'])
      property_dict['prefee_amount_dec'] = property_dict['amount_dec'] - property_dict['convenience_fee_dec']
      property_dict['convenience_pct'] = '{{ pay_card_convenience_pct }}'
      if self.more_tmpl_dict:
        property_dict.update(self.more_tmpl_dict)
      if self.sch_card_text_tmpl:
        mail_subject = self.sch_card_subj_tmpl.render(property_dict)
        mail_text_body = self.sch_card_text_tmpl.render(property_dict)
      if self.sch_card_html_tmpl:
        mail_html_body = self.sch_card_html_tmpl.render(property_dict)
      else:
        mail_html_body = None
      if mail_subject and mail_text_body:
        if self.demo_mode:
          ChargeMail.send_html_mail('{{ payment_mail_to_demo }}', None, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)
        else:
          mail_bcc = ChargeMail.get_pmt_bcc_emails(self.dbconn, property_dict['cust_acct_id'])
          ChargeMail.send_html_mail(pmt_nmi_txn['email'], mail_bcc, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)

  def insert_pmt_vault_txn(self, pmt_vault_id, amount, convenience_fee, scheduled_date, note, email, processor='nmi', do_commit=True):
    """Create a row for a vault transaction in pmt_vault_txn_tbl.

    Args:
      pmt_vault_id (int): primary key for pmt_vault_tbl that specifies the payment method.
      amount (decimal.Decimal): dollar amount of vault transaction (for card transactions, total amount including conv fee).
      convenience_fee (decimal.Decimal): dollar amount of convenience fee (subtotal = amount - convenience_fee).
      scheduled_date (datetime.date): scheduled date when customer pay transaction should occur, in local time of the customer.
      processor (str): name of merchant processor; can be nmi or payeezy.
      note (str): user-entered note; can be None.
      email (str): email where confirmation of initial scheduling and subsequent payment processing result should be sent.
      do_commit (bool): True = perform database commit; False = refrain from commit.

    Returns:
      resulting row from pmt_vault_txn_vw after transaction scheduled.
    """
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('INSERT INTO pmt_vault_txn_tbl (pmt_vault_id, amount, convenience_fee, scheduled_date, processor, note, email) VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING *', (pmt_vault_id, amount, convenience_fee, scheduled_date, processor, note, email))
    pmt_vault_txn = curdict.fetchone()
    if do_commit:
      self.dbconn.commit()
    curdict.execute('SELECT * from pmt_vault_txn_vw WHERE id = %s', (pmt_vault_txn['id'],))
    pmt_vault_txn = curdict.fetchone()
    return pmt_vault_txn

  def schedule_vault_txn(self, pmt_vault_id, amount, convenience_fee, scheduled_date, note, email, processor='nmi'):
    """Schedule a vault transaction.

    Args:
      pmt_vault_id (int): primary key for pmt_vault_tbl that specifies the payment method.
      amount (decimal.Decimal): dollar amount of ACH debit.
      convenience_fee (decimal.Decimal): dollar amount of convenience fee (subtotal = amount - convenience_fee).
      scheduled_date (datetime.date): scheduled date when customer pay transaction should occur, in local time of the customer.
      processor (str): name of merchant processor; can be nmi or payeezy.
      note (str): user-entered note; can be None.
      email (str): email where confirmation of initial scheduling and subsequent payment processing result should be sent.

    Returns:
      resulting row from pmt_vault_txn_vw after transaction scheduled.

    Raises:
      ScheduledPastError: If payment has scheduled date in past.
    """
    # if the scheduled date is in the past, throw an error because we can only schedule payments in the future
    if not tzconvert.is_future_date_for_cust(self.dbconn, cust_acct_id, scheduled_date):
      raise ScheduledPastError('tried to schedule vault payment in past or today; must be in future; cust_acct_id %s, amount %s, scheduled_date %s, email %s' % (cust_acct_id, amount, scheduled_date, email))
    pmt_vault_txn = self.insert_pmt_vault_txn(pmt_vault_id, amount, convenience_fee, scheduled_date, processor, note, email)
    self.send_mail_vault_scheduled(pmt_vault_txn)
    return pmt_vault_txn

  def send_mail_vault_scheduled(self, pmt_vault_txn):
    """Send email to customer confirming a scheduled vault payment.

    Args:
      pmt_vault_txn (dict): a dict containing a row from pmt_vault_txn_vw.
    """
    if pmt_vault_txn['email']:
      mail_subject = None
      mail_text_body = None
      property_dict = dict(pmt_vault_txn)
      if pmt_vault_txn['pmt_method'] == 'ach':
        property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
        if self.more_tmpl_dict:
          property_dict.update(self.more_tmpl_dict)
        if self.sch_ach_text_tmpl:
          mail_subject = self.sch_ach_subj_tmpl.render(property_dict)
          mail_text_body = self.sch_ach_text_tmpl.render(property_dict)
        if self.sch_ach_html_tmpl:
          mail_html_body = self.sch_ach_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
      elif pmt_vault_txn['pmt_method'] == 'card':
        match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(property_dict['expire_date']));
        property_dict['expire_mmyyyy'] = match.group(2) + '/' + match.group(1)
        property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
        property_dict['convenience_fee_dec'] = dbutil.cast_money_to_decimal(property_dict['convenience_fee'])
        property_dict['prefee_amount_dec'] = property_dict['amount_dec'] - property_dict['convenience_fee_dec']
        property_dict['convenience_pct'] = '{{ pay_card_convenience_pct }}'
        if self.more_tmpl_dict:
          property_dict.update(self.more_tmpl_dict)
        if self.sch_card_text_tmpl:
          mail_subject = self.sch_card_subj_tmpl.render(property_dict)
          mail_text_body = self.sch_card_text_tmpl.render(property_dict)
        if self.sch_card_html_tmpl:
          mail_html_body = self.sch_card_html_tmpl.render(property_dict)
        else:
          mail_html_body = None
      else:
        raise Exception('send_mail_vault_scheduled: unknown pmt_method: %s' % (pmt_vault_txn['pmt_method'],))
      if mail_subject and mail_text_body:
        if self.demo_mode:
          ChargeMail.send_html_mail('{{ payment_mail_to_demo }}', None, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)
        else:
          mail_bcc = ChargeMail.get_pmt_bcc_emails(self.dbconn, property_dict['cust_acct_id'])
          ChargeMail.send_html_mail(pmt_vault_txn['email'], mail_bcc, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn)

  def send_mail_vault_txn(self, pmt_vault_txn, do_commit=True):
    """Send email to customer about a completed vault payment (success or failure).

    Args:
      pmt_vault_txn (dict): a dict containing a row from pmt_vault_txn_vw.
         required key - 'email_id' using chargefacade.set_more_templ_dict
      do_commit (bool): True = perform database commit; False = refrain from commit.
    """
    if pmt_vault_txn['email']:
      mail_subject = None
      mail_text_body = None
      property_dict = dict(pmt_vault_txn)
      curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      property_dict['cust_acct_name'] = pmt_vault_txn['name']
      property_dict['pay_base_url'] = '{{ pay_base_url }}'
      property_dict['static_base_url'] = '{{ static_base_url }}'
      curdict.close()
      if pmt_vault_txn['pmt_method'] == 'ach':
        property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
        if self.more_tmpl_dict:
          property_dict.update(self.more_tmpl_dict)
        if property_dict['response'] == '1':
          if self.ach_succ_text_tmpl:
            mail_subject = self.ach_succ_subj_tmpl.render(property_dict)
            mail_text_body = self.ach_succ_text_tmpl.render(property_dict)
          if self.ach_succ_html_tmpl:
            mail_html_body = self.ach_succ_html_tmpl.render(property_dict)
          else:
            mail_html_body = None
        else:
          if self.ach_fail_text_tmpl:
            mail_subject = self.ach_fail_subj_tmpl.render(property_dict)
            mail_text_body = self.ach_fail_text_tmpl.render(property_dict)
          if self.ach_fail_html_tmpl:
            mail_html_body = self.ach_fail_html_tmpl.render(property_dict)
          else:
            mail_html_body = None
        if mail_subject and mail_text_body:
          if self.demo_mode:
            ChargeMail.send_html_mail('{{ payment_mail_to_demo }}', None, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn, do_commit)
          else:
            mail_bcc = ChargeMail.get_pmt_bcc_emails(self.dbconn, property_dict['cust_acct_id'])
            ChargeMail.send_html_mail(property_dict['email'], mail_bcc, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn, do_commit)
      elif pmt_vault_txn['pmt_method'] == 'card':
        match = re.match(r'(\d{4})-(\d\d)-(\d\d)', str(property_dict['expire_date']));
        property_dict['expire_mmyyyy'] = match.group(2) + '/' + match.group(1)
        property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
        property_dict['convenience_fee_dec'] = dbutil.cast_money_to_decimal(property_dict['convenience_fee'])
        property_dict['prefee_amount_dec'] = property_dict['amount_dec'] - property_dict['convenience_fee_dec']
        property_dict['convenience_pct'] = '{{ pay_card_convenience_pct }}'
        if self.more_tmpl_dict:
          property_dict.update(self.more_tmpl_dict)
        if property_dict['response'] == '1':
          if self.card_succ_text_tmpl:
            mail_subject = self.card_succ_subj_tmpl.render(property_dict)
            mail_text_body = self.card_succ_text_tmpl.render(property_dict)
          if self.card_succ_html_tmpl:
            mail_html_body = self.card_succ_html_tmpl.render(property_dict)
          else:
            mail_html_body = None
        else:
          if self.card_fail_text_tmpl:
            mail_subject = self.card_fail_subj_tmpl.render(property_dict)
            mail_text_body = self.card_fail_text_tmpl.render(property_dict)
          if self.card_fail_html_tmpl:
            mail_html_body = self.card_fail_html_tmpl.render(property_dict)
          else:
            mail_html_body = None
        if mail_subject and mail_text_body:
          if self.demo_mode:
            ChargeMail.send_html_mail('{{ payment_mail_to_demo }}', None, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn, do_commit)
          else:
            mail_bcc = ChargeMail.get_pmt_bcc_emails(self.dbconn, property_dict['cust_acct_id'])
            ChargeMail.send_html_mail(property_dict['email'], mail_bcc, mail_subject, mail_text_body, mail_html_body, property_dict['email_id'], property_dict['cust_acct_id'], self.dbconn, do_commit)
      else:
        raise Exception('send_mail_vault_txn: unknown pmt_method: %s' % (pmt_vault_txn['pmt_method'],))

  def process_pmt_vault_txn(self, pmt_vault_txn_id):
    """Process a vault transaction from an ID value.

    Should only be called on payment processing server, not on web server for customers (e.g. pay.microoffice.com).

    Args:
      pmt_vault_txn_id (int): ID value of a row in pmt_vault_txn_vw.

    Returns:
      resulting row from pmt_vault_txn_vw after transaction completed.

    Raises:
      PaymentTransactionNotFoundError: if payment transaction not found in pmt_vault_txn_vw.
      UnknownPaymentMethodError: if payment method is unknown or unhandled.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * from pmt_vault_txn_vw WHERE id = %s', (pmt_vault_txn_id, ))
    pmt_vault_txn = curdict.fetchone()
    curdict.close()
    if not pmt_vault_txn:
      raise PaymentTransactionNotFoundError('row in pmt_vault_txn_vw with ID %s not found' % pmt_vault_txn_id)
    return self.process_pmt_vault_txn_row(pmt_vault_txn)

  def process_pmt_vault_txn_row(self, pmt_vault_txn):
    """Process a vault transaction from a row.

    Should only be called on payment processing server, not on web server for customers (e.g. pay.microoffice.com).

    Args:
      pmt_vault_txn (dict): a dict containing a row from pmt_vault_txn_vw (must be the view, not a table).

    Returns:
      resulting row from pmt_vault_txn_vw after transaction completed.

    Raises:
      UnknownPaymentMethodError: if payment method is unknown or unhandled.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    self.pmt_gateway.clear()
    if self.demo_mode:
      self.pmt_gateway.set_username('demo')
    else:
      self.pmt_gateway.set_username(pmt_vault_txn['processor_username'])
    if pmt_vault_txn['hold']:
      raise HeldError('transaction in pmt_vault_txn_vw has hold set to True; row ID %s' % pmt_vault_txn['id'])
    if pmt_vault_txn['request_posted_ts'] != None or pmt_vault_txn['response'] != None or pmt_vault_txn['transactionid'] != None:
      raise AlreadyProcessedError('transaction in pmt_vault_txn_vw already processed; row ID %s' % pmt_vault_txn['id'])
    import decrypt
    decrypt_client = decrypt.DecryptClient()
    decrypted_row = decrypt_client.decrypt_row('pmt_vault_txn_vw', pmt_vault_txn['id'])
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    if pmt_vault_txn['pmt_method'] == 'ach':
      pmt_nmi_txn_ach = self.insert_pmt_nmi_txn_ach(pmt_vault_txn['cust_acct_id'], pmt_vault_txn['account_type'], pmt_vault_txn['owned_by'], pmt_vault_txn['registered_name'], decrypted_row['routing_number'], decrypted_row['account_number'], dbutil.cast_money_to_decimal(pmt_vault_txn['amount']), pmt_vault_txn['scheduled_date'], pmt_vault_txn['note'], pmt_vault_txn['email'], do_commit=False)
      curdict.execute('UPDATE pmt_nmi_txn_ach_tbl SET pmt_vault_txn_id = %s WHERE id = %s', (pmt_vault_txn['id'], pmt_nmi_txn_ach['id']))
      pmt_nmi_txn = self.process_pmt_nmi_txn(pmt_nmi_txn_ach['id'], routing_number=decrypted_row['routing_number'], account_number=decrypted_row['account_number'])
    elif pmt_vault_txn['pmt_method'] == 'card':
      pmt_nmi_txn_card = self.insert_pmt_nmi_txn_card(pmt_vault_txn['cust_acct_id'], decrypted_row['card_number'], pmt_vault_txn['expire_date'], decrypted_row['cvv'], pmt_vault_txn['billing_first_name'], pmt_vault_txn['billing_last_name'], pmt_vault_txn['billing_company'], pmt_vault_txn['billing_address_1'], pmt_vault_txn['billing_address_2'], pmt_vault_txn['billing_address_num'], pmt_vault_txn['billing_city'], pmt_vault_txn['billing_state'], pmt_vault_txn['billing_zip_code'], pmt_vault_txn['billing_country'], dbutil.cast_money_to_decimal(pmt_vault_txn['amount']), dbutil.cast_money_to_decimal(pmt_vault_txn['convenience_fee']), pmt_vault_txn['scheduled_date'], pmt_vault_txn['note'], pmt_vault_txn['email'], do_commit=False)
      curdict.execute('UPDATE pmt_nmi_txn_card_tbl SET pmt_vault_txn_id = %s WHERE id = %s', (pmt_vault_txn['id'], pmt_nmi_txn_card['id']))
      pmt_nmi_txn = self.process_pmt_nmi_txn(pmt_nmi_txn_card['id'], card_number=decrypted_row['card_number'], cvv=decrypted_row['cvv'])
    else:
      raise UnknownPaymentMethodError('unknown payment method %s; must be ach or card' % pmt_vault_txn['pmt_method'])
    curdict.execute('SELECT * FROM pmt_vault_txn_vw WHERE id = %s', (pmt_vault_txn['id'], ))
    pmt_vault_txn = curdict.fetchone()
    curdict.close()
    return pmt_vault_txn

  def process_pmt_vault_txn_as_client(self, pmt_vault_txn_id):
    """Process a vault transaction from an ID value and as a client (web server facing the customer).

    Should only be called on web server for customers (e.g. pay.microoffice.com), not on the payment processing server.

    Args:
      pmt_vault_txn_id (int): ID value of a row in pmt_vault_txn_vw.

    Returns:
      resulting row from pmt_vault_txn_vw after transaction completed.
    """
    responseIO = io.BytesIO()
    curlObj = pycurl.Curl()
    curlObj.setopt(pycurl.POST, 1)
    # set connect timeout to 15 seconds
    curlObj.setopt(pycurl.CONNECTTIMEOUT, 15)
    # set timeout to 15 seconds
    curlObj.setopt(pycurl.TIMEOUT, 15)
    curlObj.setopt(pycurl.HEADER, 0)
    curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
    curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
    curlObj.setopt(pycurl.WRITEFUNCTION, responseIO.write)
    url = '{{ payapi_base_url }}/process_vault_txn'
    curlObj.setopt(pycurl.URL, url)
    query = ''
    query += 'pmt_vault_txn_id=' + urllib.parse.quote(str(pmt_vault_txn_id))
    if self.demo_mode:
      query += '&'
      query += 'demo_mode=1'
    curlObj.setopt(pycurl.POSTFIELDS, query)
    curlObj.perform()
    data = responseIO.getvalue().decode('utf-8')
    pmt_vault_txn = json.loads(data)
    import dateutil.parser
    pmt_vault_txn['scheduled_date'] = dateutil.parser.parse(pmt_vault_txn['scheduled_date']).date()
    pmt_vault_txn['scheduled_ts_utc'] = dateutil.parser.parse(pmt_vault_txn['scheduled_ts_utc'])
    if 'expire_date' in pmt_vault_txn and pmt_vault_txn['expire_date']:
      pmt_vault_txn['expire_date'] = dateutil.parser.parse(pmt_vault_txn['expire_date']).date()
    pmt_vault_txn['request_posted_ts'] = dateutil.parser.parse(pmt_vault_txn['request_posted_ts'])
    pmt_vault_txn['created_ts'] = dateutil.parser.parse(pmt_vault_txn['created_ts'])
    if 'modified_ts' in pmt_vault_txn and pmt_vault_txn['modified_ts']:
      pmt_vault_txn['modified_ts'] = dateutil.parser.parse(pmt_vault_txn['modified_ts'])
    return pmt_vault_txn

  def process_confirm_txn_ach(self, pmt_vault_id):
    """Sends and inserts confirmation transactions from a pmt_vault_ID value.

    Should only be called on payment processing server, not on web server for customers (e.g. pay.microoffice.com).

    Args:
      pmt_vault_id (int): ID value of a row/account in pmt_vault_vw.

    Returns:
      true or false, depending on whether both confirmation transactions were successful

    Raises:
      PaymentTransactionNotFoundError: if payment transaction not found in pmt_vault_txn_vw.
      UnknownPaymentMethodError: if payment method is unknown or unhandled.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('INSERT INTO pmt_method_confirm_tbl (pmt_vault_id) VALUES (%s) RETURNING *', (pmt_vault_id,))
    pmt_method_confirm = curdict.fetchone()
    curdict.execute('SELECT * from pmt_vault_vw WHERE id = %s', (pmt_vault_id,))
    pmt_vault_acc = curdict.fetchone()
    if not pmt_vault_acc:
      raise UnknownPaymentMethodError('row in pmt_vault_txn_vw with ID %s not found' % pmt_vault_txn_id)
    self.pmt_gateway.clear()
    if self.demo_mode:
      self.pmt_gateway.set_username('demo')
    else: # TODO: get cust acct, then building, then username
      curdict.execute('SELECT * FROM cust_acct_tbl WHERE id = %s', (pmt_vault_acc['cust_acct_id'],))
      cust_acct = curdict.fetchone()
      curdict.execute('SELECT * FROM floor_tbl WHERE id = %s', (cust_acct['primary_floor_id'],))
      floor = curdict.fetchone()
      curdict.execute('SELECT * FROM building_tbl WHERE id = %s', (floor['building_id'],))
      building = curdict.fetchone()
      self.pmt_gateway.set_username(building['nmi_username'])
    curdict.close()
    a = random.randint(1,99)
    b = random.randint(1,99)
    while (b == a):
      b = random.randint(1,99)
    a = decimal.Decimal(a) / decimal.Decimal(100)
    b = decimal.Decimal(b) / decimal.Decimal(100)
    import decrypt
    decrypt_client = decrypt.DecryptClient()
    decrypted_row = decrypt_client.decrypt_row('pmt_vault_vw', pmt_vault_acc['id'])
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    if pmt_vault_acc['pmt_method'] == 'ach':
      curdict.execute('INSERT INTO pmt_nmi_txn_ach_tbl (cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, email, txn_purpose, pmt_method_confirm_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *', (pmt_vault_acc['cust_acct_id'], pmt_vault_acc['account_type'], pmt_vault_acc['owned_by'], pmt_vault_acc['registered_name'], pmt_vault_acc['routing_number'], decrypted_row['account_number'], -a, datetime.datetime.now(), None, pmt_vault_acc['email_address'], 'confirm', pmt_method_confirm['id']))
      pmt_nmi_txn_ach_1 = curdict.fetchone()
      curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn_ach_1['id'],))
      pmt_nmi_txn_1 = curdict.fetchone()
      curdict.execute('INSERT INTO pmt_nmi_txn_ach_tbl (cust_acct_id, account_type, owned_by, registered_name, routing_number, account_number, amount, scheduled_date, note, email, txn_purpose, pmt_method_confirm_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *', (pmt_vault_acc['cust_acct_id'], pmt_vault_acc['account_type'], pmt_vault_acc['owned_by'], pmt_vault_acc['registered_name'], pmt_vault_acc['routing_number'], decrypted_row['account_number'], -b, datetime.datetime.now(), None, pmt_vault_acc['email_address'], 'confirm', pmt_method_confirm['id']))
      pmt_nmi_txn_ach_2 = curdict.fetchone()
      curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn_ach_2['id'],))
      pmt_nmi_txn_2 = curdict.fetchone()
      self.dbconn.commit()
      pmt_nmi_txn_1_response = self.credit_pmt_nmi_txn_ach(pmt_nmi_txn_1, routing_number=pmt_vault_acc['routing_number'], account_number=decrypted_row['account_number'])
      pmt_nmi_txn_2_response = self.credit_pmt_nmi_txn_ach(pmt_nmi_txn_2, routing_number=pmt_vault_acc['routing_number'], account_number=decrypted_row['account_number'])
    #card functionality not implemented
    else:
      raise UnknownPaymentMethodError('Invalid payment method %s; must be ach' % pmt_vault_acc['pmt_method'])
    return (pmt_nmi_txn_1_response == '1') and (pmt_nmi_txn_2_response == '1')

  def credit_pmt_nmi_txn_ach(self, pmt_nmi_txn, routing_number=None, account_number=None):
    """Process an NMI/Solupay ACH confirmation transaction from a row, i.e. credit the money to the account

    If routing_number or account_number missing from args, load both from pmt_nmi_txn_vw (must load decrypt module in that case).

    Args:
      pmt_nmi_txn (dict): a dict containing a row from pmt_nmi_txn_vw (must be the view, not a table).
      routing_number (optional[str]): 9 digit routing number
      account_number (optional[str]): numeric account number

    Returns:
      response from merchant processor

    Raises:
      BadPaymentMethodError: If payment method is not 'ach'.
      HeldError: If payment transaction has hold flag set to True.
      AlreadyProcessedError: If payment transaction has already been processed.
      ScheduledFutureError: If payment transaction is scheduled in the future.
      pycurl.error: if problem communicating with NMI payment gateway.
    """
    if pmt_nmi_txn['pmt_method'] != 'ach':
      raise BadPaymentMethodError('payment method must be ach for process_nmi_txn_ach, not %s' % pmt_nmi_txn['pmt_method'])
    if pmt_nmi_txn['hold']:
      raise HeldError('transaction in pmt_nmi_txn_ach_tbl has hold set to True; row ID %s' % pmt_nmi_txn['id'])
    if pmt_nmi_txn['request_posted_ts'] != None or pmt_nmi_txn['response'] != None or pmt_nmi_txn['transactionid'] != None:
      raise AlreadyProcessedError('transaction in pmt_nmi_txn_ach_tbl already processed; row ID %s' % pmt_nmi_txn['id'])
    # check if current time + ACH delay is past the scheduled datetime
    if pmt_nmi_txn['scheduled_ts_utc'] > (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(hours=self.ach_delay_hours)):
      raise ScheduledFutureError('transaction in pmt_nmi_txn_ach_tbl has scheduled date in future; row ID %s' % pmt_nmi_txn['id'])
    self.pmt_gateway.clear()
    if self.demo_mode:
      self.pmt_gateway.set_username('demo')
    else:
      self.pmt_gateway.set_username(pmt_nmi_txn['nmi_username'])
    if not routing_number or not account_number:
      import decrypt
      decrypt_client = decrypt.DecryptClient()
      decrypted_row = decrypt_client.decrypt_row('pmt_nmi_txn_vw', pmt_nmi_txn['id'])
      routing_number = decrypted_row['routing_number']
      account_number = decrypted_row['account_number']
    http_txn_dict = self.pmt_gateway.do_ach_credit(pmt_nmi_txn['account_type'], pmt_nmi_txn['owned_by'], pmt_nmi_txn['registered_name'], routing_number, account_number, dbutil.cast_money_to_decimal(pmt_nmi_txn['amount']), order_id=pmt_nmi_txn['id'], order_description='cust_acct_id %s, cust_acct_name %s' % (pmt_nmi_txn['cust_acct_id'], pmt_nmi_txn['name']))
    query = http_txn_dict['query']
    # mask sensitive info
    query = re.sub(r'^password=([^&]*)|&password=([^&]*)', r'password=***', query)
    query = re.sub(r'^checkaba=([^&]*)|&checkaba=([^&]*)', r'checkaba=***', query)
    query = re.sub(r'^checkaccount=([^&]*)|&checkaccount=([^&]*)', r'checkaccount=***', query)
    response_body = http_txn_dict['response_body']
    response_dict = http_txn_dict['response_dict']
    extra_column_assign = ''
    extra_column_values = []
    for column_name in ['response', 'responsetext', 'authcode', 'transactionid', 'avsresponse', 'response_code', 'order_description', 'cvvresponse', 'orderid']:
      if column_name in response_dict:
        extra_column_assign += ', ' + column_name + ' = %s'
        extra_column_values.append(response_dict[column_name])
    cur = self.dbconn.cursor()
    cur.execute('UPDATE pmt_nmi_txn_ach_tbl SET request_posted_ts = now(), request_body = %s, response_body = %s' + extra_column_assign + ' WHERE id = %s', (query, response_body) + tuple(extra_column_values) + (pmt_nmi_txn['id'], ))
    self.dbconn.commit()
    if 'transactionid' in response_dict:
      query_returned = False
      num_query_attempts = 0
      while not query_returned and num_query_attempts < self.max_query_attempts:
        try:
          num_query_attempts += 1
          http_txn_dict = self.pmt_gateway.run_query(transaction_id=response_dict['transactionid'])
          query_returned = True
        except pycurl.error:
          pass
      if query_returned:
        txn_xml = http_txn_dict['response_body']
        cur.execute('UPDATE pmt_nmi_txn_ach_tbl SET txn_xml = xmlparse(document %s) WHERE id = %s', (txn_xml, pmt_nmi_txn['id']))
        self.dbconn.commit()
    cur.close()
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn['id'],))
    pmt_nmi_txn = curdict.fetchone()
    curdict.close()
    return response_dict['response']

  def process_confirm_txn_ach_as_client(self, pmt_vault_id):
    """Process 2 confirmation transactions from an pmt_vault_id value and as a client (web server facing the customer).

    Should only be called on web server for customers (e.g. pay.microoffice.com), not on the payment processing server.

    Args:
      pmt_vault_id (int): ID value of the (ach) account to be confirmed

    Returns:
      true or false, depending on whether both confirmation transactions were successful
    """
    responseIO = io.BytesIO()
    curlObj = pycurl.Curl()
    curlObj.setopt(pycurl.POST, 1)
    # set connect timeout to 15 seconds
    curlObj.setopt(pycurl.CONNECTTIMEOUT, 15)
    # set timeout to 15 seconds
    curlObj.setopt(pycurl.TIMEOUT, 15)
    curlObj.setopt(pycurl.HEADER, 0)
    curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
    curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
    curlObj.setopt(pycurl.WRITEFUNCTION, responseIO.write)
    url = '{{ payapi_base_url }}/process_confirm_txn'
    curlObj.setopt(pycurl.URL, url)
    query = ''
    query += 'pmt_vault_id=' + urllib.parse.quote(str(pmt_vault_id))
    if self.demo_mode:
      query += '&'
      query += 'demo_mode=1'
    curlObj.setopt(pycurl.POSTFIELDS, query)
    curlObj.perform()
    data = responseIO.getvalue().decode('utf-8')
    pmt_vault_txn_status = json.loads(data)
    return pmt_vault_txn_status

  def void_confirmation_txn(self, pmt_nmi_txn_id):
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn_id,))
    txn = curdict.fetchone()
    self.pmt_gateway.clear()
    if self.demo_mode:
      self.pmt_gateway.set_username('demo')
    else:
      self.pmt_gateway.set_username(txn['nmi_username'])
    http_txn_dict = self.pmt_gateway.void_ach_txn(txn['transactionid'], order_id=txn['id'], order_description='cust_acct_id %s, cust_acct_name %s' % (txn['cust_acct_id'], txn['name']))
    response_body = http_txn_dict['response_body']
    response_dict = http_txn_dict['response_dict']
    cur.close()
    sync_txn_action_lib.sync_txn_actions(response_body, self.dbconn)
    curdict = self.dbconn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE id = %s', (txn['id'],))
    pmt_nmi_txn = curdict.fetchone()
    curdict.close()
    return(response_dict['response'])

class PayeezyFacade():
  """
  Facade combining Payeezy gateway, database access/update, and email
  """

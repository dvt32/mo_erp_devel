import psycopg2
import decimal
import re

# OID values for money type from pg_type catalog
pg_money_type_oid_tuple = (790, 791)

def cast_money_to_decimal(s, cur=None):
  '''Cast PostgreSQL money type to decimal.Decimal by stripping localizations from money as string.

  Args:
    s (str): localized money as string
    cur (optional [psycopg2.extensions.cursor]): database cursor object

  Returns:
    decimal.Decimal object with value of money column

  '''
  if s is None: return None
  if isinstance(s, decimal.Decimal):
    # if s is already of type decimal.Decimal, just return s
    return s
  else:
    # otherwise strip out localizations and convert to decimal.Decimal
    return decimal.Decimal(s.replace(',','').replace('$',''))

def register_money_caster(conn):
  '''Register the cast money function as type handler for money type in PostgreSQL.

  Args:
    conn (psycopg2.extensions.connection): database connection object
  '''
  money_caster = psycopg2.extensions.new_type(pg_money_type_oid_tuple, 'money_to_decimal', cast_money_to_decimal)
  psycopg2.extensions.register_type(money_caster, conn)

class BadTableNameError(Exception):
  """Exception raised when table name is not valid PostgreSQL identifier."""
  pass

def insert_row_from_dict(cursor, tbl_name, row):
  '''Insert a row into a table using a dict or OrderedDict. Use OrderedDict if column order matters (e.g. for logging).

  Does not issue connection.commit, so if commit is desired, do so after calling this function.

  Args:
    cursor (psycopg2.extensions.cursor): psycopg2 cursor object (needs execute method).
    tbl_name (str): name of the table to insert data into.
    row (dict or OrderedDict): dict object containing column names and values (use OrderedDict if column order matters).

  Raises:
    BadTableNameError: if table name is invalid PostgreSQL identifier.
  '''
  # check if tbl_name is valid PostgreSQL identifier
  if not re.fullmatch('[a-zA-Z0-9_]+', tbl_name):
    raise BadTableNameError('bad table name in insert_row_from_dict: ' + tbl_name)
  column_names = row.keys()
  cursor.execute('INSERT INTO ' + tbl_name + ' (' + ', '.join(column_names) + ') VALUES (' + ', '.join(list(map(lambda x: '%(' + x + ')s', column_names))) + ')', row)

import psycopg2.extras
import datetime
import decimal
from dateutil.relativedelta import relativedelta
import chargefacade
import dbutil
from email.mime.text import MIMEText
import email.utils
import email.header
import smtplib
import database

def debit_back_confirmation_amounts(pmt_vault_id, single_amount=None):
  ''' Function to debit back the confirmation amounts that have been credited to a customers account. Only charges if the account has been confirmed, or if the pmt confirm window has passed.

  Args: pmt_vault_id : the pmt vault id of the card 

  Return: nothing, but print out or send errors if failed

  '''
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute("SELECT * FROM pmt_method_confirm_tbl WHERE id in (SELECT DISTINCT pmt_method_confirm_tbl.id FROM pmt_method_confirm_tbl, pmt_nmi_txn_vw WHERE pmt_method_confirm_tbl.id = pmt_nmi_txn_vw.pmt_method_confirm_id AND pmt_method_confirm_tbl.pmt_vault_id = %s AND pmt_nmi_txn_vw.amount < 0::money AND pmt_nmi_txn_vw.request_posted_ts < (now() - interval '{{pmt_confirm_window}} days'))", (pmt_vault_id,))
  confirm = curdict.fetchone()
  curdict.execute('SELECT * FROM pmt_vault_vw WHERE id = %s and confirmed_ts IS NOT NULL', (pmt_vault_id,))
  ach = curdict.fetchone()
  if not ach and not confirm:
    if not confirm:
      print("{{pmt_confirm_window}} days has not yet passed for the ACH account confirmation with id:", pmt_vault_id)
      return
    if not ach:
      print("ACH not yet confirmed:", dict(ach))
      return
  curdict.execute('SELECT * FROM pmt_method_confirm_tbl WHERE pmt_vault_id = %s', (pmt_vault_id,))
  confirm = curdict.fetchone()
  curdict.execute('SELECT * FROM pmt_nmi_txn_ach_tbl WHERE txn_purpose = %s AND pmt_method_confirm_id = %s', ('confirm', confirm['id']))
  txns = [dict(row) for row in curdict]
  if not single_amount:
    amounts = [abs(dbutil.cast_money_to_decimal(row['amount'])) for row in txns]
    am = amounts[0] + amounts[1]
  else:
    am = abs(dbutil.cast_money_to_decimal(single_amount))
  nmi_facade = chargefacade.NmiFacade(conn)
  pmt_vault_txn = nmi_facade.insert_pmt_vault_txn(pmt_vault_id, am, decimal.Decimal(0), datetime.datetime.now(), None, None)
  result_pmt_vault_txn = nmi_facade.process_pmt_vault_txn_as_client(pmt_vault_txn['id'])
  if result_pmt_vault_txn['response'] != '1':
    print('Debit confirmation transaction failed:', dict(result_pmt_vault_txn))
    mail_body = '''
There was an error trying to debit the following confirmation transaction:
Customer Name: %s
Customer ID: %s
Amount: %s
Payment Method: %s
Routing Number: %s
Account Number: %s
Account Type: %s
Owned By: %s
Regiseterd Name: %s
Transaction Response: %s
Transaction ID: %s''' % (result_pmt_vault_txn['name'], result_pmt_vault_txn['cust_acct_id'], result_pmt_vault_txn['amount'], result_pmt_vault_txn['pmt_method'], result_pmt_vault_txn['routing_number'], result_pmt_vault_txn['account_number'], result_pmt_vault_txn['account_type'], result_pmt_vault_txn['owned_by'], result_pmt_vault_txn['registered_name'], result_pmt_vault_txn['responsetext'], result_pmt_vault_txn['transactionid'])
    mail_msg = MIMEText(mail_body)
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = '{{ payment_mail_from }}'
    mail_msg['To'] = '{{ payment_mail_to }}'
    mail_msg['Subject'] = email.header.Header('Error in Debiting Back Customer Confirmation Amounts', 'utf-8')
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.send_message(mail_msg)
    mailer.quit()
  return

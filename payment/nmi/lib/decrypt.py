import pycurl
import urllib
import urllib.parse
import io
import json

class DecryptClient():
  """
  Client to decrypt rows in the database.
  """

  crypto_base_url = '{{ crypto_base_url }}';

  def __init__(self):
    pass

  def decrypt_row(self, table_name, row_id, http_method='GET'):
    if {{ is_production_deployment }} == 1:
      query = ''
      query += 'tbl=' + urllib.parse.quote(str(table_name)) + '&'
      query += 'id=' + urllib.parse.quote(str(row_id)) + '&'
      query += 'request_user_id=41'
      responseIO = io.BytesIO()
      curlObj = pycurl.Curl()
      if http_method == 'GET':
        curlObj.setopt(pycurl.HTTPGET,1)
        curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
        curlObj.setopt(pycurl.TIMEOUT,15)
        curlObj.setopt(pycurl.HEADER,0)
        curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
        curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
        curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write);
        curlObj.setopt(pycurl.URL, self.crypto_base_url + '/decrypt_row' + '?' + query)
      elif http_method == 'POST':
        curlObj.setopt(pycurl.POST,1)
        curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
        curlObj.setopt(pycurl.TIMEOUT,15)
        curlObj.setopt(pycurl.HEADER,0)
        curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
        curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
        curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write);
        curlObj.setopt(pycurl.URL, self.crypto_base_url + '/decrypt_row')
        curlObj.setopt(pycurl.POSTFIELDS,query)
      else:
        raise Exception('Unsupported HTTP method: ' + http_method)
      curlObj.perform()
      data = responseIO.getvalue().decode('utf-8')
      return json.loads(data)
    elif {{ is_production_deployment }} == 0:
      data = {
        "card_number": "4111111111111111",
        "cvv": "999",
        "routing_number": "122100024",
        "account_number": "123456789"
      }
      return data
    

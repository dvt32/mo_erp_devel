import pycurl
import urllib
import urllib.parse
import io
import re

class Gateway():
  '''Connector to NMI/Solupay payment gateway
  '''

  nmi_credential_dict = {}
  if '{{ nmi_username_1 }}' != '':
    nmi_credential_dict['{{ nmi_username_1 }}'] = {'password': '{{ nmi_password_1 }}', 'nmi_group' : '{{ nmi_group_1 }}'}
  if '{{ nmi_username_2 }}' != '':
    nmi_credential_dict['{{ nmi_username_2 }}'] = {'password': '{{ nmi_password_2 }}', 'nmi_group' : '{{ nmi_group_2 }}'}
  if '{{ nmi_username_3 }}' != '':
    nmi_credential_dict['{{ nmi_username_3 }}'] = {'password': '{{ nmi_password_3 }}', 'nmi_group' : '{{ nmi_group_3 }}'}
  if '{{ nmi_username_4 }}' != '':
    nmi_credential_dict['{{ nmi_username_4 }}'] = {'password': '{{ nmi_password_4 }}', 'nmi_group' : '{{ nmi_group_4 }}'}
  if '{{ nmi_username_5 }}' != '':
    nmi_credential_dict['{{ nmi_username_5 }}'] = {'password': '{{ nmi_password_5 }}', 'nmi_group' : '{{ nmi_group_5 }}'}
  nmi_credential_dict['demo'] = {'password': 'password'}

  nmi_transact_url = 'https://secure.nmi.com/api/transact.php'
  nmi_query_url = 'https://solupay.transactiongateway.com/api/query.php'
  nmi_demo_transact_url = 'https://solupay.transactiongateway.com/api/transact.php'

  # pass in username='demo' to use the demo gateway
  def __init__(self, username=None):
    '''Constructor for Gateway class.

    Args:
      username (optional [str]): NMI username to use; "demo" for demo account with demo URL.
    '''
    self.login = dict()
    if username != None:
      self.set_username(username)
    self.vault = dict()
    self.vault_sale = dict()
    self.ach_sale = dict()
    self.card_sale = dict()

  def set_username(self, username):
    self.login['username'] = username
    if username in self.nmi_credential_dict:
      self.login['password'] = self.nmi_credential_dict[username]['password']
    else:
      raise Exception('credential dict lacks key for username: ' + username)

  def clear(self):
    self.login.clear()
    self.vault.clear()
    self.vault_sale.clear()
    self.ach_sale.clear()
    self.card_sale.clear()

  def missing_param(self, param_name):
    raise Exception('required ' + param_name + ' missing')

  # successful add:
  # {'customer_vault_id': '2373',
  #  'response': '1',
  #  'response_code': '100',
  #  'responsetext': 'Customer Added'}

  # successful update:
  # {'customer_vault_id': '2373',
  #  'response': '1',
  #  'response_code': '100',
  #  'responsetext': 'Customer Update Successful'}

  # attempt to add an already-existing customer vault entry:
  # {'response': '3',
  #  'response_code': '300',
  #  'responsetext': 'Duplicate Customer Vault Id REFID:3181259527'}

  # attempt to update a nonexistent customer vault entry:
  # {'customer_vault_id': '2373',
  #  'response': '3',
  #  'response_code': '300',
  #  'responsetext': 'Invalid Customer Vault Id REFID:3181259579'}

  def set_customer_card(self, customer_vault_id, ccnumber, ccexp, cvv, first_name, last_name, company, address1, address2, city, state, zip_code, country, phone, fax, email):
    http_txn_dict = self.customer_card_op('add_customer', customer_vault_id, ccnumber, ccexp, cvv, first_name, last_name, company, address1, address2, city, state, zip_code, country, phone, fax, email)
    response_dict = http_txn_dict['response_dict']
    if response_dict['response_code'] == '100':
      # successful add
      pass
    elif response_dict['response_code'] == '300' and response_dict['responsetext'][:27] == 'Duplicate Customer Vault Id':
      # add operation responded with duplicate error, so try update
      http_txn_dict = self.customer_card_op('update_customer', customer_vault_id, ccnumber, ccexp, cvv, first_name, last_name, company, address1, address2, city, state, zip_code, country, phone, fax, email)
    return http_txn_dict
    
  def customer_card_op(self, operation, customer_vault_id, ccnumber, ccexp, cvv, first_name, last_name, company, address1, address2, city, state, zip_code, country, phone, fax, email):
    '''Add a customer credit card to the vault (no shipping)

    Args:
      operation (str): add_customer or update_customer
      customer_vault_id (str): unique identifier for customer vault entry.
      ccnumber (str): credit card number.
      ccexp (str): credit card expiration in MMYY or YYYY-MM-DD format. converted to MMYY format.
      cvv (str): card security code
      first_name (str): cardholder's first name.
      last_name (str): cardholder's last name.
      company (str): cardholder's company.
      address1 (str): card billing address.
      address2 (str): card billing address, line 2.
      city (str): card billing city.
      state (str): card billing state (two character abbreviation).
      zip_code (str): card billing postal code.
      country (str): card billing country code (empty for US). ISO 3166-1 alpha-2 (2 character abbreviation).
      phone (str): billing phone number.
      fax (str): billing fax number.
      email (str): billing email address.
    '''
    if operation not in ('add_customer', 'update_customer'):
      raise Exception('operation unsupported: ' + operation)
    self.vault['customer_vault'] = operation
    self.vault['customer_vault_id'] = customer_vault_id or self.missing_param('customer_vault_id')
    self.vault['ccnumber'] = ccnumber or self.missing_param('ccnumber')
    if not ccexp:
      self.missing_param('ccexp')
    match = re.match(r'(\d\d)(\d\d)-(\d\d)-(\d\d)', str(ccexp))
    if match:
      # YYYY-MM-DD format, so convert it to MMYY format
      self.vault['ccexp'] = match.group(3) + match.group(2)
    else:
      match = re.match(r'(\d\d)(\d\d)', str(ccexp))
      if match:
        # already in MMYY format, so just assign as-is
        self.vault['ccexp'] = str(ccexp)
      else:
        raise Exception('ccexp must be YYYY-MM-DD or MMYY format; you entered: ' + str(ccexp))
    self.vault['cvv'] = cvv or ''
    self.vault['payment'] = 'creditcard'
    self.vault['first_name'] = first_name or self.missing_param('first_name')
    self.vault['last_name'] = last_name or self.missing_param('last_name')
    self.vault['company'] = company or ''
    self.vault['address1'] = address1 or self.missing_param('address1')
    self.vault['address2'] = address2 or ''
    self.vault['city'] = city or self.missing_param('city')
    self.vault['state'] = state
    if country and country == 'US' and not state:
      self.missing_param('state')
    self.vault['zip'] = zip_code or self.missing_param('zip_code')
    self.vault['country'] = country or ''
    self.vault['phone'] = phone or ''
    self.vault['fax'] = fax or ''
    self.vault['email'] = email or ''
    return self.act_on_vault()

  def customer_card_op_with_ship(self, operation, customer_vault_id, ccnumber, ccexp, cvv, first_name, last_name, company, address1, address2, city, state, zip_code, country, phone, fax, email, shipping_firstname, shipping_lastname, shipping_company, shipping_address1, shipping_address2, shipping_city, shipping_state, shipping_zip, shipping_country, shipping_phone, shipping_fax, shipping_email):
    '''Add a customer credit card to the vault (with shipping)

    Args:
      operation (str): add_customer or update_customer
      customer_vault_id (str): unique identifier for customer vault entry.
      ccnumber (str): credit card number.
      ccexp (str): credit card expiration in MMYY format.
      cvv (str): card security code
      first_name (str): cardholder's first name.
      last_name (str): cardholder's last name.
      company (str): cardholder's company.
      address1 (str): card billing address.
      address2 (str): card billing address, line 2.
      city (str): card billing city.
      state (str): card billing state (two character abbreviation).
      zip_code (str): card billing postal code.
      country (str): card billing country code (empty for US). ISO 3166-1 alpha-2 (2 character abbreviation).
      phone (str): billing phone number.
      fax (str): billing fax number.
      email (str): billing email address.
      shipping_firstname (str): shipping first name.
      shipping_lastname (str): shipping last name.
      shipping_company (str): shipping company.
      shipping_address1 (str): shipping address.
      shipping_address2 (str): shipping address, line 2.
      shipping_city (str): shipping city.
      shipping_state (str): shipping state.
      shipping_zip (str): shipping postal code.
      shipping_country (str): shipping country code. ISO 3166-1 alpha-2 (2 character abbreviation).
      shipping_phone (str): shipping phone number.
      shipping_fax (str): shipping fax number.
      shipping_email (str): shipping email address.
    '''
    if operation not in ('add_customer', 'update_customer'):
      raise Exception('operation unsupported: ' + operation)
    self.vault['customer_vault'] = operation
    self.vault['customer_vault_id'] = customer_vault_id or self.missing_param('customer_vault_id')
    self.vault['ccnumber'] = ccnumber or self.missing_param('ccnumber')
    if not ccexp:
      self.missing_param('ccexp')
    match = re.match(r'(\d\d)(\d\d)-(\d\d)-(\d\d)', str(ccexp))
    if match:
      # YYYY-MM-DD format, so convert it to MMYY format
      self.vault['ccexp'] = match.group(3) + match.group(2)
    else:
      match = re.match(r'(\d\d)(\d\d)', str(ccexp))
      if match:
        # already in MMYY format, so just assign as-is
        self.vault['ccexp'] = str(ccexp)
      else:
        raise Exception('ccexp must be YYYY-MM-DD or MMYY format; you entered: ' + str(ccexp))
    self.vault['cvv'] = cvv or ''
    self.vault['payment'] = 'creditcard'
    self.vault['first_name'] = first_name or self.missing_param('first_name')
    self.vault['last_name'] = last_name or self.missing_param('last_name')
    self.vault['company'] = company or ''
    self.vault['address1'] = address1 or self.missing_param('address1')
    self.vault['address2'] = address2 or ''
    self.vault['city'] = city or self.missing_param('city')
    self.vault['state'] = state
    if country and country == 'US' and not state:
      self.missing_param('state')
    self.vault['zip'] = zip_code or self.missing_param('zip_code')
    self.vault['country'] = country or ''
    self.vault['phone'] = phone or ''
    self.vault['fax'] = fax or ''
    self.vault['email'] = email or ''
    self.vault['shipping_firstname'] = shipping_firstname or ''
    self.vault['shipping_lastname'] = shipping_lastname or ''
    self.vault['shipping_company'] = shipping_company or ''
    self.vault['shipping_address1'] = shipping_address1 or ''
    self.vault['shipping_address2'] = shipping_address2 or ''
    self.vault['shipping_city'] = shipping_city or ''
    self.vault['shipping_state'] = shipping_state or ''
    self.vault['shipping_zip'] = shipping_zip or ''
    self.vault['shipping_country'] = shipping_country or ''
    self.vault['shipping_phone'] = shipping_phone or ''
    self.vault['shipping_fax'] = shipping_fax or ''
    self.vault['shipping_email'] = shipping_email or ''
    return self.act_on_vault()

  def set_customer_ach(self, customer_vault_id, checkname, checkaba, checkaccount, account_holder_type, account_type):
    http_txn_dict = self.customer_ach_op('add_customer', customer_vault_id, checkname, checkaba, checkaccount, account_holder_type, account_type)
    response_dict = http_txn_dict['response_dict']
    if response_dict['response_code'] == '100':
      # successful add
      pass
    elif response_dict['response_code'] == '300' and response_dict['responsetext'][:27] == 'Duplicate Customer Vault Id':
      # add operation responded with duplicate error, so try update
      http_txn_dict = self.customer_ach_op('update_customer', customer_vault_id, checkname, checkaba, checkaccount, account_holder_type, account_type)
    return http_txn_dict
    
  def customer_ach_op(self, operation, customer_vault_id, checkname, checkaba, checkaccount, account_holder_type, account_type):
    '''Add a customer bank account to the vault

    Args:
      operation (str): add_customer or update_customer
      customer_vault_id (str): unique identifier for customer vault entry.
      checkname (str): name on the customer's ACH account.
      checkaba (str): customer's bank routing number.
      checkaccount (str): customer's bank account number.
      account_holder_type (str): customer's ACH account entity (personal or business).
      account_type (str): customer's ACH account type (checking or savings).
    '''
    if operation not in ('add_customer', 'update_customer'):
      raise Exception('operation unsupported: ' + operation)
    self.vault['customer_vault'] = operation
    self.vault['customer_vault_id'] = customer_vault_id
    self.vault['checkname'] = checkname
    self.vault['checkaba'] = checkaba
    self.vault['checkaccount'] = checkaccount
    self.vault['account_holder_type'] = account_holder_type
    self.vault['account_type'] = account_type
    return self.act_on_vault()

  def delete_customer(self, customer_vault_id):
    self.vault['customer_vault'] = 'delete_customer'
    self.vault['customer_vault_id'] = customer_vault_id
    return self.act_on_vault()

  def do_vault_sale(self, customer_vault_id, amount, order_id=None, order_description=None):
    self.vault_sale['customer_vault_id'] = customer_vault_id
    self.vault_sale['amount'] = amount
    if order_id:
      self.vault_sale['orderid'] = order_id
    if order_description:
      self.vault_sale['order_description'] = order_description
    return self.act_on_vault_sale()

  def do_ach_sale(self, account_type, owned_by, registered_name, routing_number, account_number, amount, order_id=None, order_description=None):
    self.ach_sale['type'] = 'sale'
    self.ach_sale['payment'] = 'check'
    self.ach_sale['checkname'] = registered_name
    self.ach_sale['checkaba'] = routing_number
    self.ach_sale['checkaccount'] = account_number
    self.ach_sale['account_holder_type'] = owned_by
    self.ach_sale['account_type'] = account_type
    self.ach_sale['amount'] = amount
    if order_id:
      self.ach_sale['orderid'] = order_id
    if order_description:
      self.ach_sale['order_description'] = order_description
    return self.act_on_ach_sale()

  def do_ach_credit(self, account_type, owned_by, registered_name, routing_number, account_number, amount, order_id=None, order_description=None):
    self.ach_sale['type'] = 'credit'
    self.ach_sale['payment'] = 'check'
    self.ach_sale['checkname'] = registered_name
    self.ach_sale['checkaba'] = routing_number
    self.ach_sale['checkaccount'] = account_number
    self.ach_sale['account_holder_type'] = owned_by
    self.ach_sale['account_type'] = account_type
    self.ach_sale['amount'] = amount
    if order_id:
      self.ach_sale['orderid'] = order_id
    if order_description:
      self.ach_sale['order_description'] = order_description
    return self.act_on_ach_sale()

  def void_ach_txn(self, transaction_id, order_id=None, order_description=None):
    self.ach_sale['type'] = 'void'
    self.ach_sale['transactionid'] = transaction_id
    if order_id:
      self.ach_sale['orderid'] = order_id
    if order_description:
      self.ach_sale['order_description'] = order_description
    return self.act_on_ach_sale()

  def do_card_sale(self, card_number, expire_date, cvv, billing_first_name, billing_last_name, billing_company, billing_address_1, billing_address_2, billing_city, billing_state, billing_zip_code, billing_country, amount, order_id=None, order_description=None):
    self.card_sale['type'] = 'sale'
    self.card_sale['payment'] = 'creditcard'
    self.card_sale['ccnumber'] = card_number
    match = re.match(r'(\d\d)(\d\d)-(\d\d)-(\d\d)', str(expire_date))
    if match:
      # YYYY-MM-DD format, so convert it to MMYY format
      self.card_sale['ccexp'] = match.group(3) + match.group(2)
    else:
      match = re.match(r'(\d\d)(\d\d)', str(expire_date))
      if match:
        # already in MMYY format, so just assign as-is
        self.card_sale['ccexp'] = str(expire_date)
      else:
        raise Exception('expire_date must be YYYY-MM-DD or MMYY format; you entered: ' + str(expire_date))
    self.card_sale['cvv'] = cvv
    self.card_sale['first_name'] = billing_first_name
    self.card_sale['last_name'] = billing_last_name
    self.card_sale['company'] = billing_company
    self.card_sale['address1'] = billing_address_1
    self.card_sale['address2'] = billing_address_2
    self.card_sale['city'] = billing_city
    self.card_sale['state'] = billing_state
    self.card_sale['zip'] = billing_zip_code
    self.card_sale['country'] = billing_country
    self.card_sale['amount'] = amount
    if order_id:
      self.card_sale['orderid'] = order_id
    if order_description:
      self.card_sale['order_description'] = order_description
    return self.act_on_card_sale()

  def act_on_vault(self):
    if 'username' not in self.login or not self.login['username'] or 'password' not in self.login or not self.login['password']:
      raise Exception('no username and/or password specified in gateway object')
    query  = ""
    # Vault information
    for key,value in self.vault.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    # Login information
    query += "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password'])
    return self.do_transact_post(query)

  def act_on_vault_sale(self):
    if 'username' not in self.login or not self.login['username'] or 'password' not in self.login or not self.login['password']:
      raise Exception('no username and/or password specified in gateway object')
    query  = ""
    # Vault sale information
    for key,value in self.vault_sale.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    # Login information
    query += "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password'])
    return self.do_transact_post(query)

  def act_on_ach_sale(self):
    if 'username' not in self.login or not self.login['username'] or 'password' not in self.login or not self.login['password']:
      raise Exception('no username and/or password specified in gateway object')
    query  = ""
    # ACH sale information
    for key,value in self.ach_sale.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    # Login information
    query += "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password'])
    return self.do_transact_post(query)

  def act_on_card_sale(self):
    if 'username' not in self.login or not self.login['username'] or 'password' not in self.login or not self.login['password']:
      raise Exception('no username and/or password specified in gateway object')
    query  = ""
    # Credit card sale information
    for key,value in self.card_sale.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    # Login information
    query += "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password'])
    return self.do_transact_post(query)

  def do_transact_post(self, query):
    responseIO = io.BytesIO()
    curlObj = pycurl.Curl()
    curlObj.setopt(pycurl.POST, 1)
    # set connect timeout to 60 seconds
    curlObj.setopt(pycurl.CONNECTTIMEOUT, 60)
    # set timeout to 60 seconds
    curlObj.setopt(pycurl.TIMEOUT, 60)
    curlObj.setopt(pycurl.HEADER, 0)
    curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
    curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
    curlObj.setopt(pycurl.WRITEFUNCTION, responseIO.write)
    if self.login['username'] == 'demo':
      url = self.nmi_demo_transact_url
    else:
      url = self.nmi_transact_url
    curlObj.setopt(pycurl.URL, url)
    curlObj.setopt(pycurl.POSTFIELDS, query)
    curlObj.perform()
    data = responseIO.getvalue().decode('utf-8')
    data_dict = urllib.parse.parse_qs(data)
    response_dict = {}
    for key,value in data_dict.items():
      response_dict[key] = value[0]
    http_txn_dict = {}
    http_txn_dict['query'] = query
    http_txn_dict['response_body'] = data
    http_txn_dict['response_dict'] = response_dict
    return http_txn_dict

  def run_query(self, condition=None, transaction_type=None, action_type=None, transaction_id=None, order_id=None, last_name=None, email=None, cc_number=None, merchant_defined_field_1=None, start_date=None, end_date=None, report_type=None, mobile_device_license=None, mobile_device_nickname=None, customer_vault_id=None):
    query_dict = {}
    if condition:
      valid_choices = ('pending', 'pendingsettlement', 'failed', 'canceled', 'complete', 'unknown')
      if condition in valid_choices:
        query_dict['condition'] = condition
      else:
        raise Exception('Valid choices for condition are: ' + ', '.join(valid_choices) + '. You entered: ' + condition)
    if transaction_type:
      valid_choices = ('cc', 'ck')
      if transaction_type in valid_choices:
        query_dict['transaction_type'] = transaction_type
      else:
        raise Exception('Valid choices for transaction_type are: ' + ', '.join(valid_choices) + '. You entered: ' + transaction_type)
    if action_type:
      valid_choices = ('sale', 'refund', 'credit', 'auth', 'capture', 'void', 'return')
      if action_type in valid_choices:
        query_dict['action_type'] = action_type
      else:
        raise Exception('Valid choices for action_type are: ' + ', '.join(valid_choices) + '. You entered: ' + action_type)
    if transaction_id:
      query_dict['transaction_id'] = transaction_id
    if order_id:
      query_dict['order_id'] = order_id
    if last_name:
      query_dict['last_name'] = last_name
    if email:
      query_dict['email'] = email
    if cc_number:
      query_dict['cc_number'] = cc_number
    if merchant_defined_field_1:
      query_dict['merchant_defined_field_1'] = merchant_defined_field_1
    if start_date:
      if re.match(r'\d{8}', start_date) or re.match(r'\d{14}', start_date):
        query_dict['start_date'] = start_date
      else:
        raise Exception('start_date must be YYYYMMDD or YYYYMMDDhhmmss; you entered: ' + start_date)
    if end_date:
      if re.match(r'\d{8}', end_date) or re.match(r'\d{14}', end_date):
        query_dict['end_date'] = end_date
      else:
        raise Exception('end_date must be YYYYMMDD or YYYYMMDDhhmmss; you entered: ' + end_date)
    if report_type:
      valid_choices = ('receipt', 'customer_vault')
      if report_type in valid_choices:
        query_dict['report_type'] = report_type
      else:
        raise Exception('Valid choices for report_type are: ' + ', '.join(valid_choices) + '. You entered: ' + report_type)
    if mobile_device_license:
      query_dict['mobile_device_license'] = mobile_device_license
    if mobile_device_nickname:
      query_dict['mobile_device_nickname'] = mobile_device_nickname
    if customer_vault_id:
      if report_type == 'customer_vault':
        query_dict['customer_vault_id'] = customer_vault_id
      else:
        raise Exception('In order to specify customer_vault_id, you must also specify report_type = customer_vault')
    query  = ""
    # Query parameters
    for key,value in query_dict.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    # Login Information
    query += "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password'])
    return self.do_query_post(query)

  def do_query_post(self, query):
    responseIO = io.BytesIO()
    curlObj = pycurl.Curl()
    curlObj.setopt(pycurl.POST, 1)
    # set connect timeout to 60 seconds
    curlObj.setopt(pycurl.CONNECTTIMEOUT, 60)
    # set timeout to 60 seconds
    curlObj.setopt(pycurl.TIMEOUT, 60)
    curlObj.setopt(pycurl.HEADER, 0)
    curlObj.setopt(pycurl.SSL_VERIFYPEER,1)
    curlObj.setopt(pycurl.SSL_VERIFYHOST,2)
    curlObj.setopt(pycurl.WRITEFUNCTION, responseIO.write)
    curlObj.setopt(pycurl.URL, self.nmi_query_url)
    curlObj.setopt(pycurl.POSTFIELDS, query)
    curlObj.perform()
    data = responseIO.getvalue().decode('utf-8')
    http_txn_dict = {}
    http_txn_dict['query'] = query
    http_txn_dict['response_body'] = data
    return http_txn_dict


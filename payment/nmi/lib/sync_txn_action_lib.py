import argparse
import psycopg2
import psycopg2.extras
import re
import datetime
import dateutil.parser
import xml.etree.ElementTree
import solupay

def sync_txn_actions(txn_xml, conn, pmt_nmi_txn_dict=None, verbose=False, show_old=False):
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  nm_response_tree = xml.etree.ElementTree.fromstring(txn_xml)
  action_subelem_list = ['amount', 'action_type', 'date', 'success', 'ip_address', 'source', 'username', 'response_text', 'batch_id', 'processor_batch_id', 'response_code', 'processor_response_text', 'processor_response_code']
  for transaction in nm_response_tree.findall('./transaction'):
    queued_print = None
    transaction_id = transaction.findtext('./transaction_id')
    if pmt_nmi_txn_dict:
      if not transaction_id in pmt_nmi_txn_dict:
        if verbose:
          print('--- skipping unknown transaction id %s' % (transaction_id,))
        continue
      pmt_nmi_txn = pmt_nmi_txn_dict[transaction_id]
    else:
      curdict.execute('SELECT * FROM pmt_nmi_txn_vw WHERE transactionid = %s', (transaction_id,))
      pmt_nmi_txn = curdict.fetchone()
    if verbose:
      queued_print = '+ pmt_nmi_txn_vw %s, user %s, txn id %s, flr %s, cust %s' % (pmt_nmi_txn['id'], pmt_nmi_txn['nmi_username'], transaction_id, pmt_nmi_txn['floor_label'], pmt_nmi_txn['name'])
    # load existing actions in table so we can quickly check which have already been stored.
    curdict.execute('SELECT * FROM pmt_nmi_txn_action_tbl WHERE pmt_nmi_txn_id = %s', (pmt_nmi_txn['id'],))
    action_by_ts_dict = { row['action_ts'].strftime('%Y%m%d%H%M%S'): row for row in curdict }
    for action in transaction.findall('./action'):
      # if the action is already present in the table, skip to next action.
      # we assume that no two actions of a particular transaction can have the same timestamp (date).
      action_date = action.findtext('./date')
      if action_date in action_by_ts_dict:
        if verbose and show_old:
          if queued_print:
            print(queued_print)
            queued_print = None
          print('  already synced action, date %s' % (action_date,))
        continue
      # otherwise, action is not present in the table, so insert it.
      action_dict = {}
      for subelem_name in action_subelem_list:
        action_dict[subelem_name] = action.findtext('./' + subelem_name)
      action_dict['pmt_nmi_txn_id'] = pmt_nmi_txn['id']
      action_dict['action_ts'] = dateutil.parser.parse(action_dict['date']).replace(tzinfo=datetime.timezone.utc)
      del action_dict['date']
      if action_dict['success'] == '1':
        action_dict['success'] = True
      else:
        action_dict['success'] = False
      # delete keys for columns that are NULL
      for key in ('ip_address', 'source', 'username', 'response_text', 'processor_batch_id', 'processor_response_text', 'processor_response_code'):
        if not action_dict[key]:
          del action_dict[key]
      col_name_list = []
      col_val_list = []
      for key in sorted(action_dict):
        col_name_list.append(key)
        col_val_list.append(action_dict[key])
      col_name_str = ', '.join(col_name_list)
      placeholder_str = ', '.join(['%s'] * len(col_name_list))
      if verbose:
        if queued_print:
          print(queued_print)
          queued_print = None
        print('  new action: ' + ', '.join([str(x) for x in col_val_list]))
      curdict.execute('INSERT INTO pmt_nmi_txn_action_tbl (' + col_name_str + ') VALUES (' + placeholder_str + ')', tuple(col_val_list))
    # compare txn_xml from latest Solupay query to pmt_nmi_txn_{x}_tbl.txn_xml and update if changed.
    txn_xml_from_solupay = '<nm_response>\n\t' + xml.etree.ElementTree.tostring(transaction, encoding='unicode', short_empty_elements=False).rstrip() + '\n</nm_response>\n'
    tbl_name = 'pmt_nmi_txn_' + pmt_nmi_txn['pmt_method'] + '_tbl'
    if txn_xml_from_solupay != pmt_nmi_txn['txn_xml']:
      if verbose:
        if queued_print:
          print(queued_print)
          queued_print = None
        print('  update ' + tbl_name + ' column txn_xml')
      curdict.execute('UPDATE ' + tbl_name + ' SET txn_xml = xmlparse(document %s) WHERE id = %s', (txn_xml_from_solupay, pmt_nmi_txn['id']))
    conn.commit()

def sync_txn_by_id(pmt_nmi_txn_id, conn):
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT *, amount::numeric AS amount_dec FROM pmt_nmi_txn_vw WHERE id = %s', (pmt_nmi_txn_id,))
  pmt_nmi_txn = curdict.fetchone()
  pmt_gateway = solupay.Gateway(pmt_nmi_txn['nmi_username'])
  # retrieve all transactions (one) with desired transaction IDs.
  http_txn_dict = pmt_gateway.run_query(transaction_id=pmt_nmi_txn['transactionid'])
  sync_txn_actions(http_txn_dict['response_body'], conn)

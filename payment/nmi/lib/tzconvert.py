import pytz
import re
import psycopg2.extras
import datetime


def utc_to_building_datetime(datetime_utc, building_id, conn):
  """Convert UTC datetime to particular timezone of building

  Args: 
    datetime_utc (datetime.date or datetime.datetime): a date, either datetime.date or datetime.datetime
    building_id (int): unique building ID, the timezone of which to use
    conn (psycopg2.extensions.connection): connection to database.

  Returns
   datetime.datetime object with building time zone
  """
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM building_tbl WHERE id = %s', (building_id,))
  building = curdict.fetchone()
  building_timezone = pytz.timezone(building['timezone'])
  # if not datetime.datetime instance, then it is a datetime.date, so convert to datetime.datetime
  if not(isinstance(datetime_utc, datetime.datetime)):
    datetime_utc = datetime.datetime.combine(datetime_utc, datetime.time())
  # if no timezone info (meaning it is a naive datetime), convert to localized datetime in timezone UTC
  if not datetime_utc.tzinfo:
    utc_timezone = pytz.timezone('UTC')
    datetime_utc = utc_timezone.localize(datetime_utc)
  return datetime_utc.astimezone(building_timezone)


def date_to_utc_datetime(date, timezone):
  """Convert date in a particular time zone to UTC datetime.

  Determine the UTC datetime equivalent of date, where date is a date in a specified time zone.

  Args:
    date (datetime.date or str): a date, either datetime.date object or string in YYYY-MM-DD format
    timezone (pytz.tzinfo.DstTzInfo or str): time zone of the date; if string, in Olson format (tzdata/zoneinfo)

  Returns:
    datetime.datetime object with time zone UTC
  """
  # convert date to datetime; still expressed in local time, but is a naive time (no time zone awareness)
  if isinstance(date, datetime.date):
    naive_datetime = datetime.datetime(date.year, date.month, date.day)
  elif isinstance(date, str):
    match = re.match(r'(\d{4})-(\d\d)-(\d\d)', date)
    if match:
      naive_datetime = datetime.datetime(int(match.group(1)), int(match.group(2)), int(match.group(3)))
    else:
      raise Exception('date must be datetime.date object or string in YYYY-MM-DD format; you passed %s' % date)
  else:
    raise Exception('date must be datetime.date object or string in YYYY-MM-DD format; you passed %s' % date)
  if isinstance(timezone, pytz.tzinfo.DstTzInfo):
    pass
  elif isinstance(timezone, str):
    timezone = pytz.timezone(timezone)
  else:
    raise Exception('timezone must be pytz.tzinfo.DstTzInfo object or string in Olson format (tzdata/zoneinfo); you passed %s' % timezone)
  # convert naive datetime to tz-aware datetime (localized)
  datetime_local = timezone.localize(naive_datetime)
  # convert localized time to UTC
  datetime_utc = datetime_local.astimezone(datetime.timezone.utc)
  return datetime_utc

def cust_timezone(conn, cust_acct_id):
  """Look up customer's time zone.

  Args:
    conn (psycopg2.extensions.connection): connection to database.
    cust_acct_id (int): primary key for cust_acct_tbl.

  Returns:
    pytz.tzinfo.DstTzInfo object representing customer's time zone
  """
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT building_tbl.* FROM cust_acct_tbl, floor_tbl, building_tbl WHERE cust_acct_tbl.id = %s AND cust_acct_tbl.primary_floor_id = floor_tbl.id AND floor_tbl.building_id = building_tbl.id', (cust_acct_id,))
  building = curdict.fetchone()
  return pytz.timezone(building['timezone'])

def is_future_date_for_cust(conn, cust_acct_id, date):
  """Determine if a date (interpreted in the customer's local time zone) is in the future.

  Args:
    conn (psycopg2.extensions.connection): connection to database.
    cust_acct_id (int): primary key for cust_acct_tbl.
    date (datetime.date): date to check.

  Returns:
    True if date is in the future.
    False if date is not in the future.
  """
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT building_tbl.* FROM cust_acct_tbl, floor_tbl, building_tbl WHERE cust_acct_tbl.id = %s AND cust_acct_tbl.primary_floor_id = floor_tbl.id AND floor_tbl.building_id = building_tbl.id', (cust_acct_id,))
  building = curdict.fetchone()
  datetime_utc = date_to_utc_datetime(date, building['timezone'])
  return datetime_utc > datetime.datetime.now(datetime.timezone.utc)

def cust_datetime_to_utc_datetime(conn, cust_acct_id, naive_dt):
  """Convert a naive datetime (interpreted in the customer's local time zone) to tz-aware datetime in UTC time zone.

  For example, if you pass 2016-06-16 11:00:00 (naive datetime) and the customer's time zone is America/New_York, then the return
  value will be 2016-06-16 15:00:00+00:00 (i.e. UTC datetime).

  Args:
    conn (psycopg2.extensions.connection): connection to database.
    cust_acct_id (int): primary key for cust_acct_tbl.
    naive_dt (datetime.datetime): naive datetime.

  Returns:
    tz-aware datetime at UTC.

  Raises:
    ValueError: Not naive datetime (i.e. tzinfo is already set)
  """
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT building_tbl.* FROM cust_acct_tbl, floor_tbl, building_tbl WHERE cust_acct_tbl.id = %s AND cust_acct_tbl.primary_floor_id = floor_tbl.id AND floor_tbl.building_id = building_tbl.id', (cust_acct_id,))
  building = curdict.fetchone()
  timezone = pytz.timezone(building['timezone'])
  # convert naive datetime naive_dt to tz-aware datetime (localized to time zone of customer).
  # timezone.localize(naive_dt) will throw ValueError if naive_dt is not a naive datetime.
  datetime_local = timezone.localize(naive_dt)
  # convert localized time to UTC
  return datetime_local.astimezone(datetime.timezone.utc)


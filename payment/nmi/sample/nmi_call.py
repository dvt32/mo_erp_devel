#!/usr/bin/python3

import pycurl
import urllib
import urllib.parse
import io
import pprint

class gwapi():
  def __init__(self):
    self.login= dict()
    self.order = dict()
    self.billing = dict()
    self.shipping = dict()
    self.vault = dict()
    self.vaultsale = dict()
    self.responses = dict()

  def setLogin(self,username,password):
    self.login['password'] = password
    self.login['username'] = username

  def setOrder(self, orderid, orderdescription, tax, shipping, ponumber,ipadress):
    self.order['orderid'] = orderid;
    self.order['orderdescription'] = orderdescription
    self.order['shipping'] = '{0:.2f}'.format(float(shipping))
    self.order['ipaddress'] = ipadress
    self.order['tax'] = '{0:.2f}'.format(float(tax))
    self.order['ponumber'] = ponumber

  def setBilling(self, firstname, lastname, company, address1, address2, city, state, zip, country, phone, fax, email, website):
    self.billing['firstname'] = firstname
    self.billing['lastname']  = lastname
    self.billing['company']   = company
    self.billing['address1']  = address1
    self.billing['address2']  = address2
    self.billing['city']      = city
    self.billing['state']     = state
    self.billing['zip']       = zip
    self.billing['country']   = country
    self.billing['phone']     = phone
    self.billing['fax']       = fax
    self.billing['email']     = email
    self.billing['website']   = website

  def setShipping(self, firstname, lastname, company, address1, address2, city, state, zipcode, country, email):
    self.shipping['firstname'] = firstname
    self.shipping['lastname']  = lastname
    self.shipping['company']   = company
    self.shipping['address1']  = address1
    self.shipping['address2']  = address2
    self.shipping['city']      = city
    self.shipping['state']     = state
    self.shipping['zip']       = zipcode
    self.shipping['country']   = country
    self.shipping['email']     = email

  def setVault(self, customer_vault, customer_vault_id, billing_id, ccnumber, ccexp, checkname, checkaba, checkaccount, account_holder_type, account_type, sec_code, currency, payment, orderid, order_description, merchant_defined_field_num, ponumber, tax, tax_exempt, shipping, first_name, last_name, address1, city, state, zip_code, country, phone, email, company, address2, fax, shipping_id, shipping_firstname, shipping_lastname, shipping_company, shipping_address1, shipping_address2, shipping_city, shipping_state, shipping_zip, shipping_country, shipping_phone, shipping_fax, shipping_email):
    self.vault['customer_vault'] = customer_vault
    self.vault['customer_vault_id'] = customer_vault_id
    self.vault['billing_id'] = billing_id
    self.vault['ccnumber'] = ccnumber
    self.vault['ccexp'] = ccexp
    self.vault['checkname'] = checkname
    self.vault['checkaba'] = checkaba
    self.vault['checkaccount'] = checkaccount
    self.vault['account_holder_type'] = account_holder_type
    self.vault['account_type'] = account_type
    self.vault['sec_code'] = sec_code
    self.vault['currency'] = currency
    self.vault['payment'] = payment
    self.vault['orderid'] = orderid
    self.vault['order_description'] = order_description
    self.vault['merchant_defined_field_#'] = merchant_defined_field_num
    self.vault['ponumber'] = ponumber
    self.vault['tax'] = tax
    self.vault['tax_exempt'] = tax_exempt
    self.vault['shipping'] = shipping
    self.vault['first_name'] = first_name
    self.vault['last_name'] = last_name
    self.vault['address1'] = address1
    self.vault['city'] = city
    self.vault['state'] = state
    self.vault['zip'] = zip_code
    self.vault['country'] = country
    self.vault['phone'] = phone
    self.vault['email'] = email
    self.vault['company'] = company
    self.vault['address2'] = address2
    self.vault['fax'] = fax
    self.vault['shipping_id'] = shipping_id
    self.vault['shipping_firstname'] = shipping_firstname
    self.vault['shipping_lastname'] = shipping_lastname
    self.vault['shipping_company'] = shipping_company
    self.vault['shipping_address1'] = shipping_address1
    self.vault['shipping_address2'] = shipping_address2
    self.vault['shipping_city'] = shipping_city
    self.vault['shipping_state'] = shipping_state
    self.vault['shipping_zip'] = shipping_zip
    self.vault['shipping_country'] = shipping_country
    self.vault['shipping_phone'] = shipping_phone
    self.vault['shipping_fax'] = shipping_fax
    self.vault['shipping_email'] = shipping_email

  def setVaultSale(self, customer_vault_id, amount, currency, processor_id, descriptor, descriptor_phone, orderdescription, orderid):
    self.vaultsale['customer_vault_id'] = customer_vault_id
    self.vaultsale['amount'] = amount
    self.vaultsale['currency'] = currency
    self.vaultsale['processor_id'] = processor_id
    self.vaultsale['descriptor'] = descriptor
    self.vaultsale['descriptor_phone'] = descriptor_phone
    self.vaultsale['orderdescription'] = orderdescription
    self.vaultsale['orderid'] = orderid

  def doSale(self,amount, ccnumber, ccexp, cvv=''):
    query  = ""
    # Login Information
    query = query + "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password']) + "&"
    # Sales Information
    query += "ccnumber=" + urllib.parse.quote(ccnumber) + "&"
    query += "ccexp=" + urllib.parse.quote(ccexp) + "&"
    query += "amount=" + urllib.parse.quote('{0:.2f}'.format(float(amount))) + "&"
    if (cvv!=''):
      query += "cvv=" + urllib.parse.quote(cvv) + "&"
    # Order Information
    for key,value in self.order.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    # Billing Information
    for key,value in self.billing.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    # Shipping Information
    for key,value in self.shipping.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    query += "type=sale"
    return self.doPost(query)

  def doUpdateVault(self):
    query  = ""
    # Login Information
    query = query + "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password']) + "&"
    # Vault Information
    for key,value in self.vault.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    query += "customer_vault=" + urllib.parse.quote(self.vault['customer_vault'])
    return self.doPost(query)

  def doVaultSale(self):
    query  = ""
    # Login Information
    query = query + "username=" + urllib.parse.quote(self.login['username']) + "&"
    query += "password=" + urllib.parse.quote(self.login['password']) + "&"
    # Vault Sale Information
    for key,value in self.vaultsale.items():
      query += key +"=" + urllib.parse.quote(str(value)) + "&"
    query += "customer_vault_id=" + urllib.parse.quote(self.vaultsale['customer_vault_id'])
    return self.doPost(query)

  def doPost(self,query):
    responseIO = io.BytesIO()
    curlObj = pycurl.Curl()
    curlObj.setopt(pycurl.POST,1)
    curlObj.setopt(pycurl.CONNECTTIMEOUT,15)
    curlObj.setopt(pycurl.TIMEOUT,15)
    curlObj.setopt(pycurl.HEADER,0)
    curlObj.setopt(pycurl.SSL_VERIFYPEER,0)
    curlObj.setopt(pycurl.WRITEFUNCTION,responseIO.write);
    curlObj.setopt(pycurl.URL,"https://secure.nmi.com/api/transact.php")
    curlObj.setopt(pycurl.POSTFIELDS,query)
    curlObj.perform()
    data = responseIO.getvalue().decode('utf-8')
    temp = urllib.parse.parse_qs(data)
    for key,value in temp.items():
      self.responses[key] = value[0]
    pprint.pprint(self.responses)
    return self.responses['response']

def main():
  # NOTE: your username and password should replace the ones below
  gw = gwapi()
  gw.setLogin("MicOffMan", "Jasd%k21b")
#  gw.setLogin("demo", "password")
  gw.setVaultSale('testing abc 1','1.01','USD',"","","",'test txn',"")
  r = gw.doVaultSale()
  print(gw.responses['response'])

#  gw.setVault('add_customer','testing abc 1',"",'4111111111111111','10/25',"","","","","","","USD",'creditcard',"","","","","",'true',"",'One','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','1 TEST',"","","","","","","","","","","","","","","")
#  gw.setVault('add_customer','testing abc 2',"",'5431111111111111','10/25',"","","","","","","USD",'creditcard',"","","","","",'false',"",'Two','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','2 TEST',"","","","","","","","","","","","","","","")
#  gw.setVault('add_customer','testing abc 3',"",'6011601160116611','10/25',"","","","","","","USD",'creditcard',"","","","","",'false',"",'Three','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','3 TEST',"","","","","","","","","","","","","","","")
#  r = gw.doUpdateVault()
#  print(gw.responses['response'])
#  gw.setVault('add_customer','testing abc 4',"",'341111111111111','10/25',"","","","","","","USD",'creditcard',"","","","","",'false',"",'Four','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','4 TEST',"","","","","","","","","","","","","","","")
#  r = gw.doUpdateVault()
#  print(gw.responses['response'])
#  gw.setVault('add_customer','testing abc 5',"","","",'5 Test','123123123','123123123','personal','savings','WEB',"USD",'check',"","","","","",'false',"",'Five','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','5 TEST',"","","","","","","","","","","","","","","")
#  r = gw.doUpdateVault()
#  print(gw.responses['response'])
#  gw.setVault('add_customer','testing abc 6',"","","",'6 Test','123123123','123123123','business','savings','WEB',"USD",'check',"","","","","",'false',"",'Six','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','6 TEST',"","","","","","","","","","","","","","","")
#  r = gw.doUpdateVault()
#  print(gw.responses['response'])
#  gw.setVault('add_customer','testing abc 7',"","","",'7 Test','123123123','123123123','personal','checking','WEB',"USD",'check',"","","","","",'false',"",'Seven','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','7 TEST',"","","","","","","","","","","","","","","")
#  r = gw.doUpdateVault()
#  print(gw.responses['response'])
#  gw.setVault('add_customer','testing abc 8',"","","",'8 Test','123123123','123123123','business','checking','WEB',"USD",'check',"","","","","",'false',"",'Eight','Test','123 Abc Street','New York','NY','10018',"",'212-646-8976','wchao@microoffice.com','8 TEST',"","","","","","","","","","","","","","","")
#  r = gw.doUpdateVault()
#  print(gw.responses['response'])
#  gw.setBilling("John","Smith","Acme, Inc.","123 Main St","Suite 200", "Beverly Hills",
#                "CA","90210","US","555-555-5555","555-555-5556","support@example.com",
#                "www.example.com")
#  gw.setShipping("Mary","Smith","na","124 Shipping Main St","Suite Ship", "Beverly Hills",
#                 "CA","90210","US","support@example.com")
#  gw.setOrder("1234","Big Order",1, 2, "PO1234","65.192.14.10")
#  r = gw.doSale("5.00","4111111111111111","1212",'999')
#  print(gw.responses['response'])
#  if (int(gw.responses['response']) == 1) :
#    print("Approved")
#  elif (int(gw.responses['response']) == 2) :
#    print("Declined")
#  elif (int(gw.responses['response']) == 3) :
#    print("Error")

if __name__ == "__main__":
  main()

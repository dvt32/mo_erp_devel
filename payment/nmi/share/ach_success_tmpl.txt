Dear Member,

You scheduled an ACH/bank payment for debit today. Thank you for your payment.

Customer Account ID: {{ cust_acct_id }}
Customer Account Name: {{ name }}
Location: {{ floor_label }}
Registered Name on Bank Account: {{ registered_name }}
Routing Number: {{ routing_number }}
Account Number: {{ account_number }}
Account Type: {{ account_type }}
Owned By: {{ owned_by }}
Amount: {{ amount }}
Note: {{ note or '' }}
Transaction Response: {{ responsetext }}
Transaction ID: {{ transactionid }}

Please note that ACH/bank payments can take a few days to clear. Payments can bounce for a number of reasons: wrong routing or account number, insufficient funds, blocks imposed by the customer or the bank, or other causes. If the payment subsequently bounces, you will be assessed a bounce fee in addition to having to make a replacement payment, so please check that you have sufficient funds in your account and no blocks are in place by your bank.

If you feel this payment to be in error, please contact us at billing@microoffice.com and forward this email along with an explanation of the issue.

Sincerely,
The Micro Office Team

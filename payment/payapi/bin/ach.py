from flask import request, Response
import re
import warnings
import psycopg2.extras
import database
import decrypt

def find_cust_ach():
  '''RESTful endpoint that looks up a bank account number and returns the pmt_vault_id (or empty string if no match).

  Must be called with POST request to avoid having request parameters show up in Apache access log.

  Args:
    cust_acct_id (int): ID value of cust_acct_tbl row.
    routing_number (str): routing number to look up.
    account_number (str): account number to look up.

  Returns:
    pmt_vault_id or empty string if no match.
  '''
  request_dict = request.form
  cust_acct_id = int(request_dict.get('cust_acct_id'))
  desired_routing_number = request_dict.get('routing_number')
  desired_account_number = request_dict.get('account_number')
  if not desired_routing_number or not re.match(r'\d{9}', desired_routing_number):
    output = 'find_cust_ach: bad routing_number (invalid format): %s' % desired_routing_number
    warnings.warn(output)
    # 400 Bad Request
    response = Response(output, status=400, mimetype='text/plain')
    return response
  if not desired_account_number or not re.match(r'\d+', desired_account_number):
    output = 'find_cust_ach: bad account_number (invalid format): %s' % desired_account_number
    warnings.warn(output)
    # 400 Bad Request
    response = Response(output, status=400, mimetype='text/plain')
    return response
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_ach_tbl WHERE cust_acct_id = %s AND is_active', (cust_acct_id,))
  pmt_vault_id = None
  decrypt_client = decrypt.DecryptClient()
  for pmt_vault_ach in curdict:
    routing_pattern = pmt_vault_ach['routing_number'].replace('*', '\d')
    account_pattern = pmt_vault_ach['account_number'].replace('*', '\d')
    if re.match(routing_pattern, desired_routing_number) and re.match(account_pattern, desired_account_number):
      decrypted_row = decrypt_client.decrypt_row('pmt_vault_vw', pmt_vault_ach['id'])
      print(decrypted_row)
      if desired_routing_number == pmt_vault_ach['routing_number'] and desired_account_number == decrypted_row['account_number']:
        pmt_vault_id = pmt_vault_ach['id']
        break
  if pmt_vault_id:
    output = str(pmt_vault_id)
  else:
    output = ''
  # 200 OK
  response = Response(output, status=200, mimetype='text/plain')
  return response

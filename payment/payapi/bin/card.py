from flask import request, Response
import re
import warnings
import psycopg2.extras
import database
import decrypt

def find_cust_card():
  '''RESTful endpoint that looks up a credit card number and returns the pmt_vault_id (or empty string if no match).

  Must be called with POST request to avoid having request parameters show up in Apache access log.

  Args:
    cust_acct_id (int): ID value of cust_acct_tbl row.
    card_number (str): credit card number to look up.

  Returns:
    pmt_vault_id or empty string if no match.
  '''
  request_dict = request.form
  cust_acct_id = int(request_dict.get('cust_acct_id'))
  desired_card_number = request_dict.get('card_number')
  # one digit followed by one or more (digit|asterisk) characters followed by four digits.
  if not desired_card_number or not re.match(r'\d[\d\*]+\d{4}', desired_card_number):
    output = 'find_cust_card: bad card_number (invalid format): %s' % desired_card_number
    warnings.warn(output)
    # 400 Bad Request
    response = Response(output, status=400, mimetype='text/plain')
    return response
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM pmt_vault_card_tbl WHERE cust_acct_id = %s AND is_active', (cust_acct_id,))
  pmt_vault_id = None
  decrypt_client = decrypt.DecryptClient()
  for pmt_vault_card in curdict:
    card_pattern = pmt_vault_card['card_number'].replace('*', '\d')
    if re.match(card_pattern, desired_card_number):
      decrypted_row = decrypt_client.decrypt_row('pmt_vault_vw', pmt_vault_card['id'])
      if desired_card_number == decrypted_row['card_number']:
        pmt_vault_id = pmt_vault_card['id']
        break
  if pmt_vault_id:
    output = str(pmt_vault_id)
  else:
    output = ''
  # 200 OK
  print(output)
  response = Response(output, status=200, mimetype='text/plain')
  return response

from flask import request, Response
import re
import warnings
import psycopg2.extras
import json
import datetime
import database
import decrypt
import chargefacade

def process_vault_txn():
  '''RESTful endpoint that processes a vault transaction and returns a JSON object with results.

  Must be called with POST request to avoid having request parameters show up in Apache access log.

  Args:
    pmt_vault_txn_id (int): ID value of pmt_vault_txn_tbl row.
    demo_mode (optional [int]): either 0 (false) or 1 (true) based on whether to use demo mode.

  Returns:
    JSON object with resulting row from pmt_vault_txn_vw.
  '''
  request_dict = request.form
  pmt_vault_txn_id = int(request_dict.get('pmt_vault_txn_id'))
  is_demo_mode = int(request_dict.get('demo_mode', 0))
  if is_demo_mode == 1:
    is_demo_mode = True
  elif is_demo_mode == 0:
    is_demo_mode = False
  else:
    raise Exception('process_vault_txn: bad value for demo_mode (%s); must be 0 or 1' % (is_demo_mode))
  conn = database.get_connection()
  nmi_facade = chargefacade.NmiFacade(conn)
  if is_demo_mode:
    nmi_facade.set_demo_mode(True)
  pmt_vault_txn = nmi_facade.process_pmt_vault_txn(pmt_vault_txn_id)
  date_handler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, datetime.datetime)
    or isinstance(obj, datetime.date)
    else None)
  output = json.dumps(dict(pmt_vault_txn), default=date_handler)
  # 200 OK
  response = Response(output, status=200, mimetype='text/plain')
  return response


def process_confirm_txn():
  '''RESTful endpoint that processes a confirmation transaction and returns a JSON object with results.

  Must be called with POST request to avoid having request parameters show up in Apache access log.

  Args:
    pmt_vault_id (int): ID value of pmt_vault_(ach/card)_tbl row.
    demo_mode (optional [int]): either 0 (false) or 1 (true) based on whether to use demo mode.

  Returns:
    JSON object with true or false, depending on whether both confirmation transactions were successful
  '''
  request_dict = request.form
  pmt_vault_id = int(request_dict.get('pmt_vault_id'))
  is_demo_mode = int(request_dict.get('demo_mode', 0))
  if is_demo_mode == 1:
    is_demo_mode = True
  elif is_demo_mode == 0:
    is_demo_mode = False
  else:
    raise Exception('process_vault_txn: bad value for demo_mode (%s); must be 0 or 1' % (is_demo_mode))
  conn = database.get_connection()
  nmi_facade = chargefacade.NmiFacade(conn)
  if is_demo_mode:
    nmi_facade.set_demo_mode(True)
  pmt_vault_txns_status = nmi_facade.process_confirm_txn_ach(pmt_vault_id)
  output = json.dumps(pmt_vault_txns_status)
  # 200 OK
  response = Response(output, status=200, mimetype='text/plain')
  return response

import sys
sys.path.append('/var/www/sites/{{ payapi_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ payapi_fqdn }}/lib')
from flask import Flask, render_template, g
import card
import ach
import vault

app = Flask(__name__)
app.secret_key = '{{ payapi_flask_secret_key }}'
app.debug = {{ payapi_flask_app_debug }}

@app.teardown_appcontext
def teardown_database(exception):
  conn = getattr(g, '_database', None)
  if conn is not None:
    conn.close()

@app.route('/find_cust_card', methods=['POST'])
def find_cust_card():
  return card.find_cust_card()

@app.route('/find_cust_ach', methods=['POST'])
def find_cust_ach():
  return ach.find_cust_ach()

@app.route('/process_vault_txn', methods=['POST'])
def process_vault_txn():
  return vault.process_vault_txn()

@app.route('/process_confirm_txn', methods=['POST'])
def process_confirm_txn():
  return vault.process_confirm_txn()

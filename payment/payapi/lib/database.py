from flask import g
import psycopg2

def get_connection():
  conn = getattr(g, '_database', None)
  if conn is None:
    conn = g._database = psycopg2.connect(database='{{ pmt_proc_db_name }}', user='{{ pmt_proc_db_username }}', password='{{ pmt_proc_db_password }}', host='{{ pmt_proc_db_host }}', sslmode='{{ pmt_proc_db_sslmode }}')
  return conn

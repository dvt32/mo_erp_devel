﻿//For TESTING PURPOSES ONLY. NOT FOR PRODUCTION USE
using System;
using System.Xml;
using System.Net;
using System.IO;
using System.Data;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.Xml.XPath;

public partial class wsREST : System.Web.UI.Page
{
    protected void Button1_Click(object sender, EventArgs e)
    {
        StringBuilder string_builder = new StringBuilder();
        using (StringWriter string_writer = new StringWriter(string_builder))
        {
            using (XmlTextWriter xml_writer = new XmlTextWriter(string_writer))
            {     //build XML string 
                xml_writer.Formatting = Formatting.Indented;
                xml_writer.WriteStartElement("Transaction");
                xml_writer.WriteElementString("ExactID", "XXXXXX-XX");//Gateway ID
                xml_writer.WriteElementString("Password", "XXXXXXXX");//Password
                xml_writer.WriteElementString("Transaction_Type", "00");
                xml_writer.WriteElementString("DollarAmount", "1.66");
                xml_writer.WriteElementString("Expiry_Date", "1214");
                xml_writer.WriteElementString("CardHoldersName", "C# REST Client");
                xml_writer.WriteElementString("Card_Number", "4111111111111111");
                xml_writer.WriteEndElement();
			}
		}
		string xml_string = string_builder.ToString();

		//SHA1 hash on XML string
		ASCIIEncoding encoder = new ASCIIEncoding();
		byte[] xml_byte = encoder.GetBytes(xml_string);
		SHA1CryptoServiceProvider sha1_crypto = new SHA1CryptoServiceProvider();
		string hash = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "");
		string hashed_content = hash.ToLower();

		//assign values to hashing and header variables
		string keyID = "XXX";//key ID
		string key = "XXXXXXXXXXXXXXXXXXXXX";//Hmac key
		string method = "POST\n";
		string type = "application/xml";//REST XML
		string time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
		string uri = "/transaction/v13";
		string hash_data = method + type + "\n" + hashed_content + "\n" + time + "\n" + uri;
		//hmac sha1 hash with key + hash_data
		HMAC hmac_sha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key)); //key
		byte[] hmac_data = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data)); //data
		//base64 encode on hmac_data
		string base64_hash = Convert.ToBase64String(hmac_data);
		string url = "https://api.demo.globalgatewaye4.firstdata.com" + uri; //DEMO Endpoint
		
		//begin HttpWebRequest 
		HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
		web_request.Method = "POST";
		web_request.ContentType = type;
		web_request.Accept = "*/*";
		web_request.Headers.Add("x-gge4-date", time);
		web_request.Headers.Add("x-gge4-content-sha1", hashed_content);
		web_request.Headers.Add("Authorization", "GGE4_API " + keyID + ":" + base64_hash);
		web_request.ContentLength = xml_string.Length;
		
		// write and send request data 
		using(StreamWriter stream_writer = new StreamWriter(web_request.GetRequestStream()))
		{
			stream_writer.Write(xml_string);
		}

		//get response and read into string
		string response_string;
		try
		{
			using(HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
			{
				using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
				{
					response_string = response_stream.ReadToEnd();
				}
			
				//load xml
				XmlDocument xmldoc = new XmlDocument();
				xmldoc.LoadXml(response_string);
				XmlNodeList nodelist = xmldoc.SelectNodes("TransactionResult");
				//bind XML source DataList control
				DataList1.DataSource = nodelist;
				DataList1.DataBind();
				//output raw XML for debugging
			  
			   request_label.Text = "<b>Request</b><br />" + web_request.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(xml_string);
			   response_label.Text = "<b>Response</b><br />" + web_response.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(response_string);
		   }
		}
		//read stream for remote error response
		 catch (WebException ex)
		{
			 if (ex.Response != null) 
			{
				using (HttpWebResponse error_response = (HttpWebResponse)ex.Response) 
				{
					using (StreamReader reader = new StreamReader(error_response.GetResponseStream())) 
					{
						string remote_ex = reader.ReadToEnd();
						error.Text = remote_ex;
					}
				}
			}
		}
	}
}

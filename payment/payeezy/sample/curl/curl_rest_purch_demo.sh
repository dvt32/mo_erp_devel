#!/bin/sh

curl -H 'Content-Type: application/xml; charset=UTF-8' \
     -H 'Accept: application/xml' \
     -d '<?xml version="1.0" encoding="UTF-8"?>
<Transaction>
  <ExactID>JB9898-30</ExactID>
  <Password>b621ltg4nlvvsetzeo71gu26ipjr6vb4</Password>
  <Transaction_Type>00</Transaction_Type>
  <CardHoldersName>John Public</CardHoldersName>
  <Card_Number>4111111111111111</Card_Number>
  <DollarAmount>15.75</DollarAmount>
  <Expiry_Date>0117</Expiry_Date>
</Transaction>' \
     https://api.demo.globalgatewaye4.firstdata.com/transaction

echo

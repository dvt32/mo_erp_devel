#!/bin/sh

curl -H 'SOAPAction: http://secure2.e-xact.com/vplug-in/transaction/rpc-enc/SendAndCommit' \
     -H 'Content-Type: text/xml' \
     -d '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="http://secure2.e-xact.com/vplug-in/transaction/rpc-enc/" xmlns:types="http://secure2.e-xact.com/vplug-in/transaction/rpc-enc/encodedTypes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<q1:SendAndCommit xmlns:q1="http://secure2.e-xact.com/vplug-in/transaction/rpc-enc/Request">
<SendAndCommitSource href="#id1" />
</q1:SendAndCommit>
<types:Transaction id="id1" xsi:type="types:Transaction">
<ExactID xsi:type="xsd:string">JB9898-30</ExactID>
<Password xsi:type="xsd:string">b621ltg4nlvvsetzeo71gu26ipjr6vb4</Password>
<Transaction_Type xsi:type="xsd:string">01</Transaction_Type>
<DollarAmount xsi:type="xsd:string">16.75</DollarAmount>
<Card_Number xsi:type="xsd:string">4111111111111111</Card_Number>
<Expiry_Date xsi:type="xsd:string">1019</Expiry_Date>
<CardHoldersName xsi:type="xsd:string">Walter Sobchak</CardHoldersName>
</types:Transaction>
</soap:Body>
</soap:Envelope>' \
     https://api.demo.globalgatewaye4.firstdata.com/transaction/v11
# URL must be v11 or earlier because v12 introduced mandatory HMAC hash, which is much easier to do in a programming language

echo

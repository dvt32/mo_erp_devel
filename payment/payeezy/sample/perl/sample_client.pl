#!/usr/bin/env perl -w
#
# Perl sample code to use against the First Data Global Gateway e4℠ web service API: https://api.globalgatewaye4.firstdata.com
# Copyright (c) 2009.  All Rights Reserved.
# Last Updated: January 26, 2010
#
# Requirements:
# Perl v5.8.8
# SOAP::Lite http://soaplite.com
# Crypt::SSLeay for https support
# Optional (but useful):
# Data::Dumper http://search.cpan.org/~smueller/Data-Dumper-2.125/
# 

use strict;
use SOAP::Lite;
use Data::Dumper;

my $proxy = "https://api.globalgatewaye4.firstdata.com/transaction/v8";
my $url = "http://secure2.e-xact.com/vplug-in/transaction/rpc-enc/SendAndCommit";

# Determine "next year" for the expiry date
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime time;
my $nextyear = $year - 99;

my %trxnProperties = (
  "ExactID" => "",
  "Password" => "",
  "Transaction_Type" => "00",
  "DollarAmount" => "150.00",
  "Card_Number" => "4111111111111111",
  "Transaction_Tag" => "",
  "Track1" => "",
  "Track2" => "",
  "Authorization_Num" => "",
  "Expiry_Date" => "01$nextyear",
  "CardHoldersName" => "Perl Webservice Test",
  "VerificationStr1" => "",
  "VerificationStr2" => "",
  "CVD_Presence_Ind" => "",
  "Secure_AuthRequired" => "",
  "Secure_AuthResult" => "",
  "Ecommerce_Flag" => "",
  "XID" => "",
  "CAVV" => "",
  "CAVV_Algorithm" => "",
  "Reference_No" => "",
  "Customer_Ref" => "",
  "Reference_3" => "",
  "Language" => "en",
  "Client_IP" => "127.0.0.1",
  "Client_Email" => "",
  "ZipCode" => "",
  "Tax1Amount" => "",
  "Tax1Number" => "",
  "Tax2Amount" => "",
  "Tax2Number" => "",
  "SurchargeAmount" => "",
  "PAN" => ""
);


my %trxn = ("Transaction" => \%trxnProperties);

my $client = SOAP::Lite->proxy($proxy)->uri($url);

$client->on_action(sub { qq("$_[0]") });

my $result = $client->Process(\%trxn);

unless ($result->fault) {
  print Dumper($result->result());
} else {
  print join ', ', 
        $result->faultcode,
        $result->faultstring, "\n";
}

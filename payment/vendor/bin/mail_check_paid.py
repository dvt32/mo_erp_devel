#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import jinja2
import re
import dbutil
import mailutil
import validate_email

def main():
  argparser = argparse.ArgumentParser(description='Send email confirmation of vendor check payment transaction.')
  argparser.add_argument('check_paid_id', help='ID value of row in check_paid_tbl', type=int)
  argparser.add_argument('--bcc', action='append', help='Email address to bcc. Can be specified multiple times. If omitted, use default of {{ vend_mail_bcc }}.', type=valid_email)
  args = argparser.parse_args()
  conn = psycopg2.connect(database='{{ vend_pmt_db_name }}', user='{{ vend_pmt_db_username }}', password='{{ vend_pmt_db_password }}', host='{{ vend_pmt_db_host }}', sslmode='{{ vend_pmt_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT *, amount::numeric AS amount_dec FROM check_paid_tbl WHERE id = %s', (args.check_paid_id,))
  check_paid = curdict.fetchone()
  if not check_paid:
    conn.close()
    raise Exception('check_paid_tbl lacks row with ID ' + str(args.check_paid_id))
  if check_paid['email']:
    mail_from = '{{ vend_mail_from }}'
    mail_to = check_paid['email']
    if args.bcc:
      mail_bcc = ', '.join(args.bcc)
    else:
      mail_bcc = '{{ vend_mail_bcc }}'
    curdict.execute('SELECT * FROM company_tbl WHERE id = %s', (check_paid['payor_company_id'],))
    company = curdict.fetchone()
    mail_subject = company['name'] + ' remittance advice for payment to ' + check_paid['payee_name']
    with open('{{ payment_deploy_basedir }}/share/vendor_check_payment_text_tmpl.txt', 'r') as tmpl_file:
      vend_check_pmt_text_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    property_dict = dict(check_paid)
    vendor = None
    if check_paid['payee_table_column'] == 'vendor_tbl.id' and check_paid['payee_fk_id']:
      property_dict['vendor_id'] = check_paid['payee_fk_id']
      curdict.execute('SELECT * FROM vendor_tbl WHERE id = %s', (check_paid['payee_fk_id'],))
      vendor = curdict.fetchone()
      property_dict['vendor_name'] = vendor['name']
    property_dict['payor_company_name'] = company['name']
    property_dict['source_routing_number'] = check_paid['source_routing_number']
    property_dict['source_account_number'] = check_paid['source_account_number']
    property_dict['payee_name'] = check_paid['payee_name']
    property_dict['amount'] = check_paid['amount']
    property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
    property_dict['check_number'] = check_paid['check_number']
    property_dict['check_date'] = check_paid['check_date']
    property_dict['note'] = check_paid['note']
    property_dict['email'] = check_paid['email']
    property_dict['txn_number'] = check_paid['txn_number']
    property_dict['check_paid_id'] = check_paid['id']
    property_dict['vend_mail_from'] = '{{ vend_mail_from }}'
    if check_paid['payee_table_column'] == 'vendor_tbl.id' and check_paid['payee_fk_id']:
      curdict.execute('SELECT * FROM vendor_tbl WHERE id = %s', (check_paid['payee_fk_id'],))
      vendor = curdict.fetchone()
      if vendor:
        vendor_addr_list = []
        for addr_num in range(1, 5):
          if vendor['vendor_address_addr_' + str(addr_num)]:
            vendor_addr_list.append(vendor['vendor_address_addr_' + str(addr_num)])
        if vendor['vendor_address_city'] and vendor['vendor_address_state']:
          vendor_csz = vendor['vendor_address_city'] + ', ' + vendor['vendor_address_state']
          if vendor['vendor_address_postal_code']:
            vendor_csz = vendor_csz + ' ' + vendor['vendor_address_postal_code']
          vendor_addr_list.append(vendor_csz)
          if vendor['vendor_address_country']:
            vendor_addr_list.append(vendor['vendor_address_country'])
        property_dict['vendor_address'] = ''.join([line + '\n' for line in vendor_addr_list])
    mail_text_body = vend_check_pmt_text_tmpl.render(property_dict)
    mailutil.send_text_mail(mail_from, mail_to, mail_bcc, mail_subject, mail_text_body)

def valid_email(s):
  if validate_email.validate_email(s):
    return s
  else:
    msg = 'Enter valid email address; you entered {}'.format(s)
    raise argparse.ArgumentTypeError(msg)

if __name__ == "__main__":
  main()

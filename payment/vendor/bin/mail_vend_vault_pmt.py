#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import jinja2
import re
import dbutil
import mailutil
import validate_email

def main():
  argparser = argparse.ArgumentParser(description='Send email confirmation of vendor payment transaction.')
  argparser.add_argument('ach_credit_txn_id', help='ID value of row in ach_credit_txn_vw', type=int)
  argparser.add_argument('--bcc', action='append', help='Email address to bcc. Can be specified multiple times. If omitted, use default of {{ vend_mail_bcc }}.', type=valid_email)
  args = argparser.parse_args()
  conn = psycopg2.connect(database='{{ vend_pmt_db_name }}', user='{{ vend_pmt_db_username }}', password='{{ vend_pmt_db_password }}', host='{{ vend_pmt_db_host }}', sslmode='{{ vend_pmt_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT *, amount::numeric AS amount_dec FROM ach_credit_txn_vw WHERE id = %s', (args.ach_credit_txn_id,))
  ach_credit_txn = curdict.fetchone()
  if not ach_credit_txn:
    conn.close()
    raise Exception('ach_credit_txn_vw lacks row with ID ' + str(args.ach_credit_txn_id))
  if ach_credit_txn['email']:
    mail_from = '{{ vend_mail_from }}'
    mail_to = ach_credit_txn['email']
    if args.bcc:
      mail_bcc = ', '.join(args.bcc)
    else:
      mail_bcc = '{{ vend_mail_bcc }}'
    mail_subject = ach_credit_txn['payor_company_name'] + ' remittance advice for payment to ' + ach_credit_txn['payee_name']
    with open('{{ payment_deploy_basedir }}/share/vendor_payment_text_tmpl.txt', 'r') as tmpl_file:
      vend_pmt_text_tmpl = jinja2.Template(tmpl_file.read(), keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
    property_dict = dict(ach_credit_txn)
    vendor = None
    if ach_credit_txn['payee_table_column'] == 'vendor_tbl.id' and ach_credit_txn['payee_fk_id']:
      property_dict['vendor_id'] = ach_credit_txn['payee_fk_id']
      curdict.execute('SELECT * FROM vendor_tbl WHERE id = %s', (ach_credit_txn['payee_fk_id'],))
      vendor = curdict.fetchone()
      property_dict['vendor_name'] = vendor['name']
    property_dict['payor_company_name'] = ach_credit_txn['payor_company_name']
    property_dict['payee_name'] = ach_credit_txn['payee_name']
    property_dict['routing_number'] = ach_credit_txn['payee_routing_number']
    property_dict['account_number'] = ach_credit_txn['payee_account_number']
    property_dict['amount'] = ach_credit_txn['amount']
    property_dict['amount_dec'] = dbutil.cast_money_to_decimal(property_dict['amount'])
    property_dict['note'] = ach_credit_txn['note']
    property_dict['when_paid'] = ach_credit_txn['when_paid']
    property_dict['txn_number'] = ach_credit_txn['txn_number']
    property_dict['ach_credit_txn_id'] = ach_credit_txn['id']
    property_dict['vend_mail_from'] = '{{ vend_mail_from }}'
    mail_text_body = vend_pmt_text_tmpl.render(property_dict)
    mailutil.send_text_mail(mail_from, mail_to, mail_bcc, mail_subject, mail_text_body)

def valid_email(s):
  if validate_email.validate_email(s):
    return s
  else:
    msg = 'Enter valid email address; you entered {}'.format(s)
    raise argparse.ArgumentTypeError(msg)

if __name__ == "__main__":
  main()

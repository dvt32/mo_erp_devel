#!{{ payment_deploy_basedir }}/venv/bin/python3

import sys
sys.path.insert(1, '{{ payment_deploy_basedir }}/lib')
import argparse
import psycopg2
import psycopg2.extras
import decrypt
import collections
import dbutil
import re

def main():
  argparser = argparse.ArgumentParser(description='Create vendor vault payment transaction.')
  argparser.add_argument('corp_bank_account_id', help='ID value of row in corp_bank_account_tbl where we can fetch payor bank account', type=int)
  argparser.add_argument('vend_vault_id', help='ID value of row in vend_vault_vw where we can fetch payee bank account', type=int)
  argparser.add_argument('amount', help='Dollar (and optionally cents) amount of vendor payment', type=valid_currency)
  argparser.add_argument('scheduled_date', help='Scheduled date of transaction in YYYYMMDD format', type=valid_date)
  argparser.add_argument('--payee_name', help='Override name of payee from vend_vault_vw')
  argparser.add_argument('--note', help='Note associated with payment transaction')
  argparser.add_argument('--requester_id', help='ID of employee who requested this payment', type=int)
  notify_requester_parser = argparser.add_mutually_exclusive_group(required=False)
  notify_requester_parser.add_argument('--notify_requester', help='Notify the requester', dest='notify_requester', action='store_true')
  argparser.add_argument('--approver_id', help='ID of employee who approved this payment', type=int)
  notify_approver_parser = argparser.add_mutually_exclusive_group(required=False)
  notify_approver_parser.add_argument('--notify_approver', help='Notify the approver', dest='notify_approver', action='store_true')
  send_email_parser = argparser.add_mutually_exclusive_group(required=False)
  send_email_parser.add_argument('--send_email', help='Send email to vendor upon payment', action='store_true')
  argparser.add_argument('--verbose', help='Show payee routing and account numbers.', action='store_true')
  argparser.set_defaults(notify_requester=False)
  argparser.set_defaults(notify_approver=False)
  argparser.set_defaults(send_email=False)
  args = argparser.parse_args()

  conn = psycopg2.connect(database='{{ vend_pmt_db_name }}', user='{{ vend_pmt_db_username }}', password='{{ vend_pmt_db_password }}', host='{{ vend_pmt_db_host }}', sslmode='{{ vend_pmt_db_sslmode }}')
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  decrypt_client = decrypt.DecryptClient()
  curdict.execute('SELECT * FROM vend_vault_vw WHERE id = %s', (args.vend_vault_id,))
  vend_vault = curdict.fetchone()
  decrypted_vend_vault = decrypt_client.decrypt_row('vend_vault_vw', args.vend_vault_id)
  if vend_vault['vendor_id']:
    curdict.execute('SELECT * FROM vendor_tbl WHERE id = %s', (vend_vault['vendor_id'],))
    vendor = curdict.fetchone()
  else:
    vendor = None
  curdict.execute('SELECT * FROM corp_bank_account_tbl WHERE id = %s', (args.corp_bank_account_id,))
  corp_bank_account = curdict.fetchone()
  dec_corp_bank_account = decrypt_client.decrypt_row('corp_bank_account_tbl', args.corp_bank_account_id)
  payee_name = vend_vault['name']
  if args.payee_name:
    payee_name = args.payee_name
  if args.verbose:
    print('Payor Routing Number: ' + corp_bank_account['routing_number'])
    print('Payor Account Number: ' + corp_bank_account['account_number'])
    print('Vendor ID: ' + str(vend_vault['vendor_id']))
    print('Payee name: ' + payee_name)
    print('Scheduled Date: ' + args.scheduled_date)
    print('Amount: $' + args.amount)
    print('Payee Routing Number: ' + vend_vault['routing_number'])
    print('Payee Account Number: ' + decrypted_vend_vault['account_number'])
  r = collections.OrderedDict()
  r['source_routing_number'] = corp_bank_account['routing_number']
  r['source_account_number'] = dec_corp_bank_account['account_number']
  r['payee_name'] = payee_name
  r['payee_table_column'] = 'vendor_tbl.id'
  r['payee_fk_id'] = vend_vault['vendor_id']
  r['payee_routing_number'] = vend_vault['routing_number']
  r['payee_account_number'] = decrypted_vend_vault['account_number']
  r['payee_account_function'] = vend_vault['account_type']
  r['payee_account_ownership'] = vend_vault['owned_by']
  r['amount'] = args.amount
  r['scheduled_date'] = args.scheduled_date
  if args.note:
    r['note'] = args.note
  if vendor and vendor['email']:
    r['email'] = vendor['email']
  r['requester_id'] = args.requester_id
  r['notify_requester'] = args.notify_requester
  if args.approver_id:
    r['approver_id'] = args.approver_id
  r['notify_approver'] = args.notify_approver
  dbutil.insert_row_from_dict(curdict, 'ach_credit_txn_tbl', r)
  conn.commit()

def valid_currency(s):
  if re.match(r'^\d+$', s) or re.match(r'^\d+\.\d\d$', s):
    return s
  else:
    msg = 'Enter currency amount in CCC or CCC.cc format; you entered {}'.format(s)
    raise argparse.ArgumentTypeError(msg)

def valid_date(s):
  if re.match(r'^\d{8}$', s) or re.match(r'^\d{14}$', s):
    return s
  else:
    msg = 'Enter date in YYYYMMDD or YYYYMMDDhhmmss format; you entered {}'.format(s)
    raise argparse.ArgumentTypeError(msg)

if __name__ == "__main__":
  main()

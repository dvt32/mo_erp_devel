from email.mime.text import MIMEText
import email.utils
import email.header
import smtplib

def send_text_mail(mail_from, mail_to, mail_bcc, mail_subject, mail_body):
  """Send a plain text email.

  Args:
    mail_from: email address of sender
    mail_to: email address of recipient
    mail_subject: subject line of the email
    mail_body: body of the email in plain text
  """
  mail_msg = MIMEText(mail_body)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = mail_from
  mail_msg['To'] = mail_to
  if mail_bcc:
    mail_msg['Bcc'] = mail_bcc
  mail_msg['Subject'] = email.header.Header(mail_subject, 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()

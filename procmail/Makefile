include $(repo_root)/common/func.mk
include $(repo_root)/common/python.mk

.PHONY: setup install clean

perl_modules := YAML CPAN DBI DBD::Pg LWP::UserAgent LWP::Protocol::https Mail::Message IPC::Run Storable Encode HTTP::DAV Socket Mail::Sendmail Mail::Sender MIME::Base64 Date::Calc Mozilla::CA
comma := ,

setup:
	$(call check_system_properties,msgaction_home_dir local_lib_url)
ifneq ($(linux_distribution),debian)
	$(error only debian distribution is supported in setup-procmail target)
endif
ifeq ($(is_production_deployment), 1)
# install Perl local::lib and Perl modules into a local::lib (like Python virtualenv)
	@echo ..install Perl local::lib and Perl modules into local::lib
	@su msgaction $(CURDIR)/build_helper/bootstrap_local_lib.sh $(local_lib_url) '$(perl_modules)'
else
	$(error devel deployment not written yet in setup-procmail target)
endif

install: jinja2_pkg
	$(call check_system_properties,msgaction_home_dir outbound_smtp_server faxage_url faxage_username faxage_company faxage_password voicemail_from_email voicemail_no_recipient_email voicecloud_api_username voicecloud_api_devkey database_name database_hostname data_source database_username database_password dav_base_url dav_username dav_password dav_max_file_id)
	$(eval _file_list:=)
	@echo ..create deployment directories
	@for i in bin dtd lib; do mkdir -p $(msgaction_home_dir)/$$i; chown msgaction.msgaction $(msgaction_home_dir)/$$i; done
	@echo ..copy files to deployment directories
	@$(foreach file,$(wildcard $(CURDIR)/bin/*.pl),$(call copy_enqueue,$(file),$(msgaction_home_dir)/bin/$(notdir $(file)),msgaction.msgaction,0700);)
	@$(foreach file,$(wildcard $(CURDIR)/dtd/*),$(call copy_enqueue,$(file),$(msgaction_home_dir)/dtd/$(notdir $(file)),msgaction.msgaction,0700);)
	@$(foreach file,$(wildcard $(CURDIR)/lib/*.pm),$(call copy_enqueue,$(file),$(msgaction_home_dir)/lib/$(notdir $(file)),msgaction.msgaction,0700);)
	@$(call copy_enqueue,$(CURDIR)/rc/procmailrc,$(msgaction_home_dir)/.procmailrc,msgaction.msgaction,0644,1)
# create directory for procmail MAILDIR. should not be needed, but useful just in case to avoid polluting /home/msgaction.
	@mkdir -p $(msgaction_home_dir)/mail
	@chown msgaction.msgaction $(msgaction_home_dir)/mail
	@echo ..substitute properties in copied files
	@$(call subst_batch,$(system_properties))

clean:
	$(call check_system_properties,msgaction_home_dir)
	@rm -fr $(msgaction_home_dir)/{.cpan,perl5}
# strip 'eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"' from .bashrc if it is there.
	@if grep -q 'eval "$$(perl -I$$HOME/perl5/lib/perl5 -Mlocal::lib)"' $(msgaction_home_dir)/.bashrc; then \
	  perl -pi -e 's|eval "\$$\(perl -I\$$HOME/perl5/lib/perl5 -Mlocal::lib\)"\n||' $(msgaction_home_dir)/.bashrc; \
	fi
	@if [ -d $(msgaction_home_dir)/mail ]; then rmdir $(msgaction_home_dir)/mail; fi
	@rm -f $(msgaction_home_dir)/bin/*.pl
	@rm -f $(msgaction_home_dir)/dtd/*
	@rm -f $(msgaction_home_dir)/lib/*.pm
	@for i in bin dtd lib; do \
	  if [ -d "$(msgaction_home_dir)/$$i" ] && [ ! "$$(ls -A $(msgaction_home_dir)/$$i)" ]; then \
	    rmdir $(msgaction_home_dir)/$$i; \
	  fi; \
	done
	@rm -f $(msgaction_home_dir)/.procmailrc

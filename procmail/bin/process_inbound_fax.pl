#!/usr/bin/perl

use strict;
local($^W) = 1;

use lib "{{ msgaction_home_dir }}/perl5/lib/perl5";
use local::lib;
use DBI qw(:sql_types);
use Mail::Message;
use Mail::Sender;
use lib "{{ msgaction_home_dir }}/lib";
use init_database;
use select_row_with_query;
use select_row;
use put_file;
use insert_row;
use get_row_list;
use lead_pad;

my $account_id_of_corporate = 1;

main();

sub main {
  my $message = Mail::Message->read(\*STDIN);
  my $subject = $message->subject();
  my ($normalized_from_name, $normalized_sender_id, $fax_number);
  if ($subject =~ /^pFax from (.+?) to (\d{10})$/) {
    my @from_addresses = $message->from();
    # We only care about the first "From" address.
    my $mail_address = $from_addresses[0];
    # Get the formatted "From" address.
    my $from_name = $mail_address->name() || "";
    # Normalize the name and sender ID.
    $normalized_from_name = $from_name || "";
    $normalized_from_name =~ s/[^-a-zA-Z0-9]+/_/g;
    $normalized_sender_id = $1;
    $fax_number = $2;
    $normalized_sender_id =~ s/[^-a-zA-Z0-9]+/_/g;
  } elsif ($subject =~ /^MyFax Delivery from/) {
    # Normalize the name and sender ID.
    my @outer_message_part_list = $message->body()->parts();
    my @inner_message_part_list = $outer_message_part_list[0]->body()->parts();
    my $main_body = $inner_message_part_list[0]->body()->decoded()->string();
    $main_body =~ /^Sending Fax: (.*)/m;
    $normalized_from_name = $1;
    $normalized_from_name =~ s/[^-a-zA-Z0-9]+/_/g;
    $main_body =~ /^Caller Id: (.*)/m;
    $normalized_sender_id = $1;
    $normalized_sender_id =~ s/[^-a-zA-Z0-9]+/_/g;
    $main_body =~ /^Receiving Fax Number: \((\d{3})\) (\d{3})-(\d{4})/m;
    $fax_number = $1 . $2 . $3;
  } elsif ($subject =~ /^Micro Office Fax/) {
    # Normalize the name and sender ID.
    my @message_part_list = $message->body()->parts();
    my $main_body = $message_part_list[0]->body()->decoded()->string();
    $main_body =~ /^Sending Fax: (.*)/m;
    $normalized_from_name = $1;
    $normalized_from_name =~ s/\s+/ /;
    $normalized_from_name =~ s/^\s+//;
    $normalized_from_name =~ s/\s+$//;
    $normalized_from_name =~ s/[^-a-zA-Z0-9]+/_/g;
    $main_body =~ /^Caller ID: (.*)/m;
    $normalized_sender_id = $1;
    $normalized_sender_id =~ s/\s+/ /;
    $normalized_sender_id =~ s/^\s+//;
    $normalized_sender_id =~ s/\s+$//;
    $normalized_sender_id =~ s/[^-a-zA-Z0-9]+/_/g;
    $main_body =~ /^Receiving Fax Number: (\d{10})/m;
    $fax_number = $1;
  } else {
    # if bad signature, just silently eat the message. It was probably spam.
    return;
  }
  # Determine the current time.
  my ($second, $minute, $hour, $day, $month, $year, $wday, $yday, $isdst) = localtime(time());
  $year += 1900;
  $month = lead_pad($month + 1, 2);
  $day = lead_pad($day, 2);
  $hour = lead_pad($hour, 2);
  $minute = lead_pad($minute, 2);
  $second = lead_pad($second, 2);
  my $dbh = init_database();
  my ($msg_timestamp, $msg_size, $msg_filename);
  # Find the attachment with a content type of "application/pdf"
  my @attachments = $message->body()->parts();
  # The content type returned by Message::Body->type() (for pFax) will look like this:
  #   Content-Type: application/pdf; name="pFax_12May2007_20-22-38.pdf"
  # except it will not have the preceding "content-type: " string.
  # The content type returned for MyFax will just be "application/pdf" without the quotes.
  for my $attachment (@attachments) {
    my $message_body = $attachment->body();
    my $content_type = $message_body->type();
    if ($content_type =~ m{^application/pdf}) {
      my $pdf_file_data = $message_body->decoded()->string();
      my $namespace = "/fax/in";
      my $filename;
      if ($fax_number eq "6462531258") {
	$filename = "Shared Fax $year$month$day$hour$minute$second $normalized_from_name $normalized_sender_id.pdf";
      } else {
	$filename = "Fax $year$month$day$hour$minute$second $normalized_from_name $normalized_sender_id.pdf";
      }
      $filename =~ s/\s+/ /g;
      ($msg_timestamp, $msg_size, $msg_filename) = ("$year-$month-$day $hour:$minute:$second", length($pdf_file_data), $filename);
      my $sql_query = "select * from fax_number_tbl where fax_number = ?";
      my @sql_param_list = ($fax_number);
      my $fax_number_row = select_row_with_query($dbh, $sql_query, \@sql_param_list);
      my $email_action = "none";
      my $cc_email;
      my ($account, $person);
      if ($fax_number_row) {
	$email_action = $fax_number_row->{"email_action"};
	$cc_email = $fax_number_row->{"cc_email"};
	my $account_id = $fax_number_row->{"account_id"};
	my $person_id = $fax_number_row->{"person_id"};
	$account = select_row($dbh, "account_vw", $account_id);
	$person = select_row($dbh, "person_vw", $person_id);
      }
      if (($fax_number eq "6462531258") || ($email_action ne "attach_delete")) {
	# if fax number is shared, always save the fax in storage.
	# if fax number is private, save the fax in storage if email_action is not "attach_delete".
	my $mime_type = "application/pdf";
	my $file_id = put_file($dbh, $namespace, $filename, $mime_type, \$pdf_file_data);
	my @sql_column_list;
	if (($account) && ($person)) {
	  # if we know who owns the private fax number, link the fax to the account now.
	  push(@sql_column_list, ["account_id", $account->{"id"}, SQL_INTEGER]);
	  push(@sql_column_list, ["person_id", $person->{"id"}, SQL_INTEGER]);
	  push(@sql_column_list, ["note", "Fax at $msg_timestamp"]);
	}
	push(@sql_column_list, ["fax_number", $fax_number]);
	push(@sql_column_list, ["file_id", $file_id, SQL_INTEGER]);
	push(@sql_column_list, ["filename", $filename]);
	push(@sql_column_list, ["file_size", length($pdf_file_data), SQL_INTEGER]);
	push(@sql_column_list, ["file_timestamp", "$year-$month-$day $hour:$minute:$second"]);
	insert_row($dbh, "fax_in_tbl", \@sql_column_list);
      }
      if (($email_action ne "none") && ($account) && ($account->{"is_active"}) && ($person)) {
	my @email_list;
	if (($person->{"is_active"}) && ($person->{"email"})) {
	  push(@email_list, $person->{"email"});
	}
	if ($cc_email) {
	  push(@email_list, split(/,/, $cc_email));
	}
	# if there was some email action and we have the email address, we need to send an email to the owner of the fax line.
	if (@email_list) {
	  my $mail_sender = Mail::Sender->new
	    ({"smtp" => "{{ outbound_smtp_server }}", "from" => qq{"Micro Office Fax Service" <service\@microoffice.com>}});
	  my $mail_sender_to = join(", ", @email_list);
	  my $email_subject =
	    "Fax from $normalized_from_name $normalized_sender_id to $fax_number at $msg_timestamp";
	  $email_subject =~ s/\s+/ /g;
	  if ($email_action eq "notify") {
	    my $msg_body;
	    if ($account->{"id"} == $account_id_of_corporate) {
	      $msg_body = <<"---END_BLOCK---;";
An incoming fax was received. Please log in to https://operations.microoffice.com to view it. Here are the details of the fax:

| Timestamp: $msg_timestamp
|      Size: $msg_size bytes
|  Filename: $msg_filename

Your private faxes show up in "Claimed Documents" under the Communications and Imaging section.
---END_BLOCK---;
	    } else {
	      $msg_body = <<"---END_BLOCK---;";
An incoming fax was received. Please log in to https://bizcenter.microoffice.com to view it. Here are the details of the fax:

| Timestamp: $msg_timestamp
|      Size: $msg_size bytes
|  Filename: $msg_filename

Your private faxes show up in "My Documents" under the Communications and Imaging section.
---END_BLOCK---;
	    }
	    $mail_sender->MailMsg({"to" => $mail_sender_to, "subject" => $email_subject, "msg" => $msg_body});
	  } elsif (($email_action eq "attach") || ($email_action eq "attach_delete")) {
	    $mail_sender->OpenMultipart
	      ({"to" => $mail_sender_to, "subject" => $email_subject});
	    my $msg_body = <<"---END_BLOCK---;";
An incoming fax was received. Here are the details of the fax:

| Timestamp: $msg_timestamp
|      Size: $msg_size bytes
|  Filename: $msg_filename

The fax is attached as a PDF.
---END_BLOCK---;
	    $mail_sender->Body({"msg" => $msg_body});
	    my $attachment_filename = "Fax $year$month$day$hour$minute$second $normalized_from_name $normalized_sender_id.pdf";
	    $attachment_filename =~ s/\s+/_/g;
	    $mail_sender->Part({"description" => $attachment_filename,
				"ctype" => qq{application/pdf; name="$attachment_filename"},
				"encoding" => 'Base64',
				"disposition" => qq{attachment; filename="$attachment_filename"},
				"msg" => $pdf_file_data,});
	    $mail_sender->Close();
	  } else {
	    die "unknown email_action: $email_action";
	  }
	}
      }
      # only process one PDF attachment. There should not be more than one anyway. If there is, this code needs to be fixed.
      last;
    }
  }
  if ($fax_number eq "6462531258") {
    my $sql_query = "select * from cust_pers_tbl where inbound_fax_alert_cutoff_dt > now()";
    my @cust_pers_list;
    get_row_list($dbh, $sql_query, undef, \@cust_pers_list);
    my $mail_sender;
    if (@cust_pers_list) {
      $mail_sender = Mail::Sender->new
	({"smtp" => "{{ outbound_smtp_server }}", "from" => "Inbound Fax at Micro Office <service\@microoffice.com>"});
      my $msg_subject = "Inbound fax alert at $msg_timestamp ($msg_size bytes)";
      for my $cust_pers (@cust_pers_list) {
	my $inbound_fax_alert_cutoff_dt = $cust_pers->{"inbound_fax_alert_cutoff_dt"};
	my $msg_body = <<"---END_BLOCK---;";
An inbound fax was received. Please log in to https://bizcenter.microoffice.com to view it. Here are the details of the fax:

| Timestamp: $msg_timestamp
|      Size: $msg_size bytes
|  Filename: $msg_filename

You will continue receiving inbound fax alerts until $inbound_fax_alert_cutoff_dt. If you would like to deactivate inbound fax alerts, visit https://bizcenter.microoffice.com and go to Unclaimed Faxes, then click on the deactivate inbound fax alerts link.
---END_BLOCK---;
	$mail_sender->MailMsg({"to" => $cust_pers->{"email"}, "subject" => $msg_subject, "msg" => $msg_body});
      }
    }
  }
  $dbh->disconnect();
}


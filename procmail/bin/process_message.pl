#!/usr/bin/perl

use strict;
local($^W) = 1;

use lib "{{ msgaction_home_dir }}/perl5/lib/perl5";
use local::lib;
use Mail::Message;
use lib "{{ msgaction_home_dir }}/lib";
use process_virtual_office_sale;
use process_prospective_customer;
use process_broker_referral;
use process_friend_referral;
use process_stella_prospect;

main();

sub main {
  my $message = Mail::Message->read(\*STDIN);
  my @from_addresses = $message->from();
  # We only care about the first "From" address.
  my $mail_address = $from_addresses[0];
  my $sender_email_address = $mail_address->address();
  my $subject = $message->subject();
  if (($sender_email_address eq "webmaster\@microoffice.com") || ($sender_email_address eq "webmaster\@microoffice.us")) {
    if ($subject =~ /^\[virtual_office\] /) {
      process_virtual_office_sale($message);
    } elsif ($subject =~ /^\[regular_lead\] /) {
      process_prospective_customer($message);
    } elsif ($subject =~ /^\[broker_referral\] /) {
      process_broker_referral($message);
    } elsif ($subject =~ /^\[friend_referral\] /) {
      process_friend_referral($message);
    }
  } elsif ($sender_email_address eq "webmaster\@stellanetworks.com") {
    if ($subject =~ /^\[regular_lead\] /) {
      process_stella_prospect($message);
    }
  }
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use lib "{{ msgaction_home_dir }}/perl5/lib/perl5";
use local::lib;
use DBI qw(:sql_types);
use Mail::Message;
use Mail::Sender;
use lib "{{ msgaction_home_dir }}/lib";
use init_database;
use get_database_charset;
use select_row_with_query;
use select_row;
use send_fax;
use lead_pad;
use put_file;
use insert_row;

my $account_id_of_corporate = 1;

main();

sub main {
  my $message = Mail::Message->read(\*STDIN);
  my $subject = $message->subject();
  my @from_address_list = $message->from();
  my $from_address = $from_address_list[0]->address();
  my @to_address_list = $message->to();
  my $to_address = $to_address_list[0]->address();
  my ($mac_address, $fax_number);
  if ($from_address =~ /^scanner-([0-9a-f]{12})\@(fax212|afaxq)\.com$/i) {
    $mac_address = lc($1);
  } else {
    # if bad signature, just silently eat the message. It was probably spam.
    return;
  }
  if ($subject !~ /^Scanned image from scanner-$mac_address$/i) {
    # if bad signature, just silently eat the message. It was probably spam.
    return;
  }
  my $dbh = init_database();
  my $db_charset = get_database_charset();
  my $sql_query = "select * from fax_machine_tbl where mac_address = ?";
  my @sql_param_list = ($mac_address);
  my $fax_machine = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if (!$fax_machine) {
    # not a valid fax machine, so generate warning and exit.
    warn "ERROR: invalid fax machine: $mac_address";
    return;
  }
  my $account = select_row($dbh, "account_vw", $fax_machine->{"account_id"});
  my $person = select_row($dbh, "person_vw", $fax_machine->{"person_id"});
  if ($to_address =~ /^1?(\d{10})\@(fax212|afaxq)\.com$/) {
    $fax_number = $1;
    my $sending_person = $person;
    my $sending_account_type = ($account->{"id"} == $account_id_of_corporate) ? "corporate" : "customer";
    # Find the attachment with a content type of "application/pdf"
    my @attachments = $message->body()->parts();
    # The content type returned by Message::Body->type() will look like this:
    #   Content-Type: application/pdf; name="UXB800-091119155121.pdf"
    # except it will not have the preceding "content-type: " string.
    for my $attachment (@attachments) {
      my $message_body = $attachment->body();
      my $content_type = $message_body->type();
      if ($content_type =~ m{^application/pdf}) {
	my $pdf_file_data = $message_body->decoded()->string();
	my $recipient_name = $fax_number;
	my @filename_list = ("fax.pdf");
	my @file_content_list = ($pdf_file_data);
	my $send_email_alert = $fax_machine->{"email_action"} eq "notify" ? 1 : 0;
	my $job_id;
	eval {
	  $job_id = send_fax
	    (
	     "dbh" => $dbh,
	     "db_charset" => $db_charset,
	     "sending_person" => $sending_person,
	     "sending_account_type" => $sending_account_type,
	     "recipient_name" => $recipient_name,
	     "fax_number" => $fax_number,
	     "filename_list" => \@filename_list,
	     "file_content_list" => \@file_content_list,
	     "send_email_alert" => $send_email_alert,
	    );
	};
	if ($@) {
	  my $error_message = $@;
	  chomp($error_message);
	  die $error_message;
	}
	# only process one PDF attachment. There should not be more than one anyway. If there is, this code needs to be fixed.
	last;
      }
    }
  } elsif ($to_address =~ /^me\@(fax212|afaxq)\.com$/i) {
    # person scanned to himself. put it in "My Documents" and also send via email.

    # Find the attachment with a content type of "application/pdf"
    my @attachments = $message->body()->parts();
    # The content type returned by Message::Body->type() will look like this:
    #   Content-Type: application/pdf; name="UXB800-091119155121.pdf"
    # except it will not have the preceding "content-type: " string.
    for my $attachment (@attachments) {
      my $message_body = $attachment->body();
      my $content_type = $message_body->type();
      if ($content_type =~ m{^application/pdf}) {
	my $pdf_file_data = $message_body->decoded()->string();
	my $msg_size = length($pdf_file_data);
	my $mail_sender = Mail::Sender->new
	  ({"smtp" => "{{ outbound_smtp_server }}", "from" => qq{"Micro Office Scan Service" <service\@microoffice.com>}});
	my $mail_sender_to = $person->{"email"};
	# Determine the current time.
	my ($second, $minute, $hour, $day, $month, $year, $wday, $yday, $isdst) = localtime(time());
	$year += 1900;
	$month = lead_pad($month + 1, 2);
	$day = lead_pad($day, 2);
	$hour = lead_pad($hour, 2);
	$minute = lead_pad($minute, 2);
	$second = lead_pad($second, 2);
	my $msg_timestamp = "$year-$month-$day $hour:$minute:$second";
	# insert the file into the scan table.
	my $namespace = "/scan/private";
	my $filename = "Private Scan $year$month$day$hour$minute$second.pdf";
	my $mime_type = "application/pdf";
	my $file_id = put_file($dbh, $namespace, $filename, $mime_type, \$pdf_file_data);
	my $scanner_name = "Sharp-$mac_address";
	my @sql_column_list;
	push(@sql_column_list, ["account_id", $fax_machine->{"account_id"}, SQL_INTEGER]);
	push(@sql_column_list, ["person_id", $fax_machine->{"person_id"}, SQL_INTEGER]);
	push(@sql_column_list, ["file_id", $file_id, SQL_INTEGER]);
	push(@sql_column_list, ["filename", $filename]);
	push(@sql_column_list, ["scanner_name", $scanner_name]);
	push(@sql_column_list, ["file_size", length($pdf_file_data), SQL_INTEGER]);
	push(@sql_column_list, ["file_timestamp", "$year-$month-$day $hour:$minute:$second"]);
	insert_row($dbh, "scan_tbl", \@sql_column_list);
	# also send the private scan via email.
	my $email_subject = "Private scan at $msg_timestamp";
	$email_subject =~ s/\s+/ /g;
	$mail_sender->OpenMultipart
	  ({"to" => $mail_sender_to, "subject" => $email_subject});
	my $msg_body = <<"---END_BLOCK---;";
You scanned a document. Here are the details of the scan:

| Timestamp: $msg_timestamp
|      Size: $msg_size bytes

The scanned document is attached as a PDF.
---END_BLOCK---;
	$mail_sender->Body({"msg" => $msg_body});
	my $attachment_filename = "Private Scan $year$month$day$hour$minute$second.pdf";
	$attachment_filename =~ s/\s+/_/g;
	$mail_sender->Part({"description" => $attachment_filename,
			    "ctype" => qq{application/pdf; name="$attachment_filename"},
			    "encoding" => 'Base64',
			    "disposition" => qq{attachment; filename="$attachment_filename"},
			    "msg" => $pdf_file_data,});
	$mail_sender->Close();
	# only process one PDF attachment. There should not be more than one anyway. If there is, this code needs to be fixed.
	last;
      }
    }
  } else {
    # if bad signature, generate warning.
    warn "ERROR: bad to_address: $to_address";
    if ($person->{"email"}) {
      my $mail_sender = Mail::Sender->new
	({"smtp" => "{{ outbound_smtp_server }}", "from" => qq{"Micro Office Fax Service" <service\@microoffice.com>}});
      my $mail_sender_to = $person->{"email"};
      my $email_subject =
	"Bad fax number specified";
      my $msg_body = <<"---END_BLOCK---;";
You specified the to address as: $to_address

Please specify the to address as 9876543210\@afaxq.com

where 9876543210 is the 10 digit fax number (do not put a 1 in front).
---END_BLOCK---;
      $mail_sender->MailMsg({"to" => $mail_sender_to, "subject" => $email_subject, "msg" => $msg_body});
    }
  }
  $dbh->disconnect();
}


#!/usr/bin/perl

use strict;
local($^W) = 1;

use lib "{{ msgaction_home_dir }}/perl5/lib/perl5";
use local::lib;
use DBI qw(:sql_types);
use LWP::UserAgent;
use Mail::Message;
use Mail::Sender;
use lib "{{ msgaction_home_dir }}/lib";
use init_database;
use select_row;
use select_row_with_query;
use update_row;

my $faxage_url = "{{ faxage_url }}";
my $username = "{{ faxage_username }}";
my $company = "{{ faxage_company }}";
my $password = "{{ faxage_password }}";

main();

sub main {
  my $message = Mail::Message->read(\*STDIN);
  my @from_addresses = $message->from();
  # We only care about the first "From" address.
  my $mail_address = $from_addresses[0];
  # Get the formatted "From" address.
  my $from_name = $mail_address->name() || "";
  # Get just the email address.
  my $sender_email_address = $mail_address->address();
  my $subject = $message->subject();
  # check that the header signature is appropriate.
  if (($sender_email_address !~ /^support\@faxage.com$/) || ($subject !~ /^\s*(\d+)\s*$/)) {
    # if bad signature, just silently eat the message. It was probably spam.
    return;
  }
  # Job ID is also in the body, so we just get it there.
  #my $job_id = $1;
  my $status_record_line = $message->body()->decoded()->string();
  $status_record_line =~ s/^[\s\n]+//gis;
  $status_record_line =~ s/[\s\n]+$//gis;
  my ($job_id, $comm_id, $recipient_name, $fax_number, $short_status, $long_status, $send_time, $complete_time, $xmit_time, $page_count) = split(/\t/, $status_record_line);
  my $dbh = init_database();
  my $sql_query = "select * from fax_out_tbl where job_id = ?";
  my @sql_param_list = ([$job_id, SQL_INTEGER]);
  my $fax_out = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if (!$fax_out) {
    $dbh->disconnect();
    # no matching record in database, so ignore status message.
    return;
  }
  my @sql_column_list;
  push(@sql_column_list, ["comm_id", $comm_id]);
  push(@sql_column_list, ["short_status", $short_status]);
  push(@sql_column_list, ["long_status", $long_status]);
  push(@sql_column_list, ["send_time", $send_time]);
  if ($short_status ne "pending") {
    $complete_time = $send_time if ($complete_time eq "0000-00-00 00:00:00");
    push(@sql_column_list, ["complete_time", $complete_time]);
    push(@sql_column_list, ["xmit_time", $xmit_time]);
    push(@sql_column_list, ["page_count", ($page_count =~ /^\s*$/) ? undef : $page_count, SQL_INTEGER]);
  }
  # update the sent fax record in the database using the information from FaxAge.
  update_row($dbh, "fax_out_tbl", \@sql_column_list, $fax_out->{"id"});
  # if send_email_alert is true, then send out an email alert to the person.
  if ($fax_out->{"send_email_alert"}) {
    my $person = select_row($dbh, "person_vw", $fax_out->{"person_id"});
    if (($person) && ($person->{"email"})) {
      my $email = $person->{"email"};
      my $mail_sender = Mail::Sender->new
	({"smtp" => "{{ outbound_smtp_server }}", "from" => "Fax Status at Micro Office <service\@microoffice.com>"});
      my $outgoing_subject = "Fax status report for job ID $job_id";
      my $outgoing_body .= <<"---END_BLOCK---;";
|  Recipient Name: $recipient_name
|      Fax Number: $fax_number
|          Status: $short_status
| Extended Status: $long_status
|       Time Sent: $send_time MT
|  Time Completed: $complete_time MT
|   Transmit Time: $xmit_time
|      Page Count: $page_count

Times are shown in Mountain Time (MT) because that is where the fax server is located.
---END_BLOCK---;
      $mail_sender->MailMsg({"to" => $email, "subject" => $outgoing_subject, "msg" => $outgoing_body});
    }
  }
  $dbh->disconnect();
  # if not pending, clear the sent fax record from the list on the FaxAge servers.
  if ($short_status ne "pending") {
    my %param_map =
      (
       "username" => $username,
       "company" => $company,
       "password" => $password,
       "operation" => "clear",
      );
    $param_map{"jobid"} = $job_id;
    my $user_agent = LWP::UserAgent->new();
    my $response = $user_agent->post($faxage_url, \%param_map);
  }
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use lib "{{ msgaction_home_dir }}/perl5/lib/perl5";
use local::lib;
use DBI qw(:sql_types);
use Mail::Message;
use lib "{{ msgaction_home_dir }}/lib";
use init_database;
use put_file;
use insert_row;
use lead_pad;

main();

sub main {
  my $message = Mail::Message->read(\*STDIN);
  my @from_addresses = $message->from();
  # We only care about the first "From" address.
  my $mail_address = $from_addresses[0];
  # Get the formatted "From" address.
  my $from_name = $mail_address->name() || "";
  # Get just the email address.
  my $sender_email_address = $mail_address->address();
  my $subject = $message->subject();
  # check that the header signature is appropriate.
  if (($from_name !~ /^Sharp-/i) || ($sender_email_address !~ /^scanner\@(microoffice|stellanetworks)\.(com|us)$/i) ||
      ($subject !~ /^Scan$/i)) {
    # if bad signature, just silently eat the message. It was probably spam.
    return;
  }
  my $dbh = init_database();
  # Find the attachment with a content type of "application/pdf"
  my @attachments = $message->body()->parts();
  # The content type returned by Message::Body->type() will look like this:
  #   Content-Type: application/pdf; name="UXB800-080519163050.pdf"
  # except it will not have the preceding "content-type: " string.
  for my $attachment (@attachments) {
    my $message_body = $attachment->body();
    my $content_type = $message_body->type();
    if ($content_type =~ m{^application/pdf; name="UXB800-(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)\.pdf"}) {
      # Determine the current time.
      my ($second, $minute, $hour, $day, $month, $year, $wday, $yday, $isdst) = localtime(time());
      $year += 1900;
      $month = lead_pad($month + 1, 2);
      $day = lead_pad($day, 2);
      $hour = lead_pad($hour, 2);
      $minute = lead_pad($minute, 2);
      $second = lead_pad($second, 2);
      my $pdf_file_data = $message_body->decoded()->string();
      my $namespace = "/scan/public";
      my $filename = "Public Scan $year$month$day$hour$minute$second.pdf";
      my $mime_type = "application/pdf";
      my $file_id = put_file($dbh, $namespace, $filename, $mime_type, \$pdf_file_data);
      my $scanner_name = $from_name;
      my @sql_column_list;
      push(@sql_column_list, ["file_id", $file_id, SQL_INTEGER]);
      push(@sql_column_list, ["filename", $filename]);
      push(@sql_column_list, ["scanner_name", $scanner_name]);
      push(@sql_column_list, ["file_size", length($pdf_file_data), SQL_INTEGER]);
      push(@sql_column_list, ["file_timestamp", "$year-$month-$day $hour:$minute:$second"]);
      insert_row($dbh, "scan_tbl", \@sql_column_list);
      # only process one PDF attachment. There should not be more than one anyway. If there is, this code needs to be fixed.
      last;
    }
  }
  $dbh->disconnect();
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use lib "{{ msgaction_home_dir }}/perl5/lib/perl5";
use local::lib;
use DBI qw(:sql_types);
use LWP::UserAgent;
use Mail::Message;
use Mail::Sender;
use lib "{{ msgaction_home_dir }}/lib";
use init_database;
use put_file;
use insert_row;
use lead_pad;
use get_month_number;
use url_encode;
use update_row;
use dump_to_file;
use select_row_with_query;

main();

sub main {
  my $message = Mail::Message->read(\*STDIN);
  my $subject = $message->subject();
  my $mail_from_address = ($message->from())[0];
  my $from_email_address = $mail_from_address->address();
  if ($from_email_address !~ m{^(voicemail\@microoffice\.com|voicemail\@stellanetworks\.com|voicemail\@thirdgearnetworks\.com)$}) {
    # if bad signature, just silently eat the message. It was probably spam.
    return;
  }
  my ($caller_id_name, $caller_id_number, $msg_len_hr, $msg_len_min, $msg_len_sec);
  if ($subject =~ m{^"(.*)" <(\w+)> (\d\d):(\d\d):(\d\d)}) {
    ($caller_id_name, $caller_id_number, $msg_len_hr, $msg_len_min, $msg_len_sec) = ($1, $2, $3, $4, $5);
  } else {
    # if bad signature, just silently eat the message. It was probably spam.
    return;
  }
  my $message_duration = "$msg_len_hr:$msg_len_min:$msg_len_sec";
  my @attachment_list = $message->body()->parts();
  # structure of email should be a multipart/alternative block followed by an audio/wav block. The multipart/alternative block
  # should contain a text/plain block and a text/html block, both of which contain the same content (but in different formats).
  my $part_body;
  $part_body = $attachment_list[0]->body();
  my $message_body;
  if ($part_body->type() =~ m{^multipart/alternative}) {
    $part_body = ($part_body->parts())[0]->body();
    if ($part_body->type() =~ m{^text/plain}) {
      $message_body = $part_body->decoded()->string();
    } else {
      die "bad message body";
    }
  } else {
    die "bad message body";
  }
  my $vm_timestamp;
  if ($message_body =~ m{^Created: \w+, (\w+) (\d+) (\d+), (\d\d) (\d\d) (\w+)}m) {
    # FreeSWITCH makes day, hour, minute two digits (padded with leading zero if needed).
    my ($month_name, $day, $year, $hour, $min, $am_pm) = ($1, $2, $3, $4, $5, $6);
    my $month_number = lead_pad(get_month_number($month_name), 2);
    if ($am_pm =~ /^AM$/) {
      if (int($hour) == 12) {
	$hour = "00";
      }
    } elsif ($am_pm =~ /^PM$/) {
      if ($hour < 12) {
	$hour += 12;
      }
    } else {
      die "bad AM/PM field";
    }
    $vm_timestamp = $year . "-" . $month_number . "-" . $day . " " . $hour . ":" . $min . ":00";
  } else {
    die "no Created field";
  }
  my $mailbox_account;
  if ($message_body =~ m{^Account: (\w+)}m) {
    $mailbox_account = $1;
  } else {
    die "no Account field";
  }
  $part_body = $attachment_list[1]->body();
  my $audio_file_data;
  if ($part_body->type() =~ m{^audio/wav}) {
    $audio_file_data = $part_body->decoded()->string();
  } else {
    die "bad audio part";
  }
  # Determine the current time.
  my ($second, $minute, $hour, $day, $month, $year, $wday, $yday, $isdst) = localtime(time());
  $year += 1900;
  $month = lead_pad($month + 1, 2);
  $day = lead_pad($day, 2);
  $hour = lead_pad($hour, 2);
  $minute = lead_pad($minute, 2);
  $second = lead_pad($second, 2);

  my $dbh = init_database();
  my $email_from = '{{ voicemail_from_email }}';
  my $email_to = "";
  my $sql_query = "select * from voicemail_box_tbl where box_number = ?";
  my @sql_param_list = ($mailbox_account);
  my $voicemail_box = select_row_with_query($dbh, $sql_query, \@sql_param_list);
  if ($voicemail_box) {
    $email_to = $voicemail_box->{"email"};
    if (!$email_to) {
      $email_to = '{{ voicemail_no_recipient_email }}';
    }
  } else {
    die "unrecognized mailbox account";
  }

  my $namespace = "/phone/vm/voicecloud";
  my $filename = "VM $year$month$day$hour$minute$second $caller_id_name $caller_id_number $mailbox_account.wav";
  my $mime_type = "audio/wav";
  my $file_id = put_file($dbh, $namespace, $filename, $mime_type, \$audio_file_data);
  my @sql_column_list;
  push(@sql_column_list, ["file_id", $file_id, SQL_INTEGER]);
  push(@sql_column_list, ["email_from", $email_from]);
  push(@sql_column_list, ["email_to", $email_to]);
  push(@sql_column_list, ["caller_id_name", $caller_id_name]);
  push(@sql_column_list, ["caller_id_number", $caller_id_number]);
  push(@sql_column_list, ["mailbox_account", $mailbox_account]);
  push(@sql_column_list, ["message_timestamp", $vm_timestamp]);
  push(@sql_column_list, ["message_duration", $message_duration]);
  my $voicecloud_txn_id = insert_row($dbh, "voicecloud_txn_tbl", \@sql_column_list, "voicecloud_txn_seq");
  # call VoiceCloud API to initiate transcription.
  my $file_url = "https://api.stellanetworks.com/voicecloud/get_audio_file.cgi?id=$voicecloud_txn_id";
  $caller_id_number =~ s/[^\d]//g;
  my $voicecloud_url = "https://api.voicecloud.com/api.cgi?action=sendfile&username={{ voicecloud_api_username }}&devkey={{ voicecloud_api_devkey }}&idtype=username&fileurl=" . url_encode($file_url) . "&filetype=wav&callerid=$caller_id_number";
  my $user_agent = LWP::UserAgent->new();
  if ('{{ voicecloud_user_agent }}' ne '') {
    $user_agent->agent("{{ voicecloud_user_agent }}");
  }
  if ('{{ voicecloud_ca_cert_filename }}' ne '') {
    $user_agent->ssl_opts("SSL_ca_file" => '/etc/ssl/certs/{{ voicecloud_ca_cert_filename }}');
  }
  push(@{$user_agent->requests_redirectable}, "POST");
  $user_agent->cookie_jar({});
  my $response = $user_agent->get($voicecloud_url);
  my $response_content = $response->content();
  if ($response_content =~ m{<transactionuid>(\d+)</transactionuid>}i) {
    my $transaction_uid = $1;
    @sql_column_list = (["transaction_uid", $transaction_uid, SQL_INTEGER]);
    update_row($dbh, "voicecloud_txn_tbl", \@sql_column_list, $voicecloud_txn_id);
  } else {
    # if the response from VoiceCloud was an error, or there was no response, at least send the WAV file.
    $dbh->disconnect();
    dump_to_file("/tmp/voicecloud-response-html-$file_id.xml", $response_content);
    my $mail_sender = Mail::Sender->new
      ({"smtp" => "{{ outbound_smtp_server }}", "from" => $email_from});
    my $msg_subject = "$caller_id_name <$caller_id_number> $message_duration";
    my $msg_body = <<"---END_BLOCK---;";
|   CID Name: $caller_id_name
| CID Number: $caller_id_number
|  Timestamp: $vm_timestamp
|   Duration: $message_duration
|    Mailbox: $mailbox_account

There was a temporary problem with transcription. The audio of your voicemail has been attached. We regret the inconvenience and are working to restore transcription as soon as possible.
---END_BLOCK---;
    $mail_sender->OpenMultipart
      ({"to" => $email_to, "subject" => $msg_subject});
    $mail_sender->Body({"msg" => $msg_body});
    $vm_timestamp =~ s/[^\d]//g;
    $caller_id_name =~ s/[^-a-zA-Z0-9]+/_/g;
    $caller_id_number =~ s/[^-a-zA-Z0-9]+/_/g;
    my $attachment_filename = "vm_" . $vm_timestamp . "_" . $caller_id_name . "_" . $caller_id_number . ".wav";
    $mail_sender->Part({"description" => $attachment_filename,
			"ctype" => qq{audio/wav; name="$attachment_filename"},
			"encoding" => 'Base64',
			"disposition" => qq{attachment; filename="$attachment_filename"},
			"msg" => $audio_file_data,});
    $mail_sender->Close();
    die "transactionuid not found in $response_content";
  }
  $dbh->disconnect();
}

#!/bin/bash

LOCAL_LIB_URL=$1
PERL_MODULES=$2
export POSTGRES_LIB="`pg_config|grep LIBDIR|awk '{print $3}'` -lssl -lcrypto"
cd ~
if [ ! -e ~/perl5/bin/cpan ]; then
  LOCAL_LIB_TGZ_FILE=`echo $LOCAL_LIB_URL | sed 's|^.*/||'`
  LOCAL_LIB_DIR=${LOCAL_LIB_TGZ_FILE%.tar.gz}
  wget $LOCAL_LIB_URL
  tar xzf $LOCAL_LIB_TGZ_FILE
  cd $LOCAL_LIB_DIR
  perl Makefile.PL --bootstrap
  make test && make install
  cd ~
  rm -fr $LOCAL_LIB_DIR
  rm -f $LOCAL_LIB_TGZ_FILE
fi
if ! grep -q 'eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"' ~/.bashrc; then
  echo 'eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"' >>~/.bashrc
fi
eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"
# use default settings when user input required. Needed for Mail::Sender
export PERL_MM_USE_DEFAULT=1
cpan -I $PERL_MODULES

use strict;
local($^W) = 1;

use LWP::UserAgent;

sub call_faxage_api {
  my %param_map = @_;
  my $user_agent = LWP::UserAgent->new();
  $param_map{"username"} = '{{ faxage_username }}';
  $param_map{"company"} = '{{ faxage_company }}';
  $param_map{"password"} = '{{ faxage_password }}';
  my $response = $user_agent->post('{{ faxage_url }}', \%param_map);
  my $response_content = $response->content();
  return $response_content;
}

1;

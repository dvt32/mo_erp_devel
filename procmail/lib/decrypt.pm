use strict;
local($^W) = 1;

use IPC::Run qw(run);

sub decrypt($) {
  my $encrypted_content = shift;
  my $gpg_cmd = "/usr/bin/gpg";
  my $gpg_homedir = '{{ msgaction_home_dir }}/.gnupg';
  my @gpg_arg_list = ("--homedir", $gpg_homedir, "--decrypt", "--quiet");
  my @cmd_list = ($gpg_cmd, @gpg_arg_list);
  my $decrypted_content = "";
  run(\@cmd_list, \$encrypted_content, \$decrypted_content);
  return $decrypted_content;
}

1;

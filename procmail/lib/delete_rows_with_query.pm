use strict;
local($^W) = 1;

use DBD::Pg qw(:pg_types);

# sql_param_list is undef if no params or a ref to an array. Each element of the array is either a scalar with the param value or a ref to an array as follows:
#   (column_value, sql_data_type)
#   column_value is the value of the column.
#   sql_data_type is SQL_INTEGER, SQL_BOOLEAN, or other constant from DBI, or a hash ref with driver-specific options. It can also be the string "pg_bytea".
sub delete_rows_with_query($$$) {
  my ($dbh, $sql_query, $sql_param_list) = @_;
  my $sth = $dbh->prepare($sql_query);
  if ($sql_param_list) {
    for(my $i = 0; $i < scalar(@$sql_param_list); $i++) {
      my $column_value = $sql_param_list->[$i];
      if (ref($column_value)) {
        my $sql_data_type;
        ($column_value, $sql_data_type) = @$column_value;
        if (lc($sql_data_type) eq "pg_bytea") {
          $sql_data_type = { "pg_type" => DBD::Pg::PG_BYTEA };
        }
        $sth->bind_param($i + 1, $column_value, $sql_data_type);
      } else {
        $sth->bind_param($i + 1, $column_value);
      }
    }
  }
  my $rv = $sth->execute();
  $sth->finish();
  return $rv;
}

1;

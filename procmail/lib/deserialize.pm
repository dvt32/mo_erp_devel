use strict;
local($^W) = 1;

use Storable qw(thaw);

sub deserialize($) {
  my $serialized_object_ref = shift;
  return thaw($serialized_object_ref);
}

1;

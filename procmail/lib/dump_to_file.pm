use strict;
local($^W) = 1;

use IO::File;

sub dump_to_file($$) {
  my ($filename, $file_content) = @_;
  # basic sanity check. If filename exceeds 255 characters, assume it is the file content and that parameters were
  # accidentally swapped.
  if (length($filename) > 255) {
    die "filename greater than 255 chars; maybe you swapped filename and file_content?";
  }
  my $fh = IO::File->new();
  $fh->open(">$filename");
  print $fh $file_content;
  $fh->close();
}

1;

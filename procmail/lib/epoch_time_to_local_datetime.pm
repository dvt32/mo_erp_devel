use strict;
local($^W) = 1;

# converts epoch time (seconds since the epoch, always GMT/UTC) to datetime (local time zone).
# year is four digit year (e.g. 2006).
# month is 1 - 12.
# day is 1 - 31.
# hour is 0 - 23.
# minute is 0 - 59.
# second is 0 - 59.
sub epoch_time_to_local_datetime($) {
  my $epoch_time = shift;
  my ($year, $month, $day, $hour, $minute, $second) = (localtime($epoch_time))[5, 4, 3, 2, 1, 0];
  $year += 1900;
  $month++;
  return ($year, $month, $day, $hour, $minute, $second);
}

1;

use strict;
local($^W) = 1;

sub get_database_charset() {
  my $charset = "UTF-8";
  return $charset;
}

1;

use strict;
local($^W) = 1;

sub get_id($$) {
  my $dbh = shift;
  my $sequence_name = shift;
  my $sth = $dbh->prepare("select nextval(?) as id");
  $sth->bind_param(1, $sequence_name);
  $sth->execute();
  my $row = $sth->fetchrow_hashref();
  $sth->finish();
  return $row->{"id"};
}

1;

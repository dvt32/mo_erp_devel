use strict;
local($^W) = 1;

# pass name of month as parameter. can be 3 letters ("Jan") or full name ("January"). Capitalization ignored.
# Returns 1 to 12 or throws an error.
sub get_month_number($) {
  my $month_name = lc(shift);
  my @month_list = (qw(january february march april may june july august september october november december));
  my $month_number;
  for(my $i = 0; $i < scalar(@month_list); $i++) {
    if (($month_name eq $month_list[$i]) || ($month_name eq substr($month_list[$i], 0, 3))) {
      $month_number = $i + 1;
      last;
    }
  }
  if (!$month_number) {
    die "bad month name: $month_name";
  }
  return $month_number;
}

1;

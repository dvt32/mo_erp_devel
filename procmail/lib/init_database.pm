use strict;
local($^W) = 1;

use DBI qw(:sql_types);

sub init_database() {
  my $dbh = DBI->connect('{{ data_source }}', '{{ database_username }}', '{{ database_password }}', {"pg_server_prepare" => 0});
  return $dbh;
}

1;

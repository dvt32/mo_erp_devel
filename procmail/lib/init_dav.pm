use strict;
local($^W) = 1;

use HTTP::DAV;

sub init_dav() {
  my $dav = HTTP::DAV->new();
  $dav->credentials("-user" => '{{ dav_username }}', "-pass" => '{{ dav_password }}', "-url" => '{{ dav_base_url }}');
  $dav->open("-url" => '{{ dav_base_url }}') or die "couldn't open {{ dav_base_url }}: " . $dav->message();
  return ($dav, '{{ dav_base_url }}', {{ dav_max_file_id }});
}

1;

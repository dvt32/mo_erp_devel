use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use DBD::Pg qw(:pg_types);
use get_id;

# sql_column_list is a ref to an array of refs to arrays, each of which looks like so:
#   (column_name, column_value, sql_data_type)
#   column_value can take one of three forms: [1] the value itself, [2] a ref to a one-element array with the first element as literal text to be inserted, or [3] a ref to a two-element array with the first element as the placeholder text and the second element as the value.
#   sql_data_type is SQL_INTEGER, SQL_BOOLEAN, or other constant from DBI, or a hash ref with driver-specific options. It can also be the string "pg_bytea".
# sequence_name is optional. if specified, insert_row gets a sequence ID and returns the ID of the newly inserted row.
# Example sql_column_list entries:
#   ("room_id", $room_id, SQL_INTEGER)
#   ("start_dt", ["timestamp ?", $start_dt])
#   ("note", $note)
#   ("creation_dt", ["now()"])
sub insert_row($$$;$) {
  my ($dbh, $table_name, $sql_column_list, $sequence_name) = @_;
  my @column_list = @$sql_column_list;
  my $id;
  if (defined($sequence_name)) {
    $id = get_id($dbh, $sequence_name);
    unshift(@column_list, ["id", $id, SQL_INTEGER]);
  }
  my $column_names = join(", ", map {$_->[0]} @column_list);
  my $placeholders = join(", ", map {ref($_->[1]) eq "ARRAY" ? $_->[1]->[0] : "?"} @column_list);
  my $sth = $dbh->prepare("insert into $table_name ($column_names) values ($placeholders)");
  my $bind_column_number = 1;
  my $force_pg_server_prepare = 0;
  for(my $column_list_index = 0; $column_list_index < scalar(@column_list); $column_list_index++) {
    my ($column_name, $column_value, $sql_data_type) = @{$column_list[$column_list_index]};
    if ((ref($column_value) eq "ARRAY") && (scalar(@$column_value) < 2)) {
      next;
    } else {
      my $value_to_bind = (ref($column_value) eq "ARRAY") ? $column_value->[-1] : $column_value;
      if (defined($sql_data_type)) {
	if (!ref($sql_data_type)) {
	  if ($sql_data_type =~ /^pg_/i) {
	    if (lc($sql_data_type) eq "pg_bytea") {
	      $sql_data_type = { "pg_type" => DBD::Pg::PG_BYTEA };
	      # bytea requires pg_server_prepare set to true.
	      # if not, data will swamp connection and you will get out of memory errors from the driver.
	      $force_pg_server_prepare = 1;
	    }
	  }
	}
	$sth->bind_param($bind_column_number, $value_to_bind, $sql_data_type);
      } else {
	$sth->bind_param($bind_column_number, $value_to_bind);
      }
      $bind_column_number++;
    }
  }
  if ($force_pg_server_prepare) {
    $sth->{"pg_server_prepare"} = 1;
  }
  $sth->execute();
  $sth->finish();
  return $id;
}

1;

use strict;
local($^W) = 1;

sub lead_pad {
  my ($value, $num_digits) = @_;
  if (length($value) < $num_digits) {
    $value = "0"x($num_digits - length($value)) . $value;
  }
  return $value;
}

1;

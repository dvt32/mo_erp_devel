use strict;
local($^W) = 1;

use Mail::Sender;
use XML::LibXML;
use HTML::Entities ();
use Encode ();
use decrypt;
use init_database;
use generate_vcard;
use timestamp_to_components;
use utc_datetime_to_local_datetime;
use components_to_timestamp;
use get_local_time_zone;

sub process_broker_referral {
  my $message = shift;
  my $encrypted_body = $message->body()->string();
  my $decrypted_body = decrypt($encrypted_body);
  my $is_spam = 0;
  if (($decrypted_body =~ m{Content-Type:}) && ($decrypted_body =~ m{MIME-Version:})) {
    $is_spam = 1;
  }
  if ($is_spam) {
    # if it is a spam broker referral, do not generate a database entry or an email notification.
    warn "ERROR: skipped spam broker referral.";
    return;
  }
  my $xml_parser = XML::LibXML->new();
  $xml_parser->no_network(1);
  my $dtd_dir = "{{ msgaction_home_dir }}/dtd";
  $xml_parser->load_catalog("file://$dtd_dir/catalog.xml");
  my $xml_document = $xml_parser->parse_string($decrypted_body);
  my $root_node = $xml_document->documentElement();
  my $broker_referral_node = $root_node->find("/broker_referral")->[0];
  my @param_name_list = (qw(request_datetime initial_page_url referrer_url remote_host remote_addr x_forwarded_for user_agent broker_salutation broker_first_name broker_middle_name broker_last_name broker_suffix broker_title broker_company broker_address_1 broker_address_2 broker_city broker_state broker_province broker_zip_code broker_country broker_work_phone_number broker_cell_phone_number broker_work_fax_number broker_email_address type_of_business intended_use amount_of_space rental_start_date duration_of_rental desired_location client_salutation client_first_name client_middle_name client_last_name client_suffix client_title client_company client_address_1 client_address_2 client_city client_state client_province client_zip_code client_country client_work_phone_number client_cell_phone_number client_work_fax_number client_email_address marketing_source note domain_name));
  my %param_map = ();
  my $merged_content = "";
  for my $param_key (@param_name_list) {
    my $param_value = HTML::Entities::decode($broker_referral_node->find($param_key));
    $param_map{$param_key} = qq{$param_value};
    $merged_content .= " " . lc($param_value);
  }
  $merged_content =~ s/[\n\r]/ /gis;
  $merged_content =~ s/^\s+//gis;
  $merged_content =~ s/\s+/ /gis;
  $merged_content =~ s/\s+$//gis;
  my $request_datetime = $param_map{"request_datetime"};
  $request_datetime =~ s/ \w+$//;
  my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($request_datetime);
  ($year, $month, $day, $hour, $minute, $second) = utc_datetime_to_local_datetime($year, $month, $day, $hour, $minute, $second);
  $request_datetime = components_to_timestamp($year, $month, $day, $hour, $minute, $second);
  $request_datetime .= " " . get_local_time_zone($year, $month, $day, $hour, $minute, $second);
  my $desired_location = $param_map{"desired_location"};
  if ($desired_location) {
    if ($desired_location eq "36W20") {
      $desired_location = "36 W. 20th St.";
    } elsif ($desired_location eq "1375Bwy") {
      $desired_location = "1375 Broadway";
    } elsif ($desired_location eq "122W27") {
      $desired_location = "122 W. 27th St.";
    }
  }
  $param_map{"note"} =~ s/\s+$//gs;
  if (($param_map{"note"}) && ($param_map{"note"} !~ /\.$/)) {
    $param_map{"note"} .= ".";
  }
  if ((!$param_map{"state"}) && ($param_map{"province"})) {
    $param_map{"state"} = $param_map{"province"};
  }
  my $rental_start_date = $param_map{"rental_start_date"} || undef;
  if ($rental_start_date) {
    if ($rental_start_date =~ m|^\s*(\d+)/(\d+)/(\d\d+)\s*$|) {
      $rental_start_date =~ s/^\s+//;
      $rental_start_date =~ s/\s+$//;
      if (($1 < 1) || ($1 > 12) || ($2 < 1) || ($2 > 31)) {
	$param_map{"note"} .= " rental_start_date = " . $rental_start_date . ".";
	$rental_start_date = undef;
      }
    } elsif ($rental_start_date =~ m|^\s*(\d+)-(\d+)-(\d\d+)\s*$|) {
      $rental_start_date =~ s/^\s+//;
      $rental_start_date =~ s/\s+$//;
      if (($1 < 1) || ($1 > 12) || ($2 < 1) || ($2 > 31)) {
	$param_map{"note"} .= " rental_start_date = " . $rental_start_date . ".";
	$rental_start_date = undef;
      }
    } else {
      $param_map{"note"} .= " rental_start_date = " . $rental_start_date . ".";
      $rental_start_date = undef;
    }
  }
  # check for spam
  if (($param_map{"broker_first_name"} eq $param_map{"broker_last_name"}) &&
	   ($param_map{"broker_first_name"} eq $param_map{"broker_company"}) &&
	   ($param_map{"broker_first_name"} eq $param_map{"broker_address_1"})) {
    $is_spam = 1;
  }
  if (($param_map{"broker_first_name"} eq $param_map{"broker_middle_name"}) && ($param_map{"broker_middle_name"} eq $param_map{"broker_last_name"}) &&
      (($param_map{"broker_first_name"} =~ /free/i) ||
       ($param_map{"broker_first_name"} =~ /slot/i) ||
       ($param_map{"broker_first_name"} =~ /casino/i) ||
       ($param_map{"broker_first_name"} =~ /buy/i) ||
       ($param_map{"broker_first_name"} =~ /online/i) ||
       ($param_map{"broker_first_name"} =~ /vicodin/i) ||
       ($param_map{"broker_first_name"} =~ /phentermine/i) ||
       ($param_map{"broker_first_name"} =~ /sex/i) ||
       ($param_map{"broker_first_name"} =~ /girl/i) ||
       ($param_map{"broker_first_name"} =~ /cheap/i) ||
       ($param_map{"broker_first_name"} =~ /replica/i) ||
       ($param_map{"broker_first_name"} =~ /rolex/i) ||
       ($param_map{"broker_first_name"} =~ /lipitor/i) ||
       ($param_map{"broker_first_name"} =~ /order/i) ||
       ($param_map{"broker_first_name"} =~ /sale/i) ||
       ($param_map{"broker_first_name"} =~ /pharmacy/i) ||
       ($param_map{"broker_first_name"} =~ /pills/i) ||
       ($param_map{"broker_first_name"} =~ /cialis/i) ||
       ($param_map{"broker_first_name"} =~ /viagra/i) ||
       ($param_map{"broker_first_name"} =~ /payday/i) ||
       ($param_map{"broker_first_name"} =~ /loan/i) ||
       ($param_map{"broker_first_name"} =~ /stratis/i) ||
       ($param_map{"broker_first_name"} =~ /celebrex/i) ||
       ($param_map{"broker_first_name"} =~ /generic/i))) {
    $is_spam = 1;
  }
  if (($param_map{"broker_first_name"} eq $param_map{"broker_middle_name"}) && ($param_map{"broker_middle_name"} eq $param_map{"broker_last_name"}) &&
      (($param_map{"broker_title"} =~ /viagra/i) ||
       ($param_map{"broker_title"} =~ /phentermine/i) ||
       ($param_map{"note"} =~ /viagra/is) ||
       ($param_map{"note"} =~ /phentermine/is))) {
    $is_spam = 1;
  }
  {
    # spam broker referrals like to fill out the same values for a bunch of fields.
    my %duplicate_value_map = ();
    for my $key (qw(broker_first_name broker_middle_name broker_last_name broker_title broker_company broker_address_1 broker_address_2 broker_city)) {
      if ($param_map{$key}) {
	$duplicate_value_map{$param_map{$key}}++;
      }
    }
    for my $key (keys(%duplicate_value_map)) {
      # if the same value appears more than 5 times, it is probably spam.
      if ($duplicate_value_map{$key} > 5) {
	$is_spam = 1;
      }
    }
  };
  {
    # spam broker referrals like to enter the same URLs repeatedly.
    my %duplicate_url_map = ();
    my $num_urls = 0;
    for my $key (qw(broker_first_name broker_middle_name broker_last_name broker_title broker_company broker_address_1 broker_address_2 broker_city broker_work_phone_number broker_cell_phone_number broker_work_fax_number amount_of_space rental_start_date duration_of_rental desired_location marketing_source note)) {
      while(($param_map{$key}) && ($param_map{$key} =~ m{(http://[a-zA-Z0-9_\.\/\&\-]+)}g)) {
	my $url = $1;
	$duplicate_url_map{$url}++;
	$num_urls++;
      }
    }
    for my $key (keys(%duplicate_url_map)) {
      # if the same URL appears more than 4 times or (same URL appears more than twice AND total number of URLs exceeds 5), it is probably spam.
      if (($duplicate_url_map{$key} > 4) || (($duplicate_url_map{$key} > 2) && ($num_urls > 5))) {
	$is_spam = 1;
      }
    }
    # if total number of URLs more than 7, it is probably spam
    if ($num_urls > 7) {
      $is_spam = 1;
    }
  };
  # count number of spammy words in body of content. If count exceeds threshold, consider it spam.
  {
    my $spam_word_count = 0;
    my @spam_word_list = ("poker", "gratis", "jeux", "gratuites", "payday", "advance", "loan", "bonus", "reward", "casino", "holdem", "craps", "gambling", "black jack", "blackjack", "deuces", "stud", "baccarat", "roulette", "ringtone", "ring tone");
    for my $spam_word (@spam_word_list) {
      my @occurrence_list = ($merged_content =~ /$spam_word/g);
      $spam_word_count += scalar(@occurrence_list);
    }
    if ($spam_word_count > 15) {
      $is_spam = 1;
    }
  };
  if ($is_spam) {
    # if it is a spam broker referral, do not generate a database entry or an email notification.
    warn "ERROR: skipped spam broker referral.";
    return;
  }
  my $db_charset = "iso-8859-15"; # ISO-8859-15 AKA Latin-9
  my @field_list = ();
  push(@field_list, ["initial_page_url", Encode::encode($db_charset, $param_map{"initial_page_url"})]);
  push(@field_list, ["referrer_url", Encode::encode($db_charset, $param_map{"referrer_url"})]);
  push(@field_list, ["remote_host", Encode::encode($db_charset, $param_map{"remote_host"})]);
  push(@field_list, ["remote_addr", Encode::encode($db_charset, $param_map{"remote_addr"})]);
  push(@field_list, ["x_forwarded_for", Encode::encode($db_charset, $param_map{"x_forwarded_for"} || "")]);
  push(@field_list, ["user_agent", Encode::encode($db_charset, $param_map{"user_agent"})]);
  push(@field_list, ["broker_salutation", Encode::encode($db_charset, $param_map{"broker_salutation"})]);
  push(@field_list, ["broker_first_name", Encode::encode($db_charset, $param_map{"broker_first_name"})]);
  push(@field_list, ["broker_middle_name", Encode::encode($db_charset, $param_map{"broker_middle_name"})]);
  push(@field_list, ["broker_last_name", Encode::encode($db_charset, $param_map{"broker_last_name"})]);
  push(@field_list, ["broker_suffix", Encode::encode($db_charset, $param_map{"broker_suffix"})]);
  push(@field_list, ["broker_title", Encode::encode($db_charset, $param_map{"broker_title"})]);
  push(@field_list, ["broker_company", Encode::encode($db_charset, $param_map{"broker_company"})]);
  push(@field_list, ["broker_address_1", Encode::encode($db_charset, $param_map{"broker_address_1"})]);
  push(@field_list, ["broker_address_2", Encode::encode($db_charset, $param_map{"broker_address_2"})]);
  push(@field_list, ["broker_city", Encode::encode($db_charset, $param_map{"broker_city"})]);
  push(@field_list, ["broker_state", Encode::encode($db_charset, $param_map{"broker_state"})]);
  push(@field_list, ["broker_zip_code", Encode::encode($db_charset, $param_map{"broker_zip_code"})]);
  push(@field_list, ["broker_country", Encode::encode($db_charset, $param_map{"broker_country"})]);
  push(@field_list, ["broker_work_phone_number", Encode::encode($db_charset, $param_map{"broker_work_phone_number"})]);
  push(@field_list, ["broker_cell_phone_number", Encode::encode($db_charset, $param_map{"broker_cell_phone_number"})]);
  push(@field_list, ["broker_work_fax_number", Encode::encode($db_charset, $param_map{"broker_work_fax_number"})]);
  push(@field_list, ["broker_email_address", Encode::encode($db_charset, $param_map{"broker_email_address"})]);
  push(@field_list, ["type_of_business", Encode::encode($db_charset, $param_map{"type_of_business"})]);
  push(@field_list, ["intended_use", Encode::encode($db_charset, $param_map{"intended_use"})]);
  push(@field_list, ["amount_of_space", Encode::encode($db_charset, $param_map{"amount_of_space"})]);
  push(@field_list, ["rental_start_date", Encode::encode($db_charset, $rental_start_date)]);
  push(@field_list, ["duration_of_rental", Encode::encode($db_charset, $param_map{"duration_of_rental"})]);
  push(@field_list, ["desired_location", Encode::encode($db_charset, $desired_location)]);
  push(@field_list, ["client_salutation", Encode::encode($db_charset, $param_map{"client_salutation"})]);
  push(@field_list, ["client_first_name", Encode::encode($db_charset, $param_map{"client_first_name"})]);
  push(@field_list, ["client_middle_name", Encode::encode($db_charset, $param_map{"client_middle_name"})]);
  push(@field_list, ["client_last_name", Encode::encode($db_charset, $param_map{"client_last_name"})]);
  push(@field_list, ["client_suffix", Encode::encode($db_charset, $param_map{"client_suffix"})]);
  push(@field_list, ["client_title", Encode::encode($db_charset, $param_map{"client_title"})]);
  push(@field_list, ["client_company", Encode::encode($db_charset, $param_map{"client_company"})]);
  push(@field_list, ["client_address_1", Encode::encode($db_charset, $param_map{"client_address_1"})]);
  push(@field_list, ["client_address_2", Encode::encode($db_charset, $param_map{"client_address_2"})]);
  push(@field_list, ["client_city", Encode::encode($db_charset, $param_map{"client_city"})]);
  push(@field_list, ["client_state", Encode::encode($db_charset, $param_map{"client_state"})]);
  push(@field_list, ["client_zip_code", Encode::encode($db_charset, $param_map{"client_zip_code"})]);
  push(@field_list, ["client_country", Encode::encode($db_charset, $param_map{"client_country"})]);
  push(@field_list, ["client_work_phone_number", Encode::encode($db_charset, $param_map{"client_work_phone_number"})]);
  push(@field_list, ["client_cell_phone_number", Encode::encode($db_charset, $param_map{"client_cell_phone_number"})]);
  push(@field_list, ["client_work_fax_number", Encode::encode($db_charset, $param_map{"client_work_fax_number"})]);
  push(@field_list, ["client_email_address", Encode::encode($db_charset, $param_map{"client_email_address"})]);
  push(@field_list, ["marketing_source", Encode::encode($db_charset, $param_map{"marketing_source"})]);
  push(@field_list, ["note", Encode::encode($db_charset, $param_map{"note"})]);
  push(@field_list, ["domain_name", Encode::encode($db_charset, $param_map{"domain_name"})]);
  my $dbh = init_database();
  my $field_names = join(", ", map {$_->[0]} @field_list);
  my $field_placeholders = join(", ", map {"?"} @field_list);
  my $sth = $dbh->prepare("insert into broker_referral_tbl ($field_names) values ($field_placeholders)");
  for(my $i = 0; $i < scalar(@field_list); $i++) {
    my $col_num = $i + 1;
    $sth->bind_param($col_num, $field_list[$i]->[1]);
  }
  $sth->execute();
  $sth->finish();
  $dbh->disconnect();
  my $x_forwarded_for = $param_map{"x_forwarded_for"} || "";
  if ($x_forwarded_for =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) {
    my $iaddr = Socket::inet_aton($x_forwarded_for);
    my $dns_name = gethostbyaddr($iaddr, Socket::AF_INET());
    if ($dns_name) {
      $x_forwarded_for .= " ($dns_name)";
    }
  }
  my $domain_name = $param_map{"domain_name"};
  my $mail_sender = Mail::Sender->new({
    "smtp" => "{{ outbound_smtp_server }}",
    "from" => qq{"Broker Referral at www.$domain_name" <webmaster\@microoffice.com>},
  });
  my $subject = "";
  if ($param_map{"desired_location"}) {
    $subject .= "[" . $param_map{"desired_location"} . "] ";
  } else {
    $subject .= "[unspecified] ";
  }
  $subject .= "(";
  $subject .= $param_map{"broker_first_name"} . " " . $param_map{"broker_last_name"};
  if ($param_map{"broker_company"}) {
    $subject .= ", ";
    $subject .= $param_map{"broker_company"};
  }
  $subject .= ")";
  my @broker_name_component_list;
  if ($param_map{"broker_salutation"}) {
    push(@broker_name_component_list, $param_map{"broker_salutation"});
  }
  push(@broker_name_component_list, $param_map{"broker_first_name"});
  if ($param_map{"broker_middle_name"}) {
    push(@broker_name_component_list, $param_map{"broker_middle_name"});
  }
  push(@broker_name_component_list, $param_map{"broker_last_name"});
  if ($param_map{"broker_suffix"}) {
    push(@broker_name_component_list, $param_map{"broker_suffix"});
  }
  my $broker_name = join(" ", @broker_name_component_list);
  my $broker_title_and_company = "";
  if ($param_map{"broker_title"}) {
    $broker_title_and_company = $param_map{"broker_title"} . ", " . $param_map{"broker_company"};
  } else {
    $broker_title_and_company = $param_map{"broker_company"};
  }
  my $broker_address = $param_map{"broker_address_1"};
  if ($param_map{"broker_address_2"}) {
    $broker_address .= ", " . $param_map{"broker_address_2"};
  }
  my $broker_city_state_zip = "";
  if ($param_map{"broker_city"} && $param_map{"broker_state"}) {
    $broker_city_state_zip .= $param_map{"broker_city"} . ", " . $param_map{"broker_state"};
  } elsif ($param_map{"broker_city"}) {
    $broker_city_state_zip .= $param_map{"broker_city"};
  } elsif ($param_map{"broker_state"}) {
    $broker_city_state_zip .= $param_map{"broker_state"};
  }
  if ($param_map{"broker_zip_code"}) {
    $broker_city_state_zip .= " " . $param_map{"broker_zip_code"};
  }
  if (($param_map{"broker_country"}) && ($param_map{"broker_country"} ne "United States")) {
    $broker_city_state_zip .= " " . $param_map{"broker_country"};
  }
  my @space_needed_component_list;
  if ($param_map{"amount_of_space"}) {
    push(@space_needed_component_list, $param_map{"amount_of_space"});
  }
  if ($param_map{"rental_start_date"}) {
    push(@space_needed_component_list, "start " . $param_map{"rental_start_date"});
  }
  if ($param_map{"duration_of_rental"}) {
    push(@space_needed_component_list, "term " . $param_map{"duration_of_rental"});
  }
  my $space_needed = join(", ", @space_needed_component_list);
  my @client_name_component_list;
  if ($param_map{"client_salutation"}) {
    push(@client_name_component_list, $param_map{"client_salutation"});
  }
  push(@client_name_component_list, $param_map{"client_first_name"});
  if ($param_map{"client_middle_name"}) {
    push(@client_name_component_list, $param_map{"client_middle_name"});
  }
  push(@client_name_component_list, $param_map{"client_last_name"});
  if ($param_map{"client_suffix"}) {
    push(@client_name_component_list, $param_map{"client_suffix"});
  }
  my $client_name = join(" ", @client_name_component_list);
  my $client_title_and_company = "";
  if ($param_map{"client_title"}) {
    $client_title_and_company = $param_map{"client_title"} . ", " . $param_map{"client_company"};
  } else {
    $client_title_and_company = $param_map{"client_company"};
  }
  my $client_address = $param_map{"client_address_1"};
  if ($param_map{"client_address_2"}) {
    $client_address .= ", " . $param_map{"client_address_2"};
  }
  my $client_city_state_zip = "";
  if ($param_map{"client_city"} && $param_map{"client_state"}) {
    $client_city_state_zip .= $param_map{"client_city"} . ", " . $param_map{"client_state"};
  } elsif ($param_map{"client_city"}) {
    $client_city_state_zip .= $param_map{"client_city"};
  } elsif ($param_map{"client_state"}) {
    $client_city_state_zip .= $param_map{"client_state"};
  }
  if ($param_map{"client_zip_code"}) {
    $client_city_state_zip .= " " . $param_map{"client_zip_code"};
  }
  if (($param_map{"client_country"}) && ($param_map{"client_country"} ne "United States")) {
    $client_city_state_zip .= " " . $param_map{"client_country"};
  }
  my $msg_body_content = <<"---END_BLOCK---;";
A broker has filled out the broker referral form. The referral requires a response.

Here are details about the broker referral:

Broker Contact
==============
        Name: $broker_name
  Title/Firm: $broker_title_and_company
     Address: $broker_address
            : $broker_city_state_zip
  Work Phone: $param_map{"broker_work_phone_number"}
  Cell Phone: $param_map{"broker_cell_phone_number"}
  Fax Number: $param_map{"broker_work_fax_number"}
  Email Addr: $param_map{"broker_email_address"}

Client Needs
============
 Type of Bus: $param_map{"type_of_business"}
Intended Use: $param_map{"intended_use"}
Space Needed: $space_needed
    Location: $desired_location

Client Contact
==============
        Name: $client_name
  Title/Firm: $client_title_and_company
     Address: $client_address
            : $client_city_state_zip
  Work Phone: $param_map{"client_work_phone_number"}
  Cell Phone: $param_map{"client_cell_phone_number"}
  Fax Number: $param_map{"client_work_fax_number"}
  Email Addr: $param_map{"client_email_address"}

Miscellaneous
=============
 Mktg Source: $param_map{"marketing_source"}
Request Time: $request_datetime
Initial Page: $param_map{"initial_page_url"}
Referrer URL: $param_map{"referrer_url"}
 Remote Host: $param_map{"remote_host"}
Forwarded IP: $x_forwarded_for
 Domain Name: $param_map{"domain_name"}

Note
====
$param_map{"note"}
---END_BLOCK---;
  my $broker_first_name = ucfirst($param_map{"broker_first_name"});
  my $broker_middle_name = ucfirst($param_map{"broker_middle_name"});
  my $broker_last_name = ucfirst($param_map{"broker_last_name"});
  my $broker_vcard_filename = $broker_first_name . " " . $broker_last_name . ".vcf";
  my $broker_vcard_state = $param_map{"broker_state"};
  my $broker_vcard_content = generate_vcard
    ("salutation" => $param_map{"broker_salutation"},
     "first_name" => $broker_first_name,
     "middle_name" => $broker_middle_name,
     "last_name" => $broker_last_name,
     "suffix" => $param_map{"broker_suffix"},
     "title" => $param_map{"broker_title"},
     "organization" => $param_map{"broker_company"},
     "tel_work_voice" => $param_map{"broker_work_phone_number"},
     "tel_cell_voice" => $param_map{"broker_cell_phone_number"},
     "work_address_1" => $param_map{"broker_address_1"},
     "work_address_2" => $param_map{"broker_address_2"},
     "work_city" => $param_map{"broker_city"},
     "work_state" => $broker_vcard_state,
     "work_zip_code" => $param_map{"broker_zip_code"},
     "work_country" => $param_map{"broker_country"},
     "email_address" => $param_map{"broker_email_address"},);
  my $client_first_name = ucfirst($param_map{"client_first_name"});
  my $client_middle_name = ucfirst($param_map{"client_middle_name"});
  my $client_last_name = ucfirst($param_map{"client_last_name"});
  my $client_vcard_filename = $client_first_name . " " . $client_last_name . ".vcf";
  my $client_vcard_state = $param_map{"client_state"};
  my $client_vcard_content = generate_vcard
    ("salutation" => $param_map{"client_salutation"},
     "first_name" => $client_first_name,
     "middle_name" => $client_middle_name,
     "last_name" => $client_last_name,
     "suffix" => $param_map{"client_suffix"},
     "title" => $param_map{"client_title"},
     "organization" => $param_map{"client_company"},
     "tel_work_voice" => $param_map{"client_work_phone_number"},
     "tel_cell_voice" => $param_map{"client_cell_phone_number"},
     "work_address_1" => $param_map{"client_address_1"},
     "work_address_2" => $param_map{"client_address_2"},
     "work_city" => $param_map{"client_city"},
     "work_state" => $client_vcard_state,
     "work_zip_code" => $param_map{"client_zip_code"},
     "work_country" => $param_map{"client_country"},
     "email_address" => $param_map{"client_email_address"},);
  $mail_sender->OpenMultipart({
    "to" => "sales\@microoffice.com",
    "subject" => $subject,
  });
  $mail_sender->Body({"msg" => $msg_body_content});
  $mail_sender->Part({
    "description" => $broker_vcard_filename,
    "ctype" => qq{text/x-vcard; name="$broker_vcard_filename"},
    "encoding" => 'Base64',
    "disposition" => qq{attachment; filename="$broker_vcard_filename"},
    "msg" => $broker_vcard_content,
  });
  $mail_sender->Part({
    "description" => $client_vcard_filename,
    "ctype" => qq{text/x-vcard; name="$client_vcard_filename"},
    "encoding" => 'Base64',
    "disposition" => qq{attachment; filename="$client_vcard_filename"},
    "msg" => $client_vcard_content,
  });
  $mail_sender->Close();
}

1;

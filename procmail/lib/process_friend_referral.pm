use strict;
local($^W) = 1;

use Mail::Sender;
use XML::LibXML;
use HTML::Entities ();
use Encode ();
use decrypt;
use init_database;
use generate_vcard;
use timestamp_to_components;
use utc_datetime_to_local_datetime;
use components_to_timestamp;
use get_local_time_zone;

sub process_friend_referral {
  my $message = shift;
  my $encrypted_body = $message->body()->string();
  my $decrypted_body = decrypt($encrypted_body);
  my $is_spam = 0;
  if (($decrypted_body =~ m{Content-Type:}) && ($decrypted_body =~ m{MIME-Version:})) {
    $is_spam = 1;
  }
  if ($is_spam) {
    # if it is a spam friend referral, do not generate a database entry or an email notification.
    warn "ERROR: skipped spam friend referral.";
    return;
  }
  my $xml_parser = XML::LibXML->new();
  $xml_parser->no_network(1);
  my $dtd_dir = "{{ msgaction_home_dir }}/dtd";
  $xml_parser->load_catalog("file://$dtd_dir/catalog.xml");
  my $xml_document = $xml_parser->parse_string($decrypted_body);
  my $root_node = $xml_document->documentElement();
  my $friend_referral_node = $root_node->find("/friend_referral")->[0];
  my @param_name_list = (qw(request_datetime initial_page_url referrer_url remote_host remote_addr user_agent tenant_salutation tenant_first_name tenant_middle_name tenant_last_name tenant_suffix tenant_title tenant_company tenant_address_1 tenant_address_2 tenant_city tenant_state tenant_province tenant_zip_code tenant_country tenant_work_phone_number tenant_cell_phone_number tenant_work_fax_number tenant_email_address type_of_business intended_use amount_of_space rental_start_date duration_of_rental desired_location friend_salutation friend_first_name friend_middle_name friend_last_name friend_suffix friend_title friend_company friend_address_1 friend_address_2 friend_city friend_state friend_province friend_zip_code friend_country friend_work_phone_number friend_cell_phone_number friend_work_fax_number friend_email_address note domain_name));
  my %param_map = ();
  my $merged_content = "";
  for my $param_key (@param_name_list) {
    my $param_value = HTML::Entities::decode($friend_referral_node->find($param_key));
    $param_map{$param_key} = qq{$param_value};
    $merged_content .= " " . lc($param_value);
  }
  $merged_content =~ s/[\n\r]/ /gis;
  $merged_content =~ s/^\s+//gis;
  $merged_content =~ s/\s+/ /gis;
  $merged_content =~ s/\s+$//gis;
  my $request_datetime = $param_map{"request_datetime"};
  $request_datetime =~ s/ \w+$//;
  my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($request_datetime);
  ($year, $month, $day, $hour, $minute, $second) = utc_datetime_to_local_datetime($year, $month, $day, $hour, $minute, $second);
  $request_datetime = components_to_timestamp($year, $month, $day, $hour, $minute, $second);
  $request_datetime .= " " . get_local_time_zone($year, $month, $day, $hour, $minute, $second);
  my $desired_location = $param_map{"desired_location"};
  if ($desired_location) {
    if ($desired_location eq "902Bwy") {
      $desired_location = "902 Broadway";
    } elsif ($desired_location eq "1375Bwy") {
      $desired_location = "1375 Broadway";
    } elsif ($desired_location eq "1601Bwy") {
      $desired_location = "1601 Broadway";
    } elsif ($desired_location eq "318W118") {
      $desired_location = "318 W 118th St";
    } elsif ($desired_location eq "18SMich") {
      $desired_location = "18 S Michigan Ave";
    } elsif ($desired_location eq "405WSup") {
      $desired_location = "405 W Superior St";
    } elsif ($desired_location eq "122W27") {
      $desired_location = "122 W 27th St";
    }
  }
  $param_map{"note"} =~ s/\s+$//gs;
  if (($param_map{"note"}) && ($param_map{"note"} !~ /\.$/)) {
    $param_map{"note"} .= ".";
  }
  if ((!$param_map{"state"}) && ($param_map{"province"})) {
    $param_map{"state"} = $param_map{"province"};
  }
  my $rental_start_date = $param_map{"rental_start_date"} || undef;
  if ($rental_start_date) {
    if ($rental_start_date =~ m|^\s*(\d+)/(\d+)/(\d\d+)\s*$|) {
      $rental_start_date =~ s/^\s+//;
      $rental_start_date =~ s/\s+$//;
      if (($1 < 1) || ($1 > 12) || ($2 < 1) || ($2 > 31)) {
	$param_map{"note"} .= " rental_start_date = " . $rental_start_date . ".";
	$rental_start_date = undef;
      }
    } elsif ($rental_start_date =~ m|^\s*(\d+)-(\d+)-(\d\d+)\s*$|) {
      $rental_start_date =~ s/^\s+//;
      $rental_start_date =~ s/\s+$//;
      if (($1 < 1) || ($1 > 12) || ($2 < 1) || ($2 > 31)) {
	$param_map{"note"} .= " rental_start_date = " . $rental_start_date . ".";
	$rental_start_date = undef;
      }
    } else {
      $param_map{"note"} .= " rental_start_date = " . $rental_start_date . ".";
      $rental_start_date = undef;
    }
  }
  # check for spam
  if (($param_map{"tenant_first_name"} eq $param_map{"tenant_last_name"}) &&
	   ($param_map{"tenant_first_name"} eq $param_map{"tenant_company"}) &&
	   ($param_map{"tenant_first_name"} eq $param_map{"tenant_address_1"})) {
    $is_spam = 1;
  }
  if (($param_map{"tenant_first_name"} eq $param_map{"tenant_middle_name"}) && ($param_map{"tenant_middle_name"} eq $param_map{"tenant_last_name"}) &&
      (($param_map{"tenant_first_name"} =~ /free/i) ||
       ($param_map{"tenant_first_name"} =~ /slot/i) ||
       ($param_map{"tenant_first_name"} =~ /casino/i) ||
       ($param_map{"tenant_first_name"} =~ /buy/i) ||
       ($param_map{"tenant_first_name"} =~ /online/i) ||
       ($param_map{"tenant_first_name"} =~ /vicodin/i) ||
       ($param_map{"tenant_first_name"} =~ /phentermine/i) ||
       ($param_map{"tenant_first_name"} =~ /sex/i) ||
       ($param_map{"tenant_first_name"} =~ /girl/i) ||
       ($param_map{"tenant_first_name"} =~ /cheap/i) ||
       ($param_map{"tenant_first_name"} =~ /replica/i) ||
       ($param_map{"tenant_first_name"} =~ /rolex/i) ||
       ($param_map{"tenant_first_name"} =~ /lipitor/i) ||
       ($param_map{"tenant_first_name"} =~ /order/i) ||
       ($param_map{"tenant_first_name"} =~ /sale/i) ||
       ($param_map{"tenant_first_name"} =~ /pharmacy/i) ||
       ($param_map{"tenant_first_name"} =~ /pills/i) ||
       ($param_map{"tenant_first_name"} =~ /cialis/i) ||
       ($param_map{"tenant_first_name"} =~ /viagra/i) ||
       ($param_map{"tenant_first_name"} =~ /payday/i) ||
       ($param_map{"tenant_first_name"} =~ /loan/i) ||
       ($param_map{"tenant_first_name"} =~ /stratis/i) ||
       ($param_map{"tenant_first_name"} =~ /celebrex/i) ||
       ($param_map{"tenant_first_name"} =~ /generic/i))) {
    $is_spam = 1;
  }
  if (($param_map{"tenant_first_name"} eq $param_map{"tenant_middle_name"}) && ($param_map{"tenant_middle_name"} eq $param_map{"tenant_last_name"}) &&
      (($param_map{"tenant_title"} =~ /viagra/i) ||
       ($param_map{"tenant_title"} =~ /phentermine/i) ||
       ($param_map{"note"} =~ /viagra/is) ||
       ($param_map{"note"} =~ /phentermine/is))) {
    $is_spam = 1;
  }
  {
    # spam friend referrals like to fill out the same values for a bunch of fields.
    my %duplicate_value_map = ();
    for my $key (qw(friend_first_name friend_middle_name friend_last_name friend_title friend_company friend_address_1 friend_address_2 friend_city)) {
      if ($param_map{$key}) {
	$duplicate_value_map{$param_map{$key}}++;
      }
    }
    for my $key (keys(%duplicate_value_map)) {
      # if the same value appears more than 5 times, it is probably spam.
      if ($duplicate_value_map{$key} > 5) {
	$is_spam = 1;
      }
    }
  };
  {
    # spam friend referrals like to enter the same URLs repeatedly.
    my %duplicate_url_map = ();
    my $num_urls = 0;
    for my $key (qw(tenant_first_name tenant_middle_name tenant_last_name tenant_title tenant_company tenant_address_1 tenant_address_2 tenant_city tenant_work_phone_number tenant_cell_phone_number tenant_work_fax_number amount_of_space rental_start_date duration_of_rental desired_location note)) {
      while(($param_map{$key}) && ($param_map{$key} =~ m{(http://[a-zA-Z0-9_\.\/\&\-]+)}g)) {
	my $url = $1;
	$duplicate_url_map{$url}++;
	$num_urls++;
      }
    }
    for my $key (keys(%duplicate_url_map)) {
      # if the same URL appears more than 4 times or (same URL appears more than twice AND total number of URLs exceeds 5), it is probably spam.
      if (($duplicate_url_map{$key} > 4) || (($duplicate_url_map{$key} > 2) && ($num_urls > 5))) {
	$is_spam = 1;
      }
    }
    # if total number of URLs more than 7, it is probably spam
    if ($num_urls > 7) {
      $is_spam = 1;
    }
  };
  # count number of spammy words in body of content. If count exceeds threshold, consider it spam.
  {
    my $spam_word_count = 0;
    my @spam_word_list = ("poker", "gratis", "jeux", "gratuites", "payday", "advance", "loan", "bonus", "reward", "casino", "holdem", "craps", "gambling", "black jack", "blackjack", "deuces", "stud", "baccarat", "roulette", "ringtone", "ring tone");
    for my $spam_word (@spam_word_list) {
      my @occurrence_list = ($merged_content =~ /$spam_word/g);
      $spam_word_count += scalar(@occurrence_list);
    }
    if ($spam_word_count > 15) {
      $is_spam = 1;
    }
  };
  if ($is_spam) {
    # if it is a spam friend referral, do not generate a database entry or an email notification.
    warn "ERROR: skipped spam friend referral.";
    return;
  }
  my $db_charset = "iso-8859-15"; # ISO-8859-15 AKA Latin-9
  my @field_list = ();
  push(@field_list, ["initial_page_url", Encode::encode($db_charset, $param_map{"initial_page_url"})]);
  push(@field_list, ["referrer_url", Encode::encode($db_charset, $param_map{"referrer_url"})]);
  push(@field_list, ["remote_host", Encode::encode($db_charset, $param_map{"remote_host"})]);
  push(@field_list, ["remote_addr", Encode::encode($db_charset, $param_map{"remote_addr"})]);
  push(@field_list, ["user_agent", Encode::encode($db_charset, $param_map{"user_agent"})]);
  push(@field_list, ["tenant_salutation", Encode::encode($db_charset, $param_map{"tenant_salutation"})]);
  push(@field_list, ["tenant_first_name", Encode::encode($db_charset, $param_map{"tenant_first_name"})]);
  push(@field_list, ["tenant_middle_name", Encode::encode($db_charset, $param_map{"tenant_middle_name"})]);
  push(@field_list, ["tenant_last_name", Encode::encode($db_charset, $param_map{"tenant_last_name"})]);
  push(@field_list, ["tenant_suffix", Encode::encode($db_charset, $param_map{"tenant_suffix"})]);
  push(@field_list, ["tenant_title", Encode::encode($db_charset, $param_map{"tenant_title"})]);
  push(@field_list, ["tenant_company", Encode::encode($db_charset, $param_map{"tenant_company"})]);
  push(@field_list, ["tenant_address_1", Encode::encode($db_charset, $param_map{"tenant_address_1"})]);
  push(@field_list, ["tenant_address_2", Encode::encode($db_charset, $param_map{"tenant_address_2"})]);
  push(@field_list, ["tenant_city", Encode::encode($db_charset, $param_map{"tenant_city"})]);
  push(@field_list, ["tenant_state", Encode::encode($db_charset, $param_map{"tenant_state"})]);
  push(@field_list, ["tenant_zip_code", Encode::encode($db_charset, $param_map{"tenant_zip_code"})]);
  push(@field_list, ["tenant_country", Encode::encode($db_charset, $param_map{"tenant_country"})]);
  push(@field_list, ["tenant_work_phone_number", Encode::encode($db_charset, $param_map{"tenant_work_phone_number"})]);
  push(@field_list, ["tenant_cell_phone_number", Encode::encode($db_charset, $param_map{"tenant_cell_phone_number"})]);
  push(@field_list, ["tenant_work_fax_number", Encode::encode($db_charset, $param_map{"tenant_work_fax_number"})]);
  push(@field_list, ["tenant_email_address", Encode::encode($db_charset, $param_map{"tenant_email_address"})]);
  push(@field_list, ["type_of_business", Encode::encode($db_charset, $param_map{"type_of_business"})]);
  push(@field_list, ["intended_use", Encode::encode($db_charset, $param_map{"intended_use"})]);
  push(@field_list, ["amount_of_space", Encode::encode($db_charset, $param_map{"amount_of_space"})]);
  push(@field_list, ["rental_start_date", Encode::encode($db_charset, $rental_start_date)]);
  push(@field_list, ["duration_of_rental", Encode::encode($db_charset, $param_map{"duration_of_rental"})]);
  push(@field_list, ["desired_location", Encode::encode($db_charset, $desired_location)]);
  push(@field_list, ["friend_salutation", Encode::encode($db_charset, $param_map{"friend_salutation"})]);
  push(@field_list, ["friend_first_name", Encode::encode($db_charset, $param_map{"friend_first_name"})]);
  push(@field_list, ["friend_middle_name", Encode::encode($db_charset, $param_map{"friend_middle_name"})]);
  push(@field_list, ["friend_last_name", Encode::encode($db_charset, $param_map{"friend_last_name"})]);
  push(@field_list, ["friend_suffix", Encode::encode($db_charset, $param_map{"friend_suffix"})]);
  push(@field_list, ["friend_title", Encode::encode($db_charset, $param_map{"friend_title"})]);
  push(@field_list, ["friend_company", Encode::encode($db_charset, $param_map{"friend_company"})]);
  push(@field_list, ["friend_address_1", Encode::encode($db_charset, $param_map{"friend_address_1"})]);
  push(@field_list, ["friend_address_2", Encode::encode($db_charset, $param_map{"friend_address_2"})]);
  push(@field_list, ["friend_city", Encode::encode($db_charset, $param_map{"friend_city"})]);
  push(@field_list, ["friend_state", Encode::encode($db_charset, $param_map{"friend_state"})]);
  push(@field_list, ["friend_zip_code", Encode::encode($db_charset, $param_map{"friend_zip_code"})]);
  push(@field_list, ["friend_country", Encode::encode($db_charset, $param_map{"friend_country"})]);
  push(@field_list, ["friend_work_phone_number", Encode::encode($db_charset, $param_map{"friend_work_phone_number"})]);
  push(@field_list, ["friend_cell_phone_number", Encode::encode($db_charset, $param_map{"friend_cell_phone_number"})]);
  push(@field_list, ["friend_work_fax_number", Encode::encode($db_charset, $param_map{"friend_work_fax_number"})]);
  push(@field_list, ["friend_email_address", Encode::encode($db_charset, $param_map{"friend_email_address"})]);
  push(@field_list, ["note", Encode::encode($db_charset, $param_map{"note"})]);
  push(@field_list, ["domain_name", Encode::encode($db_charset, $param_map{"domain_name"})]);
  my $dbh = init_database();
  my $field_names = join(", ", map {$_->[0]} @field_list);
  my $field_placeholders = join(", ", map {"?"} @field_list);
  my $sth = $dbh->prepare("insert into friend_referral_tbl ($field_names) values ($field_placeholders)");
  for(my $i = 0; $i < scalar(@field_list); $i++) {
    my $col_num = $i + 1;
    $sth->bind_param($col_num, $field_list[$i]->[1]);
  }
  $sth->execute();
  $sth->finish();
  $dbh->disconnect();
  my $domain_name = $param_map{"domain_name"};
  my $mail_sender = Mail::Sender->new({
    "smtp" => "{{ outbound_smtp_server }}",
    "from" => qq{"Friend Referral at www.$domain_name" <webmaster\@microoffice.com>},
  });
  my $subject = "";
  if ($param_map{"desired_location"}) {
    $subject .= "[" . $param_map{"desired_location"} . "] ";
  } else {
    $subject .= "[unspecified] ";
  }
  $subject .= "(";
  $subject .= $param_map{"tenant_first_name"} . " " . $param_map{"tenant_last_name"};
  if ($param_map{"tenant_company"}) {
    $subject .= ", ";
    $subject .= $param_map{"tenant_company"};
  }
  $subject .= ")";
  my @tenant_name_component_list;
  if ($param_map{"tenant_salutation"}) {
    push(@tenant_name_component_list, $param_map{"tenant_salutation"});
  }
  push(@tenant_name_component_list, $param_map{"tenant_first_name"});
  if ($param_map{"tenant_middle_name"}) {
    push(@tenant_name_component_list, $param_map{"tenant_middle_name"});
  }
  push(@tenant_name_component_list, $param_map{"tenant_last_name"});
  if ($param_map{"tenant_suffix"}) {
    push(@tenant_name_component_list, $param_map{"tenant_suffix"});
  }
  my $tenant_name = join(" ", @tenant_name_component_list);
  my $tenant_title_and_company = "";
  if ($param_map{"tenant_title"}) {
    $tenant_title_and_company = $param_map{"tenant_title"} . ", " . $param_map{"tenant_company"};
  } else {
    $tenant_title_and_company = $param_map{"tenant_company"};
  }
  my $tenant_address = $param_map{"tenant_address_1"};
  if ($param_map{"tenant_address_2"}) {
    $tenant_address .= ", " . $param_map{"tenant_address_2"};
  }
  my $tenant_city_state_zip = "";
  if ($param_map{"tenant_city"} && $param_map{"tenant_state"}) {
    $tenant_city_state_zip .= $param_map{"tenant_city"} . ", " . $param_map{"tenant_state"};
  } elsif ($param_map{"tenant_city"}) {
    $tenant_city_state_zip .= $param_map{"tenant_city"};
  } elsif ($param_map{"tenant_state"}) {
    $tenant_city_state_zip .= $param_map{"tenant_state"};
  }
  if ($param_map{"tenant_zip_code"}) {
    $tenant_city_state_zip .= " " . $param_map{"tenant_zip_code"};
  }
  if (($param_map{"tenant_country"}) && ($param_map{"tenant_country"} ne "United States")) {
    $tenant_city_state_zip .= " " . $param_map{"tenant_country"};
  }
  my @space_needed_component_list;
  if ($param_map{"amount_of_space"}) {
    push(@space_needed_component_list, $param_map{"amount_of_space"});
  }
  if ($param_map{"rental_start_date"}) {
    push(@space_needed_component_list, "start " . $param_map{"rental_start_date"});
  }
  if ($param_map{"duration_of_rental"}) {
    push(@space_needed_component_list, "term " . $param_map{"duration_of_rental"});
  }
  my $space_needed = join(", ", @space_needed_component_list);
  my @friend_name_component_list;
  if ($param_map{"friend_salutation"}) {
    push(@friend_name_component_list, $param_map{"friend_salutation"});
  }
  push(@friend_name_component_list, $param_map{"friend_first_name"});
  if ($param_map{"friend_middle_name"}) {
    push(@friend_name_component_list, $param_map{"friend_middle_name"});
  }
  push(@friend_name_component_list, $param_map{"friend_last_name"});
  if ($param_map{"friend_suffix"}) {
    push(@friend_name_component_list, $param_map{"friend_suffix"});
  }
  my $friend_name = join(" ", @friend_name_component_list);
  my $friend_title_and_company = "";
  if ($param_map{"friend_title"}) {
    $friend_title_and_company = $param_map{"friend_title"} . ", " . $param_map{"friend_company"};
  } else {
    $friend_title_and_company = $param_map{"friend_company"};
  }
  my $friend_address = $param_map{"friend_address_1"};
  if ($param_map{"friend_address_2"}) {
    $friend_address .= ", " . $param_map{"friend_address_2"};
  }
  my $friend_city_state_zip = "";
  if ($param_map{"friend_city"} && $param_map{"friend_state"}) {
    $friend_city_state_zip .= $param_map{"friend_city"} . ", " . $param_map{"friend_state"};
  } elsif ($param_map{"friend_city"}) {
    $friend_city_state_zip .= $param_map{"friend_city"};
  } elsif ($param_map{"friend_state"}) {
    $friend_city_state_zip .= $param_map{"friend_state"};
  }
  if ($param_map{"friend_zip_code"}) {
    $friend_city_state_zip .= " " . $param_map{"friend_zip_code"};
  }
  if (($param_map{"friend_country"}) && ($param_map{"friend_country"} ne "United States")) {
    $friend_city_state_zip .= " " . $param_map{"friend_country"};
  }
  my $msg_body_content = <<"---END_BLOCK---;";
A tenant has filled out the friend referral form. The referral requires a response.

Here are details about the friend referral:

Tenant Contact
==============
        Name: $tenant_name
  Title/Firm: $tenant_title_and_company
     Address: $tenant_address
            : $tenant_city_state_zip
  Work Phone: $param_map{"tenant_work_phone_number"}
  Cell Phone: $param_map{"tenant_cell_phone_number"}
  Fax Number: $param_map{"tenant_work_fax_number"}
  Email Addr: $param_map{"tenant_email_address"}

Friend Needs
============
 Type of Bus: $param_map{"type_of_business"}
Intended Use: $param_map{"intended_use"}
Space Needed: $space_needed
    Location: $desired_location

Friend Contact
==============
        Name: $friend_name
  Title/Firm: $friend_title_and_company
     Address: $friend_address
            : $friend_city_state_zip
  Work Phone: $param_map{"friend_work_phone_number"}
  Cell Phone: $param_map{"friend_cell_phone_number"}
  Fax Number: $param_map{"friend_work_fax_number"}
  Email Addr: $param_map{"friend_email_address"}

Miscellaneous
=============
Request Time: $request_datetime
Initial Page: $param_map{"initial_page_url"}
Referrer URL: $param_map{"referrer_url"}
 Remote Host: $param_map{"remote_host"}
 Domain Name: $param_map{"domain_name"}

Note
====
$param_map{"note"}
---END_BLOCK---;
  my $tenant_first_name = ucfirst($param_map{"tenant_first_name"});
  my $tenant_middle_name = ucfirst($param_map{"tenant_middle_name"});
  my $tenant_last_name = ucfirst($param_map{"tenant_last_name"});
  my $tenant_vcard_filename = $tenant_first_name . " " . $tenant_last_name . ".vcf";
  my $tenant_vcard_state = $param_map{"tenant_state"};
  my $tenant_vcard_content = generate_vcard
    ("salutation" => $param_map{"tenant_salutation"},
     "first_name" => $tenant_first_name,
     "middle_name" => $tenant_middle_name,
     "last_name" => $tenant_last_name,
     "suffix" => $param_map{"tenant_suffix"},
     "title" => $param_map{"tenant_title"},
     "organization" => $param_map{"tenant_company"},
     "tel_work_voice" => $param_map{"tenant_work_phone_number"},
     "tel_cell_voice" => $param_map{"tenant_cell_phone_number"},
     "work_address_1" => $param_map{"tenant_address_1"},
     "work_address_2" => $param_map{"tenant_address_2"},
     "work_city" => $param_map{"tenant_city"},
     "work_state" => $tenant_vcard_state,
     "work_zip_code" => $param_map{"tenant_zip_code"},
     "work_country" => $param_map{"tenant_country"},
     "email_address" => $param_map{"tenant_email_address"},);
  my $friend_first_name = ucfirst($param_map{"friend_first_name"});
  my $friend_middle_name = ucfirst($param_map{"friend_middle_name"});
  my $friend_last_name = ucfirst($param_map{"friend_last_name"});
  my $friend_vcard_filename = $friend_first_name . " " . $friend_last_name . ".vcf";
  my $friend_vcard_state = $param_map{"friend_state"};
  my $friend_vcard_content = generate_vcard
    ("salutation" => $param_map{"friend_salutation"},
     "first_name" => $friend_first_name,
     "middle_name" => $friend_middle_name,
     "last_name" => $friend_last_name,
     "suffix" => $param_map{"friend_suffix"},
     "title" => $param_map{"friend_title"},
     "organization" => $param_map{"friend_company"},
     "tel_work_voice" => $param_map{"friend_work_phone_number"},
     "tel_cell_voice" => $param_map{"friend_cell_phone_number"},
     "work_address_1" => $param_map{"friend_address_1"},
     "work_address_2" => $param_map{"friend_address_2"},
     "work_city" => $param_map{"friend_city"},
     "work_state" => $friend_vcard_state,
     "work_zip_code" => $param_map{"friend_zip_code"},
     "work_country" => $param_map{"friend_country"},
     "email_address" => $param_map{"friend_email_address"},);
  $mail_sender->OpenMultipart({
    "to" => "marketing\@microoffice.com",
    "subject" => $subject,
  });
  $mail_sender->Body({"msg" => $msg_body_content});
  $mail_sender->Part({
    "description" => $tenant_vcard_filename,
    "ctype" => qq{text/x-vcard; name="$tenant_vcard_filename"},
    "encoding" => 'Base64',
    "disposition" => qq{attachment; filename="$tenant_vcard_filename"},
    "msg" => $tenant_vcard_content,
  });
  $mail_sender->Part({
    "description" => $friend_vcard_filename,
    "ctype" => qq{text/x-vcard; name="$friend_vcard_filename"},
    "encoding" => 'Base64',
    "disposition" => qq{attachment; filename="$friend_vcard_filename"},
    "msg" => $friend_vcard_content,
  });
  $mail_sender->Close();
}

1;

use strict;
local($^W) = 1;

use Mail::Sender;
use XML::LibXML;
use HTML::Entities ();
use Encode ();
use Socket ();
use decrypt;
use init_database;
use generate_vcard;
use timestamp_to_components;
use utc_datetime_to_local_datetime;
use components_to_timestamp;
use get_local_time_zone;

sub process_prospective_customer {
  my $message = shift;
  my $encrypted_body = $message->body()->string();
  my $decrypted_body = decrypt($encrypted_body);
  my $is_spam = 0;
  if (($decrypted_body =~ m{Content-Type:}) && ($decrypted_body =~ m{MIME-Version:})) {
    $is_spam = 1;
  }
  if ($is_spam) {
    # if it is a spam lead, do not generate a database entry or an email notification.
    warn "ERROR: skipped spam lead.";
    return;
  }
  my $xml_parser = XML::LibXML->new();
  $xml_parser->no_network(1);
  my $dtd_dir = "{{ msgaction_home_dir }}/dtd";
  $xml_parser->load_catalog("file://$dtd_dir/catalog.xml");
  my $xml_document = $xml_parser->parse_string($decrypted_body);
  my $root_node = $xml_document->documentElement();
  my $lead_node = $root_node->find("/lead")->[0];
  my @param_name_list = (qw(request_datetime initial_page_url referrer_url remote_host remote_addr x_forwarded_for user_agent salutation first_name middle_name last_name suffix title company business_description address_1 address_2 city state province zip_code country work_phone_number home_phone_number cell_phone_number other_phone_number fax_number email_address web_site amount_of_space start_date duration_of_rental desired_location marketing_source notes domain_name));
  my %param_map = ();
  my $merged_content = "";
  for my $param_key (@param_name_list) {
    my $param_value = HTML::Entities::decode($lead_node->find($param_key));
    $param_map{$param_key} = qq{$param_value};
    $merged_content .= " " . lc($param_value);
  }
  $merged_content =~ s/[\n\r]/ /gis;
  $merged_content =~ s/^\s+//gis;
  $merged_content =~ s/\s+/ /gis;
  $merged_content =~ s/\s+$//gis;
  my $request_datetime = $param_map{"request_datetime"};
  $request_datetime =~ s/ \w+$//;
  my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($request_datetime);
  ($year, $month, $day, $hour, $minute, $second) = utc_datetime_to_local_datetime($year, $month, $day, $hour, $minute, $second);
  $request_datetime = components_to_timestamp($year, $month, $day, $hour, $minute, $second);
  $request_datetime .= " " . get_local_time_zone($year, $month, $day, $hour, $minute, $second);
  my $desired_location = $param_map{"desired_location"};
  if ($desired_location) {
    if ($desired_location eq "36W20") {
      $desired_location = "36 W. 20th St.";
    } elsif ($desired_location eq "1375Bwy") {
      $desired_location = "1375 Broadway";
    } elsif ($desired_location eq "122W27") {
      $desired_location = "122 W. 27th St.";
    }
  }
  $param_map{"notes"} =~ s/\s+$//gs;
  if (($param_map{"notes"}) && ($param_map{"notes"} !~ /\.$/)) {
    $param_map{"notes"} .= ".";
  }
  if ((!$param_map{"state"}) && ($param_map{"province"})) {
    $param_map{"state"} = $param_map{"province"};
  }
  my $rental_start_date = $param_map{"start_date"} || undef;
  if ($rental_start_date) {
    if ($rental_start_date =~ m|^\s*(\d{1,2})/(\d{1,2})/(\d{2,4})\s*$|) {
      $rental_start_date =~ s/^\s+//;
      $rental_start_date =~ s/\s+$//;
      if (($1 < 1) || ($1 > 12) || ($2 < 1) || ($2 > 31)) {
	$param_map{"notes"} .= " rental_start_date = " . $rental_start_date . ".";
	$rental_start_date = undef;
      }
    } elsif ($rental_start_date =~ m|^\s*(\d{1,2})-(\d{1,2})-(\d{2,4})\s*$|) {
      $rental_start_date =~ s/^\s+//;
      $rental_start_date =~ s/\s+$//;
      if (($1 < 1) || ($1 > 12) || ($2 < 1) || ($2 > 31)) {
	$param_map{"notes"} .= " rental_start_date = " . $rental_start_date . ".";
	$rental_start_date = undef;
      }
    } else {
      $param_map{"notes"} .= " rental_start_date = " . $rental_start_date . ".";
      $rental_start_date = undef;
    }
  }
  # check for spam
  if (($param_map{"first_name"} eq $param_map{"last_name"}) &&
      ($param_map{"address_1"} eq $param_map{"address_2"}) && ($param_map{"address_1"} eq $param_map{"email_address"}) &&
      ($param_map{"notes"} =~ /tamiflu/is)) {
  } elsif (($param_map{"first_name"} eq $param_map{"last_name"}) &&
	   ($param_map{"first_name"} eq $param_map{"company"}) &&
	   ($param_map{"first_name"} eq $param_map{"business_description"}) &&
	   ($param_map{"first_name"} eq $param_map{"address_1"})) {
    $is_spam = 1;
  }
  if (($param_map{"first_name"} eq $param_map{"middle_name"}) && ($param_map{"middle_name"} eq $param_map{"last_name"}) &&
      (($param_map{"first_name"} =~ /free/i) ||
       ($param_map{"first_name"} =~ /slot/i) ||
       ($param_map{"first_name"} =~ /casino/i) ||
       ($param_map{"first_name"} =~ /buy/i) ||
       ($param_map{"first_name"} =~ /online/i) ||
       ($param_map{"first_name"} =~ /vicodin/i) ||
       ($param_map{"first_name"} =~ /phentermine/i) ||
       ($param_map{"first_name"} =~ /sex/i) ||
       ($param_map{"first_name"} =~ /girl/i) ||
       ($param_map{"first_name"} =~ /cheap/i) ||
       ($param_map{"first_name"} =~ /replica/i) ||
       ($param_map{"first_name"} =~ /rolex/i) ||
       ($param_map{"first_name"} =~ /lipitor/i) ||
       ($param_map{"first_name"} =~ /order/i) ||
       ($param_map{"first_name"} =~ /sale/i) ||
       ($param_map{"first_name"} =~ /pharmacy/i) ||
       ($param_map{"first_name"} =~ /pills/i) ||
       ($param_map{"first_name"} =~ /cialis/i) ||
       ($param_map{"first_name"} =~ /viagra/i) ||
       ($param_map{"first_name"} =~ /payday/i) ||
       ($param_map{"first_name"} =~ /loan/i) ||
       ($param_map{"first_name"} =~ /stratis/i) ||
       ($param_map{"first_name"} =~ /celebrex/i) ||
       ($param_map{"first_name"} =~ /generic/i))) {
    $is_spam = 1;
  }
  if (($param_map{"first_name"} eq $param_map{"middle_name"}) && ($param_map{"middle_name"} eq $param_map{"last_name"}) &&
      (($param_map{"title"} =~ /viagra/i) ||
       ($param_map{"title"} =~ /phentermine/i) ||
       ($param_map{"notes"} =~ /viagra/is) ||
       ($param_map{"notes"} =~ /phentermine/is))) {
    $is_spam = 1;
  }
  if (($param_map{"salutation"} =~ /^\[\-+\]$/) &&
      ($param_map{"first_name"} eq $param_map{"last_name"}) &&
      ($param_map{"first_name"} eq $param_map{"title"}) &&
      ($param_map{"business_description"} =~ /a\s+href=/s) &&
      ($param_map{"desired_location"} eq "[Make Selection]")) {
    $is_spam = 1;
  }
  if (($param_map{"salutation"} =~ /^\[\-+\]$/) &&
      ($param_map{"first_name"} eq $param_map{"last_name"}) &&
      ($param_map{"business_description"} =~ /a\s+href=/s) &&
      ($param_map{"desired_location"} eq "[Make Selection]")) {
    $is_spam = 1;
  }
  # If number of occurrences of "a href=" or "url=" exceeds 2 in title, business description, and notes, it is likely spam.
  if (((scalar(() = $param_map{"title"} =~ /a\s+href=/g) > 2) ||
       (scalar(() = $param_map{"title"} =~ /url=/g) > 2)) &&
      ((scalar(() = $param_map{"business_description"} =~ /a\s+href=/g) > 2) ||
       (scalar(() = $param_map{"business_description"} =~ /url=/g) > 2)) &&
      ((scalar(() = $param_map{"notes"} =~ /a\s+href=/g) > 2) ||
       (scalar(() = $param_map{"notes"} =~ /url=/g) > 2))) {
    $is_spam = 1;
  }
  # If first name and last name are same and number of occurrences of "http://" exceeds 5 in business description, it is likely spam.
  if (($param_map{"first_name"} eq $param_map{"last_name"}) &&
      (scalar(() = $param_map{"business_description"} =~ m{http://}g) > 5)) {
    $is_spam = 1;
  }
  {
    # spam leads like to fill out the same values for a bunch of fields.
    my %duplicate_value_map = ();
    for my $key (qw(first_name middle_name last_name title company business_description address_1 address_2 city)) {
      if ($param_map{$key}) {
	$duplicate_value_map{$param_map{$key}}++;
      }
    }
    for my $key (keys(%duplicate_value_map)) {
      # if the same value appears more than 5 times, it is probably spam.
      if ($duplicate_value_map{$key} > 5) {
	$is_spam = 1;
      }
    }
  };
  {
    # spam leads like to enter the same URLs repeatedly.
    my %duplicate_url_map = ();
    my $num_urls = 0;
    for my $key (qw(first_name middle_name last_name title company business_description address_1 address_2 city work_phone_number home_phone_number cell_phone_number other_phone_number fax_number web_site amount_of_space rental_start_date duration_of_rental desired_location marketing_source notes)) {
      while(($param_map{$key}) && ($param_map{$key} =~ m{(http://[a-zA-Z0-9_\.\/\&\-]+)}g)) {
	my $url = $1;
	$duplicate_url_map{$url}++;
	$num_urls++;
      }
    }
    for my $key (keys(%duplicate_url_map)) {
      # if the same URL appears more than 4 times or (same URL appears more than twice AND total number of URLs exceeds 5), it is probably spam.
      if (($duplicate_url_map{$key} > 4) || (($duplicate_url_map{$key} > 2) && ($num_urls > 5))) {
	$is_spam = 1;
      }
    }
    # if total number of URLs more than 7, it is probably spam
    if ($num_urls > 7) {
      $is_spam = 1;
    }
  };
  # count number of spammy words in body of content. If count exceeds threshold, consider it spam.
  {
    my $spam_word_count = 0;
    my @spam_word_list = ("poker", "gratis", "jeux", "gratuites", "payday", "advance", "loan", "bonus", "reward", "casino", "holdem", "craps", "gambling", "black jack", "blackjack", "deuces", "stud", "baccarat", "roulette", "ringtone", "ring tone");
    for my $spam_word (@spam_word_list) {
      my @occurrence_list = ($merged_content =~ /$spam_word/g);
      $spam_word_count += scalar(@occurrence_list);
    }
    if ($spam_word_count > 15) {
      $is_spam = 1;
    }
  };
  if ($is_spam) {
    # if it is a spam lead, do not generate a database entry or an email notification.
    warn "ERROR: skipped spam lead.";
    return;
  }
  my $db_charset = "iso-8859-15"; # ISO-8859-15 AKA Latin-9
  my @field_list = ();
  push(@field_list, ["initial_contact_dt", Encode::encode($db_charset, $request_datetime)]);
  push(@field_list, ["initial_page_url", Encode::encode($db_charset, $param_map{"initial_page_url"})]);
  push(@field_list, ["referrer_url", Encode::encode($db_charset, $param_map{"referrer_url"})]);
  push(@field_list, ["remote_host", Encode::encode($db_charset, $param_map{"remote_host"})]);
  push(@field_list, ["remote_addr", Encode::encode($db_charset, $param_map{"remote_addr"})]);
  push(@field_list, ["x_forwarded_for", Encode::encode($db_charset, $param_map{"x_forwarded_for"} || "")]);
  push(@field_list, ["user_agent", Encode::encode($db_charset, $param_map{"user_agent"})]);
  push(@field_list, ["salutation", Encode::encode($db_charset, $param_map{"salutation"})]);
  push(@field_list, ["first_name", Encode::encode($db_charset, $param_map{"first_name"})]);
  push(@field_list, ["middle_name", Encode::encode($db_charset, $param_map{"middle_name"})]);
  push(@field_list, ["last_name", Encode::encode($db_charset, $param_map{"last_name"})]);
  push(@field_list, ["suffix", Encode::encode($db_charset, $param_map{"suffix"})]);
  push(@field_list, ["title", Encode::encode($db_charset, $param_map{"title"})]);
  push(@field_list, ["company", Encode::encode($db_charset, $param_map{"company"})]);
  push(@field_list, ["business_description", Encode::encode($db_charset, $param_map{"business_description"})]);
  push(@field_list, ["address_1", Encode::encode($db_charset, $param_map{"address_1"})]);
  push(@field_list, ["address_2", Encode::encode($db_charset, $param_map{"address_2"})]);
  push(@field_list, ["city", Encode::encode($db_charset, $param_map{"city"})]);
  push(@field_list, ["state", Encode::encode($db_charset, $param_map{"state"})]);
  push(@field_list, ["zip_code", Encode::encode($db_charset, $param_map{"zip_code"})]);
  push(@field_list, ["country", Encode::encode($db_charset, $param_map{"country"})]);
  push(@field_list, ["work_phone_number", Encode::encode($db_charset, $param_map{"work_phone_number"})]);
  push(@field_list, ["home_phone_number", Encode::encode($db_charset, $param_map{"home_phone_number"})]);
  push(@field_list, ["cell_phone_number", Encode::encode($db_charset, $param_map{"cell_phone_number"})]);
  push(@field_list, ["other_phone_number", Encode::encode($db_charset, $param_map{"other_phone_number"})]);
  push(@field_list, ["fax_number", Encode::encode($db_charset, $param_map{"fax_number"})]);
  push(@field_list, ["email_address", Encode::encode($db_charset, $param_map{"email_address"})]);
  push(@field_list, ["web_site", Encode::encode($db_charset, $param_map{"web_site"})]);
  push(@field_list, ["amount_of_space", Encode::encode($db_charset, $param_map{"amount_of_space"})]);
  push(@field_list, ["rental_start_date", Encode::encode($db_charset, $rental_start_date)]);
  push(@field_list, ["duration_of_rental", Encode::encode($db_charset, $param_map{"duration_of_rental"})]);
  push(@field_list, ["desired_location", Encode::encode($db_charset, $desired_location)]);
  push(@field_list, ["initial_contact_method", "web"]);
  push(@field_list, ["sales_status", "live"]);
  push(@field_list, ["sales_stage", "1-awaiting response from us"]);
  push(@field_list, ["marketing_source", Encode::encode($db_charset, $param_map{"marketing_source"})]);
  push(@field_list, ["notes", Encode::encode($db_charset, $param_map{"notes"})]);
  push(@field_list, ["domain_name", Encode::encode($db_charset, $param_map{"domain_name"})]);
  my $dbh = init_database();
  my $field_names = join(", ", map {$_->[0]} @field_list);
  my $field_placeholders = join(", ", map {"?"} @field_list);
  my $sth = $dbh->prepare("insert into prospective_customer_tbl ($field_names) values ($field_placeholders)");
  for(my $i = 0; $i < scalar(@field_list); $i++) {
    my $col_num = $i + 1;
    $sth->bind_param($col_num, $field_list[$i]->[1]);
  }
  $sth->execute();
  $sth->finish();
  $dbh->disconnect();
  my $x_forwarded_for = $param_map{"x_forwarded_for"} || "";
  if ($x_forwarded_for =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) {
    my $iaddr = Socket::inet_aton($x_forwarded_for);
    my $dns_name = gethostbyaddr($iaddr, Socket::AF_INET());
    if ($dns_name) {
      $x_forwarded_for .= " ($dns_name)";
    }
  }
  my $domain_name = $param_map{"domain_name"};
  my $mail_sender = Mail::Sender->new({
    "smtp" => "{{ outbound_smtp_server }}",
    "from" => qq{"Lead at www.$domain_name" <webmaster\@microoffice.com>},
  });
  my $company = $param_map{"company"};
  my $subject = "";
  if ($param_map{"desired_location"}) {
    $subject .= "[" . $param_map{"desired_location"} . "] ";
  } else {
    $subject .= "[unspecified] ";
  }
  $subject .= "(";
  $subject .= $param_map{"first_name"} . " " . $param_map{"last_name"};
  if ($param_map{"company"}) {
    $subject .= ", ";
    $subject .= $param_map{"company"};
  }
  $subject .= ")";
  my @name_component_list;
  if ($param_map{"salutation"}) {
    push(@name_component_list, $param_map{"salutation"});
  }
  push(@name_component_list, $param_map{"first_name"});
  if ($param_map{"middle_name"}) {
    push(@name_component_list, $param_map{"middle_name"});
  }
  push(@name_component_list, $param_map{"last_name"});
  if ($param_map{"suffix"}) {
    push(@name_component_list, $param_map{"suffix"});
  }
  my $name = join(" ", @name_component_list);
  my $title_and_company = "";
  if ($param_map{"title"}) {
    $title_and_company = $param_map{"title"} . ", " . $param_map{"company"};
  } else {
    $title_and_company = $param_map{"company"};
  }
  my $address = $param_map{"address_1"};
  if ($param_map{"address_2"}) {
    $address .= ", " . $param_map{"address_2"};
  }
  my $city_state_zip = "";
  if ($param_map{"city"} && $param_map{"state"}) {
    $city_state_zip .= $param_map{"city"} . ", " . $param_map{"state"};
  } elsif ($param_map{"city"}) {
    $city_state_zip .= $param_map{"city"};
  } elsif ($param_map{"state"}) {
    $city_state_zip .= $param_map{"state"};
  }
  if ($param_map{"zip_code"}) {
    $city_state_zip .= " " . $param_map{"zip_code"};
  }
  if (($param_map{"country"}) && ($param_map{"country"} ne "United States")) {
    $city_state_zip .= " " . $param_map{"country"};
  }
  my @space_needed_component_list;
  if ($param_map{"amount_of_space"}) {
    push(@space_needed_component_list, $param_map{"amount_of_space"});
  }
  if ($param_map{"start_date"}) {
    push(@space_needed_component_list, "start " . $param_map{"start_date"});
  }
  if ($param_map{"duration_of_rental"}) {
    push(@space_needed_component_list, "term " . $param_map{"duration_of_rental"});
  }
  my $space_needed = join(", ", @space_needed_component_list);
  my $msg_body_content = <<"---END_BLOCK---;";
A lead has filled out the contact form. The lead requires a response.

Here are details about the lead:

        Name: $name
Title/Compan: $title_and_company
 Description: $param_map{"business_description"}
     Address: $address
            : $city_state_zip
  Work Phone: $param_map{"work_phone_number"}
  Home Phone: $param_map{"home_phone_number"}
  Cell Phone: $param_map{"cell_phone_number"}
 Other Phone: $param_map{"other_phone_number"}
  Fax Number: $param_map{"fax_number"}
  Email Addr: $param_map{"email_address"}
    Web Site: $param_map{"web_site"}
Space Needed: $space_needed
    Location: $desired_location
 Mktg Source: $param_map{"marketing_source"}
Request Time: $request_datetime
Initial Page: $param_map{"initial_page_url"}
Referrer URL: $param_map{"referrer_url"}
 Remote Host: $param_map{"remote_host"}
Forwarded IP: $x_forwarded_for
 Domain Name: $param_map{"domain_name"}

Notes
=====
$param_map{"notes"}
---END_BLOCK---;
  my $first_name = ucfirst($param_map{"first_name"});
  my $middle_name = ucfirst($param_map{"middle_name"});
  my $last_name = ucfirst($param_map{"last_name"});
  my $vcard_filename = $first_name . " " . $last_name . ".vcf";
  my $vcard_state = $param_map{"state"};
  my $vcard_content = generate_vcard
    ("salutation" => $param_map{"salutation"},
     "first_name" => $first_name,
     "middle_name" => $middle_name,
     "last_name" => $last_name,
     "suffix" => $param_map{"suffix"},
     "title" => $param_map{"title"},
     "organization" => $param_map{"company"},
     "tel_work_voice" => $param_map{"work_phone_number"},
     "tel_home_voice" => $param_map{"home_phone_number"},
     "tel_cell_voice" => $param_map{"cell_phone_number"},
     "work_address_1" => $param_map{"address_1"},
     "work_address_2" => $param_map{"address_2"},
     "work_city" => $param_map{"city"},
     "work_state" => $vcard_state,
     "work_zip_code" => $param_map{"zip_code"},
     "work_country" => $param_map{"country"},
     "email_address" => $param_map{"email_address"},);
  $mail_sender->OpenMultipart({
    "to" => "sales\@microoffice.com",
    "subject" => $subject,
  });
  $mail_sender->Body({"msg" => $msg_body_content});
  $mail_sender->Part({
    "description" => $vcard_filename,
    "ctype" => qq{text/x-vcard; name="$vcard_filename"},
    "encoding" => 'Base64',
    "disposition" => qq{attachment; filename="$vcard_filename"},
    "msg" => $vcard_content,
  });
  $mail_sender->Close();
}

1;

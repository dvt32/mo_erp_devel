use strict;
local($^W) = 1;

use Mail::Sender;
use XML::LibXML;
use HTML::Entities ();
use Encode ();
use decrypt;
use init_database;
use timestamp_to_components;
use utc_datetime_to_local_datetime;
use components_to_timestamp;
use get_local_time_zone;

sub process_virtual_office_sale {
  my $message = shift;
  my $encrypted_body = $message->body()->string();
  my $decrypted_body = decrypt($encrypted_body);
  my $xml_parser = XML::LibXML->new();
  $xml_parser->no_network(1);
  my $dtd_dir = "{{ msgaction_home_dir }}/dtd";
  $xml_parser->load_catalog("file://$dtd_dir/catalog.xml");
  my $xml_document = $xml_parser->parse_string($decrypted_body);
  my $root_node = $xml_document->documentElement();
  my $order_node = $root_node->find("/order")->[0];
  my @param_name_list = (qw(request_datetime initial_page_url referrer_url remote_host remote_addr x_forwarded_for user_agent salutation first_name middle_name last_name suffix title company business_description mailing_address_1 mailing_address_2 mailing_city mailing_state mailing_province mailing_zip_code mailing_country work_phone_number home_phone_number cell_phone_number other_phone_number fax_number email_address web_site name_on_card company_on_card credit_card_type credit_card_number expiration_month expiration_year card_security_code billing_address_1 billing_address_2 billing_city billing_state billing_province billing_zip_code billing_country service_plan desired_address agree_with_tos marketing_source notes domain_name));
  my %param_map = ();
  for my $param_key (@param_name_list) {
    my $param_value = HTML::Entities::decode($order_node->find($param_key));
    $param_map{$param_key} = qq{$param_value};
  }
  my $request_datetime = $param_map{"request_datetime"};
  $request_datetime =~ s/ \w+$//;
  my ($year, $month, $day, $hour, $minute, $second) = timestamp_to_components($request_datetime);
  ($year, $month, $day, $hour, $minute, $second) = utc_datetime_to_local_datetime($year, $month, $day, $hour, $minute, $second);
  $request_datetime = components_to_timestamp($year, $month, $day, $hour, $minute, $second);
  $request_datetime .= " " . get_local_time_zone($year, $month, $day, $hour, $minute, $second);
  if (!$param_map{"mailing_state"}) {
    $param_map{"mailing_state"} = $param_map{"mailing_province"};
  }
  if (!$param_map{"billing_state"}) {
    $param_map{"billing_state"} = $param_map{"billing_province"};
  }
  my $db_charset = "iso-8859-15"; # ISO-8859-15 AKA Latin-9
  my @field_list = ();
  push(@field_list, ["request_datetime", Encode::encode($db_charset, $request_datetime)]);
  push(@field_list, ["initial_page_url", Encode::encode($db_charset, $param_map{"initial_page_url"})]);
  push(@field_list, ["referrer_url", Encode::encode($db_charset, $param_map{"referrer_url"})]);
  push(@field_list, ["remote_host", Encode::encode($db_charset, $param_map{"remote_host"})]);
  push(@field_list, ["remote_addr", Encode::encode($db_charset, $param_map{"remote_addr"})]);
  push(@field_list, ["x_forwarded_for", Encode::encode($db_charset, $param_map{"x_forwarded_for"} || "")]);
  push(@field_list, ["user_agent", Encode::encode($db_charset, $param_map{"user_agent"})]);
  push(@field_list, ["salutation", Encode::encode($db_charset, $param_map{"salutation"})]);
  push(@field_list, ["first_name", Encode::encode($db_charset, $param_map{"first_name"})]);
  push(@field_list, ["middle_name", Encode::encode($db_charset, $param_map{"middle_name"})]);
  push(@field_list, ["last_name", Encode::encode($db_charset, $param_map{"last_name"})]);
  push(@field_list, ["suffix", Encode::encode($db_charset, $param_map{"suffix"})]);
  push(@field_list, ["title", Encode::encode($db_charset, $param_map{"title"})]);
  push(@field_list, ["company", Encode::encode($db_charset, $param_map{"company"})]);
  push(@field_list, ["business_description", Encode::encode($db_charset, $param_map{"business_description"})]);
  push(@field_list, ["mailing_address_1", Encode::encode($db_charset, $param_map{"mailing_address_1"})]);
  push(@field_list, ["mailing_address_2", Encode::encode($db_charset, $param_map{"mailing_address_2"})]);
  push(@field_list, ["mailing_city", Encode::encode($db_charset, $param_map{"mailing_city"})]);
  push(@field_list, ["mailing_state", Encode::encode($db_charset, $param_map{"mailing_state"})]);
  push(@field_list, ["mailing_zip_code", Encode::encode($db_charset, $param_map{"mailing_zip_code"})]);
  push(@field_list, ["mailing_country", Encode::encode($db_charset, $param_map{"mailing_country"})]);
  push(@field_list, ["work_phone_number", Encode::encode($db_charset, $param_map{"work_phone_number"})]);
  push(@field_list, ["home_phone_number", Encode::encode($db_charset, $param_map{"home_phone_number"})]);
  push(@field_list, ["cell_phone_number", Encode::encode($db_charset, $param_map{"cell_phone_number"})]);
  push(@field_list, ["other_phone_number", Encode::encode($db_charset, $param_map{"other_phone_number"})]);
  push(@field_list, ["fax_number", Encode::encode($db_charset, $param_map{"fax_number"})]);
  push(@field_list, ["email_address", Encode::encode($db_charset, $param_map{"email_address"})]);
  push(@field_list, ["web_site", Encode::encode($db_charset, $param_map{"web_site"})]);
  push(@field_list, ["name_on_card", Encode::encode($db_charset, $param_map{"name_on_card"})]);
  push(@field_list, ["company_on_card", Encode::encode($db_charset, $param_map{"company_on_card"})]);
  push(@field_list, ["credit_card_type", Encode::encode($db_charset, $param_map{"credit_card_type"})]);
  push(@field_list, ["credit_card_number", Encode::encode($db_charset, $param_map{"credit_card_number"})]);
  push(@field_list, ["expiration_month", Encode::encode($db_charset, $param_map{"expiration_month"})]);
  push(@field_list, ["expiration_year", Encode::encode($db_charset, $param_map{"expiration_year"})]);
  push(@field_list, ["card_security_code", Encode::encode($db_charset, $param_map{"card_security_code"})]);
  push(@field_list, ["billing_address_1", Encode::encode($db_charset, $param_map{"billing_address_1"})]);
  push(@field_list, ["billing_address_2", Encode::encode($db_charset, $param_map{"billing_address_2"})]);
  push(@field_list, ["billing_city", Encode::encode($db_charset, $param_map{"billing_city"})]);
  push(@field_list, ["billing_state", Encode::encode($db_charset, $param_map{"billing_state"})]);
  push(@field_list, ["billing_zip_code", Encode::encode($db_charset, $param_map{"billing_zip_code"})]);
  push(@field_list, ["billing_country", Encode::encode($db_charset, $param_map{"billing_country"})]);
  push(@field_list, ["service_plan", Encode::encode($db_charset, $param_map{"service_plan"})]);
  push(@field_list, ["desired_address", Encode::encode($db_charset, $param_map{"desired_address"})]);
  push(@field_list, ["marketing_source", Encode::encode($db_charset, $param_map{"marketing_source"})]);
  push(@field_list, ["notes", Encode::encode($db_charset, $param_map{"notes"})]);
  push(@field_list, ["domain_name", Encode::encode($db_charset, $param_map{"domain_name"})]);
  my $dbh = init_database();
  my $field_names = join(", ", map {$_->[0]} @field_list);
  my $field_placeholders = join(", ", map {"?"} @field_list);
  my $sth = $dbh->prepare("insert into virtual_office_application_tbl ($field_names) values ($field_placeholders)");
  for(my $i = 0; $i < scalar(@field_list); $i++) {
    my $col_num = $i + 1;
    $sth->bind_param($col_num, $field_list[$i]->[1]);
  }
  $sth->execute();
  $sth->finish();
  $dbh->disconnect();
  my $x_forwarded_for = $param_map{"x_forwarded_for"} || "";
  if ($x_forwarded_for =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) {
    my $iaddr = Socket::inet_aton($x_forwarded_for);
    my $dns_name = gethostbyaddr($iaddr, Socket::AF_INET());
    if ($dns_name) {
      $x_forwarded_for .= " ($dns_name)";
    }
  }
  my $domain_name = $param_map{"domain_name"};
  my $mail_sender = Mail::Sender->new({
    "smtp" => "{{ outbound_smtp_server }}",
    "from" => qq{"Virtual Office Signup at www.$domain_name" <webmaster\@microoffice.com>},
  });
  my $first_name = $param_map{"first_name"};
  my $last_name = $param_map{"last_name"};
  my $company = $param_map{"company"};
  my $subject = "";
  $subject .= "[Virtual Office] ";
  $subject .= "(";
  $subject .= "$first_name $last_name";
  if ($company) {
    $subject .= ", $company";
  }
  $subject .= ")";
  # concatenate name components.
  my @name_component_list;
  if ($param_map{"salutation"}) {
    push(@name_component_list, $param_map{"salutation"});
  }
  push(@name_component_list, $param_map{"first_name"});
  if ($param_map{"middle_name"}) {
    push(@name_component_list, $param_map{"middle_name"});
  }
  push(@name_component_list, $param_map{"last_name"});
  if ($param_map{"suffix"}) {
    push(@name_component_list, $param_map{"suffix"});
  }
  my $name = join(" ", @name_component_list);
  # concatenate title and company.
  my $title_and_company = "";
  if ($param_map{"title"}) {
    $title_and_company = $param_map{"title"} . ", " . $param_map{"company"};
  } else {
    $title_and_company = $param_map{"company"};
  }
  # concatenate mailing address.
  my $mailing_address = $param_map{"mailing_address_1"};
  if ($param_map{"mailing_address_2"}) {
    $mailing_address .= ", " . $param_map{"mailing_address_2"};
  }
  my $mailing_city_state_zip = "";
  if ($param_map{"mailing_city"} && $param_map{"mailing_state"}) {
    $mailing_city_state_zip .= $param_map{"mailing_city"} . ", " . $param_map{"mailing_state"};
  } elsif ($param_map{"mailing_city"}) {
    $mailing_city_state_zip .= $param_map{"mailing_city"};
  } elsif ($param_map{"mailing_state"}) {
    $mailing_city_state_zip .= $param_map{"mailing_state"};
  }
  if ($param_map{"mailing_zip_code"}) {
    $mailing_city_state_zip .= " " . $param_map{"mailing_zip_code"};
  }
  if (($param_map{"mailing_country"}) && ($param_map{"mailing_country"} ne "United States")) {
    $mailing_city_state_zip .= " " . $param_map{"mailing_country"};
  }
  # concatenate billing address.
  my $billing_address = $param_map{"billing_address_1"};
  if ($param_map{"billing_address_2"}) {
    $billing_address .= ", " . $param_map{"billing_address_2"};
  }
  my $billing_city_state_zip = "";
  if ($param_map{"billing_city"} && $param_map{"billing_state"}) {
    $billing_city_state_zip .= $param_map{"billing_city"} . ", " . $param_map{"billing_state"};
  } elsif ($param_map{"billing_city"}) {
    $billing_city_state_zip .= $param_map{"billing_city"};
  } elsif ($param_map{"billing_state"}) {
    $billing_city_state_zip .= $param_map{"billing_state"};
  }
  if ($param_map{"billing_zip_code"}) {
    $billing_city_state_zip .= " " . $param_map{"billing_zip_code"};
  }
  if (($param_map{"billing_country"}) && ($param_map{"billing_country"} ne "United States")) {
    $billing_city_state_zip .= " " . $param_map{"billing_country"};
  }
  my $msg_content = <<"---END_BLOCK---;";
An order has been placed for the virtual office service. The payment
details have been stored in the database. Make sure to charge the
customer and send a confirmation.

Here are further details on the order:

        Name: $name
Title/Compan: $title_and_company
 Description: $param_map{"business_description"}
Mail Address: $mailing_address
            : $mailing_city_state_zip
Bill Address: $billing_address
            : $billing_city_state_zip
  Work Phone: $param_map{"work_phone_number"}
  Home Phone: $param_map{"home_phone_number"}
  Cell Phone: $param_map{"cell_phone_number"}
 Other Phone: $param_map{"other_phone_number"}
  Fax Number: $param_map{"fax_number"}
  Email Addr: $param_map{"email_address"}
    Web Site: $param_map{"web_site"}
Service Plan: $param_map{"service_plan"}
Desired Addr: $param_map{"desired_address"}
 Mktg Source: $param_map{"marketing_source"}
Request Time: $request_datetime
Initial Page: $param_map{"initial_page_url"}
Referrer URL: $param_map{"referrer_url"}
 Remote Host: $param_map{"remote_host"}
 Remote Addr: $param_map{"remote_addr"}
Forwarded IP: $x_forwarded_for
  User Agent: $param_map{"user_agent"}
 Domain Name: $param_map{"domain_name"}

Notes
=====
$param_map{"notes"}
---END_BLOCK---;
  $mail_sender->MailMsg({
    "to" => "sales\@microoffice.com",
    "subject" => $subject,
    "msg" => $msg_content,
  });
}

1;

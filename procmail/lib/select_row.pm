use strict;
local($^W) = 1;

use DBI qw(:sql_types);

# select a single row from a table given it's ID value.
# pass in the database handle, table name, and row ID.
sub select_row($$$) {
  my ($dbh, $table_name, $row_id) = @_;
  my $sth = $dbh->prepare("select * from $table_name where id = ?");
  $sth->bind_param(1, $row_id, SQL_INTEGER);
  $sth->execute();
  my $row = $sth->fetchrow_hashref();
  $sth->finish();
  return $row;
}

1;

use strict;
local($^W) = 1;

use Mail::Sendmail;

sub send_email(@) {
  my %param_hash = @_;
  my %mail_arg_hash =
    ("From" => $param_hash{"from"},
     "To" => $param_hash{"to"},
     "Subject" => $param_hash{"subject"},
     "Message" => $param_hash{"message"},
     "SMTP" => "{{ outbound_smtp_server }}",
     );
  sendmail(%mail_arg_hash);
}

1;

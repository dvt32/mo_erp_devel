use strict;
local($^W) = 1;

use Storable qw(nfreeze);

sub serialize($) {
  my $object_ref = shift;
  return nfreeze($object_ref);
}

1;

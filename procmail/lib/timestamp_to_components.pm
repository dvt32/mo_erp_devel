use strict;
local($^W) = 1;

# timestamp has format "yyyy-mm-dd hh:mm:ss"
sub timestamp_to_components($) {
  my $timestamp = shift;
  $timestamp =~ /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)$/;
  my ($year, $month, $day, $hour, $minute, $second) = ($1, $2, $3, $4, $5, $6);
  return ($year, $month, $day, $hour, $minute, $second);
}

1;

use strict;
local($^W) = 1;

sub url_encode($) {
  my $value = shift;
  $value =~ s/([^ a-zA-Z0-9_.-])/sprintf("%%%02x", ord($1))/egs;
  $value =~ tr/ /+/;
  return $value;
}

1;

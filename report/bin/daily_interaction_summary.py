#!{{ report_basedir }}/venv/bin/python3
import sys
sys.path.append('{{ report_basedir }}/lib')
import interactions_summary

''' Sends interactions in the past 0 days (today only) '''
if __name__ == '__main__':
  days = 0
  subject = 'Daily Interactions Summary'
  send_if_empty = True
  interactions_summary.send_interactions(days, subject, send_if_empty)

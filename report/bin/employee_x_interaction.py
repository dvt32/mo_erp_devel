#!{{ report_basedir }}/venv/bin/python3

"""Sends out an email of each employee's interactions
We group employees by building
"""
import sys
sys.path.append('{{ report_basedir }}/lib')
from email.mime.text import MIMEText
import psycopg2
import psycopg2.extras
from database import get_connection
from util import get_recipients, get_mailer
import util

interactions_sender = '{{ report_interactions_sender }}'

def main():
  conn = get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.execute('SELECT id, short_global_label FROM building_tbl WHERE is_active')
  buildings = curdict.fetchall()
  # Increase in rent
  for building in buildings:
    recipients = get_recipients(curdict, building['id'], 'Weekly Employee Interactions')

    if not recipients:
      # No recipient, don't send an aggregate email
      # This is potentially incorrect (perhaps we want individual emails going out)
      # but it saves a lot of querying
      continue

    # Get list of employee for this building
    curdict.execute('''
    SELECT DISTINCT
      first_name,
      last_name,
      employee_id,
      email
    FROM desk_sale_interaction_tbl
    LEFT JOIN employee_tbl ON
      employee_tbl.id = desk_sale_interaction_tbl.employee_id
    WHERE
      desk_id IN (SELECT id FROM desk_vw WHERE floor_id IN (SELECT id FROM floor_tbl WHERE building_id = %s)) AND
      date BETWEEN current_date - interval '1 week' AND current_date;
    ''', (building['id'],))

    employees = {row['employee_id'] : {
      'first_name': row['first_name'],
      'last_name': row['last_name'],
      'email': row['email']
    } for row in curdict}

    if not employees:
      # No employee interactions, skip this building
      continue

    for employee_id in employees:
      curdict.execute('''
      SELECT
        desk_id,
        label,
        assigned_number,
        date,
        desk_sale_interaction_tbl.note
      FROM
        desk_sale_interaction_tbl
      LEFT JOIN desk_tbl ON
        desk_tbl.id = desk_sale_interaction_tbl.desk_id
      LEFT JOIN floor_tbl ON
        desk_tbl.floor_id = floor_tbl.id
      WHERE
        employee_id = %s AND
        desk_sale_interaction_tbl.desk_id IN
          (SELECT id FROM desk_tbl WHERE floor_id IN
            (SELECT id FROM floor_tbl WHERE building_id = %s)) AND
        date BETWEEN current_date - interval '8 days' AND current_date
      ''', (employee_id,building['id'],))
      employees[employee_id]['notes'] = curdict.fetchall()

      # Send individual emails
      employee_email = employees[employee_id]['email']
      if not util.validate_mo_email(employee_email):
        continue

      # set up property_dict
      property_dict = {}
      property_dict['employees'] = { employee_id : employees[employee_id] }
      property_dict['building_id'] = building['id']

      # Generate body
      html_body = util.render_body('employee_x_interaction.html', property_dict)
      msg = MIMEText(html_body, 'html')
      msg['Subject'] = "Your Interactions - {}".format(building['short_global_label'])
      msg['From'] = interactions_sender
      msg['To'] = employee_email
      util.send_mail(msg)

    # Set up property_dict
    property_dict = {}
    property_dict['employees'] = employees
    property_dict['building_id'] = building['id']

    # Generate body
    html_body = util.render_body('employee_x_interaction.html', property_dict)
    msg = MIMEText(html_body, 'html')
    msg['Subject'] = "Employee Interactions - {}".format(building['short_global_label'])
    msg['From'] = interactions_sender
    msg['To'] = recipients
    util.send_mail(msg)

if __name__ == '__main__':
  main()

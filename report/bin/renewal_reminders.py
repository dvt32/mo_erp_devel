#!{{ report_basedir }}/venv/bin/python3
import sys
sys.path.append('{{ report_basedir }}/lib')
import smtplib
from email.mime.text import MIMEText
import psycopg2
import psycopg2.extras
from datetime import date, timedelta
from database import get_connection
import util

renewal_sender = '{{ report_interactions_sender }}'
renewals_range_1 = ({{ report_renewal_range_1 }})
renewals_range_2 = ({{ report_renewal_range_2 }})

def main():
  conn = get_connection()
  send_renewals(conn, renewals_range_1)
  send_renewals(conn, renewals_range_2)
  send_rent_increase(conn, renewals_range_1)

def send_rent_increase(conn, date_range):
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.execute('SELECT id, short_global_label FROM building_tbl WHERE is_active')
  buildings = curdict.fetchall()

  # Increase in rent
  for building in buildings:
    recipients = util.get_recipients(curdict, building['id'], 'Desk Renewals')
    if not recipients:
      # No recipients, skip this building
      continue

    curdict.execute('''
    SELECT
      account_vw.name,
      account_vw.id,
      desk_vw.increase_date,
      desk_vw.increase_date - CURRENT_DATE as days,
      desk_vw.current_price,
      cust_pers_tbl.email
    FROM
      account_vw,
      desk_vw,
      cust_pers_tbl
    WHERE
      (desk_vw.increase_date - CURRENT_DATE) BETWEEN %s AND %s AND
      desk_vw.account_id = account_vw.id AND
      account_vw.primary_contact_person_id=cust_pers_tbl.id AND
      desk_vw.id IN ( SELECT id FROM desk_vw WHERE floor_id IN (SELECT id FROM floor_tbl WHERE building_id = %s))
    ''',(date_range[0], date_range[1], building['id'],))

    # No messages for this building
    if curdict.rowcount == 0:
      body = 'There are no contracts with an increase date in the upcoming {} to {} days.'.format(date_range[0], date_range[1])

    else:
      # Generate body
      body = ''
      for row in curdict:
        body+= '''{} is up for increase in {} days on {}. They currently pay {}. Their email address is {}. Please review the license agreement details.\n'''.format(row['name'], row['days'], row['increase_date'], row['current_price'], row['email'],)

    msg = MIMEText(body)
    msg['Subject'] = "Increase Reminder Summary - {}".format(building['short_global_label'])
    msg['From'] = renewal_sender
    msg['To'] = recipients
    util.send_mail(msg)

def send_renewals(conn, date_range):
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT id, short_global_label FROM building_tbl WHERE is_active')
  buildings = curdict.fetchall()

  for building in buildings:
    recipients = util.get_recipients(curdict, building['id'], 'Desk Renewals')

    if not recipients:
      # No recipients, skip this building
      continue

    # This could be improved by checking if there exists a follow-up contract with the same account
    curdict.execute('''
    SELECT
      account_vw.name,
      account_vw.id,
      desk_vw.end_date,
      desk_vw.current_price,
      cust_pers_tbl.email,
      desk_vw.assigned_number,
      floor_vw.short_global_label,
      desk_vw.end_date - current_date as days
    FROM
      account_vw,
      desk_vw,
      cust_pers_tbl,
      floor_vw
    WHERE
      (desk_vw.end_date - current_date) BETWEEN %s AND %s AND
      desk_vw.account_id = account_vw.id AND
      account_vw.primary_contact_person_id = cust_pers_tbl.id AND
      floor_vw.id=desk_vw.floor_id AND
      floor_vw.building_id = %s
    ''', (date_range[0], date_range[1], building['id'],))

    # No messages for this building
    if curdict.rowcount == 0:
      body = 'There are no contracts with an end date in the upcoming {} to {} days.'.format(date_range[0], date_range[1])
    else:
      # Generate body
      body = ''
      for row in curdict:
        body += '''{} (Desk: {}, Floor: {}) is up for renewal in {} days on {} and they may also be due for the standard rent increase of 5%. They currently pay {}. Please review the license agreement details and execute a renewal agreement right away. Their email address is {}.\n\n'''.format(row['name'], row['assigned_number'], row['short_global_label'], row['days'], row['end_date'], row['current_price'], row['email'],)

    msg = MIMEText(body)
    msg['Subject'] = "Renewal Reminder Summary - {}".format(building['short_global_label'])
    msg['From'] = renewal_sender
    msg['To'] = recipients

    util.send_mail(msg)

if __name__ == '__main__':
  main()

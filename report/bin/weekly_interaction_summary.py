#!{{ report_basedir }}/venv/bin/python3
import sys
sys.path.append('{{ report_basedir }}/lib')
import interactions_summary

''' Sends interactions within the past 7 days'''
if __name__ == '__main__':
  # Note that if any interactions are added after this email is sent on the same day
  # They are not included in the weekly email
  since_x_days_ago = 6
  subject = 'Weekly Interactions Summary'
  send_if_empty=True
  interactions_summary.send_interactions(since_x_days_ago, subject, send_if_empty)

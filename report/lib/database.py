import psycopg2

def get_connection():
  return psycopg2.connect(database='{{ report_db_name }}', user='{{ report_db_username }}', password='{{ report_db_password }}', host='{{ report_db_host }}', sslmode='{{ report_db_sslmode }}')

import smtplib
from email.mime.text import MIMEText
import psycopg2
import psycopg2.extras
from datetime import date, timedelta
from database import get_connection
import util

interactions_sender = '{{ report_interactions_sender }}'

def send_interactions(since_x_days_ago, subject, send_if_empty):
  """ Sends interactions grouped by location

  Args
    since_x_days_ago (int): time range, expects nonnegative integer.
                            0 - Only today's interactions
                            6 - Interactions for the past week
  """
  conn = get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

  curdict.execute('SELECT id, short_global_label FROM building_tbl where is_active')
  buildings = curdict.fetchall()

  for building in buildings:
    recipients = util.get_recipients(curdict, building['id'], 'Desk Sale Interactions')

    if not recipients:
      # No recipients; Skip this building
      continue

    # Set up property_dict
    # property_dict = util.get_base_report_prop()
    property_dict = {}
    property_dict['building_id'] = building['id']
    property_dict['interaction_desks'] = interaction_desks = get_desks_with_interactions(building['id'], since_x_days_ago, curdict)
    property_dict['modified_desks'] = modified_desks = get_desks_with_modified_contracts(building['id'], since_x_days_ago, curdict)

    interactions = {}
    modified_contracts = {}

    if interaction_desks:
      for desk in interaction_desks:
        interactions[desk['id']] = get_interactions(desk['id'], since_x_days_ago, curdict)

      property_dict['interactions'] = interactions

    if modified_desks:
      for desk in modified_desks:
        modified_contracts[desk['id']] = get_modified_contracts(desk['id'], since_x_days_ago, curdict)

      property_dict['contracts'] = modified_contracts

    # Skip if empty and not send_if_empty
    if 'interactions' not in property_dict and 'contracts' not in property_dict and not send_if_empty:
      continue

    # Setup template
    html_body = util.render_body('interactions_summary.html', property_dict)
    msg = MIMEText(html_body, 'html')

    # Setup email
    if since_x_days_ago == 0:
      msg['Subject'] = '{} - {} for {}'.format(subject, building['short_global_label'], date.today())
    else:
      msg['Subject'] = '{} - {} for {} to {}'.format(subject, building['short_global_label'], date.today() - timedelta(days=since_x_days_ago), date.today())

    msg['From'] = interactions_sender
    msg['To'] = recipients

    # Send email
    util.send_mail(msg)

def get_desks_with_interactions(building_id, since_x_days_ago, curdict):
  # Returns an immutable dict of desks containing their id, floor label, assigned number
  # account, and end date
  # only returns desks with an associated desk_sale_interaction_tbl row that has been submitted
  # in the last since_x_days_ago period
  curdict.execute('''
    SELECT
      desk_vw.id,
      floor_tbl.label,
      desk_vw.assigned_number,
      cust_acct_tbl.name as acct_name,
      desk_vw.end_date as end
    FROM
      desk_vw
    LEFT JOIN floor_tbl
      ON floor_tbl.id = desk_vw.floor_id
    LEFT JOIN cust_acct_tbl
      ON cust_acct_tbl.id = desk_vw.account_id
    WHERE
      desk_vw.floor_id IN (SELECT id FROM floor_tbl WHERE building_id = %s) AND
      desk_vw.id IN (SELECT desk_id FROM desk_sale_interaction_tbl WHERE date BETWEEN current_date - %s AND current_date)
    ORDER BY floor_tbl.label, desk_vw.assigned_number''', (building_id, since_x_days_ago,))
  return curdict.fetchall()

def get_interactions(desk_id, since_x_days_ago, curdict):
  # Gets interactions associated with a specified desk_id
  # Returns note, employee.first_name + employee.last_name, date of interaction
  curdict.execute('''
    SELECT
      interactions.note,
      first_name || ' ' || last_name as employee,
      interactions.date
    FROM desk_sale_interaction_tbl interactions
    LEFT JOIN employee_tbl ON interactions.employee_id = employee_tbl.id
    WHERE
      interactions.date BETWEEN current_date - %s AND current_date AND
      desk_id = %s''', (since_x_days_ago, desk_id,))
  return curdict.fetchall()

def get_desks_with_modified_contracts(building_id, since_x_days_ago, curdict):
  # gets desks that have an associated row in desk_price_tbl that has been
  # created or modified in the past since_x_days_ago period
  curdict.execute('''
  SELECT
    desk_tbl.id,
    floor_tbl.label,
    desk_tbl.assigned_number
  FROM
    desk_tbl
  LEFT JOIN floor_tbl
    ON floor_tbl.id = desk_tbl.floor_id
  WHERE
    desk_tbl.id IN (
      SELECT
        desk_id
      FROM
        desk_price_tbl
      WHERE
        floor_tbl.id IN (SELECT id FROM floor_tbl WHERE building_id = %s)
        AND
        (
          (CAST(created_ts AS DATE) BETWEEN current_date - %s AND current_date)
          OR
          (CAST(modified_ts AS DATE) BETWEEN current_date - %s AND current_date)
        )
    )
  ORDER BY
    floor_tbl.label, desk_tbl.assigned_number''', (building_id, since_x_days_ago, since_x_days_ago,))
  return curdict.fetchall()

def get_modified_contracts(desk_id, since_x_days_ago, curdict):
  # gets rows from desk_price_tbl that have been modified in the past since_x_days_ago period
  # and are associated with the given desk_id
  curdict.execute('''
      SELECT
        name,
        desk_price_tbl.start_date as start,
        desk_price_tbl.end_date as end,
        price,
        internet_revenue as internet,
        internet_lines,
        phone_revenue as phone,
        phone_lines,
        portal_lines AS num_portal_fees,
        desk_price_tbl.note,
        desk_price_tbl.created_ts
      FROM desk_price_tbl
      LEFT JOIN desk_tbl
        ON desk_price_tbl.desk_id = desk_tbl.id
      LEFT JOIN cust_acct_tbl
        ON desk_price_tbl.account_id = cust_acct_tbl.id
      LEFT JOIN floor_tbl
        ON floor_tbl.id = desk_tbl.floor_id
      WHERE (CAST(desk_price_tbl.created_ts AS DATE) BETWEEN current_date - %s AND current_date
        OR CAST(desk_price_tbl.modified_ts AS DATE) between current_date - %s AND current_date)
        AND desk_id = %s
      ORDER BY desk_price_tbl.created_ts''', (since_x_days_ago, since_x_days_ago, desk_id,))
  return curdict.fetchall()

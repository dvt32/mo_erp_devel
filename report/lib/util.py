import re
import smtplib
import jinja2

EMAIL_REGEX = re.compile('^.*@(microoffice|coalitionspace)\.com')

def get_mailer():
  return smtplib.SMTP('{{ outbound_mail_server }}')

def send_mail(msg):
  s = get_mailer()
  s.send_message(msg)
  s.quit()

def get_recipients(curdict, building_id, report_name):
  curdict.execute('''
  SELECT
    email
  FROM report_email_tbl
  LEFT JOIN employee_tbl
    ON report_email_tbl.employee_id = employee_tbl.id
  WHERE report_email_tbl.building_id = %s
  AND report_type_id IN (SELECT
    id
  FROM report_type_tbl
  WHERE name = %s)''', (building_id, report_name,))

  # Get all emails
  recipients = [ row['email'] for row in curdict ]

  # Filter invalid emails
  recipients = [ x for x in filter(EMAIL_REGEX.search, recipients) ]

  # Format into a single string and return (SMTP compliant)
  return ', '.join(recipients)

def validate_mo_email(email):
  return EMAIL_REGEX.match(email)

def get_base_report_prop():
  # Returns a dictionary containing some useful system properties
  return { 'operations_flask': '{{ operations_flask }}', 'operations_fqdn': '{{ operations_fqdn }}', 'static_base_url': '{{ static_base_url }}' }

def render_body(template_name, property_dict):
  ''' Returns a template for an email

  Args:
    template_name (string): Name of the html template in report_basedir/share
    property_dict (dict): any additional keyword values required to render (e.g. building_id)
  '''

  # setup jinja2 environment
  loader = jinja2.FileSystemLoader('{{ report_basedir }}/share')
  environment = jinja2.Environment(loader=loader)

  # Add some common keys to the properties dictionary
  property_dict.update(get_base_report_prop())

  return environment.get_template(template_name).render(property_dict)

from flask import request, abort, redirect, url_for, flash, render_template, g
import json
import sys
import re


def arping(ip):
  with open('/proc/net/arp') as f:
    for line in f:
      if ip in line:
        match = re.search(r'([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})', line)
        try:
          mac = match.group(0)
          return mac
        except: 
          mac = "not found"
  return mac

def get_mac():
  mac = arping(request.remote_addr)
  return mac

def show_mac():
  mac = arping(request.remote_addr)
  if (request.args):
    if request.args['user'] == 'member':
      url = '{{ netauth_base_url }}/new_device'
    elif request.args['user'] == 'guest':
      url = '{{ netauth_base_url }}/register_guest'
    elif request.args['user'] == 'temporary':
      url = '{{ netauth_base_url }}/temporary'
  else:
    url = None
  return render_template("show_mac.html", mac=mac, url=url)
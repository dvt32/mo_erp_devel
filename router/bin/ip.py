from flask import request, abort, redirect, url_for, flash, render_template, g
import subprocess
from itsdangerous import URLSafeTimedSerializer, TimedSerializer, SignatureExpired, BadSignature
import json
from operator import itemgetter
import database
import psycopg2.extras
import datetime

def get_members():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT netauth_device_cust_tbl.* FROM netauth_device_cust_tbl, cust_acct_tbl WHERE netauth_device_cust_tbl.cust_acct_id = cust_acct_tbl.id AND cust_acct_tbl.is_network_enabled AND mac_addr IS NOT NULL')
  members = [dict(row) for row in curdict]
  return members

def get_guests():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT netauth_device_guest_tbl.* FROM netauth_device_guest_tbl, cust_acct_tbl WHERE expire_ts > now() AND cust_activated_ts IS NOT NULL AND netauth_device_guest_tbl.cust_acct_id = cust_acct_tbl.id AND cust_acct_tbl.is_network_enabled AND mac_addr IS NOT NULL')
  guests = [dict(row) for row in curdict]
  return guests

def get_transients():
  conn = database.get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM netauth_device_transient_tbl WHERE expire_ts > now() AND activated_ts < now() AND mac_addr IS NOT NULL')
  transients = [dict(row) for row in curdict]
  return transients

def get_pinned():
  pass

def update_ip():
  members = get_members()
  guests = get_guests()
  transients = get_transients()
  rules = dict(guests=guests, members=members, transients=transients)
  date_handler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, datetime.datetime)
    else None)
  json_rules = json.dumps(rules, default=date_handler)
  proc = subprocess.Popen('{{ router_basedir }}/bin/ipset_setuid_wrapper', stdin=subprocess.PIPE)
  proc.communicate(bytes(json_rules, 'utf8'))
  return json.dumps("OK")



#!/usr/bin/python3
import sys

def iprange_to_name(subnet, to_print=0):
  lasti = subnet.rfind('.')
  ipset_name = 'netauth-' + subnet[0:lasti].replace('.', '-')
  if to_print:
    print(ipset_name)
  return ipset_name

def main():
  subnet = sys.argv[1]
  toprint = sys.argv[2]
  iprange_to_name(subnet, toprint)

if __name__ == "__main__":
  main()
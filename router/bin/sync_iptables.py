#!{{ router_basedir }}/venv/bin/python3
import sys
import ipsetpy
import subprocess
import os
import json
import copy
import re
import ipaddress
import datetime
import dateutil.parser
IPSET_COMMAND = 'ipset'
IPSET_TIMEOUT = 2

def create_ipmac_set(set_name, type_name, range, exist=False, command_timeout=None):
  arguments = ["create", set_name, type_name, 'range', range]
  if exist:
      arguments.append('-exist')
  return ipset_send_command(*arguments, command_timeout=command_timeout)

def ipset_send_command(*arguments, **kv_arguments):
  try:
    command_timeout = kv_arguments.get('command_timeout', IPSET_TIMEOUT)
    if command_timeout is None:
        command_timeout = IPSET_TIMEOUT
    command_list = kv_arguments.get('command_list', None)
    process = subprocess.Popen((IPSET_COMMAND, ) + arguments, universal_newlines=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    if command_list:
      for command in command_list:
        process.stdin.write(command)
    result, error = process.communicate(timeout=command_timeout)
  except subprocess.TimeoutExpired:
    process.kill()
    raise IpsetCommandHangs()
  except FileNotFoundError:
    raise IpsetNotFound()
  if process.returncode != 0:
    print(error)
  return result

def parse_mac(mac):
    return mac[0:2] + ":" + mac[2:4] + ":" + mac[4:6] + ":" + mac[6:8] + ":" + mac[8:10] + ":" + mac[10:12]

def main():
  data = ""
  for line in sys.stdin.readlines():
    data += line
  ipsets_rules = json.loads(data)
  members = ipsets_rules['members']
  guests = ipsets_rules['guests'] + ipsets_rules['transients']
  # get the newest database info from the request (passed to stdin)
  guest_expire_dict = {}
  database_mems = set([])
  database_guests = set([])
  for m in members:
    database_mems.add(parse_mac(m['mac_addr']))
  for g in guests:
    mac = parse_mac(g['mac_addr'])
    database_guests.add(mac)
    guest_expire_dict[mac] = g['expire_ts']
  # get current mac addresses in ipset ipsets_rules
  rules = ipsetpy.ipset_list()
  matches = re.findall(r'(([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2}))', rules)
  ipsets_rules = set([])
  for mac in matches:
    ipsets_rules.add(mac[0].lower())
  # calculate the members we need to add, the guests we need to add, and all we need to delete
  add_mems = (database_mems - ipsets_rules)
  add_guests = (database_guests - ipsets_rules)
  delete_macs = (ipsets_rules - (database_mems | database_guests))
  # add the entries to ipset
  for mem in add_mems:
    ipsetpy.ipset_add_entry('{{ ipset_name }}', mem, exist=True)
  # add guest entries to ipset with a timeout
  for guest in add_guests:
      expire = dateutil.parser.parse(guest_expire_dict[guest])
      now = datetime.datetime.now(datetime.timezone.utc)
      timeout = int((expire - now).total_seconds())
      ipsetpy.ipset_add_entry('{{ ipset_name }}', guest, entry_timeout=timeout, exist=True)
  # create dict mapping network to ip,mac for entries we need to delete
  del_dict = {}
  for mac in delete_macs:
    ipsetpy.ipset_del_entry('{{ ipset_name }}', mac, exist=True)
  return json.dumps("OK")

if __name__ == "__main__":
  main()

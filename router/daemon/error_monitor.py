#!/usr/bin/python3
import sys
sys.path.insert(1, '{{ router_daemon_basedir }}/lib')
import smtplib
import os
import ntpath
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import email.utils
import email.header
import datetime
from time import sleep
def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)

errors = ['client denied by server configuration','provided via HTTP are different','AH00126: Invalid URI in request','rejecting client initiated renegotiation']

def main():
  now = datetime.datetime.now()
  filename = ntpath.basename(sys.argv[1])
  if sys.argv[2] == 'IN_MODIFY':
    sleep(5)
    with open('/var/log/apache2/router-error.log') as f:
      last = None
      for last in (line for line in f if line.rstrip('\n')):
        pass
    for error in errors:
      if error in last:
        return
    with open('/tmp/crontestlog.txt', 'a') as log:
      message = '%s %s at %s \n' % (filename, sys.argv[2], now)
      log.write(message)
    sleep(60)
    mail_msg = MIMEMultipart()
    mail_body = 'Error on router site. See attached error and access logs.\n\n'
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = 'billing@microoffice.com'
    mail_msg['To'] = '{{ bugs_email }}'
    mail_msg['Subject'] = email.header.Header('Router Site Error', 'utf-8')
    mail_msg.attach(MIMEText(mail_body))
    with open('/var/log/apache2/router-error.log', 'rb') as f:
      part = MIMEBase('application', "octet-stream")
      part.set_payload( f.read() )
      encoders.encode_base64(part)
      part.add_header('Content-Disposition', 'attachment', filename='router-error.log')
      mail_msg.attach(part)
    with open('/var/log/apache2/router-access.log', 'rb') as f:
      part = MIMEBase('application', "octet-stream")
      part.set_payload( f.read() )
      encoders.encode_base64(part)
      part.add_header('Content-Disposition', 'attachment', filename='router-access.log')
      mail_msg.attach(part)
    mailer = smtplib.SMTP('{{ outbound_mail_server }}')
    mailer.sendmail(mail_msg['From'], mail_msg['To'], mail_msg.as_string())
    mailer.quit()
  elif sys.argv[2] == 'IN_MOVE_SELF':
    with open('/tmp/crontestlog.txt', 'a') as log:
      message = '%s %s at %s \n' % (filename, sys.argv[2], datetime.datetime.now())
      log.write(message)
    sleep(15)
    touch('/etc/incron.d/router_error_monitor')
    
  

if __name__ == "__main__":
  main()

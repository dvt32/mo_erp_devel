import sys
sys.path.append('/var/www/sites/{{ router_fqdn }}/bin')
sys.path.append('/var/www/sites/{{ router_fqdn }}/lib')
from flask import Flask
from flask import request, abort, redirect, url_for, flash, render_template, g
import auth
import ip

app = Flask(__name__)
app.secret_key = '{{ router_flask_secret_key }}'
app.debug = {{ router_flask_app_debug }}

@app.context_processor
def define_urls():
  return dict(static_base_url='{{ static_base_url }}', netauth_base_url='{{ netauth_base_url }}')
  
@app.route('/get_mac', methods=['POST'])
def get_mac():
  return auth.get_mac()
  
@app.route('/show_mac')
def show_mac():
  return auth.show_mac()

@app.route('/update_ip', methods=['POST'])
def update_ip():
  return ip.update_ip()
from flask import g
import psycopg2

def get_connection():
  conn = getattr(g, '_database', None)
  if conn is None:
    conn = g._database = psycopg2.connect(database='{{ router_db_name }}', user='{{ router_db_username }}', password='{{ router_db_password }}', host='{{ router_db_host }}', sslmode='{{ router_db_sslmode }}')
  return conn

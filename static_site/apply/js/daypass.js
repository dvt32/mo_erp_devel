/** Daypass site functions **/

$(function() {
    $( "#date" ).datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
});

document.addEventListener('DOMContentLoaded', function() {
    // hide the billing_province
    document.getElementById("billing_province").parentElement.parentElement.style.display = 'none';
    // hide or show price (depending on form state)
    changePricing();
}, false);

var country = document.getElementById("billing_country");
country.addEventListener('change', function(){changeState('billing')});

var address = document.getElementById("building_floor");
address.addEventListener('change', changePricing);

var num_people_field = document.getElementById('num_people');
num_people_field.addEventListener('change', changePricing);

var link_helper = document.getElementById('link_helper');
var pricing_dict = JSON.parse(link_helper.getAttribute('data-pricing-dict'));
var cc_fee = JSON.parse(link_helper.getAttribute('data-cc-fee'));
link_helper.parentNode.removeChild(link_helper);

function changePricing() {
    // get id
    var desired_addr = document.getElementById('building_floor');
    var id = desired_addr.options[desired_addr.selectedIndex].value;

    if (id=='') {
	// hide if nothing selected
        document.getElementById('cc_fee_row').style.display='none';
	document.getElementById('daypass_price_row').style.display='none';
    } else {
        // Update values
	var num_people = num_people_field.options[num_people_field.selectedIndex].value;
	var daypass_fee = pricing_dict[id] * num_people;
	daypass_fee = addCCFee(daypass_fee, cc_fee);

	// update elements
	document.getElementById('daypass_price_td').innerHTML = toCurrency(daypass_fee);

	// make elements visible
	document.getElementById('cc_fee_row').style.display='table-row';
	document.getElementById('daypass_price_row').style.display='table-row';
    }
}

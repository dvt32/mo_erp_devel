$.validator.addMethod("cc", function(value, element) {
  return this.optional(element) || /^\d+\*+\d+$/.test(value) || (/^\d{15,16}$/.test(value) && luhn(value));
}, "Not a valid credit card number");

$.validator.addMethod("cvv", function(value, element) {
  return this.optional(element) || /^\d{3,4}$/.test(value) || (/^\*{3,4}$/.test(value));
}, "Not a valid cvv");

$.validator.addMethod("zip_format", function(value, element) {
  return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
}, "Not a valid zip code format");

$.validator.addMethod("money", function(value, element) {
  return this.optional(element) || /^\d+(\.\d{2})?$/.test(value);
}, "Not a valid money amount format");

$.validator.addMethod("valid_expire", function(value, element) {
  var d = new Date();
  var year = d.getFullYear() % 1000;
  var month = d.getMonth() + 1;
  value = parseInt(value);
  var form_year = parseInt($(element).closest('form').find("input[name=expire_year], input[name=card_expire_year]").val());
  var form_month = parseInt($(element).closest('form').find("input[name=expire_month], input[name=card_expire_month]").val());
  if ((year * 12 + month) > (form_year * 12 + form_month)) {
    return false;
  }
  else if ($(element).closest('form').find("input[name=expire_year], input[name=card_expire_year]").val().length == 0) {
    return false;
  } else {return this.optional(element) || true}
}, "Please enter a date in the future");

$.validator.addMethod("laxEmail", function(value, element) {
  // allow any non-whitespace characters as the host part
  return this.optional( element ) || /^[a-zA-Z0-9.!#$%&*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test( value );
}, 'Please enter a valid email address.');

// validate date of service to be after today
$.validator.addMethod("future_date", function(value, element) {
  var d = new Date();
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var day = d.getDate();
  var time = value.split("-");
  if (year * 12 + month > parseInt(time[0]) * 12 + parseInt(time[1])) {
    return false;
  }
  else if (year * 12 + month === parseInt(time[0]) * 12 + parseInt(time[1]) && day > parseInt(time[2])) {
    return false;
  }
  else {
    return this.optional( element ) || true;
  }
}, "Please choose today or a future date");


function luhn(cc) {
  var sum = 0;
  cc = cc.split("").reverse().join("");
    for (var i = 0; i < cc.length; i++) {
      var digit = parseInt(cc.charAt(i));
      if (i % 2 == 1) {
        digit *= 2;
      }
    if (digit > 9) {
        digit -= 9;
    }
    sum += digit;
  }
  return ((sum % 10)==0)
}


$(function () {
var cardOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
    error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
    },
    rules: {
      billing_first_name: "required",
      billing_last_name: "required",
      card_number: {
        required: true,
        cc: true,
        maxlength: 16,
        minlength: 15
      },
      expire_year: {
        required: true,
        digits: true,
        valid_expire: true
      },
      expire_month: {
        required: true,
        valid_expire: {
          depends: function (element) {
            return $(element).closest('form').find("#expire_year").val().length != 0;
          }
        }
      },
      cvv: {
        required: true,
        cvv: true,
        minlength: 3
      },
      billing_address_1: "required",
      billing_city: "required",
      billing_state: {
        required: {
          depends: function (element) {
            return $(element).closest('form').find("#billing_country").val() == 'US';
          }
        }
      },
      billing_state_nonus: {
        required: {
          depends: function (element) {
            return $(element).closest('form').find("#billing_country").val() != 'US';
          }
        }
      },
      billing_zip_code: {
        required: true,
        zip_format: {
          depends: function (element) {
            return $(element).closest('form').find("#billing_country").val() == 'US';
          }
        },
        remote:
        {
            url: "/zip_check",
            type: "post",
            data: {state: function () { return $( "#billing_state" ).val();},
                  zip: function () { return $( "#billing_zip_code" ).val();},
                  country: function () { return $("#billing_country").val();}
            }
        }
      },
      email: {
        required: true,
        laxEmail: true
      },
      building_floor: {
        required: true
      },
      amount: {
        required: true,
        money: true,
        min: 0.01
      },
      date: {
        required: true,
        future_date: true
      }
    }
  };
  $("#daypassForm").validate(cardOptions);})

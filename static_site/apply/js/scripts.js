// global functions for apply site

function toCurrency(amount) {
    // Returns a string that converts cents into dollar amount with prepended $
    // Convert to dollars
    var result = eval(amount) / 100;
    // prepend with $ and set fixed number of decimal places
    return '$' + result.toFixed(2);
}

function addTax(amount, tax) {
    // amount is money amount in cents (tax only applied to phone)
    // tax is tax amount * 1000 (percentage as integer, e.g. 8.875% -> 8857
    var result = eval(amount) * (100000 + eval(tax));
    result = Math.round(result / 1000); // rounded to nearest cent
    return result / 100;
}

function addCCFee(amount, cc_fee) {
    // cc_fee is percentage as integer (e.g. 3% -> 3)
    // multiply input by 100% + CCFee, return result rounded to nearest cent
    var result = eval(amount) * (cc_fee + 100);
    result = Math.round(result / 100); // round to nearest cent
    return result;
}

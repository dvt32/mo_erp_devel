$.validator.addMethod("cc", function(value, element) {
  return this.optional(element) || (/^\d{15,16}$/.test(value) && luhn(value));
}, "Not a valid credit card number");

$.validator.addMethod("cvv", function(value, element) {
  return this.optional(element) || /^\d{3,4}$/.test(value);
}, "Not a valid cvv");

$.validator.addMethod("routing_valid", function(value, element) {
  return this.optional(element) || (/^\d{9}$/.test(value));
}, "Not a valid routing number");

$.validator.addMethod("zip_format", function(value, element) {
  return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
}, "Not a valid zip code format");

$.validator.addMethod("money", function(value, element) {
  return this.optional(element) || /^\d+(\.\d{2})?$/.test(value);
}, "Not a valid money amount format");

$.validator.addMethod("phone_format", function(value, element) {
  return this.optional(element) || /^(\+\d+\s)?\d+$/.test(value);
}, "Not a valid phone number format. Please enter only digits, and '+' if international");

$.validator.addMethod("not_null", function(value, element) {
  return this.optional(element) || value != '';
}, "Please make a valid selection");

$.validator.addMethod("valid_expire", function(value, element) {
  var d = new Date();
  var year = d.getFullYear() % 1000;
  var month = d.getMonth() + 1;
  value = parseInt(value);
  var form_year = parseInt($(element).closest('form').find("input[name=expire_year]").val());
  var form_month = parseInt($(element).closest('form').find("input[name=expire_month]").val());
  if ((year * 12 + month) > (form_year * 12 + form_month)) {
    return false;
  }
  else if ($(element).closest('form').find("input[name=expire_year]").val().length == 0) {
    return false;
  } else {return this.optional(element) || true}
}, "Please enter a date in the future");

function luhn(cc) {
  var sum = 0;
  cc = cc.split("").reverse().join("");
    for (var i = 0; i < cc.length; i++) {
      var digit = parseInt(cc.charAt(i));
      if (i % 2 == 1) {
        digit *= 2;
      }
    if (digit > 9) {
        digit -= 9;
    }
    sum += digit;
  }
  return ((sum % 10)==0)
}

$(function () {
    var cardOptions = {
	card_first_name: "required",
	card_last_name: "required",
	card_billing_city: "required",
	card_billing_state: {
            required: {
		depends: function (element) {
		    return $(element).closest('form').find("#card_billing_country").val() == 'US';
		}
            },
            not_null: {
		depends: function (element) {
		    return $(element).closest('form').find("#card_billing_country").val() == 'US';
		}
            }
	},
	card_billing_zip_code: {
            required: true,
            zip_format: {
		depends: function (element) {
		    return $(element).closest('form').find("#card_billing_country").val() == 'US';
		}
            },
            remote:
            {
		url: "/zip_check",
		type: "post",
		data: {state: function () { return $( "#card_billing_state" ).val();},
                       zip: function () { return $( "#card_billing_zip_code" ).val();},
                       country: function () { return $("#carD_billing_country").val();}
		      }
            }
	},
	card_billing_country: "required",
	{#phone: {
            required: true,
            digits: {
		depends: function (element) {
		    return $(element).closest('form').find("#intl_phone").val();
		}
            },
            phone_format: true
	},#}
	card_number: {
            required: true,
            cc: true,
            maxlength: 16,
            minlength: 15
	},
	card_expire_year: {
            required: true,
            digits: true,
            valid_expire: true
	},
	card_expire_month: {
            required: true,
            valid_expire: {
		depends: function (element) {
		    return $(element).closest('form').find("#card_expire_year").val().length != 0;
		}
            }
	},
	card_cvv: {
            required: true,
            cvv: true,
            minlength: 3
	},
	card_billing_address_1: "required",
	card_billing_city: "required",
	card_billing_state: {
            required: {
		depends: function (element) {
		    return $(element).closest('form').find("#card_billing_country").val() == 'US';
		}
            },
            not_null: {
		depends: function (element) {
		    return $(element).closest('form').find("#card_billing_country").val() == 'US';
		}
            }
	},
	card_billing_zip_code: {
            required: true,
            zip_format: {
		depends: function (element) {
		    return $(element).closest('form').find("#card_billing_country").val() == 'US';
		}
            },
            remote:
            {
		url: "/zip_check",
		type: "post",
		data: {state: function () { return $( "#card_billing_state" ).val();},
                       zip: function () { return $( "#card_billing_zip_code" ).val();},
                       country: function () { return $("#card_billing_country").val();}
		      }
            }
	}
    };

    var achOptions = {
	bank_routing_number: "required",
	bank_account_number: "required",
	bank_account_type: "required",
	bank_owned_by: "required",
	bank_registered_name: "required"
    };

    var virtualOptions = {
	errorClass: "error",
	highlight: function(element, errorClass) {
	    $(element).addClass(errorClass)
	    $(element).parent().parent().next("tr.error-row").removeClass('formhide')
	},
	unhighlight: function(element, errorClass) {
	    $(element).removeClass(errorClass)
	    $(element).parent().parent().next("tr.error-row").addClass('formhide')
	    $(element).parent().parent().next("tr.error-row").find('label').remove();
	},
	errorPlacement: function(error, element) {
	    error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
	},
	rules: {
	    email: {
		required: true,
		email: true
	    },
	    plan: {
		required: true,
		not_null: true,
	    },
	    desired_address: {
		required: true,
		not_null: true
	    },
	    terms: "required"
	}
    };
    $.extend(virtualOptions.rules, cardOptions);
    $.extend(virtualOptions.rules, achOptions);
    $("#apply_virtual").validate(virtualOptions);

});

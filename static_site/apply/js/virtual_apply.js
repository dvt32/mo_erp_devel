/**
  *
  * Javascript functions for virtual_apply
  *
  */

// hide billing_state_nonus and add_phone_to_plan
document.addEventListener('DOMContentLoaded', function() {
    // onload hide billing_province (non-US only option)
    document.getElementById("card_billing_province").parentElement.parentElement.style.display = 'none';
    // hide or show fields (depending on form state)
    changePricing();
    togglePaymentMethod();
}, false);

// get list of prices (passed as string from form)
var link_helper = document.getElementById('link_helper');
var cc_fee = JSON.parse(link_helper.getAttribute('data-cc-fee'));
var dict = JSON.parse(link_helper.getAttribute('data-pricing-dict'));
link_helper.parentNode.removeChild(link_helper);

var Today = new Date();
var num_days = new Date(Today.getYear(), Today.getMonth()+1, 0).getDate();

function changePricing() {
    // get id
    var desired_addr = document.getElementById('desired_address');
    var id = desired_addr.options[desired_addr.selectedIndex].value;

    if (id==0) {
	// hide if nothing selected
	hideServiceOptions();
    } else {
        // Update values
	var mo_fee = dict[id]['monthly_fee'];
	var setup_fee = dict[id]['setup_fee'];
	var scanning_fee = dict[id]['scanning_fee'];
	var forwarding_fee = dict[id]['forwarding_fee'];
	var phone_fee = addTax(dict[id]['phone_fee'], dict[id]['tax_rate']);
	var initial_fee = mo_fee + setup_fee;

	// add phone if checked
	if (document.getElementById('add_phone_to_plan').checked) {
	    initial_fee = initial_fee + phone_fee;
	    mo_fee = mo_fee + phone_fee;
	}

	// add discount if needed
	initial_fee = initial_fee - Discount(initial_fee);

	// add CC Fee if card is checked
	if (document.getElementById('pmt_method-0').checked) {
	    initial_fee = addCCFee(initial_fee, cc_fee);
	}

	// update elements
	document.getElementById('initial_fee').innerHTML = toCurrency(initial_fee);
	document.getElementById('monthly_fee').innerHTML = toCurrency(mo_fee);
	document.getElementById('setup_fee').innerHTML = toCurrency(setup_fee);
	document.getElementById('phone_pricing').innerHTML = toCurrency(phone_fee) + '/mo (incl. tax)';
	document.getElementById('scanning_pricing').innerHTML = toCurrency(scanning_fee) + '/page, weekly service. Scanned items are shredded unless mail forwarding is also selected';

	// special case mail forwarding; separate to monthly fee
	var forwarding_frequency = document.getElementById('mail_forwarding_frequency');
	var frequency = forwarding_frequency.options[forwarding_frequency.selectedIndex].value;
	var forwarding_price_multiplier = 1;

	if (frequency == 'monthly') {
	    forwarding_price_multiplier = 4;
	} else if (frequency == 'biweekly') {
	    forwarding_price_multiplier = 2;
	} else {
	    forwarding_price_multiplier = 1;
	}

	forwarding_fee = forwarding_fee * forwarding_price_multiplier;

	document.getElementById('forwarding_pricing').innerHTML = toCurrency(forwarding_fee) + ' every ';

	// make elements visible
	showServiceOptions();

	document.getElementById('pricing_table').style.display='block';
    }
}

function hideServiceOptions() {
    document.getElementById('pricing_table').style.display='none';
    document.getElementById('add_phone_to_plan').checked = false;
    document.getElementById('add_phone_to_plan').parentElement.parentElement.style.display='none';
    document.getElementById('add_mail_scanning_to_plan').checked = false;
    document.getElementById('add_mail_scanning_to_plan').parentElement.parentElement.style.display='none';
    document.getElementById('add_mail_forwarding_to_plan').checked = false;
    document.getElementById('add_mail_forwarding_to_plan').parentElement.parentElement.style.display='none';
}

function showServiceOptions() {
    /** Displays service options (phone, mail_scanning, mail_forwarding **/
    document.getElementById('add_phone_to_plan').parentElement.parentElement.style.display='table-row';
    document.getElementById('add_mail_scanning_to_plan').parentElement.parentElement.style.display='table-row';
    document.getElementById('add_mail_forwarding_to_plan').parentElement.parentElement.style.display='table-row';
}

function Discount(amount) {
    // Calculates discount if today > 28
    // also adds disclaimer if CC fee is added or if discount is added
    var desired_addr = document.getElementById('desired_address');
    var id = desired_addr.options[desired_addr.selectedIndex].value;
    var title = document.getElementById('initial_fee_title');
    var fee_title = document.getElementById('initial_fee');
    var explanation = document.getElementById('initial_fee_expl');
    var is_card = document.getElementById('pmt_method-0').checked;
    if (is_card) {
  	title.title = 'This includes a ' + cc_fee + '% credit card fee.'
    } else {
	title.title = '';
    }
    fee_title.title = title.title;
    explanation.innerHTML = title.title;
    var date = dict[id]['local_date'];
    if (date > 28) {
	var discount = (date - 28) / num_days;
	title.title = title.title + ' An additional ' + (discount * 100).toFixed(2) + '% discount is applied because today is the ' + date + 'th. Your next charge will be on the 28th of the next month.'
	fee_title.title = title.title;
	explanation.innerHTML = title.title;
	discount = amount * discount;
	return discount;
    }
    // No discount
    return 0;
}
function setPmtMethod(type) {
    if (type=='card') {
	document.getElementById("pmt_method-0").checked = true;
    } else {
	document.getElementById("pmt_method-1").checked = true;
    }
    togglePaymentMethod();
}

function togglePaymentMethod() {
    var is_card = document.getElementById('pmt_method-0').checked;
    var card_rows = document.getElementsByClassName("cardRow");
    var bank_rows = document.getElementsByClassName("bankRow");

    if (is_card) {
	var card_style = '';
	var bank_style = 'none';
    } else {
	var card_style = 'none';
	var bank_style = '';
    }

    for (var i = 0; i < card_rows.length; i++) {
	card_rows[i].style.display = card_style;
    }

    for (var i = 0; i < bank_rows.length; i++) {
	bank_rows[i].style.display = bank_style;
    }
    changePricing();
    clearErrors(); // Toggling between payment method does not allow errors to be shown
}

function clearErrors() {
    // Removes all error displays (unhighlights and removes labels)
    $('label.error').remove();
    $('.error').removeClass('error');
}


/** Update price when the state of any of the follow elements are changed **/
var address = document.getElementById("desired_address");
address.addEventListener('change', changePricing);

var phone = document.getElementById('add_phone_to_plan');
phone.addEventListener('change', changePricing);

var forwarding = document.getElementById('add_mail_forwarding_to_plan');
forwarding.addEventListener('change', changePricing);

var forwarding_frequency = document.getElementById('mail_forwarding_frequency');
forwarding_frequency.addEventListener('change', changePricing);

var country = document.getElementById('card_billing_country');
country.addEventListener('change', function(){changeState('card_billing')});

document.getElementById('pmt_method-0').addEventListener('change', function() {togglePaymentMethod();});
document.getElementById('pmt_method-1').addEventListener('change', function() {togglePaymentMethod();});

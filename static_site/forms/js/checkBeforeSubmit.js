var wasSubmitted = false;

window.onload = function() {
    // assign onsubmit to forms if there is exacty one
    // this function does not handle the case of multiple forms on a single page
    if (document.forms.length==1) {
	document.forms[0].onsubmit = checkBeforeSubmit;
    }
}

function checkBeforeSubmit(){
    // prevent double submissions
    if(!wasSubmitted) {
	// specific to WTForms
	var submit = document.querySelectorAll('[type="submit"]')[0];
	submit.disabled = true;
	submit.style.cursor = 'wait'
	submit.value = 'Please wait...';
	setTimeout(reset, 2000);
	wasSubmitted = true;
	return wasSubmitted;
    }

    return false;
}

function reset() {
    var submit = document.querySelectorAll('[type="submit"]')[0];
    submit.disabled = false;
    submit.style.cursor = 'auto';
    submit.value = 'Submit';
    wasSubmitted = false;
}

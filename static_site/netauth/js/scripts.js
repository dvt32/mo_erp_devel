$(function () {
  function isMac(mac) {
    return /([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})/.test(mac);
  }
  
  function htmlDecode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    // handle case of empty input
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
  }
  
  if (typeof(routerUrl) !== "undefined" && routerUrl !== "NONE" && window.location.pathname != "/") {
    $.ajax({
      url: routerUrl + "/get_mac",
      type: "POST",
      timeout: 3000,
      success: function (response) {
        if (!isMac(response)) {
          $("noscript").replaceWith($('<div>' + htmlDecode($("noscript").html()) + '</div>'));
        } else {
          $("form").find("#mac_address").val(response);
        }
      },
      error: function (error) {
        $("noscript").replaceWith($('<div>' + htmlDecode($("noscript").html()) + '</div>'));
      }
    });
  }
  
  // if javascript is disable, the button will be hidden
  $("#quick").attr('style', 'display:inline-block;');
  
  $("#quick").click(function() {
    if (typeof(routerUrl) !== "undefined" && routerUrl !== "NONE") {
      $.ajax({
        url: routerUrl + "/get_mac",
        type: "POST",
        timeout: 3000,
        success: function (response) {
          if (isMac(response)) {
            $.ajax({
              url: "/quick_add?mac=" + response,
              type: "POST",
              timeout: 3000,
              success: function (response) {//"DONE"
                location.reload(true);  
              },
              error: function (error) {
                window.location.replace('/new_device');
              }
            });
          } else {
            window.location.replace('/new_device');
          }
        },
        error: function (error) {
          window.location.replace('/new_device');
        }
      });
    }
  });
  
  
  
  $(".btn-guest.delete").click(function () {
    var id = $(this).attr("data-id");
    var c = confirm("Are you sure you wish to delete this guest device?");
    if (c == true) {
      $.ajax({
        url: "/delete_guest?id=" + id,
        type: "POST",
        success: function (response) { 
          response = JSON.parse(response);
          if (response !== 'deleted') {
            var d = alert("Something went wrong deleting this device");
          }
          location.reload(true);
        }
      });
    }
  });
  
  $(".btn-manage.delete").click(function () {
    var id = $(this).attr("data-id");
    var c = confirm("Are you sure you wish to delete this device?");
    if (c == true) {
      $.ajax({
        url: "/delete_device?id=" + id,
        type: "POST",
        success: function (response) {
          response = JSON.parse(response);
          if (response !== 'deleted') {
            var d = alert("Something went wrong deleting this device");
          }
          location.reload(true);
        }
      });
    }
  });
  
  

  $("#mac_btn").click(function() {
    $("#mac_info").toggle();
  });

});


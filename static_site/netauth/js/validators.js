$.validator.addMethod("not_null", function(value, element) {
  return this.optional(element) || value != '';
}, "Please make a valid selection");


$.validator.addMethod("mac", function(value, element) {
  return this.optional(element) || (/([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})/.test(value));
}, "Not a valid MAC Address");

$.validator.addMethod("code", function(value, element) {
  return this.optional(element) || (/([0-9A-Z]{6})/.test(value));
}, "Not a valid Activation Code");

$(function () {
var loginOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
      $(element).parent().parent().next("tr.error-row").find('label').remove();
      error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
    },
    rules: {
      email: {
        required: true,
        email: true
      },
      password: "required"
    }
  };

  $("form#login").validate(loginOptions);

var registerOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
      $(element).parent().parent().next("tr.error-row").find('label').remove();
      error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
    },
    rules: {
      name: "required",
      company: "required",
      building: "required",
      email: {
        required: true,
        email: true
      }
    }
  };

  $("form#register").validate(registerOptions)


var passwordOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
      $(element).parent().parent().next("tr.error-row").find('label').remove();
      error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
    },
    rules: {
      token: "required",
      password: "required",
      confirm: {
        required: true,
        equalTo: "#password"
      }
    }
  };

  $("form#password_form").validate(passwordOptions);

  var guestOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
      $(element).parent().parent().next("tr.error-row").find('label').remove();
      error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
    },
    rules: {
      first_name: "required",
      last_name: "required",
      company: "required",
      guest_email: {
        required: true,
        email: true
      },
      member_email: {
        required: true,
        email: true
      }
    }
  };

  $("form#guest").validate(guestOptions);
  
  
  var guestRequestOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
      $(element).parent().parent().next("tr.error-row").find('label').remove();
      error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
    },
    rules: {
      first_name: "required",
      last_name: "required",
      company: "required",
      guest_email: {
        required: true,
        email: true
      },
    }
  };

  $("form#request_guest").validate(guestRequestOptions);
  
  var guestRegisterOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
      $(element).parent().parent().next("tr.error-row").find('label').remove();
      error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
      console.log(error);
    },
    rules: {
      mac_address: {
        required: true,
        mac: true,
      },
      activation_code: {
        required: true,
        code: true,
      },
    }
  };

  $("form#register_guest").validate(guestRegisterOptions);

  var addOptions = {
    errorClass: "error",
    highlight: function(element, errorClass) {
      $(element).addClass(errorClass)
      $(element).parent().parent().next("tr.error-row").removeClass('formhide')
    },
    unhighlight: function(element, errorClass) {
      $(element).removeClass(errorClass)
      $(element).parent().parent().next("tr.error-row").addClass('formhide')
      $(element).parent().parent().next("tr.error-row").find('label').remove();
    },
    errorPlacement: function(error, element) {
      $(element).parent().parent().next("tr.error-row").find('label').remove();
      error.appendTo($(element).parent().parent().next("tr.error-row").find('td.error-cell'));
    },
    rules: {
      nickname: "required",
      mac_address: {
        required: true,
        mac: true
      }
    }
  };

  $("form#add_device").validate(addOptions)

});
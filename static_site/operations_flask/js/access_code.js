function toggleAdvanced() {
  var x = document.getElementsByClassName("advanced");
  for (var i = 0; i < x.length; i++) {
    if (x[i].style.display === "none" || x[i].style.display === "") {
      x[i].style.display = "table-row";
    } else {
      x[i].style.display = "none";
    }
  }
}
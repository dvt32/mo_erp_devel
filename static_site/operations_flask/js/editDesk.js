var $SCRIPT_ROOT = "{% if is_production_deployment=='1' %}https{% else %}http{% endif %}://{{ operations_fqdn }}/{{ operations_flask }}/space"
var priceCounter = 1;
var interactionCounter = 1;

Date.prototype.addMonth = function() {
    return new Date(this.getFullYear(), this.getMonth()+1, this.getDate());
}

Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
	    (mm>9 ? '' : '0') + mm,
	    (dd>9 ? '' : '0') + dd
	   ].join('-');
};

$(document).ready(function() {
    ajaxSetup()
    addSelect2($('.selectpicker'));
    $('.employee_selectpicker').select2();
});

function addSelect2(field) {
    // Adds select2 to a field or set of fields (by Jquery selector)
    field.each(
	function() {
	    $(this).select2({
		minimumInputLength: 2,
		ajax: {
		    dataType: 'json',
		    delay: 250,
		    url: $SCRIPT_ROOT + '/query_accounts',
		    processResults: function(data) {
			return {
			    results: data.items
			};
		    }
		}
	    });
	}
    );
}

function addInteractionRow(employee) {
    // optional employee argument used to set default
    var table = document.getElementById('desk_sale_interaction_table');
    var row_index = document.getElementById('add_interaction_row').rowIndex;

    var interaction_row = table.insertRow(row_index);
    var error_row = table.insertRow(row_index+1);

    interaction_row.setAttribute('id', 'new_interaction_row_' + interactionCounter);
    error_row.setAttribute('id', 'new_interaction_error_row_' + interactionCounter);

    var note_input = setupTextarea('new_interaction_note_' + interactionCounter);
    var employee_input = document.createElement('select');
    var date_input = setupDateInput('new_interaction_date_' + interactionCounter);
    date_input.setAttribute('value', (new Date()).yyyymmdd());
    var $options = $('select#employee_list_select_template > option').clone();
    $(employee_input).append($options);

    employee_input.setAttribute('name', 'new_interaction_employee_' + interactionCounter);
    employee_input.value = employee;

    var delete_button = document.createElement('button');
    delete_button.setAttribute('type', 'button');
    delete_button.setAttribute('data-interaction-counter', interactionCounter);
    delete_button.onclick = function() { deleteInteractionRow($(this).attr('data-interaction-counter')); }
    delete_button.innerHTML = 'Delete This';

    insert_input_and_error_cell(interaction_row, error_row, note_input);
    insert_input_and_error_cell(interaction_row, error_row, employee_input);
    insert_input_and_error_cell(interaction_row, error_row, date_input);
    insert_input_and_error_cell(interaction_row, error_row, delete_button);
    $(employee_input).select2(); // start select2 after appended to body

    interactionCounter = interactionCounter + 1;

}

function addPriceRow() {
    var table = document.getElementById('desk_price_table');
    var row_index = document.getElementById('add_price_row').rowIndex;

    var price_row = table.insertRow(row_index);
    var error_row = table.insertRow(row_index+1);

    var price_input = setupNumericInput('new_price_rent_' + priceCounter);
    var rent_discount_div = document.createElement('p');
    var internet_input = setupNumericInput('new_price_internet_' + priceCounter);
    var internet_lines_input = setupIntegerInput('new_price_lines_internet_' + priceCounter);
    var internet_discount_div = document.createElement('p');
    var phone_input = setupNumericInput('new_price_phone_' + priceCounter);
    var phone_lines_input = setupIntegerInput('new_price_lines_phone_' + priceCounter);
    var phone_discount_div = document.createElement('p');
    var portal_lines_input = setupIntegerInput('new_price_lines_portal_' + priceCounter);
    var price_per_sf_div = document.createElement('p');
    var is_monthly_input = setupIsMonthlyInput('new_price_monthly' + priceCounter);
    var start_input = setupDateInput('new_price_start_' + priceCounter);
    var end_input = setupDateInput('new_price_end_' + priceCounter);
    var note_input = setupTextarea('new_price_note_' + priceCounter);

    var account_input = document.createElement('select');
    var $options = $('select#waiting_account > option').clone();
    $(account_input).append($options);
    var delete_button = document.createElement('button');

    // Sets default value by looking for an existing input with name 'price_rent_##' or 'new_price_rent_##'
    price_input.setAttribute('value', $("input[name*='price_rent_']:last").val());

    internet_input.setAttribute('value', $("input[name*='price_internet_']:last").val());
    phone_input.setAttribute('value', $("input[name*='price_phone_']:last").val());

    internet_lines_input.setAttribute('value', $("input[name*='price_lines_internet_']:last").val());
    phone_lines_input.setAttribute('value', $("input[name*='price_lines_phone_']:last").val());
    portal_lines_input.setAttribute('value', $("input[name*='price_lines_portal_']:last").val());

    price_row.setAttribute('id', 'new_price_row_' + priceCounter);
    error_row.setAttribute('id', 'new_price_error_row_' + priceCounter);
    error_row.setAttribute('class', 'error-row');

    var prev_end_date = new Date($("input[name*='price_end']:last").val());

    if (prev_end_date) {
	// start dates are first day of the next month
	var new_start_date = new Date(prev_end_date.getFullYear(), prev_end_date.getMonth() + 1);
	start_input.setAttribute('value', new_start_date.yyyymmdd());
	// end dates are the last day of the next month (month +2 and then subtract 1 day)
	// 1 is first day of a month, 0 is last day of the previous month
	var new_end_date = new Date(prev_end_date.getFullYear(), prev_end_date.getMonth() + 2, 0);
	end_input.setAttribute('value', new_end_date.yyyymmdd());
    }

    account_input.setAttribute('id', 'account_' + priceCounter);
    account_input.setAttribute('name', 'new_price_account_'+ priceCounter);
    var prev_account_value = $("select[name*='price_account']:last").val();
    $(account_input).val(prev_account_value);

    delete_button.setAttribute('type', 'button');
    delete_button.setAttribute('data-price-counter', priceCounter);
    delete_button.onclick = function() { deletePriceRow($(this).attr('data-price-counter')); }
    delete_button.innerHTML = 'Delete This';

    insert_input_and_error_cell(price_row, error_row, price_input);
    insert_input_and_error_cell(price_row, error_row, price_per_sf_div);
    insert_input_and_error_cell(price_row, error_row, rent_discount_div);
    insert_input_and_error_cell(price_row, error_row, internet_input);
    insert_input_and_error_cell(price_row, error_row, internet_lines_input);
    insert_input_and_error_cell(price_row, error_row, internet_discount_div);
    insert_input_and_error_cell(price_row, error_row, phone_input);
    insert_input_and_error_cell(price_row, error_row, phone_lines_input);
    insert_input_and_error_cell(price_row, error_row, phone_discount_div);
    insert_input_and_error_cell(price_row, error_row, portal_lines_input);
    insert_input_and_error_cell(price_row, error_row, is_monthly_input);
    insert_input_and_error_cell(price_row, error_row, start_input);
    insert_input_and_error_cell(price_row, error_row, end_input);
    insert_input_and_error_cell(price_row, error_row, account_input);
    insert_input_and_error_cell(price_row, error_row, note_input);
    insert_input_and_error_cell(price_row, error_row, delete_button);

    addSelect2($(account_input)); // Start the select2 library after it's appended to the document
    priceCounter = priceCounter + 1;
}

function deleteInteractionRow(interactionCounter) {
    document.getElementById('new_interaction_row_' + interactionCounter).remove();
    document.getElementById('new_interaction_error_row_' + interactionCounter).remove();
}

function insert_input_and_error_cell(row, err_row, input) {
    /** Each input needs a container (insertCell) and is appended
	In addition, the error row needs a container **/
    row.insertCell().appendChild(input);
    err_row.insertCell().setAttribute('class', 'error_cell');
}

function setupNumericInput(name) {
    var input = document.createElement('input');
    input.setAttribute('name', name);
    input.setAttribute('type', 'number');
    input.setAttribute('min', '0.00');
    input.setAttribute('max', '99999.99');
    input.setAttribute('step', '0.01');
    return input;
}

function setupIntegerInput(name) {
    var input = document.createElement('input');
    input.setAttribute('name', name);
    input.setAttribute('type', 'number');
    input.setAttribute('min', '0');
    input.setAttribute('max', '999');
    input.setAttribute('step', '1');
    return input;
}

function setupTextarea(name) {
    input = document.createElement('textarea');
    input.setAttribute('name', name);
    return input;
}

function setupDateInput(name) {
    var input = document.createElement('input');
    input.setAttribute('type', 'date');
    input.setAttribute('name', name);
    return input;
}

function setupIsMonthlyInput(name) {
    var input = document.createElement('select');
    input.name = 'new_price_monthly_' + priceCounter;
    var opt1 = document.createElement("option");
    var opt2 = document.createElement("option");
    opt1.text = 'License';
    opt2.text = 'Monthly';
    input.appendChild(opt1);
    input.appendChild(opt2);
    return input;
}

function deletePriceRow(priceCounter) {
    document.getElementById('new_price_row_' + priceCounter).remove();
    document.getElementById('new_price_error_row_' + priceCounter).remove();
}

var form_is_valid;
function validateForm() {
    /**  We're just checking the inputs created 'by hand'
     i.e. any price_ or new_price rows. These aren't created by the WTForms module
    **/
    $('td.error_cell').text('');
    form_is_valid = true;
    var table = $('#desk_table')[0];
    $("input[name*='price_'],select[name*='price_account'").each(function() {
	if ($(this).val() == '') {
	    form_is_valid = false;
	    display_error($(this), 'This input is required');
	} else if (($(this).attr('name').indexOf('start') == -1) && ($(this).attr('name').indexOf('end')== -1)) {
	    // it's a price / numeric input
	    var numStr = $(this).val();
	    var regex  = /^\d+(?:\.\d{0,2})?$/;
	    if (!regex.test(numStr)) {
		form_is_valid = false;
		display_error($(this), 'Invalid format');
	    }
	} else {
	    // it's a date input, validate format here
	    // datepicker is doing the validation for us (html5)
	}

    });

    var date_dictionary = {}
    // validate end >= start for each field
    $("input[name*='price_start_'").each(function() {
	var end_name = $(this).attr('name').replace(/start/,'end');
	var start = $(this).val();
	var end = $("input[name='" + end_name + "']");
	start_time = (new Date(start)).getTime();
	end_time = (new Date(end.val())).getTime();
	if (start_time >= end_time) {
	    display_error($(end), 'End earlier than start!');
	    form_is_valid = false;
	}
	date_dictionary[$(this).attr('name')] = {'start':start_time, 'end': end_time};
    });

    // Make sure there's no overlap
    for (i in date_dictionary) {
	for (j in date_dictionary) {
	    if (i != j) {
		if (is_overlapping(date_dictionary[i]['start'], date_dictionary[i]['end'], date_dictionary[j]['start'], date_dictionary[j]['end'])) {
		    display_error($("input[name='" + i + "']"), 'Overlap found.');
		    form_is_valid = false;
		}
	    }
	}
    }

    $("textarea[name*='interaction'],input[name*='interaction_'],select[name*='interaction']").each(function() {
	if ($(this).val()=="") {
	    form_is_valid = false;
	    display_sale_interaction_error($(this), 'This field cannot be empty!');
	}
    });
    return form_is_valid;
}

function get_row_inx(cell) {
    return cell.closest('tr').index();
}

function get_col_inx(cell) {
    var td = cell.closest('td');
    var tr = cell.closest('tr');
    return tr.children().index(td);
}

function display_error(cell, message) {
    row_inx = get_row_inx(cell) + 2;
    col_inx = get_col_inx(cell);
    var table = $('#desk_price_table')[0];
    var error_td = $(table.rows[row_inx].cells[col_inx]);
    error_td.html(message);
}

function display_sale_interaction_error(cell, message) {
    row_inx = get_row_inx(cell) + 2;
    col_inx = get_col_inx(cell);
    var table = $('#desk_sale_interaction_table')[0];
    var error_td = $(table.rows[row_inx].cells[col_inx]);
    error_td.html(message);
}

function ajaxSetup() {
    csrf_token = $('#csrf_token').attr('value');
    $.ajaxSetup({
	cache: false,  // required for IE, or else the request never gets sent
	beforeSend: function(xhr, settings) {
	    xhr.setRequestHeader("X-CSRFToken", csrf_token);
	}
    });
}

function delete_existing_price_row(priceCounter, deskId) {
    $.getJSON($SCRIPT_ROOT + '/edit_desk_info/delete_desk_price', {
	desk_price_id: priceCounter,
	desk_id: deskId
    })
	.done(function(data) {
	    if ('success' in data) {
		document.getElementById('price_row_' + data['desk_price_id']).remove();
		document.getElementById('price_error_row_' + data['desk_price_id']).remove();
	    }
	});
}

function delete_existing_interaction_row(interactionCounter, deskId) {
    $.getJSON($SCRIPT_ROOT + '/edit_desk_info/delete_desk_sale_interaction', {
	desk_sale_interaction_id: interactionCounter,
	desk_id: deskId
    })
	.done(function(data) {
	    if ('success' in data) {
		document.getElementById('interaction_row_' + data['desk_sale_interaction_id']).remove();
		document.getElementById('interaction_error_row_' + data['desk_sale_interaction_id']).remove();
	    }
	});
}

function is_overlapping(startA, endA, startB, endB) {
    return (startA < endB && endA > startB)
}

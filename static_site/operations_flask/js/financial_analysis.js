var total_rent_revenue;
var total_sf;
var occupied_sf;
var query_dict = {};

$(document).ready(function() {
    // adapted from https://stackoverflow.com/questions/5448545
    location.search.substr(1).split("&").forEach(function(item) {query_dict[item.split("=")[0]] = item.split("=")[1]})
    total_sf = parseFloat($('#total_available_sf').html());
    update_table();
    update_buttons();
});

function calculate_total_parameter(class_selector) {
    // calculates total for a specific type
    // e.g. .rent_revenue, .phone_revenue, .internet_revenue
    var sum = 0;
    var exclude_ids = get_exclude_ids();
    $('.account_row').each(function() {
	if (exclude_ids.indexOf($(this).data('id')) == -1) {
	    sum += parseFloat($(this).find(class_selector).html());
	}
    });
    return sum;
}

function calculate_excluded_parameter(class_selector) {
    // calculates total for a specific type (ignored values only)
    // e.g. .rent_revenue, .phone_revenue, .internet_revenue
    var sum = 0;
    var exclude_ids = get_exclude_ids();
    $('.account_row').each(function() {
	if (exclude_ids.indexOf($(this).data('id')) != -1) {
	    sum += parseFloat($(this).find(class_selector).html());
	}
    });
    return sum;
}

function update_buttons() {
    var exclude_ids = get_exclude_ids();

    $('.account_row').each(function() {
	if (exclude_ids.indexOf($(this).data('id')) != -1) {
	    $(this).find('button').html('Include me');
	}
    });
}

function get_exclude_ids() {
    if ('exclude_ids' in query_dict) {
	return query_dict['exclude_ids'].split(',').map(Number);
    }
    return [];
}

function toggle_id(id) {
    // adds account id to GET exclude_ids parameter
    var exclude_ids = get_exclude_ids()
    if ('exclude_ids' in query_dict) {
	if (exclude_ids.indexOf(id) == -1) {
	    // id is not in array
	    exclude_ids.push(id);
	} else {
	    // id is in array (remove it)
	    remove_from_array(exclude_ids, id);
	}
	value = exclude_ids.join(',');
    } else {
	value = id;
    }
    if (value=='') {
	remove_url_parameter('exclude_ids');
    } else {
	insertParam('exclude_ids', value);
    }
}

function remove_from_array(array, element) {
    const index = array.indexOf(element);
    array.splice(index, 1);
}

function update_table() {
    var rent_revenue = calculate_total_parameter('.rent_revenue');
    var phone_revenue = calculate_total_parameter('.phone_revenue');
    var internet_revenue = calculate_total_parameter('.internet_revenue');
    var occupied_sf = calculate_total_parameter('.occupied_sf');
    var ignored_sf = calculate_excluded_parameter('.occupied_sf');
    var total_sf_with_exclusion = total_sf - ignored_sf;
    $('#total_revenue').html(rent_revenue + phone_revenue + internet_revenue);
    $('#total_rent_revenue').html(rent_revenue);
    $('#total_phone_revenue').html(phone_revenue);
    $('#total_internet_revenue').html(internet_revenue);
    $('#total_occupied_sf').html(occupied_sf);
    $('#percent_occupied').html(
	(occupied_sf / total_sf_with_exclusion * 100).toFixed(2)
    );
    $('#total_available_sf').html(total_sf_with_exclusion);
    $('#total_ignored_sf').html(ignored_sf);
}

function insertParam(key, value) {
    // adapted from https://stackoverflow.com/questions/486896
    // updates GET parameter
    key = encodeURI(key); value = encodeURI(value);

    var kvp = document.location.search.substr(1).split('&');

    var i=kvp.length; var x; while(i--)
    {
	x = kvp[i].split('=');

	if (x[0]==key)
	{
	    x[1] = value;
	    kvp[i] = x.join('=');
	    break;
	}
    }

    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&');
}

function remove_url_parameter(parameter) {
    //https://stackoverflow.com/questions/1634748/
    //prefer to use l.search if you have a location/link object
    url = document.location.href;
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
    }
    window.location.replace(url);
}

/**
 *
 * Used for manage_daypass to submit updates to daypasses
 *
 **/

var $SCRIPT_ROOT = "{% if is_production_deployment=='1' %}https{% else %}http{% endif %}://{{ operations_fqdn }}/{{ operations_flask }}/daypass"

$( document ).ready(function() {
    ajax_setup();
});


function ajax_setup() {
    csrf_token = $('#csrf_token').attr('value');
    $.ajaxSetup({
	cache: false,  // required for IE, or else the request never gets sent
	beforeSend: function(xhr, settings) {
	    xhr.setRequestHeader("X-CSRFToken", csrf_token);
	}
    });
}

function update_daypass_note(pass_id) {
    var note = $('#note-' + pass_id).val();
    $.getJSON($SCRIPT_ROOT + '/_update_note', {
	pass_id: pass_id,
	note: note
    });
}

function update_daypass_status(pass_id, status) {
    $.getJSON($SCRIPT_ROOT + '/_update_status', {
	pass_id: pass_id,
	status: status
    }).done(function(data) {
	if (data['result'] == 'success') {
	    if (status=='TRUE') {
		console.log('ok');
		$('#update-btn-' + pass_id).html('redeemed!');
		$('#update-btn-' + pass_id).attr('onclick', "update_daypass_status(" + pass_id + ",'FALSE');");
	    } else {
		$('#update-btn-' + pass_id).html('reactivated!');
		$('#update-btn-' + pass_id).attr('onclick', "update_daypass_status(" + pass_id + ",'TRUE');");
	    }
	}
    });
}

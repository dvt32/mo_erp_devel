var $SCRIPT_ROOT = "{% if is_production_deployment=='1' %}https{% else %}http{% endif %}://{{ operations_fqdn }}/{{ operations_flask }}/finance/report";

$(document).ready(function() {
    $('.select2').select2();
    ajax_setup();
});

function ajax_setup() {
    csrf_token = $('#csrf_token').attr('value');
    $('#csrf_token').remove();
    $.ajaxSetup({
	cache: false,  // required for IE, or else the request never gets sent
	beforeSend: function(xhr, settings) {
	    xhr.setRequestHeader("X-CSRFToken", csrf_token);
	}
    });
}

function delete_recipient(report_type_id, employee_id) {
    if (!confirm('Are you sure you want to remove this person?')) {
	return;
    }
    $.getJSON($SCRIPT_ROOT + '/delete_recipient', {
	building_id: get('building_id'),
	report_type_id: report_type_id,
	employee_id: employee_id
    });
    $("#report-" + report_type_id + "-employee-" + employee_id).remove();
}

function add_recipient(report_type_id) {
    var employee_id = $('#select-' + report_type_id).val();
    var first_name = $('#select-' + report_type_id).text();

    $.getJSON($SCRIPT_ROOT + '/add_recipient', {
	building_id: get('building_id'),
	report_type_id: report_type_id,
	employee_id: employee_id
    }).done(function(data) {
	if ('error' in data) {
	    alert(data['error']);
	} else {
	    /** Force a reload so we don't have to deal with inserting a td **/
	    location.reload();
	}
    });
}

function get(name){
    /** parses the value for a given request.GET argument **/
    if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
	return decodeURIComponent(name[1]);
}

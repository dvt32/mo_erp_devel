var dragging_to_create_reservation;
var dragging_to_resize_reservation;
var dragging_to_shift_reservation;
var selected_reservation_id;
var start_row_index;
var start_col_index;
var total_rows;
var current_row_index;
var rows_above_selected;
var rows_below_selected;
var $SCRIPT_ROOT = "{% if is_production_deployment=='1' %}https{% else %}http{% endif %}://{{ operations_fqdn }}/{{ operations_flask }}/meet"
var reservation_data;
var table;
var refresh_timeout = {{ reserve_room_refresh_delay }}; // value in seconds
var csrf_token;
var last_hover_row_index=-1;
var last_hover_col_index=-1;
var last_top_cell;
var recent_overlay_toggle = false;
var prevent_taphold = false;

function initialize() {
    ajax_setup(); // required to set csrf_token
    get_reservations();
    setInterval(function() {get_reservations();}, refresh_timeout * 1000);

    /** only show 9-5 **/
    total_rows = $('#room_reservation_table tr').length - 2; // total rows not including two header rows
    table = $("#room_reservation_table")[0]; // selector returns a list so we have to get the first one
    toggle_rows(); // get_reservation call inside here

    /** show the calendar **/
    $('#dp').datepicker({ dateFormat: 'yy-mm-dd', defaultDate: get('date') });
    $('#dp').datepicker('show');

    set_outside_table_click_event(); // clears highlighted rows if clicked outside
    set_date_links(); // also sets calendar default date
}

$(document).ready(function () {
    /** initialization **/
    initialize();

    /** Specific bug with ESCAPE and 'clickoutside' workaround **/
    $(document).keyup(function (e) {
	if (e.keyCode == $.ui.keyCode.ESCAPE && $('#dialog').hasClass('ui-dialog-content')) {
	    $("#dialog").dialog("close").unbind('clickoutside');
	}
    });

    /** bind appropriate interactions **/
    $("table#room_reservation_table")
	.disableSelection()
	.on('click taphold', function(e) {
	    if (e.type == 'taphold' && prevent_taphold)	return;
	    if ($(e.target).closest('td').is('.schedule_reservation_occupied_slot_end, .schedule_reservation_occupied_slot_start, .schedule_reservation_occupied_slot_middle')) {
		selected_reservation_id = $(e.target).closest('td').attr('data-reservation-id');
		show_edit_note_dialog(e);
	    }
	})
	.on('touchstart', function(e) {
	    handle_mousedown(e);
	})
	.on('mousedown', function(e) {
	    handle_mousedown(e);
	});

    $(document)
	.on('touchmove', function(e) {
	    if (e.originalEvent.touches.length==3) {
		if (!recent_overlay_toggle) {
		    recent_overlay_toggle = true;
		    $('#overlay').toggle();
		    if ($('#dialog').hasClass('ui-dialog-content') && $('#dialog').dialog('isOpen')) {
			$('#dialog').dialog('close').unbind('clickoutside');
		    }
		    clear_highlighted();
		    setTimeout(function() {recent_overlay_toggle=false;}, 500);
		    return;
		}

	    }
	    if( dragging_to_create_reservation ) {
		handle_dragging_to_create_reservation(e);
	    } else if (dragging_to_resize_reservation) {
		handle_dragging_to_resize_reservation(e);
	    } else if (dragging_to_shift_reservation) {
		handle_dragging_to_shift_reservation(e);
	    }
	})
	.on('mousemove', function(e) {
	    if( dragging_to_create_reservation ) {
		handle_dragging_to_create_reservation(e);
	    } else if (dragging_to_resize_reservation) {
		handle_dragging_to_resize_reservation(e);
	    } else if (dragging_to_shift_reservation) {
		handle_dragging_to_shift_reservation(e);
	    }
	} )
	.on('touchend mouseup', function(e) {
	    handle_mouseup(e);
	});
});

/** dialog functions **/
function show_create_reservation_dialog(e) {
    /** touch doesn't have pageX
     set e to the closest td so that the dialog is correctly positioned **/
    if (typeof(e.pageX)=='undefined') {
	var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
	e = $(document.elementFromPoint(touch.clientX, touch.clientY)).closest('td');
    }
    var buttons = [];
    buttons.push({
	text:"Submit",
	class: "submit-button",
	click: function() {
	    /** The form runs get_highlighted_slots() before submit
		to figure out the appropriate time range **/
	    $('form').submit();
	}
    });
    $("#dialog").dialog(
	{
	    title: "Create Reservation",
	    position: {
		my: "center",
		of: e
	    },
	    modal: true,
	    buttons: buttons,
	    open: function() {
		$('#dialog').focus();
	    },
	    close: function() {
		clear_highlighted();
		$(this).unbind('clickoutside');
	    }
	}
    );


    // required because of the other dialog requiring separate side buttons
    $('.ui-dialog-buttonset>button:last-child').css('float','right');

    /** create 'click outside' event 50ms after the dialog is created, otherwise it will auto-close **/
    setTimeout(function() {$('#dialog').bind('clickoutside', function() {
	prevent_taphold = false;
	$("#dialog").dialog("close");
    });}, 250);
}

function show_edit_note_dialog(e) {
    populate_page_with_reservations();
    if (typeof(e.pageX)=='undefined') {
	e = $(e.target);
    }
    var buttons = [];
    buttons.push({
	icon: 'ui-icon-trash',
	class: 'delete-button',
	click: function() {
	    delete_reservation();
	}
    });
    buttons.push({
	text:"Submit",
	class: 'submit-button',
	click:function() {
	    update_reservation_note();
	}
    });
    var cells = $("td[data-reservation-id='" + selected_reservation_id + "']");
    var time_range = get_time_range(get_row_inx(cells.first()), get_row_inx(cells.last()));

    $('#note_dialog').dialog(
	{
	    title: reservation_data[selected_reservation_id]['account_name'],
	    position: {
		my: "center",
		of: e
	    },
	    modal: true,
	    buttons: buttons,
	    open: function() {
		$('#reservation_info').html('When: <b>' + time_range + '</b>');
		$('input#edit_note').val(reservation_data[selected_reservation_id]['note']);
		$('#note_dialog input#edit_note').keypress(function(e) {
		    if (e.keyCode == $.ui.keyCode.ENTER) {
			$(this).parent().parent().parent().find("button:contains('Submit')").trigger("click");
		    }
		});
		$('#note_dialog input').focus();
	    },
	    close:function() {
		clear_highlighted();
		populate_page_with_reservations();
		$(this).unbind('clickoutside');
	    }
	}
    );

    $('.ui-dialog-buttonset>button:last-child').css('float','right');

    setTimeout(function() {
	$('#note_dialog').bind(
	    'clickoutside',
	    function() {
		$('#note_dialog').dialog('close').unbind('clickoutside');
		prevent_taphold = false;
	    }
	)
    },
	       500);

}
/** end dialog functions **/

/** message handling functions **/
function display_message(message) {
    clear_messages();
    $('<div class="response-div"><span class="closebtn" onclick="this.parentElement.style.display=' + "'none'" + ';">&times;</span>' + message + '</div>').prependTo('body');
    window.scrollTo(0,0);
}

function clear_messages() {
    $('.response-div').remove();
}
/** end message handling functions **/

/** Ajax functions **/
function update_reservation() {
    /** submits change to a reservation
	on done: updates page with all reservations
	on fail: displays message and updates page  **/
    var updated_reservation_cells = $("td[data-reservation-id='" + selected_reservation_id + "']");
    $.getJSON($SCRIPT_ROOT + '/_update_reservation', {
	reservation_id: selected_reservation_id,
	start_ts: updated_reservation_cells.first().attr('data-time-start'),
	end_ts: updated_reservation_cells.last().attr('data-time-end'),
	room_id: updated_reservation_cells.first().attr('data-room-id')
    }).done(function(data) {
	if ('error' in eval(data)) {
	    display_message(data['error']);
	}
	get_reservations();
    }).fail(function(jqxhr, textStatus, error) {
	clear_messages();
	var err = textStatus + ", " + error;
	display_message('Failed to contact server, your changes may not be saved. ' + err);
	get_reservations();
    });
}

function update_reservation_note() {
    $.getJSON($SCRIPT_ROOT + '/_update_reservation_note', {
	reservation_id: selected_reservation_id,
	note: $('input#edit_note').val()
    }).done(function(data) {
	if ('error' in eval(data)) {
	    display_message(data['error']);
	}
	get_reservations();
    }).fail(function(jqxhr, textStatus, error) {
	clear_messages();
	var err = textStatus + ", " + error;
	display_message('Failed to contact server, your changes may not be saved. ' + err);
	get_reservations();
    });
}

function get_reservations() {
    $.getJSON($SCRIPT_ROOT + '/_get_reservation', {
	date: get('date'),
	floor_id: get('floor_id'),
	room_type: get('room_type')
    })
	.done(function(data) {
	    if ('error' in eval(data)) {
		display_message(data['error']);
	    } else {
		reservation_data = data;
		populate_page_with_reservations();
	    }
	}).fail(function(jqxhr, textStatus, error) {
	    clear_messages();
	    var err = textStatus + ", " + error;
	    display_message('Failed to get reservations. Your changes may not be saved.');
	});
}

function delete_reservation() {
    $.getJSON($SCRIPT_ROOT + '/_delete_reservation', {
	reservation_id: selected_reservation_id
    })
	.done(function(data) {
	    get_reservations();
	}).fail(function(jqxhr, textStatus, error) {
	    clear_messages();
	    var err = textStatus + ", " + error;
	    display_message('Failed to delete reservation. Your changes may not be saved.');
	    get_reservations();
	});
}

function ajax_setup() {
    csrf_token = $('#csrf_token').attr('value');
    $.ajaxSetup({
	cache: false,  // required for IE, or else the request never gets sent
	beforeSend: function(xhr, settings) {
	    xhr.setRequestHeader("X-CSRFToken", csrf_token);
	}
    });
}
/** end Ajax functions **/

function generate_resize_div() {
    var resizer = $("<div class='resize-div'></div>");

    resizer.mousedown(function(e) {
	dragging_to_resize_reservation = true;
	parent_td = $(this).closest('td');
	selected_reservation_id = parent_td.attr('data-reservation-id');
	start_col_index = parent_td.parent().children().index(parent_td);
	var top_element = $("td[data-reservation-id='" + selected_reservation_id + "']").first();
	start_row_index = get_row_inx(top_element);
    });

    resizer.on('touchstart', function(e) {
	dragging_to_resize_reservation = true;
	dragging_to_resize_reservation = true;
	parent_td = $(this).closest('td');
	selected_reservation_id = parent_td.attr('data-reservation-id');
	start_col_index = parent_td.parent().children().index(parent_td);
	var top_element = $("td[data-reservation-id='" + selected_reservation_id + "']").first();
	start_row_index = get_row_inx(top_element);
    });
    return resizer;
}

function clear_page() {
    /** reset all elements **/
    var existing_reservations = $('td.schedule_reservation_occupied_slot_end, td.schedule_reservation_occupied_slot_start, td.schedule_reservation_occupied_slot_middle');
    existing_reservations.attr('class', 'schedule_reservation_available_slot');
    existing_reservations.removeAttr('data-reservation-id');
    $('td.schedule_reservation_available_slot > div.text-div').not(":contains('---')").html('---');
    $('div.resize-div').remove();
}

function clear_highlighted() {
    if (typeof start_row_index !== typeof undefined) {
	$(table.rows[start_row_index].cells[start_col_index]).children('div.text-div').html('---').css('font-weight','');
    }
    var highlighted = $('td.highlighted');
    highlighted.first().children('div.text-div').html('---');
    highlighted.removeClass('highlighted');
    $('input#note').val('');
}

function fill_rows_with_reservation(column_index, start_row_index, end_row_index, reservation_id, account_name) {
    /** account name is only expected when called by get_reservations but not handle_dragging_to_ functions **/

    // reset elements
    var reservation_cells = $("td[data-reservation-id='" + reservation_id + "']");
    reservation_cells.first().removeAttr('title').children('div.text-div').html('---'); // start_slot html, removes title and text
    reservation_cells.eq(1).children('div.text-div').html('---'); // time-range html, if out of range nothing happens
    reservation_cells.attr('class', 'schedule_reservation_available_slot')
	.removeAttr('data-reservation-id');

    // create reservation with new top/bottom
    var top_cell = $(table.rows[start_row_index].cells[column_index]);
    top_cell.attr('class', 'schedule_reservation_occupied_slot_start')
	.attr('data-reservation-id', reservation_id)
	.children('div.text-div').html(get_time_range(start_row_index, end_row_index));

    var length = end_row_index - start_row_index;
    if (length==0) {
	top_cell.addClass('schedule_reservation_occupied_slot_end');
    } else {
	var end_cell = $(table.rows[end_row_index].cells[column_index]);
	end_cell.attr('class', 'schedule_reservation_occupied_slot_end');
	end_cell.attr('data-reservation-id', reservation_id);

	/** Given a start and end index will fill all rows in
	    between with appropriate class and attributes **/
	for (i=start_row_index+1; i< end_row_index; i++) {
	    var cell = $(table.rows[i].cells[column_index]);
	    cell.attr('data-reservation-id', reservation_id);
	    cell.attr('class', 'schedule_reservation_occupied_slot_middle');
	}
    }

    // Add account_name and time (only called with get_reservation)
    if (account_name) {
	if (length==0) {
	    html = get_time_range(start_row_index, end_row_index) + " " + account_name;
	} else {
	    html = account_name;
	    var time_display_cell = $(table.rows[start_row_index+1].cells[column_index]);
	    time_display_cell.addClass('schedule_reservation_occupied_slot_time_range')
		.children('div.text-div').html(get_time_range(start_row_index, end_row_index));
	}
	top_cell.attr('title', account_name)
	    .children('div.text-div').html(html);

    }
}

function populate_page_with_reservations() {
    /** don't run if user is dragging **/
    if (dragging_to_create_reservation || dragging_to_resize_reservation || dragging_to_shift_reservation) {
	setTimeout(function() {populate_page_with_reservations();}, 1000);
	return;
    }
    /** don't run if user has a dialog open **/
    if (($('#dialog').hasClass('ui-dialog-content') && $('#dialog').dialog('isOpen'))
	||($('#note_dialog').hasClass('ui-dialog-content') && $('#note_dialog').dialog('isOpen'))) {
	setTimeout(function() {populate_page_with_reservations();}, 1000);
	return;
    }
    /** Handles the case of a changed or new reservation **/
    if (reservation_data === undefined) {
	setTimeout(function() {populate_page_with_reservations();}, 1000);
	return;
    }
    clear_page();

    /** populate appropriate cells **/
    data = eval(reservation_data);
    for (var reservation_id in data) {
	r_dict = eval(data[reservation_id]);
	var start = $("td[data-time-start='" + r_dict['start_ts'] + "'][data-room-id='" + r_dict['room_id'] + "']");
	var end = $("td[data-time-end='" + r_dict['end_ts'] + "'][data-room-id='" + r_dict['room_id'] + "']");
	start_row = get_row_inx(start);
	end_row = get_row_inx(end);
	fill_rows_with_reservation(get_col_inx(start), start_row, end_row, reservation_id, r_dict['account_name']);
    }

    // add resize div
    $('td.schedule_reservation_occupied_slot_end').append(generate_resize_div());
}


function get_row_inx(el) {
    /** pass a jquery object representing a td, get the row index **/
    return el.parent().parent().children().index($(el).parent());
}

function get_col_inx(el) {
    /** pass a jquery object representing a td, get the column index **/
    return el.parent().children().index(el);
}

function get(name){
    /** parses the value for a given request.GET argument **/
    if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
	return decodeURIComponent(name[1]);
}

function get_highlighted_time_slots() {
    /** called on form submit, populates the hidden field with start | end | room_id **/
    if ($('.highlighted').length == 0) {
	display_message('Please select a start time for your new reservation!');
  	return false;
    }
    /** extract start, end, room id **/
    var start_time = $('.highlighted:first').attr('data-time-start');
    var end_time = $('.highlighted:last').attr('data-time-end');
    var room_id = $('.highlighted:first').attr('data-room-id');

    /**  populate hidden field with | separated values **/
    $('#user_input').val( start_time + ' | ' + end_time + ' | ' + room_id);
}

function set_date_links() {
    /** Sets the link for the >> and << symbols
	next to the current date **/
    var base_url = $('div#link_helper').text();
    $('div#link_helper').remove();
    var get_date = get('date');
    $('#dp').val(get_date); // set default
    var d;
    if (get_date === undefined) {
	d = new Date();
    } else {
	d = new Date(get('date'));
    }
    d.setDate(d.getDate() - 1);
    $('a#prev_date').attr('href',base_url + '&date=' + format_date(d)).text('<<');
    d.setDate(d.getDate() + 2);
    $('a#next_date').attr('href',base_url + '&date=' + format_date(d)).text('>>');
}

function format_date(d) {
    // d is a date object
    // returns string in format YYYY-MM-DD
    var month = ('0' + (d.getUTCMonth() + 1)).slice(-2);
    var date = ('0' + d.getUTCDate()).slice(-2);
    var year = d.getUTCFullYear();
    var date_string = year + '-' + month + '-' + date;
    return date_string;
}

function set_outside_table_click_event() {
    $('table#room_reservation_table').bind('clickoutside', function() {
	if ($("#dialog").hasClass('ui-dialog-content')) {
	    if (!$('#dialog').dialog('isOpen')) {
		clear_highlighted();
	    }
	}
    });
}

function jump_to_date() {
    /** redirects user to the selected date on the JQuery UI Calendar **/
    var re = /^\d{4}-\d{2}-\d{2}$/;
    var date = $('#dp').val();
    if (re.test(date)) {
	data = {'floor_id':get('floor_id'), 'room_type':get('room_type'), 'date':date };
	window.location.href = $SCRIPT_ROOT + '/reserve_room?' + encodeQueryData(data);
    }
}

function go_to_legacy() {
    data = {'floor_id':get('floor_id'), 'room_type':get('room_type') };
    date = get('date');
    if (date!==undefined)
	data['date'] = date;
    window.location.href = "https://operations.microoffice.com/1g/meet/show_schedule.cgi?" + encodeQueryData(data);
}

function encodeQueryData(data) {
    /** converts arguments into GET string **/
    var ret = [];
    for (var d in data) {
	ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    }
    return ret.join('&');
}


/** mousemove functions **/
function get_nearest_valid_touch_element(e) {
    var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
    var hover_element = $(document.elementFromPoint(touch.clientX, touch.clientY));
    if (!hover_element) return; //handle null case
    hover_element = hover_element.closest('td');
    return hover_element;

}
function handle_dragging_to_create_reservation(e) {
    if (typeof(e.originalEvent.touches) == 'undefined') {
	var hover_element = $(e.target).closest('td');

    } else {
	var hover_element = get_nearest_valid_touch_element(e);
	if (!hover_element) return;
    }
    var hover_row_index = get_row_inx(hover_element);
    // check if mouse have moved to a different spot. Skips a lot of event triggers
    if (last_hover_row_index == hover_row_index) return;
    last_hover_row_index = hover_row_index;
    if (hover_element.length != 1) return clear_highlighted();
    if (hover_row_index < start_row_index) return clear_highlighted();
    $('.highlighted').removeClass('highlighted');
    // highlight all rows between start and current (where start is the first selected)
    for (i=start_row_index; i<= hover_row_index; i++) {
	cell = $(table.rows[i].cells[start_col_index]);
 	/** prevent overlapping reservation **/
	if (cell.hasClass('schedule_reservation_occupied_slot_start')) {
	    hover_row_index = i-1;
	    break;
	}
	$(cell).addClass('highlighted');
    }
    var time_range = get_time_range(start_row_index, hover_row_index);
    $('td.highlighted:first').children('div.text-div').html(time_range).css('font-weight', 'bold');
}

function handle_dragging_to_resize_reservation(e) {
    if (typeof(e.originalEvent.touches) == 'undefined') {
	var hover_element = $(e.target).closest('td');
    } else {
	var hover_element = get_nearest_valid_touch_element(e);
	if (!hover_element) return;
    }
    var hover_row_index = get_row_inx(hover_element);

    // check if mouse have moved to a different spot. Skips a lot of event triggers
    if (last_hover_row_index == hover_row_index) return;
    last_hover_row_index = hover_row_index;
    // check if hovering above the start
    var valid_dragging_range = hover_row_index >= start_row_index;

    // forego any calculations if it's already false
    if (valid_dragging_range) {
	for (i=start_row_index; i<=hover_row_index; i++) {
	    var cell_attr = $(table.rows[i].cells[start_col_index]).attr('data-reservation-id');
	    // don't let user drag into occupied slot
	    if (cell_attr != selected_reservation_id && cell_attr != undefined) {
		valid_dragging_range = false;
		break;
	    }
	}
    }
    if (valid_dragging_range) {
	if (!prevent_taphold && has_reservation_changed()) {
	    prevent_taphold = true;
	}
	fill_rows_with_reservation(start_col_index, start_row_index, hover_row_index, selected_reservation_id);
    }
}

function handle_dragging_to_shift_reservation(e) {
    if (typeof(e.originalEvent.touches) == 'undefined') {
	var hover_element = $(e.target).closest('td');
    } else {
	var hover_element = get_nearest_valid_touch_element(e);
	if (!hover_element) return; // handle null case
    }
    var hover_row_index = get_row_inx(hover_element);
    var hover_col_index = get_col_inx(hover_element);

    // check if mouse have moved to a different spot. Skips a lot of event triggers
    if (last_hover_row_index == hover_row_index && last_hover_col_index==hover_col_index) return;
    last_hover_row_index = hover_row_index;
    last_hover_col_index = hover_col_index;
    // valid columns are positive and even
    if (hover_col_index < 2 || (hover_col_index % 2) == 1 ) {
	hover_col_index = start_col_index;
    }
    start_col_index = hover_col_index; // ensure that it doesn't "jump" back over
    /** Make sure we don't go out of bounds of the table **/
    var new_top_index = hover_row_index - rows_above_selected;
    var new_bot_index = hover_row_index + rows_below_selected;
    var valid_dragging_range = new_top_index > 2 && new_bot_index <= total_rows + 1;
    if (valid_dragging_range) {
	for (var i=new_top_index; i<=new_bot_index; i++) {
	    var cell_attr = $(table.rows[i].cells[hover_col_index]).attr('data-reservation-id');
	    if (cell_attr != selected_reservation_id && cell_attr != undefined) {
		valid_dragging_range = false;
		break;
	    }
	}
    }
    if (valid_dragging_range) {
	if (!prevent_taphold && has_reservation_changed()) {
	    prevent_taphold = true;
	}
	fill_rows_with_reservation(hover_col_index, new_top_index, new_bot_index, selected_reservation_id);
    }
}

function handle_mousedown(e) {
    target = $(e.target).closest('td');
    if(typeof(e.pageX)!='undefined')
	e.preventDefault(); // preventDefault breaks touch propagation (i.e. touchstart will fire and then prevent the click after touchend)
    if (target.hasClass('schedule_reservation_available_slot')) {
	dragging_to_create_reservation = true;
	target.addClass( 'highlighted' );
	start_col_index = get_col_inx(target);
	start_row_index = get_row_inx(target);
    } else if (target.is('.schedule_reservation_occupied_slot_end, .schedule_reservation_occupied_slot_start, .schedule_reservation_occupied_slot_middle')) {
	dragging_to_shift_reservation = true;
	selected_reservation_id = target.attr('data-reservation-id');
	$("td[data-reservation-id='" + selected_reservation_id + "'] > div.resize-div").css('background-color', 'transparent');
	start_col_index = get_col_inx(target);
	current_row_index = get_row_inx(target);

	var top_element = $("td[data-reservation-id='" + selected_reservation_id + "']").first();
	var bot_element = $("td[data-reservation-id='" + selected_reservation_id + "']").last();
	rows_above_selected = current_row_index - get_row_inx(top_element);
	rows_below_selected = get_row_inx(bot_element) - current_row_index;
    }
}

function handle_mouseup(e) {
    /** submit AJAX to update reservation **/
    prevent_taphold = false;
    if (dragging_to_resize_reservation || dragging_to_shift_reservation) {
	dragging_to_shift_reservation = false;
	dragging_to_resize_reservation = false;
	if (has_reservation_changed())
	    update_reservation(); // ajax function
	else
	    populate_page_with_reservations(); // refresh page with stale data (because nothing has been changed)
    } else if (dragging_to_create_reservation) {
	dragging_to_create_reservation = false;
	if ($('td.highlighted').length > 0) {
	    show_create_reservation_dialog(e);
	}
	else {
	    clear_highlighted();
	}
    }
    last_hover_row_index = -1;
    last_hover_col_index = -1;
}
/** end mousemove handling functions **/

function has_reservation_changed() {
    /** Checks if the reservation has been changed (start, end, or room)
	Used to prevent redundant ajax calls **/
    var start_cell = $("td.schedule_reservation_occupied_slot_start[data-reservation-id='" + selected_reservation_id + "']");
    current_start = start_cell.attr('data-time-start');
    current_end = $("td.schedule_reservation_occupied_slot_end[data-reservation-id='" + selected_reservation_id + "']").attr('data-time-end');
    current_room = start_cell.attr('data-room-id');

    original_start = reservation_data[selected_reservation_id]['start_ts'];
    original_end = reservation_data[selected_reservation_id]['end_ts'];
    original_room = reservation_data[selected_reservation_id]['room_id'];
    return ! ( current_start== original_start &&
	       current_end == original_end &&
	       current_room == original_room);
}


function get_time_range(start_row_index, end_row_index) {
    if (start_row_index == end_row_index) {
	var x = $(table.rows[start_row_index].cells[0]).html().substring(0,5);
	if (x.charAt(0) == '0')
	    x = x.substring(1,5);
	return x;
    }
    /** cells[0] refers to the time-slot td (9:00 AM, 9:15 AM ...)
	The start time is simply the HTML but the end time is saved
	as an HTML data-attribute. **/
    start_time = $(table.rows[start_row_index].cells[0]).html();
    end_time = $(table.rows[end_row_index].cells[0]).attr('data-end-time');
    return start_time + ' - ' + end_time;
}

function toggle_rows() {
    /** Hides 12 AM - 8:45 AM and 5 PM to 12 PM **/
    var min = get_row_inx($("td.schedule_time:first"));
    var upper_lim = get_row_inx($("td.schedule_time[data-end-time='09:15 AM']"));
    var lower_lim = get_row_inx($("td.schedule_time[data-end-time='05:15 PM']"));
    $('#room_reservation_table tr').slice(min, upper_lim).toggle();
    $('#room_reservation_table tr').slice(lower_lim).toggle();
}

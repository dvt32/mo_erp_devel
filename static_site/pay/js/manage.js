$(function () {

var cardAddOptions = {
  errorClass: "form-control",
  highlight: function(element, errorClass) { highlightPayFn(element, errorClass) },
  unhighlight: function(element, errorClass) { unhighlightPayFn(element, errorClass) },
  errorPlacement: function(error, element) { errorPayPlace(error, element) },
  rules: card_rules($("form#form-card-add"))
};

var cardEditOptions = {
  errorClass: "form-control",
  highlight: function(element, errorClass) { highlightPayFn(element, errorClass) },
  unhighlight: function(element, errorClass) { unhighlightPayFn(element, errorClass) },
  errorPlacement: function(error, element) { errorPayPlace(error, element) },
  rules: card_rules($("form#form-card-edit"))
};

var achAddOptions = {
  errorClass: "form-control",
  highlight: function(element, errorClass) { highlightPayFn(element, errorClass) },
  unhighlight: function(element, errorClass) { unhighlightPayFn(element, errorClass) },
  errorPlacement: function(error, element) { errorPayPlace(error, element) },
  rules: ach_rules($("form#form-ach-add"))
};

var achEditOptions = {
  errorClass: "form-control",
  highlight: function(element, errorClass) { highlightPayFn(element, errorClass) },
  unhighlight: function(element, errorClass) { unhighlightPayFn(element, errorClass) },
  errorPlacement: function(error, element) { errorPayPlace(error, element) },
  rules: ach_rules($("form#form-ach-edit"))
};

$("form#form-card-add").validate(cardAddOptions);
$("form#form-card-edit").validate(cardEditOptions);
$("form#form-ach-add").validate(achAddOptions);
$("form#form-ach-edit").validate(achEditOptions);

$("button.ach-confirm").click(function() {
    var el = this;
    var container = $(el).parent().parent().parent();
    var ID = $(this).attr("data-id");
    link = "/check_confirmed_ach?pmt_vault_id=" + ID;
    $.ajax({
      url: link,
      type: "POST",
      success: function (response) {
        response = JSON.parse(response);
        if (response == "confirmed") {
          $("nav").after("<h4>This account is already confirmed! Try refreshing the page.</h4>");
        } else if (response == "unconfirmed") {
          var confirmModal = "<div class='modal fade confirm' id='ach-confirm-modal' tabindex='-1' role='dialog' aria-labelledby='ach-confirm-label' aria-hidden='true'>" +
            "<div class='modal-dialog'>" + 
              "<div class='modal-content'>" +
                "<div class='modal-header'>" +
                "<button type='button' class='close edit' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + 
                    "Confirm Bank Account" +
                "</div>" +
                "<div class='modal-body'>" +
                "<p>To confirm your Bank Account, we'll desposit two small amounts (between $0.01 and $0.99) into your account. These may take 3-5 days to show up. Once you see these deposits, come back to this page, click on the Confirm button, and enter in the two amounts. Your bank account will then be confirmed.</p>" +
                  "<button type='button' id='confirm-ach-btn' class='btn btn-default'>Send Confirmation Deposits</button>" +
                "</div>" +    
              "</div>" +
            "</div>" +
          "</div>";
          confirmModal = $(confirmModal);
          $("body").append(confirmModal);
          $("#confirm-ach-btn").click(function () {
            confirmACH(el);
            $(this).before("<span class='glyphicon glyphicon-refresh loading'></span></br>");
            $("#confirm-ach-btn").unbind('click');
          });
          $(document).on('hidden.bs.modal', '#ach-confirm-modal', function () {
            $(this).remove();
          });
          confirmModal.modal("show");
        } else if (response == "pending") {
          $form = $("#form-pending");
          $form.find("input[name=pmt_vault_id]").val(ID);
          $form.find("label[for=pending-routing_number]").html("Routing Number: " + $(el).attr("data-routing"))
          $form.find("label[for=pending-account_number]").html("Account Number: " + $(el).attr("data-account"))
          $form.find("label[for=pending-registered_name]").html("Registered Name: " + $(el).attr("data-name"))
          $("#ach-pending-modal").modal("show");
        } else {
          $("nav").after("<h4>" + reponse + "</h4>");
        }
      }
    });
  });

  function confirmACH(el) {
    var id = $(el).attr("data-id");
    var link = "/send_confirm_ach?pmt_vault_id=" + id;
    $.ajax({
      url: link,
      type: "POST",
      success: function (response) {
        location.reload(true);
      }
    });
  }


$("button#prioritize").click(function () {
    var cards = $("#manage-card-pmt div[id^=card-]").not("[id$=display]");
    var achs = $("#manage-ach-pmt div[id^=ach-]").not("[id$=display]");
    var pmts = $.merge(cards, achs);
    var body = "";
    $.each(pmts, function (i, pmt) {
      var el = $(pmt).clone(false);
      var btnID = el.find("button[id^=card-edit-], button[id^=ach-edit-]").attr("id");
      pmtID = btnID.replace(/.*-edit-/, "");
      el.attr("id", "pmt_" + pmtID);
      el.removeClass('just-added');
      el.find("div[id$=display]").remove();
      el.find("table").unwrap();
      el.find("span").replaceWith("<span class='glyphicon glyphicon-resize-vertical arrow'></span>");
      var id = el.attr("id");
      el = el[0].outerHTML;
      body += el;
    })
    var modal = 
        "<div class='modal fade' id='prioritize_modal' data-backdrop='static' data-keyboard='false' tabindex='-1' role='dialog' aria-labelledby='prioritize_modal_label'>" + 
          "<div class='modal-dialog' role='document'>" +
            "<div class='modal-content'>" +
              "<div class='modal-header'>" +
              "<button type='button' class='btn btn-default save' id='save-order' aria-label='Close'>Save</button>" +
                "<button type='button' class='close edit' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
                "<h4 class='modal-title' id='prioritize_modal_label'>" +
                "Prioritize Payment Methods" +
              "</h4>" +
              "</div>" +
              "<div class='modal-body' id='reorder'>" +
              "<div> Drag and drop payment methods in the order you would like them to be used and displayed </div>" +
              body + 
              "</div>" +
            "</div>" +
          "</div>" +
        "</div>";
        modal = $(modal);
        $("body").append(modal);
        $("#reorder").sortable({
          cursor: "move", 
          items: "> .list-group.manage"
        });
        $("#save-order").click(function () {
          var data = $("#reorder").sortable("toArray");
          var hash = "";
          $.each(data, function (i, datum) {
            hash += datum;
            hash += "=" + (i + 1);
            if (i < data.length-1) {
              hash += "&"
            }
          });
          $.ajax({
            type: "POST",
            url: "/new_order",
            data: hash,
            success: function(response) {
              response = JSON.parse(response);
              if (response == "reordered") {
                location.reload(true);
              } else if (response == "invalid") {
                $("#reorder").prepend("<h3 style='color: red;'>You are not autorized to reorder that payment method</h3>")
              }
            },
            error: function(response) {
              console.log(response);
            }
          })
        });
        modal.modal('show');
  });

  $(document).on('hidden.bs.modal', '#prioritize_modal', function () {
    $(this).remove();
  });

});
$(function () {
  // Load validators first!
  // Custom validator methods are in validators.js
  
  function recur_options($form) {
    crules = card_rules($form);
    arules = ach_rules($form)
    rrules = recur_rules($form);
    rules = $.extend(crules, arules, rrules);
    var recurOptions = {
      errorClass: "form-control",
      highlight: function(element, errorClass) { highlightPayFn(element, errorClass) },
      unhighlight: function(element, errorClass) { unhighlightPayFn(element, errorClass) },
      errorPlacement: function(error, element) { errorPayPlace(error, element) },
      submitHandler: function (form) {
        $('<input />').attr('type', 'hidden')
            .attr('name', "pmt_vault_id")
            .attr('value', $(form).find("[id*=pmt_vault_id]").val())
            .appendTo(form);
        form.submit();
      },
      rules: rules 
    };
    return recurOptions;
  }
  

  

  $("form#form-recur").validate(recur_options($("form#form-recur")));
  $("form#form-recur-edit").validate(recur_options($("form#form-recur-edit")));

});
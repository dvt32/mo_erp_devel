/*
 *
 * Global JavaScript for Microoffice Payment Portal
 * 2016
 *
 * Jacob Rienstra
 *
 */



$(function () {
	
/******
	ALL PAGES
*****/
var pmts;
$.ajax({
	url: "/saved_pmts",
	type: "POST",
	success: function (response) {
		response = JSON.parse(response);
		if (typeof(response) == "object") {
			pmts = response;
		}
		setDropDowns();
		return;
	}
});

	// Control the height of the navbar; make sticky at top
	var nav = $("nav.navbar");
  var ps = $("nav.navbar").next();
  var navClass = "stuck";
  var avt = $('nav.navbar').height() + 54;
    
  $(window).scroll(function() {
  	navChange(this);
  });

  function navChange(window) {
  	var side = $("#navbar-main.navmenu");
  	var barHeight = $("nav.navbar").position().top - $(window).scrollTop() + 54;
    if( $(this).scrollTop() >= avt) {
      nav.addClass(navClass);
      ps.css('margin-top', '84px');
      side.css('top', '54px');
    } else {
      nav.removeClass(navClass);
      ps.css('margin-top', '0px');
      side.css('top', barHeight)
    }
  }
  // Changes navbar class based on screen width to allow mobile dropdown menu
	// media query event handler
	if (matchMedia) {

		// media query change
		function WidthChange(mq) {
		  wc();
		}

		function wc () {
			if (mq.matches) {
		    $("#navbar-main").attr("class", "navbar navbar-default");
		    $("#navbar-main").find("a.dropdown-toggle").attr('aria-expanded', false);
		    $("#navbar-main").find("li.dropdown").removeClass('open');
		    $("#navbar-main").removeAttr('style');
		  } else {
		    $("#navbar-main").attr("class", "navmenu navmenu-default navmenu-fixed-left offcanvas");
		    $("#navbar-main").find("a.dropdown-toggle").attr('aria-expanded', true);
		    //$("#navbar-main").find("li.dropdown").addClass('open');
		  }
		}

		var mq = window.matchMedia("all and (min-width: 768px)");
		wc();
		mq.addListener(WidthChange);
		WidthChange(mq);

	}

	// Logging utitilites
	$("form#login").submit(function () {
		$.ajax({
			url: '/instrument/login_attempt/' + $("#email").val(),
			type: "GET"
		});
	});

	// Links the actual input element to the visible checkbox when the 
	// checked property is changed, animates color changes.
	// Visible box/checkmark is AFTER input element in DOM
	$("input[type=radio], input[type=checkbox]").each(function () {
		$(this).change(function () {
			// console.log($(this).val(), $(this).prop("checked")); // (checking what's going on)
			var $button = $(this).next("button.btn-check");
			var $checkmark = $button.find("span.checkmark");
			var $label = $button.next("label");
			if ($(this).prop("checked")) {
				$checkmark.animate({
	    		opacity: 0,
	  		}, 200);
	  		$button.animate({
	  			backgroundColor: "#fff", 
	  			borderColor: "#4678b9"
	  		}, 200);
	  		$label.animate({
	  			backgroundColor: "#edf2f8"
	  		}, 200);
			}
			else if (!$(this).prop("checked")) {
				$checkmark.animate({
	    		opacity: 1,
	  		}, 200);
	  		$button.animate({
	  			backgroundColor: "#8cd98c",
	  			borderColor: "#53923a"
	  		}, 200);
	  		$label.animate({
	  			backgroundColor: "#66cc66"
	  		}, 200);
			}
		});
	});

	// Links visible button checkbox element to change of actual checkbox input element
	$("button.btn-check").each(function () {
		$(this).click(function () {
			var $box = $(this).prev("input[type=checkbox]");
			$box.trigger("change");
			$box.prop("checked", !$box.prop("checked"));
		});
	});

	// On selecting button, change the selector to the "New Payment Method,"
	// which is always the last element. Uses relative elements, so should work for all
	// forms on the page. 
	// ddslick itself has a handler to reset the forms info
	$("button.btn-check.radio[id*=pmt_method]").each(function () {
		$(this).click(function () {
			var $form = $(this).closest("form");
			var $pmts = $form.find("[id*=pmt_vault_id]");
			var i = $pmts.find("ul").find("li").length-1;
			$pmts.ddslick('select', {index: i});
			// Though selection would usually take care of clearing,
			// if won't if the customer has filled out any of:
			// card number, cvv, account number, routing number
			// so we do it manually here, just in case.
			resetPmtForm($form);
		 	$form.validate().resetForm();
			radioClickHandler(this);
		});
	});

	// Changes the input elements and visible elements on radio selection
	function radioClickHandler(el) {
		// actual input element
		var $box = $(el).prev("input[type=radio]");
		// if not checked
		if (!$box.prop("checked")) {
			// all real inputs
			var $inputs = $(el).parent().find("input[type=radio]");
			// trigger a change in all inputs, including $box
			$inputs.each(function () { $(this).trigger("change"); });
			// set proper checked property for clicked input
			$box.prop("checked", !$box.prop("checked"));
			// sets the parent element (the select) to the proper value, card or ach;
			// this is what is actually passed in
			$box.parent().val($box.val());
			// triggers the change, which calls changeFormType
			$box.parent().trigger("change");
			// resets form
			$box.closest("form").validate().resetForm();
		}
	}

	// on pmt_method value change, change the form type accordingly
	$("div[id*=pmt_method]").change(function () {
				changeFormType(this);
			});

	// Utility function, only hides and shows proper fields. does not reset data or validation checks
	// also does not change radio values or appearance
	function changeFormType(el) {
		if ($(el).val() == "card") {
			$(el).closest("form").find(".ach-input").addClass("formhide");
			$(el).closest("form").find(".ach-input").find("input").val('');
			$(el).closest("form").find(".card-input").each(function () { $(this).removeClass("formhide")});
			$(el).closest("form").find("label[for=amount]").html("Amount*");
		} else if ($(el).val() == "ach") {
			$(el).closest("form").find(".card-input").each(function () { $(this).addClass("formhide")});
			$(el).closest("form").find(".card-input").find("input").val('');
			$(el).closest("form").find(".ach-input").removeClass("formhide");	
			$(el).closest("form").find("label[for=amount]").html("Amount");
		}
	}

	// Removes "checked" attribute for radios 
	// then sets it properly based on the pmt_method value passed in from server
	$("div[id*=pmt_method]").each(function() {
		$(this).find("input[type=radio]").each( function () { $(this).removeAttr("checked")});
		var selector = 'input[value=' + $(this).attr("value") + ']';
	  $(this).find(selector).prop("checked", true);
	  $(this).find(selector).attr("checked", true);
		$(this).val($(this).find("input[type=radio][checked=checked]").val());
	});
	

	// For each dropdown, sets the proper data attributes using the JSON object from the server
	// Then sets up ddslick dropdown on it
	function setDropDowns() {
		$("select[name=pmt_vault_id]").each(function () {
			//while (done == 0) {};
			var $els = $(this).find("option");
			for (var i = 0; i < $els.length; i++) {
				if ($els[i].value == '') {
					$($els[i]).attr("data-description", "---")
					$($els[i]).attr("data-imageSrc", "{{static_base_url}}/pay/img/creditcheck.png")
				} else if (typeof(pmts) !== 'undefined') {
					var result = $.grep(pmts, function(e){ return $els[i].value == e.id });
					if (result.length != 1) {
					  alert("There was an error retrieving this Payment Method");
					} else if (result.length == 1) {
						var pmt = result[0];
						var description = "";
						var imgSrc = "";
						if (pmt.pmt_method == "card") {
							description = pmt.billing_first_name + " " + pmt.billing_last_name;
							imgSrc = "{{static_base_url}}/pay/img/" + credit_type(pmt.card_number) + ".png";
						} else if (pmt.pmt_method == "ach") {
							imgSrc = "{{static_base_url}}/pay/img/check2.png"
							if (pmt.bank != "") {
								description = pmt.registered_name + " @ " + pmt.bank;
							} else {
								description = pmt.registered_name
							}
						}
						$($els[i]).attr("data-description", description);
						$($els[i]).attr("data-imageSrc", imgSrc);
					}
				}
			}
			$(this).ddslick({
		    width: 400,
		    defaultSelectedIndex: 0,
		    imagePosition:"left",
		    onSelected: function(data, obj) {
		    	var $form = $(obj).closest("form");
					var id = data.selectedData.value;
					$form.find("div[id*=pmt_vault_id]").val(id);
		    	if (id != '') { // saved payment method
		 				fillPmtForm(id, $form);
		 			} else if (id == '') { // new payment method
		 				/* This is an unfortunately needed error check.
		 				 * If the server returns on an error, with the form full of info,
		 				 * and the customer entered a new payment method (id = '') 
		 				 * ddslick will treat the page load as selecting that, 
		 				 * and clear the form. This is obviously not good.
		 				 * So, we only clear the form when both the card number field,
		 				 * cvv field, account number and routing number are all
		 				 * Either empty or have non digit characters (*'s)
		 				 * This should only happen when switching from a saved payment method 
		 				 * being filled out to a new payment method.
		 				 * Unfortunately, this prevents resetting upon selection 
		 				 * of "card" or "ach" while a saved payment is selected, 
		 				 * so that is above, in the click listener for those.
		 				 * This fails to be a sufficient check if, say, absence of any
		 				 * of those fields IS the error, but that really shouldn't happen.
		 				 */
		 				var cn = $form.find("[name=card_number]").val();
		 				var cvv = $form.find("[name=cvv]").val();
		 				var acc = $form.find("[name=account_number]").val();
		 				if (!$.isNumeric(cn) && !$.isNumeric(cvv) && !$.isNumeric(acc)) {
		 					resetPmtForm($form);
		 					$form.validate().resetForm();
		 				}	
		 			}
		    }     
			});
			if (typeof(pmts) == 'undefined') {
				$("[id=pmt_vault_id]").addClass('formhide');
			}
		});
	}

	// resets given form to default state
	function resetPmtForm ($form) {
		$form.find("input, select").not($(".dd-option-value, [name=csrf_token], [name=txn_id], [name=txn_type], .dd-selected-value, [name=pmt_vault_id], [type=radio], [type=button], [name=billing_country], [name=expire_month], [name=expire_month], [name=billing_state], [name=owned_by], [name=amount], [name=scheduled_date], [name=note], [name=save_pmt_method], [name=reminder], [name=charge_today], .btn")).each( function () { $(this).val('')});
			$form.find("select[name=expire_month]").val(1);
			$form.find("select[name=billing_country]").val('US');
			$form.find("select[name=account_type]").val('checking');
			$form.find("select[name=owned_by]").val('business');
			changeSt($form);
			$form.find("input[name=card_number]").attr("readonly", false);
			$form.find("input[name=card_number]").off('click');
			$form.find("input[name=account_number]").attr("readonly", false);
			$form.find("input[name=routing_number]").attr("readonly", false);
			$form.find("input[name=account_number]").off('click');
			$form.find("input[name=routing_number]").off('click');
	}

	// fills a pmt type form with the information from pmt_vauld_id = id
	// resets form, then changes the type as appropriate, then fills the fields
	function fillPmtForm(id, $form) {
		var result = $.grep(pmts, function(e){ return e.id == id; });
		if (result.length == 0) {
  		alert("There was an error filling in this card's information");
		} else if (result.length == 1) {
			// resetPmtForm($form);
			if (result[0].pmt_method == "card") {
				radioClickHandler($form.find("input[value=card]").next("button"));
				fillCardForm($form, result[0]);
			} else if (result[0].pmt_method == "ach") {
				radioClickHandler($form.find("input[value=ach]").next('button'));
				fillACHForm($form, result[0]);
			}
			// validate the non-filled in fields
	 		$form.find('input[id^=pmt-]').not($('input[name=amount], input[name=scheduled_date], input[name=save_pmt_method], input.btn')).each(function() { $(this).valid(); });
		} else {
		  alert("Something went wrong, and you have duplicate payment entries.")
		}
	}		

	function fillCardForm($form, card) {
	  var date = card.expire_date;
	  var matches = date.match(/\d{2,4}/g);
	  var year = matches[0];
		var month = parseInt(matches[1]);
	  $form.find("input[name=billing_first_name]").val(card.billing_first_name);
	  $form.find("input[name=billing_last_name]").val(card.billing_last_name);
	  $form.find("input[name=card_number]").val(card.card_number);
	  $form.find("input[name=card_number]").attr("readonly", true);
	  $form.find("input[name=card_number]").click(function () {alert("If you're trying to pay with a different card than the one selected, please select \'Enter New Card\' from the dropdown menu.")});
	  $form.find("select[name=expire_month]").val(month);
	  $form.find("input[name=expire_year]").val(year.slice(2,4));
	  $form.find("input[name=cvv]").val("****");
	  $form.find("input[name=billing_company]").val(card.billing_company);
	 	$form.find("input[name=billing_address_1]").val(card.billing_address_1);
	 	$form.find("input[name=billing_address_2]").val(card.billing_address_2);
	 	$form.find("select[name=billing_country]").val(card.billing_country);
	 	changeSt($form);
	 	if (card.billing_country != 'US') {
	 			$form.find("input[name=billing_state_nonus]").val(card.billing_state);
	 	} else if (card.billing_country == 'US') {
	 		$form.find("select[name=billing_state]").val(card.billing_state);
	 	}
	 	$form.find("input[name=billing_city]").val(card.billing_city);
	 	$form.find("input[name=billing_zip_code]").val(card.billing_zip_code);
	}		

	function fillACHForm($form, ach) {
	  $form.find("input[name=routing_number]").val(ach.routing_number);
	  $form.find("input[name=routing_number]").attr("readonly", true);
	  $form.find("input[name=account_number]").val(ach.account_number);
	  $form.find("input[name=account_number]").attr("readonly", true);
	  $form.find("input[name=account_number]").click(function () {alert("If you're trying to pay with a different ACH Account than the one selected, please select \'Enter New ACH Account\' from the dropdown menu.")});
	   $form.find("input[name=routing_number]").click(function () {alert("If you're trying to pay with a different ACH Account than the one selected, please select \'Enter New ACH Account\' from the dropdown menu.")});
	  $form.find("select[name=account_type]").val(ach.account_type);
	  $form.find("select[name=owned_by]").val(ach.owned_by);
	  $form.find("input[name=registered_name]").val(ach.registered_name);
	}

	function fillRecurForm($form, recur) {
		$form.find("[name=recur_id]").val(recur.id);
		$form.find("[name=day_of_month]").val(recur.day_of_month);
		$form.find("[name=day_of_month]").attr("readonly", true);
		/*if (recur.start_date !== null || recur.end_date !== null || recur.max_amount !== null) {
			$("#advanced-edit-btn").trigger('click');
		}*/
		/*$form.find("[name=start_date]").val(recur.start_date);
		$form.find("[name=end_date]").val(recur.end_date);*/
		if (recur.max_amount != null) {
			$form.find("[name=max_amount]").val(recur.max_amount.replace('$', ''));
		}
		if (typeof(recur.reminder) == "undefined") {
			$form.find("[name=reminder]").trigger("change");
			$form.find("[name=reminder]").prop("checked", false);
		}
		$form.find("[name=remind_days_before]").val(recur.remind_days_before);
		/*if (recur.remind_days_before == null) {
			$("[name=reminder-btn]").trigger('click');
		}*/
		$form.find("[name=note]").val(recur.note);
	}

	// Controls toggler togglee classes for checkmark inputs
	$("input.toggler").change(function () {
      var targets = ".togglee[target=" + $(this).attr("target") + "]";
      $(this).closest("form").find(targets).each( function () {
        if ($(this).hasClass("formhide")) {
            $(this).removeClass("formhide");
          } else {
            $(this).addClass("formhide");
            $(this).find("input").val('');
          }
      });
    });

	// Controls toggler togglee classes for buttons
  $("button.toggler").click(function () {
	    if ($(this).attr("switch") != undefined) {
	      var temp = $(this).attr("switch");
	      $(this).attr("switch", $(this).html());
	      $(this).html(temp);
	    }
      var targets = ".togglee[target=" + $(this).attr("target") + "]";
      $(this).closest("form").find(targets).each( function () {
        if ($(this).hasClass("formhide")) {
            $(this).removeClass("formhide");
          } else {
            $(this).find("input").val('');
            $(this).addClass("formhide")
          }
      });
    });
  
  // function to change the state field visible
  function changeSt (form) {
		if ($(form).find("select[name=billing_country]").val() != 'US') {
			$($(form).find("select[name=billing_state]")).parent().addClass("formhide");
			$($(form).find("input[name=billing_state_nonus]")).parent().removeClass("formhide");
		} else if ($(form).find("select[name=billing_country]").val() == 'US') {
			$($(form).find("select[name=billing_state]")).parent().removeClass("formhide");
			$($(form).find("input[name=billing_state_nonus]")).parent().addClass("formhide")
		}

	};

	// set this up for each form with a billing country
	$("select[name=billing_country]").each(function () {
		$(this).change(function() {changeSt(this.closest("form"))});
	});


	Date.prototype.addMonths = function (m) {
    var d = new Date(this);
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
	}

	$("[name=day_of_month], [name=start_date], [name=end_date]").each(function () {
		$(this).change(function () {
				var $form = $(this).closest("form");
				var dom = $form.find("[name=day_of_month]").val();
				var sd = $form.find("[name=start_date]").val();
				var ed = $form.find("[name=end_date]").val();
				if (dom != '') {
					dom = parseInt(dom);
					var today = new Date();
					var first = new Date();
					first.setDate(dom);
					// if today's date is past or equal to the set day of the month, 
					// then the first payment will be next month on that date
					// else, it's this month
					if (today.getDate() >= dom) {
						first = first.addMonths(1);
					}
				}
				if (sd != '') {
					// parse returns the datetime at midnight of GMT. this ends up being a day earlier
					// when converted to EST. so we add six hours of milliseconds to counter that
					sd = Date.parse(sd) + (6 * 3600 * 1000);
					sd = new Date(sd);
					while (first < sd) {
						first = first.addMonths(1);
					}
				}
				second = first.addMonths(1);
				third = first.addMonths(2);
				if (ed != '') {
					ed = Date.parse(ed) + (6 * 3600 * 1000);
					ed = new Date(ed);
					if (second > ed) second = "";
				}
				var text = "*Note: The next time your card or bank account will be charged will be " + first.toLocaleDateString();
				if (second != "") {
					text += ", followed by " + second.toLocaleDateString() + " and then every month thereafter";
					if (ed != "") {
						text += " (up until " + ed.toLocaleDateString() + ")";
					}
				}
				text += "."
				if (today.getDate() == dom) {
					text += " If you wish to also be charged today, check the Charge Today button and enter an amount."
				} 
				$form.find("p.next-payments").html(text);
		});
	});

	$("[name=scheduled_date]").change(function () {
				var $form = $(this).closest("form");
				var sd = $(this).val();
				if (sd != '') {
					var date = new Date();
					// parse returns the datetime at midnight of GMT. this ends up being a day earlier
					// when converted to EST. so we add six hours of milliseconds to counter that
					sd = Date.parse(sd) + (6 * 3600 * 1000);
					sd = new Date(sd);
					date = date.addMonths(1);
					console.log(date);
					console.log(sd);
					if (sd > date) {
						text = "Warning: scheduled date is more than one month in the future."
						$form.find("p.next-payments").html(text);
					}
				}
				
				
		});






	function edit(el, type) {
		var ID = $(el).attr("data-id");
		// for upcoming -- nmi or vault
		var txn_type = $(el).attr("data-type")
		var link = "/edit_" + type
		if (type == "card" || type == "ach") {
			 link += "?pmt_vault_id=" + ID;
		} else if (type == "upcoming") {
			link += "?id=" + ID + "&txn_type=" + txn_type;
		} else {
			link += "?id=" + ID;
		}
		$.ajax({
			url: link,
			type: "GET",
			success: function (response) {
				response = JSON.parse(response);
				if (typeof(response) !== "object") {
					$("nav").after("<div class='flash-error'>" + response + "</div>");
					return;
				}
				if (type == "card") {
					var card = response;
					var $form = $("#form-card-edit");
					$form.find("[name=pmt_vault_id]").val(card.id)
				  $form.find("label[for=card_number-edit]").html("Credit Card Number: " + card.card_number);
				  fillCardForm($form, card);
					$("#modal-card-edit").modal('show');



				} else if (type == "ach") {
					var ach = response;
				  var $form = $("#form-ach-edit");
				  $form.find("[name=pmt_vault_id]").val(ach.id)
				  $form.find("label[for=routing_number-edit]").html("Routing Number: " + ach.routing_number);
				  $form.find("label[for=account_number-edit]").html("Account Number: " + ach.account_number);
					fillACHForm($form, ach);
					$("#modal-ach-edit").modal('show');



				} else if (type == "recur") {
					var recur = response;
					var $form = $("#form-recur-edit");
					$form.find("[id*=pmt_vault_id]").ddslick('select', {value: recur.pmt_vault_id});
					fillRecurForm($form, recur);
					$("#modal-recur-edit").modal('show');


				} else if (type == "upcoming") {
					var upcoming = response;
					var $form = $("#form-upcoming");
					$form.find("input[name=txn_id]").val(upcoming.id);
					$form.find("input[name=txn_type]").val(upcoming.txn_type);
					if (upcoming.txn_type == "vault") {
						$form.find("[id*=pmt_vault_id]").ddslick('select', {value: response.pmt_vault_id});
					} else if (upcoming.txn_type == "nmi") {
						$form.find("[id*=pmt_vault_id]").ddslick('select', {value: ''});
						if (result[0].pmt_method == "card") {
							radioClickHandler($form.find("input[value=card]").next("button"));
							fillCardForm($form, upcoming);
						} else if (result[0].pmt_method == "ach") {
							radioClickHandler($form.find("input[value=ach]").next('button'));
							fillACHForm($form, upcoming);
						}
					}
					$form.find("input[name=amount]").val(upcoming.amount.replace(/\$/g, ''));
					$form.find("input[name=scheduled_date]").val(upcoming.scheduled_date);
					$form.find("input[name=note]").val(upcoming.note);
					$("#modal-upcoming-edit").modal('show');
				}
			}
		});
	};

	$("button.btn-card-edit").click(function() {
		edit(this, "card");
	});

	$("button.btn-ach-edit").click(function() {
		edit(this, "ach");
	});

	$("button.btn-recur-edit").click(function () {
		edit(this, "recur")
	});

	$("button.btn-upcoming-edit").click(function () {
		edit(this, "upcoming")
	});

	$(document).on('hidden.bs.modal', '#modal-card-edit', function () {
		$(this).find("form")[0].reset();
	});

	$(document).on('hidden.bs.modal', '#modal-ach-edit', function () {
		$(this).find("form")[0].reset();
	});

	$(document).on('hidden.bs.modal', '#modal-recur-edit', function () {
		$(this).find("form")[0].reset();
	});

	$(document).on('hidden.bs.modal', '#modal-upcoming-edit', function () {
		$(this).find("form")[0].reset();
	});


	function deletePmt(el, type) {
		var id = $(el).attr("data-id");
		delModal = $("#modal-delete-confirm");
		delModal.modal("show");
		$("#btn-delete-confirm").click(function () {
			var link = ""
			if (type == "card" || type == "ach") {
				link = "/delete_" + type + "?pmt_vault_id=" + id;
			} else if (type == "upcoming") {
				var txn_type = $(el).attr("data-type")
				link = "/delete_" + type + "?" + type + "_id=" + id + "&txn_type=" + txn_type;
			} else {
				link = "/delete_" + type + "?" + type + "_id=" + id
			}
			$.ajax({
				url: link,
				type: "POST",
				success: function (response) {
					location.reload(true);
				}
			});
		});
	}

	$("button.btn-card-delete").click(function() {
		deletePmt(this, "card");
	});

	$("button.btn-ach-delete").click(function() {
		deletePmt(this, "ach");
	});

	$("button.btn-recur-delete").click(function() {
		deletePmt(this, "recur");
	});

	$("button.btn-upcoming-delete").click(function() {
		deletePmt(this, "upcoming");
	});

	$(".list-group.manage, .panel").on('show.bs.collapse', function () {
		$(this).find("span.showit").removeClass('glyphicon-chevron-down');
		$(this).find("span.showit").addClass('glyphicon-chevron-up');
	});

	$(".list-group.manage, .panel").on('hide.bs.collapse', function () {
		$(this).find("span.showit").removeClass('glyphicon-chevron-up');
		$(this).find("span.showit").addClass('glyphicon-chevron-down');
	});

	$("[id*=day_of_month]").on('input', function() {
		v = $(this).val();
		d = v.slice(v.length-1, v.length);
		switch(d) {
		case "":
    		$(this).parent().find(".suffix").html("  ");
    		break;
    case "1":
        $(this).parent().find(".suffix").html("st");
        break;
    case "2":
        $(this).parent().find(".suffix").html("nd");
        break;
    case "3":
        $(this).parent().find(".suffix").html("rd");
        break;
    default:
        $(this).parent().find(".suffix").html("th");
        break;
		}
	});


	function credit_type (number) {
		if (number[0] == "3") {
			return "amex";
		} else if (number[0] == "4") {
			return "visa";
		} else if (number[0] == "5") {
			return "mastercard";
		} else if (number[0] == "6") {
			return "discover";
		} else {
			return "credit";
		}
	}

	$("select[name=owned_by]").change(function () {
		if ($(this).val() == "business") {
			$("label[for=registered_name]").html("Business Name on Account");
		} else if ($(this).val() == "personal") {
			$("label[for=registered_name]").html("Personal Name on Account");
		}
	});

	/* Much more detailed card type checking to implement later
			{
        name: 'amex',
        pattern: /^3[47]/,
        valid_length: [15]
      }, {
        name: 'diners_club_carte_blanche',
        pattern: /^30[0-5]/,
        valid_length: [14]
      }, {
        name: 'diners_club_international',
        pattern: /^36/,
        valid_length: [14]
      }, {
        name: 'jcb',
        pattern: /^35(2[89]|[3-8][0-9])/,
        valid_length: [16]
      }, {
        name: 'laser',
        pattern: /^(6304|670[69]|6771)/,
        valid_length: [16, 17, 18, 19]
      }, {
        name: 'visa_electron',
        pattern: /^(4026|417500|4508|4844|491(3|7))/,
        valid_length: [16]
      }, {
        name: 'visa',
        pattern: /^4/,
        valid_length: [16]
      }, {
        name: 'mastercard',
        pattern: /^5[1-5]/,
        valid_length: [16]
      }, {
        name: 'maestro',
        pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
        valid_length: [12, 13, 14, 15, 16, 17, 18, 19]
      }, {
        name: 'discover',
        pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
        valid_length: [16]
      } */
	


});


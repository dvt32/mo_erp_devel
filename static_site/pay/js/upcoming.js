$(function () {
  // Load validators first!
  // Custom validator methods are in validators.js
    
    var $form = $("#form-upcoming");
    crules = card_rules($form);
    arules = ach_rules($form);
    prules = pmt_rules($form);
    rules = $.extend(crules, arules, prules);
    var onetimeOptions = {
      errorClass: "form-control",
      highlight: function(element, errorClass) { highlightPayFn(element, errorClass) },
      unhighlight: function(element, errorClass) { unhighlightPayFn(element, errorClass) },
      errorPlacement: function(error, element) { errorPayPlace(error, element) },
      submitHandler: function (form) {
        $('<input />').attr('type', 'hidden')
            .attr('name', "pmt_vault_id")
            .attr('value', $(form).find("[id*=pmt_vault_id]").val())
            .appendTo(form);
        form.submit();
      },
      rules: rules 
    };
  
  $("#form-upcoming").validate(onetimeOptions);

});
navigator.browserInfo = (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return { 'browser':'IE', 'version': (tem[1] || '')};
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return { 'browser': M[0], 'version': M[1] };
})();


$.validator.addMethod("cc", function(value, element) {
  return this.optional(element) || /^\d+\*+\d+$/.test(value) || (/^\d{15,16}$/.test(value) && luhn(value));
}, "Not a valid credit card number");

$.validator.addMethod("cvv", function(value, element) {
  return this.optional(element) || /^\d{3,4}$/.test(value) || (/^\*{3,4}$/.test(value));
}, "Not a valid cvv");

$.validator.addMethod("routing_valid", function(value, element) {
  return this.optional(element) || /^\d+\**\d+$/.test(value) || (/^\d{9}$/.test(value));
}, "Not a valid routing number");

$.validator.addMethod("zip_format", function(value, element) {
  return this.optional(element) || /^\d{5}(-\d{4})?$/.test(value);
}, "Not a valid zip code format");

$.validator.addMethod("date_format", function(value, element) {
  return this.optional(element) || /^\d{4}-\d{2}-\d{2}$/.test(value);
}, "For this browser, dates must be in yyyy-mm-dd format");

$.validator.addMethod("money", function(value, element) {
  return this.optional(element) || /^\d+(\.\d{2})?$/.test(value);
}, "Not a valid money amount format");

$.validator.addMethod("valid_expire", function(value, element) {
  var d = new Date();
  var year = d.getFullYear() % 1000;
  var month = d.getMonth() + 1;
  value = parseInt(value);
  var form_year = parseInt($(element).closest('form').find("input[name=expire_year]").val());
  var form_month = parseInt($(element).closest('form').find("select[name=expire_month]").val());
  if ((year * 12 + month) > (form_year * 12 + form_month)) {
    return false;
  } 
  else if ($(element).closest('form').find("input[name=expire_year]").val().length == 0) {
    return false;
  } else {return this.optional(element) || true}
}, "Please enter a date in the future");

$.validator.addMethod("isnt_today", function(value, element) {
  var d = new Date();
  var form_d = new Date(Date.parse(value));
  var d_format = d.toISOString().substr(0, 10)
  var form_d_format = form_d.toISOString().substr(0, 10)
  console.log(d_format, form_d_format);
  return d_format != form_d_format;
}, "You cannot edit an upcoming payment to today. Delete this payment then make a new one.");


$.validator.addMethod("after_today", function(value, element) {
  if (value == "") { return true; }
  var d = new Date();
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var day = d.getDate();
  var re = /\d{2,4}/g;
  var list = value.match(re);
  var form_year = parseInt(list[0]);
  var form_month = parseInt(list[1]);
  var form_day = parseInt(list[2]);
  if (list.length == 3) {
    if (form_year < year) {
      return false;
    } else if (form_year == year && form_month < month) {
      return false;
    } else if (form_year == year && form_month > month) {
      return true;
    } else if (form_year == year && form_month == month && form_day < day) {
      return false;
    } else if (form_year >= year + 1 && form_month > month) {
      return false;
    } else { return true; }
  }
  else {return this.optional(element) || true;}
}, "This date is in the past, or > 1 year in the future");

$.validator.addMethod("after_start", function(value, element) {
  if (value == "") { return true; }
  var sd = $(element).closest('form').find("[name=start_date]").val();
  if (sd == '') return true
  else {
    sd = Date.parse(sd) + (6 * 3600 * 1000);
    sd = new Date(sd);
    ed = Date.parse(value) + (6 * 3600 * 1000);
    ed = new Date(ed);
    return ed > sd
  }
}, "End date must be after start date");



function luhn(cc) {
  var sum = 0;
  cc = cc.split("").reverse().join("");
    for (var i = 0; i < cc.length; i++) {
      var digit = parseInt(cc.charAt(i));
      if (i % 2 == 1) {
        digit *= 2;
      }
    if (digit > 9) {
        digit -= 9;
    }
    sum += digit;
  }
  return ((sum % 10)==0)
}


/*********
 Utility highlight, unhighlight, and error placement functions
 *********/

 function highlightFn(element, errorClass) {
    $(element).parent().addClass("has-error has-feedback");
    $(element).parent().find("span").remove();
    $(element).after("<span class='glyphicon glyphicon-remove form-control-feedback' aria-hidden='true'></span>");
    $(element).parent().next(".form-group").css("display", "table");
  }

  function unhighlightFn(element, errorClass) {
    $(element).parent().find("span").remove();
    $(element).parent().removeClass("has-error has-feedback");
    $(element).parent().next(".form-group").css("display", "none");
  }

  function errorPlace(error, element) {
    $(element).parent().next(".form-group").empty(); 
    error.appendTo($(element).parent().next(".form-group"));
  }

  function highlightPayFn(element, errorClass) {
    $form = $(element).closest("form");
    if ($(element).is("[name=expire_month]")) {
        $form.find("[name=expire_year]").parent().addClass("has-error has-feedback");
        $form.find("[name=expire_year]").parent().find("span").remove();
        $form.find("[name=expire_year]").after("<span class='glyphicon glyphicon-remove form-control-feedback' aria-hidden='true'></span>");
        $form.find("[name=expire_year]").parent().next(".form-group").css("display", "table");
      } else {
        $(element).parent().addClass("has-error has-feedback");
        $(element).parent().find("span").remove();
        $(element).after("<span class='glyphicon glyphicon-remove form-control-feedback' aria-hidden='true'></span>");
        $(element).parent().next(".form-group").css("display", "table");
      }
  }

  function unhighlightPayFn(element, errorClass) {
    $form = $(element).closest("form");
    if ($(element).is("[name=expire_month]")) {
      $form.find("[name=expire_year]").parent().find("span").remove();
      $form.find("[name=expire_year]").parent().removeClass("has-error has-feedback");
      $form.find("[name=expire_year]").parent().next(".form-group").css("display", "none");
    } else {
      $(element).parent().find("span").remove();
      $(element).parent().removeClass("has-error has-feedback");
      $(element).parent().next(".form-group").css("display", "none");
    }
  }

  function errorPayPlace(error, element) {
    $form = $(element).closest("form");
    if ($(element).is("[name=expire_month]")) {
      if ($(element).parent().nextAll(".form-group").eq(2).find("label").length != 0) {
        $(element).parent().nextAll(".form-group").eq(2).empty();
      }
      error.appendTo($(element).parent().nextAll(".form-group").eq(2));
    } else {
      $(element).parent().next(".form-group").empty(); 
      error.appendTo($(element).parent().next(".form-group"));
    }
  }

  function is_card(element) {
    $form = $(element).closest("form");
    if (!$form.is(".form-pmt")) return true;
    return $form.find("[id*=pmt_method]").val() == 'card';
  }

  function is_ach(element) {
    $form = $(element).closest("form");
    if (!$form.is(".form-pmt")) return true;
    return $form.find("[id*=pmt_method]").val() == 'ach';
  }

function card_rules($form) {
  function zip() {
      return $form.find("[name=billing_zip_code]" ).val();
  }
  function state() {
    return $form.find("[name=billing_state]" ).val();
  }
  var rules =
    {
      billing_first_name: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        }
      },
      billing_last_name: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        }
      },
      expire_year: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        },
        digits: {
          depends: function (element) {
            return is_card(element);
          }
        },
        valid_expire: {
          depends: function (element) {
            return is_card(element);
          }
        }
      },
      expire_month: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        },
        valid_expire: {
          depends: function (element) {
            $form = $(element).closest("form")
            return $form.find("[name=expire_year]").val().length != 0 && is_card(element);
          }
        }
      },
      card_number: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        },
        cc: true,
        maxlength: 16,
        minlength: 15
      },
      cvv: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        },
        cvv: true,
        minlength: 3
      },
      billing_address_1: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        }
      },
      billing_city: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        }
      },
      billing_state: {
        required: {
          depends: function (element) {
            $form = $(element).closest("form");
            return $form.find("[name=billing_country]").val() == 'US' && is_card(element);
          }
        }
      },
      billing_zip_code: {
        required: {
          depends: function (element) {
            return is_card(element);
          }
        },
        zip_format: {
          depends: function (element) {
            $form = $(element).closest("form");
            return $form.find("[name=billing_country]").val() == 'US' && is_card(element);
          }
        },
        remote: {
          param: {
            url: "/zip_check",
            type: "post",
            data: { zip: function () { return zip() },
                    state: function () { return state() } }
          },
          depends: function (element) {
            $form = $(element).closest("form");
            return $form.find("[name=billing_country]").val() == 'US' && is_card(element);
          }
        }
      }
    }
  return rules;
}



function ach_rules($form) {
  var rules = 
  { 
    routing_number: {
      required: {
        depends: function (element) {
          return is_ach(element);
        }
      },
      minlength: 9,
      maxlength: 9,
      routing_valid: {
        depends: function (element) {
          return is_ach(element);
        }
      },
      remote: {
        param: {
          url: "/routing_check",
          type: "post"
        },
        depends: function(element) {
          return /^\d{9}$/.test($(element).closest('form').find("[name=routing_number]").val()); 
        }   
      }
    },
    account_number: {
      required: {
        depends: function (element) {
          return is_ach(element);
        }
      }
    },  
    account_type: {
      required: {
        depends: function (element) {
          return is_ach(element);
        }
      }
    },  
    owned_by: {
      required: {
        depends: function (element) {
          return is_ach(element);
        }
      }
    },  
    registered_name: {
      required: {
        depends: function (element) {
          return is_ach(element);
        }
      },
      maxlength: 64
    }  
  }
  return rules;
}

function recur_rules($form) {
  var rules = {
  day_of_month: {
      required: true,
      range: [1,28]
    },
    start_date: {
      date_format: {
        depends: function() { return navigator.browserInfo.browser == "IE" || navigator.browserInfo.browser == "Firefox" }
      },
      after_today: {
        depends: function () { return $(this).val() != ""; }
      }
    },
    end_date: {
      date_format: {
        depends: function() { return navigator.browserInfo.browser == "IE" || navigator.browserInfo.browser == "Firefox" }
      },
      after_today: {
        depends: function (element) { return $(element).val() != ""; }
      },
      after_start: true
    },
    max_amount: {
      money: true,
      min: 0.01
    },
    amount: {
      required: {
        depends: function(element) {
          $form = $(element).closest("form");
          return $form.find("[name=charge_today]").prop('checked'); 
        }
      },
      money: true,
      min: 0.01
    },
    remind_days_before: {
      required : {
        depends: function (element) { 
          $form = $(element).closest("form");
          return $form.find("[name=reminder]").prop('checked'); 
        }
      },
      range: [1,15]
    }
  }
  return rules;
}


function pmt_rules($form) {
  var rules = 
  { amount: {
      required: true,
      money: true,
      min: 0.01
    },
    scheduled_date: {
      required: true,
      date: true,
      date_format: {
        depends: function() { return navigator.browserInfo.browser == "IE" || navigator.browserInfo.browser == "Firefox" }
      },
      after_today: true,
      isnt_today: {
        depends: function (element) {
          return !$(element).closest('form').is("#form-onetime");
        }
      }
    }
  };
  return rules;
}








$(function () {


  $("form#login").validate({
    errorClass: "form-control",
    highlight: function (element, errorClass) {highlightFn(element, errorClass);},
    unhighlight: function (element, errorClass) {unhighlightFn(element, errorClass);},
    errorPlacement: function (error, element) {errorPlace(error, element);},
    rules: {
      email: {
        required: true,
        email: true
      },
      passowrd: "required",
    }
  });

  $("form#register").validate({
    errorClass: "form-control",
    highlight: function (element, errorClass) {highlightFn(element, errorClass);},
    unhighlight: function (element, errorClass) {unhighlightFn(element, errorClass);},
    errorPlacement: function (error, element) {errorPlace(error, element);},
    rules: {
      email: {
        required: true,
        email: true
      }
    }
  });

  $("form#change-pwrd").validate({
    errorClass: "form-control",
    highlight: function (element, errorClass) {highlightFn(element, errorClass);},
    unhighlight: function (element, errorClass) {unhighlightFn(element, errorClass);},
    errorPlacement: function (error, element) {errorPlace(error, element);},
    rules: {
      old_pwrd: "required",
      new_pwrd: "required",
      confirm: {
        required: true,
        equalTo: "#new_pwrd"
      }
    }
  });

  $("form#change-email").validate({
    errorClass: "form-control",
    highlight: function (element, errorClass) {highlightFn(element, errorClass);},
    unhighlight: function (element, errorClass) {unhighlightFn(element, errorClass);},
    errorPlacement: function (error, element) {errorPlace(error, element);},
    rules: {
      email: {
        required: true,
        email: true
      }
    }
  });

  $("form#password-email").validate({
    errorClass: "form-control",
    highlight: function (element, errorClass) {highlightFn(element, errorClass);},
    unhighlight: function (element, errorClass) {unhighlightFn(element, errorClass);},
    errorPlacement: function (error, element) {errorPlace(error, element);},
    rules: {
      email: {
        required: true,
        email: true
      }
    }
  });

  $("form#form-pending").validate({
    errorClass: "form-control",
    highlight: function (element, errorClass) {highlightFn(element, errorClass);},
    unhighlight: function (element, errorClass) {unhighlightFn(element, errorClass);},
    errorPlacement: function (error, element) {errorPlace(error, element);},
    rules: {
      amount_1: {
        required: true,
        min: 1,
        max: 99
      },
      amount_2: {
        required: true,
        min: 1,
        max: 99
      }
    }
  });

});
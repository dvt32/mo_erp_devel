#!/usr/bin/env python3.4
"""Generate the code to scramble table columns."""

import argparse
from configparser import ConfigParser
import logging
import os.path
from pprint import pprint
from random import randint
import re
import sys
import urllib.request

from psycopg2 import connect, ProgrammingError
from psycopg2.extras import DictCursor


# location of zip codes file
ZIPSOURCEURL = "http://federalgovernmentzipcodes.us/free-zipcode-database-Primary.csv"

# defaults; change them in the properties file.
DBNAME = "microoffice"
DBHOST = "localhost"
DBUSER = "fn"
DBPASS = "."

# provide a table name as a string or a regex:
# don't scramble these tables.
IGNORETABLES = [re.compile(r"^ss_"), "access_level_tbl", "aco_tbl",
                "alias_tbl", "building_tbl", "company_tbl", "country_tbl",
                re.compile(r"^enum_"), "floor_tbl", "holiday_tbl",
                "ip_address_tbl", "isp_tbl", "mailbox_tbl", "reservation_tbl",
                "room_tbl", "state_tbl", "bank_tbl", "config_setting_tbl", "fedach_participant_tbl", "fedwire_participant_tbl", "merchant_processor_tbl", "pmt_vault_pref_tbl", "time_zone_tbl"]
# don't scramble these tables either.  they need to be processed manually.
MANUALTABLES = ["result_item_tbl", re.compile(r"^voicemail_prefs"), "voicemail_msgs_tbl"]

HEADER = '''
from datetime import datetime
from inspect import stack
from random import choice, randint, sample, shuffle
from string import ascii_letters, digits

from helpers import *

ZIPS = getzips("{zipfname}")

'''

TEMPLATE = '''
def {funcname}(cursor, fname, doneset):
    {nothandled}
    cursor.execute("select id from {table}")
    rows = [row["id"] for row in cursor.fetchall()]

    year = datetime.today().year
    {extra}

    for rowid in rows:
        {assignlines}

        cursor.execute("""update {table} set
        {sqlines}
        where id = %s""",
                       ({columns},
                        rowid))
        {extrasql}

    logprocessed("{table}", len(rows))
    doneset.add(fname)

'''


def gettables(cursor):
    """Return a list of table names in the database."""

    cursor.execute("""select table_name from information_schema.tables
    where table_schema = 'public' and table_type = 'BASE TABLE'""")

    tables = [row["table_name"] for row in cursor.fetchall()]

    # remove tables that we either shouldn't mangle or have to be scrambled manually.
    IGNORETABLES.extend(MANUALTABLES)
    for entry in set(IGNORETABLES):
        if isinstance(entry, str):
            tables.remove(entry)
        else:
            removetables = [table for table in filter(entry.match, tables)]
            for table in removetables:
                tables.remove(table)

    return tables


def getzips(zipfname):
    """Download file with zip codes if necessary."""

    if not os.path.exists(zipfname):
        logging.info("downloading %s", ZIPSOURCEURL)
        with open(zipfname, "w") as fzip:
            data = urllib.request.urlopen(ZIPSOURCEURL).read()
            fzip.write(data.decode("ascii"))


def guess(companybanknums, table, colname, coltype, charlength, precision, scale):
    """If we recognize the table/column name, return an appropriate transform for it.

    If we don't know what to do with a column or don't want to
    transform it, return None.

    charlength is valid for varchar/character fields, and has the
    maximum length.

    As we learn of more column types, this can be improved.
    """

    if table == "desk_tbl":
        if colname in ("list_price", "current_price"):
            return "randnumstr(500, 1000)"
    elif table == "proximity_card_tbl":
        if colname == "number":
            return "int(randnumstr(100, 65000))"
        elif colname == "site_code":
            return "int(randnumstr(1, 250))"
    elif coltype in ("bpchar", "varchar"):
        if not charlength:
            charlength = 1
            logging.critical("no charlength for char column %s", colname)
        if table == "broker_referral_tbl":
            if colname in ("broker_address_2", "client_address_2"):
                return "''"
            elif colname.find("address_1") != -1:
                return "randaddress(rowid, %d)" % charlength
            elif colname.find("phone_") != -1:
                return "randphone(%d)" % charlength
            elif colname.find("fax_") != -1:
                return "randphone(%d)" % charlength
            elif colname.find("email") != -1:
                value = ('"%s' % table) + '_%s@mailtest.microoffice.com" % rowid'
                if len(value) > charlength:
                    value = value.replace(table, 't').replace('mailtest.microoffice.com', 'f')
                return value
            else:
                return None
        if table == "bank_account_tbl":
            if colname == "bank_account_type":
                return "weightedchoice(['checking', 'saving'], [90, 10])"
            elif colname == "bank_account_ownership_type":
                return "weightedchoice(['business', 'personal'], [80, 20])"
            elif colname in ("depository_name", "city", "state", "zip_code",
            "country", "bank_routing_number", "bank_account_nickname"):
                return "choice(accounts).%s" % colname
        if table == "ach_debit_txn_tbl":
            if colname == "customer_bank_id":
                return "choice(custbank)"
            elif colname == "company_bank_acct_type":
                return "'C'"
            elif colname == "bank_account_holder_type":
                return 'choice(["personal", "business"])'
            elif colname == "customer_bank_acct_num":
                return "randbankaccount(%d)" % charlength
            elif colname == "company_bank_acct_num":
                return "choice(%s)" % companybanknums
            elif colname == "customer_name":
                return "randfullname(%d)" % charlength
        if table == "cust_acct_tbl":
            if colname in ("name", "external_name"):
                return None
        if table == "fax_in_tbl" and colname == "filename":
            return "randnumstr() + '.pdf'"
        if table == "fax_number_tbl":
            if colname in ("email_action", "in_device_action", "in_device_target"):
                return None
        if table == "file_tbl":
            if colname in ("mime_type", "namespace"):
                return None
        if table == "deposit_bank_account_tbl" and colname == "account_number":
            return "randbankaccount(%d)" % charlength
        if colname.find("time_zone") != -1:
            return "random_us_time_zone()"
        if colname.find("month") != -1:
            return "randnumstr(1, 12, %d)" % charlength
        elif colname.find("year") != -1:
            return "randnumstr(year, year + 10, %d)" % charlength
        elif colname.find("hash") != -1:
            return "randstr(%d, source=HEXDIGITS)" % charlength
        elif colname.find("salt") != -1:
            return "randstr(%d, source=HEXDIGITS)" % charlength
        elif colname == "card_number" and table != "credit_card_payment_tbl":
            return "randstr(%d)" % charlength
        elif colname == "card_security_code":
            return "randstr(%d)" % charlength
        elif colname.find("address_num") != -1:
            return "str(rowid)"
        elif colname == "country":
            return None
        elif colname.find("address_1") != -1 or colname.find("address1") != -1:
            return "randaddress(rowid, %d)" % charlength
        elif colname.find("address_2") != -1 or colname.find("address_3") != -1:
            return "''"
        elif colname == "address2" != -1:
            return "''"
        elif colname == "social_security_number":
            return "randstr(%d)" % charlength
        elif colname.find("city") != -1:
            return "ZIPS[randzip].city"
        elif colname.find("state") != -1:
            return "ZIPS[randzip].state"
        elif colname.find("zip") != -1:
            return "randfullname(%d)" % charlength
            return "ZIPS[randzip].zip; randzip = randint(0, len(ZIPS)-1)"
        elif colname.find("type") != -1:
            return 'choice(["C", "S"])'
        elif colname.find("bank_acc") != -1 or colname.find("account_num") != -1:
            return "randstr(lmin=1, lmax=%d)" % charlength
        elif colname.find("routing") != -1:
            return "randstr(%d)" % charlength
        elif colname.find("phone_") != -1 or colname == "phone":
            return "randphone(%d)" % charlength
        elif colname.find("fax_") != -1 or colname == "fax":
            return "randphone(%d)" % charlength
        elif colname == "billing_name":
            return "randfullname(%d)" % charlength
        elif colname == "name":
            # special case for enum_agreement_template_tbl:
            # the name field is a varchar, but does not have a length
            if charlength is None:
                charlength = 8
            return "randfullname(maxlen=%d)" % charlength
        elif colname.find("first_name") != -1:
            return "randname(FNAMES, rowid, maxlen=%d)" % charlength
        elif colname.find("middle_name") != -1:
            return "randname(FNAMES, rowid, maxlen=%d)" % charlength
        elif colname.find("last_name") != -1:
            return "randname(LNAMES, rowid, maxlen=%d)" % charlength
        elif colname.find("username") != -1:
            return "randstr(length=%d, source=ascii_letters)" % charlength
        elif colname.find("_company") != -1:
            return "randstr(lmin=1, lmax=%d, source=ALPHANUM)" % charlength
        elif colname in ("item_name", "role_name"):
            return "randname(FNAMES, rowid, maxlen=%d).lower()" % charlength
        elif colname == "windows_domain_name":
            return "'microoffice'"
        elif colname == "windows_upn_suffix":
            return 'choice(["microoffice.com", "coalitionspace.com"])'
        elif colname == "windows_logon_name":
            return '"%s" % (randname(FNAMES, rowid, ' + 'maxlen=%d).lower())' % charlength
        elif colname.find("domain") != -1:
            return '"%s.%s" % (randstr(10, source=ascii_letters).lower(), choice(["com", "org", "net"]))'
        elif colname.find("hostname") != -1:
            return '"%s.%s.%s" % (randstr(10, source=ascii_letters).lower(), randstr(10, source=ascii_letters).lower(), choice(["com", "org", "net"]))'
        elif colname.startswith("server"):
            return "randname(FNAMES, rowid, maxlen=%d).lower()" % charlength
        elif colname.find("_name") != -1:
            return '"%s" % (randname(FNAMES, rowid, ' + 'maxlen=%d))' % charlength
        elif colname.find("salutation") != -1:
            return 'choice(["Mr", "Miss", "Mrs"])'
        elif colname.find("_suffix") != -1:
            return 'choice(["Inc", "Ltd", "Corp"])'
        elif colname.find("url") != -1 or colname.find("web_site") != -1:
            return '"https://g.co/search?q=%s" % randname(SNAMES, rowid, ' + 'maxlen=%d)' % charlength
        elif colname == "allowed_ip_addr":
            return "NULL"
        elif colname.find("_ip") != -1 or colname == "ip_address":
            return '"%s.%s.%s.%s" % (randstr(2), randstr(2), randstr(2), randstr(2))'
        elif colname.find("password") != -1:
            return "randstr(lmin=1, lmax=%d, source=ALPHANUM)" % charlength
        elif colname == "operating_system":
            return 'choice(["Linux", "Windows", "Solaris"])'
        elif colname == "mac_address":
            return '"%s%s%s" % (randstr(4, source=HEXDIGITS), randstr(4, source=HEXDIGITS), randstr(4, source=HEXDIGITS))'
        elif colname.find("email") != -1 or colname in ("from_hdr", "to_hdr"):
            value = ('"%s' % table) + '_%s@mailtest.microoffice.com" % rowid'
            if len(value) > charlength:
                value = value.replace(table, 't').replace('mailtest.microoffice.com', 'f')
            return value
        elif colname == "payer":
            return "randstr(10)"
        elif colname.find("tax_id") != -1:
            return "randstr(%d)" % charlength
        elif colname in ("remote_addr", "remote_host", "invoice_number") and table == "credit_card_payment_tbl":
            return None
        elif not colname.endswith("_id"):
            return "randstr(%d)" % charlength
    elif coltype == "bool":
        if table == "ach_debit_txn_tbl":
            if colname in ("create_prenote", "hold_transfer"):
                return '"false"'
            elif colname == "cancelled":
                return None
        if table == "employee_tbl" and colname == "is_active":
            return "True"
        elif colname in ("processed", "cancelled") and table == "credit_card_payment_tbl":
            return None
        elif colname in ("is_active", "is_primary") and table == "bank_account_tbl":
            return None
        else:
            return 'choice(["true", "false"])'
    elif coltype == "text":
        if (colname == "note" and
            table in ("fax_in_tbl", "fax_number_tbl",
                      "cust_acct_tbl", "credit_card_payment_tbl",
                      "file_tbl")):
            return None
        elif (colname == "notes" and
              table in ("bank_account_tbl", "ach_debit_txn_tbl")):
            return None
        elif (colname == "transaction_json" and
              table == "credit_card_payment_tbl"):
            return "''"
        else:
            return "randstr(lmin=1, lmax=20)"
    elif coltype.startswith("int") and colname != "id" and not colname.endswith("_id"):
        # hack for cust_pers_tbl_user_id_key, desk_tbl_floor_id_key
        return "rowid+25000"
    elif coltype == "float8":
        return "float(randstr(2))"
    elif coltype == "numeric":
        if not precision:
            logging.critical("no precision for numeric column %s", colname)
            precision = 2
        if not scale:
            logging.critical("no scale for numeric column %s", colname)
            scale = 0
        if colname == "amount" and table == "ach_debit_txn_tbl":
            return "randamount()"
        if colname == "subtotal" and table == "credit_card_payment_tbl":
            return None
        size = max(1, precision - scale - 1)
        return "int(randstr(lmin=%d, lmax=%d))" % (size, size)
    elif coltype == "date":
        if colname == "birthday":
            return '"%s-%s-%s" % (randnumstr(1960, 2000, 4), randnumstr(1, 12, 2), randnumstr(1, 28, 2))'
        elif colname == "charge_date" and table == "credit_card_payment_tbl":
            return None
    elif coltype == "inet":
        return '"%s.%s.%s.%s" % (randstr(2), randstr(2), randstr(2), randstr(2))'

    return None


def columns(cursor, table, companybanknums):
    """Table columns we know how to transform and the ones we don't.

    Returns a dictionary with two keys: "scrambled" with a list of
    columns we know how to transform (and what to transform them
    with), and "unmodified" with a list of columns we don't know what
    to do with.
    """

    retdict = {"scrambled": [], "unmodified": []}

    cursor.execute("""select column_name, udt_name, character_maximum_length,
    numeric_precision, numeric_scale from information_schema.columns where
    table_schema = 'public' and table_name = %s""", (table,))
    rows = cursor.fetchall()

    rows.sort(key=lambda row: row["column_name"])

    for row in rows:
        # ignore timestamps and ids for now
        if (row["column_name"] != "id" and
            # foreign keys, maybe?
            # not row["column_name"].endswith("_id") and
            # ignore dates/times
            row["udt_name"] not in ("timestamp", "timestamptz", "timetz") and
            # columns with ms_access in the names are used for unique constraints
            not row["column_name"].startswith("ms_access")):
            transform = guess(companybanknums, table, *row)
            logging.debug("working with: %s %s, transform: %s", table, row, transform)
            if transform is None:
                retdict["unmodified"].append(tuple(row))
            elif transform == "NULL":
                retdict["scrambled"].append((row["column_name"], None))
            else:
                retdict["scrambled"].append((row["column_name"], transform))

    return retdict


def generate(args, tables, coldict, zipfname):
    """Generate Python code to scramble tables."""

    fgen = open(os.path.join(args.path, "scramblefns.py"), "w")
    fgen.write(HEADER.format(**locals()))

    writecount = 0

    for table in tables:
        # if any column has a "zip" field, set this to True
        # we use this to shuffle the ZIPS array for the function
        haszip = False
        tdict = coldict[table]
        funcname = "scramble_" + table.replace("_tbl", "")
        if len(tdict["unmodified"]):
            nothandled = '"""Fields not handled:\n\n      - '
            nothandled += "\n      - ".join([row[0] for row in tdict["unmodified"]])
            nothandled += '\n    """\n'
        else:
            nothandled = ''
        assignls = []
        sqlinels = []
        columnls = []
        extra = ''
        extrasql = ''
        for row in tdict["scrambled"]:
            if row[0].find("zip") != -1:
                haszip = True
            if row[0] == "class":
                assignls.append("_%s = %s" % (row[0], row[1]))
                columnls.append("_%s" % row[0])
                sqlinels.append("%s = %%s" % row[0])
            elif row[0] == "ds_envelopeID":
                assignls.append("%s = %s" % (row[0], row[1]))
                columnls.append("%s" % row[0])
                sqlinels.append("\"%s\" = %%s" % row[0])
            else:
                assignls.append("%s = %s" % (row[0], row[1]))
                columnls.append(row[0])
                sqlinels.append("%s = %%s" % row[0])
        assignlines = "\n        ".join(assignls)
        sqlines = ",\n        ".join(sqlinels)
        columns = ",\n                        ".join(columnls)
        if haszip:
            extra += "\n    shuffle(ZIPS); randzip = randint(0, len(ZIPS)-1)"
        if table == "bank_account_tbl":
            extra += "\n    accounts = getbankaccounts(cursor)"
        if table == "ach_debit_txn_tbl":
            extra += "\n    custbank = getcustbank(cursor)"
        # cust_acct_tbl: must scramble cust_pers_table first
        if table == "cust_acct_tbl":
            extra += "\n    scramble_cust_pers(cursor, 'scramble_cust_pers', doneset)"
            extrasql += "\n    # handle name and external_name columns here."
            extrasql += "\n    custacctname(cursor, rowid)"
        if table == "credit_card_payment_tbl":
            extrasql += "\n    calculateccfee(cursor, rowid)"
        if table == "expense_tbl":
            extrasql += "\n    rand_payment_identifier(cursor, rowid)"
        if table == "file_tbl":
            extrasql += "\n    filenameext(cursor, rowid)"

        if not len(columnls):
            logging.info("nothing to do for %s", table)
        else:
            fgen.write(TEMPLATE.format(**locals()))
            writecount += 1
            logging.debug("wrote %s function", funcname)

    fgen.close()
    logging.info("functions written out: %d", writecount)


def main():
    """Main branching logic."""

    parser = argparse.ArgumentParser(description="Generate table scramblers")
    parser.add_argument("-c", "--config", nargs="?",
                        type=argparse.FileType("r"),
                        help="read a config file")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="print debug messages")
    parser.add_argument("-g", "--generate", action="store_true",
                        help="generate scramblefns.py")
    parser.add_argument("-p", "--path", default=".",
                        help="directory to save scramblefns.py in")
    parser.add_argument("-l", "--list", dest="list",
                        choices=["all", "scrambled", "unknown"],
                        help="list columns to stderr")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else:
        logging.basicConfig(level=logging.INFO, format="%(message)s")

    config = ConfigParser(default_section="scramble")
    # fake section name
    props = "[root]\n"
    if args.config:
        props = props + args.config.read()
    config.read_string(props)

    cachedir = config["root"].get("database_cache_dir", "/tmp")
    zipfname = os.path.join(cachedir, os.path.basename(ZIPSOURCEURL))
    if not os.path.exists(cachedir):
        logging.info("creating cache dir: %s", cachedir)
        os.mkdir(cachedir)

    logging.info("starting...")

    getzips(zipfname)

    cnx = connect(database=config["root"].get("scramble_dbname", DBNAME),
                  user=config["root"].get("scramble_dbuser", DBUSER),
                  password=config["root"].get("scramble_dbpass", DBPASS),
                  host=config["root"].get("scramble_dbhost", DBHOST))
    cursor = cnx.cursor(cursor_factory=DictCursor)

    retdict = {}

    tables = gettables(cursor)

    # special case:

    # For company_bank_acct_num, initially generate 3 random numbers,
    # each one 8 - 10 digits long. Those will be the company bank
    # accounts, and each row in ach_debit_txn_tbl should randomly
    # select one of the three initially generated bank account numbers
    # as the value for company_bank_acct_num.
    companybanknums = []
    for i in range(3):
        companybanknums.append(str(randint(10**7, (10**10)-1)))

    for table in tables:
        retdict[table] = columns(cursor, table, companybanknums)

    logging.info("done.")
    cnx.rollback()
    cursor.close()
    cnx.close()

    if args.generate:
        generate(args, tables, retdict, zipfname)
    else:
        logging.warn("NOTE: -g argument not given, nothing written out.")

    if args.list:
        for table in tables:
            logging.info("%s: ", table)
            if args.list in ("scrambled", "all"):
                for row in retdict[table]["scrambled"]:
                    logging.info("\t%s: %s", row[0], row[1])
            if args.list in ("unknown", "all"):
                for row in retdict[table]["unmodified"]:
                    logging.info("fix:\t%s (%s)", row[0], row[1])


if __name__ == '__main__':
    main()

# eof

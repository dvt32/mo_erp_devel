"""Misc defines."""

from collections import namedtuple
from csv import reader
from datetime import datetime
from decimal import Decimal
from inspect import stack
import logging
from random import choice, randint, random, sample
import re
from string import ascii_letters, ascii_uppercase, capwords, digits

DIGITS = digits + digits
ALPHANUM = ascii_letters + digits
HEXDIGITS = digits + "ABCDEF"
SPACES = re.compile(r"\s{2,}")

# from http://www.ssa.gov/oact/babynames/decades/names2000s.html
FNAMES =["Aaliyah","Aaron","Abby", "Abigail", "Abraham", "Adam",
         "Addison", "Adrian", "Adriana", "Adrianna", "Aidan", "Aiden",
         "Alan", "Alana", "Alejandro", "Alex", "Alexa", "Alexander",
         "Alexandra", "Alexandria", "Alexia", "Alexis", "Alicia",
         "Allison", "Alondra", "Alyssa", "Amanda", "Amber", "Amelia",
         "Amy", "Ana", "Andrea", "Andres", "Andrew", "Angel",
         "Angela", "Angelica", "Angelina", "Anna", "Anthony",
         "Antonio", "Ariana", "Arianna", "Ashley", "Ashlyn", "Ashton",
         "Aubrey", "Audrey", "Austin", "Autumn", "Ava", "Avery",
         "Ayden", "Bailey", "Barbara", "Benjamin", "Bianca", "Blake",
         "Braden", "Bradley", "Brady", "Brandon", "Brayden",
         "Breanna", "Brendan", "Brian", "Briana", "Brianna",
         "Brittany", "Brody", "Brooke", "Brooklyn", "Bryan", "Bryce",
         "Bryson", "Caden", "Caitlin", "Caitlyn", "Caleb", "Cameron",
         "Camila", "Carlos", "Caroline", "Carson", "Carter",
         "Cassandra", "Cassidy", "Catherine", "Cesar", "Charles",
         "Charlotte", "Chase", "Chelsea", "Cheyenne", "Chloe",
         "Christian", "Christina", "Christopher", "Claire", "Cody",
         "Colby", "Cole", "Colin", "Collin", "Colton", "Conner",
         "Connor", "Cooper", "Courtney", "Cristian", "Crystal",
         "Daisy", "Dakota", "Dalton", "Damian", "Daniel", "Daniela",
         "Danielle", "David", "Delaney", "Derek", "Destiny", "Devin",
         "Devon", "Diana", "Diego", "Dominic", "Donovan", "Dorothy",
         "Dylan", "Edgar", "Eduardo", "Edward", "Edwin", "Eli",
         "Elias", "Elijah", "Elizabeth", "Ella", "Ellie", "Emily",
         "Emma", "Emmanuel", "Eric", "Erica", "Erick", "Erik", "Erin",
         "Ethan", "Eva", "Evan", "Evelyn", "Faith", "Fernando",
         "Francisco", "Gabriel", "Gabriela", "Gabriella", "Gabrielle",
         "Gage", "Garrett", "Gavin", "Genesis", "George", "Gianna",
         "Giovanni", "Giselle", "Grace", "Gracie", "Grant", "Gregory",
         "Hailey", "Haley", "Hannah", "Hayden", "Hector", "Henry",
         "Hope", "Hunter", "Ian", "Isaac", "Isabel", "Isabella",
         "Isabelle", "Isaiah", "Ivan", "Jack", "Jackson", "Jacob",
         "Jacqueline", "Jada", "Jade", "Jaden", "Jake", "Jalen",
         "James", "Jared", "Jasmin", "Jasmine", "Jason", "Javier",
         "Jayden", "Jayla", "Jazmin", "Jeffrey", "Jenna", "Jennifer",
         "Jeremiah", "Jeremy", "Jesse", "Jessica", "Jesus", "Jillian",
         "Jocelyn", "Joel", "John", "Johnathan", "Jonah", "Jonathan",
         "Jordan", "Jordyn", "Jorge", "Jose", "Joseph", "Joshua",
         "Josiah", "Juan", "Julia", "Julian", "Juliana", "Justin",
         "Kaden", "Kaitlyn", "Kaleb", "Karen", "Karina", "Kate",
         "Katelyn", "Katherine", "Kathryn", "Katie", "Kayla",
         "Kaylee", "Kelly", "Kelsey", "Kendall", "Kennedy", "Kenneth",
         "Kevin", "Kiara", "Kimberly", "Kyle", "Kylee", "Kylie",
         "Landon", "Laura", "Lauren", "Layla", "Leah", "Leonardo",
         "Leslie", "Levi", "Liam", "Liliana", "Lillian", "Lilly",
         "Lily", "Linda", "Lindsey", "Logan", "Lucas", "Lucy", "Luis",
         "Luke", "Lydia", "Mackenzie", "Madeline", "Madelyn",
         "Madison", "Makayla", "Makenzie", "Malachi", "Manuel",
         "Marco", "Marcus", "Margaret", "Maria", "Mariah", "Mario",
         "Marissa", "Mark", "Martin", "Mary", "Mason", "Matthew",
         "Max", "Maxwell", "Maya", "Mckenzie", "Megan", "Melanie",
         "Melissa", "Mia", "Micah", "Michael", "Michelle", "Miguel",
         "Mikayla", "Miranda", "Molly", "Morgan", "Mya", "Naomi",
         "Natalia", "Natalie", "Nathan", "Nathaniel", "Nevaeh",
         "Nicholas", "Nicolas", "Nicole", "Noah", "Nolan", "Oliver",
         "Olivia", "Omar", "Oscar", "Owen", "Paige", "Parker",
         "Patricia", "Patrick", "Paul", "Payton", "Peter", "Peyton",
         "Preston", "Rachel", "Raymond", "Reagan", "Rebecca",
         "Ricardo", "Richard", "Riley", "Robert", "Ruby", "Ryan",
         "Rylee", "Sabrina", "Sadie", "Samantha", "Samuel", "Sara",
         "Sarah", "Savannah", "Sean", "Sebastian", "Serenity",
         "Sergio", "Seth", "Shane", "Shawn", "Shelby", "Sierra",
         "Skylar", "Sofia", "Sophia", "Sophie", "Spencer",
         "Stephanie", "Stephen", "Steven", "Summer", "Susan",
         "Sydney", "Tanner", "Taylor", "Thomas", "Tiffany", "Timothy",
         "Travis", "Trenton", "Trevor", "Trinity", "Tristan", "Tyler",
         "Valeria", "Valerie", "Vanessa", "Veronica", "Victor",
         "Victoria", "Vincent", "Wesley", "William", "Wyatt",
         "Xavier", "Zachary", "Zoe","Zoey"]

# from http://names.mongabay.com/data/1000.html
LNAMES = ["Abbott", "Acevedo", "Acosta", "Adams", "Adkins", "Aguilar",
          "Aguirre", "Alexander", "Ali", "Allen", "Allison",
          "Alvarado", "Alvarez", "Andersen", "Anderson", "Andrade",
          "Andrews", "Anthony", "Archer", "Arellano", "Arias",
          "Armstrong", "Arnold", "Arroyo", "Ashley", "Atkins",
          "Atkinson", "Austin", "Avery", "Avila", "Ayala", "Ayers",
          "Bailey", "Baird", "Baker", "Baldwin", "Ball", "Ballard",
          "Banks", "Barajas", "Barber", "Barker", "Barnes", "Barnett",
          "Barr", "Barrera", "Barrett", "Barron", "Barry", "Bartlett",
          "Barton", "Bass", "Bates", "Bauer", "Bautista", "Baxter",
          "Bean", "Beard", "Beasley", "Beck", "Becker", "Bell",
          "Beltran", "Bender", "Benitez", "Benjamin", "Bennett",
          "Benson", "Bentley", "Benton", "Berg", "Berger", "Bernard",
          "Berry", "Best", "Bird", "Bishop", "Black", "Blackburn",
          "Blackwell", "Blair", "Blake", "Blanchard", "Blankenship",
          "Blevins", "Bolton", "Bond", "Bonilla", "Booker", "Boone",
          "Booth", "Bowen", "Bowers", "Bowman", "Boyd", "Boyer",
          "Boyle", "Bradford", "Bradley", "Bradshaw", "Brady",
          "Branch", "Brandt", "Braun", "Bray", "Brennan", "Brewer",
          "Bridges", "Briggs", "Bright", "Brock", "Brooks", "Brown",
          "Browning", "Bruce", "Bryan", "Bryant", "Buchanan", "Buck",
          "Buckley", "Bullock", "Burch", "Burgess", "Burke",
          "Burnett", "Burns", "Burton", "Bush", "Butler", "Byrd",
          "Cabrera", "Cain", "Calderon", "Caldwell", "Calhoun",
          "Callahan", "Camacho", "Cameron", "Campbell", "Campos",
          "Cannon", "Cantrell", "Cantu", "Cardenas", "Carey",
          "Carlson", "Carney", "Carpenter", "Carr", "Carrillo",
          "Carroll", "Carson", "Carter", "Case", "Casey", "Castaneda",
          "Castillo", "Castro", "Cervantes", "Chambers", "Chan",
          "Chandler", "Chaney", "Chang", "Chapman", "Charles",
          "Chase", "Chavez", "Chen", "Cherry", "Choi", "Christensen",
          "Christian", "Chung", "Church", "Cisneros", "Clark",
          "Clarke", "Clay", "Clayton", "Clements", "Cline", "Cobb",
          "Cochran", "Coffey", "Cohen", "Cole", "Coleman", "Collier",
          "Collins", "Colon", "Combs", "Compton", "Conley", "Conner",
          "Conrad", "Contreras", "Conway", "Cook", "Cooke", "Cooley",
          "Cooper", "Copeland", "Cordova", "Cortez", "Costa", "Cowan",
          "Cox", "Craig", "Crane", "Crawford", "Crosby", "Cross",
          "Cruz", "Cuevas", "Cummings", "Cunningham", "Curry",
          "Curtis", "Dalton", "Daniel", "Daniels", "Daugherty",
          "Davenport", "David", "Davidson", "Davies", "Davila",
          "Davis", "Dawson", "Day", "Dean", "Decker", "Delacruz",
          "Deleon", "Delgado", "Dennis", "Diaz", "Dickerson",
          "Dickson", "Dillon", "Dixon", "Dodson", "Dominguez",
          "Donaldson", "Donovan", "Dorsey", "Dougherty", "Douglas",
          "Downs", "Doyle", "Drake", "Duarte", "Dudley", "Duffy",
          "Duke", "Duncan", "Dunlap", "Dunn", "Duran", "Durham",
          "Dyer", "Eaton", "Edwards", "Elliott", "Ellis", "Ellison",
          "English", "Erickson", "Escobar", "Esparza", "Espinoza",
          "Estes", "Estrada", "Evans", "Everett", "Ewing", "Farley",
          "Farmer", "Farrell", "Faulkner", "Ferguson", "Fernandez",
          "Ferrell", "Fields", "Figueroa", "Finley", "Fischer",
          "Fisher", "Fitzgerald", "Fitzpatrick", "Fleming",
          "Fletcher", "Flores", "Flowers", "Floyd", "Flynn", "Foley",
          "Forbes", "Ford", "Foster", "Fowler", "Fox", "Francis",
          "Franco", "Frank", "Franklin", "Frazier", "Frederick",
          "Freeman", "French", "Frey", "Friedman", "Fritz", "Frost",
          "Fry", "Frye", "Fuentes", "Fuller", "Gaines", "Gallagher",
          "Gallegos", "Galloway", "Galvan", "Gamble", "Garcia",
          "Gardner", "Garner", "Garrett", "Garrison", "Garza",
          "Gates", "Gay", "Gentry", "George", "Gibbs", "Gibson",
          "Gilbert", "Giles", "Gill", "Gillespie", "Gilmore", "Glass",
          "Glenn", "Glover", "Golden", "Gomez", "Gonzales",
          "Gonzalez", "Good", "Goodman", "Goodwin", "Gordon", "Gould",
          "Graham", "Grant", "Graves", "Gray", "Green", "Greene",
          "Greer", "Gregory", "Griffin", "Griffith", "Grimes",
          "Gross", "Guerra", "Guerrero", "Gutierrez", "Guzman",
          "Haas", "Hahn", "Hale", "Haley", "Hall", "Hamilton",
          "Hammond", "Hampton", "Hancock", "Haney", "Hanna", "Hansen",
          "Hanson", "Hardin", "Harding", "Hardy", "Harmon", "Harper",
          "Harrell", "Harrington", "Harris", "Harrison", "Hart",
          "Hartman", "Harvey", "Hatfield", "Hawkins", "Hayden",
          "Hayes", "Haynes", "Hays", "Heath", "Hebert", "Henderson",
          "Hendricks", "Hendrix", "Henry", "Hensley", "Henson",
          "Herman", "Hernandez", "Herrera", "Herring", "Hess",
          "Hester", "Hickman", "Hicks", "Higgins", "Hill", "Hines",
          "Hinton", "Ho", "Hobbs", "Hodge", "Hodges", "Hoffman",
          "Hogan", "Holden", "Holder", "Holland", "Holloway",
          "Holmes", "Holt", "Hood", "Hooper", "Hoover", "Hopkins",
          "Horn", "Horne", "Horton", "House", "Houston", "Howard",
          "Howe", "Howell", "Huang", "Hubbard", "Huber", "Hudson",
          "Huerta", "Huff", "Huffman", "Hughes", "Hull", "Humphrey",
          "Hunt", "Hunter", "Hurley", "Hurst", "Hutchinson", "Huynh",
          "Ibarra", "Ingram", "Irwin", "Jackson", "Jacobs",
          "Jacobson", "James", "Jarvis", "Jefferson", "Jenkins",
          "Jennings", "Jensen", "Jimenez", "Johns", "Johnson",
          "Johnston", "Jones", "Jordan", "Joseph", "Joyce", "Juarez",
          "Kaiser", "Kane", "Kaufman", "Keith", "Keller", "Kelley",
          "Kelly", "Kemp", "Kennedy", "Kent", "Kerr", "Key", "Khan",
          "Kidd", "Kim", "King", "Kirby", "Kirk", "Klein", "Kline",
          "Knapp", "Knight", "Knox", "Koch", "Kramer", "Krause",
          "Krueger", "Lam", "Lamb", "Lambert", "Landry", "Lane",
          "Lang", "Lara", "Larsen", "Larson", "Lawrence", "Lawson",
          "Le", "Leach", "Leblanc", "Lee", "Leon", "Leonard",
          "Lester", "Levine", "Levy", "Lewis", "Li", "Lin", "Lindsey",
          "Little", "Liu", "Livingston", "Lloyd", "Logan", "Long",
          "Lopez", "Love", "Lowe", "Lowery", "Lozano", "Lucas",
          "Lucero", "Luna", "Lutz", "Lynch", "Lynn", "Lyons",
          "Macdonald", "Macias", "Mack", "Madden", "Maddox",
          "Mahoney", "Maldonado", "Malone", "Mann", "Manning",
          "Marks", "Marquez", "Marsh", "Marshall", "Martin",
          "Martinez", "Mason", "Massey", "Mata", "Mathews", "Mathis",
          "Matthews", "Maxwell", "May", "Mayer", "Maynard", "Mayo",
          "Mays", "Mcbride", "Mccall", "Mccann", "Mccarthy",
          "Mccarty", "Mcclain", "Mcclure", "Mcconnell", "Mccormick",
          "Mccoy", "Mccullough", "Mcdaniel", "Mcdonald", "Mcdowell",
          "Mcfarland", "Mcgee", "Mcgrath", "Mcguire", "Mcintosh",
          "Mcintyre", "Mckay", "Mckee", "Mckenzie", "Mckinney",
          "Mcknight", "Mclaughlin", "Mclean", "Mcmahon", "Mcmillan",
          "Mcneil", "Mcpherson", "Meadows", "Medina", "Mejia",
          "Melendez", "Melton", "Mendez", "Mendoza", "Mercado",
          "Mercer", "Merritt", "Meyer", "Meyers", "Meza", "Michael",
          "Middleton", "Miles", "Miller", "Mills", "Miranda",
          "Mitchell", "Molina", "Monroe", "Montes", "Montgomery",
          "Montoya", "Moody", "Moon", "Mooney", "Moore", "Mora",
          "Morales", "Moran", "Moreno", "Morgan", "Morris",
          "Morrison", "Morrow", "Morse", "Morton", "Moses", "Mosley",
          "Moss", "Moyer", "Mueller", "Mullen", "Mullins", "Munoz",
          "Murillo", "Murphy", "Murray", "Myers", "Nash", "Navarro",
          "Neal", "Nelson", "Newman", "Newton", "Nguyen", "Nichols",
          "Nicholson", "Nielsen", "Nixon", "Noble", "Nolan", "Norman",
          "Norris", "Norton", "Novak", "Nunez", "Obrien", "Ochoa",
          "Oconnell", "Oconnor", "Odom", "Odonnell", "Oliver",
          "Olsen", "Olson", "Oneal", "Oneill", "Orozco", "Orr",
          "Ortega", "Ortiz", "Osborn", "Osborne", "Owen", "Owens",
          "Pace", "Pacheco", "Padilla", "Page", "Palmer", "Park",
          "Parker", "Parks", "Parrish", "Parsons", "Patel", "Patrick",
          "Patterson", "Patton", "Paul", "Payne", "Pearson", "Peck",
          "Pena", "Pennington", "Perez", "Perkins", "Perry", "Peters",
          "Petersen", "Peterson", "Petty", "Pham", "Phelps",
          "Phillips", "Pierce", "Pineda", "Pittman", "Pitts",
          "Pollard", "Ponce", "Poole", "Pope", "Porter", "Potter",
          "Potts", "Powell", "Powers", "Pratt", "Preston", "Price",
          "Prince", "Proctor", "Pruitt", "Pugh", "Quinn", "Ramirez",
          "Ramos", "Ramsey", "Randall", "Randolph", "Rangel",
          "Rasmussen", "Ray", "Raymond", "Reed", "Reese", "Reeves",
          "Reid", "Reilly", "Reyes", "Reynolds", "Rhodes", "Rice",
          "Rich", "Richard", "Richards", "Richardson", "Richmond",
          "Riddle", "Riggs", "Riley", "Rios", "Ritter", "Rivas",
          "Rivera", "Rivers", "Roach", "Robbins", "Roberson",
          "Roberts", "Robertson", "Robinson", "Robles", "Rocha",
          "Rodgers", "Rodriguez", "Rogers", "Rojas", "Rollins",
          "Roman", "Romero", "Rosales", "Rosario", "Rose", "Ross",
          "Roth", "Rowe", "Rowland", "Roy", "Rubio", "Ruiz", "Rush",
          "Russell", "Russo", "Ryan", "Salas", "Salazar", "Salinas",
          "Sampson", "Sanchez", "Sanders", "Sandoval", "Sanford",
          "Santana", "Santiago", "Santos", "Saunders", "Savage",
          "Sawyer", "Schaefer", "Schmidt", "Schmitt", "Schneider",
          "Schroeder", "Schultz", "Schwartz", "Scott", "Sellers",
          "Serrano", "Sexton", "Shaffer", "Shah", "Shannon", "Sharp",
          "Shaw", "Shea", "Shelton", "Shepard", "Shepherd",
          "Sheppard", "Sherman", "Shields", "Short", "Silva",
          "Simmons", "Simon", "Simpson", "Sims", "Singh", "Singleton",
          "Skinner", "Sloan", "Small", "Smith", "Snow", "Snyder",
          "Solis", "Solomon", "Sosa", "Soto", "Sparks", "Spears",
          "Spence", "Spencer", "Stafford", "Stanley", "Stanton",
          "Stark", "Steele", "Stein", "Stephens", "Stephenson",
          "Stevens", "Stevenson", "Stewart", "Stokes", "Stone",
          "Stout", "Strickland", "Strong", "Stuart", "Suarez",
          "Sullivan", "Summers", "Sutton", "Swanson", "Sweeney",
          "Tanner", "Tapia", "Tate", "Taylor", "Terrell", "Terry",
          "Thomas", "Thompson", "Thornton", "Todd", "Torres",
          "Townsend", "Tran", "Travis", "Trevino", "Trujillo",
          "Tucker", "Turner", "Tyler", "Underwood", "Valdez",
          "Valencia", "Valentine", "Valenzuela", "Vance", "Vang",
          "Vargas", "Vasquez", "Vaughan", "Vaughn", "Vazquez", "Vega",
          "Velasquez", "Velazquez", "Velez", "Villa", "Villanueva",
          "Villarreal", "Villegas", "Vincent", "Wade", "Wagner",
          "Walker", "Wall", "Wallace", "Waller", "Walls", "Walsh",
          "Walter", "Walters", "Walton", "Wang", "Ward", "Ware",
          "Warner", "Warren", "Washington", "Waters", "Watkins",
          "Watson", "Watts", "Weaver", "Webb", "Weber", "Webster",
          "Weeks", "Weiss", "Welch", "Wells", "Werner", "West",
          "Wheeler", "Whitaker", "White", "Whitehead", "Whitney",
          "Wiggins", "Wilcox", "Wiley", "Wilkerson", "Wilkins",
          "Wilkinson", "Williams", "Williamson", "Willis", "Wilson",
          "Winters", "Wise", "Wolf", "Wolfe", "Wong", "Wood",
          "Woodard", "Woods", "Woodward", "Wright", "Wu", "Wyatt",
          "Yang", "Yates", "Yoder", "York", "Young", "Yu", "Zamora",
          "Zavala", "Zhang", "Zimmerman", "Zuniga"]

# from http://www.washingtonpost.com/wp-srv/special/politics/govbeat/streetnames/listof1000.html
SNAMES = ["100th", "110th", "120th", "130th", "140th", "150th",
          "160th", "20th", "21st", "22nd", "23rd", "24th", "25th",
          "26th", "27th", "28th", "29th", "30th", "31st", "32nd",
          "33rd", "34th", "35th", "36th", "37th", "38th", "39th",
          "40th", "41st", "42nd", "43rd", "44th", "45th", "46th",
          "47th", "48th", "50th", "52nd", "54th", "55th", "60th",
          "70th", "80th", "A", "Abbey", "Aberdeen", "Academy",
          "Access", "Acorn", "Adams", "Airport", "Alabama", "Albert",
          "Alder", "Alexander", "Alice", "Allen", "Allison", "Alpine",
          "Amanda", "Amber", "Amherst", "Amy", "Anderson", "Andrew",
          "Andrews", "Ann", "Anthony", "Apache", "Apple", "Applewood",
          "Arbor", "Arlington", "Armstrong", "Arnold", "Arrowhead",
          "Arthur", "Ash", "Ashland", "Ashley", "Ashwood", "Aspen",
          "Aster", "Atlantic", "Auburn", "Augusta", "Austin",
          "Autumn", "Avalon", "Avon", "Azalea", "B", "Bailey",
          "Baker", "Baldwin", "Balsam", "Barbara", "Barnes", "Barton",
          "Bass", "Bay", "Bayberry", "Bayview", "Beach", "Beacon",
          "Bear", "Beaver", "Bedford", "Beech", "Beechwood", "Bell",
          "Belmont", "Bennett", "Benton", "Berkshire", "Berry",
          "Beverly", "Birch", "Birchwood", "Bishop", "Blackberry",
          "Blair", "Blake", "Blossom", "Blue Heron", "Blue Jay", "Blue Ridge",
          "Blueberry", "Bluebird", "Bluff", "Bond", "Bonnie",
          "Boulder", "Boxwood", "Boyd", "Bradford", "Bradley",
          "Branch", "Brandon", "Brentwood", "Briar", "Briarwood",
          "Bridge", "Brighton", "Bristol", "Brittany", "Broad",
          "Broadway", "Brook", "Brooks", "Brookside", "Brookwood",
          "Brown", "Bruce", "Bryan", "Bryant", "Buchanan", "Buck",
          "Buckeye", "Buckingham", "Buena Vista", "Buffalo", "Burns",
          "Burton", "Bush", "Butler", "Butternut", "C", "Cactus",
          "Caldwell", "California", "Cambridge", "Camden", "Camelot",
          "Cameron", "Camp", "Campbell", "Canal", "Cannon",
          "Canterbury", "Canyon", "Cardinal", "Carlton", "Carol",
          "Carolina", "Carpenter", "Carriage", "Carroll", "Carson",
          "Carter", "Carver", "Cascade", "Castle", "Catherine",
          "Cedar", "Cedar Ridge", "Cedarwood", "Cemetery",
          "Centennial", "Center", "Central", "Chandler", "Chapel",
          "Chapman", "Charles", "Charlotte", "Chase", "Chatham",
          "Chelsea", "Cherokee", "Cherry", "Chester", "Chestnut",
          "Cheyenne", "Chippewa", "Choctaw", "Christopher", "Church",
          "Churchill", "Cindy", "Circle", "Clark", "Clay", "Clayton",
          "Clearview", "Clearwater", "Cleveland", "Cliff", "Clifton",
          "Clinton", "Clover", "Club", "Clubhouse", "Cobblestone",
          "Cole", "Coleman", "College", "Collins", "Colonial",
          "Colony", "Colorado", "Columbia", "Columbus", "Comanche",
          "Commerce", "Commercial", "Concord", "Cook", "Cooper",
          "Coral", "Cornell", "Cottage", "Cottonwood", "Country",
          "Country Club", "Countryside", "County", "County Line",
          "Court", "Courtney", "Cove", "Coventry", "Cox", "Coyote",
          "Craig", "Crane", "Crawford", "Creek", "Creekside",
          "Crescent", "Crest", "Crestview", "Crestwood", "Cross",
          "Crown", "Crystal", "Cumberland", "Curtis", "Cypress", "D",
          "Daisy", "Dakota", "Dale", "Dallas", "Dana", "Daniel",
          "David", "Davis", "Dawn", "Dawson", "Dean", "Deer", "Deer Creek",
          "Deer Run", "Deerfield", "Deerwood", "Delaware",
          "Delta", "Dennis", "Depot", "Derby", "Devon", "Devonshire",
          "Dewey", "Diamond", "Division", "Dixie", "Dixon", "Doe",
          "Dogwood", "Donna", "Dorothy", "Douglas", "Dove", "Dover",
          "Downing", "Drake", "Driftwood", "Duke", "Duncan", "Dunn",
          "E", "Eagle", "Earl", "East", "Eastwood", "Easy", "Echo",
          "Eden", "Edgewater", "Edgewood", "Edward", "Edwards",
          "Eighteenth", "Eighth", "Eleventh", "Elizabeth", "Elk",
          "Elliott", "Ellis", "Elm", "Elmwood", "Emerald", "Emerson",
          "Emily", "Enterprise", "Erie", "Essex", "Euclid", "Evans",
          "Everett", "Evergreen", "F", "Fairfield", "Fairview",
          "Fairway", "Falcon", "Farm", "Fawn", "Ferguson", "Fern",
          "Fernwood", "Field", "Fieldstone", "Fifteenth", "Fifth",
          "Finch", "Fir", "First", "Fisher", "Flint", "Florence",
          "Florida", "Ford", "Forest", "Fork", "Forrest", "Foster",
          "Fountain", "Fourteenth", "Fourth", "Fowler", "Fox", "Fox Run",
          "Francis", "Franklin", "Frederick", "Freedom",
          "Freeman", "Fremont", "Friendship", "Front", "Frontage",
          "Frontier", "Fuller", "Fulton", "G", "Garden", "Gardner",
          "Garfield", "Garrett", "Gary", "Gateway", "George",
          "Georgia", "Gibson", "Gilbert", "Glen", "Glendale", "Glenn",
          "Glenwood", "Golden", "Gordon", "Grace", "Graham", "Grand",
          "Grandview", "Granite", "Grant", "Gray", "Green", "Green Acres",
          "Greenbriar", "Greenfield", "Greenway", "Greenwood",
          "Gregory", "Griffin", "Grove", "H", "Hale", "Hall",
          "Hamilton", "Hampton", "Hancock", "Hanover", "Harbor",
          "Harding", "Harmony", "Harper", "Harris", "Harrison",
          "Hart", "Harvard", "Harvest", "Harvey", "Hastings", "Haven",
          "Hawk", "Hawkins", "Hawthorne", "Hayes", "Hazel", "Heather",
          "Helen", "Hemlock", "Henderson", "Henry", "Heritage",
          "Heron", "Hickory", "Hidden Valley", "High", "Highland",
          "Highview", "Hill", "Hillcrest", "Hillside", "Hilltop",
          "Hillview", "Holiday", "Holland", "Holly", "Holmes",
          "Homestead", "Honeysuckle", "Hoover", "Hope", "Horizon",
          "Horseshoe", "Houston", "Howard", "Howell", "Huckleberry",
          "Hudson", "Hughes", "Hummingbird", "Hunt", "Hunter",
          "Hunters", "Huntington", "Huron", "Illinois",
          "Independence", "Indian", "Indiana", "Industrial",
          "Inverness", "Iowa", "Iris", "Ironwood", "Iroquois",
          "Island", "Ivy", "Jackson", "Jade", "James", "Jasmine",
          "Jason", "Jasper", "Jay", "Jefferson", "Jenkins",
          "Jennifer", "John", "Johnson", "Jones", "Jordan", "Joseph",
          "Joy", "Joyce", "Juniper", "Kansas", "Karen", "Kay",
          "Kelly", "Kennedy", "Kensington", "Kent", "Kentucky",
          "Kenwood", "Keystone", "Kimberly", "King", "Kings",
          "Kingston", "Knight", "Knollwood", "Knox", "Lafayette",
          "Lake", "Lake Shore", "Lake View", "Lakeshore", "Lakeside",
          "Lakeview", "Lakewood", "Lancaster", "Lane", "Lark",
          "Larkspur", "Laura", "Laurel", "Lawrence", "Lee", "Leonard",
          "Leslie", "Lewis", "Lexington", "Liberty", "Lilac",
          "Lincoln", "Linda", "Linden", "Lindsey", "Linwood", "Lisa",
          "Little", "Live Oak", "Locust", "Logan", "Lois", "Long",
          "Longview", "Lookout", "Loop", "Louise", "Lynn", "Madison",
          "Magnolia", "Main", "Mallard", "Manchester", "Manor",
          "Maple", "Maplewood", "Margaret", "Marie", "Marigold",
          "Marina", "Marion", "Mark", "Market", "Marsh", "Marshall",
          "Martha", "Martin", "Mary", "Maryland", "Mason", "Maxwell",
          "May", "McDonald", "McKinley", "Meadow", "Meadowbrook",
          "Meadowlark", "Meadows", "Meadowview", "Melody", "Melrose",
          "Memorial", "Memory", "Meridian", "Mesa", "Mesquite",
          "Michael", "Michigan", "Middle", "Midway", "Mill", "Mill Creek",
          "Miller", "Mills", "Milton", "Mimosa", "Mission",
          "Mitchell", "Mockingbird", "Mohawk", "Monroe", "Montana",
          "Monterey", "Montgomery", "Moore", "Morgan", "Morningside",
          "Morris", "Morrison", "Morton", "Moss", "Mountain",
          "Mountain View", "Mulberry", "Murphy", "Murray", "Mustang",
          "Myers", "Myrtle", "Nancy", "Navajo", "Nelson", "New",
          "Newport", "Newton", "Nichols", "Nineteenth", "Ninth",
          "Noble", "Norman", "North", "Northwood", "Norwood",
          "Nottingham", "Oak", "Oak Grove", "Oak Hill", "Oak Ridge",
          "Oakdale", "Oakland", "Oakmont", "Oakridge", "Oakview",
          "Oakwood", "Ohio", "Old Mill", "Olive", "Oliver", "Opal",
          "Orange", "Orchard", "Oregon", "Oriole", "Osage", "Osprey",
          "Overlook", "Owens", "Oxford", "Pacific", "Page", "Palm",
          "Palmer", "Palmetto", "Palomino", "Paradise", "Park",
          "Parker", "Parkside", "Parkview", "Parkway", "Parkwood",
          "Partridge", "Patricia", "Patrick", "Patterson", "Patton",
          "Paul", "Pawnee", "Peach", "Peachtree", "Pearl", "Pebble",
          "Pecan", "Penn", "Pennsylvania", "Penny", "Perkins",
          "Perry", "Persimmon", "Peterson", "Pheasant", "Phillips",
          "Pierce", "Pike", "Pin Oak", "Pine", "Pine Grove", "Pine Hill",
          "Pine Ridge", "Pine Tree", "Pinecrest", "Pinehurst",
          "Pineview", "Pinewood", "Pioneer", "Plantation", "Plaza",
          "Pleasant", "Pleasant View", "Plum", "Plymouth", "Point",
          "Polk", "Pond", "Ponderosa", "Poplar", "Porter", "Powell",
          "Prairie", "Preston", "Price", "Primrose", "Princeton",
          "Prospect", "Providence", "Quail", "Quarry", "Railroad",
          "Rainbow", "Raintree", "Ranch", "Randall", "Randolph",
          "Raven", "Ray", "Raymond", "Rebecca", "Red Fox", "Red Oak",
          "Redbud", "Redwood", "Reed", "Remington", "Reynolds",
          "Rice", "Richard", "Richardson", "Richmond", "Ridge",
          "Ridgecrest", "Ridgeview", "Ridgeway", "Ridgewood", "Riley",
          "River", "Riverside", "Riverview", "Robert", "Roberts",
          "Robin", "Robinson", "Rock", "Rocky", "Rogers", "Rolling Hills",
          "Roosevelt", "Rose", "Rosewood", "Ross", "Royal",
          "Ruby", "Russell", "Ruth", "Ryan", "Sage", "Salem",
          "Sanders", "Sandpiper", "Sandstone", "Sandy", "Santa Fe",
          "Sarah", "Saratoga", "Savannah", "Sawmill", "Scenic",
          "School", "Scott", "Second", "Seminole", "Seneca",
          "Sequoia", "Serenity", "Seventeenth", "Seventh", "Shadow",
          "Shady", "Shamrock", "Shannon", "Sharon", "Shaw", "Shawnee",
          "Sheffield", "Shelby", "Sheridan", "Sherman", "Sherwood",
          "Shiloh", "Shirley", "Shore", "Short", "Sierra", "Silver",
          "Simmons", "Simpson", "Sioux", "Sixteenth", "Sixth",
          "Skyline", "Skyview", "Sleepy Hollow", "Smith", "Somerset",
          "South", "Sparrow", "Spencer", "Spring", "Spring Creek",
          "Spring Valley", "Springfield", "Spruce", "Stagecoach",
          "Stanley", "Star", "State", "Stephens", "Sterling",
          "Stevens", "Stewart", "Stone", "Stratford", "Strawberry",
          "Stuart", "Sullivan", "Summer", "Summit", "Sundance",
          "Sunflower", "Sunnyside", "Sunrise", "Sunset", "Sunshine",
          "Surrey", "Susan", "Sutton", "Swan", "Sycamore", "Sylvan",
          "Taft", "Tamarack", "Tanglewood", "Tara", "Taylor", "Teal",
          "Temple", "Tenth", "Terrace", "Terry", "Texas", "Third",
          "Thirteenth", "Thomas", "Thompson", "Tiffany", "Timber",
          "Timber Ridge", "Timberline", "Todd", "Tomahawk", "Tower",
          "Travis", "Trinity", "Tucker", "Tulip", "Turner", "Twelfth",
          "Twin Oaks", "Tyler", "Union", "University", "Valley",
          "Valley View", "Van Buren", "Vernon", "Victoria", "Victory",
          "View", "Villa", "Village", "Vine", "Violet", "Virginia",
          "Vista", "Wagner", "Walker", "Wall", "Wallace", "Walnut",
          "Walton", "Ward", "Warren", "Washington", "Water",
          "Waterford", "Watson", "Waverly", "Wayne", "Weaver", "Webb",
          "Webster", "Wedgewood", "Wellington", "Wells", "Wesley",
          "West", "Western", "Westminster", "Westview", "Westwood",
          "Wheeler", "Whippoorwill", "Whispering Pines", "White",
          "White Oak", "White Pine", "Whitetail", "Whitney",
          "Wilderness", "Wildflower", "Wildwood", "William",
          "Williams", "Williamsburg", "Willis", "Willow", "Willow Creek",
          "Wilson", "Winchester", "Windmill", "Windsor",
          "Winston", "Winter", "Wisteria", "Wolf", "Wood", "Woodbine",
          "Woodcrest", "Woodhaven", "Woodland", "Woodlawn",
          "Woodridge", "Woods", "Woodside", "Woodview", "Wren",
          "Wright", "Yale", "York", "Yorkshire", "Young"]

SNAMES2 = ["Street", "Road", "Avenue", "Lane", "Drive", "Circle", "Way"]

TIMEZONES = ["America/New_York", "America/Chicago", "America/Phoenix",
             "America/Los_Angeles"]

# business name generator.  adapted from
# http://online-generator.com/js/generators/business.js
BCOLORS = ["white", "black", "blue", "green", "silver", "indigo",
           "gold", "golden"]

BFIRST = ["alpha", "an", "ap", "beta", "big", "bio", "can", "d-",
          "dalt", "dam", "dan", "dento", "ding", "dom", "don", "don",
          "dong", "double", "drip", "duo", "e-", "fase", "fin",
          "free", "fresh", "fun", "ganja", "ganz", "geo", "gogo",
          "good", "grave", "groove", "hat", "hay", "hot", "hot",
          "hot", "ice", "inch", "indi", "j-", "jay", "joy", "k-",
          "kan", "kay", "key", "kin", "kon", "konk", "lam", "lat",
          "lexi", "lot", "mat", "math", "med", "move", "nam", "new",
          "nim", "o-", "onto", "ope", "open", "over", "ozer", "phys",
          "quad", "quo", "qvo", "ran", "ran", "rank", "re", "red",
          "ron", "round", "run", "sail", "salt", "san", "san", "sao",
          "scot", "sil", "silicon", "single", "sol", "solo", "son",
          "span", "stan", "stat", "stim", "strong", "sum", "sun",
          "tam", "tamp", "tan", "temp", "tin", "ton", "tran", "trans",
          "tree", "tres", "trio", "tripple", "tris", "true", "trust",
          "u-", "una", "uni", "uno", "vaia", "vento", "via", "vila",
          "villa", "viva", "vol", "volt", "voya", "x-", "xx-", "xxx-",
          "y-", "year", "zaam", "zam", "zath", "zen", "zer", "zim",
          "zon", "zoo", "zot", "zum", "zumma", "zun"]
BMIDDLE = ["alpha", "an", "ap", "beta", "bio", "can", "dam", "dan",
           "din", "dom", "don", "don", "dub", "fin", "geo", "go",
           "hat", "hot", "hot", "jay", "joy", "kay", "key", "lam",
           "lat", "lot", "mat", "med", "nam", "nim", "ot", "ove",
           "oze", "quad", "quo", "qvo", "ran", "ran", "red", "rem",
           "ron", "run", "sail", "san", "san", "sao", "sil", "sol",
           "solo", "son", "stat", "sun", "tam", "tan", "tin", "ton",
           "trans", "trax", "trip", "una", "uni", "uno", "via",
           "viva", "vol", "zaam", "zam", "zar", "zat", "zen", "zim",
           "zoo", "zoom", "zot", "zoz", "zum", "zun"]
BLAST = ["com", "core", "dax", "dex", "dexon", "dom", "dox", "ex",
         "fan", "find", "green", "ing", "ity", "kix", "lax", "lex",
         "lux", "nix", "phase", "ron", "tam", "tax", "taxon",
         "technology", "tex", "texon", "tom", "tone", "trax", "zap",
         "zone"]
BBOTH = ["bam", "base", "can", "cane", "care", "city", "con", "cone",
         "fase", "fax", "fix", "how", "ice", "in", "is", "it", "la", "lab",
         "lam", "lane", "line", "quote", "tech", "techi", "techno", "zoom"]

### helpers

# zip codes and related details
Zip = namedtuple("Zip", ["zip", "city", "state", "country"])
# bank_account_tbl rows
BankAccount = namedtuple("BankAccount",
                         ["depository_name", "city", "state", "zip_code",
                          "country", "bank_routing_number",
                          "bank_account_nickname"])


def getzips(fname):
    """Read zip codes and related city/state info from a CSV-formatted file."""

    retls = []
    with open(fname) as fzip:
        for row in reader(fzip):
            if (row[1] == 'STANDARD' and
                row[0].strip() and row[2].strip() and row[3].strip()):
                retls.append(Zip(zip=row[0], city=capwords(row[2]), state=row[3], country='USA'))

    return retls


def getcustbank(cursor):
    """Return a list of unique customer_bank_id values from ach_debit_txn_tbl"""

    cursor.execute("select distinct customer_bank_id from ach_debit_txn_tbl")

    return [row[0] for row in cursor.fetchall()]


def getbankaccounts(cursor):
    """Return a list of 100 randomly selected bank account rows."""

    def clean(name):
        """Perform a little cleanup on name fields."""
        return SPACES.sub(" ",
                          "".join([char for char in name
                                   if char not in digits]).strip())

    retls = []

    cursor.execute("""select * from bank_account_tbl
    order by random() limit 100""")
    for row in cursor.fetchall():
        retls.append(BankAccount(depository_name=clean(row["depository_name"]),
                                 city=row["city"],
                                 state=row["state"],
                                 zip_code=row["zip_code"],
                                 country=row["country"],
                                 bank_routing_number=row["bank_routing_number"],
                                 bank_account_nickname=clean(row["bank_account_nickname"])))

    return retls


def randfromexisting(cursor, table, column, unique=True, limit=None):
    """Return a random row.

    Load all rows from the specified table (or X number of rows with unique
    value for specified column) and then randomly chooses from amongst those
    values.
    """

    if unique:
        column = "distinct %s" % column

    limitto = ""
    if limit and limit > 0:
        limitto = " limit %d" % limit

    cursor.execute("select %s from %s %s" % (column, table, limitto))
    rows = cursor.fetchall()

    return choice(cursor)


def randstr(length=10, lmin=None, lmax=None, source=DIGITS):
    """Return a random string of a certain length.

    If lmin and lmax are provided, length is set to a random number
    from lmin to lmax (inclusive).

    Set source to ascii_letters to return letters instead, or
    ALPHANUM to return a mix of letters and digits.

    """

    if lmin and lmax:
        length = randint(lmin, lmax)

    # sometimes the wanted length is longer than len(source), which
    # makes sample() throw an exception.
    # adjust length accordingly
    length = min(length, len(source))

    return ''.join(sample(source, length))


def randnumstr(lmin=1, lmax=12, length=2):
    """Return a number between min and max with a certain minimum length.

    The number is prefixed with zeros if necessary.
    """

    fmt = "%%0%dd" % length

    return fmt % randint(lmin, lmax)


def randname(namelist, rowid, maxlen=None):
    """Return a random from namelist with rowid appended.

    If maxlen is provided, return the last maxlen characters.
    """

    name = "%s%d" % (choice(namelist), rowid)
    if maxlen:
        return name[-maxlen:]

    return name


def randfullname(maxlen=None):
    """Return a random full name.

    If maxlen is provided, return the last maxlen characters.
    """

    if randint(0, 100) < 10:
        name = " ".join([choice(FNAMES), choice(ascii_uppercase), choice(LNAMES)])
    else:
        name = " ".join([choice(FNAMES), choice(LNAMES)])

    if maxlen:
        return name[-maxlen:]

    return name


def randcompanyname(cursor, maxlen=None):
    """Return a random company name.

    If maxlen is provided, return the last maxlen characters.
    """

    name = ""

    prefix = BFIRST[:]
    prefix.extend(BBOTH)
    prefix.extend(BCOLORS)

    suffix = BLAST[:]
    suffix.extend(BBOTH)

    middle = BMIDDLE[:]

    prename = choice(prefix).lower()
    midname = choice(middle).lower()
    sufname = choice(suffix).lower()

    percent = randint(0, 99)
    if percent < 25:
        if len(prename) + len(sufname) < 8:
            name = prename.capitalize() + midname + sufname
        else:
            name = prename.capitalize() + sufname
    elif percent >= 25 and percent < 30:
        if prename[1] != "-":
            name = prename.capitalize() + midname + "-" + sufname
        else:
            name = prename.capitalize() + sufname
    elif percent >= 30 and percent < 32:
        if prename[1] != "-":
            name = prename + "-" + sufname
        else:
            name = prename + sufname
    elif percent >= 32 and percent < 45:
        name = prename + sufname
    elif percent >= 45 and percent < 90:
        name = prename.capitalize() + sufname
    else:
        # expensive...
        cursor.execute("""select first_name, middle_name, last_name
        from cust_pers_tbl""")
        rows = cursor.fetchall()
        row = choice(rows)
        name = "%s %s %s" % (row["first_name"], row["middle_name"], row["last_name"])

    if len(name) > maxlen:
        return name[-maxlen:]

    return name



def randphone(maxlen=None):
    """Return a random phone number.

    https://en.wikipedia.org/wiki/North_American_Numbering_Plan#Numbering_system

    If maxlen is provided, return only that many digits.
    """

    npa = "%d%d%d" % (randint(2, 9), randint(0, 9), randint(0, 9))
    while True:
        nxx = "%d%d%d" % (randint(2, 9), randint(0, 9), randint(0, 9))
        if not nxx.endswith("11"):
            break
    phone = "%s-%s-%s" % (npa, nxx, randstr(4))

    if maxlen:
        if maxlen == 10:
            # most likely case.
            return phone.replace("-", "")
        return phone[-maxlen:]

    return phone


def randaddress(num, maxlen=None):
    """Return a random street address.

    If maxlen is provided, return the last maxlen characters.
    """

    address = "%s %s %s" % (num, choice(SNAMES), choice(SNAMES2))
    if maxlen:
        return address[-maxlen:]

    return address


def weightedchoice(optionslist, weightslist):
    """Returns a biased chosen value from optionslist.

    The length of the weightslist must be the same as the length of
    the optionslist.  sum(weightslist) == 100, each weight must be an
    integer.
    """

    assert len(optionslist) == len(weightslist)
    assert sum(weightslist) == 100
    assert all([val for val in weightslist if isinstance(val, int)])

    # this is not the greatest way to do it.
    ls = []
    for idx, val in enumerate(optionslist):
        ls.extend([val] * weightslist[idx])

    return choice(ls)


def randamount():
    """Returns a weighted amount value for the ach_debit_txn_tbl."""

    dollar = random()
    cents = random()
    amount = 0

    if dollar < 0.9:
        amount = randint(395, 1209)
    elif dollar > 0.9 and dollar < 0.95:
        amount = randint(0, 394)
    elif dollar > 0.95 and dollar < 1.0:
        amount = randint(1210, 9999)

    if cents < 0.1:
        amount = amount + (randint(1, 99) / 100.0)

    return amount


def randbankaccount(lmax):
    """Generate a random bank account number.

    lmax is either 15 or the length of the column, whichever is
    smaller.

    The bank account numbers range from 10^5 to 10^lmax - 1, but with a
    distribution of 25% eight digits long, 25% nine, 25% ten, and 25%
    other.

    """

    lmin = 5
    lmax = min(lmax, 15)

    randval = randint(1, 4)
    if randval == 1:
        return str(randint(10**lmin, 10**lmax - 1))
    elif randval == 2:
        length = min(lmax, 8)
    elif randval == 3:
        length = min(lmax, 9)
    elif randval == 4:
        length = min(lmax, 10)

    return str(randint(10**(length-1), 10**length - 1))


def random_us_time_zone():
    """Return a random US timezone."""

    return choice(TIMEZONES)


def rand_payment_identifier(cursor, rowid):
    """Return a random payment identifier if current value is over 6 digits."""

    cursor.execute("""select payment_identifier from expense_tbl
    where id = %s""", (rowid,))
    row = cursor.fetchone()
    if len(row["payment_identifier"]) <= 6:
        return row["payment_identifier"]

    return randnumstr()


def calculateccfee(cursor, rowid):
    """Calculate convenience fee for credit_card_payment_tbl.

    Also randomize subtotal and card_number values.
    """

    cursor.execute("""select card_number, subtotal from credit_card_payment_tbl
    where id = %s""", (rowid,))
    row = cursor.fetchone()
    if row["card_number"].startswith("3712"):
        fee = Decimal("0.035") * row["subtotal"]
    else:
        fee = Decimal("0.03") * row["subtotal"]

    cursor.execute("""update credit_card_payment_tbl
    set card_number = %s, subtotal = %s, convenience_fee = %s
    where id = %s""",
                   (randstr(24), randamount(), fee, rowid))


def custacctname(cursor, rowid):
    """Set name and external_name to a random company name in cust_acct_tbl."""

    # by the time we're at this stage, we don't know
    # what the column maxlen value is.  for now, use
    # 64, since that's the size of external_name.
    name = randcompanyname(cursor, 64)

    cursor.execute("""update cust_acct_tbl
    set name = %s, external_name = %s
    where id = %s""",
                   (name, name, rowid))


def filenameext(cursor, rowid):
    """Set filename suffix based on the mime_type."""

    cursor.execute("""select filename, mime_type from file_tbl
    where id = %s""", (rowid,))
    row = cursor.fetchone()

    suffix = ".raw"

    if row["mime_type"] == "application/pdf":
        suffix = ".pdf"
    elif row["mime_type"] == "audio/wav":
        suffix = ".wav"

    name = row["filename"] + suffix
    if len(name) > 255:
        name[-maxlen:]

    cursor.execute("""update file_tbl
    set filename = %s
    where id = %s""", (name, rowid))


def funcname(depth=1):
    """Return the name of a calling function as a string.

    The depth parameter specifies how far up the stack to look.

    Found at https://stackoverflow.com/a/5067654
    """

    return stack()[depth][3]


def logprocessed(table, rows):
    """Log how many rows were processed."""

    logging.info("table: %-40s rows processed: %8d", table, rows)


# eof

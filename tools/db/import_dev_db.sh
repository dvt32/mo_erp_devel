#!/bin/bash
sudo -u postgres createdb microoffice_devel

sudo -u postgres psql < database/dcl/create_roles.sql

sudo -u postgres psql -d microoffice_devel< ~/dump.sql

for f in database/dcl/*.sql
do
	echo "Applying $f"
	sudo -u postgres psql < $f
done




#!/usr/bin/env python3.4
"""Scramble rows in the MicroOffice database."""

import argparse
from configparser import ConfigParser
from datetime import datetime
from inspect import getmembers, isfunction
import logging
import sys

from psycopg2 import *
from psycopg2.extras import DictCursor

try:
    import helpers
    import scramblefns
except ImportError as exc:
    sys.stderr.write("cannot open scramblefns.py; maybe you need to generate it?\n")
    sys.stderr.write("exception: %s\n" % str(exc))
    sys.exit(1)


# defaults; change them in the properties file.
DBNAME = "microoffice"
DBHOST = "localhost"
DBUSER = "fn"
DBPASS = "."

# some tables need to be processed manually because they lack an "id" column.
def manualtables(cursor):
    """Blah."""

    # ['voicemail_msgs_tbl', 'voicemail_prefs_tbl', 'voicemail_prefs']

    print("TODO: manual tables")


def main():
    """Main branching logic."""

    parser = argparse.ArgumentParser(description="Scramble database tables")
    parser.add_argument("-c", "--config", nargs="?",
                        type=argparse.FileType("r"),
                        help="read a config file")
    parser.add_argument("-n", "--no-commit", action="store_true", dest="skip",
                        help="rollback the transaction, even if there are no errors.")

    args = parser.parse_args()
    config = ConfigParser(default_section="scramble")
    # fake section name
    props = "[root]\n"
    if args.config:
        props = props + args.config.read()
    config.read_string(props)

    cnx = connect(database=config["root"].get("scramble_dbname", DBNAME),
                  user=config["root"].get("scramble_dbuser", DBUSER),
                  password=config["root"].get("scramble_dbpass", DBPASS),
                  host=config["root"].get("scramble_dbhost", DBHOST))
    cursor = cnx.cursor(cursor_factory=DictCursor)

    logging.info("starting...")

    # this is a little annoying.  we want to call all the functions in
    # scramblefns, but since scramblefns imports all the functions
    # from helpers, we need to exclude them.
    hfuncs = set([o[0] for o in getmembers(helpers) if isfunction(o[1])])
    functions = [o for o in getmembers(scramblefns) if isfunction(o[1]) and o[0] not in hfuncs]

    failed = False

    # keep a list of tables we've already scrambled, so we don't
    # scramble them again.  this is needed for cust_pers_tbl, since
    # it's scrambled from cust_acct_tbl
    done = set()

    manualtables(cursor)
    for function in functions:
        if function[0] not in done:
            try:
                function[1](cursor, function[0], done)
            except (ProgrammingError, DataError, IntegrityError) as exc:
                logging.error("something went wrong: %s", str(exc))
                failed = True
                cnx.rollback()
        else:
            # logging.info("skipping %s; already scrambled" % function[0])
            pass

    if args.skip and not failed:
        cnx.rollback()
        logging.critical("NOTE: everything worked!")
        logging.critical("NOTE: --no-commit was specified; not committing anything to the database.")
    elif failed:
        logging.critical("NOTE: something failed; not committing transaction.")
        cnx.rollback()
    else:
        cnx.commit()
        logging.info("done.")

    cursor.close()
    cnx.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
    main()

# eof

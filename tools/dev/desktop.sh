#!/bin/sh
sed -i 's/main/main contrib/g' /etc/apt/sources.list
apt-get update
apt-get install task-lxde-desktop xvfb chromium x11vnc xfce4 ttf-mscorefonts-installer
echo "$(router_ipv4_addr) router-122w27f10.microoffice.com" >> /etc/hosts
Xvfb :1 -screen 0 1440x900x24 &
DISPLAY=:1.0 xfce4-session &
x11vnc -display :1.0 -nopw -listen $IPADDR

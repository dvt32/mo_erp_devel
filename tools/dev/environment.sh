MYHOST=`hostname`
MYDOMAIN=microoffice
BSHOST=fedora.int.microoffice.com
export DEBIAN_FRONTEND=noninteractive
apt -y install emacs-nox fail2ban ipset tmux
timedatectl set-timezone America/New_York
cat <<__EOF__ | debconf-set-selections -v
locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8
locales locales/default_environment_locale select en_US.UTF-8
__EOF__
update-locale LANG=en_US.UTF-8
dpkg-reconfigure -f noninteractive locales
curl --remote-time http://$BSHOST/bootstrap/authorized_keys >> /root/.ssh/authorized_keys
for i in bash_profile emacs forward tmux.conf; do curl --output "/root/.$i" --remote-time http://$BSHOST/bootstrap/$MYDOMAIN/.$i; done
chmod 0644 /root/.{bash_profile,emacs,tmux.conf}
chmod 0600 /root/.forward
# set window name in tmux to hostname
if ! grep -q '^PROMPT_COMMAND=' /etc/bash.bashrc; then
  cp -a /etc/bash.bashrc /etc/bash.bashrc.orig
  cat <<'__EOF__' >> /etc/bash.bashrc
# only display hostname in window title
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${HOSTNAME%%.*}\007"'
    ;;
screen*)
    PROMPT_COMMAND='echo -ne "\033k${HOSTNAME%%.*}\033\\"'
    ;;
*)
    ;;
esac
__EOF__
fi
cat <<__EOF__ > /etc/fail2ban/jail.local
[sshd]
enabled=true
action=iptables-ipset-proto6-allports[name={MYHOST}-ssh,bantime=86400]
# 1 day. also need to update bantime in iptables-ipset-proto6-allports action above
bantime=86400
__EOF__
sed -i s/{MYHOST}/$MYHOST/g /etc/fail2ban/jail.local
git config --global user.name "NAME"
git config --global user.email "EMAIL"
apt-get install build-essential python-dev automake autoconf libtool libssl-dev pkg-config
git clone https://github.com/facebook/watchman.git
cd watchman
git checkout v4.9.0
./autogen.sh
./configure
make
make install
mv watchman /usr/local/bin/watchman
cd ~
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
source ~/.nvm/nvm.sh
nvm install node
NODEV=`nvm current`
/root/.nvm/versions/node/$NODEV/bin/npm install -g nuclide
sed -i 's#subject=\.\*/CN=(\[\^/\\n\]\*)#subject=\.\*/?CN ?= ?(\[\^/\\n\]\*)#g' ~/.nvm/versions/node/$NODEV/lib/node_modules/nuclide/pkg/nuclide-server/scripts/nuclide_certificates_generator.py

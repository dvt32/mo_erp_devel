#!/bin/bash

# Create Debian 9 KVM guest.
# Supports hosts running CentOS 7 and Debian 9.

show_help() {
  cat <<__EOF__
usage: $SCRIPT_NAME --guest=x --imagesize=x --bridgedev=x --rootpwd=x [-h|--help] [--httpproxy=x]

Create Debian 9 KVM guest using preseed.cfg file.

required arguments:
  --guest      name of the guest
  --bridgedev  network bridge device

optional arguments:
  -h, --help   show this help message and exit
  --ram        memory in the guest in MB, default 2048 MB (2 GB)
  --imagesize  size of the qcow2 disk image, e.g. 16G, 64G, 96G; default 16G
  --rootpwd    root password in the guest, default randomly generated
  --httpproxy  URL of HTTP proxy

e.g.:
  $SCRIPT_NAME --guest mydebian --imagesize 96G --bridgedev brvlan11 --rootpwd mypassword --httpproxy http://aptcache.sna8.microoffice.com:3142
__EOF__
}
SCRIPT_NAME=`basename $0`
# if no arguments, display help.
if [ $# -lt 1 ]; then
  show_help
  exit 0
fi
QEMU_IMG=`which qemu-img`
if [ -z "$QEMU_IMG" ]; then
  echo missing required qemu-img command
  exit 1
fi
VIRT_INSTALL=`which virt-install`
if [ -z "$VIRT_INSTALL" ]; then
  echo missing required virt-install command
  exit 1
fi
GETOPT=`which getopt`
if [ -z "$GETOPT" ]; then
  echo missing required getopt command
  exit 1
fi
getopt --test > /dev/null
if [[ $? != 4 ]]; then
  echo "I’m sorry, `getopt --test` failed in this environment."
  exit 1
fi
SHORTOPT=h
LONGOPT=help,guest:,ram:,imagesize:,bridgedev:,rootpwd:,httpproxy:
# -temporarily store output to be able to check for errors
# -activate advanced mode getopt quoting e.g. via “--options”
# -pass arguments only via   -- "$@"   to separate them correctly
PARSEDOPT=`getopt --options $SHORTOPT --longoptions $LONGOPT --name "$0" -- "$@"`
if [[ $? != 0 ]]; then
  # e.g. $? == 1
  #  then getopt has complained about wrong arguments to stdout
  exit 2
fi
# use eval with "$PARSEDOPT" to properly handle the quoting
eval set -- "$PARSEDOPT"
# now parse options
while true; do
  case "$1" in
    -h|--help)
      show_help
      exit 0
      ;;
    --guest)
      GUEST="$2"
      shift 2
      ;;
    --ram)
      RAM="$2"
      shift 2
      ;;
    --imagesize)
      IMAGESIZE="$2"
      shift 2
      ;;
    --bridgedev)
      BRIDGEDEV="$2"
      shift 2
      ;;
    --rootpwd)
      ROOTPWD="$2"
      shift 2
      ;;
    --httpproxy)
      HTTPPROXY="$2"
      shift 2
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "Programming error"
      exit 3
      ;;
  esac
done
if [ -z "$GUEST" ]; then
  echo missing required --guest option
  exit 1
fi
if [ -z "$RAM" ]; then
  RAM=2048
fi
if [ -z "$IMAGESIZE" ]; then
  IMAGESIZE=16G
fi
if [ -z "$BRIDGEDEV" ]; then
  echo missing required --bridgedev option
  exit 1
fi
if [ -z "$ROOTPWD" ]; then
  PWGEN=`which pwgen`
  if [ -z "$PWGEN" ]; then
    echo missing required pwgen command - needed when ROOTPWD is omitted
    exit 1
  fi
  ROOTPWD=`pwgen -s 16 1`
  # set flag indicating that we generated the root password; later if flag is true, then we print the password 
  ROOTPWD_GEN=1
fi
if [ -z "$HTTPPROXY" ]; then
  PROXYSLASH=
else
  PROXYSLASH=$HTTPPROXY/
fi
qemu-img create -f qcow2 -o preallocation=metadata,lazy_refcounts=on /var/lib/libvirt/images/$GUEST.qcow2 $IMAGESIZE
chmod 0600 /var/lib/libvirt/images/$GUEST.qcow2
# see https://www.debian.org/releases/stable/i386/apbs03.html.en
# see https://www.debian.org/releases/stretch/example-preseed.txt
cat <<__EOF__ > preseed.cfg
#_preseed_V1
d-i debian-installer/locale string en_US
d-i keyboard-configuration/xkb-keymap select us
d-i netcfg/choose_interface select auto
d-i netcfg/get_hostname string unassigned-hostname
d-i netcfg/get_domain string unassigned-domain
d-i netcfg/hostname string $GUEST
d-i netcfg/use_autoconfig boolean true
d-i mirror/country string manual
d-i mirror/http/hostname string http.us.debian.org
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string $HTTPPROXY
d-i clock-setup/utc boolean true
d-i time/zone string US/Eastern
d-i clock-setup/ntp boolean true
d-i passwd/make-user boolean false
d-i passwd/root-password password $ROOTPWD
d-i passwd/root-password-again password $ROOTPWD
tasksel tasksel/first multiselect standard, ssh-server
d-i debian-installer/main-menu select Finish the installation
d-i pkgsel/upgrade select full-upgrade
d-i finish-install/reboot_in_progress note
d-i debconf/priority select high
d-i popularity-contest/participate boolean false
d-i partman-auto/method string regular
d-i partman-auto/disk string /dev/vda
d-i partman-auto/expert_recipe string \\
  root-only :: \\
    500 10000 -1 ext4 \\
      \$primary{ } \$bootable{ } \\
      method{ format } format{ } \\
      use_filesystem{ } filesystem{ ext4 } \\
      mountpoint{ / } \\
    .
partman-basicfilesystems partman-basicfilesystems/no_swap boolean false
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/confirm_nooverwrite boolean true
d-i grub-installer/only_debian boolean true
d-i grub-installer/bootdev string /dev/vda
d-i preseed/late_command string in-target sed -i s/"GRUB_CMDLINE_LINUX_DEFAULT=\\"quiet\\""/"GRUB_CMDLINE_LINUX_DEFAULT=\\"quiet console=ttyS0\\""/g /etc/default/grub; in-target update-grub; in-target sed -i s/"#PermitRootLogin prohibit-password"/"PermitRootLogin yes"/g /etc/ssh/sshd_config
__EOF__
# consult https://www.debian.org/mirror/list for location URLs if other mirrors needed
# omit --initrd-inject=preseed.cfg if preseed is not being used
virt-install --name $GUEST --ram 2048 --disk path=/var/lib/libvirt/images/$GUEST.qcow2,format=qcow2 --vcpus 2 --os-type linux --os-variant debiantesting --network bridge=$BRIDGEDEV --graphics vnc --location http://${PROXYSLASH}http.us.debian.org/debian/dists/stable/main/installer-amd64/ --extra-args 'console=ttyS0' --initrd-inject=preseed.cfg
if [ "$ROOTPWD_GEN" == "1" ]; then
  echo Generated root password: $ROOTPWD
fi

#!/bin/bash
apt-get install git make curl
mkdir --mode=0700 /root/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfxoap+SzXNUymGXnI4048kjF+Hr9FFQTrrDqSxdlcIwWLL/u22IaatiB8iQiBfXqGXymUuy0vuVz+p+Swu+W6gtqxITWvhvPsixeLFMHv2JbO2U7vqLhTgzMW10FJJpNtMd23cHGqqLHGiLgPE70fs0pK1YWzz20krKFqRTj83D5cXF5CWRSoHpv7EBUDntDrhYBbcNVT3zUUbiJ+eER0Sc5VHT3rCYqLxocgzn5bggfOV/TxNsZ0Hcxm6xZlyCyD10OQ5Hsodw13LbegIYqhYLA+sgfMeTa0VLKA0Sp14bz2J8L7nCZLo3q2iauyoEImzai9OOmnRNNMhhpA9sxP Jacob@Jacobs-MacBook-Air.local
" >> ~/.ssh/authorized_keys
chmod 0600 /root/.ssh/authorized_keys
BSHOST=fedora.int.microoffice.com
mkdir /root/bin
for i in ssh-reagent; do curl --output "/root/bin/$i" --remote-time http://$BSHOST/bootstrap/bin/$i; done
chmod 0700 /root/bin/ssh-reagent

source ~/bin/ssh-reagent
mkdir projects
cd projects
git clone git@bitbucket.org:wchao/mo_erp.git

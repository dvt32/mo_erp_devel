#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI qw(:sql_types);
use Mail::Sender;
use FindBin;
use lib "$FindBin::Bin/../lib";
use get_row_list;
use get_row_by_id_map;

main();

sub init_database() {
  my $database_name = "microoffice";
  my $database_hostname = "kleene.microoffice.com";
  my $data_source = "dbi:Pg:dbname=$database_name;host=$database_hostname;sslmode=require";
  my $database_username = "mo_report";
  my $database_password = "***";
  my $dbh = DBI->connect($data_source, $database_username, $database_password);
  return $dbh;
}

sub main() {
  my $dbh = init_database();
  my $sql_query = "select * from prospective_customer_tbl where follow_up_date = current_date and reminder_email_address is not null and reminder_email_address <> ? order by last_name, first_name, company";
  my @sql_param_list = ("");
  my @prospective_customer_list;
  get_row_list($dbh, $sql_query, \@sql_param_list, \@prospective_customer_list);
  $sql_query = "select sales_interaction_tbl.* from sales_interaction_tbl, prospective_customer_tbl where prospective_customer_tbl.follow_up_date = current_date and prospective_customer_tbl.reminder_email_address is not null and prospective_customer_tbl.reminder_email_address <> ? and sales_interaction_tbl.prospective_customer_id = prospective_customer_tbl.id";
  @sql_param_list = ("");
  my (%sales_interaction_by_id);
  get_row_by_id_map($dbh, $sql_query, \@sql_param_list, \%sales_interaction_by_id);
  $dbh->disconnect();
  my %sales_interaction_id_list_by_prospective_customer_id;
  for my $sales_interaction_id (keys(%sales_interaction_by_id)) {
    my $sales_interaction = $sales_interaction_by_id{$sales_interaction_id};
    push(@{$sales_interaction_id_list_by_prospective_customer_id{$sales_interaction->{"prospective_customer_id"}}}, $sales_interaction_id);
  }
  my $mail_sender = Mail::Sender->new({
    "smtp" => "localhost",
    "from" => qq{"Sales Follow-up Reminder" <sales\@microoffice.com>},
  });
  for my $prospective_customer (@prospective_customer_list) {
    my $lead_url = qq{https://operations.microoffice.com/sales/show_prospect_detail.cgi?id=} . $prospective_customer->{"id"};
    my $reminder_email_address = $prospective_customer->{"reminder_email_address"};
    my $subject = "Follow-up reminder ";
    $subject .= "(";
    $subject .= $prospective_customer->{"first_name"} . " " . $prospective_customer->{"last_name"};
    if ($prospective_customer->{"company"}) {
      $subject .= ", ";
      $subject .= $prospective_customer->{"company"};
    }
    $subject .= ")";
    my @name_component_list;
    if ($prospective_customer->{"salutation"}) {
      push(@name_component_list, $prospective_customer->{"salutation"});
    }
    push(@name_component_list, $prospective_customer->{"first_name"});
    if ($prospective_customer->{"middle_name"}) {
      push(@name_component_list, $prospective_customer->{"middle_name"});
    }
    push(@name_component_list, $prospective_customer->{"last_name"});
    if ($prospective_customer->{"suffix"}) {
      push(@name_component_list, $prospective_customer->{"suffix"});
    }
    my $name = join(" ", @name_component_list);
    my $title_and_company = "";
    if ($prospective_customer->{"title"}) {
      $title_and_company = $prospective_customer->{"title"} . ", " . $prospective_customer->{"company"};
    } else {
      $title_and_company = $prospective_customer->{"company"} || "";
    }
    my $address = $prospective_customer->{"address_1"};
    if ($prospective_customer->{"address_2"}) {
      $address .= ", " . $prospective_customer->{"address_2"};
    }
    my $city_state_zip = "";
    if ($prospective_customer->{"city"} && $prospective_customer->{"state"}) {
      $city_state_zip .= $prospective_customer->{"city"} . ", " . $prospective_customer->{"state"};
    } elsif ($prospective_customer->{"city"}) {
      $city_state_zip .= $prospective_customer->{"city"};
    } elsif ($prospective_customer->{"state"}) {
      $city_state_zip .= $prospective_customer->{"state"};
    }
    if ($prospective_customer->{"zip_code"}) {
      $city_state_zip .= " " . $prospective_customer->{"zip_code"};
    }
    if (($prospective_customer->{"country"}) && ($prospective_customer->{"country"} ne "United States")) {
      $city_state_zip .= " " . $prospective_customer->{"country"};
    }
    my @space_needed_component_list;
    if ($prospective_customer->{"amount_of_space"}) {
      push(@space_needed_component_list, $prospective_customer->{"amount_of_space"});
    }
    if ($prospective_customer->{"start_date"}) {
      push(@space_needed_component_list, "start " . $prospective_customer->{"start_date"});
    }
    if ($prospective_customer->{"duration_of_rental"}) {
      push(@space_needed_component_list, "term " . $prospective_customer->{"duration_of_rental"});
    }
    my $space_needed = join(", ", @space_needed_component_list);
    my $business_description = $prospective_customer->{"business_description"} || "";
    my $work_phone_number = $prospective_customer->{"work_phone_number"} || "";
    my $home_phone_number = $prospective_customer->{"home_phone_number"} || "";
    my $cell_phone_number = $prospective_customer->{"cell_phone_number"} || "";
    my $other_phone_number = $prospective_customer->{"other_phone_number"} || "";
    my $fax_number = $prospective_customer->{"fax_number"} || "";
    my $email_address = $prospective_customer->{"email_address"} || "";
    my $web_site = $prospective_customer->{"web_site"} || "";
    my $desired_location = $prospective_customer->{"desired_location"} || "";
    my $marketing_source = $prospective_customer->{"marketing_source"} || "";
    my $initial_contact_dt = $prospective_customer->{"initial_contact_dt"} || "";
    my $remote_host = $prospective_customer->{"remote_host"} || "";
    my $initial_page_url = $prospective_customer->{"initial_page_url"} || "";
    my $referrer_url = $prospective_customer->{"referrer_url"} || "";
    my $notes = $prospective_customer->{"notes"} || "";
    my $msg_content = <<"---END_BLOCK---;";
The following lead needs follow-up:
$lead_url

        Name: $name
Title/Compan: $title_and_company
 Description: $business_description
     Address: $address
            : $city_state_zip
  Work Phone: $work_phone_number
  Home Phone: $home_phone_number
  Cell Phone: $cell_phone_number
 Other Phone: $other_phone_number
  Fax Number: $fax_number
  Email Addr: $email_address
    Web Site: $web_site
Space Needed: $space_needed
    Location: $desired_location
 Mktg Source: $marketing_source
Request Time: $initial_contact_dt
Initial Page: $initial_page_url
Referrer URL: $referrer_url
 Remote Host: $remote_host

Notes
=====
$notes
---END_BLOCK---;
    my @sorted_sales_interaction_id_list = sort {
      my $sales_interaction_a = $sales_interaction_by_id{$a};
      my $sales_interaction_b = $sales_interaction_by_id{$b};
      $sales_interaction_b->{"interaction_dt"} cmp $sales_interaction_a->{"interaction_dt"};
    } @{$sales_interaction_id_list_by_prospective_customer_id{$prospective_customer->{"id"}}};
    if (scalar(@sorted_sales_interaction_id_list)) {
      $msg_content .= <<"---END_BLOCK---;";

Interactions
============
---END_BLOCK---;
    }
    for my $sales_interaction_id (@sorted_sales_interaction_id_list) {
      my $sales_interaction = $sales_interaction_by_id{$sales_interaction_id};
      $msg_content .= <<"---END_BLOCK---;";

$sales_interaction->{"interaction_dt"} $sales_interaction->{"interaction_type"} $sales_interaction->{"salesperson"}:
$sales_interaction->{"notes"}
---END_BLOCK---;
    }
    $mail_sender->MailMsg({
      "to" => $reminder_email_address,
      "subject" => $subject,
      "msg" => $msg_content,
    });
  }
}

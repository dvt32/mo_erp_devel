#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI;
use Mail::Sender;

# Used as a range
use constant RENEWALS_REMINDER => [0, 40];
use constant RENEWALS_REMINDER2 => [41, 70];
use constant INCREASE_REMINDER => 40;
use constant RENEWALS_RECIPIENT => "renewals\@microoffice.com";
use constant INCREASE_RECIPIENT => "renewals\@microoffice.com";

main();

sub init_database() {
  my $database_name = "microoffice";
  my $database_hostname = "kleene.microoffice.com";
  my $data_source = "dbi:Pg:dbname=$database_name;host=$database_hostname;sslmode=require";
  my $database_username = "mo_report";
  my $database_password = "***";
  my $dbh = DBI->connect($data_source, $database_username, $database_password);
  return $dbh;
}

sub main {
  my $dbh = init_database();
  my $mail_sender = Mail::Sender->new({
    "smtp" => "localhost",
    "from" => qq{"Renewal Reminder" <sales\@microoffice.com>},
  });
  my $states = $dbh->selectall_arrayref("select distinct state from building_tbl");
  foreach (@$states) {
    my $current_state = $_->[0];

    # agreement renewals
    send_renewals($dbh, $mail_sender, $current_state, RENEWALS_REMINDER );
    send_renewals($dbh, $mail_sender, $current_state, RENEWALS_REMINDER2);

    # increase rent
    my $increases;
    my $result = $dbh->selectall_arrayref("select account_vw.name, account_vw.id, desk_vw.increase_date, desk_vw.current_price, cust_pers_tbl.email from account_vw, desk_vw, cust_pers_tbl where (desk_vw.increase_date - current_date) = ".INCREASE_REMINDER." and desk_vw.account_id = account_vw.id and account_vw.primary_contact_person_id=cust_pers_tbl.id and desk_vw.id in (select id from desk_vw where floor_id in (select id from floor_vw where building_id in (select id from building_tbl where state = '".$current_state."')))");
    foreach (@$result) {
      my $company_name = $_->[0];
      my $increase_date = $_->[2];
      my $current_price = $_->[3];
      my $email = $_->[4];
      my $days = INCREASE_REMINDER;
      my $msg_content = <<"---END_BLOCK---;";
$company_name is up for increase in $days days on $increase_date. They currently pay $current_price. Their email address is $email. Please review the license agreement details.
---END_BLOCK---;
      $increases .= $msg_content . "\n";
    }
    my $to_email_address = INCREASE_RECIPIENT;
    my $subject = "Increase Reminder Summary - ".$current_state;
    $mail_sender->MailMsg({
    "to" => $to_email_address,
    "subject" => $subject,
    "msg" => $increases,
    });
  }
}

sub send_renewals($) {
  my $dbh = shift;
  my $mail_sender = shift;
  my $current_state = shift;
  my $renewal_reminder = shift;
  my $renewals;
  my $result = $dbh->selectall_arrayref("select account_vw.name, account_vw.id, desk_vw.end_date, desk_vw.current_price, cust_pers_tbl.email, desk_vw.assigned_number, floor_vw.short_global_label, desk_vw.end_date - current_date from account_vw, desk_vw, cust_pers_tbl, floor_vw where (desk_vw.end_date - current_date) between ".$renewal_reminder->[0]." and  ".$renewal_reminder->[1]." and desk_vw.account_id = account_vw.id and account_vw.primary_contact_person_id=cust_pers_tbl.id and floor_vw.id=desk_vw.floor_id and floor_vw.building_id in (select id from building_tbl where state = '".$current_state."')");
  foreach (@$result) {
    my $company_name = $_->[0];
    my $end_date = $_->[2];
    my $current_price = $_->[3];
    my $email = $_->[4];
    my $desk_assigned_number = $_->[5];
    my $floor_label = $_->[6];
    my $days = $_->[7];
    my $msg_content = <<"---END_BLOCK---;";
$company_name (Desk: $desk_assigned_number, Floor: $floor_label) is up for renewal in $days days on $end_date and they may also be due for the standard rent increase of 5%. They currently pay $current_price. Please review the license agreement details and execute a renewal agreement right away. Their email address is $email.
---END_BLOCK---;
    $renewals .= $msg_content . "\n";
  }

  my $to_email_address = RENEWALS_RECIPIENT;
  my $subject = "Renewal Reminder Summary - ".$current_state;
  $mail_sender->MailMsg({
	  "to" => $to_email_address,
	  "subject" => $subject,
	  "msg" => $renewals,
  });
}

#!/usr/bin/perl

use strict;
local($^W) = 1;

use DBI;
use FindBin;
use lib "$FindBin::Bin/../lib";
use send_email;
use update_row;

main();

sub init_database() {
  my $database_name = "microoffice";
  my $database_hostname = "kleene.microoffice.com";
  my $data_source = "dbi:Pg:dbname=$database_name;host=$database_hostname;sslmode=require";
  my $database_username = "mo_operator";
  my $database_password = "***";
  my $dbh = DBI->connect($data_source, $database_username, $database_password);
  return $dbh;
}

sub main() {
  my $dbh = init_database();
  # if process_id is not null and sent_dt is null, some other send_emails.pl process is running.
  my $sth;
  $sth = $dbh->prepare("select * from email_tbl where process_id is not null and sent_dt is null");
  $sth->execute();
  my @pending_email_list;
  while(my $row = $sth->fetchrow_hashref()) {
    push(@pending_email_list, $row);
  }
  $sth->finish();
  # if there are emails being processed by another send_emails.pl process, exit. Begin on next cron job invocation. Technically there is a small race
  # condition, but we do not care because there is no harm from multiple processes running. We just do not want too many running at once.
  if (@pending_email_list) {
    $dbh->disconnect();
    return;
  }
  my $process_id = $$;
  # tag pending email rows with our process id so no other processes try to work on them.
  $sth = $dbh->prepare("update email_tbl set process_id = ? where process_id is null and sent_dt is null");
  $sth->bind_param(1, $process_id, SQL_INTEGER);
  $sth->execute();
  $sth->finish();
  # fetch the tagged rows.
  $sth = $dbh->prepare("select * from email_tbl where process_id = ? and sent_dt is null");
  $sth->bind_param(1, $process_id, SQL_INTEGER);
  $sth->execute();
  while(my $row = $sth->fetchrow_hashref()) {
    push(@pending_email_list, $row);
  }
  $sth->finish();
  # process the pending emails.
  for my $email (@pending_email_list) {
    send_email
      ("from" => $email->{"from_hdr"},
       "to" => $email->{"to_hdr"},
       "subject" => $email->{"subject_hdr"},
       "message" => $email->{"message_body"});
    my $email_id = $email->{"id"};
    my @sql_column_list;
    push(@sql_column_list, ["sent_dt", ["now()"]]);
    update_row($dbh, "email_tbl", \@sql_column_list, $email_id);
  }
  $dbh->disconnect();
}

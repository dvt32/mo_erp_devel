# variables needed by the build/install makefiles

# 0 for development environment, 1 for production environment
is_production_deployment=

# secret_base_dir only needed if is_production_deployment is true. secrets not needed for development.
secret_base_dir=

# intermediate_cert_filename is for SSL certificates in Apache httpd (only for production, not for deployment)
intermediate_cert_filename=comodo_bundle.crt

# tz_area is first level under /usr/share/zoneinfo
tz_area=America
# tz_zone is second level under /usr/share/zoneinfo (i.e. under tz_area)
tz_zone=New_York

# leave use_systemd blank to auto-detect (sometimes wrong); set to 0 to use old-style init scripts
use_systemd=

# how many hours ACH transactions are delayed.
pay_ach_delay_hours=24

# temp directory to store files in the build that aren't needed
build_cache_dir=/tmp/build_cache

# mail server for sending outbound mail
outbound_mail_server=mailout-int.microoffice.com
outbound_auth_mail_server=
outbound_mail_user=
outbound_mail_pw=

# ------------------------------------
# ######## Client Pay Site ##########
# ------------------------------------

# Apache httpd IPv4 and IPv6 addresses to bind to for virtual host pay
pay_ipv4_addr=
pay_ipv6_addr=
# Apache httpd server name
pay_fqdn=pay.microoffice.com
# base url to be used in email links and resource access
pay_base_url=
# ???
pay_server_name=

# WSGI Group Name for Python; in Apache conf files, automatically append v4 and v6 for the IPv4 and IPv6 processes
pay_wsgi_group_name=pay
# On or Off depending on whether automatic reloading of scripts (based on timestamp) is desired
pay_wsgi_script_reloading=On
# pay deployment directory
# pay deployment
pay_basedir=/var/www/sites/$(pay_fqdn)
pay_commondir=/var/www/lib/pay
# daemon directory for error monitoring
pay_daemon_basedir=/home/deploy
# secret key for Flask deployment for pay web site
pay_flask_secret_key=
# secret key for itsdangerous
pay_itsdangerous_secret_key=
# either True or False - best to set to False for production except during brief moments when needed
pay_flask_app_debug=


#payment processor database
pmt_proc_db_host=localhost
pmt_proc_db_sslmode=disable
pmt_proc_db_name=microoffice_devel
pmt_proc_db_username=
pmt_proc_db_password=

##### Site Variables ######

# convenience percentage for credit card transactions
pay_card_convenience_pct=3
# number of days to confirm an account
pmt_confirm_window=15


# --------------------------------------
# ######## Payment Processor ##########
# --------------------------------------

# ???
payment_deploy_basedir=/home/deploy
# payment deploy directory
payapi_basedir=/var/www/sites/payapi.microoffice.com
payapi_ip_addr=127.0.0.1
payapi_fqdn=payapi.microoffice.com
payapi_base_url=http://localhost:2080
payapi_server_name=http://localhost:2080

payapi_wsgi_group_name=payapi
payapi_wsgi_script_reloading=On

payapi_flask_secret_key=
payapi_flask_app_debug=True

vend_pmt_db_host=localhost
vend_pmt_db_sslmode=disable
vend_pmt_db_name=microoffice_devel
vend_pmt_db_username=
vend_pmt_db_password=

vend_mail_from=billing@microoffice.com
vend_mail_bcc=billing@microoffice.com


# --------------------------------
# ######## Crypto Site ##########
# --------------------------------

crypto_server_name=http://localhost:3080
crypto_basedir=/var/www/sites/crypto.microoffice.com
crypto_base_url=http://localhost:3080
crypto_fqdn=crypto.microoffice.com
crypto_ip_addr=127.0.0.1

crypto_wsgi_group_name=crypto
crypto_wsgi_script_reloading=On

crypto_flask_secret_key=
crypto_flask_app_debug=True

#crypto site databases
crypto_db_hostname=localhost
crypto_db_sslmode=disable
crypto_db_name=microoffice_devel
crypto_db_username=
crypto_db_password=

# --------------------------------
# ######## Static Site ##########
# --------------------------------
static_basedir=/var/www/sites/static.microoffice.com
static_ipv4_addr=
static_ipv4_port=
static_ipv6_addr=
static_fqdn=static.microoffice.com
static_base_url=
static_server_name=

reserve_room_refresh_delay=60
operations_flask=201605_flask

# external resources
jquery_url=http://devrc.int.microoffice.com/jquery/jquery-2.2.4.min.js
jquery_ui_url= http://devrc.int.microoffice.com/jquery/jquery-ui-1.11.4.zip
jquery_val_url=http://devrc.int.microoffice.com/jquery/jquery-validation-1.15.0.zip
bootstrap_url=http://devrc.int.microoffice.com/bootstrap/bootstrap-3.3.6-dist.zip
jasny_bootstrap_url=http://devrc.int.microoffice.com/bootstrap/jasny-bootstrap-3.1.3-dist.zip


# -------------------------------
# ######## Apply Site ##########
# -------------------------------
apply_basedir=/var/www/sites/apply.microoffice.com
apply_ipv4_addr=
apply_ipv4_port=
apply_ipv6_addr=
apply_fqdn=apply.microoffice.com
apply_base_url=
apply_http_server_name=

apply_wsgi_group_name=apply
apply_wsgi_script_reloading=On

apply_itsdangerous_secret_key=

apply_flask_secret_key=
apply_flask_app_debug=True

# how long a single use link will last. eventually should be under operations site
single_use_max_age=604800

##### Site Vars #####
# apply site properties
payment_mail_from=billing@microoffice.com

payment_mail_to=

payment_mail_bcc=

payment_mail_to_demo=

daypass_mail_bcc=billing@microoffice.com

daypass_mail_from=billing@microoffice.com

#email to send if user tries to sign up who is not in cust_pers_tbl
register_attempt_email=
# account to email if there's a mismatch in
account_mismatch_to=billing@microoffice.com

# email to send information in demo mode
demo_email=
bugs_email=

#apply site databases
apply_db_host=localhost
apply_db_sslmode=disable
apply_db_name=microoffice_devel
apply_db_username=mo_apply
apply_db_password=

# -----------------------------------
# ######## Netauth Portal ##########
# -----------------------------------

netauth_basedir=/var/www/sites/netauth.microoffice.com
netauth_ipv4_addr=
netauth_ipv6_addr=
netauth_fqdn=netauth.microoffice.com
netauth_base_url=
netauth_server_name=

netauth_itsdangerous_secret_key=

netauth_wsgi_group_name=portal
netauth_wsgi_script_reloading=On

netauth_flask_secret_key=
netauth_flask_app_debug=True


##### Router Site #####
router_basedir=/var/www/sites/router.microoffice.com
# depends on router, set there
router_ipv4_addr=
# depends on router
router_fqdn=
# https://fqdn
router_base_url=
# fqdn
router_server_name=

router_wsgi_group_name=router
router_wsgi_script_reloading=On

router_flask_secret_key=
router_flask_app_debug=True

max_user_token_age=300

netauth_subnets=192.168.122.0/24 10.192.243.0/24

net_interfaces=eth0 eth1 eth2

netauth_db_host=localhost
netauth_db_sslmode=disable
netauth_db_name=microoffice_devel
netauth_db_username=
netauth_db_password=





# NMI credentials for SoluPay transactions (add more numbers as needed, up to five)
nmi_username_1=
nmi_password_1=
nmi_group_1=
nmi_username_2=
nmi_password_2=
nmi_group_2=
nmi_username_3=
nmi_password_3=
nmi_group_3=
nmi_username_4=
nmi_password_4=
nmi_group_4=
nmi_username_5=
nmi_password_5=
nmi_group_5=

# Apache httpd IP address to bind to for virtual host operations
operations_ip_addr=*
# Apache httpd server name
operations_fqdn=operations.local

# Apache httpd IP address to bind to for virtual host bizcenter
bizcenter_ip_addr=*
# Apache httpd server name
bizcenter_fqdn=bizcenter.local

# webmaster email address to let users email us when error occurs
webmaster_email=webmaster@microoffice.com

# operations_employee_x properties apply only for development deployments, not for production deployments
# ID value of employee that should be updated in sample database
operations_existing_employee_id=41
# new value for first name of employee with ID above
operations_employee_first_name=Faried
# new value for last name of employee with ID above
operations_employee_last_name=Nawaz
# new value for Windows logon name for employee row with ID above
operations_employee_logon=fnawaz
# new value for password for employee; keep it alphanumeric characters to eliminate need to escape characters
operations_employee_password=a

# bizcenter_cust_x properties apply only for development deployments, not for production deployments
# ID value of customer that should be updated in sample database
bizcenter_existing_cust_id=1
# new value for first name of customer with ID above
bizcenter_cust_first_name=Faried
# new value for last name of customer with ID above
bizcenter_cust_last_name=Nawaz
# new value for Windows logon name for customer row with ID above
bizcenter_cust_email=fnawaz@bizcenter.local
# new value for password for customer; keep it alphanumeric characters to eliminate need to escape characters
bizcenter_cust_password=a

# databases
main_database_hostname=localhost
main_database_sslmode=disable
main_database_name=microoffice_devel
main_database_pay_username=
main_database_pay_password=
main_database_operations_username=wchao
main_database_operations_password=abc
main_database_bizcenter_username=wchao
main_database_bizcenter_password=abc
main_database_investor_username=wchao
main_database_investor_password=abc
main_database_kiosk_username=wchao
main_database_kiosk_password=abc
#admin credentials
admin_db_username=
admin_db_password=
# properties related to sample database for development deployment
database_cache_dir=/tmp/dbsample
main_database_create_as_username=postgres
# number of parallel threads in pg_restore of dbsample.
# for fastest dbsample load, set to # of processors shown with cat /proc/cpuinfo.
dbsample_num_jobs=8

phone_database_hostname=localhost
phone_database_sslmode=disable
phone_database_name=microoffice
phone_database_username=wchao
phone_database_password=abc

call_log_database_hostname=telco.int.stellanetworks.com
call_log_database_sslmode=disable
call_log_database_name=call_log
call_log_database_username=cluser
call_log_database_password=clpass

# common perl pm files
perl_commondir=/var/www/lib/perl/1g

# operations deployment
operations_basedir=/var/www/sites/$(operations_fqdn)
operations_commondir=/var/www/lib/operations
# cakephp
operations_2g_urlprefix=/2g/webroot/index.php
# js, css, img, etc.
operations_2g_assetprefix=/2g/webroot

# investor site deployment
investor_basedir=/var/www/sites/investor.microoffice.com
investor_commondir=/var/www/lib/investor

# bizcenter site deployment
bizcenter_basedir=/var/www/sites/$(bizcenter_fqdn)
bizcenter_commondir=/var/www/lib/bizcenter
bizcenter_2g_urlprefix=/2g/webroot/index.php
bizcenter_2g_assetprefix=/2g/webroot

# new php code
php_base_url=/3g

# misc accounts
faxage_username=faxuser
faxage_password=fauxpas
faxage_company=corpinc

dav_base_url=/dav
dav_username=dave
dav_password=nopas

quickbooks_login_name=quick
quickbooks_password=books

wifi_ssid=hidden
wifi_password=unknown

intellim_username=intel
intellim_password=lim
intellim_base_url=http://int.int

mandrill_api_key=mkey

docusign_email=someone@somewhere
docusign_password=doc
docusign_integrator_key=dkey
# v1, v2
docusign_version=v2
# environment, e.g.: demo, test, www
docusign_environment=demo
# DocuSign Account ID (optional). for multiple accounts user: if empty, default account will be used; otherwise, use account ID
docusign_account_id=
docusign_base_url=http://doc.int

# Sales Order Form
sof_library=/var/www/lib/salesorderform
sof_basedir=/var/www/sites/$(operations_fqdn)
sof_db_file=sofconstants.php

# scramble properties for tools/db/*.py
scramble_dbname=microoffice
scramble_dbhost=localhost
scramble_dbuser=username
scramble_dbpass=password

# need to double any percent signs (%) because Make interprets them specially.
# check substitution to see if any other chars need to be escaped.
# "openssl rand -base64 32" or "tr -cd '[:alnum:]' < /dev/urandom | fold -w44 | head -n1"
operations_3g_yii_cookie_validation_key=blah

# Essensys API integration
essensys_client_id=trustedclient
essensys_client_secret=
essensys_user=
essensys_password=

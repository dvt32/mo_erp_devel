#!/usr/bin/python3
"""Perform substitution of properties in one or more files."""

import sys
import argparse
import configparser
import jinja2
import re
import os

def main(argv):
  argparser = argparse.ArgumentParser(description='Perform substitution of properties in one or more files. Input files must be in Unicode encoding.')
  argparser.add_argument('--makefile', help='properties from propertyfile are in Makefile format (meaning single backslash should be stripped)', action='store_true')
  argparser.add_argument('--preservetime', help='preserve timestamp on target file(s)', action='store_true')
  argparser.add_argument('--setproperty', help='set a property in key=value format; can be multiple', action='append')
  argparser.add_argument('--listinfile', help='targetfile should be interpreted as a file containing a list of files, one per line', action='store_true')
  argparser.add_argument('propertyfile', help='file containing properties in INI format', type=argparse.FileType('r'))
  argparser.add_argument('targetfile', help='one or more files needing substitutions', nargs='+')
  args = argparser.parse_args()
  confparser = configparser.ConfigParser()
  # override optionxform to eliminate case-insensitivity by omitting lowercase transformation
  confparser.optionxform = lambda option: option
  # fake section name so we can use ConfigParser functionality for INI file parsing instead of writing our own
  properties_text = '[root]\n'
  properties_text += args.propertyfile.read()
  # if makefile option is specified, then propertyfile needs backslashes stripped (every backslash followed by character is replaced with the character after the backslash)
  if args.makefile:
    properties_text = re.sub(r'\\(.)', r'\1', properties_text)
  # if there are manually-specified properties on the command line, add them to the INI file.
  # since they come later, they will overwrite properties in the property file.
  if args.setproperty != None:
    for property in args.setproperty:
      # each property should be in key=value format
      properties_text += property + '\n'
  confparser.read_string(properties_text)
  # drop one level lower than the fake 'root' section
  property_dict = confparser['root']
  # some values in property file may reference other properties using $(propertyname)
  # resolve those references now
  var_ref_re = re.compile(r'\$\(([^)]+)\)')
  property_dict_keys = property_dict.keys()
  for key in property_dict_keys:
    match = var_ref_re.search(property_dict[key])
    while match:
      _var_name = match.group(1)
      if _var_name in property_dict_keys:
        property_dict[key] = re.sub(r'\$\('+_var_name+r'\)', property_dict[_var_name], property_dict[key])
      else:
        raise Exception('substprop.py: unable to substitute for $(%s) because it is an unknown property in %s' % (_var_name, args.propertyfile.name))
      match = var_ref_re.search(property_dict[key])
  if args.listinfile:
    file_list = []
    for list_filename in args.targetfile:
      with open(list_filename, 'r') as file_containing_list:
        for line in file_containing_list:
          file_path = line.lstrip().rstrip()
          if file_path != '':
            file_list.append(file_path)
  else:
    file_list = args.targetfile
  # loop through the target files
  for target_filename in file_list:
    target_file = open(target_filename, 'r+')
    try:
      raw_target = target_file.read()
    except:
      print('substprop.py: error reading file %s' % target_filename)
      raise
    try:
      # StrictUndefined raises exception when variables are missing
      target_template = jinja2.Template(raw_target, keep_trailing_newline=True, undefined=jinja2.StrictUndefined)
      rendered_target = target_template.render(property_dict)
    except:
      print('Jinja problem while rendering file %s' % target_filename)
      raise
    if raw_target != rendered_target:
      if args.preservetime:
        orig_atime = os.path.getatime(target_filename)
        orig_mtime = os.path.getmtime(target_filename)
      target_file.seek(0)
      target_file.write(rendered_target)
      target_file.truncate()
      if args.preservetime:
        os.utime(target_filename, (orig_atime, orig_mtime))
    target_file.close()
  args.propertyfile.close()

if __name__ == "__main__":
  main(sys.argv[1:])

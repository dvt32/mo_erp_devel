#!{{ freeswitch_basedir }}/venv/bin/python3
import os
from os.path import basename
from pydub import AudioSegment
import psycopg2
import psycopg2.extras
from datetime import datetime
import smtplib
import email.utils
import email.header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import re

max_duration = {{ freeswitch_max_recording_duration }} * 1000

def main():
  base_dir = '{{ freeswitch_basedir }}/recordings/'
  conn = get_connection()
  curdict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
  curdict.execute('SELECT * FROM recording_email_tbl')
  # Files must strictly follow format YYYY-MM-DD-HH-MM-SS_#########_#########.wav
  # They are skipped otherwise
  pattern = r'\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}_.+_.+\.wav$'
  for row in curdict:
    dir_path = os.path.join(base_dir,row['recording_dir'])
    if not os.path.isdir(dir_path):
      continue
    for file in os.listdir(dir_path):
      if re.search(pattern, file):
        file_path = os.path.join(dir_path, file)
        audio = AudioSegment.from_wav(file_path)

        # Split the audio if necessary
        if len(audio) > max_duration:
          for i in range(0, len(audio), max_duration):
            mp3_file_name = file_path[:-4] + '-part-{:02d}.mp3'.format(int(i / max_duration) + 1)
            with open(mp3_file_name, 'wb') as f:
              audio[i:i+max_duration].export(f, format='mp3')

            # Pass audio_part because it has been split into two or more pieces
            send_mail(row['email'], mp3_file_name, int(i/max_duration) + 1)
            os.remove(mp3_file_name)
        else:
          mp3_file_name = file_path[:-4] + '.mp3'
          audio.export(mp3_file_name, format='mp3')

          # Get user's email and send it
          send_mail(row['email'], mp3_file_name)
          os.remove(mp3_file_name)
        # Delete files (We're done with them.)
        os.remove(file_path)


def send_mail(mail_to, file, audio_part=None):
  ''' Sends an email to the user with file as attachment '''
  # Extract information from file name, [:-4] removes trailing .wav
  time, destination_no, caller_id_no = file[:-4].split('/')[-1].split('_')

  # remove trailing
  caller_id_no = caller_id_no

  # Remove
  if '-' in caller_id_no:
    caller_id_no = caller_id_no.split('-')[0]

  # Reformat time into MM/DD/YY HH:MM AM
  time = datetime.strptime(time, '%Y-%m-%d-%H-%M-%S').strftime('%m/%d/%y %I:%M %p')

  mail_msg = MIMEMultipart('alternative')
  html_body = text_body = 'Please find attached the recording of your call at time %s to number %s with caller id number %s. Note that we do not keep a copy of this recording and it is deleted from our servers after being sent. We encourage you to save this file locally.' % (time, destination_no, caller_id_no)
  text = MIMEText(text_body, 'plain')
  html = MIMEText(html_body, 'html')
  mail_msg.attach(text)
  mail_msg.attach(html)
  mail_msg['Date'] = email.utils.formatdate(localtime=True)
  mail_msg['From'] = '{{ freeswitch_mail_from }}'
  # emails are stored in the database as comma separated values and validate at the db level
  mail_msg['To'] = ", ".join(mail_to.split(','))


  with open(file, "rb") as fil:
    part = MIMEApplication(
      fil.read(),
      Name=basename(file)
    )
    # After the file is closed
    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(file)
    mail_msg.attach(part)

  subject = 'Recording of call on %s to %s with caller ID %s' % (time, destination_no, caller_id_no)
  if audio_part is not None:
    subject += ' part ' + str(audio_part)
  mail_msg['Subject'] = email.header.Header(subject, 'utf-8')
  mailer = smtplib.SMTP('{{ outbound_mail_server }}')
  mailer.send_message(mail_msg)
  mailer.quit()

def get_connection():
  return psycopg2.connect(database='{{ freeswitch_db_name }}', user='{{ freeswitch_db_username }}', password='{{ freeswitch_db_password }}', host='{{ freeswitch_db_host }}', sslmode='{{ freeswitch_db_sslmode }}')

if __name__=='__main__':
  main()

include $(repo_root)/common/apache.mk
include $(repo_root)/common/firewall.mk
include $(repo_root)/common/func.mk
include $(repo_root)/common/postgresql.mk
include $(repo_root)/common/python.mk

.PHONY: websetup install clean

# libcurl4-openssl-dev needed by pycurl
deb_packages := libcurl4-openssl-dev incron
# Apache modules rewrite and ssl needed for SSL; headers needed for HSTS (in conjunction with SSL)
apache_modules := rewrite headers
# pycurl needed for solupay library. jinja2 needed despite jinja2_pkg dependency because we need jinja2 in pip

websetup: apache_pkg apache_start_on_boot iptables_pkg iptables_conf jinja2_pkg
	$(call check_system_properties,transition_basedir transition_ipv4_addr transition_fqdn)
ifneq ($(linux_distribution),debian)
	$(error only debian distribution is supported in websetup-transition target)
endif
	@mkdir -p $(transition_basedir)
# install required packages in APT repos
	@$(call install_packages,$(deb_packages))
	@$(call install_apache_site,"$(CURDIR)/apacheconf/production/transition.conf",transition,$($@_tmpdir),$(system_properties))
	@$(call enable_apache_modules,$(apache_modules),$($@_tmpdir))
	@$(call restart_apache_if_needed,$($@_tmpdir))
	@echo Create DNS mapping from $(transition_fqdn) to IP address \($(transition_ipv4_addr)\)

install: jinja2_pkg
	$(call check_system_properties,transition_basedir transition_base_url)
	$(eval _file_list:=)
	@echo ..create deployment directories for command line clients
	@for i in templates; do mkdir -p $(transition_basedir)/$$i; chown www-data.www-data $(transition_basedir)/$$i; done
	@echo ..create web deployment directories for transition site
	@$(foreach file,$(wildcard $(CURDIR)/templates/*),$(call copy_file,$(file),$(transition_basedir)/$(notdir $(file)),www-data.www-data,0755);)

clean:
	$(call check_system_properties,build_cache_dir transition_basedir)
	@rm -fr $(transition_basedir)
	@$(call rm_apache_site,transition,$($@_tmpdir))
	@$(call restart_apache_if_needed,$($@_tmpdir))

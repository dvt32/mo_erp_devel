#!/usr/bin/python3
import sys
import os
import argparse
import csv
import datetime
from decimal import Decimal, getcontext
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
import email.utils
import email.header
import smtplib
from email import encoders

worker_email = 'jrienstra@microoffice.com'
'''
To make using this easier to use, add the following to ~/.bachrc, then reload bash
with 'source ~/.bashrc'. This way, you can type work [arg] anywhere on the machine and it'll 
execute this file with the argument you passed.
alias work='function _work(){ ~/projects/mo_erp/util/work.py $1; };_work'
Replace ~/projects/mo_erp/util/work.py with the full path to the work.py file

'''
def main():
  getcontext().prec = 3 # set decimal precision
  # set parameters for csv file
  fieldnames = ['start', 'end', 'time']
  header = {'start': 'start', 'end': 'end', 'time': 'time'}
  # parse arguments
  argparser = argparse.ArgumentParser(description='Log work time')
  argparser.add_argument('action', help='start, end, print, clear, or check')
  args = argparser.parse_args()
  # start
  if args.action == 'start':
    # check if the file already exists or not
    newfile = not os.path.isfile('/root/hour_logs/hours.csv') 
    with open('/root/hour_logs/hours.csv', 'a+', newline='') as f:
      # we have to read and write, for error checking
      writer = csv.DictWriter(f, fieldnames)
      reader = csv.DictReader(f)
      # a+ allows read/write without overwriting
      # but it sets the pointer at the end of the file
      # so we have to set it back to the beginning
      f.seek(0)
      # if newfile, write the header
      if newfile:
        writer.writerow(header)
      # back to the beginning
      f.seek(0)
      # check there are no incomplete rows
      for row in reader:
        if row['end'] == '':
          print("You haven't ended your previous session yet.")
          return
      # back to the beginning
      print("Starting work session...")
      # enter current time
      writer.writerow({'start': datetime.datetime.now()})
      print("Session started.")
      return
  # if ending a session or checking time. they share a lot of code
  elif args.action == 'end' or args.action == 'check':
    with open('/root/hour_logs/hours.csv', 'r+', newline='') as f:
      # we only need to read at this point
      reader = csv.DictReader(f)
      # put rows in a list, so we iterate through them easier
      # we will then rewrite the file with these rows after modification
      rows = [dict(row) for row in reader]
      # initialize counters
      time = 0
      total = 0
      # we assume there is an active session
      active = True;
      for i, row in enumerate(rows):
        # error checking
        # if incomplete and not the last row
        if i != len(rows) - 1 and row['end'] == '':
          print("There was an error, and a pxrevious row is incomplete. Perhaps you didn't end before starting.")
          return
        # if last row is complete (no active session)
        elif i == len(rows) - 1 and row['end'] != '':
          # if you're trying to end, you cannot
          if args.action == 'end':
            print("You haven't started a new session that you can end.")
            return
          # if you're checking, we mark there is no active session, then add the time
          elif args.action == 'check':
            total += Decimal(row['time'])
        # last row and incomplete
        elif i == len(rows) - 1 and row['end'] == '':
          if args.action == 'end':
            # set row end data
            row['end'] = datetime.datetime.now()
            # parse start value, get difference
            date_time = row['end'] - datetime.datetime.strptime(row['start'], '%Y-%m-%d %H:%M:%S.%f')
            # total time in minutes
            row['time'] = (Decimal(date_time.total_seconds()) / Decimal(60))
            # save to print the time from this session
            time = row['time']
            # add to total time
            total += time
            continue
          elif args.action == 'check':
            date_time = datetime.datetime.now() - datetime.datetime.strptime(row['start'], '%Y-%m-%d %H:%M:%S.%f')
            time = (Decimal(date_time.total_seconds()) / Decimal(60))
            total += time
        # else this is a previous complete row. just add the minute total
        else:
          total += Decimal(row['time'])
      # loop is done
      else:
        if args.action == 'end':
          if time == 0:
            print("Some other error occured. Check the csv file %s" % f.name)
            return
          # else everything is alright
          else:
            print("Ending work session...")
            # go to beginning of file
            f.seek(0)
            # create writer
            writer = csv.DictWriter(f, fieldnames)
            # write header
            writer.writerow(header)
            # write all rows
            writer.writerows(rows)
            print("Work session ended. Total time was %s minutes = %s hours." % (time, time / Decimal(60)))
            print("Total for this period is %s hours." % (total / Decimal(60)))
            return
        elif args.action == 'check':
          if time == 0:
            print("Total for this period is %s hours." % (total / Decimal(60)))
            return
          else:
            print("Current work session time is %s minutes = %s hours." % (time, time / Decimal(60)))
            print("Total for this period is %s hours." % (total / Decimal(60)))
            return
  # prints stats, sends hours as csv file by email, renames hour log file for storage
  elif args.action == 'print':
    fname = ''
    with open('/root/hour_logs/hours.csv', 'r+', newline='') as f:
      reader = csv.DictReader(f)
      # load rows of file into list of dicts
      rows = [dict(row) for row in reader]
      # get total time
      total = sum(Decimal(row['time']) for row in rows)
      print("Total time of %s hours this week" % (total / Decimal(60)))
      # get start and end dates for file name
      start = datetime.datetime.strptime(rows[0]['start'], '%Y-%m-%d %H:%M:%S.%f')
      start = str(start.date()).replace('-','')
      end = datetime.datetime.strptime(rows[len(rows)-1]['start'], '%Y-%m-%d %H:%M:%S.%f')
      end = str(end.date()).replace('-','')
      fname = 'hours.' + start + "_" + end + '.csv'
      # go to end of file (position 0 from end of file)
      f.seek(0, 2)
      writer = csv.writer(f)
      # write an extra row with total time, this time in hours
      writer.writerow(['Total Hours: %s' % (total / Decimal(60))])
    # multipart for files
    mail_msg = MIMEMultipart()
    # set email info. eventually may want to add html or something to make it look less suspect
    mail_body = 'Here are my hours! (see the file timestamps for start and end dates) \n\n'
    mail_msg['Date'] = email.utils.formatdate(localtime=True)
    mail_msg['From'] = 'it@microoffice.com'
    mail_msg['Cc'] = 'wchao@microoffice.com,' + worker_email
    mail_msg['To'] = 'rsharma@microoffice.com'
    mail_msg['Subject'] = email.header.Header('Jacob Rienstra Hours', 'utf-8')
    mail_msg.attach(MIMEText(mail_body))
    # get log file, attach it
    with open('/root/hour_logs/hours.csv', 'rb') as f:
      part = MIMEBase('application', "octet-stream")
      part.set_payload( f.read() )
      encoders.encode_base64(part)
      part.add_header('Content-Disposition', 'attachment', filename=fname)
      mail_msg.attach(part)
    # send mail
    mailer = smtplib.SMTP('mailout-int.microoffice.com')
    mailer.send_message(mail_msg)
    mailer.quit()
    print("Hours sent!")
    # move the file to a record keeping place
    os.rename('/root/hour_logs/hours.csv', '/root/hour_logs/' + fname)
    return
  # only clears the current hour log. mainly for testing purposes.
  elif args.action == 'clear':
    if os.path.isfile('/root/hour_logs/hours.csv'):
      os.remove('/root/hour_logs/hours.csv')
    return
  else:
    print("Not a valid argument")


if __name__ == "__main__":
  main()
